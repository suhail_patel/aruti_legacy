﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImageWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImageWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    Private mintImgRefId As Integer = 0

#End Region

#Region " Enums "

    Public Enum enImageMode
        IMG_EMPLOYEE = 1
        IMG_DEPENDANT = 2
        IMG_APPLICANT = 3
    End Enum

#End Region

#Region " Display Dialog "

    Public Sub displayDialog(ByVal eIMode As enImageMode)
        Try
            mintImgRefId = eIMode
            Call SetVisibility(eIMode)
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImageWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImageWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportImgs_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportImgs.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportImgs_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportImgs.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If ctrl.Visible = True Then
                                    If CType(ctrl, ComboBox).Text = "" Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data to."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                        e.Cancel = True
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Next

                        For Each ctrl As Control In objpnlData.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If ctrl.Visible = True Then
                                    If CType(ctrl, ComboBox).Text = "" Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data to."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                        e.Cancel = True
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Next
                    End If
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetVisibility(ByVal iRefId As Integer)
        Try
            Select Case iRefId
                Case enImageMode.IMG_EMPLOYEE
                    lblDLastname.Visible = False : cboDpndt_Lastname.Visible = False : objlblSign7.Visible = False
                    lblDFirstname.Visible = False : cboDpndt_Firstname.Visible = False : objlblSign5.Visible = False
                    lblApplicant.Visible = False : cboApplicantCode.Visible = False : objlblSign6.Visible = False
                    objpnlData.Location = New Point(14, 129)
                Case enImageMode.IMG_DEPENDANT
                    lblDFirstname.Visible = True : cboDpndt_Firstname.Visible = True : objlblSign5.Visible = True
                    lblDLastname.Visible = True : cboDpndt_Lastname.Visible = True : objlblSign7.Visible = True
                    lblApplicant.Visible = False : cboApplicantCode.Visible = False : objlblSign6.Visible = False
                    objpnlData.Location = New Point(14, 183)
                Case enImageMode.IMG_APPLICANT
                    lblEmployeeCode.Visible = False : cboEmployeeCode.Visible = False : objlblSign1.Visible = False
                    lblDFirstname.Visible = False : cboDpndt_Firstname.Visible = False : objlblSign5.Visible = False
                    lblDLastname.Visible = False : cboDpndt_Lastname.Visible = False : objlblSign7.Visible = False
                    lblApplicant.Visible = True : cboApplicantCode.Visible = True : objlblSign6.Visible = True
                    objpnlData.Location = New Point(14, 129)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each ctrl As Control In objpnlData.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboPhoto_Name.Items.Add(dtColumns.ColumnName)
                cboPhoto_Path.Items.Add(dtColumns.ColumnName)
                cboApplicantCode.Items.Add(dtColumns.ColumnName)
                cboDpndt_Firstname.Items.Add(dtColumns.ColumnName)
                cboDpndt_Lastname.Items.Add(dtColumns.ColumnName)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 20, "Employeecode") Then
                    dtColumns.Caption = "Employeecode"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 21, "Photo_Path") Then
                    dtColumns.Caption = "Photo_Path"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 22, "Photo_Name") Then
                    dtColumns.Caption = "Photo_Name"
                End If
                'Gajanan (24 Nov 2018) -- End
            Next



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        ezWait.Active = True
        Try

            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            If mintImgRefId = enImageMode.IMG_DEPENDANT Then
                mdt_ImportData_Others.Columns.Add("dependant", System.Type.GetType("System.String"))
                mdt_ImportData_Others.Columns.Add("fname", System.Type.GetType("System.String"))
                mdt_ImportData_Others.Columns.Add("lname", System.Type.GetType("System.String"))
            End If
            mdt_ImportData_Others.Columns.Add("ppath", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pname", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                Select Case mintImgRefId
                    Case enImageMode.IMG_EMPLOYEE, enImageMode.IMG_DEPENDANT
                        drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                    Case enImageMode.IMG_APPLICANT
                        drNewRow.Item("ecode") = dtRow.Item(cboApplicantCode.Text).ToString.Trim
                End Select

                If mintImgRefId = enImageMode.IMG_DEPENDANT Then
                    drNewRow.Item("dependant") = dtRow.Item(cboDpndt_Firstname.Text).ToString.Trim & " " & dtRow.Item(cboDpndt_Lastname.Text).ToString.Trim
                    drNewRow.Item("fname") = dtRow.Item(cboDpndt_Firstname.Text).ToString.Trim
                    drNewRow.Item("lname") = dtRow.Item(cboDpndt_Lastname.Text).ToString.Trim
                End If

                drNewRow.Item("ppath") = dtRow.Item(cboPhoto_Path.Text).ToString.Trim
                drNewRow.Item("pname") = dtRow.Item(cboPhoto_Name.Text).ToString.Trim

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            If blnIsNotThrown = True Then
                If mintImgRefId = enImageMode.IMG_APPLICANT Then
                    colhEmployee.HeaderText = "Applicant"
                End If
                colhEmployee.DataPropertyName = "ecode"
                If mintImgRefId = enImageMode.IMG_DEPENDANT Then
                    dgcolhDependants.DataPropertyName = "dependant"
                Else
                    dgcolhDependants.Visible = False
                End If
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Select Case mintImgRefId
                Case enImageMode.IMG_EMPLOYEE
                    Call Import_Data_Employee()
                Case enImageMode.IMG_DEPENDANT
                    Call Import_Data_Dependant()
                Case enImageMode.IMG_APPLICANT

            End Select

            ezWait.Active = False
            eZeeWizImportImgs.BackEnabled = False
            eZeeWizImportImgs.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboPhoto_Path.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Photo Path cannot be blank. Please set Photo Path in order to import Employee Image(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboPhoto_Name.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Photo Name cannot be blank. Please set Photo Name in order to import Employee Image(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboDpndt_Firstname.Visible = True Then
                    If .Item(cboDpndt_Firstname.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Dependant Firstname cannot be blank. Please set Dependant Firstname in order to import Dependant Image(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboDpndt_Lastname.Visible = True Then
                    If .Item(cboDpndt_Lastname.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Dependant Lastname cannot be blank. Please set Dependant Lastname in order to import Dependant Image(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboApplicantCode.Visible = True Then
                    If .Item(cboApplicantCode.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Applicant Code cannot be blank. Please set Applicant Code in order to import Employee Image(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data_Employee()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim intEmpId As Integer = 0
            Dim objEImg As clsemp_Images
            Dim objEMaster As New clsEmployee_Master

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                intEmpId = 0

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmpId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING PATH PRESENT
                If dtRow.Item("ppath").ToString.Trim.Length > 0 Then
                    If IO.Directory.Exists(dtRow.Item("ppath")) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Photo Path Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If


                '------------------------------ CHECKING PHOTO PRESENT
                Dim sFile As String = String.Empty
                If dtRow.Item("pname").ToString.Trim.Length > 0 Then
                    If dtRow.Item("ppath").ToString.Length - 1 = dtRow.Item("ppath").ToString.LastIndexOf("\") Then
                        sFile = dtRow.Item("ppath").ToString & dtRow.Item("pname").ToString
                    Else
                        sFile = dtRow.Item("ppath").ToString & "\" & dtRow.Item("pname").ToString
                    End If
                    If IO.File.Exists(sFile) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Photo Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING PHOTO SIZE
                If ConfigParameter._Object._IsImgInDataBase Then
                    If System.IO.File.Exists(sFile) Then
                        Dim mintByes As Integer = 6291456    ' 6 MB = 6291456 Bytes
                        Dim fl As New System.IO.FileInfo(sFile)
                        If fl.Length > mintByes Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Sorry,You cannot upload file greater than ") & (mintByes / 1024) / 1024 & Language.getMessage(mstrModuleName, 14, " MB.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                objEImg = New clsemp_Images

                objEImg._Employeeunkid = intEmpId
                objEImg._Companyunkid = Company._Object._Companyunkid
                '------------------------------ CHECKING IF DATA PRESENT
                If objEImg.Import_IsExists = True Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Image Already Present.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                End If

                objEImg._Photo = ImageCompression(sFile)
                objEImg._Userunkid = User._Object._Userunkid
                objEImg._Isvoid = False
                objEImg._Voiddatetime = Nothing
                objEImg._Voiduserunkid = -1


                If objEImg.Import_Image() = True Then
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objEImg._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data_Employee", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub Import_Data_Dependant()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim intEmpId As Integer = 0
            Dim intDependantId As Integer = 0

            Dim objEMaster As New clsEmployee_Master
            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim objdependant_Images As clsdependant_Images

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                intEmpId = 0 : intDependantId = 0

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmpId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF DEPENDANT PRESENT.
                If dtRow.Item("dependant").ToString.Trim.Length > 0 Then
                    intDependantId = objDependant.GetDependantUnkid(intEmpId, dtRow.Item("fname"), dtRow.Item("lname"))
                    If intDependantId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Dependant Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING PATH PRESENT
                If dtRow.Item("ppath").ToString.Trim.Length > 0 Then
                    If IO.Directory.Exists(dtRow.Item("ppath")) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Photo Path Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If


                '------------------------------ CHECKING PHOTO PRESENT
                Dim sFile As String = String.Empty
                If dtRow.Item("pname").ToString.Trim.Length > 0 Then
                    If dtRow.Item("ppath").ToString.Length - 1 = dtRow.Item("ppath").ToString.LastIndexOf("\") Then
                        sFile = dtRow.Item("ppath").ToString & dtRow.Item("pname").ToString
                    Else
                        sFile = dtRow.Item("ppath").ToString & "\" & dtRow.Item("pname").ToString
                    End If
                    If IO.File.Exists(sFile) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Photo Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING PHOTO SIZE
                If ConfigParameter._Object._IsImgInDataBase Then
                    If System.IO.File.Exists(sFile) Then
                        Dim mintByes As Integer = 6291456    ' 6 MB = 6291456 Bytes
                        Dim fl As New System.IO.FileInfo(sFile)
                        If fl.Length > mintByes Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Sorry,You cannot upload file greater than ") & (mintByes / 1024) / 1024 & Language.getMessage(mstrModuleName, 14, " MB.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                objdependant_Images = New clsdependant_Images

                objdependant_Images._Employeeunkid = intEmpId
                objdependant_Images._Companyunkid = Company._Object._Companyunkid
                objdependant_Images._Dependantunkid = intDependantId

                '------------------------------ CHECKING IF DATA PRESENT
                If objdependant_Images.Import_IsExists = True Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Image Already Present.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                End If

                objdependant_Images._Photo = ImageCompression(sFile)
                objdependant_Images._Userunkid = User._Object._Userunkid
                objdependant_Images._Isvoid = False
                objdependant_Images._Voiddatetime = Nothing
                objdependant_Images._Voiduserunkid = -1


                If objdependant_Images.Import_Image() = True Then
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objdependant_Images._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data_Dependant", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub


#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Image Processing Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = Nothing
            lstCombos = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            lstCombos.AddRange(From p In objpnlData.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p))
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End


                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END


    'Gajanan (24 Nov 2018) -- Start
    'Enhancement : Import template column headers should read from language set for 
    'users (custom 1) and columns' date formats for importation should be clearly known 
    'UAT No:TC017 NMB
    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable
                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                'dTable.Columns.Add("Employeecode") : dTable.Columns.Add("Photo_Path") : dTable.Columns.Add("Photo_Name")
                With dTable
                    .Columns.Add(Language.getMessage(mstrModuleName, 20, "Employeecode"), System.Type.GetType("System.String"))
                    .Columns.Add(Language.getMessage(mstrModuleName, 21, "Photo_Path"), System.Type.GetType("System.String"))
                    .Columns.Add(Language.getMessage(mstrModuleName, 22, "Photo_Name"), System.Type.GetType("System.String"))
                End With
                'Gajanan (24 Nov 2018) -- End

                dsList.Tables.Add(dTable.Copy)
                OpenXML_Export(strFilePath, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
            dsList = Nothing
        End Try

    End Sub
    'Gajanan (24 Nov 2018) -- End

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region


 
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.eZeeWizImportImgs.CancelText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_CancelText" , Me.eZeeWizImportImgs.CancelText)
			Me.eZeeWizImportImgs.NextText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_NextText" , Me.eZeeWizImportImgs.NextText)
			Me.eZeeWizImportImgs.BackText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_BackText" , Me.eZeeWizImportImgs.BackText)
			Me.eZeeWizImportImgs.FinishText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_FinishText" , Me.eZeeWizImportImgs.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblPhotoName.Text = Language._Object.getCaption(Me.lblPhotoName.Name, Me.lblPhotoName.Text)
            Me.lblPhotoPath.Text = Language._Object.getCaption(Me.lblPhotoPath.Name, Me.lblPhotoPath.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblDFirstname.Text = Language._Object.getCaption(Me.lblDFirstname.Name, Me.lblDFirstname.Text)
            Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.dgcolhDependants.HeaderText = Language._Object.getCaption(Me.dgcolhDependants.Name, Me.dgcolhDependants.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblDLastname.Text = Language._Object.getCaption(Me.lblDLastname.Name, Me.lblDLastname.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data to.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s).")
            Language.setMessage(mstrModuleName, 4, "Photo Path cannot be blank. Please set Photo Path in order to import Employee Image(s).")
            Language.setMessage(mstrModuleName, 5, "Photo Name cannot be blank. Please set Photo Name in order to import Employee Image(s).")
            Language.setMessage(mstrModuleName, 6, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 7, "Fail")
            Language.setMessage(mstrModuleName, 8, "Photo Path Not Found.")
            Language.setMessage(mstrModuleName, 9, "Photo Not Found.")
            Language.setMessage(mstrModuleName, 10, "Success")
            Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 12, "Image Already Present.")
            Language.setMessage(mstrModuleName, 13, "Sorry,You cannot upload file greater than")
            Language.setMessage(mstrModuleName, 14, " MB.")
            Language.setMessage(mstrModuleName, 15, "Dependant Firstname cannot be blank. Please set Dependant Firstname in order to import Dependant Image(s).")
            Language.setMessage(mstrModuleName, 16, "Dependant Lastname cannot be blank. Please set Dependant Lastname in order to import Dependant Image(s).")
            Language.setMessage(mstrModuleName, 17, "Applicant Code cannot be blank. Please set Applicant Code in order to import Employee Image(s).")
            Language.setMessage(mstrModuleName, 18, "Dependant Not Found.")
			Language.setMessage(mstrModuleName, 19, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 20, "Employeecode")
			Language.setMessage(mstrModuleName, 21, "Photo_Path")
			Language.setMessage(mstrModuleName, 22, "Photo_Name")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class