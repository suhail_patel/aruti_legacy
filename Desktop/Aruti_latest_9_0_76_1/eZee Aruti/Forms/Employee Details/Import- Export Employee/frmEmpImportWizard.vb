﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading 'S.SANDEEP [ 18 SEP 2012 ]

#End Region

Public Class frmEmpImportWizard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpImportWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    'S.SANDEEP [ 01 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'S.SANDEEP [ 01 SEP 2012 ] -- END

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END

#End Region

#Region " From's Events "

    Private Sub frmEmpImportWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            'Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            txtFilePath.BackColor = GUI.ColorComp
            radDefinedSalary.Checked = True
            radActiveEmployee.Checked = True

            'Sohail (10 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._RetirementBy = 1 Then
                cboRetirementdate.Enabled = False
            Else
                cboRetirementdate.Enabled = True
            End If
            'Sohail (10 Apr 2013) -- End

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            Dim objHead As New clsTransactionHead
            Dim dsHead As DataSet = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 ", False, False, False, 2)
            If dsHead.Tables(0).Rows.Count > 0 Then
                chkAssignDefaulTranHeads.Checked = True
                chkAssignDefaulTranHeads.Visible = True
            Else
                chkAssignDefaulTranHeads.Checked = False
                chkAssignDefaulTranHeads.Visible = False
            End If
            'Sohail (18 Feb 2019) -- End

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            If ConfigParameter._Object._PolicyManagementTNA Then
                cboPolicy.Enabled = True
            Else
                cboPolicy.Enabled = False
            End If
            lblMessage2.Text = Language.getMessage(mstrModuleName, 38, "1. Please set all date(s) columns in selected file as blank if not available.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 39, "2. Please add Allergies/Medical Disabilities in comma ',' separated value if having multiple value for employee like data1,data2,data3 etc.")
            'S.SANDEEP [14-JUN-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpImportWizard_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Select Case e.NewIndex
            Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                Call CreateDataTable()
        End Select
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Sohail (13 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
                    Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
                    'Sohail (21 Aug 2015) -- End
                    If dsEmp.Tables("Period").Rows.Count <= 0 Then
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please first Create atleast One Period."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Sohail (13 Jan 2012) -- End

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        'Sandeep [ 25 APRIL 2011 ] -- Start
                        mds_ImportData = New DataSet
                        'Sandeep [ 25 APRIL 2011 ] -- End 
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END

                    ElseIf ImportFile.Extension.ToLower = ".xml" Then

                        'Sandeep [ 25 APRIL 2011 ] -- Start
                        mds_ImportData = New DataSet
                        'Sandeep [ 25 APRIL 2011 ] -- End 


                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()

                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In tabpMandatoryInfo.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboBranch.Name.ToUpper, cboSection.Name.ToUpper, cboCountry.Name.ToUpper, cboState.Name.ToUpper, cboCity.Name.ToUpper, cboPincode.Name.ToUpper, _
                                        cboAddress1.Name.ToUpper, cboAddress2.Name.ToUpper, cboGender.Name.ToUpper, cboOtherName.Name.ToUpper, cboBirthDate.Name.ToUpper, cboTeam.Name.ToUpper, _
                                        cboEOC_Date.Name.ToUpper, cboEOCReason.Name.ToUpper, cboTitle.Name.ToUpper, cboSectionGroup.Name.ToUpper, cboUnit.Name.ToUpper, cboUnitGroup.Name.ToUpper, _
                                        cboMaritalStatus.Name.ToUpper, cboMarriedDate.Name.ToUpper, cboEmail.Name.ToUpper, cboPayPoint.Name.ToUpper, cboPayType.Name.ToUpper
                                        'Case "CBOBRANCH", "CBOSECTION", "CBOCOUNTRY", _
                                        '     "CBOSTATE", "CBOCITY", "CBOPINCODE", _
                                        '     "CBOADDRESS1", "CBOADDRESS2", "CBOGENDER", _
                                        '     "CBOOTHERNAME", "CBOBIRTHDATE", "CBOTEAM", _
                                        '     "CBOTERMINATIONDATE", "CBOTERMINATIONREASON", "CBOTITLE", _
                                        '     "CBOSECTIONGROUP", "CBOUNIT", "CBOUNITGROUP", _
                                        '     "CBOMARITALSTATUS", "CBOMARRIEDDATE", "CBOEMAIL", "CBOEOC_DATE", "CBOPAYTYPE", "CBOPAYPOINT", _
                                        '     "CBOEOCREASON", "CBOEOC_DATE"
                                        'S.SANDEEP [19-JUL-2018] -- END

                                        'Nilay (17 Feb 2017) -- [CBOPAYPOINT]
                                        'S.SANDEEP [ 10 DEC 2013 ] -- START -- END
                                        'S.SANDEEP [25 Jan 2016] -- START {cboPayType} -- END



                                        If radInactiveEmployee.Checked = True AndAlso CType(ctrl, ComboBox).Name.ToUpper = cboEOC_Date.Name.ToUpper Then
                                            If CType(ctrl, ComboBox).Text = "" Then
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please select Termination date field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                                e.Cancel = True
                                                Exit For
                                            End If
                                            'Anjan [ 25 Apr 2013 ] -- Start
                                            'ENHANCEMENT : TRA CHANGES
                                        ElseIf CType(ctrl, ComboBox).Name.ToUpper = cboBirthDate.Name.ToUpper AndAlso ConfigParameter._Object._RetirementBy = 1 Then
                                            If CType(ctrl, ComboBox).Text = "" Then
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Please select Birth date field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                                e.Cancel = True
                                                Exit For
                                            End If
                                            'Anjan [ 25 Apr 2013 ] -- End
                                        End If

                                        Continue For
                                    Case Else
                                        'Sohail (10 Apr 2013) -- Start
                                        'TRA - ENHANCEMENT
                                        'If CType(ctrl, ComboBox).Text = "" Then
                                        If (CType(ctrl, ComboBox).Enabled = True AndAlso CType(ctrl, ComboBox).Text = "") OrElse (ctrl.Name = cboTransHead.Name AndAlso CInt(cboTransHead.SelectedValue) <= 0) Then
                                            'Sohail (10 Apr 2013) -- End
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            CType(ctrl, ComboBox).Focus()
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            'For Each ctrl As Control In gbFieldMapping.Controls
            '    If TypeOf ctrl Is ComboBox AndAlso ctrl.Name <> "cboTransHead" Then
            '        Call ClearCombo(CType(ctrl, ComboBox))
            '    End If
            'Next

            'For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
            '    cboAppointedDate.Items.Add(dtColumns.ColumnName)
            '    cboRetirementdate.Items.Add(dtColumns.ColumnName) 'Sohail (10 Apr 2013)
            '    cboBirthDate.Items.Add(dtColumns.ColumnName)
            '    cboCostcenter.Items.Add(dtColumns.ColumnName)
            '    cboDepartment.Items.Add(dtColumns.ColumnName)
            '    cboDisplayName.Items.Add(dtColumns.ColumnName)
            '    cboEmployeeCode.Items.Add(dtColumns.ColumnName)
            '    cboEmploymentType.Items.Add(dtColumns.ColumnName)
            '    cboFirstName.Items.Add(dtColumns.ColumnName)
            '    cboGradeName.Items.Add(dtColumns.ColumnName)
            '    cboGradeGroup.Items.Add(dtColumns.ColumnName)
            '    cboGradeLevel.Items.Add(dtColumns.ColumnName)
            '    cboJob.Items.Add(dtColumns.ColumnName)
            '    cboOtherName.Items.Add(dtColumns.ColumnName)
            '    cboSalary.Items.Add(dtColumns.ColumnName)
            '    cboShift.Items.Add(dtColumns.ColumnName)
            '    cboSurname.Items.Add(dtColumns.ColumnName)
            '    cboAddress1.Items.Add(dtColumns.ColumnName)
            '    cboAddress2.Items.Add(dtColumns.ColumnName)
            '    cboBranch.Items.Add(dtColumns.ColumnName)
            '    cboCity.Items.Add(dtColumns.ColumnName)
            '    cboCountry.Items.Add(dtColumns.ColumnName)
            '    cboGender.Items.Add(dtColumns.ColumnName)
            '    cboPincode.Items.Add(dtColumns.ColumnName)
            '    cboSection.Items.Add(dtColumns.ColumnName)
            '    cboState.Items.Add(dtColumns.ColumnName)
            '    cboTeam.Items.Add(dtColumns.ColumnName)
            '    cboLeavingDate.Items.Add(dtColumns.ColumnName)
            '    cboEOCReason.Items.Add(dtColumns.ColumnName)
            '    cboTitle.Items.Add(dtColumns.ColumnName)
            '    cboSectionGroup.Items.Add(dtColumns.ColumnName)
            '    cboUnit.Items.Add(dtColumns.ColumnName)
            '    cboUnitGroup.Items.Add(dtColumns.ColumnName)
            '    cboMaritalStatus.Items.Add(dtColumns.ColumnName)
            '    cboMarriedDate.Items.Add(dtColumns.ColumnName)
            '    'S.SANDEEP [ 29 DEC 2011 ] -- START
            '    'ENHANCEMENT : TRA CHANGES 
            '    'TYPE : EMPLOYEMENT CONTRACT PROCESS
            '    cboJobGroup.Items.Add(dtColumns.ColumnName)
            '    cboDepartmentGroup.Items.Add(dtColumns.ColumnName)
            '    cboClassGroup.Items.Add(dtColumns.ColumnName)
            '    cboClasses.Items.Add(dtColumns.ColumnName)
            '    'S.SANDEEP [ 29 DEC 2011 ] -- END

            '    'S.SANDEEP [ 01 SEP 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    cboEmail.Items.Add(dtColumns.ColumnName)
            '    'S.SANDEEP [ 01 SEP 2012 ] -- END

            '    'S.SANDEEP [ 10 DEC 2013 ] -- START
            '    cboEOC_Date.Items.Add(dtColumns.ColumnName)
            '    'S.SANDEEP [ 10 DEC 2013 ] -- END

            '    'Shani(17-Nov-2015) -- Start
            '    'ENHANCEMENT : Pay type -Include this on Employee Master Importation
            '    cboPayType.Items.Add(dtColumns.ColumnName)
            '    'Shani(17-Nov-2015) -- End

            '    'Nilay (17 Feb 2017) -- Start
            '    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            '    cboPayPoint.Items.Add(dtColumns.ColumnName)
            '    'Nilay (17 Feb 2017) -- End

            'Next
            Dim ctrls = GetAll(Me, GetType(ComboBox))
            Dim listcombo As List(Of Control) = ctrls.ToList()
            For Each ctrl As Control In listcombo
                If CType(ctrl, ComboBox).Name = cboTransHead.Name Then Continue For
                Call ClearCombo(CType(ctrl, ComboBox))
            Next
            Dim columnNames() As String = mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)().Select(Function(x) x.ColumnName).ToArray()
            For Each ctrl As Control In listcombo
                If CType(ctrl, ComboBox).Name = cboTransHead.Name Then Continue For
                If CType(ctrl, ComboBox).Enabled = False Then Continue For
                CType(ctrl, ComboBox).Items.AddRange(columnNames)
            Next
            'S.SANDEEP [14-JUN-2018] -- END


            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known 
            'UAT No:TC017 NMB



            If mds_ImportData.Tables(0).Columns IsNot Nothing Then
                For Each dc As DataColumn In mds_ImportData.Tables(0).Columns
                    If dc.ColumnName = Language.getMessage(mstrModuleName, 51, "EmployeeCode") Then
                        dc.Caption = "EmployeeCode"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 52, "FirstName") Then
                        dc.Caption = "FirstName"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 53, "OtherName") Then
                        dc.Caption = "OtherName"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 54, "Surname") Then
                        dc.Caption = "Surname"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 55, "EmploymentType") Then
                        dc.Caption = "EmploymentType"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 56, "Shift") Then
                        dc.Caption = "Shift"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 57, "Department") Then
                        dc.Caption = "Department"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 58, "Job") Then
                        dc.Caption = "Job"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 59, "Costcenter") Then
                        dc.Caption = "Costcenter"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 60, "GradeGroup") Then
                        dc.Caption = "GradeGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 61, "GradeName") Then
                        dc.Caption = "GradeName"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 62, "GradeLevel") Then
                        dc.Caption = "GradeLevel"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 63, "Salary") Then
                        dc.Caption = "Caption"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 64, "AppointedDate") Then
                        dc.Caption = "Salary"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 65, "BirthDate") Then
                        dc.Caption = "BirthDate"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 66, "DisplayName") Then
                        dc.Caption = "DisplayName"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 68, "Gender") Then
                        dc.Caption = "Gender"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 69, "Email") Then
                        dc.Caption = "Email"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 70, "Retirementdate") Then
                        dc.Caption = "Retirementdate"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 71, "EOC_Date") Then
                        dc.Caption = "EOC_Date"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 72, "EOCReason") Then
                        dc.Caption = "EOCReason"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 73, "PayType") Then
                        dc.Caption = "PayType"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 74, "PayPoint") Then
                        dc.Caption = "PayPoint"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 75, "Policy") Then
                        dc.Caption = "Policy"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 76, "Title") Then
                        dc.Caption = "Title"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 77, "Branch") Then
                        dc.Caption = "Branch"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 78, "DepartmentGroup") Then
                        dc.Caption = "DepartmentGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 79, "SectionGroup") Then
                        dc.Caption = "SectionGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 80, "Section") Then
                        dc.Caption = "Section"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 81, "UnitGroup") Then
                        dc.Caption = "UnitGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 82, "Unit") Then
                        dc.Caption = "Unit"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 83, "Team") Then
                        dc.Caption = "Team"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 84, "JobGroup") Then
                        dc.Caption = "JobGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 85, "ClassGroup") Then
                        dc.Caption = "ClassGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 86, "Classes") Then
                        dc.Caption = "Classes"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 87, "LeavingDate") Then
                        dc.Caption = "LeavingDate"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 88, "MaritalStatus") Then
                        dc.Caption = "MaritalStatus"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 89, "MarriedDate") Then
                        dc.Caption = "MarriedDate"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 90, "Complexion") Then
                        dc.Caption = "Complexion"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 91, "SportHobbies") Then
                        dc.Caption = "SportHobbies"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 92, "BloodGroup") Then
                        dc.Caption = "BloodGroup"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 93, "EyeColor") Then
                        dc.Caption = "EyeColor"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 94, "Nationality") Then
                        dc.Caption = "Nationality"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 95, "Ethincity") Then
                        dc.Caption = "Ethincity"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 96, "Religion") Then
                        dc.Caption = "Religion"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 97, "Hair") Then
                        dc.Caption = "Hair"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 110, "Language1") Then
                        dc.Caption = "Language1"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 102, "Language2") Then
                        dc.Caption = "Language2"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 103, "Language3") Then
                        dc.Caption = "Language3"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 104, "Language4") Then
                        dc.Caption = "Language4"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 105, "ExtraTel") Then
                        dc.Caption = "ExtraTel"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 106, "Height") Then
                        dc.Caption = "Height"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 107, "Weight") Then
                        dc.Caption = "Weight"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 108, "Allergies") Then
                        dc.Caption = "Allergies"
                    ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 109, "MedicalDisabilities") Then
                        dc.Caption = "MedicalDisabilities"
                    End If
                Next
                mds_ImportData.AcceptChanges()
            End If
            'Gajanan (24 Nov 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("firstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("othername", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("surname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employmenttype", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("shift", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("department", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("job", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("costcenter", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("gradegroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("grade", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("gradelevel", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("scale", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("appointeddate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("retirementdate", System.Type.GetType("System.String")) 'Sohail (10 Apr 2013)
            mdt_ImportData_Others.Columns.Add("birthdate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("displayname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("branch", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("section", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("country", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("state", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("city", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pincode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("address1", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("address2", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("gender", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("title", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("sectiongroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("unitgroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("unit", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("team", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("terminationdate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("terminationreason", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("marriedstatus", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("marrieddate", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            mdt_ImportData_Others.Columns.Add("dept_group", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("job_group", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("classgroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("classes", System.Type.GetType("System.String"))
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            'S.SANDEEP [ 01 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdt_ImportData_Others.Columns.Add("email", System.Type.GetType("System.String"))
            'S.SANDEEP [ 01 SEP 2012 ] -- END

            'S.SANDEEP [ 10 DEC 2013 ] -- START
            mdt_ImportData_Others.Columns.Add("eocdate", System.Type.GetType("System.String"))
            'S.SANDEEP [ 10 DEC 2013 ] -- END

            'Shani(17-Nov-2015) -- Start
            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
            mdt_ImportData_Others.Columns.Add("paytype", System.Type.GetType("System.String"))
            'Shani(17-Nov-2015) -- End

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            mdt_ImportData_Others.Columns.Add("paypoint", System.Type.GetType("System.String"))
            'Nilay (17 Feb 2017) -- End


            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            mdt_ImportData_Others.Columns.Add("Policy", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Complexion", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("SportHobbies", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("BloodGroup", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("EyeColor", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Nationality", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Ethincity", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Religion", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Hair", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Language1", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Language2", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Language3", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Language4", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ExtraTel", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Height", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Weight", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Allergies", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("MedicalDisabilities", System.Type.GetType("System.String"))
            'S.SANDEEP [14-JUN-2018] -- END

            Dim drNewRow As DataRow


            'Sandeep (14 Apr 2011) -- Start
            'Issue : to remove full blank row from file.
            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")

            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next

            mds_ImportData.AcceptChanges()
            'Sandeep (14 Apr 2011) -- End

            'Sandeep (14 Apr 2011) -- End

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows

                'Checking for Invalid Data
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString
                drNewRow.Item("firstname") = dtRow.Item(cboFirstName.Text).ToString

                If cboOtherName.Text.Trim.Length > 0 Then
                    drNewRow.Item("othername") = dtRow.Item(cboOtherName.Text).ToString
                Else
                    drNewRow.Item("othername") = ""
                End If

                drNewRow.Item("surname") = dtRow.Item(cboSurname.Text).ToString
                drNewRow.Item("employmenttype") = dtRow.Item(cboEmploymentType.Text).ToString
                drNewRow.Item("shift") = dtRow.Item(cboShift.Text).ToString
                drNewRow.Item("department") = dtRow.Item(cboDepartment.Text).ToString
                drNewRow.Item("job") = dtRow.Item(cboJob.Text).ToString
                drNewRow.Item("costcenter") = dtRow.Item(cboCostcenter.Text).ToString
                drNewRow.Item("gradegroup") = dtRow.Item(cboGradeGroup.Text).ToString
                drNewRow.Item("grade") = dtRow.Item(cboGradeName.Text).ToString
                drNewRow.Item("gradelevel") = dtRow.Item(cboGradeLevel.Text).ToString
                drNewRow.Item("scale") = dtRow.Item(cboSalary.Text).ToString
                drNewRow.Item("appointeddate") = dtRow.Item(cboAppointedDate.Text).ToString
                'Sohail (10 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                If cboRetirementdate.Text.Trim.Length > 0 Then
                    drNewRow.Item("retirementdate") = dtRow.Item(cboRetirementdate.Text).ToString
                Else
                    drNewRow.Item("retirementdate") = ""
                End If
                'Sohail (10 Apr 2013) -- End

                'S.SANDEEP [ 09 AUG 2011 ] -- START
                If cboBirthDate.Text.Trim.Length > 0 Then
                    drNewRow.Item("birthdate") = dtRow.Item(cboBirthDate.Text).ToString
                Else
                    drNewRow.Item("birthdate") = Nothing
                End If
                'S.SANDEEP [ 09 AUG 2011 ] -- END 
                drNewRow.Item("displayname") = dtRow.Item(cboDisplayName.Text).ToString

                If cboBranch.Text.Trim.Length > 0 Then
                    drNewRow.Item("branch") = dtRow.Item(cboBranch.Text).ToString
                Else
                    drNewRow.Item("branch") = ""
                End If

                If cboSection.Text.Trim.Length > 0 Then
                    drNewRow.Item("section") = dtRow.Item(cboSection.Text).ToString
                Else
                    drNewRow.Item("section") = ""
                End If

                If cboCountry.Text.Trim.Length > 0 Then
                    drNewRow.Item("country") = dtRow.Item(cboCountry.Text).ToString
                Else
                    drNewRow.Item("country") = ""
                End If

                If cboState.Text.Trim.Length > 0 Then
                    drNewRow.Item("state") = dtRow.Item(cboState.Text).ToString
                Else
                    drNewRow.Item("state") = ""
                End If

                If cboCity.Text.Trim.Length > 0 Then
                    drNewRow.Item("city") = dtRow.Item(cboCity.Text).ToString
                Else
                    drNewRow.Item("city") = ""
                End If

                If cboPincode.Text.Trim.Length > 0 Then
                    drNewRow.Item("pincode") = dtRow.Item(cboPincode.Text).ToString
                Else
                    drNewRow.Item("pincode") = ""
                End If

                If cboAddress1.Text.Trim.Length > 0 Then
                    drNewRow.Item("address1") = dtRow.Item(cboAddress1.Text).ToString
                Else
                    drNewRow.Item("address1") = ""
                End If

                If cboAddress2.Text.Trim.Length > 0 Then
                    drNewRow.Item("address2") = dtRow.Item(cboAddress2.Text).ToString
                Else
                    drNewRow.Item("address2") = ""
                End If

                If cboGender.Text.Trim.Length > 0 Then
                    drNewRow.Item("gender") = dtRow.Item(cboGender.Text).ToString
                Else
                    drNewRow.Item("gender") = ""
                End If

                If cboTitle.Text.Trim.Length > 0 Then
                    drNewRow.Item("title") = dtRow.Item(cboTitle.Text).ToString
                Else
                    drNewRow.Item("title") = ""
                End If

                If cboSectionGroup.Text.Trim.Length > 0 Then
                    drNewRow.Item("sectiongroup") = dtRow.Item(cboSectionGroup.Text).ToString
                Else
                    drNewRow.Item("sectiongroup") = ""
                End If

                If cboUnitGroup.Text.Trim.Length > 0 Then
                    drNewRow.Item("unitgroup") = dtRow.Item(cboUnitGroup.Text).ToString
                Else
                    drNewRow.Item("unitgroup") = ""
                End If

                If cboUnit.Text.Trim.Length > 0 Then
                    drNewRow.Item("unit") = dtRow.Item(cboUnit.Text).ToString
                Else
                    drNewRow.Item("unit") = ""
                End If

                If cboTeam.Text.Trim.Length > 0 Then
                    drNewRow.Item("team") = dtRow.Item(cboTeam.Text).ToString
                Else
                    drNewRow.Item("team") = ""
                End If

                If cboLeavingDate.Text.Trim.Length > 0 Then
                    drNewRow.Item("terminationdate") = dtRow.Item(cboLeavingDate.Text).ToString
                Else
                    drNewRow.Item("terminationdate") = ""
                End If

                If cboEOCReason.Text.Trim.Length > 0 Then
                    drNewRow.Item("terminationreason") = dtRow.Item(cboEOCReason.Text).ToString
                Else
                    drNewRow.Item("terminationreason") = ""
                End If

                If cboMaritalStatus.Text.Trim.Length > 0 Then
                    drNewRow.Item("marriedstatus") = dtRow.Item(cboMaritalStatus.Text).ToString
                Else
                    drNewRow.Item("marriedstatus") = ""
                End If

                If cboMarriedDate.Text.Trim.Length > 0 Then
                    drNewRow.Item("marrieddate") = dtRow.Item(cboMarriedDate.Text).ToString
                Else
                    drNewRow.Item("marrieddate") = ""
                End If

                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                If cboJobGroup.Text.Trim.Length > 0 Then
                    drNewRow.Item("job_group") = dtRow.Item(cboJobGroup.Text).ToString
                Else
                    drNewRow.Item("job_group") = ""
                End If

                If cboDepartmentGroup.Text.Length > 0 Then
                    drNewRow.Item("dept_group") = dtRow.Item(cboDepartmentGroup.Text).ToString
                Else
                    drNewRow.Item("dept_group") = ""
                End If

                If cboClassGroup.Text.Length > 0 Then
                    drNewRow.Item("classgroup") = dtRow.Item(cboClassGroup.Text).ToString
                Else
                    drNewRow.Item("classgroup") = ""
                End If

                If cboClasses.Text.Length > 0 Then
                    drNewRow.Item("classes") = dtRow.Item(cboClasses.Text).ToString
                Else
                    drNewRow.Item("classes") = ""
                End If
                'S.SANDEEP [ 29 DEC 2011 ] -- END

                'S.SANDEEP [ 01 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES


                'Pinkal (01-Dec-2012) -- Start
                'Enhancement : TRA Changes

                Select Case ConfigParameter._Object._DisplayNameSetting

                    Case enDisplayNameSetting.NONE

                        If cboEmail.Text.Trim.Length > 0 Then
                            drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString
                        Else
                            drNewRow.Item("email") = ""
                        End If

                    Case enDisplayNameSetting.EMPLOYEECODE

                        If cboEmail.Text.Trim.Length > 0 Then
                            If dtRow.Item(cboEmail.Text).ToString.Trim.Length > 0 Then
                                drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString
                            Else
                                'S.SANDEEP [ 30 JAN 2013 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'drNewRow.Item("email") = drNewRow.Item("employeecode").ToString.Trim & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                                If ConfigParameter._Object._CompanyDomain.ToString().Trim.Length > 0 Then
                                    drNewRow.Item("email") = drNewRow.Item("employeecode").ToString.Trim & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                                Else
                                    drNewRow.Item("email") = ""
                                End If
                                'S.SANDEEP [ 30 JAN 2013 ] -- END
                            End If
                        Else
                            'S.SANDEEP [ 30 JAN 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'drNewRow.Item("email") = drNewRow.Item("employeecode").ToString.Trim & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                            If ConfigParameter._Object._CompanyDomain.ToString().Trim.Length > 0 Then
                                drNewRow.Item("email") = drNewRow.Item("employeecode").ToString.Trim & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                            Else
                                drNewRow.Item("email") = ""
                            End If
                            'S.SANDEEP [ 30 JAN 2013 ] -- END
                        End If



                    Case enDisplayNameSetting.FLASTNAME

                        If ConfigParameter._Object._CompanyDomain.ToString().Trim.Length > 0 Then
                            drNewRow.Item("email") = drNewRow.Item("firstname").ToString().Trim.Substring(0, 1) & drNewRow.Item("surname").ToString() & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                        Else
                            drNewRow.Item("email") = ""
                        End If

                    Case enDisplayNameSetting.FIRSTNAME_LASTNAME

                        If ConfigParameter._Object._CompanyDomain.ToString().Trim.Length > 0 Then
                            drNewRow.Item("email") = drNewRow.Item("firstname").ToString().Trim & "." & drNewRow.Item("surname").ToString() & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                        Else
                            drNewRow.Item("email") = ""
                        End If

                    Case enDisplayNameSetting.LASTNAME_FIRSTNAME

                        If ConfigParameter._Object._CompanyDomain.ToString().Trim.Length > 0 Then
                            drNewRow.Item("email") = drNewRow.Item("surname").ToString() & "." & drNewRow.Item("firstname").ToString().Trim & "@" & ConfigParameter._Object._CompanyDomain.ToString().Trim
                        Else
                            drNewRow.Item("email") = ""
                        End If

                End Select

                'Pinkal (01-Dec-2012) -- End

                'S.SANDEEP [ 01 SEP 2012 ] -- END

                'S.SANDEEP [ 10 DEC 2013 ] -- START
                If cboEOC_Date.Text.Trim.Length > 0 Then
                    drNewRow.Item("eocdate") = dtRow.Item(cboEOC_Date.Text.Trim)
                Else
                    drNewRow.Item("eocdate") = ""
                End If
                'S.SANDEEP [ 10 DEC 2013 ] -- END

                'Shani(17-Nov-2015) -- Start
                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                If cboPayType.Text.Length > 0 Then
                    drNewRow.Item("paytype") = dtRow.Item(cboPayType.Text).ToString
                Else
                    drNewRow.Item("paytype") = ""
                End If
                'Shani(17-Nov-2015) -- End

                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                If cboPayPoint.Text.Length > 0 Then
                    drNewRow.Item("paypoint") = dtRow.Item(cboPayPoint.Text).ToString
                Else
                    drNewRow.Item("paypoint") = ""
                End If
                'Nilay (17 Feb 2017) -- End

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If cboPolicy.Text.Trim.Length > 0 Then
                    drNewRow.Item("Policy") = dtRow.Item(cboPolicy.Text).ToString
                Else
                    drNewRow.Item("Policy") = ""
                End If

                If cboComplexion.Text.Trim.Length > 0 Then
                    drNewRow.Item("Complexion") = dtRow.Item(cboComplexion.Text).ToString
                Else
                    drNewRow.Item("Complexion") = ""
                End If

                If cboSportHobbies.Text.Trim.Length > 0 Then
                    drNewRow.Item("SportHobbies") = dtRow.Item(cboSportHobbies.Text).ToString
                Else
                    drNewRow.Item("SportHobbies") = ""
                End If

                If cboBloodGroup.Text.Trim.Length > 0 Then
                    drNewRow.Item("BloodGroup") = dtRow.Item(cboBloodGroup.Text).ToString
                Else
                    drNewRow.Item("BloodGroup") = ""
                End If

                If cboEyeColor.Text.Trim.Length > 0 Then
                    drNewRow.Item("EyeColor") = dtRow.Item(cboEyeColor.Text).ToString
                Else
                    drNewRow.Item("EyeColor") = ""
                End If

                If cboNationality.Text.Trim.Length > 0 Then
                    drNewRow.Item("Nationality") = dtRow.Item(cboNationality.Text).ToString
                Else
                    drNewRow.Item("Nationality") = ""
                End If

                If cboEthincity.Text.Trim.Length > 0 Then
                    drNewRow.Item("Ethincity") = dtRow.Item(cboEthincity.Text).ToString
                Else
                    drNewRow.Item("Ethincity") = ""
                End If

                If cboReligion.Text.Trim.Length > 0 Then
                    drNewRow.Item("Religion") = dtRow.Item(cboReligion.Text).ToString
                Else
                    drNewRow.Item("Religion") = ""
                End If

                If cboHair.Text.Trim.Length > 0 Then
                    drNewRow.Item("Hair") = dtRow.Item(cboHair.Text).ToString
                Else
                    drNewRow.Item("Hair") = ""
                End If

                If cboLanguage1.Text.Trim.Length > 0 Then
                    drNewRow.Item("Language1") = dtRow.Item(cboLanguage1.Text).ToString
                Else
                    drNewRow.Item("Language1") = ""
                End If

                If cboLanguage2.Text.Trim.Length > 0 Then
                    drNewRow.Item("Language2") = dtRow.Item(cboLanguage2.Text).ToString
                Else
                    drNewRow.Item("Language2") = ""
                End If

                If cboLanguage3.Text.Trim.Length > 0 Then
                    drNewRow.Item("Language3") = dtRow.Item(cboLanguage3.Text).ToString
                Else
                    drNewRow.Item("Language3") = ""
                End If

                If cboLanguage4.Text.Trim.Length > 0 Then
                    drNewRow.Item("Language4") = dtRow.Item(cboLanguage4.Text).ToString
                Else
                    drNewRow.Item("Language4") = ""
                End If

                If cboExtraTel.Text.Trim.Length > 0 Then
                    drNewRow.Item("ExtraTel") = dtRow.Item(cboExtraTel.Text).ToString
                Else
                    drNewRow.Item("ExtraTel") = ""
                End If

                If cboHeight.Text.Trim.Length > 0 Then
                    drNewRow.Item("Height") = dtRow.Item(cboHeight.Text).ToString
                Else
                    drNewRow.Item("Height") = ""
                End If

                If cboWeight.Text.Trim.Length > 0 Then
                    drNewRow.Item("Weight") = dtRow.Item(cboWeight.Text).ToString
                Else
                    drNewRow.Item("Weight") = ""
                End If

                If cboAllergies.Text.Trim.Length > 0 Then
                    drNewRow.Item("Allergies") = dtRow.Item(cboAllergies.Text).ToString
                Else
                    drNewRow.Item("Allergies") = ""
                End If

                If cboMedicalDisabilities.Text.Trim.Length > 0 Then
                    drNewRow.Item("MedicalDisabilities") = dtRow.Item(cboMedicalDisabilities.Text).ToString
                Else
                    drNewRow.Item("MedicalDisabilities") = ""
                End If
                'S.SANDEEP [14-JUN-2018] -- END

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "displayname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboCostcenter.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Cost Center cannot be blank. Please set the Cost Center to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboDepartment.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Department cannot be blank. Please set the Department to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboDisplayName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Display Name cannot be blank. Please set the Display Name to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee Code cannot be blank. Please set the Employee Code to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboEmploymentType.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Employment Type cannot be blank. Please set the Employment Type to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboFirstName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "First Name cannot be blank. Please set the First Name to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboGradeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Grade cannot be blank. Please set the Grade to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboGradeGroup.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Grade Group cannot be blank. Please set the Grade Group to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboGradeLevel.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Grade Level cannot be blank. Please set the Grade Level to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboJob.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Job cannot be blank. Please set the Job to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'If .Item(cboOtherName.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Other Name cannot be blank. Please set the Other Name to import employee(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                If .Item(cboSalary.Text).ToString.Trim.Length <= 0 Then
                    If radDefinedSalary.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Salary cannot be blank. Please set the Salary to import employee(s)."), enMsgBoxStyle.Information)
                    ElseIf radGradedSalary.Checked = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Increment cannot be blank. Please set the Increment to import employee(s)."), enMsgBoxStyle.Information)
                    End If
                    Return False
                End If
                If .Item(cboShift.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Shift cannot be blank. Please set the Shift to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboSurname.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Surname cannot be blank. Please set the Surname to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If CInt(cboTransHead.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Transacation Head is mandatory information. Please select transaction Head to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Sohail (10 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                If .Item(cboAppointedDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Appointment Date is mandatory information. Please select Appointment Date to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboRetirementdate.Enabled = True AndAlso .Item(cboRetirementdate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Retirement Date is mandatory information. Please select Retirement Date to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (10 Apr 2013) -- End


                'Pinkal (1-Sep-2014) -- Start
                'Enhancement - TRA GENDER COMPULOSRY IN EMPLOYEE AND APPLICANT MASTER AS WELL AS IMPORTATION AND PUT APPOINTMENT CHECK BOX ON EMPLOYEE MASTER
                If .Item(cboGender.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Gender is mandatory information. Please select Gender to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Pinkal (1-Sep-2014) -- End

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If cboPolicy.Enabled = True AndAlso .Item(cboPolicy.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 98, "Policy is mandatory information. Please select Policy to import employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [14-JUN-2018] -- END

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'Dim objShift As New clsshift_master
            Dim objShift As New clsNewshift_master
            'Pinkal (01-Dec-2012) -- End


            Dim objDept As New clsDepartment
            Dim objJob As New clsJobs
            Dim objCost As New clscostcenter_master
            Dim objGradeGrp As New clsGradeGroup
            Dim objGrade As New clsGrade
            Dim objGLevel As New clsGradeLevel
            Dim objWageTran As New clsWagesTran
            Dim objBranch As New clsStation
            Dim objSection As New clsSections
            Dim objCountry As New clsMasterData
            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim objPincode As New clszipcode_master

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'Dim objTranHead As New clsTransactionHead
            Dim objJobGrp As New clsJobGroup
            Dim objDeptGrp As New clsDepartmentGroup
            Dim objClassGrp As New clsClassGroup
            Dim objClasses As New clsClass
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            Dim objSectionGrp As New clsSectionGroup
            Dim objUnitGrp As New clsUnitGroup
            Dim objUnit As New clsUnits
            Dim objTeam As New clsTeams
            Dim objReason As New clsAction_Reason
            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            Dim objpolicy_master As New clspolicy_master
            'S.SANDEEP [14-JUN-2018] -- END

            Dim objEmp As clsEmployee_Master

            Dim intEmplTypeId As Integer = -1
            Dim intShiftId As Integer = -1
            Dim intDepartId As Integer = -1
            Dim intJobId As Integer = -1
            Dim intCostCenterId As Integer = -1
            Dim intGradeGrpId As Integer = -1
            Dim intGradeId As Integer = -1
            Dim intGLevelId As Integer = -1
            Dim intBranchId As Integer = -1
            Dim intSectionId As Integer = -1
            Dim intCountryId As Integer = -1
            Dim intStateId As Integer = -1
            Dim intCityId As Integer = -1
            Dim intPincodeId As Integer = -1
            Dim intGenderId As Integer = -1
            Dim intEmpId As Integer = -1

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'Dim intTranHeadId As Integer = -1
            Dim intJobGroupId As Integer = -1
            Dim intDeptGrpId As Integer = -1
            Dim intClassGrpId As Integer = -1
            Dim intClassId As Integer = -1
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            Dim intSectionGrpId As Integer = -1
            Dim intUnitGrpId As Integer = -1
            Dim intUnitId As Integer = -1
            Dim intTeamId As Integer = -1
            Dim intReasonId As Integer = -1
            Dim intMarriedId As Integer = -1
            Dim intTitleId As Integer = -1

            'Shani(17-Nov-2015) -- Start
            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
            Dim intPayTypeId As Integer = -1
            'Shani(17-Nov-2015) -- End

            'Nilay (17 Feb 2017) -- Start
            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
            Dim intPayPointId As Integer = -1
            'Nilay (17 Feb 2017) -- End

            'Varsha (10 Nov 2017) -- Start
            'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
            Dim objMasterData As New clsMasterData
            Dim intThemeId As Integer = objMasterData.GetDefaultThemeId(Company._Object._Companyunkid, Nothing)
            'Varsha (10 Nov 2017) -- End

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            Dim intPolicyId, intComplexionId, intBloodGroupId, intEyeColorId, intNationalityId, intEthinicityId, intRelegionId, intHairId, intLang1Id, intAllergyId, intDisabilitiesId, intLang2Id, intLang3Id, intLang4Id As Integer
            'S.SANDEEP [14-JUN-2018] -- END

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows

                'S.SANDEEP [ 07 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                intEmplTypeId = -1 : intShiftId = -1
                intDepartId = -1 : intJobId = -1
                intCostCenterId = -1 : intGradeGrpId = -1
                intGradeId = -1 : intGLevelId = -1
                intBranchId = -1 : intSectionId = -1
                intCountryId = -1 : intStateId = -1
                intCityId = -1 : intPincodeId = -1
                intGenderId = -1 : intEmpId = -1
                intJobGroupId = -1 : intDeptGrpId = -1
                intClassGrpId = -1 : intClassId = -1
                intSectionGrpId = -1 : intUnitGrpId = -1
                intUnitId = -1 : intTeamId = -1
                intReasonId = -1 : intMarriedId = -1 : intTitleId = -1
                'S.SANDEEP [ 07 SEP 2012 ] -- END

                'Shani(17-Nov-2015) -- Start
                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                intPayTypeId = -1
                'Shani(17-Nov-2015) -- End

                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                intPayPointId = -1
                'Nilay (17 Feb 2017) -- End

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                intPolicyId = 0 : intComplexionId = 0
                intBloodGroupId = 0 : intEyeColorId = 0 : intNationalityId = 0 : intEthinicityId = 0
                intRelegionId = 0 : intHairId = 0 : intLang1Id = 0
                intLang2Id = 0 : intLang3Id = 0 : intLang4Id = 0 : intAllergyId = 0 : intDisabilitiesId = 0
                'S.SANDEEP [14-JUN-2018] -- END

                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : DECIMAL CHANGES
                'Dim dblEmpScale As Double = 0
                Dim decEmpScale As Decimal = 0
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                'S.SANDEEP [ 01 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                '----------------------> EMAIL
                If dtRow.Item("email").ToString.Trim.Length > 0 Then
                    'S.SANDEEP [ 11 MAR 2014 ] -- START
                    'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
                    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                    'S.SANDEEP [ 11 MAR 2014 ] -- END
                    If Expression.IsMatch(dtRow.Item("email").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 25, "Invalid Email Address.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    'Pinkal (01-Dec-2012) -- Start
                    'Enhancement : TRA Changes

                    Dim objMaster As New clsMasterData
                    If objMaster.IsEmailPresent("hremployee_master", dtRow.Item("email").ToString.Trim) = True Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage("frmEmployeeMaster", 60, "Sorry, this email address is already used by some employee. please provide another email address.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    'Pinkal (01-Dec-2012) -- End


                End If
                'S.SANDEEP [ 01 SEP 2012 ] -- END


                '---------------------> EMPLOYMENT TYPE
                If dtRow.Item("employmenttype").ToString.Trim.Length > 0 Then
                    intEmplTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, dtRow.Item("employmenttype").ToString.Trim)
                    If intEmplTypeId <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        If dtRow.Item("employmenttype").ToString.Length > 5 Then
                            objCMaster._Alias = dtRow.Item("employmenttype").ToString.PadRight(5, CChar(" ")).Substring(0, 4)
                            objCMaster._Code = dtRow.Item("employmenttype").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        Else
                            objCMaster._Alias = dtRow.Item("employmenttype").ToString.Trim.ToUpper
                            objCMaster._Code = dtRow.Item("employmenttype").ToString.Trim.ToUpper
                        End If
                        objCMaster._Name = dtRow.Item("employmenttype").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE

                        If objCMaster.Insert() Then
                            intEmplTypeId = objCMaster._Masterunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intEmplTypeId = -1
                        End If
                    End If
                End If

                '---------------------> SHIFT MASTER
                If dtRow.Item("shift").ToString.Trim.Length > 0 Then
                    intShiftId = objShift.GetShiftUnkId(dtRow.Item("shift").ToString.Trim)
                    If intShiftId <= 0 Then
                        If Not objShift Is Nothing Then objShift = Nothing

                        'Pinkal (01-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        'objShift = New clsshift_master
                        objShift = New clsNewshift_master
                        'Pinkal (01-Dec-2012) -- End


                        If dtRow.Item("shift").ToString.Length > 5 Then
                            objShift._Shiftcode = dtRow.Item("shift").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        Else
                            objShift._Shiftcode = dtRow.Item("shift").ToString.Trim.ToUpper
                        End If
                        objShift._Shiftname = dtRow("shift").ToString.Trim
                        objShift._Shifttypeunkid = 0
                        objShift._Isactive = True

                        'Pinkal (01-Dec-2012) -- Start
                        'Enhancement : TRA Changes

                        'objShift._Starttime = Nothing
                        'objShift._Endtime = Nothing

                        Dim mdtDays As DataTable = GenerateShiftDays()

                        'Pinkal (01-Dec-2012) -- End


                        If objShift.Insert(mdtDays) Then
                            intShiftId = objShift._Shiftunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objShift._Shiftcode & "/" & objShift._Shiftname & ":" & objShift._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intShiftId = -1
                        End If
                    End If
                End If

                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                '---------------------> DEPARMENT Group
                If dtRow.Item("dept_group").ToString.Trim.Length > 0 Then
                    intDeptGrpId = CInt(objDeptGrp.GetDepartmentGrpUnkId(dtRow.Item("dept_group").ToString.Trim))
                    If intDeptGrpId <= 0 Then
                        If Not objDeptGrp Is Nothing Then objDeptGrp = Nothing
                        objDeptGrp = New clsDepartmentGroup

                        objDeptGrp._Code = dtRow.Item("dept_group").ToString.Trim
                        objDeptGrp._Name = dtRow.Item("dept_group").ToString.Trim
                        objDeptGrp._Isactive = True

                        If objDeptGrp.Insert Then
                            intDeptGrpId = objDeptGrp._Deptgroupunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objDeptGrp._Code & "/" & objDeptGrp._Name & " : " & objDeptGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                    End If
                End If
                'S.SANDEEP [ 29 DEC 2011 ] -- END

                '---------------------> DEPARMENT
                If dtRow.Item("department").ToString.Trim.Length > 0 Then
                    intDepartId = objDept.GetDepartmentUnkId(dtRow.Item("department").ToString.Trim)
                    If intDepartId <= 0 Then
                        If Not objDept Is Nothing Then objDept = Nothing
                        objDept = New clsDepartment

                        If dtRow.Item("department").ToString.Length > 5 Then
                            objDept._Code = dtRow.Item("department").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        Else
                            objDept._Code = dtRow.Item("department").ToString.Trim.ToUpper
                        End If
                        objDept._Name = dtRow.Item("department").ToString.Trim
                        objDept._Isactive = True
                        objDept._Deptgroupunkid = 0
                        objDept._Stationunkid = 0

                        If objDept.Insert Then
                            intDepartId = objDept._Departmentunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objDept._Code & "/" & objDept._Name & " : " & objDept._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intDepartId = -1
                        End If
                    End If
                End If

                '---------------------> COST CENTER
                If dtRow.Item("costcenter").ToString.Trim.Length > 0 Then
                    intCostCenterId = objCost.GetCostCenterUnkId(dtRow.Item("costcenter").ToString.Trim)
                    If intCostCenterId <= 0 Then
                        If Not objCost Is Nothing Then objCost = Nothing
                        objCost = New clscostcenter_master

                        If dtRow.Item("costcenter").ToString.Length > 5 Then
                            objCost._Costcentercode = dtRow.Item("costcenter").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        Else
                            objCost._Costcentercode = dtRow.Item("costcenter").ToString.Trim.ToUpper
                        End If
                        objCost._Costcentergroupmasterunkid = 0
                        objCost._Costcentername = dtRow.Item("costcenter").ToString.Trim
                        objCost._Isactive = True

                        If objCost.Insert Then
                            intCostCenterId = objCost._Costcenterunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCost._Costcentercode & "/" & objCost._Costcentername & ":" & objCost._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intCostCenterId = -1
                        End If
                    End If
                End If

                '---------------------> GRADE GROUP
                If dtRow.Item("gradegroup").ToString.Trim.Length > 0 Then
                    intGradeGrpId = objGradeGrp.GetGradeGrpUnkId(dtRow.Item("gradegroup").ToString.Trim)
                    If intGradeGrpId <= 0 Then
                        If Not objGradeGrp Is Nothing Then objGradeGrp = Nothing

                        objGradeGrp = New clsGradeGroup

                        If dtRow.Item("gradegroup").ToString.Trim.Length > 5 Then
                            objGradeGrp._Code = dtRow.Item("gradegroup").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        Else
                            objGradeGrp._Code = dtRow.Item("gradegroup").ToString.Trim.ToUpper
                        End If
                        objGradeGrp._Name = dtRow.Item("gradegroup").ToString.Trim
                        objGradeGrp._Isactive = True

                        If objGradeGrp.Insert Then
                            intGradeGrpId = objGradeGrp._Gradegroupunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objGradeGrp._Code & "/" & objGradeGrp._Name & ":" & objGradeGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intGradeGrpId = -1
                        End If
                    End If
                End If
                '---------------------> GRADE
                If dtRow.Item("grade").ToString.Trim.Length > 0 Then
                    intGradeId = objGrade.GetGradeUnkId(dtRow.Item("grade").ToString.Trim)
                    If intGradeId <= 0 Then
                        If Not objGrade Is Nothing Then objGrade = Nothing

                        objGrade = New clsGrade

                        objGrade._Gradegroupunkid = intGradeGrpId
                        If dtRow.Item("grade").ToString.Length > 5 Then
                            objGrade._Code = dtRow.Item("grade").ToString.Trim
                        Else
                            objGrade._Code = dtRow.Item("grade").ToString.PadRight(5, CChar(" ")).Substring(0, 4).ToUpper
                        End If
                        objGrade._Name = dtRow.Item("grade").ToString.Trim.ToUpper
                        objGrade._Isactive = True

                        If objGrade.Insert Then
                            intGradeId = objGrade._Gradeunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objGrade._Code & "/" & objGrade._Name & ":" & objGrade._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intGradeId = -1
                        End If
                    End If
                End If
                '---------------------> GRADE LEVEL
                If dtRow.Item("gradelevel").ToString.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'intGLevelId = objGLevel.GetGradeLevelUnkId(dtRow.Item("gradelevel").ToString.Trim)
                    intGLevelId = objGLevel.GetGradeLevelUnkId(dtRow.Item("gradelevel").ToString.Trim, intGradeId, intGradeGrpId)
                    'S.SANDEEP [ 07 NOV 2011 ] -- END
                    If intGLevelId <= 0 Then
                        If Not objGLevel Is Nothing Then objGLevel = Nothing

                        objGLevel = New clsGradeLevel

                        objGLevel._Code = dtRow.Item("gradelevel").ToString.Trim.ToUpper
                        objGLevel._Gradeunkid = intGradeId
                        objGLevel._Name = dtRow.Item("gradelevel").ToString.Trim
                        objGLevel._Isactive = True

                        If objGLevel.Insert Then
                            intGLevelId = objGLevel._Gradelevelunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objGLevel._Code & "/" & objGLevel._Name & ":" & objGLevel._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intGLevelId = -1
                        End If
                    End If
                End If

                '---------------------> SCALE
                'Dim dWRow As DataRow = Nothing
                'dWRow = dtWagesTran.NewRow
                'dWRow.Item("gradegroupunkid") = intGradeGrpId
                'dWRow.Item("gradeunkid") = intGradeId
                'dWRow.Item("gradelevelunkid") = intGLevelId
                'dWRow.Item("salary") = Format(cdec(dtRow.Item("scale")), GUI.fmtCurrency)
                'dtWagesTran.Rows.Add(dWRow)
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'Dim dsList As DataSet = objWageTran.getScaleInfo(intGradeId, intGLevelId, "List")
                Dim dsList As DataSet = objWageTran.getScaleInfo(intGradeId, intGLevelId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "List")
                'Sohail (27 Apr 2016) -- End
                If radDefinedSalary.Checked = True Then
                    If dsList.Tables("List").Rows.Count > 0 Then
                        'IF SALARY FROM FILE FALLS IN THE RANGE OF SCALE AND MAXIMUM FOR THAT GRADE AND GRADE LEVEL IT WILL INSERT THE SALARY ELSE "0" INSERTED
                        Dim dtTemp() As DataRow = dsList.Tables("List").Select(CDec(dtRow.Item("scale").ToString) & " >= salary AND " & CDec(dtRow.Item("scale").ToString) & " <= maximum")
                        If dtTemp.Length > 0 Then

                            'S.SANDEEP [ 23 JUNE 2011 ] -- START
                            'ISSUE : DECIMAL CHANGES
                            'dblEmpScale = CDec(Format(CDec(dtRow.Item("scale")), GUI.fmtCurrency))
                            decEmpScale = CDec(Format(CDec(dtRow.Item("scale")), GUI.fmtCurrency))
                            'S.SANDEEP [ 23 JUNE 2011 ] -- END 

                        End If
                    End If
                ElseIf radGradedSalary.Checked = True Then
                    If dsList.Tables("List").Rows.Count > 0 Then
                        'IF ADDITION OF SALARY FROM FILE  & SCALE FROM THAT GRADE AND GRADE LEVEL EXCEEDS THE MAXIMUM THEN "0" WILL BE INSERTED


                        'S.SANDEEP [ 23 JUNE 2011 ] -- START
                        'ISSUE : DECIMAL CHANGES
                        'dblEmpScale = CDec(dsList.Tables("List").Rows(0).Item("salary")) + CDec(dtRow.Item("scale"))
                        'If dblEmpScale > CDec(dsList.Tables("List").Rows(0).Item("maximum")) Then
                        '    dblEmpScale = 0
                        'End If

                        decEmpScale = CDec(dsList.Tables("List").Rows(0).Item("salary")) + CDec(dtRow.Item("scale"))
                        If decEmpScale > CDec(dsList.Tables("List").Rows(0).Item("maximum")) Then
                            decEmpScale = 0
                        End If
                        'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                    End If
                End If

                '---------------------> BRANCH
                If dtRow.Item("branch").ToString.Trim.Length > 0 Then
                    intBranchId = objBranch.GetStationUnkId(dtRow.Item("branch").ToString.Trim)
                    If intBranchId <= 0 Then
                        If Not objBranch Is Nothing Then objBranch = Nothing
                        objBranch = New clsStation

                        objBranch._Code = dtRow.Item("branch").ToString.Trim.ToUpper
                        objBranch._Name = dtRow.Item("branch").ToString.Trim
                        objBranch._Isactive = True

                        If objBranch.Insert Then
                            intBranchId = objBranch._Stationunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objBranch._Code & "/" & objBranch._Name & ":" & objBranch._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intBranchId = -1
                        End If
                    End If
                End If

                '---------------------> SECTION GROUP
                If dtRow.Item("sectiongroup").ToString.Trim.Length > 0 Then
                    intSectionGrpId = objSectionGrp.GetSectionGrpUnkId(dtRow.Item("sectiongroup").ToString.Trim)
                    If intSectionGrpId <= 0 Then
                        If Not objSectionGrp Is Nothing Then objSectionGrp = Nothing
                        objSectionGrp = New clsSectionGroup

                        objSectionGrp._Code = dtRow.Item("sectiongroup").ToString.Trim
                        objSectionGrp._Isactive = True
                        objSectionGrp._Name = dtRow.Item("sectiongroup").ToString.Trim

                        If objSectionGrp.Insert Then
                            intSectionGrpId = objSectionGrp._Sectiongroupunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objSectionGrp._Code & "/" & objSectionGrp._Name & ":" & objSectionGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                    End If
                End If

                '---------------------> SECTION
                If dtRow.Item("section").ToString.Trim.Length > 0 Then
                    intSectionId = objSection.GetSectionUnkId(dtRow.Item("section").ToString.Trim)
                    If intSectionId <= 0 Then
                        If Not objSection Is Nothing Then objSection = Nothing
                        objSection = New clsSections

                        objSection._Code = dtRow.Item("section").ToString.Trim
                        'objSection._Departmentunkid = intDepartId
                        'objSection._Sectiongroupunkid = intSectionGrpId
                        objSection._Isactive = True
                        objSection._Name = dtRow.Item("section").ToString.Trim

                        If objSection.Insert Then
                            intSectionId = objSection._Sectionunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objSection._Code & "/" & objSection._Name & ":" & objSection._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intSectionId = -1
                        End If
                    End If
                End If

                '---------------------> UNIT GROUP
                If dtRow.Item("unitgroup").ToString.Trim.Length > 0 Then
                    intUnitGrpId = objUnitGrp.GetUnitGrpUnkId(dtRow.Item("unitgroup").ToString.Trim)
                    If intUnitGrpId <= 0 Then
                        If Not objUnitGrp Is Nothing Then objUnitGrp = Nothing
                        objUnitGrp = New clsUnitGroup

                        objUnitGrp._Code = dtRow.Item("unitgroup").ToString.Trim
                        objUnitGrp._Isactive = True
                        objUnitGrp._Name = dtRow.Item("unitgroup").ToString.Trim
                        'objUnitGrp._Sectionunkid = intSectionId

                        If objUnitGrp.Insert Then
                            intUnitGrpId = objUnitGrp._Unitgroupunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objUnitGrp._Code & "/" & objUnitGrp._Name & ":" & objUnitGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '---------------------> UNIT
                If dtRow.Item("unit").ToString.Trim.Length > 0 Then
                    intUnitId = objUnit.GetUnitUnkId(dtRow.Item("unit").ToString.Trim)
                    If intUnitId <= 0 Then
                        If Not objUnit Is Nothing Then objUnit = Nothing
                        objUnit = New clsUnits

                        objUnit._Code = dtRow.Item("unit").ToString.Trim
                        objUnit._Isactive = True
                        objUnit._Name = dtRow.Item("unit").ToString.Trim
                        'objUnit._Sectionunkid = intSectionId
                        'objUnit._Unitgroupunkid = intUnitGrpId

                        If objUnit.Insert Then
                            intUnitId = objUnit._Unitunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objUnit._Code & "/" & objUnit._Name & ":" & objUnit._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '---------------------> TEAM
                If dtRow.Item("team").ToString.Trim.Length > 0 Then
                    intTeamId = objTeam.GetTeamUnkId(dtRow.Item("team").ToString.Trim)

                    If intTeamId <= 0 Then
                        If Not objTeam Is Nothing Then objTeam = Nothing
                        objTeam = New clsTeams

                        objTeam._Code = dtRow.Item("team").ToString.Trim
                        objTeam._Isactive = True
                        objTeam._Name = dtRow.Item("team").ToString.Trim
                        'objTeam._Unitunkid = intUnitId

                        If objTeam.Insert Then
                            intTeamId = objTeam._Teamunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objTeam._Code & "/" & objTeam._Name & ":" & objTeam._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '---------------------> TERMINATION REASON
                If dtRow.Item("terminationreason").ToString.Trim.Length > 0 Then
                    intReasonId = objReason.GetReasonUnkid(True, dtRow.Item("terminationreason").ToString.Trim)

                    If intReasonId <= 0 Then
                        If Not objReason Is Nothing Then objReason = Nothing
                        objReason = New clsAction_Reason

                        objReason._Code = dtRow.Item("terminationreason").ToString.Trim
                        objReason._Isactive = True
                        objReason._Isreason = True
                        objReason._Reason_Action = dtRow.Item("terminationreason").ToString.Trim

                        If objReason.Insert Then
                            intReasonId = objReason._Actionreasonunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objReason._Code & "/" & objReason._Reason_Action & ":" & objReason._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '---------------------> MARITAL STATUS
                If dtRow.Item("marriedstatus").ToString.Trim.Length > 0 Then
                    intMarriedId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.MARRIED_STATUS, dtRow.Item("marriedstatus").ToString.Trim)

                    If intMarriedId <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("marriedstatus").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.MARRIED_STATUS
                        objCMaster._Name = dtRow.Item("marriedstatus").ToString.Trim

                        If objCMaster.Insert Then
                            intMarriedId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '---------------------> TITLE
                If dtRow.Item("title").ToString.Trim.Length > 0 Then
                    intTitleId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TITLE, dtRow.Item("title").ToString.Trim)
                    If intTitleId <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("title").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.TITLE
                        objCMaster._Name = dtRow.Item("title").ToString.Trim

                        If objCMaster.Insert Then
                            intTitleId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If



                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                '---------------------> JOB GROUP
                If dtRow.Item("job_group").ToString.Trim.Length > 0 Then
                    intJobGroupId = objJobGrp.GetJobGroupUnkId(dtRow.Item("job_group").ToString.Trim)
                    If intJobGroupId <= 0 Then
                        If Not objJobGrp Is Nothing Then objJobGrp = Nothing
                        objJobGrp = New clsJobGroup

                        objJobGrp._Code = dtRow.Item("job_group").ToString.Trim
                        objJobGrp._Name = dtRow.Item("job_group").ToString.Trim
                        objJobGrp._Isactive = True

                        If objJobGrp.Insert Then
                            intJobGroupId = objJobGrp._Jobgroupunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objJobGrp._Code & "/" & objJobGrp._Name & ":" & objJobGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                    End If
                End If
                'S.SANDEEP [ 29 DEC 2011 ] -- END

                '---------------------> JOB
                If dtRow.Item("job").ToString.Trim.Length > 0 Then
                    intJobId = objJob.GetJobUnkId(dtRow.Item("job").ToString.Trim)
                    If intJobId <= 0 Then
                        If Not objJob Is Nothing Then objJob = Nothing
                        objJob = New clsJobs

                        objJob._Create_Date = ConfigParameter._Object._CurrentDateAndTime
                        objJob._Isactive = True
                        objJob._Job_Code = dtRow.Item("job").ToString.Trim.ToUpper
                        objJob._Job_Level = 0
                        objJob._Jobgradeunkid = 0
                        objJob._Jobgroupunkid = 0
                        objJob._Jobunitunkid = 0
                        objJob._Report_Tounkid = 0
                        objJob._Userunkid = User._Object._Userunkid
                        objJob._Job_Name = dtRow.Item("job").ToString.Trim
                        If intSectionId > 0 Then
                            objJob._Jobsectionunkid = intSectionId
                        Else
                            objJob._Jobsectionunkid = 0
                        End If

                        'Sohail (18 Feb 2020) -- Start
                        'NMB Enhancement # : Pick employment type for type of job on job master.
                        objJob._Critical = 0
                        objJob._Jobtypeunkid = 0
                        'Sohail (18 Feb 2020) -- End

                        If objJob.Insert Then
                            intJobId = objJob._Jobunkid
                        Else
                            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objJob._Job_Code & "/" & objJob._Job_Name & ":" & objJob._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                            intJobId = -1
                        End If
                    End If
                End If



                'S.SANDEEP [ 04 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                ''---------------------> COUNTRY
                'If dtRow.Item("country").ToString.Trim.Length > 0 Then
                '    intCountryId = objCountry.GetCountryUnkId(dtRow.Item("country").ToString.Trim)
                '    If intCountryId <= 0 Then
                '        If Not objCountry Is Nothing Then objCountry = Nothing

                '        objCountry = New clsMasterData

                '        intCountryId = objCountry.InsertCountry(dtRow.Item("country").ToString.Trim)
                '    End If
                'End If

                ''---------------------> STATE
                'If dtRow.Item("state").ToString.Trim.Length > 0 Then
                '    intStateId = objState.GetStateUnkId(dtRow.Item("state").ToString.Trim)
                '    If intStateId <= 0 Then
                '        If Not objState Is Nothing Then objState = Nothing

                '        objState = New clsstate_master

                '        objState._Code = dtRow.Item("state").ToString.Trim
                '        objState._Countryunkid = intCountryId
                '        objState._Isactive = True
                '        objState._Name = dtRow.Item("state").ToString.Trim

                '        If objState.Insert Then
                '            intStateId = objState._Stateunkid
                '        Else
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                '            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objState._Code & "/" & objState._Name & ":" & objState._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                '            intStateId = -1
                '        End If
                '    End If
                'End If

                ''---------------------> CITY
                'If dtRow.Item("city").ToString.Trim.Length > 0 Then
                '    intCityId = objCity.GetCityUnkId(dtRow.Item("city").ToString.Trim)
                '    If intCityId <= 0 Then
                '        If Not objCity Is Nothing Then objCity = Nothing

                '        objCity = New clscity_master

                '        objCity._Code = dtRow.Item("city").ToString.Trim
                '        objCity._Countryunkid = intCountryId
                '        objCity._Isactive = True
                '        objCity._Name = dtRow.Item("city").ToString.Trim
                '        objCity._Stateunkid = intStateId

                '        If objCity.Insert Then
                '            intCityId = objCity._Cityunkid
                '        Else
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                '            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objCity._Code & "/" & objCity._Name & ":" & objCity._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                '            intCityId = -1
                '        End If
                '    End If
                'End If

                ''---------------------> PINCODE
                'If dtRow.Item("pincode").ToString.Trim.Length > 0 Then
                '    intPincodeId = objPincode.GetZipCodeUnkId(dtRow.Item("pincode").ToString.Trim)
                '    If intPincodeId <= 0 Then
                '        If Not objPincode Is Nothing Then objPincode = Nothing

                '        objPincode = New clszipcode_master

                '        objPincode._Cityunkid = intCityId
                '        objPincode._Countryunkid = intCountryId
                '        objPincode._Isactive = True
                '        objPincode._Stateunkid = intStateId
                '        objPincode._Zipcode_Code = dtRow.Item("pincode").ToString.Trim
                '        objPincode._Zipcode_No = dtRow.Item("pincode").ToString.Trim

                '        If objPincode.Insert Then
                '            intPincodeId = objPincode._Zipcodeunkid
                '        Else
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- START
                '            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objPincode._Zipcode_Code & "/" & objPincode._Zipcode_No & ":" & objPincode._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
                '            intPincodeId = -1
                '        End If
                '    End If
                'End If
                'S.SANDEEP [ 04 APRIL 2012 ] -- END




                '---------------------> GENDER
                If dtRow.Item("gender").ToString.Trim.Length > 0 Then
                    Dim strMale As String = Language.getMessage("clsMasterData", 73, "Male")
                    Dim strFemale As String = Language.getMessage("clsMasterData", 74, "Female")
                    Select Case dtRow.Item("gender").ToString.Trim.ToUpper
                        Case strMale.ToUpper
                            intGenderId = 1
                        Case strFemale.ToUpper
                            intGenderId = 2
                        Case Else
                            intGenderId = 0
                    End Select
                End If



                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                'Dim dsTranHead As New DataSet
                'If radDefinedSalary.Checked = True Then
                '    dsTranHead = objTranHead.getComboList("List", False, enTranHeadType.EarningForEmployees, enCalcType.DEFINED_SALARY, enTypeOf.Salary)
                '    If dsTranHead.Tables("List").Rows.Count > 0 Then
                '        intTranHeadId = CInt(dsTranHead.Tables("List").Rows(0)("tranheadunkid"))
                '    End If
                'ElseIf radGradedSalary.Checked = True Then
                '    dsTranHead = objTranHead.getComboList("List", False, enTranHeadType.EarningForEmployees, enCalcType.OnAttendance, enTypeOf.Salary)
                '    If dsTranHead.Tables("List").Rows.Count > 0 Then
                '        intTranHeadId = CInt(dsTranHead.Tables("List").Rows(0)("tranheadunkid"))
                '    End If
                'End If

                If dtRow.Item("classgroup").ToString.Trim.Length > 0 Then
                    intClassGrpId = objClassGrp.GetClassGrpUnkId(dtRow.Item("classgroup").ToString.Trim)
                    If intClassGrpId <= 0 Then
                        If Not objClassGrp Is Nothing Then objClassGrp = Nothing
                        objClassGrp = New clsClassGroup

                        objClassGrp._Code = dtRow.Item("classgroup").ToString.Trim
                        objClassGrp._Name = dtRow.Item("classgroup").ToString.Trim
                        objClassGrp._Isactive = True

                        If objClassGrp.Insert Then
                            intClassGrpId = objClassGrp._Classgroupunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objClassGrp._Code & "/" & objClassGrp._Name & ":" & objClassGrp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("classes").ToString.Trim.Length > 0 Then
                    intClassId = objClasses.GetClassUnkId(dtRow.Item("classes").ToString.Trim)
                    If intClassId <= 0 Then
                        If Not objClasses Is Nothing Then objClasses = Nothing
                        objClasses = New clsClass

                        objClasses._Code = dtRow.Item("classes").ToString.Trim
                        objClasses._Name = dtRow.Item("classes").ToString.Trim
                        objClasses._Isactive = True
                        objClasses._Classgroupunkid = intClassGrpId

                        If objClasses.Insert Then
                            intClassId = objClasses._Classesunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objClasses._Code & "/" & objClasses._Name & ":" & objClasses._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                    End If
                End If
                'S.SANDEEP [ 29 DEC 2011 ] -- END

                'Shani(17-Nov-2015) -- Start
                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                If dtRow.Item("paytype").ToString.Trim.Length > 0 Then
                    intPayTypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.PAY_TYPE, dtRow.Item("paytype").ToString.Trim)
                    If intPayTypeId <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master
                        objCMaster._Code = dtRow.Item("paytype").ToString.Trim
                        objCMaster._Alias = ""
                        objCMaster._Name = dtRow.Item("paytype").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.PAY_TYPE

                        If objCMaster.Insert Then
                            intPayTypeId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If

                    End If
                End If
                'Shani(17-Nov-2015) -- End

                'Nilay (17 Feb 2017) -- Start
                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                If dtRow.Item("paypoint").ToString.Trim.Length > 0 Then
                    Dim objPayPoint As New clspaypoint_master
                    intPayPointId = objPayPoint.GetPayPointUnkidByCode(dtRow.Item("paypoint").ToString.Trim)

                    If intPayPointId <= 0 Then
                        objPayPoint = New clspaypoint_master
                        objPayPoint._Paypointalias = ""
                        objPayPoint._Paypointcode = dtRow.Item("paypoint").ToString.Trim
                        objPayPoint._Paypointname = dtRow.Item("paypoint").ToString.Trim
                        objPayPoint._Description = ""
                        objPayPoint._Isactive = True

                        If objPayPoint.Insert Then
                            intPayPointId = objPayPoint._Paypointunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objPayPoint._Paypointcode & "/" & objPayPoint._Paypointname & ":" & objPayPoint._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If
                'Nilay (17 Feb 2017) -- End


                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If dtRow.Item("Policy").ToString.Trim.Length > 0 Then
                    objpolicy_master = New clspolicy_master
                    intPolicyId = objpolicy_master.GetPolicyUnkid(dtRow.Item("Policy").ToString.Trim)
                    If intPolicyId <= 0 Then
                        objpolicy_master._Policycode = dtRow.Item("Policy").ToString.Trim
                        objpolicy_master._Policyname = dtRow.Item("Policy").ToString.Trim
                        objpolicy_master._Userunkid = User._Object._Userunkid
                        objpolicy_master._Voiddatetime = Nothing
                        objpolicy_master._Voidreason = ""
                        objpolicy_master._Voiduserunkid = -1
                        objpolicy_master._Isvoid = False
                        objpolicy_master._dtOTPenalty = Nothing

                        Dim dtTabl As DataTable = GeneratePolicyDays()
                        If objpolicy_master.Insert(dtTabl) Then
                            intPolicyId = objpolicy_master._Policyunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objpolicy_master._Policycode & "/" & objpolicy_master._Policyname & ":" & objpolicy_master._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Complexion").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intComplexionId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.COMPLEXION, dtRow.Item("Complexion").ToString.Trim)
                    If intComplexionId <= 0 Then
                        objCMaster._Code = dtRow.Item("Complexion").ToString.Trim
                        objCMaster._Name = dtRow.Item("Complexion").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.COMPLEXION
                        If objCMaster.Insert() Then
                            intComplexionId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("BloodGroup").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intBloodGroupId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.BLOOD_GROUP, dtRow.Item("BloodGroup").ToString.Trim)
                    If intBloodGroupId <= 0 Then
                        objCMaster._Code = dtRow.Item("BloodGroup").ToString.Trim
                        objCMaster._Name = dtRow.Item("BloodGroup").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.BLOOD_GROUP
                        If objCMaster.Insert() Then
                            intBloodGroupId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("EyeColor").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intEyeColorId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.EYES_COLOR, dtRow.Item("EyeColor").ToString.Trim)
                    If intEyeColorId <= 0 Then
                        objCMaster._Code = dtRow.Item("EyeColor").ToString.Trim
                        objCMaster._Name = dtRow.Item("EyeColor").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.EYES_COLOR
                        If objCMaster.Insert() Then
                            intEyeColorId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Ethincity").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intEthinicityId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ETHNICITY, dtRow.Item("Ethincity").ToString.Trim)
                    If intEthinicityId <= 0 Then
                        objCMaster._Code = dtRow.Item("Ethincity").ToString.Trim
                        objCMaster._Name = dtRow.Item("Ethincity").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.ETHNICITY
                        If objCMaster.Insert() Then
                            intEthinicityId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Religion").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intRelegionId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RELIGION, dtRow.Item("Religion").ToString.Trim)
                    If intRelegionId <= 0 Then
                        objCMaster._Code = dtRow.Item("Religion").ToString.Trim
                        objCMaster._Name = dtRow.Item("Religion").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.RELIGION
                        If objCMaster.Insert() Then
                            intRelegionId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Hair").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intHairId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.HAIR_COLOR, dtRow.Item("Hair").ToString.Trim)
                    If intHairId <= 0 Then
                        objCMaster._Code = dtRow.Item("Hair").ToString.Trim
                        objCMaster._Name = dtRow.Item("Hair").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.HAIR_COLOR
                        If objCMaster.Insert() Then
                            intHairId = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Language1").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intLang1Id = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.LANGUAGES, dtRow.Item("Language1").ToString.Trim)
                    If intLang1Id <= 0 Then
                        objCMaster._Code = dtRow.Item("Language1").ToString.Trim
                        objCMaster._Name = dtRow.Item("Language1").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.LANGUAGES
                        If objCMaster.Insert() Then
                            intLang1Id = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Language2").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intLang2Id = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.LANGUAGES, dtRow.Item("Language2").ToString.Trim)
                    If intLang2Id <= 0 Then
                        objCMaster._Code = dtRow.Item("Language2").ToString.Trim
                        objCMaster._Name = dtRow.Item("Language2").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.LANGUAGES
                        If objCMaster.Insert() Then
                            intLang2Id = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Language3").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intLang3Id = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.LANGUAGES, dtRow.Item("Language3").ToString.Trim)
                    If intLang3Id <= 0 Then
                        objCMaster._Code = dtRow.Item("Language3").ToString.Trim
                        objCMaster._Name = dtRow.Item("Language3").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.LANGUAGES
                        If objCMaster.Insert() Then
                            intLang3Id = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("Language4").ToString.Trim.Length > 0 Then
                    objCMaster = New clsCommon_Master
                    intLang4Id = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.LANGUAGES, dtRow.Item("Language4").ToString.Trim)
                    If intLang4Id <= 0 Then
                        objCMaster._Code = dtRow.Item("Language4").ToString.Trim
                        objCMaster._Name = dtRow.Item("Language4").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.LANGUAGES
                        If objCMaster.Insert() Then
                            intLang4Id = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                Dim dblHeight As Double = 0 : Dim dblWeight As Double = 0
                If dtRow.Item("Height").ToString.Trim.Length > 0 Then
                    Double.TryParse(dtRow.Item("Height").ToString.Trim, dblHeight)
                    If dblHeight <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 100, "Invalid Height Data.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("Weight").ToString.Trim.Length > 0 Then
                    Double.TryParse(dtRow.Item("Weight").ToString.Trim, dblWeight)
                    If dblWeight <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 101, "Invalid Weight Data.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                Dim mstrAllergies As String = "" : Dim mstrDisabilities As String = ""
                Dim intUnkid As Integer = 0
                If dtRow.Item("Allergies").ToString.Trim.Length > 0 Then
                    Dim strAllergy() As String = dtRow.Item("Allergies").ToString.Trim.Split(CChar(","))
                    If strAllergy.Length > 0 Then
                        Dim strData As String = ""
                        For index As Integer = 0 To strAllergy.Length - 1
                            strData = strAllergy(index).ToString().Trim
                            If strData.Trim.Length > 0 Then
                                objCMaster = New clsCommon_Master
                                intUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ALLERGIES, strData)
                                If intUnkid <= 0 Then
                                    objCMaster._Code = strData
                                    objCMaster._Name = strData
                                    objCMaster._Isactive = True
                                    objCMaster._Mastertype = clsCommon_Master.enCommonMaster.ALLERGIES
                                    If objCMaster.Insert() Then
                                        intUnkid = objCMaster._Masterunkid
                                    Else
                                        dtRow.Item("image") = imgError
                                        dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                        dtRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                    End If
                                End If
                                mstrAllergies &= "," & intUnkid.ToString()
                            End If
                        Next
                    End If                    
                End If
                If mstrAllergies.Trim.Length > 0 Then mstrAllergies = Mid(mstrAllergies, 2)

                intUnkid = 0
                If dtRow.Item("MedicalDisabilities").ToString.Trim.Length > 0 Then
                    Dim strDisabilities() As String = dtRow.Item("MedicalDisabilities").ToString.Trim.Split(CChar(","))
                    If strDisabilities.Length > 0 Then
                        Dim strData As String = ""
                        For index As Integer = 0 To strDisabilities.Length - 1
                            strData = strDisabilities(index).ToString().Trim
                            If strData.Trim.Length > 0 Then
                                objCMaster = New clsCommon_Master
                                intUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.DISABILITIES, strData)
                                If intUnkid <= 0 Then
                                    objCMaster._Code = strData
                                    objCMaster._Name = strData
                                    objCMaster._Isactive = True
                                    objCMaster._Mastertype = clsCommon_Master.enCommonMaster.DISABILITIES
                                    If objCMaster.Insert() Then
                                        intUnkid = objCMaster._Masterunkid
                                    Else
                                        dtRow.Item("image") = imgError
                                        dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                        dtRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                    End If
                                End If
                                mstrDisabilities &= "," & intUnkid.ToString()
                            End If
                        Next
                    End If
                End If
                If mstrDisabilities.Trim.Length > 0 Then mstrDisabilities = Mid(mstrDisabilities, 2)

                If dtRow.Item("Nationality").ToString.Trim.Length > 0 Then
                    objCountry = New clsMasterData
                    intNationalityId = objCountry.GetCountryUnkId(dtRow.Item("Nationality").ToString.Trim)
                End If
                'S.SANDEEP [14-JUN-2018] -- END


                Dim blnResult As Boolean = True
                Try
                    objEmp = New clsEmployee_Master

                    'S.SANDEEP [ 10 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If dtRow.Item("displayname").ToString.Trim.Length > 0 Then
                    '    intEmpId = -1
                    '    intEmpId = objEmp.GetEmployeeUnkid(dtRow.Item("displayname").ToString.Trim)
                    'End If

                    If dtRow.Item("displayname").ToString.Trim.Length > 0 Then
                        intEmpId = -1
                        intEmpId = objEmp.GetEmployeeUnkid(dtRow.Item("displayname").ToString.Trim.ToLower)
                    End If
                    'S.SANDEEP [ 10 DEC 2012 ] -- END



                    If intEmpId <= 0 AndAlso dtRow.Item("displayname").ToString.Trim.Length <= 0 _
                    Or dtRow.Item("firstname").ToString.Trim.Length <= 0 _
                    Or dtRow.Item("surname").ToString.Trim.Length <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                    If intEmpId <= 0 Then

                        Try
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'objEmp._Appointeddate = CDate(dtRow.Item("appointeddate"))

                            'S.SANDEEP [ 09 AUG 2011 ] -- START
                            'If dtRow.Item("appointeddate").ToString.Trim.Length > 0 Then
                            If dtRow.Item("appointeddate").ToString.Trim.Length > 0 And IsDate(dtRow.Item("appointeddate")) Then
                                'S.SANDEEP [ 09 AUG 2011 ] -- END 
                                objEmp._Appointeddate = CDate(dtRow.Item("appointeddate"))
                                'S.SANDEEP [ 12 OCT 2011 ] -- START

                                'S.SANDEEP [ 27 AUG 2014 ] -- START
                                'If ConfigParameter._Object._RetirementBy = 1 Then
                                '    Dim dtRetireDate As Date = Nothing
                                '    dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                '    objEmp._Termination_To_Date = dtRetireDate
                                'End If
                                If intGenderId > 0 Then
                                    Select Case intGenderId
                                        Case 1  'MALE
                                            If ConfigParameter._Object._RetirementBy = 1 Then
                                                Dim dtRetireDate As Date = Nothing
                                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                                objEmp._Termination_To_Date = dtRetireDate
                                            End If
                                        Case 2  'FEMALE
                                            If ConfigParameter._Object._FRetirementBy = 1 Then
                                                Dim dtRetireDate As Date = Nothing
                                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._FRetirementValue, CDate(dtRow.Item("appointeddate")))
                                                objEmp._Termination_To_Date = dtRetireDate
                                            End If
                                    End Select
                                Else
                                    If ConfigParameter._Object._RetirementBy = 1 Then
                                        Dim dtRetireDate As Date = Nothing
                                        dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                        objEmp._Termination_To_Date = dtRetireDate
                                    End If
                                End If
                                'S.SANDEEP [ 27 AUG 2014 ] -- END

                                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 32, "Invalid Appointment date")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try

                        'Sohail (10 Apr 2013) -- Start
                        'TRA - ENHANCEMENT
                        Try
                            If ConfigParameter._Object._RetirementBy <> 1 Then
                                If dtRow.Item("retirementdate").ToString.Trim.Length > 0 And IsDate(dtRow.Item("retirementdate")) Then
                                    If CDate(dtRow.Item("retirementdate")) > CDate(dtRow.Item("appointeddate")) Then
                                        objEmp._Termination_To_Date = CDate(dtRow.Item("retirementdate"))

                                        'S.SANDEEP [ 27 AUG 2014 ] -- START
                                        'If ConfigParameter._Object._RetirementBy = 1 Then
                                        '    Dim dtRetireDate As Date = Nothing
                                        '    dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                        '    objEmp._Termination_To_Date = dtRetireDate
                                        'End If
                                        If intGenderId > 0 Then
                                            Select Case intGenderId
                                                Case 1  'MALE
                                                    If ConfigParameter._Object._RetirementBy = 1 Then
                                                        Dim dtRetireDate As Date = Nothing
                                                        dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                                        objEmp._Termination_To_Date = dtRetireDate
                                                    End If
                                                Case 2  'FEMALE
                                                    If ConfigParameter._Object._FRetirementBy = 1 Then
                                                        Dim dtRetireDate As Date = Nothing
                                                        dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._FRetirementValue, CDate(dtRow.Item("appointeddate")))
                                                        objEmp._Termination_To_Date = dtRetireDate
                                                    End If
                                            End Select
                                        Else
                                            If ConfigParameter._Object._RetirementBy = 1 Then
                                                Dim dtRetireDate As Date = Nothing
                                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("appointeddate")))
                                                objEmp._Termination_To_Date = dtRetireDate
                                            End If
                                        End If
                                        'S.SANDEEP [ 27 AUG 2014 ] -- END
                                    Else
                                        dtRow.Item("image") = imgError
                                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Retirement date should be greater than Appointment date.")
                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                        dtRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                    End If
                                Else
                                    dtRow.Item("image") = imgError
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 33, "Invalid Retirement date")
                                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                    dtRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try
                        'Sohail (10 Apr 2013) -- End

                        Try
                            'Sandeep [ 25 APRIL 2011 ] -- Start
                            'objEmp._Birthdate = CDate(dtRow.Item("birthdate"))

                            'S.SANDEEP [ 09 AUG 2011 ] -- START
                            'If dtRow.Item("birthdate").ToString.Trim.Length > 0 Then
                            If dtRow.Item("birthdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("birthdate")) Then
                                'S.SANDEEP [ 09 AUG 2011 ] -- END 
                                objEmp._Birthdate = CDate(dtRow.Item("birthdate"))
                                'S.SANDEEP [ 12 OCT 2011 ] -- START


                                'S.SANDEEP [ 27 AUG 2014 ] -- START
                                'If ConfigParameter._Object._RetirementBy = 1 Then
                                '    Dim dtRetireDate As Date = Nothing
                                '    dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("birthdate")))
                                '    objEmp._Termination_To_Date = dtRetireDate
                                'End If
                                If intGenderId > 0 Then
                                    Select Case intGenderId
                                        Case 1  'MALE
                                            If ConfigParameter._Object._RetirementBy = 1 Then
                                                Dim dtRetireDate As Date = Nothing
                                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("birthdate")))
                                                objEmp._Termination_To_Date = dtRetireDate
                                            End If
                                        Case 2  'FEMALE
                                            If ConfigParameter._Object._FRetirementBy = 1 Then
                                                Dim dtRetireDate As Date = Nothing
                                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._FRetirementValue, CDate(dtRow.Item("birthdate")))
                                                objEmp._Termination_To_Date = dtRetireDate
                                            End If
                                    End Select
                                Else
                                    If ConfigParameter._Object._RetirementBy = 1 Then
                                        Dim dtRetireDate As Date = Nothing
                                        dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, CDate(dtRow.Item("birthdate")))
                                        objEmp._Termination_To_Date = dtRetireDate
                                    End If
                                End If
                                'S.SANDEEP [ 27 AUG 2014 ] -- END


                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                'Anjan [ 25 Apr 2013 ] -- Start
                                'ENHANCEMENT : TRA CHANGES
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 34, "Invalid Birth date")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                'Anjan [ 25 Apr 2013 ] -- End
                            End If
                            'Sandeep [ 25 APRIL 2011 ] -- End 
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try


                        'S.SANDEEP [ 10 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'If radActiveEmployee.Checked = True Then
                        '    objEmp._Isactive = True
                        'ElseIf radInactiveEmployee.Checked = True Then
                        '    Try
                        '        If dtRow.Item("terminationdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("terminationdate")) Then
                        '            objEmp._Termination_From_Date = CDate(dtRow.Item("terminationdate"))
                        '            'S.SANDEEP [ 28 FEB 2012 ] -- START
                        '            'ENHANCEMENT : TRA CHANGES
                        '            If objEmp._Termination_To_Date < objEmp._Termination_From_Date Then
                        '                Throw New Exception()
                        '            End If
                        '            'S.SANDEEP [ 28 FEB 2012 ] -- END
                        '        End If
                        '    Catch ex As Exception
                        '        dtRow.Item("image") = imgError
                        '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                        '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        '        dtRow.Item("objStatus") = 2
                        '        objError.Text = CStr(Val(objError.Text) + 1)
                        '        Continue For
                        '    End Try
                        '    objEmp._Isactive = False
                        '    objEmp._Actionreasonunkid = intReasonId
                        'End If

                        objEmp._Isactive = True
                        Try
                            If dtRow.Item("terminationdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("terminationdate")) Then
                                objEmp._Termination_From_Date = CDate(dtRow.Item("terminationdate"))
                                If objEmp._Termination_To_Date < objEmp._Termination_From_Date Then
                                    Throw New Exception()
                                End If
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try
                        objEmp._Actionreasonunkid = intReasonId
                        'S.SANDEEP [ 10 DEC 2012 ] -- END


                        Try
                            If dtRow.Item("marrieddate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("marrieddate")) Then
                                objEmp._Anniversary_Date = CDate(dtRow.Item("marrieddate"))
                                objEmp._Maritalstatusunkid = intMarriedId
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try


                        'Gajanan [29-Oct-2020] -- Start   
                        'Enhancement:Worked On Succession Module
                        If dtRow.Item("job").ToString.Trim.Length > 0 Then
                            If objJob.IsJobAssignAsKeyRoleToEmployee(intJobId, intEmpId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) Then
                                dtRow.Item("image") = imgWarring
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 117, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                                dtRow.Item("objStatus") = 0
                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                Continue For
                            End If
                        End If
                        'Gajanan [29-Oct-2020] -- End

                        objEmp._Sectiongroupunkid = intSectionGrpId
                        objEmp._Unitgroupunkid = intUnitGrpId
                        objEmp._Unitunkid = intUnitId
                        objEmp._Teamunkid = intTeamId
                        objEmp._Titalunkid = intTitleId
                        objEmp._Maritalstatusunkid = intMarriedId
                        objEmp._Departmentunkid = intDepartId
                        'S.SANDEEP [ 10 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objEmp._Displayname = dtRow.Item("displayname").ToString
                        objEmp._Displayname = dtRow.Item("displayname").ToString.ToLower
                        'S.SANDEEP [ 10 DEC 2012 ] -- END
                        objEmp._Employeecode = dtRow.Item("employeecode").ToString
                        objEmp._Employmenttypeunkid = intEmplTypeId
                        objEmp._Firstname = dtRow.Item("firstname").ToString
                        objEmp._Gender = intGenderId
                        objEmp._Gradegroupunkid = intGradeGrpId
                        objEmp._Gradelevelunkid = intGLevelId
                        objEmp._Gradeunkid = intGradeId
                        objEmp._Jobunkid = intJobId
                        objEmp._Othername = dtRow.Item("othername").ToString

                        'S.SANDEEP [ 01 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objEmp._Password = dtRow.Item("displayname").ToString
                        If ConfigParameter._Object._IsPasswordAutoGenerated = True Then
                            Dim objPwdOp As New clsPassowdOptions
                            If objPwdOp._IsPasswordLenghtSet Then
                                If objPwdOp._PasswordLength < 6 Then
                                    objEmp._Password = clsRandomPassword.Generate(8, 8)
                                Else
                                    objEmp._Password = clsRandomPassword.Generate(objPwdOp._PasswordLength, objPwdOp._PasswordLength)
                                End If
                            Else
                                objEmp._Password = clsRandomPassword.Generate(8, 8)
                            End If
                        Else
                            objEmp._Password = clsRandomPassword.Generate(8, 8)
                        End If
                        'S.SANDEEP [ 01 DEC 2012 ] -- END

                        objEmp._Present_Address1 = dtRow.Item("address1").ToString
                        objEmp._Present_Address2 = dtRow.Item("address2").ToString
                        objEmp._Present_Countryunkid = intCountryId
                        objEmp._Present_Post_Townunkid = intCityId
                        objEmp._Present_Stateunkid = intStateId
                        objEmp._Present_Postcodeunkid = intPincodeId
                        'S.SANDEEP [ 23 JUNE 2011 ] -- START
                        'ISSUE : DECIMAL CHANGES
                        'objEmp._Scale = dblEmpScale
                        objEmp._Scale = decEmpScale
                        'S.SANDEEP [ 23 JUNE 2011 ] -- END 
                        objEmp._Sectionunkid = intSectionId


                        'Pinkal (15-Oct-2013) -- Start
                        'Enhancement : TRA Changes
                        'objEmp._Shiftunkid = intShiftId
                        'Pinkal (15-Oct-2013) -- End


                        objEmp._Stationunkid = intBranchId
                        objEmp._Surname = dtRow.Item("surname").ToString

                        'S.SANDEEP [ 29 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES 
                        'TYPE : EMPLOYEMENT CONTRACT PROCESS
                        'objEmp._Tranhedunkid = intTranHeadId
                        objEmp._Tranhedunkid = CInt(cboTransHead.SelectedValue)
                        objEmp._Deptgroupunkid = intDeptGrpId
                        objEmp._Jobgroupunkid = intJobGroupId
                        objEmp._Classgroupunkid = intClassGrpId
                        objEmp._Classunkid = intClassId
                        'S.SANDEEP [ 29 DEC 2011 ] -- END
                        objEmp._Costcenterunkid = intCostCenterId

                        'S.SANDEEP [ 01 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES


                        'Shani(17-Nov-2015) -- Start
                        'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                        objEmp._Paytypeunkid = intPayTypeId
                        'Shani(17-Nov-2015) -- End

                        'Nilay (17 Feb 2017) -- Start
                        'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
                        objEmp._Paypointunkid = intPayPointId
                        'Nilay (17 Feb 2017) -- End

                        'Sohail (18 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                        objEmp._AssignDefaultTransactionHeads = chkAssignDefaulTranHeads.Checked
                        'Sohail (18 Feb 2019) -- End

                        'Pinkal (01-Dec-2012) -- Start
                        'Enhancement : TRA Changes

                        If ConfigParameter._Object._DisplayNameSetting = enDisplayNameSetting.EMPLOYEECODE Then
                            If dtRow.Item("email").ToString.Trim.Length > 0 Then
                                objEmp._IsAutomateEmail = False
                            Else
                                objEmp._IsAutomateEmail = True
                            End If
                        End If

                        'Pinkal (01-Dec-2012) -- End

                        objEmp._Email = dtRow.Item("email").ToString.Trim

                        'S.SANDEEP [ 01 SEP 2012 ] -- END

                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Issue : to be set as false by default on import.
                        'objEmp._Isapproved = User._Object.Privilege._AllowToApproveEmployee
                        objEmp._Isapproved = False
                        'S.SANDEEP [ 18 SEP 2012 ] -- END

                        'S.SANDEEP [ 23 JAN 2012 ] -- START_
                        'ENHANCEMENT : TRA CHANGES
                        Try
                            If dtRow.Item("appointeddate").ToString.Trim.Length > 0 And IsDate(dtRow.Item("appointeddate")) Then
                                'S.SANDEEP [23-JUN-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002336}
                                'objEmp._Confirmation_Date = CDate(dtRow.Item("appointeddate"))
                                If ConfigParameter._Object._ConfirmationMonth > 0 Then
                                    objEmp._Confirmation_Date = CDate(dtRow.Item("appointeddate")).AddMonths(ConfigParameter._Object._ConfirmationMonth)
                                Else
                                objEmp._Confirmation_Date = CDate(dtRow.Item("appointeddate"))
                            End If
                                'S.SANDEEP [23-JUN-2018] -- END
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Data")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try
                        'S.SANDEEP [ 23 JAN 2012 ] -- END
                    Else
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = intEmpId
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                        'S.SANDEEP [04 JUN 2015] -- END
                    End If

                    'Anjan [ 29 May 2013 ] -- Start
                    'ENHANCEMENT : Recruitment TRA changes requested by Andrew
                    objEmp._Companyunkid = Company._Object._Companyunkid
                    'Anjan [ 29 May 2013 ] -- End

                    'S.SANDEEP [ 18 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If intShiftId > 0 Then
                        Dim objSftTran As New clsEmployee_Shift_Tran
                        objSftTran._EmployeeUnkid = intEmpId
                        Dim mdTable As DataTable
                        mdTable = objSftTran._SDataTable.Copy

                        Dim dtSRow As DataRow
                        dtSRow = mdTable.NewRow
                        dtSRow.Item("shifttranunkid") = -1
                        dtSRow.Item("employeeunkid") = intEmpId
                        dtSRow.Item("shiftunkid") = intShiftId
                        dtSRow.Item("isdefault") = True
                        dtSRow.Item("userunkid") = User._Object._Userunkid
                        dtSRow.Item("isvoid") = False
                        dtSRow.Item("voiduserunkid") = -1
                        dtSRow.Item("voiddatetime") = DBNull.Value
                        dtSRow.Item("voidreason") = ""
                        dtSRow.Item("AUD") = "A"
                        'S.SANDEEP [ 26 SEPT 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtSRow.Item("effectivedate") = objEmp._Appointeddate
                        'S.SANDEEP [ 26 SEPT 2013 ] -- END
                        mdTable.Rows.Add(dtSRow)
                        objEmp._dtShiftTran = mdTable.Copy
                    End If
                    'S.SANDEEP [ 18 DEC 2012 ] -- END

                    'S.SANDEEP [ 10 DEC 2013 ] -- START
                    Try
                        If dtRow.Item("eocdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("eocdate")) Then
                            objEmp._Empl_Enddate = CDate(dtRow.Item("eocdate"))
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 35, "Invalid EOC date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try
                    'S.SANDEEP [ 10 DEC 2013 ] -- END

                    'Varsha (10 Nov 2017) -- Start
                    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
                    objEmp._Theme_Id = intThemeId
                    'Varsha (10 Nov 2017) -- End

                    'S.SANDEEP [14-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {NMB}
                    objEmp._PolicyUnkid = intPolicyId
                    objEmp._Complexionunkid = intComplexionId
                    If dtRow("SportHobbies").ToString.Trim.Length > 0 Then
                        objEmp._Sports_Hobbies = dtRow("SportHobbies").ToString.Trim
                    Else
                        objEmp._Sports_Hobbies = ""
                    End If
                    objEmp._Bloodgroupunkid = intBloodGroupId
                    objEmp._Eyecolorunkid = intEyeColorId
                    objEmp._Nationalityunkid = intNationalityId
                    objEmp._Ethincityunkid = intEthinicityId
                    objEmp._Religionunkid = intRelegionId
                    objEmp._Hairunkid = intHairId
                    objEmp._Language1unkid = intLang1Id
                    objEmp._Language2unkid = intLang2Id
                    objEmp._Language3unkid = intLang3Id
                    objEmp._Language4unkid = intLang4Id
                    If dtRow("ExtraTel").ToString.Trim.Length > 0 Then
                        objEmp._Extra_Tel_No = dtRow("ExtraTel").ToString.Trim
                    Else
                        objEmp._Extra_Tel_No = ""
                    End If
                    objEmp._Height = dblHeight
                    objEmp._Weight = dblWeight
                    If mstrAllergies.Trim.Length > 0 Then
                        objEmp._Allergies = mstrAllergies
                    End If
                    If mstrDisabilities.Trim.Length > 0 Then
                        objEmp._Disabilities = mstrDisabilities
                    End If
                    'S.SANDEEP [14-JUN-2018] -- END

                    'S.SANDEEP [26-SEP-2018] -- START
                    objEmp._IsFlexAccountCreated = False
                    'S.SANDEEP [26-SEP-2018] -- END
'Sohail (02 Mar 2020) -- Start
                    'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                    objEmp._BaseCountryunkid = Company._Object._Localization_Country
                    'Sohail (02 Mar 2020) -- End

                    If intEmpId <= 0 Then
                        'Sohail (10 Apr 2013) -- Start
                        'TRA - ENHANCEMENT

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objEmp.Insert(Nothing, Nothing, True) Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objEmp.Insert(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, Nothing, Nothing, True) Then

                        'Nilay (27 Apr 2016) -- Start
                        'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                        '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
                        'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                        '                 CStr(ConfigParameter._Object._IsArutiDemo), _
                        '                 Company._Object._Total_Active_Employee_ForAllCompany, _
                        '                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                        '                 ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                 ConfigParameter._Object._UserAccessModeSetting, True, _
                        '                 ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime) Then

                        'S.SANDEEP [08 DEC 2016] -- START
                        'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                        'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                        '                 CStr(ConfigParameter._Object._IsArutiDemo), _
                        '                 Company._Object._Total_Active_Employee_ForAllCompany, _
                        '                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                        '                 ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                 ConfigParameter._Object._UserAccessModeSetting, True, _
                        '                 ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                        '                 ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                        '                 ConfigParameter._Object._SalaryAnniversaryMonthBy) Then


                        'Pinkal (18-Aug-2018) -- Start
                        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                        'If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                        '                 CStr(ConfigParameter._Object._IsArutiDemo), _
                        '                 Company._Object._Total_Active_Employee_ForAllCompany, _
                        '                 ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                        '                 ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                        '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                 ConfigParameter._Object._UserAccessModeSetting, True, _
                        '             ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                        '             ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                        '             ConfigParameter._Object._SalaryAnniversaryMonthBy, , , , , , , , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then

                        If objEmp.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                                         CStr(ConfigParameter._Object._IsArutiDemo), _
                                         Company._Object._Total_Active_Employee_ForAllCompany, _
                                         ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                                         ConfigParameter._Object._EmployeeCodePrifix, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                                     ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._SalaryAnniversarySetting, _
                                    ConfigParameter._Object._SalaryAnniversaryMonthBy, ConfigParameter._Object._CreateADUserFromEmpMst, , , , , , , , , getHostName(), _
                                    getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then

                            'Pinkal (18-Aug-2018) -- End

                            'S.SANDEEP [08 DEC 2016] -- END


                            'Nilay (27 Apr 2016) -- End


                            'Sohail (21 Aug 2015) -- End
                            'S.SANDEEP [04 JUN 2015] -- END

                            'Dim blnEmployeeExceed As Boolean = False
                            'If objEmp.Insert(Nothing, Nothing, True, , blnEmployeeExceed) Then
                            'Sohail (10 Apr 2013) -- End

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'intEmpId = objEmp._Employeeunkid
                            intEmpId = objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                            'S.SANDEEP [04 JUN 2015] -- END

                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 21, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objEmp._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            'Sohail (10 Apr 2013) -- Start
                            'TRA - ENHANCEMENT
                            'If blnEmployeeExceed = True Then
                            '    Exit For
                            'End If
                            'Sohail (10 Apr 2013) -- End
                        End If
                    Else
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Employee Already Exist")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 20, "Fail")
                        dtRow.Item("objStatus") = 0

                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    End If

                Catch ex As Exception
                    DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
                End Try

            Next

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If Val(objSuccess.Text) > 0 Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 18 SEP 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Send_Notification()
        Try
            If User._Object.Privilege._AllowToApproveEmployee = False Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                Dim dUList As New DataSet
                dUList = objUsr.Get_UserBy_PrivilegeId(344)
                If dUList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dUList.Tables(0).Rows
                        Dim objSMail As New clsSendMail
                        'S.SANDEEP [ 31 DEC 2013 ] -- START
                        'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")))
                        StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")))
                        'S.SANDEEP [ 31 DEC 2013 ] -- END
                        objSMail._ToEmail = CStr(dRow.Item("UEmail"))
                        objSMail._Subject = Language.getMessage(mstrModuleName, 30, "Notifications to Approve newly Hired Employee(s)")
                        objSMail._Message = StrMessage
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        'Sohail (17 Dec 2014) -- Start
                        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                        'objSMail._Form_Name = mstrModuleName
                        objSMail._Form_Name = "" 'Please pass Form Name for WEB
                        'Sohail (17 Dec 2014) -- End
                        objSMail._LogEmployeeUnkid = -1
                        objSMail._OperationModeId = enLogin_Mode.DESKTOP
                        objSMail._UserUnkid = User._Object._Userunkid
                        objSMail._SenderAddress = User._Object._Email
                        objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSMail.SendMail()
                        objSMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                        objSMail = Nothing
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal iUserId As Integer) As String 'S.SANDEEP [ 31 DEC 2013 ] -- START {iUserId} -- END
        'Private Function Set_Notification_Approvals(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 111, "Dear") & " " & "<b>" & " " & getTitleCase(StrUserName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End



            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that " & objSuccess.Text.ToString & " employee(s) have been added to the employee list by importation please go approve them. </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 112, "This is to inform you that") & " " & objSuccess.Text.ToString & " " & Language.getMessage(mstrModuleName, 113, "employee(s) have been added to the employee list by importation please go approve them.") & "</span></p>")


            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b> </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 114, "By user") & " " & "<b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b>" & Language.getMessage(mstrModuleName, 115, "from Machine") & "<b>" & getHostName.ToString & " " & "</b>" & Language.getMessage(mstrModuleName, 116, "and IPAddress") & "<b>" & getIP.ToString & "</b> </span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 36, "Please click the link below to approve/reject employee.") & "</span></p>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append(ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid & "|" & iUserId & "|" & FinancialYear._Object._YearUnkid & "|" & "1")) & "</span></p>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        End Try
    End Function
    'S.SANDEEP [ 18 SEP 2012 ] -- END



    'Pinkal (01-Dec-2012) -- Start
    'Enhancement : TRA Changes


    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Function GeneratePolicyDays() As DataTable
        Dim mdtPolicyDays As DataTable = Nothing
        Try
            Dim objPolicyTran As New clsPolicy_tran
            objPolicyTran.GetPolicyTran()
            mdtPolicyDays = objPolicyTran._DataList.Clone()
            objPolicyTran.GenerateDefaultDays(mdtPolicyDays, 0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GeneratePolicyDays", mstrModuleName)
        Finally
        End Try
        Return mdtPolicyDays
    End Function
    'S.SANDEEP [14-JUN-2018] -- END

    Private Function GenerateShiftDays() As DataTable
        Dim mdtDays As DataTable = Nothing
        Try
            Dim days() As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames

            If mdtDays Is Nothing Then
                mdtDays = New DataTable("ShiftDays")
                mdtDays.Columns.Add("shifttranunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("shiftunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("dayId", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("days", Type.GetType("System.String"))
                mdtDays.Columns.Add("isweekend", Type.GetType("System.Boolean"))
                mdtDays.Columns.Add("starttime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("endtime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("breaktime", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("workinghrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("calcovertimeafter", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcshorttimebefore", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halffromhrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("halftohrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("nightfromhrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("nighttohrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("breaktimeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("workinghrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcovertimeafterinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcshorttimebeforeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halffromhrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halftohrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("AUD", Type.GetType("System.String"))
                'S.SANDEEP [12 MAY 2016] -- START
                mdtDays.Columns.Add("daystart_time", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
                'Dim objShiftTran As New clsshift_tran
                'objShiftTran.GetShiftTran()
                'mdtDays = objShiftTran._dtShiftday.Clone()
                'S.SANDEEP [12 MAY 2016] -- END


                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]
                mdtDays.Columns.Add("countotmins_aftersftendtime", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("countotmins_aftersftendtimeinsec", Type.GetType("System.Int32"))
                'Pinkal (03-Aug-2018) -- End


                For i As Integer = 0 To 6
                    Dim dr As DataRow = mdtDays.NewRow
                    dr("shifttranunkid") = -1
                    dr("shiftunkid") = -1
                    dr("dayId") = i
                    dr("Days") = days(i).ToString()
                    dr("isweekend") = False
                    'S.SANDEEP [15-SEP-2017] -- START
                    'dr("starttime") = ConfigParameter._Object._CurrentDateAndTime.Date
                    'dr("endtime") = ConfigParameter._Object._CurrentDateAndTime.Date

                    dr("starttime") = ConfigParameter._Object._CurrentDateAndTime
                    dr("endtime") = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [15-SEP-2017] -- END
                    dr("breaktime") = 0
                    dr("workinghrs") = 0.0
                    dr("calcovertimeafter") = 0
                    dr("calcshorttimebefore") = 0
                    dr("halffromhrs") = 0.0
                    dr("halftohrs") = 0.0
                    'S.SANDEEP [15-SEP-2017] -- START
                    'dr("nightfromhrs") = ConfigParameter._Object._CurrentDateAndTime.Date
                    'dr("nighttohrs") = ConfigParameter._Object._CurrentDateAndTime.Date

                    dr("nightfromhrs") = ConfigParameter._Object._CurrentDateAndTime
                    dr("nighttohrs") = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [15-SEP-2017] -- END
                    dr("breaktimeinsec") = 0
                    dr("workinghrsinsec") = 0
                    dr("calcovertimeafterinsec") = 0
                    dr("calcshorttimebeforeinsec") = 0
                    dr("halffromhrsinsec") = 0.0
                    dr("halftohrsinsec") = 0.0
                    dr("AUD") = "A"
                    'S.SANDEEP [12 MAY 2016] -- START

                    'S.SANDEEP [15-SEP-2017] -- START
                    'dr("daystart_time") = DBNull.Value
                    dr("daystart_time") = ConfigParameter._Object._CurrentDateAndTime.AddHours(-5)
                    'S.SANDEEP [15-SEP-2017] -- END

                    'S.SANDEEP [12 MAY 2016] -- END


                    'Pinkal (03-Aug-2018) -- Start
                    'Enhancement - Changes For B5 [Ref #289]
                    dr("countotmins_aftersftendtime") = 0
                    dr("countotmins_aftersftendtimeinsec") = 0
                    'Pinkal (03-Aug-2018) -- End

                    mdtDays.Rows.Add(dr)
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateShiftDays", mstrModuleName)
            Return Nothing
        End Try
        Return mdtDays
    End Function

    'Pinkal (01-Dec-2012) -- End

    'S.SANDEEP [ 18 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'objShiftTran._EmployeeUnkid = mintEmployeeUnkid
    '            mdtShiftTran = objShiftTran._SDataTable
    'S.SANDEEP [ 18 DEC 2012 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Radio Button's Events "

    Private Sub radDefinedSalary_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radDefinedSalary.CheckedChanged, _
                                                                                                                    radGradedSalary.CheckedChanged
        Try

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            Dim objTranHead As New clsTransactionHead : Dim dsTranHead As New DataSet
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            Select Case CType(sender, RadioButton).Name.ToUpper
                Case "RADDEFINEDSALARY"
                    lblSalary.Text = Language.getMessage(mstrModuleName, 17, "Salary")
                    'S.SANDEEP [ 29 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES 
                    'TYPE : EMPLOYEMENT CONTRACT PROCESS
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsTranHead = objTranHead.getComboList("List", True, enTranHeadType.EarningForEmployees, enCalcType.DEFINED_SALARY, enTypeOf.Salary)
                    dsTranHead = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EarningForEmployees, enCalcType.DEFINED_SALARY, enTypeOf.Salary)
                    'Sohail (21 Aug 2015) -- End

                    'S.SANDEEP [ 29 DEC 2011 ] -- END
                Case "RADGRADEDSALARY"
                    lblSalary.Text = Language.getMessage(mstrModuleName, 18, "Increment")
                    'S.SANDEEP [ 29 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES 
                    'TYPE : EMPLOYEMENT CONTRACT PROCESS
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsTranHead = objTranHead.getComboList("List", True, enTranHeadType.EarningForEmployees, enCalcType.OnAttendance, enTypeOf.Salary)
                    dsTranHead = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EarningForEmployees, enCalcType.OnAttendance, enTypeOf.Salary)
                    'Sohail (21 Aug 2015) -- End
                    'S.SANDEEP [ 29 DEC 2011 ] -- END
                    cboSalary.SelectedIndex = -1
            End Select

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            With cboTransHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsTranHead.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 29 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radDefinedSalary_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub radActiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radActiveEmployee.CheckedChanged, _
    '                                                                                                                 radInactiveEmployee.CheckedChanged
    '    Try
    '        Select Case CType(sender, RadioButton).Name.ToUpper
    '            Case "RADACTIVEEMPLOYEE"
    '                If CType(sender, RadioButton).Checked = True Then
    '                    cboTerminationDate.SelectedIndex = -1 : cboTerminationReason.SelectedIndex = -1
    '                    cboTerminationDate.Enabled = False : cboTerminationReason.Enabled = False
    '                End If
    '            Case "RADINACTIVEEMPLOYEE"
    '                If CType(sender, RadioButton).Checked = True Then
    '                    cboTerminationDate.SelectedIndex = -1 : cboTerminationReason.SelectedIndex = -1
    '                    cboTerminationDate.Enabled = True : cboTerminationReason.Enabled = True
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radActiveEmployee_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region "ContextMenu's Event"


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            dvGriddata.RowFilter = "objStatus = '2'"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                Dim savDialog As New SaveFileDialog
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Employee Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

#End Region

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
#Region " Link Event(s) "

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable                

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                '.Columns.Add("EmployeeCode", System.Type.GetType("System.String"))
                '.Columns.Add("FirstName", System.Type.GetType("System.String"))
                '.Columns.Add("OtherName", System.Type.GetType("System.String"))
                '.Columns.Add("Surname", System.Type.GetType("System.String"))
                '.Columns.Add("EmploymentType", System.Type.GetType("System.String"))
                '.Columns.Add("Shift", System.Type.GetType("System.String"))
                '.Columns.Add("Department", System.Type.GetType("System.String"))
                '.Columns.Add("Job", System.Type.GetType("System.String"))
                '.Columns.Add("Costcenter", System.Type.GetType("System.String"))
                '.Columns.Add("GradeGroup", System.Type.GetType("System.String"))
                '.Columns.Add("GradeName", System.Type.GetType("System.String"))
                '.Columns.Add("GradeLevel", System.Type.GetType("System.String"))
                '.Columns.Add("Salary", System.Type.GetType("System.String"))
                '.Columns.Add("AppointedDate", System.Type.GetType("System.String"))
                '.Columns.Add("BirthDate", System.Type.GetType("System.String"))
                '.Columns.Add("DisplayName", System.Type.GetType("System.String"))
                '.Columns.Add("Gender", System.Type.GetType("System.String"))
                '.Columns.Add("Email", System.Type.GetType("System.String"))
                '.Columns.Add("Retirementdate", System.Type.GetType("System.String"))
                '.Columns.Add("EOC_Date", System.Type.GetType("System.String"))
                '.Columns.Add("EOCReason", System.Type.GetType("System.String"))
                '.Columns.Add("PayType", System.Type.GetType("System.String"))
                '.Columns.Add("PayPoint", System.Type.GetType("System.String"))
                '.Columns.Add("Policy", System.Type.GetType("System.String"))
                '.Columns.Add("Title", System.Type.GetType("System.String"))
                '.Columns.Add("Branch", System.Type.GetType("System.String"))
                '.Columns.Add("DepartmentGroup", System.Type.GetType("System.String"))
                '.Columns.Add("SectionGroup", System.Type.GetType("System.String"))
                '.Columns.Add("Section", System.Type.GetType("System.String"))
                '.Columns.Add("UnitGroup", System.Type.GetType("System.String"))
                '.Columns.Add("Unit", System.Type.GetType("System.String"))
                '.Columns.Add("Team", System.Type.GetType("System.String"))
                '.Columns.Add("JobGroup", System.Type.GetType("System.String"))
                '.Columns.Add("ClassGroup", System.Type.GetType("System.String"))
                '.Columns.Add("Classes", System.Type.GetType("System.String"))
                '.Columns.Add("LeavingDate", System.Type.GetType("System.String"))
                '.Columns.Add("MaritalStatus", System.Type.GetType("System.String"))
                '.Columns.Add("MarriedDate", System.Type.GetType("System.String"))
                '.Columns.Add("Complexion", System.Type.GetType("System.String"))
                '.Columns.Add("SportHobbies", System.Type.GetType("System.String"))
                '.Columns.Add("BloodGroup", System.Type.GetType("System.String"))
                '.Columns.Add("EyeColor", System.Type.GetType("System.String"))
                '.Columns.Add("Nationality", System.Type.GetType("System.String"))
                '.Columns.Add("Ethincity", System.Type.GetType("System.String"))
                '.Columns.Add("Religion", System.Type.GetType("System.String"))
                '.Columns.Add("Hair", System.Type.GetType("System.String"))
                '.Columns.Add("Language1", System.Type.GetType("System.String"))
                '.Columns.Add("Language2", System.Type.GetType("System.String"))
                '.Columns.Add("Language3", System.Type.GetType("System.String"))
                '.Columns.Add("Language4", System.Type.GetType("System.String"))
                '.Columns.Add("ExtraTel", System.Type.GetType("System.String"))
                '.Columns.Add("Height", System.Type.GetType("System.String"))
                '.Columns.Add("Weight", System.Type.GetType("System.String"))
                '.Columns.Add("Allergies", System.Type.GetType("System.String"))
                '.Columns.Add("MedicalDisabilities", System.Type.GetType("System.String"))

                .Columns.Add(Language.getMessage(mstrModuleName, 51, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 52, "FirstName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 53, "OtherName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 54, "Surname"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 55, "EmploymentType"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 56, "Shift"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 57, "Department"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 58, "Job"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 59, "Costcenter"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 60, "GradeGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 61, "GradeName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 62, "GradeLevel"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 63, "Salary"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 64, "AppointedDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 65, "BirthDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 66, "DisplayName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 68, "Gender"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 69, "Email"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 70, "Retirementdate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 71, "EOC_Date"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 72, "EOCReason"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 73, "PayType"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 74, "PayPoint"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 75, "Policy"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 76, "Title"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 77, "Branch"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 78, "DepartmentGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 79, "SectionGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 80, "Section"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 81, "UnitGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 82, "Unit"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 83, "Team"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 84, "JobGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 85, "ClassGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 86, "Classes"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 87, "LeavingDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 88, "MaritalStatus"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 89, "MarriedDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 90, "Complexion"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 91, "SportHobbies"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 92, "BloodGroup"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 93, "EyeColor"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 94, "Nationality"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 95, "Ethincity"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 96, "Religion"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 97, "Hair"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 110, "Language1"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 102, "Language2"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 103, "Language3"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 104, "Language4"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 105, "ExtraTel"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 106, "Height"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 107, "Weight"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 108, "Allergies"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 109, "MedicalDisabilities"), System.Type.GetType("System.String"))
                'Gajanan (24 Nov 2018) -- End`

            End With
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim ctl = GetAll(Me, GetType(ComboBox))

            Dim lstCombos As List(Of Control) = ctl.ToList()
            For Each ctrl In lstCombos
                If CType(ctrl, ComboBox).Name = cboTransHead.Name Then Continue For
                If CType(ctrl, ComboBox).Enabled = False Then Continue For
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault

                'Gajanan (24 Nov 2018) -- end

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [14-JUN-2018] -- END




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText" , Me.eZeeWizImportEmp.CancelText)
			Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText" , Me.eZeeWizImportEmp.NextText)
			Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText" , Me.eZeeWizImportEmp.BackText)
			Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText" , Me.eZeeWizImportEmp.FinishText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.lblMessage2.Text = Language._Object.getCaption(Me.lblMessage2.Name, Me.lblMessage2.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.radDefinedSalary.Text = Language._Object.getCaption(Me.radDefinedSalary.Name, Me.radDefinedSalary.Text)
            Me.radGradedSalary.Text = Language._Object.getCaption(Me.radGradedSalary.Name, Me.radGradedSalary.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
            Me.lblAddress1.Text = Language._Object.getCaption(Me.lblAddress1.Name, Me.lblAddress1.Text)
            Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
            Me.lblPincode.Text = Language._Object.getCaption(Me.lblPincode.Name, Me.lblPincode.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.lblDisplayName.Text = Language._Object.getCaption(Me.lblDisplayName.Name, Me.lblDisplayName.Text)
            Me.lblBirthDate.Text = Language._Object.getCaption(Me.lblBirthDate.Name, Me.lblBirthDate.Text)
            Me.lblAppointdate.Text = Language._Object.getCaption(Me.lblAppointdate.Name, Me.lblAppointdate.Text)
            Me.lblSalary.Text = Language._Object.getCaption(Me.lblSalary.Name, Me.lblSalary.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblGradeGrp.Text = Language._Object.getCaption(Me.lblGradeGrp.Name, Me.lblGradeGrp.Text)
            Me.lblCostcenter.Text = Language._Object.getCaption(Me.lblCostcenter.Name, Me.lblCostcenter.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDept.Text = Language._Object.getCaption(Me.lblDept.Name, Me.lblDept.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblEmpType.Text = Language._Object.getCaption(Me.lblEmpType.Name, Me.lblEmpType.Text)
            Me.lblOtherName.Text = Language._Object.getCaption(Me.lblOtherName.Name, Me.lblOtherName.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblSurName.Text = Language._Object.getCaption(Me.lblSurName.Name, Me.lblSurName.Text)
            Me.lblEmpTitle.Text = Language._Object.getCaption(Me.lblEmpTitle.Name, Me.lblEmpTitle.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblTerminationDate.Text = Language._Object.getCaption(Me.lblTerminationDate.Name, Me.lblTerminationDate.Text)
            Me.GroupBox1.Text = Language._Object.getCaption(Me.GroupBox1.Name, Me.GroupBox1.Text)
            Me.radActiveEmployee.Text = Language._Object.getCaption(Me.radActiveEmployee.Name, Me.radActiveEmployee.Text)
            Me.radInactiveEmployee.Text = Language._Object.getCaption(Me.radInactiveEmployee.Name, Me.radInactiveEmployee.Text)
            Me.lblMarriedDate.Text = Language._Object.getCaption(Me.lblMarriedDate.Name, Me.lblMarriedDate.Text)
            Me.lblMaritalStatus.Text = Language._Object.getCaption(Me.lblMaritalStatus.Name, Me.lblMaritalStatus.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.tabpMandatoryInfo.Text = Language._Object.getCaption(Me.tabpMandatoryInfo.Name, Me.tabpMandatoryInfo.Text)
            Me.tabpOptionalMapping.Text = Language._Object.getCaption(Me.tabpOptionalMapping.Name, Me.tabpOptionalMapping.Text)
            Me.lblTransHead.Text = Language._Object.getCaption(Me.lblTransHead.Name, Me.lblTransHead.Text)
            Me.lblClasses.Text = Language._Object.getCaption(Me.lblClasses.Name, Me.lblClasses.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
            Me.lblRetirementdate.Text = Language._Object.getCaption(Me.lblRetirementdate.Name, Me.lblRetirementdate.Text)
            Me.lblEOC.Text = Language._Object.getCaption(Me.lblEOC.Name, Me.lblEOC.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
			Me.lblPayPoint.Text = Language._Object.getCaption(Me.lblPayPoint.Name, Me.lblPayPoint.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.lblTermReason.Text = Language._Object.getCaption(Me.lblTermReason.Name, Me.lblTermReason.Text)
			Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.Name, Me.lblPolicy.Text)
			Me.lblHair.Text = Language._Object.getCaption(Me.lblHair.Name, Me.lblHair.Text)
			Me.lblReligion.Text = Language._Object.getCaption(Me.lblReligion.Name, Me.lblReligion.Text)
			Me.lblEthinCity.Text = Language._Object.getCaption(Me.lblEthinCity.Name, Me.lblEthinCity.Text)
			Me.lblNationality.Text = Language._Object.getCaption(Me.lblNationality.Name, Me.lblNationality.Text)
			Me.lblEyeColor.Text = Language._Object.getCaption(Me.lblEyeColor.Name, Me.lblEyeColor.Text)
			Me.lblBloodGroup.Text = Language._Object.getCaption(Me.lblBloodGroup.Name, Me.lblBloodGroup.Text)
			Me.lblComplexion.Text = Language._Object.getCaption(Me.lblComplexion.Name, Me.lblComplexion.Text)
			Me.lblLanguage4.Text = Language._Object.getCaption(Me.lblLanguage4.Name, Me.lblLanguage4.Text)
			Me.lblLanguage3.Text = Language._Object.getCaption(Me.lblLanguage3.Name, Me.lblLanguage3.Text)
			Me.lblLanguage2.Text = Language._Object.getCaption(Me.lblLanguage2.Name, Me.lblLanguage2.Text)
			Me.lblLanguage1.Text = Language._Object.getCaption(Me.lblLanguage1.Name, Me.lblLanguage1.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblHeight.Text = Language._Object.getCaption(Me.lblHeight.Name, Me.lblHeight.Text)
			Me.lblExtraTel.Text = Language._Object.getCaption(Me.lblExtraTel.Name, Me.lblExtraTel.Text)
			Me.lblMedicalDisabilities.Text = Language._Object.getCaption(Me.lblMedicalDisabilities.Name, Me.lblMedicalDisabilities.Text)
			Me.lblAllergies.Text = Language._Object.getCaption(Me.lblAllergies.Name, Me.lblAllergies.Text)
			Me.lblSportHobbies.Text = Language._Object.getCaption(Me.lblSportHobbies.Name, Me.lblSportHobbies.Text)
			Me.chkAssignDefaulTranHeads.Text = Language._Object.getCaption(Me.chkAssignDefaulTranHeads.Name, Me.chkAssignDefaulTranHeads.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage("frmEmployeeMaster", 60, "Sorry, this email address is already used by some employee. please provide another email address.")
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Cost Center cannot be blank. Please set the Cost Center to import employee(s).")
            Language.setMessage(mstrModuleName, 4, "Department cannot be blank. Please set the Department to import employee(s).")
            Language.setMessage(mstrModuleName, 5, "Display Name cannot be blank. Please set the Display Name to import employee(s).")
            Language.setMessage(mstrModuleName, 6, "Employee Code cannot be blank. Please set the Employee Code to import employee(s).")
            Language.setMessage(mstrModuleName, 7, "Employment Type cannot be blank. Please set the Employment Type to import employee(s).")
            Language.setMessage(mstrModuleName, 8, "First Name cannot be blank. Please set the First Name to import employee(s).")
            Language.setMessage(mstrModuleName, 9, "Grade cannot be blank. Please set the Grade to import employee(s).")
            Language.setMessage(mstrModuleName, 10, "Grade Group cannot be blank. Please set the Grade Group to import employee(s).")
            Language.setMessage(mstrModuleName, 11, "Grade Level cannot be blank. Please set the Grade Level to import employee(s).")
            Language.setMessage(mstrModuleName, 12, "Job cannot be blank. Please set the Job to import employee(s).")
            Language.setMessage(mstrModuleName, 13, "Increment cannot be blank. Please set the Increment to import employee(s).")
            Language.setMessage(mstrModuleName, 14, "Salary cannot be blank. Please set the Salary to import employee(s).")
            Language.setMessage(mstrModuleName, 15, "Shift cannot be blank. Please set the Shift to import employee(s).")
            Language.setMessage(mstrModuleName, 16, "Surname cannot be blank. Please set the Surname to import employee(s).")
            Language.setMessage(mstrModuleName, 17, "Salary")
            Language.setMessage(mstrModuleName, 18, "Increment")
            Language.setMessage(mstrModuleName, 19, "Invalid Data")
            Language.setMessage(mstrModuleName, 20, "Fail")
            Language.setMessage(mstrModuleName, 21, "Success")
            Language.setMessage(mstrModuleName, 22, "Employee Already Exist")
            Language.setMessage(mstrModuleName, 23, "Please select Termination date field to Import Data.")
            Language.setMessage(mstrModuleName, 24, "Transacation Head is mandatory information. Please select transaction Head to import employee(s).")
            Language.setMessage(mstrModuleName, 25, "Invalid Email Address.")
            Language.setMessage(mstrModuleName, 26, "Retirement date should be greater than Appointment date.")
            Language.setMessage(mstrModuleName, 27, "Appointment Date is mandatory information. Please select Appointment Date to import employee(s).")
            Language.setMessage(mstrModuleName, 28, "Retirement Date is mandatory information. Please select Retirement Date to import employee(s).")
            Language.setMessage(mstrModuleName, 29, "Please select Birth date field to Import Data.")
            Language.setMessage(mstrModuleName, 30, "Notifications to Approve newly Hired Employee(s)")
            Language.setMessage(mstrModuleName, 31, "Please first Create atleast One Period.")
            Language.setMessage(mstrModuleName, 32, "Invalid Appointment date")
            Language.setMessage(mstrModuleName, 33, "Invalid Retirement date")
            Language.setMessage(mstrModuleName, 34, "Invalid Birth date")
            Language.setMessage(mstrModuleName, 35, "Invalid EOC date")
            Language.setMessage(mstrModuleName, 36, "Please click the link below to approve/reject employee.")
            Language.setMessage(mstrModuleName, 37, "Gender is mandatory information. Please select Gender to import employee(s).")
			Language.setMessage(mstrModuleName, 38, "1. Please set all date(s) columns in selected file as blank if not available.")
			Language.setMessage(mstrModuleName, 39, "2. Please add Allergies/Medical Disabilities in comma ',' separated value if having multiple value for employee like data1,data2,data3 etc.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 51, "EmployeeCode")
			Language.setMessage(mstrModuleName, 52, "FirstName")
			Language.setMessage(mstrModuleName, 53, "OtherName")
			Language.setMessage(mstrModuleName, 54, "Surname")
			Language.setMessage(mstrModuleName, 55, "EmploymentType")
			Language.setMessage(mstrModuleName, 56, "Shift")
			Language.setMessage(mstrModuleName, 57, "Department")
			Language.setMessage(mstrModuleName, 58, "Job")
			Language.setMessage(mstrModuleName, 59, "Costcenter")
			Language.setMessage(mstrModuleName, 60, "GradeGroup")
			Language.setMessage(mstrModuleName, 61, "GradeName")
			Language.setMessage(mstrModuleName, 62, "GradeLevel")
			Language.setMessage(mstrModuleName, 63, "Salary")
			Language.setMessage(mstrModuleName, 64, "AppointedDate")
			Language.setMessage(mstrModuleName, 65, "BirthDate")
			Language.setMessage(mstrModuleName, 66, "DisplayName")
			Language.setMessage(mstrModuleName, 67, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 68, "Gender")
			Language.setMessage(mstrModuleName, 69, "Email")
			Language.setMessage(mstrModuleName, 70, "Retirementdate")
			Language.setMessage(mstrModuleName, 71, "EOC_Date")
			Language.setMessage(mstrModuleName, 72, "EOCReason")
			Language.setMessage(mstrModuleName, 73, "PayType")
			Language.setMessage(mstrModuleName, 74, "PayPoint")
			Language.setMessage(mstrModuleName, 75, "Policy")
			Language.setMessage(mstrModuleName, 76, "Title")
			Language.setMessage(mstrModuleName, 77, "Branch")
			Language.setMessage(mstrModuleName, 78, "DepartmentGroup")
			Language.setMessage(mstrModuleName, 79, "SectionGroup")
			Language.setMessage(mstrModuleName, 80, "Section")
			Language.setMessage(mstrModuleName, 81, "UnitGroup")
			Language.setMessage(mstrModuleName, 82, "Unit")
			Language.setMessage(mstrModuleName, 83, "Team")
			Language.setMessage(mstrModuleName, 84, "JobGroup")
			Language.setMessage(mstrModuleName, 85, "ClassGroup")
			Language.setMessage(mstrModuleName, 86, "Classes")
			Language.setMessage(mstrModuleName, 87, "LeavingDate")
			Language.setMessage(mstrModuleName, 88, "MaritalStatus")
			Language.setMessage(mstrModuleName, 89, "MarriedDate")
			Language.setMessage(mstrModuleName, 90, "Complexion")
			Language.setMessage(mstrModuleName, 91, "SportHobbies")
			Language.setMessage(mstrModuleName, 92, "BloodGroup")
			Language.setMessage(mstrModuleName, 93, "EyeColor")
			Language.setMessage(mstrModuleName, 94, "Nationality")
			Language.setMessage(mstrModuleName, 95, "Ethincity")
			Language.setMessage(mstrModuleName, 96, "Religion")
			Language.setMessage(mstrModuleName, 97, "Hair")
			Language.setMessage(mstrModuleName, 98, "Policy is mandatory information. Please select Policy to import employee(s).")
			Language.setMessage(mstrModuleName, 100, "Invalid Height Data.")
			Language.setMessage(mstrModuleName, 101, "Invalid Weight Data.")
			Language.setMessage(mstrModuleName, 102, "Language2")
			Language.setMessage(mstrModuleName, 103, "Language3")
			Language.setMessage(mstrModuleName, 104, "Language4")
			Language.setMessage(mstrModuleName, 105, "ExtraTel")
			Language.setMessage(mstrModuleName, 106, "Height")
			Language.setMessage(mstrModuleName, 107, "Weight")
			Language.setMessage(mstrModuleName, 108, "Allergies")
			Language.setMessage(mstrModuleName, 109, "MedicalDisabilities")
			Language.setMessage(mstrModuleName, 110, "Language1")
			Language.setMessage(mstrModuleName, 111, "Dear")
			Language.setMessage(mstrModuleName, 112, "This is to inform you that")
			Language.setMessage(mstrModuleName, 113, "employee(s) have been added to the employee list by importation please go approve them.")
			Language.setMessage(mstrModuleName, 114, "By user")
			Language.setMessage(mstrModuleName, 115, "from Machine")
			Language.setMessage(mstrModuleName, 116, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 60, "Sorry, this email address is already used by some employee. please provide another email address.")
			Language.setMessage("clsMasterData", 73, "Male")
			Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage(mstrModuleName, 117, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class