﻿Option Strict On

'Last Message Index = 21

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmEditImportedEmployee

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEditImprotedEmployee"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable("Emp_Data")
    Private objEmp As clsEmployee_Master
    Dim dblMinScale As Decimal
    Dim dblMaxScale As Decimal

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            Dim objEmpType As New clsCommon_Master
            dsCombos = objEmpType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmpType")
            cboEmpType.ValueMember = "masterunkid"
            cboEmpType.DisplayMember = "name"
            cboEmpType.DataSource = dsCombos.Tables("EmpType")

            dsCombos = Nothing

            'Shani(17-Nov-2015) -- Start
            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
            dsCombos = objEmpType.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            cboPayType.ValueMember = "masterunkid"
            cboPayType.DisplayMember = "name"
            cboPayType.DataSource = dsCombos.Tables("PayType")
            'Shani(17-Nov-2015) -- End

            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes
            'Dim objShift As New clsshift_master
            Dim objShift As New clsNewshift_master
            'Pinkal (03-Jul-2013) -- End

            dsCombos = objShift.getListForCombo("Shift", True)
            cboShift.ValueMember = "shiftunkid"
            cboShift.DisplayMember = "name"
            cboShift.DataSource = dsCombos.Tables("Shift")

            dsCombos = Nothing
            Dim objDept As New clsDepartment
            dsCombos = objDept.getComboList("Dept", True)
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DisplayMember = "name"
            cboDepartment.DataSource = dsCombos.Tables("Dept")

            dsCombos = Nothing
            Dim objjob As New clsJobs
            dsCombos = objjob.getComboList("Job", True)
            cboJob.ValueMember = "jobunkid"
            cboJob.DisplayMember = "name"
            cboJob.DataSource = dsCombos.Tables("Job")

            dsCombos = Nothing
            Dim objGradegrp As New clsGradeGroup
            dsCombos = objGradegrp.getComboList("GradeGrp", True)
            cboGradeGrp.ValueMember = "gradegroupunkid"
            cboGradeGrp.DisplayMember = "name"
            cboGradeGrp.DataSource = dsCombos.Tables("GradeGrp")


            dsCombos = Nothing
            Dim objTranHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            cboTranhead.ValueMember = "tranheadunkid"
            cboTranhead.DisplayMember = "name"
            cboTranhead.DataSource = dsCombos.Tables("TranHead")


            dsCombos = Nothing
            Dim objCostCenter As New clscostcenter_master
            dsCombos = objCostCenter.getComboList("CostCenter", True)
            cboCostcenter.ValueMember = "costcenterunkid"
            cboCostcenter.DisplayMember = "costcentername"
            cboCostcenter.DataSource = dsCombos.Tables("CostCenter")

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsCombos = Nothing
            Dim objStation As New clsStation
            dsCombos = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
            End With

            dsCombos = Nothing
            Dim objDeptGrp As New clsDepartmentGroup
            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
            End With

            dsCombos = Nothing
            Dim objSection As New clsSections
            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
            End With

            dsCombos = Nothing
            Dim objUnit As New clsUnits
            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
            End With

            dsCombos = Nothing
            Dim objUnitGroup As New clsUnitGroup
            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = Nothing
            Dim objTeam As New clsTeams
            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = Nothing
            Dim objSectionGrp As New clsSectionGroup
            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            cboEmployeeStatus.Items.Clear()
            With cboEmployeeStatus
                .Items.Add(Language.getMessage("frmEmployeeList", 10, "Select"))
                .Items.Add(Language.getMessage("frmEmployeeList", 11, "(A). Approved"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [ 08 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhCostCenterId.DataPropertyName = "costcenterunkid"
            objdgcolhDepartmentId.DataPropertyName = "departmentunkid"
            objdgcolhEmploymentId.DataPropertyName = "employmenttypeunkid"
            objdgcolhGradeGrpId.DataPropertyName = "gradegroupunkid"
            objdgcolhGradeId.DataPropertyName = "gradeunkid"
            objdgcolhGradeLevelId.DataPropertyName = "gradelevelunkid"
            objdgcolhJobId.DataPropertyName = "jobunkid"
            objdgcolhShiftId.DataPropertyName = "shiftunkid"
            objdgcolhTranHeadId.DataPropertyName = "tranhedunkid"

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objdgcolhBranchId.DataPropertyName = "stationunkid"
            objdgcolhDeptGrpId.DataPropertyName = "deptgroupunkid"
            objdgcolhSecGrpId.DataPropertyName = "sectiongroupunkid"
            objdgcolhSectionId.DataPropertyName = "sectionunkid"
            objdgcolhUnitGrpId.DataPropertyName = "unitgroupunkid"
            objdgcolhUnitId.DataPropertyName = "unitunkid"
            objdgcolhTeamId.DataPropertyName = "teamunkid"
            'S.SANDEEP [ 08 OCT 2012 ] -- END

            dgcolhCostCenter.DataPropertyName = "costcenter"
            dgcolhDept.DataPropertyName = "DeptName"
            dgcolhEmployeeCode.DataPropertyName = "employeecode"
            dgcolhEmpType.DataPropertyName = "Comm_Name"
            dgcolhFirstName.DataPropertyName = "name"
            dgcolhGrade.DataPropertyName = "grade"
            dgcolhGradeGrp.DataPropertyName = "gradegroup"
            dgcolhGradelevel.DataPropertyName = "gradelevel"
            dgcolhJob.DataPropertyName = "job_name"
            dgcolhSalaray.DataPropertyName = "scale"
            dgcolhShift.DataPropertyName = "shiftname"
            dgcolhTranHead.DataPropertyName = "tranhead"

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dgcolhBranch.DataPropertyName = "station"
            dgcolhDeptGroup.DataPropertyName = "deptgroup"
            dgcolhSectionGroup.DataPropertyName = "sectiongroup"
            dgcolhSection.DataPropertyName = "section"
            dgcolhUnitGroup.DataPropertyName = "unitgroup"
            dgcolhUnit.DataPropertyName = "unit"
            dgcolhTeam.DataPropertyName = "team"
            'S.SANDEEP [ 08 OCT 2012 ] -- END

            'Shani(17-Nov-2015) -- Start
            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
            dgcolhPayType.DataPropertyName = "paytype"
            objdgcolhPayType.DataPropertyName = "paytypeunkid"
            'Shani(17-Nov-2015) -- End

            dgvImportInfo.DataSource = mdtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If radApplytoAll.Checked = False And radApplytoChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one action to set the value."), enMsgBoxStyle.Information)
                Exit Function
            End If

            Dim objWagesTran As New clsWagesTran
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'dsList = objWagesTran.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), "Scale")
            dsList = objWagesTran.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "Scale")
            'Sohail (27 Apr 2016) -- End
            If dsList.Tables("Scale").Rows.Count > 0 Then
                dblMinScale = CDec(dsList.Tables("Scale").Rows(0).Item("salary").ToString)
                dblMaxScale = CDec(dsList.Tables("Scale").Rows(0).Item("maximum").ToString)
                If CDec(txtSalary.Text) < dblMinScale Or CDec(txtSalary.Text) > dblMaxScale Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Scale should be in between ") & dblMinScale & Language.getMessage(mstrModuleName, 5, " And ") & dblMaxScale & "", enMsgBoxStyle.Information)
                    txtSalary.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub Fill_Emp_Data()
        Try
            Dim StrIEmployeeIds As String = String.Empty
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetList("EData", True)
            dsList = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                   User._Object._Userunkid, _
                                   FinancialYear._Object._YearUnkid, _
                                   Company._Object._Companyunkid, _
                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                   ConfigParameter._Object._UserAccessModeSetting, _
                                    False, True, "EData", _
                                   ConfigParameter._Object._ShowFirstAppointmentDate, , , , , , False) 'S.SANDEEP [25 Jan 2016] -- START -- END


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            ''S.SANDEEP [ 06 SEP 2011 ] -- START
            ''StrIEmployeeIds = objEmp.GetImportedEmployeeIds()
            'StrIEmployeeIds = objEmp.GetImportedEmployeeIds(True)
            ''S.SANDEEP [ 06 SEP 2011 ] -- END 

            'If StrIEmployeeIds.Length > 0 Then
            '    mdtTable = New DataView(dsList.Tables(0), "employeeunkid IN (" & StrIEmployeeIds & ")", "", DataViewRowState.CurrentRows).ToTable
            '    mdtTable.Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
            '    'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    mdtTable.Columns.Add("IsChanged", Type.GetType("System.Boolean")).DefaultValue = False
            '    'S.SANDEEP [ 06 SEP 2011 ] -- END 
            'End If

            mdtTable = New DataView(dsList.Tables(0), "isapproved = 0", "", DataViewRowState.CurrentRows).ToTable
            mdtTable.Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("IsChanged", Type.GetType("System.Boolean")).DefaultValue = False
            'S.SANDEEP [ 08 OCT 2012 ] -- END

            If mdtTable.Rows.Count > 0 Then
                Call FillGirdView()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Emp_Data", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEditImprotedEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmp = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboEmployeeStatus.Visible = User._Object.Privilege._AllowToApproveEmployee
            lblStatus.Visible = User._Object.Privilege._AllowToApproveEmployee
            'S.SANDEEP [ 08 OCT 2012 ] -- END

            Call FillCombo()
            Call Fill_Emp_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEditImprotedEmployee_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    'Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If IsValid() = False Then Exit Sub

    '        If radApplytoAll.Checked = True Then

    '            For Each dtRow As DataRow In mdtTable.Rows
    '                dtRow.Item("costcenter") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtRow.Item("costcenter"), cboCostcenter.Text)
    '                dtRow.Item("DeptName") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtRow.Item("DeptName"), cboDepartment.Text)
    '                dtRow.Item("Comm_Name") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtRow.Item("Comm_Name"), cboEmpType.Text)
    '                dtRow.Item("grade") = IIf(CInt(cboGrade.SelectedValue) = 0, dtRow.Item("grade"), cboGrade.Text)
    '                dtRow.Item("gradegroup") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtRow.Item("gradegroup"), cboGradeGrp.Text)
    '                dtRow.Item("gradelevel") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtRow.Item("gradelevel"), cboGradeLevel.Text)
    '                dtRow.Item("job_name") = IIf(CInt(cboJob.SelectedValue) = 0, dtRow.Item("job_name"), cboJob.Text)
    '                dtRow.Item("scale") = IIf(txtSalary.Decimal = 0, dtRow.Item("scale"), txtSalary.Decimal)
    '                dtRow.Item("shiftname") = IIf(CInt(cboShift.SelectedValue) = 0, dtRow.Item("shiftname"), cboShift.Text)
    '                dtRow.Item("tranhead") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtRow.Item("tranhead"), cboTranhead.Text)

    '                dtRow.Item("costcenterunkid") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtRow.Item("costcenterunkid"), CInt(cboCostcenter.SelectedValue))
    '                dtRow.Item("departmentunkid") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtRow.Item("departmentunkid"), CInt(cboDepartment.SelectedValue))
    '                dtRow.Item("employmenttypeunkid") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtRow.Item("employmenttypeunkid"), CInt(cboEmpType.SelectedValue))
    '                dtRow.Item("gradegroupunkid") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtRow.Item("gradegroupunkid"), CInt(cboGradeGrp.SelectedValue))
    '                dtRow.Item("gradeunkid") = IIf(CInt(cboGrade.SelectedValue) = 0, dtRow.Item("gradeunkid"), CInt(cboGrade.SelectedValue))
    '                dtRow.Item("gradelevelunkid") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtRow.Item("gradelevelunkid"), CInt(cboGradeLevel.SelectedValue))
    '                dtRow.Item("jobunkid") = IIf(CInt(cboJob.SelectedValue) = 0, dtRow.Item("jobunkid"), CInt(cboJob.SelectedValue))
    '                dtRow.Item("shiftunkid") = IIf(CInt(cboShift.SelectedValue) = 0, dtRow.Item("shiftunkid"), CInt(cboShift.SelectedValue))
    '                dtRow.Item("tranhedunkid") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtRow.Item("tranhedunkid"), CInt(cboTranhead.SelectedValue))
    '                'S.SANDEEP [ 06 SEP 2011 ] -- START
    '                dtRow.Item("IsChanged") = True
    '                'S.SANDEEP [ 06 SEP 2011 ] -- END 
    '                mdtTable.AcceptChanges()
    '            Next

    '        ElseIf radApplytoChecked.Checked = True Then
    '            Dim dtTempRow() As DataRow = mdtTable.Select("IsChecked = " & True)
    '            If dtTempRow.Length > 0 Then
    '                For i As Integer = 0 To dtTempRow.Length - 1

    '                    dtTempRow(i).Item("costcenter") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtTempRow(i).Item("costcenter"), cboCostcenter.Text)
    '                    dtTempRow(i).Item("DeptName") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtTempRow(i).Item("DeptName"), cboDepartment.Text)
    '                    dtTempRow(i).Item("Comm_Name") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtTempRow(i).Item("Comm_Name"), cboEmpType.Text)
    '                    dtTempRow(i).Item("grade") = IIf(CInt(cboGrade.SelectedValue) = 0, dtTempRow(i).Item("grade"), cboGrade.Text)
    '                    dtTempRow(i).Item("gradegroup") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtTempRow(i).Item("gradegroup"), cboGradeGrp.Text)
    '                    dtTempRow(i).Item("gradelevel") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtTempRow(i).Item("gradelevel"), cboGradeLevel.Text)
    '                    dtTempRow(i).Item("job_name") = IIf(CInt(cboJob.SelectedValue) = 0, dtTempRow(i).Item("job_name"), cboJob.Text)
    '                    dtTempRow(i).Item("scale") = IIf(txtSalary.Decimal = 0, dtTempRow(i).Item("scale"), txtSalary.Decimal)
    '                    dtTempRow(i).Item("shiftname") = IIf(CInt(cboShift.SelectedValue) = 0, dtTempRow(i).Item("shiftname"), cboShift.Text)
    '                    dtTempRow(i).Item("tranhead") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtTempRow(i).Item("tranhead"), cboTranhead.Text)

    '                    dtTempRow(i).Item("costcenterunkid") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtTempRow(i).Item("costcenterunkid"), CInt(cboCostcenter.SelectedValue))
    '                    dtTempRow(i).Item("departmentunkid") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtTempRow(i).Item("departmentunkid"), CInt(cboDepartment.SelectedValue))
    '                    dtTempRow(i).Item("employmenttypeunkid") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtTempRow(i).Item("employmenttypeunkid"), CInt(cboEmpType.SelectedValue))
    '                    dtTempRow(i).Item("gradegroupunkid") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtTempRow(i).Item("gradegroupunkid"), CInt(cboGradeGrp.SelectedValue))
    '                    dtTempRow(i).Item("gradeunkid") = IIf(CInt(cboGrade.SelectedValue) = 0, dtTempRow(i).Item("gradeunkid"), CInt(cboGrade.SelectedValue))
    '                    dtTempRow(i).Item("gradelevelunkid") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtTempRow(i).Item("gradelevelunkid"), CInt(cboGradeLevel.SelectedValue))
    '                    dtTempRow(i).Item("jobunkid") = IIf(CInt(cboJob.SelectedValue) = 0, dtTempRow(i).Item("jobunkid"), CInt(cboJob.SelectedValue))
    '                    dtTempRow(i).Item("shiftunkid") = IIf(CInt(cboShift.SelectedValue) = 0, dtTempRow(i).Item("shiftunkid"), CInt(cboShift.SelectedValue))
    '                    dtTempRow(i).Item("tranhedunkid") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtTempRow(i).Item("tranhedunkid"), CInt(cboTranhead.SelectedValue))
    '                    'S.SANDEEP [ 06 SEP 2011 ] -- START
    '                    dtTempRow(i).Item("IsChanged") = True
    '                    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    '                    dtTempRow(i).AcceptChanges()
    '                Next
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim blnFlag As Boolean = False
            Dim blnIsGradeChanged As Boolean = False
            Dim blnErrror As Boolean = False

            Dim dtIsChanged() As DataRow = mdtTable.Select("IsChanged = true")

            If dtIsChanged.Length > 0 Then
                For i As Integer = 0 To dtIsChanged.Length - 1

                    'Gajanan [18-NOV-2019] -- Start
                    GC.Collect()
                    'Gajanan [18-NOV-2019] -- End


                    'S.SANDEEP [ 18 JAN 2014 ] -- START
                    'objEmp._Employeeunkid = CInt(dtIsChanged(i)("employeeunkid").ToString)
                    'objEmp._Employeecode = dtIsChanged(i)("employeecode").ToString
                    'objEmp._Titalunkid = CInt(dtIsChanged(i)("titleunkid").ToString)
                    'objEmp._Firstname = dtIsChanged(i)("firstname").ToString
                    'objEmp._Surname = dtIsChanged(i)("surname").ToString
                    'objEmp._Othername = dtIsChanged(i)("othername").ToString

                    'If Not IsDBNull(dtIsChanged(i)("appointeddate")) Then
                    '    objEmp._Appointeddate = CDate(eZeeDate.convertDate(dtIsChanged(i)("appointeddate").ToString).ToShortDateString)
                    'End If

                    'objEmp._Gender = CInt(dtIsChanged(i)("gender").ToString)
                    'objEmp._Employmenttypeunkid = CInt(dtIsChanged(i)("employmenttypeunkid").ToString)
                    'objEmp._Paytypeunkid = CInt(dtIsChanged(i)("paytypeunkid").ToString)
                    'objEmp._Paypointunkid = CInt(dtIsChanged(i)("paypointunkid").ToString)
                    'objEmp._Loginname = dtIsChanged(i)("loginname").ToString

                    ''Sandeep [ 16 MAY 2011 ] -- Start
                    'Dim StrPassword As String = clsSecurity.Decrypt(dtIsChanged(i)("password").ToString, "ezee")
                    'objEmp._Password = StrPassword
                    ''Sandeep [ 16 MAY 2011 ] -- End 

                    'objEmp._Email = dtIsChanged(i)("email").ToString
                    'objEmp._Displayname = dtIsChanged(i)("displayname").ToString

                    ''Pinkal (15-Oct-2013) -- Start
                    ''Enhancement : TRA Changes
                    ''objEmp._Shiftunkid = CInt(dtIsChanged(i)("shiftunkid").ToString)
                    ''Pinkal (15-Oct-2013) -- End


                    'If Not IsDBNull(dtIsChanged(i)("birthdate")) Then
                    '    objEmp._Birthdate = CDate(eZeeDate.convertDate(dtIsChanged(i)("birthdate").ToString).ToShortDateString)
                    'End If

                    'objEmp._Birth_Ward = dtIsChanged(i)("birth_ward").ToString
                    'objEmp._Birthcertificateno = dtIsChanged(i)("birthcertificateno").ToString
                    'objEmp._Birthstateunkid = CInt(dtIsChanged(i)("birthstateunkid").ToString)
                    'objEmp._Birthcountryunkid = CInt(dtIsChanged(i)("birthcountryunkid").ToString)
                    'objEmp._Birthcityunkid = CInt(dtIsChanged(i)("birthcityunkid").ToString)
                    'objEmp._Birth_Village = dtIsChanged(i)("birth_village").ToString
                    'objEmp._Work_Permit_No = dtIsChanged(i)("work_permit_no").ToString
                    'objEmp._Workcountryunkid = CInt(dtIsChanged(i)("workcountryunkid").ToString)
                    'objEmp._Work_Permit_Issue_Place = dtIsChanged(i)("work_permit_issue_place").ToString

                    'If Not IsDBNull(dtIsChanged(i)("work_permit_issue_date")) Then
                    '    objEmp._Work_Permit_Issue_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("work_permit_issue_date").ToString).ToShortDateString)
                    'End If

                    'If Not IsDBNull(dtIsChanged(i)("work_permit_expiry_date")) Then
                    '    objEmp._Work_Permit_Expiry_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("work_permit_expiry_date").ToString).ToShortDateString)
                    'End If

                    'objEmp._Complexionunkid = CInt(dtIsChanged(i)("complexionunkid").ToString)
                    'objEmp._Bloodgroupunkid = CInt(dtIsChanged(i)("bloodgroupunkid").ToString)
                    'objEmp._Eyecolorunkid = CInt(dtIsChanged(i)("eyecolorunkid").ToString)
                    'objEmp._Nationalityunkid = CInt(dtIsChanged(i)("nationalityunkid").ToString)
                    'objEmp._Ethincityunkid = CInt(dtIsChanged(i)("ethnicityunkid").ToString)
                    'objEmp._Religionunkid = CInt(dtIsChanged(i)("religionunkid").ToString)
                    'objEmp._Hairunkid = CInt(dtIsChanged(i)("hairunkid").ToString)
                    'objEmp._Language1unkid = CInt(dtIsChanged(i)("language1unkid").ToString)
                    'objEmp._Language2unkid = CInt(dtIsChanged(i)("language2unkid").ToString)
                    'objEmp._Language3unkid = CInt(dtIsChanged(i)("language3unkid").ToString)
                    'objEmp._Language4unkid = CInt(dtIsChanged(i)("language4unkid").ToString)
                    'objEmp._Extra_Tel_No = dtIsChanged(i)("extra_tel_no").ToString
                    'objEmp._Height = CDec(dtIsChanged(i)("height").ToString)
                    'objEmp._Weight = CDec(dtIsChanged(i)("Weight").ToString)
                    'objEmp._Maritalstatusunkid = CInt(dtIsChanged(i)("maritalstatusunkid").ToString)

                    'If Not IsDBNull(dtIsChanged(i)("anniversary_date")) Then
                    '    objEmp._Anniversary_Date = CDate(eZeeDate.convertDate(Format(dtIsChanged(i)("anniversary_date"), "yyyyMMdd")).ToShortDateString)
                    'End If

                    'objEmp._Sports_Hobbies = dtIsChanged(i)("sports_hobbies").ToString
                    'objEmp._Present_Address1 = dtIsChanged(i)("present_address1").ToString
                    'objEmp._Present_Address2 = dtIsChanged(i)("present_address2").ToString
                    'objEmp._Present_Countryunkid = CInt(dtIsChanged(i)("present_countryunkid").ToString)
                    'objEmp._Present_Postcodeunkid = CInt(dtIsChanged(i)("present_postcodeunkid").ToString)
                    'objEmp._Present_Stateunkid = CInt(dtIsChanged(i)("present_stateunkid").ToString)
                    'objEmp._Present_Provicnce = dtIsChanged(i)("present_provicnce").ToString
                    'objEmp._Present_Post_Townunkid = CInt(dtIsChanged(i)("present_post_townunkid").ToString)
                    'objEmp._Present_Road = dtIsChanged(i)("present_road").ToString
                    'objEmp._Present_Estate = dtIsChanged(i)("present_estate").ToString
                    'objEmp._Present_Plotno = dtIsChanged(i)("present_plotNo").ToString
                    'objEmp._Present_Mobile = dtIsChanged(i)("present_mobile").ToString
                    'objEmp._Present_Alternateno = dtIsChanged(i)("present_alternateno").ToString
                    'objEmp._Present_Tel_No = dtIsChanged(i)("present_tel_no").ToString
                    'objEmp._Present_Fax = dtIsChanged(i)("present_fax").ToString
                    'objEmp._Present_Email = dtIsChanged(i)("present_email").ToString
                    'objEmp._Domicile_Address1 = dtIsChanged(i)("domicile_address1").ToString
                    'objEmp._Domicile_Address2 = dtIsChanged(i)("domicile_address2").ToString
                    'objEmp._Domicile_Countryunkid = CInt(dtIsChanged(i)("domicile_countryunkid").ToString)
                    'objEmp._Domicile_Postcodeunkid = CInt(dtIsChanged(i)("domicile_postcodeunkid").ToString)
                    'objEmp._Domicile_Stateunkid = CInt(dtIsChanged(i)("domicile_stateunkid").ToString)
                    'objEmp._Domicile_Provicnce = dtIsChanged(i)("domicile_provicnce").ToString
                    'objEmp._Domicile_Post_Townunkid = CInt(dtIsChanged(i)("domicile_post_townunkid").ToString)
                    'objEmp._Domicile_Road = dtIsChanged(i)("domicile_road").ToString
                    'objEmp._Domicile_Estate = dtIsChanged(i)("domicile_estate").ToString
                    'objEmp._Domicile_Plotno = dtIsChanged(i)("domicile_plotNo").ToString
                    'objEmp._Domicile_Mobile = dtIsChanged(i)("domicile_mobile").ToString
                    'objEmp._Domicile_Alternateno = dtIsChanged(i)("domicile_alternateno").ToString
                    'objEmp._Domicile_Tel_No = dtIsChanged(i)("domicile_tel_no").ToString
                    'objEmp._Domicile_Fax = dtIsChanged(i)("domicile_fax").ToString
                    'objEmp._Domicile_Email = dtIsChanged(i)("domicile_email").ToString
                    'objEmp._Emer_Con_Firstname = dtIsChanged(i)("emer_con_firstname").ToString
                    'objEmp._Emer_Con_Lastname = dtIsChanged(i)("emer_con_lastname").ToString
                    'objEmp._Emer_Con_Address = dtIsChanged(i)("emer_con_address").ToString
                    'objEmp._Emer_Con_Countryunkid = CInt(dtIsChanged(i)("emer_con_countryunkid").ToString)
                    'objEmp._Emer_Con_Postcodeunkid = CInt(dtIsChanged(i)("emer_con_postcodeunkid").ToString)
                    'objEmp._Emer_Con_State = CInt(dtIsChanged(i)("emer_con_state").ToString)
                    'objEmp._Emer_Con_Provicnce = dtIsChanged(i)("emer_con_provicnce").ToString
                    'objEmp._Emer_Con_Post_Townunkid = CInt(dtIsChanged(i)("emer_con_post_townunkid").ToString)
                    'objEmp._Emer_Con_Road = dtIsChanged(i)("emer_con_road").ToString
                    'objEmp._Emer_Con_Estate = dtIsChanged(i)("emer_con_estate").ToString
                    'objEmp._Emer_Con_Plotno = dtIsChanged(i)("emer_con_plotNo").ToString
                    'objEmp._Emer_Con_Mobile = dtIsChanged(i)("emer_con_mobile").ToString
                    'objEmp._Emer_Con_Alternateno = dtIsChanged(i)("emer_con_alternateno").ToString
                    'objEmp._Emer_Con_Tel_No = dtIsChanged(i)("emer_con_tel_no").ToString
                    'objEmp._Emer_Con_Fax = dtIsChanged(i)("emer_con_fax").ToString
                    'objEmp._Emer_Con_Email = dtIsChanged(i)("emer_con_email").ToString
                    'objEmp._Stationunkid = CInt(dtIsChanged(i)("stationunkid").ToString)
                    'objEmp._Deptgroupunkid = CInt(dtIsChanged(i)("deptgroupunkid").ToString)
                    'objEmp._Departmentunkid = CInt(dtIsChanged(i)("departmentunkid").ToString)
                    'objEmp._Sectionunkid = CInt(dtIsChanged(i)("sectionunkid").ToString)
                    'objEmp._Unitunkid = CInt(dtIsChanged(i)("unitunkid").ToString)
                    'objEmp._Jobgroupunkid = CInt(dtIsChanged(i)("jobgroupunkid").ToString)
                    'objEmp._Jobunkid = CInt(dtIsChanged(i)("jobunkid").ToString)
                    'objEmp._Gradegroupunkid = CInt(dtIsChanged(i)("gradegroupunkid").ToString)
                    'objEmp._Gradeunkid = CInt(dtIsChanged(i)("gradeunkid").ToString)

                    'If objEmp._Gradelevelunkid <> CInt(dtIsChanged(i)("gradelevelunkid").ToString) Then
                    '    objEmp._Gradelevelunkid = CInt(dtIsChanged(i)("gradelevelunkid").ToString)
                    '    blnIsGradeChanged = True
                    'Else
                    '    objEmp._Gradelevelunkid = CInt(dtIsChanged(i)("gradelevelunkid").ToString)
                    'End If

                    'objEmp._Accessunkid = CInt(dtIsChanged(i)("accessunkid").ToString)
                    'objEmp._Classgroupunkid = CInt(dtIsChanged(i)("classgroupunkid").ToString)
                    'objEmp._Classunkid = CInt(dtIsChanged(i)("classunkid").ToString)
                    'objEmp._Serviceunkid = CInt(dtIsChanged(i)("serviceunkid").ToString)
                    'objEmp._Costcenterunkid = CInt(dtIsChanged(i)("costcenterunkid").ToString)
                    'objEmp._Tranhedunkid = CInt(dtIsChanged(i)("tranhedunkid").ToString)
                    'objEmp._Actionreasonunkid = CInt(dtIsChanged(i)("actionreasonunkid").ToString)

                    ''S.SANDEEP [ 08 OCT 2012 ] -- START
                    ''ENHANCEMENT : TRA CHANGES
                    'objEmp._Sectiongroupunkid = CInt(dtIsChanged(i)("sectiongroupunkid").ToString)
                    'objEmp._Unitgroupunkid = CInt(dtIsChanged(i)("unitgroupunkid").ToString)
                    'objEmp._Teamunkid = CInt(dtIsChanged(i)("teamunkid").ToString)
                    'If User._Object.Privilege._AllowToApproveEmployee = True Then
                    '    Select Case cboEmployeeStatus.SelectedIndex
                    '        Case 0  'SELECT
                    '            objEmp._Isapproved = CBool(dtIsChanged(i)("isapproved").ToString)
                    '        Case 1  'APPROVED
                    '            objEmp._Isapproved = True
                    '    End Select
                    'End If
                    ''S.SANDEEP [ 08 OCT 2012 ] -- END

                    'If dtIsChanged(i)("suspended_from_date").ToString.Trim <> "" Then
                    '    objEmp._Suspende_From_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("suspended_from_date").ToString).ToShortDateString)
                    'End If

                    'If dtIsChanged(i)("suspended_to_date").ToString.Trim <> "" Then
                    '    objEmp._Suspende_To_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("suspended_to_date").ToString).ToShortDateString)
                    'End If

                    'If dtIsChanged(i)("probation_from_date").ToString.Trim <> "" Then
                    '    objEmp._Probation_From_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("probation_from_date").ToString).ToShortDateString)
                    'End If

                    'If dtIsChanged(i)("probation_to_date").ToString.Trim <> "" Then
                    '    objEmp._Probation_To_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("probation_to_date").ToString).ToShortDateString)
                    'End If

                    'If dtIsChanged(i)("termination_from_date").ToString.Trim <> "" Then
                    '    objEmp._Termination_From_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("termination_from_date").ToString).ToShortDateString)
                    'End If

                    'If dtIsChanged(i)("termination_to_date").ToString.Trim <> "" Then
                    '    objEmp._Termination_To_Date = CDate(eZeeDate.convertDate(dtIsChanged(i)("termination_to_date").ToString).ToShortDateString)
                    'End If

                    'objEmp._Remark = dtIsChanged(i)("remark").ToString
                    'objEmp._Isactive = CBool(dtIsChanged(i)("isactive").ToString)

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmp._Employeeunkid = CInt(dtIsChanged(i)("employeeunkid").ToString)
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtIsChanged(i)("employeeunkid").ToString)
                    'S.SANDEEP [04 JUN 2015] -- END

                    objEmp._Employmenttypeunkid = CInt(dtIsChanged(i)("employmenttypeunkid").ToString)
                    objEmp._Stationunkid = CInt(dtIsChanged(i)("stationunkid").ToString)
                    objEmp._Deptgroupunkid = CInt(dtIsChanged(i)("deptgroupunkid").ToString)
                    objEmp._Departmentunkid = CInt(dtIsChanged(i)("departmentunkid").ToString)
                    objEmp._Sectionunkid = CInt(dtIsChanged(i)("sectionunkid").ToString)
                    objEmp._Unitunkid = CInt(dtIsChanged(i)("unitunkid").ToString)
                    objEmp._Jobunkid = CInt(dtIsChanged(i)("jobunkid").ToString)
                    objEmp._Gradegroupunkid = CInt(dtIsChanged(i)("gradegroupunkid").ToString)
                    objEmp._Gradeunkid = CInt(dtIsChanged(i)("gradeunkid").ToString)
                    If objEmp._Gradelevelunkid <> CInt(dtIsChanged(i)("gradelevelunkid").ToString) Then
                        objEmp._Gradelevelunkid = CInt(dtIsChanged(i)("gradelevelunkid").ToString)
                        blnIsGradeChanged = True
                    Else
                        objEmp._Gradelevelunkid = CInt(dtIsChanged(i)("gradelevelunkid").ToString)
                    End If
                    objEmp._Costcenterunkid = CInt(dtIsChanged(i)("costcenterunkid").ToString)
                    objEmp._Tranhedunkid = CInt(dtIsChanged(i)("tranhedunkid").ToString)
                    objEmp._Sectiongroupunkid = CInt(dtIsChanged(i)("sectiongroupunkid").ToString)
                    objEmp._Unitgroupunkid = CInt(dtIsChanged(i)("unitgroupunkid").ToString)
                    objEmp._Teamunkid = CInt(dtIsChanged(i)("teamunkid").ToString)

                    'Shani(17-Nov-2015) -- Start
                    'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                    objEmp._Paytypeunkid = CInt(dtIsChanged(i)("paytypeunkid").ToString)
                    'Shani(17-Nov-2015) -- End

                    'S.SANDEEP [17-DEC-2018] -- START
                    'If User._Object.Privilege._AllowToApproveEmployee = True Then
                    '    Select Case cboEmployeeStatus.SelectedIndex
                    '        Case 0  'SELECT
                    '            objEmp._Isapproved = CBool(dtIsChanged(i)("isapproved").ToString)
                    '        Case 1  'APPROVED
                    '            objEmp._Isapproved = True
                    '    End Select
                    'End If

                    If ConfigParameter._Object._SkipEmployeeApprovalFlow = False Then
                        objEmp._Isapproved = False
                    Else
                    If User._Object.Privilege._AllowToApproveEmployee = True Then
                        Select Case cboEmployeeStatus.SelectedIndex
                            Case 0  'SELECT
                                objEmp._Isapproved = CBool(dtIsChanged(i)("isapproved").ToString)
                            Case 1  'APPROVED
                                objEmp._Isapproved = True
                        End Select
                    End If
                    End If
                    'S.SANDEEP [17-DEC-2018] -- END
                    
                    'S.SANDEEP [ 18 JAN 2014 ] -- END

                    'Sohail (09 Jun 2014) -- Start
                    'Enhancement - Salary as per Days Present for Flat rate or Formula based salary if employee join or leaving in between month. 
                    Dim objTrnHead As New clsTransactionHead
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTrnHead._Tranheadunkid = CInt(dtIsChanged(i)("tranhedunkid").ToString)
                    objTrnHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dtIsChanged(i)("tranhedunkid").ToString)
                    'Sohail (21 Aug 2015) -- End
                    objEmp._TrnHeadTypeId = objTrnHead._Trnheadtype_Id
                    objTrnHead = Nothing
                    'Sohail (09 Jun 2014) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnFlag = objEmp.Update
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objEmp.Update(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid)


                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                    'blnFlag = objEmp.Update(FinancialYear._Object._DatabaseName, _
                    '                        FinancialYear._Object._YearUnkid, _
                    '                        Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, _
                    '                        CStr(ConfigParameter._Object._IsArutiDemo), _
                    '                        Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, _
                    '                        User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, _
                    '                        ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                        True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime.Date, _
                    '                        , , , , , , , , , , , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP)

                    blnFlag = objEmp.Update(FinancialYear._Object._DatabaseName, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, _
                                            CStr(ConfigParameter._Object._IsArutiDemo), _
                                            Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, _
                                            User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, _
                                            ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CreateADUserFromEmpMst, _
                                          ConfigParameter._Object._UserMustchangePwdOnNextLogon, , , , , , , , , , , , , getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP)


                    'Pinkal (18-Aug-2018) -- End

                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP})} -- END
                    'Sohail (21 Aug 2015) -- End
                    'S.SANDEEP [04 JUN 2015] -- END
                    If blnFlag = False And objEmp._Message <> "" Then
                        eZeeMsgBox.Show(objEmp._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        Dim objMData As New clsMasterData

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Dim mintCurrPerid As Integer = objMData.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, enStatusType.Open)
                        Dim mintCurrPerid As Integer = objMData.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, FinancialYear._Object._YearUnkid, enStatusType.Open)
                        'S.SANDEEP [04 JUN 2015] -- END

                        If mintCurrPerid > 0 Then
                            Dim mdecCurrentScale As Decimal = 0
                            Dim intSalIncTranId As Integer = -1
                            Dim objSalInc As New clsSalaryIncrement
                            Dim dsList As DataSet = objMData.Get_Current_Scale("List", CInt(dtIsChanged(i)("employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                intSalIncTranId = CInt(dsList.Tables("List").Rows(0)("salaryincrementtranunkid"))
                            End If


                            Dim objWagesTran As New clsWagesTran
                            'Sohail (27 Apr 2016) -- Start
                            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                            'dsList = objWagesTran.getScaleInfo(CInt(dtIsChanged(i)("gradeunkid")), CInt(dtIsChanged(i)("gradelevelunkid")), "Scale")
                            dsList = objWagesTran.getScaleInfo(CInt(dtIsChanged(i)("gradeunkid")), CInt(dtIsChanged(i)("gradelevelunkid")), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "Scale")
                            'Sohail (27 Apr 2016) -- End
                            If dsList.Tables("Scale").Rows.Count > 0 Then
                                dblMinScale = CDec(dsList.Tables("Scale").Rows(0).Item("salary").ToString)
                                dblMaxScale = CDec(dsList.Tables("Scale").Rows(0).Item("maximum").ToString)
                                If CDec(dtIsChanged(i)("scale")) < dblMinScale Or CDec(dtIsChanged(i)("scale")) > dblMaxScale Then
                                    blnErrror = True
                                    Dim idx As Integer = mdtTable.Rows.IndexOf(dtIsChanged(i))
                                    dgvImportInfo.Rows(idx).DefaultCellStyle.BackColor = Color.Red
                                    Continue For
                                End If
                            End If

                            'If (mdecCurrentScale + CDec(dtIsChanged(i)("scale").ToString)) > dblMaxScale Then
                            '    Dim idx As Integer = mdtTable.Rows.IndexOf(dtIsChanged(i))
                            '    dgvImportInfo.Rows(idx).DefaultCellStyle.BackColor = Color.Red
                            '    blnErrror = True
                            'End If

                            With objSalInc
                                ._Salaryincrementtranunkid = intSalIncTranId
                                '._Periodunkid = mintCurrPerid
                                ._Employeeunkid = CInt(dtIsChanged(i)("employeeunkid"))
                                'Sohail (02 Mar 2020) -- Start
                                'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                                If ._Incrementdate = ._ActualDate Then
                                    ._ActualDate = objEmp._Appointeddate
                                End If
                                'Sohail (02 Mar 2020) -- End
                                'S.SANDEEP [ 15 MAY 2014 ] -- START
                                '._Incrementdate = ConfigParameter._Object._CurrentDateAndTime
                                ._Incrementdate = objEmp._Appointeddate
                                'S.SANDEEP [ 15 MAY 2014 ] -- END
                                ._Newscale = CDec(dtIsChanged(i)("scale").ToString)
                                ._Gradegroupunkid = CInt(dtIsChanged(i)("gradegroupunkid").ToString)
                                ._Gradeunkid = CInt(dtIsChanged(i)("gradeunkid").ToString)
                                ._Gradelevelunkid = CInt(dtIsChanged(i)("gradelevelunkid").ToString)
                                ._Isgradechange = blnIsGradeChanged
                                ._Userunkid = User._Object._Userunkid
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                '.Update()

                                'S.SANDEEP |15-MAR-2019| -- START
                                'ISSUE : EMPLOYEE SCALE WAS NOT UPDATING DUE TO {blnUpdateEmployeeScale WAS TRUE.} AND TRANSACTION HEAD WAS ALREADY PRESENT AND GIVING ISSUE
                                'ISSUE : 0003589
                                '.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                '        Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                '        True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime)

                                .Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, False)
                                'S.SANDEEP |15-MAR-2019| -- END
                                'Sohail (24 Feb 2022) - [User._Object.Privilege._AllowToApproveEarningDeduction]=[True]

                                
                                'Sohail (21 Aug 2015) -- End
                            End With
                            objMData = Nothing : mdecCurrentScale = 0 : objSalInc = Nothing : dsList.Dispose()
                        Else
                            blnErrror = True
                        End If
                    End If
                Next
            End If

            If blnErrror = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Some information cannot be edited due to some problem in scale range or period has not been created."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Information edited successfully."), enMsgBoxStyle.Information)
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUpdate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSalary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSalary.Click
        Dim frm As New frmWagetable_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSalary_Click", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 08 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBranch.Click, _
                                                                                                             objbtnSearchDeptGrp.Click, _
                                                                                                             objbtnSearchDepartment.Click, _
                                                                                                             objbtnSearchSecGroup.Click, _
                                                                                                             objbtnSearchSection.Click, _
                                                                                                             objbtnSearchUnitGrp.Click, _
                                                                                                             objbtnSearchUnits.Click, _
                                                                                                             objbtnSearchTeam.Click, _
                                                                                                             objbtnSearchJob.Click, _
                                                                                                             objbtnSearchGradeLevel.Click, _
                                                                                                             objbtnSearchGrade.Click, _
                                                                                                             objbtnSearchGradeGrp.Click, _
                                                                                                             objbtnSearchCostCenter.Click, _
                                                                                                              objbtnSearchEmplType.Click, _
                                                                                                              objbtnSearchPayType.Click
        'Shani(17-Nov-2015) -- [objbtnSearchPayType.Click]

        Dim frm As New frmCommonSearch
        Dim cbo As New ComboBox
        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHBRANCH"
                    cbo = cboStation
                Case "OBJBTNSEARCHDEPTGRP"
                    cbo = cboDepartmentGrp
                Case "OBJBTNSEARCHDEPARTMENT"
                    cbo = cboDepartment
                Case "OBJBTNSEARCHSECGROUP"
                    cbo = cboSectionGroup
                Case "OBJBTNSEARCHSECTION"
                    cbo = cboSections
                Case "OBJBTNSEARCHUNITGRP"
                    cbo = cboUnitGroup
                Case "OBJBTNSEARCHUNITS"
                    cbo = cboUnits
                Case "OBJBTNSEARCHTEAM"
                    cbo = cboTeams
                Case "OBJBTNSEARCHJOB"
                    cbo = cboJob
                Case "OBJBTNSEARCHGRADELEVEL"
                    cbo = cboGradeLevel
                Case "OBJBTNSEARCHGRADE"
                    cbo = cboGrade
                Case "OBJBTNSEARCHGRADEGRP"
                    cbo = cboGradeGrp
                Case "OBJBTNSEARCHCOSTCENTER"
                    cbo = cboCostcenter
                Case "OBJBTNSEARCHSHIFT"
                    cbo = cboShift
                Case "OBJBTNSEARCHEMPLTYPE"
                    cbo = cboEmpType

                    'Shani(17-Nov-2015) -- Start
                    'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                Case "objbtnSearchPayType"
                    cbo = cboPayType
                    'Shani(17-Nov-2015) -- End

            End Select

            If cbo.DataSource IsNot Nothing Then
                With frm
                    .DisplayMember = cbo.DisplayMember
                    .ValueMember = cbo.ValueMember
                    .CodeMember = ""
                    .DataSource = CType(cbo.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Focus()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkSet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSet.Click
        Try
            If IsValid() = False Then Exit Sub
            If radApplytoAll.Checked = True Then
                For Each dtRow As DataRow In mdtTable.Rows
                    dtRow.Item("costcenter") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtRow.Item("costcenter"), cboCostcenter.Text)
                    dtRow.Item("DeptName") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtRow.Item("DeptName"), cboDepartment.Text)
                    dtRow.Item("Comm_Name") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtRow.Item("Comm_Name"), cboEmpType.Text)
                    dtRow.Item("grade") = IIf(CInt(cboGrade.SelectedValue) = 0, dtRow.Item("grade"), cboGrade.Text)
                    dtRow.Item("gradegroup") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtRow.Item("gradegroup"), cboGradeGrp.Text)
                    dtRow.Item("gradelevel") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtRow.Item("gradelevel"), cboGradeLevel.Text)
                    dtRow.Item("job_name") = IIf(CInt(cboJob.SelectedValue) = 0, dtRow.Item("job_name"), cboJob.Text)
                    dtRow.Item("scale") = IIf(txtSalary.Decimal = 0, dtRow.Item("scale"), txtSalary.Decimal)
                    dtRow.Item("shiftname") = IIf(CInt(cboShift.SelectedValue) = 0, dtRow.Item("shiftname"), cboShift.Text)
                    dtRow.Item("tranhead") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtRow.Item("tranhead"), cboTranhead.Text)

                    'S.SANDEEP [ 08 OCT 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtRow.Item("station") = IIf(CInt(cboStation.SelectedValue) = 0, dtRow.Item("station"), cboStation.Text)
                    dtRow.Item("deptgroup") = IIf(CInt(cboDepartmentGrp.SelectedValue) = 0, dtRow.Item("deptgroup"), cboDepartmentGrp.Text)
                    dtRow.Item("sectiongroup") = IIf(CInt(cboSectionGroup.SelectedValue) = 0, dtRow.Item("sectiongroup"), cboSectionGroup.Text)
                    dtRow.Item("section") = IIf(CInt(cboSections.SelectedValue) = 0, dtRow.Item("section"), cboSections.Text)
                    dtRow.Item("unitgroup") = IIf(CInt(cboUnitGroup.SelectedValue) = 0, dtRow.Item("unitgroup"), cboUnitGroup.Text)
                    dtRow.Item("unit") = IIf(CInt(cboUnits.SelectedValue) = 0, dtRow.Item("unit"), cboUnits.Text)
                    dtRow.Item("team") = IIf(CInt(cboTeams.SelectedValue) = 0, dtRow.Item("team"), cboTeams.Text)
                    'S.SANDEEP [ 08 OCT 2012 ] -- END

                    dtRow.Item("costcenterunkid") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtRow.Item("costcenterunkid"), CInt(cboCostcenter.SelectedValue))
                    dtRow.Item("departmentunkid") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtRow.Item("departmentunkid"), CInt(cboDepartment.SelectedValue))
                    dtRow.Item("employmenttypeunkid") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtRow.Item("employmenttypeunkid"), CInt(cboEmpType.SelectedValue))
                    dtRow.Item("gradegroupunkid") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtRow.Item("gradegroupunkid"), CInt(cboGradeGrp.SelectedValue))
                    dtRow.Item("gradeunkid") = IIf(CInt(cboGrade.SelectedValue) = 0, dtRow.Item("gradeunkid"), CInt(cboGrade.SelectedValue))
                    dtRow.Item("gradelevelunkid") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtRow.Item("gradelevelunkid"), CInt(cboGradeLevel.SelectedValue))
                    dtRow.Item("jobunkid") = IIf(CInt(cboJob.SelectedValue) = 0, dtRow.Item("jobunkid"), CInt(cboJob.SelectedValue))
                    dtRow.Item("shiftunkid") = IIf(CInt(cboShift.SelectedValue) = 0, dtRow.Item("shiftunkid"), CInt(cboShift.SelectedValue))
                    dtRow.Item("tranhedunkid") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtRow.Item("tranhedunkid"), CInt(cboTranhead.SelectedValue))

                    'S.SANDEEP [ 08 OCT 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dtRow.Item("stationunkid") = IIf(CInt(cboStation.SelectedValue) = 0, dtRow.Item("stationunkid"), CInt(cboStation.SelectedValue))
                    dtRow.Item("deptgroupunkid") = IIf(CInt(cboDepartmentGrp.SelectedValue) = 0, dtRow.Item("deptgroupunkid"), CInt(cboDepartmentGrp.SelectedValue))
                    dtRow.Item("sectiongroupunkid") = IIf(CInt(cboSectionGroup.SelectedValue) = 0, dtRow.Item("sectiongroupunkid"), CInt(cboSectionGroup.SelectedValue))
                    dtRow.Item("sectionunkid") = IIf(CInt(cboSections.SelectedValue) = 0, dtRow.Item("sectionunkid"), CInt(cboSections.SelectedValue))
                    dtRow.Item("unitgroupunkid") = IIf(CInt(cboUnitGroup.SelectedValue) = 0, dtRow.Item("unitgroupunkid"), CInt(cboUnitGroup.SelectedValue))
                    dtRow.Item("unitunkid") = IIf(CInt(cboUnits.SelectedValue) = 0, dtRow.Item("unitunkid"), CInt(cboUnits.SelectedValue))
                    dtRow.Item("teamunkid") = IIf(CInt(cboTeams.SelectedValue) = 0, dtRow.Item("teamunkid"), CInt(cboTeams.SelectedValue))
                    'S.SANDEEP [ 08 OCT 2012 ] -- END

                    'Shani(17-Nov-2015) -- Start
                    'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                    dtRow.Item("paytype") = IIf(CInt(cboPayType.SelectedValue) = 0, dtRow.Item("paytype"), cboPayType.Text)
                    dtRow.Item("paytypeunkid") = IIf(CInt(cboPayType.SelectedValue) = 0, dtRow.Item("paytypeunkid"), CInt(cboPayType.SelectedValue))
                    'Shani(17-Nov-2015) -- End

                    dtRow.Item("IsChanged") = True
                    mdtTable.AcceptChanges()
                Next
            ElseIf radApplytoChecked.Checked = True Then
                Dim dtTempRow() As DataRow = mdtTable.Select("IsChecked = " & True)
                If dtTempRow.Length > 0 Then
                    For i As Integer = 0 To dtTempRow.Length - 1
                        dtTempRow(i).Item("costcenter") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtTempRow(i).Item("costcenter"), cboCostcenter.Text)
                        dtTempRow(i).Item("DeptName") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtTempRow(i).Item("DeptName"), cboDepartment.Text)
                        dtTempRow(i).Item("Comm_Name") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtTempRow(i).Item("Comm_Name"), cboEmpType.Text)
                        dtTempRow(i).Item("grade") = IIf(CInt(cboGrade.SelectedValue) = 0, dtTempRow(i).Item("grade"), cboGrade.Text)
                        dtTempRow(i).Item("gradegroup") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtTempRow(i).Item("gradegroup"), cboGradeGrp.Text)
                        dtTempRow(i).Item("gradelevel") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtTempRow(i).Item("gradelevel"), cboGradeLevel.Text)
                        dtTempRow(i).Item("job_name") = IIf(CInt(cboJob.SelectedValue) = 0, dtTempRow(i).Item("job_name"), cboJob.Text)
                        dtTempRow(i).Item("scale") = IIf(txtSalary.Decimal = 0, dtTempRow(i).Item("scale"), txtSalary.Decimal)
                        dtTempRow(i).Item("shiftname") = IIf(CInt(cboShift.SelectedValue) = 0, dtTempRow(i).Item("shiftname"), cboShift.Text)
                        dtTempRow(i).Item("tranhead") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtTempRow(i).Item("tranhead"), cboTranhead.Text)

                        'S.SANDEEP [ 08 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtTempRow(i).Item("station") = IIf(CInt(cboStation.SelectedValue) = 0, dtTempRow(i).Item("station"), cboStation.Text)
                        dtTempRow(i).Item("deptgroup") = IIf(CInt(cboDepartmentGrp.SelectedValue) = 0, dtTempRow(i).Item("deptgroup"), cboDepartmentGrp.Text)
                        dtTempRow(i).Item("sectiongroup") = IIf(CInt(cboSectionGroup.SelectedValue) = 0, dtTempRow(i).Item("sectiongroup"), cboSectionGroup.Text)
                        dtTempRow(i).Item("section") = IIf(CInt(cboSections.SelectedValue) = 0, dtTempRow(i).Item("section"), cboSections.Text)
                        dtTempRow(i).Item("unitgroup") = IIf(CInt(cboUnitGroup.SelectedValue) = 0, dtTempRow(i).Item("unitgroup"), cboUnitGroup.Text)
                        dtTempRow(i).Item("unit") = IIf(CInt(cboUnits.SelectedValue) = 0, dtTempRow(i).Item("unit"), cboUnits.Text)
                        dtTempRow(i).Item("team") = IIf(CInt(cboTeams.SelectedValue) = 0, dtTempRow(i).Item("team"), cboTeams.Text)
                        'S.SANDEEP [ 08 OCT 2012 ] -- END

                        dtTempRow(i).Item("costcenterunkid") = IIf(CInt(cboCostcenter.SelectedValue) = 0, dtTempRow(i).Item("costcenterunkid"), CInt(cboCostcenter.SelectedValue))
                        dtTempRow(i).Item("departmentunkid") = IIf(CInt(cboDepartment.SelectedValue) = 0, dtTempRow(i).Item("departmentunkid"), CInt(cboDepartment.SelectedValue))
                        dtTempRow(i).Item("employmenttypeunkid") = IIf(CInt(cboEmpType.SelectedValue) = 0, dtTempRow(i).Item("employmenttypeunkid"), CInt(cboEmpType.SelectedValue))
                        dtTempRow(i).Item("gradegroupunkid") = IIf(CInt(cboGradeGrp.SelectedValue) = 0, dtTempRow(i).Item("gradegroupunkid"), CInt(cboGradeGrp.SelectedValue))
                        dtTempRow(i).Item("gradeunkid") = IIf(CInt(cboGrade.SelectedValue) = 0, dtTempRow(i).Item("gradeunkid"), CInt(cboGrade.SelectedValue))
                        dtTempRow(i).Item("gradelevelunkid") = IIf(CInt(cboGradeLevel.SelectedValue) = 0, dtTempRow(i).Item("gradelevelunkid"), CInt(cboGradeLevel.SelectedValue))
                        dtTempRow(i).Item("jobunkid") = IIf(CInt(cboJob.SelectedValue) = 0, dtTempRow(i).Item("jobunkid"), CInt(cboJob.SelectedValue))
                        dtTempRow(i).Item("shiftunkid") = IIf(CInt(cboShift.SelectedValue) = 0, dtTempRow(i).Item("shiftunkid"), CInt(cboShift.SelectedValue))
                        dtTempRow(i).Item("tranhedunkid") = IIf(CInt(cboTranhead.SelectedValue) = 0, dtTempRow(i).Item("tranhedunkid"), CInt(cboTranhead.SelectedValue))

                        'S.SANDEEP [ 08 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        dtTempRow(i).Item("stationunkid") = IIf(CInt(cboStation.SelectedValue) = 0, dtTempRow(i).Item("stationunkid"), CInt(cboStation.SelectedValue))
                        dtTempRow(i).Item("deptgroupunkid") = IIf(CInt(cboDepartmentGrp.SelectedValue) = 0, dtTempRow(i).Item("deptgroupunkid"), CInt(cboDepartmentGrp.SelectedValue))
                        dtTempRow(i).Item("sectiongroupunkid") = IIf(CInt(cboSectionGroup.SelectedValue) = 0, dtTempRow(i).Item("sectiongroupunkid"), CInt(cboSectionGroup.SelectedValue))
                        dtTempRow(i).Item("sectionunkid") = IIf(CInt(cboSections.SelectedValue) = 0, dtTempRow(i).Item("sectionunkid"), CInt(cboSections.SelectedValue))
                        dtTempRow(i).Item("unitgroupunkid") = IIf(CInt(cboUnitGroup.SelectedValue) = 0, dtTempRow(i).Item("unitgroupunkid"), CInt(cboUnitGroup.SelectedValue))
                        dtTempRow(i).Item("unitunkid") = IIf(CInt(cboUnits.SelectedValue) = 0, dtTempRow(i).Item("unitunkid"), CInt(cboUnits.SelectedValue))
                        dtTempRow(i).Item("teamunkid") = IIf(CInt(cboTeams.SelectedValue) = 0, dtTempRow(i).Item("teamunkid"), CInt(cboTeams.SelectedValue))
                        'S.SANDEEP [ 08 OCT 2012 ] -- END

                        'Shani(17-Nov-2015) -- Start
                        'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                        'Shani(17-Nov-2015) -- Start
                        'ENHANCEMENT : Pay type -Include this on Employee Master Importation
                        dtTempRow(i).Item("paytype") = IIf(CInt(cboPayType.SelectedValue) = 0, dtTempRow(i).Item("paytype"), cboPayType.Text)
                        dtTempRow(i).Item("paytypeunkid") = IIf(CInt(cboPayType.SelectedValue) = 0, dtTempRow(i).Item("paytypeunkid"), CInt(cboPayType.SelectedValue))
                        'Shani(17-Nov-2015) -- End
                        'Shani(17-Nov-2015) -- End

                        dtTempRow(i).Item("IsChanged") = True


                        dtTempRow(i).AcceptChanges()
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lnkSet_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 08 OCT 2012 ] -- END


#End Region

#Region " Combobox Events "

    Private Sub cboGradeGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGrp.SelectedIndexChanged
        Try
            Dim dsCombos As DataSet = Nothing
            Dim objGrade As New clsGrade
            dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGrp.SelectedValue))
            cboGrade.ValueMember = "gradeunkid"
            cboGrade.DisplayMember = "name"
            cboGrade.DataSource = dsCombos.Tables("Grade")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try
            Dim dsCombos As DataSet = Nothing
            Dim objGradeLevel As New clsGradeLevel
            dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            cboGradeLevel.ValueMember = "gradelevelunkid"
            cboGradeLevel.DisplayMember = "name"
            cboGradeLevel.DataSource = dsCombos.Tables("GradeLevel")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Dim objWagesTran As New clsWagesTran
        Dim decScale As Decimal = 0 'Sohail (11 May 2011)
        Dim objWages As New clsWagesTran
        Try
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), decScale)
            objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), decScale)
            'Sohail (27 Apr 2016) -- End
            txtSalary.Text = CStr(decScale)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 08 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStation.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboStation.SelectedValue), "DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboStation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartmentGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartmentGrp.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDepartmentGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Department")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDepartmentGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSectionGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSectionGroup.SelectedValue), "Section", True)
                With cboSections
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Section")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboSectionGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                With cboUnits
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Unit")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboUnitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet
                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSections.SelectedValue))
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnits.SelectedValue))
                With cboTeams
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 08 OCT 2012 ] -- END

#End Region

    'S.SANDEEP [ 06 SEP 2011 ] -- START
#Region " DataGrid Event's "

    Private Sub dgvImportInfo_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvImportInfo.DataError
        e.Cancel = False
    End Sub

    Private Sub dgvImportInfo_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvImportInfo.EditingControlShowing
        Try
            If (Me.dgvImportInfo.CurrentCell.ColumnIndex = dgcolhSalaray.Index) And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellValueChanged
        If e.RowIndex = -1 Then Exit Sub
        If IsDBNull(dgvImportInfo.Rows(e.RowIndex).Cells(dgcolhSalaray.Index).Value) Then
            dgvImportInfo.Rows(e.RowIndex).Cells(dgcolhSalaray.Index).Value = 0
        End If
    End Sub

#End Region
    'S.SANDEEP [ 06 SEP 2011 ] -- END 






    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnUpdate.GradientBackColor = GUI._ButttonBackColor
            Me.btnUpdate.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnUpdate.Text = Language._Object.getCaption(Me.btnUpdate.Name, Me.btnUpdate.Text)
            Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
            Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
            Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
            Me.lblEmpType.Text = Language._Object.getCaption(Me.lblEmpType.Name, Me.lblEmpType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.lblCostcenter.Text = Language._Object.getCaption(Me.lblCostcenter.Name, Me.lblCostcenter.Text)
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblGradeGrp.Text = Language._Object.getCaption(Me.lblGradeGrp.Name, Me.lblGradeGrp.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblSalary.Text = Language._Object.getCaption(Me.lblSalary.Name, Me.lblSalary.Text)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.lnkSet.Text = Language._Object.getCaption(Me.lnkSet.Name, Me.lnkSet.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.dgcolhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeeCode.Name, Me.dgcolhEmployeeCode.HeaderText)
            Me.dgcolhFirstName.HeaderText = Language._Object.getCaption(Me.dgcolhFirstName.Name, Me.dgcolhFirstName.HeaderText)
            Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
            Me.dgcolhDeptGroup.HeaderText = Language._Object.getCaption(Me.dgcolhDeptGroup.Name, Me.dgcolhDeptGroup.HeaderText)
            Me.dgcolhDept.HeaderText = Language._Object.getCaption(Me.dgcolhDept.Name, Me.dgcolhDept.HeaderText)
            Me.dgcolhSectionGroup.HeaderText = Language._Object.getCaption(Me.dgcolhSectionGroup.Name, Me.dgcolhSectionGroup.HeaderText)
            Me.dgcolhSection.HeaderText = Language._Object.getCaption(Me.dgcolhSection.Name, Me.dgcolhSection.HeaderText)
            Me.dgcolhUnitGroup.HeaderText = Language._Object.getCaption(Me.dgcolhUnitGroup.Name, Me.dgcolhUnitGroup.HeaderText)
            Me.dgcolhUnit.HeaderText = Language._Object.getCaption(Me.dgcolhUnit.Name, Me.dgcolhUnit.HeaderText)
            Me.dgcolhTeam.HeaderText = Language._Object.getCaption(Me.dgcolhTeam.Name, Me.dgcolhTeam.HeaderText)
            Me.dgcolhEmpType.HeaderText = Language._Object.getCaption(Me.dgcolhEmpType.Name, Me.dgcolhEmpType.HeaderText)
            Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhGradeGrp.HeaderText = Language._Object.getCaption(Me.dgcolhGradeGrp.Name, Me.dgcolhGradeGrp.HeaderText)
            Me.dgcolhGrade.HeaderText = Language._Object.getCaption(Me.dgcolhGrade.Name, Me.dgcolhGrade.HeaderText)
            Me.dgcolhGradelevel.HeaderText = Language._Object.getCaption(Me.dgcolhGradelevel.Name, Me.dgcolhGradelevel.HeaderText)
            Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
            Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
            Me.dgcolhSalaray.HeaderText = Language._Object.getCaption(Me.dgcolhSalaray.Name, Me.dgcolhSalaray.HeaderText)
            Me.dgcolhPayType.HeaderText = Language._Object.getCaption(Me.dgcolhPayType.Name, Me.dgcolhPayType.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmEmployeeList", 10, "Select")
            Language.setMessage("frmEmployeeList", 11, "(A). Approved")
            Language.setMessage(mstrModuleName, 1, "Please select atleast one action to set the value.")
            Language.setMessage(mstrModuleName, 2, "Some information cannot be edited due to some problem in scale range or period has not been created.")
            Language.setMessage(mstrModuleName, 3, "Information edited successfully.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Scale should be in between")
            Language.setMessage(mstrModuleName, 5, " And")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'Public Class frmEditImprotedEmployee1

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmImportExportHeads"
'    Private mblnCancel As Boolean = True
'    Private dsList As New DataSet
'    Private mdtTable As New DataTable
'    Private objIExcel As ExcelData

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog() As Boolean
'        Try
'            Me.ShowDialog()
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Try
'            Dim objEmpType As New clsCommon_Master
'            dsCombos = objEmpType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmpType")
'            cboEmpType.ValueMember = "masterunkid"
'            cboEmpType.DisplayMember = "name"
'            cboEmpType.DataSource = dsCombos.Tables("EmpType")

'            dsCombos = Nothing
'            Dim objShift As New clsshift_master
'            dsCombos = objShift.getListForCombo("Shift", True)
'            cboShift.ValueMember = "shiftunkid"
'            cboShift.DisplayMember = "name"
'            cboShift.DataSource = dsCombos.Tables("Shift")

'            dsCombos = Nothing
'            Dim objDept As New clsDepartment
'            dsCombos = objDept.getComboList("Dept", True)
'            cboDepartment.ValueMember = "departmentunkid"
'            cboDepartment.DisplayMember = "name"
'            cboDepartment.DataSource = dsCombos.Tables("Dept")

'            dsCombos = Nothing
'            Dim objjob As New clsJobs
'            dsCombos = objjob.getComboList("Job", True)
'            cboJob.ValueMember = "jobunkid"
'            cboJob.DisplayMember = "name"
'            cboJob.DataSource = dsCombos.Tables("Job")

'            dsCombos = Nothing
'            Dim objGradegrp As New clsGradeGroup
'            dsCombos = objGradegrp.getComboList("GradeGrp", True)
'            cboGradeGrp.ValueMember = "gradegroupunkid"
'            cboGradeGrp.DisplayMember = "name"
'            cboGradeGrp.DataSource = dsCombos.Tables("GradeGrp")


'            dsCombos = Nothing
'            Dim objTranHead As New clsTransactionHead
'            dsCombos = objTranHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
'            cboTranhead.ValueMember = "tranheadunkid"
'            cboTranhead.DisplayMember = "name"
'            cboTranhead.DataSource = dsCombos.Tables("TranHead")


'            dsCombos = Nothing
'            Dim objCostCenter As New clscostcenter_master
'            dsCombos = objCostCenter.getComboList("CostCenter", True)
'            cboCostcenter.ValueMember = "costcenterunkid"
'            cboCostcenter.DisplayMember = "costcentername"
'            cboCostcenter.DataSource = dsCombos.Tables("CostCenter")

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillGirdView()
'        Try
'            dgvImportInfo.AutoGenerateColumns = False


'            objdgcolhCheck.DataPropertyName = "IsChecked"
'            dgcolhEmployee.DataPropertyName = "employeecode"
'            dgcolhFirstName.DataPropertyName = "firstname"
'            dgcolhOthername.DataPropertyName = "othername"
'            dgcolhSurname.DataPropertyName = "surname"
'            dgcolhEmpType.DataPropertyName = "employmenttypename"
'            dgcolhShift.DataPropertyName = "shiftname"
'            dgcolhDept.DataPropertyName = "deptname"
'            dgcolhJob.DataPropertyName = "jobname"
'            dgcolhGradeGrp.DataPropertyName = "gradegrpname"
'            dgcolhGrade.DataPropertyName = "gradename"
'            dgcolhGradelevel.DataPropertyName = "gradelevelname"
'            dgcolhTranHead.DataPropertyName = "tranheadname"
'            dgcolhCostCenter.DataPropertyName = "costcentername"
'            dgcolhSalaray.DataPropertyName = "scale"
'            dgcolhappointeddate.DataPropertyName = "appointeddate"

'            dgvImportInfo.DataSource = mdtTable
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmImportEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objIExcel = New ExcelData
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call FillCombo()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmImportEmployee_Load", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "

'    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click

'        If radApplytoAll.Checked = False And radApplySelected.Checked = False And radApplytoChecked.Checked = False Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please atleast one action to set the value."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If CInt(cboEmpType.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employeement Type is compulsory Information. Please Select Employeement Type."), enMsgBoxStyle.Information)
'            cboEmpType.Select()
'            Exit Sub

'        ElseIf CInt(cboShift.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Shift is compulsory Information. Please Select Shift."), enMsgBoxStyle.Information)
'            cboShift.Select()
'            Exit Sub

'        ElseIf CInt(cboDepartment.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Deparment is compulsory Information. Please Select Deparment."), enMsgBoxStyle.Information)
'            cboDepartment.Select()
'            Exit Sub

'        ElseIf CInt(cboJob.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Job is compulsory Information. Please Select Job."), enMsgBoxStyle.Information)
'            cboJob.Select()
'            Exit Sub

'        ElseIf CInt(cboGradeGrp.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Grade Group is compulsory Information. Please Select Grade Group."), enMsgBoxStyle.Information)
'            cboGradeGrp.Select()
'            Exit Sub

'        ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Grade is compulsory Information. Please Select Grade."), enMsgBoxStyle.Information)
'            cboGrade.Select()
'            Exit Sub

'        ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Grade Level is compulsory Information. Please Select Grade Level."), enMsgBoxStyle.Information)
'            cboGradeLevel.Select()
'            Exit Sub

'        ElseIf txtSalary.Text = "" Or cdec(txtSalary.Text.Trim) = 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Salary cannot be blank. Salary is required information."), enMsgBoxStyle.Information)
'            cboGradeLevel.Select()
'            Exit Sub

'        ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Transaction Head is compulsory Information. Please Select Transaction Head."), enMsgBoxStyle.Information)
'            cboTranhead.Select()
'            Exit Sub

'        ElseIf CInt(cboCostcenter.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Cost Center is compulsory Information. Please Select Cost Center."), enMsgBoxStyle.Information)
'            cboCostcenter.Select()
'            Exit Sub

'        End If

'        If mdtTable.Rows.Count = 0 Then Exit Sub

'        Try
'            If radApplytoAll.Checked = True Then

'                For Each dRow As DataRow In mdtTable.Rows
'                    dRow.Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
'                    dRow.Item("shiftunkid") = CInt(cboShift.SelectedValue)
'                    dRow.Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
'                    dRow.Item("jobunkid") = CInt(cboJob.SelectedValue)
'                    dRow.Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
'                    dRow.Item("gradeunkid") = CInt(cboGrade.SelectedValue)
'                    dRow.Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
'                    dRow.Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
'                    dRow.Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
'                    dRow.Item("scale") = cdec(txtSalary.Text.Trim)
'                    dRow.Item("appointeddate") = dtAppointdate.Value.Date
'                    dRow.Item("IsChange") = True

'                    dRow.Item("employmenttypename") = cboEmpType.Text
'                    dRow.Item("shiftname") = cboShift.Text
'                    dRow.Item("deptname") = cboDepartment.Text
'                    dRow.Item("jobname") = cboJob.Text
'                    dRow.Item("gradegrpname") = cboGradeGrp.Text
'                    dRow.Item("gradename") = cboGrade.Text
'                    dRow.Item("gradelevelname") = cboGradeLevel.Text
'                    dRow.Item("tranheadname") = cboTranhead.Text
'                    dRow.Item("costcentername") = cboCostcenter.Text

'                    mdtTable.AcceptChanges()

'                Next

'            ElseIf radApplySelected.Checked = True Then

'                Dim intSelectedIndex As Integer
'                If dgvImportInfo.SelectedRows.Count > 0 Then

'                    intSelectedIndex = dgvImportInfo.SelectedRows(0).Index

'                    mdtTable.Rows(intSelectedIndex).Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("shiftunkid") = CInt(cboShift.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("jobunkid") = CInt(cboJob.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("gradeunkid") = CInt(cboGrade.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
'                    mdtTable.Rows(intSelectedIndex).Item("scale") = cdec(txtSalary.Text.Trim)
'                    mdtTable.Rows(intSelectedIndex).Item("appointeddate") = dtAppointdate.Value.Date
'                    mdtTable.Rows(intSelectedIndex).Item("IsChange") = True


'                    mdtTable.Rows(intSelectedIndex).Item("employmenttypename") = cboEmpType.Text
'                    mdtTable.Rows(intSelectedIndex).Item("shiftname") = cboShift.Text
'                    mdtTable.Rows(intSelectedIndex).Item("deptname") = cboDepartment.Text
'                    mdtTable.Rows(intSelectedIndex).Item("jobname") = cboJob.Text
'                    mdtTable.Rows(intSelectedIndex).Item("gradegrpname") = cboGradeGrp.Text
'                    mdtTable.Rows(intSelectedIndex).Item("gradename") = cboGrade.Text
'                    mdtTable.Rows(intSelectedIndex).Item("gradelevelname") = cboGradeLevel.Text
'                    mdtTable.Rows(intSelectedIndex).Item("tranheadname") = cboTranhead.Text
'                    mdtTable.Rows(intSelectedIndex).Item("costcentername") = cboCostcenter.Text

'                End If


'            ElseIf radApplytoChecked.Checked = True Then

'                Dim dtTemp() As DataRow = mdtTable.Select("IsChecked = True And IsChange = False")
'                'If dtTemp.Length <= 0 Then
'                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select atleast one item to assign."), enMsgBoxStyle.Information)
'                '    Exit Sub
'                'End If

'                For i As Integer = 0 To dtTemp.Length - 1

'                    dtTemp(i).Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
'                    dtTemp(i).Item("shiftunkid") = CInt(cboShift.SelectedValue)
'                    dtTemp(i).Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
'                    dtTemp(i).Item("jobunkid") = CInt(cboJob.SelectedValue)
'                    dtTemp(i).Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
'                    dtTemp(i).Item("gradeunkid") = CInt(cboGrade.SelectedValue)
'                    dtTemp(i).Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
'                    dtTemp(i).Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
'                    dtTemp(i).Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
'                    dtTemp(i).Item("scale") = cdec(txtSalary.Text.Trim)
'                    dtTemp(i).Item("appointeddate") = dtAppointdate.Value.Date
'                    dtTemp(i).Item("IsChange") = True

'                    dtTemp(i).Item("employmenttypename") = cboEmpType.Text
'                    dtTemp(i).Item("shiftname") = cboShift.Text
'                    dtTemp(i).Item("deptname") = cboDepartment.Text
'                    dtTemp(i).Item("jobname") = cboJob.Text
'                    dtTemp(i).Item("gradegrpname") = cboGradeGrp.Text
'                    dtTemp(i).Item("gradename") = cboGrade.Text
'                    dtTemp(i).Item("gradelevelname") = cboGradeLevel.Text
'                    dtTemp(i).Item("tranheadname") = cboTranhead.Text
'                    dtTemp(i).Item("costcentername") = cboCostcenter.Text

'                Next

'            End If
'            Call FillGirdView()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim ofdlgOpen As New OpenFileDialog
'        Dim ObjFile As FileInfo
'        Try

'            'Pinkal (24-Jan-2011) -- Start

'            If ConfigParameter._Object._ExportDataPath = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set Export Data Path from Aruti configuration -> Options -> Path."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                Exit Sub
'            End If

'            'Pinkal (24-Jan-2011) -- End


'            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath

'            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx""
'            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
'                ObjFile = New FileInfo(ofdlgOpen.FileName)
'                txtFilePath.Text = ofdlgOpen.FileName
'                Select Case ofdlgOpen.FilterIndex
'                    Case 1
'                        dsList.Tables.Clear()
'                        dsList.ReadXml(txtFilePath.Text)
'                    Case 2
'                        dsList = objIExcel.Import(txtFilePath.Text)
'                End Select
'                Dim frm As New frmEmployeeMapping
'                If frm.displayDialog(dsList) = False Then
'                    mblnCancel = False
'                    Exit Sub
'                End If
'                mdtTable = frm._DataTable
'                Call FillGirdView()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
'        Try
'            mblnCancel = True
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
'        Dim blnFlag As Boolean = False
'        Dim dtTemp() As DataRow = Nothing

'        dtTemp = mdtTable.Select("employmenttypeunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please set the Employment Type in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("shiftunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please set the Shift  in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("departmentunkid =  -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set the Department in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("jobunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please set the Job in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("gradegroupunkid =  -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please set the Grade Group in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("gradeunkid =  -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please set the Grade in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("gradelevelunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please set the Grade Level in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("scale <=  0 ")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please enter the Salary in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("tranhedunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please set the Transaction Head in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        dtTemp = mdtTable.Select("costcenterunkid = -1")
'        If dtTemp.Length > 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set the Cost Center in order to Import file."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If


'        Try
'            Dim objemployee As New clsEmployee_Master

'            For i As Integer = 0 To mdtTable.Rows.Count - 1

'                objemployee._Employeecode = mdtTable.Rows(i)("employeecode").ToString
'                objemployee._Titalunkid = CInt(mdtTable.Rows(i)("titleunkid").ToString)
'                objemployee._Firstname = mdtTable.Rows(i)("firstname").ToString
'                objemployee._Surname = mdtTable.Rows(i)("surname").ToString
'                objemployee._Othername = mdtTable.Rows(i)("othername").ToString

'                If Not IsDBNull(mdtTable.Rows(i)("appointeddate")) Then
'                    objemployee._Appointeddate = CDate(mdtTable.Rows(i)("appointeddate"))
'                End If

'                objemployee._Gender = CInt(mdtTable.Rows(i)("gender").ToString)
'                objemployee._Employmenttypeunkid = CInt(mdtTable.Rows(i)("employmenttypeunkid").ToString)
'                objemployee._Paytypeunkid = CInt(mdtTable.Rows(i)("paytypeunkid").ToString)
'                objemployee._Paypointunkid = CInt(mdtTable.Rows(i)("paypointunkid").ToString)
'                objemployee._Loginname = mdtTable.Rows(i)("loginname").ToString
'                objemployee._Password = mdtTable.Rows(i)("password").ToString
'                objemployee._Email = mdtTable.Rows(i)("email").ToString
'                objemployee._Displayname = mdtTable.Rows(i)("displayname").ToString
'                objemployee._Shiftunkid = CInt(mdtTable.Rows(i)("shiftunkid").ToString)

'                If Not IsDBNull(mdtTable.Rows(i)("birthdate")) Then
'                    objemployee._Birthdate = CDate(mdtTable.Rows(i)("birthdate"))
'                End If

'                objemployee._Birth_Ward = mdtTable.Rows(i)("birth_ward").ToString
'                objemployee._Birthcertificateno = mdtTable.Rows(i)("birthcertificateno").ToString
'                objemployee._Birthstateunkid = CInt(mdtTable.Rows(i)("birthstateunkid").ToString)
'                objemployee._Birthcountryunkid = CInt(mdtTable.Rows(i)("birthcountryunkid").ToString)
'                objemployee._Birthcityunkid = CInt(mdtTable.Rows(i)("birthcityunkid").ToString)
'                objemployee._Birth_Village = mdtTable.Rows(i)("birth_village").ToString
'                objemployee._Work_Permit_No = mdtTable.Rows(i)("work_permit_no").ToString
'                objemployee._Workcountryunkid = CInt(mdtTable.Rows(i)("workcountryunkid").ToString)
'                objemployee._Work_Permit_Issue_Place = mdtTable.Rows(i)("work_permit_issue_place").ToString

'                If Not IsDBNull(mdtTable.Rows(i)("work_permit_issue_date")) Then
'                    objemployee._Work_Permit_Issue_Date = CDate(mdtTable.Rows(i)("work_permit_issue_date"))
'                End If

'                If Not IsDBNull(mdtTable.Rows(i)("work_permit_expiry_date")) Then
'                    objemployee._Work_Permit_Expiry_Date = CDate(mdtTable.Rows(i)("work_permit_expiry_date"))
'                End If

'                objemployee._Complexionunkid = CInt(mdtTable.Rows(i)("complexionunkid").ToString)
'                objemployee._Bloodgroupunkid = CInt(mdtTable.Rows(i)("bloodgroupunkid").ToString)
'                objemployee._Eyecolorunkid = CInt(mdtTable.Rows(i)("eyecolorunkid").ToString)
'                objemployee._Nationalityunkid = CInt(mdtTable.Rows(i)("nationalityunkid").ToString)
'                objemployee._Ethincityunkid = CInt(mdtTable.Rows(i)("ethnicityunkid").ToString)
'                objemployee._Religionunkid = CInt(mdtTable.Rows(i)("religionunkid").ToString)
'                objemployee._Hairunkid = CInt(mdtTable.Rows(i)("hairunkid").ToString)
'                objemployee._Language1unkid = CInt(mdtTable.Rows(i)("language1unkid").ToString)
'                objemployee._Language2unkid = CInt(mdtTable.Rows(i)("language2unkid").ToString)
'                objemployee._Language3unkid = CInt(mdtTable.Rows(i)("language3unkid").ToString)
'                objemployee._Language4unkid = CInt(mdtTable.Rows(i)("language4unkid").ToString)
'                objemployee._Extra_Tel_No = mdtTable.Rows(i)("extra_tel_no").ToString
'                objemployee._Height = cdec(mdtTable.Rows(i)("height").ToString)
'                objemployee._Weight = cdec(mdtTable.Rows(i)("Weight").ToString)
'                objemployee._Maritalstatusunkid = CInt(mdtTable.Rows(i)("maritalstatusunkid").ToString)

'                If Not IsDBNull(mdtTable.Rows(i)("anniversary_date")) Then
'                    objemployee._Anniversary_Date = CDate(mdtTable.Rows(i)("anniversary_date"))
'                End If

'                objemployee._Sports_Hobbies = mdtTable.Rows(i)("sports_hobbies").ToString
'                objemployee._Present_Address1 = mdtTable.Rows(i)("present_address1").ToString
'                objemployee._Present_Address2 = mdtTable.Rows(i)("present_address2").ToString
'                objemployee._Present_Countryunkid = CInt(mdtTable.Rows(i)("present_countryunkid").ToString)
'                objemployee._Present_Postcodeunkid = CInt(mdtTable.Rows(i)("present_postcodeunkid").ToString)
'                objemployee._Present_Stateunkid = CInt(mdtTable.Rows(i)("present_stateunkid").ToString)
'                objemployee._Present_Provicnce = mdtTable.Rows(i)("present_provicnce").ToString
'                objemployee._Present_Post_Townunkid = CInt(mdtTable.Rows(i)("present_post_townunkid").ToString)
'                objemployee._Present_Road = mdtTable.Rows(i)("present_road").ToString
'                objemployee._Present_Estate = mdtTable.Rows(i)("present_estate").ToString
'                objemployee._Present_Plotno = mdtTable.Rows(i)("present_plotNo").ToString
'                objemployee._Present_Mobile = mdtTable.Rows(i)("present_mobile").ToString
'                objemployee._Present_Alternateno = mdtTable.Rows(i)("present_alternateno").ToString
'                objemployee._Present_Tel_No = mdtTable.Rows(i)("present_tel_no").ToString
'                objemployee._Present_Fax = mdtTable.Rows(i)("present_fax").ToString
'                objemployee._Present_Email = mdtTable.Rows(i)("present_email").ToString
'                objemployee._Domicile_Address1 = mdtTable.Rows(i)("domicile_address1").ToString
'                objemployee._Domicile_Address2 = mdtTable.Rows(i)("domicile_address2").ToString
'                objemployee._Domicile_Countryunkid = CInt(mdtTable.Rows(i)("domicile_countryunkid").ToString)
'                objemployee._Domicile_Postcodeunkid = CInt(mdtTable.Rows(i)("domicile_postcodeunkid").ToString)
'                objemployee._Domicile_Stateunkid = CInt(mdtTable.Rows(i)("domicile_stateunkid").ToString)
'                objemployee._Domicile_Provicnce = mdtTable.Rows(i)("domicile_provicnce").ToString
'                objemployee._Domicile_Post_Townunkid = CInt(mdtTable.Rows(i)("domicile_post_townunkid").ToString)
'                objemployee._Domicile_Road = mdtTable.Rows(i)("domicile_road").ToString
'                objemployee._Domicile_Estate = mdtTable.Rows(i)("domicile_estate").ToString
'                objemployee._Domicile_Plotno = mdtTable.Rows(i)("domicile_plotNo").ToString
'                objemployee._Domicile_Mobile = mdtTable.Rows(i)("domicile_mobile").ToString
'                objemployee._Domicile_Alternateno = mdtTable.Rows(i)("domicile_alternateno").ToString
'                objemployee._Domicile_Tel_No = mdtTable.Rows(i)("domicile_tel_no").ToString
'                objemployee._Domicile_Fax = mdtTable.Rows(i)("domicile_fax").ToString
'                objemployee._Domicile_Email = mdtTable.Rows(i)("domicile_email").ToString
'                objemployee._Emer_Con_Firstname = mdtTable.Rows(i)("emer_con_firstname").ToString
'                objemployee._Emer_Con_Lastname = mdtTable.Rows(i)("emer_con_lastname").ToString
'                objemployee._Emer_Con_Address = mdtTable.Rows(i)("emer_con_address").ToString
'                objemployee._Emer_Con_Countryunkid = CInt(mdtTable.Rows(i)("emer_con_countryunkid").ToString)
'                objemployee._Emer_Con_Postcodeunkid = CInt(mdtTable.Rows(i)("emer_con_postcodeunkid").ToString)
'                objemployee._Emer_Con_State = CInt(mdtTable.Rows(i)("emer_con_state").ToString)
'                objemployee._Emer_Con_Provicnce = mdtTable.Rows(i)("emer_con_provicnce").ToString
'                objemployee._Emer_Con_Post_Townunkid = CInt(mdtTable.Rows(i)("emer_con_post_townunkid").ToString)
'                objemployee._Emer_Con_Road = mdtTable.Rows(i)("emer_con_road").ToString
'                objemployee._Emer_Con_Estate = mdtTable.Rows(i)("emer_con_estate").ToString
'                objemployee._Emer_Con_Plotno = mdtTable.Rows(i)("emer_con_plotNo").ToString
'                objemployee._Emer_Con_Mobile = mdtTable.Rows(i)("emer_con_mobile").ToString
'                objemployee._Emer_Con_Alternateno = mdtTable.Rows(i)("emer_con_alternateno").ToString
'                objemployee._Emer_Con_Tel_No = mdtTable.Rows(i)("emer_con_tel_no").ToString
'                objemployee._Emer_Con_Fax = mdtTable.Rows(i)("emer_con_fax").ToString
'                objemployee._Emer_Con_Email = mdtTable.Rows(i)("emer_con_email").ToString
'                objemployee._Stationunkid = CInt(mdtTable.Rows(i)("stationunkid").ToString)
'                objemployee._Deptgroupunkid = CInt(mdtTable.Rows(i)("deptgroupunkid").ToString)
'                objemployee._Departmentunkid = CInt(mdtTable.Rows(i)("departmentunkid").ToString)
'                objemployee._Sectionunkid = CInt(mdtTable.Rows(i)("sectionunkid").ToString)
'                objemployee._Unitunkid = CInt(mdtTable.Rows(i)("unitunkid").ToString)
'                objemployee._Jobgroupunkid = CInt(mdtTable.Rows(i)("jobgroupunkid").ToString)
'                objemployee._Jobunkid = CInt(mdtTable.Rows(i)("jobunkid").ToString)
'                objemployee._Gradegroupunkid = CInt(mdtTable.Rows(i)("gradegroupunkid").ToString)
'                objemployee._Gradeunkid = CInt(mdtTable.Rows(i)("gradeunkid").ToString)
'                objemployee._Gradelevelunkid = CInt(mdtTable.Rows(i)("gradelevelunkid").ToString)
'                objemployee._Scale = cdec(mdtTable.Rows(i)("scale").ToString)
'                objemployee._Accessunkid = CInt(mdtTable.Rows(i)("accessunkid").ToString)
'                objemployee._Classgroupunkid = CInt(mdtTable.Rows(i)("classgroupunkid").ToString)
'                objemployee._Classunkid = CInt(mdtTable.Rows(i)("classunkid").ToString)
'                objemployee._Serviceunkid = CInt(mdtTable.Rows(i)("serviceunkid").ToString)
'                objemployee._Costcenterunkid = CInt(mdtTable.Rows(i)("costcenterunkid").ToString)
'                objemployee._Tranhedunkid = CInt(mdtTable.Rows(i)("tranhedunkid").ToString)
'                objemployee._Actionreasonunkid = CInt(mdtTable.Rows(i)("actionreasonunkid").ToString)

'                If mdtTable.Rows(i)("suspended_from_date").ToString.Trim <> "" Then
'                    objemployee._Suspende_From_Date = CDate(mdtTable.Rows(i)("suspended_from_date"))
'                End If

'                If mdtTable.Rows(i)("suspended_to_date").ToString.Trim <> "" Then
'                    objemployee._Suspende_To_Date = CDate(mdtTable.Rows(i)("suspended_to_date"))
'                End If

'                If mdtTable.Rows(i)("probation_from_date").ToString.Trim <> "" Then
'                    objemployee._Probation_From_Date = CDate(mdtTable.Rows(i)("probation_from_date"))
'                End If

'                If mdtTable.Rows(i)("probation_to_date").ToString.Trim <> "" Then
'                    objemployee._Probation_To_Date = CDate(mdtTable.Rows(i)("probation_to_date"))
'                End If

'                If mdtTable.Rows(i)("termination_from_date").ToString.Trim <> "" Then
'                    objemployee._Termination_From_Date = CDate(mdtTable.Rows(i)("termination_from_date"))
'                End If

'                If mdtTable.Rows(i)("termination_to_date").ToString.Trim <> "" Then
'                    objemployee._Termination_To_Date = CDate(mdtTable.Rows(i)("termination_to_date"))
'                End If

'                objemployee._Remark = mdtTable.Rows(i)("remark").ToString
'                objemployee._Isactive = CBool(mdtTable.Rows(i)("isactive").ToString)

'                blnFlag = objemployee.Insert()
'                If blnFlag = False And objemployee._Message <> "" Then
'                    eZeeMsgBox.Show(objemployee._Message, enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'            Next
'            mblnCancel = False
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddSalary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSalary.Click
'        Dim frm As New frmWagetable_AddEdit
'        Try
'            frm.ShowDialog()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddSalary_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " DataGridview Event "
'    'Anjan (29 Jan 2011)-Start
'    'Issue : this made is been commented temporarily.
'    'Private Sub dgvImportInfo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellContentClick
'    '    Try
'    '        If e.RowIndex <= -1 Then Exit Sub

'    '        If CBool(mdtTable.Rows(e.RowIndex)("IsChecked")) = True Then

'    '            mdtTable.Rows(e.RowIndex)("IsChecked") = False
'    '            mdtTable.Rows(e.RowIndex).Item("employmenttypeunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("shiftunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("departmentunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("jobunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("gradegroupunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("gradeunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("gradelevelunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("tranhedunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("costcenterunkid") = -1
'    '            mdtTable.Rows(e.RowIndex).Item("scale") = 0.0
'    '            mdtTable.Rows(e.RowIndex).Item("appointeddate") = DBNull.Value
'    '            mdtTable.Rows(e.RowIndex).Item("IsChange") = False

'    '            mdtTable.Rows(e.RowIndex).Item("employmenttypename") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("shiftname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("deptname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("jobname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("gradegrpname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("gradename") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("gradelevelname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("tranheadname") = ""
'    '            mdtTable.Rows(e.RowIndex).Item("costcentername") = ""

'    '            dgvImportInfo.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = mdtTable.Rows(e.RowIndex)("IsChecked")

'    '            dgvImportInfo.RefreshEdit()
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'    '    End Try
'    'End Sub
'    'Anjan (29 Jan 2011)-End
'#End Region

'#Region "Combobox Event"

'    Private Sub cboGradeGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGrp.SelectedIndexChanged
'        Try
'            Dim dsCombos As DataSet = Nothing
'            Dim objGrade As New clsGrade
'            dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGrp.SelectedValue))
'            cboGrade.ValueMember = "gradeunkid"
'            cboGrade.DisplayMember = "name"
'            cboGrade.DataSource = dsCombos.Tables("Grade")
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGradeGrp_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
'        Try
'            Dim dsCombos As DataSet = Nothing
'            Dim objGradeLevel As New clsGradeLevel
'            dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
'            cboGradeLevel.ValueMember = "gradelevelunkid"
'            cboGradeLevel.DisplayMember = "name"
'            cboGradeLevel.DataSource = dsCombos.Tables("GradeLevel")
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
'        Dim objWagesTran As New clsWagesTran
'        Dim dblScale As Double = 0
'        Dim objWages As New clsWagesTran
'        Try
'            objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), dblScale)
'            txtSalary.Text = CStr(dblScale)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region
'End Class