﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeMapping
    Private ReadOnly mstrModuleName As String = "frmTranHeadMapping"
    Private dsData As New DataSet
    Private mblnCancel As Boolean = True
    Private dtTable As DataTable
    Private dsDataList As New DataSet
    Dim objEmp As clsEmployee_Master

#Region " Properties "

    Public ReadOnly Property _DataTable() As DataTable
        Get
            Return dtTable
        End Get
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal dsList As DataSet) As Boolean
        Try
            dsData = dsList
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmp = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            cboEmployeeCode.Items.Clear()
            cboFirstName.Items.Clear()
            cboOtherName.Items.Clear()
            cboSurname.Items.Clear()
            SetData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeMapping_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " Private Methods "

    Private Sub SetData()
        Try
            dtTable = New DataTable("Employee")
            For Each dtColumns As DataColumn In dsData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboFirstName.Items.Add(dtColumns.ColumnName)
                cboOtherName.Items.Add(dtColumns.ColumnName)
                cboSurname.Items.Add(dtColumns.ColumnName)
            Next


            dtTable.Columns.Add("employeecode", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("titleunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("firstname", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("surname", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("othername", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("appointeddate", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("gender", Type.GetType("System.Int16")).DefaultValue = -1

            dtTable.Columns.Add("employmenttypeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employmenttypename", Type.GetType("System.String")).DefaultValue = ""


            dtTable.Columns.Add("paytypeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("paypointunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("loginname", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("password", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("email", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("displayname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("shiftunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("shiftname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("birthdate", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("birth_ward", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("birthcertificateno", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("birthstateunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("birthcountryunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("birthcityunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("birth_village", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("work_permit_no", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("workcountryunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("work_permit_issue_place", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("work_permit_issue_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("work_permit_expiry_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("complexionunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("bloodgroupunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("eyecolorunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("nationalityunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("ethnicityunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("religionunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("hairunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("language1unkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("language2unkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("language3unkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("language4unkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("extra_tel_no", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("height", Type.GetType("System.Decimal")).DefaultValue = 0.0
            dtTable.Columns.Add("Weight", Type.GetType("System.Decimal")).DefaultValue = 0.0
            dtTable.Columns.Add("maritalstatusunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("anniversary_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("sports_hobbies", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_address1", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_address2", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_countryunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("present_postcodeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("present_stateunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("present_provicnce", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_post_townunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("present_road", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_estate", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_plotNo", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_mobile", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_alternateno", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_tel_no", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_fax", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("present_email", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_address1", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_address2", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_countryunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("domicile_postcodeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("domicile_stateunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("domicile_provicnce", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_post_townunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("domicile_road", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_estate", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_plotNo", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_mobile", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_alternateno", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_tel_no", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_fax", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("domicile_email", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_firstname", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_lastname", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_address", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_countryunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("emer_con_postcodeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("emer_con_state", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("emer_con_provicnce", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_post_townunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("emer_con_road", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_estate", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_plotNo", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_mobile", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_alternateno", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_tel_no", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_fax", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("emer_con_email", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("stationunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("deptgroupunkid", Type.GetType("System.Int32")).DefaultValue = -1


            dtTable.Columns.Add("departmentunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("deptname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("sectionunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("unitunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("jobgroupunkid", Type.GetType("System.Int32")).DefaultValue = -1

            dtTable.Columns.Add("jobunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("jobname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("gradegroupunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradegrpname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("gradeunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradename", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("gradelevelunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("gradelevelname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("scale", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("accessunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("classgroupunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("classunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("serviceunkid", Type.GetType("System.Int32")).DefaultValue = -1

            dtTable.Columns.Add("costcenterunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("costcentername", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("tranhedunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("tranheadname", Type.GetType("System.String")).DefaultValue = ""

            dtTable.Columns.Add("actionreasonunkid", Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("suspended_from_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("suspended_to_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("probation_from_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("probation_to_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("termination_from_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("termination_to_date", Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            dtTable.Columns.Add("remark", Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("isactive", Type.GetType("System.Boolean")).DefaultValue = True
            dtTable.Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("IsChange", Type.GetType("System.Boolean")).DefaultValue = False

        Catch ex As Exception
            mblnCancel = False
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Dim mintCode As Integer = ConfigParameter._Object._NextEmployeeCodeNo
            For i As Integer = 0 To dsData.Tables(0).Rows.Count - 1
                Dim dRow As DataRow
                dRow = dtTable.NewRow

                If ConfigParameter._Object._EmployeeCodeNotype = 1 Then
                    dRow.Item("employeecode") = ConfigParameter._Object._EmployeeCodePrifix & mintCode
                    mintCode += 1
                Else
                    dRow.Item("employeecode") = dsData.Tables(0).Rows(i)(cboEmployeeCode.Text)
                End If

                dRow.Item("firstname") = dsData.Tables(0).Rows(i)(cboFirstName.Text)
                dRow.Item("othername") = dsData.Tables(0).Rows(i)(cboOtherName.Text)
                dRow.Item("surname") = dsData.Tables(0).Rows(i)(cboSurname.Text)

                dtTable.Rows.Add(dRow)

            Next
            mblnCancel = False
            objEmp = Nothing
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblSurName.Text = Language._Object.getCaption(Me.lblSurName.Name, Me.lblSurName.Text)
			Me.lblOtherName.Text = Language._Object.getCaption(Me.lblOtherName.Name, Me.lblOtherName.Text)
			Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class