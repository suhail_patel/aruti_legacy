﻿Option Strict On
#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmBulkUpdateDataWizard

#Region " Private Variables "

    Private mstrModuleName As String = "frmBulkUpdateDataWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)

#End Region

#Region " From's Events "

    Private Sub frmBulkUpdateData_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
            If ConfigParameter._Object._DisplayNameSetting > 0 Then
                chkDisplayName.Checked = False : chkDisplayName.Enabled = False
            End If

            'Gajanan [13-May-2020] -- Start
            'Enhancement:Add New Field In Bulk Employee Import
            chkFirstappointmentDate.Visible = ConfigParameter._Object._ShowFirstAppointmentDate
            objlblSign32.Visible = ConfigParameter._Object._ShowFirstAppointmentDate
            lblAppointmentDate.Visible = ConfigParameter._Object._ShowFirstAppointmentDate
            cboAppointmentDate.Visible = ConfigParameter._Object._ShowFirstAppointmentDate
            'Gajanan [13-May-2020] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBulkUpdateData_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"
            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If
            objFileOpen = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizUpdateEmployee_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizUpdateEmployee.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub WizUpdateEmployee_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizUpdateEmployee.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizUpdateEmployee.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim lstcheckbox As List(Of Control) = (From p In gbDatatoUpdate.Controls.Cast(Of Control)() Where (TypeOf p Is CheckBox) Select (p)).ToList
                    Dim lstchecked As List(Of Control) = lstcheckbox.Where(Function(x) CType(x, CheckBox).Checked = True).Select(Function(y) y).ToList()
                    If lstchecked.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "No update information selected to update. Please select atleast one update information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)
                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True) : mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    EnableControl()
                    Call SetDataCombo()
                Case WizUpdateEmployee.Pages.IndexOf(wizPageMapping)
                    Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) And (p).Enabled = True Select (p)).ToList
                    For Each ctrl As Control In lstCombos
                        If CType(ctrl, ComboBox).Text = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                            Exit For
                        End If
                    Next
                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim lstcheckbox As List(Of Control) = (From p In gbDatatoUpdate.Controls.Cast(Of Control)() Where (TypeOf p Is CheckBox) Select (p)).ToList
            Dim lstchecked As List(Of Control) = lstcheckbox.Where(Function(x) CType(x, CheckBox).Checked = True).Select(Function(y) y).ToList()
            If lstchecked.Count > 0 Then
                Dim dtTable As New DataTable("BulkUpdate")
                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'dtTable.Columns.Add("EmployeeCode", GetType(System.String)).DefaultValue = ""
                dtTable.Columns.Add(Language.getMessage(mstrModuleName, 78, "EmployeeCode"), System.Type.GetType("System.String")).DefaultValue = ""
                'Gajanan (24 Nov 2018) -- End


                For Each ctrl As Control In lstchecked
                    Select Case ctrl.Name.ToUpper
                        Case chkEmployeeCode.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("NewEmployeeCode", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 66, "NewEmployeeCode"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkFirstname.Name.ToUpper

                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Firstname", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 67, "Firstname"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkSurname.Name.ToUpper

                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Surname", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 68, "Surname"), System.Type.GetType("System.String")).DefaultValue = ""

                            'Gajanan (24 Nov 2018) -- End
                        Case chkPassword.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Password", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 69, "Password"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkEmploymentType.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("EmploymentType", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 70, "EmploymentType"), System.Type.GetType("System.String")).DefaultValue = ""

                            'Gajanan (24 Nov 2018) -- End
                        Case chkGender.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Gender", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 71, "Gender"), System.Type.GetType("System.String")).DefaultValue = ""

                            'Gajanan (24 Nov 2018) -- End
                        Case chkDisplayName.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Displayname", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 72, "Displayname"), System.Type.GetType("System.String")).DefaultValue = ""

                            'Gajanan (24 Nov 2018) -- End
                        Case chkEmail.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Email", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 73, "Email"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkBirthdate.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("Birthdate", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 74, "Birthdate"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkTransctionHead.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("TransactionHead", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 75, "TransactionHead"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End
                        Case chkPayType.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("PayType", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 76, "PayType"), System.Type.GetType("System.String")).DefaultValue = ""

                            'Gajanan (24 Nov 2018) -- End
                        Case chkPayPoint.Name.ToUpper
                            'Gajanan (24 Nov 2018) -- Start
                            'Enhancement : Import template column headers should read from language set for 
                            'users (custom 1) and columns' date formats for importation should be clearly known 
                            'UAT No:TC017 NMB
                            'dtTable.Columns.Add("PayPoint", GetType(System.String)).DefaultValue = ""
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 77, "PayPoint"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan (24 Nov 2018) -- End


                            'Gajanan [13-May-2020] -- Start
                            'Enhancement:Add New Field In Bulk Employee Import
                        Case chkMiddlename.Name.ToUpper
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 79, "Middlename"), System.Type.GetType("System.String")).DefaultValue = ""
                            
                        Case chkFirstappointmentDate.Name.ToUpper
                            dtTable.Columns.Add(Language.getMessage(mstrModuleName, 80, "FirstAppointmentDate"), System.Type.GetType("System.String")).DefaultValue = ""
                            'Gajanan [13-May-2020] -- End
                    End Select
                Next
                Dim dsList As New DataSet
                dsList.Tables.Add(dtTable.Copy)
                Dim dlgSaveFile As New SaveFileDialog
                dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
                If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                    OpenXML_Export(dlgSaveFile.FileName, dsList)
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 59, "Template Exported Successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 60, "Please check atleast one information to update and get file update format."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) And (p).Enabled = True Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl As Control In lstCombos
                Call ClearCombo(CType(ctrl, ComboBox))
            Next

            Dim columnNames() As String = mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)().Select(Function(x) x.ColumnName).ToArray()
            For Each ctrl As Control In lstCombos
                CType(ctrl, ComboBox).Items.AddRange(columnNames)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True
            mdt_ImportData_Others.Columns.Add("ecode", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ncode", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("firstname", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("surname", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("password", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("empltype", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("gender", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("displayname", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("email", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("birthdate", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("tranhead", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("paytype", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("paypoint", GetType(System.String)).DefaultValue = ""
            'Gajanan [13-May-2020] -- Start
            'Enhancement:Add New Field In Bulk Employee Import
            mdt_ImportData_Others.Columns.Add("firstappointmentdate", GetType(System.String)).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("middlename", GetType(System.String)).DefaultValue = ""
            'Gajanan [13-May-2020] -- End

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()
            Dim drNewRow As DataRow
            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboNewEmployeeCode.Enabled = True Then
                    drNewRow.Item("ncode") = dtRow.Item(cboNewEmployeeCode.Text).ToString.Trim
                End If
                If cboFirstname.Enabled = True Then
                    drNewRow.Item("firstname") = dtRow.Item(cboFirstname.Text).ToString.Trim
                End If
                If cboSurname.Enabled = True Then
                    drNewRow.Item("surname") = dtRow.Item(cboSurname.Text).ToString.Trim
                End If
                If cboPassword.Enabled = True Then
                    drNewRow.Item("password") = dtRow.Item(cboPassword.Text).ToString.Trim
                End If
                If cboEmploymentType.Enabled = True Then
                    drNewRow.Item("empltype") = dtRow.Item(cboEmploymentType.Text).ToString.Trim
                End If
                If cboGender.Enabled = True Then
                    drNewRow.Item("gender") = dtRow.Item(cboGender.Text).ToString.Trim
                End If
                If cboDisplayname.Enabled = True Then
                    drNewRow.Item("displayname") = dtRow.Item(cboDisplayname.Text).ToString.Trim
                End If
                If cboEmail.Enabled = True Then
                    drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString.Trim
                End If
                If cboBirthdate.Enabled = True Then
                    drNewRow.Item("birthdate") = dtRow.Item(cboBirthdate.Text).ToString.Trim
                End If
                If cboTransactionHead.Enabled = True Then
                    drNewRow.Item("tranhead") = dtRow.Item(cboTransactionHead.Text).ToString.Trim
                End If
                If cboPayType.Enabled = True Then
                    drNewRow.Item("paytype") = dtRow.Item(cboPayType.Text).ToString.Trim
                End If
                If cboPayPoint.Enabled = True Then
                    drNewRow.Item("paypoint") = dtRow.Item(cboPayPoint.Text).ToString.Trim
                End If

                'Gajanan [13-May-2020] -- Start
                'Enhancement:Add New Field In Bulk Employee Import
                If cboMiddlename.Enabled = True Then
                    drNewRow.Item("middlename") = dtRow.Item(cboMiddlename.Text).ToString.Trim
                End If
                If cboAppointmentDate.Enabled = True Then
                    drNewRow.Item("firstappointmentdate") = dtRow.Item(cboAppointmentDate.Text).ToString.Trim
                End If
                'Gajanan [13-May-2020] -- End


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizUpdateEmployee.BackEnabled = False
            WizUpdateEmployee.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
            ezWait.Active = False
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
                If cboEmail.Enabled = True Then
                    If .Item(cboEmail.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Email is mandatory information. Please select Email to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboDisplayname.Enabled = True Then
                    If .Item(cboDisplayname.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Display Name is mandatory information. Please select Display Name to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboBirthdate.Enabled = True Then
                    If .Item(cboBirthdate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Birthdate is mandatory information. Please select Birthdate to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    If IsDate(.Item(cboBirthdate.Text).ToString.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Birthdate format is not correct. Please set correct Birthdate format  to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboGender.Enabled = True Then
                    If .Item(cboGender.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Gender is mandatory information. Please select Gender to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboTransactionHead.Enabled = True Then
                    If .Item(cboTransactionHead.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 62, "Transaction Head Code is mandatory information. Please select Tran. Head Code to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboPayType.Enabled = True Then
                    If .Item(cboPayType.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 63, "Pay Type is mandatory information. Please select Pay Type to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboPayPoint.Enabled = True Then
                    If .Item(cboPayPoint.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 53, "Pay Point is mandatory information. Please select Pay Point to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboNewEmployeeCode.Enabled = True Then
                    If .Item(cboNewEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 55, "New Employeecode is mandatory information. Please select New Employeecode to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboFirstname.Enabled = True Then
                    If .Item(cboFirstname.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 56, "Fisrtname is mandatory information. Please select Fisrtname to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboSurname.Enabled = True Then
                    If .Item(cboSurname.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 64, "Surname is mandatory information. Please select Surname to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                If cboPassword.Enabled = True Then
                    If .Item(cboPassword.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 57, "Password is mandatory information. Please select Password to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                If cboEmploymentType.Enabled = True Then
                    If .Item(cboEmploymentType.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 58, "Employment Type is mandatory information. Please select Employment Type to continue."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                'S.SANDEEP [13-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}

                'S.SANDEEP [16-NOV-2018] -- START
                'If .Item(cboGender.Text).ToString.Trim.Length > 0 Then
                '    Select Case .Item(cboGender.Text).ToString.ToUpper
                '        Case "MALE", "FEMALE", "M", "F"
                '        Case Else
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 65, "Gender data in file is not vaild."), enMsgBoxStyle.Information)
                '            Return False
                '    End Select
                'End If
                If cboGender.Enabled AndAlso cboGender.Text.Trim.Length > 0 Then
                If .Item(cboGender.Text).ToString.Trim.Length > 0 Then
                    Select Case .Item(cboGender.Text).ToString.ToUpper
                        Case "MALE", "FEMALE", "M", "F"
                        Case Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 65, "Gender data in file is not vaild."), enMsgBoxStyle.Information)
                            Return False
                    End Select
                End If
                End If
                'S.SANDEEP [16-NOV-2018] -- END

                'S.SANDEEP [13-AUG-2018] -- END
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            Dim objEmp As New clsEmployee_Master
            Dim objCMaster As New clsCommon_Master
            Dim objTranHead As New clsTransactionHead
            Dim objPayPoint As New clspaypoint_master

            Dim intGenderId As Integer = -1
            Dim iTranHeadUnkId As Integer
            Dim iTranHeadTypeID As Integer
            Dim intPayTypeUnkid As Integer = -1
            Dim intPayPointUnkid As Integer = -1
            Dim intEmploymenttypeId As Integer = -1
            Dim iEmpId As Integer = -1

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                intGenderId = 0 : iTranHeadUnkId = 0 : iTranHeadTypeID = 0
                intPayPointUnkid = 0 : intPayTypeUnkid = 0 : iEmpId = 0 : intEmploymenttypeId = 0

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF EMAIL IS VALID.
                If dtRow.Item("email").ToString.Trim.Length > 0 Then
                    If Expression.IsMatch(dtRow.Item("email").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Invalid Email.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("gender").ToString.Trim.Length > 0 AndAlso dtRow.Item("gender").ToString.Trim <> "-1" Then
                    'S.SANDEEP [13-AUG-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                    'Dim strMale As String = Language.getMessage("clsMasterData", 73, "Male")
                    'Dim strFemale As String = Language.getMessage("clsMasterData", 74, "Female")
                    'Select Case dtRow.Item("gender").ToString.Trim.ToUpper
                    '    Case strMale.ToUpper
                    '        intGenderId = 1
                    '    Case strFemale.ToUpper
                    '        intGenderId = 2
                    '    Case Else
                    '        intGenderId = 0
                    'End Select
                    Select Case dtRow.Item("gender").ToString.Trim.ToUpper
                        Case "MALE", "M"
                            intGenderId = 1
                        Case "FEMALE", "F"
                            intGenderId = 2
                            'S.SANDEEP |02-MAR-2020| -- START
                            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                            'Case Else
                            '    intGenderId = 0
                            'S.SANDEEP |02-MAR-2020| -- END
                    End Select
                    'S.SANDEEP [13-AUG-2018] -- END
                End If

                '------------------------------ PAY TYPE
                If dtRow.Item("paytype").ToString.Trim.Length > 0 Then
                    intPayTypeUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.PAY_TYPE, dtRow.Item("paytype").ToString.Trim)
                    If intPayTypeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 51, "Pay Type Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("paypoint").ToString.Trim.Length > 0 Then
                    intPayPointUnkid = objPayPoint.GetPayPointUnkidByCode(dtRow.Item("paypoint").ToString.Trim)
                    If intPayPointUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 54, "Pay Point Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("empltype").ToString.Trim.Length > 0 Then
                    intEmploymenttypeId = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, dtRow.Item("empltype").ToString.Trim)
                    If intEmploymenttypeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 61, "Employment Type Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmpId

                '------------------------------ CHECKING IF Transaction Head Code
                If dtRow.Item("tranhead").ToString.Trim.Length > 0 Then
                    iTranHeadUnkId = 0 : iTranHeadTypeID = 0
                    Dim dHead As DataSet = Nothing
                    dHead = objTranHead.GetList("List", , , , , , , "trnheadcode = '" & dtRow.Item("tranhead").ToString.Trim() & "' AND typeof_id = " & enTypeOf.Salary & "")
                    If dHead.Tables("List").Rows.Count > 0 Then
                        iTranHeadUnkId = CInt(dHead.Tables("List").Rows(0)("tranheadunkid"))
                        iTranHeadTypeID = CInt(dHead.Tables("List").Rows(0)("trnheadtype_id"))
                    End If
                    If iTranHeadUnkId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 52, "Transaction Head Code Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objEmp._Tranhedunkid
                    If iTranHeadTypeID <> objTranHead._Trnheadtype_Id Then
                        If objEmp._Tranhedunkid <> iTranHeadUnkId Then
                            Dim dsData As New DataSet : Dim dRow() As DataRow = Nothing
                            Dim objSal As New clsSalaryIncrement : Dim objED As New clsEarningDeduction
                            Dim objCPeriod As New clsMasterData : Dim iCPeriodId As Integer = -1
                            Dim objPeriod As New clscommom_period_Tran
                            iCPeriodId = objCPeriod.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iCPeriodId
                            If objPeriod._End_Date < objEmp._Appointeddate Then
                                iCPeriodId = objCPeriod.getCurrentPeriodID(enModuleReference.Payroll, objEmp._Appointeddate, FinancialYear._Object._YearUnkid, enStatusType.Open)
                            End If
                            If objED.isExist(FinancialYear._Object._DatabaseName, objEmp._Employeeunkid(objPeriod._End_Date), objEmp._Tranhedunkid, , iCPeriodId) = True Then
                                dsData = objSal.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "List", Nothing, CStr(objEmp._Employeeunkid(objPeriod._End_Date)), iCPeriodId, FinancialYear._Object._YearUnkid)
                                If dsData.Tables("List").Rows.Count > 0 Then
                                    dRow = dsData.Tables("List").Select("periodunkid > " & iCPeriodId)
                                    If dRow.Length > 0 Then
                                        dtRow.Item("image") = imgError
                                        dtRow.Item("message") = Language.getMessage("frmEmployeeMaster", 82, "Sorry, you cannot change the transaction head. Reason there are some increment done for the future period. Please delete them first.")
                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                                        dtRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                    End If
                                End If
                                dsData = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EList", , objEmp._Employeeunkid(objPeriod._End_Date).ToString, , , , objEmp._Tranhedunkid)
                                If dsData.Tables("EList").Rows.Count > 0 Then
                                    dRow = dsData.Tables("EList").Select("end_date > '" & eZeeDate.convertDate(objPeriod._End_Date) & "'")
                                    If dRow.Length > 0 Then
                                        dtRow.Item("image") = imgError
                                        dtRow.Item("message") = Language.getMessage("frmEmployeeMaster", 83, "Sorry, you cannot change the transaction head. Reason there are some heads posted for the future period. Please delete them first.")
                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                                        dtRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                If dtRow.Item("email").ToString.Trim.Trim.Length > 0 Then
                    objEmp._Email = dtRow.Item("email").ToString
                End If

                If dtRow.Item("displayname").ToString.Trim.Length > 0 Then
                    objEmp._Displayname = dtRow.Item("displayname").ToString
                End If

                If IsDBNull(dtRow.Item("birthdate")) = False AndAlso dtRow.Item("birthdate").ToString.Trim.Length > 0 Then
                    If CDate(dtRow.Item("birthdate")) <> Nothing Then
                        objEmp._Birthdate = CDate(dtRow.Item("birthdate"))
                    End If
                End If

                'S.SANDEEP |02-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                'If intGenderId >= 0 Then
                '    objEmp._Gender = intGenderId
                'End If

                If intGenderId > 0 Then
                    objEmp._Gender = intGenderId
                End If
                'S.SANDEEP |02-MAR-2020| -- END
                

                If iTranHeadUnkId > 0 Then
                    objEmp._Tranhedunkid = iTranHeadUnkId
                    objEmp._TrnHeadTypeId = iTranHeadTypeID
                End If

                If intPayTypeUnkid > 0 Then
                    objEmp._Paytypeunkid = intPayTypeUnkid
                End If

                If intPayPointUnkid > 0 Then
                    objEmp._Paypointunkid = intPayPointUnkid
                End If

                If intEmploymenttypeId > 0 Then
                    objEmp._Employmenttypeunkid = intEmploymenttypeId
                End If

                If dtRow("firstname").ToString().Trim.Length > 0 Then
                    objEmp._Firstname = dtRow("firstname").ToString().Trim
                End If

              

                If dtRow("surname").ToString().Trim.Length > 0 Then
                    objEmp._Surname = dtRow("surname").ToString().Trim
                End If

                If dtRow("password").ToString().Trim.Length > 0 Then
                    objEmp._Password = dtRow("password").ToString().Trim
                End If

                If dtRow("ncode").ToString().Trim.Length > 0 Then
                    objEmp._Employeecode = dtRow("ncode").ToString().Trim
                End If


                'Gajanan [13-May-2020] -- Start
                'Enhancement:Add New Field In Bulk Employee Import
                If IsDBNull(dtRow.Item("firstappointmentdate")) = False AndAlso dtRow.Item("firstappointmentdate").ToString.Trim.Length > 0 Then
                    If CDate(dtRow.Item("firstappointmentdate")) <> Nothing Then
                        objEmp._FirstAppointmentdate = CDate(dtRow.Item("firstappointmentdate"))
                    End If
                End If

                If dtRow("middlename").ToString().Trim.Length > 0 Then
                    objEmp._Othername = dtRow("middlename").ToString().Trim
                End If
                'Gajanan [13-May-2020] -- End



                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEmp.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, _
                '                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , , , , , , , , , , , _
                '                 getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then

                If objEmp.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, _
                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, , , , , , , , , , , , , _
                                 getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then
                    'Pinkal (18-Aug-2018) -- End


                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 18, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objEmp._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub EnableControl()
        Try
            Dim lstcheckbox As List(Of Control) = (From p In gbDatatoUpdate.Controls.Cast(Of Control)() Where (TypeOf p Is CheckBox) Select (p)).ToList
            For Each ctrl As Control In lstcheckbox
                Select Case ctrl.Name.ToUpper
                    Case chkEmployeeCode.Name.ToUpper
                        objlblSign26.Enabled = chkEmployeeCode.Checked
                        cboNewEmployeeCode.Enabled = chkEmployeeCode.Checked
                    Case chkFirstname.Name.ToUpper
                        objlblSign27.Enabled = chkFirstname.Checked
                        cboFirstname.Enabled = chkFirstname.Checked
                    Case chkSurname.Name.ToUpper
                        objlblSign28.Enabled = chkSurname.Checked
                        cboSurname.Enabled = chkSurname.Checked
                    Case chkPassword.Name.ToUpper
                        objlblSign29.Enabled = chkPassword.Checked
                        cboPassword.Enabled = chkPassword.Checked
                    Case chkEmploymentType.Name.ToUpper
                        objlblSign30.Enabled = chkEmploymentType.Checked
                        cboEmploymentType.Enabled = chkEmploymentType.Checked
                    Case chkGender.Name.ToUpper
                        objlblSign9.Enabled = chkGender.Checked
                        cboGender.Enabled = chkGender.Checked
                    Case chkDisplayName.Name.ToUpper
                        objlblSign3.Enabled = chkDisplayName.Checked
                        cboDisplayname.Enabled = chkDisplayName.Checked
                    Case chkEmail.Name.ToUpper
                        objlblSign2.Enabled = chkEmail.Checked
                        cboEmail.Enabled = chkEmail.Checked
                    Case chkBirthdate.Name.ToUpper
                        objlblSign10.Enabled = chkBirthdate.Checked
                        cboBirthdate.Enabled = chkBirthdate.Checked
                    Case chkTransctionHead.Name.ToUpper
                        objlblSign23.Enabled = chkTransctionHead.Checked
                        cboTransactionHead.Enabled = chkTransctionHead.Checked
                    Case chkPayType.Name.ToUpper
                        objlblSign24.Enabled = chkPayType.Checked
                        cboPayType.Enabled = chkPayType.Checked
                    Case chkPayPoint.Name.ToUpper
                        objlblSign25.Enabled = chkPayPoint.Checked
                        cboPayPoint.Enabled = chkPayPoint.Checked

                        'Gajanan [23-April-2020] -- Start
                        'Enhancement:Missing AT For Grievance Module
                    Case chkMiddlename.Name.ToUpper
                        objlblSign31.Enabled = chkMiddlename.Checked
                        cboMiddlename.Enabled = chkMiddlename.Checked

                    Case chkFirstappointmentDate.Name.ToUpper
                        objlblSign32.Enabled = chkFirstappointmentDate.Checked
                        cboAppointmentDate.Enabled = chkFirstappointmentDate.Checked
                        'Gajanan [23-April-2020] -- End


                End Select
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControl", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ComboBox Event "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Enabled = True Then
                            If cr.Name <> cmb.Name Then
                                If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    cmb.SelectedIndex = -1
                                    cmb.Select()
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = CType(dgData.DataSource, DataView).ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Update Employee Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDatatoUpdate.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDatatoUpdate.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.WizUpdateEmployee.CancelText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_CancelText", Me.WizUpdateEmployee.CancelText)
            Me.WizUpdateEmployee.NextText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_NextText", Me.WizUpdateEmployee.NextText)
            Me.WizUpdateEmployee.BackText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_BackText", Me.WizUpdateEmployee.BackText)
            Me.WizUpdateEmployee.FinishText = Language._Object.getCaption(Me.WizUpdateEmployee.Name & "_FinishText", Me.WizUpdateEmployee.FinishText)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.gbDatatoUpdate.Text = Language._Object.getCaption(Me.gbDatatoUpdate.Name, Me.gbDatatoUpdate.Text)
            Me.chkDisplayName.Text = Language._Object.getCaption(Me.chkDisplayName.Name, Me.chkDisplayName.Text)
            Me.chkEmail.Text = Language._Object.getCaption(Me.chkEmail.Name, Me.chkEmail.Text)
            Me.elOthers.Text = Language._Object.getCaption(Me.elOthers.Name, Me.elOthers.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblDisplayname.Text = Language._Object.getCaption(Me.lblDisplayname.Name, Me.lblDisplayname.Text)
            Me.chkBirthdate.Text = Language._Object.getCaption(Me.chkBirthdate.Name, Me.chkBirthdate.Text)
            Me.chkGender.Text = Language._Object.getCaption(Me.chkGender.Name, Me.chkGender.Text)
            Me.lblBirthdate.Text = Language._Object.getCaption(Me.lblBirthdate.Name, Me.lblBirthdate.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.chkTransctionHead.Text = Language._Object.getCaption(Me.chkTransctionHead.Name, Me.chkTransctionHead.Text)
            Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.chkPayType.Text = Language._Object.getCaption(Me.chkPayType.Name, Me.chkPayType.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.chkPayPoint.Text = Language._Object.getCaption(Me.chkPayPoint.Name, Me.chkPayPoint.Text)
            Me.lblPayPoint.Text = Language._Object.getCaption(Me.lblPayPoint.Name, Me.lblPayPoint.Text)
            Me.chkFirstname.Text = Language._Object.getCaption(Me.chkFirstname.Name, Me.chkFirstname.Text)
            Me.chkEmployeeCode.Text = Language._Object.getCaption(Me.chkEmployeeCode.Name, Me.chkEmployeeCode.Text)
            Me.chkEmploymentType.Text = Language._Object.getCaption(Me.chkEmploymentType.Name, Me.chkEmploymentType.Text)
            Me.chkPassword.Text = Language._Object.getCaption(Me.chkPassword.Name, Me.chkPassword.Text)
            Me.chkSurname.Text = Language._Object.getCaption(Me.chkSurname.Name, Me.chkSurname.Text)
            Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
            Me.lblEmploymentType.Text = Language._Object.getCaption(Me.lblEmploymentType.Name, Me.lblEmploymentType.Text)
            Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
            Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.Name, Me.lblSurname.Text)
            Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
            Me.lblNewEmployeeCode.Text = Language._Object.getCaption(Me.lblNewEmployeeCode.Name, Me.lblNewEmployeeCode.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
            Me.chkMiddlename.Text = Language._Object.getCaption(Me.chkMiddlename.Name, Me.chkMiddlename.Text)
            Me.chkFirstappointmentDate.Text = Language._Object.getCaption(Me.chkFirstappointmentDate.Name, Me.chkFirstappointmentDate.Text)
            Me.lblMiddlename.Text = Language._Object.getCaption(Me.lblMiddlename.Name, Me.lblMiddlename.Text)
            Me.lblAppointmentDate.Text = Language._Object.getCaption(Me.chkFirstname.Name, Me.lblAppointmentDate.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage("frmEmployeeMaster", 82, "Sorry, you cannot change the transaction head. Reason there are some increment done for the future period. Please delete them first.")
            Language.setMessage("frmEmployeeMaster", 83, "Sorry, you cannot change the transaction head. Reason there are some heads posted for the future period. Please delete them first.")
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue.")
            Language.setMessage(mstrModuleName, 4, "Email is mandatory information. Please select Email to continue.")
            Language.setMessage(mstrModuleName, 5, "Display Name is mandatory information. Please select Display Name to continue.")
            Language.setMessage(mstrModuleName, 12, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 13, "Fail")
            Language.setMessage(mstrModuleName, 14, "Invalid Email.")
            Language.setMessage(mstrModuleName, 18, "Success")
            Language.setMessage(mstrModuleName, 19, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 21, "No update information selected to update. Please select atleast one update information.")
            Language.setMessage(mstrModuleName, 22, "Birthdate is mandatory information. Please select Birthdate to continue.")
            Language.setMessage(mstrModuleName, 23, "Birthdate format is not correct. Please set correct Birthdate format  to continue.")
            Language.setMessage(mstrModuleName, 28, "Gender is mandatory information. Please select Gender to continue.")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "is already Mapped with")
            Language.setMessage(mstrModuleName, 51, "Pay Type Not Found.")
            Language.setMessage(mstrModuleName, 52, "Transaction Head Code Not Found.")
            Language.setMessage(mstrModuleName, 53, "Pay Point is mandatory information. Please select Pay Point to continue.")
            Language.setMessage(mstrModuleName, 54, "Pay Point Not Found.")
            Language.setMessage(mstrModuleName, 55, "New Employeecode is mandatory information. Please select New Employeecode to continue.")
            Language.setMessage(mstrModuleName, 56, "Fisrtname is mandatory information. Please select Fisrtname to continue.")
            Language.setMessage(mstrModuleName, 57, "Password is mandatory information. Please select Password to continue.")
            Language.setMessage(mstrModuleName, 58, "Employment Type is mandatory information. Please select Employment Type to continue.")
            Language.setMessage(mstrModuleName, 59, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 60, "Please check atleast one information to update and get file update format.")
            Language.setMessage(mstrModuleName, 61, "Employment Type Not Found.")
            Language.setMessage(mstrModuleName, 62, "Transaction Head Code is mandatory information. Please select Tran. Head Code to continue.")
            Language.setMessage(mstrModuleName, 63, "Pay Type is mandatory information. Please select Pay Type to continue.")
            Language.setMessage(mstrModuleName, 64, "Surname is mandatory information. Please select Surname to continue.")
			Language.setMessage(mstrModuleName, 65, "Gender data in file is not vaild.")
			Language.setMessage(mstrModuleName, 66, "NewEmployeeCode")
			Language.setMessage(mstrModuleName, 67, "Firstname")
			Language.setMessage(mstrModuleName, 68, "Surname")
			Language.setMessage(mstrModuleName, 69, "Password")
			Language.setMessage(mstrModuleName, 70, "EmploymentType")
			Language.setMessage(mstrModuleName, 71, "Gender")
			Language.setMessage(mstrModuleName, 72, "Displayname")
			Language.setMessage(mstrModuleName, 73, "Email")
			Language.setMessage(mstrModuleName, 74, "Birthdate")
			Language.setMessage(mstrModuleName, 75, "TransactionHead")
			Language.setMessage(mstrModuleName, 76, "PayType")
			Language.setMessage(mstrModuleName, 77, "PayPoint")
			Language.setMessage(mstrModuleName, 78, "EmployeeCode")
            Language.setMessage(mstrModuleName, 79, "Middlename")
            Language.setMessage(mstrModuleName, 80, "FirstAppointmentDate")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'S.SANDEEP [14-JUN-2018] -- START {NMB} -- END
'
'
'
'Public Class frmBulkUpdateDataWizard
'    Private mstrModuleName As String = "frmBulkUpdateDataWizard"

'#Region " Private Variables "

'    Private mds_ImportData As DataSet
'    Private m_dsImportData_eZee As DataSet
'    Private mdt_ImportData_Others As New DataTable
'    Private dvGriddata As DataView = Nothing
'    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
'    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
'    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
'    'S.SANDEEP [ 11 MAR 2014 ] -- START
'    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
'    'Private Expression As New System.Text.RegularExpressions.Regex(strExpression)
'    Private Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
'    'S.SANDEEP [ 11 MAR 2014 ] -- END

'#End Region

'#Region " From's Events "

'    Private Sub frmBulkUpdateData_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            txtFilePath.BackColor = GUI.ColorComp

'            'Anjan [12 August 2015] -- Start
'            'ENHANCEMENT : this feature was overlapping configuration setting and was updating displayname given from file.
'            If ConfigParameter._Object._DisplayNameSetting > 0 Then
'                chkDisplayName.Checked = False
'                chkDisplayName.Enabled = False
'            End If
'            'Anjan [12 August 2015] -- End


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmBulkUpdateData_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button Event "

'    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
'        Try
'            Dim objFileOpen As New OpenFileDialog
'            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

'            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
'                txtFilePath.Text = objFileOpen.FileName
'            End If

'            objFileOpen = Nothing

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " eZee Wizard "

'    Private Sub WizUpdateEmployee_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizUpdateEmployee.AfterSwitchPages
'        Try
'            Select Case e.NewIndex
'                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
'                    Call CreateDataTable()
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_AfterSwitchPages", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub WizUpdateEmployee_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizUpdateEmployee.BeforeSwitchPages
'        Try
'            Select Case e.OldIndex
'                Case WizUpdateEmployee.Pages.IndexOf(wizPageFile)
'                    If Not System.IO.File.Exists(txtFilePath.Text) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                        e.Cancel = True
'                        Exit Sub
'                    End If

'                    'Shani [ 09 DEC 2014 ] -- START
'                    'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                    'If chkClass.Checked = False AndAlso chkClassGrp.Checked = False AndAlso _
'                    'chkDepartment.Checked = False AndAlso chkDisplayName.Checked = False AndAlso _
'                    'chkEmail.Checked = False AndAlso chkJob.Checked = False AndAlso chkRetirementDt.Checked = False Then
'                    If chkClass.Checked = False AndAlso chkClassGrp.Checked = False AndAlso _
'                       chkDepartment.Checked = False AndAlso chkDisplayName.Checked = False AndAlso _
'                       chkEmail.Checked = False AndAlso chkJob.Checked = False AndAlso chkRetirementDt.Checked = False AndAlso _
'                       chkGender.Checked = False AndAlso chkBirthdate.Checked = False AndAlso chkEOCDate.Checked = False AndAlso chkLeavingDate.Checked = False AndAlso _
'                        chkBranch.Checked = False AndAlso chkDepGroup.Checked = False AndAlso chkSecGroup.Checked = False AndAlso chkSections.Checked = False AndAlso _
'                         chkUnitGroup.Checked = False AndAlso chkUnit.Checked = False AndAlso chkTeam.Checked = False AndAlso chkJobGroup.Checked = False AndAlso chkCostCenter.Checked = False _
'                         AndAlso chkTransctionHead.Checked = False AndAlso chkPayType.Checked = False AndAlso chkPayPoint.Checked = False Then
'                        'Nilay (17 Feb 2017) -- [chkPayPoint.Checked = False]
'                        'Shani(09-Sep-2015) -- [chkTransctionHead]
'                        'Shani [ 09 DEC 2014 ] -- END
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "No update information selected to update. Please select atleast one update information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                        e.Cancel = True
'                        Exit Sub
'                    End If

'                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

'                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
'                        mds_ImportData = New DataSet
'                        Using iCSV As New clsCSVData
'                            iCSV.LoadCSV(txtFilePath.Text, True)
'                            mds_ImportData = iCSV.CSVDataSet.Copy
'                        End Using
'                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
'                        'S.SANDEEP [12-Jan-2018] -- START
'                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
'                        'Dim iExcelData As New ExcelData
'                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
'                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
'                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
'                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
'                        'S.SANDEEP [12-Jan-2018] -- END
'                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
'                        mds_ImportData = New DataSet
'                        mds_ImportData.ReadXml(txtFilePath.Text)
'                    Else
'                        e.Cancel = True
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                        Exit Sub
'                    End If

'                    Call SetDataCombo()
'                Case WizUpdateEmployee.Pages.IndexOf(wizPageMapping)
'                    If e.NewIndex > e.OldIndex Then
'                        For Each ctrl As Control In gbFiledMapping.Controls
'                            If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
'                                If CType(ctrl, ComboBox).Text = "" Then
'                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                                    e.Cancel = True
'                                    Exit For
'                                End If
'                            End If
'                        Next
'                    End If
'                Case WizUpdateEmployee.Pages.IndexOf(wizPageData)
'                    Me.Close()
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "WizUpdateEmployee_BeforeSwitchPages", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private Function "

'    Private Sub SetDataCombo()
'        Try
'            For Each ctrl As Control In gbFiledMapping.Controls
'                If TypeOf ctrl Is ComboBox Then
'                    Call ClearCombo(CType(ctrl, ComboBox))
'                End If
'            Next
'            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
'                cboClass.Items.Add(dtColumns.ColumnName)
'                cboClassGrp.Items.Add(dtColumns.ColumnName)
'                cboDepartment.Items.Add(dtColumns.ColumnName)
'                cboDisplayname.Items.Add(dtColumns.ColumnName)
'                cboEmail.Items.Add(dtColumns.ColumnName)
'                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
'                cboJob.Items.Add(dtColumns.ColumnName)
'                cboRetirementDt.Items.Add(dtColumns.ColumnName)
'                'Shani [ 09 DEC 2014 ] -- START
'                'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                cboGender.Items.Add(dtColumns.ColumnName)
'                cboEOCDate.Items.Add(dtColumns.ColumnName)
'                cboBirthdate.Items.Add(dtColumns.ColumnName)
'                cboLeavingDate.Items.Add(dtColumns.ColumnName)

'                cboBranch.Items.Add(dtColumns.ColumnName)
'                cboDepGroup.Items.Add(dtColumns.ColumnName)
'                cboSecGroup.Items.Add(dtColumns.ColumnName)
'                cboSections.Items.Add(dtColumns.ColumnName)
'                cboUnitGroup.Items.Add(dtColumns.ColumnName)
'                cboUnit.Items.Add(dtColumns.ColumnName)
'                cboTeam.Items.Add(dtColumns.ColumnName)
'                cboJobGroup.Items.Add(dtColumns.ColumnName)
'                cboCostCenter.Items.Add(dtColumns.ColumnName)
'                'Shani [ 09 DEC 2014 ] -- END


'                'Pinkal (12-Mar-2015) -- Start
'                'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'                cboEOCReason.Items.Add(dtColumns.ColumnName)
'                'Pinkal (12-Mar-2015) -- End


'                'Shani(09-Sep-2015) -- Start
'                'ENHANCEMENT : Add Transaction Head
'                cboTransactionHead.Items.Add(dtColumns.ColumnName)
'                'Shani(09-Sep-2015) -- End

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                cboPayType.Items.Add(dtColumns.ColumnName)
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                cboPayPoint.Items.Add(dtColumns.ColumnName)
'                'Nilay (17 Feb 2017) -- End

'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
'        Try
'            cboComboBox.Items.Clear()
'            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub CreateDataTable()
'        Try
'            Dim blnIsNotThrown As Boolean = True
'            ezWait.Active = True

'            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("email", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("dname", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("rdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
'            mdt_ImportData_Others.Columns.Add("deptname", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("job", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("clsgrp", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("class", System.Type.GetType("System.String")).DefaultValue = ""

'            'Shani [ 09 DEC 2014 ] -- START
'            'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'            mdt_ImportData_Others.Columns.Add("birthdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
'            mdt_ImportData_Others.Columns.Add("gender", System.Type.GetType("System.String")).DefaultValue = -1
'            mdt_ImportData_Others.Columns.Add("eocdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing

'            'Pinkal (12-Mar-2015) -- Start
'            'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'            mdt_ImportData_Others.Columns.Add("eocreason", System.Type.GetType("System.String")).DefaultValue = ""
'            'Pinkal (12-Mar-2015) -- End

'            mdt_ImportData_Others.Columns.Add("leavingdate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing

'            mdt_ImportData_Others.Columns.Add("branch", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("departmentgroup", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("secgroup", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("sections", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("unitgroup", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("unit", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("team", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("jobgroup", System.Type.GetType("System.String")).DefaultValue = ""
'            mdt_ImportData_Others.Columns.Add("costcenter", System.Type.GetType("System.String")).DefaultValue = ""
'            'Shani [ 09 DEC 2014 ] -- END


'            'Shani(09-Sep-2015) -- Start
'            'ENHANCEMENT : Add Transaction Head
'            mdt_ImportData_Others.Columns.Add("tranheadcode", System.Type.GetType("System.String")).DefaultValue = ""
'            'Shani(09-Sep-2015) -- End

'            'Shani(17-Nov-2015) -- Start
'            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'            mdt_ImportData_Others.Columns.Add("paytype", System.Type.GetType("System.String")).DefaultValue = ""
'            'Shani(17-Nov-2015) -- End

'            'Nilay (17 Feb 2017) -- Start
'            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'            mdt_ImportData_Others.Columns.Add("paypoint", System.Type.GetType("System.String")).DefaultValue = ""
'            'Nilay (17 Feb 2017) -- End

'            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
'            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
'            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
'            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

'            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
'            For i As Integer = 0 To dtTemp.Length - 1
'                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
'            Next
'            mds_ImportData.AcceptChanges()

'            Dim drNewRow As DataRow

'            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
'                blnIsNotThrown = CheckInvalidData(dtRow)
'                If blnIsNotThrown = False Then Exit Sub
'                drNewRow = mdt_ImportData_Others.NewRow
'                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

'                If cboEmail.Enabled = True Then
'                    drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString.Trim
'                End If

'                If cboDisplayname.Enabled = True Then
'                    drNewRow.Item("dname") = dtRow.Item(cboDisplayname.Text).ToString.Trim
'                End If

'                If cboRetirementDt.Enabled = True Then
'                    drNewRow.Item("rdate") = dtRow.Item(cboRetirementDt.Text).ToString.Trim
'                End If

'                If cboDepartment.Enabled = True Then
'                    drNewRow.Item("deptname") = dtRow.Item(cboDepartment.Text).ToString.Trim
'                End If

'                If cboJob.Enabled = True Then
'                    drNewRow.Item("job") = dtRow.Item(cboJob.Text).ToString.Trim
'                End If

'                If cboClassGrp.Enabled = True Then
'                    drNewRow.Item("clsgrp") = dtRow.Item(cboClassGrp.Text).ToString.Trim
'                End If

'                If cboClass.Enabled = True Then
'                    drNewRow.Item("class") = dtRow.Item(cboClass.Text).ToString.Trim
'                End If

'                'Shani [ 09 DEC 2014 ] -- START
'                'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                If cboGender.Enabled = True Then
'                    drNewRow.Item("gender") = dtRow.Item(cboGender.Text).ToString.Trim
'                End If
'                If cboBirthdate.Enabled = True Then
'                    drNewRow.Item("birthdate") = dtRow.Item(cboBirthdate.Text).ToString.Trim
'                End If
'                If cboEOCDate.Enabled = True Then
'                    drNewRow.Item("eocdate") = dtRow.Item(cboEOCDate.Text).ToString.Trim

'                    'Pinkal (12-Mar-2015) -- Start
'                    'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'                    drNewRow.Item("eocreason") = dtRow.Item(cboEOCReason.Text).ToString.Trim
'                    'Pinkal (12-Mar-2015) -- End

'                End If
'                If cboLeavingDate.Enabled = True Then
'                    drNewRow.Item("leavingdate") = dtRow.Item(cboLeavingDate.Text).ToString.Trim
'                End If

'                If cboBranch.Enabled = True Then
'                    drNewRow.Item("branch") = dtRow.Item(cboBranch.Text).ToString.Trim
'                End If
'                If cboDepGroup.Enabled = True Then
'                    drNewRow.Item("departmentgroup") = dtRow.Item(cboDepGroup.Text).ToString.Trim
'                End If
'                If cboSecGroup.Enabled = True Then
'                    drNewRow.Item("secgroup") = dtRow.Item(cboSecGroup.Text).ToString.Trim
'                End If
'                If cboSections.Enabled = True Then
'                    drNewRow.Item("sections") = dtRow.Item(cboSections.Text).ToString.Trim
'                End If
'                If cboUnitGroup.Enabled = True Then
'                    drNewRow.Item("unitgroup") = dtRow.Item(cboUnitGroup.Text).ToString.Trim
'                End If
'                If cboUnit.Enabled = True Then
'                    drNewRow.Item("unit") = dtRow.Item(cboUnit.Text).ToString.Trim
'                End If
'                If cboTeam.Enabled = True Then
'                    drNewRow.Item("team") = dtRow.Item(cboTeam.Text).ToString.Trim
'                End If
'                If cboJobGroup.Enabled = True Then
'                    drNewRow.Item("jobgroup") = dtRow.Item(cboJobGroup.Text).ToString.Trim
'                End If
'                If cboCostCenter.Enabled = True Then
'                    drNewRow.Item("costcenter") = dtRow.Item(cboCostCenter.Text).ToString.Trim
'                End If

'                'Shani [ 09 DEC 2014 ] -- END

'                'Shani(09-Sep-2015) -- Start
'                'ENHANCEMENT : Add Transaction Head
'                If cboTransactionHead.Enabled = True Then
'                    drNewRow.Item("tranheadcode") = dtRow.Item(cboTransactionHead.Text).ToString.Trim
'                End If
'                'Shani(09-Sep-2015) -- End

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                If cboPayType.Enabled = True Then
'                    drNewRow.Item("paytype") = dtRow.Item(cboPayType.Text).ToString.Trim
'                End If
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                If cboPayPoint.Enabled = True Then
'                    drNewRow.Item("paypoint") = dtRow.Item(cboPayPoint.Text).ToString.Trim
'                End If
'                'Nilay (17 Feb 2017) -- End

'                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
'                drNewRow.Item("Message") = ""
'                drNewRow.Item("Status") = ""
'                drNewRow.Item("objStatus") = ""

'                mdt_ImportData_Others.Rows.Add(drNewRow)
'                objTotal.Text = CStr(Val(objTotal.Text) + 1)
'            Next


'            If blnIsNotThrown = True Then
'                colhEmployee.DataPropertyName = "ecode"
'                colhMessage.DataPropertyName = "message"
'                objcolhImage.DataPropertyName = "image"
'                colhStatus.DataPropertyName = "status"
'                objcolhstatus.DataPropertyName = "objStatus"
'                dgData.AutoGenerateColumns = False
'                dvGriddata = New DataView(mdt_ImportData_Others)
'                dgData.DataSource = dvGriddata
'            End If

'            Call Import_Data()

'            ezWait.Active = False
'            WizUpdateEmployee.BackEnabled = False
'            WizUpdateEmployee.CancelText = "Finish"

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
'        Finally
'            ezWait.Active = False
'        End Try
'    End Sub

'    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
'        Try
'            With dtRow
'                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
'                    Return False
'                End If

'                If cboEmail.Enabled = True Then
'                    If .Item(cboEmail.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Email is mandatory information. Please select Email to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboDisplayname.Enabled = True Then
'                    If .Item(cboDisplayname.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Display Name is mandatory information. Please select Display Name to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboRetirementDt.Enabled = True Then
'                    If .Item(cboRetirementDt.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Retirement Date is mandatory information. Please select Retirement Date to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                    If IsDate(.Item(cboRetirementDt.Text).ToString.Trim) = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Retirement Date format is not correct. Please set correct Retirement Date format  to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboDepartment.Enabled = True Then
'                    If .Item(cboDepartment.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Department is mandatory information. Please select Department to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboJob.Enabled = True Then
'                    If .Item(cboJob.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Job is mandatory information. Please select Job to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboClassGrp.Enabled = True Then
'                    If .Item(cboClassGrp.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Class Group is mandatory information. Please select Class Group to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboClass.Enabled = True Then
'                    If .Item(cboClass.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Class is mandatory information. Please select Class to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                'Shani [ 09 DEC 2014 ] -- START
'                'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                If cboBirthdate.Enabled = True Then
'                    If .Item(cboBirthdate.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Birthdate is mandatory information. Please select Birthdate to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                    If IsDate(.Item(cboBirthdate.Text).ToString.Trim) = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Birthdate format is not correct. Please set correct Birthdate format  to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboEOCDate.Enabled = True Then
'                    If .Item(cboEOCDate.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "EOC Date is mandatory information. Please select EOC Date to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                    If IsDate(.Item(cboEOCDate.Text).ToString.Trim) = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "EOC Date format is not correct. Please set correct EOC Date format  to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If

'                    'Pinkal (12-Mar-2015) -- Start
'                    'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.

'                    If .Item(cboEOCReason.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "EOC Reason is mandatory information. Please select EOC Reason to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If

'                    'Pinkal (12-Mar-2015) -- End

'                End If

'                If cboLeavingDate.Enabled = True Then
'                    If .Item(cboLeavingDate.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Leaving Date is mandatory information. Please select Leaving Date to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                    If IsDate(.Item(cboLeavingDate.Text).ToString.Trim) = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Leaving Date format is not correct. Please set correct Leaving Date format  to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboGender.Enabled = True Then
'                    If .Item(cboGender.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Gender is mandatory information. Please select Gender to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If


'                If cboBranch.Enabled = True Then
'                    If .Item(cboBranch.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Branch is mandatory information. Please select Branch to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboDepGroup.Enabled = True Then
'                    If .Item(cboDepGroup.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Department Group is mandatory information. Please select Department Group to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If

'                If cboSecGroup.Enabled = True Then
'                    If .Item(cboSecGroup.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sections Group is mandatory information. Please select Sections Group to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboSections.Enabled = True Then
'                    If .Item(cboSections.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sections is mandatory information. Please select Sections to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboUnitGroup.Enabled = True Then
'                    If .Item(cboUnitGroup.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Unit Group is mandatory information. Please select Unit Group to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboUnit.Enabled = True Then
'                    If .Item(cboUnit.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Unit is mandatory information. Please select Unit to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboTeam.Enabled = True Then
'                    If .Item(cboTeam.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Team is mandatory information. Please select Team to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboJobGroup.Enabled = True Then
'                    If .Item(cboJobGroup.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Job Group is mandatory information. Please select Job Group to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                If cboCostCenter.Enabled = True Then
'                    If .Item(cboCostCenter.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                'Shani [ 09 DEC 2014 ] -- END

'                'Shani(09-Sep-2015) -- Start
'                'ENHANCEMENT : Add Transaction Head
'                If cboTransactionHead.Enabled = True Then
'                    If .Item(cboTransactionHead.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Transaction Head Code is mandatory information. Please select Tran. Head Code to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                'Shani(09-Sep-2015) -- End

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                If cboPayType.Enabled = True Then
'                    If .Item(cboPayType.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Pay Type is mandatory information. Please select Pay Type to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                If cboPayPoint.Enabled = True Then
'                    If .Item(cboPayPoint.Text).ToString.Trim.Length <= 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 53, "Pay Point is mandatory information. Please select Pay Point to continue."), enMsgBoxStyle.Information)
'                        Return False
'                    End If
'                End If
'                'Nilay (17 Feb 2017) -- End

'            End With
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Sub Import_Data()
'        Try
'            btnFilter.Enabled = False
'            If mdt_ImportData_Others.Rows.Count <= 0 Then
'                Exit Sub
'            End If

'            Dim objEmp As New clsEmployee_Master : Dim objDept As New clsDepartment
'            Dim objJob As New clsJobs : Dim objClsGrp As New clsClassGroup
'            Dim objClass As New clsClass

'            Dim iEmpId As Integer : Dim iDeptId As Integer
'            Dim iJobId As Integer : Dim iClsGrpId As Integer
'            Dim iClsId As Integer

'            'Shani [ 09 DEC 2014 ] -- START
'            'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'            Dim intGenderId As Integer = -1

'            Dim iBranch As Integer : Dim iDeptGrp As Integer : Dim iSecGrp As Integer
'            Dim iSections As Integer : Dim iUnitGrp As Integer : Dim iUnit As Integer
'            Dim iTeam As Integer : Dim iJobgrp As Integer : Dim iCostCntr As Integer

'            Dim objBranch As New clsStation : Dim objDeptGrp As New clsDepartmentGroup
'            Dim objSecGrp As New clsSectionGroup : Dim objSection As New clsSections
'            Dim objUnitGrp As New clsUnitGroup : Dim objUnit As New clsUnits
'            Dim objTeam As New clsTeams : Dim objJobGrp As New clsJobGroup
'            Dim objCostCenter As New clscostcenter_master
'            'Shani [ 09 DEC 2014 ] -- END

'            'Shani(09-Sep-2015) -- Start
'            'ENHANCEMENT : Add Transaction Head
'            Dim iTranHeadUnkId As Integer
'            Dim iTranHeadTypeID As Integer
'            Dim objTranHead As New clsTransactionHead
'            'Shani(09-Sep-2015) -- End


'            'Shani(17-Nov-2015) -- Start
'            'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'            Dim intPayTypeUnkid As Integer = -1
'            Dim objCMaster As New clsCommon_Master
'            'Shani(17-Nov-2015) -- End

'            'Nilay (17 Feb 2017) -- Start
'            'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'            Dim intPayPointUnkid As Integer = -1
'            Dim objPayPoint As New clspaypoint_master
'            'Nilay (17 Feb 2017) -- End

'            'Pinkal (12-Mar-2015) -- Start
'            'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'            Dim objReason As New clsAction_Reason
'            Dim iReasonId As Integer = 0
'            'Pinkal (12-Mar-2015) -- End


'            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
'                iEmpId = 0 : iDeptId = 0 : iJobId = 0 : iClsGrpId = 0 : iClsId = 0

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                intPayTypeUnkid = 0
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                intPayPointUnkid = 0
'                'Nilay (17 Feb 2017) -- End

'                Try
'                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
'                    Application.DoEvents()
'                    ezWait.Refresh()
'                Catch ex As Exception
'                End Try

'                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
'                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
'                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
'                    If iEmpId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF EMAIL IS VALID.
'                If dtRow.Item("email").ToString.Trim.Length > 0 Then
'                    If Expression.IsMatch(dtRow.Item("email").ToString.Trim) = False Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Invalid Email.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF DEPARTMENT PRESENT.
'                If dtRow.Item("deptname").ToString.Trim.Length > 0 Then
'                    iDeptId = objDept.GetDepartmentUnkId(dtRow.Item("deptname").ToString.Trim)
'                    If iDeptId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Department Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF JOB PRESENT.
'                If dtRow.Item("job").ToString.Trim.Length > 0 Then
'                    iJobId = objJob.GetJobUnkId(dtRow.Item("job").ToString.Trim)
'                    If iJobId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Job Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF CLASS GROUP PRESENT.
'                If dtRow.Item("clsgrp").ToString.Trim.Length > 0 Then
'                    iClsGrpId = objClsGrp.GetClassGrpUnkId(dtRow.Item("clsgrp").ToString.Trim)
'                    If iClsGrpId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Class Group Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF CLASS GROUP PRESENT.
'                If dtRow.Item("class").ToString.Trim.Length > 0 Then
'                    iClsId = objClass.GetClassUnkId(dtRow.Item("class").ToString.Trim)
'                    If iClsId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Class Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If


'                'Shani [ 09 DEC 2014 ] -- START
'                'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                '------------------------------ CHECKING IF Gender PRESENT.

'                'Shani(24-Aug-2015) -- Start
'                'Changes : 
'                'If dtRow.Item("gender").ToString.Trim.Length > 0 Then
'                If dtRow.Item("gender").ToString.Trim.Length > 0 AndAlso dtRow.Item("gender").ToString.Trim <> "-1" Then
'                    'Shani(24-Aug-2015) -- End
'                    Dim strMale As String = Language.getMessage("clsMasterData", 73, "Male")
'                    Dim strFemale As String = Language.getMessage("clsMasterData", 74, "Female")
'                    Select Case dtRow.Item("gender").ToString.Trim.ToUpper
'                        Case strMale.ToUpper
'                            intGenderId = 1
'                        Case strFemale.ToUpper
'                            intGenderId = 2
'                        Case Else
'                            intGenderId = 0
'                    End Select
'                End If
'                '------------------------------ CHECKING IF Branch PRESENT.
'                If dtRow.Item("branch").ToString.Trim.Length > 0 Then
'                    iBranch = objBranch.GetStationUnkId(dtRow.Item("branch").ToString.Trim)
'                    If iBranch <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 38, "Branch Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Department GROUP PRESENT.
'                If dtRow.Item("departmentgroup").ToString.Trim.Length > 0 Then
'                    iDeptGrp = CInt(objDeptGrp.GetDepartmentGrpUnkId(dtRow.Item("departmentgroup").ToString.Trim))
'                    If iDeptGrp <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 39, "Department Group Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Sections GROUP PRESENT.
'                If dtRow.Item("secgroup").ToString.Trim.Length > 0 Then
'                    iSecGrp = objSecGrp.GetSectionGrpUnkId(dtRow.Item("secgroup").ToString.Trim)
'                    If iSecGrp <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 40, "Sections Group Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If
'                '------------------------------ CHECKING IF Sections PRESENT.
'                If dtRow.Item("sections").ToString.Trim.Length > 0 Then
'                    iSections = objSection.GetSectionUnkId(dtRow.Item("sections").ToString.Trim)
'                    If iSections <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 41, "Sections Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Unit Group PRESENT.
'                If dtRow.Item("unitgroup").ToString.Trim.Length > 0 Then
'                    iUnitGrp = objUnitGrp.GetUnitGrpUnkId(dtRow.Item("unitgroup").ToString.Trim)
'                    If iUnitGrp <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 42, "Unit Group Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Unit PRESENT.
'                If dtRow.Item("unit").ToString.Trim.Length > 0 Then
'                    iUnit = objUnit.GetUnitUnkId(dtRow.Item("unit").ToString.Trim)
'                    If iUnit <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 43, "Unit Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Team PRESENT.
'                If dtRow.Item("team").ToString.Trim.Length > 0 Then
'                    iTeam = objTeam.GetTeamUnkId(dtRow.Item("team").ToString.Trim)
'                    If iTeam <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 44, "Team Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                '------------------------------ CHECKING IF Job GROUP PRESENT.
'                If dtRow.Item("jobgroup").ToString.Trim.Length > 0 Then
'                    iJobgrp = objJobGrp.GetJobGroupUnkId(dtRow.Item("jobgroup").ToString.Trim)
'                    If iJobgrp <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 45, "Job Group Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If
'                '------------------------------ CHECKING IF Cost Center PRESENT.
'                If dtRow.Item("costcenter").ToString.Trim.Length > 0 Then
'                    iCostCntr = objCostCenter.GetCostCenterUnkId(dtRow.Item("costcenter").ToString.Trim)
'                    If iCostCntr <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 46, "Cost Center Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                'Shani [ 09 DEC 2014 ] -- END

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                '------------------------------ PAT TYPE
'                If dtRow.Item("paytype").ToString.Trim.Length > 0 Then
'                    intPayTypeUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.PAY_TYPE, dtRow.Item("paytype").ToString.Trim)
'                    If intPayTypeUnkid <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 51, "Pay Type Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                If dtRow.Item("paypoint").ToString.Trim.Length > 0 Then
'                    intPayPointUnkid = objPayPoint.GetPayPointUnkidByCode(dtRow.Item("paypoint").ToString.Trim)
'                    If intPayPointUnkid <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 54, "Pay Point Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If
'                'Nilay (17 Feb 2017) -- End


'                'Pinkal (12-Mar-2015) -- Start
'                'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.

'                '------------------------------ CHECKING IF EOC REASON PRESENT.
'                If dtRow.Item("eocreason").ToString.Trim.Length > 0 Then
'                    iReasonId = objReason.GetReasonUnkid(True, dtRow.Item("eocreason").ToString.Trim)
'                    If iReasonId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 48, "EOC Reason Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If
'                End If

'                'Pinkal (12-Mar-2015) -- End

'                'Shani(09-Sep-2015) -- Start
'                'ENHANCEMENT : Add Transaction Head

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'objEmp._Employeeunkid = iEmpId
'                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmpId
'                'S.SANDEEP [04 JUN 2015] -- END


'                '------------------------------ CHECKING IF Transaction Head Code
'                If dtRow.Item("tranheadcode").ToString.Trim.Length > 0 Then
'                    iTranHeadUnkId = 0
'                    iTranHeadTypeID = 0
'                    Dim dHead As DataSet = Nothing
'                    dHead = objTranHead.GetList("List", , , , , , , "trnheadcode = '" & dtRow.Item("tranheadcode").ToString.Trim() & "' AND typeof_id = " & enTypeOf.Salary & "")
'                    If dHead.Tables("List").Rows.Count > 0 Then
'                        iTranHeadUnkId = CInt(dHead.Tables("List").Rows(0)("tranheadunkid"))
'                        iTranHeadTypeID = CInt(dHead.Tables("List").Rows(0)("trnheadtype_id"))
'                    End If
'                    If iTranHeadUnkId <= 0 Then
'                        dtRow.Item("image") = imgError
'                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 52, "Transaction Head Code Not Found.")
'                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                        dtRow.Item("objStatus") = 2
'                        objError.Text = CStr(Val(objError.Text) + 1)
'                        Continue For
'                    End If

'                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objEmp._Tranhedunkid
'                    If iTranHeadTypeID <> objTranHead._Trnheadtype_Id Then
'                        If objEmp._Tranhedunkid <> iTranHeadUnkId Then
'                            Dim dsData As New DataSet : Dim dRow() As DataRow = Nothing
'                            Dim objSal As New clsSalaryIncrement : Dim objED As New clsEarningDeduction
'                            Dim objCPeriod As New clsMasterData : Dim iCPeriodId As Integer = -1
'                            Dim objPeriod As New clscommom_period_Tran

'                            'S.SANDEEP [04 JUN 2015] -- START
'                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                            'iCPeriodId = objCPeriod.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
'                            iCPeriodId = objCPeriod.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
'                            'S.SANDEEP [04 JUN 2015] -- END


'                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iCPeriodId

'                            If objPeriod._End_Date < objEmp._Appointeddate Then

'                                'S.SANDEEP [04 JUN 2015] -- START
'                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                                'iCPeriodId = objCPeriod.getCurrentPeriodID(enModuleReference.Payroll, objEmp._Appointeddate, enStatusType.Open, FinancialYear._Object._YearUnkid)
'                                iCPeriodId = objCPeriod.getCurrentPeriodID(enModuleReference.Payroll, objEmp._Appointeddate, FinancialYear._Object._YearUnkid, enStatusType.Open)
'                                'S.SANDEEP [04 JUN 2015] -- END

'                            End If


'                            'S.SANDEEP [04 JUN 2015] -- START
'                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                            If objED.isExist(FinancialYear._Object._DatabaseName, objEmp._Employeeunkid(objPeriod._End_Date), objEmp._Tranhedunkid, , iCPeriodId) = True Then
'                                'If objED.isExist(FinancialYear._Object._DatabaseName, objEmp._Employeeunkid, objEmp._Tranhedunkid, , iCPeriodId) = True Then
'                                'S.SANDEEP [04 JUN 2015] -- END

'                                'S.SANDEEP [04 JUN 2015] -- START
'                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                                'dsData = objSal.GetList("List", CStr(objEmp._Employeeunkid), FinancialYear._Object._YearUnkid)
'                                dsData = objSal.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "List", Nothing, CStr(objEmp._Employeeunkid(objPeriod._End_Date)), iCPeriodId, FinancialYear._Object._YearUnkid)
'                                'S.SANDEEP [04 JUN 2015] -- END

'                                If dsData.Tables("List").Rows.Count > 0 Then
'                                    dRow = dsData.Tables("List").Select("periodunkid > " & iCPeriodId)
'                                    If dRow.Length > 0 Then
'                                        dtRow.Item("image") = imgError
'                                        dtRow.Item("message") = Language.getMessage("frmEmployeeMaster", 82, "Sorry, you cannot change the transaction head. Reason there are some increment done for the future period. Please delete them first.")
'                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                                        dtRow.Item("objStatus") = 2
'                                        objError.Text = CStr(Val(objError.Text) + 1)
'                                        Continue For
'                                    End If
'                                End If

'                                'S.SANDEEP [04 JUN 2015] -- START
'                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                                'dsData = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EList", , objEmp._Employeeunkid.ToString, , , , objEmp._Tranhedunkid)
'                                dsData = objED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EList", , objEmp._Employeeunkid(objPeriod._End_Date).ToString, , , , objEmp._Tranhedunkid)
'                                'S.SANDEEP [04 JUN 2015] -- END
'                                If dsData.Tables("EList").Rows.Count > 0 Then
'                                    'Sohail (31 Mar 2016) -- Start
'                                    'Issue - 58.1 - Future period should be counted on period end date instead of period id.
'                                    'dRow = dsData.Tables("EList").Select("periodunkid > " & iCPeriodId)
'                                    dRow = dsData.Tables("EList").Select("end_date > '" & eZeeDate.convertDate(objPeriod._End_Date) & "'")
'                                    'Sohail (31 Mar 2016) -- End
'                                    If dRow.Length > 0 Then
'                                        dtRow.Item("image") = imgError
'                                        dtRow.Item("message") = Language.getMessage("frmEmployeeMaster", 83, "Sorry, you cannot change the transaction head. Reason there are some heads posted for the future period. Please delete them first.")
'                                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                                        dtRow.Item("objStatus") = 2
'                                        objError.Text = CStr(Val(objError.Text) + 1)
'                                        Continue For
'                                    End If
'                                End If
'                            End If
'                        End If
'                    End If
'                End If
'                'Shani(09-Sep-2015) -- End




'                If dtRow.Item("email").ToString.Trim.Trim.Length > 0 Then
'                    objEmp._Email = dtRow.Item("email").ToString
'                End If

'                If dtRow.Item("dname").ToString.Trim.Length > 0 Then
'                    objEmp._Displayname = dtRow.Item("dname").ToString
'                End If

'                If IsDBNull(dtRow.Item("rdate")) = False Then
'                    If CDate(dtRow.Item("rdate")) <> Nothing Then
'                        objEmp._Termination_To_Date = CDate(dtRow.Item("rdate"))
'                    End If
'                End If

'                If iDeptId > 0 Then
'                    objEmp._Departmentunkid = iDeptId
'                End If

'                If iJobId > 0 Then
'                    objEmp._Jobunkid = iJobId
'                End If

'                If iClsGrpId > 0 Then
'                    objEmp._Classgroupunkid = iClsGrpId
'                End If

'                If iClsId > 0 Then
'                    objEmp._Classunkid = iClsId
'                End If


'                'Shani [ 09 DEC 2014 ] -- START
'                'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                If IsDBNull(dtRow.Item("birthdate")) = False Then
'                    If CDate(dtRow.Item("birthdate")) <> Nothing Then
'                        objEmp._Birthdate = CDate(dtRow.Item("birthdate"))
'                    End If
'                End If


'                'Pinkal (12-Mar-2015) -- Start
'                'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'                If iReasonId > 0 Then
'                    objEmp._Actionreasonunkid = iReasonId
'                    If IsDBNull(dtRow.Item("eocdate")) = False Then
'                        If CDate(dtRow.Item("eocdate")) <> Nothing Then
'                            objEmp._Empl_Enddate = CDate(dtRow.Item("eocdate"))
'                        End If
'                    End If
'                End If
'                'Pinkal (12-Mar-2015) -- End

'                If IsDBNull(dtRow.Item("leavingdate")) = False Then
'                    If CDate(dtRow.Item("leavingdate")) <> Nothing Then
'                        objEmp._Termination_From_Date = CDate(dtRow.Item("leavingdate"))
'                    End If
'                End If

'                If intGenderId >= 0 Then
'                    objEmp._Gender = intGenderId
'                End If

'                If iBranch > 0 Then
'                    objEmp._Stationunkid = iBranch
'                End If

'                If iDeptGrp > 0 Then
'                    objEmp._Deptgroupunkid = iDeptGrp
'                End If

'                If iSecGrp > 0 Then
'                    objEmp._Sectiongroupunkid = iSecGrp
'                End If

'                If iSections > 0 Then
'                    objEmp._Sectionunkid = iSections
'                End If

'                If iUnitGrp > 0 Then
'                    objEmp._Unitgroupunkid = iUnitGrp
'                End If

'                If iUnit > 0 Then
'                    objEmp._Unitunkid = iUnit
'                End If

'                If iTeam > 0 Then
'                    objEmp._Teamunkid = iTeam
'                End If

'                If iJobgrp > 0 Then
'                    objEmp._Jobgroupunkid = iJobgrp
'                End If

'                If iCostCntr > 0 Then
'                    objEmp._Costcenterunkid = iCostCntr
'                End If

'                'Shani [ 09 DEC 2014 ] -- END


'                'Shani(09-Sep-2015) -- Start
'                'ENHANCEMENT : Add Transaction Head
'                If iTranHeadUnkId > 0 Then
'                    objEmp._Tranhedunkid = iTranHeadUnkId
'                    objEmp._TrnHeadTypeId = iTranHeadTypeID
'                End If
'                'Shani(09-Sep-2015) -- End

'                'Shani(17-Nov-2015) -- Start
'                'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                If intPayTypeUnkid > 0 Then
'                    objEmp._Paytypeunkid = intPayTypeUnkid
'                End If
'                'Shani(17-Nov-2015) -- End

'                'Nilay (17 Feb 2017) -- Start
'                'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                If intPayPointUnkid > 0 Then
'                    objEmp._Paypointunkid = intPayPointUnkid
'                End If
'                'Nilay (17 Feb 2017) -- End

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'If objEmp.Update Then
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'If objEmp.Update(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid) Then
'                If objEmp.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, _
'                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , , , , , , , , , , , , _
'                                 getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then
'                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP})} -- END
'                    'Sohail (21 Aug 2015) -- End
'                    'S.SANDEEP [04 JUN 2015] -- END
'                    dtRow.Item("image") = imgAccept
'                    dtRow.Item("message") = ""
'                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 18, "Success")
'                    dtRow.Item("objStatus") = 1
'                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
'                Else
'                    dtRow.Item("image") = imgError
'                    dtRow.Item("message") = objEmp._Message
'                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
'                    dtRow.Item("objStatus") = 2
'                    objError.Text = CStr(Val(objError.Text) + 1)
'                End If

'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
'        Finally
'            btnFilter.Enabled = True
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    'Shani [ 09 DEC 2014 ] -- START
'    'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'    'Private Sub chkEmail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmail.CheckedChanged, chkDisplayName.CheckedChanged, chkRetirementDt.CheckedChanged, chkDepartment.CheckedChanged, chkClassGrp.CheckedChanged, chkJob.CheckedChanged, chkClass.CheckedChanged, chkClass.CheckedChanged, chkClass.CheckedChanged
'    'Shani [ 09 DEC 2014 ] -- END

'    Private Sub chkEmail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmail.CheckedChanged, _
'                                                                                                            chkDisplayName.CheckedChanged, _
' _
' _
' _
' _
' _
'                                                                                                            chkClass.CheckedChanged, _
'                                                                                                            chkClass.CheckedChanged, _
'                                                                                                            chkGender.CheckedChanged, _
'                                                                                                            chkBirthdate.CheckedChanged, _
' _
' _
' _
' _
' _
' _
' _
' _
' _
' _
' _
'                                                                                                            chkTransctionHead.CheckedChanged, _
'                                                                                                            chkPayType.CheckedChanged, _
'                                                                                                            chkPayPoint.CheckedChanged

'        'Nilay (17 Feb 2017) -- [chkPayPoint.CheckedChanged]
'        'Shani(17-Nov-2015) -- [chkPayType.CheckedChanged]
'        'Shani(09-Sep-2015) -- [chkTransctionHead.CheckedChanged]
'        Try
'            Select Case CType(sender, CheckBox).Name.ToString.ToUpper
'                Case chkEmail.Name.ToUpper
'                    If chkEmail.Checked = True Then
'                        objlblSign2.Enabled = True : lblEmail.Enabled = True : cboEmail.Enabled = True
'                    Else
'                        objlblSign2.Enabled = False : lblEmail.Enabled = False : cboEmail.Enabled = False
'                    End If
'                Case chkDisplayName.Name.ToUpper
'                    If chkDisplayName.Checked = True Then
'                        objlblSign3.Enabled = True : lblDisplayname.Enabled = True : cboDisplayname.Enabled = True
'                    Else
'                        objlblSign3.Enabled = False : lblDisplayname.Enabled = False : cboDisplayname.Enabled = False
'                    End If
'                Case chkRetirementDt.Name.ToUpper
'                    If chkRetirementDt.Checked = True Then
'                        objlblSign4.Enabled = True : lblRetirementDt.Enabled = True : cboRetirementDt.Enabled = True
'                    Else
'                        objlblSign4.Enabled = False : lblRetirementDt.Enabled = False : cboRetirementDt.Enabled = False
'                    End If
'                Case chkDepartment.Name.ToUpper
'                    If chkDepartment.Checked = True Then
'                        objlblSign5.Enabled = True : lblDepartment.Enabled = True : cboDepartment.Enabled = True
'                    Else
'                        objlblSign5.Enabled = False : lblDepartment.Enabled = False : cboDepartment.Enabled = False
'                    End If
'                Case chkJob.Name.ToUpper
'                    If chkJob.Checked = True Then
'                        objlblSign6.Enabled = True : lblJob.Enabled = True : cboJob.Enabled = True
'                    Else
'                        objlblSign6.Enabled = False : lblJob.Enabled = False : cboJob.Enabled = False
'                    End If
'                Case chkClassGrp.Name.ToUpper
'                    If chkClassGrp.Checked = True Then
'                        objlblSign7.Enabled = True : lblClassGrp.Enabled = True : cboClassGrp.Enabled = True
'                    Else
'                        objlblSign7.Enabled = False : lblClassGrp.Enabled = False : cboClassGrp.Enabled = False
'                    End If
'                Case chkClass.Name.ToUpper
'                    If chkClass.Checked = True Then
'                        objlblSign8.Enabled = True : lblClass.Enabled = True : cboClass.Enabled = True
'                    Else
'                        objlblSign8.Enabled = False : lblClass.Enabled = False : cboClass.Enabled = False
'                    End If

'                    'Shani [ 09 DEC 2014 ] -- START
'                    'Included (Gender,Birthdate,Eoc Date,Leaving Date,Branch,DepartmentGroup,Section Group,Sections,Unit Group,Unit,Team,Job Group & costcenter) on Employee Bulk Update Import Wizard
'                Case chkGender.Name.ToUpper
'                    If chkGender.Checked = True Then
'                        objlblSign9.Enabled = True : lblGender.Enabled = True : cboGender.Enabled = True
'                    Else
'                        objlblSign9.Enabled = False : lblGender.Enabled = False : cboGender.Enabled = False
'                    End If
'                Case chkBirthdate.Name.ToUpper
'                    If chkBirthdate.Checked = True Then
'                        objlblSign10.Enabled = True : lblBirthdate.Enabled = True : cboBirthdate.Enabled = True
'                    Else
'                        objlblSign10.Enabled = False : lblBirthdate.Enabled = False : cboBirthdate.Enabled = False
'                    End If
'                Case chkEOCDate.Name.ToUpper
'                    If chkEOCDate.Checked = True Then
'                        objlblSign11.Enabled = True : lblECODate.Enabled = True : cboEOCDate.Enabled = True
'                    Else
'                        objlblSign11.Enabled = False : lblECODate.Enabled = False : cboEOCDate.Enabled = False
'                    End If

'                    'Pinkal (12-Mar-2015) -- Start
'                    'Enhancement - Putting EOC Reason in Bulk Update Employee Data Screen.
'                    chkEOCReason.Checked = chkEOCDate.Checked
'                    objlblSign22.Enabled = chkEOCDate.Checked
'                    lblEOCReason.Enabled = chkEOCDate.Checked
'                    cboEOCReason.Enabled = chkEOCDate.Checked
'                    'Pinkal (12-Mar-2015) -- End

'                Case chkLeavingDate.Name.ToUpper
'                    If chkLeavingDate.Checked = True Then
'                        objlblSign12.Enabled = True : lblLeavingDate.Enabled = True : cboLeavingDate.Enabled = True
'                    Else
'                        objlblSign12.Enabled = False : lblLeavingDate.Enabled = False : cboLeavingDate.Enabled = False
'                    End If
'                Case chkBranch.Name.ToUpper
'                    If chkBranch.Checked = True Then
'                        objlblSign13.Enabled = True : lblBranch.Enabled = True : cboBranch.Enabled = True
'                    Else
'                        objlblSign13.Enabled = False : lblBranch.Enabled = False : cboBranch.Enabled = False
'                    End If
'                Case chkDepGroup.Name.ToUpper
'                    If chkDepGroup.Checked = True Then
'                        objlblSign14.Enabled = True : lblDepGroup.Enabled = True : cboDepGroup.Enabled = True
'                    Else
'                        objlblSign14.Enabled = False : lblDepGroup.Enabled = False : cboDepGroup.Enabled = False
'                    End If
'                Case chkSecGroup.Name.ToUpper
'                    If chkSecGroup.Checked = True Then
'                        objlblSign15.Enabled = True : lblSecGroup.Enabled = True : cboSecGroup.Enabled = True
'                    Else
'                        objlblSign15.Enabled = False : lblSecGroup.Enabled = False : cboSecGroup.Enabled = False
'                    End If
'                Case chkSections.Name.ToUpper
'                    If chkSections.Checked = True Then
'                        objlblSign16.Enabled = True : lblSections.Enabled = True : cboSections.Enabled = True
'                    Else
'                        objlblSign16.Enabled = False : lblSections.Enabled = False : cboSections.Enabled = False
'                    End If
'                Case chkUnitGroup.Name.ToUpper
'                    If chkUnitGroup.Checked = True Then
'                        objlblSign17.Enabled = True : lblUnitGroup.Enabled = True : cboUnitGroup.Enabled = True
'                    Else
'                        objlblSign17.Enabled = False : lblUnitGroup.Enabled = False : cboUnitGroup.Enabled = False
'                    End If
'                Case chkUnit.Name.ToUpper
'                    If chkUnit.Checked = True Then
'                        objlblSign18.Enabled = True : lblUnit.Enabled = True : cboUnit.Enabled = True
'                    Else
'                        objlblSign18.Enabled = False : lblUnit.Enabled = False : cboUnit.Enabled = False
'                    End If
'                Case chkTeam.Name.ToUpper
'                    If chkTeam.Checked = True Then
'                        objlblSign19.Enabled = True : lblTeam.Enabled = True : cboTeam.Enabled = True
'                    Else
'                        objlblSign19.Enabled = False : lblTeam.Enabled = False : cboTeam.Enabled = False
'                    End If
'                Case chkJobGroup.Name.ToUpper
'                    If chkJobGroup.Checked = True Then
'                        objlblSign20.Enabled = True : lblJobGroup.Enabled = True : cboJobGroup.Enabled = True
'                    Else
'                        objlblSign20.Enabled = False : lblJobGroup.Enabled = False : cboJobGroup.Enabled = False
'                    End If
'                Case chkCostCenter.Name.ToUpper
'                    If chkCostCenter.Checked = True Then
'                        objlblSign21.Enabled = True : lblCostCenter.Enabled = True : cboCostCenter.Enabled = True
'                    Else
'                        objlblSign21.Enabled = False : lblCostCenter.Enabled = False : cboCostCenter.Enabled = False
'                    End If
'                    'Shani [ 09 DEC 2014 ] -- END

'                    'Shani(09-Sep-2015) -- Start
'                    'ENHANCEMENT : Add Transaction Head
'                Case chkTransctionHead.Name.ToUpper
'                    If chkTransctionHead.Checked = True Then
'                        objlblSign23.Enabled = True : lblTransactionHead.Enabled = True : cboTransactionHead.Enabled = True
'                    Else
'                        objlblSign23.Enabled = False : lblTransactionHead.Enabled = False : cboTransactionHead.Enabled = False
'                    End If
'                    'Shani(09-Sep-2015) -- End

'                    'Shani(17-Nov-2015) -- Start
'                    'ENHANCEMENT : Pay type -Include this on Employee Master Importation
'                Case chkPayType.Name.ToUpper
'                    If chkPayType.Checked = True Then
'                        objlblSign24.Enabled = True : lblPayType.Enabled = True : cboPayType.Enabled = True
'                    Else
'                        objlblSign24.Enabled = False : lblPayType.Enabled = False : cboPayType.Enabled = False
'                    End If
'                    'Shani(17-Nov-2015) -- End

'                    'Nilay (17 Feb 2017) -- Start
'                    'ISSUE #44: ENHANCEMENT: I-Tax Form B report changes
'                Case chkPayPoint.Name.ToUpper
'                    If chkPayPoint.Checked = True Then
'                        objlblSign25.Enabled = True : lblPayPoint.Enabled = True : cboPayPoint.Enabled = True
'                    Else
'                        objlblSign25.Enabled = False : lblPayPoint.Enabled = False : cboPayPoint.Enabled = False
'                    End If
'                    'Nilay (17 Feb 2017) -- End

'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkEmail_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            Dim cmb As ComboBox = CType(sender, ComboBox)

'            If cmb.Text <> "" Then
'                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
'                For Each cr As Control In gbFiledMapping.Controls
'                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
'                        If cr.Enabled = True Then
'                            If cr.Name <> cmb.Name Then
'                                If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
'                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
'                                    cmb.SelectedIndex = -1
'                                    cmb.Select()
'                                    Exit Sub
'                                End If
'                            End If
'                        End If
'                    End If
'                Next
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
'        Try
'            Dim savDialog As New SaveFileDialog
'            dvGriddata.RowFilter = "objStatus = 2"
'            Dim dtTable As DataTable = CType(dgData.DataSource, DataView).ToTable
'            If dtTable.Rows.Count > 0 Then
'                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
'                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
'                    dtTable.Columns.Remove("image")
'                    dtTable.Columns.Remove("objstatus")

'                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Update Employee Wizard") = True Then
'                        Process.Start(savDialog.FileName)
'                    End If
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
'        Try
'            dvGriddata.RowFilter = "objStatus = 1"
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
'        Try
'            dvGriddata.RowFilter = "objStatus = 2"
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
'        Try
'            dvGriddata.RowFilter = "objStatus = 0"
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
'        Try
'            dvGriddata.RowFilter = ""
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'End Class