﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeList

#Region " Private Varaibles "
    Private objEmployee As clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "frmEmployeeList"
    Private mstrGuests() As String
    Private dsList As New DataSet

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private mblnIsFromMail As Boolean = False
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'Pinkal (12-Oct-2011) -- Start
    Private mblnExport_Print As Boolean = False
    Private mstrEmployeeId As String
    'Pinkal (12-Oct-2011) -- End


    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 



    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrCheckField As String
    Private mblnIsDynamicField As Boolean = False
    'Pinkal (14-Aug-2012) -- End

    'Shani(19-Nov-2015) -- Start
    'ENHANCEMENT : ADD/REMOVE Dynamic Fileds.
    Dim mdtRow As New DataTable
    Dim dtView As DataView
    'Shani(19-Nov-2015) -- End


    'Pinkal (17-Dec-2015) -- Start
    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
    Private mdtDynamicEmployeeList As DataTable = Nothing
    'Pinkal (17-Dec-2015) -- End


#End Region

#Region " Properties "

    Public ReadOnly Property EmailList() As String()
        Get
            Return mstrGuests
        End Get
    End Property

    Public Property _DataView() As DataSet
        Get
            Return dsList
        End Get
        Set(ByVal value As DataSet)
            dsList = value
        End Set
    End Property

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Public WriteOnly Property _IsFromMail() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property
    'S.SANDEEP [ 12 OCT 2011 ] -- END 


    'Pinkal (12-Oct-2011) -- Start

    Public WriteOnly Property IsExport_Print() As Boolean
        Set(ByVal value As Boolean)
            mblnExport_Print = value
        End Set
    End Property

    Public ReadOnly Property EmployeeIDs() As String
        Get
            Return mstrEmployeeId
        End Get
    End Property

    'Pinkal (12-Oct-2011) -- End

#End Region

#Region " Private Function "

    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub fillList()
    '    Dim dsAccess As New DataSet
    '    Dim StrSearching As String = ""
    '    Dim dtTable As New DataTable

    '    Try
    '        If User._Object.Privilege._AllowToViewEmpList = True Then
    '            dsAccess = objEmployee.GetList("EmpList", , False) 'Anjan (05 Aug 2011) Issue: by default only active employees were coming.

    '            If CInt(cboDepartment.SelectedValue) > 0 Then
    '                StrSearching &= "AND departmentunkid = " & CInt(cboDepartment.SelectedValue) & " "
    '            End If

    '            If CInt(cboJob.SelectedValue) > 0 Then
    '                StrSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue) & " "
    '            End If

    '            If CInt(cboShiftInfo.SelectedValue) > 0 Then
    '                StrSearching &= "AND shiftunkid = " & CInt(cboShiftInfo.SelectedValue) & " "
    '            End If

    '            If CInt(cboEmploymentType.SelectedValue) > 0 Then
    '                StrSearching &= "AND employmenttypeunkid = " & CInt(cboEmploymentType.SelectedValue) & " "
    '            End If

    '            If txtEmployeeCode.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND employeecode LIKE '%" & txtEmployeeCode.Text & "%'"
    '            End If

    '            'S.SANDEEP [ 25 DEC 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'If txtFirstname.Text.Trim.Length > 0 Then
    '            '    StrSearching &= "AND firstname LIKE '%" & txtFirstname.Text & "%'"
    '            'End If

    '            'If txtSurname.Text.Trim.Length > 0 Then
    '            '    StrSearching &= "AND surname LIKE '%" & txtSurname.Text & "%'"
    '            'End If
    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                StrSearching &= "AND employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
    '            End If
    '            'S.SANDEEP [ 25 DEC 2011 ] -- END

    '            If txtOtherName.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND othername LIKE '%" & txtOtherName.Text & "%'"
    '            End If


    '            'S.SANDEEP [ 29 DEC 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES 
    '            'TYPE : EMPLOYEMENT CONTRACT PROCESS
    '            'If radActive.Checked Then
    '            '    StrSearching &= "AND isactive = " & CBool(radActive.Checked) & " " & _
    '            '                    "AND probation_from_date = '' " & _
    '            '                    "AND suspended_from_date = '' " & _
    '            '                    "AND termination_from_date = '' "
    '            'End If
    '            If cboEmployeeStatus.SelectedIndex = 1 Then
    '                StrSearching &= "AND isactive = true " & _
    '                                "AND probation_from_date = '' " & _
    '                                "AND suspended_from_date = '' " & _
    '                                "AND termination_from_date = '' "

    '            End If
    '            'S.SANDEEP [ 29 DEC 2011 ] -- END



    '            If radProbated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND probation_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND probation_to_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND probation_from_date <> " & "''"
    '                End If
    '            End If

    '            If radSuspended.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND suspended_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND suspended_from_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND suspended_from_date <> " & "''"
    '                End If
    '            End If

    '            If radTerminated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND termination_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND termination_from_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND termination_from_date <> " & "''"
    '                End If
    '            End If



    '            'S.SANDEEP [ 29 DEC 2011 ] -- START
    '            'ENHANCEMENT : TRA CHANGES 
    '            'TYPE : EMPLOYEMENT CONTRACT PROCESS
    '            'If radNewEmployee.Checked Then
    '            '    StrSearching &= "AND isactive = " & CBool(False) & " " & _
    '            '                    "AND probation_from_date = '' " & _
    '            '                    "AND suspended_from_date = '' " & _
    '            '                    "AND termination_from_date = '' "
    '            'End If
    '            If cboEmployeeStatus.SelectedIndex = 2 Then
    '                StrSearching &= "AND isactive = false " & _
    '                                "AND probation_from_date = '' " & _
    '                                "AND suspended_from_date = '' " & _
    '                                "AND termination_from_date = '' "
    '            End If
    '            'S.SANDEEP [ 29 DEC 2011 ] -- END

    '            If CInt(cboBranch.SelectedValue) > 0 Then
    '                StrSearching &= "AND stationunkid = " & CInt(cboBranch.SelectedValue)
    '            End If


    '            'Anjan (21 Nov 2011)-Start
    '            'ENHANCEMENT : TRA COMMENTS
    '            If mstrAdvanceFilter.Length > 0 Then
    '                StrSearching &= "AND " & mstrAdvanceFilter
    '            End If
    '            'Anjan (21 Nov 2011)-End 


    '            If StrSearching.Trim.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '                dtTable = New DataView(dsAccess.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = dsAccess.Tables(0)
    '            End If

    '            Dim lvItem As ListViewItem

    '            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
    '            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

    '            lvEmployeeList.Items.Clear()

    '            lvEmployeeList.BeginUpdate()
    '            For Each drRow As DataRow In dtTable.Rows
    '                lvItem = New ListViewItem

    '                lvItem.Text = ""
    '                lvItem.SubItems.Add(drRow("employeecode").ToString)
    '                lvItem.SubItems.Add(drRow("firstname").ToString & " " & drRow("othername").ToString & " " & drRow("surname").ToString)
    '                lvItem.SubItems.Add(drRow("DeptName").ToString)
    '                lvItem.SubItems.Add(drRow("job_name").ToString)
    '                lvItem.SubItems.Add(drRow("shiftname").ToString)
    '                lvItem.SubItems.Add(drRow("Comm_Name").ToString)
    '                lvItem.SubItems.Add(drRow("email").ToString)

    '                'S.SANDEEP [ 29 DEC 2011 ] -- START
    '                'ENHANCEMENT : TRA CHANGES 
    '                'TYPE : EMPLOYEMENT CONTRACT PROCESS
    '                If CBool(drRow.Item("isactive")) = True Then
    '                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 19, "A"))
    '                Else
    '                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 19, "P"))
    '                End If
    '                'S.SANDEEP [ 29 DEC 2011 ] -- END

    '                lvItem.Tag = drRow("employeeunkid")

    '                'Sohail (04 Jan 2012) -- Start
    '                If CBool(drRow.Item("isclear")) = True Then
    '                    lvItem.BackColor = Color.Red
    '                    lvItem.ForeColor = Color.Yellow
    '                End If
    '                'Sohail (04 Jan 2012) -- End

    '                lvEmployeeList.Items.Add(lvItem)

    '            Next
    '            lvEmployeeList.EndUpdate()

    '            If lvEmployeeList.Items.Count > 10 Then
    '                colhEmploymentType.Width = 110 - 20
    '            Else
    '                colhEmploymentType.Width = 110
    '            End If

    '            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
    '            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
    '    Finally
    '        dsAccess.Dispose()
    '    End Try
    'End Sub
    'S.SANDEEP [ 18 AUG 2012 ] -- END


    Private Sub FillCombo()
        Dim objDepartment As New clsDepartment
        Dim objJobs As New clsJobs
        Dim objShift As New clsNewshift_master
        Dim objCommon As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim objBranch As New clsStation
        Try
            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
            End With

            dsCombos = objJobs.getComboList("Jobs", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Jobs")
            End With

            dsCombos = objShift.getListForCombo("Shift", True)
            With cboShiftInfo
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Shift")
                'Pinkal (09-Feb-2017) -- Start
                'Enhancement - Solved Bug in LGC(Le Grand Casino).
                .SelectedValue = 0
                'Pinkal (09-Feb-2017) -- End

            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
            With cboEmploymentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("EmplType")
            End With

            dsCombos = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With


            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''dsCombos = objEmployee.GetEmployeeList("List", True, True, , , , , , , , , , , , , , , , , , , False)
            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                       User._Object._Userunkid, _
            '                                       FinancialYear._Object._YearUnkid, _
            '                                       Company._Object._Companyunkid, _
            '                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                       ConfigParameter._Object._UserAccessModeSetting, _
            '                                       False, False, "List", True, , , , , , , , , , , , , , , , , , False)
            ''S.SANDEEP [04 JUN 2015] -- END



            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("List")
            '    .SelectedValue = 0
            '    'S.SANDEEP [04 APR 2015] -- START
            '    If CInt(.SelectedValue) <= 0 Then
            '        .Text = ""
            '    End If
            '    'S.SANDEEP [04 APR 2015] -- END
            'End With


            cboEmployeeStatus.Items.Clear()
            With cboEmployeeStatus
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 11, "(A). Approved"))
                .Items.Add(Language.getMessage(mstrModuleName, 12, "(P). Pending"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objDepartment = Nothing
            objJobs = Nothing
            objShift = Nothing
            objCommon = Nothing
            dsCombos = Nothing
            objBranch = Nothing
        End Try
    End Sub

    'Pinkal (17-Dec-2015) -- Start
    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

    'Private Sub GetCheckedData()
    '    Try
    '        If Me._DataView IsNot Nothing Then
    '            If dsList.Tables.Count > 0 Then
    '                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '                    For j As Integer = 0 To lvEmployeeList.Items.Count - 1
    '                        If CInt(dsList.Tables(0).Rows(i)("EmpId")) = CInt(lvEmployeeList.Items(j).Tag) Then
    '                            lvEmployeeList.Items(j).Checked = True
    '                        End If
    '                    Next
    '                Next
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetCheckedData", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (17-Dec-2015) -- End

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddEmployee
            btnEdit.Enabled = User._Object.Privilege._EditEmployee
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployee

            mnuEnrollFP.Enabled = User._Object.Privilege._AllowEnrollFingerPrint
            mnuDeleteFP.Enabled = User._Object.Privilege._AllowDeleteFingerPrint
            mnuEnrollNewCard.Enabled = User._Object.Privilege._AllowEnrollNewCard
            mnuDeleteEnrolledCard.Enabled = User._Object.Privilege._AllowDeleteEnrolledCard
            mnuImportEmployee.Enabled = User._Object.Privilege._AllowImportEmployee

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuEmployeeBenefits.Enabled = User._Object.Privilege._AddEmployeeBenefit
            mnuEmployeeDependents.Enabled = User._Object.Privilege._AddDependant
            mnuEmployeeExperiences.Enabled = User._Object.Privilege._AddEmployeeExperience
            mnuEmployeeQualifications.Enabled = User._Object.Privilege._AddEmployeeQualification
            mnuEmployeeReferences.Enabled = User._Object.Privilege._AddEmployeeReferee
            mnuEmployeeSkills.Enabled = User._Object.Privilege._AddEmployeeSkill
            mnuAssignedCompanyAssets.Enabled = User._Object.Privilege._AddEmployeeAssets
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuEditImportedEmployee.Enabled = User._Object.Privilege._EditImportedEmployee
            mnuImportEligibleApplicant.Enabled = User._Object.Privilege._AllowtoImportEligibleApplicant
            mnuScanDocuments.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            mnuImportMembership.Enabled = User._Object.Privilege._AllowToImporttMembership
            mnuGetMembershipTemplate.Enabled = User._Object.Privilege._AllowToExportMembership
            mnuReportTo.Enabled = User._Object.Privilege._AllowToSetReportingTo
            mnuViewDiary.Enabled = User._Object.Privilege._AllowToViewDiary

            'S.SANDEEP [ 08 OCT 2012 ] -- END

            'S.SANDEEP [ 28 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuImportImages.Enabled = ConfigParameter._Object._IsImgInDataBase
            'S.SANDEEP [ 28 MAR 2013 ] -- END

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes
            If mnuImportImages.Enabled = True Then
                mnuImportImages.Enabled = User._Object.Privilege._AllowToImportPhoto
            End If

            'Pinkal (01-Apr-2013) -- End


            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                mnuEnrollFP.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices)
                mnuDeleteFP.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices)
                mnuEnrollNewCard.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices)
                mnuDeleteEnrolledCard.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices)
                mnuImportEligibleApplicant.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)
                mnuEmployeebiodata.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management)
                objSep0.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management)
            End If

            'S.SANDEEP [ 15 April 2013 ] -- END


            'Pinkal (24-Aug-2013) -- Start
            'Enhancement : TRA Changes
            mnuImportEmpIdentities.Visible = User._Object.Privilege._AllowToImportEmpIdentity
            'Pinkal (24-Aug-2013) -- End


            'S.SANDEEP [ 26 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuVAssigned_Policy.Visible = ConfigParameter._Object._PolicyManagementTNA
            'S.SANDEEP [ 26 SEPT 2013 ] -- END

            'Anjan [06 April 2015] -- Start
            'ENHANCEMENT : Implementing Peoplesoft integration,requested by Andrew.
            mnuImpPPLSoftData.Enabled = User._Object.Privilege._AllowtoImportPeopleSoftData
            objSep3.Visible = ConfigParameter._Object._IsImportPeoplesoftData
            mnuImpPPLSoftData.Visible = ConfigParameter._Object._IsImportPeoplesoftData
            'Anjan [06 April 2015] -- End


            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            mnuImportEmpShift.Visible = User._Object.Privilege._AllowToImportEmployeeShiftAssignment
            'Pinkal (03-May-2017) -- End



            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            mnuImportBirthInformation.Enabled = User._Object.Privilege._AllowToImportEmployeeBirthInfo
            mnuImportAddress.Enabled = User._Object.Privilege._AllowToImportEmployeeAddress

            mnuUpdateImpDetails.Enabled = User._Object.Privilege._AllowToUpdateEmployeeDetails
            mnuUpdateEmployeeMovement.Enabled = User._Object.Privilege._AllowToUpdateEmployeeMovements

            btnExportList.Enabled = User._Object.Privilege._AllowToExportEmployeeList
            lnkAddRemoveField.Enabled = User._Object.Privilege._AllowToAddRemoveFields

            'Varsha Rana (17-Oct-2017) -- End

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            If ConfigParameter._Object._SkipEmployeeApprovalFlow = False AndAlso User._Object.Privilege._AllowToPerformEmpSubmitForApproval Then
                mnuSubmitForApproval.Visible = True
            Else
                mnuSubmitForApproval.Visible = False
            End If

            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                mnuApproverLevel.Visible = True
                mnuMovementApprover.Visible = True
                mnuApproveRejectMovments.Visible = True
            Else
                mnuApproverLevel.Visible = False
                mnuMovementApprover.Visible = False
                mnuApproveRejectMovments.Visible = False
            End If
            'S.SANDEEP [20-JUN-2018] -- End

            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            mnuFlexcubeData.Visible = ConfigParameter._Object._IsHRFlexcubeIntegrated
            'S.SANDEEP [09-AUG-2018] -- END
            mnuOrbitRequest.Visible = ConfigParameter._Object._IsOrbitIntegrated
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    Private Sub GetEmployeeEmail()
        Try

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Check Employee(s) from the list to perform further operation on it."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If dgEmployeeList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Check Employee(s) from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Pinkal (17-Dec-2015) -- End

            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            mstrGuests = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0

            Dim iEDx As Integer = -1 : Dim iNDx As Integer = -1

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.Columns.ContainsKey("Email") = True Then
            '    iEDx = lvEmployeeList.Columns.IndexOf(lvEmployeeList.Columns("Email"))
            'End If

            'If lvEmployeeList.Columns.ContainsKey("Employee Name") = True Then
            '    iNDx = lvEmployeeList.Columns.IndexOf(lvEmployeeList.Columns("Employee Name"))
            'End If

            'S.SANDEEP [12 OCT 2016] -- START
            'If dgEmployeeList.Columns.Contains("objdgcolh" & Language.getMessage("clsEmployee_Master", 52, "Email")) = True AndAlso dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 52, "Email")).Visible Then
            '    iEDx = dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 52, "Email")).Index
            'End If

            'If dgEmployeeList.Columns.Contains("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")) = True AndAlso dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")).Visible Then
            '    iNDx = dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")).Index
            'End If

            If dgEmployeeList.Columns.Contains(Language.getMessage("clsEmployee_Master", 52, "Email")) = True AndAlso dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 52, "Email")).Visible Then
                iEDx = dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 52, "Email")).Index
            End If

            If dgEmployeeList.Columns.Contains(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) = True AndAlso dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).Visible Then
                iNDx = dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).Index
            End If
            'S.SANDEEP [12 OCT 2016] -- END


            'Pinkal (17-Dec-2015) -- End

            If iEDx < 0 Or iNDx < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, The following columns are mandatory (Email,Employee Name). Please add these columns in order to send mail."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.CheckedItems.Count > 0 Then
            '    ReDim mstrGuests(lvEmployeeList.CheckedItems.Count + 1)
            '    For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
            '        With lvEmployeeList.CheckedItems(i)
            '            If .SubItems(iEDx).Text <> "" Then
            '                mstrGuests(intCnt) = .SubItems(iNDx).Text & " " & "<" & .SubItems(iEDx).Text & ">"
            '                If strNumber = "" Then
            '                    strNumber = .Tag.ToString
            '                Else
            '                    strNumber = strNumber & " , " & .Tag.ToString
            '                End If
            '                intCnt += 1
            '            Else
            '                If blnFlag = False Then
            '                    Dim strMsg As String = Language.getMessage(mstrModuleName, 6, "Some of the email address(s) are blank.And will not added to the list. Do you want to continue?")
            '                    If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '                        blnFlag = True
            '                        Continue For
            '                    Else
            '                        blnFlag = False
            '                        Exit Sub
            '                    End If
            '                End If
            '            End If
            '        End With
            '    Next
            '    If strNumber <> "" Then

            '        'S.SANDEEP [04 JUN 2015] -- START
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module)
            '        dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
            '        'S.SANDEEP [04 JUN 2015] -- END

            '    End If
            '    Me.Close()
            'End If


            Dim drRow() As DataRow = CType(dgEmployeeList.DataSource, DataTable).Select("ischeck = True")
            If drRow.Length > 0 Then
                ReDim mstrGuests(drRow.Length - 1)
                For i As Integer = 0 To drRow.Length - 1
                    If drRow(i)("Email").ToString().Trim.Length > 0 Then
                        mstrGuests(intCnt) = drRow(i)("Employee Name").ToString() & " " & "<" & drRow(i)("Email").ToString() & ">"
                        If strNumber = "" Then
                            strNumber = drRow(i)("employeeunkid").ToString()
                        Else
                            strNumber = strNumber & " , " & drRow(i)("employeeunkid").ToString()
                        End If
                        intCnt += 1
                    Else
                        If blnFlag = False Then
                            Dim strMsg As String = Language.getMessage(mstrModuleName, 6, "Some of the email address(s) are blank. Do you want to continue?")
                            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                blnFlag = True
                                Continue For
                            Else
                                blnFlag = False
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                If strNumber <> "" Then
                    'S.SANDEEP |09-APR-2019| -- START
                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END
                End If
                Me.Close()

            End If

            'Pinkal (17-Dec-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeEmail", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployeePrint_Export_Letter()
        Try
            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            mstrGuests = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0

            Dim iNDx As Integer = -1

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'If lvEmployeeList.Columns.ContainsKey("Employee Name") = True Then
            ' iNDx = lvEmployeeList.Columns.IndexOf(lvEmployeeList.Columns("Employee Name"))
            ' End If

            'S.SANDEEP [06-MAR-2018] -- START
            'ISSUE/ENHANCEMENT : {#Export Letter}
            'If dgEmployeeList.Columns.Contains("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")) = True AndAlso dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")).Visible Then
            '    iNDx = dgEmployeeList.Columns("objdgcolh" & Language.getMessage("clsEmployee_Master", 46, "Employee Name").Trim.Replace(" ", "_")).Index
            'End If

            If dgEmployeeList.Columns.Contains(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) = True AndAlso dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).Visible Then
                iNDx = dgEmployeeList.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).Index
            End If
            'S.SANDEEP [06-MAR-2018] -- END
            

            

            'Pinkal (17-Dec-2015) -- End



            If iNDx < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, The following column is mandatory (Employee Name). Please add this column in order to print/export letter."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.CheckedItems.Count > 0 Then
            '    ReDim mstrGuests(lvEmployeeList.CheckedItems.Count + 1)
            '    For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
            '        With lvEmployeeList.CheckedItems(i)
            '            If .Tag IsNot Nothing AndAlso .Tag.ToString <> "" Then
            '                mstrGuests(intCnt) = .SubItems(iNDx).Text
            '                If strNumber = "" Then
            '                    strNumber = .Tag.ToString
            '                    mstrEmployeeId = .Tag.ToString()
            '                Else
            '                    strNumber = strNumber & " , " & .Tag.ToString
            '                    mstrEmployeeId = mstrEmployeeId & " , " & .Tag.ToString
            '                End If
            '                intCnt += 1
            '            End If
            '        End With
            '    Next

            '    If strNumber <> "" Then

            '        'S.SANDEEP [04 JUN 2015] -- START
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module)
            '        dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
            '        'S.SANDEEP [04 JUN 2015] -- END
            '    End If
            '    Me.Close()
            'End If

            Dim drRow() As DataRow = CType(dgEmployeeList.DataSource, DataTable).Select("ischeck = True")
            If drRow.Length > 0 Then
                ReDim mstrGuests(drRow.Length - 1)
                For i As Integer = 0 To drRow.Length - 1
                    If drRow(i)("Employee Name") IsNot Nothing AndAlso drRow(i)("employeeunkid").ToString <> "" Then
                        mstrGuests(intCnt) = drRow(i)("Employee Name").ToString
                        If strNumber = "" Then
                            strNumber = drRow(i)("employeeunkid").ToString()
                            mstrEmployeeId = drRow(i)("employeeunkid").ToString()
                        Else
                            strNumber = strNumber & " , " & drRow(i)("employeeunkid").ToString()
                            mstrEmployeeId = mstrEmployeeId & " , " & drRow(i)("employeeunkid").ToString()
                        End If
                        intCnt += 1
                    End If
                Next

                If strNumber <> "" Then
                    'S.SANDEEP |09-APR-2019| -- START
                    'dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END
                End If
                Me.Close()
            End If

            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeePrint_Export_Letter", mstrModuleName)
        End Try
    End Sub

    Private Function GetDynamicField() As String
        Dim mstrField As String = ""
        Try
            'S.SANDEEP [29 SEP 2016] -- START
            'ENHANCEMENT : DEFINING ENUM FOR EMPLYOEE FEILD, LIST TAKING LONG TIME TO FILL


            ''Shani(19-Nov-2015) -- Start
            ''ENHANCEMENT : ADD/REMOVE Dynamic Fileds.
            ''For i As Integer = 0 To chkEmpfields.CheckedItems.Count - 1
            ''    mstrField &= chkEmpfields.CheckedItems(i).ToString() & ","
            ''Next

            ''If mstrField.Length > 0 Then
            ''    mstrField = mstrField.Substring(0, mstrField.Length - 1)
            ''    'S.SANDEEP [ 18 AUG 2012 ] -- START
            ''    'ENHANCEMENT : TRA CHANGES
            ''Else
            ''    mstrField = ConfigParameter._Object._Checked_Fields
            ''    'S.SANDEEP [ 18 AUG 2012 ] -- END
            ''End If
            'If mdtRow IsNot Nothing AndAlso mdtRow.Rows.Count > 0 Then
            '    mdtRow.AcceptChanges()
            '    Dim dtRow() As DataRow = mdtRow.Select("Ischecked='True'")
            '    For Each xRow In dtRow
            '        mstrField &= xRow("name").ToString() & ","
            'Next
            'End If
            'If mstrField.Length > 0 Then
            '    mstrField = mstrField.Substring(0, mstrField.Length - 1)
            'Else
            '    If dtView IsNot Nothing Then

            '        'Pinkal (17-Dec-2015) -- Start
            '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            '        'dtView.RowFilter = "name in ('Code','Employee Name','Department','Job','Shift','Employement Type','Email')"
            '        dtView.RowFilter = "name in ('" & Language.getMessage("clsEmployee_Master", 42, "Code") & "','" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "','" & _
            '                                    Language.getMessage("clsEmployee_Master", 120, "Department") & "','" & Language.getMessage("clsEmployee_Master", 118, "Job") & "','" & _
            '                                    Language.getMessage("clsEmployee_Master", 119, "Shift") & "','" & Language.getMessage("clsEmployee_Master", 121, "Employement Type") & "','" & _
            '                                    Language.getMessage("clsEmployee_Master", 52, "Email") & "','" & Language.getMessage("clsEmployee_Master", 213, "Status") & "')"
            '        'Pinkal (17-Dec-2015) -- End


            '        For Each rowview As DataRowView In dtView
            '            rowview.Item("IsChecked") = True
            '        Next
            '        dtView.Table.AcceptChanges()
            '        dtView.RowFilter = ""

            '        'Pinkal (17-Dec-2015) -- Start
            '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            '        'mstrField = "Code,Employee Name,Department,Job,Shift,Employement Type,Email"
            '        mstrField = Language.getMessage("clsEmployee_Master", 42, "Code") & "," & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 120, "Department") & "," & Language.getMessage("clsEmployee_Master", 118, "Job") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 119, "Shift") & "," & Language.getMessage("clsEmployee_Master", 121, "Employement Type") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 52, "Email") & "," & Language.getMessage("clsEmployee_Master", 213, "Status")
            '        'Pinkal (17-Dec-2015) -- End



            '    Else
            '        If ConfigParameter._Object._Checked_Fields.Trim.Length > 0 Then
            '    mstrField = ConfigParameter._Object._Checked_Fields
            '        Else
            '            'Pinkal (17-Dec-2015) -- Start
            '            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            '            ' mstrField = "Code,Employee Name,Department,Job,Shift,Employement Type,Email"
            '            mstrField = Language.getMessage("clsEmployee_Master", 42, "Code") & "," & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "," & _
            '                                        Language.getMessage("clsEmployee_Master", 120, "Department") & "," & Language.getMessage("clsEmployee_Master", 118, "Job") & "," & _
            '                                        Language.getMessage("clsEmployee_Master", 119, "Shift") & "," & Language.getMessage("clsEmployee_Master", 121, "Employement Type") & "," & _
            '                                        Language.getMessage("clsEmployee_Master", 52, "Email") & "," & Language.getMessage("clsEmployee_Master", 213, "Status")
            '            'Pinkal (17-Dec-2015) -- End
            '        End If
            '    End If
            'End If
            ''Shani(19-Nov-2015) -- End

            If mdtRow IsNot Nothing AndAlso mdtRow.Rows.Count > 0 Then
                mdtRow.AcceptChanges()
                mstrField = String.Join(",", mdtRow.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("Ischecked") = True).Select(Function(y) y.Field(Of Integer)("Id").ToString).ToArray)
            End If
            If mstrField.Length <= 0 Then
                If dtView IsNot Nothing Then
                    dtView.RowFilter = "Id IN(" & clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status & ")"
                    Dim StrCheck_Fields As String = String.Empty
                    StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status
                    Dim intSeparatedValue() As Integer = Nothing
                    intSeparatedValue = StrCheck_Fields.Split(CChar(",")).[Select](Function(n) Integer.Parse(n.Trim())).ToArray()
                    For intIndex As Integer = 0 To intSeparatedValue.Length - 1
                        Dim xrow() As DataRow = mdtRow.Select("Id = '" & intSeparatedValue.ElementAt(intIndex) & "'")
                        If xrow.Length > 0 Then
                            xrow(0).Item("Ischecked") = True
                        End If
                    Next
                    dtView.Table.AcceptChanges()
                    dtView.RowFilter = ""
                    mstrField = StrCheck_Fields
                Else
                    mstrField = ConfigParameter._Object._Checked_Fields
                    If mstrField.Trim.Length <= 0 Then mstrField = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status
                End If
            End If
            'S.SANDEEP [29 SEP 2016] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDynamicField", mstrModuleName)
        End Try
        Return mstrField
    End Function


    'Pinkal (17-Dec-2015) -- Start
    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

    'Private Sub FillListForDynamicColumn(ByVal mstrChkField As String)
    '    Dim dsAccess As New DataSet
    '    Dim StrSearching As String = ""
    '    Dim dtTable As New DataTable
    '    Dim objMstData As New clsMasterData
    '    Dim dsScale As New DataSet
    '    Try
    '        If User._Object.Privilege._AllowToViewEmpList = True Then
    '            If CInt(cboDepartment.SelectedValue) > 0 Then
    '                StrSearching &= "AND ISNULL(ETRF.deptgroupunkid,0) = " & CInt(cboDepartment.SelectedValue) & " "
    '            End If

    '            If CInt(cboJob.SelectedValue) > 0 Then
    '                StrSearching &= "AND ISNULL(ERECAT.jobunkid,0) = " & CInt(cboJob.SelectedValue) & " "
    '            End If

    '            If CInt(cboEmploymentType.SelectedValue) > 0 Then
    '                StrSearching &= "AND hremployee_master.employmenttypeunkid = " & CInt(cboEmploymentType.SelectedValue) & " "
    '            End If

    '            If txtEmployeeCode.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND hremployee_master.employeecode LIKE '%" & txtEmployeeCode.Text & "%'"
    '            End If

    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                StrSearching &= "AND hremployee_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
    '            End If

    '            If txtOtherName.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND ISNULL(hremployee_master.othername,'') LIKE '%" & txtOtherName.Text & "%'"
    '            End If

    '            If radProbated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND ISNULL(EPROB.probation_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(EPROB.probation_to_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND ISNULL(EPROB.probation_from_date,'') <> " & "''"
    '                End If
    '            End If

    '            If radSuspended.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND ISNULL(ESUSP.suspended_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(ESUSP.suspended_to_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND ISNULL(ESUSP.suspended_from_date,'') <> " & "''"
    '                End If
    '            End If

    '            If radTerminated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND ISNULL(ETERM.termination_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(ETERM.termination_from_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND ISNULL(ETERM.termination_from_date,'') <> " & "''"
    '                End If
    '            End If

    '            Select Case cboEmployeeStatus.SelectedIndex
    '                Case 1
    '                    StrSearching &= "AND hremployee_master.isapproved = 1 "
    '                Case 2
    '                    StrSearching &= "AND hremployee_master.isapproved = 0 "
    '            End Select

    '            If CInt(cboBranch.SelectedValue) > 0 Then
    '                StrSearching &= "AND ISNULL(ETRF.stationunkid,0) = " & CInt(cboBranch.SelectedValue)
    '            End If

    '            If mstrAdvanceFilter.Length > 0 Then
    '                StrSearching &= "AND " & mstrAdvanceFilter
    '            End If

    '            If StrSearching.Trim.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'If mblnIsFromMail = True Then
    '            '    dsAccess = objEmployee.GetListForDynamicField("EmpList", , False, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , StrSearching)
    '            'Else
    '            '    If chkShowActiveEmployee.CheckState = CheckState.Checked Then
    '            '        dsAccess = objEmployee.GetListForDynamicField("EmpList", , False, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , StrSearching)
    '            '    Else
    '            '        dsAccess = objEmployee.GetListForDynamicField("EmpList", , False, , , , , , StrSearching)
    '            '    End If
    '            'End If

    '            Dim blnIncludeInactiveEmp As Boolean = False
    '            If mblnIsFromMail = True Or chkShowActiveEmployee.CheckState = CheckState.Checked Then
    '                blnIncludeInactiveEmp = False
    '            Else
    '                blnIncludeInactiveEmp = True
    '            End If
    '            dsAccess = objEmployee.GetListForDynamicField(FinancialYear._Object._DatabaseName, _
    '                                                          User._Object._Userunkid, _
    '                                                          FinancialYear._Object._YearUnkid, _
    '                                                          Company._Object._Companyunkid, _
    '                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                          ConfigParameter._Object._UserAccessModeSetting, False, _
    '                                                          blnIncludeInactiveEmp, "List", , , StrSearching, _
    '                                                          ConfigParameter._Object._ShowFirstAppointmentDate, _
    '                                                          User._Object.Privilege._AllowTo_View_Scale)
    '            'S.SANDEEP [04 JUN 2015] -- END



    '            dtTable = dsAccess.Tables(0)

    '            Dim lvItem As ListViewItem

    '            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
    '            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

    '            lvEmployeeList.Items.Clear()

    '            If dtTable.Rows.Count > 0 Then
    '                lvEmployeeList.Columns.Clear()
    '            End If
    '            lvEmployeeList.BeginUpdate()

    '            Dim xColValue() As String = Nothing
    '            If mstrChkField.Trim.Length > 0 Then
    '                xColValue = mstrChkField.Trim.Split(CChar(","))
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'Dim dtDate As Date = CDate(dtTable.Rows(0)("Leaving Date"))
    '            'S.SANDEEP [04 JUN 2015] -- END



    '            If xColValue Is Nothing Then Exit Sub



    '            For Each drRow As DataRow In dtTable.Rows
    '                lvItem = New ListViewItem
    '                lvItem.Tag = drRow("employeeunkid")

    '                If Not lvEmployeeList.Columns.ContainsKey("Check") Then
    '                    If mblnIsFromMail Then
    '                        lvEmployeeList.CheckBoxes = True
    '                        lvEmployeeList.Columns.Add("Check", "", 25)
    '                    Else
    '                        lvEmployeeList.CheckBoxes = False
    '                        lvEmployeeList.Columns.Add("Check", "", 0)
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Code") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Code") Then
    '                        lvEmployeeList.Columns.Add("Code", Language.getMessage(mstrModuleName, 22, "Code"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Code").ToString)
    '                End If


    '                If Array.IndexOf(xColValue, "Employee Name") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Employee Name") Then
    '                        lvEmployeeList.Columns.Add("Employee Name", Language.getMessage(mstrModuleName, 23, "Employee Name"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Employee Name").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Department") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Department") Then
    '                        lvEmployeeList.Columns.Add("Department", Language.getMessage(mstrModuleName, 24, "Department"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Department").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Job") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Job") Then
    '                        lvEmployeeList.Columns.Add("Job", Language.getMessage(mstrModuleName, 25, "Job"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Job").ToString)
    '                End If

    '                'If mstrChkField.Contains("Shift") Then
    '                '    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '                '    'ENHANCEMENT : TRA CHANGES
    '                '    'If Not lvEmployeeList.Columns.ContainsKey("Shift") Then
    '                '    '    lvEmployeeList.Columns.Add("Shift", "Shift", 100)
    '                '    'End If
    '                '    'lvItem.SubItems.Add(drRow("Shift").ToString)
    '                '    'S.SANDEEP [ 26 SEPT 2013 ] -- END
    '                'End If

    '                If Array.IndexOf(xColValue, "Employement Type") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Employement Type") Then
    '                        lvEmployeeList.Columns.Add("Employement Type", Language.getMessage(mstrModuleName, 26, "Employement Type"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Employement Type").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Email") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Email") Then
    '                        lvEmployeeList.Columns.Add("Email", Language.getMessage(mstrModuleName, 27, "Email"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Email").ToString)
    '                End If

    '                If ConfigParameter._Object._ShowFirstAppointmentDate AndAlso Array.IndexOf(xColValue, "First Appointed Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("First Appointed Date") Then
    '                        lvEmployeeList.Columns.Add("First Appointed Date", Language.getMessage(mstrModuleName, 28, "First Appointed Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("First Appointed Date")) Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("First Appointed Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Appointed Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Appointed Date") Then
    '                        lvEmployeeList.Columns.Add("Appointed Date", Language.getMessage(mstrModuleName, 29, "Appointed Date"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Appointed Date").ToString).ToShortDateString)
    '                End If

    '                If Array.IndexOf(xColValue, "Gender Type") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Gender Type") Then
    '                        lvEmployeeList.Columns.Add("Gender Type", Language.getMessage(mstrModuleName, 30, "Gender Type"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Gender Type").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Login Name") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Login Name") Then
    '                        lvEmployeeList.Columns.Add("Login Name", Language.getMessage(mstrModuleName, 31, "Login Name"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Login Name").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Display Name") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Display Name") Then
    '                        lvEmployeeList.Columns.Add("Display Name", Language.getMessage(mstrModuleName, 32, "Display Name"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Display Name").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Birth Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth Date") Then
    '                        lvEmployeeList.Columns.Add("Birth Date", Language.getMessage(mstrModuleName, 33, "Birth Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Birth Date")) AndAlso drRow("Birth Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Birth Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Birth Ward") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth Ward") Then
    '                        lvEmployeeList.Columns.Add("Birth Ward", Language.getMessage(mstrModuleName, 34, "Birth Ward"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Birth Ward").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Birth Certificate No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth Certificate No") Then
    '                        lvEmployeeList.Columns.Add("Birth Certificate No", Language.getMessage(mstrModuleName, 35, "Birth Certificate No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Birth Certificate No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Birth Village") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth Village") Then
    '                        lvEmployeeList.Columns.Add("Birth Village", Language.getMessage(mstrModuleName, 36, "Birth Village"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Birth Village").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Work Permit No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Permit No") Then
    '                        lvEmployeeList.Columns.Add("Work Permit No", Language.getMessage(mstrModuleName, 37, "Work Permit No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Work Permit No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Work Permit Issue Place") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Permit Issue Place") Then
    '                        lvEmployeeList.Columns.Add("Work Permit Issue Place", Language.getMessage(mstrModuleName, 38, "Work Permit Issue Place"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Work Permit Issue Place").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Work Permit Issue Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Permit Issue Date") Then
    '                        lvEmployeeList.Columns.Add("Work Permit Issue Date", Language.getMessage(mstrModuleName, 39, "Work Permit Issue Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Work Permit Issue Date")) AndAlso drRow("Work Permit Issue Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Work Permit Issue Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Work Permit Expiry Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Permit Expiry Date") Then
    '                        lvEmployeeList.Columns.Add("Work Permit Expiry Date", Language.getMessage(mstrModuleName, 40, "Work Permit Expiry Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Work Permit Expiry Date")) AndAlso drRow("Work Permit Expiry Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Work Permit Expiry Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Extra Tel. No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Extra Tel. No") Then
    '                        lvEmployeeList.Columns.Add("Extra Tel. No", Language.getMessage(mstrModuleName, 41, "Extra Tel. No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Extra Tel. No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Height") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Height") Then
    '                        lvEmployeeList.Columns.Add("Height", Language.getMessage(mstrModuleName, 42, "Height"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Height").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Weight") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Weight") Then
    '                        lvEmployeeList.Columns.Add("Weight", Language.getMessage(mstrModuleName, 43, "Weight"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Weight").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Annivesary Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Annivesary Date") Then
    '                        lvEmployeeList.Columns.Add("Annivesary Date", Language.getMessage(mstrModuleName, 44, "Annivesary Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Annivesary Date")) AndAlso drRow("Annivesary Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Annivesary Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Sports Hobbies") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Sports Hobbies") Then
    '                        lvEmployeeList.Columns.Add("Sports Hobbies", Language.getMessage(mstrModuleName, 45, "Sports Hobbies"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Sports Hobbies").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Address1") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Address1") Then
    '                        lvEmployeeList.Columns.Add("Present Address1", Language.getMessage(mstrModuleName, 46, "Present Address1"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Address1").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Address2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Address2") Then
    '                        lvEmployeeList.Columns.Add("Present Address2", Language.getMessage(mstrModuleName, 47, "Present Address2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Address2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Provicnce") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Provicnce") Then
    '                        lvEmployeeList.Columns.Add("Present Provicnce", Language.getMessage(mstrModuleName, 48, "Present Provicnce"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Provicnce").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Road") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Road") Then
    '                        lvEmployeeList.Columns.Add("Present Road", Language.getMessage(mstrModuleName, 49, "Present Road"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Road").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Estate") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Estate") Then
    '                        lvEmployeeList.Columns.Add("Present Estate", Language.getMessage(mstrModuleName, 50, "Present Estate"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Estate").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Plot No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Plot No") Then
    '                        lvEmployeeList.Columns.Add("Present Plot No", Language.getMessage(mstrModuleName, 51, "Present Plot No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Plot No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Mobile") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Mobile") Then
    '                        lvEmployeeList.Columns.Add("Present Mobile", Language.getMessage(mstrModuleName, 52, "Present Mobile"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Mobile").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Alternate No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present  Alternate No") Then
    '                        lvEmployeeList.Columns.Add("Present Alternate No", Language.getMessage(mstrModuleName, 53, "Present Alternate No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Alternate No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Tel. No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Tel. No") Then
    '                        lvEmployeeList.Columns.Add("Present Tel. No", Language.getMessage(mstrModuleName, 54, "Present Tel. No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Tel. No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Fax") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Fax") Then
    '                        lvEmployeeList.Columns.Add("Present Fax", Language.getMessage(mstrModuleName, 55, "Present Fax"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Fax").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Email") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Email") Then
    '                        lvEmployeeList.Columns.Add("Present Email", Language.getMessage(mstrModuleName, 56, "Present Email"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Email").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Address1") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Address1") Then
    '                        lvEmployeeList.Columns.Add("Domicile Address1", Language.getMessage(mstrModuleName, 57, "Domicile Address1"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Address1").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Address2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Address2") Then
    '                        lvEmployeeList.Columns.Add("Domicile Address2", Language.getMessage(mstrModuleName, 58, "Domicile Address2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Address2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Provicnce") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Provicnce") Then
    '                        lvEmployeeList.Columns.Add("Domicile Provicnce", Language.getMessage(mstrModuleName, 59, "Domicile Provicnce"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Provicnce").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Road") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Road") Then
    '                        lvEmployeeList.Columns.Add("Domicile Road", Language.getMessage(mstrModuleName, 60, "Domicile Road"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Road").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Estate") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Estate") Then
    '                        lvEmployeeList.Columns.Add("Domicile Estate", Language.getMessage(mstrModuleName, 61, "Domicile Estate"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Estate").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile PlotNo") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile PlotNo") Then
    '                        lvEmployeeList.Columns.Add("Domicile PlotNo", Language.getMessage(mstrModuleName, 62, "Domicile PlotNo"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile PlotNo").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Mobile") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Mobile") Then
    '                        lvEmployeeList.Columns.Add("Domicile Mobile", Language.getMessage(mstrModuleName, 63, "Domicile Mobile"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Mobile").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Alternate No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Alternate No") Then
    '                        lvEmployeeList.Columns.Add("Domicile Alternate No", Language.getMessage(mstrModuleName, 64, "Domicile Alternate No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Alternate No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Tel. No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Tel. No") Then
    '                        lvEmployeeList.Columns.Add("Domicile Tel. No", Language.getMessage(mstrModuleName, 66, "Domicile Tel. No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Tel. No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Fax") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Fax") Then
    '                        lvEmployeeList.Columns.Add("Domicile Fax", Language.getMessage(mstrModuleName, 67, "Domicile Fax"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Fax").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Email") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Email") Then
    '                        lvEmployeeList.Columns.Add("Domicile Email", Language.getMessage(mstrModuleName, 68, "Domicile Email"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Email").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Address1") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Address1") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Address1", Language.getMessage(mstrModuleName, 69, "Recruitment Address1"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Address1").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Address2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Address2") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Address2", Language.getMessage(mstrModuleName, 70, "Recruitment Address2"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Address2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Provicnce") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Provicnce") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Province", Language.getMessage(mstrModuleName, 71, "Recruitment Province"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Provicnce").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Road") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Road") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Road", Language.getMessage(mstrModuleName, 72, "Recruitment Road"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Road").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Estate") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Estate") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Estate", Language.getMessage(mstrModuleName, 73, "Recruitment Estate"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Estate").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment PlotNo") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment PlotNo") Then
    '                        lvEmployeeList.Columns.Add("Recruitment PlotNo", Language.getMessage(mstrModuleName, 74, "Recruitment PlotNo"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment PlotNo").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Tel. No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Tel. No") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Tel. No", Language.getMessage(mstrModuleName, 75, "Recruitment Tel. No"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Tel. No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Firstname") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Firstname") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Firstname", Language.getMessage(mstrModuleName, 76, "Emer. Con. Firstname"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Firstname").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Lastname") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Lastname") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Lastname", Language.getMessage(mstrModuleName, 77, "Emer. Con. Lastname"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Lastname").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Address") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Address") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Address", Language.getMessage(mstrModuleName, 78, "Emer. Con. Address"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Address").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. State") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. State", Language.getMessage(mstrModuleName, 79, "Emer. Con. State"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Provicnce") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Provicnce") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Provicnce", Language.getMessage(mstrModuleName, 80, "Emer. Con. Provicnce"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Provicnce").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Road") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Road") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Road", Language.getMessage(mstrModuleName, 81, "Emer. Con. Road"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Road").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Estate") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Estate") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Estate", Language.getMessage(mstrModuleName, 82, "Emer. Con. Estate"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Estate").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. PlotNo") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. PlotNo") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Estate", Language.getMessage(mstrModuleName, 83, "Emer. Con. PlotNo"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. PlotNo").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Mobile") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Mobile") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile", Language.getMessage(mstrModuleName, 84, "Emer. Con. Mobile"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Mobile").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Alternate No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Alternate No") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Alternate No", Language.getMessage(mstrModuleName, 85, "Emer. Con. Alternate No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Alternate No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Tel. No") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Tel. No") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Tel. No", Language.getMessage(mstrModuleName, 86, "Emer. Con. Tel. No"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Tel. No").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Fax") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Fax") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Fax", Language.getMessage(mstrModuleName, 87, "Emer. Con. Fax"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Fax").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Email") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Email") Then
    '                        'Sohail (10 Jul 2014) -- Start
    '                        'Enhancement - Custom Language.
    '                        'lvEmployeeList.Columns.Add("Emer. Con. Email", "Emer. Con. Email", 100)
    '                        lvEmployeeList.Columns.Add("Emer. Con. Email", Language.getMessage(mstrModuleName, 88, "Emer. Con. Email"), 100)
    '                        'Sohail (10 Jul 2014) -- End
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Email").ToString)
    '                End If

    '                If User._Object.Privilege._AllowTo_View_Scale = True Then
    '                    If Array.IndexOf(xColValue, "Scale") > -1 Then
    '                        If Not lvEmployeeList.Columns.ContainsKey("Scale") Then
    '                            lvEmployeeList.Columns.Add("Scale", Language.getMessage(mstrModuleName, 89, "Scale"), 100)
    '                        End If

    '                        dsScale = objMstData.Get_Current_Scale("Salary", CInt(drRow("employeeunkid")), DateTime.Today)
    '                        If dsScale.Tables("Salary").Rows.Count > 0 Then
    '                            lvItem.SubItems.Add(Format(CDec(dsScale.Tables("Salary").Rows(0).Item("newscale").ToString), GUI.fmtCurrency))
    '                        Else
    '                            lvItem.SubItems.Add(Format(CDec(0), GUI.fmtCurrency))
    '                        End If
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Suspended From Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Suspended From Date") Then
    '                        lvEmployeeList.Columns.Add("Suspended From Date", Language.getMessage(mstrModuleName, 90, "Suspended From Date"), 100)
    '                    End If

    '                    If Not IsDBNull(drRow("Suspended From Date")) AndAlso drRow("Suspended From Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Suspended From Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Suspended To Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Suspended To Date") Then
    '                        lvEmployeeList.Columns.Add("Suspended To Date", Language.getMessage(mstrModuleName, 91, "Suspended To Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Suspended To Date")) AndAlso drRow("Suspended To Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Suspended To Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Probation From Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Probation From Date") Then
    '                        lvEmployeeList.Columns.Add("Probation From Date", Language.getMessage(mstrModuleName, 92, "Probation From Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Probation From Date")) AndAlso drRow("Probation From Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Probation From Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Probation To Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Probation To Date") Then
    '                        lvEmployeeList.Columns.Add("Probation To Date", Language.getMessage(mstrModuleName, 93, "Probation To Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Probation To Date")) AndAlso drRow("Probation To Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Probation To Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "EOC Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("EOC Date") Then
    '                        lvEmployeeList.Columns.Add("EOC Date", Language.getMessage(mstrModuleName, 94, "EOC Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("EOC Date")) AndAlso drRow("EOC Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("EOC Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Leaving Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Leaving Date") Then
    '                        lvEmployeeList.Columns.Add("Leaving Date", Language.getMessage(mstrModuleName, 95, "Leaving Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Leaving Date")) AndAlso drRow("Leaving Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Leaving Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Retirement Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Retirement Date") Then
    '                        lvEmployeeList.Columns.Add("Retirement Date", Language.getMessage(mstrModuleName, 96, "Retirement Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Retirement Date")) AndAlso drRow("Retirement Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Retirement Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Reinstatement Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Reinstatement Date") Then
    '                        lvEmployeeList.Columns.Add("Reinstatement Date", Language.getMessage(mstrModuleName, 97, "Reinstatement Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Reinstatement Date")) AndAlso drRow("Reinstatement Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Reinstatement Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Remark") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Remark") Then
    '                        lvEmployeeList.Columns.Add("Remark", Language.getMessage(mstrModuleName, 98, "Remark"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Remark").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Firstname2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Firstname2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Firstname2", Language.getMessage(mstrModuleName, 99, "Emer. Con. Firstname2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Firstname2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Lastname2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Lastname2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Lastname2", Language.getMessage(mstrModuleName, 100, "Emer. Con. Lastname2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Lastname2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. State2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. State2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. State2", Language.getMessage(mstrModuleName, 101, "Emer. Con. State2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. State2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Provicnce2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Provicnce2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Provicnce2", Language.getMessage(mstrModuleName, 102, "Emer. Con. Provicnce2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Provicnce2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Road2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Road2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Road2", Language.getMessage(mstrModuleName, 103, "Emer. Con. Road2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Road2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Estate2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Estate2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Estate2", Language.getMessage(mstrModuleName, 104, "Emer. Con. Estate2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Estate2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Plotno2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Plotno2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Plotno2", Language.getMessage(mstrModuleName, 105, "Emer. Con. Plotno2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Plotno2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Mobile2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Mobile2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile2", Language.getMessage(mstrModuleName, 106, "Emer. Con. Mobile2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Mobile2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Alternate No2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Alternate No2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Alternate No2", Language.getMessage(mstrModuleName, 107, "Emer. Con. Alternate No2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Alternate No2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Tel No2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Tel No2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Tel No2", Language.getMessage(mstrModuleName, 108, "Emer. Con. Tel No2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Tel No2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Fax2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Fax2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Fax2", Language.getMessage(mstrModuleName, 109, "Emer. Con. Fax2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Fax2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Email2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Email2") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Email2", Language.getMessage(mstrModuleName, 110, "Emer. Con. Email2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Email2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Firstname3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Firstname3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Firstname3", Language.getMessage(mstrModuleName, 111, "Emer. Con. Firstname3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Firstname3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Lastname3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Lastname3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Lastname3", Language.getMessage(mstrModuleName, 112, "Emer. Con. Lastname3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Lastname3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Address3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Address3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Address3", Language.getMessage(mstrModuleName, 113, "Emer. Con. Address3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Address3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. State3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. State3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. State3", Language.getMessage(mstrModuleName, 114, "Emer. Con. State3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. State3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Provicnce3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Provicnce3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Provicnce3", Language.getMessage(mstrModuleName, 115, "Emer. Con. Provicnce3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Provicnce3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Road3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Road3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Road3", Language.getMessage(mstrModuleName, 116, "Emer. Con. Road3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Road3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Estate3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Estate3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Estate3", Language.getMessage(mstrModuleName, 117, "Emer. Con. Estate3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Estate3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. plotno3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. plotno3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Estate3", Language.getMessage(mstrModuleName, 118, "Emer. Con. plotno3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. plotno3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Mobile3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Mobile3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile3", Language.getMessage(mstrModuleName, 119, "Emer. Con. Mobile3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Mobile3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Alternate No3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Alternate No3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile3", Language.getMessage(mstrModuleName, 120, "Emer. Con. Alternate No3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Alternate No3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Tel. No3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Tel. No3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile3", Language.getMessage(mstrModuleName, 121, "Emer. Con. Tel. No3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Tel. No3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Fax3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Fax3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile3", Language.getMessage(mstrModuleName, 122, "Emer. Con. Fax3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Fax3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emer. Con. Email3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emer. Con. Email3") Then
    '                        lvEmployeeList.Columns.Add("Emer. Con. Mobile3", Language.getMessage(mstrModuleName, 123, "Emer. Con. Email3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emer. Con. Email3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Allocation Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Allocation Reason") Then
    '                        lvEmployeeList.Columns.Add("Allocation Reason", Language.getMessage(mstrModuleName, 124, "Allocation Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Allocation Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Re-hire Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Re-hire Reason") Then
    '                        lvEmployeeList.Columns.Add("Re-hire Reason", Language.getMessage(mstrModuleName, 179, "Re-hire Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Re-hire Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recategorize Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recategorize Reason") Then
    '                        lvEmployeeList.Columns.Add("Recategorize Reason", Language.getMessage(mstrModuleName, 180, "Recategorize Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recategorize Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Work Permit Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Permit Reason") Then
    '                        lvEmployeeList.Columns.Add("Work Permit Reason", Language.getMessage(mstrModuleName, 181, "Work Permit Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Work Permit Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Cost Center Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Cost Center Reason") Then
    '                        lvEmployeeList.Columns.Add("Cost Center Reason", Language.getMessage(mstrModuleName, 182, "Cost Center Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Cost Center Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Suspension Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Suspension Reason") Then
    '                        lvEmployeeList.Columns.Add("Suspension Reason", Language.getMessage(mstrModuleName, 183, "Suspension Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Suspension Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Probation Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Probation Reason") Then
    '                        lvEmployeeList.Columns.Add("Probation Reason", Language.getMessage(mstrModuleName, 184, "Probation Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Probation Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Retirement Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Retirement Reason") Then
    '                        lvEmployeeList.Columns.Add("Retirement Reason", Language.getMessage(mstrModuleName, 185, "Retirement Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Retirement Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Exclude Payroll") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Exclude Payroll") Then
    '                        lvEmployeeList.Columns.Add("Exclude Payroll", Language.getMessage(mstrModuleName, 125, "Exclude Payroll"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Exclude Payroll").ToString)
    '                End If


    '                If Array.IndexOf(xColValue, "Cofirmation Date") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Cofirmation Date") Then
    '                        lvEmployeeList.Columns.Add("Cofirmation Date", Language.getMessage(mstrModuleName, 186, "Confirmation Date"), 100)
    '                    End If
    '                    If Not IsDBNull(drRow("Cofirmation Date")) AndAlso drRow("Cofirmation Date").ToString().Trim <> "" Then
    '                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Cofirmation Date").ToString).ToShortDateString)
    '                    Else
    '                        lvItem.SubItems.Add("")
    '                    End If
    '                End If

    '                If Array.IndexOf(xColValue, "Confirmation Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Confirmation Reason") Then
    '                        lvEmployeeList.Columns.Add("Confirmation Reason", Language.getMessage(mstrModuleName, 187, "Confirmation Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Confirmation Reason").ToString)
    '                End If


    '                If Array.IndexOf(xColValue, "Title") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Title") Then
    '                        lvEmployeeList.Columns.Add("Title", Language.getMessage(mstrModuleName, 126, "Title"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Title").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Pay Type") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Pay Type") Then
    '                        lvEmployeeList.Columns.Add("Pay Type", Language.getMessage(mstrModuleName, 127, "Pay Type"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Pay Type").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Pay Point") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Pay Point") Then
    '                        lvEmployeeList.Columns.Add("Pay Point", Language.getMessage(mstrModuleName, 128, "Pay Point"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Pay Point").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Birth State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth State") Then
    '                        lvEmployeeList.Columns.Add("Birth State", Language.getMessage(mstrModuleName, 129, "Birth State"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Birth State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Birth Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Birth Country") Then
    '                        lvEmployeeList.Columns.Add("Birth Country", Language.getMessage(mstrModuleName, 130, "Birth Country"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Birth Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Work Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Work Country") Then
    '                        lvEmployeeList.Columns.Add("Work Country", Language.getMessage(mstrModuleName, 131, "Work Country"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Work Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Complexion") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Complexion") Then
    '                        lvEmployeeList.Columns.Add("Complexion", Language.getMessage(mstrModuleName, 132, "Complexion"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Complexion").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Blood Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Blood Group") Then
    '                        lvEmployeeList.Columns.Add("Blood Group", Language.getMessage(mstrModuleName, 133, "Blood Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Blood Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Eye Color") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Eye Color") Then
    '                        lvEmployeeList.Columns.Add("Eye Color", Language.getMessage(mstrModuleName, 134, "Eye Color"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Eye Color").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Nationality") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Nationality") Then
    '                        lvEmployeeList.Columns.Add("Nationality", Language.getMessage(mstrModuleName, 135, "Nationality"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Nationality").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Ethnicity") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Ethnicity") Then
    '                        lvEmployeeList.Columns.Add("Ethnicity", Language.getMessage(mstrModuleName, 136, "Ethnicity"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Ethnicity").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Religion") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Religion") Then
    '                        lvEmployeeList.Columns.Add("Religion", Language.getMessage(mstrModuleName, 137, "Religion"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Religion").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Hair Color") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Hair Color") Then
    '                        lvEmployeeList.Columns.Add("Hair Color", Language.getMessage(mstrModuleName, 138, "Hair Color"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Hair Color").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Language1") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Language1") Then
    '                        lvEmployeeList.Columns.Add("Language1", Language.getMessage(mstrModuleName, 139, "Language1"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Language1").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Language2") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Language2") Then
    '                        lvEmployeeList.Columns.Add("Language2", Language.getMessage(mstrModuleName, 140, "Language2"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Language2").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Language3") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Language3") Then
    '                        lvEmployeeList.Columns.Add("Language3", Language.getMessage(mstrModuleName, 141, "Language3"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Language3").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Language4") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Language4") Then
    '                        lvEmployeeList.Columns.Add("Language4", Language.getMessage(mstrModuleName, 142, "Language4"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Language4").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Marital Status") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Marital Status") Then
    '                        lvEmployeeList.Columns.Add("Marital Status", Language.getMessage(mstrModuleName, 143, "Marital Status"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Marital Status").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Country") Then
    '                        lvEmployeeList.Columns.Add("Present Country", Language.getMessage(mstrModuleName, 144, "Present Country"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Postcode") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Postcode") Then
    '                        lvEmployeeList.Columns.Add("Present Postcode", Language.getMessage(mstrModuleName, 145, "Present Postcode"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Postcode").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present State") Then
    '                        lvEmployeeList.Columns.Add("Present State", Language.getMessage(mstrModuleName, 146, "Present State"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Present Post Town") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Present Post Town") Then
    '                        lvEmployeeList.Columns.Add("Present Post Town", Language.getMessage(mstrModuleName, 147, "Present Post Town"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Present Post Town").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Country") Then
    '                        lvEmployeeList.Columns.Add("Domicile Country", Language.getMessage(mstrModuleName, 148, "Domicile Country"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Postcode") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Postcode") Then
    '                        lvEmployeeList.Columns.Add("Domicile Postcode", Language.getMessage(mstrModuleName, 149, "Domicile Postcode"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Postcode").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile State") Then
    '                        lvEmployeeList.Columns.Add("Domicile State", Language.getMessage(mstrModuleName, 150, "Domicile State"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Domicile Post Town") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Domicile Post Town") Then
    '                        lvEmployeeList.Columns.Add("Domicile Post Town", Language.getMessage(mstrModuleName, 151, "Domicile Post Town"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Domicile Post Town").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Country") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Country", Language.getMessage(mstrModuleName, 152, "Recruitment Country"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Postcode") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Postcode") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Postcode", Language.getMessage(mstrModuleName, 153, "Recruitment Postcode"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Postcode").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment State") Then
    '                        lvEmployeeList.Columns.Add("Recruitment State", Language.getMessage(mstrModuleName, 154, "Recruitment State"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Recruitment Post Town") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Recruitment Post Town") Then
    '                        lvEmployeeList.Columns.Add("Recruitment Post Town", Language.getMessage(mstrModuleName, 155, "Recruitment Post Town"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Recruitment Post Town").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emergency Country") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emergency Country") Then
    '                        lvEmployeeList.Columns.Add("Emergency Country", Language.getMessage(mstrModuleName, 156, "Emergency Country"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emergency Country").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emergency Postcode") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emergency Postcode") Then
    '                        lvEmployeeList.Columns.Add("Emergency Postcode", Language.getMessage(mstrModuleName, 157, "Emergency Postcode"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emergency Postcode").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emergency State") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emergency State") Then
    '                        lvEmployeeList.Columns.Add("Emergency State", Language.getMessage(mstrModuleName, 158, "Emergency State"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emergency State").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Emergency Post Town") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Emergency Post Town") Then
    '                        lvEmployeeList.Columns.Add("Emergency Post Town", Language.getMessage(mstrModuleName, 159, "Emergency Post Town"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Emergency Post Town").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Station") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Station") Then
    '                        lvEmployeeList.Columns.Add("Station", Language.getMessage(mstrModuleName, 160, "Station"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Station").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Dept. Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Dept. Group") Then
    '                        lvEmployeeList.Columns.Add("Dept. Group", Language.getMessage(mstrModuleName, 161, "Dept. Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Dept. Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Section") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Section") Then
    '                        lvEmployeeList.Columns.Add("Section", Language.getMessage(mstrModuleName, 162, "Section"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Section").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Unit") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Unit") Then
    '                        lvEmployeeList.Columns.Add("Unit", Language.getMessage(mstrModuleName, 163, "Unit"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Unit").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Job Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Job Group") Then
    '                        lvEmployeeList.Columns.Add("Job Group", Language.getMessage(mstrModuleName, 164, "Job Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Job Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Grade Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Grade Group") Then
    '                        lvEmployeeList.Columns.Add("Grade Group", Language.getMessage(mstrModuleName, 165, "Grade Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Grade Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Grade") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Grade") Then
    '                        lvEmployeeList.Columns.Add("Grade", Language.getMessage(mstrModuleName, 166, "Grade"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Grade").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Grade Level") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Grade Level") Then
    '                        lvEmployeeList.Columns.Add("Grade Level", Language.getMessage(mstrModuleName, 167, "Grade Level"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Grade Level").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Class Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Class Group") Then
    '                        lvEmployeeList.Columns.Add("Class Group", Language.getMessage(mstrModuleName, 168, "Class Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Class Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Class") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Class") Then
    '                        lvEmployeeList.Columns.Add("Class", Language.getMessage(mstrModuleName, 169, "Class"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Class").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Cost Center") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Cost Center") Then
    '                        lvEmployeeList.Columns.Add("Cost Center", Language.getMessage(mstrModuleName, 170, "Cost Center"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Cost Center").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Transaction Head") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Transaction Head") Then
    '                        lvEmployeeList.Columns.Add("Transaction Head", Language.getMessage(mstrModuleName, 171, "Transaction Head"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Transaction Head").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Reason") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Reason") Then
    '                        lvEmployeeList.Columns.Add("Reason", Language.getMessage(mstrModuleName, 172, "Reason"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Reason").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Team") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Team") Then
    '                        lvEmployeeList.Columns.Add("Team", Language.getMessage(mstrModuleName, 173, "Team"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Team").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Unit Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Unit Group") Then
    '                        lvEmployeeList.Columns.Add("Unit Group", Language.getMessage(mstrModuleName, 174, "Unit Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Unit Group").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Section Group") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Section Group") Then
    '                        lvEmployeeList.Columns.Add("Section Group", Language.getMessage(mstrModuleName, 175, "Section Group"), 100)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Section Group").ToString)
    '                End If

    '                If Not lvEmployeeList.Columns.ContainsKey("Active") Then
    '                    lvEmployeeList.Columns.Add("Active", Language.getMessage(mstrModuleName, 176, "Status"), 50)
    '                End If

    '                If CBool(drRow.Item("isapproved")) = True Then
    '                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 14, "A"))
    '                Else
    '                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 15, "P"))
    '                End If


    '                If Array.IndexOf(xColValue, "Reporting To") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Reporting To") Then
    '                        lvEmployeeList.Columns.Add("Reporting To", Language.getMessage(mstrModuleName, 177, "Reporting To"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Reporting To").ToString)
    '                End If

    '                If Array.IndexOf(xColValue, "Reporting Job") > -1 Then
    '                    If Not lvEmployeeList.Columns.ContainsKey("Reporting Job") Then
    '                        lvEmployeeList.Columns.Add("Reporting Job", Language.getMessage(mstrModuleName, 178, "Reporting Job"), 150)
    '                    End If
    '                    lvItem.SubItems.Add(drRow("Reporting Job").ToString)
    '                End If

    '                If CBool(drRow.Item("isclear")) = True Then
    '                    lvItem.BackColor = Color.Red
    '                    lvItem.ForeColor = Color.Yellow
    '                End If

    '                lvEmployeeList.Items.Add(lvItem)
    '            Next

    '            lvEmployeeList.EndUpdate()

    '            If lvEmployeeList.Items.Count > 10 Then
    '                colhEmploymentType.Width = 110 - 20
    '            Else
    '                colhEmploymentType.Width = 110
    '            End If

    '            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
    '            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillListForDynamicColumn", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub


    Private Sub FillListForDynamicColumn(ByVal mstrChkField As String)
        Dim dsAccess As New DataSet
        Dim StrSearching As String = ""
        Dim dtTable As New DataTable
        Dim objMstData As New clsMasterData
        Dim dsScale As New DataSet
        Try
            If User._Object.Privilege._AllowToViewEmpList = True Then
                If CInt(cboDepartment.SelectedValue) > 0 Then
                    'S.SANDEEP [14 MAR 2016] -- START
                    'StrSearching &= "AND ISNULL(ETRF.deptgroupunkid,0) = " & CInt(cboDepartment.SelectedValue) & " "
                    StrSearching &= "AND ISNULL(ETRF.departmentunkid,0) = " & CInt(cboDepartment.SelectedValue) & " "
                    'S.SANDEEP [14 MAR 2016] -- END
                End If

                If CInt(cboJob.SelectedValue) > 0 Then
                    StrSearching &= "AND ISNULL(ERECAT.jobunkid,0) = " & CInt(cboJob.SelectedValue) & " "
                End If

                If CInt(cboEmploymentType.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.employmenttypeunkid = " & CInt(cboEmploymentType.SelectedValue) & " "
                End If

                If txtEmployeeCode.Text.Trim.Length > 0 Then
                    StrSearching &= "AND hremployee_master.employeecode LIKE '%" & txtEmployeeCode.Text & "%'"
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
                End If

                If txtOtherName.Text.Trim.Length > 0 Then
                    StrSearching &= "AND ISNULL(hremployee_master.othername,'') LIKE '%" & txtOtherName.Text & "%'"
                End If

                If radProbated.Checked Then
                    If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
                        StrSearching &= "AND ISNULL(EPROB.probation_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(EPROB.probation_to_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
                    Else
                        StrSearching &= "AND ISNULL(EPROB.probation_from_date,'') <> " & "''"
                    End If
                End If

                If radSuspended.Checked Then
                    If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
                        StrSearching &= "AND ISNULL(ESUSP.suspended_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(ESUSP.suspended_to_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
                    Else
                        StrSearching &= "AND ISNULL(ESUSP.suspended_from_date,'') <> " & "''"
                    End If
                End If

                If radTerminated.Checked Then
                    If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
                        StrSearching &= "AND ISNULL(ETERM.termination_from_date,'') >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND ISNULL(ETERM.termination_from_date,'') <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
                    Else
                        StrSearching &= "AND ISNULL(ETERM.termination_from_date,'') <> " & "''"
                    End If
                End If

                Select Case cboEmployeeStatus.SelectedIndex
                    Case 1
                        StrSearching &= "AND hremployee_master.isapproved = 1 "
                    Case 2
                        StrSearching &= "AND hremployee_master.isapproved = 0 "
                End Select

                If CInt(cboBranch.SelectedValue) > 0 Then
                    StrSearching &= "AND ISNULL(ETRF.stationunkid,0) = " & CInt(cboBranch.SelectedValue)
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Trim.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                Dim blnIncludeInactiveEmp As Boolean = False
                If mblnIsFromMail = True Or chkShowActiveEmployee.CheckState = CheckState.Checked Then
                    blnIncludeInactiveEmp = False
                Else
                    blnIncludeInactiveEmp = True
                End If


                dgEmployeeList.Columns.Clear()


                'S.SANDEEP [29 SEP 2016] -- START
                'ENHANCEMENT : DEFINING ENUM FOR EMPLYOEE FEILD, LIST TAKING LONG TIME TO FILL
                Dim StrCheck_Fields As String = String.Empty
                StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status
                If IsNumeric(ConfigParameter._Object._Checked_Fields.Split(CChar(","))(0)) = False Then
                    ConfigParameter._Object._Checked_Fields = StrCheck_Fields
                    Save_List_Headers(StrCheck_Fields)
                End If
                'S.SANDEEP [29 SEP 2016] -- END

                dsAccess = objEmployee.GetListForDynamicField(ConfigParameter._Object._Checked_Fields, _
                                                              FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, False, _
                                                              blnIncludeInactiveEmp, "List", , , StrSearching, _
                                                              ConfigParameter._Object._ShowFirstAppointmentDate, _
                                                              User._Object.Privilege._AllowTo_View_Scale, mblnIsFromMail, False) 'S.SANDEEP [29 SEP 2016] -- START {ConfigParameter._Object._Checked_Fields} -- END




                dtTable = dsAccess.Tables(0)
                dgEmployeeList.DataSource = dtTable


                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                If dgEmployeeList.ColumnCount > 0 Then
                    dgEmployeeList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
                    dgEmployeeList.AllowUserToResizeRows = False
                    dgEmployeeList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
                    If dgEmployeeList.Columns.Contains("ischeck") Then
                        dgEmployeeList.Columns("ischeck").Resizable = DataGridViewTriState.False
                    End If
                End If
                'S.SANDEEP [25 OCT 2016] -- END




                'S.SANDEEP [29 SEP 2016] -- START
                'ENHANCEMENT : DEFINING ENUM FOR EMPLYOEE FEILD, LIST TAKING LONG TIME TO FILL
                If dgEmployeeList.ColumnCount > 0 Then
                    If dgEmployeeList.Columns.Contains("employeeunkid") Then
                        dgEmployeeList.Columns("employeeunkid").Visible = False
                    End If


                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    If dgEmployeeList.Columns.Contains("Present_isocode") Then
                        dgEmployeeList.Columns("Present_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Present_isosname") Then
                        dgEmployeeList.Columns("Present_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Domicile_isocode") Then
                        dgEmployeeList.Columns("Domicile_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Domicile_isosname") Then
                        dgEmployeeList.Columns("Domicile_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Recruitment_isocode") Then
                        dgEmployeeList.Columns("Recruitment_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Recruitment_isosname") Then
                        dgEmployeeList.Columns("Recruitment_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Resident_isocode") Then
                        dgEmployeeList.Columns("Resident_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Resident_isosname") Then
                        dgEmployeeList.Columns("Resident_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Birth_isocode") Then
                        dgEmployeeList.Columns("Birth_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Birth_isosname") Then
                        dgEmployeeList.Columns("Birth_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Work_isocode") Then
                        dgEmployeeList.Columns("Work_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Work_isosname") Then
                        dgEmployeeList.Columns("Work_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Emergency_isocode") Then
                        dgEmployeeList.Columns("Emergency_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Emergency_isosname") Then
                        dgEmployeeList.Columns("Emergency_isosname").Visible = False
                    End If
                   
                    If dgEmployeeList.Columns.Contains("Emer2_isocode") Then
                        dgEmployeeList.Columns("Emer2_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Emer2_isosname") Then
                        dgEmployeeList.Columns("Emer2_isosname").Visible = False
                    End If

                    If dgEmployeeList.Columns.Contains("Emer3_isocode") Then
                        dgEmployeeList.Columns("Emer3_isocode").Visible = False
                    End If
                    If dgEmployeeList.Columns.Contains("Emer3_isosname") Then
                        dgEmployeeList.Columns("Emer3_isosname").Visible = False
                    End If

                    'Pinkal (18-Aug-2018) -- End


                End If
                'Dim xColValue() As String = Nothing
                'If mstrChkField.Trim.Length > 0 Then
                '    xColValue = mstrChkField.Trim.Split(CChar(","))
                'End If

                'For Each dcColumn As DataGridViewColumn In dgEmployeeList.Columns
                '    If Array.IndexOf(xColValue, dcColumn.HeaderText) > -1 OrElse dcColumn.HeaderText = "ischeck" Then
                '        dcColumn.Visible = True
                '    Else
                '        dcColumn.Visible = False
                '    End If
                '    If dcColumn.HeaderText <> "ischeck" Then
                '        dcColumn.ReadOnly = True
                '    Else
                '        dcColumn.ReadOnly = False
                '        dcColumn.Frozen = True
                '        objChkAll.Visible = True
                '    End If
                '    If dcColumn.DataPropertyName <> "employeeunkid" AndAlso dcColumn.DataPropertyName <> "ischeck" Then
                '        dcColumn.Name = "objdgcolh" & dcColumn.HeaderText.ToString().Replace(" ", "_")
                '    End If
                '    dcColumn.SortMode = DataGridViewColumnSortMode.Automatic
                'Next

                'Array.Clear(xColValue, 0, xColValue.Length)
                'xColValue = Nothing
                'S.SANDEEP [29 SEP 2016] -- END



                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                If mblnExport_Print Or mblnIsFromMail Then
                    dgEmployeeList.Columns(0).ReadOnly = False
                End If
                'S.SANDEEP [25 OCT 2016] -- END



                'For Each drRow As DataRow In dtTable.Rows

                '    If CBool(drRow.Item("isclear")) = True Then
                '        lvItem.BackColor = Color.Red
                '        lvItem.ForeColor = Color.Yellow
                '    End If

                'Next

                AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillListForDynamicColumn", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim mdtTable As DataTable = CType(dgEmployeeList.DataSource, DataTable)
            Dim drRow As DataRow() = mdtTable.Select("ischeck = True")

            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            If drRow.Length <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployeeList.Rows.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployeeList.Rows.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If

            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (17-Dec-2015) -- End

    Private Function Save_List_Headers(ByVal StrChkData As String) As Boolean
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            objConfig._Checked_Fields = StrChkData
            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Save_List_Headers", mstrModuleName)
            Return False
        End Try
    End Function

    'S.SANDEEP [19-JAN-2017] -- START
    'ISSUE/ENHANCEMENT : Listing Employee(s) From the Employee Listing Screen
    Private Function Update_DataRow(ByVal dr As DataRow) As Boolean
        Try
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 48, "Appointed Date"))) = False AndAlso _
                    dr.Item(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 48, "Appointed Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 54, "Birth Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 54, "Birth Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 54, "Birth Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 54, "Birth Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 54, "Birth Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 54, "Birth Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 114, "Probation To Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 114, "Probation To Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 114, "Probation To Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 114, "Probation To Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 114, "Probation To Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 114, "Probation To Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 116, "Retirement Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 116, "Retirement Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 158, "EOC Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 158, "EOC Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 158, "EOC Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 158, "EOC Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 158, "EOC Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 158, "EOC Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 115, "Leaving Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 115, "Leaving Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 113, "Probation From Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 113, "Probation From Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 113, "Probation From Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 113, "Probation From Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 113, "Probation From Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 113, "Probation From Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date")).ToString).ToShortDateString
                End If
            End If
            If dr.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")) Then
                If IsDBNull(dr.Item(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date"))) = False AndAlso _
                dr.Item(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")).ToString.Trim.Length > 0 Then
                    If IsDate(dr.Item(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date"))) = False Then dr.Item(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")) = eZeeDate.convertDate(dr.Item(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")).ToString).ToShortDateString
                End If
            End If
            dr.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [19-JAN-2017] -- END


#End Region

#Region " Form's Events "

    Private Sub frmEmployeeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
        'If e.KeyCode = Keys.Delete And lvEmployeeList.Focused = True Then
        ' Call btnDelete.PerformClick()
        ' End If

        If e.KeyCode = Keys.Delete And dgEmployeeList.Focused = True Then
            Call btnDelete.PerformClick()
        End If

        'Pinkal (17-Dec-2015) -- End


    End Sub

    Private Sub frmEmployeeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployee = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            If mblnIsFromMail = True Then

                'Pinkal (17-Dec-2015) -- Start
                'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
                'lvEmployeeList.CheckBoxes = True
                ' objcolhCheck.Width = 25
                objChkAll.Visible = True
                'Pinkal (17-Dec-2015) -- End

                lnkAddRemoveField.Visible = False
            Else

                'Pinkal (17-Dec-2015) -- Start
                'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
                'lvEmployeeList.CheckBoxes = False
                objChkAll.Visible = False
                'objcolhCheck.Width = 0
                ' colhEmployeeName.Width = colhEmployeeName.Width + 20
                'Pinkal (17-Dec-2015) -- End
            End If

            Call FillCombo()

            'Call fillList()



            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'If Me._DataView.Tables.Count <> 0 Then
            '    Call GetCheckedData()
            'End If
            'Pinkal (17-Dec-2015) -- End



            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
                Dim dsMachineSetting As New DataSet
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")
                If dsMachineSetting.Tables.Count > 0 Then
                    For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1
                        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 1 Then    'FingerPrint
                            mnuEnrollFP.Text = Language.getMessage(mstrModuleName, 1, "Enroll Employee Finger Print")
                            mnuDeleteFP.Text = Language.getMessage(mstrModuleName, 2, "Delete Employee Enroll Finger Print")

                        ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 2 Then  'RFID
                            mnuEnrollFP.Text = Language.getMessage(mstrModuleName, 3, "Enroll Employee Card")
                            mnuDeleteFP.Text = Language.getMessage(mstrModuleName, 4, "Delete Employee Enroll Card")
                        End If
                    Next
                End If
            Else
                mnuEnrollFP.Visible = False
                mnuDeleteFP.Visible = False
            End If

            Call SetVisibility()


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'lvEmployeeList.Columns.Clear()
            'lvEmployeeList.GridLines = False

            'If lvEmployeeList.Items.Count > 0 Then lvEmployeeList.Items(0).Selected = True
            'lvEmployeeList.Select()

            'Pinkal (17-Dec-2015) -- End

            'S.SANDEEP [14 MAR 2015] -- START
            chkShowActiveEmployee.CheckState = CheckState.Checked
            'S.SANDEEP [14 MAR 2015] -- END

            'S.SANDEEP [09 APR 2015] -- START
            cboEmployee.Select()
            'S.SANDEEP [09 APR 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployee = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    objEmployee.Delete(CInt(lvEmployeeList.SelectedItems(0).Tag))
            '    If objEmployee._Message <> "" Then
            '        eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            '    lvEmployeeList.SelectedItems(0).Remove()

            '    If lvEmployeeList.Items.Count <= 0 Then
            '        Exit Try
            '    End If
            'End If
            'lvEmployeeList.Select()


            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objEmployee.Delete(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value))
                If objEmployee._Message <> "" Then
                    eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            dgEmployeeList.Focus()

            'Pinkal (17-Dec-2015) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        'Anjan [19 February 2015] -- Start
        'Issue : Removed this checkpoint as once employee was in clearance status it was not getting reinstated again.
        'Sohail (04 Jan 2012) -- Start
        'If lvEmployeeList.SelectedItems(0).BackColor = Color.Red Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! Employee Clearance is done for this employee."), enMsgBoxStyle.Information) '?1
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If
        'Sohail (04 Jan 2012) -- End

        'Anjan [19 February 2015] -- End


        Dim frm As New frmEmployeeMaster
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If frm.displayDialog(CInt(lvEmployeeList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            '    Call FillListForDynamicColumn(GetDynamicField())
            'End If
            'frm = Nothing

            'If lvEmployeeList.Items.Count <= 0 Then Exit Try
            If frm.displayDialog(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value), enAction.EDIT_ONE) Then
                Call FillListForDynamicColumn(GetDynamicField())
            End If

            frm = Nothing

            If dgEmployeeList.RowCount <= 0 Then Exit Try

            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeMaster
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                'Pinkal (14-Aug-2012) -- Start
                'Enhancement : TRA Changes

                'S.SANDEEP [06 MAY 2015] -- START
                'TEMP - SOL - DATA READER
                'Call FillListForDynamicColumn(GetDynamicField())
                'S.SANDEEP [06 MAY 2015] -- END

                'Pinkal (14-Aug-2012) -- End
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillListForDynamicColumn(GetDynamicField())

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'Call objbtnSearch.ShowResult(CStr(lvEmployeeList.Items.Count))
            Call objbtnSearch.ShowResult(CStr(dgEmployeeList.RowCount))
            'Pinkal (17-Dec-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtEmployeeCode.Text = ""
            txtFirstname.Text = ""
            txtOtherName.Text = ""
            txtSurname.Text = ""
            cboDepartment.SelectedValue = 0
            cboEmploymentType.SelectedValue = 0
            cboShiftInfo.SelectedValue = 0
            cboJob.SelectedValue = 0
            dtpDateFrom.Checked = False
            dtpDateTo.Checked = False
            cboEmployeeStatus.SelectedIndex = 0
            radProbated.Checked = False
            radSuspended.Checked = False
            radTerminated.Checked = False
            cboBranch.SelectedValue = 0
            mstrAdvanceFilter = ""
            'S.SANDEEP [14 MAR 2015] -- START
            cboEmployee.SelectedValue = 0
            chkShowActiveEmployee.Checked = True
            'S.SANDEEP [14 MAR 2015] -- END

            Call FillListForDynamicColumn(GetDynamicField())


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'Call objbtnSearch.ShowResult(CStr(lvEmployeeList.Items.Count))
            Call objbtnSearch.ShowResult(CStr(dgEmployeeList.RowCount))
            'Pinkal (17-Dec-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMailClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMailClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If mblnExport_Print = False Then
                GetEmployeeEmail()
            Else
                GetEmployeePrint_Export_Letter()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                'S.SANDEEP [09 APR 2015] -- START
                'cboEmployee.Focus()
                'S.SANDEEP [09 APR 2015] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnFieldClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFieldClose.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            FillListForDynamicColumn(GetDynamicField())
            gbColumns.Visible = False

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'lvEmployeeList.Enabled = True
            dgEmployeeList.Enabled = True
            'Pinkal (17-Dec-2015) -- End

            gbFilterCriteria.Enabled = True
            objefemailFooter.Enabled = True
            objFooter.Enabled = True
            mblnIsDynamicField = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFieldClose_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim StrCheckedColumns As String = String.Empty
            StrCheckedColumns = GetDynamicField()
            If StrCheckedColumns.Trim.Length > 0 Then
                If Save_List_Headers(StrCheckedColumns) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Problem in saving checked fields data."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please check atleast one field in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call btnFieldClose_Click(sender, e)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'S.SANDEEP [19-JAN-2017] -- START
    'ISSUE/ENHANCEMENT : Listing Employee(s) From the Employee Listing Screen
    Private Sub btnExportList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportList.Click
        Try
            If dgEmployeeList.Columns.Count > 0 Then
                Dim row = dgEmployeeList.Columns.OfType(Of DataGridViewColumn).Where(Function(x) x.Visible = True).Select(Function(y) y.HeaderText).ToArray()
                If row.Count > 0 Then
                    Dim xTab As DataTable = CType(dgEmployeeList.DataSource, DataTable).DefaultView.ToTable("List", False, row)
                    If xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 48, "Appointed Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 47, "First Appointed Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 54, "Birth Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 65, "Annivesary Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 111, "Suspended From Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 112, "Suspended To Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 113, "Probation From Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 114, "Probation To Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date")) OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 158, "EOC Date")) _
                       OrElse xTab.Columns.Contains(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")) Then
                        Dim dRows As DataRow() = xTab.Select("")
                        dRows.ToList.ForEach(Function(x) Update_DataRow(x))
                    End If

                    Dim StrFilter As String = String.Empty
                    StrFilter &= "," & Language.getMessage(mstrModuleName, 105, "Employee As On Date") & " : " & gfrmMDI.dtpEmployeeAsOnDate.Value.Date.ToShortDateString
                    StrFilter &= "," & Language.getMessage(mstrModuleName, 104, "Total Employee") & " : " & dgEmployeeList.RowCount.ToString
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        StrFilter &= "," & lblEmployee.Text & " : " & cboEmployee.Text
                    End If
                    If CInt(cboBranch.SelectedValue) > 0 Then
                        StrFilter &= "," & lblBranch.Text & " : " & cboBranch.Text
                    End If
                    If CInt(cboDepartment.SelectedValue) > 0 Then
                        StrFilter &= "," & lblDepartment.Text & " : " & cboDepartment.Text
                    End If
                    If CInt(cboJob.SelectedValue) > 0 Then
                        StrFilter &= "," & lblJob.Text & " : " & cboJob.Text
                    End If
                    If radTerminated.Checked = True Then
                        StrFilter &= "," & radTerminated.Text
                    End If
                    If radSuspended.Checked = True Then
                        StrFilter &= "," & radSuspended.Text
                    End If
                    If radProbated.Checked = True Then
                        StrFilter &= "," & radProbated.Text
                    End If
                    If dtpDateFrom.Checked = True AndAlso dtpDateTo.Checked = True Then
                        StrFilter &= "," & Language.getMessage(mstrModuleName, 100, "From Date:") & " " & dtpDateFrom.Value.Date.ToShortDateString & " " & Language.getMessage(mstrModuleName, 101, "To") & " " & dtpDateTo.Value.Date.ToShortDateString
                        'ElseIf dtpDateFrom.Checked = True Then
                        '    StrFilter &= "," & Language.getMessage(mstrModuleName, 100, "From Date:") & " " & dtpDateFrom.Value.Date.ToShortDateString & " "
                        'ElseIf dtpDateTo.Checked = True Then
                        '    StrFilter &= "," & Language.getMessage(mstrModuleName, 102, "Date To:") & " " & dtpDateTo.Value.Date.ToShortDateString & " "
                    End If
                    If txtOtherName.Text.Trim.Length > 0 Then
                        StrFilter &= "," & lblOthername.Text & " : " & txtOtherName.Text
                    End If
                    If CInt(cboEmploymentType.SelectedValue) > 0 Then
                        StrFilter &= "," & lblEmploymentType.Text & " : " & cboEmploymentType.Text
                    End If
                    If txtEmployeeCode.Text.Trim.Length > 0 Then
                        StrFilter &= "," & lblEmployeeCode.Text & " : " & txtEmployeeCode.Text
                    End If
                    If chkShowActiveEmployee.Checked = True Then
                        StrFilter &= "," & chkShowActiveEmployee.Text
                    End If
                    If cboEmployeeStatus.SelectedIndex > 0 Then
                        StrFilter &= "," & lblStatus.Text & " : " & cboEmployeeStatus.Text
                    End If
                    If StrFilter.Trim.Length > 0 Then StrFilter = Mid(StrFilter, 2)
                    Dim objEmpListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                    If objEmpListing.ListEmployee(StrFilter, xTab, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, Me.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Employee List exported successfully to the given path."), enMsgBoxStyle.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportList_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19-JAN-2017] -- END

#End Region

#Region " Menu Events "

    Private Sub mnuScanDocuments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanDocuments.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.


            ''Nilay (03-Dec-2015) -- Start
            ''ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
            'If lvEmployeeList.SelectedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            ''frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "")
            'frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", _
            '                  CInt(lvEmployeeList.SelectedItems(0).Tag), , , True)
            ''Nilay (03-Dec-2015) -- End

            If dgEmployeeList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "")

            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", _
            '                  mstrModuleName, CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value), , , True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", _
                              mstrModuleName, True, CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value), , , True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END

            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanDocuments_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuImportEligibleApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEligibleApplicant.Click
        Try
            Dim ObjFrm As New frmImport_Applicants
            ObjFrm.ShowDialog()
            Call FillListForDynamicColumn(GetDynamicField())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEligibleApplicant_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuDeleteEnrolledCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeleteEnrolledCard.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim objSwipeCard As New clsSwipeCardData

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'objSwipeCard.Delete(CInt(lvEmployeeList.SelectedItems(0).Tag))
            objSwipeCard.Delete(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value))
            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDeleteEnrolledCard_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEnrollNewCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnrollNewCard.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim objfrm As New frmSwipeCardData

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'Call objfrm.displayDialog(CInt(lvEmployeeList.SelectedItems(0).Tag))
            Call objfrm.displayDialog(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value))
            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEnrollNewCard_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmployee.Click
        Try
            Dim frm As New frmEmpImportWizard
            frm.ShowDialog()
            Call FillListForDynamicColumn(GetDynamicField())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEnrollFP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnrollFP.Click
        Try
            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then

                Dim objfrm As New frmfingerPrint_enroll
                Dim dsMachineSetting As New DataSet
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")
                If dsMachineSetting.Tables.Count > 0 Then
                    For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1
                        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 1 Then    'FingerPrint
                            objfrm.Text = Language.getMessage(mstrModuleName, 1, "Enroll Employee Finger Print")
                        ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 2 Then  'RFID
                            objfrm.Text = Language.getMessage(mstrModuleName, 3, "Enroll Employee Card")
                        End If
                    Next
                    objfrm.displayDialog(-1, enAction.ADD_CONTINUE)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEnrollFP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuDeleteFP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeleteFP.Click
        Try

            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
                Dim dsMachineSetting As New DataSet
                Dim objfrm As New frmDeletefingerPrint
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")
                If dsMachineSetting.Tables.Count > 0 Then
                    For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1
                        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 1 Then    'FingerPrint
                            objfrm.Text = Language.getMessage(mstrModuleName, 2, "Delete Employee Enroll Finger Print")
                        ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 2 Then  'RFID
                            objfrm.Text = Language.getMessage(mstrModuleName, 4, "Delete Employee Enroll Card")
                        End If
                    Next

                    objfrm.displayDialog(-1, enAction.ADD_CONTINUE)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDeleteFP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEditImportedEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditImportedEmployee.Click
        Try
            Dim frm As New frmEditImportedEmployee
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEditImportedEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportMembership.Click
        Try
            Dim frm As New frmImportMemberships
            frm.ShowDialog()
            Call FillListForDynamicColumn(GetDynamicField())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportMembership_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGetMembershipTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetMembershipTemplate.Click
        Dim objMembership As New clsMembershipTran
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'strFilePath = ObjFile.DirectoryName & "\"
                'strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'strFilePath &= ObjFile.Extension
                strFilePath = dlgSaveFile.FileName
                dsList = objMembership.GetBlankTemplate("List")
                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XML
                        dsList.WriteXml(strFilePath)
                    Case 2   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetMembershipTemplate_Click", mstrModuleName)
        Finally
            objMembership = Nothing
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    Private Sub mnuViewDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewDiary.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End
        Try
            Dim frm As New frmEmployeeDiary

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm.displayDialog(CInt(lvEmployeeList.SelectedItems(0).Tag))
            frm.displayDialog(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value))
            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewDiary_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuEmployeeDependents_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployeeDependents.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmDependantsAndBeneficiariesList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEmployeeDependents_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeExperiences_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmployeeExperiences.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Select()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmJobHistory_ExperienceList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End

            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEmployeeExperiences_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeQualifications_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmployeeQualifications.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End
        Try
            Dim frm As New frmQualificationsList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End


            frm.ShowDialog()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEmployeeQualifications_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeReferences_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmployeeReferences.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmEmployeeRefereeList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End

            frm.ShowDialog()


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEmployeeReferences_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeSkills_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmployeeSkills.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmEmployee_Skill_List

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End


            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEmployeeSkills_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuAssignedCompanyAssets_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAssignedCompanyAssets.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmAssetsRegisterList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End


            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuAssignedCompanyAssets_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEmployeeBenefits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEmployeeBenefits.Click

        'Pinkal (17-Dec-2015) -- Start
        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

        'If lvEmployeeList.SelectedItems.Count <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '    lvEmployeeList.Select()
        '    Exit Sub
        'End If

        If dgEmployeeList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
            dgEmployeeList.Focus()
            Exit Sub
        End If

        'Pinkal (17-Dec-2015) -- End

        Try
            Dim frm As New frmBenefitCoverage_List

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm._EmployeeUnkid = Convert.ToInt32(lvEmployeeList.SelectedItems(0).Tag)
            frm._EmployeeUnkid = Convert.ToInt32(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            'Pinkal (17-Dec-2015) -- End

            frm.ShowDialog()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuEmployeeBenefits_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuList.Click
        Try
            Dim dsTab As DataSet
            Dim objReporting As New clsReportingToEmployee
            dsTab = objReporting.Get_UnReportingEmp("List", ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._EmployeeAsOnDate)
            If dsTab.Tables(0).Rows.Count > 0 Then
                Dim dlgSaveFile As New SaveFileDialog
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim IExcel As New ExcelData
                'S.SANDEEP [12-Jan-2018] -- END

                dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
                If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                    'S.SANDEEP [12-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 0001843
                    'IExcel.Export(dlgSaveFile.FileName, dsTab)
                    OpenXML_Export(dlgSaveFile.FileName, dsTab)
                    'S.SANDEEP [12-Jan-2018] -- END
                    If IO.File.Exists(dlgSaveFile.FileName) Then
                        Process.Start(dlgSaveFile.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuList_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSetReportingTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetReportingTo.Click
        Try

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.SelectedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
            '    lvEmployeeList.Select()
            '    Exit Sub
            'End If

            If dgEmployeeList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information)
                dgEmployeeList.Focus()
                Exit Sub
            End If

            'Pinkal (17-Dec-2015) -- End

            Dim frm As New frmReportingToEmp

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'frm.displayDialog(CInt(lvEmployeeList.SelectedItems(0).Tag))
            frm.displayDialog(CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value))
            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "mnuSetReportingTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportReportingTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportReportingTo.Click
        Dim frm As New frmImportReportTo
        Try
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportReportingTo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuGetFileFormat_ReportTO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat_ReportTO.Click
        Dim objReportTo As New clsReportingToEmployee
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                dsList = objReportTo.Get_File("List")
                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XML
                        dsList.WriteXml(strFilePath)
                    Case 2   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_ReportTO_Click", mstrModuleName)
        Finally
            objReportTo = Nothing
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    Private Sub mnuImportToDatabase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuImportToDatabase.Click
        Dim frm As New frmImageWizard
        Try
            frm.displayDialog(frmImageWizard.enImageMode.IMG_EMPLOYEE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportToDatabase_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPhotoFile.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable
                dTable.Columns.Add("Employeecode") : dTable.Columns.Add("Photo_Path") : dTable.Columns.Add("Photo_Name")
                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub

    Private Sub mnuImportEmpIdentities_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmpIdentities.Click
        Try
            Dim frm As New frmImportIdentities
            frm.ShowDialog()
            Call FillListForDynamicColumn(GetDynamicField())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEmpIdentities_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuAssign_Shift_Policy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssign_Shift_Policy.Click
        Dim frm As New frmShiftPolicyGlobalAssign
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignShift_Policy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuVAssigned_Shift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVAssigned_Shift.Click
        Dim frm As New frmViewAssigned_Shift
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVAssigned_Shift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuVAssigned_Policy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVAssigned_Policy.Click
        Dim frm As New frmViewAssigned_Policy
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVAssigned_Policy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try

            'Shani(19-Nov-2015) -- Start
            'ENHANCEMENT : ADD/REMOVE Dynamic Fileds.
            'If txtSearch.Text.Trim.Length > 0 Then
            '    Dim idx As Integer = chkEmpfields.FindString(txtSearch.Text, -1)
            '    If idx <> -1 Then
            '        chkEmpfields.SetSelected(idx, False)
            '        chkEmpfields.SelectedItem = chkEmpfields.Items(idx)
            '    End If
            'End If

            Dim StrSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                StrSearch = "name like '%" & txtSearch.Text.Trim & "%'"
            End If
            dtView.RowFilter = StrSearch
            dgvchkEmpfields.DataSource = dtView
            'Shani(19-Nov-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuAssigndayoff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssigndayoff.Click
        Dim frm As New frmEmpDayOffList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssigndayoff_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuUpdateImpDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdateImpDetails.Click
        Dim frm As New frmBulkUpdateDataWizard
        frm.ShowDialog()
    End Sub

    Private Sub mnuTransfers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTransfers.Click
        Dim frm As New frmEmployeeTransfers

        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeUnkid = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTransfers_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuRecategorize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRecategorize.Click
        Dim frm As New frmEmployeeRecategorize

        'Gajanan [11-Dec-2019] -- Start   
        'Enhancement:Worked On December Cut Over Enhancement For NMB
        If dgEmployeeList.SelectedRows.Count > 0 Then
            frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
        End If
        'Gajanan [11-Dec-2019] -- End

        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRecategorize_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuProbation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuProbation.Click
        Dim frm As New frmEmployeeDates
        'Gajanan [11-Dec-2019] -- Start   
        'Enhancement:Worked On December Cut Over Enhancement For NMB
        If dgEmployeeList.SelectedRows.Count > 0 Then
            frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
        End If
        'Gajanan [11-Dec-2019] -- End
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_PROBATION
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuProbation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuConfirmation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConfirmation.Click
        Dim frm As New frmEmployeeDates
        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuConfirmation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuSuspension_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSuspension.Click
        Dim frm As New frmEmployeeDates
        Try

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_SUSPENSION
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSuspension_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuTermination_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTermination.Click
        Dim frm As New frmEmployeeDates
        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_TERMINATION
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTermination_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuRetirements_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRetirements.Click
        Dim frm As New frmEmployeeDates
        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_RETIREMENT
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRetirements_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuWorkPermit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWorkPermit.Click, mnuResidentPermit.Click 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {mnuResidentPermit} -- END
        Dim frm As New frmWorkPermitTransaction
        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            'frm.ShowDialog()
            Dim blnIsResidentPermit As Boolean = False
            If CType(sender, ToolStripMenuItem).Name.ToUpper = mnuResidentPermit.Name.ToUpper Then
                blnIsResidentPermit = True
            End If
            frm.displayDialog(blnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuWorkPermit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCostCenter.Click
        Dim frm As New frmEmployeeOtherDetails
        Try

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._IsCostCenter = True
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCostCenter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuReHire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReHire.Click
        Dim frm As New frmEmployeeRehireList
        Try
            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuReHire_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuImpPPLSoftData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImpPPLSoftData.Click
        Dim frm As New frmPeoplesoftMapping
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImpPPLSoftData_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Shani(18-Jan-2016) -- Start
    Private Sub mnuUpdateEmployeeMovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdateEmployeeMovement.Click
        Dim frm As New frmBulkMovementUpdateWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUpdateEmployeeMovement_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani(18-Jan-2016) -- End


    'Shani(06-APR-2016) -- Start
    'Enhancement : new form for Global Void Membership 
    Private Sub mnuGlobalVoidMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalVoidMembership.Click
        Dim frm As New frmMembershipGlobalVoid
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalVoidMembership_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani(06-APR-2016) -- End 

    'Nilay (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Private Sub mnuAssignSalaryAnniversaryMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAssignSalaryAnniversaryMonth.Click
        Try
            Dim frm As New frmEmployeeSalaryAnniversaryMonth_List

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignSalaryAnniversaryMonth_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (27 Apr 2016) -- End

    'Shani(13-JUL-2016) -- Start
    Private Sub mnuImportAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuImportAddress.Click
        Dim frm As New frmAddressImportWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportAddress_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuImportBirthInformation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuImportBirthInformation.Click
        Dim frm As New frmBirthInformationImportWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportBirthInformation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani(13-JUL-2016) -- End

    'Pinkal (10-Aug-2016) -- Start
    'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.

    Private Sub mnuImportEmpShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmpShift.Click

        'S.SANDEEP |29-MAR-2019| -- START
        'ENHANCEMENT : 0002803 {PAPAYE}.
        'Dim frm As New frmEmployeeShiftImportWizard
        Dim frm As New frmImportEmployeeShift
        'S.SANDEEP |29-MAR-2019| -- END
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportEmpShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (10-Aug-2016) -- End


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    Private Sub mnuSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitForApproval.Click
        Try
            Dim objFrmsubmitForApproval As New frmEmpSubmitForApproval
            objFrmsubmitForApproval.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitForApproval_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [20-JUN-2018] -- End

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private Sub mnuApproverLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproverLevel.Click
        Dim frm As New frmEmpApproverLevelList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._ApproverType = enEmpApproverType.APPR_MOVEMENT
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproverLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuMovementApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMovementApprover.Click
        Dim frm As New frmEmployee_ApproverList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._ApproverType = enEmpApproverType.APPR_MOVEMENT
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMovementApprover_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuApproveRejectMovments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveRejectMovments.Click
        Dim frm As New frmAppRejEmpMovement
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproveRejectMovments_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [20-JUN-2018] -- END

    'S.SANDEEP [09-AUG-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#292}
    Private Sub mnuFlexcubeFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFlexcubeData.Click
        'S.SANDEEP |25-NOV-2019| -- START
        'ISSUE/ENHANCEMENT : FlexCube
        'Dim frm As New frmFlexcubeFileGeneration
        Dim frm As New frmFlexcubeErrorView
        'S.SANDEEP |25-NOV-2019| -- END
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuFlexcubeFile_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [09-AUG-2018] -- END

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private Sub mnuExemption_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExemption.Click
        Dim frm As New frmEmployeeDates
        Try

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If dgEmployeeList.SelectedRows.Count > 0 Then
                frm._EmployeeId = CInt(dgEmployeeList.SelectedRows(0).Cells("employeeunkid").Value)
            End If
            'Gajanan [11-Dec-2019] -- End
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._DateTypeId = enEmp_Dates_Transaction.DT_EXEMPTION
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExemption_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (21 Oct 2019) -- End

    Private Sub mnuOrbitRequest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOrbitRequest.Click
        Dim frm As New frmOrbitRequest
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuOrbitRequest_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    lvItem.Checked = CBool(objChkAll.CheckState)
            'Next
            'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked

            Dim dtTable As DataTable = CType(dgEmployeeList.DataSource, DataTable)
            If dtTable.Rows.Count > 0 Then
                RemoveHandler dgEmployeeList.CellContentClick, AddressOf dgEmployeeList_CellContentClick
                RemoveHandler dgEmployeeList.CellContentDoubleClick, AddressOf dgEmployeeList_CellContentClick
                For Each dr As DataRow In dtTable.Rows
                    dr("ischeck") = objChkAll.Checked
                    dr.EndEdit()
                Next
                dtTable.AcceptChanges()
                AddHandler dgEmployeeList.CellContentClick, AddressOf dgEmployeeList_CellContentClick
                AddHandler dgEmployeeList.CellContentDoubleClick, AddressOf dgEmployeeList_CellContentClick
            End If

            'Pinkal (17-Dec-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkShowActiveEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowActiveEmployee.CheckedChanged
        Try
            If chkShowActiveEmployee.Checked = True Then radTerminated.Checked = False
            radTerminated.Enabled = Not chkShowActiveEmployee.Checked

            Dim dsCombos As New DataSet
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   False, Not chkShowActiveEmployee.Checked, "List", True, , , , , , , , , , , , , , , , , , False)
            'S.SANDEEP [04 JUN 2015] -- END



            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                'S.SANDEEP [04 APR 2015] -- START
                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
                'S.SANDEEP [04 APR 2015] -- END
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowActiveEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(19-Nov-2015) -- Start
    'ENHANCEMENT : ADD/REMOVE Dynamic Fileds.
    Private Sub chkAllChecked_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllChecked.CheckedChanged
        For Each rowview As DataRowView In dtView
            rowview.Item("IsChecked") = CBool(chkAllChecked.CheckState)
        Next
        dtView.Table.AcceptChanges()
    End Sub
    'Shani(19-Nov-2015) -- End

#End Region

#Region " Listview Events "

    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            'If lvEmployeeList.CheckedItems.Count <= 0 Then
            '    objChkAll.CheckState = CheckState.Unchecked
            'ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
            '    objChkAll.CheckState = CheckState.Indeterminate
            'ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
            '    objChkAll.CheckState = CheckState.Checked
            'End If

            'Pinkal (17-Dec-2015) -- End

            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvEmployeeList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.Enter Then

                'Pinkal (17-Dec-2015) -- Start
                'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
                'If lvEmployeeList.SelectedItems.Count > 0 Then
                '    Call btnEdit_Click(New Object, New EventArgs)
                'End If
                'Pinkal (17-Dec-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call FillListForDynamicColumn(GetDynamicField())
            End If
            If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""

            'Pinkal (17-Dec-2015) -- Start
            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            'lvEmployeeList.Focus()
            'If lvEmployeeList.Items.Count > 0 Then lvEmployeeList.Items(0).Selected = True

            dgEmployeeList.Focus()
            If dgEmployeeList.RowCount > 0 Then dgEmployeeList.SelectedRows(0).Selected = True

            'Pinkal (17-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'S.SANDEEP [04 JUN 2015] -- END
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAddRemoveField_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddRemoveField.LinkClicked
        Try
            Me.Cursor = Cursors.WaitCursor


            'Shani(19-Nov-2015) -- Start
            'ENHANCEMENT : ADD/REMOVE Dynamic Fileds.
            'If chkEmpfields.Items.Count <= 0 Then
            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'Dim dsColumns As DataSet = objEmployee.GetListForDynamicField("EmpList", , False).Clone
            '    Dim dsColumns As DataSet = objEmployee.GetListForDynamicField(FinancialYear._Object._DatabaseName, _
            '                                                                  User._Object._Userunkid, _
            '                                                                  FinancialYear._Object._YearUnkid, _
            '                                                                  Company._Object._Companyunkid, _
            '                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                  ConfigParameter._Object._UserAccessModeSetting, True, False, "EmpList", , , , _
            '                                                                  ConfigParameter._Object._ShowFirstAppointmentDate, User._Object.Privilege._AllowTo_View_Scale).Clone
            '    'S.SANDEEP [04 JUN 2015] -- END



            '    'S.SANDEEP [ 18 AUG 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If ConfigParameter._Object._Checked_Fields.Trim.Length <= 0 Then
            '        Dim StrCheck_Fields As String = String.Empty
            '        StrCheck_Fields = "Code,Employee Name,Department,Job,Shift,Employement Type,Email"
            '        Call Save_List_Headers(StrCheck_Fields)
            '    End If
            '    Dim StrSeparatedValue() As String = ConfigParameter._Object._Checked_Fields.ToString.Split(CChar(","))
            '    'S.SANDEEP [ 18 AUG 2012 ] -- END

            '    For Each dr As DataColumn In dsColumns.Tables(0).Columns

            '        'S.SANDEEP [ 18 SEP 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'If dr.ColumnName.Trim.ToString().Contains("id") Or dr.ColumnName.Trim.ToString().Contains("Password") Or _
            '        '   dr.ColumnName.Trim.ToString().Contains("First Name") Or dr.ColumnName.Trim.ToString().Contains("Surname") Or dr.ColumnName.Trim.ToString().Contains("Other Name") Or _
            '        '   dr.ColumnName.Trim.ToString().Contains("isclear") Or dr.ColumnName.Trim.ToString().Contains("isactive") Then Continue For
            '        If dr.ColumnName.Trim.ToString().Contains("id") Or dr.ColumnName.Trim.ToString().Contains("Password") Or _
            '           dr.ColumnName.Trim.ToString().Contains("First Name") Or dr.ColumnName.Trim.ToString().Contains("Surname") Or dr.ColumnName.Trim.ToString().Contains("Other Name") Or _
            '           dr.ColumnName.Trim.ToString().Contains("isclear") Or dr.ColumnName.Trim.ToString().Contains("isactive") Or dr.ColumnName.Trim.ToString().Contains("isapproved") Or _
            '           dr.ColumnName.Trim.ToString().Contains("gender") Then Continue For 'S.SANDEEP [ 23 MAY 2014 ] -- START -- END
            '        'S.SANDEEP [ 18 SEP 2012 ] -- END




            '        'S.SANDEEP [ 18 AUG 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'If dr.ColumnName = "Code" Or dr.ColumnName = "Employee Name" Or dr.ColumnName = "Department" Or dr.ColumnName = "Job" Or dr.ColumnName = "Shift" Or dr.ColumnName = "Employement Type" Or dr.ColumnName = "Email" Then
            '        '    chkEmpfields.Items.Add(dr.ColumnName, True)
            '        'Else
            '        '    chkEmpfields.Items.Add(dr.ColumnName, False)
            '        'End If
            '        If StrSeparatedValue.Length > 0 Then
            '            Dim blnIsChecked As Boolean = False
            '            For iCnt As Integer = 0 To StrSeparatedValue.Length - 1
            '                If dr.ColumnName = StrSeparatedValue(iCnt) Then
            '                    chkEmpfields.Items.Add(dr.ColumnName, True)
            '                    blnIsChecked = True
            '                    Exit For
            '                End If
            '            Next
            '            If blnIsChecked = False Then
            '                chkEmpfields.Items.Add(dr.ColumnName, False)
            '            End If
            '        Else
            '            If dr.ColumnName = "Code" Or dr.ColumnName = "Employee Name" Or dr.ColumnName = "Department" Or dr.ColumnName = "Job" Or dr.ColumnName = "Shift" Or dr.ColumnName = "Employement Type" Or dr.ColumnName = "Email" Then
            '                chkEmpfields.Items.Add(dr.ColumnName, True)
            '            Else
            '                chkEmpfields.Items.Add(dr.ColumnName, False)
            '            End If
            '        End If
            '        'S.SANDEEP [ 18 AUG 2012 ] -- END

            '    Next
            'End If


            'S.SANDEEP [29 SEP 2016] -- START
            'ENHANCEMENT : DEFINING ENUM FOR EMPLYOEE FEILD, LIST TAKING LONG TIME TO FILL
            'If dgvchkEmpfields.Rows.Count <= 0 Then
            '    Dim dsColumns As DataSet = objEmployee.GetListForDynamicField(FinancialYear._Object._DatabaseName, _
            '                                                                  User._Object._Userunkid, _
            '                                                                  FinancialYear._Object._YearUnkid, _
            '                                                                  Company._Object._Companyunkid, _
            '                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                  ConfigParameter._Object._UserAccessModeSetting, True, False, "EmpList", , , , _
            '                                                                  ConfigParameter._Object._ShowFirstAppointmentDate, User._Object.Privilege._AllowTo_View_Scale).Clone
            '    If ConfigParameter._Object._Checked_Fields.Trim.Length <= 0 Then
            '        Dim StrCheck_Fields As String = String.Empty

            '        'Pinkal (17-Dec-2015) -- Start
            '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            '        'StrCheck_Fields = "Code,Employee Name,Department,Job,Shift,Employement Type,Email"
            '        StrCheck_Fields = Language.getMessage("clsEmployee_Master", 42, "Code") & "," & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 120, "Department") & "," & Language.getMessage("clsEmployee_Master", 118, "Job") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 119, "Shift") & "," & Language.getMessage("clsEmployee_Master", 121, "Employement Type") & "," & _
            '                                    Language.getMessage("clsEmployee_Master", 52, "Email") & "," & Language.getMessage("clsEmployee_Master", 213, "Status")
            '        'Pinkal (17-Dec-2015) -- End


            '        Call Save_List_Headers(StrCheck_Fields)
            '    End If
            '    Dim StrSeparatedValue() As String = ConfigParameter._Object._Checked_Fields.ToString.Split(CChar(","))

            '    Dim dtRow As DataRow
            '    mdtRow = New DataTable
            '    mdtRow.Columns.Add("Ischecked", Type.GetType("System.Boolean"))
            '    mdtRow.Columns.Add("Name", Type.GetType("System.String"))

            '    For Each dr As DataColumn In dsColumns.Tables(0).Columns

            '        'Pinkal (17-Dec-2015) -- Start
            '        'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

            '        'If dr.ColumnName.Trim.ToString().Contains("id") Or dr.ColumnName.Trim.ToString().Contains("Password") Or _
            '        '   dr.ColumnName.Trim.ToString().Contains("First Name") Or dr.ColumnName.Trim.ToString().Contains("Surname") Or dr.ColumnName.Trim.ToString().Contains("Other Name") Or _
            '        '   dr.ColumnName.Trim.ToString().Contains("isclear") Or dr.ColumnName.Trim.ToString().Contains("isactive") Or dr.ColumnName.Trim.ToString().Contains("isapproved") Or _
            '        '   dr.ColumnName.Trim.ToString().Contains("gender") Then Continue For 'S.SANDEEP [ 23 MAY 2014 ] -- START -- END

            '        If dr.ColumnName.Trim.ToString().Contains("id") Or dr.ColumnName.Trim.ToString().Contains(Language.getMessage("clsEmployee_Master", 51, "Password")) Or _
            '          dr.ColumnName.Trim.ToString().Contains(Language.getMessage("clsEmployee_Master", 43, "First Name")) Or dr.ColumnName.Trim.ToString().Contains(Language.getMessage(mstrModuleName, 44, "Surname")) Or _
            '          dr.ColumnName.Trim.ToString().Contains(Language.getMessage(mstrModuleName, 45, "Other Name")) Or dr.ColumnName.Trim.ToString().Contains("isclear") Or dr.ColumnName.Trim.ToString().Contains("isactive") Or _
            '          dr.ColumnName.Trim.ToString().Contains("isapproved") Or dr.ColumnName.Trim.ToString().Contains("gender") Then Continue For '

            '        'Pinkal (17-Dec-2015) -- End


            '        If StrSeparatedValue.Length > 0 Then
            '            Dim blnIsChecked As Boolean = False
            '            For iCnt As Integer = 0 To StrSeparatedValue.Length - 1
            '                If dr.ColumnName = StrSeparatedValue(iCnt) Then
            '                    dtRow = mdtRow.NewRow
            '                    dtRow("Ischecked") = True
            '                    dtRow("Name") = dr.ColumnName
            '                    mdtRow.Rows.Add(dtRow)
            '                    blnIsChecked = True
            '                    Exit For
            '                End If
            '            Next
            '            If blnIsChecked = False Then
            '                dtRow = mdtRow.NewRow
            '                dtRow("Ischecked") = False
            '                dtRow("Name") = dr.ColumnName
            '                mdtRow.Rows.Add(dtRow)
            '            End If
            '        Else

            '            'Pinkal (17-Dec-2015) -- Start
            '            'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.
            '            'If dr.ColumnName = "Code" Or dr.ColumnName = "Employee Name" Or dr.ColumnName = "Department" Or dr.ColumnName = "Job" Or dr.ColumnName = "Shift" Or dr.ColumnName = "Employement Type" Or dr.ColumnName = "Email" Then
            '            If dr.ColumnName = Language.getMessage("clsEmployee_Master", 42, "Code") Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 46, "Employee Name") _
            '                                        Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 120, "Department") Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 118, "Job") _
            '                                        Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 119, "Shift") Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 121, "Employement Type") _
            '                                        Or dr.ColumnName = Language.getMessage("clsEmployee_Master", 52, "Email") Or dr.ColumnName = Language.getMessage(mstrModuleName, 213, "Status") Then
            '                'Pinkal (17-Dec-2015) -- End


            '                dtRow = mdtRow.NewRow
            '                dtRow("Ischecked") = True
            '                dtRow("Name") = dr.ColumnName
            '                mdtRow.Rows.Add(dtRow)
            '            Else
            '                dtRow = mdtRow.NewRow
            '                dtRow("Ischecked") = False
            '                dtRow("Name") = dr.ColumnName
            '                mdtRow.Rows.Add(dtRow)
            '            End If
            '        End If
            '    Next
            '    dgvchkEmpfields.AutoGenerateColumns = False
            '    dtView = mdtRow.DefaultView
            '    objcohchecked.DataPropertyName = "Ischecked"
            '    cohFiled.DataPropertyName = "Name"
            '    dgvchkEmpfields.DataSource = dtView
            '    txtSearch.Text = ""
            'End If

            dgvchkEmpfields.AutoGenerateColumns = False
            mdtRow = objEmployee.GetDynamicFieldColName(ConfigParameter._Object._ShowFirstAppointmentDate, User._Object.Privilege._AllowTo_View_Scale)
            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status
            If ConfigParameter._Object._Checked_Fields.Trim.Length <= 0 Then
                Call Save_List_Headers(StrCheck_Fields)
            Else
                If IsNumeric(ConfigParameter._Object._Checked_Fields.Split(CChar(","))(0)) = False Then
                    ConfigParameter._Object._Checked_Fields = StrCheck_Fields
                    Call Save_List_Headers(StrCheck_Fields)
                End If
            End If

            Dim intSeparatedValue() As Integer = Nothing
            intSeparatedValue = ConfigParameter._Object._Checked_Fields.Split(CChar(",")).[Select](Function(n) Integer.Parse(n.Trim())).ToArray()
            For intIndex As Integer = 0 To intSeparatedValue.Length - 1
                Dim xrow() As DataRow = mdtRow.Select("Id = '" & intSeparatedValue.ElementAt(intIndex) & "'")
                If xrow.Length > 0 Then
                    xrow(0).Item("Ischecked") = True
                End If
            Next

            mdtRow.AcceptChanges()
            dtView = mdtRow.DefaultView
            objcohchecked.DataPropertyName = "Ischecked"
            cohFiled.DataPropertyName = "Name"
            dgvchkEmpfields.DataSource = dtView
            txtSearch.Text = ""
            'S.SANDEEP [29 SEP 2016] -- END

            dgEmployeeList.Enabled = False

            gbFilterCriteria.Enabled = False
            objefemailFooter.Enabled = False
            objFooter.Enabled = False
            gbColumns.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddRemoveField_LinkClicked", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    'S.SANDEEP [03-APR-2017] -- START
    'ISSUE/ENHANCEMENT : Missing Employee Details
    Private Sub mnuMissingEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMissingEmployee.Click
        Dim frm As New frmMissingEmployeeList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMissingEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [03-APR-2017] -- END

#End Region


    'Pinkal (17-Dec-2015) -- Start
    'Enhancement - Working on Employee List Screen Enhancement For Performance Issue.

#Region "DataGrid Events"

    Private Sub dgEmployeeList_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgEmployeeList.CellPainting
        Try

            If e.RowIndex <= -1 Then Exit Sub

            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            dgEmployeeList.Columns(e.ColumnIndex).ReadOnly = True
            'S.SANDEEP [25 OCT 2016] -- END

            If dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 48, "Appointed Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 47, "First Appointed Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 54, "Birth Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 60, "Work Permit Issue Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 61, "Work Permit Expiry Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 65, "Annivesary Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 111, "Suspended From Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 112, "Suspended To Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 113, "Probation From Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 114, "Probation To Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 115, "Leaving Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 116, "Retirement Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 157, "Reinstatement Date") OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 158, "EOC Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage(mstrModuleName, 243, "Resident Permit Issue Date") _
                   OrElse dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage(mstrModuleName, 244, "Resident Permit Expiry Date") Then 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END

                If dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            ElseIf dgEmployeeList.Columns.Contains("ischeck") Then
                dgEmployeeList.Columns("ischeck").Width = 25
                dgEmployeeList.Columns("ischeck").HeaderText = ""
                'S.SANDEEP [25 OCT 2016] -- START
                'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                dgEmployeeList.Columns("ischeck").ReadOnly = False
                'S.SANDEEP [25 OCT 2016] -- END

                'S.SANDEEP [22 MAR 2016] -- START
            ElseIf dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = Language.getMessage("clsEmployee_Master", 110, "Scale") Then
                dgEmployeeList.Columns(e.ColumnIndex).DefaultCellStyle.Format = GUI.fmtCurrency
                'S.SANDEEP [22 MAR 2016] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployeeList_CellPainting", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [22 MAR 2016] -- START
    Private Sub dgEmployeeList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgEmployeeList.DataError

    End Sub
    'S.SANDEEP [22 MAR 2016] -- END

    Private Sub dgEmployeeList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployeeList.CellContentClick, dgEmployeeList.CellContentDoubleClick
        Try

            If e.RowIndex = -1 Then Exit Sub

            If Me.dgEmployeeList.IsCurrentCellDirty Then
                Me.dgEmployeeList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If dgEmployeeList.Columns(e.ColumnIndex).DataPropertyName = "ischeck" Then

                RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

                Dim mdtTable As DataTable = CType(dgEmployeeList.DataSource, DataTable)
                Dim blnFlg As Boolean = CBool(dgEmployeeList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                Dim drRow() As DataRow = mdtTable.Select("employeeunkid = " & CInt(dgEmployeeList.Rows(e.RowIndex).Cells("employeeunkid").Value))

                If drRow.Length > 0 Then
                    mdtTable.AcceptChanges()
                    SetCheckBoxValue()
                End If

                AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployeeList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Pinkal (17-Dec-2015) -- End

    'Last Message Index = 12

    'Public Class frmEmployeeList

    '#Region " Private Varaibles "
    '    Private objEmployee As clsEmployee_Master
    '    Private ReadOnly mstrModuleName As String = "frmEmployeeList"
    '    'Sandeep [ 21 Aug 2010 ] -- Start
    '    Private mstrGuests() As String
    '    Private dsList As New DataSet
    '    'Sandeep [ 21 Aug 2010 ] -- End 

    '    'S.SANDEEP [ 12 OCT 2011 ] -- START
    '    Private mblnIsFromMail As Boolean = False
    '    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    '#End Region

    '    'Sandeep [ 21 Aug 2010 ] -- Start
    '#Region " Properties "
    '    Public ReadOnly Property EmailList() As String()
    '        Get
    '            Return mstrGuests
    '        End Get
    '    End Property

    '    Public Property _DataView() As DataSet
    '        Get
    '            Return dsList
    '        End Get
    '        Set(ByVal value As DataSet)
    '            dsList = value
    '        End Set
    '    End Property

    '    'S.SANDEEP [ 12 OCT 2011 ] -- START
    '    Public WriteOnly Property _IsFromMail() As Boolean
    '        Set(ByVal value As Boolean)
    '            mblnIsFromMail = value
    '        End Set
    '    End Property
    '    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    '#End Region
    '    'Sandeep [ 21 Aug 2010 ] -- End 

    '#Region " Private Function "

    '    Private Sub fillList()
    '        Dim dsAccess As New DataSet
    '        Dim StrSearching As String = ""
    '        Dim dtTable As New DataTable
    '        'Sandeep [ 21 Aug 2010 ] -- Start
    '        Dim intIndex As Integer = 0
    '        'Sandeep [ 21 Aug 2010 ] -- End 

    '        Try
    '            'dsAccess = objEmployee.GetList("EmpList")
    '            dsAccess = objEmployee.GetList("EmpList", , False) 'Anjan (05 Aug 2011) Issue: by default only active employees were coming.

    '            If CInt(cboDepartment.SelectedValue) > 0 Then
    '                StrSearching &= "AND departmentunkid = " & CInt(cboDepartment.SelectedValue) & " "
    '            End If

    '            If CInt(cboJob.SelectedValue) > 0 Then
    '                StrSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue) & " "
    '            End If

    '            If CInt(cboShiftInfo.SelectedValue) > 0 Then
    '                StrSearching &= "AND shiftunkid = " & CInt(cboShiftInfo.SelectedValue) & " "
    '            End If

    '            If CInt(cboEmploymentType.SelectedValue) > 0 Then
    '                StrSearching &= "AND employmenttypeunkid = " & CInt(cboEmploymentType.SelectedValue) & " "
    '            End If

    '            If txtEmployeeCode.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND employeecode LIKE '%" & txtEmployeeCode.Text & "%'"
    '            End If

    '            If txtFirstname.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND firstname LIKE '%" & txtFirstname.Text & "%'"
    '            End If

    '            If txtOtherName.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND othername LIKE '%" & txtOtherName.Text & "%'"
    '            End If

    '            If txtSurname.Text.Trim.Length > 0 Then
    '                StrSearching &= "AND surname LIKE '%" & txtSurname.Text & "%'"
    '            End If

    '            If radActive.Checked Then
    '                StrSearching &= "AND isactive = " & CBool(radActive.Checked) & " " & _
    '                                "AND probation_from_date = '' " & _
    '                                "AND suspended_from_date = '' " & _
    '                                "AND termination_from_date = '' "
    '            End If

    '            If radProbated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND probation_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND probation_to_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND probation_from_date <> " & "''"
    '                End If
    '            End If

    '            If radSuspended.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND suspended_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND suspended_from_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND suspended_from_date <> " & "''"
    '                End If
    '            End If

    '            If radTerminated.Checked Then
    '                If dtpDateFrom.Checked AndAlso dtpDateTo.Checked Then
    '                    StrSearching &= "AND termination_from_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value) & "' AND termination_from_date <='" & eZeeDate.convertDate(dtpDateTo.Value) & "' "
    '                Else
    '                    StrSearching &= "AND termination_from_date <> " & "''"
    '                End If
    '            End If

    '            'S.SANDEEP [ 13 JUNE 2011 ] -- START
    '            'ISSUE : EMPLOYEE & LOAN PRIVILEGE
    '            If radNewEmployee.Checked Then
    '                StrSearching &= "AND isactive = " & CBool(False) & " " & _
    '                                "AND probation_from_date = '' " & _
    '                                "AND suspended_from_date = '' " & _
    '                                "AND termination_from_date = '' "
    '            End If
    '            'S.SANDEEP [ 13 JUNE 2011 ] -- END


    '            'S.SANDEEP [ 17 AUG 2011 ] -- START
    '            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '            If CInt(cboBranch.SelectedValue) > 0 Then
    '                StrSearching &= "AND stationunkid = " & CInt(cboBranch.SelectedValue)
    '            End If
    '            'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '            If StrSearching.Trim.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '                dtTable = New DataView(dsAccess.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = dsAccess.Tables(0)
    '            End If


    '            'Sandeep [ 21 Aug 2010 ] -- Start
    '            lvEmployeeList.LVItems = Nothing
    '            If dtTable.Rows.Count > 0 Then
    '                lvEmployeeList.LVItems = New ListViewItem(dtTable.Rows.Count - 1) {}
    '            End If
    '            'Sandeep [ 21 Aug 2010 ] -- End 

    '            Dim lvItem As ListViewItem

    '            lvEmployeeList.Items.Clear()
    '            For Each drRow As DataRow In dtTable.Rows
    '                lvItem = New ListViewItem
    '                lvItem.Text = drRow("employeecode").ToString
    '                lvItem.SubItems.Add(drRow("firstname").ToString & " " & drRow("othername").ToString & " " & drRow("surname").ToString)
    '                lvItem.SubItems.Add(drRow("DeptName").ToString)
    '                lvItem.SubItems.Add(drRow("job_name").ToString)
    '                lvItem.SubItems.Add(drRow("shiftname").ToString)
    '                lvItem.SubItems.Add(drRow("Comm_Name").ToString)
    '                'Sandeep [ 21 Aug 2010 ] -- Start
    '                lvItem.SubItems.Add(drRow("email").ToString)
    '                'Sandeep [ 21 Aug 2010 ] -- End 

    '                lvItem.Tag = drRow("employeeunkid")
    '                'Sandeep [ 21 Aug 2010 ] -- Start
    '                'lvEmployeeList.Items.Add(lvItem)
    '                lvEmployeeList.LVItems(intIndex) = lvItem
    '                intIndex += 1
    '                'Sandeep [ 21 Aug 2010 ] -- End
    '            Next

    '            'Sandeep [ 21 Aug 2010 ] -- Start
    '            If intIndex > 0 Then
    '                lvEmployeeList.VirtualListSize = intIndex
    '            Else
    '                lvEmployeeList.VirtualListSize = 0
    '            End If

    '            lvEmployeeList.Invalidate()
    '            lvEmployeeList.Reset()
    '            'Sandeep [ 21 Aug 2010 ] -- End 


    '            If lvEmployeeList.Items.Count > 10 Then
    '                colhEmploymentType.Width = 115 - 20
    '            Else
    '                colhEmploymentType.Width = 115
    '            End If

    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
    '        Finally
    '            dsAccess.Dispose()
    '        End Try
    '    End Sub

    '    Private Sub FillCombo()
    '        Dim objDepartment As New clsDepartment
    '        Dim objJobs As New clsJobs
    '        Dim objShift As New clsshift_master
    '        Dim objCommon As New clsCommon_Master
    '        Dim dsCombos As New DataSet
    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        Dim objBranch As New clsStation
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 
    '        Try
    '            dsCombos = objDepartment.getComboList("Department", True)
    '            With cboDepartment
    '                .ValueMember = "departmentunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombos.Tables("Department")
    '            End With

    '            dsCombos = objJobs.getComboList("Jobs", True)
    '            With cboJob
    '                .ValueMember = "jobunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombos.Tables("Jobs")
    '            End With

    '            dsCombos = objShift.getListForCombo("Shift", True)
    '            With cboShiftInfo
    '                .ValueMember = "shiftunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombos.Tables("Shift")
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
    '            With cboEmploymentType
    '                .ValueMember = "masterunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombos.Tables("EmplType")
    '            End With

    '            'S.SANDEEP [ 17 AUG 2011 ] -- START
    '            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '            dsCombos = objBranch.getComboList("List", True)
    '            With cboBranch
    '                .ValueMember = "stationunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombos.Tables("List")
    '                .SelectedValue = 0
    '            End With
    '            'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '        Finally
    '            objDepartment = Nothing
    '            objJobs = Nothing
    '            objShift = Nothing
    '            objCommon = Nothing
    '            dsCombos = Nothing
    '            'S.SANDEEP [ 17 AUG 2011 ] -- START
    '            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '            objBranch = Nothing
    '            'S.SANDEEP [ 17 AUG 2011 ] -- END    
    '        End Try
    '    End Sub

    '    Private Sub GetCheckedData()
    '        Try
    '            If Me._DataView IsNot Nothing Then
    '                If dsList.Tables.Count > 0 Then
    '                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '                        For j As Integer = 0 To lvEmployeeList.Items.Count - 1
    '                            If CInt(dsList.Tables(0).Rows(i)("EmpId")) = CInt(lvEmployeeList.Items(j).Tag) Then
    '                                lvEmployeeList.Items(j).Checked = True
    '                                lvEmployeeList.CheckItem(lvEmployeeList.Items(j))
    '                                lvEmployeeList.Invalidate(lvEmployeeList.Items(j).Bounds)
    '                            End If
    '                        Next
    '                    Next
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "GetCheckedData", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub SetVisibility()

    '        Try

    '            btnNew.Enabled = User._Object.Privilege._AddEmployee
    '            btnEdit.Enabled = User._Object.Privilege._EditEmployee
    '            btnDelete.Enabled = User._Object.Privilege._DeleteEmployee

    '            mnuEnrollFP.Enabled = User._Object.Privilege._AllowEnrollFingerPrint
    '            mnuDeleteFP.Enabled = User._Object.Privilege._AllowDeleteFingerPrint
    '            mnuEnrollNewCard.Enabled = User._Object.Privilege._AllowEnrollNewCard
    '            mnuDeleteEnrolledCard.Enabled = User._Object.Privilege._AllowDeleteEnrolledCard
    '            mnuImportEmployee.Enabled = User._Object.Privilege._AllowImportEmployee

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
    '        End Try

    '    End Sub
    '    'Private Sub AssignContextMenuItemText()
    '    '    Try
    '    '        objtsmiNew.Text = btnNew.Text
    '    '        objtsmiEdit.Text = btnEdit.Text
    '    '        objtsmiDelete.Text = btnDelete.Text
    '    '    Catch ex As Exception
    '    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    '    End Try
    '    'End Sub
    '#End Region

    '#Region " Form's Events "

    '    Private Sub frmEmployeeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
    '        If e.KeyCode = Keys.Delete And lvEmployeeList.Focused = True Then
    '            Call btnDelete.PerformClick()
    '        End If
    '    End Sub

    '    Private Sub frmEmployeeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
    '        If Asc(e.KeyChar) = 27 Then
    '            Me.Close()
    '        ElseIf Asc(e.KeyChar) = 13 Then
    '            SendKeys.Send("{TAB}")
    '            e.Handled = True
    '        End If
    '    End Sub

    '    Private Sub frmEmployeeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '        objEmployee = New clsEmployee_Master
    '        Try
    '            Call Set_Logo(Me, gApplicationType)
    '            Call Language.setLanguage(Me.Name)
    '            'Anjan (02 Sep 2011)-Start
    '            'Issue : Including Language Settings.
    '            Call OtherSettings()
    '            'Anjan (02 Sep 2011)-End 
    '            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

    '            'Call OtherSettings()
    '            radActive.Checked = True


    '            'S.SANDEEP [ 12 OCT 2011 ] -- START
    '            If mblnIsFromMail = True Then
    '                lvEmployeeList.CheckBoxes = True
    '            Else
    '                lvEmployeeList.CheckBoxes = False
    '            End If
    '            'S.SANDEEP [ 12 OCT 2011 ] -- END 

    '            Call FillCombo()

    '            Call fillList()


    '            If Me._DataView.Tables.Count <> 0 Then
    '                Call GetCheckedData()
    '            End If


    '            'Pinkal (03-Jan-2011) -- Start

    '            If ConfigParameter._Object._CommunicationTypeId = enCommunctionType.FingerPrint Then
    '                mnuEnrollFP.Text = Language.getMessage(mstrModuleName, 1, "Enroll Employee Finger Print")
    '                mnuDeleteFP.Text = Language.getMessage(mstrModuleName, 2, "Delete Employee Enroll Finger Print")

    '            ElseIf ConfigParameter._Object._CommunicationTypeId = enCommunctionType.RFID Then
    '                mnuEnrollFP.Text = Language.getMessage(mstrModuleName, 3, "Enroll Employee Card")
    '                mnuDeleteFP.Text = Language.getMessage(mstrModuleName, 4, "Delete Employee Enroll Card")

    '                'Pinkal (16-Feb-2011)-- Start

    '            ElseIf ConfigParameter._Object._CommunicationTypeId = enCommunctionType.None Then
    '                mnuEnrollFP.Visible = False
    '                mnuDeleteFP.Visible = False

    '                'Pinkal (16-Feb-2011)-- End
    '            End If

    '            'Pinkal (03-Jan-2011) -- End

    '            Call SetVisibility()

    '            If lvEmployeeList.Items.Count > 0 Then lvEmployeeList.Items(0).Selected = True
    '            lvEmployeeList.Select()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub frmEmployeeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '        objEmployee = Nothing
    '    End Sub

    '    'Anjan (02 Sep 2011)-Start
    '    'Issue : Including Language Settings.
    '    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '        Dim objfrm As New frmLanguage
    '        Try
    '            If User._Object._Isrighttoleft = True Then
    '                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objfrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objfrm)
    '            End If

    '            Call SetMessages()

    '            clsEmployee_Master.SetMessages()
    '            objfrm._Other_ModuleNames = "clsEmployee_Master"
    '            objfrm.displayDialog(Me)

    '            Call SetLanguage()

    '        Catch ex As System.Exception
    '            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '        Finally
    '            objfrm.Dispose()
    '            objfrm = Nothing
    '        End Try
    '    End Sub
    '    'Anjan (02 Sep 2011)-End 

    '    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    '    Dim objfrm As New frmLanguage
    '    '    Try
    '    '        If User._Object._RightToLeft = True Then
    '    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '    '            objfrm.RightToLeftLayout = True
    '    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '    '        End If

    '    '        Call SetMessages()

    '    '        clsAirLine.SetMessages()
    '    '        objfrm._Other_ModuleNames = "clsAirLine"
    '    '        objfrm.displayDialog(Me)

    '    '        Call SetLanguage()

    '    '    Catch ex As System.Exception
    '    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    '    Finally
    '    '        objfrm.Dispose()
    '    '        objfrm = Nothing
    '    '    End Try
    '    'End Sub

    '#End Region

    '#Region " Buttons "
    '    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Me.Close()
    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '        If lvEmployeeList.SelectedIndices.Count < 1 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        'If objEmployee.isUsed(CInt(lvEmployeeList.SelectedItems(0).Tag)) Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee. Reason: This Employee is in use."), enMsgBoxStyle.Information) '?2
    '        '    lvEmployeeList.Select()
    '        '    Exit Sub
    '        'End If
    '        Try
    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)

    '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Are you sure you want to delete this Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
    '                objEmployee.Delete(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag))
    '                If objEmployee._Message <> "" Then
    '                    eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If
    '                lvEmployeeList.SelectedItems(0).Remove()

    '                If lvEmployeeList.Items.Count <= 0 Then
    '                    Exit Try
    '                End If

    '                If lvEmployeeList.Items.Count = intSelectedIndex Then
    '                    intSelectedIndex = lvEmployeeList.Items.Count - 1
    '                    lvEmployeeList.Items(intSelectedIndex).Selected = True
    '                    lvEmployeeList.EnsureVisible(intSelectedIndex)
    '                ElseIf lvEmployeeList.Items.Count <> 0 Then
    '                    lvEmployeeList.Items(intSelectedIndex).Selected = True
    '                    lvEmployeeList.EnsureVisible(intSelectedIndex)
    '                End If
    '            End If
    '            lvEmployeeList.Select()
    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '        If lvEmployeeList.SelectedIndices.Count < 1 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        Dim frm As New frmEmployeeMaster
    '        Try
    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)

    '            'If User._Object._RightToLeft = True Then
    '            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            '    frm.RightToLeftLayout = True
    '            '    Call Language.ctlRightToLeftlayOut(frm)
    '            'End If

    '            If frm.displayDialog(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag), enAction.EDIT_ONE) Then
    '                Call fillList()
    '            End If
    '            frm = Nothing

    '            If lvEmployeeList.Items.Count <= 0 Then Exit Try


    '            'Sandeep [ 09 Oct 2010 ] -- Start
    '            'Issues Reported by Vimal
    '            'lvEmployeeList.Items(intSelectedIndex).Selected = True
    '            'lvEmployeeList.EnsureVisible(intSelectedIndex)
    '            'lvEmployeeList.Select()
    '            'Sandeep [ 09 Oct 2010 ] -- End 
    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '        Finally
    '            If frm IsNot Nothing Then frm.Dispose()
    '        End Try
    '    End Sub

    '    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
    '        Dim frm As New frmEmployeeMaster
    '        Try
    '            'If User._Object._RightToLeft = True Then
    '            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            '    frm.RightToLeftLayout = True
    '            '    Call Language.ctlRightToLeftlayOut(frm)
    '            'End If
    '            If frm.displayDialog(-1, enAction.ADD_ONE) Then
    '                Call fillList()
    '            End If
    '        Catch ex As Exception
    '            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
    '        Finally
    '            If frm IsNot Nothing Then frm.Dispose()
    '        End Try
    '    End Sub

    '    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
    '        Try
    '            Call fillList()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
    '        Try
    '            txtEmployeeCode.Text = ""
    '            txtFirstname.Text = ""
    '            txtOtherName.Text = ""
    '            txtSurname.Text = ""
    '            cboDepartment.SelectedValue = 0
    '            cboEmploymentType.SelectedValue = 0
    '            cboShiftInfo.SelectedValue = 0
    '            cboJob.SelectedValue = 0
    '            dtpDateFrom.Checked = False
    '            dtpDateTo.Checked = False
    '            radActive.Checked = True
    '            radProbated.Checked = False
    '            radSuspended.Checked = False
    '            radTerminated.Checked = False
    '            'S.SANDEEP [ 17 AUG 2011 ] -- START
    '            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '            cboBranch.SelectedValue = 0
    '            'S.SANDEEP [ 17 AUG 2011 ] -- END 
    '            Call fillList()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    'S.SANDEEP [ 13 JUNE 2011 ] -- START
    '    'ISSUE : EMPLOYEE & LOAN PRIVILEGE
    '    'Private Sub radActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radActive.CheckedChanged, radAll.CheckedChanged, radProbated.CheckedChanged, radSuspended.CheckedChanged, radTerminated.CheckedChanged
    '    Private Sub radActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radActive.CheckedChanged, radAll.CheckedChanged, radProbated.CheckedChanged, radSuspended.CheckedChanged, radTerminated.CheckedChanged, radNewEmployee.CheckedChanged
    '        'S.SANDEEP [ 13 JUNE 2011 ] -- END
    '        Try
    '            If radActive.Checked Or radAll.Checked Then
    '                dtpDateFrom.Enabled = False
    '                dtpDateTo.Enabled = False
    '            Else
    '                dtpDateFrom.Enabled = True
    '                dtpDateTo.Enabled = True
    '            End If
    '            Call fillList()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "radActive_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Sandeep [ 21 Aug 2010 ] -- Start
    '    Private Sub btnMailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMailClose.Click
    '        Try
    '            Me.Close()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnMailClose_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
    '        Try
    '            Dim strNumber As String = ""
    '            Dim objLetterFields As New clsLetterFields
    '            mstrGuests = New String() {}
    '            Dim blnFlag As Boolean = False
    '            Dim intCnt As Integer = 0
    '            If lvEmployeeList.CheckedItemIdArray.Length > 0 Then
    '                ReDim mstrGuests(lvEmployeeList.CheckedItemIdArray.Length + 1)
    '                For i As Integer = 0 To lvEmployeeList.CheckedItemIdArray.Length - 1
    '                    With lvEmployeeList.LVItems(CInt(lvEmployeeList.CheckedItemIdArray(i)))
    '                        If .SubItems(colhEmail.Index).Text <> "" Then
    '                            mstrGuests(intCnt) = .SubItems(colhEmployeeName.Index).Text & " " & "<" & .SubItems(colhEmail.Index).Text & ">"
    '                            If strNumber = "" Then
    '                                strNumber = .Tag.ToString
    '                            Else
    '                                strNumber = strNumber & " , " & .Tag.ToString
    '                            End If
    '                            intCnt += 1
    '                        Else
    '                            If blnFlag = False Then
    '                                Dim strMsg As String = Language.getMessage(mstrModuleName, 11, "Some of the email address(s) are blank.And will not added to the list. Do you want to continue?")
    '                                If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
    '                                    blnFlag = True
    '                                    Continue For
    '                                Else
    '                                    blnFlag = False
    '                                    Exit Sub
    '                                End If
    '                            End If
    '                        End If
    '                    End With
    '                Next
    '                If strNumber <> "" Then
    '                    dsList = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Employee_Module)
    '                End If
    '                Me.Close()
    '            Else
    '                If lvEmployeeList.SelectedIndices.Count > 0 Then
    '                    dsList = objLetterFields.GetEmployeeData(CStr(lvEmployeeList.LVItems(lvEmployeeList.SelectedIndices.Item(0)).Tag), enImg_Email_RefId.Employee_Module)
    '                    Me.Close()
    '                End If
    '            End If


    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
    '        End Try
    '    End Sub
    '    'Sandeep [ 21 Aug 2010 ] -- End 
    '#End Region

    '#Region " Controls "
    '    Private Sub mnuScanIdentifyInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanIdentifyInfo.Click
    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Employee from list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If

    '        Dim ObjFrm As New frmScanOrAttachmentInfo

    '        Try
    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    '            dsList = objEmployee.GetEmployee_Identity(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag), False, "Ids")
    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                ObjFrm._Dataset = dsList
    '                ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 5, "Select Identity")
    '                ObjFrm._RefId = enScanAttactRefId.EMPLOYEE_IDENTITY
    '                ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag)
    '                ObjFrm._IsComboVisible = True
    '                ObjFrm._EmployeeName = lvEmployeeList.LVItems(intSelectedIndex).SubItems(colhEmployeeName.Index).Text

    '                ObjFrm.ShowDialog()
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please add atleast one Identity to particular employee."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuScanIdentifyInfo_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub mnuScanMemDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanMemDocument.Click

    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Employee from list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If

    '        Dim ObjFrm As New frmScanOrAttachmentInfo

    '        Try

    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    '            dsList = objEmployee.GetEmployee_Membership(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag), False, "Membership")

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                ObjFrm._Dataset = dsList
    '                ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 6, "Select Membership")
    '                ObjFrm._RefId = enScanAttactRefId.EMPLOYEE_MEMBERSHIP
    '                ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag)
    '                ObjFrm._IsComboVisible = True
    '                ObjFrm._EmployeeName = lvEmployeeList.LVItems(intSelectedIndex).SubItems(colhEmployeeName.Index).Text

    '                ObjFrm.ShowDialog()
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one Membership to particular employee."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuScanMemDocument_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub mnuImportEligibleApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEligibleApplicant.Click
    '        Try
    '            Dim ObjFrm As New frmImport_Applicants
    '            'ObjFrm._Is_Import = True
    '            ObjFrm.ShowDialog()
    '            Call fillList()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuImportEligibleApplicant_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub mnuDeleteEnrolledCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeleteEnrolledCard.Click
    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        Try
    '            Dim objSwipeCard As New clsSwipeCardData

    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)

    '            objSwipeCard.Delete(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag))
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuDeleteEnrolledCard_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub mnuEnrollNewCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnrollNewCard.Click
    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        Try
    '            Dim objfrm As New frmSwipeCardData

    '            'If User._Object._RightToLeft = True Then
    '            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            '    objfrm.RightToLeftLayout = True
    '            '    Call Language.ctlRightToLeftlayOut(objfrm)
    '            'End If

    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)

    '            Call objfrm.displayDialog(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag))

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuEnrollNewCard_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Pinkal (22-Dec-2010) -- Start

    '    Private Sub mnuImportEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportEmployee.Click
    '        Try
    '            'Sandeep [ 12 MARCH 2011 ] -- Start
    '            'Dim objFrm As New frmImportEmployee
    '            'objFrm.displayDialog()
    '            'fillList()
    '            Dim frm As New frmEmpImportWizard
    '            frm.ShowDialog()
    '            Call fillList()
    '            'Sandeep [ 12 MARCH 2011 ] -- End 
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuImportEmployee_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Pinkal (22-Dec-2010) -- End


    '    'Pinkal (03-Jan-2011) -- Start

    '    Private Sub mnuEnrollFP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnrollFP.Click
    '        Try
    '            Dim objfrm As New frmfingerPrint_enroll

    '            If ConfigParameter._Object._CommunicationTypeId = enCommunctionType.FingerPrint Then
    '                objfrm.Text = Language.getMessage(mstrModuleName, 1, "Enroll Employee Finger Print")
    '            ElseIf ConfigParameter._Object._CommunicationTypeId = enCommunctionType.RFID Then
    '                objfrm.Text = Language.getMessage(mstrModuleName, 3, "Enroll Employee Card")
    '            End If

    '            objfrm.displayDialog(-1, enAction.ADD_CONTINUE)
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuEnrollFP_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub mnuDeleteFP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeleteFP.Click
    '        Try
    '            Dim objfrm As New frmDeletefingerPrint

    '            If ConfigParameter._Object._CommunicationTypeId = enCommunctionType.FingerPrint Then
    '                objfrm.Text = Language.getMessage(mstrModuleName, 2, "Delete Employee Enroll Finger Print")
    '            ElseIf ConfigParameter._Object._CommunicationTypeId = enCommunctionType.RFID Then
    '                objfrm.Text = Language.getMessage(mstrModuleName, 4, "Delete Employee Enroll Card")
    '            End If

    '            objfrm.displayDialog(-1, enAction.ADD_CONTINUE)
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuDeleteFP_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Pinkal (03-Jan-2011) -- End

    '#End Region

    '    Private Sub mnuEditImportedEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditImportedEmployee.Click
    '        Try
    '            Dim frm As New frmEditImportedEmployee
    '            frm.ShowDialog()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuEditImportedEmployee_Click", mstrModuleName)
    '        End Try
    '    End Sub


    '    'Sandeep [ 21 APRIL 2011 ] -- Start
    '    'Issue : SCAN & ATTACHMENT ENHANCEMENT
    '    Private Sub mnuScanOtherDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanOtherDoc.Click
    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        Dim ObjFrm As New frmScanOrAttachmentInfo
    '        Try
    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    '            ObjFrm._RefId = enScanAttactRefId.OTHER_DOCUMENTS
    '            ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag)
    '            ObjFrm._IsComboVisible = False
    '            ObjFrm._EmployeeName = lvEmployeeList.LVItems(intSelectedIndex).SubItems(colhEmployeeName.Index).Text
    '            ObjFrm._ActionTag = CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag)
    '            ObjFrm._TextValue = Language.getMessage(mstrModuleName, 9, "Other Document")
    '            ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 9, "Other Document")
    '            ObjFrm.ShowDialog()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuScanOtherDoc_Click", mstrModuleName)
    '        Finally
    '            If ObjFrm IsNot Nothing Then ObjFrm.Dispose()
    '        End Try
    '    End Sub
    '    'Sandeep [ 21 APRIL 2011 ] -- End 
    '    'S.SANDEEP [ 18 May 2011 ] -- START
    '    Private Sub mnuImportMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportMembership.Click
    '        Try
    '            Dim frm As New frmImportMemberships
    '            frm.ShowDialog()
    '            Call fillList()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuImportMembership_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub mnuGetMembershipTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetMembershipTemplate.Click
    '        Dim objMembership As New clsMembershipTran
    '        Dim IExcel As New ExcelData
    '        Dim dsList As New DataSet
    '        Dim path As String = String.Empty
    '        Dim strFilePath As String = String.Empty
    '        Dim dlgSaveFile As New SaveFileDialog
    '        Dim ObjFile As System.IO.FileInfo
    '        Try
    '            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Execl files(*.xlsx)|*.xlsx""
    '            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
    '                strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
    '                strFilePath &= ObjFile.Extension

    '                dsList = objMembership.GetBlankTemplate("List")
    '                Select Case dlgSaveFile.FilterIndex
    '                    Case 1   'XML
    '                        dsList.WriteXml(strFilePath)
    '                    Case 2   'XLS
    '                        IExcel.Export(strFilePath, dsList)
    '                End Select
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Template Exported Successfully."), enMsgBoxStyle.Information)
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuGetMembershipTemplate_Click", mstrModuleName)
    '        Finally
    '            objMembership = Nothing
    '            IExcel = Nothing
    '            dsList = Nothing
    '        End Try
    '    End Sub

    '    'S.SANDEEP [ 18 May 2011 ] -- END 



    '    'S.SANDEEP [ 15 SEP 2011 ] -- START
    '    'ENHANCEMENT : CODE OPTIMIZATION
    '    Private Sub mnuViewDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewDiary.Click
    '        If lvEmployeeList.SelectedIndices.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvEmployeeList.Select()
    '            Exit Sub
    '        End If
    '        Try
    '            Dim frm As New frmEmployeeDiary
    '            Dim intSelectedIndex As Integer
    '            intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    '            frm.displayDialog(CInt(lvEmployeeList.LVItems(intSelectedIndex).Tag))
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "mnuViewDiary_Click", mstrModuleName)
    '        Finally
    '        End Try
    '    End Sub
    '    'S.SANDEEP [ 15 SEP 2011 ] -- END 
    '#Region " Controls "

    ''S.SANDEEP [ 18 FEB 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES
    ''Private Sub mnuScanIdentifyInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanIdentifyInfo.Click
    ''    If lvEmployeeList.SelectedItems.Count <= 0 Then
    ''        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Employee from list to perform further operation on it."), enMsgBoxStyle.Information)
    ''        lvEmployeeList.Select()
    ''        Exit Sub
    ''    End If

    ''    Dim ObjFrm As New frmScanOrAttachmentInfo

    ''    Try
    ''        Dim intSelectedIndex As Integer
    ''        intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    ''        dsList = objEmployee.GetEmployee_Identity(CInt(lvEmployeeList.SelectedItems(0).Tag), False, "Ids")
    ''        If dsList.Tables(0).Rows.Count > 0 Then
    ''            ObjFrm._Dataset = dsList
    ''            ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 5, "Select Identity")
    ''            ObjFrm._RefId = enScanAttactRefId.EMPLOYEE_IDENTITY
    ''            ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''            ObjFrm._IsComboVisible = True
    ''            ObjFrm._EmployeeName = lvEmployeeList.SelectedItems(0).SubItems(colhEmployeeName.Index).Text

    ''            ObjFrm.ShowDialog()
    ''        Else
    ''            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please add atleast one Identity to particular employee."), enMsgBoxStyle.Information)
    ''            Exit Sub
    ''        End If

    ''    Catch ex As Exception
    ''        DisplayError.Show("-1", ex.Message, "mnuScanIdentifyInfo_Click", mstrModuleName)
    ''    End Try
    ''End Sub

    ''Private Sub mnuScanMemDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanMemDocument.Click

    ''    If lvEmployeeList.SelectedItems.Count <= 0 Then
    ''        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Employee from list to perform further operation on it."), enMsgBoxStyle.Information)
    ''        lvEmployeeList.Select()
    ''        Exit Sub
    ''    End If

    ''        Dim ObjFrm As New frmScanOrAttachmentInfo

    ''    Try

    ''        Dim intSelectedIndex As Integer
    ''        intSelectedIndex = lvEmployeeList.SelectedIndices.Item(0)
    ''        dsList = objEmployee.GetEmployee_Membership(CInt(lvEmployeeList.SelectedItems(0).Tag), False, "Membership")

    ''        If dsList.Tables(0).Rows.Count > 0 Then
    ''            ObjFrm._Dataset = dsList
    ''            ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 6, "Select Membership")
    ''            ObjFrm._RefId = enScanAttactRefId.EMPLOYEE_MEMBERSHIP
    ''            ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''            ObjFrm._IsComboVisible = True
    ''            ObjFrm._EmployeeName = lvEmployeeList.SelectedItems(0).SubItems(colhEmployeeName.Index).Text

    ''        ObjFrm.ShowDialog()
    ''        Else
    ''            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one Membership to particular employee."), enMsgBoxStyle.Information)
    ''            Exit Sub
    ''        End If

    ''    Catch ex As Exception
    ''        DisplayError.Show("-1", ex.Message, "mnuScanMemDocument_Click", mstrModuleName)
    ''    End Try
    ''End Sub

    ''Private Sub mnuScanOtherDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanOtherDoc.Click
    ''    If lvEmployeeList.SelectedItems.Count <= 0 Then
    ''        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    ''        lvEmployeeList.Select()
    ''        Exit Sub
    ''    End If
    ''    Dim ObjFrm As New frmScanOrAttachmentInfo
    ''    Try
    ''        ObjFrm._RefId = enScanAttactRefId.OTHER_DOCUMENTS
    ''        ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''        ObjFrm._IsComboVisible = False
    ''        ObjFrm._EmployeeName = lvEmployeeList.SelectedItems(0).SubItems(colhEmployeeName.Index).Text
    ''        ObjFrm._ActionTag = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''        ObjFrm._TextValue = Language.getMessage(mstrModuleName, 9, "Other Document")
    ''        ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 9, "Other Document")
    ''        ObjFrm.ShowDialog()
    ''    Catch ex As Exception
    ''        DisplayError.Show("-1", ex.Message, "mnuScanOtherDoc_Click", mstrModuleName)
    ''    Finally
    ''        If ObjFrm IsNot Nothing Then ObjFrm.Dispose()
    ''    End Try
    ''End Sub
    ''Private Sub mnuScanOtherDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanOtherDoc.Click
    ''    If lvEmployeeList.SelectedItems.Count <= 0 Then
    ''        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information)
    ''        lvEmployeeList.Select()
    ''        Exit Sub
    ''    End If
    ''    Dim ObjFrm As New frmScanOrAttachmentInfo
    ''    Try
    ''        ObjFrm._RefId = enScanAttactRefId.OTHER_DOCUMENTS
    ''        ObjFrm._EmployeeUnkid = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''        ObjFrm._IsComboVisible = False
    ''        ObjFrm._EmployeeName = lvEmployeeList.SelectedItems(0).SubItems(colhEmployeeName.Index).Text
    ''        ObjFrm._ActionTag = CInt(lvEmployeeList.SelectedItems(0).Tag)
    ''        ObjFrm._TextValue = Language.getMessage(mstrModuleName, 9, "Other Document")
    ''        ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 9, "Other Document")
    ''        ObjFrm.ShowDialog()
    ''    Catch ex As Exception
    ''        DisplayError.Show("-1", ex.Message, "mnuScanOtherDoc_Click", mstrModuleName)
    ''    Finally
    ''        If ObjFrm IsNot Nothing Then ObjFrm.Dispose()
    ''    End Try
    ''End Sub

    ''S.SANDEEP [ 18 FEB 2012 ] -- END









    ''Anjan (21 Nov 2011)-Start
    ''ENHANCEMENT : TRA COMMENTS

    ''Anjan (21 Nov 2011)-End 

    ''S.SANDEEP [ 14 AUG 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES

    ''S.SANDEEP [ 14 AUG 2012 ] -- END

    ''Pinkal (14-Aug-2012) -- Start
    ''Enhancement : TRA Changes


    ''Pinkal (14-Aug-2012) -- End

    ''S.SANDEEP [ 07 SEP 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES







    ''Pinkal (01-Apr-2015) -- End





    ''S.SANDEEP [09 APR 2015] -- START


    ''Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            Dim c As Char = Convert.ToChar(e.KeyCode)
    '            frm.TypedText = c.ToString
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                'If CInt(cboEmployee.SelectedValue) > 0 Then
    '                '    Call FillListForDynamicColumn(GetDynamicField())
    '                '    If lvEmployeeList.Items.Count > 0 Then lvEmployeeList.Items(0).Selected = True
    '                '    lvEmployeeList.Select()
    '                'End If
    '                'If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_KeyDown", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub


    'S.SANDEEP [09 APR 2015] -- END
    '#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
         
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbColumns.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbColumns.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnExportList.GradientBackColor = GUI._ButttonBackColor
            Me.btnExportList.GradientForeColor = GUI._ButttonFontColor

            Me.btnMailClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnMailClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnFieldClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnFieldClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblOthername.Text = Language._Object.getCaption(Me.lblOthername.Name, Me.lblOthername.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.Name, Me.lblSurname.Text)
            Me.lblAppointDate.Text = Language._Object.getCaption(Me.lblAppointDate.Name, Me.lblAppointDate.Text)
            Me.lblEmploymentType.Text = Language._Object.getCaption(Me.lblEmploymentType.Name, Me.lblEmploymentType.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.Name, Me.lblDateTo.Text)
            Me.radProbated.Text = Language._Object.getCaption(Me.radProbated.Name, Me.radProbated.Text)
            Me.radSuspended.Text = Language._Object.getCaption(Me.radSuspended.Name, Me.radSuspended.Text)
            Me.radTerminated.Text = Language._Object.getCaption(Me.radTerminated.Name, Me.radTerminated.Text)
            Me.btnExportList.Text = Language._Object.getCaption(Me.btnExportList.Name, Me.btnExportList.Text)
            Me.mnuEnrollNewCard.Text = Language._Object.getCaption(Me.mnuEnrollNewCard.Name, Me.mnuEnrollNewCard.Text)
            Me.mnuDeleteEnrolledCard.Text = Language._Object.getCaption(Me.mnuDeleteEnrolledCard.Name, Me.mnuDeleteEnrolledCard.Text)
            Me.mnuScanDocuments.Text = Language._Object.getCaption(Me.mnuScanDocuments.Name, Me.mnuScanDocuments.Text)
            Me.btnMailClose.Text = Language._Object.getCaption(Me.btnMailClose.Name, Me.btnMailClose.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.ToolStripSeparator1.Text = Language._Object.getCaption(Me.ToolStripSeparator1.Name, Me.ToolStripSeparator1.Text)
            Me.mnuEnrollFP.Text = Language._Object.getCaption(Me.mnuEnrollFP.Name, Me.mnuEnrollFP.Text)
            Me.mnuDeleteFP.Text = Language._Object.getCaption(Me.mnuDeleteFP.Name, Me.mnuDeleteFP.Text)
            Me.mnuEditImportedEmployee.Text = Language._Object.getCaption(Me.mnuEditImportedEmployee.Name, Me.mnuEditImportedEmployee.Text)
            Me.mnuGetMembershipTemplate.Text = Language._Object.getCaption(Me.mnuGetMembershipTemplate.Name, Me.mnuGetMembershipTemplate.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.mnuViewDiary.Text = Language._Object.getCaption(Me.mnuViewDiary.Name, Me.mnuViewDiary.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.mnuEmployeebiodata.Text = Language._Object.getCaption(Me.mnuEmployeebiodata.Name, Me.mnuEmployeebiodata.Text)
            Me.mnuEmployeeDependents.Text = Language._Object.getCaption(Me.mnuEmployeeDependents.Name, Me.mnuEmployeeDependents.Text)
            Me.mnuEmployeeQualifications.Text = Language._Object.getCaption(Me.mnuEmployeeQualifications.Name, Me.mnuEmployeeQualifications.Text)
            Me.mnuEmployeeExperiences.Text = Language._Object.getCaption(Me.mnuEmployeeExperiences.Name, Me.mnuEmployeeExperiences.Text)
            Me.mnuEmployeeReferences.Text = Language._Object.getCaption(Me.mnuEmployeeReferences.Name, Me.mnuEmployeeReferences.Text)
            Me.mnuEmployeeSkills.Text = Language._Object.getCaption(Me.mnuEmployeeSkills.Name, Me.mnuEmployeeSkills.Text)
            Me.mnuEmployeeBenefits.Text = Language._Object.getCaption(Me.mnuEmployeeBenefits.Name, Me.mnuEmployeeBenefits.Text)
            Me.mnuAssignedCompanyAssets.Text = Language._Object.getCaption(Me.mnuAssignedCompanyAssets.Name, Me.mnuAssignedCompanyAssets.Text)
            Me.gbColumns.Text = Language._Object.getCaption(Me.gbColumns.Name, Me.gbColumns.Text)
            Me.btnFieldClose.Text = Language._Object.getCaption(Me.btnFieldClose.Name, Me.btnFieldClose.Text)
            Me.lnkAddRemoveField.Text = Language._Object.getCaption(Me.lnkAddRemoveField.Name, Me.lnkAddRemoveField.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.mnuReportTo.Text = Language._Object.getCaption(Me.mnuReportTo.Name, Me.mnuReportTo.Text)
            Me.mnuList.Text = Language._Object.getCaption(Me.mnuList.Name, Me.mnuList.Text)
            Me.mnuSetReportingTo.Text = Language._Object.getCaption(Me.mnuSetReportingTo.Name, Me.mnuSetReportingTo.Text)
            Me.mnuImportReportingTo.Text = Language._Object.getCaption(Me.mnuImportReportingTo.Name, Me.mnuImportReportingTo.Text)
            Me.mnuGetFileFormat_ReportTO.Text = Language._Object.getCaption(Me.mnuGetFileFormat_ReportTO.Name, Me.mnuGetFileFormat_ReportTO.Text)
            Me.mnuShift_Policy.Text = Language._Object.getCaption(Me.mnuShift_Policy.Name, Me.mnuShift_Policy.Text)
            Me.mnuAssign_Shift_Policy.Text = Language._Object.getCaption(Me.mnuAssign_Shift_Policy.Name, Me.mnuAssign_Shift_Policy.Text)
            Me.mnuVAssigned_Shift.Text = Language._Object.getCaption(Me.mnuVAssigned_Shift.Name, Me.mnuVAssigned_Shift.Text)
            Me.mnuVAssigned_Policy.Text = Language._Object.getCaption(Me.mnuVAssigned_Policy.Name, Me.mnuVAssigned_Policy.Text)
            Me.mnuAssigndayoff.Text = Language._Object.getCaption(Me.mnuAssigndayoff.Name, Me.mnuAssigndayoff.Text)
            Me.mnuUpdateImpDetails.Text = Language._Object.getCaption(Me.mnuUpdateImpDetails.Name, Me.mnuUpdateImpDetails.Text)
            Me.cmnuEmployeeMovement.Text = Language._Object.getCaption(Me.cmnuEmployeeMovement.Name, Me.cmnuEmployeeMovement.Text)
            Me.mnuTransfers.Text = Language._Object.getCaption(Me.mnuTransfers.Name, Me.mnuTransfers.Text)
            Me.mnuRecategorize.Text = Language._Object.getCaption(Me.mnuRecategorize.Name, Me.mnuRecategorize.Text)
            Me.chkShowActiveEmployee.Text = Language._Object.getCaption(Me.chkShowActiveEmployee.Name, Me.chkShowActiveEmployee.Text)
            Me.mnuProbation.Text = Language._Object.getCaption(Me.mnuProbation.Name, Me.mnuProbation.Text)
            Me.mnuConfirmation.Text = Language._Object.getCaption(Me.mnuConfirmation.Name, Me.mnuConfirmation.Text)
            Me.mnuSuspension.Text = Language._Object.getCaption(Me.mnuSuspension.Name, Me.mnuSuspension.Text)
            Me.mnuTermination.Text = Language._Object.getCaption(Me.mnuTermination.Name, Me.mnuTermination.Text)
            Me.mnuReHire.Text = Language._Object.getCaption(Me.mnuReHire.Name, Me.mnuReHire.Text)
            Me.mnuRetirements.Text = Language._Object.getCaption(Me.mnuRetirements.Name, Me.mnuRetirements.Text)
            Me.mnuWorkPermit.Text = Language._Object.getCaption(Me.mnuWorkPermit.Name, Me.mnuWorkPermit.Text)
            Me.mnuCostCenter.Text = Language._Object.getCaption(Me.mnuCostCenter.Name, Me.mnuCostCenter.Text)
            Me.chkAllChecked.Text = Language._Object.getCaption(Me.chkAllChecked.Name, Me.chkAllChecked.Text)
            Me.cohFiled.HeaderText = Language._Object.getCaption(Me.cohFiled.Name, Me.cohFiled.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.mnuUpdateEmployeeMovement.Text = Language._Object.getCaption(Me.mnuUpdateEmployeeMovement.Name, Me.mnuUpdateEmployeeMovement.Text)
            Me.mnuGlobalVoidMembership.Text = Language._Object.getCaption(Me.mnuGlobalVoidMembership.Name, Me.mnuGlobalVoidMembership.Text)
            Me.mnuAssignSalaryAnniversaryMonth.Text = Language._Object.getCaption(Me.mnuAssignSalaryAnniversaryMonth.Name, Me.mnuAssignSalaryAnniversaryMonth.Text)
            Me.mnuImport.Text = Language._Object.getCaption(Me.mnuImport.Name, Me.mnuImport.Text)
            Me.mnuImportEligibleApplicant.Text = Language._Object.getCaption(Me.mnuImportEligibleApplicant.Name, Me.mnuImportEligibleApplicant.Text)
            Me.mnuImportEmployee.Text = Language._Object.getCaption(Me.mnuImportEmployee.Name, Me.mnuImportEmployee.Text)
            Me.mnuImportImages.Text = Language._Object.getCaption(Me.mnuImportImages.Name, Me.mnuImportImages.Text)
            Me.mnuImportMembership.Text = Language._Object.getCaption(Me.mnuImportMembership.Name, Me.mnuImportMembership.Text)
            Me.mnuImportEmpIdentities.Text = Language._Object.getCaption(Me.mnuImportEmpIdentities.Name, Me.mnuImportEmpIdentities.Text)
            Me.mnuImportAddress.Text = Language._Object.getCaption(Me.mnuImportAddress.Name, Me.mnuImportAddress.Text)
            Me.mnuImportToDatabase.Text = Language._Object.getCaption(Me.mnuImportToDatabase.Name, Me.mnuImportToDatabase.Text)
            Me.mnuPhotoFile.Text = Language._Object.getCaption(Me.mnuPhotoFile.Name, Me.mnuPhotoFile.Text)
            Me.mnuImpPPLSoftData.Text = Language._Object.getCaption(Me.mnuImpPPLSoftData.Name, Me.mnuImpPPLSoftData.Text)
            Me.mnuImportBirthInformation.Text = Language._Object.getCaption(Me.mnuImportBirthInformation.Name, Me.mnuImportBirthInformation.Text)
            Me.mnuImportEmpShift.Text = Language._Object.getCaption(Me.mnuImportEmpShift.Name, Me.mnuImportEmpShift.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsEmployee_Master", 46, "Employee Name")
            Language.setMessage("clsEmployee_Master", 47, "First Appointed Date")
            Language.setMessage("clsEmployee_Master", 48, "Appointed Date")
            Language.setMessage("clsEmployee_Master", 52, "Email")
            Language.setMessage("clsEmployee_Master", 54, "Birth Date")
            Language.setMessage("clsEmployee_Master", 60, "Work Permit Issue Date")
            Language.setMessage("clsEmployee_Master", 61, "Work Permit Expiry Date")
            Language.setMessage("clsEmployee_Master", 65, "Annivesary Date")
            Language.setMessage("clsEmployee_Master", 110, "Scale")
            Language.setMessage("clsEmployee_Master", 111, "Suspended From Date")
            Language.setMessage("clsEmployee_Master", 112, "Suspended To Date")
            Language.setMessage("clsEmployee_Master", 113, "Probation From Date")
            Language.setMessage("clsEmployee_Master", 114, "Probation To Date")
            Language.setMessage("clsEmployee_Master", 115, "Leaving Date")
            Language.setMessage("clsEmployee_Master", 116, "Retirement Date")
            Language.setMessage("clsEmployee_Master", 157, "Reinstatement Date")
            Language.setMessage("clsEmployee_Master", 158, "EOC Date")
            Language.setMessage("clsEmployee_Master", 160, "Cofirmation Date")
            Language.setMessage(mstrModuleName, 1, "Enroll Employee Finger Print")
            Language.setMessage(mstrModuleName, 2, "Delete Employee Enroll Finger Print")
            Language.setMessage(mstrModuleName, 3, "Enroll Employee Card")
            Language.setMessage(mstrModuleName, 4, "Delete Employee Enroll Card")
            Language.setMessage(mstrModuleName, 5, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Some of the email address(s) are blank. Do you want to continue?")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this Employee?")
            Language.setMessage(mstrModuleName, 8, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 9, "Please Check Employee(s) from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 10, "Select")
            Language.setMessage(mstrModuleName, 11, "(A). Approved")
            Language.setMessage(mstrModuleName, 12, "(P). Pending")
            Language.setMessage(mstrModuleName, 13, "Select Employee")
            Language.setMessage(mstrModuleName, 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings.")
            Language.setMessage(mstrModuleName, 18, "Please check atleast one field in order to save.")
            Language.setMessage(mstrModuleName, 19, "Problem in saving checked fields data.")
            Language.setMessage(mstrModuleName, 20, "Sorry, The following columns are mandatory (Email,Employee Name). Please add these columns in order to send mail.")
            Language.setMessage(mstrModuleName, 21, "Sorry, The following column is mandatory (Employee Name). Please add this column in order to print/export letter.")
            Language.setMessage(mstrModuleName, 100, "From Date:")
            Language.setMessage(mstrModuleName, 101, "To")
            Language.setMessage(mstrModuleName, 103, "Employee List exported successfully to the given path.")
            Language.setMessage(mstrModuleName, 104, "Total Employee")
            Language.setMessage(mstrModuleName, 105, "Employee As On Date")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class





