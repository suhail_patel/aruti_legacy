﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeMaster
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Me.components = New System.ComponentModel.Container
Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeMaster))
Me.pnlEmployeeMaster = New System.Windows.Forms.Panel
Me.objFooter = New eZee.Common.eZeeFooter
Me.objtblPanel = New System.Windows.Forms.TableLayoutPanel
Me.lblParentData = New System.Windows.Forms.Label
Me.Panel1 = New System.Windows.Forms.Panel
Me.Panel3 = New System.Windows.Forms.Panel
Me.lblPendingData = New System.Windows.Forms.Label
Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
Me.btnOperation = New eZee.Common.eZeeSplitButton
Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
Me.mnuScanAttachViewDoc = New System.Windows.Forms.ToolStripMenuItem
Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
Me.gbEmployeeDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.pnlImages = New System.Windows.Forms.Panel
Me.imgSignature = New eZee.Common.eZeeImageControl
Me.imgImageControl = New eZee.Common.eZeeImageControl
Me.objpnlButtons = New System.Windows.Forms.Panel
Me.objlblValue = New System.Windows.Forms.Label
Me.objbtnSignature = New eZee.Common.eZeeGradientButton
Me.objbtnPhoto = New eZee.Common.eZeeGradientButton
Me.objbtnSearchPolicy = New eZee.Common.eZeeGradientButton
Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
Me.cboPolicy = New System.Windows.Forms.ComboBox
Me.lblPolicy = New System.Windows.Forms.Label
Me.txtShift = New System.Windows.Forms.TextBox
Me.tabcEmployeeDetails = New System.Windows.Forms.TabControl
Me.tabpMainInfo = New System.Windows.Forms.TabPage
Me.gbEmployeeAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.pnlEAllocation = New System.Windows.Forms.Panel
Me.chkAssignDefaulTranHeads = New System.Windows.Forms.CheckBox
Me.lblReason = New System.Windows.Forms.Label
Me.txtAllocationReason = New System.Windows.Forms.TextBox
Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
Me.cboJobGroup = New System.Windows.Forms.ComboBox
Me.objbtnSearchJobGroup = New eZee.Common.eZeeGradientButton
Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
Me.lblJobGroup = New System.Windows.Forms.Label
Me.objbtnSearchGradeGrp = New eZee.Common.eZeeGradientButton
Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
Me.objbtnSearchGrades = New eZee.Common.eZeeGradientButton
Me.cboJob = New System.Windows.Forms.ComboBox
Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
Me.lblJob = New System.Windows.Forms.Label
Me.objbtnSearchClassGrp = New eZee.Common.eZeeGradientButton
Me.objbtnAddJob = New eZee.Common.eZeeGradientButton
Me.objbtnSearchGradeLevel = New eZee.Common.eZeeGradientButton
Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
Me.lblTeam = New System.Windows.Forms.Label
Me.cboTeams = New System.Windows.Forms.ComboBox
Me.lblUnitGroup = New System.Windows.Forms.Label
Me.cboUnitGroup = New System.Windows.Forms.ComboBox
Me.lblSectionGroup = New System.Windows.Forms.Label
Me.cboSectionGroup = New System.Windows.Forms.ComboBox
Me.cboStation = New System.Windows.Forms.ComboBox
Me.objbtnAddCostCenter = New eZee.Common.eZeeGradientButton
Me.objbtnAddScale = New eZee.Common.eZeeGradientButton
Me.cboGradeGroup = New System.Windows.Forms.ComboBox
Me.lblTransactionHead = New System.Windows.Forms.Label
Me.lblGradeGroup = New System.Windows.Forms.Label
Me.objbtnAddTranHead = New eZee.Common.eZeeGradientButton
Me.objbtnAddGradeLevel = New eZee.Common.eZeeGradientButton
Me.cboTransactionHead = New System.Windows.Forms.ComboBox
Me.lblBranch = New System.Windows.Forms.Label
Me.lblCostCenter = New System.Windows.Forms.Label
Me.objbtnAddGrade = New eZee.Common.eZeeGradientButton
Me.cboCostCenter = New System.Windows.Forms.ComboBox
Me.objbtnAddDepartment = New eZee.Common.eZeeGradientButton
Me.lblClass = New System.Windows.Forms.Label
Me.objbtnAddGradeGrp = New eZee.Common.eZeeGradientButton
Me.cboClass = New System.Windows.Forms.ComboBox
Me.lblGradeLevel = New System.Windows.Forms.Label
Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
Me.cboLevel = New System.Windows.Forms.ComboBox
Me.cboUnits = New System.Windows.Forms.ComboBox
Me.lblUnits = New System.Windows.Forms.Label
Me.cboGrade = New System.Windows.Forms.ComboBox
Me.cboClassGroup = New System.Windows.Forms.ComboBox
Me.lblGrade = New System.Windows.Forms.Label
Me.lblClassGroup = New System.Windows.Forms.Label
Me.lblDepartmentGroup = New System.Windows.Forms.Label
Me.cboDepartment = New System.Windows.Forms.ComboBox
Me.lblScale = New System.Windows.Forms.Label
Me.txtScale = New eZee.TextBox.NumericTextBox
Me.lblSection = New System.Windows.Forms.Label
Me.lblDepartment = New System.Windows.Forms.Label
Me.cboSections = New System.Windows.Forms.ComboBox
Me.tabpAdditionalInfo = New System.Windows.Forms.TabPage
Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.lnkAttachBirthInfo = New System.Windows.Forms.LinkLabel
Me.tabcBWInfo = New System.Windows.Forms.TabControl
Me.tabpBirthInfo = New System.Windows.Forms.TabPage
Me.lblbirthinfoapproval = New System.Windows.Forms.Label
Me.lnkCopyDomicileAddress = New System.Windows.Forms.LinkLabel
Me.lblBirthVillage = New System.Windows.Forms.Label
Me.lblBirthChiefdom = New System.Windows.Forms.Label
Me.lblBirthTown = New System.Windows.Forms.Label
Me.cboBrithVillage = New System.Windows.Forms.ComboBox
Me.cboBrithTown = New System.Windows.Forms.ComboBox
Me.cboBrithChiefdom = New System.Windows.Forms.ComboBox
Me.txtVillage = New eZee.TextBox.AlphanumericTextBox
Me.lblState = New System.Windows.Forms.Label
Me.cboBirthCountry = New System.Windows.Forms.ComboBox
Me.lblVillage = New System.Windows.Forms.Label
Me.txtCertificateNo = New eZee.TextBox.AlphanumericTextBox
Me.cboBirthState = New System.Windows.Forms.ComboBox
Me.lblCertificateNo = New System.Windows.Forms.Label
Me.cboBirthCity = New System.Windows.Forms.ComboBox
Me.lblCountry = New System.Windows.Forms.Label
Me.txtWard = New eZee.TextBox.AlphanumericTextBox
Me.lblCity = New System.Windows.Forms.Label
Me.lblWard = New System.Windows.Forms.Label
Me.tabpWorkPermit = New System.Windows.Forms.TabPage
Me.elWorkPermit = New eZee.Common.eZeeLine
Me.txtResidentIssuePlace = New eZee.TextBox.AlphanumericTextBox
Me.lblResidentIssuePlace = New System.Windows.Forms.Label
Me.txtResidentPermitNo = New eZee.TextBox.AlphanumericTextBox
Me.dtpResidentExpiryDate = New System.Windows.Forms.DateTimePicker
Me.lblResidentPermitNo = New System.Windows.Forms.Label
Me.lblResidentExpiryDate = New System.Windows.Forms.Label
Me.cboResidentIssueCountry = New System.Windows.Forms.ComboBox
Me.dtpResidentIssueDate = New System.Windows.Forms.DateTimePicker
Me.lblResidentIssueCountry = New System.Windows.Forms.Label
Me.lblResidentIssueDate = New System.Windows.Forms.Label
Me.elResidentPermit = New eZee.Common.eZeeLine
Me.txtPlaceofIssue = New eZee.TextBox.AlphanumericTextBox
Me.lblPlaceOfIssue = New System.Windows.Forms.Label
Me.txtWorkPermitNo = New eZee.TextBox.AlphanumericTextBox
Me.dtpExpiryDate = New System.Windows.Forms.DateTimePicker
Me.lblWorkPermitNo = New System.Windows.Forms.Label
Me.lblExpiryDate = New System.Windows.Forms.Label
Me.cboIssueCountry = New System.Windows.Forms.ComboBox
Me.dtpIssueDate = New System.Windows.Forms.DateTimePicker
Me.lblIssueCountry = New System.Windows.Forms.Label
Me.lblIssueDate = New System.Windows.Forms.Label
Me.gbDatesDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.tabcDates_Remark = New System.Windows.Forms.TabControl
Me.tabpDates = New System.Windows.Forms.TabPage
Me.pnlEDates = New System.Windows.Forms.Panel
Me.dtpFirstAppointDate = New System.Windows.Forms.DateTimePicker
Me.LblFirstAppointmentDate = New System.Windows.Forms.Label
Me.chkExclude = New System.Windows.Forms.CheckBox
Me.dtpReinstatementDate = New System.Windows.Forms.DateTimePicker
Me.lblReinstatementDate = New System.Windows.Forms.Label
Me.objbtnAddREndDate = New eZee.Common.eZeeGradientButton
Me.objbtnAddRSuspDate = New eZee.Common.eZeeGradientButton
Me.objbtnAddRLeaveDate = New eZee.Common.eZeeGradientButton
Me.objbtnAddRPDate = New eZee.Common.eZeeGradientButton
Me.objbtnAddRConfDate = New eZee.Common.eZeeGradientButton
Me.dtpEndEmplDate = New System.Windows.Forms.DateTimePicker
Me.dtpAppointdate = New System.Windows.Forms.DateTimePicker
Me.lblAppointDate = New System.Windows.Forms.Label
Me.lblEmplDate = New System.Windows.Forms.Label
Me.dtpConfirmationDate = New System.Windows.Forms.DateTimePicker
Me.lblConfirmationDate = New System.Windows.Forms.Label
Me.objbtnAddRtirementReminder = New eZee.Common.eZeeGradientButton
Me.objbtnAddBirthReminder = New eZee.Common.eZeeGradientButton
Me.dtpRetirementDate = New System.Windows.Forms.DateTimePicker
Me.lblRetirementDate = New System.Windows.Forms.Label
Me.dtpSuspendedFrom = New System.Windows.Forms.DateTimePicker
Me.dtpSuspendedTo = New System.Windows.Forms.DateTimePicker
Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
Me.lblSuspendedTo = New System.Windows.Forms.Label
Me.dtpBirthdate = New System.Windows.Forms.DateTimePicker
Me.lblSuspendedFrom = New System.Windows.Forms.Label
Me.cboEndEmplReason = New System.Windows.Forms.ComboBox
Me.lblEmplReason = New System.Windows.Forms.Label
Me.lblBirtdate = New System.Windows.Forms.Label
Me.dtpProbationDateFrom = New System.Windows.Forms.DateTimePicker
Me.lblProbationFrom = New System.Windows.Forms.Label
Me.dtpLeavingDate = New System.Windows.Forms.DateTimePicker
Me.dtpProbationDateTo = New System.Windows.Forms.DateTimePicker
Me.lblLeavingDate = New System.Windows.Forms.Label
Me.lblProbationTo = New System.Windows.Forms.Label
Me.tabpRemark = New System.Windows.Forms.TabPage
Me.txtTerminateReason = New eZee.TextBox.AlphanumericTextBox
Me.lnkView = New System.Windows.Forms.LinkLabel
Me.gbLoginAndOtherDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.lnkClose = New System.Windows.Forms.LinkLabel
Me.objStLine2 = New eZee.Common.eZeeStraightLine
Me.txtEmployeeEmail = New eZee.TextBox.AlphanumericTextBox
Me.lnkHolidayInfo = New System.Windows.Forms.LinkLabel
Me.lblDisplayName = New System.Windows.Forms.Label
Me.lblLoginName = New System.Windows.Forms.Label
Me.txtLoginName = New eZee.TextBox.AlphanumericTextBox
Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
Me.txtDisplayName = New eZee.TextBox.AlphanumericTextBox
Me.lblPassword = New System.Windows.Forms.Label
Me.lblEmployeeEmail = New System.Windows.Forms.Label
Me.lnkShiftInformation = New System.Windows.Forms.LinkLabel
Me.tabpPersonalInfo = New System.Windows.Forms.TabPage
Me.pnlPersonalInfo = New System.Windows.Forms.Panel
Me.gbAddress = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.pnlPersonalInformation = New System.Windows.Forms.Panel
Me.gbRecruitmentAddress = New System.Windows.Forms.GroupBox
Me.lnkAttachRecruitment = New System.Windows.Forms.LinkLabel
Me.lblrecruitmentaddressapproval = New System.Windows.Forms.Label
Me.lblRecruitmentTown1 = New System.Windows.Forms.Label
Me.cboRecruitmentTown1 = New System.Windows.Forms.ComboBox
Me.lblRecruitmentVillage = New System.Windows.Forms.Label
Me.lnkRecruitmentCopyAddress = New System.Windows.Forms.LinkLabel
Me.cboRecruitmentVillage = New System.Windows.Forms.ComboBox
Me.lblRecruitmentChiefdom = New System.Windows.Forms.Label
Me.cboRecruitmentChiefdom = New System.Windows.Forms.ComboBox
Me.lblRecruitmentRoadStreet1 = New System.Windows.Forms.Label
Me.cboRecruitmentRoadStreet1 = New System.Windows.Forms.ComboBox
Me.cboRecruitmentProvince1 = New System.Windows.Forms.ComboBox
Me.lblRecruitmentProvince1 = New System.Windows.Forms.Label
Me.txtRecruitmentPlotNo = New eZee.TextBox.AlphanumericTextBox
Me.lblRPlotNo = New System.Windows.Forms.Label
Me.txtRecruitmentTelNo = New eZee.TextBox.AlphanumericTextBox
Me.lblRTelNo = New System.Windows.Forms.Label
Me.txtRecruitmentProvince = New eZee.TextBox.AlphanumericTextBox
Me.lblRRegion = New System.Windows.Forms.Label
Me.txtRecruitmentEstate = New eZee.TextBox.AlphanumericTextBox
Me.txtRecruitmentRoad = New eZee.TextBox.AlphanumericTextBox
Me.lblRStreet = New System.Windows.Forms.Label
Me.lblREState = New System.Windows.Forms.Label
Me.cboRecruitmentState = New System.Windows.Forms.ComboBox
Me.lblRState = New System.Windows.Forms.Label
Me.cboRecruitmentPostCode = New System.Windows.Forms.ComboBox
Me.cboRecruitmentTown = New System.Windows.Forms.ComboBox
Me.lblRCode = New System.Windows.Forms.Label
Me.lblRCity = New System.Windows.Forms.Label
Me.lblRAddress2 = New System.Windows.Forms.Label
Me.cboRecruitmentCountry = New System.Windows.Forms.ComboBox
Me.txtRecruitmentAddress2 = New eZee.TextBox.AlphanumericTextBox
Me.lblRCountry = New System.Windows.Forms.Label
Me.txtRecruitmentAddress1 = New eZee.TextBox.AlphanumericTextBox
Me.lblRAddress1 = New System.Windows.Forms.Label
Me.GbDomicileAddress = New System.Windows.Forms.GroupBox
Me.lnkAttachDomicile = New System.Windows.Forms.LinkLabel
Me.lbldomicileaddressapproval = New System.Windows.Forms.Label
Me.lblDomicileTown1 = New System.Windows.Forms.Label
Me.cboDomicileTown1 = New System.Windows.Forms.ComboBox
Me.lblDomicilevillage = New System.Windows.Forms.Label
Me.cboDomicilevillage = New System.Windows.Forms.ComboBox
Me.lblDomicileChiefdom = New System.Windows.Forms.Label
Me.cboDomicileChiefdom = New System.Windows.Forms.ComboBox
Me.lblDomicileRoadStreet1 = New System.Windows.Forms.Label
Me.cboDomicileRoadStreet1 = New System.Windows.Forms.ComboBox
Me.cboDomicileProvince1 = New System.Windows.Forms.ComboBox
Me.lblDomicileProvince1 = New System.Windows.Forms.Label
Me.cboDomicileState = New System.Windows.Forms.ComboBox
Me.lblDomicileState = New System.Windows.Forms.Label
Me.lblDomicileAddress2 = New System.Windows.Forms.Label
Me.cboDomicilePostCode = New System.Windows.Forms.ComboBox
Me.cboDomicileTown = New System.Windows.Forms.ComboBox
Me.txtDomicileAltNo = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileAltNo = New System.Windows.Forms.Label
Me.txtDomicilePlotNo = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicilePlotNo = New System.Windows.Forms.Label
Me.txtDomicileProvince = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileProvince = New System.Windows.Forms.Label
Me.lblDomicilePostCode = New System.Windows.Forms.Label
Me.txtDomicileFax = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileFax = New System.Windows.Forms.Label
Me.txtDomicileMobile = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileMobileNo = New System.Windows.Forms.Label
Me.txtDomicileTelNo = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileTelNo = New System.Windows.Forms.Label
Me.txtDomicileEstate = New eZee.TextBox.AlphanumericTextBox
Me.txtDomicileRoad = New eZee.TextBox.AlphanumericTextBox
Me.txtDomicileEmail = New eZee.TextBox.AlphanumericTextBox
Me.cboDomicileCountry = New System.Windows.Forms.ComboBox
Me.txtDomicileAddress2 = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileRoad = New System.Windows.Forms.Label
Me.lblDomicileEState = New System.Windows.Forms.Label
Me.lblDomicileEmail = New System.Windows.Forms.Label
Me.lblDomicilePostTown = New System.Windows.Forms.Label
Me.lnkCopyAddress = New System.Windows.Forms.LinkLabel
Me.lblDomicilePostCountry = New System.Windows.Forms.Label
Me.txtDomicileAddress1 = New eZee.TextBox.AlphanumericTextBox
Me.lblDomicileAddress1 = New System.Windows.Forms.Label
Me.GbPresentAddress = New System.Windows.Forms.GroupBox
Me.lnkAttachPresent = New System.Windows.Forms.LinkLabel
Me.lblpresentaddressapproval = New System.Windows.Forms.Label
Me.lblPresentTown1 = New System.Windows.Forms.Label
Me.cboPresentTown1 = New System.Windows.Forms.ComboBox
Me.lblPresentVillage = New System.Windows.Forms.Label
Me.cboPresentVillage = New System.Windows.Forms.ComboBox
Me.lblPresentChiefdom = New System.Windows.Forms.Label
Me.cboPresentChiefdom = New System.Windows.Forms.ComboBox
Me.lblPresentRoadStreet1 = New System.Windows.Forms.Label
Me.cboPresentRoadStreet1 = New System.Windows.Forms.ComboBox
Me.cboPresentProvRegion1 = New System.Windows.Forms.ComboBox
Me.lblPersentProvince1 = New System.Windows.Forms.Label
Me.lblAddress2 = New System.Windows.Forms.Label
Me.lblPresentState = New System.Windows.Forms.Label
Me.cboState = New System.Windows.Forms.ComboBox
Me.cboTown = New System.Windows.Forms.ComboBox
Me.cboPostCode = New System.Windows.Forms.ComboBox
Me.lblPostTown = New System.Windows.Forms.Label
Me.txtRoad = New eZee.TextBox.AlphanumericTextBox
Me.lblRoad = New System.Windows.Forms.Label
Me.txtFax = New eZee.TextBox.AlphanumericTextBox
Me.lblFax = New System.Windows.Forms.Label
Me.txtAlternativeNo = New eZee.TextBox.AlphanumericTextBox
Me.txtEstate = New eZee.TextBox.AlphanumericTextBox
Me.lblEstate = New System.Windows.Forms.Label
Me.txtProvince = New eZee.TextBox.AlphanumericTextBox
Me.lblProvince = New System.Windows.Forms.Label
Me.lblAlternativeNo = New System.Windows.Forms.Label
Me.txtMobile = New eZee.TextBox.AlphanumericTextBox
Me.txtPlotNo = New eZee.TextBox.AlphanumericTextBox
Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
Me.cboPresentCountry = New System.Windows.Forms.ComboBox
Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
Me.lblPloteNo = New System.Windows.Forms.Label
Me.lblMobile = New System.Windows.Forms.Label
Me.lblEmail = New System.Windows.Forms.Label
Me.lblTelNo = New System.Windows.Forms.Label
Me.lblPostCountry = New System.Windows.Forms.Label
Me.lblPostcode = New System.Windows.Forms.Label
Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
Me.lblAddress = New System.Windows.Forms.Label
Me.tabpEmergencyContact = New System.Windows.Forms.TabPage
Me.gbEmergencyInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.pnlEmergencyContact = New System.Windows.Forms.Panel
Me.GbEmergencyContact1 = New System.Windows.Forms.GroupBox
Me.lnkAttachEmerContact1 = New System.Windows.Forms.LinkLabel
Me.lblEAddressapproval1 = New System.Windows.Forms.Label
Me.txtEmgEmail = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgEmail = New System.Windows.Forms.Label
Me.txtEmgFax = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgFax = New System.Windows.Forms.Label
Me.txtEmgFirstName = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgAltNo = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgTelNo = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgTelNo = New System.Windows.Forms.Label
Me.lblEmgAltNo = New System.Windows.Forms.Label
Me.lblEmgFirstName = New System.Windows.Forms.Label
Me.txtEmgLastName = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgPlotNo = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgMobile = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgMobile = New System.Windows.Forms.Label
Me.lblEmgLastName = New System.Windows.Forms.Label
Me.lblEmgPlotNo = New System.Windows.Forms.Label
Me.txtEmgAddress = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgProvince = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgProvince = New System.Windows.Forms.Label
Me.lblEmgAddress = New System.Windows.Forms.Label
Me.cboEmgCountry = New System.Windows.Forms.ComboBox
Me.lblEmgPostCountry = New System.Windows.Forms.Label
Me.txtEmgEstate = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgState = New System.Windows.Forms.ComboBox
Me.lblEmgEstate = New System.Windows.Forms.Label
Me.lblEmgPostTown = New System.Windows.Forms.Label
Me.lblEmgState = New System.Windows.Forms.Label
Me.cboEmgTown = New System.Windows.Forms.ComboBox
Me.txtEmgRoad = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgPostCode = New System.Windows.Forms.ComboBox
Me.lblEmgPostCode = New System.Windows.Forms.Label
Me.lblEmgRoad = New System.Windows.Forms.Label
Me.GbEmergencyContact2 = New System.Windows.Forms.GroupBox
Me.lnkAttachEmerContact2 = New System.Windows.Forms.LinkLabel
Me.lblEAddressapproval2 = New System.Windows.Forms.Label
Me.txtEmgEmail2 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgEmail2 = New System.Windows.Forms.Label
Me.lnkCopyEAddress2 = New System.Windows.Forms.LinkLabel
Me.txtEmgFax2 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgFax2 = New System.Windows.Forms.Label
Me.txtEmgFirstName2 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgAltNo2 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgTelNo2 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgTelNo2 = New System.Windows.Forms.Label
Me.lblEmgAltNo2 = New System.Windows.Forms.Label
Me.lblEmgFirstName2 = New System.Windows.Forms.Label
Me.txtEmgLastName2 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgPlotNo2 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgMobile2 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgMobile2 = New System.Windows.Forms.Label
Me.lblEmgLastName2 = New System.Windows.Forms.Label
Me.lblEmgPlotNo2 = New System.Windows.Forms.Label
Me.txtEmgAddress2 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgProvince2 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgProvince2 = New System.Windows.Forms.Label
Me.lblEmgAddress2 = New System.Windows.Forms.Label
Me.cboEmgCountry2 = New System.Windows.Forms.ComboBox
Me.lblEmgPostCountry2 = New System.Windows.Forms.Label
Me.txtEmgEstate2 = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgState2 = New System.Windows.Forms.ComboBox
Me.lblEmgEstate2 = New System.Windows.Forms.Label
Me.lblEmgPostTown2 = New System.Windows.Forms.Label
Me.lblEmgState2 = New System.Windows.Forms.Label
Me.cboEmgTown2 = New System.Windows.Forms.ComboBox
Me.txtEmgRoad2 = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgPostCode2 = New System.Windows.Forms.ComboBox
Me.lblEmgPostCode2 = New System.Windows.Forms.Label
Me.lblEmgRoad2 = New System.Windows.Forms.Label
Me.GbEmergencyContact3 = New System.Windows.Forms.GroupBox
Me.lnkAttachEmerContact3 = New System.Windows.Forms.LinkLabel
Me.lblEAddressapproval3 = New System.Windows.Forms.Label
Me.lnkCopyEAddress3 = New System.Windows.Forms.LinkLabel
Me.txtEmgEmail3 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgEmail3 = New System.Windows.Forms.Label
Me.txtEmgFax3 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgFax3 = New System.Windows.Forms.Label
Me.txtEmgFirstName3 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgAltNo3 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgTelNo3 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgTelNo3 = New System.Windows.Forms.Label
Me.lblEmgAltNo3 = New System.Windows.Forms.Label
Me.lblEmgFirstName3 = New System.Windows.Forms.Label
Me.txtEmgLastName3 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgPlotNo3 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgMobile3 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgMobile3 = New System.Windows.Forms.Label
Me.lblEmgLastName3 = New System.Windows.Forms.Label
Me.lblEmgPlotNo3 = New System.Windows.Forms.Label
Me.txtEmgAddress3 = New eZee.TextBox.AlphanumericTextBox
Me.txtEmgProvince3 = New eZee.TextBox.AlphanumericTextBox
Me.lblEmgProvince3 = New System.Windows.Forms.Label
Me.lblEmgAddress3 = New System.Windows.Forms.Label
Me.cboEmgCountry3 = New System.Windows.Forms.ComboBox
Me.lblEmgPostCountry3 = New System.Windows.Forms.Label
Me.txtEmgEstate3 = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgState3 = New System.Windows.Forms.ComboBox
Me.lblEmgEstate3 = New System.Windows.Forms.Label
Me.lblEmgPostTown3 = New System.Windows.Forms.Label
Me.lblEmgState3 = New System.Windows.Forms.Label
Me.cboEmgTown3 = New System.Windows.Forms.ComboBox
Me.txtEmgRoad3 = New eZee.TextBox.AlphanumericTextBox
Me.cboEmgPostCode3 = New System.Windows.Forms.ComboBox
Me.lblEmgPostCode3 = New System.Windows.Forms.Label
Me.lblEmgRoad3 = New System.Windows.Forms.Label
Me.tabpIdentityInfo = New System.Windows.Forms.TabPage
Me.pnlIdentifyInfo = New System.Windows.Forms.Panel
Me.gbIDInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.lnkIdScanAttachDoc = New System.Windows.Forms.LinkLabel
Me.objbtnAddIDType = New eZee.Common.eZeeGradientButton
Me.chkMakeAsDefault = New System.Windows.Forms.CheckBox
Me.objLine2 = New eZee.Common.eZeeLine
Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
Me.pnlIdentityInfo = New System.Windows.Forms.Panel
Me.lvIdendtifyInformation = New System.Windows.Forms.ListView
Me.colhIdType = New System.Windows.Forms.ColumnHeader
Me.colhIdSerialNo = New System.Windows.Forms.ColumnHeader
Me.colhIdNo = New System.Windows.Forms.ColumnHeader
Me.colhIssueCountry = New System.Windows.Forms.ColumnHeader
Me.colhPlaceOfIssue = New System.Windows.Forms.ColumnHeader
Me.colhIssueDate = New System.Windows.Forms.ColumnHeader
Me.colhExpiryDate = New System.Windows.Forms.ColumnHeader
Me.objcolhIdTypeId = New System.Windows.Forms.ColumnHeader
Me.objcolhCountryId = New System.Windows.Forms.ColumnHeader
Me.colhDLClass = New System.Windows.Forms.ColumnHeader
Me.colhDefault = New System.Windows.Forms.ColumnHeader
Me.objcolhIDGUID = New System.Windows.Forms.ColumnHeader
Me.colhOperationType = New System.Windows.Forms.ColumnHeader
Me.objcolhtranguid = New System.Windows.Forms.ColumnHeader
Me.lblDLClass = New System.Windows.Forms.Label
Me.txtDLClass = New eZee.TextBox.AlphanumericTextBox
Me.lblIdPlaceOfIssue = New System.Windows.Forms.Label
Me.txtIDPlaceofIssue = New eZee.TextBox.AlphanumericTextBox
Me.dtpIdExpiryDate = New System.Windows.Forms.DateTimePicker
Me.lblIdExpiryDate = New System.Windows.Forms.Label
Me.dtpIdIssueDate = New System.Windows.Forms.DateTimePicker
Me.lblIdIssueDate = New System.Windows.Forms.Label
Me.lblIdIssueCountry = New System.Windows.Forms.Label
Me.cboIdIssueCountry = New System.Windows.Forms.ComboBox
Me.lblIdentityNo = New System.Windows.Forms.Label
Me.txtIdentityNo = New eZee.TextBox.AlphanumericTextBox
Me.txtSerialNo = New eZee.TextBox.AlphanumericTextBox
Me.lblIdSerialNo = New System.Windows.Forms.Label
Me.lblIdType = New System.Windows.Forms.Label
Me.cboIdType = New System.Windows.Forms.ComboBox
Me.tabpMembership = New System.Windows.Forms.TabPage
Me.pnlMembershipinfo = New System.Windows.Forms.Panel
Me.gbMembershipInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.lnkActivateMembership = New System.Windows.Forms.LinkLabel
Me.Panel2 = New System.Windows.Forms.Panel
Me.lblMembershipName = New System.Windows.Forms.Label
Me.objbtnAddMembership = New eZee.Common.eZeeGradientButton
Me.lblMembershipType = New System.Windows.Forms.Label
Me.objbtnAddMemCategory = New eZee.Common.eZeeGradientButton
Me.cboMembershipType = New System.Windows.Forms.ComboBox
Me.cboMemCategory = New System.Windows.Forms.ComboBox
Me.txtMembershipNo = New eZee.TextBox.AlphanumericTextBox
Me.objelLine4 = New eZee.Common.eZeeLine
Me.lblMembershipRemark = New System.Windows.Forms.Label
Me.btnDeleteMembership = New eZee.Common.eZeeLightButton(Me.components)
Me.lblMembershipNo = New System.Windows.Forms.Label
Me.btnAddMembership = New eZee.Common.eZeeLightButton(Me.components)
Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
Me.lblMemIssueDate = New System.Windows.Forms.Label
Me.dtpMemIssueDate = New System.Windows.Forms.DateTimePicker
Me.lblActivationDate = New System.Windows.Forms.Label
Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
Me.pnlMembershipDataInfo = New System.Windows.Forms.Panel
Me.lvMembershipInfo = New eZee.Common.eZeeListView(Me.components)
Me.colhMembershipCategory = New System.Windows.Forms.ColumnHeader
Me.colhMembershipType = New System.Windows.Forms.ColumnHeader
Me.colhMembershipNo = New System.Windows.Forms.ColumnHeader
Me.colhMemIssueDate = New System.Windows.Forms.ColumnHeader
Me.colhStartDate = New System.Windows.Forms.ColumnHeader
Me.colhEndDate = New System.Windows.Forms.ColumnHeader
Me.colhRemark = New System.Windows.Forms.ColumnHeader
Me.objcolhMemCatUnkid = New System.Windows.Forms.ColumnHeader
Me.objcolMembershipUnkid = New System.Windows.Forms.ColumnHeader
Me.objcolhMGUID = New System.Windows.Forms.ColumnHeader
Me.objcolhccategory = New System.Windows.Forms.ColumnHeader
Me.objcolhccategoryId = New System.Windows.Forms.ColumnHeader
Me.colhMOperationType = New System.Windows.Forms.ColumnHeader
Me.objcolhMtranguid = New System.Windows.Forms.ColumnHeader
Me.lblMemExpiryDate = New System.Windows.Forms.Label
Me.btnEditMembership = New eZee.Common.eZeeLightButton(Me.components)
Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
Me.tabpOtherInfo = New System.Windows.Forms.TabPage
Me.gbOtherInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.lnkIdAttachOtherInfo = New System.Windows.Forms.LinkLabel
Me.lblotherinfoapproval = New System.Windows.Forms.Label
Me.objStOtherInfo = New eZee.Common.eZeeStraightLine
Me.txtWeight = New eZee.TextBox.NumericTextBox
Me.txtHeight = New eZee.TextBox.NumericTextBox
Me.txtSportHobbies = New eZee.TextBox.AlphanumericTextBox
Me.dtpMaritalDate = New System.Windows.Forms.DateTimePicker
Me.lblMaritalDate = New System.Windows.Forms.Label
Me.lblSportsHobbies = New System.Windows.Forms.Label
Me.cboMaritalStatus = New System.Windows.Forms.ComboBox
Me.lvDisabilities = New System.Windows.Forms.ListView
Me.colhDisabilites = New System.Windows.Forms.ColumnHeader
Me.lblMaritalStatus = New System.Windows.Forms.Label
Me.lblDisabilities = New System.Windows.Forms.Label
Me.lvAllergis = New System.Windows.Forms.ListView
Me.colhAllergies = New System.Windows.Forms.ColumnHeader
Me.lblAllergies = New System.Windows.Forms.Label
Me.lblWeight = New System.Windows.Forms.Label
Me.lblHeight = New System.Windows.Forms.Label
Me.lblLanguage4 = New System.Windows.Forms.Label
Me.cboLanguage4 = New System.Windows.Forms.ComboBox
Me.lblLanguage3 = New System.Windows.Forms.Label
Me.cboLanguage3 = New System.Windows.Forms.ComboBox
Me.lblLanguage2 = New System.Windows.Forms.Label
Me.cboLanguage2 = New System.Windows.Forms.ComboBox
Me.lblLanguage1 = New System.Windows.Forms.Label
Me.cboLanguage1 = New System.Windows.Forms.ComboBox
Me.txtExtraTel = New eZee.TextBox.AlphanumericTextBox
Me.lblExtraTel = New System.Windows.Forms.Label
Me.lblHair = New System.Windows.Forms.Label
Me.cboHair = New System.Windows.Forms.ComboBox
Me.lblReligion = New System.Windows.Forms.Label
Me.cboReligion = New System.Windows.Forms.ComboBox
Me.lblEthinCity = New System.Windows.Forms.Label
Me.cboEthincity = New System.Windows.Forms.ComboBox
Me.lblNationality = New System.Windows.Forms.Label
Me.cboNationality = New System.Windows.Forms.ComboBox
Me.lblEyeColor = New System.Windows.Forms.Label
Me.cboEyeColor = New System.Windows.Forms.ComboBox
Me.lblBloodGroup = New System.Windows.Forms.Label
Me.cboBloodGroup = New System.Windows.Forms.ComboBox
Me.lblComplexion = New System.Windows.Forms.Label
Me.cboComplexion = New System.Windows.Forms.ComboBox
Me.tabpShiftAssignment = New System.Windows.Forms.TabPage
Me.pnlShift = New System.Windows.Forms.Panel
Me.pnlShiftAssignment = New System.Windows.Forms.Panel
Me.dgvAssignedShift = New System.Windows.Forms.DataGridView
Me.dgcolhEffective_Date = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.dgcolhShiftType = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.dgcolhShiftName = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.lvAssignedShift = New eZee.Common.eZeeListView(Me.components)
Me.colhShiftType = New System.Windows.Forms.ColumnHeader
Me.colhShiftName = New System.Windows.Forms.ColumnHeader
Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
Me.btnSetDefault = New eZee.Common.eZeeLightButton(Me.components)
Me.btnRemoveDefault = New eZee.Common.eZeeLightButton(Me.components)
Me.btnDeleteShift = New eZee.Common.eZeeLightButton(Me.components)
Me.btnAssignShift = New eZee.Common.eZeeLightButton(Me.components)
Me.lblShiftAssignment = New System.Windows.Forms.Label
Me.objbtnAddShift = New eZee.Common.eZeeGradientButton
Me.gbAssignedShift = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.tabpPolicy = New System.Windows.Forms.TabPage
Me.pnlAssignedPolicy = New System.Windows.Forms.Panel
Me.dgvPolicy = New System.Windows.Forms.DataGridView
Me.dgcolhPEffective_Date = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.dgcolhPolicy = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.tapbClearance = New System.Windows.Forms.TabPage
Me.pnlClearance = New System.Windows.Forms.Panel
Me.gbClearanceInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
Me.chkMarkClear = New System.Windows.Forms.CheckBox
Me.lvClearance = New eZee.Common.eZeeListView(Me.components)
Me.colhClGroupName = New System.Windows.Forms.ColumnHeader
Me.colhClParticulars = New System.Windows.Forms.ColumnHeader
Me.colhClAmount = New System.Windows.Forms.ColumnHeader
Me.colhClStatus = New System.Windows.Forms.ColumnHeader
Me.colhClRemark = New System.Windows.Forms.ColumnHeader
Me.objbtnAddPayPoint = New eZee.Common.eZeeGradientButton
Me.objbtnAddPayType = New eZee.Common.eZeeGradientButton
Me.objbtnAddEmploymentType = New eZee.Common.eZeeGradientButton
Me.objbtnAddTitle = New eZee.Common.eZeeGradientButton
Me.cboPaypoint = New System.Windows.Forms.ComboBox
Me.lblPaypoint = New System.Windows.Forms.Label
Me.cboPayType = New System.Windows.Forms.ComboBox
Me.lblPayType = New System.Windows.Forms.Label
Me.lblShift = New System.Windows.Forms.Label
Me.cboEmploymentType = New System.Windows.Forms.ComboBox
Me.lblEmploymentType = New System.Windows.Forms.Label
Me.cboGender = New System.Windows.Forms.ComboBox
Me.lblGender = New System.Windows.Forms.Label
Me.lblTitle = New System.Windows.Forms.Label
Me.txtOtherName = New eZee.TextBox.AlphanumericTextBox
Me.objLine = New eZee.Common.eZeeLine
Me.txtFirstname = New eZee.TextBox.AlphanumericTextBox
Me.txtSurname = New eZee.TextBox.AlphanumericTextBox
Me.txtEmployeeCode = New eZee.TextBox.AlphanumericTextBox
Me.cboTitle = New System.Windows.Forms.ComboBox
Me.lblEmployeeCode = New System.Windows.Forms.Label
Me.txtIdNo = New eZee.TextBox.AlphanumericTextBox
Me.lblOthername = New System.Windows.Forms.Label
Me.lblFirstName = New System.Windows.Forms.Label
Me.lblSurname = New System.Windows.Forms.Label
Me.lblIdNo = New System.Windows.Forms.Label
Me.cboShift = New System.Windows.Forms.ComboBox
Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
Me.pnlEmployeeMaster.SuspendLayout
Me.objFooter.SuspendLayout
Me.objtblPanel.SuspendLayout
Me.cmnuOperations.SuspendLayout
Me.gbEmployeeDetails.SuspendLayout
Me.pnlImages.SuspendLayout
Me.objpnlButtons.SuspendLayout
Me.tabcEmployeeDetails.SuspendLayout
Me.tabpMainInfo.SuspendLayout
Me.gbEmployeeAllocation.SuspendLayout
Me.pnlEAllocation.SuspendLayout
Me.tabpAdditionalInfo.SuspendLayout
Me.EZeeCollapsibleContainer1.SuspendLayout
Me.tabcBWInfo.SuspendLayout
Me.tabpBirthInfo.SuspendLayout
Me.tabpWorkPermit.SuspendLayout
Me.gbDatesDetails.SuspendLayout
Me.tabcDates_Remark.SuspendLayout
Me.tabpDates.SuspendLayout
Me.pnlEDates.SuspendLayout
Me.tabpRemark.SuspendLayout
Me.gbLoginAndOtherDetails.SuspendLayout
Me.tabpPersonalInfo.SuspendLayout
Me.pnlPersonalInfo.SuspendLayout
Me.gbAddress.SuspendLayout
Me.pnlPersonalInformation.SuspendLayout
Me.gbRecruitmentAddress.SuspendLayout
Me.GbDomicileAddress.SuspendLayout
Me.GbPresentAddress.SuspendLayout
Me.tabpEmergencyContact.SuspendLayout
Me.gbEmergencyInfo.SuspendLayout
Me.pnlEmergencyContact.SuspendLayout
Me.GbEmergencyContact1.SuspendLayout
Me.GbEmergencyContact2.SuspendLayout
Me.GbEmergencyContact3.SuspendLayout
Me.tabpIdentityInfo.SuspendLayout
Me.pnlIdentifyInfo.SuspendLayout
Me.gbIDInformation.SuspendLayout
Me.pnlIdentityInfo.SuspendLayout
Me.tabpMembership.SuspendLayout
Me.pnlMembershipinfo.SuspendLayout
Me.gbMembershipInfo.SuspendLayout
Me.Panel2.SuspendLayout
Me.pnlMembershipDataInfo.SuspendLayout
Me.tabpOtherInfo.SuspendLayout
Me.gbOtherInfo.SuspendLayout
Me.tabpShiftAssignment.SuspendLayout
Me.pnlShift.SuspendLayout
Me.pnlShiftAssignment.SuspendLayout
CType(Me.dgvAssignedShift,System.ComponentModel.ISupportInitialize).BeginInit
Me.tabpPolicy.SuspendLayout
Me.pnlAssignedPolicy.SuspendLayout
CType(Me.dgvPolicy,System.ComponentModel.ISupportInitialize).BeginInit
Me.tapbClearance.SuspendLayout
Me.pnlClearance.SuspendLayout
Me.gbClearanceInfo.SuspendLayout
Me.SuspendLayout
'
'pnlEmployeeMaster
'
Me.pnlEmployeeMaster.Controls.Add(Me.objFooter)
Me.pnlEmployeeMaster.Controls.Add(Me.gbEmployeeDetails)
Me.pnlEmployeeMaster.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlEmployeeMaster.Location = New System.Drawing.Point(0, 0)
Me.pnlEmployeeMaster.Name = "pnlEmployeeMaster"
Me.pnlEmployeeMaster.Size = New System.Drawing.Size(864, 550)
Me.pnlEmployeeMaster.TabIndex = 0
'
'objFooter
'
Me.objFooter.BorderColor = System.Drawing.Color.Silver
Me.objFooter.Controls.Add(Me.objtblPanel)
Me.objFooter.Controls.Add(Me.btnClose)
Me.objFooter.Controls.Add(Me.btnOperation)
Me.objFooter.Controls.Add(Me.btnSaveInfo)
Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
Me.objFooter.Location = New System.Drawing.Point(0, 500)
Me.objFooter.Name = "objFooter"
Me.objFooter.Size = New System.Drawing.Size(864, 50)
Me.objFooter.TabIndex = 23
'
'objtblPanel
'
Me.objtblPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
Me.objtblPanel.ColumnCount = 2
Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20!))
Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 205!))
Me.objtblPanel.Controls.Add(Me.lblParentData, 1, 1)
Me.objtblPanel.Controls.Add(Me.Panel1, 0, 0)
Me.objtblPanel.Controls.Add(Me.Panel3, 0, 1)
Me.objtblPanel.Controls.Add(Me.lblPendingData, 1, 0)
Me.objtblPanel.Location = New System.Drawing.Point(333, 4)
Me.objtblPanel.Margin = New System.Windows.Forms.Padding(0)
Me.objtblPanel.Name = "objtblPanel"
Me.objtblPanel.RowCount = 3
Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20!))
Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20!))
Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20!))
Me.objtblPanel.Size = New System.Drawing.Size(199, 43)
Me.objtblPanel.TabIndex = 189
Me.objtblPanel.Visible = false
'
'lblParentData
'
Me.lblParentData.BackColor = System.Drawing.Color.Transparent
Me.lblParentData.Dock = System.Windows.Forms.DockStyle.Fill
Me.lblParentData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblParentData.ForeColor = System.Drawing.Color.Black
Me.lblParentData.Location = New System.Drawing.Point(25, 22)
Me.lblParentData.Name = "lblParentData"
Me.lblParentData.Size = New System.Drawing.Size(199, 20)
Me.lblParentData.TabIndex = 183
Me.lblParentData.Text = "Parent Detail"
Me.lblParentData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'Panel1
'
Me.Panel1.BackColor = System.Drawing.Color.PowderBlue
Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
Me.Panel1.Location = New System.Drawing.Point(4, 4)
Me.Panel1.Name = "Panel1"
Me.Panel1.Size = New System.Drawing.Size(14, 14)
Me.Panel1.TabIndex = 0
'
'Panel3
'
Me.Panel3.BackColor = System.Drawing.Color.LightCoral
Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
Me.Panel3.Location = New System.Drawing.Point(4, 25)
Me.Panel3.Name = "Panel3"
Me.Panel3.Size = New System.Drawing.Size(14, 14)
Me.Panel3.TabIndex = 1
'
'lblPendingData
'
Me.lblPendingData.BackColor = System.Drawing.Color.Transparent
Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPendingData.ForeColor = System.Drawing.Color.Black
Me.lblPendingData.Location = New System.Drawing.Point(25, 1)
Me.lblPendingData.Name = "lblPendingData"
Me.lblPendingData.Size = New System.Drawing.Size(172, 20)
Me.lblPendingData.TabIndex = 182
Me.lblPendingData.Text = "Pending Approval"
Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnClose
'
Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnClose.BackColor = System.Drawing.Color.White
Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"),System.Drawing.Image)
Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnClose.BorderColor = System.Drawing.Color.Empty
Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnClose.FlatAppearance.BorderSize = 0
Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnClose.ForeColor = System.Drawing.Color.Black
Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnClose.GradientForeColor = System.Drawing.Color.Black
Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnClose.Location = New System.Drawing.Point(759, 11)
Me.btnClose.Name = "btnClose"
Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnClose.Size = New System.Drawing.Size(93, 30)
Me.btnClose.TabIndex = 23
Me.btnClose.Text = "&Close"
Me.btnClose.UseVisualStyleBackColor = true
'
'btnOperation
'
Me.btnOperation.BorderColor = System.Drawing.Color.Black
Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
Me.btnOperation.Location = New System.Drawing.Point(14, 11)
Me.btnOperation.Name = "btnOperation"
Me.btnOperation.ShowDefaultBorderColor = true
Me.btnOperation.Size = New System.Drawing.Size(106, 30)
Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
Me.btnOperation.TabIndex = 124
Me.btnOperation.Text = "Operations"
'
'cmnuOperations
'
Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScanAttachViewDoc})
Me.cmnuOperations.Name = "cmnuOperations"
Me.cmnuOperations.Size = New System.Drawing.Size(113, 26)
'
'mnuScanAttachViewDoc
'
Me.mnuScanAttachViewDoc.Name = "mnuScanAttachViewDoc"
Me.mnuScanAttachViewDoc.Size = New System.Drawing.Size(112, 22)
Me.mnuScanAttachViewDoc.Text = "Browse"
'
'btnSaveInfo
'
Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.btnSaveInfo.BackColor = System.Drawing.Color.White
Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"),System.Drawing.Image)
Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnSaveInfo.FlatAppearance.BorderSize = 0
Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSaveInfo.Location = New System.Drawing.Point(660, 11)
Me.btnSaveInfo.Name = "btnSaveInfo"
Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
Me.btnSaveInfo.TabIndex = 5
Me.btnSaveInfo.Text = "&Save"
Me.btnSaveInfo.UseVisualStyleBackColor = true
'
'gbEmployeeDetails
'
Me.gbEmployeeDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.gbEmployeeDetails.BorderColor = System.Drawing.Color.Black
Me.gbEmployeeDetails.Checked = false
Me.gbEmployeeDetails.CollapseAllExceptThis = false
Me.gbEmployeeDetails.CollapsedHoverImage = Nothing
Me.gbEmployeeDetails.CollapsedNormalImage = Nothing
Me.gbEmployeeDetails.CollapsedPressedImage = Nothing
Me.gbEmployeeDetails.CollapseOnLoad = false
Me.gbEmployeeDetails.Controls.Add(Me.pnlImages)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnSearchPolicy)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnSearchShift)
Me.gbEmployeeDetails.Controls.Add(Me.cboPolicy)
Me.gbEmployeeDetails.Controls.Add(Me.lblPolicy)
Me.gbEmployeeDetails.Controls.Add(Me.txtShift)
Me.gbEmployeeDetails.Controls.Add(Me.tabcEmployeeDetails)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnAddPayPoint)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnAddPayType)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnAddEmploymentType)
Me.gbEmployeeDetails.Controls.Add(Me.objbtnAddTitle)
Me.gbEmployeeDetails.Controls.Add(Me.cboPaypoint)
Me.gbEmployeeDetails.Controls.Add(Me.lblPaypoint)
Me.gbEmployeeDetails.Controls.Add(Me.cboPayType)
Me.gbEmployeeDetails.Controls.Add(Me.lblPayType)
Me.gbEmployeeDetails.Controls.Add(Me.lblShift)
Me.gbEmployeeDetails.Controls.Add(Me.cboEmploymentType)
Me.gbEmployeeDetails.Controls.Add(Me.lblEmploymentType)
Me.gbEmployeeDetails.Controls.Add(Me.cboGender)
Me.gbEmployeeDetails.Controls.Add(Me.lblGender)
Me.gbEmployeeDetails.Controls.Add(Me.lblTitle)
Me.gbEmployeeDetails.Controls.Add(Me.txtOtherName)
Me.gbEmployeeDetails.Controls.Add(Me.objLine)
Me.gbEmployeeDetails.Controls.Add(Me.txtFirstname)
Me.gbEmployeeDetails.Controls.Add(Me.txtSurname)
Me.gbEmployeeDetails.Controls.Add(Me.txtEmployeeCode)
Me.gbEmployeeDetails.Controls.Add(Me.cboTitle)
Me.gbEmployeeDetails.Controls.Add(Me.lblEmployeeCode)
Me.gbEmployeeDetails.Controls.Add(Me.txtIdNo)
Me.gbEmployeeDetails.Controls.Add(Me.lblOthername)
Me.gbEmployeeDetails.Controls.Add(Me.lblFirstName)
Me.gbEmployeeDetails.Controls.Add(Me.lblSurname)
Me.gbEmployeeDetails.Controls.Add(Me.lblIdNo)
Me.gbEmployeeDetails.Controls.Add(Me.cboShift)
Me.gbEmployeeDetails.ExpandedHoverImage = Nothing
Me.gbEmployeeDetails.ExpandedNormalImage = Nothing
Me.gbEmployeeDetails.ExpandedPressedImage = Nothing
Me.gbEmployeeDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbEmployeeDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbEmployeeDetails.HeaderHeight = 25
Me.gbEmployeeDetails.HeaderMessage = ""
Me.gbEmployeeDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbEmployeeDetails.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbEmployeeDetails.HeightOnCollapse = 0
Me.gbEmployeeDetails.LeftTextSpace = 0
Me.gbEmployeeDetails.Location = New System.Drawing.Point(0, 0)
Me.gbEmployeeDetails.Name = "gbEmployeeDetails"
Me.gbEmployeeDetails.OpenHeight = 545
Me.gbEmployeeDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbEmployeeDetails.ShowBorder = true
Me.gbEmployeeDetails.ShowCheckBox = false
Me.gbEmployeeDetails.ShowCollapseButton = false
Me.gbEmployeeDetails.ShowDefaultBorderColor = true
Me.gbEmployeeDetails.ShowDownButton = false
Me.gbEmployeeDetails.ShowHeader = true
Me.gbEmployeeDetails.Size = New System.Drawing.Size(864, 500)
Me.gbEmployeeDetails.TabIndex = 0
Me.gbEmployeeDetails.Temp = 0
Me.gbEmployeeDetails.Text = "Employee Details"
Me.gbEmployeeDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'pnlImages
'
Me.pnlImages.Controls.Add(Me.imgSignature)
Me.pnlImages.Controls.Add(Me.imgImageControl)
Me.pnlImages.Controls.Add(Me.objpnlButtons)
Me.pnlImages.Location = New System.Drawing.Point(648, 33)
Me.pnlImages.Name = "pnlImages"
Me.pnlImages.Size = New System.Drawing.Size(201, 165)
Me.pnlImages.TabIndex = 110
'
'imgSignature
'
Me.imgSignature._FileMode = false
Me.imgSignature._Image = Nothing
Me.imgSignature._ShowAddButton = true
Me.imgSignature._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
Me.imgSignature._ShowDeleteButton = true
Me.imgSignature.BackColor = System.Drawing.Color.Transparent
Me.imgSignature.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.imgSignature.Location = New System.Drawing.Point(12, 3)
Me.imgSignature.Name = "imgSignature"
Me.imgSignature.Size = New System.Drawing.Size(105, 54)
Me.imgSignature.TabIndex = 62
'
'imgImageControl
'
Me.imgImageControl._FileMode = false
Me.imgImageControl._Image = Nothing
Me.imgImageControl._ShowAddButton = true
Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
Me.imgImageControl._ShowDeleteButton = true
Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.imgImageControl.Location = New System.Drawing.Point(12, 63)
Me.imgImageControl.Name = "imgImageControl"
Me.imgImageControl.Size = New System.Drawing.Size(105, 59)
Me.imgImageControl.TabIndex = 22
'
'objpnlButtons
'
Me.objpnlButtons.Controls.Add(Me.objlblValue)
Me.objpnlButtons.Controls.Add(Me.objbtnSignature)
Me.objpnlButtons.Controls.Add(Me.objbtnPhoto)
Me.objpnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom
Me.objpnlButtons.Location = New System.Drawing.Point(0, 139)
Me.objpnlButtons.Name = "objpnlButtons"
Me.objpnlButtons.Size = New System.Drawing.Size(201, 26)
Me.objpnlButtons.TabIndex = 0
'
'objlblValue
'
Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.objlblValue.ForeColor = System.Drawing.Color.Maroon
Me.objlblValue.Location = New System.Drawing.Point(30, 7)
Me.objlblValue.Name = "objlblValue"
Me.objlblValue.Size = New System.Drawing.Size(139, 13)
Me.objlblValue.TabIndex = 111
Me.objlblValue.Text = "##"
Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.TopCenter
'
'objbtnSignature
'
Me.objbtnSignature.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.objbtnSignature.BackColor = System.Drawing.Color.Transparent
Me.objbtnSignature.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSignature.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSignature.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSignature.BorderSelected = false
Me.objbtnSignature.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSignature.Image = Global.Aruti.Main.My.Resources.Resources.icons8_circled_right_24
Me.objbtnSignature.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSignature.Location = New System.Drawing.Point(175, 3)
Me.objbtnSignature.Name = "objbtnSignature"
Me.objbtnSignature.Size = New System.Drawing.Size(21, 21)
Me.objbtnSignature.TabIndex = 110
'
'objbtnPhoto
'
Me.objbtnPhoto.BackColor = System.Drawing.Color.Transparent
Me.objbtnPhoto.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnPhoto.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnPhoto.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnPhoto.BorderSelected = false
Me.objbtnPhoto.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnPhoto.Image = Global.Aruti.Main.My.Resources.Resources.icons8_back_arrow_24__1_
Me.objbtnPhoto.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnPhoto.Location = New System.Drawing.Point(3, 3)
Me.objbtnPhoto.Name = "objbtnPhoto"
Me.objbtnPhoto.Size = New System.Drawing.Size(21, 21)
Me.objbtnPhoto.TabIndex = 109
'
'objbtnSearchPolicy
'
Me.objbtnSearchPolicy.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchPolicy.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchPolicy.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchPolicy.BorderSelected = false
Me.objbtnSearchPolicy.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchPolicy.Location = New System.Drawing.Point(604, 168)
Me.objbtnSearchPolicy.Name = "objbtnSearchPolicy"
Me.objbtnSearchPolicy.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchPolicy.TabIndex = 108
'
'objbtnSearchShift
'
Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchShift.BorderSelected = false
Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchShift.Location = New System.Drawing.Point(604, 141)
Me.objbtnSearchShift.Name = "objbtnSearchShift"
Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchShift.TabIndex = 107
'
'cboPolicy
'
Me.cboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPolicy.FormattingEnabled = true
Me.cboPolicy.Location = New System.Drawing.Point(432, 168)
Me.cboPolicy.Name = "cboPolicy"
Me.cboPolicy.Size = New System.Drawing.Size(166, 21)
Me.cboPolicy.TabIndex = 50
'
'lblPolicy
'
Me.lblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPolicy.Location = New System.Drawing.Point(340, 171)
Me.lblPolicy.Name = "lblPolicy"
Me.lblPolicy.Size = New System.Drawing.Size(86, 15)
Me.lblPolicy.TabIndex = 49
Me.lblPolicy.Text = "Policy"
Me.lblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtShift
'
Me.txtShift.BackColor = System.Drawing.Color.White
Me.txtShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtShift.Location = New System.Drawing.Point(432, 141)
Me.txtShift.Name = "txtShift"
Me.txtShift.ReadOnly = true
Me.txtShift.Size = New System.Drawing.Size(166, 21)
Me.txtShift.TabIndex = 47
'
'tabcEmployeeDetails
'
Me.tabcEmployeeDetails.Controls.Add(Me.tabpMainInfo)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpAdditionalInfo)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpPersonalInfo)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpEmergencyContact)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpIdentityInfo)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpMembership)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpOtherInfo)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpShiftAssignment)
Me.tabcEmployeeDetails.Controls.Add(Me.tabpPolicy)
Me.tabcEmployeeDetails.Controls.Add(Me.tapbClearance)
Me.tabcEmployeeDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.tabcEmployeeDetails.HotTrack = true
Me.tabcEmployeeDetails.Location = New System.Drawing.Point(3, 204)
Me.tabcEmployeeDetails.Multiline = true
Me.tabcEmployeeDetails.Name = "tabcEmployeeDetails"
Me.tabcEmployeeDetails.SelectedIndex = 0
Me.tabcEmployeeDetails.Size = New System.Drawing.Size(858, 292)
Me.tabcEmployeeDetails.TabIndex = 24
'
'tabpMainInfo
'
Me.tabpMainInfo.Controls.Add(Me.gbEmployeeAllocation)
Me.tabpMainInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpMainInfo.Name = "tabpMainInfo"
Me.tabpMainInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpMainInfo.Size = New System.Drawing.Size(850, 266)
Me.tabpMainInfo.TabIndex = 0
Me.tabpMainInfo.Text = "Allocation/Analysis"
Me.tabpMainInfo.UseVisualStyleBackColor = true
'
'gbEmployeeAllocation
'
Me.gbEmployeeAllocation.BorderColor = System.Drawing.Color.Black
Me.gbEmployeeAllocation.Checked = false
Me.gbEmployeeAllocation.CollapseAllExceptThis = false
Me.gbEmployeeAllocation.CollapsedHoverImage = Nothing
Me.gbEmployeeAllocation.CollapsedNormalImage = Nothing
Me.gbEmployeeAllocation.CollapsedPressedImage = Nothing
Me.gbEmployeeAllocation.CollapseOnLoad = true
Me.gbEmployeeAllocation.Controls.Add(Me.pnlEAllocation)
Me.gbEmployeeAllocation.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbEmployeeAllocation.ExpandedHoverImage = Nothing
Me.gbEmployeeAllocation.ExpandedNormalImage = Nothing
Me.gbEmployeeAllocation.ExpandedPressedImage = Nothing
Me.gbEmployeeAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbEmployeeAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbEmployeeAllocation.HeaderHeight = 25
Me.gbEmployeeAllocation.HeaderMessage = ""
Me.gbEmployeeAllocation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbEmployeeAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbEmployeeAllocation.HeightOnCollapse = 178
Me.gbEmployeeAllocation.LeftTextSpace = 0
Me.gbEmployeeAllocation.Location = New System.Drawing.Point(3, 3)
Me.gbEmployeeAllocation.Margin = New System.Windows.Forms.Padding(0)
Me.gbEmployeeAllocation.Name = "gbEmployeeAllocation"
Me.gbEmployeeAllocation.OpenHeight = 260
Me.gbEmployeeAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbEmployeeAllocation.ShowBorder = true
Me.gbEmployeeAllocation.ShowCheckBox = false
Me.gbEmployeeAllocation.ShowCollapseButton = false
Me.gbEmployeeAllocation.ShowDefaultBorderColor = true
Me.gbEmployeeAllocation.ShowDownButton = false
Me.gbEmployeeAllocation.ShowHeader = true
Me.gbEmployeeAllocation.Size = New System.Drawing.Size(844, 260)
Me.gbEmployeeAllocation.TabIndex = 0
Me.gbEmployeeAllocation.Temp = 0
Me.gbEmployeeAllocation.Text = "Employee Allocation"
Me.gbEmployeeAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'pnlEAllocation
'
Me.pnlEAllocation.AutoScroll = true
Me.pnlEAllocation.Controls.Add(Me.chkAssignDefaulTranHeads)
Me.pnlEAllocation.Controls.Add(Me.lblReason)
Me.pnlEAllocation.Controls.Add(Me.txtAllocationReason)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchUnits)
Me.pnlEAllocation.Controls.Add(Me.cboJobGroup)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchJobGroup)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchTeam)
Me.pnlEAllocation.Controls.Add(Me.lblJobGroup)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchGradeGrp)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchJob)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchGrades)
Me.pnlEAllocation.Controls.Add(Me.cboJob)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchClass)
Me.pnlEAllocation.Controls.Add(Me.lblJob)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchClassGrp)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddJob)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchGradeLevel)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchCostCenter)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchUnitGrp)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchSection)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchDepartment)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchSecGroup)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchDeptGrp)
Me.pnlEAllocation.Controls.Add(Me.objbtnSearchBranch)
Me.pnlEAllocation.Controls.Add(Me.lblTeam)
Me.pnlEAllocation.Controls.Add(Me.cboTeams)
Me.pnlEAllocation.Controls.Add(Me.lblUnitGroup)
Me.pnlEAllocation.Controls.Add(Me.cboUnitGroup)
Me.pnlEAllocation.Controls.Add(Me.lblSectionGroup)
Me.pnlEAllocation.Controls.Add(Me.cboSectionGroup)
Me.pnlEAllocation.Controls.Add(Me.cboStation)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddCostCenter)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddScale)
Me.pnlEAllocation.Controls.Add(Me.cboGradeGroup)
Me.pnlEAllocation.Controls.Add(Me.lblTransactionHead)
Me.pnlEAllocation.Controls.Add(Me.lblGradeGroup)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddTranHead)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddGradeLevel)
Me.pnlEAllocation.Controls.Add(Me.cboTransactionHead)
Me.pnlEAllocation.Controls.Add(Me.lblBranch)
Me.pnlEAllocation.Controls.Add(Me.lblCostCenter)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddGrade)
Me.pnlEAllocation.Controls.Add(Me.cboCostCenter)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddDepartment)
Me.pnlEAllocation.Controls.Add(Me.lblClass)
Me.pnlEAllocation.Controls.Add(Me.objbtnAddGradeGrp)
Me.pnlEAllocation.Controls.Add(Me.cboClass)
Me.pnlEAllocation.Controls.Add(Me.lblGradeLevel)
Me.pnlEAllocation.Controls.Add(Me.cboDepartmentGrp)
Me.pnlEAllocation.Controls.Add(Me.cboLevel)
Me.pnlEAllocation.Controls.Add(Me.cboUnits)
Me.pnlEAllocation.Controls.Add(Me.lblUnits)
Me.pnlEAllocation.Controls.Add(Me.cboGrade)
Me.pnlEAllocation.Controls.Add(Me.cboClassGroup)
Me.pnlEAllocation.Controls.Add(Me.lblGrade)
Me.pnlEAllocation.Controls.Add(Me.lblClassGroup)
Me.pnlEAllocation.Controls.Add(Me.lblDepartmentGroup)
Me.pnlEAllocation.Controls.Add(Me.cboDepartment)
Me.pnlEAllocation.Controls.Add(Me.lblScale)
Me.pnlEAllocation.Controls.Add(Me.txtScale)
Me.pnlEAllocation.Controls.Add(Me.lblSection)
Me.pnlEAllocation.Controls.Add(Me.lblDepartment)
Me.pnlEAllocation.Controls.Add(Me.cboSections)
Me.pnlEAllocation.Location = New System.Drawing.Point(2, 26)
Me.pnlEAllocation.Name = "pnlEAllocation"
Me.pnlEAllocation.Size = New System.Drawing.Size(839, 230)
Me.pnlEAllocation.TabIndex = 0
'
'chkAssignDefaulTranHeads
'
Me.chkAssignDefaulTranHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.chkAssignDefaulTranHeads.Location = New System.Drawing.Point(558, 171)
Me.chkAssignDefaulTranHeads.Name = "chkAssignDefaulTranHeads"
Me.chkAssignDefaulTranHeads.Size = New System.Drawing.Size(241, 17)
Me.chkAssignDefaulTranHeads.TabIndex = 62
Me.chkAssignDefaulTranHeads.Text = "Assign Default Transaction Heads"
Me.chkAssignDefaulTranHeads.UseVisualStyleBackColor = true
'
'lblReason
'
Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblReason.Location = New System.Drawing.Point(7, 171)
Me.lblReason.Name = "lblReason"
Me.lblReason.Size = New System.Drawing.Size(82, 30)
Me.lblReason.TabIndex = 0
Me.lblReason.Text = "Allocation Reason"
Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.lblReason.Visible = false
'
'txtAllocationReason
'
Me.txtAllocationReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtAllocationReason.Location = New System.Drawing.Point(95, 171)
Me.txtAllocationReason.Multiline = true
Me.txtAllocationReason.Name = "txtAllocationReason"
Me.txtAllocationReason.ReadOnly = true
Me.txtAllocationReason.Size = New System.Drawing.Size(402, 53)
Me.txtAllocationReason.TabIndex = 1
Me.txtAllocationReason.Visible = false
'
'objbtnSearchUnits
'
Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchUnits.BorderSelected = false
Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchUnits.Location = New System.Drawing.Point(503, 9)
Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchUnits.TabIndex = 44
'
'cboJobGroup
'
Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboJobGroup.DropDownWidth = 200
Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboJobGroup.FormattingEnabled = true
Me.cboJobGroup.Location = New System.Drawing.Point(642, 9)
Me.cboJobGroup.Name = "cboJobGroup"
Me.cboJobGroup.Size = New System.Drawing.Size(130, 21)
Me.cboJobGroup.TabIndex = 25
'
'objbtnSearchJobGroup
'
Me.objbtnSearchJobGroup.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchJobGroup.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchJobGroup.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchJobGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchJobGroup.BorderSelected = false
Me.objbtnSearchJobGroup.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchJobGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchJobGroup.Location = New System.Drawing.Point(778, 9)
Me.objbtnSearchJobGroup.Name = "objbtnSearchJobGroup"
Me.objbtnSearchJobGroup.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchJobGroup.TabIndex = 54
'
'objbtnSearchTeam
'
Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchTeam.BorderSelected = false
Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchTeam.Location = New System.Drawing.Point(503, 36)
Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchTeam.TabIndex = 45
'
'lblJobGroup
'
Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblJobGroup.Location = New System.Drawing.Point(555, 11)
Me.lblJobGroup.Name = "lblJobGroup"
Me.lblJobGroup.Size = New System.Drawing.Size(75, 17)
Me.lblJobGroup.TabIndex = 24
Me.lblJobGroup.Text = "Job Group"
Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnSearchGradeGrp
'
Me.objbtnSearchGradeGrp.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchGradeGrp.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchGradeGrp.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchGradeGrp.BorderSelected = false
Me.objbtnSearchGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchGradeGrp.Location = New System.Drawing.Point(528, 63)
Me.objbtnSearchGradeGrp.Name = "objbtnSearchGradeGrp"
Me.objbtnSearchGradeGrp.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchGradeGrp.TabIndex = 47
'
'objbtnSearchJob
'
Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchJob.BorderSelected = false
Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchJob.Location = New System.Drawing.Point(805, 36)
Me.objbtnSearchJob.Name = "objbtnSearchJob"
Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchJob.TabIndex = 61
'
'objbtnSearchGrades
'
Me.objbtnSearchGrades.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchGrades.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchGrades.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchGrades.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchGrades.BorderSelected = false
Me.objbtnSearchGrades.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchGrades.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchGrades.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchGrades.Location = New System.Drawing.Point(528, 90)
Me.objbtnSearchGrades.Name = "objbtnSearchGrades"
Me.objbtnSearchGrades.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchGrades.TabIndex = 49
'
'cboJob
'
Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboJob.DropDownWidth = 200
Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboJob.FormattingEnabled = true
Me.cboJob.Location = New System.Drawing.Point(642, 36)
Me.cboJob.Name = "cboJob"
Me.cboJob.Size = New System.Drawing.Size(130, 21)
Me.cboJob.TabIndex = 27
'
'objbtnSearchClass
'
Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchClass.BorderSelected = false
Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchClass.Location = New System.Drawing.Point(778, 90)
Me.objbtnSearchClass.Name = "objbtnSearchClass"
Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchClass.TabIndex = 57
'
'lblJob
'
Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblJob.Location = New System.Drawing.Point(555, 38)
Me.lblJob.Name = "lblJob"
Me.lblJob.Size = New System.Drawing.Size(75, 17)
Me.lblJob.TabIndex = 26
Me.lblJob.Text = "Job"
Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnSearchClassGrp
'
Me.objbtnSearchClassGrp.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchClassGrp.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchClassGrp.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchClassGrp.BorderSelected = false
Me.objbtnSearchClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchClassGrp.Location = New System.Drawing.Point(778, 63)
Me.objbtnSearchClassGrp.Name = "objbtnSearchClassGrp"
Me.objbtnSearchClassGrp.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchClassGrp.TabIndex = 56
'
'objbtnAddJob
'
Me.objbtnAddJob.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddJob.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddJob.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddJob.BorderSelected = false
Me.objbtnAddJob.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddJob.Location = New System.Drawing.Point(778, 36)
Me.objbtnAddJob.Name = "objbtnAddJob"
Me.objbtnAddJob.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddJob.TabIndex = 55
'
'objbtnSearchGradeLevel
'
Me.objbtnSearchGradeLevel.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchGradeLevel.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchGradeLevel.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchGradeLevel.BorderSelected = false
Me.objbtnSearchGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchGradeLevel.Location = New System.Drawing.Point(530, 117)
Me.objbtnSearchGradeLevel.Name = "objbtnSearchGradeLevel"
Me.objbtnSearchGradeLevel.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchGradeLevel.TabIndex = 52
'
'objbtnSearchCostCenter
'
Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchCostCenter.BorderSelected = false
Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(805, 117)
Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchCostCenter.TabIndex = 60
'
'objbtnSearchUnitGrp
'
Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchUnitGrp.BorderSelected = false
Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(231, 144)
Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchUnitGrp.TabIndex = 43
'
'objbtnSearchSection
'
Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchSection.BorderSelected = false
Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchSection.Location = New System.Drawing.Point(231, 117)
Me.objbtnSearchSection.Name = "objbtnSearchSection"
Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchSection.TabIndex = 42
'
'objbtnSearchDepartment
'
Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchDepartment.BorderSelected = false
Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchDepartment.Location = New System.Drawing.Point(255, 63)
Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchDepartment.TabIndex = 39
'
'objbtnSearchSecGroup
'
Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchSecGroup.BorderSelected = false
Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(231, 90)
Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchSecGroup.TabIndex = 41
'
'objbtnSearchDeptGrp
'
Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchDeptGrp.BorderSelected = false
Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(231, 38)
Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchDeptGrp.TabIndex = 37
'
'objbtnSearchBranch
'
Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnSearchBranch.BorderSelected = false
Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnSearchBranch.Location = New System.Drawing.Point(231, 9)
Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
Me.objbtnSearchBranch.TabIndex = 36
'
'lblTeam
'
Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblTeam.Location = New System.Drawing.Point(280, 38)
Me.lblTeam.Name = "lblTeam"
Me.lblTeam.Size = New System.Drawing.Size(75, 17)
Me.lblTeam.TabIndex = 14
Me.lblTeam.Text = "Team"
Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboTeams
'
Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboTeams.DropDownWidth = 200
Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboTeams.FormattingEnabled = true
Me.cboTeams.Location = New System.Drawing.Point(367, 36)
Me.cboTeams.Name = "cboTeams"
Me.cboTeams.Size = New System.Drawing.Size(130, 21)
Me.cboTeams.TabIndex = 15
'
'lblUnitGroup
'
Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblUnitGroup.Location = New System.Drawing.Point(7, 146)
Me.lblUnitGroup.Name = "lblUnitGroup"
Me.lblUnitGroup.Size = New System.Drawing.Size(82, 17)
Me.lblUnitGroup.TabIndex = 10
Me.lblUnitGroup.Text = "Unit Group"
Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboUnitGroup
'
Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboUnitGroup.DropDownWidth = 200
Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboUnitGroup.FormattingEnabled = true
Me.cboUnitGroup.Location = New System.Drawing.Point(95, 144)
Me.cboUnitGroup.Name = "cboUnitGroup"
Me.cboUnitGroup.Size = New System.Drawing.Size(130, 21)
Me.cboUnitGroup.TabIndex = 11
'
'lblSectionGroup
'
Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSectionGroup.Location = New System.Drawing.Point(7, 92)
Me.lblSectionGroup.Name = "lblSectionGroup"
Me.lblSectionGroup.Size = New System.Drawing.Size(82, 17)
Me.lblSectionGroup.TabIndex = 6
Me.lblSectionGroup.Text = "Sec. Group"
Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboSectionGroup
'
Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboSectionGroup.DropDownWidth = 200
Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboSectionGroup.FormattingEnabled = true
Me.cboSectionGroup.Location = New System.Drawing.Point(95, 90)
Me.cboSectionGroup.Name = "cboSectionGroup"
Me.cboSectionGroup.Size = New System.Drawing.Size(130, 21)
Me.cboSectionGroup.TabIndex = 7
'
'cboStation
'
Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboStation.DropDownWidth = 200
Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboStation.FormattingEnabled = true
Me.cboStation.Location = New System.Drawing.Point(95, 9)
Me.cboStation.Name = "cboStation"
Me.cboStation.Size = New System.Drawing.Size(130, 21)
Me.cboStation.TabIndex = 1
'
'objbtnAddCostCenter
'
Me.objbtnAddCostCenter.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddCostCenter.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddCostCenter.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddCostCenter.BorderSelected = false
Me.objbtnAddCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddCostCenter.Location = New System.Drawing.Point(778, 117)
Me.objbtnAddCostCenter.Name = "objbtnAddCostCenter"
Me.objbtnAddCostCenter.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddCostCenter.TabIndex = 58
'
'objbtnAddScale
'
Me.objbtnAddScale.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddScale.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddScale.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddScale.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddScale.BorderSelected = false
Me.objbtnAddScale.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddScale.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddScale.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddScale.Location = New System.Drawing.Point(503, 144)
Me.objbtnAddScale.Name = "objbtnAddScale"
Me.objbtnAddScale.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddScale.TabIndex = 53
'
'cboGradeGroup
'
Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboGradeGroup.DropDownWidth = 200
Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboGradeGroup.FormattingEnabled = true
Me.cboGradeGroup.Location = New System.Drawing.Point(367, 63)
Me.cboGradeGroup.Name = "cboGradeGroup"
Me.cboGradeGroup.Size = New System.Drawing.Size(130, 21)
Me.cboGradeGroup.TabIndex = 17
'
'lblTransactionHead
'
Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblTransactionHead.Location = New System.Drawing.Point(555, 146)
Me.lblTransactionHead.Name = "lblTransactionHead"
Me.lblTransactionHead.Size = New System.Drawing.Size(75, 17)
Me.lblTransactionHead.TabIndex = 34
Me.lblTransactionHead.Text = "Trans. Head"
Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblGradeGroup
'
Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblGradeGroup.Location = New System.Drawing.Point(280, 65)
Me.lblGradeGroup.Name = "lblGradeGroup"
Me.lblGradeGroup.Size = New System.Drawing.Size(75, 17)
Me.lblGradeGroup.TabIndex = 16
Me.lblGradeGroup.Text = "Grade Group"
Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddTranHead
'
Me.objbtnAddTranHead.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddTranHead.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddTranHead.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddTranHead.BorderSelected = false
Me.objbtnAddTranHead.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddTranHead.Location = New System.Drawing.Point(778, 144)
Me.objbtnAddTranHead.Name = "objbtnAddTranHead"
Me.objbtnAddTranHead.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddTranHead.TabIndex = 59
Me.objbtnAddTranHead.Visible = false
'
'objbtnAddGradeLevel
'
Me.objbtnAddGradeLevel.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddGradeLevel.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddGradeLevel.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddGradeLevel.BorderSelected = false
Me.objbtnAddGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddGradeLevel.Location = New System.Drawing.Point(503, 117)
Me.objbtnAddGradeLevel.Name = "objbtnAddGradeLevel"
Me.objbtnAddGradeLevel.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddGradeLevel.TabIndex = 51
'
'cboTransactionHead
'
Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboTransactionHead.DropDownWidth = 200
Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboTransactionHead.FormattingEnabled = true
Me.cboTransactionHead.Location = New System.Drawing.Point(642, 144)
Me.cboTransactionHead.Name = "cboTransactionHead"
Me.cboTransactionHead.Size = New System.Drawing.Size(130, 21)
Me.cboTransactionHead.TabIndex = 35
'
'lblBranch
'
Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblBranch.Location = New System.Drawing.Point(7, 11)
Me.lblBranch.Name = "lblBranch"
Me.lblBranch.Size = New System.Drawing.Size(82, 17)
Me.lblBranch.TabIndex = 0
Me.lblBranch.Text = "Branch"
Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblCostCenter
'
Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblCostCenter.Location = New System.Drawing.Point(555, 119)
Me.lblCostCenter.Name = "lblCostCenter"
Me.lblCostCenter.Size = New System.Drawing.Size(75, 17)
Me.lblCostCenter.TabIndex = 32
Me.lblCostCenter.Text = "Cost Center"
Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddGrade
'
Me.objbtnAddGrade.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddGrade.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddGrade.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddGrade.BorderSelected = false
Me.objbtnAddGrade.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddGrade.Location = New System.Drawing.Point(503, 90)
Me.objbtnAddGrade.Name = "objbtnAddGrade"
Me.objbtnAddGrade.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddGrade.TabIndex = 48
'
'cboCostCenter
'
Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboCostCenter.DropDownWidth = 200
Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboCostCenter.FormattingEnabled = true
Me.cboCostCenter.Location = New System.Drawing.Point(642, 117)
Me.cboCostCenter.Name = "cboCostCenter"
Me.cboCostCenter.Size = New System.Drawing.Size(130, 21)
Me.cboCostCenter.TabIndex = 33
'
'objbtnAddDepartment
'
Me.objbtnAddDepartment.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddDepartment.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddDepartment.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddDepartment.BorderSelected = false
Me.objbtnAddDepartment.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddDepartment.Location = New System.Drawing.Point(231, 63)
Me.objbtnAddDepartment.Name = "objbtnAddDepartment"
Me.objbtnAddDepartment.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddDepartment.TabIndex = 38
'
'lblClass
'
Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblClass.Location = New System.Drawing.Point(555, 92)
Me.lblClass.Name = "lblClass"
Me.lblClass.Size = New System.Drawing.Size(75, 17)
Me.lblClass.TabIndex = 30
Me.lblClass.Text = "Class"
Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddGradeGrp
'
Me.objbtnAddGradeGrp.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddGradeGrp.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddGradeGrp.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddGradeGrp.BorderSelected = false
Me.objbtnAddGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddGradeGrp.Location = New System.Drawing.Point(503, 63)
Me.objbtnAddGradeGrp.Name = "objbtnAddGradeGrp"
Me.objbtnAddGradeGrp.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddGradeGrp.TabIndex = 46
'
'cboClass
'
Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboClass.DropDownWidth = 200
Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboClass.FormattingEnabled = true
Me.cboClass.Location = New System.Drawing.Point(642, 90)
Me.cboClass.Name = "cboClass"
Me.cboClass.Size = New System.Drawing.Size(130, 21)
Me.cboClass.TabIndex = 31
'
'lblGradeLevel
'
Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblGradeLevel.Location = New System.Drawing.Point(280, 119)
Me.lblGradeLevel.Name = "lblGradeLevel"
Me.lblGradeLevel.Size = New System.Drawing.Size(75, 17)
Me.lblGradeLevel.TabIndex = 20
Me.lblGradeLevel.Text = "Grade Level"
Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDepartmentGrp
'
Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDepartmentGrp.DropDownWidth = 200
Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDepartmentGrp.FormattingEnabled = true
Me.cboDepartmentGrp.Location = New System.Drawing.Point(95, 36)
Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
Me.cboDepartmentGrp.Size = New System.Drawing.Size(130, 21)
Me.cboDepartmentGrp.TabIndex = 3
'
'cboLevel
'
Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboLevel.DropDownWidth = 200
Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboLevel.FormattingEnabled = true
Me.cboLevel.Location = New System.Drawing.Point(367, 117)
Me.cboLevel.Name = "cboLevel"
Me.cboLevel.Size = New System.Drawing.Size(130, 21)
Me.cboLevel.TabIndex = 21
'
'cboUnits
'
Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboUnits.DropDownWidth = 200
Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboUnits.FormattingEnabled = true
Me.cboUnits.Location = New System.Drawing.Point(367, 9)
Me.cboUnits.Name = "cboUnits"
Me.cboUnits.Size = New System.Drawing.Size(130, 21)
Me.cboUnits.TabIndex = 13
'
'lblUnits
'
Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblUnits.Location = New System.Drawing.Point(280, 11)
Me.lblUnits.Name = "lblUnits"
Me.lblUnits.Size = New System.Drawing.Size(75, 17)
Me.lblUnits.TabIndex = 12
Me.lblUnits.Text = "Units"
Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboGrade
'
Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboGrade.DropDownWidth = 200
Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboGrade.FormattingEnabled = true
Me.cboGrade.Location = New System.Drawing.Point(367, 90)
Me.cboGrade.Name = "cboGrade"
Me.cboGrade.Size = New System.Drawing.Size(130, 21)
Me.cboGrade.TabIndex = 19
'
'cboClassGroup
'
Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboClassGroup.DropDownWidth = 200
Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboClassGroup.FormattingEnabled = true
Me.cboClassGroup.Location = New System.Drawing.Point(642, 63)
Me.cboClassGroup.Name = "cboClassGroup"
Me.cboClassGroup.Size = New System.Drawing.Size(130, 21)
Me.cboClassGroup.TabIndex = 29
'
'lblGrade
'
Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblGrade.Location = New System.Drawing.Point(280, 92)
Me.lblGrade.Name = "lblGrade"
Me.lblGrade.Size = New System.Drawing.Size(75, 17)
Me.lblGrade.TabIndex = 18
Me.lblGrade.Text = "Grade"
Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblClassGroup
'
Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblClassGroup.Location = New System.Drawing.Point(555, 65)
Me.lblClassGroup.Name = "lblClassGroup"
Me.lblClassGroup.Size = New System.Drawing.Size(75, 17)
Me.lblClassGroup.TabIndex = 28
Me.lblClassGroup.Text = "Class Group"
Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDepartmentGroup
'
Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDepartmentGroup.Location = New System.Drawing.Point(7, 38)
Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
Me.lblDepartmentGroup.Size = New System.Drawing.Size(82, 17)
Me.lblDepartmentGroup.TabIndex = 2
Me.lblDepartmentGroup.Text = "Dept. Group"
Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDepartment
'
Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDepartment.DropDownWidth = 200
Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDepartment.FormattingEnabled = true
Me.cboDepartment.Location = New System.Drawing.Point(95, 63)
Me.cboDepartment.Name = "cboDepartment"
Me.cboDepartment.Size = New System.Drawing.Size(130, 21)
Me.cboDepartment.TabIndex = 5
'
'lblScale
'
Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblScale.Location = New System.Drawing.Point(280, 146)
Me.lblScale.Name = "lblScale"
Me.lblScale.Size = New System.Drawing.Size(75, 17)
Me.lblScale.TabIndex = 22
Me.lblScale.Text = "Scale"
Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtScale
'
Me.txtScale.AllowNegative = true
Me.txtScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
Me.txtScale.DigitsInGroup = 3
Me.txtScale.Flags = 0
Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtScale.Location = New System.Drawing.Point(367, 144)
Me.txtScale.MaxDecimalPlaces = 6
Me.txtScale.MaxWholeDigits = 21
Me.txtScale.Name = "txtScale"
Me.txtScale.Prefix = ""
Me.txtScale.RangeMax = 1.7976931348623157E+308
Me.txtScale.RangeMin = -1.7976931348623157E+308
Me.txtScale.Size = New System.Drawing.Size(130, 21)
Me.txtScale.TabIndex = 23
Me.txtScale.Text = "0"
Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
'
'lblSection
'
Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSection.Location = New System.Drawing.Point(7, 119)
Me.lblSection.Name = "lblSection"
Me.lblSection.Size = New System.Drawing.Size(82, 17)
Me.lblSection.TabIndex = 8
Me.lblSection.Text = "Sections"
Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDepartment
'
Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDepartment.Location = New System.Drawing.Point(7, 65)
Me.lblDepartment.Name = "lblDepartment"
Me.lblDepartment.Size = New System.Drawing.Size(82, 17)
Me.lblDepartment.TabIndex = 4
Me.lblDepartment.Text = "Department"
Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboSections
'
Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboSections.DropDownWidth = 200
Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboSections.FormattingEnabled = true
Me.cboSections.Location = New System.Drawing.Point(95, 117)
Me.cboSections.Name = "cboSections"
Me.cboSections.Size = New System.Drawing.Size(130, 21)
Me.cboSections.TabIndex = 9
'
'tabpAdditionalInfo
'
Me.tabpAdditionalInfo.Controls.Add(Me.EZeeCollapsibleContainer1)
Me.tabpAdditionalInfo.Controls.Add(Me.gbDatesDetails)
Me.tabpAdditionalInfo.Controls.Add(Me.gbLoginAndOtherDetails)
Me.tabpAdditionalInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpAdditionalInfo.Name = "tabpAdditionalInfo"
Me.tabpAdditionalInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpAdditionalInfo.Size = New System.Drawing.Size(850, 266)
Me.tabpAdditionalInfo.TabIndex = 2
Me.tabpAdditionalInfo.Text = "Personal/Dates"
Me.tabpAdditionalInfo.UseVisualStyleBackColor = true
'
'EZeeCollapsibleContainer1
'
Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
Me.EZeeCollapsibleContainer1.Checked = false
Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = false
Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
Me.EZeeCollapsibleContainer1.CollapseOnLoad = false
Me.EZeeCollapsibleContainer1.Controls.Add(Me.lnkAttachBirthInfo)
Me.EZeeCollapsibleContainer1.Controls.Add(Me.tabcBWInfo)
Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.EZeeCollapsibleContainer1.HeaderHeight = 25
Me.EZeeCollapsibleContainer1.HeaderMessage = ""
Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(3, 3)
Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
Me.EZeeCollapsibleContainer1.OpenHeight = 300
Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.EZeeCollapsibleContainer1.ShowBorder = true
Me.EZeeCollapsibleContainer1.ShowCheckBox = false
Me.EZeeCollapsibleContainer1.ShowCollapseButton = false
Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = true
Me.EZeeCollapsibleContainer1.ShowDownButton = false
Me.EZeeCollapsibleContainer1.ShowHeader = true
Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(411, 260)
Me.EZeeCollapsibleContainer1.TabIndex = 4
Me.EZeeCollapsibleContainer1.Temp = 0
Me.EZeeCollapsibleContainer1.Text = "Birth/Work Permit Information"
Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkAttachBirthInfo
'
Me.lnkAttachBirthInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachBirthInfo.Location = New System.Drawing.Point(230, 5)
Me.lnkAttachBirthInfo.Name = "lnkAttachBirthInfo"
Me.lnkAttachBirthInfo.Size = New System.Drawing.Size(178, 17)
Me.lnkAttachBirthInfo.TabIndex = 56
Me.lnkAttachBirthInfo.TabStop = true
Me.lnkAttachBirthInfo.Text = "Scan/Attach Document"
Me.lnkAttachBirthInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'tabcBWInfo
'
Me.tabcBWInfo.Controls.Add(Me.tabpBirthInfo)
Me.tabcBWInfo.Controls.Add(Me.tabpWorkPermit)
Me.tabcBWInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.tabcBWInfo.Location = New System.Drawing.Point(3, 26)
Me.tabcBWInfo.Name = "tabcBWInfo"
Me.tabcBWInfo.SelectedIndex = 0
Me.tabcBWInfo.Size = New System.Drawing.Size(406, 234)
Me.tabcBWInfo.TabIndex = 1
'
'tabpBirthInfo
'
Me.tabpBirthInfo.Controls.Add(Me.lblbirthinfoapproval)
Me.tabpBirthInfo.Controls.Add(Me.lnkCopyDomicileAddress)
Me.tabpBirthInfo.Controls.Add(Me.lblBirthVillage)
Me.tabpBirthInfo.Controls.Add(Me.lblBirthChiefdom)
Me.tabpBirthInfo.Controls.Add(Me.lblBirthTown)
Me.tabpBirthInfo.Controls.Add(Me.cboBrithVillage)
Me.tabpBirthInfo.Controls.Add(Me.cboBrithTown)
Me.tabpBirthInfo.Controls.Add(Me.cboBrithChiefdom)
Me.tabpBirthInfo.Controls.Add(Me.txtVillage)
Me.tabpBirthInfo.Controls.Add(Me.lblState)
Me.tabpBirthInfo.Controls.Add(Me.cboBirthCountry)
Me.tabpBirthInfo.Controls.Add(Me.lblVillage)
Me.tabpBirthInfo.Controls.Add(Me.txtCertificateNo)
Me.tabpBirthInfo.Controls.Add(Me.cboBirthState)
Me.tabpBirthInfo.Controls.Add(Me.lblCertificateNo)
Me.tabpBirthInfo.Controls.Add(Me.cboBirthCity)
Me.tabpBirthInfo.Controls.Add(Me.lblCountry)
Me.tabpBirthInfo.Controls.Add(Me.txtWard)
Me.tabpBirthInfo.Controls.Add(Me.lblCity)
Me.tabpBirthInfo.Controls.Add(Me.lblWard)
Me.tabpBirthInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpBirthInfo.Name = "tabpBirthInfo"
Me.tabpBirthInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpBirthInfo.Size = New System.Drawing.Size(398, 208)
Me.tabpBirthInfo.TabIndex = 0
Me.tabpBirthInfo.Text = "Birth Info"
Me.tabpBirthInfo.UseVisualStyleBackColor = true
'
'lblbirthinfoapproval
'
Me.lblbirthinfoapproval.BackColor = System.Drawing.Color.PowderBlue
Me.lblbirthinfoapproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblbirthinfoapproval.ForeColor = System.Drawing.Color.Black
Me.lblbirthinfoapproval.Location = New System.Drawing.Point(5, 9)
Me.lblbirthinfoapproval.Name = "lblbirthinfoapproval"
Me.lblbirthinfoapproval.Size = New System.Drawing.Size(165, 16)
Me.lblbirthinfoapproval.TabIndex = 218
Me.lblbirthinfoapproval.Text = "Pending Approval"
Me.lblbirthinfoapproval.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblbirthinfoapproval.Visible = false
'
'lnkCopyDomicileAddress
'
Me.lnkCopyDomicileAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkCopyDomicileAddress.Location = New System.Drawing.Point(252, 9)
Me.lnkCopyDomicileAddress.Name = "lnkCopyDomicileAddress"
Me.lnkCopyDomicileAddress.Size = New System.Drawing.Size(140, 13)
Me.lnkCopyDomicileAddress.TabIndex = 33
Me.lnkCopyDomicileAddress.TabStop = true
Me.lnkCopyDomicileAddress.Text = "Copy Domicile Address"
'
'lblBirthVillage
'
Me.lblBirthVillage.Location = New System.Drawing.Point(5, 144)
Me.lblBirthVillage.Name = "lblBirthVillage"
Me.lblBirthVillage.Size = New System.Drawing.Size(77, 15)
Me.lblBirthVillage.TabIndex = 20
Me.lblBirthVillage.Text = "Village1"
'
'lblBirthChiefdom
'
Me.lblBirthChiefdom.Location = New System.Drawing.Point(209, 118)
Me.lblBirthChiefdom.Name = "lblBirthChiefdom"
Me.lblBirthChiefdom.Size = New System.Drawing.Size(69, 15)
Me.lblBirthChiefdom.TabIndex = 19
Me.lblBirthChiefdom.Text = "Chiefdom "
'
'lblBirthTown
'
Me.lblBirthTown.Location = New System.Drawing.Point(5, 118)
Me.lblBirthTown.Name = "lblBirthTown"
Me.lblBirthTown.Size = New System.Drawing.Size(77, 15)
Me.lblBirthTown.TabIndex = 18
Me.lblBirthTown.Text = "Town1"
'
'cboBrithVillage
'
Me.cboBrithVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBrithVillage.DropDownWidth = 180
Me.cboBrithVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBrithVillage.FormattingEnabled = true
Me.cboBrithVillage.Location = New System.Drawing.Point(88, 142)
Me.cboBrithVillage.Name = "cboBrithVillage"
Me.cboBrithVillage.Size = New System.Drawing.Size(112, 21)
Me.cboBrithVillage.TabIndex = 17
'
'cboBrithTown
'
Me.cboBrithTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBrithTown.DropDownWidth = 180
Me.cboBrithTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBrithTown.FormattingEnabled = true
Me.cboBrithTown.Location = New System.Drawing.Point(88, 115)
Me.cboBrithTown.Name = "cboBrithTown"
Me.cboBrithTown.Size = New System.Drawing.Size(112, 21)
Me.cboBrithTown.TabIndex = 16
'
'cboBrithChiefdom
'
Me.cboBrithChiefdom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBrithChiefdom.DropDownWidth = 180
Me.cboBrithChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBrithChiefdom.FormattingEnabled = true
Me.cboBrithChiefdom.Location = New System.Drawing.Point(284, 115)
Me.cboBrithChiefdom.Name = "cboBrithChiefdom"
Me.cboBrithChiefdom.Size = New System.Drawing.Size(104, 21)
Me.cboBrithChiefdom.TabIndex = 15
'
'txtVillage
'
Me.txtVillage.Flags = 0
Me.txtVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtVillage.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtVillage.Location = New System.Drawing.Point(284, 88)
Me.txtVillage.Name = "txtVillage"
Me.txtVillage.Size = New System.Drawing.Size(104, 21)
Me.txtVillage.TabIndex = 13
'
'lblState
'
Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblState.Location = New System.Drawing.Point(209, 37)
Me.lblState.Name = "lblState"
Me.lblState.Size = New System.Drawing.Size(69, 15)
Me.lblState.TabIndex = 6
Me.lblState.Text = "State"
Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboBirthCountry
'
Me.cboBirthCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBirthCountry.DropDownWidth = 180
Me.cboBirthCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBirthCountry.FormattingEnabled = true
Me.cboBirthCountry.Location = New System.Drawing.Point(88, 34)
Me.cboBirthCountry.Name = "cboBirthCountry"
Me.cboBirthCountry.Size = New System.Drawing.Size(112, 21)
Me.cboBirthCountry.TabIndex = 5
'
'lblVillage
'
Me.lblVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblVillage.Location = New System.Drawing.Point(209, 91)
Me.lblVillage.Name = "lblVillage"
Me.lblVillage.Size = New System.Drawing.Size(69, 15)
Me.lblVillage.TabIndex = 12
Me.lblVillage.Text = "Village"
Me.lblVillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtCertificateNo
'
Me.txtCertificateNo.BackColor = System.Drawing.SystemColors.Window
Me.txtCertificateNo.Flags = 0
Me.txtCertificateNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtCertificateNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtCertificateNo.Location = New System.Drawing.Point(88, 88)
Me.txtCertificateNo.Name = "txtCertificateNo"
Me.txtCertificateNo.Size = New System.Drawing.Size(112, 21)
Me.txtCertificateNo.TabIndex = 3
'
'cboBirthState
'
Me.cboBirthState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBirthState.DropDownWidth = 180
Me.cboBirthState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBirthState.FormattingEnabled = true
Me.cboBirthState.Location = New System.Drawing.Point(284, 34)
Me.cboBirthState.Name = "cboBirthState"
Me.cboBirthState.Size = New System.Drawing.Size(104, 21)
Me.cboBirthState.TabIndex = 7
'
'lblCertificateNo
'
Me.lblCertificateNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblCertificateNo.Location = New System.Drawing.Point(5, 91)
Me.lblCertificateNo.Name = "lblCertificateNo"
Me.lblCertificateNo.Size = New System.Drawing.Size(77, 15)
Me.lblCertificateNo.TabIndex = 2
Me.lblCertificateNo.Text = "Cert. No."
Me.lblCertificateNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboBirthCity
'
Me.cboBirthCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBirthCity.DropDownWidth = 180
Me.cboBirthCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBirthCity.FormattingEnabled = true
Me.cboBirthCity.Location = New System.Drawing.Point(88, 61)
Me.cboBirthCity.Name = "cboBirthCity"
Me.cboBirthCity.Size = New System.Drawing.Size(112, 21)
Me.cboBirthCity.TabIndex = 11
'
'lblCountry
'
Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblCountry.Location = New System.Drawing.Point(5, 37)
Me.lblCountry.Name = "lblCountry"
Me.lblCountry.Size = New System.Drawing.Size(77, 15)
Me.lblCountry.TabIndex = 4
Me.lblCountry.Text = "Country"
Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtWard
'
Me.txtWard.Flags = 0
Me.txtWard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtWard.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtWard.Location = New System.Drawing.Point(284, 61)
Me.txtWard.Name = "txtWard"
Me.txtWard.Size = New System.Drawing.Size(104, 21)
Me.txtWard.TabIndex = 9
'
'lblCity
'
Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblCity.Location = New System.Drawing.Point(5, 64)
Me.lblCity.Name = "lblCity"
Me.lblCity.Size = New System.Drawing.Size(77, 15)
Me.lblCity.TabIndex = 10
Me.lblCity.Text = "City"
Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblWard
'
Me.lblWard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblWard.Location = New System.Drawing.Point(209, 64)
Me.lblWard.Name = "lblWard"
Me.lblWard.Size = New System.Drawing.Size(69, 15)
Me.lblWard.TabIndex = 8
Me.lblWard.Text = "Ward"
Me.lblWard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabpWorkPermit
'
Me.tabpWorkPermit.Controls.Add(Me.elWorkPermit)
Me.tabpWorkPermit.Controls.Add(Me.txtResidentIssuePlace)
Me.tabpWorkPermit.Controls.Add(Me.lblResidentIssuePlace)
Me.tabpWorkPermit.Controls.Add(Me.txtResidentPermitNo)
Me.tabpWorkPermit.Controls.Add(Me.dtpResidentExpiryDate)
Me.tabpWorkPermit.Controls.Add(Me.lblResidentPermitNo)
Me.tabpWorkPermit.Controls.Add(Me.lblResidentExpiryDate)
Me.tabpWorkPermit.Controls.Add(Me.cboResidentIssueCountry)
Me.tabpWorkPermit.Controls.Add(Me.dtpResidentIssueDate)
Me.tabpWorkPermit.Controls.Add(Me.lblResidentIssueCountry)
Me.tabpWorkPermit.Controls.Add(Me.lblResidentIssueDate)
Me.tabpWorkPermit.Controls.Add(Me.elResidentPermit)
Me.tabpWorkPermit.Controls.Add(Me.txtPlaceofIssue)
Me.tabpWorkPermit.Controls.Add(Me.lblPlaceOfIssue)
Me.tabpWorkPermit.Controls.Add(Me.txtWorkPermitNo)
Me.tabpWorkPermit.Controls.Add(Me.dtpExpiryDate)
Me.tabpWorkPermit.Controls.Add(Me.lblWorkPermitNo)
Me.tabpWorkPermit.Controls.Add(Me.lblExpiryDate)
Me.tabpWorkPermit.Controls.Add(Me.cboIssueCountry)
Me.tabpWorkPermit.Controls.Add(Me.dtpIssueDate)
Me.tabpWorkPermit.Controls.Add(Me.lblIssueCountry)
Me.tabpWorkPermit.Controls.Add(Me.lblIssueDate)
Me.tabpWorkPermit.Location = New System.Drawing.Point(4, 22)
Me.tabpWorkPermit.Name = "tabpWorkPermit"
Me.tabpWorkPermit.Padding = New System.Windows.Forms.Padding(3)
Me.tabpWorkPermit.Size = New System.Drawing.Size(398, 208)
Me.tabpWorkPermit.TabIndex = 1
Me.tabpWorkPermit.Text = "Work / Resident Permit"
Me.tabpWorkPermit.UseVisualStyleBackColor = true
'
'elWorkPermit
'
Me.elWorkPermit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.elWorkPermit.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.elWorkPermit.Location = New System.Drawing.Point(9, 7)
Me.elWorkPermit.Name = "elWorkPermit"
Me.elWorkPermit.Size = New System.Drawing.Size(385, 14)
Me.elWorkPermit.TabIndex = 21
Me.elWorkPermit.Text = "Work Permit"
'
'txtResidentIssuePlace
'
Me.txtResidentIssuePlace.Flags = 0
Me.txtResidentIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtResidentIssuePlace.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtResidentIssuePlace.Location = New System.Drawing.Point(97, 181)
Me.txtResidentIssuePlace.Name = "txtResidentIssuePlace"
Me.txtResidentIssuePlace.Size = New System.Drawing.Size(294, 21)
Me.txtResidentIssuePlace.TabIndex = 16
'
'lblResidentIssuePlace
'
Me.lblResidentIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblResidentIssuePlace.Location = New System.Drawing.Point(9, 184)
Me.lblResidentIssuePlace.Name = "lblResidentIssuePlace"
Me.lblResidentIssuePlace.Size = New System.Drawing.Size(82, 15)
Me.lblResidentIssuePlace.TabIndex = 15
Me.lblResidentIssuePlace.Text = "Place Of Issue"
Me.lblResidentIssuePlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtResidentPermitNo
'
Me.txtResidentPermitNo.BackColor = System.Drawing.SystemColors.Window
Me.txtResidentPermitNo.Flags = 0
Me.txtResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtResidentPermitNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtResidentPermitNo.Location = New System.Drawing.Point(97, 127)
Me.txtResidentPermitNo.Name = "txtResidentPermitNo"
Me.txtResidentPermitNo.Size = New System.Drawing.Size(112, 21)
Me.txtResidentPermitNo.TabIndex = 12
'
'dtpResidentExpiryDate
'
Me.dtpResidentExpiryDate.Checked = false
Me.dtpResidentExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpResidentExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpResidentExpiryDate.Location = New System.Drawing.Point(287, 154)
Me.dtpResidentExpiryDate.Name = "dtpResidentExpiryDate"
Me.dtpResidentExpiryDate.ShowCheckBox = true
Me.dtpResidentExpiryDate.Size = New System.Drawing.Size(104, 21)
Me.dtpResidentExpiryDate.TabIndex = 20
'
'lblResidentPermitNo
'
Me.lblResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblResidentPermitNo.Location = New System.Drawing.Point(9, 130)
Me.lblResidentPermitNo.Name = "lblResidentPermitNo"
Me.lblResidentPermitNo.Size = New System.Drawing.Size(82, 15)
Me.lblResidentPermitNo.TabIndex = 11
Me.lblResidentPermitNo.Text = "Permit No."
Me.lblResidentPermitNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblResidentExpiryDate
'
Me.lblResidentExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblResidentExpiryDate.Location = New System.Drawing.Point(215, 157)
Me.lblResidentExpiryDate.Name = "lblResidentExpiryDate"
Me.lblResidentExpiryDate.Size = New System.Drawing.Size(66, 15)
Me.lblResidentExpiryDate.TabIndex = 19
Me.lblResidentExpiryDate.Text = "Expiry Date"
Me.lblResidentExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboResidentIssueCountry
'
Me.cboResidentIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboResidentIssueCountry.DropDownWidth = 120
Me.cboResidentIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboResidentIssueCountry.FormattingEnabled = true
Me.cboResidentIssueCountry.Location = New System.Drawing.Point(97, 154)
Me.cboResidentIssueCountry.Name = "cboResidentIssueCountry"
Me.cboResidentIssueCountry.Size = New System.Drawing.Size(112, 21)
Me.cboResidentIssueCountry.TabIndex = 14
'
'dtpResidentIssueDate
'
Me.dtpResidentIssueDate.Checked = false
Me.dtpResidentIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpResidentIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpResidentIssueDate.Location = New System.Drawing.Point(287, 127)
Me.dtpResidentIssueDate.Name = "dtpResidentIssueDate"
Me.dtpResidentIssueDate.ShowCheckBox = true
Me.dtpResidentIssueDate.Size = New System.Drawing.Size(104, 21)
Me.dtpResidentIssueDate.TabIndex = 18
'
'lblResidentIssueCountry
'
Me.lblResidentIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblResidentIssueCountry.Location = New System.Drawing.Point(9, 157)
Me.lblResidentIssueCountry.Name = "lblResidentIssueCountry"
Me.lblResidentIssueCountry.Size = New System.Drawing.Size(82, 15)
Me.lblResidentIssueCountry.TabIndex = 13
Me.lblResidentIssueCountry.Text = "Issue Country"
Me.lblResidentIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblResidentIssueDate
'
Me.lblResidentIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblResidentIssueDate.Location = New System.Drawing.Point(215, 130)
Me.lblResidentIssueDate.Name = "lblResidentIssueDate"
Me.lblResidentIssueDate.Size = New System.Drawing.Size(65, 15)
Me.lblResidentIssueDate.TabIndex = 17
Me.lblResidentIssueDate.Text = "Issue Date"
Me.lblResidentIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'elResidentPermit
'
Me.elResidentPermit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.elResidentPermit.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.elResidentPermit.Location = New System.Drawing.Point(6, 108)
Me.elResidentPermit.Name = "elResidentPermit"
Me.elResidentPermit.Size = New System.Drawing.Size(385, 14)
Me.elResidentPermit.TabIndex = 10
Me.elResidentPermit.Text = "Resident Permit"
'
'txtPlaceofIssue
'
Me.txtPlaceofIssue.Flags = 0
Me.txtPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtPlaceofIssue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtPlaceofIssue.Location = New System.Drawing.Point(97, 80)
Me.txtPlaceofIssue.Name = "txtPlaceofIssue"
Me.txtPlaceofIssue.Size = New System.Drawing.Size(294, 21)
Me.txtPlaceofIssue.TabIndex = 5
'
'lblPlaceOfIssue
'
Me.lblPlaceOfIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPlaceOfIssue.Location = New System.Drawing.Point(9, 83)
Me.lblPlaceOfIssue.Name = "lblPlaceOfIssue"
Me.lblPlaceOfIssue.Size = New System.Drawing.Size(82, 15)
Me.lblPlaceOfIssue.TabIndex = 4
Me.lblPlaceOfIssue.Text = "Place Of Issue"
Me.lblPlaceOfIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtWorkPermitNo
'
Me.txtWorkPermitNo.BackColor = System.Drawing.SystemColors.Window
Me.txtWorkPermitNo.Flags = 0
Me.txtWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtWorkPermitNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtWorkPermitNo.Location = New System.Drawing.Point(97, 26)
Me.txtWorkPermitNo.Name = "txtWorkPermitNo"
Me.txtWorkPermitNo.Size = New System.Drawing.Size(112, 21)
Me.txtWorkPermitNo.TabIndex = 1
'
'dtpExpiryDate
'
Me.dtpExpiryDate.Checked = false
Me.dtpExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpExpiryDate.Location = New System.Drawing.Point(287, 53)
Me.dtpExpiryDate.Name = "dtpExpiryDate"
Me.dtpExpiryDate.ShowCheckBox = true
Me.dtpExpiryDate.Size = New System.Drawing.Size(104, 21)
Me.dtpExpiryDate.TabIndex = 9
'
'lblWorkPermitNo
'
Me.lblWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblWorkPermitNo.Location = New System.Drawing.Point(9, 29)
Me.lblWorkPermitNo.Name = "lblWorkPermitNo"
Me.lblWorkPermitNo.Size = New System.Drawing.Size(82, 15)
Me.lblWorkPermitNo.TabIndex = 0
Me.lblWorkPermitNo.Text = "Permit No."
Me.lblWorkPermitNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblExpiryDate
'
Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblExpiryDate.Location = New System.Drawing.Point(215, 56)
Me.lblExpiryDate.Name = "lblExpiryDate"
Me.lblExpiryDate.Size = New System.Drawing.Size(66, 15)
Me.lblExpiryDate.TabIndex = 8
Me.lblExpiryDate.Text = "Expiry Date"
Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboIssueCountry
'
Me.cboIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboIssueCountry.DropDownWidth = 120
Me.cboIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboIssueCountry.FormattingEnabled = true
Me.cboIssueCountry.Location = New System.Drawing.Point(97, 53)
Me.cboIssueCountry.Name = "cboIssueCountry"
Me.cboIssueCountry.Size = New System.Drawing.Size(112, 21)
Me.cboIssueCountry.TabIndex = 3
'
'dtpIssueDate
'
Me.dtpIssueDate.Checked = false
Me.dtpIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpIssueDate.Location = New System.Drawing.Point(287, 26)
Me.dtpIssueDate.Name = "dtpIssueDate"
Me.dtpIssueDate.ShowCheckBox = true
Me.dtpIssueDate.Size = New System.Drawing.Size(104, 21)
Me.dtpIssueDate.TabIndex = 7
'
'lblIssueCountry
'
Me.lblIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIssueCountry.Location = New System.Drawing.Point(9, 56)
Me.lblIssueCountry.Name = "lblIssueCountry"
Me.lblIssueCountry.Size = New System.Drawing.Size(82, 15)
Me.lblIssueCountry.TabIndex = 2
Me.lblIssueCountry.Text = "Issue Country"
Me.lblIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblIssueDate
'
Me.lblIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIssueDate.Location = New System.Drawing.Point(215, 29)
Me.lblIssueDate.Name = "lblIssueDate"
Me.lblIssueDate.Size = New System.Drawing.Size(65, 15)
Me.lblIssueDate.TabIndex = 6
Me.lblIssueDate.Text = "Issue Date"
Me.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'gbDatesDetails
'
Me.gbDatesDetails.BorderColor = System.Drawing.Color.Black
Me.gbDatesDetails.Checked = false
Me.gbDatesDetails.CollapseAllExceptThis = false
Me.gbDatesDetails.CollapsedHoverImage = Nothing
Me.gbDatesDetails.CollapsedNormalImage = Nothing
Me.gbDatesDetails.CollapsedPressedImage = Nothing
Me.gbDatesDetails.CollapseOnLoad = false
Me.gbDatesDetails.Controls.Add(Me.tabcDates_Remark)
Me.gbDatesDetails.Controls.Add(Me.lnkView)
Me.gbDatesDetails.ExpandedHoverImage = Nothing
Me.gbDatesDetails.ExpandedNormalImage = Nothing
Me.gbDatesDetails.ExpandedPressedImage = Nothing
Me.gbDatesDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbDatesDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbDatesDetails.HeaderHeight = 25
Me.gbDatesDetails.HeaderMessage = ""
Me.gbDatesDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbDatesDetails.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbDatesDetails.HeightOnCollapse = 0
Me.gbDatesDetails.LeftTextSpace = 0
Me.gbDatesDetails.Location = New System.Drawing.Point(420, 3)
Me.gbDatesDetails.Name = "gbDatesDetails"
Me.gbDatesDetails.OpenHeight = 198
Me.gbDatesDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbDatesDetails.ShowBorder = true
Me.gbDatesDetails.ShowCheckBox = false
Me.gbDatesDetails.ShowCollapseButton = false
Me.gbDatesDetails.ShowDefaultBorderColor = true
Me.gbDatesDetails.ShowDownButton = false
Me.gbDatesDetails.ShowHeader = true
Me.gbDatesDetails.Size = New System.Drawing.Size(429, 260)
Me.gbDatesDetails.TabIndex = 3
Me.gbDatesDetails.Temp = 0
Me.gbDatesDetails.Text = "Employee Dates"
Me.gbDatesDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabcDates_Remark
'
Me.tabcDates_Remark.Controls.Add(Me.tabpDates)
Me.tabcDates_Remark.Controls.Add(Me.tabpRemark)
Me.tabcDates_Remark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.tabcDates_Remark.Location = New System.Drawing.Point(2, 26)
Me.tabcDates_Remark.Name = "tabcDates_Remark"
Me.tabcDates_Remark.SelectedIndex = 0
Me.tabcDates_Remark.Size = New System.Drawing.Size(421, 233)
Me.tabcDates_Remark.TabIndex = 29
'
'tabpDates
'
Me.tabpDates.Controls.Add(Me.pnlEDates)
Me.tabpDates.Location = New System.Drawing.Point(4, 22)
Me.tabpDates.Margin = New System.Windows.Forms.Padding(0)
Me.tabpDates.Name = "tabpDates"
Me.tabpDates.Size = New System.Drawing.Size(413, 207)
Me.tabpDates.TabIndex = 0
Me.tabpDates.Text = "Employee Dates"
Me.tabpDates.UseVisualStyleBackColor = true
'
'pnlEDates
'
Me.pnlEDates.AutoScroll = true
Me.pnlEDates.Controls.Add(Me.dtpFirstAppointDate)
Me.pnlEDates.Controls.Add(Me.LblFirstAppointmentDate)
Me.pnlEDates.Controls.Add(Me.chkExclude)
Me.pnlEDates.Controls.Add(Me.dtpReinstatementDate)
Me.pnlEDates.Controls.Add(Me.lblReinstatementDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddREndDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddRSuspDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddRLeaveDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddRPDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddRConfDate)
Me.pnlEDates.Controls.Add(Me.dtpEndEmplDate)
Me.pnlEDates.Controls.Add(Me.dtpAppointdate)
Me.pnlEDates.Controls.Add(Me.lblAppointDate)
Me.pnlEDates.Controls.Add(Me.lblEmplDate)
Me.pnlEDates.Controls.Add(Me.dtpConfirmationDate)
Me.pnlEDates.Controls.Add(Me.lblConfirmationDate)
Me.pnlEDates.Controls.Add(Me.objbtnAddRtirementReminder)
Me.pnlEDates.Controls.Add(Me.objbtnAddBirthReminder)
Me.pnlEDates.Controls.Add(Me.dtpRetirementDate)
Me.pnlEDates.Controls.Add(Me.lblRetirementDate)
Me.pnlEDates.Controls.Add(Me.dtpSuspendedFrom)
Me.pnlEDates.Controls.Add(Me.dtpSuspendedTo)
Me.pnlEDates.Controls.Add(Me.objbtnAddReason)
Me.pnlEDates.Controls.Add(Me.lblSuspendedTo)
Me.pnlEDates.Controls.Add(Me.dtpBirthdate)
Me.pnlEDates.Controls.Add(Me.lblSuspendedFrom)
Me.pnlEDates.Controls.Add(Me.cboEndEmplReason)
Me.pnlEDates.Controls.Add(Me.lblEmplReason)
Me.pnlEDates.Controls.Add(Me.lblBirtdate)
Me.pnlEDates.Controls.Add(Me.dtpProbationDateFrom)
Me.pnlEDates.Controls.Add(Me.lblProbationFrom)
Me.pnlEDates.Controls.Add(Me.dtpLeavingDate)
Me.pnlEDates.Controls.Add(Me.dtpProbationDateTo)
Me.pnlEDates.Controls.Add(Me.lblLeavingDate)
Me.pnlEDates.Controls.Add(Me.lblProbationTo)
Me.pnlEDates.Location = New System.Drawing.Point(0, 0)
Me.pnlEDates.Margin = New System.Windows.Forms.Padding(0)
Me.pnlEDates.Name = "pnlEDates"
Me.pnlEDates.Size = New System.Drawing.Size(411, 207)
Me.pnlEDates.TabIndex = 2
'
'dtpFirstAppointDate
'
Me.dtpFirstAppointDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpFirstAppointDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpFirstAppointDate.Location = New System.Drawing.Point(90, 9)
Me.dtpFirstAppointDate.Name = "dtpFirstAppointDate"
Me.dtpFirstAppointDate.Size = New System.Drawing.Size(106, 21)
Me.dtpFirstAppointDate.TabIndex = 38
'
'LblFirstAppointmentDate
'
Me.LblFirstAppointmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.LblFirstAppointmentDate.Location = New System.Drawing.Point(8, 8)
Me.LblFirstAppointmentDate.Name = "LblFirstAppointmentDate"
Me.LblFirstAppointmentDate.Size = New System.Drawing.Size(78, 26)
Me.LblFirstAppointmentDate.TabIndex = 37
Me.LblFirstAppointmentDate.Text = "First Appoint Date"
Me.LblFirstAppointmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'chkExclude
'
Me.chkExclude.CheckAlign = System.Drawing.ContentAlignment.TopLeft
Me.chkExclude.Location = New System.Drawing.Point(225, 147)
Me.chkExclude.Name = "chkExclude"
Me.chkExclude.Size = New System.Drawing.Size(167, 42)
Me.chkExclude.TabIndex = 36
Me.chkExclude.Text = "Exclude From Payroll Process for EOC Date / Leaving Date"
Me.chkExclude.TextAlign = System.Drawing.ContentAlignment.TopLeft
Me.chkExclude.UseVisualStyleBackColor = true
'
'dtpReinstatementDate
'
Me.dtpReinstatementDate.Checked = false
Me.dtpReinstatementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpReinstatementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpReinstatementDate.Location = New System.Drawing.Point(90, 253)
Me.dtpReinstatementDate.Name = "dtpReinstatementDate"
Me.dtpReinstatementDate.ShowCheckBox = true
Me.dtpReinstatementDate.Size = New System.Drawing.Size(106, 21)
Me.dtpReinstatementDate.TabIndex = 35
'
'lblReinstatementDate
'
Me.lblReinstatementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblReinstatementDate.Location = New System.Drawing.Point(8, 255)
Me.lblReinstatementDate.Name = "lblReinstatementDate"
Me.lblReinstatementDate.Size = New System.Drawing.Size(78, 17)
Me.lblReinstatementDate.TabIndex = 34
Me.lblReinstatementDate.Text = "Reinstatement"
Me.lblReinstatementDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddREndDate
'
Me.objbtnAddREndDate.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddREndDate.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddREndDate.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddREndDate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddREndDate.BorderSelected = false
Me.objbtnAddREndDate.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddREndDate.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddREndDate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddREndDate.Location = New System.Drawing.Point(198, 145)
Me.objbtnAddREndDate.Name = "objbtnAddREndDate"
Me.objbtnAddREndDate.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddREndDate.TabIndex = 33
'
'objbtnAddRSuspDate
'
Me.objbtnAddRSuspDate.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddRSuspDate.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddRSuspDate.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddRSuspDate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddRSuspDate.BorderSelected = false
Me.objbtnAddRSuspDate.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddRSuspDate.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddRSuspDate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddRSuspDate.Location = New System.Drawing.Point(368, 118)
Me.objbtnAddRSuspDate.Name = "objbtnAddRSuspDate"
Me.objbtnAddRSuspDate.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddRSuspDate.TabIndex = 32
'
'objbtnAddRLeaveDate
'
Me.objbtnAddRLeaveDate.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddRLeaveDate.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddRLeaveDate.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddRLeaveDate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddRLeaveDate.BorderSelected = false
Me.objbtnAddRLeaveDate.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddRLeaveDate.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddRLeaveDate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddRLeaveDate.Location = New System.Drawing.Point(198, 172)
Me.objbtnAddRLeaveDate.Name = "objbtnAddRLeaveDate"
Me.objbtnAddRLeaveDate.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddRLeaveDate.TabIndex = 31
'
'objbtnAddRPDate
'
Me.objbtnAddRPDate.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddRPDate.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddRPDate.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddRPDate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddRPDate.BorderSelected = false
Me.objbtnAddRPDate.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddRPDate.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddRPDate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddRPDate.Location = New System.Drawing.Point(367, 91)
Me.objbtnAddRPDate.Name = "objbtnAddRPDate"
Me.objbtnAddRPDate.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddRPDate.TabIndex = 29
'
'objbtnAddRConfDate
'
Me.objbtnAddRConfDate.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddRConfDate.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddRConfDate.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddRConfDate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddRConfDate.BorderSelected = false
Me.objbtnAddRConfDate.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddRConfDate.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddRConfDate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddRConfDate.Location = New System.Drawing.Point(368, 36)
Me.objbtnAddRConfDate.Name = "objbtnAddRConfDate"
Me.objbtnAddRConfDate.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddRConfDate.TabIndex = 29
'
'dtpEndEmplDate
'
Me.dtpEndEmplDate.Checked = false
Me.dtpEndEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpEndEmplDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpEndEmplDate.Location = New System.Drawing.Point(90, 145)
Me.dtpEndEmplDate.Name = "dtpEndEmplDate"
Me.dtpEndEmplDate.ShowCheckBox = true
Me.dtpEndEmplDate.Size = New System.Drawing.Size(106, 21)
Me.dtpEndEmplDate.TabIndex = 28
'
'dtpAppointdate
'
Me.dtpAppointdate.Checked = false
Me.dtpAppointdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpAppointdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpAppointdate.Location = New System.Drawing.Point(90, 36)
Me.dtpAppointdate.Name = "dtpAppointdate"
Me.dtpAppointdate.ShowCheckBox = true
Me.dtpAppointdate.Size = New System.Drawing.Size(106, 21)
Me.dtpAppointdate.TabIndex = 7
'
'lblAppointDate
'
Me.lblAppointDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblAppointDate.Location = New System.Drawing.Point(8, 38)
Me.lblAppointDate.Name = "lblAppointDate"
Me.lblAppointDate.Size = New System.Drawing.Size(78, 17)
Me.lblAppointDate.TabIndex = 6
Me.lblAppointDate.Text = "Appoint Date"
Me.lblAppointDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmplDate
'
Me.lblEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmplDate.Location = New System.Drawing.Point(8, 147)
Me.lblEmplDate.Name = "lblEmplDate"
Me.lblEmplDate.Size = New System.Drawing.Size(78, 17)
Me.lblEmplDate.TabIndex = 27
Me.lblEmplDate.Text = "EOC Date"
Me.lblEmplDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpConfirmationDate
'
Me.dtpConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpConfirmationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpConfirmationDate.Location = New System.Drawing.Point(263, 36)
Me.dtpConfirmationDate.Name = "dtpConfirmationDate"
Me.dtpConfirmationDate.Size = New System.Drawing.Size(104, 21)
Me.dtpConfirmationDate.TabIndex = 26
'
'lblConfirmationDate
'
Me.lblConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblConfirmationDate.Location = New System.Drawing.Point(198, 38)
Me.lblConfirmationDate.Name = "lblConfirmationDate"
Me.lblConfirmationDate.Size = New System.Drawing.Size(60, 17)
Me.lblConfirmationDate.TabIndex = 25
Me.lblConfirmationDate.Text = "Conf. Date"
Me.lblConfirmationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddRtirementReminder
'
Me.objbtnAddRtirementReminder.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddRtirementReminder.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddRtirementReminder.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddRtirementReminder.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddRtirementReminder.BorderSelected = false
Me.objbtnAddRtirementReminder.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddRtirementReminder.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddRtirementReminder.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddRtirementReminder.Location = New System.Drawing.Point(198, 226)
Me.objbtnAddRtirementReminder.Name = "objbtnAddRtirementReminder"
Me.objbtnAddRtirementReminder.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddRtirementReminder.TabIndex = 20
'
'objbtnAddBirthReminder
'
Me.objbtnAddBirthReminder.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddBirthReminder.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddBirthReminder.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddBirthReminder.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddBirthReminder.BorderSelected = false
Me.objbtnAddBirthReminder.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddBirthReminder.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
Me.objbtnAddBirthReminder.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddBirthReminder.Location = New System.Drawing.Point(198, 64)
Me.objbtnAddBirthReminder.Name = "objbtnAddBirthReminder"
Me.objbtnAddBirthReminder.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddBirthReminder.TabIndex = 24
'
'dtpRetirementDate
'
Me.dtpRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpRetirementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpRetirementDate.Location = New System.Drawing.Point(90, 226)
Me.dtpRetirementDate.Name = "dtpRetirementDate"
Me.dtpRetirementDate.Size = New System.Drawing.Size(106, 21)
Me.dtpRetirementDate.TabIndex = 15
'
'lblRetirementDate
'
Me.lblRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRetirementDate.Location = New System.Drawing.Point(8, 228)
Me.lblRetirementDate.Name = "lblRetirementDate"
Me.lblRetirementDate.Size = New System.Drawing.Size(78, 17)
Me.lblRetirementDate.TabIndex = 14
Me.lblRetirementDate.Text = "Retr. Date"
Me.lblRetirementDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpSuspendedFrom
'
Me.dtpSuspendedFrom.Checked = false
Me.dtpSuspendedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpSuspendedFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpSuspendedFrom.Location = New System.Drawing.Point(90, 118)
Me.dtpSuspendedFrom.Name = "dtpSuspendedFrom"
Me.dtpSuspendedFrom.ShowCheckBox = true
Me.dtpSuspendedFrom.Size = New System.Drawing.Size(106, 21)
Me.dtpSuspendedFrom.TabIndex = 1
'
'dtpSuspendedTo
'
Me.dtpSuspendedTo.Checked = false
Me.dtpSuspendedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpSuspendedTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpSuspendedTo.Location = New System.Drawing.Point(263, 118)
Me.dtpSuspendedTo.Name = "dtpSuspendedTo"
Me.dtpSuspendedTo.ShowCheckBox = true
Me.dtpSuspendedTo.Size = New System.Drawing.Size(104, 21)
Me.dtpSuspendedTo.TabIndex = 11
'
'objbtnAddReason
'
Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddReason.BorderSelected = false
Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddReason.Location = New System.Drawing.Point(355, 199)
Me.objbtnAddReason.Name = "objbtnAddReason"
Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddReason.TabIndex = 18
'
'lblSuspendedTo
'
Me.lblSuspendedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSuspendedTo.Location = New System.Drawing.Point(198, 120)
Me.lblSuspendedTo.Name = "lblSuspendedTo"
Me.lblSuspendedTo.Size = New System.Drawing.Size(63, 17)
Me.lblSuspendedTo.TabIndex = 10
Me.lblSuspendedTo.Text = "Susp. To"
Me.lblSuspendedTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpBirthdate
'
Me.dtpBirthdate.Checked = false
Me.dtpBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpBirthdate.Location = New System.Drawing.Point(90, 64)
Me.dtpBirthdate.Name = "dtpBirthdate"
Me.dtpBirthdate.ShowCheckBox = true
Me.dtpBirthdate.Size = New System.Drawing.Size(106, 21)
Me.dtpBirthdate.TabIndex = 23
'
'lblSuspendedFrom
'
Me.lblSuspendedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSuspendedFrom.Location = New System.Drawing.Point(8, 120)
Me.lblSuspendedFrom.Name = "lblSuspendedFrom"
Me.lblSuspendedFrom.Size = New System.Drawing.Size(78, 17)
Me.lblSuspendedFrom.TabIndex = 0
Me.lblSuspendedFrom.Text = "Susp. From"
Me.lblSuspendedFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEndEmplReason
'
Me.cboEndEmplReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEndEmplReason.DropDownWidth = 120
Me.cboEndEmplReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEndEmplReason.FormattingEnabled = true
Me.cboEndEmplReason.Location = New System.Drawing.Point(90, 199)
Me.cboEndEmplReason.Name = "cboEndEmplReason"
Me.cboEndEmplReason.Size = New System.Drawing.Size(252, 21)
Me.cboEndEmplReason.TabIndex = 17
'
'lblEmplReason
'
Me.lblEmplReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmplReason.Location = New System.Drawing.Point(8, 201)
Me.lblEmplReason.Name = "lblEmplReason"
Me.lblEmplReason.Size = New System.Drawing.Size(83, 17)
Me.lblEmplReason.TabIndex = 16
Me.lblEmplReason.Text = "EOC Reason"
Me.lblEmplReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblBirtdate
'
Me.lblBirtdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblBirtdate.Location = New System.Drawing.Point(8, 66)
Me.lblBirtdate.Name = "lblBirtdate"
Me.lblBirtdate.Size = New System.Drawing.Size(78, 17)
Me.lblBirtdate.TabIndex = 22
Me.lblBirtdate.Text = "Birth Date"
Me.lblBirtdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpProbationDateFrom
'
Me.dtpProbationDateFrom.Checked = false
Me.dtpProbationDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpProbationDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpProbationDateFrom.Location = New System.Drawing.Point(90, 91)
Me.dtpProbationDateFrom.Name = "dtpProbationDateFrom"
Me.dtpProbationDateFrom.ShowCheckBox = true
Me.dtpProbationDateFrom.Size = New System.Drawing.Size(106, 21)
Me.dtpProbationDateFrom.TabIndex = 3
'
'lblProbationFrom
'
Me.lblProbationFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblProbationFrom.Location = New System.Drawing.Point(8, 93)
Me.lblProbationFrom.Name = "lblProbationFrom"
Me.lblProbationFrom.Size = New System.Drawing.Size(78, 17)
Me.lblProbationFrom.TabIndex = 2
Me.lblProbationFrom.Text = "Prob. From"
Me.lblProbationFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpLeavingDate
'
Me.dtpLeavingDate.Checked = false
Me.dtpLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpLeavingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpLeavingDate.Location = New System.Drawing.Point(90, 172)
Me.dtpLeavingDate.Name = "dtpLeavingDate"
Me.dtpLeavingDate.ShowCheckBox = true
Me.dtpLeavingDate.Size = New System.Drawing.Size(106, 21)
Me.dtpLeavingDate.TabIndex = 5
'
'dtpProbationDateTo
'
Me.dtpProbationDateTo.Checked = false
Me.dtpProbationDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpProbationDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpProbationDateTo.Location = New System.Drawing.Point(262, 91)
Me.dtpProbationDateTo.Name = "dtpProbationDateTo"
Me.dtpProbationDateTo.ShowCheckBox = true
Me.dtpProbationDateTo.Size = New System.Drawing.Size(104, 21)
Me.dtpProbationDateTo.TabIndex = 13
'
'lblLeavingDate
'
Me.lblLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLeavingDate.Location = New System.Drawing.Point(8, 174)
Me.lblLeavingDate.Name = "lblLeavingDate"
Me.lblLeavingDate.Size = New System.Drawing.Size(78, 17)
Me.lblLeavingDate.TabIndex = 4
Me.lblLeavingDate.Text = "Leaving Date"
Me.lblLeavingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblProbationTo
'
Me.lblProbationTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblProbationTo.Location = New System.Drawing.Point(198, 93)
Me.lblProbationTo.Name = "lblProbationTo"
Me.lblProbationTo.Size = New System.Drawing.Size(63, 17)
Me.lblProbationTo.TabIndex = 12
Me.lblProbationTo.Text = "Prob. To"
Me.lblProbationTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabpRemark
'
Me.tabpRemark.Controls.Add(Me.txtTerminateReason)
Me.tabpRemark.Location = New System.Drawing.Point(4, 22)
Me.tabpRemark.Margin = New System.Windows.Forms.Padding(0)
Me.tabpRemark.Name = "tabpRemark"
Me.tabpRemark.Size = New System.Drawing.Size(413, 207)
Me.tabpRemark.TabIndex = 1
Me.tabpRemark.Text = "Remark"
Me.tabpRemark.UseVisualStyleBackColor = true
'
'txtTerminateReason
'
Me.txtTerminateReason.BackColor = System.Drawing.SystemColors.Window
Me.txtTerminateReason.Dock = System.Windows.Forms.DockStyle.Fill
Me.txtTerminateReason.Flags = 0
Me.txtTerminateReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtTerminateReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtTerminateReason.Location = New System.Drawing.Point(0, 0)
Me.txtTerminateReason.Multiline = true
Me.txtTerminateReason.Name = "txtTerminateReason"
Me.txtTerminateReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
Me.txtTerminateReason.Size = New System.Drawing.Size(413, 207)
Me.txtTerminateReason.TabIndex = 9
'
'lnkView
'
Me.lnkView.BackColor = System.Drawing.Color.Transparent
Me.lnkView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkView.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkView.Location = New System.Drawing.Point(259, 4)
Me.lnkView.Name = "lnkView"
Me.lnkView.Size = New System.Drawing.Size(164, 17)
Me.lnkView.TabIndex = 15
Me.lnkView.TabStop = true
Me.lnkView.Text = "[ View Login Details ]"
Me.lnkView.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'gbLoginAndOtherDetails
'
Me.gbLoginAndOtherDetails.BorderColor = System.Drawing.Color.Black
Me.gbLoginAndOtherDetails.Checked = false
Me.gbLoginAndOtherDetails.CollapseAllExceptThis = false
Me.gbLoginAndOtherDetails.CollapsedHoverImage = Nothing
Me.gbLoginAndOtherDetails.CollapsedNormalImage = Nothing
Me.gbLoginAndOtherDetails.CollapsedPressedImage = Nothing
Me.gbLoginAndOtherDetails.CollapseOnLoad = false
Me.gbLoginAndOtherDetails.Controls.Add(Me.lnkClose)
Me.gbLoginAndOtherDetails.Controls.Add(Me.objStLine2)
Me.gbLoginAndOtherDetails.Controls.Add(Me.txtEmployeeEmail)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lnkHolidayInfo)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lblDisplayName)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lblLoginName)
Me.gbLoginAndOtherDetails.Controls.Add(Me.txtLoginName)
Me.gbLoginAndOtherDetails.Controls.Add(Me.txtPassword)
Me.gbLoginAndOtherDetails.Controls.Add(Me.txtDisplayName)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lblPassword)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lblEmployeeEmail)
Me.gbLoginAndOtherDetails.Controls.Add(Me.lnkShiftInformation)
Me.gbLoginAndOtherDetails.ExpandedHoverImage = Nothing
Me.gbLoginAndOtherDetails.ExpandedNormalImage = Nothing
Me.gbLoginAndOtherDetails.ExpandedPressedImage = Nothing
Me.gbLoginAndOtherDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbLoginAndOtherDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbLoginAndOtherDetails.HeaderHeight = 25
Me.gbLoginAndOtherDetails.HeaderMessage = ""
Me.gbLoginAndOtherDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbLoginAndOtherDetails.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbLoginAndOtherDetails.HeightOnCollapse = 0
Me.gbLoginAndOtherDetails.LeftTextSpace = 0
Me.gbLoginAndOtherDetails.Location = New System.Drawing.Point(420, 3)
Me.gbLoginAndOtherDetails.Name = "gbLoginAndOtherDetails"
Me.gbLoginAndOtherDetails.OpenHeight = 198
Me.gbLoginAndOtherDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbLoginAndOtherDetails.ShowBorder = true
Me.gbLoginAndOtherDetails.ShowCheckBox = false
Me.gbLoginAndOtherDetails.ShowCollapseButton = false
Me.gbLoginAndOtherDetails.ShowDefaultBorderColor = true
Me.gbLoginAndOtherDetails.ShowDownButton = false
Me.gbLoginAndOtherDetails.ShowHeader = true
Me.gbLoginAndOtherDetails.Size = New System.Drawing.Size(428, 256)
Me.gbLoginAndOtherDetails.TabIndex = 2
Me.gbLoginAndOtherDetails.Temp = 0
Me.gbLoginAndOtherDetails.Text = "Login And Other Details"
Me.gbLoginAndOtherDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkClose
'
Me.lnkClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkClose.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkClose.Location = New System.Drawing.Point(348, 4)
Me.lnkClose.Name = "lnkClose"
Me.lnkClose.Size = New System.Drawing.Size(74, 17)
Me.lnkClose.TabIndex = 17
Me.lnkClose.TabStop = true
Me.lnkClose.Text = "[ X ]"
Me.lnkClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'objStLine2
'
Me.objStLine2.BackColor = System.Drawing.Color.Transparent
Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Horizontal
Me.objStLine2.Location = New System.Drawing.Point(11, 143)
Me.objStLine2.Name = "objStLine2"
Me.objStLine2.Size = New System.Drawing.Size(396, 17)
Me.objStLine2.TabIndex = 13
Me.objStLine2.Text = "EZeeStraightLine1"
'
'txtEmployeeEmail
'
Me.txtEmployeeEmail.Flags = 0
Me.txtEmployeeEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmployeeEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmployeeEmail.Location = New System.Drawing.Point(113, 116)
Me.txtEmployeeEmail.Name = "txtEmployeeEmail"
Me.txtEmployeeEmail.Size = New System.Drawing.Size(294, 21)
Me.txtEmployeeEmail.TabIndex = 7
'
'lnkHolidayInfo
'
Me.lnkHolidayInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkHolidayInfo.Location = New System.Drawing.Point(8, 189)
Me.lnkHolidayInfo.Name = "lnkHolidayInfo"
Me.lnkHolidayInfo.Size = New System.Drawing.Size(357, 16)
Me.lnkHolidayInfo.TabIndex = 11
Me.lnkHolidayInfo.TabStop = true
Me.lnkHolidayInfo.Text = "Exempt Holiday Information"
Me.lnkHolidayInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDisplayName
'
Me.lblDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDisplayName.Location = New System.Drawing.Point(8, 91)
Me.lblDisplayName.Name = "lblDisplayName"
Me.lblDisplayName.Size = New System.Drawing.Size(99, 15)
Me.lblDisplayName.TabIndex = 4
Me.lblDisplayName.Text = "Display Name"
Me.lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblLoginName
'
Me.lblLoginName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLoginName.Location = New System.Drawing.Point(8, 36)
Me.lblLoginName.Name = "lblLoginName"
Me.lblLoginName.Size = New System.Drawing.Size(99, 15)
Me.lblLoginName.TabIndex = 0
Me.lblLoginName.Text = "Login Name"
Me.lblLoginName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtLoginName
'
Me.txtLoginName.Flags = 0
Me.txtLoginName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtLoginName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtLoginName.Location = New System.Drawing.Point(113, 33)
Me.txtLoginName.Name = "txtLoginName"
Me.txtLoginName.Size = New System.Drawing.Size(294, 21)
Me.txtLoginName.TabIndex = 1
'
'txtPassword
'
Me.txtPassword.Flags = 0
Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtPassword.Location = New System.Drawing.Point(113, 61)
Me.txtPassword.Name = "txtPassword"
Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
Me.txtPassword.Size = New System.Drawing.Size(294, 21)
Me.txtPassword.TabIndex = 3
'
'txtDisplayName
'
Me.txtDisplayName.Flags = 0
Me.txtDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDisplayName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDisplayName.Location = New System.Drawing.Point(113, 88)
Me.txtDisplayName.Name = "txtDisplayName"
Me.txtDisplayName.Size = New System.Drawing.Size(294, 21)
Me.txtDisplayName.TabIndex = 5
'
'lblPassword
'
Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPassword.Location = New System.Drawing.Point(8, 64)
Me.lblPassword.Name = "lblPassword"
Me.lblPassword.Size = New System.Drawing.Size(99, 15)
Me.lblPassword.TabIndex = 2
Me.lblPassword.Text = "Password"
Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmployeeEmail
'
Me.lblEmployeeEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmployeeEmail.Location = New System.Drawing.Point(8, 119)
Me.lblEmployeeEmail.Name = "lblEmployeeEmail"
Me.lblEmployeeEmail.Size = New System.Drawing.Size(99, 15)
Me.lblEmployeeEmail.TabIndex = 6
Me.lblEmployeeEmail.Text = "Email"
Me.lblEmployeeEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkShiftInformation
'
Me.lnkShiftInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkShiftInformation.Location = New System.Drawing.Point(8, 163)
Me.lnkShiftInformation.Name = "lnkShiftInformation"
Me.lnkShiftInformation.Size = New System.Drawing.Size(357, 16)
Me.lnkShiftInformation.TabIndex = 9
Me.lnkShiftInformation.TabStop = true
Me.lnkShiftInformation.Text = "Shift Information"
Me.lnkShiftInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabpPersonalInfo
'
Me.tabpPersonalInfo.Controls.Add(Me.pnlPersonalInfo)
Me.tabpPersonalInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpPersonalInfo.Name = "tabpPersonalInfo"
Me.tabpPersonalInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpPersonalInfo.Size = New System.Drawing.Size(850, 266)
Me.tabpPersonalInfo.TabIndex = 1
Me.tabpPersonalInfo.Text = "Address"
Me.tabpPersonalInfo.UseVisualStyleBackColor = true
'
'pnlPersonalInfo
'
Me.pnlPersonalInfo.Controls.Add(Me.gbAddress)
Me.pnlPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlPersonalInfo.Location = New System.Drawing.Point(3, 3)
Me.pnlPersonalInfo.Name = "pnlPersonalInfo"
Me.pnlPersonalInfo.Size = New System.Drawing.Size(844, 260)
Me.pnlPersonalInfo.TabIndex = 1
'
'gbAddress
'
Me.gbAddress.BorderColor = System.Drawing.Color.Black
Me.gbAddress.Checked = false
Me.gbAddress.CollapseAllExceptThis = false
Me.gbAddress.CollapsedHoverImage = Nothing
Me.gbAddress.CollapsedNormalImage = Nothing
Me.gbAddress.CollapsedPressedImage = Nothing
Me.gbAddress.CollapseOnLoad = false
Me.gbAddress.Controls.Add(Me.pnlPersonalInformation)
Me.gbAddress.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbAddress.ExpandedHoverImage = Nothing
Me.gbAddress.ExpandedNormalImage = Nothing
Me.gbAddress.ExpandedPressedImage = Nothing
Me.gbAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbAddress.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbAddress.HeaderHeight = 25
Me.gbAddress.HeaderMessage = ""
Me.gbAddress.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbAddress.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbAddress.HeightOnCollapse = 0
Me.gbAddress.LeftTextSpace = 0
Me.gbAddress.Location = New System.Drawing.Point(0, 0)
Me.gbAddress.Name = "gbAddress"
Me.gbAddress.OpenHeight = 351
Me.gbAddress.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbAddress.ShowBorder = true
Me.gbAddress.ShowCheckBox = false
Me.gbAddress.ShowCollapseButton = false
Me.gbAddress.ShowDefaultBorderColor = true
Me.gbAddress.ShowDownButton = false
Me.gbAddress.ShowHeader = true
Me.gbAddress.Size = New System.Drawing.Size(844, 260)
Me.gbAddress.TabIndex = 0
Me.gbAddress.Temp = 0
Me.gbAddress.Text = "Address Info"
Me.gbAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'pnlPersonalInformation
'
Me.pnlPersonalInformation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.pnlPersonalInformation.AutoScroll = true
Me.pnlPersonalInformation.BackColor = System.Drawing.Color.Transparent
Me.pnlPersonalInformation.Controls.Add(Me.gbRecruitmentAddress)
Me.pnlPersonalInformation.Controls.Add(Me.GbDomicileAddress)
Me.pnlPersonalInformation.Controls.Add(Me.GbPresentAddress)
Me.pnlPersonalInformation.Location = New System.Drawing.Point(2, 26)
Me.pnlPersonalInformation.Name = "pnlPersonalInformation"
Me.pnlPersonalInformation.Size = New System.Drawing.Size(839, 234)
Me.pnlPersonalInformation.TabIndex = 219
'
'gbRecruitmentAddress
'
Me.gbRecruitmentAddress.Controls.Add(Me.lnkAttachRecruitment)
Me.gbRecruitmentAddress.Controls.Add(Me.lblrecruitmentaddressapproval)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRecruitmentTown1)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentTown1)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRecruitmentVillage)
Me.gbRecruitmentAddress.Controls.Add(Me.lnkRecruitmentCopyAddress)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentVillage)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRecruitmentChiefdom)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentChiefdom)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRecruitmentRoadStreet1)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentRoadStreet1)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentProvince1)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRecruitmentProvince1)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentPlotNo)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRPlotNo)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentTelNo)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRTelNo)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentProvince)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRRegion)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentEstate)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentRoad)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRStreet)
Me.gbRecruitmentAddress.Controls.Add(Me.lblREState)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentState)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRState)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentPostCode)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentTown)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRCode)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRCity)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRAddress2)
Me.gbRecruitmentAddress.Controls.Add(Me.cboRecruitmentCountry)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentAddress2)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRCountry)
Me.gbRecruitmentAddress.Controls.Add(Me.txtRecruitmentAddress1)
Me.gbRecruitmentAddress.Controls.Add(Me.lblRAddress1)
Me.gbRecruitmentAddress.Location = New System.Drawing.Point(14, 489)
Me.gbRecruitmentAddress.Name = "gbRecruitmentAddress"
Me.gbRecruitmentAddress.Size = New System.Drawing.Size(796, 203)
Me.gbRecruitmentAddress.TabIndex = 120
Me.gbRecruitmentAddress.TabStop = false
Me.gbRecruitmentAddress.Text = "Recruitment Address"
'
'lnkAttachRecruitment
'
Me.lnkAttachRecruitment.BackColor = System.Drawing.Color.White
Me.lnkAttachRecruitment.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachRecruitment.Location = New System.Drawing.Point(609, 16)
Me.lnkAttachRecruitment.Name = "lnkAttachRecruitment"
Me.lnkAttachRecruitment.Size = New System.Drawing.Size(168, 17)
Me.lnkAttachRecruitment.TabIndex = 187
Me.lnkAttachRecruitment.TabStop = true
Me.lnkAttachRecruitment.Text = "Scan/Attach Document"
Me.lnkAttachRecruitment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblrecruitmentaddressapproval
'
Me.lblrecruitmentaddressapproval.BackColor = System.Drawing.Color.PowderBlue
Me.lblrecruitmentaddressapproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblrecruitmentaddressapproval.ForeColor = System.Drawing.Color.Black
Me.lblrecruitmentaddressapproval.Location = New System.Drawing.Point(355, 1)
Me.lblrecruitmentaddressapproval.Name = "lblrecruitmentaddressapproval"
Me.lblrecruitmentaddressapproval.Size = New System.Drawing.Size(169, 16)
Me.lblrecruitmentaddressapproval.TabIndex = 186
Me.lblrecruitmentaddressapproval.Text = "Pending Approval"
Me.lblrecruitmentaddressapproval.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblrecruitmentaddressapproval.Visible = false
'
'lblRecruitmentTown1
'
Me.lblRecruitmentTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRecruitmentTown1.Location = New System.Drawing.Point(273, 147)
Me.lblRecruitmentTown1.Name = "lblRecruitmentTown1"
Me.lblRecruitmentTown1.Size = New System.Drawing.Size(76, 15)
Me.lblRecruitmentTown1.TabIndex = 150
Me.lblRecruitmentTown1.Text = "Town1"
Me.lblRecruitmentTown1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentTown1
'
Me.cboRecruitmentTown1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentTown1.FormattingEnabled = true
Me.cboRecruitmentTown1.Location = New System.Drawing.Point(354, 144)
Me.cboRecruitmentTown1.Name = "cboRecruitmentTown1"
Me.cboRecruitmentTown1.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentTown1.TabIndex = 149
'
'lblRecruitmentVillage
'
Me.lblRecruitmentVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRecruitmentVillage.Location = New System.Drawing.Point(16, 147)
Me.lblRecruitmentVillage.Name = "lblRecruitmentVillage"
Me.lblRecruitmentVillage.Size = New System.Drawing.Size(76, 15)
Me.lblRecruitmentVillage.TabIndex = 148
Me.lblRecruitmentVillage.Text = "Village"
Me.lblRecruitmentVillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkRecruitmentCopyAddress
'
Me.lnkRecruitmentCopyAddress.BackColor = System.Drawing.Color.White
Me.lnkRecruitmentCopyAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkRecruitmentCopyAddress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkRecruitmentCopyAddress.Location = New System.Drawing.Point(707, 1)
Me.lnkRecruitmentCopyAddress.Name = "lnkRecruitmentCopyAddress"
Me.lnkRecruitmentCopyAddress.Size = New System.Drawing.Size(74, 13)
Me.lnkRecruitmentCopyAddress.TabIndex = 64
Me.lnkRecruitmentCopyAddress.TabStop = true
Me.lnkRecruitmentCopyAddress.Text = "Copy Address"
'
'cboRecruitmentVillage
'
Me.cboRecruitmentVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentVillage.FormattingEnabled = true
Me.cboRecruitmentVillage.Location = New System.Drawing.Point(97, 144)
Me.cboRecruitmentVillage.Name = "cboRecruitmentVillage"
Me.cboRecruitmentVillage.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentVillage.TabIndex = 147
'
'lblRecruitmentChiefdom
'
Me.lblRecruitmentChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRecruitmentChiefdom.Location = New System.Drawing.Point(529, 120)
Me.lblRecruitmentChiefdom.Name = "lblRecruitmentChiefdom"
Me.lblRecruitmentChiefdom.Size = New System.Drawing.Size(76, 15)
Me.lblRecruitmentChiefdom.TabIndex = 146
Me.lblRecruitmentChiefdom.Text = "Chiefdom"
Me.lblRecruitmentChiefdom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentChiefdom
'
Me.cboRecruitmentChiefdom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentChiefdom.FormattingEnabled = true
Me.cboRecruitmentChiefdom.Location = New System.Drawing.Point(611, 117)
Me.cboRecruitmentChiefdom.Name = "cboRecruitmentChiefdom"
Me.cboRecruitmentChiefdom.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentChiefdom.TabIndex = 145
'
'lblRecruitmentRoadStreet1
'
Me.lblRecruitmentRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRecruitmentRoadStreet1.Location = New System.Drawing.Point(272, 120)
Me.lblRecruitmentRoadStreet1.Name = "lblRecruitmentRoadStreet1"
Me.lblRecruitmentRoadStreet1.Size = New System.Drawing.Size(76, 15)
Me.lblRecruitmentRoadStreet1.TabIndex = 144
Me.lblRecruitmentRoadStreet1.Text = "Road/Street1"
Me.lblRecruitmentRoadStreet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentRoadStreet1
'
Me.cboRecruitmentRoadStreet1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentRoadStreet1.FormattingEnabled = true
Me.cboRecruitmentRoadStreet1.Location = New System.Drawing.Point(354, 117)
Me.cboRecruitmentRoadStreet1.Name = "cboRecruitmentRoadStreet1"
Me.cboRecruitmentRoadStreet1.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentRoadStreet1.TabIndex = 143
'
'cboRecruitmentProvince1
'
Me.cboRecruitmentProvince1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentProvince1.FormattingEnabled = true
Me.cboRecruitmentProvince1.Location = New System.Drawing.Point(97, 117)
Me.cboRecruitmentProvince1.Name = "cboRecruitmentProvince1"
Me.cboRecruitmentProvince1.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentProvince1.TabIndex = 142
'
'lblRecruitmentProvince1
'
Me.lblRecruitmentProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRecruitmentProvince1.Location = New System.Drawing.Point(15, 120)
Me.lblRecruitmentProvince1.Name = "lblRecruitmentProvince1"
Me.lblRecruitmentProvince1.Size = New System.Drawing.Size(76, 15)
Me.lblRecruitmentProvince1.TabIndex = 141
Me.lblRecruitmentProvince1.Text = "Prov/Region1"
Me.lblRecruitmentProvince1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRecruitmentPlotNo
'
Me.txtRecruitmentPlotNo.Flags = 0
Me.txtRecruitmentPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentPlotNo.Location = New System.Drawing.Point(97, 171)
Me.txtRecruitmentPlotNo.Name = "txtRecruitmentPlotNo"
Me.txtRecruitmentPlotNo.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentPlotNo.TabIndex = 138
'
'lblRPlotNo
'
Me.lblRPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRPlotNo.Location = New System.Drawing.Point(15, 174)
Me.lblRPlotNo.Name = "lblRPlotNo"
Me.lblRPlotNo.Size = New System.Drawing.Size(76, 15)
Me.lblRPlotNo.TabIndex = 137
Me.lblRPlotNo.Text = "Plot No"
Me.lblRPlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRecruitmentTelNo
'
Me.txtRecruitmentTelNo.Flags = 0
Me.txtRecruitmentTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentTelNo.Location = New System.Drawing.Point(354, 171)
Me.txtRecruitmentTelNo.Name = "txtRecruitmentTelNo"
Me.txtRecruitmentTelNo.Size = New System.Drawing.Size(170, 21)
Me.txtRecruitmentTelNo.TabIndex = 140
'
'lblRTelNo
'
Me.lblRTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRTelNo.Location = New System.Drawing.Point(272, 174)
Me.lblRTelNo.Name = "lblRTelNo"
Me.lblRTelNo.Size = New System.Drawing.Size(76, 15)
Me.lblRTelNo.TabIndex = 139
Me.lblRTelNo.Text = "Tel. No"
Me.lblRTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRecruitmentProvince
'
Me.txtRecruitmentProvince.Flags = 0
Me.txtRecruitmentProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentProvince.Location = New System.Drawing.Point(97, 90)
Me.txtRecruitmentProvince.Name = "txtRecruitmentProvince"
Me.txtRecruitmentProvince.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentProvince.TabIndex = 132
'
'lblRRegion
'
Me.lblRRegion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRRegion.Location = New System.Drawing.Point(15, 93)
Me.lblRRegion.Name = "lblRRegion"
Me.lblRRegion.Size = New System.Drawing.Size(76, 15)
Me.lblRRegion.TabIndex = 131
Me.lblRRegion.Text = "Prov/Region"
Me.lblRRegion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRecruitmentEstate
'
Me.txtRecruitmentEstate.Flags = 0
Me.txtRecruitmentEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentEstate.Location = New System.Drawing.Point(611, 90)
Me.txtRecruitmentEstate.Name = "txtRecruitmentEstate"
Me.txtRecruitmentEstate.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentEstate.TabIndex = 136
'
'txtRecruitmentRoad
'
Me.txtRecruitmentRoad.Flags = 0
Me.txtRecruitmentRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentRoad.Location = New System.Drawing.Point(354, 90)
Me.txtRecruitmentRoad.Name = "txtRecruitmentRoad"
Me.txtRecruitmentRoad.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentRoad.TabIndex = 134
'
'lblRStreet
'
Me.lblRStreet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRStreet.Location = New System.Drawing.Point(272, 93)
Me.lblRStreet.Name = "lblRStreet"
Me.lblRStreet.Size = New System.Drawing.Size(76, 15)
Me.lblRStreet.TabIndex = 133
Me.lblRStreet.Text = "Road/Street"
Me.lblRStreet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblREState
'
Me.lblREState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblREState.Location = New System.Drawing.Point(529, 93)
Me.lblREState.Name = "lblREState"
Me.lblREState.Size = New System.Drawing.Size(76, 15)
Me.lblREState.TabIndex = 135
Me.lblREState.Text = "Estate"
Me.lblREState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentState
'
Me.cboRecruitmentState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentState.FormattingEnabled = true
Me.cboRecruitmentState.Location = New System.Drawing.Point(97, 63)
Me.cboRecruitmentState.Name = "cboRecruitmentState"
Me.cboRecruitmentState.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentState.TabIndex = 126
'
'lblRState
'
Me.lblRState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRState.Location = New System.Drawing.Point(15, 66)
Me.lblRState.Name = "lblRState"
Me.lblRState.Size = New System.Drawing.Size(76, 15)
Me.lblRState.TabIndex = 125
Me.lblRState.Text = "State"
Me.lblRState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentPostCode
'
Me.cboRecruitmentPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentPostCode.FormattingEnabled = true
Me.cboRecruitmentPostCode.Location = New System.Drawing.Point(611, 63)
Me.cboRecruitmentPostCode.Name = "cboRecruitmentPostCode"
Me.cboRecruitmentPostCode.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentPostCode.TabIndex = 130
'
'cboRecruitmentTown
'
Me.cboRecruitmentTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentTown.FormattingEnabled = true
Me.cboRecruitmentTown.Location = New System.Drawing.Point(354, 63)
Me.cboRecruitmentTown.Name = "cboRecruitmentTown"
Me.cboRecruitmentTown.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentTown.TabIndex = 128
'
'lblRCode
'
Me.lblRCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRCode.Location = New System.Drawing.Point(529, 66)
Me.lblRCode.Name = "lblRCode"
Me.lblRCode.Size = New System.Drawing.Size(76, 15)
Me.lblRCode.TabIndex = 129
Me.lblRCode.Text = "Post Code"
Me.lblRCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblRCity
'
Me.lblRCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRCity.Location = New System.Drawing.Point(272, 66)
Me.lblRCity.Name = "lblRCity"
Me.lblRCity.Size = New System.Drawing.Size(76, 15)
Me.lblRCity.TabIndex = 127
Me.lblRCity.Text = "Post Town"
Me.lblRCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblRAddress2
'
Me.lblRAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRAddress2.Location = New System.Drawing.Point(273, 39)
Me.lblRAddress2.Name = "lblRAddress2"
Me.lblRAddress2.Size = New System.Drawing.Size(76, 15)
Me.lblRAddress2.TabIndex = 121
Me.lblRAddress2.Text = "Address2"
Me.lblRAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboRecruitmentCountry
'
Me.cboRecruitmentCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboRecruitmentCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboRecruitmentCountry.FormattingEnabled = true
Me.cboRecruitmentCountry.Location = New System.Drawing.Point(612, 36)
Me.cboRecruitmentCountry.Name = "cboRecruitmentCountry"
Me.cboRecruitmentCountry.Size = New System.Drawing.Size(169, 21)
Me.cboRecruitmentCountry.TabIndex = 124
'
'txtRecruitmentAddress2
'
Me.txtRecruitmentAddress2.Flags = 0
Me.txtRecruitmentAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentAddress2.Location = New System.Drawing.Point(354, 36)
Me.txtRecruitmentAddress2.Name = "txtRecruitmentAddress2"
Me.txtRecruitmentAddress2.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentAddress2.TabIndex = 122
'
'lblRCountry
'
Me.lblRCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRCountry.Location = New System.Drawing.Point(530, 39)
Me.lblRCountry.Name = "lblRCountry"
Me.lblRCountry.Size = New System.Drawing.Size(76, 15)
Me.lblRCountry.TabIndex = 123
Me.lblRCountry.Text = "Post Country"
Me.lblRCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRecruitmentAddress1
'
Me.txtRecruitmentAddress1.Flags = 0
Me.txtRecruitmentAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRecruitmentAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRecruitmentAddress1.Location = New System.Drawing.Point(98, 36)
Me.txtRecruitmentAddress1.Name = "txtRecruitmentAddress1"
Me.txtRecruitmentAddress1.Size = New System.Drawing.Size(169, 21)
Me.txtRecruitmentAddress1.TabIndex = 120
'
'lblRAddress1
'
Me.lblRAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRAddress1.Location = New System.Drawing.Point(16, 39)
Me.lblRAddress1.Name = "lblRAddress1"
Me.lblRAddress1.Size = New System.Drawing.Size(76, 15)
Me.lblRAddress1.TabIndex = 119
Me.lblRAddress1.Text = "Address1"
Me.lblRAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'GbDomicileAddress
'
Me.GbDomicileAddress.Controls.Add(Me.lnkAttachDomicile)
Me.GbDomicileAddress.Controls.Add(Me.lbldomicileaddressapproval)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileTown1)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileTown1)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicilevillage)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicilevillage)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileChiefdom)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileChiefdom)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileRoadStreet1)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileRoadStreet1)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileProvince1)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileProvince1)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileState)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileState)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileAddress2)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicilePostCode)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileTown)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileAltNo)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileAltNo)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicilePlotNo)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicilePlotNo)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileProvince)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileProvince)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicilePostCode)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileFax)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileFax)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileMobile)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileMobileNo)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileTelNo)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileTelNo)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileEstate)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileRoad)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileEmail)
Me.GbDomicileAddress.Controls.Add(Me.cboDomicileCountry)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileAddress2)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileRoad)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileEState)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileEmail)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicilePostTown)
Me.GbDomicileAddress.Controls.Add(Me.lnkCopyAddress)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicilePostCountry)
Me.GbDomicileAddress.Controls.Add(Me.txtDomicileAddress1)
Me.GbDomicileAddress.Controls.Add(Me.lblDomicileAddress1)
Me.GbDomicileAddress.Location = New System.Drawing.Point(14, 245)
Me.GbDomicileAddress.Name = "GbDomicileAddress"
Me.GbDomicileAddress.Size = New System.Drawing.Size(796, 233)
Me.GbDomicileAddress.TabIndex = 120
Me.GbDomicileAddress.TabStop = false
Me.GbDomicileAddress.Text = "Domicile Address"
'
'lnkAttachDomicile
'
Me.lnkAttachDomicile.BackColor = System.Drawing.Color.White
Me.lnkAttachDomicile.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachDomicile.Location = New System.Drawing.Point(609, 16)
Me.lnkAttachDomicile.Name = "lnkAttachDomicile"
Me.lnkAttachDomicile.Size = New System.Drawing.Size(168, 17)
Me.lnkAttachDomicile.TabIndex = 186
Me.lnkAttachDomicile.TabStop = true
Me.lnkAttachDomicile.Text = "Scan/Attach Document"
Me.lnkAttachDomicile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lbldomicileaddressapproval
'
Me.lbldomicileaddressapproval.BackColor = System.Drawing.Color.PowderBlue
Me.lbldomicileaddressapproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lbldomicileaddressapproval.ForeColor = System.Drawing.Color.Black
Me.lbldomicileaddressapproval.Location = New System.Drawing.Point(355, 1)
Me.lbldomicileaddressapproval.Name = "lbldomicileaddressapproval"
Me.lbldomicileaddressapproval.Size = New System.Drawing.Size(169, 16)
Me.lbldomicileaddressapproval.TabIndex = 185
Me.lbldomicileaddressapproval.Text = "Pending Approval"
Me.lbldomicileaddressapproval.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lbldomicileaddressapproval.Visible = false
'
'lblDomicileTown1
'
Me.lblDomicileTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileTown1.Location = New System.Drawing.Point(273, 148)
Me.lblDomicileTown1.Name = "lblDomicileTown1"
Me.lblDomicileTown1.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileTown1.TabIndex = 148
Me.lblDomicileTown1.Text = "Town1"
Me.lblDomicileTown1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicileTown1
'
Me.cboDomicileTown1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileTown1.FormattingEnabled = true
Me.cboDomicileTown1.Location = New System.Drawing.Point(355, 145)
Me.cboDomicileTown1.Name = "cboDomicileTown1"
Me.cboDomicileTown1.Size = New System.Drawing.Size(169, 21)
Me.cboDomicileTown1.TabIndex = 147
'
'lblDomicilevillage
'
Me.lblDomicilevillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicilevillage.Location = New System.Drawing.Point(16, 148)
Me.lblDomicilevillage.Name = "lblDomicilevillage"
Me.lblDomicilevillage.Size = New System.Drawing.Size(76, 15)
Me.lblDomicilevillage.TabIndex = 146
Me.lblDomicilevillage.Text = "Village"
Me.lblDomicilevillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicilevillage
'
Me.cboDomicilevillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicilevillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicilevillage.FormattingEnabled = true
Me.cboDomicilevillage.Location = New System.Drawing.Point(98, 145)
Me.cboDomicilevillage.Name = "cboDomicilevillage"
Me.cboDomicilevillage.Size = New System.Drawing.Size(168, 21)
Me.cboDomicilevillage.TabIndex = 145
'
'lblDomicileChiefdom
'
Me.lblDomicileChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileChiefdom.Location = New System.Drawing.Point(530, 121)
Me.lblDomicileChiefdom.Name = "lblDomicileChiefdom"
Me.lblDomicileChiefdom.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileChiefdom.TabIndex = 144
Me.lblDomicileChiefdom.Text = "Chiefdom"
Me.lblDomicileChiefdom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicileChiefdom
'
Me.cboDomicileChiefdom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileChiefdom.FormattingEnabled = true
Me.cboDomicileChiefdom.Location = New System.Drawing.Point(612, 118)
Me.cboDomicileChiefdom.Name = "cboDomicileChiefdom"
Me.cboDomicileChiefdom.Size = New System.Drawing.Size(169, 21)
Me.cboDomicileChiefdom.TabIndex = 143
'
'lblDomicileRoadStreet1
'
Me.lblDomicileRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileRoadStreet1.Location = New System.Drawing.Point(273, 121)
Me.lblDomicileRoadStreet1.Name = "lblDomicileRoadStreet1"
Me.lblDomicileRoadStreet1.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileRoadStreet1.TabIndex = 142
Me.lblDomicileRoadStreet1.Text = "Road/Street1"
Me.lblDomicileRoadStreet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicileRoadStreet1
'
Me.cboDomicileRoadStreet1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileRoadStreet1.FormattingEnabled = true
Me.cboDomicileRoadStreet1.Location = New System.Drawing.Point(355, 118)
Me.cboDomicileRoadStreet1.Name = "cboDomicileRoadStreet1"
Me.cboDomicileRoadStreet1.Size = New System.Drawing.Size(169, 21)
Me.cboDomicileRoadStreet1.TabIndex = 141
'
'cboDomicileProvince1
'
Me.cboDomicileProvince1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileProvince1.FormattingEnabled = true
Me.cboDomicileProvince1.Location = New System.Drawing.Point(98, 118)
Me.cboDomicileProvince1.Name = "cboDomicileProvince1"
Me.cboDomicileProvince1.Size = New System.Drawing.Size(168, 21)
Me.cboDomicileProvince1.TabIndex = 140
'
'lblDomicileProvince1
'
Me.lblDomicileProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileProvince1.Location = New System.Drawing.Point(16, 121)
Me.lblDomicileProvince1.Name = "lblDomicileProvince1"
Me.lblDomicileProvince1.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileProvince1.TabIndex = 139
Me.lblDomicileProvince1.Text = "Prov/Region1"
Me.lblDomicileProvince1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicileState
'
Me.cboDomicileState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileState.FormattingEnabled = true
Me.cboDomicileState.Location = New System.Drawing.Point(98, 64)
Me.cboDomicileState.Name = "cboDomicileState"
Me.cboDomicileState.Size = New System.Drawing.Size(168, 21)
Me.cboDomicileState.TabIndex = 116
'
'lblDomicileState
'
Me.lblDomicileState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileState.Location = New System.Drawing.Point(16, 67)
Me.lblDomicileState.Name = "lblDomicileState"
Me.lblDomicileState.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileState.TabIndex = 115
Me.lblDomicileState.Text = "State"
Me.lblDomicileState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDomicileAddress2
'
Me.lblDomicileAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileAddress2.Location = New System.Drawing.Point(272, 40)
Me.lblDomicileAddress2.Name = "lblDomicileAddress2"
Me.lblDomicileAddress2.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileAddress2.TabIndex = 111
Me.lblDomicileAddress2.Text = "Address2"
Me.lblDomicileAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboDomicilePostCode
'
Me.cboDomicilePostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicilePostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicilePostCode.FormattingEnabled = true
Me.cboDomicilePostCode.Location = New System.Drawing.Point(612, 64)
Me.cboDomicilePostCode.Name = "cboDomicilePostCode"
Me.cboDomicilePostCode.Size = New System.Drawing.Size(169, 21)
Me.cboDomicilePostCode.TabIndex = 120
'
'cboDomicileTown
'
Me.cboDomicileTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileTown.FormattingEnabled = true
Me.cboDomicileTown.Location = New System.Drawing.Point(355, 64)
Me.cboDomicileTown.Name = "cboDomicileTown"
Me.cboDomicileTown.Size = New System.Drawing.Size(169, 21)
Me.cboDomicileTown.TabIndex = 118
'
'txtDomicileAltNo
'
Me.txtDomicileAltNo.Flags = 0
Me.txtDomicileAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileAltNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileAltNo.Location = New System.Drawing.Point(612, 172)
Me.txtDomicileAltNo.Name = "txtDomicileAltNo"
Me.txtDomicileAltNo.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileAltNo.TabIndex = 132
'
'lblDomicileAltNo
'
Me.lblDomicileAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileAltNo.Location = New System.Drawing.Point(530, 175)
Me.lblDomicileAltNo.Name = "lblDomicileAltNo"
Me.lblDomicileAltNo.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileAltNo.TabIndex = 131
Me.lblDomicileAltNo.Text = "Alternative No"
Me.lblDomicileAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicilePlotNo
'
Me.txtDomicilePlotNo.Flags = 0
Me.txtDomicilePlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicilePlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicilePlotNo.Location = New System.Drawing.Point(97, 172)
Me.txtDomicilePlotNo.Name = "txtDomicilePlotNo"
Me.txtDomicilePlotNo.Size = New System.Drawing.Size(169, 21)
Me.txtDomicilePlotNo.TabIndex = 128
'
'lblDomicilePlotNo
'
Me.lblDomicilePlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicilePlotNo.Location = New System.Drawing.Point(16, 175)
Me.lblDomicilePlotNo.Name = "lblDomicilePlotNo"
Me.lblDomicilePlotNo.Size = New System.Drawing.Size(76, 15)
Me.lblDomicilePlotNo.TabIndex = 127
Me.lblDomicilePlotNo.Text = "Plot No"
Me.lblDomicilePlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileProvince
'
Me.txtDomicileProvince.Flags = 0
Me.txtDomicileProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileProvince.Location = New System.Drawing.Point(98, 91)
Me.txtDomicileProvince.Name = "txtDomicileProvince"
Me.txtDomicileProvince.Size = New System.Drawing.Size(168, 21)
Me.txtDomicileProvince.TabIndex = 122
'
'lblDomicileProvince
'
Me.lblDomicileProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileProvince.Location = New System.Drawing.Point(16, 94)
Me.lblDomicileProvince.Name = "lblDomicileProvince"
Me.lblDomicileProvince.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileProvince.TabIndex = 121
Me.lblDomicileProvince.Text = "Prov/Region"
Me.lblDomicileProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDomicilePostCode
'
Me.lblDomicilePostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicilePostCode.Location = New System.Drawing.Point(530, 67)
Me.lblDomicilePostCode.Name = "lblDomicilePostCode"
Me.lblDomicilePostCode.Size = New System.Drawing.Size(76, 15)
Me.lblDomicilePostCode.TabIndex = 119
Me.lblDomicilePostCode.Text = "Post Code"
Me.lblDomicilePostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileFax
'
Me.txtDomicileFax.Flags = 0
Me.txtDomicileFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileFax.Location = New System.Drawing.Point(355, 172)
Me.txtDomicileFax.Name = "txtDomicileFax"
Me.txtDomicileFax.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileFax.TabIndex = 136
'
'lblDomicileFax
'
Me.lblDomicileFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileFax.Location = New System.Drawing.Point(273, 175)
Me.lblDomicileFax.Name = "lblDomicileFax"
Me.lblDomicileFax.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileFax.TabIndex = 135
Me.lblDomicileFax.Text = "Fax"
Me.lblDomicileFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileMobile
'
Me.txtDomicileMobile.Flags = 0
Me.txtDomicileMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileMobile.Location = New System.Drawing.Point(612, 145)
Me.txtDomicileMobile.Name = "txtDomicileMobile"
Me.txtDomicileMobile.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileMobile.TabIndex = 130
'
'lblDomicileMobileNo
'
Me.lblDomicileMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileMobileNo.Location = New System.Drawing.Point(530, 148)
Me.lblDomicileMobileNo.Name = "lblDomicileMobileNo"
Me.lblDomicileMobileNo.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileMobileNo.TabIndex = 129
Me.lblDomicileMobileNo.Text = "Mobile"
Me.lblDomicileMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileTelNo
'
Me.txtDomicileTelNo.Flags = 0
Me.txtDomicileTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileTelNo.Location = New System.Drawing.Point(97, 199)
Me.txtDomicileTelNo.Name = "txtDomicileTelNo"
Me.txtDomicileTelNo.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileTelNo.TabIndex = 134
'
'lblDomicileTelNo
'
Me.lblDomicileTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileTelNo.Location = New System.Drawing.Point(16, 202)
Me.lblDomicileTelNo.Name = "lblDomicileTelNo"
Me.lblDomicileTelNo.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileTelNo.TabIndex = 133
Me.lblDomicileTelNo.Text = "Tel. No"
Me.lblDomicileTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileEstate
'
Me.txtDomicileEstate.Flags = 0
Me.txtDomicileEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileEstate.Location = New System.Drawing.Point(612, 91)
Me.txtDomicileEstate.Name = "txtDomicileEstate"
Me.txtDomicileEstate.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileEstate.TabIndex = 126
'
'txtDomicileRoad
'
Me.txtDomicileRoad.Flags = 0
Me.txtDomicileRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileRoad.Location = New System.Drawing.Point(355, 91)
Me.txtDomicileRoad.Name = "txtDomicileRoad"
Me.txtDomicileRoad.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileRoad.TabIndex = 124
'
'txtDomicileEmail
'
Me.txtDomicileEmail.Flags = 0
Me.txtDomicileEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileEmail.Location = New System.Drawing.Point(355, 199)
Me.txtDomicileEmail.Name = "txtDomicileEmail"
Me.txtDomicileEmail.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileEmail.TabIndex = 138
'
'cboDomicileCountry
'
Me.cboDomicileCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboDomicileCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboDomicileCountry.FormattingEnabled = true
Me.cboDomicileCountry.Location = New System.Drawing.Point(611, 37)
Me.cboDomicileCountry.Name = "cboDomicileCountry"
Me.cboDomicileCountry.Size = New System.Drawing.Size(169, 21)
Me.cboDomicileCountry.TabIndex = 114
'
'txtDomicileAddress2
'
Me.txtDomicileAddress2.Flags = 0
Me.txtDomicileAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileAddress2.Location = New System.Drawing.Point(354, 37)
Me.txtDomicileAddress2.Name = "txtDomicileAddress2"
Me.txtDomicileAddress2.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileAddress2.TabIndex = 112
'
'lblDomicileRoad
'
Me.lblDomicileRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileRoad.Location = New System.Drawing.Point(273, 94)
Me.lblDomicileRoad.Name = "lblDomicileRoad"
Me.lblDomicileRoad.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileRoad.TabIndex = 123
Me.lblDomicileRoad.Text = "Road/Street"
Me.lblDomicileRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDomicileEState
'
Me.lblDomicileEState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileEState.Location = New System.Drawing.Point(530, 94)
Me.lblDomicileEState.Name = "lblDomicileEState"
Me.lblDomicileEState.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileEState.TabIndex = 125
Me.lblDomicileEState.Text = "Estate"
Me.lblDomicileEState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDomicileEmail
'
Me.lblDomicileEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileEmail.Location = New System.Drawing.Point(273, 202)
Me.lblDomicileEmail.Name = "lblDomicileEmail"
Me.lblDomicileEmail.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileEmail.TabIndex = 137
Me.lblDomicileEmail.Text = "Email"
Me.lblDomicileEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDomicilePostTown
'
Me.lblDomicilePostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicilePostTown.Location = New System.Drawing.Point(273, 67)
Me.lblDomicilePostTown.Name = "lblDomicilePostTown"
Me.lblDomicilePostTown.Size = New System.Drawing.Size(76, 15)
Me.lblDomicilePostTown.TabIndex = 117
Me.lblDomicilePostTown.Text = "Post Town"
Me.lblDomicilePostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkCopyAddress
'
Me.lnkCopyAddress.BackColor = System.Drawing.Color.White
Me.lnkCopyAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkCopyAddress.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkCopyAddress.Location = New System.Drawing.Point(707, 0)
Me.lnkCopyAddress.Name = "lnkCopyAddress"
Me.lnkCopyAddress.Size = New System.Drawing.Size(74, 13)
Me.lnkCopyAddress.TabIndex = 32
Me.lnkCopyAddress.TabStop = true
Me.lnkCopyAddress.Text = "Copy Address"
'
'lblDomicilePostCountry
'
Me.lblDomicilePostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicilePostCountry.Location = New System.Drawing.Point(529, 40)
Me.lblDomicilePostCountry.Name = "lblDomicilePostCountry"
Me.lblDomicilePostCountry.Size = New System.Drawing.Size(76, 15)
Me.lblDomicilePostCountry.TabIndex = 113
Me.lblDomicilePostCountry.Text = "Post Country"
Me.lblDomicilePostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDomicileAddress1
'
Me.txtDomicileAddress1.Flags = 0
Me.txtDomicileAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDomicileAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDomicileAddress1.Location = New System.Drawing.Point(97, 37)
Me.txtDomicileAddress1.Name = "txtDomicileAddress1"
Me.txtDomicileAddress1.Size = New System.Drawing.Size(169, 21)
Me.txtDomicileAddress1.TabIndex = 110
'
'lblDomicileAddress1
'
Me.lblDomicileAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDomicileAddress1.Location = New System.Drawing.Point(15, 40)
Me.lblDomicileAddress1.Name = "lblDomicileAddress1"
Me.lblDomicileAddress1.Size = New System.Drawing.Size(76, 15)
Me.lblDomicileAddress1.TabIndex = 109
Me.lblDomicileAddress1.Text = "Address1"
Me.lblDomicileAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'GbPresentAddress
'
Me.GbPresentAddress.Controls.Add(Me.lnkAttachPresent)
Me.GbPresentAddress.Controls.Add(Me.lblpresentaddressapproval)
Me.GbPresentAddress.Controls.Add(Me.lblPresentTown1)
Me.GbPresentAddress.Controls.Add(Me.cboPresentTown1)
Me.GbPresentAddress.Controls.Add(Me.lblPresentVillage)
Me.GbPresentAddress.Controls.Add(Me.cboPresentVillage)
Me.GbPresentAddress.Controls.Add(Me.lblPresentChiefdom)
Me.GbPresentAddress.Controls.Add(Me.cboPresentChiefdom)
Me.GbPresentAddress.Controls.Add(Me.lblPresentRoadStreet1)
Me.GbPresentAddress.Controls.Add(Me.cboPresentRoadStreet1)
Me.GbPresentAddress.Controls.Add(Me.cboPresentProvRegion1)
Me.GbPresentAddress.Controls.Add(Me.lblPersentProvince1)
Me.GbPresentAddress.Controls.Add(Me.lblAddress2)
Me.GbPresentAddress.Controls.Add(Me.lblPresentState)
Me.GbPresentAddress.Controls.Add(Me.cboState)
Me.GbPresentAddress.Controls.Add(Me.cboTown)
Me.GbPresentAddress.Controls.Add(Me.cboPostCode)
Me.GbPresentAddress.Controls.Add(Me.lblPostTown)
Me.GbPresentAddress.Controls.Add(Me.txtRoad)
Me.GbPresentAddress.Controls.Add(Me.lblRoad)
Me.GbPresentAddress.Controls.Add(Me.txtFax)
Me.GbPresentAddress.Controls.Add(Me.lblFax)
Me.GbPresentAddress.Controls.Add(Me.txtAlternativeNo)
Me.GbPresentAddress.Controls.Add(Me.txtEstate)
Me.GbPresentAddress.Controls.Add(Me.lblEstate)
Me.GbPresentAddress.Controls.Add(Me.txtProvince)
Me.GbPresentAddress.Controls.Add(Me.lblProvince)
Me.GbPresentAddress.Controls.Add(Me.lblAlternativeNo)
Me.GbPresentAddress.Controls.Add(Me.txtMobile)
Me.GbPresentAddress.Controls.Add(Me.txtPlotNo)
Me.GbPresentAddress.Controls.Add(Me.txtEmail)
Me.GbPresentAddress.Controls.Add(Me.txtTelNo)
Me.GbPresentAddress.Controls.Add(Me.cboPresentCountry)
Me.GbPresentAddress.Controls.Add(Me.txtAddress2)
Me.GbPresentAddress.Controls.Add(Me.lblPloteNo)
Me.GbPresentAddress.Controls.Add(Me.lblMobile)
Me.GbPresentAddress.Controls.Add(Me.lblEmail)
Me.GbPresentAddress.Controls.Add(Me.lblTelNo)
Me.GbPresentAddress.Controls.Add(Me.lblPostCountry)
Me.GbPresentAddress.Controls.Add(Me.lblPostcode)
Me.GbPresentAddress.Controls.Add(Me.txtAddress1)
Me.GbPresentAddress.Controls.Add(Me.lblAddress)
Me.GbPresentAddress.ForeColor = System.Drawing.Color.Black
Me.GbPresentAddress.Location = New System.Drawing.Point(14, 11)
Me.GbPresentAddress.Name = "GbPresentAddress"
Me.GbPresentAddress.Size = New System.Drawing.Size(796, 225)
Me.GbPresentAddress.TabIndex = 119
Me.GbPresentAddress.TabStop = false
Me.GbPresentAddress.Text = "Present Address"
'
'lnkAttachPresent
'
Me.lnkAttachPresent.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachPresent.Location = New System.Drawing.Point(613, 9)
Me.lnkAttachPresent.Name = "lnkAttachPresent"
Me.lnkAttachPresent.Size = New System.Drawing.Size(178, 17)
Me.lnkAttachPresent.TabIndex = 185
Me.lnkAttachPresent.TabStop = true
Me.lnkAttachPresent.Text = "Scan/Attach Document"
Me.lnkAttachPresent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblpresentaddressapproval
'
Me.lblpresentaddressapproval.BackColor = System.Drawing.Color.PowderBlue
Me.lblpresentaddressapproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblpresentaddressapproval.ForeColor = System.Drawing.Color.Black
Me.lblpresentaddressapproval.Location = New System.Drawing.Point(358, 0)
Me.lblpresentaddressapproval.Name = "lblpresentaddressapproval"
Me.lblpresentaddressapproval.Size = New System.Drawing.Size(165, 16)
Me.lblpresentaddressapproval.TabIndex = 184
Me.lblpresentaddressapproval.Text = "Pending Approval"
Me.lblpresentaddressapproval.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblpresentaddressapproval.Visible = false
'
'lblPresentTown1
'
Me.lblPresentTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPresentTown1.Location = New System.Drawing.Point(273, 139)
Me.lblPresentTown1.Name = "lblPresentTown1"
Me.lblPresentTown1.Size = New System.Drawing.Size(76, 15)
Me.lblPresentTown1.TabIndex = 144
Me.lblPresentTown1.Text = "Town1"
Me.lblPresentTown1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboPresentTown1
'
Me.cboPresentTown1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentTown1.FormattingEnabled = true
Me.cboPresentTown1.Location = New System.Drawing.Point(355, 136)
Me.cboPresentTown1.Name = "cboPresentTown1"
Me.cboPresentTown1.Size = New System.Drawing.Size(169, 21)
Me.cboPresentTown1.TabIndex = 143
'
'lblPresentVillage
'
Me.lblPresentVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPresentVillage.Location = New System.Drawing.Point(16, 139)
Me.lblPresentVillage.Name = "lblPresentVillage"
Me.lblPresentVillage.Size = New System.Drawing.Size(76, 15)
Me.lblPresentVillage.TabIndex = 142
Me.lblPresentVillage.Text = "Village"
Me.lblPresentVillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboPresentVillage
'
Me.cboPresentVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentVillage.FormattingEnabled = true
Me.cboPresentVillage.Location = New System.Drawing.Point(98, 136)
Me.cboPresentVillage.Name = "cboPresentVillage"
Me.cboPresentVillage.Size = New System.Drawing.Size(168, 21)
Me.cboPresentVillage.TabIndex = 141
'
'lblPresentChiefdom
'
Me.lblPresentChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPresentChiefdom.Location = New System.Drawing.Point(530, 112)
Me.lblPresentChiefdom.Name = "lblPresentChiefdom"
Me.lblPresentChiefdom.Size = New System.Drawing.Size(76, 15)
Me.lblPresentChiefdom.TabIndex = 140
Me.lblPresentChiefdom.Text = "Chiefdom"
Me.lblPresentChiefdom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboPresentChiefdom
'
Me.cboPresentChiefdom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentChiefdom.FormattingEnabled = true
Me.cboPresentChiefdom.Location = New System.Drawing.Point(612, 109)
Me.cboPresentChiefdom.Name = "cboPresentChiefdom"
Me.cboPresentChiefdom.Size = New System.Drawing.Size(169, 21)
Me.cboPresentChiefdom.TabIndex = 139
'
'lblPresentRoadStreet1
'
Me.lblPresentRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPresentRoadStreet1.Location = New System.Drawing.Point(273, 112)
Me.lblPresentRoadStreet1.Name = "lblPresentRoadStreet1"
Me.lblPresentRoadStreet1.Size = New System.Drawing.Size(76, 15)
Me.lblPresentRoadStreet1.TabIndex = 138
Me.lblPresentRoadStreet1.Text = "Road/Street1"
Me.lblPresentRoadStreet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboPresentRoadStreet1
'
Me.cboPresentRoadStreet1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentRoadStreet1.FormattingEnabled = true
Me.cboPresentRoadStreet1.Location = New System.Drawing.Point(355, 109)
Me.cboPresentRoadStreet1.Name = "cboPresentRoadStreet1"
Me.cboPresentRoadStreet1.Size = New System.Drawing.Size(169, 21)
Me.cboPresentRoadStreet1.TabIndex = 137
'
'cboPresentProvRegion1
'
Me.cboPresentProvRegion1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentProvRegion1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentProvRegion1.FormattingEnabled = true
Me.cboPresentProvRegion1.Location = New System.Drawing.Point(98, 109)
Me.cboPresentProvRegion1.Name = "cboPresentProvRegion1"
Me.cboPresentProvRegion1.Size = New System.Drawing.Size(168, 21)
Me.cboPresentProvRegion1.TabIndex = 136
'
'lblPersentProvince1
'
Me.lblPersentProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPersentProvince1.Location = New System.Drawing.Point(16, 112)
Me.lblPersentProvince1.Name = "lblPersentProvince1"
Me.lblPersentProvince1.Size = New System.Drawing.Size(76, 15)
Me.lblPersentProvince1.TabIndex = 135
Me.lblPersentProvince1.Text = "Prov/Region1"
Me.lblPersentProvince1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblAddress2
'
Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblAddress2.Location = New System.Drawing.Point(273, 31)
Me.lblAddress2.Name = "lblAddress2"
Me.lblAddress2.Size = New System.Drawing.Size(76, 15)
Me.lblAddress2.TabIndex = 107
Me.lblAddress2.Text = "Address2"
Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblPresentState
'
Me.lblPresentState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPresentState.Location = New System.Drawing.Point(16, 58)
Me.lblPresentState.Name = "lblPresentState"
Me.lblPresentState.Size = New System.Drawing.Size(76, 15)
Me.lblPresentState.TabIndex = 111
Me.lblPresentState.Text = "State"
Me.lblPresentState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboState
'
Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboState.FormattingEnabled = true
Me.cboState.Location = New System.Drawing.Point(98, 55)
Me.cboState.Name = "cboState"
Me.cboState.Size = New System.Drawing.Size(169, 21)
Me.cboState.TabIndex = 112
'
'cboTown
'
Me.cboTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboTown.FormattingEnabled = true
Me.cboTown.Location = New System.Drawing.Point(355, 55)
Me.cboTown.Name = "cboTown"
Me.cboTown.Size = New System.Drawing.Size(169, 21)
Me.cboTown.TabIndex = 114
'
'cboPostCode
'
Me.cboPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPostCode.FormattingEnabled = true
Me.cboPostCode.Location = New System.Drawing.Point(612, 55)
Me.cboPostCode.Name = "cboPostCode"
Me.cboPostCode.Size = New System.Drawing.Size(169, 21)
Me.cboPostCode.TabIndex = 116
'
'lblPostTown
'
Me.lblPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPostTown.Location = New System.Drawing.Point(273, 58)
Me.lblPostTown.Name = "lblPostTown"
Me.lblPostTown.Size = New System.Drawing.Size(76, 15)
Me.lblPostTown.TabIndex = 113
Me.lblPostTown.Text = "Post Town"
Me.lblPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtRoad
'
Me.txtRoad.Flags = 0
Me.txtRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRoad.Location = New System.Drawing.Point(355, 82)
Me.txtRoad.Name = "txtRoad"
Me.txtRoad.Size = New System.Drawing.Size(169, 21)
Me.txtRoad.TabIndex = 120
'
'lblRoad
'
Me.lblRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblRoad.Location = New System.Drawing.Point(273, 85)
Me.lblRoad.Name = "lblRoad"
Me.lblRoad.Size = New System.Drawing.Size(76, 15)
Me.lblRoad.TabIndex = 119
Me.lblRoad.Text = "Road/Street"
Me.lblRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtFax
'
Me.txtFax.Flags = 0
Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtFax.Location = New System.Drawing.Point(355, 190)
Me.txtFax.Name = "txtFax"
Me.txtFax.Size = New System.Drawing.Size(169, 21)
Me.txtFax.TabIndex = 132
'
'lblFax
'
Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblFax.Location = New System.Drawing.Point(273, 191)
Me.lblFax.Name = "lblFax"
Me.lblFax.Size = New System.Drawing.Size(76, 15)
Me.lblFax.TabIndex = 131
Me.lblFax.Text = "Fax"
Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtAlternativeNo
'
Me.txtAlternativeNo.Flags = 0
Me.txtAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtAlternativeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtAlternativeNo.Location = New System.Drawing.Point(612, 163)
Me.txtAlternativeNo.Name = "txtAlternativeNo"
Me.txtAlternativeNo.Size = New System.Drawing.Size(169, 21)
Me.txtAlternativeNo.TabIndex = 128
'
'txtEstate
'
Me.txtEstate.Flags = 0
Me.txtEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEstate.Location = New System.Drawing.Point(612, 82)
Me.txtEstate.Name = "txtEstate"
Me.txtEstate.Size = New System.Drawing.Size(169, 21)
Me.txtEstate.TabIndex = 122
'
'lblEstate
'
Me.lblEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEstate.Location = New System.Drawing.Point(530, 85)
Me.lblEstate.Name = "lblEstate"
Me.lblEstate.Size = New System.Drawing.Size(76, 15)
Me.lblEstate.TabIndex = 121
Me.lblEstate.Text = "Estate"
Me.lblEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtProvince
'
Me.txtProvince.Flags = 0
Me.txtProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtProvince.Location = New System.Drawing.Point(98, 82)
Me.txtProvince.Name = "txtProvince"
Me.txtProvince.Size = New System.Drawing.Size(169, 21)
Me.txtProvince.TabIndex = 118
'
'lblProvince
'
Me.lblProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblProvince.Location = New System.Drawing.Point(16, 85)
Me.lblProvince.Name = "lblProvince"
Me.lblProvince.Size = New System.Drawing.Size(76, 15)
Me.lblProvince.TabIndex = 117
Me.lblProvince.Text = "Prov/Region"
Me.lblProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblAlternativeNo
'
Me.lblAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblAlternativeNo.Location = New System.Drawing.Point(530, 166)
Me.lblAlternativeNo.Name = "lblAlternativeNo"
Me.lblAlternativeNo.Size = New System.Drawing.Size(76, 15)
Me.lblAlternativeNo.TabIndex = 127
Me.lblAlternativeNo.Text = "Alternative No"
Me.lblAlternativeNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtMobile
'
Me.txtMobile.Flags = 0
Me.txtMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtMobile.Location = New System.Drawing.Point(612, 136)
Me.txtMobile.Name = "txtMobile"
Me.txtMobile.Size = New System.Drawing.Size(169, 21)
Me.txtMobile.TabIndex = 126
'
'txtPlotNo
'
Me.txtPlotNo.Flags = 0
Me.txtPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtPlotNo.Location = New System.Drawing.Point(355, 163)
Me.txtPlotNo.Name = "txtPlotNo"
Me.txtPlotNo.Size = New System.Drawing.Size(169, 21)
Me.txtPlotNo.TabIndex = 124
'
'txtEmail
'
Me.txtEmail.Flags = 0
Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmail.Location = New System.Drawing.Point(98, 190)
Me.txtEmail.Name = "txtEmail"
Me.txtEmail.Size = New System.Drawing.Size(169, 21)
Me.txtEmail.TabIndex = 134
'
'txtTelNo
'
Me.txtTelNo.Flags = 0
Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtTelNo.Location = New System.Drawing.Point(98, 163)
Me.txtTelNo.Name = "txtTelNo"
Me.txtTelNo.Size = New System.Drawing.Size(169, 21)
Me.txtTelNo.TabIndex = 130
'
'cboPresentCountry
'
Me.cboPresentCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPresentCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPresentCountry.FormattingEnabled = true
Me.cboPresentCountry.Location = New System.Drawing.Point(612, 28)
Me.cboPresentCountry.Name = "cboPresentCountry"
Me.cboPresentCountry.Size = New System.Drawing.Size(169, 21)
Me.cboPresentCountry.TabIndex = 110
'
'txtAddress2
'
Me.txtAddress2.Flags = 0
Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtAddress2.Location = New System.Drawing.Point(355, 28)
Me.txtAddress2.Name = "txtAddress2"
Me.txtAddress2.Size = New System.Drawing.Size(169, 21)
Me.txtAddress2.TabIndex = 108
'
'lblPloteNo
'
Me.lblPloteNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPloteNo.Location = New System.Drawing.Point(273, 166)
Me.lblPloteNo.Name = "lblPloteNo"
Me.lblPloteNo.Size = New System.Drawing.Size(76, 15)
Me.lblPloteNo.TabIndex = 123
Me.lblPloteNo.Text = "Plot No"
Me.lblPloteNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblMobile
'
Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMobile.Location = New System.Drawing.Point(530, 139)
Me.lblMobile.Name = "lblMobile"
Me.lblMobile.Size = New System.Drawing.Size(76, 15)
Me.lblMobile.TabIndex = 125
Me.lblMobile.Text = "Mobile"
Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmail
'
Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmail.Location = New System.Drawing.Point(16, 191)
Me.lblEmail.Name = "lblEmail"
Me.lblEmail.Size = New System.Drawing.Size(76, 15)
Me.lblEmail.TabIndex = 133
Me.lblEmail.Text = "Email"
Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblTelNo
'
Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblTelNo.Location = New System.Drawing.Point(16, 166)
Me.lblTelNo.Name = "lblTelNo"
Me.lblTelNo.Size = New System.Drawing.Size(76, 15)
Me.lblTelNo.TabIndex = 129
Me.lblTelNo.Text = "Tel. No"
Me.lblTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblPostCountry
'
Me.lblPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPostCountry.Location = New System.Drawing.Point(530, 31)
Me.lblPostCountry.Name = "lblPostCountry"
Me.lblPostCountry.Size = New System.Drawing.Size(76, 15)
Me.lblPostCountry.TabIndex = 109
Me.lblPostCountry.Text = "Post Country"
Me.lblPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblPostcode
'
Me.lblPostcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPostcode.Location = New System.Drawing.Point(530, 58)
Me.lblPostcode.Name = "lblPostcode"
Me.lblPostcode.Size = New System.Drawing.Size(76, 15)
Me.lblPostcode.TabIndex = 115
Me.lblPostcode.Text = "Post Code"
Me.lblPostcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtAddress1
'
Me.txtAddress1.BackColor = System.Drawing.Color.White
Me.txtAddress1.Flags = 0
Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtAddress1.Location = New System.Drawing.Point(98, 28)
Me.txtAddress1.Name = "txtAddress1"
Me.txtAddress1.Size = New System.Drawing.Size(169, 21)
Me.txtAddress1.TabIndex = 106
'
'lblAddress
'
Me.lblAddress.BackColor = System.Drawing.Color.Transparent
Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblAddress.Location = New System.Drawing.Point(16, 31)
Me.lblAddress.Name = "lblAddress"
Me.lblAddress.Size = New System.Drawing.Size(76, 15)
Me.lblAddress.TabIndex = 105
Me.lblAddress.Text = "Address1"
Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabpEmergencyContact
'
Me.tabpEmergencyContact.Controls.Add(Me.gbEmergencyInfo)
Me.tabpEmergencyContact.Location = New System.Drawing.Point(4, 22)
Me.tabpEmergencyContact.Name = "tabpEmergencyContact"
Me.tabpEmergencyContact.Padding = New System.Windows.Forms.Padding(3)
Me.tabpEmergencyContact.Size = New System.Drawing.Size(850, 266)
Me.tabpEmergencyContact.TabIndex = 6
Me.tabpEmergencyContact.Text = "Emergency Contacts"
Me.tabpEmergencyContact.UseVisualStyleBackColor = true
'
'gbEmergencyInfo
'
Me.gbEmergencyInfo.BorderColor = System.Drawing.Color.Black
Me.gbEmergencyInfo.Checked = false
Me.gbEmergencyInfo.CollapseAllExceptThis = false
Me.gbEmergencyInfo.CollapsedHoverImage = Nothing
Me.gbEmergencyInfo.CollapsedNormalImage = Nothing
Me.gbEmergencyInfo.CollapsedPressedImage = Nothing
Me.gbEmergencyInfo.CollapseOnLoad = false
Me.gbEmergencyInfo.Controls.Add(Me.pnlEmergencyContact)
Me.gbEmergencyInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbEmergencyInfo.ExpandedHoverImage = Nothing
Me.gbEmergencyInfo.ExpandedNormalImage = Nothing
Me.gbEmergencyInfo.ExpandedPressedImage = Nothing
Me.gbEmergencyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbEmergencyInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbEmergencyInfo.HeaderHeight = 25
Me.gbEmergencyInfo.HeaderMessage = ""
Me.gbEmergencyInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbEmergencyInfo.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbEmergencyInfo.HeightOnCollapse = 0
Me.gbEmergencyInfo.LeftTextSpace = 0
Me.gbEmergencyInfo.Location = New System.Drawing.Point(3, 3)
Me.gbEmergencyInfo.Name = "gbEmergencyInfo"
Me.gbEmergencyInfo.OpenHeight = 300
Me.gbEmergencyInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbEmergencyInfo.ShowBorder = true
Me.gbEmergencyInfo.ShowCheckBox = false
Me.gbEmergencyInfo.ShowCollapseButton = false
Me.gbEmergencyInfo.ShowDefaultBorderColor = true
Me.gbEmergencyInfo.ShowDownButton = false
Me.gbEmergencyInfo.ShowHeader = true
Me.gbEmergencyInfo.Size = New System.Drawing.Size(844, 260)
Me.gbEmergencyInfo.TabIndex = 116
Me.gbEmergencyInfo.Temp = 0
Me.gbEmergencyInfo.Text = "Emergency Info."
Me.gbEmergencyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'pnlEmergencyContact
'
Me.pnlEmergencyContact.AutoScroll = true
Me.pnlEmergencyContact.Controls.Add(Me.GbEmergencyContact1)
Me.pnlEmergencyContact.Controls.Add(Me.GbEmergencyContact2)
Me.pnlEmergencyContact.Controls.Add(Me.GbEmergencyContact3)
Me.pnlEmergencyContact.Location = New System.Drawing.Point(2, 26)
Me.pnlEmergencyContact.Name = "pnlEmergencyContact"
Me.pnlEmergencyContact.Size = New System.Drawing.Size(839, 228)
Me.pnlEmergencyContact.TabIndex = 115
'
'GbEmergencyContact1
'
Me.GbEmergencyContact1.Controls.Add(Me.lnkAttachEmerContact1)
Me.GbEmergencyContact1.Controls.Add(Me.lblEAddressapproval1)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgEmail)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgEmail)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgFax)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgFax)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgFirstName)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgAltNo)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgTelNo)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgTelNo)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgAltNo)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgFirstName)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgLastName)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgPlotNo)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgMobile)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgMobile)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgLastName)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgPlotNo)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgAddress)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgProvince)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgProvince)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgAddress)
Me.GbEmergencyContact1.Controls.Add(Me.cboEmgCountry)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgPostCountry)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgEstate)
Me.GbEmergencyContact1.Controls.Add(Me.cboEmgState)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgEstate)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgPostTown)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgState)
Me.GbEmergencyContact1.Controls.Add(Me.cboEmgTown)
Me.GbEmergencyContact1.Controls.Add(Me.txtEmgRoad)
Me.GbEmergencyContact1.Controls.Add(Me.cboEmgPostCode)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgPostCode)
Me.GbEmergencyContact1.Controls.Add(Me.lblEmgRoad)
Me.GbEmergencyContact1.Location = New System.Drawing.Point(6, 8)
Me.GbEmergencyContact1.Name = "GbEmergencyContact1"
Me.GbEmergencyContact1.Size = New System.Drawing.Size(811, 177)
Me.GbEmergencyContact1.TabIndex = 185
Me.GbEmergencyContact1.TabStop = false
Me.GbEmergencyContact1.Text = "Emergency Contact 1 "
'
'lnkAttachEmerContact1
'
Me.lnkAttachEmerContact1.BackColor = System.Drawing.Color.White
Me.lnkAttachEmerContact1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachEmerContact1.Location = New System.Drawing.Point(637, 17)
Me.lnkAttachEmerContact1.Name = "lnkAttachEmerContact1"
Me.lnkAttachEmerContact1.Size = New System.Drawing.Size(168, 17)
Me.lnkAttachEmerContact1.TabIndex = 217
Me.lnkAttachEmerContact1.TabStop = true
Me.lnkAttachEmerContact1.Text = "Scan/Attach Document"
Me.lnkAttachEmerContact1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblEAddressapproval1
'
Me.lblEAddressapproval1.BackColor = System.Drawing.Color.PowderBlue
Me.lblEAddressapproval1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEAddressapproval1.ForeColor = System.Drawing.Color.Black
Me.lblEAddressapproval1.Location = New System.Drawing.Point(368, 0)
Me.lblEAddressapproval1.Name = "lblEAddressapproval1"
Me.lblEAddressapproval1.Size = New System.Drawing.Size(165, 16)
Me.lblEAddressapproval1.TabIndex = 216
Me.lblEAddressapproval1.Text = "Pending Approval"
Me.lblEAddressapproval1.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblEAddressapproval1.Visible = false
'
'txtEmgEmail
'
Me.txtEmgEmail.Flags = 0
Me.txtEmgEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEmail.Location = New System.Drawing.Point(627, 145)
Me.txtEmgEmail.Name = "txtEmgEmail"
Me.txtEmgEmail.Size = New System.Drawing.Size(178, 21)
Me.txtEmgEmail.TabIndex = 146
'
'lblEmgEmail
'
Me.lblEmgEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEmail.Location = New System.Drawing.Point(548, 148)
Me.lblEmgEmail.Name = "lblEmgEmail"
Me.lblEmgEmail.Size = New System.Drawing.Size(73, 15)
Me.lblEmgEmail.TabIndex = 145
Me.lblEmgEmail.Text = "Email"
Me.lblEmgEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgFax
'
Me.txtEmgFax.Flags = 0
Me.txtEmgFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFax.Location = New System.Drawing.Point(470, 145)
Me.txtEmgFax.Name = "txtEmgFax"
Me.txtEmgFax.Size = New System.Drawing.Size(72, 21)
Me.txtEmgFax.TabIndex = 144
'
'lblEmgFax
'
Me.lblEmgFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFax.Location = New System.Drawing.Point(433, 148)
Me.lblEmgFax.Name = "lblEmgFax"
Me.lblEmgFax.Size = New System.Drawing.Size(31, 15)
Me.lblEmgFax.TabIndex = 143
Me.lblEmgFax.Text = "Fax"
Me.lblEmgFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgFirstName
'
Me.txtEmgFirstName.BackColor = System.Drawing.SystemColors.Window
Me.txtEmgFirstName.Flags = 0
Me.txtEmgFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFirstName.Location = New System.Drawing.Point(85, 37)
Me.txtEmgFirstName.Name = "txtEmgFirstName"
Me.txtEmgFirstName.Size = New System.Drawing.Size(187, 21)
Me.txtEmgFirstName.TabIndex = 116
'
'txtEmgAltNo
'
Me.txtEmgAltNo.Flags = 0
Me.txtEmgAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAltNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAltNo.Location = New System.Drawing.Point(85, 145)
Me.txtEmgAltNo.Name = "txtEmgAltNo"
Me.txtEmgAltNo.Size = New System.Drawing.Size(187, 21)
Me.txtEmgAltNo.TabIndex = 140
'
'txtEmgTelNo
'
Me.txtEmgTelNo.Flags = 0
Me.txtEmgTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgTelNo.Location = New System.Drawing.Point(355, 145)
Me.txtEmgTelNo.Name = "txtEmgTelNo"
Me.txtEmgTelNo.Size = New System.Drawing.Size(72, 21)
Me.txtEmgTelNo.TabIndex = 142
'
'lblEmgTelNo
'
Me.lblEmgTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgTelNo.Location = New System.Drawing.Point(278, 148)
Me.lblEmgTelNo.Name = "lblEmgTelNo"
Me.lblEmgTelNo.Size = New System.Drawing.Size(71, 15)
Me.lblEmgTelNo.TabIndex = 141
Me.lblEmgTelNo.Text = "Tel. No"
Me.lblEmgTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAltNo
'
Me.lblEmgAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAltNo.Location = New System.Drawing.Point(5, 148)
Me.lblEmgAltNo.Name = "lblEmgAltNo"
Me.lblEmgAltNo.Size = New System.Drawing.Size(74, 15)
Me.lblEmgAltNo.TabIndex = 139
Me.lblEmgAltNo.Text = "Alt. No"
Me.lblEmgAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgFirstName
'
Me.lblEmgFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFirstName.Location = New System.Drawing.Point(5, 40)
Me.lblEmgFirstName.Name = "lblEmgFirstName"
Me.lblEmgFirstName.Size = New System.Drawing.Size(74, 15)
Me.lblEmgFirstName.TabIndex = 115
Me.lblEmgFirstName.Text = "First Name"
Me.lblEmgFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgLastName
'
Me.txtEmgLastName.Flags = 0
Me.txtEmgLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgLastName.Location = New System.Drawing.Point(355, 37)
Me.txtEmgLastName.Name = "txtEmgLastName"
Me.txtEmgLastName.Size = New System.Drawing.Size(187, 21)
Me.txtEmgLastName.TabIndex = 118
'
'txtEmgPlotNo
'
Me.txtEmgPlotNo.Flags = 0
Me.txtEmgPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgPlotNo.Location = New System.Drawing.Point(355, 118)
Me.txtEmgPlotNo.Name = "txtEmgPlotNo"
Me.txtEmgPlotNo.Size = New System.Drawing.Size(187, 21)
Me.txtEmgPlotNo.TabIndex = 136
'
'txtEmgMobile
'
Me.txtEmgMobile.Flags = 0
Me.txtEmgMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgMobile.Location = New System.Drawing.Point(627, 118)
Me.txtEmgMobile.Name = "txtEmgMobile"
Me.txtEmgMobile.Size = New System.Drawing.Size(178, 21)
Me.txtEmgMobile.TabIndex = 138
'
'lblEmgMobile
'
Me.lblEmgMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgMobile.Location = New System.Drawing.Point(548, 121)
Me.lblEmgMobile.Name = "lblEmgMobile"
Me.lblEmgMobile.Size = New System.Drawing.Size(73, 15)
Me.lblEmgMobile.TabIndex = 137
Me.lblEmgMobile.Text = "Mobile"
Me.lblEmgMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgLastName
'
Me.lblEmgLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgLastName.Location = New System.Drawing.Point(278, 40)
Me.lblEmgLastName.Name = "lblEmgLastName"
Me.lblEmgLastName.Size = New System.Drawing.Size(71, 15)
Me.lblEmgLastName.TabIndex = 117
Me.lblEmgLastName.Text = "Last Name"
Me.lblEmgLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPlotNo
'
Me.lblEmgPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPlotNo.Location = New System.Drawing.Point(278, 121)
Me.lblEmgPlotNo.Name = "lblEmgPlotNo"
Me.lblEmgPlotNo.Size = New System.Drawing.Size(71, 15)
Me.lblEmgPlotNo.TabIndex = 135
Me.lblEmgPlotNo.Text = "Plot No"
Me.lblEmgPlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgAddress
'
Me.txtEmgAddress.Flags = 0
Me.txtEmgAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAddress.Location = New System.Drawing.Point(627, 37)
Me.txtEmgAddress.Name = "txtEmgAddress"
Me.txtEmgAddress.Size = New System.Drawing.Size(178, 21)
Me.txtEmgAddress.TabIndex = 120
'
'txtEmgProvince
'
Me.txtEmgProvince.Flags = 0
Me.txtEmgProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgProvince.Location = New System.Drawing.Point(355, 91)
Me.txtEmgProvince.Name = "txtEmgProvince"
Me.txtEmgProvince.Size = New System.Drawing.Size(187, 21)
Me.txtEmgProvince.TabIndex = 130
'
'lblEmgProvince
'
Me.lblEmgProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgProvince.Location = New System.Drawing.Point(278, 94)
Me.lblEmgProvince.Name = "lblEmgProvince"
Me.lblEmgProvince.Size = New System.Drawing.Size(71, 15)
Me.lblEmgProvince.TabIndex = 129
Me.lblEmgProvince.Text = "Prov/Region"
Me.lblEmgProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAddress
'
Me.lblEmgAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAddress.Location = New System.Drawing.Point(548, 40)
Me.lblEmgAddress.Name = "lblEmgAddress"
Me.lblEmgAddress.Size = New System.Drawing.Size(73, 15)
Me.lblEmgAddress.TabIndex = 119
Me.lblEmgAddress.Text = "Address"
Me.lblEmgAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgCountry
'
Me.cboEmgCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgCountry.FormattingEnabled = true
Me.cboEmgCountry.Location = New System.Drawing.Point(85, 64)
Me.cboEmgCountry.Name = "cboEmgCountry"
Me.cboEmgCountry.Size = New System.Drawing.Size(187, 21)
Me.cboEmgCountry.TabIndex = 122
'
'lblEmgPostCountry
'
Me.lblEmgPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCountry.Location = New System.Drawing.Point(5, 67)
Me.lblEmgPostCountry.Name = "lblEmgPostCountry"
Me.lblEmgPostCountry.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCountry.TabIndex = 121
Me.lblEmgPostCountry.Text = "Post Country"
Me.lblEmgPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgEstate
'
Me.txtEmgEstate.Flags = 0
Me.txtEmgEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEstate.Location = New System.Drawing.Point(85, 118)
Me.txtEmgEstate.Name = "txtEmgEstate"
Me.txtEmgEstate.Size = New System.Drawing.Size(187, 21)
Me.txtEmgEstate.TabIndex = 134
'
'cboEmgState
'
Me.cboEmgState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgState.FormattingEnabled = true
Me.cboEmgState.Location = New System.Drawing.Point(355, 64)
Me.cboEmgState.Name = "cboEmgState"
Me.cboEmgState.Size = New System.Drawing.Size(187, 21)
Me.cboEmgState.TabIndex = 124
'
'lblEmgEstate
'
Me.lblEmgEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEstate.Location = New System.Drawing.Point(5, 121)
Me.lblEmgEstate.Name = "lblEmgEstate"
Me.lblEmgEstate.Size = New System.Drawing.Size(74, 15)
Me.lblEmgEstate.TabIndex = 133
Me.lblEmgEstate.Text = "Estate"
Me.lblEmgEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPostTown
'
Me.lblEmgPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostTown.Location = New System.Drawing.Point(548, 67)
Me.lblEmgPostTown.Name = "lblEmgPostTown"
Me.lblEmgPostTown.Size = New System.Drawing.Size(73, 15)
Me.lblEmgPostTown.TabIndex = 125
Me.lblEmgPostTown.Text = "Post Town"
Me.lblEmgPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgState
'
Me.lblEmgState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgState.Location = New System.Drawing.Point(278, 67)
Me.lblEmgState.Name = "lblEmgState"
Me.lblEmgState.Size = New System.Drawing.Size(71, 15)
Me.lblEmgState.TabIndex = 123
Me.lblEmgState.Text = "State"
Me.lblEmgState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgTown
'
Me.cboEmgTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgTown.FormattingEnabled = true
Me.cboEmgTown.Location = New System.Drawing.Point(627, 64)
Me.cboEmgTown.Name = "cboEmgTown"
Me.cboEmgTown.Size = New System.Drawing.Size(178, 21)
Me.cboEmgTown.TabIndex = 126
'
'txtEmgRoad
'
Me.txtEmgRoad.Flags = 0
Me.txtEmgRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgRoad.Location = New System.Drawing.Point(627, 91)
Me.txtEmgRoad.Name = "txtEmgRoad"
Me.txtEmgRoad.Size = New System.Drawing.Size(178, 21)
Me.txtEmgRoad.TabIndex = 132
'
'cboEmgPostCode
'
Me.cboEmgPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgPostCode.FormattingEnabled = true
Me.cboEmgPostCode.Location = New System.Drawing.Point(85, 91)
Me.cboEmgPostCode.Name = "cboEmgPostCode"
Me.cboEmgPostCode.Size = New System.Drawing.Size(187, 21)
Me.cboEmgPostCode.TabIndex = 128
'
'lblEmgPostCode
'
Me.lblEmgPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCode.Location = New System.Drawing.Point(5, 94)
Me.lblEmgPostCode.Name = "lblEmgPostCode"
Me.lblEmgPostCode.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCode.TabIndex = 127
Me.lblEmgPostCode.Text = "Post Code"
Me.lblEmgPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgRoad
'
Me.lblEmgRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgRoad.Location = New System.Drawing.Point(548, 94)
Me.lblEmgRoad.Name = "lblEmgRoad"
Me.lblEmgRoad.Size = New System.Drawing.Size(73, 15)
Me.lblEmgRoad.TabIndex = 131
Me.lblEmgRoad.Text = "Road/Street"
Me.lblEmgRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'GbEmergencyContact2
'
Me.GbEmergencyContact2.Controls.Add(Me.lnkAttachEmerContact2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEAddressapproval2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgEmail2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgEmail2)
Me.GbEmergencyContact2.Controls.Add(Me.lnkCopyEAddress2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgFax2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgFax2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgFirstName2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgAltNo2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgTelNo2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgTelNo2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgAltNo2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgFirstName2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgLastName2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgPlotNo2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgMobile2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgMobile2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgLastName2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgPlotNo2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgAddress2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgProvince2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgProvince2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgAddress2)
Me.GbEmergencyContact2.Controls.Add(Me.cboEmgCountry2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgPostCountry2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgEstate2)
Me.GbEmergencyContact2.Controls.Add(Me.cboEmgState2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgEstate2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgPostTown2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgState2)
Me.GbEmergencyContact2.Controls.Add(Me.cboEmgTown2)
Me.GbEmergencyContact2.Controls.Add(Me.txtEmgRoad2)
Me.GbEmergencyContact2.Controls.Add(Me.cboEmgPostCode2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgPostCode2)
Me.GbEmergencyContact2.Controls.Add(Me.lblEmgRoad2)
Me.GbEmergencyContact2.Location = New System.Drawing.Point(6, 194)
Me.GbEmergencyContact2.Name = "GbEmergencyContact2"
Me.GbEmergencyContact2.Size = New System.Drawing.Size(811, 182)
Me.GbEmergencyContact2.TabIndex = 184
Me.GbEmergencyContact2.TabStop = false
Me.GbEmergencyContact2.Text = "Emergency Contact 2 "
'
'lnkAttachEmerContact2
'
Me.lnkAttachEmerContact2.BackColor = System.Drawing.Color.White
Me.lnkAttachEmerContact2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachEmerContact2.Location = New System.Drawing.Point(637, 17)
Me.lnkAttachEmerContact2.Name = "lnkAttachEmerContact2"
Me.lnkAttachEmerContact2.Size = New System.Drawing.Size(168, 17)
Me.lnkAttachEmerContact2.TabIndex = 218
Me.lnkAttachEmerContact2.TabStop = true
Me.lnkAttachEmerContact2.Text = "Scan/Attach Document"
Me.lnkAttachEmerContact2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblEAddressapproval2
'
Me.lblEAddressapproval2.BackColor = System.Drawing.Color.PowderBlue
Me.lblEAddressapproval2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEAddressapproval2.ForeColor = System.Drawing.Color.Black
Me.lblEAddressapproval2.Location = New System.Drawing.Point(368, 0)
Me.lblEAddressapproval2.Name = "lblEAddressapproval2"
Me.lblEAddressapproval2.Size = New System.Drawing.Size(165, 16)
Me.lblEAddressapproval2.TabIndex = 215
Me.lblEAddressapproval2.Text = "Pending Approval"
Me.lblEAddressapproval2.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblEAddressapproval2.Visible = false
'
'txtEmgEmail2
'
Me.txtEmgEmail2.Flags = 0
Me.txtEmgEmail2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEmail2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEmail2.Location = New System.Drawing.Point(627, 148)
Me.txtEmgEmail2.Name = "txtEmgEmail2"
Me.txtEmgEmail2.Size = New System.Drawing.Size(178, 21)
Me.txtEmgEmail2.TabIndex = 179
'
'lblEmgEmail2
'
Me.lblEmgEmail2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEmail2.Location = New System.Drawing.Point(548, 151)
Me.lblEmgEmail2.Name = "lblEmgEmail2"
Me.lblEmgEmail2.Size = New System.Drawing.Size(73, 15)
Me.lblEmgEmail2.TabIndex = 178
Me.lblEmgEmail2.Text = "Email"
Me.lblEmgEmail2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkCopyEAddress2
'
Me.lnkCopyEAddress2.BackColor = System.Drawing.Color.White
Me.lnkCopyEAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkCopyEAddress2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkCopyEAddress2.Location = New System.Drawing.Point(729, 0)
Me.lnkCopyEAddress2.Name = "lnkCopyEAddress2"
Me.lnkCopyEAddress2.Size = New System.Drawing.Size(74, 13)
Me.lnkCopyEAddress2.TabIndex = 148
Me.lnkCopyEAddress2.TabStop = true
Me.lnkCopyEAddress2.Text = "Copy Address"
'
'txtEmgFax2
'
Me.txtEmgFax2.Flags = 0
Me.txtEmgFax2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFax2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFax2.Location = New System.Drawing.Point(470, 148)
Me.txtEmgFax2.Name = "txtEmgFax2"
Me.txtEmgFax2.Size = New System.Drawing.Size(72, 21)
Me.txtEmgFax2.TabIndex = 177
'
'lblEmgFax2
'
Me.lblEmgFax2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFax2.Location = New System.Drawing.Point(433, 151)
Me.lblEmgFax2.Name = "lblEmgFax2"
Me.lblEmgFax2.Size = New System.Drawing.Size(31, 15)
Me.lblEmgFax2.TabIndex = 176
Me.lblEmgFax2.Text = "Fax"
Me.lblEmgFax2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgFirstName2
'
Me.txtEmgFirstName2.BackColor = System.Drawing.SystemColors.Window
Me.txtEmgFirstName2.Flags = 0
Me.txtEmgFirstName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFirstName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFirstName2.Location = New System.Drawing.Point(85, 40)
Me.txtEmgFirstName2.Name = "txtEmgFirstName2"
Me.txtEmgFirstName2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgFirstName2.TabIndex = 149
'
'txtEmgAltNo2
'
Me.txtEmgAltNo2.Flags = 0
Me.txtEmgAltNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAltNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAltNo2.Location = New System.Drawing.Point(85, 148)
Me.txtEmgAltNo2.Name = "txtEmgAltNo2"
Me.txtEmgAltNo2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgAltNo2.TabIndex = 173
'
'txtEmgTelNo2
'
Me.txtEmgTelNo2.Flags = 0
Me.txtEmgTelNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgTelNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgTelNo2.Location = New System.Drawing.Point(355, 148)
Me.txtEmgTelNo2.Name = "txtEmgTelNo2"
Me.txtEmgTelNo2.Size = New System.Drawing.Size(72, 21)
Me.txtEmgTelNo2.TabIndex = 175
'
'lblEmgTelNo2
'
Me.lblEmgTelNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgTelNo2.Location = New System.Drawing.Point(278, 151)
Me.lblEmgTelNo2.Name = "lblEmgTelNo2"
Me.lblEmgTelNo2.Size = New System.Drawing.Size(71, 15)
Me.lblEmgTelNo2.TabIndex = 174
Me.lblEmgTelNo2.Text = "Tel. No"
Me.lblEmgTelNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAltNo2
'
Me.lblEmgAltNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAltNo2.Location = New System.Drawing.Point(5, 151)
Me.lblEmgAltNo2.Name = "lblEmgAltNo2"
Me.lblEmgAltNo2.Size = New System.Drawing.Size(74, 15)
Me.lblEmgAltNo2.TabIndex = 172
Me.lblEmgAltNo2.Text = "Alt. No"
Me.lblEmgAltNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgFirstName2
'
Me.lblEmgFirstName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFirstName2.Location = New System.Drawing.Point(5, 43)
Me.lblEmgFirstName2.Name = "lblEmgFirstName2"
Me.lblEmgFirstName2.Size = New System.Drawing.Size(74, 15)
Me.lblEmgFirstName2.TabIndex = 148
Me.lblEmgFirstName2.Text = "First Name"
Me.lblEmgFirstName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgLastName2
'
Me.txtEmgLastName2.Flags = 0
Me.txtEmgLastName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgLastName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgLastName2.Location = New System.Drawing.Point(355, 40)
Me.txtEmgLastName2.Name = "txtEmgLastName2"
Me.txtEmgLastName2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgLastName2.TabIndex = 151
'
'txtEmgPlotNo2
'
Me.txtEmgPlotNo2.Flags = 0
Me.txtEmgPlotNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgPlotNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgPlotNo2.Location = New System.Drawing.Point(355, 121)
Me.txtEmgPlotNo2.Name = "txtEmgPlotNo2"
Me.txtEmgPlotNo2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgPlotNo2.TabIndex = 169
'
'txtEmgMobile2
'
Me.txtEmgMobile2.Flags = 0
Me.txtEmgMobile2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgMobile2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgMobile2.Location = New System.Drawing.Point(627, 121)
Me.txtEmgMobile2.Name = "txtEmgMobile2"
Me.txtEmgMobile2.Size = New System.Drawing.Size(178, 21)
Me.txtEmgMobile2.TabIndex = 171
'
'lblEmgMobile2
'
Me.lblEmgMobile2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgMobile2.Location = New System.Drawing.Point(548, 124)
Me.lblEmgMobile2.Name = "lblEmgMobile2"
Me.lblEmgMobile2.Size = New System.Drawing.Size(73, 15)
Me.lblEmgMobile2.TabIndex = 170
Me.lblEmgMobile2.Text = "Mobile"
Me.lblEmgMobile2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgLastName2
'
Me.lblEmgLastName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgLastName2.Location = New System.Drawing.Point(278, 43)
Me.lblEmgLastName2.Name = "lblEmgLastName2"
Me.lblEmgLastName2.Size = New System.Drawing.Size(71, 15)
Me.lblEmgLastName2.TabIndex = 150
Me.lblEmgLastName2.Text = "Last Name"
Me.lblEmgLastName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPlotNo2
'
Me.lblEmgPlotNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPlotNo2.Location = New System.Drawing.Point(278, 124)
Me.lblEmgPlotNo2.Name = "lblEmgPlotNo2"
Me.lblEmgPlotNo2.Size = New System.Drawing.Size(71, 15)
Me.lblEmgPlotNo2.TabIndex = 168
Me.lblEmgPlotNo2.Text = "Plot No"
Me.lblEmgPlotNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgAddress2
'
Me.txtEmgAddress2.Flags = 0
Me.txtEmgAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAddress2.Location = New System.Drawing.Point(627, 40)
Me.txtEmgAddress2.Name = "txtEmgAddress2"
Me.txtEmgAddress2.Size = New System.Drawing.Size(178, 21)
Me.txtEmgAddress2.TabIndex = 153
'
'txtEmgProvince2
'
Me.txtEmgProvince2.Flags = 0
Me.txtEmgProvince2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgProvince2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgProvince2.Location = New System.Drawing.Point(355, 94)
Me.txtEmgProvince2.Name = "txtEmgProvince2"
Me.txtEmgProvince2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgProvince2.TabIndex = 163
'
'lblEmgProvince2
'
Me.lblEmgProvince2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgProvince2.Location = New System.Drawing.Point(278, 97)
Me.lblEmgProvince2.Name = "lblEmgProvince2"
Me.lblEmgProvince2.Size = New System.Drawing.Size(71, 15)
Me.lblEmgProvince2.TabIndex = 162
Me.lblEmgProvince2.Text = "Prov/Region"
Me.lblEmgProvince2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAddress2
'
Me.lblEmgAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAddress2.Location = New System.Drawing.Point(548, 43)
Me.lblEmgAddress2.Name = "lblEmgAddress2"
Me.lblEmgAddress2.Size = New System.Drawing.Size(73, 15)
Me.lblEmgAddress2.TabIndex = 152
Me.lblEmgAddress2.Text = "Address"
Me.lblEmgAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgCountry2
'
Me.cboEmgCountry2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgCountry2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgCountry2.FormattingEnabled = true
Me.cboEmgCountry2.Location = New System.Drawing.Point(85, 67)
Me.cboEmgCountry2.Name = "cboEmgCountry2"
Me.cboEmgCountry2.Size = New System.Drawing.Size(187, 21)
Me.cboEmgCountry2.TabIndex = 155
'
'lblEmgPostCountry2
'
Me.lblEmgPostCountry2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCountry2.Location = New System.Drawing.Point(5, 70)
Me.lblEmgPostCountry2.Name = "lblEmgPostCountry2"
Me.lblEmgPostCountry2.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCountry2.TabIndex = 154
Me.lblEmgPostCountry2.Text = "Post Country"
Me.lblEmgPostCountry2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgEstate2
'
Me.txtEmgEstate2.Flags = 0
Me.txtEmgEstate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEstate2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEstate2.Location = New System.Drawing.Point(85, 121)
Me.txtEmgEstate2.Name = "txtEmgEstate2"
Me.txtEmgEstate2.Size = New System.Drawing.Size(187, 21)
Me.txtEmgEstate2.TabIndex = 167
'
'cboEmgState2
'
Me.cboEmgState2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgState2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgState2.FormattingEnabled = true
Me.cboEmgState2.Location = New System.Drawing.Point(355, 67)
Me.cboEmgState2.Name = "cboEmgState2"
Me.cboEmgState2.Size = New System.Drawing.Size(187, 21)
Me.cboEmgState2.TabIndex = 157
'
'lblEmgEstate2
'
Me.lblEmgEstate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEstate2.Location = New System.Drawing.Point(5, 124)
Me.lblEmgEstate2.Name = "lblEmgEstate2"
Me.lblEmgEstate2.Size = New System.Drawing.Size(74, 15)
Me.lblEmgEstate2.TabIndex = 166
Me.lblEmgEstate2.Text = "Estate"
Me.lblEmgEstate2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPostTown2
'
Me.lblEmgPostTown2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostTown2.Location = New System.Drawing.Point(548, 70)
Me.lblEmgPostTown2.Name = "lblEmgPostTown2"
Me.lblEmgPostTown2.Size = New System.Drawing.Size(73, 15)
Me.lblEmgPostTown2.TabIndex = 158
Me.lblEmgPostTown2.Text = "Post Town"
Me.lblEmgPostTown2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgState2
'
Me.lblEmgState2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgState2.Location = New System.Drawing.Point(278, 70)
Me.lblEmgState2.Name = "lblEmgState2"
Me.lblEmgState2.Size = New System.Drawing.Size(71, 15)
Me.lblEmgState2.TabIndex = 156
Me.lblEmgState2.Text = "State"
Me.lblEmgState2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgTown2
'
Me.cboEmgTown2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgTown2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgTown2.FormattingEnabled = true
Me.cboEmgTown2.Location = New System.Drawing.Point(627, 67)
Me.cboEmgTown2.Name = "cboEmgTown2"
Me.cboEmgTown2.Size = New System.Drawing.Size(178, 21)
Me.cboEmgTown2.TabIndex = 159
'
'txtEmgRoad2
'
Me.txtEmgRoad2.Flags = 0
Me.txtEmgRoad2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgRoad2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgRoad2.Location = New System.Drawing.Point(627, 94)
Me.txtEmgRoad2.Name = "txtEmgRoad2"
Me.txtEmgRoad2.Size = New System.Drawing.Size(178, 21)
Me.txtEmgRoad2.TabIndex = 165
'
'cboEmgPostCode2
'
Me.cboEmgPostCode2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgPostCode2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgPostCode2.FormattingEnabled = true
Me.cboEmgPostCode2.Location = New System.Drawing.Point(85, 94)
Me.cboEmgPostCode2.Name = "cboEmgPostCode2"
Me.cboEmgPostCode2.Size = New System.Drawing.Size(187, 21)
Me.cboEmgPostCode2.TabIndex = 161
'
'lblEmgPostCode2
'
Me.lblEmgPostCode2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCode2.Location = New System.Drawing.Point(5, 97)
Me.lblEmgPostCode2.Name = "lblEmgPostCode2"
Me.lblEmgPostCode2.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCode2.TabIndex = 160
Me.lblEmgPostCode2.Text = "Post Code"
Me.lblEmgPostCode2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgRoad2
'
Me.lblEmgRoad2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgRoad2.Location = New System.Drawing.Point(548, 97)
Me.lblEmgRoad2.Name = "lblEmgRoad2"
Me.lblEmgRoad2.Size = New System.Drawing.Size(73, 15)
Me.lblEmgRoad2.TabIndex = 164
Me.lblEmgRoad2.Text = "Road/Street"
Me.lblEmgRoad2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'GbEmergencyContact3
'
Me.GbEmergencyContact3.Controls.Add(Me.lnkAttachEmerContact3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEAddressapproval3)
Me.GbEmergencyContact3.Controls.Add(Me.lnkCopyEAddress3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgEmail3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgEmail3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgFax3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgFax3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgFirstName3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgAltNo3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgTelNo3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgTelNo3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgAltNo3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgFirstName3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgLastName3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgPlotNo3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgMobile3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgMobile3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgLastName3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgPlotNo3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgAddress3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgProvince3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgProvince3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgAddress3)
Me.GbEmergencyContact3.Controls.Add(Me.cboEmgCountry3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgPostCountry3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgEstate3)
Me.GbEmergencyContact3.Controls.Add(Me.cboEmgState3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgEstate3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgPostTown3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgState3)
Me.GbEmergencyContact3.Controls.Add(Me.cboEmgTown3)
Me.GbEmergencyContact3.Controls.Add(Me.txtEmgRoad3)
Me.GbEmergencyContact3.Controls.Add(Me.cboEmgPostCode3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgPostCode3)
Me.GbEmergencyContact3.Controls.Add(Me.lblEmgRoad3)
Me.GbEmergencyContact3.Location = New System.Drawing.Point(6, 382)
Me.GbEmergencyContact3.Name = "GbEmergencyContact3"
Me.GbEmergencyContact3.Size = New System.Drawing.Size(811, 176)
Me.GbEmergencyContact3.TabIndex = 183
Me.GbEmergencyContact3.TabStop = false
Me.GbEmergencyContact3.Text = "Emergency Contact 3 "
'
'lnkAttachEmerContact3
'
Me.lnkAttachEmerContact3.BackColor = System.Drawing.Color.White
Me.lnkAttachEmerContact3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkAttachEmerContact3.Location = New System.Drawing.Point(637, 17)
Me.lnkAttachEmerContact3.Name = "lnkAttachEmerContact3"
Me.lnkAttachEmerContact3.Size = New System.Drawing.Size(168, 17)
Me.lnkAttachEmerContact3.TabIndex = 219
Me.lnkAttachEmerContact3.TabStop = true
Me.lnkAttachEmerContact3.Text = "Scan/Attach Document"
Me.lnkAttachEmerContact3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblEAddressapproval3
'
Me.lblEAddressapproval3.BackColor = System.Drawing.Color.PowderBlue
Me.lblEAddressapproval3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEAddressapproval3.ForeColor = System.Drawing.Color.Black
Me.lblEAddressapproval3.Location = New System.Drawing.Point(368, 0)
Me.lblEAddressapproval3.Name = "lblEAddressapproval3"
Me.lblEAddressapproval3.Size = New System.Drawing.Size(165, 16)
Me.lblEAddressapproval3.TabIndex = 214
Me.lblEAddressapproval3.Text = "Pending Approval"
Me.lblEAddressapproval3.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblEAddressapproval3.Visible = false
'
'lnkCopyEAddress3
'
Me.lnkCopyEAddress3.BackColor = System.Drawing.Color.White
Me.lnkCopyEAddress3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lnkCopyEAddress3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkCopyEAddress3.Location = New System.Drawing.Point(729, 0)
Me.lnkCopyEAddress3.Name = "lnkCopyEAddress3"
Me.lnkCopyEAddress3.Size = New System.Drawing.Size(74, 13)
Me.lnkCopyEAddress3.TabIndex = 182
Me.lnkCopyEAddress3.TabStop = true
Me.lnkCopyEAddress3.Text = "Copy Address"
'
'txtEmgEmail3
'
Me.txtEmgEmail3.Flags = 0
Me.txtEmgEmail3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEmail3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEmail3.Location = New System.Drawing.Point(628, 146)
Me.txtEmgEmail3.Name = "txtEmgEmail3"
Me.txtEmgEmail3.Size = New System.Drawing.Size(178, 21)
Me.txtEmgEmail3.TabIndex = 213
'
'lblEmgEmail3
'
Me.lblEmgEmail3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEmail3.Location = New System.Drawing.Point(549, 149)
Me.lblEmgEmail3.Name = "lblEmgEmail3"
Me.lblEmgEmail3.Size = New System.Drawing.Size(73, 15)
Me.lblEmgEmail3.TabIndex = 212
Me.lblEmgEmail3.Text = "Email"
Me.lblEmgEmail3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgFax3
'
Me.txtEmgFax3.Flags = 0
Me.txtEmgFax3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFax3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFax3.Location = New System.Drawing.Point(471, 146)
Me.txtEmgFax3.Name = "txtEmgFax3"
Me.txtEmgFax3.Size = New System.Drawing.Size(72, 21)
Me.txtEmgFax3.TabIndex = 211
'
'lblEmgFax3
'
Me.lblEmgFax3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFax3.Location = New System.Drawing.Point(434, 149)
Me.lblEmgFax3.Name = "lblEmgFax3"
Me.lblEmgFax3.Size = New System.Drawing.Size(31, 15)
Me.lblEmgFax3.TabIndex = 210
Me.lblEmgFax3.Text = "Fax"
Me.lblEmgFax3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgFirstName3
'
Me.txtEmgFirstName3.BackColor = System.Drawing.SystemColors.Window
Me.txtEmgFirstName3.Flags = 0
Me.txtEmgFirstName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgFirstName3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgFirstName3.Location = New System.Drawing.Point(86, 38)
Me.txtEmgFirstName3.Name = "txtEmgFirstName3"
Me.txtEmgFirstName3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgFirstName3.TabIndex = 183
'
'txtEmgAltNo3
'
Me.txtEmgAltNo3.Flags = 0
Me.txtEmgAltNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAltNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAltNo3.Location = New System.Drawing.Point(86, 146)
Me.txtEmgAltNo3.Name = "txtEmgAltNo3"
Me.txtEmgAltNo3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgAltNo3.TabIndex = 207
'
'txtEmgTelNo3
'
Me.txtEmgTelNo3.Flags = 0
Me.txtEmgTelNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgTelNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgTelNo3.Location = New System.Drawing.Point(356, 146)
Me.txtEmgTelNo3.Name = "txtEmgTelNo3"
Me.txtEmgTelNo3.Size = New System.Drawing.Size(72, 21)
Me.txtEmgTelNo3.TabIndex = 209
'
'lblEmgTelNo3
'
Me.lblEmgTelNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgTelNo3.Location = New System.Drawing.Point(279, 149)
Me.lblEmgTelNo3.Name = "lblEmgTelNo3"
Me.lblEmgTelNo3.Size = New System.Drawing.Size(71, 15)
Me.lblEmgTelNo3.TabIndex = 208
Me.lblEmgTelNo3.Text = "Tel. No"
Me.lblEmgTelNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAltNo3
'
Me.lblEmgAltNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAltNo3.Location = New System.Drawing.Point(6, 149)
Me.lblEmgAltNo3.Name = "lblEmgAltNo3"
Me.lblEmgAltNo3.Size = New System.Drawing.Size(74, 15)
Me.lblEmgAltNo3.TabIndex = 206
Me.lblEmgAltNo3.Text = "Alt. No"
Me.lblEmgAltNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgFirstName3
'
Me.lblEmgFirstName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgFirstName3.Location = New System.Drawing.Point(6, 41)
Me.lblEmgFirstName3.Name = "lblEmgFirstName3"
Me.lblEmgFirstName3.Size = New System.Drawing.Size(74, 15)
Me.lblEmgFirstName3.TabIndex = 182
Me.lblEmgFirstName3.Text = "First Name"
Me.lblEmgFirstName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgLastName3
'
Me.txtEmgLastName3.Flags = 0
Me.txtEmgLastName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgLastName3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgLastName3.Location = New System.Drawing.Point(356, 38)
Me.txtEmgLastName3.Name = "txtEmgLastName3"
Me.txtEmgLastName3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgLastName3.TabIndex = 185
'
'txtEmgPlotNo3
'
Me.txtEmgPlotNo3.Flags = 0
Me.txtEmgPlotNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgPlotNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgPlotNo3.Location = New System.Drawing.Point(356, 119)
Me.txtEmgPlotNo3.Name = "txtEmgPlotNo3"
Me.txtEmgPlotNo3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgPlotNo3.TabIndex = 203
'
'txtEmgMobile3
'
Me.txtEmgMobile3.Flags = 0
Me.txtEmgMobile3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgMobile3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgMobile3.Location = New System.Drawing.Point(628, 119)
Me.txtEmgMobile3.Name = "txtEmgMobile3"
Me.txtEmgMobile3.Size = New System.Drawing.Size(178, 21)
Me.txtEmgMobile3.TabIndex = 205
'
'lblEmgMobile3
'
Me.lblEmgMobile3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgMobile3.Location = New System.Drawing.Point(549, 122)
Me.lblEmgMobile3.Name = "lblEmgMobile3"
Me.lblEmgMobile3.Size = New System.Drawing.Size(73, 15)
Me.lblEmgMobile3.TabIndex = 204
Me.lblEmgMobile3.Text = "Mobile"
Me.lblEmgMobile3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgLastName3
'
Me.lblEmgLastName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgLastName3.Location = New System.Drawing.Point(279, 41)
Me.lblEmgLastName3.Name = "lblEmgLastName3"
Me.lblEmgLastName3.Size = New System.Drawing.Size(71, 15)
Me.lblEmgLastName3.TabIndex = 184
Me.lblEmgLastName3.Text = "Last Name"
Me.lblEmgLastName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPlotNo3
'
Me.lblEmgPlotNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPlotNo3.Location = New System.Drawing.Point(279, 122)
Me.lblEmgPlotNo3.Name = "lblEmgPlotNo3"
Me.lblEmgPlotNo3.Size = New System.Drawing.Size(71, 15)
Me.lblEmgPlotNo3.TabIndex = 202
Me.lblEmgPlotNo3.Text = "Plot No"
Me.lblEmgPlotNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgAddress3
'
Me.txtEmgAddress3.Flags = 0
Me.txtEmgAddress3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgAddress3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgAddress3.Location = New System.Drawing.Point(628, 38)
Me.txtEmgAddress3.Name = "txtEmgAddress3"
Me.txtEmgAddress3.Size = New System.Drawing.Size(178, 21)
Me.txtEmgAddress3.TabIndex = 187
'
'txtEmgProvince3
'
Me.txtEmgProvince3.Flags = 0
Me.txtEmgProvince3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgProvince3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgProvince3.Location = New System.Drawing.Point(356, 92)
Me.txtEmgProvince3.Name = "txtEmgProvince3"
Me.txtEmgProvince3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgProvince3.TabIndex = 197
'
'lblEmgProvince3
'
Me.lblEmgProvince3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgProvince3.Location = New System.Drawing.Point(279, 95)
Me.lblEmgProvince3.Name = "lblEmgProvince3"
Me.lblEmgProvince3.Size = New System.Drawing.Size(71, 15)
Me.lblEmgProvince3.TabIndex = 196
Me.lblEmgProvince3.Text = "Prov/Region"
Me.lblEmgProvince3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgAddress3
'
Me.lblEmgAddress3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgAddress3.Location = New System.Drawing.Point(549, 41)
Me.lblEmgAddress3.Name = "lblEmgAddress3"
Me.lblEmgAddress3.Size = New System.Drawing.Size(73, 15)
Me.lblEmgAddress3.TabIndex = 186
Me.lblEmgAddress3.Text = "Address"
Me.lblEmgAddress3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgCountry3
'
Me.cboEmgCountry3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgCountry3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgCountry3.FormattingEnabled = true
Me.cboEmgCountry3.Location = New System.Drawing.Point(86, 65)
Me.cboEmgCountry3.Name = "cboEmgCountry3"
Me.cboEmgCountry3.Size = New System.Drawing.Size(187, 21)
Me.cboEmgCountry3.TabIndex = 189
'
'lblEmgPostCountry3
'
Me.lblEmgPostCountry3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCountry3.Location = New System.Drawing.Point(6, 68)
Me.lblEmgPostCountry3.Name = "lblEmgPostCountry3"
Me.lblEmgPostCountry3.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCountry3.TabIndex = 188
Me.lblEmgPostCountry3.Text = "Post Country"
Me.lblEmgPostCountry3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtEmgEstate3
'
Me.txtEmgEstate3.Flags = 0
Me.txtEmgEstate3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgEstate3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgEstate3.Location = New System.Drawing.Point(86, 119)
Me.txtEmgEstate3.Name = "txtEmgEstate3"
Me.txtEmgEstate3.Size = New System.Drawing.Size(187, 21)
Me.txtEmgEstate3.TabIndex = 201
'
'cboEmgState3
'
Me.cboEmgState3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgState3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgState3.FormattingEnabled = true
Me.cboEmgState3.Location = New System.Drawing.Point(356, 65)
Me.cboEmgState3.Name = "cboEmgState3"
Me.cboEmgState3.Size = New System.Drawing.Size(187, 21)
Me.cboEmgState3.TabIndex = 191
'
'lblEmgEstate3
'
Me.lblEmgEstate3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgEstate3.Location = New System.Drawing.Point(6, 122)
Me.lblEmgEstate3.Name = "lblEmgEstate3"
Me.lblEmgEstate3.Size = New System.Drawing.Size(74, 15)
Me.lblEmgEstate3.TabIndex = 200
Me.lblEmgEstate3.Text = "Estate"
Me.lblEmgEstate3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgPostTown3
'
Me.lblEmgPostTown3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostTown3.Location = New System.Drawing.Point(549, 68)
Me.lblEmgPostTown3.Name = "lblEmgPostTown3"
Me.lblEmgPostTown3.Size = New System.Drawing.Size(73, 15)
Me.lblEmgPostTown3.TabIndex = 192
Me.lblEmgPostTown3.Text = "Post Town"
Me.lblEmgPostTown3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgState3
'
Me.lblEmgState3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgState3.Location = New System.Drawing.Point(279, 68)
Me.lblEmgState3.Name = "lblEmgState3"
Me.lblEmgState3.Size = New System.Drawing.Size(71, 15)
Me.lblEmgState3.TabIndex = 190
Me.lblEmgState3.Text = "State"
Me.lblEmgState3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmgTown3
'
Me.cboEmgTown3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgTown3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgTown3.FormattingEnabled = true
Me.cboEmgTown3.Location = New System.Drawing.Point(628, 65)
Me.cboEmgTown3.Name = "cboEmgTown3"
Me.cboEmgTown3.Size = New System.Drawing.Size(178, 21)
Me.cboEmgTown3.TabIndex = 193
'
'txtEmgRoad3
'
Me.txtEmgRoad3.Flags = 0
Me.txtEmgRoad3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmgRoad3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmgRoad3.Location = New System.Drawing.Point(628, 92)
Me.txtEmgRoad3.Name = "txtEmgRoad3"
Me.txtEmgRoad3.Size = New System.Drawing.Size(178, 21)
Me.txtEmgRoad3.TabIndex = 199
'
'cboEmgPostCode3
'
Me.cboEmgPostCode3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmgPostCode3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmgPostCode3.FormattingEnabled = true
Me.cboEmgPostCode3.Location = New System.Drawing.Point(86, 92)
Me.cboEmgPostCode3.Name = "cboEmgPostCode3"
Me.cboEmgPostCode3.Size = New System.Drawing.Size(187, 21)
Me.cboEmgPostCode3.TabIndex = 195
'
'lblEmgPostCode3
'
Me.lblEmgPostCode3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgPostCode3.Location = New System.Drawing.Point(6, 95)
Me.lblEmgPostCode3.Name = "lblEmgPostCode3"
Me.lblEmgPostCode3.Size = New System.Drawing.Size(74, 15)
Me.lblEmgPostCode3.TabIndex = 194
Me.lblEmgPostCode3.Text = "Post Code"
Me.lblEmgPostCode3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblEmgRoad3
'
Me.lblEmgRoad3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmgRoad3.Location = New System.Drawing.Point(549, 95)
Me.lblEmgRoad3.Name = "lblEmgRoad3"
Me.lblEmgRoad3.Size = New System.Drawing.Size(73, 15)
Me.lblEmgRoad3.TabIndex = 198
Me.lblEmgRoad3.Text = "Road/Street"
Me.lblEmgRoad3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'tabpIdentityInfo
'
Me.tabpIdentityInfo.Controls.Add(Me.pnlIdentifyInfo)
Me.tabpIdentityInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpIdentityInfo.Name = "tabpIdentityInfo"
Me.tabpIdentityInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpIdentityInfo.Size = New System.Drawing.Size(850, 266)
Me.tabpIdentityInfo.TabIndex = 3
Me.tabpIdentityInfo.Text = "IdentityInfo"
Me.tabpIdentityInfo.UseVisualStyleBackColor = true
'
'pnlIdentifyInfo
'
Me.pnlIdentifyInfo.Controls.Add(Me.gbIDInformation)
Me.pnlIdentifyInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlIdentifyInfo.Location = New System.Drawing.Point(3, 3)
Me.pnlIdentifyInfo.Name = "pnlIdentifyInfo"
Me.pnlIdentifyInfo.Size = New System.Drawing.Size(844, 260)
Me.pnlIdentifyInfo.TabIndex = 1
'
'gbIDInformation
'
Me.gbIDInformation.BorderColor = System.Drawing.Color.Black
Me.gbIDInformation.Checked = false
Me.gbIDInformation.CollapseAllExceptThis = false
Me.gbIDInformation.CollapsedHoverImage = Nothing
Me.gbIDInformation.CollapsedNormalImage = Nothing
Me.gbIDInformation.CollapsedPressedImage = Nothing
Me.gbIDInformation.CollapseOnLoad = false
Me.gbIDInformation.Controls.Add(Me.lnkIdScanAttachDoc)
Me.gbIDInformation.Controls.Add(Me.objbtnAddIDType)
Me.gbIDInformation.Controls.Add(Me.chkMakeAsDefault)
Me.gbIDInformation.Controls.Add(Me.objLine2)
Me.gbIDInformation.Controls.Add(Me.btnDelete)
Me.gbIDInformation.Controls.Add(Me.btnEdit)
Me.gbIDInformation.Controls.Add(Me.btnAdd)
Me.gbIDInformation.Controls.Add(Me.pnlIdentityInfo)
Me.gbIDInformation.Controls.Add(Me.lblDLClass)
Me.gbIDInformation.Controls.Add(Me.txtDLClass)
Me.gbIDInformation.Controls.Add(Me.lblIdPlaceOfIssue)
Me.gbIDInformation.Controls.Add(Me.txtIDPlaceofIssue)
Me.gbIDInformation.Controls.Add(Me.dtpIdExpiryDate)
Me.gbIDInformation.Controls.Add(Me.lblIdExpiryDate)
Me.gbIDInformation.Controls.Add(Me.dtpIdIssueDate)
Me.gbIDInformation.Controls.Add(Me.lblIdIssueDate)
Me.gbIDInformation.Controls.Add(Me.lblIdIssueCountry)
Me.gbIDInformation.Controls.Add(Me.cboIdIssueCountry)
Me.gbIDInformation.Controls.Add(Me.lblIdentityNo)
Me.gbIDInformation.Controls.Add(Me.txtIdentityNo)
Me.gbIDInformation.Controls.Add(Me.txtSerialNo)
Me.gbIDInformation.Controls.Add(Me.lblIdSerialNo)
Me.gbIDInformation.Controls.Add(Me.lblIdType)
Me.gbIDInformation.Controls.Add(Me.cboIdType)
Me.gbIDInformation.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbIDInformation.ExpandedHoverImage = Nothing
Me.gbIDInformation.ExpandedNormalImage = Nothing
Me.gbIDInformation.ExpandedPressedImage = Nothing
Me.gbIDInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbIDInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbIDInformation.HeaderHeight = 25
Me.gbIDInformation.HeaderMessage = ""
Me.gbIDInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbIDInformation.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbIDInformation.HeightOnCollapse = 0
Me.gbIDInformation.LeftTextSpace = 0
Me.gbIDInformation.Location = New System.Drawing.Point(0, 0)
Me.gbIDInformation.Name = "gbIDInformation"
Me.gbIDInformation.OpenHeight = 351
Me.gbIDInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbIDInformation.ShowBorder = true
Me.gbIDInformation.ShowCheckBox = false
Me.gbIDInformation.ShowCollapseButton = false
Me.gbIDInformation.ShowDefaultBorderColor = true
Me.gbIDInformation.ShowDownButton = false
Me.gbIDInformation.ShowHeader = true
Me.gbIDInformation.Size = New System.Drawing.Size(844, 260)
Me.gbIDInformation.TabIndex = 0
Me.gbIDInformation.Temp = 0
Me.gbIDInformation.Text = "Identity Information"
Me.gbIDInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkIdScanAttachDoc
'
Me.lnkIdScanAttachDoc.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkIdScanAttachDoc.Location = New System.Drawing.Point(661, 4)
Me.lnkIdScanAttachDoc.Name = "lnkIdScanAttachDoc"
Me.lnkIdScanAttachDoc.Size = New System.Drawing.Size(178, 17)
Me.lnkIdScanAttachDoc.TabIndex = 55
Me.lnkIdScanAttachDoc.TabStop = true
Me.lnkIdScanAttachDoc.Text = "Scan/Attach Document"
Me.lnkIdScanAttachDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'objbtnAddIDType
'
Me.objbtnAddIDType.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddIDType.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddIDType.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddIDType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddIDType.BorderSelected = false
Me.objbtnAddIDType.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddIDType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddIDType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddIDType.Location = New System.Drawing.Point(236, 33)
Me.objbtnAddIDType.Name = "objbtnAddIDType"
Me.objbtnAddIDType.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddIDType.TabIndex = 53
'
'chkMakeAsDefault
'
Me.chkMakeAsDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.chkMakeAsDefault.Location = New System.Drawing.Point(708, 62)
Me.chkMakeAsDefault.Name = "chkMakeAsDefault"
Me.chkMakeAsDefault.Size = New System.Drawing.Size(121, 17)
Me.chkMakeAsDefault.TabIndex = 16
Me.chkMakeAsDefault.Text = "Mark As Default"
Me.chkMakeAsDefault.UseVisualStyleBackColor = true
'
'objLine2
'
Me.objLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objLine2.Location = New System.Drawing.Point(11, 110)
Me.objLine2.Name = "objLine2"
Me.objLine2.Size = New System.Drawing.Size(525, 7)
Me.objLine2.TabIndex = 51
Me.objLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnDelete
'
Me.btnDelete.BackColor = System.Drawing.Color.White
Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"),System.Drawing.Image)
Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDelete.BorderColor = System.Drawing.Color.Empty
Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnDelete.FlatAppearance.BorderSize = 0
Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDelete.ForeColor = System.Drawing.Color.Black
Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDelete.Location = New System.Drawing.Point(734, 87)
Me.btnDelete.Name = "btnDelete"
Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDelete.Size = New System.Drawing.Size(90, 30)
Me.btnDelete.TabIndex = 19
Me.btnDelete.Text = "&Delete"
Me.btnDelete.UseVisualStyleBackColor = true
'
'btnEdit
'
Me.btnEdit.BackColor = System.Drawing.Color.White
Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"),System.Drawing.Image)
Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEdit.BorderColor = System.Drawing.Color.Empty
Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnEdit.FlatAppearance.BorderSize = 0
Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEdit.ForeColor = System.Drawing.Color.Black
Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEdit.Location = New System.Drawing.Point(638, 87)
Me.btnEdit.Name = "btnEdit"
Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEdit.Size = New System.Drawing.Size(90, 30)
Me.btnEdit.TabIndex = 18
Me.btnEdit.Text = "&Edit"
Me.btnEdit.UseVisualStyleBackColor = true
'
'btnAdd
'
Me.btnAdd.BackColor = System.Drawing.Color.White
Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"),System.Drawing.Image)
Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAdd.BorderColor = System.Drawing.Color.Empty
Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnAdd.FlatAppearance.BorderSize = 0
Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAdd.ForeColor = System.Drawing.Color.Black
Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAdd.Location = New System.Drawing.Point(542, 87)
Me.btnAdd.Name = "btnAdd"
Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAdd.Size = New System.Drawing.Size(90, 30)
Me.btnAdd.TabIndex = 17
Me.btnAdd.Text = "&Add"
Me.btnAdd.UseVisualStyleBackColor = true
'
'pnlIdentityInfo
'
Me.pnlIdentityInfo.Controls.Add(Me.lvIdendtifyInformation)
Me.pnlIdentityInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.pnlIdentityInfo.Location = New System.Drawing.Point(11, 123)
Me.pnlIdentityInfo.Name = "pnlIdentityInfo"
Me.pnlIdentityInfo.Size = New System.Drawing.Size(813, 134)
Me.pnlIdentityInfo.TabIndex = 47
'
'lvIdendtifyInformation
'
Me.lvIdendtifyInformation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhIdType, Me.colhIdSerialNo, Me.colhIdNo, Me.colhIssueCountry, Me.colhPlaceOfIssue, Me.colhIssueDate, Me.colhExpiryDate, Me.objcolhIdTypeId, Me.objcolhCountryId, Me.colhDLClass, Me.colhDefault, Me.objcolhIDGUID, Me.colhOperationType, Me.objcolhtranguid})
Me.lvIdendtifyInformation.Dock = System.Windows.Forms.DockStyle.Fill
Me.lvIdendtifyInformation.FullRowSelect = true
Me.lvIdendtifyInformation.GridLines = true
Me.lvIdendtifyInformation.Location = New System.Drawing.Point(0, 0)
Me.lvIdendtifyInformation.Name = "lvIdendtifyInformation"
Me.lvIdendtifyInformation.Size = New System.Drawing.Size(813, 134)
Me.lvIdendtifyInformation.TabIndex = 0
Me.lvIdendtifyInformation.UseCompatibleStateImageBehavior = false
Me.lvIdendtifyInformation.View = System.Windows.Forms.View.Details
'
'colhIdType
'
Me.colhIdType.Tag = "colhIdType"
Me.colhIdType.Text = "Identity Type"
Me.colhIdType.Width = 150
'
'colhIdSerialNo
'
Me.colhIdSerialNo.Tag = "colhIdSerialNo"
Me.colhIdSerialNo.Text = "Serial No"
Me.colhIdSerialNo.Width = 115
'
'colhIdNo
'
Me.colhIdNo.Tag = "colhIdNo"
Me.colhIdNo.Text = "Identity No"
Me.colhIdNo.Width = 115
'
'colhIssueCountry
'
Me.colhIssueCountry.Tag = "colhIssueCountry"
Me.colhIssueCountry.Text = "Country"
Me.colhIssueCountry.Width = 115
'
'colhPlaceOfIssue
'
Me.colhPlaceOfIssue.Tag = "colhPlaceOfIssue"
Me.colhPlaceOfIssue.Text = "Place Of Issue"
Me.colhPlaceOfIssue.Width = 115
'
'colhIssueDate
'
Me.colhIssueDate.Tag = "colhIssueDate"
Me.colhIssueDate.Text = "Issue Date"
Me.colhIssueDate.Width = 99
'
'colhExpiryDate
'
Me.colhExpiryDate.Tag = "colhExpiryDate"
Me.colhExpiryDate.Text = "Expiry Date"
Me.colhExpiryDate.Width = 100
'
'objcolhIdTypeId
'
Me.objcolhIdTypeId.Tag = "objcolhIdTypeId"
Me.objcolhIdTypeId.Width = 0
'
'objcolhCountryId
'
Me.objcolhCountryId.Tag = "objcolhCountryId"
Me.objcolhCountryId.Width = 0
'
'colhDLClass
'
Me.colhDLClass.Tag = "colhDLClass"
Me.colhDLClass.Width = 0
'
'colhDefault
'
Me.colhDefault.Tag = "colhDefault"
Me.colhDefault.Width = 0
'
'objcolhIDGUID
'
Me.objcolhIDGUID.Tag = "objcolhIDGUID"
Me.objcolhIDGUID.Width = 0
'
'colhOperationType
'
Me.colhOperationType.Tag = "colhOperationType"
Me.colhOperationType.Text = "Operation Type"
Me.colhOperationType.Width = 100
'
'objcolhtranguid
'
Me.objcolhtranguid.Text = "objcolhtranguid"
Me.objcolhtranguid.Width = 0
'
'lblDLClass
'
Me.lblDLClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDLClass.Location = New System.Drawing.Point(270, 90)
Me.lblDLClass.Name = "lblDLClass"
Me.lblDLClass.Size = New System.Drawing.Size(81, 15)
Me.lblDLClass.TabIndex = 10
Me.lblDLClass.Text = "DL Class"
Me.lblDLClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtDLClass
'
Me.txtDLClass.Flags = 0
Me.txtDLClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtDLClass.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtDLClass.Location = New System.Drawing.Point(358, 87)
Me.txtDLClass.Name = "txtDLClass"
Me.txtDLClass.Size = New System.Drawing.Size(138, 21)
Me.txtDLClass.TabIndex = 11
'
'lblIdPlaceOfIssue
'
Me.lblIdPlaceOfIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdPlaceOfIssue.Location = New System.Drawing.Point(270, 63)
Me.lblIdPlaceOfIssue.Name = "lblIdPlaceOfIssue"
Me.lblIdPlaceOfIssue.Size = New System.Drawing.Size(81, 15)
Me.lblIdPlaceOfIssue.TabIndex = 8
Me.lblIdPlaceOfIssue.Text = "Place Of Issue"
Me.lblIdPlaceOfIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtIDPlaceofIssue
'
Me.txtIDPlaceofIssue.Flags = 0
Me.txtIDPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtIDPlaceofIssue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtIDPlaceofIssue.Location = New System.Drawing.Point(358, 60)
Me.txtIDPlaceofIssue.Name = "txtIDPlaceofIssue"
Me.txtIDPlaceofIssue.Size = New System.Drawing.Size(138, 21)
Me.txtIDPlaceofIssue.TabIndex = 9
'
'dtpIdExpiryDate
'
Me.dtpIdExpiryDate.Checked = false
Me.dtpIdExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpIdExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpIdExpiryDate.Location = New System.Drawing.Point(581, 60)
Me.dtpIdExpiryDate.Name = "dtpIdExpiryDate"
Me.dtpIdExpiryDate.ShowCheckBox = true
Me.dtpIdExpiryDate.Size = New System.Drawing.Size(122, 21)
Me.dtpIdExpiryDate.TabIndex = 15
'
'lblIdExpiryDate
'
Me.lblIdExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdExpiryDate.Location = New System.Drawing.Point(504, 63)
Me.lblIdExpiryDate.Name = "lblIdExpiryDate"
Me.lblIdExpiryDate.Size = New System.Drawing.Size(75, 15)
Me.lblIdExpiryDate.TabIndex = 14
Me.lblIdExpiryDate.Text = "Expiry Date"
Me.lblIdExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpIdIssueDate
'
Me.dtpIdIssueDate.Checked = false
Me.dtpIdIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpIdIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpIdIssueDate.Location = New System.Drawing.Point(581, 33)
Me.dtpIdIssueDate.Name = "dtpIdIssueDate"
Me.dtpIdIssueDate.ShowCheckBox = true
Me.dtpIdIssueDate.Size = New System.Drawing.Size(122, 21)
Me.dtpIdIssueDate.TabIndex = 13
'
'lblIdIssueDate
'
Me.lblIdIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdIssueDate.Location = New System.Drawing.Point(504, 36)
Me.lblIdIssueDate.Name = "lblIdIssueDate"
Me.lblIdIssueDate.Size = New System.Drawing.Size(75, 15)
Me.lblIdIssueDate.TabIndex = 12
Me.lblIdIssueDate.Text = "Issue Date"
Me.lblIdIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblIdIssueCountry
'
Me.lblIdIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdIssueCountry.Location = New System.Drawing.Point(8, 90)
Me.lblIdIssueCountry.Name = "lblIdIssueCountry"
Me.lblIdIssueCountry.Size = New System.Drawing.Size(89, 15)
Me.lblIdIssueCountry.TabIndex = 4
Me.lblIdIssueCountry.Text = "Issue Country"
Me.lblIdIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboIdIssueCountry
'
Me.cboIdIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboIdIssueCountry.DropDownWidth = 180
Me.cboIdIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboIdIssueCountry.FormattingEnabled = true
Me.cboIdIssueCountry.Location = New System.Drawing.Point(103, 87)
Me.cboIdIssueCountry.Name = "cboIdIssueCountry"
Me.cboIdIssueCountry.Size = New System.Drawing.Size(127, 21)
Me.cboIdIssueCountry.TabIndex = 5
'
'lblIdentityNo
'
Me.lblIdentityNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdentityNo.Location = New System.Drawing.Point(8, 63)
Me.lblIdentityNo.Name = "lblIdentityNo"
Me.lblIdentityNo.Size = New System.Drawing.Size(89, 15)
Me.lblIdentityNo.TabIndex = 2
Me.lblIdentityNo.Text = "ID No."
Me.lblIdentityNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtIdentityNo
'
Me.txtIdentityNo.Flags = 0
Me.txtIdentityNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtIdentityNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtIdentityNo.Location = New System.Drawing.Point(103, 60)
Me.txtIdentityNo.Name = "txtIdentityNo"
Me.txtIdentityNo.Size = New System.Drawing.Size(127, 21)
Me.txtIdentityNo.TabIndex = 3
'
'txtSerialNo
'
Me.txtSerialNo.Flags = 0
Me.txtSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtSerialNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtSerialNo.Location = New System.Drawing.Point(358, 33)
Me.txtSerialNo.Name = "txtSerialNo"
Me.txtSerialNo.Size = New System.Drawing.Size(138, 21)
Me.txtSerialNo.TabIndex = 7
'
'lblIdSerialNo
'
Me.lblIdSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdSerialNo.Location = New System.Drawing.Point(270, 36)
Me.lblIdSerialNo.Name = "lblIdSerialNo"
Me.lblIdSerialNo.Size = New System.Drawing.Size(81, 15)
Me.lblIdSerialNo.TabIndex = 6
Me.lblIdSerialNo.Text = "ID Serial No"
Me.lblIdSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblIdType
'
Me.lblIdType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdType.Location = New System.Drawing.Point(8, 36)
Me.lblIdType.Name = "lblIdType"
Me.lblIdType.Size = New System.Drawing.Size(89, 15)
Me.lblIdType.TabIndex = 0
Me.lblIdType.Text = "ID Type"
Me.lblIdType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboIdType
'
Me.cboIdType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboIdType.DropDownWidth = 180
Me.cboIdType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboIdType.FormattingEnabled = true
Me.cboIdType.Location = New System.Drawing.Point(103, 33)
Me.cboIdType.Name = "cboIdType"
Me.cboIdType.Size = New System.Drawing.Size(127, 21)
Me.cboIdType.TabIndex = 1
'
'tabpMembership
'
Me.tabpMembership.Controls.Add(Me.pnlMembershipinfo)
Me.tabpMembership.Location = New System.Drawing.Point(4, 22)
Me.tabpMembership.Name = "tabpMembership"
Me.tabpMembership.Padding = New System.Windows.Forms.Padding(3)
Me.tabpMembership.Size = New System.Drawing.Size(850, 266)
Me.tabpMembership.TabIndex = 4
Me.tabpMembership.Text = "Membership"
Me.tabpMembership.UseVisualStyleBackColor = true
'
'pnlMembershipinfo
'
Me.pnlMembershipinfo.Controls.Add(Me.gbMembershipInfo)
Me.pnlMembershipinfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlMembershipinfo.Location = New System.Drawing.Point(3, 3)
Me.pnlMembershipinfo.Name = "pnlMembershipinfo"
Me.pnlMembershipinfo.Size = New System.Drawing.Size(844, 260)
Me.pnlMembershipinfo.TabIndex = 1
'
'gbMembershipInfo
'
Me.gbMembershipInfo.BorderColor = System.Drawing.Color.Black
Me.gbMembershipInfo.Checked = false
Me.gbMembershipInfo.CollapseAllExceptThis = false
Me.gbMembershipInfo.CollapsedHoverImage = Nothing
Me.gbMembershipInfo.CollapsedNormalImage = Nothing
Me.gbMembershipInfo.CollapsedPressedImage = Nothing
Me.gbMembershipInfo.CollapseOnLoad = false
Me.gbMembershipInfo.Controls.Add(Me.lnkActivateMembership)
Me.gbMembershipInfo.Controls.Add(Me.Panel2)
Me.gbMembershipInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbMembershipInfo.ExpandedHoverImage = Nothing
Me.gbMembershipInfo.ExpandedNormalImage = Nothing
Me.gbMembershipInfo.ExpandedPressedImage = Nothing
Me.gbMembershipInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbMembershipInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbMembershipInfo.HeaderHeight = 25
Me.gbMembershipInfo.HeaderMessage = ""
Me.gbMembershipInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbMembershipInfo.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbMembershipInfo.HeightOnCollapse = 0
Me.gbMembershipInfo.LeftTextSpace = 0
Me.gbMembershipInfo.Location = New System.Drawing.Point(0, 0)
Me.gbMembershipInfo.Name = "gbMembershipInfo"
Me.gbMembershipInfo.OpenHeight = 351
Me.gbMembershipInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbMembershipInfo.ShowBorder = true
Me.gbMembershipInfo.ShowCheckBox = false
Me.gbMembershipInfo.ShowCollapseButton = false
Me.gbMembershipInfo.ShowDefaultBorderColor = true
Me.gbMembershipInfo.ShowDownButton = false
Me.gbMembershipInfo.ShowHeader = true
Me.gbMembershipInfo.Size = New System.Drawing.Size(844, 260)
Me.gbMembershipInfo.TabIndex = 0
Me.gbMembershipInfo.Temp = 0
Me.gbMembershipInfo.Text = "Memberships"
Me.gbMembershipInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkActivateMembership
'
Me.lnkActivateMembership.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
Me.lnkActivateMembership.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkActivateMembership.Location = New System.Drawing.Point(642, 4)
Me.lnkActivateMembership.Name = "lnkActivateMembership"
Me.lnkActivateMembership.Size = New System.Drawing.Size(192, 17)
Me.lnkActivateMembership.TabIndex = 61
Me.lnkActivateMembership.TabStop = true
Me.lnkActivateMembership.Text = "Activate Membership"
Me.lnkActivateMembership.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'Panel2
'
Me.Panel2.Controls.Add(Me.lblMembershipName)
Me.Panel2.Controls.Add(Me.objbtnAddMembership)
Me.Panel2.Controls.Add(Me.lblMembershipType)
Me.Panel2.Controls.Add(Me.objbtnAddMemCategory)
Me.Panel2.Controls.Add(Me.cboMembershipType)
Me.Panel2.Controls.Add(Me.cboMemCategory)
Me.Panel2.Controls.Add(Me.txtMembershipNo)
Me.Panel2.Controls.Add(Me.objelLine4)
Me.Panel2.Controls.Add(Me.lblMembershipRemark)
Me.Panel2.Controls.Add(Me.btnDeleteMembership)
Me.Panel2.Controls.Add(Me.lblMembershipNo)
Me.Panel2.Controls.Add(Me.btnAddMembership)
Me.Panel2.Controls.Add(Me.txtRemark)
Me.Panel2.Controls.Add(Me.lblMemIssueDate)
Me.Panel2.Controls.Add(Me.dtpMemIssueDate)
Me.Panel2.Controls.Add(Me.lblActivationDate)
Me.Panel2.Controls.Add(Me.dtpEndDate)
Me.Panel2.Controls.Add(Me.pnlMembershipDataInfo)
Me.Panel2.Controls.Add(Me.lblMemExpiryDate)
Me.Panel2.Controls.Add(Me.btnEditMembership)
Me.Panel2.Controls.Add(Me.dtpStartDate)
Me.Panel2.Location = New System.Drawing.Point(2, 26)
Me.Panel2.Name = "Panel2"
Me.Panel2.Size = New System.Drawing.Size(840, 232)
Me.Panel2.TabIndex = 59
'
'lblMembershipName
'
Me.lblMembershipName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMembershipName.Location = New System.Drawing.Point(10, 11)
Me.lblMembershipName.Name = "lblMembershipName"
Me.lblMembershipName.Size = New System.Drawing.Size(97, 16)
Me.lblMembershipName.TabIndex = 0
Me.lblMembershipName.Text = "Mem. Category"
Me.lblMembershipName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddMembership
'
Me.objbtnAddMembership.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddMembership.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddMembership.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddMembership.BorderSelected = false
Me.objbtnAddMembership.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddMembership.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddMembership.Location = New System.Drawing.Point(248, 36)
Me.objbtnAddMembership.Name = "objbtnAddMembership"
Me.objbtnAddMembership.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddMembership.TabIndex = 57
'
'lblMembershipType
'
Me.lblMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMembershipType.Location = New System.Drawing.Point(10, 38)
Me.lblMembershipType.Name = "lblMembershipType"
Me.lblMembershipType.Size = New System.Drawing.Size(97, 16)
Me.lblMembershipType.TabIndex = 2
Me.lblMembershipType.Text = "Membership "
Me.lblMembershipType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'objbtnAddMemCategory
'
Me.objbtnAddMemCategory.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddMemCategory.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddMemCategory.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddMemCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddMemCategory.BorderSelected = false
Me.objbtnAddMemCategory.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddMemCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddMemCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddMemCategory.Location = New System.Drawing.Point(248, 9)
Me.objbtnAddMemCategory.Name = "objbtnAddMemCategory"
Me.objbtnAddMemCategory.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddMemCategory.TabIndex = 56
'
'cboMembershipType
'
Me.cboMembershipType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboMembershipType.DropDownWidth = 180
Me.cboMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboMembershipType.FormattingEnabled = true
Me.cboMembershipType.Location = New System.Drawing.Point(113, 36)
Me.cboMembershipType.Name = "cboMembershipType"
Me.cboMembershipType.Size = New System.Drawing.Size(129, 21)
Me.cboMembershipType.TabIndex = 3
'
'cboMemCategory
'
Me.cboMemCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboMemCategory.DropDownWidth = 180
Me.cboMemCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboMemCategory.FormattingEnabled = true
Me.cboMemCategory.Location = New System.Drawing.Point(113, 9)
Me.cboMemCategory.Name = "cboMemCategory"
Me.cboMemCategory.Size = New System.Drawing.Size(129, 21)
Me.cboMemCategory.TabIndex = 1
'
'txtMembershipNo
'
Me.txtMembershipNo.Flags = 0
Me.txtMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtMembershipNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtMembershipNo.Location = New System.Drawing.Point(113, 63)
Me.txtMembershipNo.Name = "txtMembershipNo"
Me.txtMembershipNo.Size = New System.Drawing.Size(129, 21)
Me.txtMembershipNo.TabIndex = 5
'
'objelLine4
'
Me.objelLine4.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objelLine4.Location = New System.Drawing.Point(10, 89)
Me.objelLine4.Name = "objelLine4"
Me.objelLine4.Size = New System.Drawing.Size(525, 13)
Me.objelLine4.TabIndex = 54
Me.objelLine4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblMembershipRemark
'
Me.lblMembershipRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMembershipRemark.Location = New System.Drawing.Point(474, 11)
Me.lblMembershipRemark.Name = "lblMembershipRemark"
Me.lblMembershipRemark.Size = New System.Drawing.Size(59, 16)
Me.lblMembershipRemark.TabIndex = 12
Me.lblMembershipRemark.Text = "Remark"
Me.lblMembershipRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnDeleteMembership
'
Me.btnDeleteMembership.BackColor = System.Drawing.Color.White
Me.btnDeleteMembership.BackgroundImage = CType(resources.GetObject("btnDeleteMembership.BackgroundImage"),System.Drawing.Image)
Me.btnDeleteMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDeleteMembership.BorderColor = System.Drawing.Color.Empty
Me.btnDeleteMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnDeleteMembership.FlatAppearance.BorderSize = 0
Me.btnDeleteMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDeleteMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDeleteMembership.ForeColor = System.Drawing.Color.Black
Me.btnDeleteMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDeleteMembership.GradientForeColor = System.Drawing.Color.Black
Me.btnDeleteMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeleteMembership.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDeleteMembership.Location = New System.Drawing.Point(739, 67)
Me.btnDeleteMembership.Name = "btnDeleteMembership"
Me.btnDeleteMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeleteMembership.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDeleteMembership.Size = New System.Drawing.Size(93, 30)
Me.btnDeleteMembership.TabIndex = 16
Me.btnDeleteMembership.Text = "&Delete"
Me.btnDeleteMembership.UseVisualStyleBackColor = true
'
'lblMembershipNo
'
Me.lblMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMembershipNo.Location = New System.Drawing.Point(10, 65)
Me.lblMembershipNo.Name = "lblMembershipNo"
Me.lblMembershipNo.Size = New System.Drawing.Size(97, 16)
Me.lblMembershipNo.TabIndex = 4
Me.lblMembershipNo.Text = "Membership No."
Me.lblMembershipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnAddMembership
'
Me.btnAddMembership.BackColor = System.Drawing.Color.White
Me.btnAddMembership.BackgroundImage = CType(resources.GetObject("btnAddMembership.BackgroundImage"),System.Drawing.Image)
Me.btnAddMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAddMembership.BorderColor = System.Drawing.Color.Empty
Me.btnAddMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnAddMembership.FlatAppearance.BorderSize = 0
Me.btnAddMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAddMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAddMembership.ForeColor = System.Drawing.Color.Black
Me.btnAddMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAddMembership.GradientForeColor = System.Drawing.Color.Black
Me.btnAddMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAddMembership.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAddMembership.Location = New System.Drawing.Point(541, 67)
Me.btnAddMembership.Name = "btnAddMembership"
Me.btnAddMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAddMembership.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAddMembership.Size = New System.Drawing.Size(93, 30)
Me.btnAddMembership.TabIndex = 14
Me.btnAddMembership.Text = "&Add"
Me.btnAddMembership.UseVisualStyleBackColor = true
'
'txtRemark
'
Me.txtRemark.Flags = 0
Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtRemark.Location = New System.Drawing.Point(541, 9)
Me.txtRemark.Multiline = true
Me.txtRemark.Name = "txtRemark"
Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
Me.txtRemark.Size = New System.Drawing.Size(291, 48)
Me.txtRemark.TabIndex = 13
'
'lblMemIssueDate
'
Me.lblMemIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMemIssueDate.Location = New System.Drawing.Point(275, 11)
Me.lblMemIssueDate.Name = "lblMemIssueDate"
Me.lblMemIssueDate.Size = New System.Drawing.Size(70, 16)
Me.lblMemIssueDate.TabIndex = 6
Me.lblMemIssueDate.Text = "Issue Date"
Me.lblMemIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpMemIssueDate
'
Me.dtpMemIssueDate.Checked = false
Me.dtpMemIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpMemIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpMemIssueDate.Location = New System.Drawing.Point(347, 9)
Me.dtpMemIssueDate.Name = "dtpMemIssueDate"
Me.dtpMemIssueDate.ShowCheckBox = true
Me.dtpMemIssueDate.Size = New System.Drawing.Size(121, 21)
Me.dtpMemIssueDate.TabIndex = 7
'
'lblActivationDate
'
Me.lblActivationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblActivationDate.Location = New System.Drawing.Point(275, 38)
Me.lblActivationDate.Name = "lblActivationDate"
Me.lblActivationDate.Size = New System.Drawing.Size(70, 16)
Me.lblActivationDate.TabIndex = 8
Me.lblActivationDate.Text = "Start Date"
Me.lblActivationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'dtpEndDate
'
Me.dtpEndDate.Checked = false
Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpEndDate.Location = New System.Drawing.Point(347, 63)
Me.dtpEndDate.Name = "dtpEndDate"
Me.dtpEndDate.ShowCheckBox = true
Me.dtpEndDate.Size = New System.Drawing.Size(121, 21)
Me.dtpEndDate.TabIndex = 11
'
'pnlMembershipDataInfo
'
Me.pnlMembershipDataInfo.Controls.Add(Me.lvMembershipInfo)
Me.pnlMembershipDataInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.pnlMembershipDataInfo.Location = New System.Drawing.Point(10, 103)
Me.pnlMembershipDataInfo.Name = "pnlMembershipDataInfo"
Me.pnlMembershipDataInfo.Size = New System.Drawing.Size(822, 126)
Me.pnlMembershipDataInfo.TabIndex = 48
'
'lvMembershipInfo
'
Me.lvMembershipInfo.BackColorOnChecked = true
Me.lvMembershipInfo.ColumnHeaders = Nothing
Me.lvMembershipInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMembershipCategory, Me.colhMembershipType, Me.colhMembershipNo, Me.colhMemIssueDate, Me.colhStartDate, Me.colhEndDate, Me.colhRemark, Me.objcolhMemCatUnkid, Me.objcolMembershipUnkid, Me.objcolhMGUID, Me.objcolhccategory, Me.objcolhccategoryId, Me.colhMOperationType, Me.objcolhMtranguid})
Me.lvMembershipInfo.CompulsoryColumns = ""
Me.lvMembershipInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.lvMembershipInfo.FullRowSelect = true
Me.lvMembershipInfo.GridLines = true
Me.lvMembershipInfo.GroupingColumn = Nothing
Me.lvMembershipInfo.HideSelection = false
Me.lvMembershipInfo.Location = New System.Drawing.Point(0, 0)
Me.lvMembershipInfo.MinColumnWidth = 50
Me.lvMembershipInfo.MultiSelect = false
Me.lvMembershipInfo.Name = "lvMembershipInfo"
Me.lvMembershipInfo.OptionalColumns = ""
Me.lvMembershipInfo.ShowMoreItem = false
Me.lvMembershipInfo.ShowSaveItem = false
Me.lvMembershipInfo.ShowSelectAll = true
Me.lvMembershipInfo.ShowSizeAllColumnsToFit = true
Me.lvMembershipInfo.Size = New System.Drawing.Size(822, 126)
Me.lvMembershipInfo.Sortable = true
Me.lvMembershipInfo.TabIndex = 0
Me.lvMembershipInfo.UseCompatibleStateImageBehavior = false
Me.lvMembershipInfo.View = System.Windows.Forms.View.Details
'
'colhMembershipCategory
'
Me.colhMembershipCategory.DisplayIndex = 1
Me.colhMembershipCategory.Tag = "colhMembershipCategory"
Me.colhMembershipCategory.Text = "Membership Category"
Me.colhMembershipCategory.Width = 135
'
'colhMembershipType
'
Me.colhMembershipType.DisplayIndex = 2
Me.colhMembershipType.Tag = "colhMembershipType"
Me.colhMembershipType.Text = "Membership"
Me.colhMembershipType.Width = 125
'
'colhMembershipNo
'
Me.colhMembershipNo.DisplayIndex = 3
Me.colhMembershipNo.Tag = "colhMembershipNo"
Me.colhMembershipNo.Text = "Membership No"
Me.colhMembershipNo.Width = 105
'
'colhMemIssueDate
'
Me.colhMemIssueDate.DisplayIndex = 4
Me.colhMemIssueDate.Tag = "colhMemIssueDate"
Me.colhMemIssueDate.Text = "Issue Date"
Me.colhMemIssueDate.Width = 90
'
'colhStartDate
'
Me.colhStartDate.DisplayIndex = 5
Me.colhStartDate.Tag = "colhStartDate"
Me.colhStartDate.Text = "Start Date"
Me.colhStartDate.Width = 90
'
'colhEndDate
'
Me.colhEndDate.DisplayIndex = 6
Me.colhEndDate.Tag = "colhEndDate"
Me.colhEndDate.Text = "End Date"
Me.colhEndDate.Width = 90
'
'colhRemark
'
Me.colhRemark.DisplayIndex = 7
Me.colhRemark.Tag = "colhRemark"
Me.colhRemark.Text = "Remark"
Me.colhRemark.Width = 180
'
'objcolhMemCatUnkid
'
Me.objcolhMemCatUnkid.DisplayIndex = 8
Me.objcolhMemCatUnkid.Tag = "objcolhMemCatUnkid"
Me.objcolhMemCatUnkid.Text = ""
Me.objcolhMemCatUnkid.Width = 0
'
'objcolMembershipUnkid
'
Me.objcolMembershipUnkid.DisplayIndex = 9
Me.objcolMembershipUnkid.Tag = "objcolMembershipUnkid"
Me.objcolMembershipUnkid.Text = ""
Me.objcolMembershipUnkid.Width = 0
'
'objcolhMGUID
'
Me.objcolhMGUID.DisplayIndex = 10
Me.objcolhMGUID.Tag = "objcolhMGUID"
Me.objcolhMGUID.Text = ""
Me.objcolhMGUID.Width = 0
'
'objcolhccategory
'
Me.objcolhccategory.DisplayIndex = 11
Me.objcolhccategory.Tag = "objcolhccategory"
Me.objcolhccategory.Width = 0
'
'objcolhccategoryId
'
Me.objcolhccategoryId.DisplayIndex = 12
Me.objcolhccategoryId.Tag = "objcolhccategoryId"
Me.objcolhccategoryId.Text = ""
Me.objcolhccategoryId.Width = 0
'
'colhMOperationType
'
Me.colhMOperationType.DisplayIndex = 0
Me.colhMOperationType.Tag = "colhMOperationType"
Me.colhMOperationType.Text = "Operation Type"
'
'objcolhMtranguid
'
Me.objcolhMtranguid.Tag = "objcolhMtranguid"
Me.objcolhMtranguid.Text = "objcolhMtranguid"
Me.objcolhMtranguid.Width = 0
'
'lblMemExpiryDate
'
Me.lblMemExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMemExpiryDate.Location = New System.Drawing.Point(275, 65)
Me.lblMemExpiryDate.Name = "lblMemExpiryDate"
Me.lblMemExpiryDate.Size = New System.Drawing.Size(70, 16)
Me.lblMemExpiryDate.TabIndex = 10
Me.lblMemExpiryDate.Text = "Expiry Date"
Me.lblMemExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'btnEditMembership
'
Me.btnEditMembership.BackColor = System.Drawing.Color.White
Me.btnEditMembership.BackgroundImage = CType(resources.GetObject("btnEditMembership.BackgroundImage"),System.Drawing.Image)
Me.btnEditMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnEditMembership.BorderColor = System.Drawing.Color.Empty
Me.btnEditMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnEditMembership.FlatAppearance.BorderSize = 0
Me.btnEditMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnEditMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnEditMembership.ForeColor = System.Drawing.Color.Black
Me.btnEditMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnEditMembership.GradientForeColor = System.Drawing.Color.Black
Me.btnEditMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnEditMembership.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnEditMembership.Location = New System.Drawing.Point(640, 67)
Me.btnEditMembership.Name = "btnEditMembership"
Me.btnEditMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnEditMembership.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnEditMembership.Size = New System.Drawing.Size(93, 30)
Me.btnEditMembership.TabIndex = 15
Me.btnEditMembership.Text = "&Edit"
Me.btnEditMembership.UseVisualStyleBackColor = true
'
'dtpStartDate
'
Me.dtpStartDate.Checked = false
Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpStartDate.Location = New System.Drawing.Point(347, 36)
Me.dtpStartDate.Name = "dtpStartDate"
Me.dtpStartDate.ShowCheckBox = true
Me.dtpStartDate.Size = New System.Drawing.Size(121, 21)
Me.dtpStartDate.TabIndex = 9
'
'tabpOtherInfo
'
Me.tabpOtherInfo.Controls.Add(Me.gbOtherInfo)
Me.tabpOtherInfo.Location = New System.Drawing.Point(4, 22)
Me.tabpOtherInfo.Name = "tabpOtherInfo"
Me.tabpOtherInfo.Padding = New System.Windows.Forms.Padding(3)
Me.tabpOtherInfo.Size = New System.Drawing.Size(850, 266)
Me.tabpOtherInfo.TabIndex = 5
Me.tabpOtherInfo.Text = "Other Info."
Me.tabpOtherInfo.UseVisualStyleBackColor = true
'
'gbOtherInfo
'
Me.gbOtherInfo.BorderColor = System.Drawing.Color.Black
Me.gbOtherInfo.Checked = false
Me.gbOtherInfo.CollapseAllExceptThis = false
Me.gbOtherInfo.CollapsedHoverImage = Nothing
Me.gbOtherInfo.CollapsedNormalImage = Nothing
Me.gbOtherInfo.CollapsedPressedImage = Nothing
Me.gbOtherInfo.CollapseOnLoad = false
Me.gbOtherInfo.Controls.Add(Me.lnkIdAttachOtherInfo)
Me.gbOtherInfo.Controls.Add(Me.lblotherinfoapproval)
Me.gbOtherInfo.Controls.Add(Me.objStOtherInfo)
Me.gbOtherInfo.Controls.Add(Me.txtWeight)
Me.gbOtherInfo.Controls.Add(Me.txtHeight)
Me.gbOtherInfo.Controls.Add(Me.txtSportHobbies)
Me.gbOtherInfo.Controls.Add(Me.dtpMaritalDate)
Me.gbOtherInfo.Controls.Add(Me.lblMaritalDate)
Me.gbOtherInfo.Controls.Add(Me.lblSportsHobbies)
Me.gbOtherInfo.Controls.Add(Me.cboMaritalStatus)
Me.gbOtherInfo.Controls.Add(Me.lvDisabilities)
Me.gbOtherInfo.Controls.Add(Me.lblMaritalStatus)
Me.gbOtherInfo.Controls.Add(Me.lblDisabilities)
Me.gbOtherInfo.Controls.Add(Me.lvAllergis)
Me.gbOtherInfo.Controls.Add(Me.lblAllergies)
Me.gbOtherInfo.Controls.Add(Me.lblWeight)
Me.gbOtherInfo.Controls.Add(Me.lblHeight)
Me.gbOtherInfo.Controls.Add(Me.lblLanguage4)
Me.gbOtherInfo.Controls.Add(Me.cboLanguage4)
Me.gbOtherInfo.Controls.Add(Me.lblLanguage3)
Me.gbOtherInfo.Controls.Add(Me.cboLanguage3)
Me.gbOtherInfo.Controls.Add(Me.lblLanguage2)
Me.gbOtherInfo.Controls.Add(Me.cboLanguage2)
Me.gbOtherInfo.Controls.Add(Me.lblLanguage1)
Me.gbOtherInfo.Controls.Add(Me.cboLanguage1)
Me.gbOtherInfo.Controls.Add(Me.txtExtraTel)
Me.gbOtherInfo.Controls.Add(Me.lblExtraTel)
Me.gbOtherInfo.Controls.Add(Me.lblHair)
Me.gbOtherInfo.Controls.Add(Me.cboHair)
Me.gbOtherInfo.Controls.Add(Me.lblReligion)
Me.gbOtherInfo.Controls.Add(Me.cboReligion)
Me.gbOtherInfo.Controls.Add(Me.lblEthinCity)
Me.gbOtherInfo.Controls.Add(Me.cboEthincity)
Me.gbOtherInfo.Controls.Add(Me.lblNationality)
Me.gbOtherInfo.Controls.Add(Me.cboNationality)
Me.gbOtherInfo.Controls.Add(Me.lblEyeColor)
Me.gbOtherInfo.Controls.Add(Me.cboEyeColor)
Me.gbOtherInfo.Controls.Add(Me.lblBloodGroup)
Me.gbOtherInfo.Controls.Add(Me.cboBloodGroup)
Me.gbOtherInfo.Controls.Add(Me.lblComplexion)
Me.gbOtherInfo.Controls.Add(Me.cboComplexion)
Me.gbOtherInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbOtherInfo.ExpandedHoverImage = Nothing
Me.gbOtherInfo.ExpandedNormalImage = Nothing
Me.gbOtherInfo.ExpandedPressedImage = Nothing
Me.gbOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbOtherInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbOtherInfo.HeaderHeight = 25
Me.gbOtherInfo.HeaderMessage = ""
Me.gbOtherInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
Me.gbOtherInfo.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbOtherInfo.HeightOnCollapse = 0
Me.gbOtherInfo.LeftTextSpace = 0
Me.gbOtherInfo.Location = New System.Drawing.Point(3, 3)
Me.gbOtherInfo.Name = "gbOtherInfo"
Me.gbOtherInfo.OpenHeight = 226
Me.gbOtherInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbOtherInfo.ShowBorder = true
Me.gbOtherInfo.ShowCheckBox = false
Me.gbOtherInfo.ShowCollapseButton = false
Me.gbOtherInfo.ShowDefaultBorderColor = true
Me.gbOtherInfo.ShowDownButton = false
Me.gbOtherInfo.ShowHeader = true
Me.gbOtherInfo.Size = New System.Drawing.Size(844, 260)
Me.gbOtherInfo.TabIndex = 7
Me.gbOtherInfo.Temp = 0
Me.gbOtherInfo.Text = "Other Info"
Me.gbOtherInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lnkIdAttachOtherInfo
'
Me.lnkIdAttachOtherInfo.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
Me.lnkIdAttachOtherInfo.Location = New System.Drawing.Point(663, 4)
Me.lnkIdAttachOtherInfo.Name = "lnkIdAttachOtherInfo"
Me.lnkIdAttachOtherInfo.Size = New System.Drawing.Size(178, 17)
Me.lnkIdAttachOtherInfo.TabIndex = 219
Me.lnkIdAttachOtherInfo.TabStop = true
Me.lnkIdAttachOtherInfo.Text = "Scan/Attach Document"
Me.lnkIdAttachOtherInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
'
'lblotherinfoapproval
'
Me.lblotherinfoapproval.BackColor = System.Drawing.Color.PowderBlue
Me.lblotherinfoapproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblotherinfoapproval.ForeColor = System.Drawing.Color.Black
Me.lblotherinfoapproval.Location = New System.Drawing.Point(492, 4)
Me.lblotherinfoapproval.Name = "lblotherinfoapproval"
Me.lblotherinfoapproval.Size = New System.Drawing.Size(165, 16)
Me.lblotherinfoapproval.TabIndex = 217
Me.lblotherinfoapproval.Text = "Pending Approval"
Me.lblotherinfoapproval.TextAlign = System.Drawing.ContentAlignment.TopCenter
Me.lblotherinfoapproval.Visible = false
'
'objStOtherInfo
'
Me.objStOtherInfo.BackColor = System.Drawing.Color.Transparent
Me.objStOtherInfo.LineType = eZee.Common.StraightLineTypes.Vertical
Me.objStOtherInfo.Location = New System.Drawing.Point(403, 34)
Me.objStOtherInfo.Name = "objStOtherInfo"
Me.objStOtherInfo.Size = New System.Drawing.Size(10, 209)
Me.objStOtherInfo.TabIndex = 114
Me.objStOtherInfo.Text = "EZeeStraightLine1"
'
'txtWeight
'
Me.txtWeight.AllowNegative = true
Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
Me.txtWeight.DigitsInGroup = 0
Me.txtWeight.Flags = 0
Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtWeight.Location = New System.Drawing.Point(279, 195)
Me.txtWeight.MaxDecimalPlaces = 6
Me.txtWeight.MaxWholeDigits = 21
Me.txtWeight.Name = "txtWeight"
Me.txtWeight.Prefix = ""
Me.txtWeight.RangeMax = 1.7976931348623157E+308
Me.txtWeight.RangeMin = -1.7976931348623157E+308
Me.txtWeight.Size = New System.Drawing.Size(123, 21)
Me.txtWeight.TabIndex = 103
Me.txtWeight.Text = "0"
Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
'
'txtHeight
'
Me.txtHeight.AllowNegative = false
Me.txtHeight.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
Me.txtHeight.DigitsInGroup = 0
Me.txtHeight.Flags = 65536
Me.txtHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtHeight.Location = New System.Drawing.Point(279, 168)
Me.txtHeight.MaxDecimalPlaces = 6
Me.txtHeight.MaxWholeDigits = 21
Me.txtHeight.Name = "txtHeight"
Me.txtHeight.Prefix = ""
Me.txtHeight.RangeMax = 1.7976931348623157E+308
Me.txtHeight.RangeMin = -1.7976931348623157E+308
Me.txtHeight.Size = New System.Drawing.Size(123, 21)
Me.txtHeight.TabIndex = 101
Me.txtHeight.Text = "0"
Me.txtHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
'
'txtSportHobbies
'
Me.txtSportHobbies.Flags = 0
Me.txtSportHobbies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtSportHobbies.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtSportHobbies.Location = New System.Drawing.Point(481, 168)
Me.txtSportHobbies.Multiline = true
Me.txtSportHobbies.Name = "txtSportHobbies"
Me.txtSportHobbies.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
Me.txtSportHobbies.Size = New System.Drawing.Size(346, 75)
Me.txtSportHobbies.TabIndex = 113
'
'dtpMaritalDate
'
Me.dtpMaritalDate.Checked = false
Me.dtpMaritalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.dtpMaritalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
Me.dtpMaritalDate.Location = New System.Drawing.Point(279, 222)
Me.dtpMaritalDate.Name = "dtpMaritalDate"
Me.dtpMaritalDate.ShowCheckBox = true
Me.dtpMaritalDate.Size = New System.Drawing.Size(123, 21)
Me.dtpMaritalDate.TabIndex = 111
'
'lblMaritalDate
'
Me.lblMaritalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMaritalDate.Location = New System.Drawing.Point(198, 225)
Me.lblMaritalDate.Name = "lblMaritalDate"
Me.lblMaritalDate.Size = New System.Drawing.Size(75, 15)
Me.lblMaritalDate.TabIndex = 110
Me.lblMaritalDate.Text = "Married Date"
Me.lblMaritalDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblSportsHobbies
'
Me.lblSportsHobbies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSportsHobbies.Location = New System.Drawing.Point(412, 168)
Me.lblSportsHobbies.Name = "lblSportsHobbies"
Me.lblSportsHobbies.Size = New System.Drawing.Size(63, 27)
Me.lblSportsHobbies.TabIndex = 112
Me.lblSportsHobbies.Text = "Sports / Hobbies"
Me.lblSportsHobbies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboMaritalStatus
'
Me.cboMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboMaritalStatus.FormattingEnabled = true
Me.cboMaritalStatus.Location = New System.Drawing.Point(86, 222)
Me.cboMaritalStatus.Name = "cboMaritalStatus"
Me.cboMaritalStatus.Size = New System.Drawing.Size(106, 21)
Me.cboMaritalStatus.TabIndex = 105
'
'lvDisabilities
'
Me.lvDisabilities.CheckBoxes = true
Me.lvDisabilities.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDisabilites})
Me.lvDisabilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lvDisabilities.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
Me.lvDisabilities.Location = New System.Drawing.Point(687, 33)
Me.lvDisabilities.Name = "lvDisabilities"
Me.lvDisabilities.Size = New System.Drawing.Size(140, 129)
Me.lvDisabilities.TabIndex = 109
Me.lvDisabilities.UseCompatibleStateImageBehavior = false
Me.lvDisabilities.View = System.Windows.Forms.View.Details
'
'colhDisabilites
'
Me.colhDisabilites.Width = 100
'
'lblMaritalStatus
'
Me.lblMaritalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblMaritalStatus.Location = New System.Drawing.Point(8, 225)
Me.lblMaritalStatus.Name = "lblMaritalStatus"
Me.lblMaritalStatus.Size = New System.Drawing.Size(73, 15)
Me.lblMaritalStatus.TabIndex = 104
Me.lblMaritalStatus.Text = "Marital Status"
Me.lblMaritalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblDisabilities
'
Me.lblDisabilities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblDisabilities.Location = New System.Drawing.Point(627, 36)
Me.lblDisabilities.Name = "lblDisabilities"
Me.lblDisabilities.Size = New System.Drawing.Size(58, 30)
Me.lblDisabilities.TabIndex = 108
Me.lblDisabilities.Text = "Medical Disabilities"
'
'lvAllergis
'
Me.lvAllergis.CheckBoxes = true
Me.lvAllergis.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllergies})
Me.lvAllergis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lvAllergis.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
Me.lvAllergis.HideSelection = false
Me.lvAllergis.Location = New System.Drawing.Point(481, 33)
Me.lvAllergis.Name = "lvAllergis"
Me.lvAllergis.Size = New System.Drawing.Size(140, 129)
Me.lvAllergis.TabIndex = 107
Me.lvAllergis.UseCompatibleStateImageBehavior = false
Me.lvAllergis.View = System.Windows.Forms.View.Details
'
'colhAllergies
'
Me.colhAllergies.Width = 100
'
'lblAllergies
'
Me.lblAllergies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblAllergies.Location = New System.Drawing.Point(411, 36)
Me.lblAllergies.Name = "lblAllergies"
Me.lblAllergies.Size = New System.Drawing.Size(63, 15)
Me.lblAllergies.TabIndex = 106
Me.lblAllergies.Text = "Allergies"
Me.lblAllergies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblWeight
'
Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblWeight.Location = New System.Drawing.Point(198, 198)
Me.lblWeight.Name = "lblWeight"
Me.lblWeight.Size = New System.Drawing.Size(75, 15)
Me.lblWeight.TabIndex = 102
Me.lblWeight.Text = "Weight"
Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblHeight
'
Me.lblHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblHeight.Location = New System.Drawing.Point(198, 171)
Me.lblHeight.Name = "lblHeight"
Me.lblHeight.Size = New System.Drawing.Size(75, 15)
Me.lblHeight.TabIndex = 100
Me.lblHeight.Text = "Height"
Me.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblLanguage4
'
Me.lblLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLanguage4.Location = New System.Drawing.Point(198, 144)
Me.lblLanguage4.Name = "lblLanguage4"
Me.lblLanguage4.Size = New System.Drawing.Size(75, 15)
Me.lblLanguage4.TabIndex = 98
Me.lblLanguage4.Text = "Language4"
Me.lblLanguage4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboLanguage4
'
Me.cboLanguage4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboLanguage4.DropDownWidth = 180
Me.cboLanguage4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboLanguage4.FormattingEnabled = true
Me.cboLanguage4.Location = New System.Drawing.Point(279, 141)
Me.cboLanguage4.Name = "cboLanguage4"
Me.cboLanguage4.Size = New System.Drawing.Size(123, 21)
Me.cboLanguage4.TabIndex = 99
'
'lblLanguage3
'
Me.lblLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLanguage3.Location = New System.Drawing.Point(198, 117)
Me.lblLanguage3.Name = "lblLanguage3"
Me.lblLanguage3.Size = New System.Drawing.Size(75, 15)
Me.lblLanguage3.TabIndex = 96
Me.lblLanguage3.Text = "Language3"
Me.lblLanguage3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboLanguage3
'
Me.cboLanguage3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboLanguage3.DropDownWidth = 180
Me.cboLanguage3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboLanguage3.FormattingEnabled = true
Me.cboLanguage3.Location = New System.Drawing.Point(279, 114)
Me.cboLanguage3.Name = "cboLanguage3"
Me.cboLanguage3.Size = New System.Drawing.Size(123, 21)
Me.cboLanguage3.TabIndex = 97
'
'lblLanguage2
'
Me.lblLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLanguage2.Location = New System.Drawing.Point(198, 90)
Me.lblLanguage2.Name = "lblLanguage2"
Me.lblLanguage2.Size = New System.Drawing.Size(75, 15)
Me.lblLanguage2.TabIndex = 94
Me.lblLanguage2.Text = "Language2"
Me.lblLanguage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboLanguage2
'
Me.cboLanguage2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboLanguage2.DropDownWidth = 180
Me.cboLanguage2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboLanguage2.FormattingEnabled = true
Me.cboLanguage2.Location = New System.Drawing.Point(279, 87)
Me.cboLanguage2.Name = "cboLanguage2"
Me.cboLanguage2.Size = New System.Drawing.Size(123, 21)
Me.cboLanguage2.TabIndex = 95
'
'lblLanguage1
'
Me.lblLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblLanguage1.Location = New System.Drawing.Point(198, 63)
Me.lblLanguage1.Name = "lblLanguage1"
Me.lblLanguage1.Size = New System.Drawing.Size(75, 15)
Me.lblLanguage1.TabIndex = 92
Me.lblLanguage1.Text = "Language1"
Me.lblLanguage1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboLanguage1
'
Me.cboLanguage1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboLanguage1.DropDownWidth = 180
Me.cboLanguage1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboLanguage1.FormattingEnabled = true
Me.cboLanguage1.Location = New System.Drawing.Point(279, 60)
Me.cboLanguage1.Name = "cboLanguage1"
Me.cboLanguage1.Size = New System.Drawing.Size(123, 21)
Me.cboLanguage1.TabIndex = 93
'
'txtExtraTel
'
Me.txtExtraTel.Flags = 0
Me.txtExtraTel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtExtraTel.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtExtraTel.Location = New System.Drawing.Point(279, 33)
Me.txtExtraTel.Name = "txtExtraTel"
Me.txtExtraTel.Size = New System.Drawing.Size(123, 21)
Me.txtExtraTel.TabIndex = 91
'
'lblExtraTel
'
Me.lblExtraTel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblExtraTel.Location = New System.Drawing.Point(198, 36)
Me.lblExtraTel.Name = "lblExtraTel"
Me.lblExtraTel.Size = New System.Drawing.Size(75, 15)
Me.lblExtraTel.TabIndex = 90
Me.lblExtraTel.Text = "Tel. Ext"
Me.lblExtraTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblHair
'
Me.lblHair.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblHair.Location = New System.Drawing.Point(8, 198)
Me.lblHair.Name = "lblHair"
Me.lblHair.Size = New System.Drawing.Size(71, 15)
Me.lblHair.TabIndex = 88
Me.lblHair.Text = "Hair"
Me.lblHair.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboHair
'
Me.cboHair.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboHair.DropDownWidth = 180
Me.cboHair.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboHair.FormattingEnabled = true
Me.cboHair.Location = New System.Drawing.Point(86, 195)
Me.cboHair.Name = "cboHair"
Me.cboHair.Size = New System.Drawing.Size(106, 21)
Me.cboHair.TabIndex = 89
'
'lblReligion
'
Me.lblReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblReligion.Location = New System.Drawing.Point(8, 171)
Me.lblReligion.Name = "lblReligion"
Me.lblReligion.Size = New System.Drawing.Size(71, 15)
Me.lblReligion.TabIndex = 86
Me.lblReligion.Text = "Religion"
Me.lblReligion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboReligion
'
Me.cboReligion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboReligion.DropDownWidth = 180
Me.cboReligion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboReligion.FormattingEnabled = true
Me.cboReligion.Location = New System.Drawing.Point(86, 168)
Me.cboReligion.Name = "cboReligion"
Me.cboReligion.Size = New System.Drawing.Size(106, 21)
Me.cboReligion.TabIndex = 87
'
'lblEthinCity
'
Me.lblEthinCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEthinCity.Location = New System.Drawing.Point(8, 144)
Me.lblEthinCity.Name = "lblEthinCity"
Me.lblEthinCity.Size = New System.Drawing.Size(71, 15)
Me.lblEthinCity.TabIndex = 84
Me.lblEthinCity.Text = "Ethnicity"
Me.lblEthinCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEthincity
'
Me.cboEthincity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEthincity.DropDownWidth = 180
Me.cboEthincity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEthincity.FormattingEnabled = true
Me.cboEthincity.Location = New System.Drawing.Point(86, 141)
Me.cboEthincity.Name = "cboEthincity"
Me.cboEthincity.Size = New System.Drawing.Size(106, 21)
Me.cboEthincity.TabIndex = 85
'
'lblNationality
'
Me.lblNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblNationality.Location = New System.Drawing.Point(8, 117)
Me.lblNationality.Name = "lblNationality"
Me.lblNationality.Size = New System.Drawing.Size(71, 15)
Me.lblNationality.TabIndex = 82
Me.lblNationality.Text = "Nationality"
Me.lblNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboNationality
'
Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboNationality.DropDownWidth = 180
Me.cboNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboNationality.FormattingEnabled = true
Me.cboNationality.Location = New System.Drawing.Point(86, 114)
Me.cboNationality.Name = "cboNationality"
Me.cboNationality.Size = New System.Drawing.Size(106, 21)
Me.cboNationality.TabIndex = 83
'
'lblEyeColor
'
Me.lblEyeColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEyeColor.Location = New System.Drawing.Point(8, 90)
Me.lblEyeColor.Name = "lblEyeColor"
Me.lblEyeColor.Size = New System.Drawing.Size(71, 15)
Me.lblEyeColor.TabIndex = 80
Me.lblEyeColor.Text = "Eye Color"
Me.lblEyeColor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEyeColor
'
Me.cboEyeColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEyeColor.DropDownWidth = 180
Me.cboEyeColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEyeColor.FormattingEnabled = true
Me.cboEyeColor.Location = New System.Drawing.Point(86, 87)
Me.cboEyeColor.Name = "cboEyeColor"
Me.cboEyeColor.Size = New System.Drawing.Size(106, 21)
Me.cboEyeColor.TabIndex = 81
'
'lblBloodGroup
'
Me.lblBloodGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblBloodGroup.Location = New System.Drawing.Point(8, 63)
Me.lblBloodGroup.Name = "lblBloodGroup"
Me.lblBloodGroup.Size = New System.Drawing.Size(71, 15)
Me.lblBloodGroup.TabIndex = 78
Me.lblBloodGroup.Text = "Blood Group"
Me.lblBloodGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboBloodGroup
'
Me.cboBloodGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboBloodGroup.DropDownWidth = 180
Me.cboBloodGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboBloodGroup.FormattingEnabled = true
Me.cboBloodGroup.Location = New System.Drawing.Point(86, 60)
Me.cboBloodGroup.Name = "cboBloodGroup"
Me.cboBloodGroup.Size = New System.Drawing.Size(106, 21)
Me.cboBloodGroup.TabIndex = 79
'
'lblComplexion
'
Me.lblComplexion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblComplexion.Location = New System.Drawing.Point(8, 36)
Me.lblComplexion.Name = "lblComplexion"
Me.lblComplexion.Size = New System.Drawing.Size(71, 15)
Me.lblComplexion.TabIndex = 76
Me.lblComplexion.Text = "Complexion"
Me.lblComplexion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboComplexion
'
Me.cboComplexion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboComplexion.DropDownWidth = 180
Me.cboComplexion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboComplexion.FormattingEnabled = true
Me.cboComplexion.Location = New System.Drawing.Point(86, 33)
Me.cboComplexion.Name = "cboComplexion"
Me.cboComplexion.Size = New System.Drawing.Size(106, 21)
Me.cboComplexion.TabIndex = 77
'
'tabpShiftAssignment
'
Me.tabpShiftAssignment.Controls.Add(Me.pnlShift)
Me.tabpShiftAssignment.Location = New System.Drawing.Point(4, 22)
Me.tabpShiftAssignment.Name = "tabpShiftAssignment"
Me.tabpShiftAssignment.Size = New System.Drawing.Size(850, 266)
Me.tabpShiftAssignment.TabIndex = 8
Me.tabpShiftAssignment.Text = "Assigned Shift"
Me.tabpShiftAssignment.UseVisualStyleBackColor = true
'
'pnlShift
'
Me.pnlShift.Controls.Add(Me.pnlShiftAssignment)
Me.pnlShift.Controls.Add(Me.gbAssignedShift)
Me.pnlShift.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlShift.Location = New System.Drawing.Point(0, 0)
Me.pnlShift.Name = "pnlShift"
Me.pnlShift.Padding = New System.Windows.Forms.Padding(3)
Me.pnlShift.Size = New System.Drawing.Size(850, 266)
Me.pnlShift.TabIndex = 1
'
'pnlShiftAssignment
'
Me.pnlShiftAssignment.Controls.Add(Me.dgvAssignedShift)
Me.pnlShiftAssignment.Controls.Add(Me.lvAssignedShift)
Me.pnlShiftAssignment.Controls.Add(Me.btnSetDefault)
Me.pnlShiftAssignment.Controls.Add(Me.btnRemoveDefault)
Me.pnlShiftAssignment.Controls.Add(Me.btnDeleteShift)
Me.pnlShiftAssignment.Controls.Add(Me.btnAssignShift)
Me.pnlShiftAssignment.Controls.Add(Me.lblShiftAssignment)
Me.pnlShiftAssignment.Controls.Add(Me.objbtnAddShift)
Me.pnlShiftAssignment.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlShiftAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.pnlShiftAssignment.Location = New System.Drawing.Point(3, 3)
Me.pnlShiftAssignment.Name = "pnlShiftAssignment"
Me.pnlShiftAssignment.Size = New System.Drawing.Size(844, 260)
Me.pnlShiftAssignment.TabIndex = 1
'
'dgvAssignedShift
'
Me.dgvAssignedShift.AllowUserToAddRows = false
Me.dgvAssignedShift.AllowUserToDeleteRows = false
Me.dgvAssignedShift.AllowUserToResizeRows = false
Me.dgvAssignedShift.BackgroundColor = System.Drawing.Color.WhiteSmoke
Me.dgvAssignedShift.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
Me.dgvAssignedShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
Me.dgvAssignedShift.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhEffective_Date, Me.dgcolhShiftType, Me.dgcolhShiftName})
Me.dgvAssignedShift.Dock = System.Windows.Forms.DockStyle.Fill
Me.dgvAssignedShift.Location = New System.Drawing.Point(0, 0)
Me.dgvAssignedShift.Name = "dgvAssignedShift"
Me.dgvAssignedShift.ReadOnly = true
Me.dgvAssignedShift.RowHeadersVisible = false
Me.dgvAssignedShift.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
Me.dgvAssignedShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
Me.dgvAssignedShift.Size = New System.Drawing.Size(844, 260)
Me.dgvAssignedShift.TabIndex = 52
'
'dgcolhEffective_Date
'
Me.dgcolhEffective_Date.HeaderText = "Effective Date"
Me.dgcolhEffective_Date.Name = "dgcolhEffective_Date"
Me.dgcolhEffective_Date.ReadOnly = true
'
'dgcolhShiftType
'
Me.dgcolhShiftType.HeaderText = "Shift Type"
Me.dgcolhShiftType.Name = "dgcolhShiftType"
Me.dgcolhShiftType.ReadOnly = true
Me.dgcolhShiftType.Width = 120
'
'dgcolhShiftName
'
Me.dgcolhShiftName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
Me.dgcolhShiftName.HeaderText = "Shift"
Me.dgcolhShiftName.Name = "dgcolhShiftName"
Me.dgcolhShiftName.ReadOnly = true
'
'lvAssignedShift
'
Me.lvAssignedShift.BackColorOnChecked = false
Me.lvAssignedShift.ColumnHeaders = Nothing
Me.lvAssignedShift.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhShiftType, Me.colhShiftName, Me.objcolhGUID})
Me.lvAssignedShift.CompulsoryColumns = ""
Me.lvAssignedShift.FullRowSelect = true
Me.lvAssignedShift.GridLines = true
Me.lvAssignedShift.GroupingColumn = Nothing
Me.lvAssignedShift.HideSelection = false
Me.lvAssignedShift.Location = New System.Drawing.Point(708, 12)
Me.lvAssignedShift.MinColumnWidth = 50
Me.lvAssignedShift.MultiSelect = false
Me.lvAssignedShift.Name = "lvAssignedShift"
Me.lvAssignedShift.OptionalColumns = ""
Me.lvAssignedShift.ShowMoreItem = false
Me.lvAssignedShift.ShowSaveItem = false
Me.lvAssignedShift.ShowSelectAll = true
Me.lvAssignedShift.ShowSizeAllColumnsToFit = true
Me.lvAssignedShift.Size = New System.Drawing.Size(125, 30)
Me.lvAssignedShift.Sortable = true
Me.lvAssignedShift.TabIndex = 47
Me.lvAssignedShift.UseCompatibleStateImageBehavior = false
Me.lvAssignedShift.View = System.Windows.Forms.View.Details
'
'colhShiftType
'
Me.colhShiftType.Tag = "colhShiftType"
Me.colhShiftType.Text = "Shift Type"
Me.colhShiftType.Width = 150
'
'colhShiftName
'
Me.colhShiftName.Tag = "colhShiftName"
Me.colhShiftName.Text = "Shift Name"
Me.colhShiftName.Width = 665
'
'objcolhGUID
'
Me.objcolhGUID.Tag = "objcolhGUID"
Me.objcolhGUID.Text = ""
Me.objcolhGUID.Width = 0
'
'btnSetDefault
'
Me.btnSetDefault.BackColor = System.Drawing.Color.White
Me.btnSetDefault.BackgroundImage = CType(resources.GetObject("btnSetDefault.BackgroundImage"),System.Drawing.Image)
Me.btnSetDefault.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnSetDefault.BorderColor = System.Drawing.Color.Empty
Me.btnSetDefault.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnSetDefault.FlatAppearance.BorderSize = 0
Me.btnSetDefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnSetDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnSetDefault.ForeColor = System.Drawing.Color.Black
Me.btnSetDefault.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnSetDefault.GradientForeColor = System.Drawing.Color.Black
Me.btnSetDefault.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnSetDefault.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnSetDefault.Location = New System.Drawing.Point(583, 12)
Me.btnSetDefault.Name = "btnSetDefault"
Me.btnSetDefault.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnSetDefault.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnSetDefault.Size = New System.Drawing.Size(121, 30)
Me.btnSetDefault.TabIndex = 51
Me.btnSetDefault.Text = "S&et Default"
Me.btnSetDefault.UseVisualStyleBackColor = true
Me.btnSetDefault.Visible = false
'
'btnRemoveDefault
'
Me.btnRemoveDefault.BackColor = System.Drawing.Color.White
Me.btnRemoveDefault.BackgroundImage = CType(resources.GetObject("btnRemoveDefault.BackgroundImage"),System.Drawing.Image)
Me.btnRemoveDefault.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnRemoveDefault.BorderColor = System.Drawing.Color.Empty
Me.btnRemoveDefault.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnRemoveDefault.FlatAppearance.BorderSize = 0
Me.btnRemoveDefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnRemoveDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnRemoveDefault.ForeColor = System.Drawing.Color.Black
Me.btnRemoveDefault.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnRemoveDefault.GradientForeColor = System.Drawing.Color.Black
Me.btnRemoveDefault.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnRemoveDefault.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnRemoveDefault.Location = New System.Drawing.Point(581, 12)
Me.btnRemoveDefault.Name = "btnRemoveDefault"
Me.btnRemoveDefault.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnRemoveDefault.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnRemoveDefault.Size = New System.Drawing.Size(121, 30)
Me.btnRemoveDefault.TabIndex = 51
Me.btnRemoveDefault.Text = "&Remove Default"
Me.btnRemoveDefault.UseVisualStyleBackColor = true
'
'btnDeleteShift
'
Me.btnDeleteShift.BackColor = System.Drawing.Color.White
Me.btnDeleteShift.BackgroundImage = CType(resources.GetObject("btnDeleteShift.BackgroundImage"),System.Drawing.Image)
Me.btnDeleteShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnDeleteShift.BorderColor = System.Drawing.Color.Empty
Me.btnDeleteShift.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnDeleteShift.FlatAppearance.BorderSize = 0
Me.btnDeleteShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnDeleteShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnDeleteShift.ForeColor = System.Drawing.Color.Black
Me.btnDeleteShift.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnDeleteShift.GradientForeColor = System.Drawing.Color.Black
Me.btnDeleteShift.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeleteShift.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnDeleteShift.Location = New System.Drawing.Point(475, 12)
Me.btnDeleteShift.Name = "btnDeleteShift"
Me.btnDeleteShift.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnDeleteShift.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnDeleteShift.Size = New System.Drawing.Size(100, 30)
Me.btnDeleteShift.TabIndex = 49
Me.btnDeleteShift.Text = "&Delete"
Me.btnDeleteShift.UseVisualStyleBackColor = true
Me.btnDeleteShift.Visible = false
'
'btnAssignShift
'
Me.btnAssignShift.BackColor = System.Drawing.Color.White
Me.btnAssignShift.BackgroundImage = CType(resources.GetObject("btnAssignShift.BackgroundImage"),System.Drawing.Image)
Me.btnAssignShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
Me.btnAssignShift.BorderColor = System.Drawing.Color.Empty
Me.btnAssignShift.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
Me.btnAssignShift.FlatAppearance.BorderSize = 0
Me.btnAssignShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
Me.btnAssignShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.btnAssignShift.ForeColor = System.Drawing.Color.Black
Me.btnAssignShift.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
Me.btnAssignShift.GradientForeColor = System.Drawing.Color.Black
Me.btnAssignShift.HoverGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssignShift.HoverGradientForeColor = System.Drawing.Color.Black
Me.btnAssignShift.Location = New System.Drawing.Point(369, 12)
Me.btnAssignShift.Name = "btnAssignShift"
Me.btnAssignShift.PressedGradientBackColor = System.Drawing.Color.Transparent
Me.btnAssignShift.PressedGradientForeColor = System.Drawing.Color.Black
Me.btnAssignShift.Size = New System.Drawing.Size(100, 30)
Me.btnAssignShift.TabIndex = 48
Me.btnAssignShift.Text = "&Add"
Me.btnAssignShift.UseVisualStyleBackColor = true
Me.btnAssignShift.Visible = false
'
'lblShiftAssignment
'
Me.lblShiftAssignment.Location = New System.Drawing.Point(13, 19)
Me.lblShiftAssignment.Name = "lblShiftAssignment"
Me.lblShiftAssignment.Size = New System.Drawing.Size(97, 16)
Me.lblShiftAssignment.TabIndex = 0
Me.lblShiftAssignment.Text = "Shift Assignment"
Me.lblShiftAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.lblShiftAssignment.Visible = false
'
'objbtnAddShift
'
Me.objbtnAddShift.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddShift.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddShift.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddShift.BorderSelected = false
Me.objbtnAddShift.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddShift.Location = New System.Drawing.Point(342, 17)
Me.objbtnAddShift.Name = "objbtnAddShift"
Me.objbtnAddShift.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddShift.TabIndex = 45
Me.objbtnAddShift.Visible = false
'
'gbAssignedShift
'
Me.gbAssignedShift.BorderColor = System.Drawing.Color.Black
Me.gbAssignedShift.Checked = false
Me.gbAssignedShift.CollapseAllExceptThis = false
Me.gbAssignedShift.CollapsedHoverImage = Nothing
Me.gbAssignedShift.CollapsedNormalImage = Nothing
Me.gbAssignedShift.CollapsedPressedImage = Nothing
Me.gbAssignedShift.CollapseOnLoad = false
Me.gbAssignedShift.ExpandedHoverImage = Nothing
Me.gbAssignedShift.ExpandedNormalImage = Nothing
Me.gbAssignedShift.ExpandedPressedImage = Nothing
Me.gbAssignedShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbAssignedShift.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbAssignedShift.HeaderHeight = 25
Me.gbAssignedShift.HeaderMessage = ""
Me.gbAssignedShift.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
Me.gbAssignedShift.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbAssignedShift.HeightOnCollapse = 0
Me.gbAssignedShift.LeftTextSpace = 0
Me.gbAssignedShift.Location = New System.Drawing.Point(76, 77)
Me.gbAssignedShift.Name = "gbAssignedShift"
Me.gbAssignedShift.OpenHeight = 300
Me.gbAssignedShift.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbAssignedShift.ShowBorder = true
Me.gbAssignedShift.ShowCheckBox = false
Me.gbAssignedShift.ShowCollapseButton = false
Me.gbAssignedShift.ShowDefaultBorderColor = true
Me.gbAssignedShift.ShowDownButton = false
Me.gbAssignedShift.ShowHeader = true
Me.gbAssignedShift.Size = New System.Drawing.Size(83, 25)
Me.gbAssignedShift.TabIndex = 0
Me.gbAssignedShift.Temp = 0
Me.gbAssignedShift.Text = "View Assigned Shift"
Me.gbAssignedShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
Me.gbAssignedShift.Visible = false
'
'tabpPolicy
'
Me.tabpPolicy.Controls.Add(Me.pnlAssignedPolicy)
Me.tabpPolicy.Location = New System.Drawing.Point(4, 22)
Me.tabpPolicy.Name = "tabpPolicy"
Me.tabpPolicy.Padding = New System.Windows.Forms.Padding(3)
Me.tabpPolicy.Size = New System.Drawing.Size(850, 266)
Me.tabpPolicy.TabIndex = 9
Me.tabpPolicy.Text = "Assigned Policy"
Me.tabpPolicy.UseVisualStyleBackColor = true
'
'pnlAssignedPolicy
'
Me.pnlAssignedPolicy.Controls.Add(Me.dgvPolicy)
Me.pnlAssignedPolicy.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlAssignedPolicy.Location = New System.Drawing.Point(3, 3)
Me.pnlAssignedPolicy.Name = "pnlAssignedPolicy"
Me.pnlAssignedPolicy.Size = New System.Drawing.Size(844, 260)
Me.pnlAssignedPolicy.TabIndex = 0
'
'dgvPolicy
'
Me.dgvPolicy.AllowUserToAddRows = false
Me.dgvPolicy.AllowUserToDeleteRows = false
Me.dgvPolicy.AllowUserToResizeRows = false
Me.dgvPolicy.BackgroundColor = System.Drawing.Color.WhiteSmoke
Me.dgvPolicy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
Me.dgvPolicy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
Me.dgvPolicy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhPEffective_Date, Me.dgcolhPolicy})
Me.dgvPolicy.Dock = System.Windows.Forms.DockStyle.Fill
Me.dgvPolicy.Location = New System.Drawing.Point(0, 0)
Me.dgvPolicy.Name = "dgvPolicy"
Me.dgvPolicy.ReadOnly = true
Me.dgvPolicy.RowHeadersVisible = false
Me.dgvPolicy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
Me.dgvPolicy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
Me.dgvPolicy.Size = New System.Drawing.Size(844, 260)
Me.dgvPolicy.TabIndex = 2
'
'dgcolhPEffective_Date
'
Me.dgcolhPEffective_Date.HeaderText = "Effective Date"
Me.dgcolhPEffective_Date.Name = "dgcolhPEffective_Date"
Me.dgcolhPEffective_Date.ReadOnly = true
'
'dgcolhPolicy
'
Me.dgcolhPolicy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
Me.dgcolhPolicy.HeaderText = "Policy"
Me.dgcolhPolicy.Name = "dgcolhPolicy"
Me.dgcolhPolicy.ReadOnly = true
'
'tapbClearance
'
Me.tapbClearance.Controls.Add(Me.pnlClearance)
Me.tapbClearance.Location = New System.Drawing.Point(4, 22)
Me.tapbClearance.Name = "tapbClearance"
Me.tapbClearance.Padding = New System.Windows.Forms.Padding(3)
Me.tapbClearance.Size = New System.Drawing.Size(850, 266)
Me.tapbClearance.TabIndex = 7
Me.tapbClearance.Text = "Clearance"
Me.tapbClearance.UseVisualStyleBackColor = true
'
'pnlClearance
'
Me.pnlClearance.Controls.Add(Me.gbClearanceInfo)
Me.pnlClearance.Dock = System.Windows.Forms.DockStyle.Fill
Me.pnlClearance.Location = New System.Drawing.Point(3, 3)
Me.pnlClearance.Name = "pnlClearance"
Me.pnlClearance.Size = New System.Drawing.Size(844, 260)
Me.pnlClearance.TabIndex = 1
'
'gbClearanceInfo
'
Me.gbClearanceInfo.BorderColor = System.Drawing.Color.Black
Me.gbClearanceInfo.Checked = false
Me.gbClearanceInfo.CollapseAllExceptThis = false
Me.gbClearanceInfo.CollapsedHoverImage = Nothing
Me.gbClearanceInfo.CollapsedNormalImage = Nothing
Me.gbClearanceInfo.CollapsedPressedImage = Nothing
Me.gbClearanceInfo.CollapseOnLoad = false
Me.gbClearanceInfo.Controls.Add(Me.chkMarkClear)
Me.gbClearanceInfo.Controls.Add(Me.lvClearance)
Me.gbClearanceInfo.Dock = System.Windows.Forms.DockStyle.Fill
Me.gbClearanceInfo.ExpandedHoverImage = Nothing
Me.gbClearanceInfo.ExpandedNormalImage = Nothing
Me.gbClearanceInfo.ExpandedPressedImage = Nothing
Me.gbClearanceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.gbClearanceInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
Me.gbClearanceInfo.HeaderHeight = 25
Me.gbClearanceInfo.HeaderMessage = ""
Me.gbClearanceInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
Me.gbClearanceInfo.HeaderMessageForeColor = System.Drawing.Color.Black
Me.gbClearanceInfo.HeightOnCollapse = 0
Me.gbClearanceInfo.LeftTextSpace = 0
Me.gbClearanceInfo.Location = New System.Drawing.Point(0, 0)
Me.gbClearanceInfo.Name = "gbClearanceInfo"
Me.gbClearanceInfo.OpenHeight = 300
Me.gbClearanceInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
Me.gbClearanceInfo.ShowBorder = true
Me.gbClearanceInfo.ShowCheckBox = false
Me.gbClearanceInfo.ShowCollapseButton = false
Me.gbClearanceInfo.ShowDefaultBorderColor = true
Me.gbClearanceInfo.ShowDownButton = false
Me.gbClearanceInfo.ShowHeader = true
Me.gbClearanceInfo.Size = New System.Drawing.Size(844, 260)
Me.gbClearanceInfo.TabIndex = 0
Me.gbClearanceInfo.Temp = 0
Me.gbClearanceInfo.Text = "Employee Clearance"
Me.gbClearanceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'chkMarkClear
'
Me.chkMarkClear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.chkMarkClear.Location = New System.Drawing.Point(702, 4)
Me.chkMarkClear.Name = "chkMarkClear"
Me.chkMarkClear.Size = New System.Drawing.Size(133, 17)
Me.chkMarkClear.TabIndex = 17
Me.chkMarkClear.Text = "Mark As Clear"
Me.chkMarkClear.UseVisualStyleBackColor = true
'
'lvClearance
'
Me.lvClearance.BackColorOnChecked = false
Me.lvClearance.ColumnHeaders = Nothing
Me.lvClearance.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhClGroupName, Me.colhClParticulars, Me.colhClAmount, Me.colhClStatus, Me.colhClRemark})
Me.lvClearance.CompulsoryColumns = ""
Me.lvClearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lvClearance.FullRowSelect = true
Me.lvClearance.GridLines = true
Me.lvClearance.GroupingColumn = Nothing
Me.lvClearance.HideSelection = false
Me.lvClearance.Location = New System.Drawing.Point(2, 26)
Me.lvClearance.MinColumnWidth = 50
Me.lvClearance.MultiSelect = false
Me.lvClearance.Name = "lvClearance"
Me.lvClearance.OptionalColumns = ""
Me.lvClearance.ShowMoreItem = false
Me.lvClearance.ShowSaveItem = false
Me.lvClearance.ShowSelectAll = true
Me.lvClearance.ShowSizeAllColumnsToFit = true
Me.lvClearance.Size = New System.Drawing.Size(840, 231)
Me.lvClearance.Sortable = true
Me.lvClearance.TabIndex = 1
Me.lvClearance.UseCompatibleStateImageBehavior = false
Me.lvClearance.View = System.Windows.Forms.View.Details
'
'colhClGroupName
'
Me.colhClGroupName.Text = "Group"
Me.colhClGroupName.Width = 0
'
'colhClParticulars
'
Me.colhClParticulars.Text = "Particulars"
Me.colhClParticulars.Width = 230
'
'colhClAmount
'
Me.colhClAmount.Text = "Amount"
Me.colhClAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
Me.colhClAmount.Width = 150
'
'colhClStatus
'
Me.colhClStatus.Text = "Status"
Me.colhClStatus.Width = 150
'
'colhClRemark
'
Me.colhClRemark.Text = "Remark"
Me.colhClRemark.Width = 300
'
'objbtnAddPayPoint
'
Me.objbtnAddPayPoint.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddPayPoint.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddPayPoint.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddPayPoint.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddPayPoint.BorderSelected = false
Me.objbtnAddPayPoint.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddPayPoint.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddPayPoint.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddPayPoint.Location = New System.Drawing.Point(604, 114)
Me.objbtnAddPayPoint.Name = "objbtnAddPayPoint"
Me.objbtnAddPayPoint.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddPayPoint.TabIndex = 44
'
'objbtnAddPayType
'
Me.objbtnAddPayType.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddPayType.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddPayType.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddPayType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddPayType.BorderSelected = false
Me.objbtnAddPayType.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddPayType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddPayType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddPayType.Location = New System.Drawing.Point(604, 60)
Me.objbtnAddPayType.Name = "objbtnAddPayType"
Me.objbtnAddPayType.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddPayType.TabIndex = 43
'
'objbtnAddEmploymentType
'
Me.objbtnAddEmploymentType.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddEmploymentType.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddEmploymentType.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddEmploymentType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddEmploymentType.BorderSelected = false
Me.objbtnAddEmploymentType.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddEmploymentType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddEmploymentType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddEmploymentType.Location = New System.Drawing.Point(313, 168)
Me.objbtnAddEmploymentType.Name = "objbtnAddEmploymentType"
Me.objbtnAddEmploymentType.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddEmploymentType.TabIndex = 42
'
'objbtnAddTitle
'
Me.objbtnAddTitle.BackColor = System.Drawing.Color.Transparent
Me.objbtnAddTitle.BackColor1 = System.Drawing.Color.Transparent
Me.objbtnAddTitle.BackColor2 = System.Drawing.Color.Transparent
Me.objbtnAddTitle.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer))
Me.objbtnAddTitle.BorderSelected = false
Me.objbtnAddTitle.DialogResult = System.Windows.Forms.DialogResult.None
Me.objbtnAddTitle.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
Me.objbtnAddTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
Me.objbtnAddTitle.Location = New System.Drawing.Point(205, 60)
Me.objbtnAddTitle.Name = "objbtnAddTitle"
Me.objbtnAddTitle.Size = New System.Drawing.Size(21, 21)
Me.objbtnAddTitle.TabIndex = 41
'
'cboPaypoint
'
Me.cboPaypoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPaypoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPaypoint.FormattingEnabled = true
Me.cboPaypoint.Location = New System.Drawing.Point(432, 114)
Me.cboPaypoint.Name = "cboPaypoint"
Me.cboPaypoint.Size = New System.Drawing.Size(166, 21)
Me.cboPaypoint.TabIndex = 19
'
'lblPaypoint
'
Me.lblPaypoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPaypoint.Location = New System.Drawing.Point(340, 117)
Me.lblPaypoint.Name = "lblPaypoint"
Me.lblPaypoint.Size = New System.Drawing.Size(86, 15)
Me.lblPaypoint.TabIndex = 18
Me.lblPaypoint.Text = "Pay Point"
Me.lblPaypoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboPayType
'
Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboPayType.FormattingEnabled = true
Me.cboPayType.Location = New System.Drawing.Point(432, 60)
Me.cboPayType.Name = "cboPayType"
Me.cboPayType.Size = New System.Drawing.Size(166, 21)
Me.cboPayType.TabIndex = 15
'
'lblPayType
'
Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblPayType.Location = New System.Drawing.Point(340, 63)
Me.lblPayType.Name = "lblPayType"
Me.lblPayType.Size = New System.Drawing.Size(86, 15)
Me.lblPayType.TabIndex = 14
Me.lblPayType.Text = "Pay Type"
Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblShift
'
Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblShift.Location = New System.Drawing.Point(340, 144)
Me.lblShift.Name = "lblShift"
Me.lblShift.Size = New System.Drawing.Size(86, 15)
Me.lblShift.TabIndex = 20
Me.lblShift.Text = "Shift"
Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboEmploymentType
'
Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboEmploymentType.FormattingEnabled = true
Me.cboEmploymentType.Location = New System.Drawing.Point(109, 168)
Me.cboEmploymentType.Name = "cboEmploymentType"
Me.cboEmploymentType.Size = New System.Drawing.Size(198, 21)
Me.cboEmploymentType.TabIndex = 11
'
'lblEmploymentType
'
Me.lblEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmploymentType.Location = New System.Drawing.Point(8, 171)
Me.lblEmploymentType.Name = "lblEmploymentType"
Me.lblEmploymentType.Size = New System.Drawing.Size(95, 15)
Me.lblEmploymentType.TabIndex = 10
Me.lblEmploymentType.Text = "Employment Type"
Me.lblEmploymentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboGender
'
Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboGender.FormattingEnabled = true
Me.cboGender.Location = New System.Drawing.Point(432, 87)
Me.cboGender.Name = "cboGender"
Me.cboGender.Size = New System.Drawing.Size(166, 21)
Me.cboGender.TabIndex = 17
'
'lblGender
'
Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblGender.Location = New System.Drawing.Point(340, 90)
Me.lblGender.Name = "lblGender"
Me.lblGender.Size = New System.Drawing.Size(86, 15)
Me.lblGender.TabIndex = 16
Me.lblGender.Text = "Gender"
Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblTitle
'
Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblTitle.Location = New System.Drawing.Point(8, 63)
Me.lblTitle.Name = "lblTitle"
Me.lblTitle.Size = New System.Drawing.Size(95, 15)
Me.lblTitle.TabIndex = 2
Me.lblTitle.Text = "Title"
Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtOtherName
'
Me.txtOtherName.BackColor = System.Drawing.SystemColors.Window
Me.txtOtherName.Flags = 0
Me.txtOtherName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtOtherName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtOtherName.Location = New System.Drawing.Point(109, 141)
Me.txtOtherName.Name = "txtOtherName"
Me.txtOtherName.Size = New System.Drawing.Size(198, 21)
Me.txtOtherName.TabIndex = 9
'
'objLine
'
Me.objLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
Me.objLine.Location = New System.Drawing.Point(11, 192)
Me.objLine.Name = "objLine"
Me.objLine.Size = New System.Drawing.Size(631, 9)
Me.objLine.TabIndex = 23
Me.objLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtFirstname
'
Me.txtFirstname.BackColor = System.Drawing.SystemColors.Window
Me.txtFirstname.Flags = 0
Me.txtFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtFirstname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtFirstname.Location = New System.Drawing.Point(109, 114)
Me.txtFirstname.Name = "txtFirstname"
Me.txtFirstname.Size = New System.Drawing.Size(198, 21)
Me.txtFirstname.TabIndex = 7
'
'txtSurname
'
Me.txtSurname.BackColor = System.Drawing.SystemColors.Window
Me.txtSurname.Flags = 0
Me.txtSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtSurname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtSurname.Location = New System.Drawing.Point(109, 87)
Me.txtSurname.Name = "txtSurname"
Me.txtSurname.Size = New System.Drawing.Size(198, 21)
Me.txtSurname.TabIndex = 5
'
'txtEmployeeCode
'
Me.txtEmployeeCode.BackColor = System.Drawing.SystemColors.Window
Me.txtEmployeeCode.Flags = 0
Me.txtEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtEmployeeCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtEmployeeCode.Location = New System.Drawing.Point(109, 33)
Me.txtEmployeeCode.Name = "txtEmployeeCode"
Me.txtEmployeeCode.Size = New System.Drawing.Size(198, 21)
Me.txtEmployeeCode.TabIndex = 1
'
'cboTitle
'
Me.cboTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboTitle.FormattingEnabled = true
Me.cboTitle.Location = New System.Drawing.Point(109, 60)
Me.cboTitle.Name = "cboTitle"
Me.cboTitle.Size = New System.Drawing.Size(90, 21)
Me.cboTitle.TabIndex = 3
'
'lblEmployeeCode
'
Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblEmployeeCode.Location = New System.Drawing.Point(8, 36)
Me.lblEmployeeCode.Name = "lblEmployeeCode"
Me.lblEmployeeCode.Size = New System.Drawing.Size(95, 15)
Me.lblEmployeeCode.TabIndex = 0
Me.lblEmployeeCode.Text = "Employee Code"
Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'txtIdNo
'
Me.txtIdNo.Flags = 0
Me.txtIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.txtIdNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
Me.txtIdNo.Location = New System.Drawing.Point(432, 33)
Me.txtIdNo.Name = "txtIdNo"
Me.txtIdNo.Size = New System.Drawing.Size(166, 21)
Me.txtIdNo.TabIndex = 13
'
'lblOthername
'
Me.lblOthername.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblOthername.Location = New System.Drawing.Point(8, 144)
Me.lblOthername.Name = "lblOthername"
Me.lblOthername.Size = New System.Drawing.Size(95, 15)
Me.lblOthername.TabIndex = 8
Me.lblOthername.Text = "Other Name"
Me.lblOthername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblFirstName
'
Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblFirstName.Location = New System.Drawing.Point(8, 117)
Me.lblFirstName.Name = "lblFirstName"
Me.lblFirstName.Size = New System.Drawing.Size(95, 15)
Me.lblFirstName.TabIndex = 6
Me.lblFirstName.Text = "First Name"
Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblSurname
'
Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblSurname.Location = New System.Drawing.Point(8, 90)
Me.lblSurname.Name = "lblSurname"
Me.lblSurname.Size = New System.Drawing.Size(95, 15)
Me.lblSurname.TabIndex = 4
Me.lblSurname.Text = "Surname"
Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'lblIdNo
'
Me.lblIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.lblIdNo.Location = New System.Drawing.Point(340, 36)
Me.lblIdNo.Name = "lblIdNo"
Me.lblIdNo.Size = New System.Drawing.Size(86, 15)
Me.lblIdNo.TabIndex = 12
Me.lblIdNo.Text = "Identify No."
Me.lblIdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
'
'cboShift
'
Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.cboShift.FormattingEnabled = true
Me.cboShift.Location = New System.Drawing.Point(432, 141)
Me.cboShift.Name = "cboShift"
Me.cboShift.Size = New System.Drawing.Size(166, 21)
Me.cboShift.TabIndex = 21
Me.cboShift.Visible = false
'
'DataGridViewTextBoxColumn1
'
Me.DataGridViewTextBoxColumn1.HeaderText = "Effective Date"
Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
'
'DataGridViewTextBoxColumn2
'
Me.DataGridViewTextBoxColumn2.HeaderText = "Shift Type"
Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
Me.DataGridViewTextBoxColumn2.Width = 120
'
'DataGridViewTextBoxColumn3
'
Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
Me.DataGridViewTextBoxColumn3.HeaderText = "Shift"
Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
'
'DataGridViewTextBoxColumn4
'
Me.DataGridViewTextBoxColumn4.HeaderText = "Effective Date"
Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
'
'DataGridViewTextBoxColumn5
'
Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
Me.DataGridViewTextBoxColumn5.HeaderText = "Policy"
Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
'
'frmEmployeeMaster
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(864, 550)
Me.Controls.Add(Me.pnlEmployeeMaster)
Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
Me.IsMdiContainer = true
Me.KeyPreview = true
Me.MaximizeBox = false
Me.MinimizeBox = false
Me.Name = "frmEmployeeMaster"
Me.ShowInTaskbar = false
Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
Me.Text = "Employee Master"
Me.pnlEmployeeMaster.ResumeLayout(false)
Me.objFooter.ResumeLayout(false)
Me.objtblPanel.ResumeLayout(false)
Me.cmnuOperations.ResumeLayout(false)
Me.gbEmployeeDetails.ResumeLayout(false)
Me.gbEmployeeDetails.PerformLayout
Me.pnlImages.ResumeLayout(false)
Me.objpnlButtons.ResumeLayout(false)
Me.tabcEmployeeDetails.ResumeLayout(false)
Me.tabpMainInfo.ResumeLayout(false)
Me.gbEmployeeAllocation.ResumeLayout(false)
Me.pnlEAllocation.ResumeLayout(false)
Me.pnlEAllocation.PerformLayout
Me.tabpAdditionalInfo.ResumeLayout(false)
Me.EZeeCollapsibleContainer1.ResumeLayout(false)
Me.tabcBWInfo.ResumeLayout(false)
Me.tabpBirthInfo.ResumeLayout(false)
Me.tabpBirthInfo.PerformLayout
Me.tabpWorkPermit.ResumeLayout(false)
Me.tabpWorkPermit.PerformLayout
Me.gbDatesDetails.ResumeLayout(false)
Me.tabcDates_Remark.ResumeLayout(false)
Me.tabpDates.ResumeLayout(false)
Me.pnlEDates.ResumeLayout(false)
Me.tabpRemark.ResumeLayout(false)
Me.tabpRemark.PerformLayout
Me.gbLoginAndOtherDetails.ResumeLayout(false)
Me.gbLoginAndOtherDetails.PerformLayout
Me.tabpPersonalInfo.ResumeLayout(false)
Me.pnlPersonalInfo.ResumeLayout(false)
Me.gbAddress.ResumeLayout(false)
Me.pnlPersonalInformation.ResumeLayout(false)
Me.gbRecruitmentAddress.ResumeLayout(false)
Me.gbRecruitmentAddress.PerformLayout
Me.GbDomicileAddress.ResumeLayout(false)
Me.GbDomicileAddress.PerformLayout
Me.GbPresentAddress.ResumeLayout(false)
Me.GbPresentAddress.PerformLayout
Me.tabpEmergencyContact.ResumeLayout(false)
Me.gbEmergencyInfo.ResumeLayout(false)
Me.pnlEmergencyContact.ResumeLayout(false)
Me.GbEmergencyContact1.ResumeLayout(false)
Me.GbEmergencyContact1.PerformLayout
Me.GbEmergencyContact2.ResumeLayout(false)
Me.GbEmergencyContact2.PerformLayout
Me.GbEmergencyContact3.ResumeLayout(false)
Me.GbEmergencyContact3.PerformLayout
Me.tabpIdentityInfo.ResumeLayout(false)
Me.pnlIdentifyInfo.ResumeLayout(false)
Me.gbIDInformation.ResumeLayout(false)
Me.gbIDInformation.PerformLayout
Me.pnlIdentityInfo.ResumeLayout(false)
Me.tabpMembership.ResumeLayout(false)
Me.pnlMembershipinfo.ResumeLayout(false)
Me.gbMembershipInfo.ResumeLayout(false)
Me.Panel2.ResumeLayout(false)
Me.Panel2.PerformLayout
Me.pnlMembershipDataInfo.ResumeLayout(false)
Me.tabpOtherInfo.ResumeLayout(false)
Me.gbOtherInfo.ResumeLayout(false)
Me.gbOtherInfo.PerformLayout
Me.tabpShiftAssignment.ResumeLayout(false)
Me.pnlShift.ResumeLayout(false)
Me.pnlShiftAssignment.ResumeLayout(false)
CType(Me.dgvAssignedShift,System.ComponentModel.ISupportInitialize).EndInit
Me.tabpPolicy.ResumeLayout(false)
Me.pnlAssignedPolicy.ResumeLayout(false)
CType(Me.dgvPolicy,System.ComponentModel.ISupportInitialize).EndInit
Me.tapbClearance.ResumeLayout(false)
Me.pnlClearance.ResumeLayout(false)
Me.gbClearanceInfo.ResumeLayout(false)
Me.ResumeLayout(false)

End Sub
    Friend WithEvents pnlEmployeeMaster As System.Windows.Forms.Panel
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents objLine As eZee.Common.eZeeLine
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents txtOtherName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFirstname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSurname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents txtIdNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOthername As System.Windows.Forms.Label
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents lblIdNo As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents tabcEmployeeDetails As System.Windows.Forms.TabControl
    Friend WithEvents tabpMainInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpPersonalInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpAdditionalInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpIdentityInfo As System.Windows.Forms.TabPage
    Friend WithEvents lblEmploymentType As System.Windows.Forms.Label
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents tabpMembership As System.Windows.Forms.TabPage
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents cboPaypoint As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaypoint As System.Windows.Forms.Label
    Friend WithEvents cboBirthState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents cboBirthCity As System.Windows.Forms.ComboBox
    Friend WithEvents txtVillage As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVillage As System.Windows.Forms.Label
    Friend WithEvents txtWard As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblWard As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents cboBirthCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtCertificateNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCertificateNo As System.Windows.Forms.Label
    Friend WithEvents txtPlaceofIssue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPlaceOfIssue As System.Windows.Forms.Label
    Friend WithEvents dtpExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents dtpIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIssueDate As System.Windows.Forms.Label
    Friend WithEvents lblIssueCountry As System.Windows.Forms.Label
    Friend WithEvents cboIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtWorkPermitNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblWorkPermitNo As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnAddTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents txtScale As eZee.TextBox.NumericTextBox
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents pnlIdentifyInfo As System.Windows.Forms.Panel
    Friend WithEvents gbIDInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkMakeAsDefault As System.Windows.Forms.CheckBox
    Friend WithEvents objLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents pnlIdentityInfo As System.Windows.Forms.Panel
    Friend WithEvents lblDLClass As System.Windows.Forms.Label
    Friend WithEvents txtDLClass As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblIdPlaceOfIssue As System.Windows.Forms.Label
    Friend WithEvents txtIDPlaceofIssue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpIdExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIdExpiryDate As System.Windows.Forms.Label
    Friend WithEvents dtpIdIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIdIssueDate As System.Windows.Forms.Label
    Friend WithEvents lblIdIssueCountry As System.Windows.Forms.Label
    Friend WithEvents cboIdIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentityNo As System.Windows.Forms.Label
    Friend WithEvents txtIdentityNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSerialNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblIdSerialNo As System.Windows.Forms.Label
    Friend WithEvents lblIdType As System.Windows.Forms.Label
    Friend WithEvents cboIdType As System.Windows.Forms.ComboBox
    Friend WithEvents pnlMembershipinfo As System.Windows.Forms.Panel
    Friend WithEvents gbMembershipInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboMemCategory As System.Windows.Forms.ComboBox
    Friend WithEvents objelLine4 As eZee.Common.eZeeLine
    Friend WithEvents btnDeleteMembership As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddMembership As eZee.Common.eZeeLightButton
    Friend WithEvents lblMemIssueDate As System.Windows.Forms.Label
    Friend WithEvents lblActivationDate As System.Windows.Forms.Label
    Friend WithEvents pnlMembershipDataInfo As System.Windows.Forms.Panel
    Friend WithEvents lvMembershipInfo As eZee.Common.eZeeListView
    Friend WithEvents colhMembershipCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMemIssueDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEditMembership As eZee.Common.eZeeLightButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblMemExpiryDate As System.Windows.Forms.Label
    Friend WithEvents lblMembershipName As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpMemIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMembershipNo As System.Windows.Forms.Label
    Friend WithEvents lblMembershipRemark As System.Windows.Forms.Label
    Friend WithEvents txtMembershipNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboMembershipType As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembershipType As System.Windows.Forms.Label
    Friend WithEvents pnlPersonalInfo As System.Windows.Forms.Panel
    Friend WithEvents gbAddress As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlPersonalInformation As System.Windows.Forms.Panel
    Friend WithEvents lnkCopyAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents lvIdendtifyInformation As System.Windows.Forms.ListView
    Friend WithEvents colhIdType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdSerialNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueCountry As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPlaceOfIssue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhExpiryDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIdTypeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCountryId As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDLClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDefault As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIDGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMemCatUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolMembershipUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddScale As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddShift As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddPayPoint As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddPayType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddEmploymentType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddTitle As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddIDType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddMemCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlEAllocation As System.Windows.Forms.Panel
    Friend WithEvents tabpOtherInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbLoginAndOtherDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtEmployeeEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkHolidayInfo As System.Windows.Forms.LinkLabel
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
    Friend WithEvents lblLoginName As System.Windows.Forms.Label
    Friend WithEvents txtLoginName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDisplayName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeEmail As System.Windows.Forms.Label
    Friend WithEvents lnkShiftInformation As System.Windows.Forms.LinkLabel
    Friend WithEvents gbOtherInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents txtHeight As eZee.TextBox.NumericTextBox
    Friend WithEvents txtSportHobbies As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpMaritalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblMaritalDate As System.Windows.Forms.Label
    Friend WithEvents lblSportsHobbies As System.Windows.Forms.Label
    Friend WithEvents cboMaritalStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lvDisabilities As System.Windows.Forms.ListView
    Friend WithEvents colhDisabilites As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblMaritalStatus As System.Windows.Forms.Label
    Friend WithEvents lblDisabilities As System.Windows.Forms.Label
    Friend WithEvents lvAllergis As System.Windows.Forms.ListView
    Friend WithEvents colhAllergies As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAllergies As System.Windows.Forms.Label
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblLanguage4 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage4 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage3 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage2 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage1 As System.Windows.Forms.Label
    Friend WithEvents cboLanguage1 As System.Windows.Forms.ComboBox
    Friend WithEvents txtExtraTel As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblExtraTel As System.Windows.Forms.Label
    Friend WithEvents lblHair As System.Windows.Forms.Label
    Friend WithEvents cboHair As System.Windows.Forms.ComboBox
    Friend WithEvents lblReligion As System.Windows.Forms.Label
    Friend WithEvents cboReligion As System.Windows.Forms.ComboBox
    Friend WithEvents lblEthinCity As System.Windows.Forms.Label
    Friend WithEvents cboEthincity As System.Windows.Forms.ComboBox
    Friend WithEvents lblNationality As System.Windows.Forms.Label
    Friend WithEvents cboNationality As System.Windows.Forms.ComboBox
    Friend WithEvents lblEyeColor As System.Windows.Forms.Label
    Friend WithEvents cboEyeColor As System.Windows.Forms.ComboBox
    Friend WithEvents lblBloodGroup As System.Windows.Forms.Label
    Friend WithEvents cboBloodGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblComplexion As System.Windows.Forms.Label
    Friend WithEvents cboComplexion As System.Windows.Forms.ComboBox
    Friend WithEvents objStOtherInfo As eZee.Common.eZeeStraightLine
    Friend WithEvents tabpEmergencyContact As System.Windows.Forms.TabPage
    Friend WithEvents pnlEmergencyContact As System.Windows.Forms.Panel
    Friend WithEvents gbEmergencyInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkCopyEAddress3 As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkCopyEAddress2 As System.Windows.Forms.LinkLabel
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents gbDatesDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEDates As System.Windows.Forms.Panel
    Friend WithEvents txtTerminateReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnAddRtirementReminder As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBirthReminder As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpRetirementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRetirementDate As System.Windows.Forms.Label
    Friend WithEvents dtpAppointdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAppointDate As System.Windows.Forms.Label
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpBirthdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEndEmplReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmplReason As System.Windows.Forms.Label
    Friend WithEvents lblBirtdate As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblSuspendedFrom As System.Windows.Forms.Label
    Friend WithEvents lblSuspendedTo As System.Windows.Forms.Label
    Friend WithEvents dtpSuspendedTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpProbationDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblProbationFrom As System.Windows.Forms.Label
    Friend WithEvents dtpLeavingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpProbationDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblLeavingDate As System.Windows.Forms.Label
    Friend WithEvents lblProbationTo As System.Windows.Forms.Label
    Friend WithEvents lnkView As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkClose As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpConfirmationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblConfirmationDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndEmplDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEmplDate As System.Windows.Forms.Label
    Friend WithEvents tabcDates_Remark As System.Windows.Forms.TabControl
    Friend WithEvents tabpDates As System.Windows.Forms.TabPage
    Friend WithEvents tabpRemark As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJobGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGrades As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents tapbClearance As System.Windows.Forms.TabPage
    Friend WithEvents gbClearanceInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlClearance As System.Windows.Forms.Panel
    Friend WithEvents lvClearance As eZee.Common.eZeeListView
    Friend WithEvents colhClGroupName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClParticulars As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkMarkClear As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnAddRPDate As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddRConfDate As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddRLeaveDate As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddREndDate As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddRSuspDate As eZee.Common.eZeeGradientButton
    Friend WithEvents txtAllocationReason As System.Windows.Forms.TextBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents dtpReinstatementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReinstatementDate As System.Windows.Forms.Label
    Friend WithEvents chkExclude As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhccategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhccategoryId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkActivateMembership As System.Windows.Forms.LinkLabel
    Friend WithEvents tabpShiftAssignment As System.Windows.Forms.TabPage
    Friend WithEvents pnlShift As System.Windows.Forms.Panel
    Friend WithEvents gbAssignedShift As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlShiftAssignment As System.Windows.Forms.Panel
    Friend WithEvents lblShiftAssignment As System.Windows.Forms.Label
    Friend WithEvents lvAssignedShift As eZee.Common.eZeeListView
    Friend WithEvents btnDeleteShift As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssignShift As eZee.Common.eZeeLightButton
    Friend WithEvents btnSetDefault As eZee.Common.eZeeLightButton
    Friend WithEvents btnRemoveDefault As eZee.Common.eZeeLightButton
    Friend WithEvents colhShiftType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhShiftName As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtShift As System.Windows.Forms.TextBox
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpFirstAppointDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblFirstAppointmentDate As System.Windows.Forms.Label
    Friend WithEvents dgvAssignedShift As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhEffective_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShiftType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShiftName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabpPolicy As System.Windows.Forms.TabPage
    Friend WithEvents pnlAssignedPolicy As System.Windows.Forms.Panel
    Friend WithEvents dgvPolicy As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhPEffective_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPolicy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents lblPolicy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkRecruitmentCopyAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScanAttachViewDoc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabcBWInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpBirthInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpWorkPermit As System.Windows.Forms.TabPage
    Friend WithEvents lblBirthVillage As System.Windows.Forms.Label
    Friend WithEvents lblBirthChiefdom As System.Windows.Forms.Label
    Friend WithEvents lblBirthTown As System.Windows.Forms.Label
    Friend WithEvents cboBrithVillage As System.Windows.Forms.ComboBox
    Friend WithEvents cboBrithTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboBrithChiefdom As System.Windows.Forms.ComboBox
    Friend WithEvents lnkCopyDomicileAddress As System.Windows.Forms.LinkLabel
    Friend WithEvents elResidentPermit As eZee.Common.eZeeLine
    Friend WithEvents txtResidentIssuePlace As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblResidentIssuePlace As System.Windows.Forms.Label
    Friend WithEvents txtResidentPermitNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpResidentExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResidentPermitNo As System.Windows.Forms.Label
    Friend WithEvents lblResidentExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboResidentIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents dtpResidentIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResidentIssueCountry As System.Windows.Forms.Label
    Friend WithEvents lblResidentIssueDate As System.Windows.Forms.Label
    Friend WithEvents elWorkPermit As eZee.Common.eZeeLine
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlImages As System.Windows.Forms.Panel
    Friend WithEvents objpnlButtons As System.Windows.Forms.Panel
    Friend WithEvents objbtnPhoto As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSignature As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblValue As System.Windows.Forms.Label
    Friend WithEvents imgSignature As eZee.Common.eZeeImageControl
    Friend WithEvents chkAssignDefaulTranHeads As System.Windows.Forms.CheckBox
    Friend WithEvents colhOperationType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhtranguid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objtblPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblParentData As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents gbRecruitmentAddress As System.Windows.Forms.GroupBox
    Friend WithEvents GbDomicileAddress As System.Windows.Forms.GroupBox
    Friend WithEvents GbPresentAddress As System.Windows.Forms.GroupBox
    Friend WithEvents lblPresentTown1 As System.Windows.Forms.Label
    Friend WithEvents cboPresentTown1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresentVillage As System.Windows.Forms.Label
    Friend WithEvents cboPresentVillage As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresentChiefdom As System.Windows.Forms.Label
    Friend WithEvents cboPresentChiefdom As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresentRoadStreet1 As System.Windows.Forms.Label
    Friend WithEvents cboPresentRoadStreet1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboPresentProvRegion1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersentProvince1 As System.Windows.Forms.Label
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblPresentState As System.Windows.Forms.Label
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblPostTown As System.Windows.Forms.Label
    Friend WithEvents txtRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRoad As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtAlternativeNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEstate As System.Windows.Forms.Label
    Friend WithEvents txtProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblProvince As System.Windows.Forms.Label
    Friend WithEvents lblAlternativeNo As System.Windows.Forms.Label
    Friend WithEvents txtMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPresentCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPloteNo As System.Windows.Forms.Label
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblPostCountry As System.Windows.Forms.Label
    Friend WithEvents lblPostcode As System.Windows.Forms.Label
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblDomicileTown1 As System.Windows.Forms.Label
    Friend WithEvents cboDomicileTown1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblDomicilevillage As System.Windows.Forms.Label
    Friend WithEvents cboDomicilevillage As System.Windows.Forms.ComboBox
    Friend WithEvents lblDomicileChiefdom As System.Windows.Forms.Label
    Friend WithEvents cboDomicileChiefdom As System.Windows.Forms.ComboBox
    Friend WithEvents lblDomicileRoadStreet1 As System.Windows.Forms.Label
    Friend WithEvents cboDomicileRoadStreet1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboDomicileProvince1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblDomicileProvince1 As System.Windows.Forms.Label
    Friend WithEvents cboDomicileState As System.Windows.Forms.ComboBox
    Friend WithEvents lblDomicileState As System.Windows.Forms.Label
    Friend WithEvents lblDomicileAddress2 As System.Windows.Forms.Label
    Friend WithEvents cboDomicilePostCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboDomicileTown As System.Windows.Forms.ComboBox
    Friend WithEvents txtDomicileAltNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileAltNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicilePlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicilePlotNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileProvince As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostCode As System.Windows.Forms.Label
    Friend WithEvents txtDomicileFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileFax As System.Windows.Forms.Label
    Friend WithEvents txtDomicileMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileMobileNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileTelNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDomicileCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtDomicileAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileRoad As System.Windows.Forms.Label
    Friend WithEvents lblDomicileEState As System.Windows.Forms.Label
    Friend WithEvents lblDomicileEmail As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostTown As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostCountry As System.Windows.Forms.Label
    Friend WithEvents txtDomicileAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileAddress1 As System.Windows.Forms.Label
    Friend WithEvents lblRecruitmentTown1 As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentTown1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblRecruitmentVillage As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentVillage As System.Windows.Forms.ComboBox
    Friend WithEvents lblRecruitmentChiefdom As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentChiefdom As System.Windows.Forms.ComboBox
    Friend WithEvents lblRecruitmentRoadStreet1 As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentRoadStreet1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboRecruitmentProvince1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblRecruitmentProvince1 As System.Windows.Forms.Label
    Friend WithEvents txtRecruitmentPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRPlotNo As System.Windows.Forms.Label
    Friend WithEvents txtRecruitmentTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRTelNo As System.Windows.Forms.Label
    Friend WithEvents txtRecruitmentProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRRegion As System.Windows.Forms.Label
    Friend WithEvents txtRecruitmentEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtRecruitmentRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRStreet As System.Windows.Forms.Label
    Friend WithEvents lblREState As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentState As System.Windows.Forms.ComboBox
    Friend WithEvents lblRState As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboRecruitmentTown As System.Windows.Forms.ComboBox
    Friend WithEvents lblRCode As System.Windows.Forms.Label
    Friend WithEvents lblRCity As System.Windows.Forms.Label
    Friend WithEvents lblRAddress2 As System.Windows.Forms.Label
    Friend WithEvents cboRecruitmentCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtRecruitmentAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRCountry As System.Windows.Forms.Label
    Friend WithEvents txtRecruitmentAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRAddress1 As System.Windows.Forms.Label
    Friend WithEvents lblpresentaddressapproval As System.Windows.Forms.Label
    Friend WithEvents lbldomicileaddressapproval As System.Windows.Forms.Label
    Friend WithEvents lblrecruitmentaddressapproval As System.Windows.Forms.Label
    Friend WithEvents GbEmergencyContact3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtEmgEmail3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress3 As System.Windows.Forms.Label
    Friend WithEvents cboEmgCountry3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCountry3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgState3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgEstate3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgState3 As System.Windows.Forms.Label
    Friend WithEvents cboEmgTown3 As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmgRoad3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgPostCode3 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCode3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad3 As System.Windows.Forms.Label
    Friend WithEvents lblEAddressapproval3 As System.Windows.Forms.Label
    Friend WithEvents GbEmergencyContact2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtEmgEmail2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress2 As System.Windows.Forms.Label
    Friend WithEvents cboEmgCountry2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCountry2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgState2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgEstate2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgState2 As System.Windows.Forms.Label
    Friend WithEvents cboEmgTown2 As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmgRoad2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgPostCode2 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCode2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad2 As System.Windows.Forms.Label
    Friend WithEvents GbEmergencyContact1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblEAddressapproval2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress As System.Windows.Forms.Label
    Friend WithEvents cboEmgCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCountry As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgState As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgEstate As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown As System.Windows.Forms.Label
    Friend WithEvents lblEmgState As System.Windows.Forms.Label
    Friend WithEvents cboEmgTown As System.Windows.Forms.ComboBox
    Friend WithEvents txtEmgRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmgPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCode As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad As System.Windows.Forms.Label
    Friend WithEvents lblEAddressapproval1 As System.Windows.Forms.Label
    Friend WithEvents colhMOperationType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMtranguid As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblotherinfoapproval As System.Windows.Forms.Label
    Friend WithEvents lblbirthinfoapproval As System.Windows.Forms.Label
    Friend WithEvents lnkIdScanAttachDoc As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachPresent As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachDomicile As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachRecruitment As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachEmerContact1 As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachEmerContact2 As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachEmerContact3 As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAttachBirthInfo As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkIdAttachOtherInfo As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPassword As System.Windows.Forms.Label
End Class
