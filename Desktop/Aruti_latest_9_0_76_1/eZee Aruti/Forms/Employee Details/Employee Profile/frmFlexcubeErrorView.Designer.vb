﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFlexcubeErrorView
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFlexcubeErrorView))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbFilter_Criteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblReqDateTo = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblReqDateFrom = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboResponseType = New System.Windows.Forms.ComboBox
        Me.lblResponseType = New System.Windows.Forms.Label
        Me.lblRequestType = New System.Windows.Forms.Label
        Me.cboRequestType = New System.Windows.Forms.ComboBox
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objlnkValue = New System.Windows.Forms.LinkLabel
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnExportData = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUserId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNames = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTermDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLvFormNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLvStDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLvRtDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReqDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFlexMsg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        Me.gbFilter_Criteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployee.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbFilter_Criteria)
        Me.Panel1.Controls.Add(Me.gbEmployee)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(837, 492)
        Me.Panel1.TabIndex = 0
        '
        'gbFilter_Criteria
        '
        Me.gbFilter_Criteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilter_Criteria.Checked = False
        Me.gbFilter_Criteria.CollapseAllExceptThis = False
        Me.gbFilter_Criteria.CollapsedHoverImage = Nothing
        Me.gbFilter_Criteria.CollapsedNormalImage = Nothing
        Me.gbFilter_Criteria.CollapsedPressedImage = Nothing
        Me.gbFilter_Criteria.CollapseOnLoad = False
        Me.gbFilter_Criteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilter_Criteria.Controls.Add(Me.lblReqDateTo)
        Me.gbFilter_Criteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilter_Criteria.Controls.Add(Me.lblReqDateFrom)
        Me.gbFilter_Criteria.Controls.Add(Me.objbtnReset)
        Me.gbFilter_Criteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilter_Criteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilter_Criteria.Controls.Add(Me.cboResponseType)
        Me.gbFilter_Criteria.Controls.Add(Me.lblResponseType)
        Me.gbFilter_Criteria.Controls.Add(Me.lblRequestType)
        Me.gbFilter_Criteria.Controls.Add(Me.cboRequestType)
        Me.gbFilter_Criteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilter_Criteria.ExpandedHoverImage = Nothing
        Me.gbFilter_Criteria.ExpandedNormalImage = Nothing
        Me.gbFilter_Criteria.ExpandedPressedImage = Nothing
        Me.gbFilter_Criteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter_Criteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter_Criteria.HeaderHeight = 25
        Me.gbFilter_Criteria.HeaderMessage = ""
        Me.gbFilter_Criteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilter_Criteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter_Criteria.HeightOnCollapse = 0
        Me.gbFilter_Criteria.LeftTextSpace = 0
        Me.gbFilter_Criteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilter_Criteria.Name = "gbFilter_Criteria"
        Me.gbFilter_Criteria.OpenHeight = 300
        Me.gbFilter_Criteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter_Criteria.ShowBorder = True
        Me.gbFilter_Criteria.ShowCheckBox = False
        Me.gbFilter_Criteria.ShowCollapseButton = False
        Me.gbFilter_Criteria.ShowDefaultBorderColor = True
        Me.gbFilter_Criteria.ShowDownButton = False
        Me.gbFilter_Criteria.ShowHeader = True
        Me.gbFilter_Criteria.Size = New System.Drawing.Size(837, 69)
        Me.gbFilter_Criteria.TabIndex = 19
        Me.gbFilter_Criteria.Temp = 0
        Me.gbFilter_Criteria.Text = "Filter Criteria"
        Me.gbFilter_Criteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(716, 36)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpEndDate.TabIndex = 320
        '
        'lblReqDateTo
        '
        Me.lblReqDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReqDateTo.Location = New System.Drawing.Point(676, 38)
        Me.lblReqDateTo.Name = "lblReqDateTo"
        Me.lblReqDateTo.Size = New System.Drawing.Size(34, 16)
        Me.lblReqDateTo.TabIndex = 319
        Me.lblReqDateTo.Text = "To"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(561, 36)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpStartDate.TabIndex = 318
        '
        'lblReqDateFrom
        '
        Me.lblReqDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReqDateFrom.Location = New System.Drawing.Point(487, 38)
        Me.lblReqDateFrom.Name = "lblReqDateFrom"
        Me.lblReqDateFrom.Size = New System.Drawing.Size(70, 16)
        Me.lblReqDateFrom.TabIndex = 317
        Me.lblReqDateFrom.Text = "From Date"
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(812, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 316
        Me.objbtnReset.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(700, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 305
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(786, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 315
        Me.objbtnSearch.TabStop = False
        '
        'cboResponseType
        '
        Me.cboResponseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResponseType.DropDownWidth = 180
        Me.cboResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResponseType.FormattingEnabled = True
        Me.cboResponseType.Location = New System.Drawing.Point(368, 36)
        Me.cboResponseType.Name = "cboResponseType"
        Me.cboResponseType.Size = New System.Drawing.Size(113, 21)
        Me.cboResponseType.TabIndex = 313
        '
        'lblResponseType
        '
        Me.lblResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponseType.Location = New System.Drawing.Point(276, 38)
        Me.lblResponseType.Name = "lblResponseType"
        Me.lblResponseType.Size = New System.Drawing.Size(88, 16)
        Me.lblResponseType.TabIndex = 312
        Me.lblResponseType.Text = "Response Type"
        '
        'lblRequestType
        '
        Me.lblRequestType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequestType.Location = New System.Drawing.Point(9, 38)
        Me.lblRequestType.Name = "lblRequestType"
        Me.lblRequestType.Size = New System.Drawing.Size(82, 16)
        Me.lblRequestType.TabIndex = 84
        Me.lblRequestType.Text = "Request Type"
        '
        'cboRequestType
        '
        Me.cboRequestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequestType.DropDownWidth = 180
        Me.cboRequestType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequestType.FormattingEnabled = True
        Me.cboRequestType.Location = New System.Drawing.Point(97, 36)
        Me.cboRequestType.Name = "cboRequestType"
        Me.cboRequestType.Size = New System.Drawing.Size(173, 21)
        Me.cboRequestType.TabIndex = 83
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbEmployee.Controls.Add(Me.objlnkValue)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(0, 70)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(836, 371)
        Me.gbEmployee.TabIndex = 308
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.Text = "Employee"
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(0, 26)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(835, 345)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(829, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(829, 313)
        Me.objpnlEmp.TabIndex = 107
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 22
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhUserId, Me.dgcolhNames, Me.dgcolhTermDate, Me.dgcolhLvFormNo, Me.dgcolhLvStDate, Me.dgcolhLvRtDate, Me.dgcolhReqDate, Me.dgcolhRefNo, Me.dgcolhStatus, Me.dgcolhFlexMsg})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(829, 313)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objlnkValue
        '
        Me.objlnkValue.BackColor = System.Drawing.Color.Transparent
        Me.objlnkValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkValue.ForeColor = System.Drawing.Color.White
        Me.objlnkValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkValue.LinkColor = System.Drawing.SystemColors.ControlText
        Me.objlnkValue.Location = New System.Drawing.Point(71, 317)
        Me.objlnkValue.Name = "objlnkValue"
        Me.objlnkValue.Size = New System.Drawing.Size(423, 17)
        Me.objlnkValue.TabIndex = 308
        Me.objlnkValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnExportData)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 442)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(837, 50)
        Me.EZeeFooter1.TabIndex = 311
        '
        'btnExportData
        '
        Me.btnExportData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExportData.BackColor = System.Drawing.Color.White
        Me.btnExportData.BackgroundImage = CType(resources.GetObject("btnExportData.BackgroundImage"), System.Drawing.Image)
        Me.btnExportData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExportData.BorderColor = System.Drawing.Color.Empty
        Me.btnExportData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExportData.FlatAppearance.BorderSize = 0
        Me.btnExportData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExportData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportData.ForeColor = System.Drawing.Color.Black
        Me.btnExportData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExportData.GradientForeColor = System.Drawing.Color.Black
        Me.btnExportData.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportData.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExportData.Location = New System.Drawing.Point(10, 10)
        Me.btnExportData.Name = "btnExportData"
        Me.btnExportData.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExportData.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExportData.Size = New System.Drawing.Size(97, 30)
        Me.btnExportData.TabIndex = 33
        Me.btnExportData.Text = "&Export"
        Me.btnExportData.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(728, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Request Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Time"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Message Id"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Error Description"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 450
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "File Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 50
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 250
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 250
        '
        'dgcolhUserId
        '
        Me.dgcolhUserId.HeaderText = "User Id"
        Me.dgcolhUserId.Name = "dgcolhUserId"
        Me.dgcolhUserId.ReadOnly = True
        '
        'dgcolhNames
        '
        Me.dgcolhNames.HeaderText = "Names"
        Me.dgcolhNames.Name = "dgcolhNames"
        Me.dgcolhNames.ReadOnly = True
        Me.dgcolhNames.Width = 150
        '
        'dgcolhTermDate
        '
        Me.dgcolhTermDate.HeaderText = "Exit Date"
        Me.dgcolhTermDate.Name = "dgcolhTermDate"
        Me.dgcolhTermDate.ReadOnly = True
        '
        'dgcolhLvFormNo
        '
        Me.dgcolhLvFormNo.HeaderText = "Leave Form No"
        Me.dgcolhLvFormNo.Name = "dgcolhLvFormNo"
        Me.dgcolhLvFormNo.ReadOnly = True
        '
        'dgcolhLvStDate
        '
        Me.dgcolhLvStDate.HeaderText = "Leave Start Date"
        Me.dgcolhLvStDate.Name = "dgcolhLvStDate"
        Me.dgcolhLvStDate.ReadOnly = True
        '
        'dgcolhLvRtDate
        '
        Me.dgcolhLvRtDate.HeaderText = "Leave End Date"
        Me.dgcolhLvRtDate.Name = "dgcolhLvRtDate"
        Me.dgcolhLvRtDate.ReadOnly = True
        '
        'dgcolhReqDate
        '
        Me.dgcolhReqDate.HeaderText = "Date"
        Me.dgcolhReqDate.Name = "dgcolhReqDate"
        Me.dgcolhReqDate.ReadOnly = True
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Refrence No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.Width = 50
        '
        'dgcolhFlexMsg
        '
        Me.dgcolhFlexMsg.HeaderText = "Message"
        Me.dgcolhFlexMsg.Name = "dgcolhFlexMsg"
        Me.dgcolhFlexMsg.ReadOnly = True
        Me.dgcolhFlexMsg.Width = 250
        '
        'frmFlexcubeErrorView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(837, 492)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFlexcubeErrorView"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Flexcube Data"
        Me.Panel1.ResumeLayout(False)
        Me.gbFilter_Criteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objlnkValue As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilter_Criteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblRequestType As System.Windows.Forms.Label
    Friend WithEvents cboRequestType As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboResponseType As System.Windows.Forms.ComboBox
    Friend WithEvents lblResponseType As System.Windows.Forms.Label
    Friend WithEvents btnExportData As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReqDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReqDateFrom As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUserId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTermDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLvFormNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLvStDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLvRtDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReqDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFlexMsg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
