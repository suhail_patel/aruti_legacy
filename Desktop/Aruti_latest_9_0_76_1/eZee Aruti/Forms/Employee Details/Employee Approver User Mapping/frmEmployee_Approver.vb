﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployee_Approver

#Region " Private Variables "

    Private mstrModuleName As String = "frmEmployee_Approver"
    Private menAction As enAction = enAction.ADD_ONE
    Private objUserMapping As clsemp_appUsermapping
    Private mblnCancel As Boolean = True
    Private eApproverType As enEmpApproverType

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal eApprType As enEmpApproverType) As Boolean
        Try
            menAction = eAction
            eApproverType = eApprType
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Try
            Dim objApproverLevel As New clsempapproverlevel_master
            dsCombo = objApproverLevel.getListForCombo(eApproverType, "List", True)
            cboApproveLevel.ValueMember = "empapplevelunkid"
            cboApproveLevel.DisplayMember = "name"
            cboApproveLevel.DataSource = dsCombo.Tables(0)
            objApproverLevel = Nothing

            Dim objUsr As New clsUserAddEdit
            If eApproverType = enEmpApproverType.APPR_EMPLOYEE Then
                dsCombo = objUsr.getNewComboList("User", 0, True, Company._Object._Companyunkid, CStr(344), FinancialYear._Object._YearUnkid, True)  '344 - Allow to Mark Employee Active
            ElseIf eApproverType = enEmpApproverType.APPR_MOVEMENT Then
                Dim strPrivilegeIds As String = CInt(enUserPriviledge.AllowToApproveRejectEmployeeTransfers) & "," & CInt(enUserPriviledge.AllowToApproveRejectEmployeeRecategorize) & "," & _
                CInt(enUserPriviledge.AllowToApproveRejectEmployeeConfirmation) & "," & CInt(enUserPriviledge.AllowToApproveRejectEmployeeSuspension) & "," & CInt(enUserPriviledge.AllowToApproveRejectEmployeeTermination) & "," & _
                CInt(enUserPriviledge.AllowToApproveRejectEmployeeRetirements) & "," & CInt(enUserPriviledge.AllowToApproveRejectEmployeeWorkPermit) & "," & CInt(enUserPriviledge.AllowToApproveRejectEmployeeResidentPermit) & "," & _
                CInt(enUserPriviledge.AllowToApproveRejectEmployeeCostCenter)
                '1192 '1193 '1194 '1195 '1196 '1197 '1198 '1199 '1200 '1201
                dsCombo = objUsr.getNewComboList("User", 0, True, Company._Object._Companyunkid, strPrivilegeIds, FinancialYear._Object._YearUnkid, True)  '344 - Allow to Mark Employee Active
            End If
            cboUser.ValueMember = "userunkid"
            cboUser.DisplayMember = "name"
            cboUser.DataSource = dsCombo.Tables(0)
            objUsr = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboApproveLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Approver Level is compulsory information.please select approver level."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboApproveLevel.Select()
                Return False
            ElseIf CInt(cboUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "User is compulsory information.please select user."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboUser.Select()
                Return False
            End If
            Dim strMessage As String = GetParentNodes()
            If strMessage.Trim.Length > 0 Then
                eZeeMsgBox.Show(strMessage, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboUser.Select()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    Private Sub SetValue()
        Try
            objUserMapping._Empapplevelunkid = CInt(cboApproveLevel.SelectedValue)
            objUserMapping._Mapuserunkid = CInt(cboUser.SelectedValue)
            objUserMapping._Isactive = True
            objUserMapping._Isvoid = False
            objUserMapping._FormName = mstrModuleName
            objUserMapping._ClientIP = getIP()
            objUserMapping._HostName = getHostName()
            objUserMapping._IsFromWeb = False
            objUserMapping._AuditUserId = User._Object._Userunkid
            objUserMapping._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objUserMapping._Approvertypeid = eApproverType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillUserAccess(ByVal dtUserAccess As DataTable)
        Try
            tvUserAccess.Nodes.Clear()

            If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                Dim xAllocationID As Integer = 0
                Dim nGroupNode As New TreeNode
                Dim nCurrentNode As New TreeNode
                For i As Integer = 0 To dtUserAccess.Rows.Count - 1
                    If CBool(dtUserAccess.Rows(i)("IsGrp")) = True Then
                        If xAllocationID <> CInt(dtUserAccess.Rows(i)("AllocationRefId")) Then
                            nGroupNode = New TreeNode
                            nGroupNode = tvUserAccess.Nodes.Add(dtUserAccess.Rows(i)("AllocationRefId").ToString(), dtUserAccess.Rows(i)("UserAccess").ToString().Trim())
                            nGroupNode.NodeFont = New Font(tvUserAccess.Font, FontStyle.Bold)
                            nGroupNode.Text = dtUserAccess.Rows(i)("UserAccess").ToString().Trim()
                            xAllocationID = CInt(dtUserAccess.Rows(i)("AllocationRefId"))
                            nGroupNode.Tag = xAllocationID
                            Continue For
                        End If
                    End If
                    nCurrentNode = nGroupNode.Nodes.Add(dtUserAccess.Rows(i)("AllocationId").ToString(), dtUserAccess.Rows(i)("UserAccess").ToString().Trim())
                    nCurrentNode.Tag = CInt(dtUserAccess.Rows(i)("AllocationId"))
                Next
                tvUserAccess.ExpandAll()
                If tvUserAccess.Nodes.Count > 0 Then
                    tvUserAccess.TopNode = tvUserAccess.Nodes(0)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Function GetParentNodes() As String
        Dim strMsg As String = String.Empty
        Try
            Dim objMaster As New clsMasterData
            Dim dsAllocation As New DataSet
            Dim strAllocIds As String = ""
            strAllocIds = CInt(enAllocation.DEPARTMENT).ToString & "," & CInt(enAllocation.JOBS).ToString & "," & CInt(enAllocation.CLASS_GROUP).ToString & "," & CInt(enAllocation.CLASSES).ToString
            dsAllocation = objMaster.GetEAllocation_Notification("List", strAllocIds)
            For Each node As TreeNode In tvUserAccess.Nodes
                Dim result As List(Of TreeNode) = GetNodes(node)
                If result.Count <= 0 Then
                    Dim strCaption As String = ""
                    Try
                        strCaption = dsAllocation.Tables(0).Select("Id = '" & node.Tag.ToString() & "'")(0)("Name").ToString()
                    Catch ex As Exception
                        strCaption = node.Text
                    End Try
                    strMsg = Language.getMessage(mstrModuleName, 4, "Sorry, Selected user does not have any user access assigned in" & " " & strCaption & ", ") & _
                             Language.getMessage(mstrModuleName, 5, "Please assign atleast one allocation to set it as approver.")
                    Exit For
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetParentNodes", mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function

    Private Function GetNodes(ByVal parentNode As TreeNode) As List(Of TreeNode)
        Dim results As List(Of TreeNode) = New List(Of TreeNode)()
        If parentNode.Nodes.Count > 0 Then
            results.Add(parentNode)
            For Each node As TreeNode In parentNode.Nodes
                results.AddRange(GetNodes(node))
            Next
        End If
        Return results
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmployee_Approver_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objUserMapping = New clsemp_appUsermapping()
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Set_Logo(Me, gApplicationType)
            Fill_Combo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_Approver_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseApprover_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboApproveLevel.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboApproveLevel.ValueMember
                    .DisplayMember = cboApproveLevel.DisplayMember
                    .CodeMember = "code"
                    .DataSource = CType(cboApproveLevel.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboUser.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub

            SetValue()

            Dim blnFlag As Boolean = objUserMapping.Insert()

            If blnFlag = False And objUserMapping._Message <> "" Then
                eZeeMsgBox.Show(objUserMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Appover mapping done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objUserMapping = Nothing
                    objUserMapping = New clsemp_appUsermapping
                    cboApproveLevel.Select()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Dim k As Integer = 0
            Dim FirstNode As TreeNode = Nothing
            tvUserAccess.SelectedNode = Nothing
            Dim blnSearch As Boolean = False
            Dim tnc As TreeNodeCollection
            tnc = tvUserAccess.Nodes
            If TxtSearchUserAccess.Text.Length > 0 Then
                For i = 0 To tvUserAccess.Nodes.Count - 1
                    tnc(i).Collapse()
                    For j = 0 To tvUserAccess.Nodes(i).Nodes.Count - 1
                        tnc(i).Nodes(j).BackColor = Color.White
                        If tnc(i).Nodes(j).Text.Trim.ToUpper.Contains(TxtSearchUserAccess.Text.Trim.ToUpper) Then
                            tnc(i).Nodes(j).BackColor = Color.PeachPuff
                            If k = 0 Then
                                FirstNode = tnc(i).Nodes(j)
                                k += 1
                            End If
                            tnc(i).Expand()
                            blnSearch = True
                        End If
                    Next
                Next

                If blnSearch = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Not found. Please try again with correct text."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    TxtSearchUserAccess.SelectAll()
                    Exit Sub
                Else
                    If FirstNode IsNot Nothing Then tvUserAccess.SelectedNode = FirstNode
                End If

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "There is no text for searching User access(s).Please Enter text for searching User access(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                TxtSearchUserAccess.Select()
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Dim tnc As TreeNodeCollection
            tnc = tvUserAccess.Nodes
            For i = 0 To tvUserAccess.Nodes.Count - 1
                For j = 0 To tvUserAccess.Nodes(i).Nodes.Count - 1
                    tnc(i).Nodes(j).BackColor = Color.White
                Next
            Next
            tvUserAccess.ExpandAll()
            TxtSearchUserAccess.Text = ""
            tvUserAccess.SelectedNode = Nothing
            TxtSearchUserAccess.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub cboUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUser.SelectedIndexChanged
        Dim objUser As New clsUserAddEdit
        Try
            Dim dtTable As DataTable = Nothing
            If CInt(cboUser.SelectedValue) > 0 Then
                dtTable = objUser.GetUserAccessList(ConfigParameter._Object._UserAccessModeSetting, CInt(cboUser.SelectedValue), Company._Object._Companyunkid)
                dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            End If
            FillUserAccess(dtTable)
            dtTable = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUser_SelectedIndexChanged", mstrModuleName)
        Finally
            objUser = Nothing
        End Try
    End Sub

#End Region

#Region "Textbox Event"

   
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
			Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
			Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.Name, Me.lblApproveLevel.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Approver Level is compulsory information.please select approver level.")
			Language.setMessage(mstrModuleName, 2, "User is compulsory information.please select user.")
			Language.setMessage(mstrModuleName, 3, "Employee Appover mapping done successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

  

End Class