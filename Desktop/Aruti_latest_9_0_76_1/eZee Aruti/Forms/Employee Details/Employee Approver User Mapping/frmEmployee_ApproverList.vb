﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmEmployee_ApproverList

#Region "Private Variable"

    Private objApprover As clsemp_appUsermapping
    Private ReadOnly mstrModuleName As String = "frmEmployee_ApproverList"
    Private mintUserMappunkid As Integer = -1
    Private eApproverType As enEmpApproverType

#End Region

#Region " Property "

    Public WriteOnly Property _ApproverType() As enEmpApproverType
        Set(ByVal value As enEmpApproverType)
            eApproverType = value
        End Set
    End Property

#End Region

#Region "Form's Event"

    Private Sub frmEmployee_ApproverList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprover = New clsemp_appUsermapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            Call SetVisibility()
            If lvApproverList.Items.Count > 0 Then lvApproverList.Items(0).Selected = True
            lvApproverList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_ApproverList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_ApproverList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployee_ApproverList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployee_ApproverList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objApprover = Nothing
    End Sub


    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemp_appUsermapping.SetMessages()
            objfrm._Other_ModuleNames = "clsemp_appUsermapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmEmployee_Approver
            If ObjFrm.displayDialog(enAction.ADD_CONTINUE, eApproverType) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverList.Select()
            Exit Sub
        End If
        'If objApprover.isUsed(CInt(lvApproverList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information) '?2
        '    lvApproverList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objApprover._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objApprover._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objApprover._Voiduserunkid = User._Object._Userunkid
                objApprover._FormName = mstrModuleName
                objApprover._ClientIP = getIP()
                objApprover._HostName = getHostName()
                objApprover._IsFromWeb = False
                objApprover._AuditUserId = User._Object._Userunkid
                objApprover._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime

                objApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag))
                lvApproverList.SelectedItems(0).Remove()

                If lvApproverList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverList.Items.Count - 1
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverList.Items.Count <> 0 Then
                    lvApproverList.Items(intSelectedIndex).Selected = True
                    lvApproverList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboUser.DataSource, DataTable)
            With cboUser
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLevel.SelectedIndex = 0
            cboUser.SelectedIndex = 0
            cboStatus.SelectedValue = 1
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLevel.DataSource, DataTable)
            With cboLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsApproverList As New DataSet
        Dim strSearching As String = ""
        Try


            If User._Object.Privilege._AllowToViewEmployeeApproverList = True Then

                If CInt(cboLevel.SelectedValue) > 0 Then
                    strSearching &= "AND hremp_appusermapping.empapplevelunkid = " & CInt(cboLevel.SelectedValue)
                End If

                If CInt(cboUser.SelectedValue) > 0 Then
                    strSearching &= "AND hremp_appusermapping.mapuserunkid = " & CInt(cboUser.SelectedValue)
                End If


                dsApproverList = objApprover.GetList(eApproverType, "List", CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)), strSearching)

                Dim lvItem As ListViewItem

                lvApproverList.Items.Clear()
                For Each drRow As DataRow In dsApproverList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("Level").ToString
                    lvItem.Tag = drRow("mappingunkid")
                    lvItem.SubItems.Add(drRow("User").ToString)
                    lvItem.SubItems.Add(drRow("Status").ToString)
                    lvApproverList.Items.Add(lvItem)
                Next

                lvApproverList.GroupingColumn = colhLevel
                lvApproverList.SortBy(colhLevel.Index, SortOrder.Ascending)
                lvApproverList.DisplayGroups(True)

                If lvApproverList.Items.Count > 16 Then
                    colhStatus.Width = 200 - 20
                Else
                    colhStatus.Width = 200
                End If

                If CInt(cboStatus.SelectedValue) = 1 Then
                    Call SetVisibility()
                Else
                    btnDelete.Enabled = False
                End If

            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsApproverList.Dispose()
        End Try

    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddEmployeeApprover
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmployeeApprover
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objLevel As New clsempapproverlevel_master
            dsList = objLevel.getListForCombo(eApproverType, "Level", True)
            cboLevel.DisplayMember = "name"
            cboLevel.ValueMember = "empapplevelunkid"
            cboLevel.DataSource = dsList.Tables(0)
            objLevel = Nothing

            Dim objUsr As New clsUserAddEdit

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsList = objUsr.getNewComboList("User", 0, True, Company._Object._Companyunkid, 344, FinancialYear._Object._YearUnkid, True)  '344 - Allow to Mark Employee Active
            dsList = objUsr.getNewComboList("User", 0, True, Company._Object._Companyunkid, CStr(344), FinancialYear._Object._YearUnkid, True)  '344 - Allow to Mark Employee Active
            'S.SANDEEP [20-JUN-2018] -- END

            cboUser.ValueMember = "userunkid"
            cboUser.DisplayMember = "name"
            cboUser.DataSource = dsList.Tables(0)
            objUsr = Nothing

            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 1
            End With
            objMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


#End Region


#Region "Menu Event"

    Private Sub mnuInActiveApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            'Dim objLeavForm As New clsleaveform
            'If objLeavForm.GetApproverPendingLeaveFormCount(CInt(lvApproverList.SelectedItems(0).Tag), "") > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You cannot inactivate this approver.Reason :This Approver has Pending Leave Application Form(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'objLeavForm = Nothing

            objApprover._FormName = mstrModuleName
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._IsFromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
            objApprover._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime

            If objApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, False) = False Then
                eZeeMsgBox.Show(objApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuInActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuActiveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            objApprover._FormName = mstrModuleName
            objApprover._ClientIP = getIP()
            objApprover._HostName = getHostName()
            objApprover._IsFromWeb = False
            objApprover._AuditUserId = User._Object._Userunkid
            objApprover._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime

            If objApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, True) = False Then
                eZeeMsgBox.Show(objApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                mnuInActiveApprover.Enabled = User._Object.Privilege._AllowtoSetInactiveEmployeeApprover
                mnuActiveApprover.Enabled = False
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = User._Object.Privilege._AllowToSetActiveEmployeeApprover
            Else
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = False
            End If
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOpearation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpearation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
			Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.btnOpearation.Text = Language._Object.getCaption(Me.btnOpearation.Name, Me.btnOpearation.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.mnuInActiveApprover.Text = Language._Object.getCaption(Me.mnuInActiveApprover.Name, Me.mnuInActiveApprover.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.mnuActiveApprover.Text = Language._Object.getCaption(Me.mnuActiveApprover.Name, Me.mnuActiveApprover.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class