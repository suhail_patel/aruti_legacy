﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Runtime.InteropServices

'Last Message Index= 14

Public Class frmfingerPrint_enroll

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmfingerPrint_enroll"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objFingerPrint As clsEmployee_Finger
    Dim objfingerDevice As FingerPrintDevice
    ' Dim mintMachineSrNo As Integer = 1
    Dim objConnection As Object = Nothing
    Dim mblnIsEnroll As Boolean = False
    Dim intFingerIndex As Integer = -1
    Dim intUserid As Integer = -1
    Dim objEmp As clsEmployee_Master

    'Sohail (02 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Dim m_Handle As Integer = 0
    Public Const TEMPLATE_SIZE As Integer = 384
    Public Const BS_MAX_TEMPLATE_PER_USER As Integer = 10

    Private m_TemplateData As Byte() = Nothing    'for finger template
    Dim mstrUserCardID As String = ""
    Dim mstrCardCustomID As String = ""
    Dim mstrAccessGroup As String = "ffffffff"
    'Sohail (02 Nov 2013) -- End

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Dim dsMachineSetting As New DataSet
    Dim objDictConnection As New Dictionary(Of String, Object)
    'Pinkal (20-Jan-2012) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Form's Event"

    Private Sub frmfingerPrint_enroll_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFingerPrint = New clsEmployee_Finger
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1
                    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 1 Then   'FingerPrint
                lblfingerIndex.Visible = True
                cboFingerIndex.Visible = True
                lblCardNo.Visible = False
                txtCardNo.Visible = False
                btnStartEnroll.Visible = True

                    ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 2 Then  'RFID
                lblfingerIndex.Visible = False
                cboFingerIndex.Visible = False
                lblCardNo.Visible = True
                txtCardNo.Visible = True
                btnStartEnroll.Visible = False

            End If

                    If CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString()) = 3 Then  'ZK SOFTWARE
                        cboFingerIndex.SelectedIndex = 0
                    End If

                Next
            End If


            InitializeDevice()
            FillCombo()

            'If ConfigParameter._Object._DeviceId = enFingerPrintDevice.ZKSoftware Then
            '    cboFingerIndex.SelectedIndex = 0
            'End If
            cboDeviceCode.Select()

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmfingerPrint_enroll_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmfingerPrint_enroll_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
        If objConnection IsNot Nothing Then
            CType(objConnection, zkemkeeper.CZKEM).Disconnect()
            GC.RemoveMemoryPressure(objConnection.ToString.Length)
            objConnection = Nothing
        End If
        objfingerDevice = Nothing
        objFingerPrint = Nothing
            Me.Dispose(True)

        Catch ex As AccessViolationException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Finger.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Finger"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Private Methods"

    Private Sub InitializeDevice()
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then


                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    'Dim DeviceId As enFingerPrintDevice = CType(ConfigParameter._Object._DeviceId, enFingerPrintDevice)
                    Dim DeviceId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                    'Dim ConnetionTypeId As enFingerPrintConnetionType = CType(ConfigParameter._Object._ConnectionTypeId, enFingerPrintConnetionType)
                    Dim ConnetionTypeId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("connectiontypeid").ToString())

                    'If dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString() <> "" Then mintMachineSrNo = CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString())


            If DeviceId > 0 Then

                objfingerDevice = New FingerPrintDevice

                Select Case DeviceId

                            Case enFingerPrintDevice.DigitalPersona     'DigitalPersona

                            Case enFingerPrintDevice.SecuGen     'SecuGen

                            Case enFingerPrintDevice.ZKSoftware   'ZKSoftware
                                objConnection = CType(objfingerDevice.SearchDevice(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), dsMachineSetting.Tables(0).Rows(i)("port").ToString(), DeviceId, ConnetionTypeId), zkemkeeper.CZKEM)

                        If objConnection IsNot Nothing Then
                                    If CType(objConnection, zkemkeeper.CZKEM).RegEvent(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), 65535) Then   'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                                        If objDictConnection.ContainsKey(dsMachineSetting.Tables(0).Rows(i)("ip").ToString()) = False Then
                                            objDictConnection.Add(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), objConnection)
                                        End If


                                        'If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then   'RFID
                                        '    AddHandler CType(objConnection, zkemkeeper.CZKEM).OnHIDNum, AddressOf Zksoftware_OnHIDNum
                                        '    AddHandler CType(objConnection, zkemkeeper.CZKEM).OnVerify, AddressOf Zksoftware_OnVerify
                                        'End If

                            End If

                        End If

                                'Sohail (02 Nov 2013) -- Start
                                'TRA - ENHANCEMENT
                            Case enFingerPrintDevice.BioStar 'BioStar
                                Dim result As Integer

                                result = clsBioStar.BS_OpenSocket(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), CInt(dsMachineSetting.Tables(0).Rows(i)("port").ToString()), m_Handle)
                                If result <> clsBioStar.BS_SUCCESS Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                Else
                                    result = clsBioStar.BS_CloseSocket(m_Handle)
                                End If
                                'Sohail (02 Nov 2013) -- End

                End Select

            End If
                Next

            Else
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeList", 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InitializeDevice", mstrModuleName)
        End Try
    End Sub

    Private Function SetEnrollData(ByVal intCommunicationTypeId As Integer, ByVal Employeeunkid As Integer, Optional ByVal fingerIndex As Integer = -1, Optional ByVal byTmpData As Byte() = Nothing, Optional ByVal iTmpLength As Integer = -1) As Boolean
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If intCommunicationTypeId = 1 Then    'FingerPrint
                objFingerPrint._EmployeeUnkId = Employeeunkid
                objFingerPrint._Finger_Mask = objFingerPrint.getFingerMask(fingerIndex)
                objFingerPrint._Finger_No = fingerIndex
                objFingerPrint._Data_Size = iTmpLength
                objFingerPrint._Fingerprint_Data = byTmpData
                objFingerPrint._DeviceCode = cboDeviceCode.Text & "||" & getIP()
                Return objFingerPrint.Insert()

            ElseIf intCommunicationTypeId = 2 Then   'RFID
                Dim objCard As New clsSwipeCardData
                objCard._EmployeeUnkId = intUserid
                objCard._CardData = txtCardNo.Text.Trim
                Return objCard.Insert()

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFingerPrintData", mstrModuleName)
        End Try
    End Function

    Private Sub GetEmployeedata()
        Try
            'FOR EMPLOYEE

            If CInt(cboEmployee.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END


                'Anjan (22 Feb 2011)-Start
                'Issue : On windows 2003 server this folder of MyPictures is not there , so it gives error.
                'imgImageControl._FilePath = My.Computer.FileSystem.SpecialDirectories.MyPictures
                imgImageControl._FilePath = ConfigParameter._Object._PhotoPath
                'Anjan (22 Feb 2011)-End

                If objEmp._ImagePath <> "" Then
                    imgImageControl._FileName = imgImageControl._FileName & "\" & objEmp._ImagePath
                Else
                    imgImageControl._FileName = ""
                End If


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes

                'Dim objShiftMst As New clsNewshift_master
                'objShiftMst._Shiftunkid = objEmp._Shiftunkid
                'objShift.Text = objShiftMst._Shiftname

                'Pinkal (15-Oct-2013) -- End

                Dim objDeptMst As New clsDepartment
                objDeptMst._Departmentunkid = objEmp._Departmentunkid
                objDepartment.Text = objDeptMst._Name

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeedata", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            objEmp = New clsEmployee_Master
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim dsList As DataSet = objEmp.GetEmployeeList("Employee", True, True)
            Dim dsList As DataSet
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables("Employee")



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
                dsList = New DataSet
                dsList.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                Dim drrow As DataRow = dsList.Tables(0).NewRow
                drrow("deviceunkid") = 0
                drrow("devicecode") = Language.getMessage(mstrModuleName, 14, "Select")
                dsList.Tables(0).Rows.InsertAt(drrow, 0)

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If i = 0 Then
                        cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim)
                    Else
                        cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Remove(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||"), _
                                                                                                              dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Length - dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||")))
                    End If

                Next

                cboDeviceCode.SelectedIndex = 0
            End If
            'Pinkal (20-Jan-2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub EnrollFingerPrint()
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    Select Case CInt(dr(0)("commdeviceid").ToString())

                        Case enFingerPrintDevice.DigitalPersona   'Digital Persona

                        Case enFingerPrintDevice.SecuGen 'SecuGen

                        Case enFingerPrintDevice.ZKSoftware 'ZK Software 

                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                            If objConnection Is Nothing Then
                                Exit Sub
                            End If
                            'Sohail (02 Nov 2013) -- End

                            Dim sOption As String = "~ZKFPVersion"
                            Dim sValue As String = ""

                    If CInt(cboFingerIndex.SelectedIndex) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index."), enMsgBoxStyle.Information)
                        cboFingerIndex.Select()
                        Exit Sub
                    End If


                            If objDictConnection.ContainsKey(dr(0)("Ip").ToString()) Then
                                objConnection = objDictConnection(dr(0)("Ip").ToString())
                            End If

                    Dim idwErrorCode As Integer

                            CType(objConnection, zkemkeeper.CZKEM).CancelOperation()

                            If CType(objConnection, zkemkeeper.CZKEM).GetSysOption(CInt(dr(0)("machinesrno").ToString()), sOption, sValue) Then


                                If sValue = "" OrElse sValue = "9" Then
                            CType(objConnection, zkemkeeper.CZKEM).DelUserTmp(CInt(dr(0)("machinesrno").ToString()), intUserid, intFingerIndex)
                    If CType(objConnection, zkemkeeper.CZKEM).StartEnroll(intUserid, intFingerIndex) = True Then
                        mblnIsEnroll = True
                    Else
                        CType(objConnection, zkemkeeper.CZKEM).GetLastError(idwErrorCode)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Operation failed,ErrorCode= ") & idwErrorCode.ToString(), enMsgBoxStyle.Information)
                    End If

                                ElseIf sValue = "10" Then
                                    ' intFlag = 0   0:Invalid
                                    ' intFlag = 1   1: Valid
                                    ' intFlag = 3   3: Threatened fingerprint template

                                    Dim intFlag As Integer = 1
                                    CType(objConnection, zkemkeeper.CZKEM).SSR_DelUserTmp(CInt(dr(0)("machinesrno").ToString()), intUserid.ToString(), intFingerIndex)
                                    If CType(objConnection, zkemkeeper.CZKEM).StartEnrollEx(intUserid.ToString(), intFingerIndex, intFlag) = True Then
                                        mblnIsEnroll = True
                                    Else
                                        CType(objConnection, zkemkeeper.CZKEM).GetLastError(idwErrorCode)
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Operation failed,ErrorCode= ") & idwErrorCode.ToString(), enMsgBoxStyle.Information)
                End If

                                End If

                            End If

                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                        Case enFingerPrintDevice.BioStar
                            If CInt(cboFingerIndex.SelectedIndex) <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index."), enMsgBoxStyle.Information)
                                cboFingerIndex.Select()
                                Exit Sub
                            End If

                            Dim objEmp As New clsEmployee_Master
                            m_TemplateData = New Byte(TEMPLATE_SIZE * 2 * 2 - 1) {}

                            Dim numOfFinger As Integer = 0
                            Dim result As Integer = 0
                            Dim fingerChecksum1 As UShort = 0
                            Dim fingerChecksum2 As UShort = 0

                            Dim handle As Integer = m_Handle
                            Dim deviceId As UInteger = 0
                            Dim m_DeviceType As Integer = 0

                            result = clsBioStar.BS_OpenSocket(dr(0)("ip").ToString(), CInt(dr(0)("port").ToString()), m_Handle)
                            If result <> clsBioStar.BS_SUCCESS Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            clsBioStar.BS_GetDeviceID(m_Handle, deviceId, m_DeviceType)
                            If deviceId <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Unable to connect the device."), enMsgBoxStyle.Information)
                                result = clsBioStar.BS_CloseSocket(m_Handle)
                                Exit Sub
                            End If
                            clsBioStar.BS_SetDeviceID(m_Handle, deviceId, m_DeviceType)

                            Select Case m_DeviceType
                                Case clsBioStar.BS_DEVICE_BIOSTATION, clsBioStar.BS_DEVICE_BIOENTRY_PLUS, clsBioStar.BS_DEVICE_BIOENTRY_W, clsBioStar.BS_DEVICE_BIOLITE, clsBioStar.BS_DEVICE_DSTATION, clsBioStar.BS_DEVICE_BIOSTATION2
                                    Dim templateData As Byte() = New Byte(TEMPLATE_SIZE * BS_MAX_TEMPLATE_PER_USER - 1) {}

                                    numOfFinger += 1

                                    Cursor.Current = Cursors.WaitCursor

                                    result = clsBioStar.BS_ScanTemplate(m_Handle, templateData)
                                    Cursor.Current = Cursors.Default

                                    If result <> clsBioStar.BS_SUCCESS Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Cannot scan the finger") & " Error" & ":" & result.ToString, enMsgBoxStyle.Information)
                                        result = clsBioStar.BS_CloseSocket(m_Handle)
                                        Exit Sub
                                    End If

                                    Buffer.BlockCopy(templateData, 0, m_TemplateData, 0, TEMPLATE_SIZE)

                                    Cursor.Current = Cursors.WaitCursor
                                    result = clsBioStar.BS_ScanTemplate(m_Handle, templateData)
                                    Cursor.Current = Cursors.Default

                                    If result <> clsBioStar.BS_SUCCESS Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Cannot scan the finger") & " Error" & ":" & result.ToString, enMsgBoxStyle.Information)
                                        result = clsBioStar.BS_CloseSocket(m_Handle)
                                        Exit Sub
                                    End If

                                    Buffer.BlockCopy(templateData, 0, m_TemplateData, TEMPLATE_SIZE, TEMPLATE_SIZE)

                                    For i As Integer = 0 To TEMPLATE_SIZE - 1
                                        fingerChecksum1 += m_TemplateData(i)
                                    Next
                            End Select

                            Select Case m_DeviceType
                                Case clsBioStar.BS_DEVICE_BIOSTATION
                                    Dim userHdr As New clsBioStar.BSUserHdrEx()

                                    userHdr.checksum = New UShort(4) {}

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'objEmp._Employeeunkid = intUserid
                                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intUserid
                                    'S.SANDEEP [04 JUN 2015] -- END

                                    userHdr.name = New Byte(32) {}
                                    'userHdr.name = System.Text.Encoding.ASCII.GetBytes(objEmp._Employeecode)
                                    'userHdr.name = System.Text.Encoding.ASCII.GetBytes(cboEmployee.Text)

                                    userHdr.department = New Byte(32) {}
                                    userHdr.password = New Byte(16) {}

                                    userHdr.authLimitCount = 0
                                    userHdr.timedAntiPassback = 0
                                    userHdr.disabled = 0

                                    userHdr.numOfFinger = CUShort(numOfFinger)

                                    userHdr.checksum(0) = CUShort(fingerChecksum1)
                                    userHdr.checksum(1) = CUShort(fingerChecksum2)

                                    userHdr.ID = CUInt(intUserid)
                                    'If (userLevel.SelectedIndex = 1) Then
                                    '    userHdr.adminLevel = CUShort(clsBioStar.BS_USER_ADMIN)
                                    'Else
                                    userHdr.adminLevel = CUShort(clsBioStar.BS_USER_NORMAL)
                                    'End If

                                    'userHdr.securityLevel = CUShort(securityLevel.SelectedIndex + clsBioStar.BS_USER_SECURITY_DEFAULT)
                                    userHdr.securityLevel = CUShort(clsBioStar.BS_USER_SECURITY_DEFAULT)

                                    'userHdr.bypassCard = CByte(cardType.SelectedIndex)
                                    userHdr.bypassCard = CByte(0) '0 = Normal,  1 = ByPass

                                    'userHdr.startDateTime = CUInt((startDate.Value.Ticks - New DateTime(1970, 1, 1).Ticks) / 10000000)
                                    'userHdr.expireDateTime = CUInt((expiryDate.Value.Ticks - New DateTime(1970, 1, 1).Ticks) / 10000000)
                                    userHdr.startDateTime = 0 'CUInt((CDate("01/Jan/2000").Ticks - New DateTime(1970, 1, 1).Ticks) / 10000000)
                                    userHdr.expireDateTime = 0 'CUInt((CDate("01/Jan/2800").Ticks - New DateTime(1970, 1, 1).Ticks) / 10000000)

                                    userHdr.duressMask = 0
                                    'If duress1.Checked Then
                                    '    userHdr.duressMask = userHdr.duressMask Or &H1
                                    'End If

                                    'If duress2.Checked Then
                                    '    userHdr.duressMask = userHdr.duressMask Or &H2
                                    'End If

                                    '***  0="Disabled", 1="Finger only", 2="Finger and Password", 3="Finger or Password", 4="Password only", 5="Card only"
                                    'If authMode.SelectedIndex = 0 Then
                                    '    userHdr.authMode = 0
                                    'Else
                                    userHdr.authMode = CUShort(1 + clsBioStar.BS_AUTH_FINGER_ONLY - 1)
                                    'End If

                                    Try
                                        userHdr.cardID = UInt32.Parse(mstrUserCardID, System.Globalization.NumberStyles.HexNumber)
                                    Catch e5 As Exception
                                        userHdr.cardID = 0
                                    End Try

                                    Try
                                        userHdr.customID = CByte(Int32.Parse(mstrCardCustomID))
                                    Catch e6 As Exception
                                        userHdr.customID = 0
                                    End Try

                                    userHdr.version = clsBioStar.BE_CARD_VERSION_1

                                    Try
                                        userHdr.accessGroupMask = UInt32.Parse(mstrAccessGroup, System.Globalization.NumberStyles.HexNumber)
                                    Catch e7 As Exception
                                        userHdr.accessGroupMask = &HFFFFFFFFL
                                    End Try

                                    Dim userInfo As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(GetType(clsBioStar.BSUserHdrEx)))
                                    Marshal.StructureToPtr(userHdr, userInfo, True)

                                    Cursor.Current = Cursors.WaitCursor
                                    result = clsBioStar.BS_EnrollUserEx(m_Handle, userInfo, m_TemplateData)
                                    Cursor.Current = Cursors.Default

                                    Marshal.FreeHGlobal(userInfo)

                                    If result <> clsBioStar.BS_SUCCESS Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Cannot enroll the user") & " Error:" & result.ToString, enMsgBoxStyle.Information)
                                        result = clsBioStar.BS_CloseSocket(m_Handle)
                                        Exit Sub
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "User enrolled successfully"), enMsgBoxStyle.Information)
                                        result = clsBioStar.BS_CloseSocket(m_Handle)
                                        Exit Sub
                                    End If

                                    'ReadUserInfo()
                            End Select
                            'Sohail (02 Nov 2013) -- End


                    End Select

                End If


            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnrollFingerPrint", mstrModuleName)
        End Try
    End Sub

    Private Sub SaveEnrollFingerPrint()
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    Select Case CInt(dr(0)("commdeviceid").ToString())

                        Case enFingerPrintDevice.DigitalPersona  'Digital Persona

                        Case enFingerPrintDevice.SecuGen  'SecuGen

                        Case enFingerPrintDevice.ZKSoftware  'Zk software

                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                            If objConnection Is Nothing Then
                                Exit Sub
                            End If
                            'Sohail (02 Nov 2013) -- End

                    If CInt(cboFingerIndex.SelectedIndex) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index."), enMsgBoxStyle.Information)
                        cboFingerIndex.Select()
                        Exit Sub

                    ElseIf objConnection Is Nothing Or mblnIsEnroll = False Then 'You haven't enrolled the templates.
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enroll the fingerprint first."), enMsgBoxStyle.Information)
                        Exit Sub

                    End If

                    Dim idwErrorCode As Integer
                    Dim iTmpLength As Integer
                    Dim sdwEnrollNumber As String = intUserid.ToString
                    Dim blnflag As Boolean = False
                            Dim intConnectionTypeId As Integer = CInt(dr(0)("connectiontypeid").ToString())
                    Dim sOption As String = "~ZKFPVersion"
                    Dim sValue As String = ""

                            CType(objConnection, zkemkeeper.CZKEM).CancelOperation()

                            If CType(objConnection, zkemkeeper.CZKEM).GetSysOption(CInt(dr(0)("machinesrno").ToString()), sOption, sValue) Then

                                'Pinkal (30-Jul-2013) -- Start
                                'Enhancement : TRA Changes

                                Cursor = Cursors.WaitCursor

                                'If sValue = "10" Then
                                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Your device is not using 9.0 arithmetic!"), enMsgBoxStyle.Information)
                                '    Return
                                'End If


                                If sValue = "" OrElse sValue = "9" Then
                                    Dim byTmpData(700) As Byte
                            If CType(objConnection, zkemkeeper.CZKEM).GetUserTmp(CInt(dr(0)("machinesrno").ToString()), intUserid, intFingerIndex, byTmpData(0), iTmpLength) Then
                                blnflag = SetEnrollData(CInt(dr(0)("commtypeid").ToString()), intUserid, intFingerIndex, byTmpData, iTmpLength)
                        If blnflag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee Fingerprint Enrollment done successfully."), enMsgBoxStyle.Information)
                                    CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString())) 'the data in the device should be refreshed 
                                            CType(objConnection, zkemkeeper.CZKEM).StartIdentify()
                            cboFingerIndex.SelectedIndex = 0
                                    cboDeviceCode.Select()

                        ElseIf blnflag = False And objFingerPrint._Message <> "" Then
                            eZeeMsgBox.Show(objFingerPrint._Message, enMsgBoxStyle.Information)

                        ElseIf blnflag = False Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Employee Fingerprint Enrollment failed."), enMsgBoxStyle.Information)
                                    Exit Sub
                        End If

                    Else

                        If CType(objConnection, zkemkeeper.CZKEM).StartVerify(intUserid, intFingerIndex) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This Employee Finger was already enrolled.Please Select another Finger."), enMsgBoxStyle.Information)
                                    Exit Sub
                        Else
                            CType(objConnection, zkemkeeper.CZKEM).GetLastError(idwErrorCode)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Operation failed,ErrorCode= ") & idwErrorCode.ToString(), enMsgBoxStyle.Information)
                                    Exit Sub
                        End If

                    End If


                                ElseIf sValue = "10" Then

                                    Try
                                        Dim intFlag As Integer = -1
                                        Dim byTmpData(2000) As Byte
                                        If CType(objConnection, zkemkeeper.CZKEM).GetUserTmpEx(CInt(dr(0)("machinesrno").ToString()), intUserid.ToString(), intFingerIndex, intFlag, byTmpData(0), iTmpLength) Then
                                            blnflag = SetEnrollData(CInt(dr(0)("commtypeid").ToString()), intUserid, intFingerIndex, byTmpData, iTmpLength)
                                            Array.Clear(byTmpData, 0, byTmpData.Length - 1)
                                            byTmpData = Nothing
                                            If blnflag Then
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee Fingerprint Enrollment done successfully."), enMsgBoxStyle.Information)
                                                CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString())) 'the data in the device should be refreshed 
                                                CType(objConnection, zkemkeeper.CZKEM).StartIdentify()
                                                cboFingerIndex.SelectedIndex = 0
                                                cboDeviceCode.Select()

                                            ElseIf blnflag = False And objFingerPrint._Message <> "" Then
                                                eZeeMsgBox.Show(objFingerPrint._Message, enMsgBoxStyle.Information)

                                            ElseIf blnflag = False Then
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Employee Fingerprint Enrollment failed."), enMsgBoxStyle.Information)
                                                Exit Sub
                                            End If

                                        Else

                                            If CType(objConnection, zkemkeeper.CZKEM).StartVerify(intUserid, intFingerIndex) Then
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This Employee Finger was already enrolled.Please Select another Finger."), enMsgBoxStyle.Information)
                                                Exit Sub
                                            Else
                                                CType(objConnection, zkemkeeper.CZKEM).GetLastError(idwErrorCode)
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Operation failed,ErrorCode= ") & idwErrorCode.ToString(), enMsgBoxStyle.Information)
                                                Exit Sub
                                            End If

                                        End If

                                    Catch ex As IndexOutOfRangeException

                                    Catch ex As OutOfMemoryException

                                    Catch ex As AccessViolationException

                                    Catch ex As Exception

                                    End Try

                                End If

                            End If

                            'Pinkal (30-Jul-2013) -- End 

                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                        Case enFingerPrintDevice.BioStar
                            Dim result As Integer = 0
                            Dim deviceId As UInteger = 0
                            Dim m_DeviceType As Integer = 0

                            'result = clsBioStar.BS_OpenSocket(dr(0)("ip").ToString(), CInt(dr(0)("port").ToString()), m_Handle)
                            'If result <> clsBioStar.BS_SUCCESS Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                            '    Exit Sub
                            'End If
                            'clsBioStar.BS_GetDeviceID(m_Handle, deviceId, m_DeviceType)
                            'If deviceId <= 0 Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Unable to connect the device."), enMsgBoxStyle.Information)
                            '    result = clsBioStar.BS_CloseSocket(m_Handle)
                            '    Exit Sub
                            'End If
                            'clsBioStar.BS_SetDeviceID(m_Handle, deviceId, m_DeviceType)

                            If SetEnrollData(CInt(dr(0)("commtypeid").ToString()), intUserid, intFingerIndex, m_TemplateData, m_TemplateData.Length) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Operation failed.") & ":" & result.ToString, enMsgBoxStyle.Information)
                            Else
                                Dim objDeviceUser As New clsEmpid_devicemapping
                                Dim intUnkId As Integer = objDeviceUser.GetUnkId(intUserid)
                                If intUnkId > 0 Then
                                    objDeviceUser._Devicemappingunkid = intUnkId
                                End If
                                objDeviceUser._Employeeunkid = intUserid
                                objDeviceUser._Deviceuserid = intUserid.ToString
                                objDeviceUser._Userunkid = User._Object._Userunkid
                                objDeviceUser._Isactive = True
                                If intUnkId > 0 Then
                                    objDeviceUser.Update()
                                Else
                                    objDeviceUser.Insert()
                                End If
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee Fingerprint Enrollment done successfully."), enMsgBoxStyle.Information)
                            End If
                            'result = clsBioStar.BS_CloseSocket(m_Handle)
                            'Sohail (02 Nov 2013) -- End

                    End Select

                End If


            End If

        Catch ex As IndexOutOfRangeException

        Catch ex As OutOfMemoryException

        Catch ex As AccessViolationException

        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "SaveEnrollFingerPrint", mstrModuleName)
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SaveEnrollCard()
        Try

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    Select Case CInt(dr(0)("commdeviceid").ToString())

                        Case enFingerPrintDevice.DigitalPersona  'Digital Persona

                        Case enFingerPrintDevice.SecuGen  'SecuGen

                        Case enFingerPrintDevice.ZKSoftware  'Zk software

                    If txtCardNo.Text.Trim = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enroll the card first."), enMsgBoxStyle.Information)
                        cboEmployee.Select()
                        Exit Sub
                    End If

                            If objDictConnection.ContainsKey(dr(0)("Ip").ToString()) Then
                                objConnection = objDictConnection(dr(0)("Ip").ToString())
                            End If

                    Dim blnflag As Boolean = False

                    'START CHECK FOR WHETHER EMPLOYEE HAD OTHER RFID OR NOT

                    Dim objCard As New clsSwipeCardData
                    If objCard.GetCardNo(intUserid) <> "" Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This Employee already have enrolled RFID.Are you sure you want to replace new RFID to previous one?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            GoTo Save
                        Else
                            Exit Sub
                        End If
                    End If

                    'END CHECK FOR WHETHER EMPLOYEE HAD OTHER RFID OR NOT

Save:
                        CType(objConnection, zkemkeeper.CZKEM).SetStrCardNumber(txtCardNo.Text.Trim)  'Before you using function SetUserInfo,set the card number to make sure you can upload it to the device
                            If CType(objConnection, zkemkeeper.CZKEM).SetUserInfo(CInt(dr(0)("machinesrno").ToString()), intUserid, cboEmployee.Text, "", 0, True) Then 'upload the user's information(card number included)

                                'blnflag = SetEnrollData(intUserid)
                                blnflag = SetEnrollData(CInt(dr(0)("commtypeid").ToString()), intUserid)

                            If blnflag Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Employee Card Enrollment done successfully."), enMsgBoxStyle.Information)
                                txtCardNo.Text = ""
                                cboEmployee.Select()
                            ElseIf blnflag = False And objFingerPrint._Message <> "" Then
                            eZeeMsgBox.Show(objFingerPrint._Message, enMsgBoxStyle.Information)
                                    Exit Sub
                            ElseIf blnflag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Employee Card Enrollment failed."), enMsgBoxStyle.Information)
                                    Exit Sub
                            End If

                        End If

            End Select

                    CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString())) 'the data in the device should be refreshed 

                End If

                '                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '                    Select Case CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                '                        Case 1  'Digital Persona

                '                        Case 2  'SecuGen

                '                        Case 3  'Zk software

                '                            If txtCardNo.Text.Trim = "" Then
                '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please enroll the card first."), enMsgBoxStyle.Information)
                '                                cboEmployee.Select()
                '                                Exit Sub
                '                            End If

                '                            Dim blnflag As Boolean = False

                '                            'START CHECK FOR WHETHER EMPLOYEE HAD OTHER RFID OR NOT

                '                            Dim objCard As New clsSwipeCardData
                '                            If objCard.GetCardNo(intUserid) <> "" Then
                '                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This Employee already have enrolled RFID.Are you sure you want to replace new RFID to previous one?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                '                                    GoTo Save
                '                                Else
                '                                    Exit Sub
                '                                End If
                '                            End If

                '                            'END CHECK FOR WHETHER EMPLOYEE HAD OTHER RFID OR NOT

                'Save:
                '                            CType(objConnection, zkemkeeper.CZKEM).SetStrCardNumber(txtCardNo.Text.Trim)  'Before you using function SetUserInfo,set the card number to make sure you can upload it to the device
                '                            If CType(objConnection, zkemkeeper.CZKEM).SetUserInfo(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), intUserid, cboEmployee.Text, "", 0, True) Then 'upload the user's information(card number included)

                '                                'blnflag = SetEnrollData(intUserid)
                '                                blnflag = SetEnrollData(CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()), intUserid)

                '                                If blnflag Then
                '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Employee Card Enrollment done successfully."), enMsgBoxStyle.Information)
                '                                    txtCardNo.Text = ""
                '                                    cboEmployee.Select()
                '                                ElseIf blnflag = False And objFingerPrint._Message <> "" Then
                '                                    eZeeMsgBox.Show(objFingerPrint._Message, enMsgBoxStyle.Information)
                '                                    Exit For
                '                                ElseIf blnflag = False Then
                '                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Employee Card Enrollment failed."), enMsgBoxStyle.Information)
                '                                    Exit For
                '                                End If

                '                            End If

                '                    End Select

                '                    CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString())) 'the data in the device should be refreshed

                '                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveEnrollCard", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Events"

    Public Sub Zksoftware_OnHIDNum(ByVal CardNumber As Integer)
        Try
            'Pinkal (21-Jan-2011) --Start

            If CInt(cboDeviceCode.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Device Code is compulsory information.Please select Device Code."), enMsgBoxStyle.Information)
                cboDeviceCode.Select()
            End If


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            End If

            txtCardNo.Text = CardNumber.ToString

            Dim objSwipe As New clsSwipeCardData
            Dim intVerifyEmpId As Integer = 0

            objSwipe.VerifyData(txtCardNo.Text.Trim, intVerifyEmpId, -1, "")

            If intVerifyEmpId > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "This Card is already enrolled."), enMsgBoxStyle.Information)
                btnSaveEnrollment.Enabled = False
                cboEmployee.Select()
            Else
                btnSaveEnrollment.Enabled = True
            End If

            'Pinkal (21-Jan-2011) --End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Zksoftware_OnHIDNum", mstrModuleName)
        End Try
    End Sub

    Public Sub Zksoftware_OnVerify(ByVal Userid As Integer)
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Zksoftware_OnHIDNum", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Try
            GetEmployeedata()
            intUserid = CInt(cboEmployee.SelectedValue)


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    If CInt(dr(0)("commtypeid").ToString()) = 2 Then   'RFID
                txtCardNo.Text = ""
                btnSaveEnrollment.Enabled = True
            End If

                End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then   'RFID
                '        txtCardNo.Text = ""
                '        btnSaveEnrollment.Enabled = True

                '    End If

                'Next

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFingerIndex_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFingerIndex.SelectedIndexChanged
        Try
            If CInt(cboFingerIndex.SelectedIndex) <= 0 Then Exit Sub
            intFingerIndex = CInt(cboFingerIndex.Text)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFingerIndex_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Private Sub btnStartEnroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartEnroll.Click

        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboDeviceCode.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Device Code is compulsory information.Please select Device Code."), enMsgBoxStyle.Information)
                cboDeviceCode.Select()
                Exit Sub
            End If


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            End If

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    'Sohail (02 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If objConnection IsNot Nothing Then

                    '    If CInt(dr(0)("commtypeid").ToString()) = 1 Then  'FingerPrint 
                    '        EnrollFingerPrint()
                    '    End If

                    'End If
                        If CInt(dr(0)("commtypeid").ToString()) = 1 Then  'FingerPrint 
                    EnrollFingerPrint()
                End If
                    'Sohail (02 Nov 2013) -- End


                End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    'Dim intConnectionTypeId As enFingerPrintConnetionType = CType(ConfigParameter._Object._ConnectionTypeId, enFingerPrintConnetionType)
                '    'Dim intConnectionTypeId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("connectiontypeid").ToString())

                '    If objConnection IsNot Nothing Then

                '        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then  'FingerPrint
                '            EnrollFingerPrint()
                '        End If

                '    End If

                'Next

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStartEnroll_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnSaveEnrollment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEnrollment.Click
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboDeviceCode.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Device Code is compulsory information.Please select Device Code."), enMsgBoxStyle.Information)
                cboDeviceCode.Select()
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub

            End If

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    'Sohail (02 Nov 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If objConnection IsNot Nothing Then
                    '    If CInt(dr(0)("commtypeid").ToString()) = 1 Then   'FingerPrint 
                    '        SaveEnrollFingerPrint()

                    '    ElseIf CInt(dr(0)("commtypeid").ToString()) = 2 Then   'RFID
                    '        SaveEnrollCard()

                    '    End If
                    'End If
                        If CInt(dr(0)("commtypeid").ToString()) = 1 Then   'FingerPrint 
                    SaveEnrollFingerPrint()

                        ElseIf CInt(dr(0)("commtypeid").ToString()) = 2 Then   'RFID
                    SaveEnrollCard()

                End If
                    'Sohail (02 Nov 2013) -- End

                End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    If objConnection IsNot Nothing Then

                '        If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then   'FingerPrint
                '            SaveEnrollFingerPrint()

                '        ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then   'RFID
                '            SaveEnrollCard()

                '        End If

                '    End If

                'Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveEnrollment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

'S.SANDEEP [ 15 SEP 2011 ] -- START
    'ENHANCEMENT : CODE OPTIMIZATION
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 15 SEP 2011 ] -- END 

#End Region

  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnStartEnroll.GradientBackColor = GUI._ButttonBackColor 
			Me.btnStartEnroll.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveEnrollment.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveEnrollment.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnStartEnroll.Text = Language._Object.getCaption(Me.btnStartEnroll.Name, Me.btnStartEnroll.Text)
			Me.btnSaveEnrollment.Text = Language._Object.getCaption(Me.btnSaveEnrollment.Name, Me.btnSaveEnrollment.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.elAssignInfo.Text = Language._Object.getCaption(Me.elAssignInfo.Name, Me.elAssignInfo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblfingerIndex.Text = Language._Object.getCaption(Me.lblfingerIndex.Name, Me.lblfingerIndex.Text)
			Me.lblImage.Text = Language._Object.getCaption(Me.lblImage.Name, Me.lblImage.Text)
			Me.imgImageControl.Text = Language._Object.getCaption(Me.imgImageControl.Name, Me.imgImageControl.Text)
			Me.lblCardNo.Text = Language._Object.getCaption(Me.lblCardNo.Name, Me.lblCardNo.Text)
			Me.lblDeviceCode.Text = Language._Object.getCaption(Me.lblDeviceCode.Name, Me.lblDeviceCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee.")
			Language.setMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index.")
			Language.setMessage(mstrModuleName, 3, "Operation failed,ErrorCode=")
			Language.setMessage(mstrModuleName, 4, "Please enroll the fingerprint first.")
			Language.setMessage(mstrModuleName, 6, "Employee Fingerprint Enrollment done successfully.")
			Language.setMessage(mstrModuleName, 7, "This Employee already have enrolled RFID.Are you sure you want to replace new RFID to previous one?")
			Language.setMessage(mstrModuleName, 8, "Operation failed.")
			Language.setMessage(mstrModuleName, 9, "This Employee Finger was already enrolled.Please Select another Finger.")
			Language.setMessage(mstrModuleName, 10, "Please enroll the card first.")
			Language.setMessage(mstrModuleName, 11, "Employee Card Enrollment done successfully.")
			Language.setMessage(mstrModuleName, 12, "Employee Card Enrollment failed.")
			Language.setMessage(mstrModuleName, 13, "This Card is already enrolled.")
			Language.setMessage(mstrModuleName, 14, "Select")
			Language.setMessage(mstrModuleName, 15, "Device Code is compulsory information.Please select Device Code.")
			Language.setMessage(mstrModuleName, 16, "Cannot scan the finger")
			Language.setMessage("frmEmployeeList", 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings.")
			Language.setMessage(mstrModuleName, 18, "Unable to connect the device.")
			Language.setMessage(mstrModuleName, 19, "Employee Fingerprint Enrollment failed.")
			Language.setMessage(mstrModuleName, 20, "User enrolled successfully")
			Language.setMessage(mstrModuleName, 17, "Cannot enroll the user")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class