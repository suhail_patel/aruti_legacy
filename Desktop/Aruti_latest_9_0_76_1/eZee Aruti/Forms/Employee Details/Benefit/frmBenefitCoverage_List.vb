﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBenefitCoverage_List

#Region " Private Varaibles "
    Private objBenefit As clsBenefitCoverage_tran
    Private ReadOnly mstrModuleName As String = "frmBenefitCoverage_List"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Property "
    Dim mintEmployeeUnkid As Integer = -1
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property
#End Region
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#Region " Private Functions "
    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim objBenefit As New clsbenefitplan_master
        'Sandeep [ 21 Aug 2010 ] -- Start
        Dim objTranHead As New clsTransactionHead
        'Sandeep [ 21 Aug 2010 ] -- End 
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If

            'Sandeep (14-Sep-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

            'Sandeep (14-Sep-2018) -- End

            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BG")
            With cboBenefitGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BG")
                .SelectedValue = 0
            End With

            dsCombos = objBenefit.getComboList("Benefit", True)
            With cboBenefitPlan
                .ValueMember = "benefitplanunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Benefit")
                .SelectedValue = 0
            End With

            'Sandeep [ 21 Aug 2010 ] -- Start

            'Vimal (01 Nov 2010) -- Start 
            'dsCombos = objTranHead.getComboList("TranHead", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, , , enTypeOf.BENEFIT)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , enTypeOf.BENEFIT)
            'Sohail (21 Aug 2015) -- End
            'Vimal (01 Nov 2010) -- End
            With cboTransactionHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("TranHead")
                .SelectedValue = 0
            End With
            'Sandeep [ 21 Aug 2010 ] -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewEmpBenefitList = True Then                'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objBenefit.GetList("BCoverage")

                'If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                '    StrSearching &= "AND BGroupId = " & CInt(cboBenefitGroup.SelectedValue) & " "
                'End If

                'If CInt(cboBenefitPlan.SelectedValue) > 0 Then
                '    StrSearching &= "AND BPlanId = " & CInt(cboBenefitPlan.SelectedValue) & " "
                'End If

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                'End If


                ''Vimal (01 Nov 2010) -- Start 
                ''If cdec(txtBenefitAmount.Decimal) > 0 Then
                ''    StrSearching &= "AND BAmount >= " & txtBenefitAmount.Text & " AND BAmount <= " & txtBenefitAmount.Text & " "
                ''End If

                ''If cdec(txtDBBenefitInPercent.Decimal) > 0 Then
                ''    StrSearching &= "AND BPercent >= " & txtDBBenefitInPercent.Text & " AND BPercent <= " & txtDBBenefitInPercent.Text & " "
                ''End If

                ''If dtpAssignDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                ''    StrSearching &= "AND StDate >= " & eZeeDate.convertDate(dtpAssignDate.Value) & " AND EdDate <= " & eZeeDate.convertDate(dtpEndDate.Value) & " "
                ''End If
                ''Vimal (01 Nov 2010) -- End




                ''Sandeep [ 21 Aug 2010 ] -- Start
                'If CInt(cboTransactionHead.SelectedValue) > 0 Then
                '    StrSearching &= "AND TranHeadId = " & CInt(cboTransactionHead.SelectedValue) & " "
                'End If
                ''Sandeep [ 21 Aug 2010 ] -- End 

                ''S.SANDEEP [ 23 JAN 2012 ] -- START
                ''ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
                'If radShowActive.Checked = True Then
                '    StrSearching &= "AND isvoid = 0 "
                'ElseIf radShowVoid.Checked = True Then
                '    StrSearching &= "AND isvoid = 1 "
                'End If
                ''S.SANDEEP [ 23 JAN 2012 ] -- END

                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 

                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("BCoverage"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("BCoverage"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'End If

                If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND hremp_benefit_coverage.benefitgroupunkid = " & CInt(cboBenefitGroup.SelectedValue) & " "
                End If

                If radShowActive.Checked = True Then
                    StrSearching &= "AND hremp_benefit_coverage.isvoid = 0 "
                ElseIf radShowVoid.Checked = True Then
                    StrSearching &= "AND hremp_benefit_coverage.isvoid = 1 "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If


                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                'Sandeep (14-Sep-2018) -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                dsList = objBenefit.GetList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                            ConfigParameter._Object._IsIncludeInactiveEmp, "BCoverage", , _
                                            CInt(cboEmployee.SelectedValue), CInt(cboBenefitPlan.SelectedValue), _
                                            CInt(cboTransactionHead.SelectedValue), StrSearching, , mblnAddApprovalCondition)

                'Sandeep (14-Sep-2018) -- End

                dtTable = New DataView(dsList.Tables("BCoverage"), "", "EmpName", DataViewRowState.CurrentRows).ToTable

                lvBenefitCoverage.BeginUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

                lvBenefitCoverage.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.SubItems.Add(dtRow.Item("BPlanName").ToString)
                    'Sandeep [ 21 Aug 2010 ] -- Start
                    lvItem.SubItems.Add(dtRow.Item("TranHead").ToString)
                    'Sandeep [ 21 Aug 2010 ] -- End 

                    'Vimal (01 Nov 2010) -- Start 
                    'Select Case cdec(dtRow.Item("BPercent"))
                    '    Case 0
                    '        lvItem.SubItems.Add("")
                    '    Case Else
                    '        lvItem.SubItems.Add(dtRow.Item("BPercent").ToString)
                    'End Select
                    'Select Case cdec(dtRow.Item("BAmount"))
                    '    Case 0
                    '        lvItem.SubItems.Add("")
                    '    Case Else
                    '        lvItem.SubItems.Add(dtRow.Item("BAmount").ToString)
                    'End Select
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StDate").ToString).ToShortDateString)
                    'If IsDBNull(dtRow.Item("EdDate")) Then
                    '    lvItem.SubItems.Add("")
                    'Else
                    '    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EdDate").ToString).ToShortDateString)
                    'End If
                    'Vimal (01 Nov 2010) -- End



                    'S.SANDEEP [ 23 JAN 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
                    lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)
                    lvItem.SubItems.Add(dtRow.Item("BGroupName").ToString)
                    If IsDBNull(dtRow.Item("isvoid")) = False Then
                        If CBool(dtRow.Item("isvoid")) = True Then
                            lvItem.ForeColor = Color.Red
                        End If
                    End If
                    'S.SANDEEP [ 23 JAN 2012 ] -- END




                    lvItem.Tag = dtRow.Item("BCoverageId")

                    lvBenefitCoverage.Items.Add(lvItem)

                    lvItem = Nothing
                Next


                'Sandeep [ 21 Aug 2010 ] -- Start
                'If lvBenefitCoverage.Items.Count > 16 Then
                '    colhBenefitPlan.Width = 190 - 10
                'Else
                '    colhBenefitPlan.Width = 190
                'End If
                If lvBenefitCoverage.Items.Count > 16 Then
                    colhBenefitPlan.Width = 150 - 15
                Else
                    colhBenefitPlan.Width = 150
                End If
                'Sandeep [ 21 Aug 2010 ] -- End 


                lvBenefitCoverage.GroupingColumn = colhEmployee
                lvBenefitCoverage.DisplayGroups(True)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvBenefitCoverage.EndUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeBenefit
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeBenefit
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeBenefit

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmBenefitCoverage_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefit = New clsBenefitCoverage_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()


            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
            radShowActive.Checked = True
            lvBenefitCoverage.GridLines = False
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            'Call FillList()

            If lvBenefitCoverage.Items.Count > 0 Then lvBenefitCoverage.Items(0).Selected = True
            lvBenefitCoverage.Select()


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetsRegisterList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvBenefitCoverage.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmAssetsRegisterList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmAssetsRegisterList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBenefit = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBenefitCoverage_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBenefitCoverage_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buton's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBenefitCoverage.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitCoverage.Select()
            Exit Sub
        End If

        If lvBenefitCoverage.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete the voided transactions."), enMsgBoxStyle.Information) '?1
            lvBenefitCoverage.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitCoverage.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objBenefit._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objBenefit._Voidreason = "Testing"
                'objBenefit._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection

                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBenefit._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objBenefit._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

                objBenefit._Voiduserunkid = User._Object._Userunkid

                objBenefit.Delete(CInt(lvBenefitCoverage.SelectedItems(0).Tag), CInt(lvBenefitCoverage.SelectedItems(0).SubItems(objcolhEmpID.Index).Text))
                'Sandeep | 17 JAN 2011 | -- START
                If objBenefit._Message <> "" Then
                    eZeeMsgBox.Show(objBenefit._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Sandeep | 17 JAN 2011 | -- END 

                lvBenefitCoverage.SelectedItems(0).Remove()

                If lvBenefitCoverage.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBenefitCoverage.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBenefitCoverage.Items.Count - 1
                    lvBenefitCoverage.Items(intSelectedIndex).Selected = True
                    lvBenefitCoverage.EnsureVisible(intSelectedIndex)
                ElseIf lvBenefitCoverage.Items.Count <> 0 Then
                    lvBenefitCoverage.Items(intSelectedIndex).Selected = True
                    lvBenefitCoverage.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBenefitCoverage.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvBenefitCoverage.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitCoverage.Select()
            Exit Sub
        End If

        If lvBenefitCoverage.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot edit the voided transactions."), enMsgBoxStyle.Information) '?1
            lvBenefitCoverage.Select()
            Exit Sub
        End If

        Dim frm As New frmEmployeeBenefitCoverage
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitCoverage.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvBenefitCoverage.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(CInt(lvBenefitCoverage.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            frm = Nothing

            lvBenefitCoverage.Items(intSelectedIndex).Selected = True
            lvBenefitCoverage.EnsureVisible(intSelectedIndex)
            lvBenefitCoverage.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeBenefitCoverage
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            '    Call FillList()
            'End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboBenefitGroup.SelectedValue = 0
            cboBenefitPlan.SelectedValue = 0

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END


            'Vimal (01 Nov 2010) -- Start 
            'txtBenefitAmount.Text = CStr(0)
            'txtDBBenefitInPercent.Text = CStr(0)
            'dtpAssignDate.Checked = False
            'dtpEndDate.Checked = False
            'Vimal (01 Nov 2010) -- End
            'Sandeep [ 21 Aug 2010 ] -- Start
            cboTransactionHead.SelectedValue = 0
            'Sandeep [ 21 Aug 2010 ] -- End 
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboTransactionHead.ValueMember
                .DisplayMember = cboTransactionHead.DisplayMember
                .DataSource = CType(cboTransactionHead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If objFrm.DisplayDialog Then
                cboTransactionHead.SelectedValue = objFrm.SelectedValue
            End If
            cboTransactionHead.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblBenefitsInPercent.Text = Language._Object.getCaption(Me.lblBenefitsInPercent.Name, Me.lblBenefitsInPercent.Text)
            Me.lblBenefitInAmount.Text = Language._Object.getCaption(Me.lblBenefitInAmount.Name, Me.lblBenefitInAmount.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhBenefitPlan.Text = Language._Object.getCaption(CStr(Me.colhBenefitPlan.Tag), Me.colhBenefitPlan.Text)
            Me.lblBenefitType.Text = Language._Object.getCaption(Me.lblBenefitType.Name, Me.lblBenefitType.Text)
            Me.lblBenefitPlan.Text = Language._Object.getCaption(Me.lblBenefitPlan.Name, Me.lblBenefitPlan.Text)
            Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.colhTransactionHead.Text = Language._Object.getCaption(CStr(Me.colhTransactionHead.Tag), Me.colhTransactionHead.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.radShowAll.Text = Language._Object.getCaption(Me.radShowAll.Name, Me.radShowAll.Text)
            Me.radShowVoid.Text = Language._Object.getCaption(Me.radShowVoid.Name, Me.radShowVoid.Text)
            Me.colhBenefitGroup.Text = Language._Object.getCaption(CStr(Me.colhBenefitGroup.Tag), Me.colhBenefitGroup.Text)
            Me.radShowActive.Text = Language._Object.getCaption(Me.radShowActive.Name, Me.radShowActive.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot edit the voided transactions.")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete the voided transactions.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class