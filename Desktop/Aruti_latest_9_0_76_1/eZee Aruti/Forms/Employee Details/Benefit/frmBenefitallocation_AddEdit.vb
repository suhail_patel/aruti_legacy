﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBenefitallocation_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBenefitallocation_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objBenefitAllocation As clsBenefitAllocation
    Private mintBenefitAllocationUnkid As Integer = -1
    Private mstrBenefitGroups As String = ""

    Private mdtTable As DataTable 'Sohail (23 Jan 2012)
    Private mstrSearchText As String = "" 'Hemant (22 June 2019)
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private mdtPeriodStartdate As DateTime
    Private mdtPeriodEnddate As DateTime
    'Sohail (17 Sep 2019) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBenefitAllocationUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBenefitAllocationUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboBranch.BackColor = GUI.ColorOptional
            cboClass.BackColor = GUI.ColorOptional
            cboCostcenter.BackColor = GUI.ColorOptional
            cboDepartment.BackColor = GUI.ColorOptional
            cboGrade.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboSection.BackColor = GUI.ColorOptional
            'Hemant (22 June 2019) -- Start
            'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
            'cboBranch.BackColor = GUI.ColorOptional
            cboDepartmentGrp.BackColor = GUI.ColorOptional
            cboSectionGroup.BackColor = GUI.ColorOptional
            cboUnitGroup.BackColor = GUI.ColorOptional
            cboTeams.BackColor = GUI.ColorOptional
            cboGradeGroup.BackColor = GUI.ColorOptional
            cboGradeLevel.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboClassGroup.BackColor = GUI.ColorOptional
            'Hemant (22 June 2019) -- End
            cboUnit.BackColor = GUI.ColorOptional
            cboBenefitGroup.BackColor = GUI.ColorComp
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (17 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objStation As New clsStation
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJob As New clsJobs
        Dim objGrade As New clsGrade
        Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master
        Dim objCommon As New clsCommon_Master
        Dim dsCombos As New DataSet


        'Pinkal (24-Feb-2017) -- Start
        'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objSectionGrp As New clsSectionGroup
        Dim objJobGrp As New clsJobGroup
        Dim objGradeGrp As New clsGradeGroup
        Dim objClassGrp As New clsClassGroup
        Dim objTeam As New clsTeams
        Dim objUnitGroup As New clsUnitGroup
        'Pinkal (24-Feb-2017) -- End
        'Sohail (17 Sep 2019) -- Start
        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        'Sohail (17 Sep 2019) -- End

        Try
            dsCombos = objStation.getComboList("Station", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboBranch) 'Hemant (22 June 2019)


            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboDepartment) 'Hemant (22 June 2019)

            dsCombos = objSection.getComboList("Section", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboSection) 'Hemant (22 June 2019)

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnit
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboUnit) 'Hemant (22 June 2019)

            dsCombos = objJob.getComboList("Jobs", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Jobs")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboJob) 'Hemant (22 June 2019)

            dsCombos = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGrade) 'Hemant (22 June 2019)

            dsCombos = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Class")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboClass) 'Hemant (22 June 2019)

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostcenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboCostcenter) 'Hemant (22 June 2019)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            dsCombos = objDeptGrp.getComboList("List", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboDepartmentGrp) 'Hemant (22 June 2019)

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboSectionGroup) 'Hemant (22 June 2019)

            dsCombos = objUnitGroup.getComboList("List", True, -1)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboUnitGroup) 'Hemant (22 June 2019)

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboTeams) 'Hemant (22 June 2019)

            dsCombos = objJobGrp.getComboList("List", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboJobGroup) 'Hemant (22 June 2019)

            dsCombos = objGradeGrp.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboGradeGroup) 'Hemant (22 June 2019)

            dsCombos = objClassGrp.getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboClassGroup) 'Hemant (22 June 2019)

            'Pinkal (24-Feb-2017) -- End


            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BenefitGroup")
            With cboBenefitGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BenefitGroup")
                .SelectedValue = 0
            End With

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            End With
            'Sohail (17 Sep 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboClass.SelectedValue = objBenefitAllocation._Classunkid
            cboCostcenter.SelectedValue = objBenefitAllocation._Costcenterunkid
            cboDepartment.SelectedValue = objBenefitAllocation._Departmentunkid
            cboGrade.SelectedValue = objBenefitAllocation._Gradeunkid
            cboJob.SelectedValue = objBenefitAllocation._Jobunkid
            cboSection.SelectedValue = objBenefitAllocation._Sectionunkid
            cboBranch.SelectedValue = objBenefitAllocation._Stationunkid
            cboUnit.SelectedValue = objBenefitAllocation._Unitunkid


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            cboDepartmentGrp.SelectedValue = objBenefitAllocation._DepartmentGroupunkid
            cboSectionGroup.SelectedValue = objBenefitAllocation._SectionGroupunkid
            cboUnitGroup.SelectedValue = objBenefitAllocation._UnitGroupunkid
            cboTeams.SelectedValue = objBenefitAllocation._Teamunkid
            cboGradeGroup.SelectedValue = objBenefitAllocation._GradeGroupunkid
            cboGradeLevel.SelectedValue = objBenefitAllocation._Gradelevelunkid
            cboJobGroup.SelectedValue = objBenefitAllocation._JobGroupunkid
            cboClassGroup.SelectedValue = objBenefitAllocation._ClassGroupunkid
            'Pinkal (24-Feb-2017) -- End
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            cboPeriod.SelectedValue = objBenefitAllocation._Periodunkid
            'Sohail (17 Sep 2019) -- End

            cboBenefitGroup.SelectedValue = objBenefitAllocation._Benefitgroupunkid
            objBenefitAllocation._Isvoid = objBenefitAllocation._Isvoid
            objBenefitAllocation._Voiddatetime = objBenefitAllocation._Voiddatetime
            objBenefitAllocation._Voiduserunkid = objBenefitAllocation._Voiduserunkid
            objBenefitAllocation._Voidreason = objBenefitAllocation._Voidreason

            If menAction = enAction.EDIT_ONE Then
                Dim objTranHead As New clsTransactionHead
                For Each dRow As DataGridViewRow In dgvBenefit.Rows
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dRow.Cells(colhTranHead.Index).Value)
                    If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                        dRow.Cells(colhAmount.Index).ReadOnly = False
                        dRow.Cells(colhAmount.Index).Style.BackColor = Nothing
                    Else
                        dRow.Cells(colhAmount.Index).ReadOnly = True
                        dRow.Cells(colhAmount.Index).Style.BackColor = GUI.ColorOptional
                    End If
                Next

                'Sohail (27 Sep 2019) -- Start
                'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
                Dim strFilter As String = ""
                Dim dicAlloc As New Dictionary(Of String, String)
                dicAlloc.Add(cboBranch.Name, "stationunkid")
                dicAlloc.Add(cboDepartment.Name, "departmentunkid")
                dicAlloc.Add(cboSection.Name, "sectionunkid")
                dicAlloc.Add(cboUnit.Name, "unitunkid")
                dicAlloc.Add(cboJob.Name, "jobunkid")
                dicAlloc.Add(cboGrade.Name, "gradeunkid")
                dicAlloc.Add(cboClass.Name, "classunkid")
                dicAlloc.Add(cboCostcenter.Name, "costcenterunkid")
                dicAlloc.Add(cboDepartmentGrp.Name, "deptgroupunkid")
                dicAlloc.Add(cboSectionGroup.Name, "sectiongroupunkid")
                dicAlloc.Add(cboUnitGroup.Name, "unitgroupunkid")
                dicAlloc.Add(cboTeams.Name, "teamunkid")
                dicAlloc.Add(cboJobGroup.Name, "jobgroupunkid")
                dicAlloc.Add(cboGradeGroup.Name, "gradegroupunkid")
                dicAlloc.Add(cboGradeLevel.Name, "gradelevelunkid")
                dicAlloc.Add(cboClassGroup.Name, "classgroupunkid")

                Dim lstCombo As List(Of ComboBox) = (From p In pnlAllication.Controls.OfType(Of ComboBox)() Where (dicAlloc.ContainsKey(p.Name) = True AndAlso CInt(p.SelectedValue) > 0) Select (p)).ToList

                For Each cbo As ComboBox In lstCombo
                    strFilter &= " AND " & dicAlloc.Item(cbo.Name) & " = " & cbo.SelectedValue.ToString & " "
                Next
                If strFilter.Trim.Length > 0 Then
                    strFilter = strFilter.Substring(4)
                End If

                Dim dsList As DataSet = objBenefitAllocation.GetEmployeeAllocationInPeriod(Nothing, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , mdtPeriodStartdate, mdtPeriodEnddate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", False, strFilter)

                Dim strEmpList As String = String.Join(",", (From p In dsList.Tables(0).Select(strFilter) Select (p.Item("employeeunkid").ToString)).Distinct.ToArray)

                If strEmpList.Trim.Length > 0 Then
                    Dim objTnALeave As New clsTnALeaveTran
                    If objTnALeave.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpList, mdtPeriodEnddate) = True Then
                        For Each cbo As ComboBox In lstCombo
                            cbo.Enabled = False
                        Next
                    End If
                End If
                'Sohail (27 Sep 2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBenefitAllocation._Classunkid = CInt(cboClass.SelectedValue)
            objBenefitAllocation._Costcenterunkid = CInt(cboCostcenter.SelectedValue)
            objBenefitAllocation._Departmentunkid = CInt(cboDepartment.SelectedValue)
            objBenefitAllocation._Gradeunkid = CInt(cboGrade.SelectedValue)
            objBenefitAllocation._Jobunkid = CInt(cboJob.SelectedValue)
            objBenefitAllocation._Sectionunkid = CInt(cboSection.SelectedValue)
            objBenefitAllocation._Stationunkid = CInt(cboBranch.SelectedValue)
            objBenefitAllocation._Unitunkid = CInt(cboUnit.SelectedValue)
            objBenefitAllocation._Benefitgroupunkid = CInt(cboBenefitGroup.SelectedValue)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objBenefitAllocation._Periodunkid = CInt(cboPeriod.SelectedValue)
            objBenefitAllocation._IsActive = True
            'Sohail (17 Sep 2019) -- End
            If mintBenefitAllocationUnkid = -1 Then
                objBenefitAllocation._Isvoid = False
                objBenefitAllocation._Voiddatetime = Nothing
                objBenefitAllocation._Voiduserunkid = -1
                objBenefitAllocation._Voidreason = ""
                objBenefitAllocation._Userunkid = User._Object._Userunkid
            Else
                objBenefitAllocation._Isvoid = objBenefitAllocation._Isvoid
                objBenefitAllocation._Voiddatetime = objBenefitAllocation._Voiddatetime
                objBenefitAllocation._Voiduserunkid = objBenefitAllocation._Voiduserunkid
                objBenefitAllocation._Voidreason = objBenefitAllocation._Voidreason
            End If

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            objBenefitAllocation._DepartmentGroupunkid = CInt(cboDepartmentGrp.SelectedValue)
            objBenefitAllocation._SectionGroupunkid = CInt(cboSectionGroup.SelectedValue)
            objBenefitAllocation._UnitGroupunkid = CInt(cboUnitGroup.SelectedValue)
            objBenefitAllocation._Teamunkid = CInt(cboTeams.SelectedValue)
            objBenefitAllocation._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
            objBenefitAllocation._GradeGroupunkid = CInt(cboGradeGroup.SelectedValue)
            objBenefitAllocation._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            objBenefitAllocation._ClassGroupunkid = CInt(cboClassGroup.SelectedValue)
            'Pinkal (24-Feb-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillBenefitPlan()
        Dim objBenefitPlan As New clsbenefitplan_master
        'Sohail (23 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objBenefitAllocTran As New clsBenefit_allocation_tran
        Dim objTranHead As New clsTransactionHead
        'Sohail (23 Jan 2012) -- End
        Dim objMaster As New clsMasterData 'Sohail (19 Mar 2016)
        Dim dsList As New DataSet
        'Dim lvItem As ListViewItem
        Try
            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objBenefitPlan.getComboList("BenefitPlan", False, CInt(cboBenefitGroup.SelectedValue))
            'lvBenefitPlan.Items.Clear()
            'For Each dtRow As DataRow In dsList.Tables("BenefitPlan").Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = dtRow.Item("name").ToString
            '    lvItem.Tag = dtRow.Item("benefitplanunkid")

            '    lvBenefitPlan.Items.Add(lvItem)

            '    lvItem = Nothing
            'Next
            objBenefitAllocTran._Benefitallocationunkid = mintBenefitAllocationUnkid
            mdtTable = objBenefitAllocTran._Datasource

            'If menAction <> enAction.EDIT_ONE Then 'Sohail (19 Mar 2016)
            Dim dRow As DataRow

            dsList = objBenefitPlan.getComboList("BenefitPlan", False, CInt(cboBenefitGroup.SelectedValue))

            For Each dtRow As DataRow In dsList.Tables("BenefitPlan").Rows
                'Sohail (19 Mar 2016) -- Start
                'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
                If mdtTable.Select("benefitplanunkid = " & CInt(dtRow.Item("benefitplanunkid")) & " ").Length > 0 Then Continue For
                'Sohail (19 Mar 2016) -- End
                dRow = mdtTable.NewRow

                'dRow.Item("benefitallocationtranunkid") = CInt(dtRow.Item("benefitallocationtranunkid"))
                'dRow.Item("benefitallocationunkid") = CInt(dtRow.Item("benefitallocationunkid"))
                dRow.Item("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                dRow.Item("benefitplanunkid") = CInt(dtRow.Item("benefitplanunkid"))
                dRow.Item("benefitplanname") = dtRow.Item("name").ToString
                'dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                'dRow.Item("trnheadname") = dtRow.Item("trnheadname").ToString
                'dRow.Item("amount") = CDec(dtRow.Item("amount"))
                'dRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                'dRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                'dRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                'dRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                'dRow.Item("AUD") = dtRow.Item("AUD").ToString

                mdtTable.Rows.Add(dRow)
            Next

            'End If 'Sohail (19 Mar 2016)
            dgvBenefit.AutoGenerateColumns = False

            'Sohail (19 Mar 2016) -- Start
            'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            dsList = objMaster.getComboListForHeadType("HeadType")
            colhTranHeadType.DisplayMember = "name"
            colhTranHeadType.ValueMember = "id"
            colhTranHeadType.DataSource = dsList.Tables("HeadType")
            colhTranHeadType.DropDownWidth = 300
            'Sohail (19 Mar 2016) -- End

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objTranHead.getComboList("Head", True, , , enTypeOf.BENEFIT)
            Dim dTable As New DataTable
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("Head", True, , , enTypeOf.BENEFIT)
            'dTable = objTranHead.getComboList("Head", , , , enTypeOf.Allowance).Tables(0)
            'Sohail (19 Mar 2016) -- Start
            'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            'dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , enTypeOf.BENEFIT)
            'dTable = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", , , , enTypeOf.Allowance).Tables(0)
            ''Sohail (21 Aug 2015) -- End
            'dsList.Tables(0).Merge(dTable)
            ''S.SANDEEP [ 20 MARCH 2012 ] -- END
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            'Sohail (19 Mar 2016) -- End

            colhTranHead.DisplayMember = "name"
            colhTranHead.ValueMember = "tranheadunkid"
            colhTranHead.DataSource = dsList.Tables("Head")
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            colhTranHead.DropDownWidth = 300
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            colhCheck.DataPropertyName = "IsChecked"
            colhBATranUnkId.DataPropertyName = "benefitallocationtranunkid"
            colhBAUnkID.DataPropertyName = "benefitallocationunkid"
            colhBPlanUnkID.DataPropertyName = "benefitplanunkid"
            colhBPLanName.DataPropertyName = "benefitplanname"
            colhTranHead.DataPropertyName = "tranheadunkid"
            colhAmount.DataPropertyName = "amount"
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            colhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'Sohail (17 Sep 2019) -- End
            colhTranHeadType.DataPropertyName = "trnheadtype_id" 'Sohail (19 Mar 2016)

            dgvBenefit.DataSource = mdtTable
            dgvBenefit.Refresh()
            'Sohail (23 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBenefitPlan", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot change benefit allocation. Reason: Period is already closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select effective period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            'Sohail (17 Sep 2019) -- End

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'If CInt(cboBranch.SelectedValue) <= 0 AndAlso _
            '   CInt(cboClass.SelectedValue) <= 0 AndAlso _
            '   CInt(cboCostcenter.SelectedValue) <= 0 AndAlso _
            '   CInt(cboDepartment.SelectedValue) <= 0 AndAlso _
            '   CInt(cboGrade.SelectedValue) <= 0 AndAlso _
            '   CInt(cboJob.SelectedValue) <= 0 AndAlso _
            '   CInt(cboSection.SelectedValue) <= 0 AndAlso _
            '   CInt(cboUnit.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Allocation to continue."), enMsgBoxStyle.Information)
            '    Return False
            'End If


            If CInt(cboBranch.SelectedValue) <= 0 AndAlso _
               CInt(cboClass.SelectedValue) <= 0 AndAlso _
               CInt(cboCostcenter.SelectedValue) <= 0 AndAlso _
               CInt(cboDepartment.SelectedValue) <= 0 AndAlso _
               CInt(cboGrade.SelectedValue) <= 0 AndAlso _
               CInt(cboJob.SelectedValue) <= 0 AndAlso _
               CInt(cboSection.SelectedValue) <= 0 AndAlso _
                CInt(cboUnit.SelectedValue) <= 0 AndAlso _
                CInt(cboDepartmentGrp.SelectedValue) <= 0 AndAlso _
                CInt(cboSectionGroup.SelectedValue) <= 0 AndAlso _
                CInt(cboUnitGroup.SelectedValue) <= 0 AndAlso _
                CInt(cboTeams.SelectedValue) <= 0 AndAlso _
                CInt(cboJobGroup.SelectedValue) <= 0 AndAlso _
                CInt(cboGradeGroup.SelectedValue) <= 0 AndAlso _
                CInt(cboGradeLevel.SelectedValue) <= 0 AndAlso _
                CInt(cboClassGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Allocation to continue."), enMsgBoxStyle.Information)
                Return False
            End If


            'Pinkal (24-Feb-2017) -- End


            If CInt(cboBenefitGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrBenefitGroups, 1, "Please select atleast one Benefit Group to continue."), enMsgBoxStyle.Information)
                cboBenefitGroup.Focus()
                Return False
            End If

            If dgvBenefit.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrBenefitGroups, 3, "Sorry! There is no Benefit Plan in this Benefit Group."), enMsgBoxStyle.Information)
                cboBenefitGroup.Focus()
                Return False
            End If

            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
            Dim strFilter As String = "" 
            Dim dicAlloc As New Dictionary(Of String, String)
            dicAlloc.Add(cboBranch.Name, "stationunkid")
            dicAlloc.Add(cboDepartment.Name, "departmentunkid")
            dicAlloc.Add(cboSection.Name, "sectionunkid")
            dicAlloc.Add(cboUnit.Name, "unitunkid")
            dicAlloc.Add(cboJob.Name, "jobunkid")
            dicAlloc.Add(cboGrade.Name, "gradeunkid")
            dicAlloc.Add(cboClass.Name, "classunkid")
            dicAlloc.Add(cboCostcenter.Name, "costcenterunkid")
            dicAlloc.Add(cboDepartmentGrp.Name, "deptgroupunkid")
            dicAlloc.Add(cboSectionGroup.Name, "sectiongroupunkid")
            dicAlloc.Add(cboUnitGroup.Name, "unitgroupunkid")
            dicAlloc.Add(cboTeams.Name, "teamunkid")
            dicAlloc.Add(cboJobGroup.Name, "jobgroupunkid")
            dicAlloc.Add(cboGradeGroup.Name, "gradegroupunkid")
            dicAlloc.Add(cboGradeLevel.Name, "gradelevelunkid")
            dicAlloc.Add(cboClassGroup.Name, "classgroupunkid")

            Dim lstCombo As List(Of ComboBox) = (From p In pnlAllication.Controls.OfType(Of ComboBox)() Where (dicAlloc.ContainsKey(p.Name) = True AndAlso CInt(p.SelectedValue) > 0) Select (p)).ToList

            For Each cbo As ComboBox In lstCombo
                strFilter &= " AND " & dicAlloc.Item(cbo.Name) & " = " & cbo.SelectedValue.ToString & " "
            Next
            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(4)
            End If

            Dim dsList As DataSet = objBenefitAllocation.GetEmployeeAllocationInPeriod(Nothing, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                               , mdtPeriodStartdate, mdtPeriodEnddate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", False, strFilter)

            Dim strEmpList As String = String.Join(",", (From p In dsList.Tables(0).Select(strFilter) Select (p.Item("employeeunkid").ToString)).Distinct.ToArray)

            If strEmpList.Trim.Length > 0 Then
                Dim objTnALeave As New clsTnALeaveTran
                If objTnALeave.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpList, mdtPeriodEnddate) = True Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Process Payroll is already done for last date of selected Period for selected allocation. Please Void Process Payroll first.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 14, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    Return False
                End If
            End If
            'Sohail (27 Sep 2019) -- End

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = True Or ConfigParameter._Object._IsArutiDemo = True Then
                Dim blnFound As Boolean = False
                For Each dRow As DataGridViewRow In dgvBenefit.Rows
                    If CInt(dRow.Cells(colhTranHead.Index).Value) > 0 Then
                        blnFound = True
                    End If
                Next
                If blnFound = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Map Transaction Head with atleast one Benefit Plan."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub SetVisibility()

        Try
            objbtnAddClass.Enabled = User._Object.Privilege._AddClasses
            objbtnAddCostCenter.Enabled = User._Object.Privilege._AddCostCenter
            objbtnAddDepartment.Enabled = User._Object.Privilege._AddDepartment
            objbtnAddGrade.Enabled = User._Object.Privilege._AddGrade
            objbtnAddJob.Enabled = User._Object.Privilege._AddJob
            objbtnAddSection.Enabled = User._Object.Privilege._AddSection
            objbtnAddStation.Enabled = User._Object.Privilege._AddStation
            objbtnAddUnit.Enabled = User._Object.Privilege._AddUnit


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            objbtnAddDeptGrp.Enabled = User._Object.Privilege._AddDepartmentGroup
            objbtnAddSectionGrp.Enabled = User._Object.Privilege._AddSectionGroup
            objbtnAddUnitGrp.Enabled = User._Object.Privilege._AddUnitGroup
            objbtnAddTeam.Enabled = User._Object.Privilege._AddTeam
            objbtnAddGradeGrp.Enabled = User._Object.Privilege._AddGradeGroup
            objbtnAddGradeLevel.Enabled = User._Object.Privilege._AddGradeLevel
            objbtnAddJobGrp.Enabled = User._Object.Privilege._AddJobGroup
            objbtnAddClassGrp.Enabled = User._Object.Privilege._AddClassGroup
            'Pinkal (24-Feb-2017) -- End


            If ConfigParameter._Object._IsArutiDemo = False Then
                objchkSelectAll.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                dgvBenefit.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 10, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmBenefitallocation_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBenefitAllocation = Nothing
    End Sub

    Private Sub frmBenefitallocation_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmBenefitallocation_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmBenefitallocation_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefitAllocation = New clsBenefitAllocation
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objBenefitAllocation._Benefitallocationunkid = mintBenefitAllocationUnkid
                cboBenefitGroup.Enabled = False 'Sohail (23 Jan 2012)
                objbtnSearchGroup.Enabled = False 'Sohail (23 Jan 2012)
                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                cboPeriod.Enabled = False
                'Sohail (17 Sep 2019) -- End
            End If

            Call GetValue()

            cboBranch.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitallocation_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBenefitAllocation.SetMessages()
            objfrm._Other_ModuleNames = "clsBenefitAllocation"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Call SetValue()

            Dim dtTable As DataTable = Nothing
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = True Or ConfigParameter._Object._IsArutiDemo = True Then
                dtTable = New DataView(mdtTable, "tranheadunkid > 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(mdtTable, "", "", DataViewRowState.CurrentRows).ToTable
            End If

            If menAction = enAction.EDIT_ONE Then
                objBenefitAllocation._Voiduserunkid = User._Object._Userunkid
                blnFlag = objBenefitAllocation.Delete(mintBenefitAllocationUnkid, dtTable)
            Else
                blnFlag = objBenefitAllocation.Insert(dtTable)
            End If

            If blnFlag = False And objBenefitAllocation._Message <> "" Then
                eZeeMsgBox.Show(objBenefitAllocation._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBenefitAllocation = Nothing
                    objBenefitAllocation = New clsBenefitAllocation
                    Call GetValue()
                    cboBranch.Focus()
                Else
                    mintBenefitAllocationUnkid = objBenefitAllocation._Benefitallocationunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Datagridview's Events "
    Private Sub dgvBenefit_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBenefit.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub 'Sohail (19 Mar 2016)
            If e.ColumnIndex = colhTranHead.Index Then
                SendKeys.Send("{F2}")
                'Sohail (19 Mar 2016) -- Start
                'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            ElseIf e.ColumnIndex = colhTranHeadType.Index Then
                SendKeys.Send("{F2}")
                'Sohail (19 Mar 2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBenefit_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBenefit_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvBenefit.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        Try
            'If e.ColumnIndex = colhTranHead.Index AndAlso CInt(dgvBenefit.CurrentRow.Cells(colhTranHeadType.Index).Value) <= 0 AndAlso CInt(dgvBenefit.CurrentRow.Cells(colhTranHead.Index).Value) > 0 Then
            If e.ColumnIndex = colhTranHead.Index AndAlso CInt(dgvBenefit.CurrentRow.Cells(colhTranHeadType.Index).Value) <= 0 Then
                Dim c As ComboBox = CType(dgvBenefit.EditingControl, ComboBox)
                If c IsNot Nothing AndAlso CInt(c.SelectedValue) > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Transaction Head Type first."), enMsgBoxStyle.Information)
                    c.SelectedValue = "0"
                    dgvBenefit.CurrentRow.Cells(colhTranHead.Index).Value = "0"
                    e.Cancel = True
                End If
            ElseIf e.ColumnIndex = colhTranHead.Index Then
                Dim objTranhead As New clsTransactionHead
                Dim c As ComboBox = CType(dgvBenefit.EditingControl, ComboBox)
                If c IsNot Nothing AndAlso CInt(c.SelectedValue) > 0 Then
                    Dim ds As DataSet = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "Head", False, , , , , , "trnheadtype_id = " & CInt(dgvBenefit.CurrentRow.Cells(colhTranHeadType.Index).Value) & " AND tranheadunkid = " & CInt(c.SelectedValue) & " ")
                    If ds.Tables("Head").Rows.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Transaction Head of") & "  " & dgvBenefit.CurrentRow.Cells(colhTranHeadType.Index).FormattedValue.ToString, enMsgBoxStyle.Information)
                        c.SelectedValue = "0"
                        dgvBenefit.CurrentRow.Cells(colhTranHead.Index).Value = "0"
                        e.Cancel = True
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Sohail (19 Mar 2016) -- Start
    'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
    Private Sub dgvBenefit_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBenefit.CellValueChanged
        If e.RowIndex < 0 Then
            Exit Sub
        End If

        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try

            If e.ColumnIndex = colhTranHeadType.Index Then
                Dim intHeadType As Integer = CInt(dgvBenefit.Rows(e.RowIndex).Cells(colhTranHeadType.Index).Value)
                'Dim dv As DataView = New DataView(dtChoices, "Department = '" & Department & "'", "", DataViewRowState.CurrentRows)
                'If cb IsNot Nothing AndAlso cb.SelectedValue IsNot Nothing Then
                dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " AND trnheadtype_id = " & intHeadType & " ")
                'Else
                '    dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
                'End If

                Dim dgvComboCell As DataGridViewComboBoxCell = CType(dgvBenefit.Rows(e.RowIndex).Cells(colhTranHead.Index), DataGridViewComboBoxCell)
                'S.SANDEEP [05-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-28}
                'With dgvComboCell
                '    .DataSource = Nothing
                '    .DisplayMember = "name"
                '    .ValueMember = "tranheadunkid"
                '    .DataSource = dsList.Tables("Head")
                '    .DropDownWidth = 300
                'End With
                With dgvComboCell
                    .DataSource = Nothing
                    .DataSource = dsList.Tables("Head").Copy()
                    .DisplayMember = "name"
                    .ValueMember = "tranheadunkid"
                    .DropDownWidth = 300
                End With
                'S.SANDEEP [05-Mar-2018] -- END
                dgvBenefit.CurrentRow.Cells(colhTranHead.Index).Value = "0"
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Mar 2016) -- End

    Private Sub dgvBenefit_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBenefit.DataError
        Try
            'Sohail (19 Mar 2016) -- Start
            'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            If dgvBenefit.CurrentCell.ColumnIndex = colhTranHeadType.Index OrElse dgvBenefit.CurrentCell.ColumnIndex = colhTranHead.Index Then
                If e.Exception.Message.ToUpper.Contains("DATAGRIDVIEWCOMBOBOXCELL VALUE IS NOT VALID.") = True Then Exit Try
            End If
            'Sohail (19 Mar 2016) -- End
            eZeeMsgBox.Show(e.Exception.Message, enMsgBoxStyle.Critical)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBenefit_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBenefit_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBenefit.EditingControlShowing
        Dim tb As TextBox
        Try
            If dgvBenefit.CurrentCell.ColumnIndex = colhTranHead.Index Then
                Dim cb As ComboBox = TryCast(e.Control, ComboBox)
                If cb IsNot Nothing Then
                    RemoveHandler cb.Validating, AddressOf TranHead_Validating

                    AddHandler cb.Validating, AddressOf TranHead_Validating
                End If
                'Sohail (19 Mar 2016) -- Start
                'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            ElseIf dgvBenefit.CurrentCell.ColumnIndex = colhTranHeadType.Index Then
                Dim cb As ComboBox = TryCast(e.Control, ComboBox)
                If cb IsNot Nothing Then
                    'RemoveHandler cb.SelectionChangeCommitted, AddressOf TranHeadType_SelectionChangeCommitted

                    'AddHandler cb.SelectionChangeCommitted, AddressOf TranHeadType_SelectionChangeCommitted
                End If
                'Sohail (19 Mar 2016) -- End
            ElseIf dgvBenefit.CurrentCell.ColumnIndex = colhAmount.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress

                AddHandler tb.KeyPress, AddressOf tb_keypress
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBenefit_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    'Sohail (19 Mar 2016) -- Start
    'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
    'Private Sub TranHeadType_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim objTranHead As New clsTransactionHead
    '    Dim dsList As DataSet
    '    Try
    '        If dgvBenefit.CurrentCell.ColumnIndex <> colhTranHeadType.Index Then Exit Try
    '        Dim cb As ComboBox = TryCast(sender, ComboBox)
    '        If cb IsNot Nothing AndAlso cb.SelectedValue IsNot Nothing Then
    '            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " AND trnheadtype_id = " & CInt(cb.SelectedValue) & " ")
    '        Else
    '            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
    '        End If
    '        colhTranHead.DataSource = Nothing
    '        colhTranHead.DisplayMember = "name"
    '        colhTranHead.ValueMember = "tranheadunkid"
    '        colhTranHead.DataSource = dsList.Tables("Head")
    '        colhTranHead.DropDownWidth = 300

    '        dgvBenefit.CurrentRow.Cells(colhTranHead.Index).Value = "0"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "TranHeadType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (19 Mar 2016) -- End

    Private Sub TranHead_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim objTranHead As New clsTransactionHead
        Try
            If dgvBenefit.CurrentCell.ColumnIndex <> colhTranHead.Index Then Exit Try
            Dim cb As ComboBox = TryCast(sender, ComboBox)
            If cb IsNot Nothing Then
                If cb.SelectedIndex <> 0 Then
                    For Each dRow As DataGridViewRow In dgvBenefit.Rows
                        If dRow.Index <> dgvBenefit.CurrentRow.Index AndAlso Convert.ToInt32(dRow.Cells(colhTranHead.Index).Value) = Convert.ToInt32(cb.SelectedValue) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! This Transaction Head is already Mapped with ") & dRow.Cells(colhBPLanName.Index).FormattedValue.ToString & Language.getMessage(mstrModuleName, 5, " Benefit Plan.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please select different Transaction Head."), enMsgBoxStyle.Information)
                            dgvBenefit.CurrentCell.Value = 0
                            cb.SelectedIndex = 0
                            e.Cancel = True
                            Exit Sub
                        End If
                    Next
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = CInt(cb.SelectedValue)
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cb.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                        dgvBenefit.CurrentRow.Cells(colhAmount.Index).ReadOnly = False
                        dgvBenefit.CurrentRow.Cells(colhAmount.Index).Style.BackColor = Nothing
                    Else
                        dgvBenefit.CurrentRow.Cells(colhAmount.Index).Value = 0
                        dgvBenefit.CurrentRow.Cells(colhAmount.Index).ReadOnly = True
                        dgvBenefit.CurrentRow.Cells(colhAmount.Index).Style.BackColor = GUI.ColorOptional
                    End If
                Else
                    dgvBenefit.CurrentRow.Cells(colhAmount.Index).Value = 0
                    dgvBenefit.CurrentRow.Cells(colhAmount.Index).ReadOnly = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TranHead_Validating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Contols "
    Private Sub objbtnAddStation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddStation.Click
        Dim frm As New frmStation_AddEdit
        Dim dscombo As New DataSet
        Dim objStation As New clsStation
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objStation.getComboList("Station", True)
                With cboBranch
                    .ValueMember = "stationunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Station")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddStation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepartment.Click
        Dim frm As New frmDepartment_AddEdit
        Dim dscombo As New DataSet
        Dim objDepartment As New clsDepartment
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objDepartment.getComboList("Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Department")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDepartment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSection.Click
        Dim frm As New frmSections_AddEdit
        Dim dscombo As New DataSet
        Dim objSection As New clsSections
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objSection.getComboList("Section", True)
                With cboSection
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Section")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSection_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddUnit.Click
        Dim frm As New frmUnits_AddEdit
        Dim dscombo As New DataSet
        Dim objUnit As New clsUnits
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objUnit.getComboList("Unit", True)
                With cboUnit
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Unit")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddUnit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJob.Click
        Dim frm As New frmJobs_AddEdit
        Dim dscombo As New DataSet
        Dim objJobs As New clsJobs
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objJobs.getComboList("Jobs", True)
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Jobs")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
        Dim frm As New frmGrade_AddEdit
        Dim dscombo As New DataSet
        Dim objGrade As New clsGrade
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objGrade.getComboList("Grade", True)
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Grade")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddClass.Click
        Dim frm As New frmClasses_AddEdit
        Dim dscombo As New DataSet
        Dim objClass As New clsClass
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objClass.getComboList("Class", True)
                With cboClass
                    .ValueMember = "classesunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Class")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddClass_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCostCenter.Click
        Dim frm As New frmCostcenter_AddEdit
        Dim dscombo As New DataSet
        Dim objCCenter As New clscostcenter_master
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objCCenter.getComboList("CostCenter", True)
                With cboCostcenter
                    .ValueMember = "costcenterunkid"
                    .DisplayMember = "costcentername"
                    .DataSource = dscombo.Tables("CostCenter")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCostCenter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBenefitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBenefitGroup.SelectedIndexChanged
        Try
            If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                Call FillBenefitPlan()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBenefitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 


            With objfrm
                .ValueMember = cboBenefitGroup.ValueMember
                .DisplayMember = cboBenefitGroup.DisplayMember
                .DataSource = CType(cboBenefitGroup.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                With cboBenefitGroup
                    .SelectedValue = objfrm.SelectedValue
                    .Focus()
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

    Private Sub objbtnAddDeptGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDeptGrp.Click
        Dim frm As New frmDepartmentGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objDeptGrp As New clsDepartmentGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objDeptGrp.getComboList("DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("DeptGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDeptGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSectionGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSectionGrp.Click
        Dim frm As New frmSectionGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objSectionGrp As New clsSectionGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objSectionGrp.getComboList("SectionGrp", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("SectionGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSectionGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddUnitGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddUnitGrp.Click
        Dim frm As New frmUnitGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objUnitGroup As New clsUnitGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objUnitGroup.getComboList("UnitGrp", True)
                With cboUnitGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("SectionGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddUnitGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTeam.Click
        Dim frm As New frmTeams_AddEdit
        Dim dscombo As New DataSet
        Dim objTeam As New clsTeams
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objTeam.getComboList("Team", True)
                With cboTeams
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("Team")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTeam_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGradeGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGradeGrp.Click
        Dim frm As New frmGradeGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objGradeGrp As New clsGradeGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objGradeGrp.getComboList("GradeGrp", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("GradeGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGradeGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddJobGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJobGrp.Click
        Dim frm As New frmJobGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objGradeGrp As New clsGradeGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objGradeGrp.getComboList("GradeGrp", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("GradeGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJobGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddClassGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddClassGrp.Click
        Dim frm As New frmClassGroup_AddEdit
        Dim dscombo As New DataSet
        Dim objClassGrp As New clsClassGroup
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dscombo = objClassGrp.getComboList("ClassGrp", True)
                With cboClassGroup
                    .ValueMember = "classgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dscombo.Tables("ClassGrp")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddClassGrp_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Feb-2017) -- End


#End Region

    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

#Region "Combobox Event"

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGrade As New clsGrade
        Try
            dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
            End With
            Call SetDefaultSearchText(cboGrade) 'Hemant (22 June 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeLevel")
            End With
            Call SetDefaultSearchText(cboGradeLevel) 'Hemant (22 June 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub cboCostcenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostcenter.KeyPress, _
                                                                                                   cboBranch.KeyPress, _
                                                                                                   cboDepartmentGrp.KeyPress, _
                                                                                                   cboDepartment.KeyPress, _
                                                                                                   cboSectionGroup.KeyPress, _
                                                                                                   cboSection.KeyPress, _
                                                                                                   cboUnitGroup.KeyPress, _
                                                                                                   cboUnit.KeyPress, _
                                                                                                   cboTeams.KeyPress, _
                                                                                                   cboGradeGroup.KeyPress, _
                                                                                                   cboGrade.KeyPress, _
                                                                                                   cboGradeLevel.KeyPress, _
                                                                                                   cboJobGroup.KeyPress, _
                                                                                                   cboJob.KeyPress, _
                                                                                                   cboClassGroup.KeyPress, _
                                                                                                   cboClass.KeyPress

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboCostcenter.Name Then
                        .CodeMember = "costcentercode"
                    ElseIf cbo.Name = cboBranch.Name OrElse cbo.Name = cboDepartmentGrp.Name OrElse cbo.Name = cboDepartment.Name OrElse _
                           cbo.Name = cboSectionGroup.Name OrElse cbo.Name = cboSection.Name OrElse cbo.Name = cboUnitGroup.Name OrElse _
                           cbo.Name = cboUnit.Name OrElse cbo.Name = cboTeams.Name OrElse cbo.Name = cboGradeGroup.Name OrElse _
                           cbo.Name = cboGrade.Name OrElse cbo.Name = cboGradeLevel.Name OrElse cbo.Name = cboJobGroup.Name OrElse _
                           cbo.Name = cboJob.Name Then
                    Else

                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.SelectedIndexChanged, _
                                                                                                   cboBranch.SelectedIndexChanged, _
                                                                                                   cboDepartmentGrp.SelectedIndexChanged, _
                                                                                                   cboDepartment.SelectedIndexChanged, _
                                                                                                   cboSectionGroup.SelectedIndexChanged, _
                                                                                                   cboSection.SelectedIndexChanged, _
                                                                                                   cboUnitGroup.SelectedIndexChanged, _
                                                                                                   cboUnit.SelectedIndexChanged, _
                                                                                                   cboTeams.SelectedIndexChanged, _
                                                                                                   cboGradeGroup.SelectedIndexChanged, _
                                                                                                   cboGrade.SelectedIndexChanged, _
                                                                                                   cboGradeLevel.SelectedIndexChanged, _
                                                                                                   cboJobGroup.SelectedIndexChanged, _
                                                                                                   cboJob.SelectedIndexChanged, _
                                                                                                   cboClassGroup.SelectedIndexChanged, _
                                                                                                   cboClass.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.GotFocus, _
                                                                                                   cboBranch.GotFocus, _
                                                                                                   cboDepartmentGrp.GotFocus, _
                                                                                                   cboDepartment.GotFocus, _
                                                                                                   cboSectionGroup.GotFocus, _
                                                                                                   cboSection.GotFocus, _
                                                                                                   cboUnitGroup.GotFocus, _
                                                                                                   cboUnit.GotFocus, _
                                                                                                   cboTeams.GotFocus, _
                                                                                                   cboGradeGroup.GotFocus, _
                                                                                                   cboGrade.GotFocus, _
                                                                                                   cboGradeLevel.GotFocus, _
                                                                                                   cboJobGroup.GotFocus, _
                                                                                                   cboJob.GotFocus, _
                                                                                                   cboClassGroup.GotFocus, _
                                                                                                   cboClass.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.Leave, _
                                                                                                   cboBranch.Leave, _
                                                                                                   cboDepartmentGrp.Leave, _
                                                                                                   cboDepartment.Leave, _
                                                                                                   cboSectionGroup.Leave, _
                                                                                                   cboSection.Leave, _
                                                                                                   cboUnitGroup.Leave, _
                                                                                                   cboUnit.Leave, _
                                                                                                   cboTeams.Leave, _
                                                                                                   cboGradeGroup.Leave, _
                                                                                                   cboGrade.Leave, _
                                                                                                   cboGradeLevel.Leave, _
                                                                                                   cboJobGroup.Leave, _
                                                                                                   cboJob.Leave, _
                                                                                                   cboClassGroup.Leave, _
                                                                                                   cboClass.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPeriodStartdate = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriodEnddate = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Sep 2019) -- End

#End Region

    'Pinkal (24-Feb-2017) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

			Me.pnlAllication.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.pnlAllication.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.pnlAllication.Text = Language._Object.getCaption(Me.pnlAllication.Name, Me.pnlAllication.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblBenefitGroup.Text = Language._Object.getCaption(Me.lblBenefitGroup.Name, Me.lblBenefitGroup.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.colhCheck.HeaderText = Language._Object.getCaption(Me.colhCheck.Name, Me.colhCheck.HeaderText)
            Me.colhBATranUnkId.HeaderText = Language._Object.getCaption(Me.colhBATranUnkId.Name, Me.colhBATranUnkId.HeaderText)
            Me.colhBAUnkID.HeaderText = Language._Object.getCaption(Me.colhBAUnkID.Name, Me.colhBAUnkID.HeaderText)
            Me.colhBPlanUnkID.HeaderText = Language._Object.getCaption(Me.colhBPlanUnkID.Name, Me.colhBPlanUnkID.HeaderText)
            Me.colhBPLanName.HeaderText = Language._Object.getCaption(Me.colhBPLanName.Name, Me.colhBPLanName.HeaderText)
            Me.colhTranHeadType.HeaderText = Language._Object.getCaption(Me.colhTranHeadType.Name, Me.colhTranHeadType.HeaderText)
            Me.colhTranHead.HeaderText = Language._Object.getCaption(Me.colhTranHead.Name, Me.colhTranHead.HeaderText)
            Me.colhAmount.HeaderText = Language._Object.getCaption(Me.colhAmount.Name, Me.colhAmount.HeaderText)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrBenefitGroups, 1, "Please select atleast one Benefit Group to continue.")
            Language.setMessage(mstrModuleName, 2, "Please Select atleast one Allocation to continue.")
            Language.setMessage(mstrBenefitGroups, 3, "Sorry! There is no Benefit Plan in this Benefit Group.")
            Language.setMessage(mstrModuleName, 4, "Sorry! This Transaction Head is already Mapped with")
            Language.setMessage(mstrModuleName, 5, " Benefit Plan.")
            Language.setMessage(mstrModuleName, 6, "Please select different Transaction Head.")
            Language.setMessage(mstrModuleName, 7, "Please Map Transaction Head with atleast one Benefit Plan.")
            Language.setMessage(mstrModuleName, 8, "Please select Transaction Head Type first.")
            Language.setMessage(mstrModuleName, 9, "Please select Transaction Head of")
            Language.setMessage(mstrModuleName, 10, "Type to Search")
			Language.setMessage(mstrModuleName, 11, "Sorry, You cannot change benefit allocation. Reason: Period is already closed.")
			Language.setMessage(mstrModuleName, 12, "Please select effective period to continue.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Process Payroll is already done for last date of selected Period for selected allocation. Please Void Process Payroll first.")
			Language.setMessage(mstrModuleName, 14, "Do you want to void Payroll?")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class