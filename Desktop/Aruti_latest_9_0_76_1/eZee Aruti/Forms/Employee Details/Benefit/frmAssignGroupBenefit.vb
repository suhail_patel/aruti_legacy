﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmAssignGroupBenefit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAssignGroupBenefit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtTran As DataTable
    Private intSelectedIdx As Integer = -1
    Private objBenefitCoverage As clsBenefitCoverage_tran
    Private mintCurrBenefitAllocationID As Integer 'Sohail (23 Jan 2012)
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
    Private mstrExemptPeriodIds As String = ""

    'Pinkal (08-Jan-2016) -- Start
    'Enhancement - Working on Employee Benefit changes given By Rutta.
    Private mintEmployeeID As Integer = -1
    Private mdtDate As Date = Nothing
    'Pinkal (08-Jan-2016) -- End


    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
    Dim dsEmployee As New DataSet
    'Dim mblnOpenFromImportApplicants As Boolean = False
    'Pinkal (24-Feb-2017) -- End

    Private mstrCheckedEmpIds As String = String.Empty
    Private mstrSearchText As String = "" 'Hemant (22 June 2019)

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, Optional ByVal intEmployeeID As Integer = -1, Optional ByVal dtDate As Date = Nothing, Optional ByVal strCheckedEmpIds As String = "") As Boolean
        Try
            menAction = eAction
            'Pinkal (08-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            mintEmployeeID = intEmployeeID
            mdtDate = dtDate
            'Pinkal (08-Jan-2016) -- End

            mstrCheckedEmpIds = strCheckedEmpIds

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            ' , Optional ByVal blnOpenFromImportApplicants As Boolean = False
            'mblnOpenFromImportApplicants = blnOpenFromImportApplicants
            'Pinkal (24-Feb-2017) -- End

            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboBenefitGroup.BackColor = GUI.ColorOptional

            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            'cboBenefitPlan.BackColor = GUI.ColorOptional
            'cboTranHead.BackColor = GUI.ColorOptional
            'Pinkal (21-Dec-2015) -- End

            cboClass.BackColor = GUI.ColorOptional
            cboDepartment.BackColor = GUI.ColorOptional
            'Hemant (22 June 2019) -- Start
            'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
            'cboGrade.BackColor = GUI.ColorComp
            'cboJob.BackColor = GUI.ColorComp
            cboGrade.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboDepartmentGrp.BackColor = GUI.ColorOptional
            cboSectionGroup.BackColor = GUI.ColorOptional
            cboUnitGroup.BackColor = GUI.ColorOptional
            cboTeams.BackColor = GUI.ColorOptional
            cboGradeGroup.BackColor = GUI.ColorOptional
            cboGradeLevel.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboClassGroup.BackColor = GUI.ColorOptional
            'Hemant (22 June 2019) -- End            
            cboSections.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorOptional
            cboBranch.BackColor = GUI.ColorOptional
            cboUnit.BackColor = GUI.ColorOptional
            cboCostCenter.BackColor = GUI.ColorOptional
            cboPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objDepartment As New clsDepartment
        Dim objJob As New clsJobs
        Dim objSection As New clsSections
        Dim objClass As New clsClass
        Dim objGrade As New clsGrade
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Dim objStation As New clsStation
        Dim objUnit As New clsUnits
        Dim objCostCenter As New clscostcenter_master
        Dim objPeriod As New clscommom_period_Tran


        'Pinkal (24-Feb-2017) -- Start
        'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objSectionGrp As New clsSectionGroup
        Dim objJobGrp As New clsJobGroup
        Dim objGradeGrp As New clsGradeGroup
        Dim objClassGrp As New clsClassGroup
        Dim objTeam As New clsTeams
        Dim objUnitGroup As New clsUnitGroup
        Dim objGradeLevel As New clsGradeLevel
        'Pinkal (24-Feb-2017) -- End

        Try
            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboDepartment) 'Hemant (22 June 2019)

            dsCombos = objJob.getComboList("Jobs", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Jobs")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboJob) 'Hemant (22 June 2019)

            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboSections) 'Hemant (22 June 2019)

            dsCombos = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Class")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboClass) 'Hemant (22 June 2019)

            dsCombos = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGrade) 'Hemant (22 June 2019)

            dsCombos = objStation.getComboList("Station", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboBranch) 'Hemant (22 June 2019)

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnit
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboUnit) 'Hemant (22 June 2019)

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboCostCenter) 'Hemant (22 June 2019)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            dsCombos = objDeptGrp.getComboList("List", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboDepartmentGrp) 'Hemant (22 June 2019)

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboSectionGroup) 'Hemant (22 June 2019)

            dsCombos = objUnitGroup.getComboList("List", True, -1)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboUnitGroup) 'Hemant (22 June 2019)

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboTeams) 'Hemant (22 June 2019)

            dsCombos = objJobGrp.getComboList("List", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboJobGroup) 'Hemant (22 June 2019)

            dsCombos = objGradeGrp.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboGradeGroup) 'Hemant (22 June 2019)

            dsCombos = objClassGrp.getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboClassGroup) 'Hemant (22 June 2019)

            dsCombos = objGradeLevel.getComboList("List", True, -1)
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboGradeLevel) 'Hemant (22 June 2019)

            'Pinkal (24-Feb-2017) -- End



            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With
            AddHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
            'Pinkal (24-Feb-2017) -- End

            'Pinkal (08-Jan-2016) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            If mdtDate <> Nothing Then
                Dim intPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtDate, FinancialYear._Object._YearUnkid, enStatusType.Open)
                If intPeriodId <= 0 Then
                    cboPeriod.SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                Else
                    cboPeriod.SelectedValue = intPeriodId
                End If
                objMaster = Nothing
            Else
                cboPeriod.SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            End If
            'Pinkal (08-Jan-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objDepartment = Nothing
            objJob = Nothing
            objSection = Nothing
            objClass = Nothing
            objGrade = Nothing
            objTranHead = Nothing
            dsCombos = Nothing
            objStation = Nothing
            objUnit = Nothing
            objCostCenter = Nothing
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim objEmployee As New clsEmployee_Master
        Dim lvItem As ListViewItem
        Dim intIndex As Integer = 0
        Try
            lvEmployeeList.Items.Clear()
            If CInt(cboPeriod.SelectedValue) <= 0 Then Exit Try
            Cursor.Current = Cursors.WaitCursor
            lvEmployeeList.BeginUpdate()
            Dim lvArray As New List(Of ListViewItem)

            Dim mstrSearch As String = ""


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    mstrSearch &= "AND employeeunkid  = " & CInt(cboEmployee.SelectedValue) & " "
            'End If
            'If CInt(cboDepartment.SelectedValue) > 0 Then
            '    mstrSearch &= "AND departmentunkid   = " & CInt(cboDepartment.SelectedValue) & " "
            'End If
            'If CInt(cboSections.SelectedValue) > 0 Then
            '    mstrSearch &= "AND sectionunkid  = " & CInt(cboSections.SelectedValue) & " "
            'End If
            'If CInt(cboUnit.SelectedValue) > 0 Then
            '    mstrSearch &= "AND unitunkid  = " & CInt(cboUnit.SelectedValue) & " "
            'End If
            'If CInt(cboGrade.SelectedValue) > 0 Then
            '    mstrSearch &= "AND gradeunkid  = " & CInt(cboGrade.SelectedValue) & " "
            'End If
            'If CInt(cboClass.SelectedValue) > 0 Then
            '    mstrSearch &= "AND classunkid  = " & CInt(cboClass.SelectedValue) & " "
            'End If
            'If CInt(cboCostCenter.SelectedValue) > 0 Then
            '    mstrSearch &= "AND costcenterunkid  = " & CInt(cboCostCenter.SelectedValue) & " "
            'End If
            'If CInt(cboJob.SelectedValue) > 0 Then
            '    mstrSearch &= "AND jobunkid  = " & CInt(cboJob.SelectedValue) & " "
            'End If
            'If CInt(cboBranch.SelectedValue) > 0 Then
            '    mstrSearch &= "AND stationunkid  = " & CInt(cboBranch.SelectedValue) & " "
            'End If


            If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.deptgroupunkid  = " & CInt(cboDepartmentGrp.SelectedValue) & " "
            End If

            If CInt(cboSectionGroup.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.sectiongroupunkid  = " & CInt(cboSectionGroup.SelectedValue) & " "
            End If

            If CInt(cboUnitGroup.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.unitgroupunkid  = " & CInt(cboUnitGroup.SelectedValue) & " "
            End If

            If CInt(cboTeams.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.teamunkid  = " & CInt(cboTeams.SelectedValue) & " "
            End If

            If CInt(cboJobGroup.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.jobgroupunkid  = " & CInt(cboJobGroup.SelectedValue) & " "
            End If

            If CInt(cboGradeGroup.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.gradegroupunkid  = " & CInt(cboGradeGroup.SelectedValue) & " "
            End If

            If CInt(cboGradeLevel.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.gradelevelunkid  = " & CInt(cboGradeLevel.SelectedValue) & " "
            End If

            If CInt(cboClassGroup.SelectedValue) > 0 Then
                mstrSearch &= "AND ADF.classgroupunkid  = " & CInt(cboClassGroup.SelectedValue) & " "
            End If

            If rdShowOnlyNewHiredEmp.Checked Then
                mstrSearch &= "AND hremployee_master.appointeddate  BETWEEN '" & eZeeDate.convertDate(mdtPeriodStartDate.Date) & "'  AND '" & eZeeDate.convertDate(mdtPeriodEndDate.Date) & "'"
            End If

            If rdShowOnlyReinstatedEmp.Checked Then
                mstrSearch &= "AND HIRE.REHIRE  BETWEEN '" & eZeeDate.convertDate(mdtPeriodStartDate.Date) & "'  AND '" & eZeeDate.convertDate(mdtPeriodEndDate.Date) & "'"
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmAssignGroupBenefit))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid _
            '                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                                 , mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, CInt(cboEmployee.SelectedValue) _
            '                                                                 , CInt(cboDepartment.SelectedValue), CInt(cboSections.SelectedValue), CInt(cboUnit.SelectedValue) _
            '                                                                 , CInt(cboGrade.SelectedValue), 0, CInt(cboClass.SelectedValue), CInt(cboCostCenter.SelectedValue) _
            '                                                                 , 0, CInt(cboJob.SelectedValue), 0, CInt(cboBranch.SelectedValue), 0, False, mstrSearch, False, True, True)

            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                             , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                             , mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", False, CInt(cboEmployee.SelectedValue) _
                                                                             , CInt(cboDepartment.SelectedValue), CInt(cboSections.SelectedValue), CInt(cboUnit.SelectedValue) _
                                                                             , CInt(cboGrade.SelectedValue), 0, CInt(cboClass.SelectedValue), CInt(cboCostCenter.SelectedValue) _
                                                                   , 0, CInt(cboJob.SelectedValue), 0, CInt(cboBranch.SelectedValue), 0, False, mstrSearch, False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End



            'Pinkal (24-Feb-2017) -- End

            For Each dtRow As DataRow In dsEmployee.Tables("Emp").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)

                'Pinkal (24-Feb-2017) -- Start
                'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
                'lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
                'Pinkal (24-Feb-2017) -- End

                lvItem.Tag = dtRow.Item("employeeunkid")
                lvArray.Add(lvItem)
                lvItem = Nothing
            Next

            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            lvEmployeeList.Items.AddRange(lvArray.ToArray)
            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            lvArray = Nothing

            lvEmployeeList.EndUpdate()
            Cursor.Current = Cursors.Default


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub FillBenefitGroup()
        Dim dsList As New DataSet
        Dim objBeneAllocation As New clsBenefitAllocation
        Try

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'dsList = objBeneAllocation.GetBenefitGroup("BenefitGroup", _
            '                                            True, _
            '                                            CInt(cboBranch.SelectedValue), _
            '                                            CInt(cboDepartment.SelectedValue), _
            '                                            CInt(cboSections.SelectedValue), _
            '                                            CInt(cboUnit.SelectedValue), _
            '                                            CInt(cboJob.SelectedValue), _
            '                                            CInt(cboGrade.SelectedValue), _
            '                                            CInt(cboClass.SelectedValue), _
            '                                            CInt(cboCostCenter.SelectedValue))

            dsList = objBeneAllocation.GetBenefitGroup("BenefitGroup", _
                                                       True, _
                                                       CInt(cboBranch.SelectedValue), _
                                                       CInt(cboDepartment.SelectedValue), _
                                                       CInt(cboSections.SelectedValue), _
                                                       CInt(cboUnit.SelectedValue), _
                                                       CInt(cboJob.SelectedValue), _
                                                       CInt(cboGrade.SelectedValue), _
                                                       CInt(cboClass.SelectedValue), _
                                                       CInt(cboCostCenter.SelectedValue), _
                                                       CInt(cboDepartmentGrp.SelectedValue), _
                                                       CInt(cboSectionGroup.SelectedValue), _
                                                       CInt(cboUnitGroup.SelectedValue), _
                                                       CInt(cboTeams.SelectedValue), _
                                                       CInt(cboJobGroup.SelectedValue), _
                                                       CInt(cboGradeGroup.SelectedValue), _
                                                       CInt(cboGradeLevel.SelectedValue), _
                                                       CInt(cboClassGroup.SelectedValue))

            'Pinkal (24-Feb-2017) -- End

            With cboBenefitGroup
                .ValueMember = "Id"
                .DisplayMember = "BGroup"
                .DataSource = dsList.Tables("BenefitGroup")
                .SelectedValue = 0
            End With

            'Pinkal (19-Sep-2017) -- Start
            'Enhancement - Solved Group Benefit Issue.
            'For Each dsRow As DataRow In dsList.Tables("BenefitGroup").Rows
            '    mintCurrBenefitAllocationID = CInt(dsRow.Item("benefitallocationunkid"))
            'Next
            'Pinkal (19-Sep-2017) -- End


            'Pinkal (08-Feb-2018) -- Start
            'Enhancement - Internal Issue given by Gajanan.
            lvEmployeeList.Items.Clear()
            'Pinkal (08-Feb-2018) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBenefitGroup", mstrModuleName)
        End Try
    End Sub


    Private Sub FillBenefitPlanList()
        Try

            dgBenefitPlan.AutoGenerateColumns = False
            objdgcolhIscheck.DataPropertyName = "ischeck"
            dgcolhBenefitPlan.DataPropertyName = "benefitplanname"
            dgcolhTranshead.DataPropertyName = "trnheadname"
            dgcolhAmount.DataPropertyName = "amount"
            objdgcolhDefaultAmount.DataPropertyName = "Defaultamount"
            dgcolhPeriod.DataPropertyName = "periodname"
            objdgcolhBenefitPlanunkid.DataPropertyName = "benefitplanunkid"
            objdgcolhTranheadunkid.DataPropertyName = "tranheadunkid"
            objdgcolhPeriodId.DataPropertyName = "periodunkid"


            dgBenefitPlan.DataSource = mdtTran

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBenefitPlanList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            If mdtTran Is Nothing Then Exit Sub

            Dim drRow As DataRow() = mdtTran.Select("ischeck = True")
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgBenefitPlan.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgBenefitPlan.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridValue(ByVal intRowindex As Integer)
        Try
            If mdtTran IsNot Nothing Then
                mdtTran.Rows(intRowindex)("benefitgroupunkid") = -1
                mdtTran.Rows(intRowindex)("benefitgroupid") = -1
                mdtTran.Rows(intRowindex)("periodunkid") = -1
                mdtTran.Rows(intRowindex)("periodname") = ""
                mdtTran.Rows(intRowindex)("actionid") = 0
                mdtTran.Rows(intRowindex)("departmentid") = -1
                mdtTran.Rows(intRowindex)("jobid") = -1
                mdtTran.Rows(intRowindex)("classid") = -1
                mdtTran.Rows(intRowindex)("gradeid") = -1
                mdtTran.Rows(intRowindex)("sectionid") = -1
                mdtTran.Rows(intRowindex)("exemptperiodid") = ""
                mdtTran.Rows(intRowindex)("amount") = CDec(dgBenefitPlan.Rows(intRowindex).Cells(objdgcolhDefaultAmount.Index).Value)
                mdtTran.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetPlan()
        Try
            If cboBenefitGroup.Items.Count > 0 Then cboBenefitGroup.SelectedValue = 0
            cboPeriod.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetPlan", mstrModuleName)
        End Try
    End Sub

    Private Function GetEmployeeIds() As String
        Dim strIds As String = String.Empty
        Try
            For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
                If strIds.Length <= 0 Then
                    strIds = CStr(lvEmployeeList.CheckedItems(i).Tag)
                Else
                    strIds &= "," & CStr(lvEmployeeList.CheckedItems(i).Tag)
                End If
            Next

            Return strIds

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeIds", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombos As DataSet
        Try

            'Pinkal (08-Jan-2016) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.

            'dsCombos = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                              User._Object._Userunkid, _
            '                              FinancialYear._Object._YearUnkid, _
            '                              Company._Object._Companyunkid, _
            '                              mdtPeriodStartDate, _
            '                              mdtPeriodEndDate, _
            '                              ConfigParameter._Object._UserAccessModeSetting, _
            '                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpList")



            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'dsCombos = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                            User._Object._Userunkid, _
            '                            FinancialYear._Object._YearUnkid, _
            '                            Company._Object._Companyunkid, _
            '                            mdtPeriodStartDate, _
            '                            mdtPeriodEndDate, _
            '                            ConfigParameter._Object._UserAccessModeSetting, _
            '                         True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpList", False, mintEmployeeID)

            If mstrCheckedEmpIds.Trim.Length > 0 Then
                mstrCheckedEmpIds = " hremployee_master.employeeunkid IN (" & mstrCheckedEmpIds & ") "
            End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                            , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                            , mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpList", True, mintEmployeeID, , , , , , , , , , , , , , mstrCheckedEmpIds)

            'Dim drRow As DataRow = dsCombos.Tables(0).NewRow
            'drRow("employeeunkid") = 0
            'drRow("name") = Language.getMessage("clsEmployee_Master", 10, "  Select")
            'dsCombos.Tables(0).Rows.InsertAt(drRow, 0)

            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("EmpList")
            '    .SelectedValue = 0
            'End With

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("EmpList")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboEmployee) 'Hemant (22 June 2019)
            'Pinkal (24-Feb-2017) -- End

            'Pinkal (08-Jan-2016) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._IsArutiDemo = False Then
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                    cboPeriod.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    chkCopyPreviousEDSlab.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    chkOverwrite.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    chkOverwritePrevEDSlabHeads.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    btnSetPeriod.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    radApplytoAll.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    radApplytoChecked.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    objdgcolhExempt.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    dgcolhTranshead.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    dgcolhAmount.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                    dgcolhPeriod.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 16, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeBenefitCoverage_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmEmployeeBenefitCoverage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.A Then
            Call btnAssign.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeBenefitCoverage_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeBenefitCoverage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefitCoverage = New clsBenefitCoverage_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()
            Call SetVisibility()


            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            'Call CreateTable()
            dgBenefitPlan.Columns(dgcolhAmount.Index).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgBenefitPlan.Columns(objdgcolhExempt.Index).DefaultCellStyle.SelectionBackColor = Color.White
            'Pinkal (21-Dec-2015) -- End


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            'If mblnOpenFromImportApplicants Then
            '    rdShowOnlyNewHiredEmp.Checked = True
            '    rdShowOnlyNewHiredEmp.Enabled = False
            '    rdShowOnlyReinstatedEmp.Enabled = False
            '    objbtnSearch_Click(New Object(), New EventArgs())
            'End If
            'Pinkal (24-Feb-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBenefitCoverage_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBenefitCoverage_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBenefitCoverage_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            cboEmployee.SelectedValue = 0
            'Pinkal (21-Dec-2015) -- End

            cboDepartment.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboGrade.SelectedValue = 0
            cboBranch.SelectedValue = 0
            cboCostCenter.SelectedValue = 0
            cboUnit.SelectedValue = 0


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            cboDepartmentGrp.SelectedValue = 0
            cboSectionGroup.SelectedValue = 0
            cboUnitGroup.SelectedValue = 0
            cboTeams.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            cboGradeGroup.SelectedValue = 0
            cboGradeLevel.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            rdShowOnlyNewHiredEmp.Checked = False
            rdShowOnlyReinstatedEmp.Checked = False

            Call FillEmployee()

            If gbEmployeeList.Tag Is Nothing Then
                gbEmployeeList.Tag = gbEmployeeList.Text
            End If
            gbEmployeeList.Text = gbEmployeeList.Tag.ToString() & " [" & lvEmployeeList.Items.Count & " Record(s) Found.]"
            'Pinkal (24-Feb-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillEmployee()

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            If gbEmployeeList.Tag Is Nothing Then
                gbEmployeeList.Tag = gbEmployeeList.Text
            End If
            gbEmployeeList.Text = gbEmployeeList.Tag.ToString() & " [" & lvEmployeeList.Items.Count & " Record(s) Found.]"
            'Pinkal (24-Feb-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSetPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetPeriod.Click
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Effective Period. Effective Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                If radApplytoAll.Checked Then

                    Dim drRow() As DataRow = mdtTran.Select("ischeck = True AND periodunkid > 0")
                    If drRow.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Some of the benefit plan(s) are already set with different periods.Are you sure you want to change those set period(s) with new ones ?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            GoTo AllCheck
                        Else
                            Dim dr() As DataRow = mdtTran.Select("ischeck = False AND periodunkid <= 0")
                            If dr.Length > 0 Then
                                For i As Integer = 0 To dr.Length - 1
                                    dr(i)("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                                    dr(i)("benefitgroupid") = CInt(cboBenefitGroup.SelectedValue)
                                    dr(i)("periodunkid") = CInt(cboPeriod.SelectedValue)
                                    dr(i)("periodname") = cboPeriod.Text
                                    dr(i)("actionid") = IIf(chkOverwrite.Checked = True, 1, 0)
                                    dr(i)("departmentid") = CInt(cboDepartment.SelectedValue)
                                    dr(i)("jobid") = CInt(cboJob.SelectedValue)
                                    dr(i)("classid") = CInt(cboClass.SelectedValue)
                                    dr(i)("gradeid") = CInt(cboGrade.SelectedValue)
                                    dr(i)("sectionid") = CInt(cboSections.SelectedValue)
                                Next

                            End If

                        End If

                    Else
AllCheck:
                        For Each dr As DataRow In mdtTran.Rows
                            dr("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                            dr("benefitgroupid") = CInt(cboBenefitGroup.SelectedValue)
                            dr("periodunkid") = CInt(cboPeriod.SelectedValue)
                            dr("periodname") = cboPeriod.Text
                            dr("actionid") = IIf(chkOverwrite.Checked = True, 1, 0)
                            dr("departmentid") = CInt(cboDepartment.SelectedValue)
                            dr("jobid") = CInt(cboJob.SelectedValue)
                            dr("classid") = CInt(cboClass.SelectedValue)
                            dr("gradeid") = CInt(cboGrade.SelectedValue)
                            dr("sectionid") = CInt(cboSections.SelectedValue)
                        Next

                    End If

                ElseIf radApplytoChecked.Checked Then

                    Dim dr() As DataRow = mdtTran.Select("ischeck = True AND periodunkid > 0")
                    If dr.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Some of the benefit plan(s) are already set with different periods.Are you sure you want to change those set period(s) with new ones ?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            For i As Integer = 0 To dr.Length - 1
                                dr(i)("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dr(i)("periodname") = cboPeriod.Text
                            Next
                        End If
                        GoTo ForSelected
                    Else
ForSelected:
                        Dim drRow() As DataRow = mdtTran.Select("ischeck = True AND periodunkid <= 0")
                        If drRow.Length > 0 Then
                            For i As Integer = 0 To drRow.Length - 1
                                drRow(i)("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                                drRow(i)("benefitgroupid") = CInt(cboBenefitGroup.SelectedValue)
                                drRow(i)("periodunkid") = CInt(cboPeriod.SelectedValue)
                                drRow(i)("periodname") = cboPeriod.Text
                                drRow(i)("actionid") = IIf(chkOverwrite.Checked = True, 1, 0)
                                drRow(i)("departmentid") = CInt(cboDepartment.SelectedValue)
                                drRow(i)("jobid") = CInt(cboJob.SelectedValue)
                                drRow(i)("classid") = CInt(cboClass.SelectedValue)
                                drRow(i)("gradeid") = CInt(cboGrade.SelectedValue)
                                drRow(i)("sectionid") = CInt(cboSections.SelectedValue)
                            Next
                            drRow = Nothing
                        Else
                            drRow = mdtTran.Select("ischeck = True ")
                            If drRow.Length <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please check atleast one benefit plan."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            drRow = Nothing
                        End If
                    End If

                End If
                mdtTran.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSetPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            Dim objTnA As New clsTnALeaveTran 'Hemant (22 June 2019)
            Dim strEmployeeIds As String = String.Empty
            strEmployeeIds = GetEmployeeIds()
            If strEmployeeIds.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one employee to assign benefit."), enMsgBoxStyle.Information)
                lvEmployeeList.Select()
                Exit Sub
            End If


            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            'If lvBenefitPlan.Items.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please add atleast one benefit plan."), enMsgBoxStyle.Information)
            '    lvBenefitPlan.Select()
            '    Exit Sub
            'End If
            Dim drRow() As DataRow = mdtTran.Select("ischeck = True")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please check atleast one benefit plan."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            drRow = Nothing

            drRow = mdtTran.Select("periodunkid <= 0")
            If drRow.Length = dgBenefitPlan.RowCount Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please set Effective Period for atleast one benefit plan."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Hemant (22 June 2019) -- Start
            'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
            For Each dtRow As DataRow In mdtTran.Select("IsCheck = 1 ")
                If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmployeeIds, mdtPeriodEndDate) = True Then
                    Cursor.Current = Cursors.Default
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employees to assign Benefit.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 15, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    Exit Try
                End If
            Next
            'Hemant (22 June 2019) -- End

            mdtTran = New DataView(mdtTran, "ischeck = True", "", DataViewRowState.CurrentRows).ToTable

            'Pinkal (21-Dec-2015) -- End

            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            Me.Cursor = Cursors.WaitCursor
            'Pinkal (21-Dec-2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBenefitCoverage.AssignGroupBenefit(strEmployeeIds, mdtTran, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked) Then
            'Sohail (19 Mar 2016) -- Start
            'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
            'If objBenefitCoverage.AssignGroupBenefit(strEmployeeIds, mdtTran, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, _
            '                                         FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
            '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                         ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime) Then


            'Pinkal (08-Feb-2018) -- Start
            'Enhancement - Internal Issue given by Gajanan.


            'If objBenefitCoverage.AssignGroupBenefit(strEmployeeIds, mdtTran, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, _
            '                                         FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
            '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                         ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, chkOverwrite.Checked) Then

            If objBenefitCoverage.AssignGroupBenefit(strEmployeeIds, mdtTran, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, _
                                                     FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                  mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, chkOverwrite.Checked) Then

                'Pinkal (08-Feb-2018) -- End

                'Sohail (19 Mar 2016) -- End
                'S.SANDEEP [04 JUN 2015] -- END



                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Benefit Plan successfully assigned to selected employee(s)."), enMsgBoxStyle.Information)
                Call btnClose_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            Me.Cursor = Cursors.Default
            'Pinkal (21-Dec-2015) -- End
        End Try
    End Sub


#End Region

#Region " ListView Events "

    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        If lvEmployeeList.CheckedItems.Count <= 0 Then
            RemoveHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
            objChkAllEmployee.CheckState = CheckState.Unchecked
            AddHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
        ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
            RemoveHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
            objChkAllEmployee.CheckState = CheckState.Indeterminate
            AddHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
        ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
            RemoveHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
            objChkAllEmployee.CheckState = CheckState.Checked
            AddHandler objChkAllEmployee.CheckedChanged, AddressOf objChkAllEmployee_CheckedChanged
        End If
    End Sub

#End Region

#Region "Combobox Event"

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.


    'Private Sub cboClass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClass.SelectedIndexChanged
    '    Try
    '        'If CInt(cboClass.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
    '    Try
    '        'If CInt(cboSections.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
    '    Try
    '        'If CInt(cboJob.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
    '    Try
    '        'If CInt(cboGrade.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
    '    Try
    '        'If CInt(cboDepartment.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
    '    Try
    '        'If CInt(cboBranch.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboCostCenter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCostCenter.SelectedIndexChanged
    '    Try
    '        'If CInt(cboCostCenter.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCostCenter_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
    '    Try
    '        'If CInt(cboUnit.SelectedValue) > 0 Then
    '        Call FillBenefitGroup()
    '        'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboUnit_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Hemant (22 June 2019) -- End

    Private Sub cboBenefitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBenefitGroup.SelectedIndexChanged
        Dim dsList As DataSet = Nothing
        Try

            Dim objBPlanTran As New clsBenefit_allocation_tran
            Dim objBenefit As New clsbenefitplan_master
            Dim mstrBenefitPlan As String = objBenefit.GetBeniftPlanFromBenefitGrp(CInt(cboBenefitGroup.SelectedValue))
            If mstrBenefitPlan.Trim = "" Then mstrBenefitPlan = "-1"

            Dim mdtBenefit As DataTable = CType(cboBenefitGroup.DataSource, DataTable)


            'Pinkal (19-Sep-2017) -- Start
            'Enhancement - Solved Group Benefit Issue.

            'mdtTran = objBPlanTran.GetList("Plan", mintCurrBenefitAllocationID, mstrBenefitPlan).Tables(0)
            If mdtBenefit IsNot Nothing AndAlso mdtBenefit.Rows.Count > 0 Then
                Dim drRow As DataRowView = CType(cboBenefitGroup.SelectedItem, DataRowView)
                If drRow IsNot Nothing Then
                    mdtTran = objBPlanTran.GetList("Plan", CInt(drRow("benefitallocationunkid")), mstrBenefitPlan).Tables(0)
                End If
            End If

            'Pinkal (19-Sep-2017) -- End


            FillBenefitPlanList()
            mstrExemptPeriodIds = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBenefitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Me.Cursor = Cursors.WaitCursor
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            End If
            Call FillEmployeeCombo()

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            'Call FillEmployee()
            'Pinkal (24-Feb-2017) -- End

            'Pinkal (08-Feb-2018) -- Start
            'Enhancement - Internal Issue given by Gajanan.
            lvEmployeeList.Items.Clear()
            'Pinkal (08-Feb-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
            objPeriod = Nothing
        End Try
    End Sub

    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    'Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged, cboDepartmentGrp.SelectedIndexChanged _
    '                                                                                                                                                                                           , cboSectionGroup.SelectedIndexChanged, cboUnitGroup.SelectedIndexChanged _
    '                                                                                                                                                                                           , cboTeams.SelectedIndexChanged, cboClassGroup.SelectedIndexChanged _
    '                                                                                                                                                                                           , cboGradeLevel.SelectedIndexChanged, cboJobGroup.SelectedIndexChanged

    '    'Pinkal (08-Feb-2018) -- 'Enhancement - Internal Issue given by Gajanan.[cboClassGroup.SelectedIndexChanged]

    '    Try
    '        FillBenefitGroup()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Hemant (22 June 2019) -- End

    'Pinkal (24-Feb-2017) -- End

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub cboCostcenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostCenter.KeyPress, _
                                                                                                   cboBranch.KeyPress, _
                                                                                                   cboDepartmentGrp.KeyPress, _
                                                                                                   cboDepartment.KeyPress, _
                                                                                                   cboSectionGroup.KeyPress, _
                                                                                                   cboSections.KeyPress, _
                                                                                                   cboUnitGroup.KeyPress, _
                                                                                                   cboUnit.KeyPress, _
                                                                                                   cboTeams.KeyPress, _
                                                                                                   cboGradeGroup.KeyPress, _
                                                                                                   cboGrade.KeyPress, _
                                                                                                   cboGradeLevel.KeyPress, _
                                                                                                   cboJobGroup.KeyPress, _
                                                                                                   cboJob.KeyPress, _
                                                                                                   cboClassGroup.KeyPress, _
                                                                                                   cboClass.KeyPress, _
                                                                                                   cboEmployee.KeyPress

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboCostCenter.Name Then
                        .CodeMember = "costcentercode"
                    ElseIf cbo.Name = cboBranch.Name OrElse cbo.Name = cboDepartmentGrp.Name OrElse cbo.Name = cboDepartment.Name OrElse _
                           cbo.Name = cboSectionGroup.Name OrElse cbo.Name = cboSections.Name OrElse cbo.Name = cboUnitGroup.Name OrElse _
                           cbo.Name = cboUnit.Name OrElse cbo.Name = cboTeams.Name OrElse cbo.Name = cboGradeGroup.Name OrElse _
                           cbo.Name = cboGrade.Name OrElse cbo.Name = cboGradeLevel.Name OrElse cbo.Name = cboJobGroup.Name OrElse _
                           cbo.Name = cboJob.Name Then
                    ElseIf cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.SelectedIndexChanged, _
                                                                                                   cboBranch.SelectedIndexChanged, _
                                                                                                   cboDepartmentGrp.SelectedIndexChanged, _
                                                                                                   cboDepartment.SelectedIndexChanged, _
                                                                                                   cboSectionGroup.SelectedIndexChanged, _
                                                                                                   cboSections.SelectedIndexChanged, _
                                                                                                   cboUnitGroup.SelectedIndexChanged, _
                                                                                                   cboUnit.SelectedIndexChanged, _
                                                                                                   cboTeams.SelectedIndexChanged, _
                                                                                                   cboGradeGroup.SelectedIndexChanged, _
                                                                                                   cboGrade.SelectedIndexChanged, _
                                                                                                   cboGradeLevel.SelectedIndexChanged, _
                                                                                                   cboJobGroup.SelectedIndexChanged, _
                                                                                                   cboJob.SelectedIndexChanged, _
                                                                                                   cboClassGroup.SelectedIndexChanged, _
                                                                                                   cboClass.SelectedIndexChanged, _
                                                                                                   cboEmployee.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try
            FillBenefitGroup()
            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.GotFocus, _
                                                                                                   cboBranch.GotFocus, _
                                                                                                   cboDepartmentGrp.GotFocus, _
                                                                                                   cboDepartment.GotFocus, _
                                                                                                   cboSectionGroup.GotFocus, _
                                                                                                   cboSections.GotFocus, _
                                                                                                   cboUnitGroup.GotFocus, _
                                                                                                   cboUnit.GotFocus, _
                                                                                                   cboTeams.GotFocus, _
                                                                                                   cboGradeGroup.GotFocus, _
                                                                                                   cboGrade.GotFocus, _
                                                                                                   cboGradeLevel.GotFocus, _
                                                                                                   cboJobGroup.GotFocus, _
                                                                                                   cboJob.GotFocus, _
                                                                                                   cboClassGroup.GotFocus, _
                                                                                                   cboClass.GotFocus, _
                                                                                                   cboEmployee.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostCenter.Leave, _
                                                                                                   cboBranch.Leave, _
                                                                                                   cboDepartmentGrp.Leave, _
                                                                                                   cboDepartment.Leave, _
                                                                                                   cboSectionGroup.Leave, _
                                                                                                   cboSections.Leave, _
                                                                                                   cboUnitGroup.Leave, _
                                                                                                   cboUnit.Leave, _
                                                                                                   cboTeams.Leave, _
                                                                                                   cboGradeGroup.Leave, _
                                                                                                   cboGrade.Leave, _
                                                                                                   cboGradeLevel.Leave, _
                                                                                                   cboJobGroup.Leave, _
                                                                                                   cboJob.Leave, _
                                                                                                   cboClassGroup.Leave, _
                                                                                                   cboClass.Leave, _
                                                                                                   cboEmployee.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End


#End Region

#Region " Checkbox's Events "

    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (21-Dec-2015) -- Start
    'Enhancement - Working on Employee Benefit changes given By Rutta.

    Private Sub objChkAllEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAllEmployee.CheckedChanged
        Try

            'If objChkAllEmployee.Checked = True Then
            '    For i As Integer = 0 To lvEmployeeList.Items.Count - 1
            '        RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        lvEmployeeList.Items(i).Checked = True
            '        AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '    Next
            'Else
            '    For i As Integer = 0 To lvEmployeeList.Items.Count - 1
            '        RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        lvEmployeeList.Items(i).Checked = False
            '        AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '    Next
            'End If

            For i As Integer = 0 To lvEmployeeList.Items.Count - 1
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                lvEmployeeList.Items(i).Checked = objChkAllEmployee.Checked
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAllEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                RemoveHandler dgBenefitPlan.CellContentClick, AddressOf dgBenefitPlan_CellContentClick
                RemoveHandler dgBenefitPlan.CellContentDoubleClick, AddressOf dgBenefitPlan_CellContentClick
                For Each dr As DataRow In mdtTran.Rows
                    dr("ischeck") = objchkSelectAll.Checked
                    dr.EndEdit()
                    If objchkSelectAll.Checked = False Then
                        SetGridValue(mdtTran.Rows.IndexOf(dr))
                    End If
                Next
                mdtTran.AcceptChanges()
                AddHandler dgBenefitPlan.CellContentClick, AddressOf dgBenefitPlan_CellContentClick
                AddHandler dgBenefitPlan.CellContentDoubleClick, AddressOf dgBenefitPlan_CellContentClick
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (21-Dec-2015) -- End


    'Pinkal (13-Mar-2018) -- Start
    'Defect - (RefID 26) In Assign Benift Group OverWrite Benefit Heads If Exist,Copy Previous ED Slab,Overwrite Privious Ed Slab Heads Not Working Together.
    Private Sub chkOverwritePrevEDSlabHeads_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOverwritePrevEDSlabHeads.CheckedChanged, chkOverwrite.CheckedChanged
        Try
            RemoveHandler chkOverwritePrevEDSlabHeads.CheckedChanged, AddressOf chkOverwritePrevEDSlabHeads_CheckedChanged
            RemoveHandler chkOverwrite.CheckedChanged, AddressOf chkOverwritePrevEDSlabHeads_CheckedChanged
            If CType(sender, CheckBox).Name.ToUpper() = chkOverwritePrevEDSlabHeads.Name.ToUpper() Then
                chkOverwrite.Checked = False
            ElseIf CType(sender, CheckBox).Name.ToUpper() = chkOverwrite.Name.ToUpper() Then
                chkOverwritePrevEDSlabHeads.Checked = False
            End If
            AddHandler chkOverwritePrevEDSlabHeads.CheckedChanged, AddressOf chkOverwritePrevEDSlabHeads_CheckedChanged
            AddHandler chkOverwrite.CheckedChanged, AddressOf chkOverwritePrevEDSlabHeads_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOverwritePrevEDSlabHeads_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (13-Mar-2018) -- End


#End Region

#Region "DataGrid Event"

    Private Sub dgBenefitPlan_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgBenefitPlan.CellContentClick, dgBenefitPlan.CellContentDoubleClick
        Try

            If e.RowIndex < 0 Then Exit Sub

            If Me.dgBenefitPlan.IsCurrentCellDirty Then
                Me.dgBenefitPlan.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objdgcolhIscheck.Index Then

                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

                If mdtTran Is Nothing Then Exit Sub

                If CBool(dgBenefitPlan.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    SetGridValue(e.RowIndex)
                End If
                mdtTran.AcceptChanges()
                SetCheckBoxValue()

                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            ElseIf e.ColumnIndex = objdgcolhExempt.Index Then

                If CBool(dgBenefitPlan.Rows(e.RowIndex).Cells(objdgcolhIscheck.Index).Value) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please check selected Benefit plan to exempt."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If lvEmployeeList.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please check atleast one employee to assign Group Benefit."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim strEmpList As String = GetEmployeeIds()

                Dim frm As New frmEmployee_Exemption_AddEdit
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = True Or ConfigParameter._Object._IsArutiDemo = True Then
                    Dim objTnA As New clsTnALeaveTran
                    If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpList, mdtPeriodEndDate) = True Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employees to assign Benefit."), enMsgBoxStyle.Information)
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employees to assign Benefit."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employees to assign Benefit.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 15, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Do you want to exempt this Transaction Head for some pay periods?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        frm.displayDialog(-1, enAction.ADD_ONE, , , , CInt(dgBenefitPlan.Rows(e.RowIndex).Cells(objdgcolhTranheadunkid.Index).Value), True, strEmpList)
                    End If
                    objTnA = Nothing
                End If
                'SET EXEMPTIONUNKID
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = True Or ConfigParameter._Object._IsArutiDemo = True Then
                    mdtTran.Rows(e.RowIndex)("exemptperiodid") = frm._ExemptPeriodIds
                Else
                    mdtTran.Rows(e.RowIndex)("exemptperiodid") = ""
                End If

                mdtTran.AcceptChanges()
                frm = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgBenefitPlan_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgBenefitPlan_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgBenefitPlan.DataBindingComplete
        Try
            If dgBenefitPlan.DataSource IsNot Nothing AndAlso dgBenefitPlan.RowCount > 0 Then
                For i As Integer = 0 To dgBenefitPlan.RowCount - 1
                    Dim lnkExempt As DataGridViewLinkCell = CType(dgBenefitPlan.Rows(i).Cells(objdgcolhExempt.Index), DataGridViewLinkCell)
                    If lnkExempt.Value Is Nothing OrElse lnkExempt.Value.ToString() = "" Then
                        lnkExempt.LinkBehavior = LinkBehavior.NeverUnderline
                        lnkExempt.Value = Language.getMessage(mstrModuleName, 10, "Exempt")
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgBenefitPlan_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgBenefitPlan_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgBenefitPlan.DataError

    End Sub

#End Region


    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
#Region "Menu Events"

    'Private Sub mnuViewDeleteEmpBenefits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewDeleteEmpBenefits.Click
    '    Try

    '        If dsEmployee Is Nothing OrElse dsEmployee.Tables.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "There is no employee(s) to View/Delete Employee benefits.Please search employee(s) again."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Rows.Count > 0 Then
    '            Dim objFrm As New frmBenefitCoverage_List
    '            objFrm._IsOpenForDeletion = True

    '            Dim drRow As DataRow = dsEmployee.Tables(0).NewRow
    '            drRow("employeeunkid") = 0
    '            drRow("employeecode") = Language.getMessage(mstrModuleName, 14, "Select")
    '            drRow("employeename") = Language.getMessage(mstrModuleName, 14, "Select")
    '            dsEmployee.Tables(0).Rows.InsertAt(drRow, 0)

    '            objFrm._dtEmployee = dsEmployee.Tables(0).Copy
    '            objFrm.ShowDialog()

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuViewDeleteEmpBenefits_Click", mstrModuleName)
    '    End Try
    'End Sub

#End Region

    'Pinkal (24-Feb-2017) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
         
            Call SetLanguage()

            Me.gbGroupAssign.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbGroupAssign.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBenefitPlan.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBenefitPlan.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnSetPeriod.GradientBackColor = GUI._ButttonBackColor
            Me.btnSetPeriod.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbGroupAssign.Text = Language._Object.getCaption(Me.gbGroupAssign.Name, Me.gbGroupAssign.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblBenefitType.Text = Language._Object.getCaption(Me.lblBenefitType.Name, Me.lblBenefitType.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.gbBenefitPlan.Text = Language._Object.getCaption(Me.gbBenefitPlan.Name, Me.gbBenefitPlan.Text)
            Me.lblValueBasis.Text = Language._Object.getCaption(Me.lblValueBasis.Name, Me.lblValueBasis.Text)
            Me.lblBenefitsInPercent.Text = Language._Object.getCaption(Me.lblBenefitsInPercent.Name, Me.lblBenefitsInPercent.Text)
            Me.lblBenefitInAmount.Text = Language._Object.getCaption(Me.lblBenefitInAmount.Name, Me.lblBenefitInAmount.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.colhEmployeeCode.Text = Language._Object.getCaption(CStr(Me.colhEmployeeCode.Tag), Me.colhEmployeeCode.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.chkOverwrite.Text = Language._Object.getCaption(Me.chkOverwrite.Name, Me.chkOverwrite.Text)
            Me.radDoNotOverwrite.Text = Language._Object.getCaption(Me.radDoNotOverwrite.Name, Me.radDoNotOverwrite.Text)
            Me.radOverwrite.Text = Language._Object.getCaption(Me.radOverwrite.Name, Me.radOverwrite.Text)
            Me.radAdd.Text = Language._Object.getCaption(Me.radAdd.Name, Me.radAdd.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.btnSetPeriod.Text = Language._Object.getCaption(Me.btnSetPeriod.Name, Me.btnSetPeriod.Text)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
            Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.dgcolhBenefitPlan.HeaderText = Language._Object.getCaption(Me.dgcolhBenefitPlan.Name, Me.dgcolhBenefitPlan.HeaderText)
            Me.dgcolhTranshead.HeaderText = Language._Object.getCaption(Me.dgcolhTranshead.Name, Me.dgcolhTranshead.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuViewDeleteEmpBenefits.Text = Language._Object.getCaption(Me.mnuViewDeleteEmpBenefits.Name, Me.mnuViewDeleteEmpBenefits.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.rdShowOnlyReinstatedEmp.Text = Language._Object.getCaption(Me.rdShowOnlyReinstatedEmp.Name, Me.rdShowOnlyReinstatedEmp.Text)
            Me.rdShowOnlyNewHiredEmp.Text = Language._Object.getCaption(Me.rdShowOnlyNewHiredEmp.Name, Me.rdShowOnlyNewHiredEmp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Do you want to exempt this Transaction Head for some pay periods?")
            Language.setMessage(mstrModuleName, 4, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employees to assign Benefit.")
            Language.setMessage(mstrModuleName, 5, "Benefit Plan successfully assigned to selected employee(s).")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one employee to assign benefit.")
            Language.setMessage(mstrModuleName, 8, "Please select Effective Period. Effective Period is mandatory information.")
            Language.setMessage(mstrModuleName, 9, "Please check atleast one benefit plan.")
            Language.setMessage(mstrModuleName, 10, "Exempt")
            Language.setMessage(mstrModuleName, 11, "Please check selected Benefit plan to exempt.")
            Language.setMessage(mstrModuleName, 12, "Please check atleast one employee to assign Group Benefit.")
            Language.setMessage(mstrModuleName, 13, "Please set Effective Period for atleast one benefit plan.")
            Language.setMessage(mstrModuleName, 14, "Some of the benefit plan(s) are already set with different periods.Are you sure you want to change those set period(s) with new ones ?")
			Language.setMessage(mstrModuleName, 15, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 16, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class