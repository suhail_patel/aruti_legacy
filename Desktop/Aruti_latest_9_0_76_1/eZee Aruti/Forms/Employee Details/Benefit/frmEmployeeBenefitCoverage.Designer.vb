﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeBenefitCoverage
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeBenefitCoverage))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlBenefitPlan = New System.Windows.Forms.Panel
        Me.gbBenefitPlan = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.pnlPeriodList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvAssignED = New System.Windows.Forms.DataGridView
        Me.colhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhEDUnkId = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhPeriodUnkID = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhPeriodName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMedicalRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnAssignedED = New eZee.Common.eZeeLine
        Me.txtTranHeadDesc = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddTranHead = New eZee.Common.eZeeGradientButton
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.lblFormula = New System.Windows.Forms.Label
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.objbtnAddBenefitPlan = New eZee.Common.eZeeGradientButton
        Me.cboBenefitPlan = New System.Windows.Forms.ComboBox
        Me.lblBenefitPlan = New System.Windows.Forms.Label
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.lnBenefitInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.lblBenefitType = New System.Windows.Forms.Label
        Me.lblValueBasis = New System.Windows.Forms.Label
        Me.lblBenefitInAmount = New System.Windows.Forms.Label
        Me.txtBenefitAmount = New eZee.TextBox.NumericTextBox
        Me.lblBenefitsInPercent = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.dtpAssignDate = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.cboValueBasis = New System.Windows.Forms.ComboBox
        Me.txtDBBenefitInPercent = New eZee.TextBox.NumericTextBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlBenefitPlan.SuspendLayout()
        Me.gbBenefitPlan.SuspendLayout()
        Me.pnlPeriodList.SuspendLayout()
        CType(Me.dgvAssignED, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBenefitPlan
        '
        Me.pnlBenefitPlan.Controls.Add(Me.gbBenefitPlan)
        Me.pnlBenefitPlan.Controls.Add(Me.lblValueBasis)
        Me.pnlBenefitPlan.Controls.Add(Me.lblBenefitInAmount)
        Me.pnlBenefitPlan.Controls.Add(Me.txtBenefitAmount)
        Me.pnlBenefitPlan.Controls.Add(Me.lblBenefitsInPercent)
        Me.pnlBenefitPlan.Controls.Add(Me.DateTimePicker2)
        Me.pnlBenefitPlan.Controls.Add(Me.dtpAssignDate)
        Me.pnlBenefitPlan.Controls.Add(Me.DateTimePicker1)
        Me.pnlBenefitPlan.Controls.Add(Me.lblStartDate)
        Me.pnlBenefitPlan.Controls.Add(Me.dtpEndDate)
        Me.pnlBenefitPlan.Controls.Add(Me.lblEndDate)
        Me.pnlBenefitPlan.Controls.Add(Me.EZeeStraightLine2)
        Me.pnlBenefitPlan.Controls.Add(Me.cboValueBasis)
        Me.pnlBenefitPlan.Controls.Add(Me.txtDBBenefitInPercent)
        Me.pnlBenefitPlan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBenefitPlan.Location = New System.Drawing.Point(0, 0)
        Me.pnlBenefitPlan.Name = "pnlBenefitPlan"
        Me.pnlBenefitPlan.Size = New System.Drawing.Size(465, 449)
        Me.pnlBenefitPlan.TabIndex = 0
        '
        'gbBenefitPlan
        '
        Me.gbBenefitPlan.BorderColor = System.Drawing.Color.Black
        Me.gbBenefitPlan.Checked = False
        Me.gbBenefitPlan.CollapseAllExceptThis = False
        Me.gbBenefitPlan.CollapsedHoverImage = Nothing
        Me.gbBenefitPlan.CollapsedNormalImage = Nothing
        Me.gbBenefitPlan.CollapsedPressedImage = Nothing
        Me.gbBenefitPlan.CollapseOnLoad = False
        Me.gbBenefitPlan.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbBenefitPlan.Controls.Add(Me.pnlPeriodList)
        Me.gbBenefitPlan.Controls.Add(Me.lnAssignedED)
        Me.gbBenefitPlan.Controls.Add(Me.txtTranHeadDesc)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnAddTranHead)
        Me.gbBenefitPlan.Controls.Add(Me.lblAmount)
        Me.gbBenefitPlan.Controls.Add(Me.txtAmount)
        Me.gbBenefitPlan.Controls.Add(Me.lblFormula)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbBenefitPlan.Controls.Add(Me.cboTransactionHead)
        Me.gbBenefitPlan.Controls.Add(Me.lblTransactionHead)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnAddBenefitPlan)
        Me.gbBenefitPlan.Controls.Add(Me.cboBenefitPlan)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitPlan)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnAddGroup)
        Me.gbBenefitPlan.Controls.Add(Me.lnBenefitInfo)
        Me.gbBenefitPlan.Controls.Add(Me.lnEmployeeName)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbBenefitPlan.Controls.Add(Me.cboEmployee)
        Me.gbBenefitPlan.Controls.Add(Me.lblEmployee)
        Me.gbBenefitPlan.Controls.Add(Me.cboBenefitGroup)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitType)
        Me.gbBenefitPlan.ExpandedHoverImage = Nothing
        Me.gbBenefitPlan.ExpandedNormalImage = Nothing
        Me.gbBenefitPlan.ExpandedPressedImage = Nothing
        Me.gbBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBenefitPlan.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBenefitPlan.HeaderHeight = 25
        Me.gbBenefitPlan.HeaderMessage = ""
        Me.gbBenefitPlan.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBenefitPlan.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBenefitPlan.HeightOnCollapse = 0
        Me.gbBenefitPlan.LeftTextSpace = 0
        Me.gbBenefitPlan.Location = New System.Drawing.Point(12, 12)
        Me.gbBenefitPlan.Name = "gbBenefitPlan"
        Me.gbBenefitPlan.OpenHeight = 180
        Me.gbBenefitPlan.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBenefitPlan.ShowBorder = True
        Me.gbBenefitPlan.ShowCheckBox = False
        Me.gbBenefitPlan.ShowCollapseButton = False
        Me.gbBenefitPlan.ShowDefaultBorderColor = True
        Me.gbBenefitPlan.ShowDownButton = False
        Me.gbBenefitPlan.ShowHeader = True
        Me.gbBenefitPlan.Size = New System.Drawing.Size(438, 377)
        Me.gbBenefitPlan.TabIndex = 0
        Me.gbBenefitPlan.Temp = 0
        Me.gbBenefitPlan.Text = "Benefit Holding"
        Me.gbBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(241, 183)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(174, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 36
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        Me.chkCopyPreviousEDSlab.Visible = False
        '
        'pnlPeriodList
        '
        Me.pnlPeriodList.Controls.Add(Me.objchkSelectAll)
        Me.pnlPeriodList.Controls.Add(Me.dgvAssignED)
        Me.pnlPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlPeriodList.Location = New System.Drawing.Point(11, 206)
        Me.pnlPeriodList.Name = "pnlPeriodList"
        Me.pnlPeriodList.Size = New System.Drawing.Size(404, 163)
        Me.pnlPeriodList.TabIndex = 34
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 3)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 17
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvAssignED
        '
        Me.dgvAssignED.AllowUserToAddRows = False
        Me.dgvAssignED.AllowUserToDeleteRows = False
        Me.dgvAssignED.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssignED.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhCheck, Me.colhEDUnkId, Me.colhPeriodUnkID, Me.colhPeriodName, Me.colhAmount, Me.colhMedicalRefNo})
        Me.dgvAssignED.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAssignED.Location = New System.Drawing.Point(0, 0)
        Me.dgvAssignED.Name = "dgvAssignED"
        Me.dgvAssignED.RowHeadersVisible = False
        Me.dgvAssignED.Size = New System.Drawing.Size(404, 163)
        Me.dgvAssignED.TabIndex = 221
        '
        'colhCheck
        '
        Me.colhCheck.HeaderText = ""
        Me.colhCheck.Name = "colhCheck"
        Me.colhCheck.Width = 25
        '
        'colhEDUnkId
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.colhEDUnkId.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhEDUnkId.HeaderText = "EDUnkID"
        Me.colhEDUnkId.Name = "colhEDUnkId"
        Me.colhEDUnkId.ReadOnly = True
        Me.colhEDUnkId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhEDUnkId.Visible = False
        '
        'colhPeriodUnkID
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F0"
        Me.colhPeriodUnkID.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhPeriodUnkID.HeaderText = "PeriodID"
        Me.colhPeriodUnkID.Name = "colhPeriodUnkID"
        Me.colhPeriodUnkID.ReadOnly = True
        Me.colhPeriodUnkID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhPeriodUnkID.Visible = False
        '
        'colhPeriodName
        '
        Me.colhPeriodName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhPeriodName.HeaderText = "Period"
        Me.colhPeriodName.Name = "colhPeriodName"
        Me.colhPeriodName.ReadOnly = True
        Me.colhPeriodName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhPeriodName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'colhAmount
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhAmount.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhAmount.HeaderText = "Amount"
        Me.colhAmount.Name = "colhAmount"
        Me.colhAmount.ReadOnly = True
        Me.colhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhAmount.Width = 120
        '
        'colhMedicalRefNo
        '
        Me.colhMedicalRefNo.HeaderText = "Reference. No"
        Me.colhMedicalRefNo.Name = "colhMedicalRefNo"
        Me.colhMedicalRefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhMedicalRefNo.Width = 120
        '
        'lnAssignedED
        '
        Me.lnAssignedED.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnAssignedED.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnAssignedED.Location = New System.Drawing.Point(8, 182)
        Me.lnAssignedED.Name = "lnAssignedED"
        Me.lnAssignedED.Size = New System.Drawing.Size(207, 18)
        Me.lnAssignedED.TabIndex = 33
        Me.lnAssignedED.Text = "Assign ED on Open Period(s)"
        Me.lnAssignedED.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTranHeadDesc
        '
        Me.txtTranHeadDesc.Flags = 0
        Me.txtTranHeadDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTranHeadDesc.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTranHeadDesc.Location = New System.Drawing.Point(424, 270)
        Me.txtTranHeadDesc.MaxLength = 0
        Me.txtTranHeadDesc.Multiline = True
        Me.txtTranHeadDesc.Name = "txtTranHeadDesc"
        Me.txtTranHeadDesc.ReadOnly = True
        Me.txtTranHeadDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTranHeadDesc.Size = New System.Drawing.Size(78, 36)
        Me.txtTranHeadDesc.TabIndex = 31
        Me.txtTranHeadDesc.Visible = False
        '
        'objbtnAddTranHead
        '
        Me.objbtnAddTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTranHead.BorderSelected = False
        Me.objbtnAddTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAddTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTranHead.Location = New System.Drawing.Point(394, 156)
        Me.objbtnAddTranHead.Name = "objbtnAddTranHead"
        Me.objbtnAddTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTranHead.TabIndex = 14
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(340, 246)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(78, 15)
        Me.lblAmount.TabIndex = 15
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.Visible = False
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Enabled = False
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(424, 243)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(78, 21)
        Me.txtAmount.TabIndex = 16
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmount.Visible = False
        '
        'lblFormula
        '
        Me.lblFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormula.Location = New System.Drawing.Point(340, 274)
        Me.lblFormula.Name = "lblFormula"
        Me.lblFormula.Size = New System.Drawing.Size(78, 15)
        Me.lblFormula.TabIndex = 17
        Me.lblFormula.Text = "Formula"
        Me.lblFormula.Visible = False
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(424, 312)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 27
        Me.objbtnSearchTranHead.Visible = False
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(111, 156)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(277, 21)
        Me.cboTransactionHead.TabIndex = 13
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(27, 159)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(78, 15)
        Me.lblTransactionHead.TabIndex = 12
        Me.lblTransactionHead.Text = "Trans. Head"
        '
        'objbtnAddBenefitPlan
        '
        Me.objbtnAddBenefitPlan.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitPlan.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitPlan.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBenefitPlan.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBenefitPlan.BorderSelected = False
        Me.objbtnAddBenefitPlan.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAddBenefitPlan.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddBenefitPlan.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBenefitPlan.Location = New System.Drawing.Point(394, 129)
        Me.objbtnAddBenefitPlan.Name = "objbtnAddBenefitPlan"
        Me.objbtnAddBenefitPlan.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBenefitPlan.TabIndex = 11
        '
        'cboBenefitPlan
        '
        Me.cboBenefitPlan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitPlan.FormattingEnabled = True
        Me.cboBenefitPlan.Location = New System.Drawing.Point(111, 129)
        Me.cboBenefitPlan.Name = "cboBenefitPlan"
        Me.cboBenefitPlan.Size = New System.Drawing.Size(277, 21)
        Me.cboBenefitPlan.TabIndex = 10
        '
        'lblBenefitPlan
        '
        Me.lblBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitPlan.Location = New System.Drawing.Point(27, 132)
        Me.lblBenefitPlan.Name = "lblBenefitPlan"
        Me.lblBenefitPlan.Size = New System.Drawing.Size(78, 15)
        Me.lblBenefitPlan.TabIndex = 9
        Me.lblBenefitPlan.Text = "Benefit Plan"
        Me.lblBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(394, 102)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 8
        '
        'lnBenefitInfo
        '
        Me.lnBenefitInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnBenefitInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnBenefitInfo.Location = New System.Drawing.Point(8, 81)
        Me.lnBenefitInfo.Name = "lnBenefitInfo"
        Me.lnBenefitInfo.Size = New System.Drawing.Size(285, 18)
        Me.lnBenefitInfo.TabIndex = 5
        Me.lnBenefitInfo.Text = "Benefit Info"
        Me.lnBenefitInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(285, 18)
        Me.lnEmployeeName.TabIndex = 1
        Me.lnEmployeeName.Text = "Employee Info"
        Me.lnEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(394, 57)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(111, 57)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(277, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(27, 60)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(111, 102)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(277, 21)
        Me.cboBenefitGroup.TabIndex = 7
        '
        'lblBenefitType
        '
        Me.lblBenefitType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitType.Location = New System.Drawing.Point(27, 105)
        Me.lblBenefitType.Name = "lblBenefitType"
        Me.lblBenefitType.Size = New System.Drawing.Size(81, 15)
        Me.lblBenefitType.TabIndex = 6
        Me.lblBenefitType.Text = "Benefit Group"
        Me.lblBenefitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValueBasis
        '
        Me.lblValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValueBasis.Location = New System.Drawing.Point(39, 208)
        Me.lblValueBasis.Name = "lblValueBasis"
        Me.lblValueBasis.Size = New System.Drawing.Size(78, 15)
        Me.lblValueBasis.TabIndex = 11
        Me.lblValueBasis.Text = "Value Basis"
        Me.lblValueBasis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblValueBasis.Visible = False
        '
        'lblBenefitInAmount
        '
        Me.lblBenefitInAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitInAmount.Location = New System.Drawing.Point(39, 262)
        Me.lblBenefitInAmount.Name = "lblBenefitInAmount"
        Me.lblBenefitInAmount.Size = New System.Drawing.Size(78, 15)
        Me.lblBenefitInAmount.TabIndex = 15
        Me.lblBenefitInAmount.Text = "In Amount"
        Me.lblBenefitInAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitInAmount.Visible = False
        '
        'txtBenefitAmount
        '
        Me.txtBenefitAmount.AllowNegative = True
        Me.txtBenefitAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 196608})
        Me.txtBenefitAmount.DigitsInGroup = 0
        Me.txtBenefitAmount.Flags = 0
        Me.txtBenefitAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitAmount.Location = New System.Drawing.Point(123, 259)
        Me.txtBenefitAmount.MaxDecimalPlaces = 6
        Me.txtBenefitAmount.MaxWholeDigits = 21
        Me.txtBenefitAmount.Name = "txtBenefitAmount"
        Me.txtBenefitAmount.Prefix = ""
        Me.txtBenefitAmount.RangeMax = 1.7976931348623157E+308
        Me.txtBenefitAmount.RangeMin = -1.7976931348623157E+308
        Me.txtBenefitAmount.Size = New System.Drawing.Size(87, 21)
        Me.txtBenefitAmount.TabIndex = 16
        Me.txtBenefitAmount.Text = "0.000"
        Me.txtBenefitAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtBenefitAmount.Visible = False
        '
        'lblBenefitsInPercent
        '
        Me.lblBenefitsInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitsInPercent.Location = New System.Drawing.Point(39, 235)
        Me.lblBenefitsInPercent.Name = "lblBenefitsInPercent"
        Me.lblBenefitsInPercent.Size = New System.Drawing.Size(78, 15)
        Me.lblBenefitsInPercent.TabIndex = 13
        Me.lblBenefitsInPercent.Text = "Percent (%)"
        Me.lblBenefitsInPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitsInPercent.Visible = False
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Checked = False
        Me.DateTimePicker2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(292, 232)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(99, 21)
        Me.DateTimePicker2.TabIndex = 19
        Me.DateTimePicker2.Visible = False
        '
        'dtpAssignDate
        '
        Me.dtpAssignDate.Checked = False
        Me.dtpAssignDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssignDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssignDate.Location = New System.Drawing.Point(265, 297)
        Me.dtpAssignDate.Name = "dtpAssignDate"
        Me.dtpAssignDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpAssignDate.TabIndex = 19
        Me.dtpAssignDate.Visible = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Checked = False
        Me.DateTimePicker1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(292, 259)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.ShowCheckBox = True
        Me.DateTimePicker1.Size = New System.Drawing.Size(99, 21)
        Me.DateTimePicker1.TabIndex = 21
        Me.DateTimePicker1.Visible = False
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(233, 235)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(62, 15)
        Me.lblStartDate.TabIndex = 18
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStartDate.Visible = False
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(265, 324)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpEndDate.TabIndex = 21
        Me.dtpEndDate.Visible = False
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(233, 262)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(62, 15)
        Me.lblEndDate.TabIndex = 20
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEndDate.Visible = False
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine2.ForeColor = System.Drawing.Color.Gray
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(216, 232)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(11, 48)
        Me.EZeeStraightLine2.TabIndex = 17
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Visible = False
        '
        'cboValueBasis
        '
        Me.cboValueBasis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboValueBasis.FormattingEnabled = True
        Me.cboValueBasis.Location = New System.Drawing.Point(123, 205)
        Me.cboValueBasis.Name = "cboValueBasis"
        Me.cboValueBasis.Size = New System.Drawing.Size(182, 21)
        Me.cboValueBasis.TabIndex = 12
        Me.cboValueBasis.Visible = False
        '
        'txtDBBenefitInPercent
        '
        Me.txtDBBenefitInPercent.AllowNegative = True
        Me.txtDBBenefitInPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDBBenefitInPercent.DigitsInGroup = 0
        Me.txtDBBenefitInPercent.Flags = 0
        Me.txtDBBenefitInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBBenefitInPercent.Location = New System.Drawing.Point(123, 232)
        Me.txtDBBenefitInPercent.MaxDecimalPlaces = 6
        Me.txtDBBenefitInPercent.MaxWholeDigits = 21
        Me.txtDBBenefitInPercent.Name = "txtDBBenefitInPercent"
        Me.txtDBBenefitInPercent.Prefix = ""
        Me.txtDBBenefitInPercent.RangeMax = 1.7976931348623157E+308
        Me.txtDBBenefitInPercent.RangeMin = -1.7976931348623157E+308
        Me.txtDBBenefitInPercent.Size = New System.Drawing.Size(87, 21)
        Me.txtDBBenefitInPercent.TabIndex = 23
        Me.txtDBBenefitInPercent.Text = "0"
        Me.txtDBBenefitInPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDBBenefitInPercent.Visible = False
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 394)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(465, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(253, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(356, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn2.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Medical Ref. No"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'frmEmployeeBenefitCoverage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(465, 449)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlBenefitPlan)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeBenefitCoverage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Benefit Coverage"
        Me.pnlBenefitPlan.ResumeLayout(False)
        Me.pnlBenefitPlan.PerformLayout()
        Me.gbBenefitPlan.ResumeLayout(False)
        Me.gbBenefitPlan.PerformLayout()
        Me.pnlPeriodList.ResumeLayout(False)
        Me.pnlPeriodList.PerformLayout()
        CType(Me.dgvAssignED, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlBenefitPlan As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbBenefitPlan As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitType As System.Windows.Forms.Label
    Friend WithEvents lblBenefitsInPercent As System.Windows.Forms.Label
    Friend WithEvents txtBenefitAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBenefitInAmount As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpAssignDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnBenefitInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnAddBenefitPlan As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBenefitPlan As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitPlan As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents cboValueBasis As System.Windows.Forms.ComboBox
    Friend WithEvents lblValueBasis As System.Windows.Forms.Label
    Friend WithEvents txtDBBenefitInPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFormula As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnAddTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents txtTranHeadDesc As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlPeriodList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lnAssignedED As eZee.Common.eZeeLine
    Friend WithEvents dgvAssignED As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents colhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhEDUnkId As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhPeriodUnkID As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhPeriodName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMedicalRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
