﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBenefitallocation_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBenefitallocation_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlAllication = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnAddClassGrp = New eZee.Common.eZeeGradientButton
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.objbtnAddJobGrp = New eZee.Common.eZeeGradientButton
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.objbtnAddTeam = New eZee.Common.eZeeGradientButton
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.objbtnAddGradeLevel = New eZee.Common.eZeeGradientButton
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.objbtnAddGradeGrp = New eZee.Common.eZeeGradientButton
        Me.objbtnAddUnitGrp = New eZee.Common.eZeeGradientButton
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.objbtnAddSectionGrp = New eZee.Common.eZeeGradientButton
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnAddDeptGrp = New eZee.Common.eZeeGradientButton
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.pnlPeriodList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvBenefit = New System.Windows.Forms.DataGridView
        Me.colhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhBATranUnkId = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhBAUnkID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBPlanUnkID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBPLanName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranHeadType = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhTranHead = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.lblBenefitGroup = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objbtnAddCostCenter = New eZee.Common.eZeeGradientButton
        Me.objbtnAddClass = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnAddJob = New eZee.Common.eZeeGradientButton
        Me.objbtnAddUnit = New eZee.Common.eZeeGradientButton
        Me.objbtnAddSection = New eZee.Common.eZeeGradientButton
        Me.objbtnAddDepartment = New eZee.Common.eZeeGradientButton
        Me.objbtnAddStation = New eZee.Common.eZeeGradientButton
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.pnlBenefitAllocation = New System.Windows.Forms.Panel
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeFooter1.SuspendLayout()
        Me.pnlAllication.SuspendLayout()
        Me.pnlPeriodList.SuspendLayout()
        CType(Me.dgvBenefit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBenefitAllocation.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 457)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(575, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(473, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(377, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'pnlAllication
        '
        Me.pnlAllication.BorderColor = System.Drawing.Color.Black
        Me.pnlAllication.Checked = False
        Me.pnlAllication.CollapseAllExceptThis = False
        Me.pnlAllication.CollapsedHoverImage = Nothing
        Me.pnlAllication.CollapsedNormalImage = Nothing
        Me.pnlAllication.CollapsedPressedImage = Nothing
        Me.pnlAllication.CollapseOnLoad = False
        Me.pnlAllication.Controls.Add(Me.lblPeriod)
        Me.pnlAllication.Controls.Add(Me.cboPeriod)
        Me.pnlAllication.Controls.Add(Me.objbtnAddClassGrp)
        Me.pnlAllication.Controls.Add(Me.cboClassGroup)
        Me.pnlAllication.Controls.Add(Me.lblClassGroup)
        Me.pnlAllication.Controls.Add(Me.objbtnAddJobGrp)
        Me.pnlAllication.Controls.Add(Me.cboJobGroup)
        Me.pnlAllication.Controls.Add(Me.lblJobGroup)
        Me.pnlAllication.Controls.Add(Me.objbtnAddTeam)
        Me.pnlAllication.Controls.Add(Me.lblTeam)
        Me.pnlAllication.Controls.Add(Me.cboTeams)
        Me.pnlAllication.Controls.Add(Me.objbtnAddGradeLevel)
        Me.pnlAllication.Controls.Add(Me.lblGradeLevel)
        Me.pnlAllication.Controls.Add(Me.cboGradeLevel)
        Me.pnlAllication.Controls.Add(Me.cboGradeGroup)
        Me.pnlAllication.Controls.Add(Me.lblGradeGroup)
        Me.pnlAllication.Controls.Add(Me.objbtnAddGradeGrp)
        Me.pnlAllication.Controls.Add(Me.objbtnAddUnitGrp)
        Me.pnlAllication.Controls.Add(Me.lblUnitGroup)
        Me.pnlAllication.Controls.Add(Me.cboUnitGroup)
        Me.pnlAllication.Controls.Add(Me.objbtnAddSectionGrp)
        Me.pnlAllication.Controls.Add(Me.lblSectionGroup)
        Me.pnlAllication.Controls.Add(Me.cboSectionGroup)
        Me.pnlAllication.Controls.Add(Me.objbtnAddDeptGrp)
        Me.pnlAllication.Controls.Add(Me.cboDepartmentGrp)
        Me.pnlAllication.Controls.Add(Me.lblDepartmentGroup)
        Me.pnlAllication.Controls.Add(Me.pnlPeriodList)
        Me.pnlAllication.Controls.Add(Me.objbtnSearchGroup)
        Me.pnlAllication.Controls.Add(Me.cboBenefitGroup)
        Me.pnlAllication.Controls.Add(Me.lblBenefitGroup)
        Me.pnlAllication.Controls.Add(Me.objelLine1)
        Me.pnlAllication.Controls.Add(Me.objbtnAddCostCenter)
        Me.pnlAllication.Controls.Add(Me.objbtnAddClass)
        Me.pnlAllication.Controls.Add(Me.objbtnAddGrade)
        Me.pnlAllication.Controls.Add(Me.objbtnAddJob)
        Me.pnlAllication.Controls.Add(Me.objbtnAddUnit)
        Me.pnlAllication.Controls.Add(Me.objbtnAddSection)
        Me.pnlAllication.Controls.Add(Me.objbtnAddDepartment)
        Me.pnlAllication.Controls.Add(Me.objbtnAddStation)
        Me.pnlAllication.Controls.Add(Me.cboCostcenter)
        Me.pnlAllication.Controls.Add(Me.lblCostCenter)
        Me.pnlAllication.Controls.Add(Me.cboClass)
        Me.pnlAllication.Controls.Add(Me.lblClass)
        Me.pnlAllication.Controls.Add(Me.cboGrade)
        Me.pnlAllication.Controls.Add(Me.lblGrade)
        Me.pnlAllication.Controls.Add(Me.cboJob)
        Me.pnlAllication.Controls.Add(Me.lblJob)
        Me.pnlAllication.Controls.Add(Me.cboUnit)
        Me.pnlAllication.Controls.Add(Me.lblUnit)
        Me.pnlAllication.Controls.Add(Me.cboSection)
        Me.pnlAllication.Controls.Add(Me.lblSection)
        Me.pnlAllication.Controls.Add(Me.cboDepartment)
        Me.pnlAllication.Controls.Add(Me.lblDepartment)
        Me.pnlAllication.Controls.Add(Me.cboBranch)
        Me.pnlAllication.Controls.Add(Me.lblBranch)
        Me.pnlAllication.ExpandedHoverImage = Nothing
        Me.pnlAllication.ExpandedNormalImage = Nothing
        Me.pnlAllication.ExpandedPressedImage = Nothing
        Me.pnlAllication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAllication.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.pnlAllication.HeaderHeight = 25
        Me.pnlAllication.HeaderMessage = ""
        Me.pnlAllication.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.pnlAllication.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.pnlAllication.HeightOnCollapse = 0
        Me.pnlAllication.LeftTextSpace = 0
        Me.pnlAllication.Location = New System.Drawing.Point(7, 7)
        Me.pnlAllication.Name = "pnlAllication"
        Me.pnlAllication.OpenHeight = 300
        Me.pnlAllication.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.pnlAllication.ShowBorder = True
        Me.pnlAllication.ShowCheckBox = False
        Me.pnlAllication.ShowCollapseButton = False
        Me.pnlAllication.ShowDefaultBorderColor = True
        Me.pnlAllication.ShowDownButton = False
        Me.pnlAllication.ShowHeader = True
        Me.pnlAllication.Size = New System.Drawing.Size(560, 443)
        Me.pnlAllication.TabIndex = 0
        Me.pnlAllication.Temp = 0
        Me.pnlAllication.Text = "Benefit Allocation"
        Me.pnlAllication.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(278, 267)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(88, 14)
        Me.lblPeriod.TabIndex = 223
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(374, 265)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(150, 21)
        Me.cboPeriod.TabIndex = 222
        '
        'objbtnAddClassGrp
        '
        Me.objbtnAddClassGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddClassGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddClassGrp.BorderSelected = False
        Me.objbtnAddClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddClassGrp.Location = New System.Drawing.Point(532, 197)
        Me.objbtnAddClassGrp.Name = "objbtnAddClassGrp"
        Me.objbtnAddClassGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddClassGrp.TabIndex = 63
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownWidth = 200
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(374, 197)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboClassGroup.TabIndex = 62
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(287, 199)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(75, 17)
        Me.lblClassGroup.TabIndex = 61
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddJobGrp
        '
        Me.objbtnAddJobGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJobGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJobGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJobGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJobGrp.BorderSelected = False
        Me.objbtnAddJobGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJobGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddJobGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJobGrp.Location = New System.Drawing.Point(532, 170)
        Me.objbtnAddJobGrp.Name = "objbtnAddJobGrp"
        Me.objbtnAddJobGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJobGrp.TabIndex = 60
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownWidth = 200
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(373, 170)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(152, 21)
        Me.cboJobGroup.TabIndex = 59
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(286, 172)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(81, 17)
        Me.lblJobGroup.TabIndex = 58
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddTeam
        '
        Me.objbtnAddTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTeam.BorderSelected = False
        Me.objbtnAddTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTeam.Location = New System.Drawing.Point(532, 116)
        Me.objbtnAddTeam.Name = "objbtnAddTeam"
        Me.objbtnAddTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTeam.TabIndex = 57
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(286, 119)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(81, 17)
        Me.lblTeam.TabIndex = 55
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTeams
        '
        Me.cboTeams.DropDownWidth = 200
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(373, 116)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(151, 21)
        Me.cboTeams.TabIndex = 56
        '
        'objbtnAddGradeLevel
        '
        Me.objbtnAddGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGradeLevel.BorderSelected = False
        Me.objbtnAddGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGradeLevel.Location = New System.Drawing.Point(253, 170)
        Me.objbtnAddGradeLevel.Name = "objbtnAddGradeLevel"
        Me.objbtnAddGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGradeLevel.TabIndex = 54
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(8, 173)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(81, 15)
        Me.lblGradeLevel.TabIndex = 52
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownWidth = 200
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(94, 170)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(151, 21)
        Me.cboGradeLevel.TabIndex = 53
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownWidth = 200
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(94, 143)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboGradeGroup.TabIndex = 48
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(8, 146)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(81, 15)
        Me.lblGradeGroup.TabIndex = 47
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGradeGrp
        '
        Me.objbtnAddGradeGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGradeGrp.BorderSelected = False
        Me.objbtnAddGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGradeGrp.Location = New System.Drawing.Point(253, 143)
        Me.objbtnAddGradeGrp.Name = "objbtnAddGradeGrp"
        Me.objbtnAddGradeGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGradeGrp.TabIndex = 49
        '
        'objbtnAddUnitGrp
        '
        Me.objbtnAddUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddUnitGrp.BorderSelected = False
        Me.objbtnAddUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddUnitGrp.Location = New System.Drawing.Point(532, 88)
        Me.objbtnAddUnitGrp.Name = "objbtnAddUnitGrp"
        Me.objbtnAddUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddUnitGrp.TabIndex = 45
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(286, 91)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(81, 17)
        Me.lblUnitGroup.TabIndex = 43
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownWidth = 200
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(373, 88)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboUnitGroup.TabIndex = 44
        '
        'objbtnAddSectionGrp
        '
        Me.objbtnAddSectionGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSectionGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSectionGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSectionGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSectionGrp.BorderSelected = False
        Me.objbtnAddSectionGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSectionGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSectionGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSectionGrp.Location = New System.Drawing.Point(532, 61)
        Me.objbtnAddSectionGrp.Name = "objbtnAddSectionGrp"
        Me.objbtnAddSectionGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSectionGrp.TabIndex = 42
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(286, 64)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(81, 17)
        Me.lblSectionGroup.TabIndex = 40
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownWidth = 200
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(373, 61)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboSectionGroup.TabIndex = 41
        '
        'objbtnAddDeptGrp
        '
        Me.objbtnAddDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDeptGrp.BorderSelected = False
        Me.objbtnAddDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDeptGrp.Location = New System.Drawing.Point(532, 33)
        Me.objbtnAddDeptGrp.Name = "objbtnAddDeptGrp"
        Me.objbtnAddDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDeptGrp.TabIndex = 39
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownWidth = 200
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(373, 33)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(151, 21)
        Me.cboDepartmentGrp.TabIndex = 38
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(286, 36)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(81, 17)
        Me.lblDepartmentGroup.TabIndex = 37
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlPeriodList
        '
        Me.pnlPeriodList.Controls.Add(Me.objchkSelectAll)
        Me.pnlPeriodList.Controls.Add(Me.dgvBenefit)
        Me.pnlPeriodList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlPeriodList.Location = New System.Drawing.Point(11, 297)
        Me.pnlPeriodList.Name = "pnlPeriodList"
        Me.pnlPeriodList.Size = New System.Drawing.Size(542, 138)
        Me.pnlPeriodList.TabIndex = 35
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 5)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 17
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        Me.objchkSelectAll.Visible = False
        '
        'dgvBenefit
        '
        Me.dgvBenefit.AllowUserToAddRows = False
        Me.dgvBenefit.AllowUserToDeleteRows = False
        Me.dgvBenefit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBenefit.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhCheck, Me.colhBATranUnkId, Me.colhBAUnkID, Me.colhBPlanUnkID, Me.colhBPLanName, Me.colhTranHeadType, Me.colhTranHead, Me.colhAmount})
        Me.dgvBenefit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBenefit.Location = New System.Drawing.Point(0, 0)
        Me.dgvBenefit.MultiSelect = False
        Me.dgvBenefit.Name = "dgvBenefit"
        Me.dgvBenefit.RowHeadersVisible = False
        Me.dgvBenefit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvBenefit.Size = New System.Drawing.Size(542, 138)
        Me.dgvBenefit.TabIndex = 221
        '
        'colhCheck
        '
        Me.colhCheck.HeaderText = ""
        Me.colhCheck.Name = "colhCheck"
        Me.colhCheck.Visible = False
        Me.colhCheck.Width = 25
        '
        'colhBATranUnkId
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.colhBATranUnkId.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhBATranUnkId.HeaderText = "BATranUnkID"
        Me.colhBATranUnkId.Name = "colhBATranUnkId"
        Me.colhBATranUnkId.ReadOnly = True
        Me.colhBATranUnkId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhBATranUnkId.Visible = False
        '
        'colhBAUnkID
        '
        Me.colhBAUnkID.HeaderText = "BAllocUnkID"
        Me.colhBAUnkID.Name = "colhBAUnkID"
        Me.colhBAUnkID.ReadOnly = True
        Me.colhBAUnkID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhBAUnkID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhBAUnkID.Visible = False
        '
        'colhBPlanUnkID
        '
        Me.colhBPlanUnkID.HeaderText = "BPlanUnkID"
        Me.colhBPlanUnkID.Name = "colhBPlanUnkID"
        Me.colhBPlanUnkID.ReadOnly = True
        Me.colhBPlanUnkID.Visible = False
        '
        'colhBPLanName
        '
        Me.colhBPLanName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhBPLanName.HeaderText = "Benefit Plan"
        Me.colhBPLanName.Name = "colhBPLanName"
        Me.colhBPLanName.ReadOnly = True
        '
        'colhTranHeadType
        '
        Me.colhTranHeadType.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colhTranHeadType.HeaderText = "Head Type"
        Me.colhTranHeadType.Name = "colhTranHeadType"
        '
        'colhTranHead
        '
        Me.colhTranHead.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colhTranHead.HeaderText = "Transaction Head"
        Me.colhTranHead.Name = "colhTranHead"
        Me.colhTranHead.Width = 120
        '
        'colhAmount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhAmount.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhAmount.HeaderText = "Amount"
        Me.colhAmount.Name = "colhAmount"
        Me.colhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.colhAmount.Width = 150
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(251, 265)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 33
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(94, 265)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboBenefitGroup.TabIndex = 31
        '
        'lblBenefitGroup
        '
        Me.lblBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitGroup.Location = New System.Drawing.Point(8, 268)
        Me.lblBenefitGroup.Name = "lblBenefitGroup"
        Me.lblBenefitGroup.Size = New System.Drawing.Size(77, 15)
        Me.lblBenefitGroup.TabIndex = 30
        Me.lblBenefitGroup.Text = "Benefit Group"
        Me.lblBenefitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 252)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(538, 8)
        Me.objelLine1.TabIndex = 29
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCostCenter
        '
        Me.objbtnAddCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCostCenter.BorderSelected = False
        Me.objbtnAddCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCostCenter.Location = New System.Drawing.Point(532, 224)
        Me.objbtnAddCostCenter.Name = "objbtnAddCostCenter"
        Me.objbtnAddCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCostCenter.TabIndex = 24
        '
        'objbtnAddClass
        '
        Me.objbtnAddClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddClass.BorderSelected = False
        Me.objbtnAddClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddClass.Location = New System.Drawing.Point(253, 224)
        Me.objbtnAddClass.Name = "objbtnAddClass"
        Me.objbtnAddClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddClass.TabIndex = 21
        '
        'objbtnAddGrade
        '
        Me.objbtnAddGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGrade.BorderSelected = False
        Me.objbtnAddGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGrade.Location = New System.Drawing.Point(532, 143)
        Me.objbtnAddGrade.Name = "objbtnAddGrade"
        Me.objbtnAddGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGrade.TabIndex = 18
        '
        'objbtnAddJob
        '
        Me.objbtnAddJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJob.BorderSelected = False
        Me.objbtnAddJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJob.Location = New System.Drawing.Point(253, 197)
        Me.objbtnAddJob.Name = "objbtnAddJob"
        Me.objbtnAddJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJob.TabIndex = 15
        '
        'objbtnAddUnit
        '
        Me.objbtnAddUnit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddUnit.BorderSelected = False
        Me.objbtnAddUnit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddUnit.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddUnit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddUnit.Location = New System.Drawing.Point(253, 116)
        Me.objbtnAddUnit.Name = "objbtnAddUnit"
        Me.objbtnAddUnit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddUnit.TabIndex = 12
        '
        'objbtnAddSection
        '
        Me.objbtnAddSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSection.BorderSelected = False
        Me.objbtnAddSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSection.Location = New System.Drawing.Point(253, 88)
        Me.objbtnAddSection.Name = "objbtnAddSection"
        Me.objbtnAddSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSection.TabIndex = 9
        '
        'objbtnAddDepartment
        '
        Me.objbtnAddDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDepartment.BorderSelected = False
        Me.objbtnAddDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDepartment.Location = New System.Drawing.Point(253, 61)
        Me.objbtnAddDepartment.Name = "objbtnAddDepartment"
        Me.objbtnAddDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDepartment.TabIndex = 6
        '
        'objbtnAddStation
        '
        Me.objbtnAddStation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddStation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddStation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddStation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddStation.BorderSelected = False
        Me.objbtnAddStation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddStation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddStation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddStation.Location = New System.Drawing.Point(253, 33)
        Me.objbtnAddStation.Name = "objbtnAddStation"
        Me.objbtnAddStation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddStation.TabIndex = 3
        '
        'cboCostcenter
        '
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(374, 224)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(151, 21)
        Me.cboCostcenter.TabIndex = 23
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(286, 227)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(76, 14)
        Me.lblCostCenter.TabIndex = 22
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(94, 224)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(151, 21)
        Me.cboClass.TabIndex = 20
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(8, 227)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(77, 15)
        Me.lblClass.TabIndex = 19
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(373, 143)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(151, 21)
        Me.cboGrade.TabIndex = 17
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(286, 146)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(81, 17)
        Me.lblGrade.TabIndex = 16
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(94, 197)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(151, 21)
        Me.cboJob.TabIndex = 14
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 200)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(81, 15)
        Me.lblJob.TabIndex = 13
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnit
        '
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(94, 116)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(151, 21)
        Me.cboUnit.TabIndex = 11
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(8, 119)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(81, 15)
        Me.lblUnit.TabIndex = 10
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(94, 88)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(151, 21)
        Me.cboSection.TabIndex = 8
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 91)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(81, 15)
        Me.lblSection.TabIndex = 7
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(94, 61)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(151, 21)
        Me.cboDepartment.TabIndex = 5
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 64)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(81, 15)
        Me.lblDepartment.TabIndex = 4
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(94, 33)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(151, 21)
        Me.cboBranch.TabIndex = 2
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(81, 15)
        Me.lblBranch.TabIndex = 1
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlBenefitAllocation
        '
        Me.pnlBenefitAllocation.Controls.Add(Me.pnlAllication)
        Me.pnlBenefitAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBenefitAllocation.Location = New System.Drawing.Point(0, 0)
        Me.pnlBenefitAllocation.Name = "pnlBenefitAllocation"
        Me.pnlBenefitAllocation.Size = New System.Drawing.Size(575, 512)
        Me.pnlBenefitAllocation.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "BAllocUnkID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "BPlanUnkID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Benefit Plan"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn4.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'frmBenefitallocation_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(575, 512)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlBenefitAllocation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBenefitallocation_AddEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Benefit Allocation"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.pnlAllication.ResumeLayout(False)
        Me.pnlPeriodList.ResumeLayout(False)
        Me.pnlPeriodList.PerformLayout()
        CType(Me.dgvBenefit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBenefitAllocation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnlAllication As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents pnlBenefitAllocation As System.Windows.Forms.Panel
    Friend WithEvents objbtnAddCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddUnit As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddSection As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddStation As eZee.Common.eZeeGradientButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlPeriodList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvBenefit As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhBATranUnkId As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhBAUnkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBPlanUnkID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBPLanName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranHeadType As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhTranHead As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnAddDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnAddSectionGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnAddJobGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
End Class
