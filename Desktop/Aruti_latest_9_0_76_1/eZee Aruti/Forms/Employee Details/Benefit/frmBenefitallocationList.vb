﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBenefitallocationList

#Region " Private Varaibles "
    Private objBenefitAllocation As clsBenefitAllocation
    Private ReadOnly mstrModuleName As String = "frmBenefitallocationList"
    Private mstrSearchText As String = "" 'Hemant (22 June 2019)
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Sep 2019) -- End
#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objStation As New clsStation
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJob As New clsJobs
        Dim objGrade As New clsGrade
        Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master
        Dim dsCombos As New DataSet

        'Pinkal (24-Feb-2017) -- Start
        'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objSectionGrp As New clsSectionGroup
        Dim objJobGrp As New clsJobGroup
        Dim objGradeGrp As New clsGradeGroup
        Dim objClassGrp As New clsClassGroup
        Dim objTeam As New clsTeams
        Dim objUnitGroup As New clsUnitGroup
        'Pinkal (24-Feb-2017) -- End
        'Sohail (17 Sep 2019) -- Start
        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (17 Sep 2019) -- End

        Try
            dsCombos = objStation.getComboList("Station", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboBranch) 'Hemant (22 June 2019)

            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboDepartment) 'Hemant (22 June 2019)

            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboSections) 'Hemant (22 June 2019)

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnit
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboUnit) 'Hemant (22 June 2019)

            dsCombos = objJob.getComboList("Jobs", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Jobs")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboJob) 'Hemant (22 June 2019)

            dsCombos = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGrade) 'Hemant (22 June 2019)

            dsCombos = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Class")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboClass) 'Hemant (22 June 2019)

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostcenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboCostcenter) 'Hemant (22 June 2019)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            dsCombos = objDeptGrp.getComboList("List", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboDepartmentGrp) 'Hemant (22 June 2019)

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboSectionGroup) 'Hemant (22 June 2019)

            dsCombos = objUnitGroup.getComboList("List", True, -1)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboUnitGroup) 'Hemant (22 June 2019)

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboTeams) 'Hemant (22 June 2019)

            dsCombos = objJobGrp.getComboList("List", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboJobGroup) 'Hemant (22 June 2019)

            dsCombos = objGradeGrp.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboGradeGroup) 'Hemant (22 June 2019)

            dsCombos = objClassGrp.getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            Call SetDefaultSearchText(cboClassGroup) 'Hemant (22 June 2019)
            'Pinkal (24-Feb-2017) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            End With

            dsCombos = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombos.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
            'Sohail (17 Sep 2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objPeriod = Nothing
            objMaster = Nothing
            'Sohail (17 Sep 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewBenefitAllocationList = True Then

                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                ''dsList = objBenefitAllocation.GetList("BenefitAllocation")
                btnInactive.Visible = False
                btnActive.Visible = False

                dsList = objBenefitAllocation.GetList("BenefitAllocation", mdtPeriodEndDate, CInt(cboActiveInactive.SelectedValue))
                'Sohail (17 Sep 2019) -- End



                'Pinkal (24-Feb-2017) -- Start
                'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

                'If CInt(cboClass.SelectedValue) > 0 Then
                '    StrSearching &= "AND classunkid = " & CInt(cboClass.SelectedValue)
                'End If

                'If CInt(cboCostcenter.SelectedValue) > 0 Then
                '    StrSearching &= "AND costcenterunkid = " & CInt(cboCostcenter.SelectedValue)
                'End If

                'If CInt(cboDepartment.SelectedValue) > 0 Then
                '    StrSearching &= "AND departmentunkid = " & CInt(cboDepartment.SelectedValue)
                'End If

                'If CInt(cboGrade.SelectedValue) > 0 Then
                '    StrSearching &= "AND gradeunkid = " & CInt(cboGrade.SelectedValue)
                'End If

                'If CInt(cboJob.SelectedValue) > 0 Then
                '    StrSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue)
                'End If

                'If CInt(cboSections.SelectedValue) > 0 Then
                '    StrSearching &= "AND sectionunkid = " & CInt(cboSections.SelectedValue)
                'End If

                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    StrSearching &= "AND stationunkid = " & CInt(cboBranch.SelectedValue)
                'End If

                'If CInt(cboUnit.SelectedValue) > 0 Then
                '    StrSearching &= "AND unitunkid = " & CInt(cboUnit.SelectedValue)
                'End If

                If CInt(cboBranch.SelectedValue) > 0 Then
                    StrSearching &= "AND stationunkid = " & CInt(cboBranch.SelectedValue)
                End If

                If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
                    StrSearching &= "AND deptgroupunkid = " & CInt(cboDepartmentGrp.SelectedValue)
                End If

                If CInt(cboDepartment.SelectedValue) > 0 Then
                    StrSearching &= "AND departmentunkid = " & CInt(cboDepartment.SelectedValue)
                End If

                If CInt(cboSectionGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND sectiongroupunkid = " & CInt(cboSectionGroup.SelectedValue)
                End If

                If CInt(cboSections.SelectedValue) > 0 Then
                    StrSearching &= "AND sectionunkid = " & CInt(cboSections.SelectedValue)
                End If

                If CInt(cboUnitGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND unitgroupunkid = " & CInt(cboUnitGroup.SelectedValue)
                End If

                If CInt(cboUnit.SelectedValue) > 0 Then
                    StrSearching &= "AND unitunkid = " & CInt(cboUnit.SelectedValue)
                End If

                If CInt(cboTeams.SelectedValue) > 0 Then
                    StrSearching &= "AND teamunkid = " & CInt(cboTeams.SelectedValue)
                End If

                If CInt(cboGradeGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND gradegroupunkid = " & CInt(cboGradeGroup.SelectedValue)
                End If

                If CInt(cboGrade.SelectedValue) > 0 Then
                    StrSearching &= "AND gradeunkid = " & CInt(cboGrade.SelectedValue)
                End If

                If CInt(cboGradeLevel.SelectedValue) > 0 Then
                    StrSearching &= "AND gradelevelunkid = " & CInt(cboGradeLevel.SelectedValue)
                End If

                If CInt(cboJobGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND jobgroupunkid = " & CInt(cboJobGroup.SelectedValue)
                End If

                If CInt(cboJob.SelectedValue) > 0 Then
                    StrSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue)
                End If

                If CInt(cboClassGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND classgroupunkid = " & CInt(cboClassGroup.SelectedValue)
                End If

                If CInt(cboClass.SelectedValue) > 0 Then
                    StrSearching &= "AND classunkid = " & CInt(cboClass.SelectedValue)
                End If

                If CInt(cboCostcenter.SelectedValue) > 0 Then
                    StrSearching &= "AND costcenterunkid = " & CInt(cboCostcenter.SelectedValue)
                End If


                'Pinkal (24-Feb-2017) -- End

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtTable = New DataView(dsList.Tables("BenefitAllocation"), StrSearching, "BenefitGrp", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables("BenefitAllocation"), "", "BenefitGrp", DataViewRowState.CurrentRows).ToTable
                End If

                lvBenefitAllocation.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    'lvItem.Text = dtRow.Item("Station").ToString
                    'lvItem.SubItems.Add(dtRow.Item("Department").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("Section").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("Unit").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("Job").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("ClassName").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("Grade").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("CostCenter").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("BenefitGrp").ToString)

                    lvItem.Text = dtRow.Item("BenefitGrp").ToString
                    'Sohail (17 Sep 2019) -- Start
                    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                    lvItem.SubItems.Add(dtRow.Item("period_name").ToString)
                    'Sohail (17 Sep 2019) -- End
                    lvItem.SubItems.Add(dtRow.Item("Station").ToString)
                    lvItem.SubItems.Add(dtRow.Item("DeptGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Department").ToString)
                    lvItem.SubItems.Add(dtRow.Item("SectionGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Section").ToString)
                    lvItem.SubItems.Add(dtRow.Item("UnitGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Unit").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Team").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GradeGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Grade").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GradeLevel").ToString)
                    lvItem.SubItems.Add(dtRow.Item("JobGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Job").ToString)
                    lvItem.SubItems.Add(dtRow.Item("ClassGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("ClassName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("CostCenter").ToString)
                    lvItem.Tag = dtRow.Item("benefitallocationunkid")
                    lvBenefitAllocation.Items.Add(lvItem)
                    lvItem = Nothing
                Next

                'Pinkal (24-Feb-2017) -- Start
                'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

                'If lvBenefitAllocation.Items.Count > 10 Then
                '    colhBenefitgroup.Width = 110 - 10
                'Else
                '    colhBenefitgroup.Width = 110
                'End If

                If lvBenefitAllocation.Items.Count > 10 Then
                    colhCostCenter.Width = 100 - 10
                Else
                    colhCostCenter.Width = 100
                End If

                'Pinkal (24-Feb-2017) -- End

                lvBenefitAllocation.GroupingColumn = colhBenefitgroup
                lvBenefitAllocation.DisplayGroups(True)

                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                    btnInactive.Visible = True
                ElseIf CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
                    btnActive.Visible = True
                End If
                'Sohail (17 Sep 2019) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        Finally
            Call objbtnSearch.ShowResult(lvBenefitAllocation.Items.Count.ToString)
            'Sohail (17 Sep 2019) -- End
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddBenefitAllocation
            btnEdit.Enabled = User._Object.Privilege._EditBenefitAllocation
            btnDelete.Enabled = User._Object.Privilege._DeleteBenefitAllocation
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            btnActive.Enabled = User._Object.Privilege._AllowToSetBenefitAllocationActive
            btnInactive.Enabled = User._Object.Privilege._AllowToSetBenefitAllocationInactive
            'Sohail (17 Sep 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 4, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End
#End Region

#Region " Form's Events "
    Private Sub frmBenefitallocationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefitAllocation = New clsBenefitAllocation
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            Call SetVisibility()

            Call FillCombo()

            'Call FillList()

            If lvBenefitAllocation.Items.Count > 0 Then lvBenefitAllocation.Items(0).Selected = True
            lvBenefitAllocation.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBenefitallocationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBenefitallocationList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvBenefitAllocation.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmAssetsRegisterList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmAssetsRegisterList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBenefitAllocation = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBenefitAllocation.SetMessages()
            objfrm._Other_ModuleNames = "clsBenefitAllocation"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBenefitAllocation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitAllocation.Select()
            Exit Sub
        End If


        If objBenefitAllocation.isUsed(CInt(lvBenefitAllocation.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Benefit Allocation. Reason: This Benefit Allocation is in use."), enMsgBoxStyle.Information) '?2
            lvBenefitAllocation.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitAllocation.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objBenefitAllocation._Isvoid = True
                'Sandeep [ 16 Oct 2010 ] -- Start
                'objBenefitAllocation._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                'objBenefitAllocation._Voidreason = "Testing"
                Dim frm As New frmReasonSelection
                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBenefitAllocation._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objBenefitAllocation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 
                objBenefitAllocation._Voiduserunkid = User._Object._Userunkid

                objBenefitAllocation.Delete(CInt(lvBenefitAllocation.SelectedItems(0).Tag))
                lvBenefitAllocation.SelectedItems(0).Remove()

                If lvBenefitAllocation.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBenefitAllocation.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBenefitAllocation.Items.Count - 1
                    lvBenefitAllocation.Items(intSelectedIndex).Selected = True
                    lvBenefitAllocation.EnsureVisible(intSelectedIndex)
                ElseIf lvBenefitAllocation.Items.Count <> 0 Then
                    lvBenefitAllocation.Items(intSelectedIndex).Selected = True
                    lvBenefitAllocation.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBenefitAllocation.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvBenefitAllocation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitAllocation.Select()
            Exit Sub
        End If

        'Sohail (17 Sep 2019) -- Start
        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry! You cannot Edit this Trasaction. This Trasaction is Inactive."), enMsgBoxStyle.Information) '?1
            lvBenefitAllocation.Select()
            Exit Sub
        End If
        'Sohail (17 Sep 2019) -- End

        Dim frm As New frmBenefitallocation_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitAllocation.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 
            Dim strSplitTag() As String = lvBenefitAllocation.SelectedItems(0).Tag.ToString.Split(CChar("|"))

            If frm.displayDialog(CInt(strSplitTag(0)), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvBenefitAllocation.Items(intSelectedIndex).Selected = True
            lvBenefitAllocation.EnsureVisible(intSelectedIndex)
            lvBenefitAllocation.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBenefitallocation_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private Sub btnActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActive.Click
        If lvBenefitAllocation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitAllocation.Select()
            Exit Sub
        End If
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
            Exit Sub
        End If
        Dim blnResult As Boolean
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to make Active this Transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            blnResult = objBenefitAllocation.MakeActiveInactive(True, CInt(lvBenefitAllocation.SelectedItems(0).Tag), User._Object._Userunkid, Nothing, "")
            If blnResult = False AndAlso objBenefitAllocation._Message <> "" Then
                eZeeMsgBox.Show(objBenefitAllocation._Message, enMsgBoxStyle.Information)
            Else
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnInactive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInactive.Click
        If lvBenefitAllocation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvBenefitAllocation.Select()
            Exit Sub
        End If

        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            Exit Sub
        End If

        Try
            Dim arrAlloc() As String = {"stationunkid", "departmentunkid", "sectionunkid", "unitunkid", "jobunkid", "gradeunkid", "classunkid" _
                                           , "costcenterunkid", "deptgroupunkid", "sectiongroupunkid", "unitgroupunkid", "teamunkid", "jobgroupunkid" _
                                           , "gradegroupunkid", "gradelevelunkid", "classgroupunkid"}

            Dim intFirstPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            If intFirstPeriodID > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstPeriodID
                Dim ds As DataSet = objBenefitAllocation.GetList("List", objPeriod._End_Date, 0, CInt(lvBenefitAllocation.SelectedItems(0).Tag))

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In ds.Tables(0).Rows
                        Dim strFilter As String = " 1 = 1 "

                        For Each col As DataColumn In ds.Tables(0).Columns
                            If arrAlloc.Contains(col.ColumnName.ToLower) = False Then Continue For

                            If CInt(dRow.Item(col.ColumnName)) > 0 Then
                                strFilter &= " AND ADF." & col.ColumnName & " = " & CInt(dRow.Item(col.ColumnName)) & " "
                            End If
                        Next

                        Dim objEmp As New clsEmployee_Master
                        Dim dsEmployee As DataSet = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                         , Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                         , True, True, "Lost", False, strAdvanceFilterQuery:=strFilter)
                        If dsEmployee.Tables(0).Rows.Count > 0 Then
                            Dim strIDs As String = String.Join(",", (From p In dsEmployee.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                            Dim objTnA As New clsTnALeaveTran
                            If objTnA.IsPayrollProcessDone(intFirstPeriodID, strIDs, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Process Payroll is already done for last date of current Period. Please Void Process Payroll first to continue.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 8, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Dim objFrm As New frmProcessPayroll
                                    objFrm.ShowDialog()
                                End If
                                Exit Sub
                            End If
                        End If
                    Next
                End If
            End If

          

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBenefitAllocation.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to make Inactive this Transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                objBenefitAllocation.MakeActiveInactive(False, CInt(lvBenefitAllocation.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)

                lvBenefitAllocation.SelectedItems(0).Remove()

                If lvBenefitAllocation.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBenefitAllocation.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBenefitAllocation.Items.Count - 1
                    lvBenefitAllocation.Items(intSelectedIndex).Selected = True
                    lvBenefitAllocation.EnsureVisible(intSelectedIndex)
                ElseIf lvBenefitAllocation.Items.Count <> 0 Then
                    lvBenefitAllocation.Items(intSelectedIndex).Selected = True
                    lvBenefitAllocation.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBenefitAllocation.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnInactive_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
            'cboClass.SelectedValue = 0
            'cboCostcenter.SelectedValue = 0
            'cboDepartment.SelectedValue = 0
            'cboGrade.SelectedValue = 0
            'cboJob.SelectedValue = 0
            'cboSections.SelectedValue = 0
            'cboBranch.SelectedValue = 0
            'cboUnit.SelectedValue = 0

            cboBranch.SelectedValue = 0
            cboDepartmentGrp.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboSectionGroup.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnit.SelectedValue = 0
            cboUnitGroup.SelectedValue = 0
            cboTeams.SelectedValue = 0
            cboGradeGroup.SelectedValue = 0
            cboGrade.SelectedValue = 0
            cboGradeLevel.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboCostcenter.SelectedValue = 0

            'Pinkal (24-Feb-2017) -- End


            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
#End Region


#Region "Combobox Event"

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGrade As New clsGrade
        Try
            dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
            End With
            Call SetDefaultSearchText(cboGrade) 'Hemant (22 June 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeLevel")
            End With
            Call SetDefaultSearchText(cboGradeLevel) 'Hemant (22 June 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (22 June 2019) -- Start
    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
    Private Sub cboCostcenter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCostcenter.KeyPress, _
                                                                                                   cboBranch.KeyPress, _
                                                                                                   cboDepartmentGrp.KeyPress, _
                                                                                                   cboDepartment.KeyPress, _
                                                                                                   cboSectionGroup.KeyPress, _
                                                                                                   cboSections.KeyPress, _
                                                                                                   cboUnitGroup.KeyPress, _
                                                                                                   cboUnit.KeyPress, _
                                                                                                   cboTeams.KeyPress, _
                                                                                                   cboGradeGroup.KeyPress, _
                                                                                                   cboGrade.KeyPress, _
                                                                                                   cboGradeLevel.KeyPress, _
                                                                                                   cboJobGroup.KeyPress, _
                                                                                                   cboJob.KeyPress, _
                                                                                                   cboClassGroup.KeyPress, _
                                                                                                   cboClass.KeyPress

        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboCostcenter.Name Then
                        .CodeMember = "costcentercode"
                    ElseIf cbo.Name = cboBranch.Name OrElse cbo.Name = cboDepartmentGrp.Name OrElse cbo.Name = cboDepartment.Name OrElse _
                           cbo.Name = cboSectionGroup.Name OrElse cbo.Name = cboSections.Name OrElse cbo.Name = cboUnitGroup.Name OrElse _
                           cbo.Name = cboUnit.Name OrElse cbo.Name = cboTeams.Name OrElse cbo.Name = cboGradeGroup.Name OrElse _
                           cbo.Name = cboGrade.Name OrElse cbo.Name = cboGradeLevel.Name OrElse cbo.Name = cboJobGroup.Name OrElse _
                           cbo.Name = cboJob.Name Then
                    Else

                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.SelectedIndexChanged, _
                                                                                                   cboBranch.SelectedIndexChanged, _
                                                                                                   cboDepartmentGrp.SelectedIndexChanged, _
                                                                                                   cboDepartment.SelectedIndexChanged, _
                                                                                                   cboSectionGroup.SelectedIndexChanged, _
                                                                                                   cboSections.SelectedIndexChanged, _
                                                                                                   cboUnitGroup.SelectedIndexChanged, _
                                                                                                   cboUnit.SelectedIndexChanged, _
                                                                                                   cboTeams.SelectedIndexChanged, _
                                                                                                   cboGradeGroup.SelectedIndexChanged, _
                                                                                                   cboGrade.SelectedIndexChanged, _
                                                                                                   cboGradeLevel.SelectedIndexChanged, _
                                                                                                   cboJobGroup.SelectedIndexChanged, _
                                                                                                   cboJob.SelectedIndexChanged, _
                                                                                                   cboClassGroup.SelectedIndexChanged, _
                                                                                                   cboClass.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.GotFocus, _
                                                                                                   cboBranch.GotFocus, _
                                                                                                   cboDepartmentGrp.GotFocus, _
                                                                                                   cboDepartment.GotFocus, _
                                                                                                   cboSectionGroup.GotFocus, _
                                                                                                   cboSections.GotFocus, _
                                                                                                   cboUnitGroup.GotFocus, _
                                                                                                   cboUnit.GotFocus, _
                                                                                                   cboTeams.GotFocus, _
                                                                                                   cboGradeGroup.GotFocus, _
                                                                                                   cboGrade.GotFocus, _
                                                                                                   cboGradeLevel.GotFocus, _
                                                                                                   cboJobGroup.GotFocus, _
                                                                                                   cboJob.GotFocus, _
                                                                                                   cboClassGroup.GotFocus, _
                                                                                                   cboClass.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCostcenter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCostcenter.Leave, _
                                                                                                   cboBranch.Leave, _
                                                                                                   cboDepartmentGrp.Leave, _
                                                                                                   cboDepartment.Leave, _
                                                                                                   cboSectionGroup.Leave, _
                                                                                                   cboSections.Leave, _
                                                                                                   cboUnitGroup.Leave, _
                                                                                                   cboUnit.Leave, _
                                                                                                   cboTeams.Leave, _
                                                                                                   cboGradeGroup.Leave, _
                                                                                                   cboGrade.Leave, _
                                                                                                   cboGradeLevel.Leave, _
                                                                                                   cboJobGroup.Leave, _
                                                                                                   cboJob.Leave, _
                                                                                                   cboClassGroup.Leave, _
                                                                                                   cboClass.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 June 2019) -- End

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
            Else
                mdtPeriodStartDate = Nothing
                mdtPeriodEndDate = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboActiveInactive_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActiveInactive.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActiveInactive_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnActive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnActive.GradientForeColor = GUI._ButttonFontColor

			Me.btnInactive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInactive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.colhBranch.Text = Language._Object.getCaption(CStr(Me.colhBranch.Tag), Me.colhBranch.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhUnit.Text = Language._Object.getCaption(CStr(Me.colhUnit.Tag), Me.colhUnit.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
            Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
            Me.colhBenefitgroup.Text = Language._Object.getCaption(CStr(Me.colhBenefitgroup.Tag), Me.colhBenefitgroup.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.colhDeptGrp.Text = Language._Object.getCaption(CStr(Me.colhDeptGrp.Tag), Me.colhDeptGrp.Text)
            Me.colhSectionGrp.Text = Language._Object.getCaption(CStr(Me.colhSectionGrp.Tag), Me.colhSectionGrp.Text)
            Me.colhUnitGrp.Text = Language._Object.getCaption(CStr(Me.colhUnitGrp.Tag), Me.colhUnitGrp.Text)
            Me.colhTeam.Text = Language._Object.getCaption(CStr(Me.colhTeam.Tag), Me.colhTeam.Text)
            Me.colhGradeGrp.Text = Language._Object.getCaption(CStr(Me.colhGradeGrp.Tag), Me.colhGradeGrp.Text)
            Me.colhGradeLevel.Text = Language._Object.getCaption(CStr(Me.colhGradeLevel.Tag), Me.colhGradeLevel.Text)
            Me.colhJobGrp.Text = Language._Object.getCaption(CStr(Me.colhJobGrp.Tag), Me.colhJobGrp.Text)
            Me.colhClassGrp.Text = Language._Object.getCaption(CStr(Me.colhClassGrp.Tag), Me.colhClassGrp.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.lblActiveInactive.Text = Language._Object.getCaption(Me.lblActiveInactive.Name, Me.lblActiveInactive.Text)
			Me.btnActive.Text = Language._Object.getCaption(Me.btnActive.Name, Me.btnActive.Text)
			Me.btnInactive.Text = Language._Object.getCaption(Me.btnInactive.Name, Me.btnInactive.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Benefit Allocation. Reason: This Benefit Allocation is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this transaction ?")
			Language.setMessage(mstrModuleName, 4, "Type to Search")
			Language.setMessage(mstrModuleName, 5, "Are you sure you want to make Active this Transaction?")
			Language.setMessage(mstrModuleName, 6, "Sorry, Process Payroll is already done for last date of current Period. Please Void Process Payroll first to continue.")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to make Inactive this Transaction?")
			Language.setMessage(mstrModuleName, 8, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 9, "Sorry! You cannot Edit this Trasaction. This Trasaction is Inactive.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class