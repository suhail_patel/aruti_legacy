﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmDependantsAndBeneficiariesList

#Region " Private Varaibles "
    Private objDependants_Benefice As clsDependants_Beneficiary_tran
    Dim mblnIsBeneficiaries As Boolean = False
    Private ReadOnly mstrModuleName As String = "frmDependantsAndBeneficiariesList"
    Private mintSelectedCountry As Integer = 0
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objADependantTran As clsDependant_beneficiaries_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    Private intParentRowIndex As Integer = -1
    'Gajanan [22-Feb-2019] -- End



    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [17-April-2019] -- End

#End Region

#Region " Properties "
    Public Property _IsBeneficiaries() As Boolean
        Get
            Return mblnIsBeneficiaries
        End Get
        Set(ByVal value As Boolean)
            mblnIsBeneficiaries = value
        End Set
    End Property

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#End Region

#Region " Private Functions "
    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty

        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim StrApproverSearching As String = String.Empty
        'Gajanan [22-Feb-2019] -- End

        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewEmpDependantsList = True Then               'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objDependants_Benefice.GetList("List")

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                ''If CInt(cboDBPostCountry.SelectedValue) > 0 Then
                ''    StrSearching &= "AND CountryId = " & CInt(cboDBPostCountry.SelectedValue) & " "
                ''End If

                'If CInt(cboDBRelation.SelectedValue) > 0 Then
                '    StrSearching &= "AND RelationId = " & CInt(cboDBRelation.SelectedValue) & " "
                'End If

                ''If CInt(cboMedicalNo.SelectedValue) > 0 Then
                ''    StrSearching &= "AND DpndtId = " & CInt(cboMedicalNo.SelectedValue) & " "
                ''End If

                ''If CInt(cboBenefitPlan.SelectedValue) > 0 Then
                ''    StrSearching &= "AND DpndtId = " & CInt(cboBenefitPlan.SelectedValue) & " "
                ''End If

                'If txtDBFirstName.Text.Trim <> "" Then
                '    StrSearching &= "AND FName LIKE '%" & txtDBFirstName.Text & "%'" & " "
                'End If

                'If txtDBLastName.Text.Trim <> "" Then
                '    StrSearching &= "AND LName LIKE '%" & txtDBLastName.Text & "%'" & " "
                'End If

                'If txtDBPassportNo.Text.Trim <> "" Then
                '    StrSearching &= "AND IdNo LIKE '%" & txtDBPassportNo.Text & "%'" & " "
                'End If

                'If CInt(cboGender.SelectedValue) > 0 Then
                '    StrSearching &= "AND GenderId = '" & CInt(cboGender.SelectedValue) & "' "
                'End If


                ''Anjan (24 Jan 2012)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If dtBirthdate.Checked = True Then
                '    StrSearching &= "AND birthdate = '" & eZeeDate.convertDate(dtBirthdate.Value) & "' "
                'End If
                ''Anjan (24 Jan 2012)-End 


                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 

                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("List"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("List"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboDBRelation.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboDBRelation.SelectedValue) & " "
                End If

                If txtDBFirstName.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE '%" & txtDBFirstName.Text & "%'" & " "
                End If

                If txtDBLastName.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE '%" & txtDBLastName.Text & "%'" & " "
                End If

                If txtDBPassportNo.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.identify_no,'') LIKE '%" & txtDBPassportNo.Text & "%'" & " "
                End If

                If CInt(cboGender.SelectedValue) > 0 Then
                    StrSearching &= "AND hrdependants_beneficiaries_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
                End If

                If dtBirthdate.Checked = True Then
                    StrSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtBirthdate.Value) & "' "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If DependantApprovalFlowVal Is Nothing Then
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        StrApproverSearching &= "AND hrdependant_beneficiaries_approval_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                    End If

                    If CInt(cboDBRelation.SelectedValue) > 0 Then
                        StrApproverSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboDBRelation.SelectedValue) & " "
                    End If

                    If txtDBFirstName.Text.Trim <> "" Then
                        StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.first_name,'') LIKE '%" & txtDBFirstName.Text & "%'" & " "
                    End If

                    If txtDBLastName.Text.Trim <> "" Then
                        StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.last_name,'') LIKE '%" & txtDBLastName.Text & "%'" & " "
                    End If

                    If txtDBPassportNo.Text.Trim <> "" Then
                        StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.identify_no,'') LIKE '%" & txtDBPassportNo.Text & "%'" & " "
                    End If

                    If CInt(cboGender.SelectedValue) > 0 Then
                        StrApproverSearching &= "AND hrdependant_beneficiaries_approval_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
                    End If

                    If dtBirthdate.Checked = True Then
                        StrApproverSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependant_beneficiaries_approval_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtBirthdate.Value) & "' "
                    End If

                    If mstrAdvanceFilter.Length > 0 Then
                        StrApproverSearching &= "AND " & mstrAdvanceFilter
                    End If

                    If StrSearching.Length > 0 Then
                        StrApproverSearching = StrApproverSearching.Substring(3)
                    End If
                End If
                'Gajanan [22-Feb-2019] -- End


               


                'Sandeep (14-Sep-2018) -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'dsList = objDependants_Benefice.GetList(FinancialYear._Object._DatabaseName, _
                '                                        User._Object._Userunkid, _
                '                                        FinancialYear._Object._YearUnkid, _
                '                                        Company._Object._Companyunkid, _
                '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                        ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , StrSearching)

                dsList = objDependants_Benefice.GetList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                        ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                        ConfigParameter._Object._IsIncludeInactiveEmp, "List", , CInt(cboEmployee.SelectedValue), StrSearching, , mblnAddApprovalCondition, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Sohail (18 May 2019) - [CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]
                'Sandeep (14-Sep-2018) -- End

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)

                If DependantApprovalFlowVal Is Nothing AndAlso mintTransactionId <= 0 Then
                    Dim dsPending As New DataSet
                    dsPending = objADependantTran.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , , StrApproverSearching, , mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsList.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [22-Feb-2019] -- End


                dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable


                lvDepandants_Benefice.Items.Clear()

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvDepandants_Benefice.BeginUpdate()
                'S.SANDEEP [04 JUN 2015] -- END
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.SubItems.Add(dtRow.Item("DpndtBefName").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Relation").ToString)
                    'lvItem.SubItems.Add(dtRow.Item("DpndtBenfData").ToString)

                    'Anjan (24 Jan 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS - need to replace country column with birthdate.
                    'RemoveHandler cboDBPostCountry.SelectedValueChanged, AddressOf cboDBPostCountry_SelectedValueChanged
                    'cboDBPostCountry.SelectedValue = CInt(dtRow.Item("CountryId").ToString)
                    'lvItem.SubItems.Add(cboDBPostCountry.Text.ToString)
                    'AddHandler cboDBPostCountry.SelectedValueChanged, AddressOf cboDBPostCountry_SelectedValueChanged
                    If dtRow.Item("birthdate").ToString.Trim = "" Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToShortDateString)
                    End If

                    'Anjan (24 Jan 2012)-End 


                    lvItem.SubItems.Add(dtRow.Item("IdNo").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Gender").ToString)
                    lvItem.Tag = dtRow.Item("DpndtTranId")


                    'Pinkal (01-Apr-2013) -- Start
                    'Enhancement : TRA Changes
                    lvItem.SubItems.Add(dtRow.Item("EmpId").ToString())
                    'Pinkal (01-Apr-2013) -- End



                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    lvItem.SubItems.Add(dtRow.Item("tranguid").ToString)
                    lvItem.SubItems(objdgcolhtranguid.Index).Tag = dtRow.Item("operationtypeid").ToString()
                    'lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)
                    If dtRow.Item("tranguid").ToString.Length > 0 Then
                        lvItem.BackColor = Color.PowderBlue
                        lvItem.ForeColor = Color.Black
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                        objtblPanel.Visible = True
                        btnApprovalinfo.Visible = False
                    End If
                    lvItem.SubItems.Add(dtRow("OperationType").ToString())
                    'Gajanan [22-Feb-2019] -- End
                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    lvItem.SubItems.Add(dtRow("isactive").ToString())
                    'Sohail (18 May 2019) -- End
                    lvDepandants_Benefice.Items.Add(lvItem)
                    lvItem = Nothing
                Next


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                colhOperationType.DisplayIndex = 0
                'Gajanan [22-Feb-2019] -- End   


                'If mblnIsBeneficiaries = False Then
                '    colhMembershipName.Text = Language.getMessage(mstrModuleName, 7, "Membership")
                'Else
                '    colhMembershipName.Text = Language.getMessage(mstrModuleName, 8, "Benefit")
                'End If

                cboDBPostCountry.SelectedValue = mintSelectedCountry

                lvDepandants_Benefice.GroupingColumn = colhEmployee
                lvDepandants_Benefice.DisplayGroups(True)


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvDepandants_Benefice.EndUpdate()
                'S.SANDEEP [04 JUN 2015] -- END


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objCountry As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMembership As New clsmembership_master
        Dim objBenefit As New clsbenefitplan_master
        'S.SANDEEP [ 23 JAN 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES 
        Dim objMaster As New clsMasterData
        'S.SANDEEP [ 23 JAN 2012 ] -- END
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If

            'Sandeep (14-Sep-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                          mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

            'Sandeep (14-Sep-2018) -- End


            'S.SANDEEP [04 JUN 2015] -- END
            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objCountry.getCountryList("Country", True)
            With cboDBPostCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("Country").Copy
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            With cboDBRelation
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Relation")
                .SelectedValue = 0
            End With


            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            dsCombos = objMaster.getGenderList("List", True)
            With cboGender
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 23 JAN 2012 ] -- END


            'dsCombos = objServices.getListForCombo(enMedicalMasterType.Service, "Services", True)
            'With cboMedicalNo
            '    .ValueMember = "mdmasterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Services")
            '    .SelectedValue = 0
            'End With

            'dsCombos = objMembership.getListForCombo("Membership", True)
            'With cboMedicalNo
            '    .ValueMember = "membershipunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Membership")
            '    .SelectedValue = 0
            'End With

            'dsCombos = objBenefit.getComboList("Benefit", True)
            'With cboBenefitPlan
            '    .ValueMember = "benefitplanunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Benefit")
            '    .SelectedValue = 0
            'End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddDependant
            btnEdit.Enabled = User._Object.Privilege._EditDependant
            btnDelete.Enabled = User._Object.Privilege._DeleteDependant
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuSetAgeLimit.Visible = ConfigParameter._Object._IsDependant_AgeLimit_Set
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuImportDependants.Enabled = User._Object.Privilege._AllowToImportDependants
            mnuExportDependants.Enabled = User._Object.Privilege._AllowToExportDependants
            'Anjan (25 Oct 2012)-End 

            'S.SANDEEP [ 28 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mnuImportImages.Enabled = ConfigParameter._Object._IsImgInDataBase
            'S.SANDEEP [ 28 MAR 2013 ] -- END

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes
            If mnuImportImages.Enabled = True Then
                mnuImportImages.Enabled = User._Object.Privilege._AllowToImportPhoto
            End If
            'Pinkal (01-Apr-2013) -- End


            'Anjan [27 August 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            mnuDependantsException.Visible = User._Object.Privilege._AllowToAddMedicalDependantException
            'Anjan [27 August 2014] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmDependantsAndBeneficiariesList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvDepandants_Benefice.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmDependantsAndBeneficiariesList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmDependantsAndBeneficiariesList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDependants_Benefice = Nothing
    End Sub

    Private Sub frmDependantsAndBeneficiariesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDependants_Benefice = New clsDependants_Beneficiary_tran
        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objADependantTran = New clsDependant_beneficiaries_approval_tran
        'Gajanan [22-Feb-2019] -- End

        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [17-April-2019] -- End



        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            'If mblnIsBeneficiaries = True Then
            '    Me.Text = Language.getMessage(mstrModuleName, 3, "Beneficiaries Info")
            '    Me.eZeeHeader.Title = Language.getMessage(mstrModuleName, 4, "Beneficiaries Info List")

            '    cboBenefitPlan.Visible = True
            '    lblBenefitPlan.Visible = True

            '    cboMedicalNo.Visible = False
            '    lblDBMedicalMembershipNo.Visible = False
            '    'pnlMedicalInfo.Visible = False
            '    'pnlBenefitInfo.Visible = True
            '    'pnlBenefitInfo.BringToFront()
            'Else
            '    Me.Text = Language.getMessage(mstrModuleName, 5, "Dependants Info")
            '    Me.eZeeHeader.Title = Language.getMessage(mstrModuleName, 6, "Dependants Info List")

            '    cboBenefitPlan.Visible = False
            '    lblBenefitPlan.Visible = False

            '    cboMedicalNo.Visible = True
            '    lblDBMedicalMembershipNo.Visible = True
            '    'pnlMedicalInfo.Visible = True
            '    'pnlBenefitInfo.Visible = False
            '    'pnlMedicalInfo.BringToFront()
            'End If

            Call SetVisibility()

            Call FillCombo()


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objtblPanel.Visible = False
            btnApprovalinfo.Visible = False
            'Gajanan [22-Feb-2019] -- End



            'Anjan (24 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            dtBirthdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtBirthdate.Checked = False
            'Anjan (24 Jan 2012)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END


            'Call FillList()

            If lvDepandants_Benefice.Items.Count > 0 Then lvDepandants_Benefice.Items(0).Selected = True
            lvDepandants_Benefice.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDependantsAndBeneficiariesList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDependants_Beneficiary_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsDependants_Beneficiary_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click


        Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text) & "'")
        If itmp.Length > 0 Then
            isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
        Else
            isEmployeeApprove = False
        End If


        If lvDepandants_Benefice.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDepandants_Benefice.Select()
            Exit Sub
        End If

        Try
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'If DependantApprovalFlowVal Is Nothing Then
            If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                
                If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End


                Dim item As ListViewItem = Nothing
                item = lvDepandants_Benefice.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvDepandants_Benefice.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvDepandants_Benefice.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [22-Feb-2019] -- End




            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDepandants_Benefice.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objDependants_Benefice._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objDependants_Benefice._Voidreason = "Testing"
                Dim frm As New frmReasonSelection

                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objDependants_Benefice._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'objDependants_Benefice._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                objDependants_Benefice._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End

                objDependants_Benefice._Voiduserunkid = User._Object._Userunkid


                'Pinkal (01-Apr-2013) -- Start
                'Enhancement : TRA Changes

                objDependants_Benefice._CompanyId = ConfigParameter._Object._Companyunkid
                objDependants_Benefice._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

                objDependants_Benefice._Employeeunkid = CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text)

                'Pinkal (01-Apr-2013) -- End


                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                Dim objDPStatus As New clsDependant_Benefice_Status_tran
                Dim dsList As DataSet = objDPStatus.GetList("List", CInt(lvDepandants_Benefice.SelectedItems(0).Tag))
                Dim strDpStatusUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("dpndtbeneficestatustranunkid").ToString)).ToArray)
                objDependants_Benefice._IsFromStatus = False
                objDependants_Benefice._DeletedpndtbeneficestatustranIDs = strDpStatusUnkIDs
                'Sohail (18 May 2019) -- End


                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'objDependants_Benefice.Delete(CInt(lvDepandants_Benefice.SelectedItems(0).Tag))



                'objDependants_Benefice.Delete(CInt(lvDepandants_Benefice.SelectedItems(0).Tag), CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text))


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If DependantApprovalFlowVal Is Nothing Then
                    objADependantTran._Isvoid = True
                    objADependantTran._Audituserunkid = User._Object._Userunkid
                    objADependantTran._Isweb = False
                    objADependantTran._Ip = getIP()
                    objADependantTran._Host = getHostName()
                    objADependantTran._Form_Name = mstrModuleName
                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    objADependantTran._DeletedpndtbeneficestatustranIDs = strDpStatusUnkIDs
                    objADependantTran._IsFromStatus = False
                    'Sohail (18 May 2019) -- End

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objADependantTran.Delete(CInt(lvDepandants_Benefice.SelectedItems(0).Tag), mstrVoidReason, Company._Object._Companyunkid) = False Then
                        If objADependantTran._Message <> "" Then
                            eZeeMsgBox.Show(objADependantTran._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                                 enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.DELETED, , lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text, , , _
                                                                 " dpndtbeneficetranunkid = " & CInt(lvDepandants_Benefice.SelectedItems(0).Tag), Nothing, , , _
                                                                 " dpndtbeneficetranunkid = " & CInt(lvDepandants_Benefice.SelectedItems(0).Tag), Nothing)
                        'Gajanan [17-April-2019] -- End

                    End If
                Else
                objDependants_Benefice.Delete(CInt(lvDepandants_Benefice.SelectedItems(0).Tag), CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text))
                End If

                'Gajanan [22-Feb-2019] -- End

                'SHANI (20 JUN 2015) -- End 
                lvDepandants_Benefice.SelectedItems(0).Remove()

                If lvDepandants_Benefice.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvDepandants_Benefice.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvDepandants_Benefice.Items.Count - 1
                    lvDepandants_Benefice.Items(intSelectedIndex).Selected = True
                    lvDepandants_Benefice.EnsureVisible(intSelectedIndex)
                ElseIf lvDepandants_Benefice.Items.Count <> 0 Then
                    lvDepandants_Benefice.Items(intSelectedIndex).Selected = True
                    lvDepandants_Benefice.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvDepandants_Benefice.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvDepandants_Benefice.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDepandants_Benefice.Select()
            Exit Sub
        End If
        Dim frm As New frmDependantsAndBeneficiaries
        Try


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If DependantApprovalFlowVal Is Nothing Then
                Dim item As ListViewItem = Nothing
                item = lvDepandants_Benefice.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvDepandants_Benefice.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvDepandants_Benefice.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [22-Feb-2019] -- End


            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDepandants_Benefice.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            frm._IsBeneficiar = mblnIsBeneficiaries

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvDepandants_Benefice.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(lvDepandants_Benefice.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                'S.SANDEEP [ 14 AUG 2012 ] -- END
                Call FillList()
            End If
            frm = Nothing

            lvDepandants_Benefice.Items(intSelectedIndex).Selected = True
            lvDepandants_Benefice.EnsureVisible(intSelectedIndex)
            lvDepandants_Benefice.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmDependantsAndBeneficiaries
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            frm._IsBeneficiar = mblnIsBeneficiaries

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            'Call FillList()
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                If mintEmployeeUnkid > 0 Then
                    Call FillList()
                End If
                'S.SANDEEP [ 14 AUG 2012 ] -- END
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'cboBenefitPlan.SelectedValue = 0
            cboDBPostCountry.SelectedValue = 0

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END


            cboDBRelation.SelectedValue = 0
            'cboMedicalNo.SelectedValue = 0
            txtDBFirstName.Text = ""
            txtDBLastName.Text = ""
            txtDBPassportNo.Text = ""
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 

            'Anjan (24 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            dtBirthdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtBirthdate.Checked = False
            'Anjan (24 Jan 2012)-End 

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprovalinfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Dim empid As Integer = 0
            If lvDepandants_Benefice.SelectedItems.Count > 0 Then
                empid = CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objcolhEmployeeId.Index).Text)
            End If
            Dim frm As New frmViewEmployeeDataApproval
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeDependants, enScreenName.frmDependantsAndBeneficiariesList, clsEmployeeDataApproval.enOperationType.NONE, "EM.employeeunkid = " & empid, False)
            If frm IsNot Nothing Then frm.Dispose()
            Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDBPostCountry_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDBPostCountry.SelectedValueChanged
        Try
            mintSelectedCountry = CInt(cboDBPostCountry.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuExportDependants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDependants.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Excel files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'strFilePath &= ObjFile.Extension
                strFilePath = dlgSaveFile.FileName
                'S.SANDEEP [12-Jan-2018] -- END

                dsList = objDependants_Benefice.GetDependantsData_Export()

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportDependants_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose() : path = String.Empty : strFilePath = String.Empty : dlgSaveFile = Nothing : ObjFile = Nothing
        End Try
    End Sub

    Private Sub mnuImportDependants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportDependants.Click
        Dim frm As New frmImportDependantWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportDependants_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    Private Sub lnkAllocations_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocations.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub mnuSetAgeLimit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetAgeLimit.Click
        Dim frm As New frmSetDependantAgeLimit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(-1)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSetAgeLimit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'S.SANDEEP [ 28 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuImportToDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportToDatabase.Click
        Dim frm As New frmImageWizard
        Try
            frm.displayDialog(frmImageWizard.enImageMode.IMG_DEPENDANT)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportToDatabase_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGetFileFormat.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath &= ObjFile.Extension
                Dim dTable As New DataTable
                dTable.Columns.Add("Employeecode") : dTable.Columns.Add("Dpndt_Firstname") : dTable.Columns.Add("Dpndt_Lastname") : dTable.Columns.Add("Photo_Path") : dTable.Columns.Add("Photo_Name")
                dsList.Tables.Add(dTable.Copy)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'IExcel.Export(strFilePath, dsList)
                strFilePath = dlgSaveFile.FileName
                OpenXML_Export(strFilePath, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGetFileFormat_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 28 MAR 2013 ] -- END

#End Region


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
#Region " Listview Events "

    Private Sub lvDepandants_Benefice_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvDepandants_Benefice.ItemSelectionChanged
        Try
            If lvDepandants_Benefice.SelectedItems.Count > 0 Then
                If intParentRowIndex <> -1 Then
                    lvDepandants_Benefice.Items(intParentRowIndex).BackColor = Color.White
                    intParentRowIndex = -1
                End If

                If lvDepandants_Benefice.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Text.Length > 0 Then

                    If lvDepandants_Benefice.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag IsNot Nothing Then
                        If CInt(lvDepandants_Benefice.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            Dim item As ListViewItem = lvDepandants_Benefice.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvDepandants_Benefice.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text = "").FirstOrDefault()
                            If item IsNot Nothing Then intParentRowIndex = item.Index
                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            'lvDepandants_Benefice.Items(intParentRowIndex).BackColor = Color.LightCoral
                            If intParentRowIndex >= 0 Then lvDepandants_Benefice.Items(intParentRowIndex).BackColor = Color.LightCoral
                            'Sohail (18 May 2019) -- End
                        End If
                    End If

                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnOperation.Visible = False
                    btnApprovalinfo.Visible = True
                    objtblPanel.Visible = True

                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnOperation.Visible = True
                    btnApprovalinfo.Visible = False
                    objtblPanel.Visible = True

                End If
            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
                btnOperation.Visible = True
                btnApprovalinfo.Visible = False
                objtblPanel.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDepandants_Benefice_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Gajanan [22-Feb-2019] -- End



    Private Sub mnuDependantsException_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDependantsException.Click
        Try
            Dim frm As New frmDependantException
            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDependantsException_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private Sub mnuSetActiveInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetActiveInactive.Click
        Try
            Dim frm As New frmDependantStatus
            'Sohail (09 Jul 2019) -- Start
            'NMB Enhancement - 76.1 - Employee and Dependant should automatically selected on Dependant status screen if is selected on Dependant list screen.
            'frm.ShowDialog()
            'Call FillList()
            Dim intId As Integer = 0
            If lvDepandants_Benefice.SelectedItems.Count > 0 Then
                intId = CInt(lvDepandants_Benefice.SelectedItems(0).Tag)
            End If
            If frm.displayDialog(intId) = True Then
            Call FillList()
            End If
            'Sohail (09 Jul 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSetActiveInactive_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 May 2019) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprovalinfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprovalinfo.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblDBLastName.Text = Language._Object.getCaption(Me.lblDBLastName.Name, Me.lblDBLastName.Text)
            Me.lblDBFirstName.Text = Language._Object.getCaption(Me.lblDBFirstName.Name, Me.lblDBFirstName.Text)
            Me.lblDBRelation.Text = Language._Object.getCaption(Me.lblDBRelation.Name, Me.lblDBRelation.Text)
            Me.lblDBPostCountry.Text = Language._Object.getCaption(Me.lblDBPostCountry.Name, Me.lblDBPostCountry.Text)
            Me.lblDBIdNo.Text = Language._Object.getCaption(Me.lblDBIdNo.Name, Me.lblDBIdNo.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhDBName.Text = Language._Object.getCaption(CStr(Me.colhDBName.Tag), Me.colhDBName.Text)
            Me.colhRelation.Text = Language._Object.getCaption(CStr(Me.colhRelation.Tag), Me.colhRelation.Text)
            Me.colhbirthdate.Text = Language._Object.getCaption(CStr(Me.colhbirthdate.Tag), Me.colhbirthdate.Text)
            Me.colhIdNo.Text = Language._Object.getCaption(CStr(Me.colhIdNo.Tag), Me.colhIdNo.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuImportDependants.Text = Language._Object.getCaption(Me.mnuImportDependants.Name, Me.mnuImportDependants.Text)
            Me.mnuExportDependants.Text = Language._Object.getCaption(Me.mnuExportDependants.Name, Me.mnuExportDependants.Text)
            Me.lnkAllocations.Text = Language._Object.getCaption(Me.lnkAllocations.Name, Me.lnkAllocations.Text)
            Me.mnuSetAgeLimit.Text = Language._Object.getCaption(Me.mnuSetAgeLimit.Name, Me.mnuSetAgeLimit.Text)
            Me.colhGender.Text = Language._Object.getCaption(CStr(Me.colhGender.Tag), Me.colhGender.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.lblBirthdate.Text = Language._Object.getCaption(Me.lblBirthdate.Name, Me.lblBirthdate.Text)
            Me.mnuImportImages.Text = Language._Object.getCaption(Me.mnuImportImages.Name, Me.mnuImportImages.Text)
            Me.mnuImportToDatabase.Text = Language._Object.getCaption(Me.mnuImportToDatabase.Name, Me.mnuImportToDatabase.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuDependantsException.Text = Language._Object.getCaption(Me.mnuDependantsException.Name, Me.mnuDependantsException.Text)
			Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.Name, Me.lblParentData.Text)
			Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)
			Me.colhOperationType.Text = Language._Object.getCaption(CStr(Me.colhOperationType.Tag), Me.colhOperationType.Text)
			Me.btnApprovalinfo.Text = Language._Object.getCaption(Me.btnApprovalinfo.Name, Me.btnApprovalinfo.Text)
			Me.colhIsActive.Text = Language._Object.getCaption(CStr(Me.colhIsActive.Tag), Me.colhIsActive.Text)
			Me.ToolStripMenuItem1.Text = Language._Object.getCaption(Me.ToolStripMenuItem1.Name, Me.ToolStripMenuItem1.Text)
			Me.mnuSetActiveInactive.Text = Language._Object.getCaption(Me.mnuSetActiveInactive.Name, Me.mnuSetActiveInactive.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
            Language.setMessage(mstrModuleName, 3, "File exported successfully.")
            Language.setMessage(mstrModuleName, 4, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class