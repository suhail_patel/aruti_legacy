﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportDependantWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportDependantWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)



    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    Private objApprovalData As clsEmployeeDataApproval
    Private objADependantBeneficiaryTran As clsDependant_beneficiaries_approval_tran

#End Region

#Region " Form's Events "

    Private Sub frmImportDependantWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objADependantBeneficiaryTran = New clsDependant_beneficiaries_approval_tran
            objApprovalData = New clsEmployeeDataApproval
            'Gajanan [17-April-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportDependantWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()

                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    Case cboBIRTHDATE.Name.ToUpper, _
                                         cboCountry.Name.ToUpper, _
                                         cboState.Name.ToUpper, _
                                         cboPincode.Name.ToUpper, _
                                         cboEMPLOYEE_FIRSTNAME.Name.ToUpper, _
                                         cboEMPLOYEE_SURNAME.Name.ToUpper, _
                                         cboADDRESS.Name.ToUpper, _
                                         cboMOBILENO.Name.ToUpper, _
                                         cboNationality.Name.ToUpper, _
                                         cboID_NO.Name.ToUpper, _
                                         cboGENDER.Name.ToUpper, _
                                         cboTELEPHONE.Name.ToUpper, _
                                         cboCity.Name.ToUpper, _
                                         cboDEPENDANT_MIDDLENAME.Name.ToUpper
                                        '"CBOBIRTHDATE", "CBOCOUNTRY", "CBOSTATE", "CBOCITY", "CBOPINCODE", "CBOEMPFIRSTNAME", _
                                        '"CBOEMPSURNAME", "CBOADDRESS", "CBOTELEPHONE", "CBOMOBILE", "CBONATIONALITY", "CBOIDNO", "CBOGENDER"
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data to."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboADDRESS.Items.Add(dtColumns.ColumnName)
                cboISBENEFICIARY.Items.Add(dtColumns.ColumnName)
                cboBIRTHDATE.Items.Add(dtColumns.ColumnName)
                cboCity.Items.Add(dtColumns.ColumnName)
                cboCountry.Items.Add(dtColumns.ColumnName)
                cboEMPLOYEE_FIRSTNAME.Items.Add(dtColumns.ColumnName)
                cboEMPLOYEE_CODE.Items.Add(dtColumns.ColumnName)
                cboEMPLOYEE_SURNAME.Items.Add(dtColumns.ColumnName)
                cboDEPENDANT_FIRSTNAME.Items.Add(dtColumns.ColumnName)
                cboID_NO.Items.Add(dtColumns.ColumnName)
                cboMOBILENO.Items.Add(dtColumns.ColumnName)
                cboNationality.Items.Add(dtColumns.ColumnName)
                cboPincode.Items.Add(dtColumns.ColumnName)
                cboRELATION.Items.Add(dtColumns.ColumnName)
                cboState.Items.Add(dtColumns.ColumnName)
                cboDEPENDANT_LASTNAME.Items.Add(dtColumns.ColumnName)
                cboTELEPHONE.Items.Add(dtColumns.ColumnName)

                'Shani (19-Oct-2016) -- Start
                'Enhancement - Add Gender In Depandent Importation Screen
                cboGENDER.Items.Add(dtColumns.ColumnName)
                'Shani (19-Oct-2016) -- End


                'Pinkal (20-Jul-2018) -- Start
                'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.
                cboDEPENDANT_MIDDLENAME.Items.Add(dtColumns.ColumnName)
                'Pinkal (20-Jul-2018) -- End

                'Gajanan [27-May-2019] -- Start              
                cboEffective_Date.Items.Add(dtColumns.ColumnName)
                'Gajanan [27-May-2019] -- End
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True


            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))


            'Gajanan [27-May-2019] -- Start              
            'mdt_ImportData_Others.Columns.Add("Dpndtfirstname", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Dpndtlastname", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("relation", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("isbeneficiary", System.Type.GetType("System.Boolean"))
            'mdt_ImportData_Others.Columns.Add("birthdate", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Country", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("State", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("City", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Zipcode", System.Type.GetType("System.String"))


            mdt_ImportData_Others.Columns.Add("Dpndtfirstname", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Company")
            mdt_ImportData_Others.Columns.Add("Dpndtlastname", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Dpndtlastname")
            mdt_ImportData_Others.Columns.Add("relation", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "relation")
            mdt_ImportData_Others.Columns.Add("isbeneficiary", System.Type.GetType("System.Boolean")).ExtendedProperties.Add("col", "isbeneficiary")
            mdt_ImportData_Others.Columns.Add("birthdate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "birthdate")
            mdt_ImportData_Others.Columns.Add("Country", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Country")
            mdt_ImportData_Others.Columns.Add("State", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "State")
            mdt_ImportData_Others.Columns.Add("City", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "City")
            mdt_ImportData_Others.Columns.Add("Zipcode", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Zipcode")

            'Gajanan [27-May-2019] -- End

            mdt_ImportData_Others.Columns.Add("Empfirstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("EmpLastname", System.Type.GetType("System.String"))



            'Gajanan [27-May-2019] -- Start              


            'mdt_ImportData_Others.Columns.Add("Address", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Telephone", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Mobile", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Nationality", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("IdNo", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("Address", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Address")
            mdt_ImportData_Others.Columns.Add("Telephone", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Telephone")
            mdt_ImportData_Others.Columns.Add("Mobile", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Mobile")
            mdt_ImportData_Others.Columns.Add("Nationality", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Nationality")
            mdt_ImportData_Others.Columns.Add("IdNo", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "IdNo")

            'Gajanan [27-May-2019] -- End


            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("CName", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'Shani (19-Oct-2016) -- Start
            'Enhancement - Add Gender In Depandent Importation Screen
            mdt_ImportData_Others.Columns.Add("Gender", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Gender")
            'Shani (19-Oct-2016) -- End

            'Pinkal (20-Jul-2018) -- Start
            'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.
            mdt_ImportData_Others.Columns.Add("Dpndtmiddlename", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Dpndtmiddlename")

            'Gajanan [27-May-2019] -- Start              
            'Gajanan [27-May-2019] -- End

            'Pinkal (20-Jul-2018) -- End

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("EffectiveDate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "EffectiveDate")

            mdt_ImportData_Others.Columns("Dpndtfirstname").SetOrdinal(1)
            mdt_ImportData_Others.Columns("Dpndtmiddlename").SetOrdinal(2)
            mdt_ImportData_Others.Columns("Dpndtlastname").SetOrdinal(3)



            'Gajanan [27-May-2019] -- End





            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEMPLOYEE_CODE.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows

                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboEMPLOYEE_CODE.Text).ToString
                drNewRow.Item("Dpndtfirstname") = dtRow.Item(cboDEPENDANT_FIRSTNAME.Text).ToString()
                drNewRow.Item("Dpndtlastname") = dtRow.Item(cboDEPENDANT_LASTNAME.Text).ToString()


                'Pinkal (20-Jul-2018) -- Start
                'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.
                If cboDEPENDANT_MIDDLENAME.Text.Trim.Length > 0 Then
                drNewRow.Item("Dpndtmiddlename") = dtRow.Item(cboDEPENDANT_MIDDLENAME.Text).ToString()
                End If
                'Pinkal (20-Jul-2018) -- End


                If cboRELATION.Text.Trim.Length > 0 Then
                drNewRow.Item("relation") = dtRow.Item(cboRELATION.Text).ToString
                End If

                If cboISBENEFICIARY.Text.Trim.Length > 0 Then
                    Dim blnFlag As Boolean = False
                    Boolean.TryParse(dtRow.Item(cboISBENEFICIARY.Text).ToString, blnFlag)
                    'drNewRow.Item("isbeneficiary") = dtRow.Item(cboISBENEFICIARY.Text).ToString
                    drNewRow.Item("isbeneficiary") = blnFlag
                    blnFlag = Nothing
                End If

                'Pinkal (20-Jul-2018) -- Start
                'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.
                If cboDEPENDANT_MIDDLENAME.Text.Trim.Length > 0 Then
                    drNewRow.Item("CName") = drNewRow.Item("Dpndtfirstname").ToString & " " & drNewRow.Item("Dpndtmiddlename") & " " & drNewRow.Item("Dpndtlastname").ToString()
                Else
                    drNewRow.Item("CName") = drNewRow.Item("Dpndtfirstname").ToString & " " & drNewRow.Item("Dpndtlastname").ToString()
                End If
                'Pinkal (20-Jul-2018) -- End

                If cboBIRTHDATE.Text.Trim.Length > 0 Then
                    drNewRow.Item("birthdate") = dtRow.Item(cboBIRTHDATE.Text).ToString.Trim
                Else
                    drNewRow.Item("birthdate") = Nothing
                End If

                If cboCountry.Text.Trim.Length > 0 Then
                    drNewRow.Item("Country") = dtRow.Item(cboCountry.Text).ToString.Trim
                Else
                    drNewRow.Item("Country") = ""
                End If

                If cboState.Text.Trim.Length > 0 Then
                    drNewRow.Item("State") = dtRow.Item(cboState.Text).ToString.Trim
                Else
                    drNewRow.Item("State") = ""
                End If

                If cboCity.Text.Trim.Length > 0 Then
                    drNewRow.Item("City") = dtRow.Item(cboCity.Text).ToString.Trim
                Else
                    drNewRow.Item("City") = ""
                End If

                If cboPincode.Text.Trim.Length > 0 Then
                    drNewRow.Item("Zipcode") = dtRow.Item(cboPincode.Text).ToString.Trim
                Else
                    drNewRow.Item("Zipcode") = ""
                End If

                If cboEMPLOYEE_FIRSTNAME.Text.Trim.Length > 0 Then
                    drNewRow.Item("Empfirstname") = dtRow.Item(cboEMPLOYEE_FIRSTNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Empfirstname") = ""
                End If

                If cboEMPLOYEE_SURNAME.Text.Trim.Length > 0 Then
                    drNewRow.Item("EmpLastname") = dtRow.Item(cboEMPLOYEE_SURNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("EmpLastname") = ""
                End If

                If cboADDRESS.Text.Trim.Length > 0 Then
                    drNewRow.Item("Address") = dtRow.Item(cboADDRESS.Text).ToString.Trim
                Else
                    drNewRow.Item("Address") = ""
                End If

                If cboTELEPHONE.Text.Trim.Length > 0 Then
                    drNewRow.Item("Telephone") = dtRow.Item(cboTELEPHONE.Text).ToString.Trim
                Else
                    drNewRow.Item("Telephone") = ""
                End If

                If cboMOBILENO.Text.Trim.Length > 0 Then
                    drNewRow.Item("Mobile") = dtRow.Item(cboMOBILENO.Text).ToString.Trim
                Else
                    drNewRow.Item("Mobile") = ""
                End If

                If cboNationality.Text.Trim.Length > 0 Then
                    drNewRow.Item("Nationality") = dtRow.Item(cboNationality.Text).ToString.Trim
                Else
                    drNewRow.Item("Nationality") = ""
                End If

                If cboID_NO.Text.Trim.Length > 0 Then
                    drNewRow.Item("IdNo") = dtRow.Item(cboID_NO.Text).ToString.Trim
                Else
                    drNewRow.Item("IdNo") = ""
                End If

                'Shani (19-Oct-2016) -- Start
                'Enhancement - Add Gender In Depandent Importation Screen
                If cboGENDER.Text.Trim.Length > 0 Then
                    drNewRow.Item("Gender") = dtRow.Item(cboGENDER.Text).ToString.Trim
                Else
                    drNewRow.Item("Gender") = ""
                End If
                'Shani (19-Oct-2016) -- End


                If cboEffective_Date.Text.Trim.Length > 0 Then
                    drNewRow.Item("EffectiveDate") = dtRow.Item(cboEffective_Date.Text).ToString()
                Else
                    drNewRow.Item("EffectiveDate") = DateAndTime.Today.Date.ToString()
                End If


                'Pinkal (20-Jul-2018) -- Start
                'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.


                'Pinkal (20-Jul-2018) -- End


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "CName"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"


            'Gajanan [27-May-2019] -- Start              
            'Catch ex As System.ArgumentException
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please check the selected file column 'isbeneficary' should only be in 'True' Or 'False' Format. Please modify it and try to import again."), enMsgBoxStyle.Information)
            'Gajanan [27-May-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEMPLOYEE_CODE.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Dependant(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboDEPENDANT_FIRSTNAME.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Dependant Firstname cannot be blank. Please set Dependant Firstname in order to import Dependant(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboDEPENDANT_LASTNAME.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Dependant Lastname cannot be blan. Please set Dependant Lastname in order to import Dependant(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboRELATION.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Relation cannot be blank. Please set Relation in order to import Dependant(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboISBENEFICIARY.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Beneficiary cannot be blank. Please set Beneficary in order to import Dependant(s)."), enMsgBoxStyle.Information)
                    Return False
                End If


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'S.SANDEEP [13-AUG-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                If cboGENDER.Text.Trim.Length > 0 Then
                If .Item(cboGENDER.Text).ToString.Trim.Length > 0 Then
                    Select Case .Item(cboGENDER.Text).ToString.ToUpper
                        Case "MALE", "FEMALE", "M", "F"
                        Case Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Gender data in file is not vaild."), enMsgBoxStyle.Information)
                            Return False
                    End Select
                End If
                End If
                'S.SANDEEP [13-AUG-2018] -- END
                'Gajanan [17-DEC-2018] -- End
            End With

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objCMaster As New clsCommon_Master
            Dim objCountry As New clsMasterData
            Dim objState As New clsstate_master
            Dim objCity As New clscity_master
            Dim objPincode As New clszipcode_master
            Dim objEMaster As New clsEmployee_Master

            Dim objDependantBeneficiaryTran As clsDependants_Beneficiary_tran


            'Gajanan [21-June-2019] -- Start      

            'Dim intRelationUnkid As Integer = -1
            'Dim intCountryId As Integer = -1
            'Dim intStateId As Integer = -1
            'Dim intCityId As Integer = -1
            'Dim intPincodeId As Integer = -1
            'Dim intNationality As Integer = -1

            Dim intRelationUnkid As Integer = 0
            Dim intCountryId As Integer = 0
            Dim intStateId As Integer = 0
            Dim intCityId As Integer = 0
            Dim intPincodeId As Integer = 0
            Dim intNationality As Integer = 0
            'Gajanan [21-June-2019] -- End

            Dim intEmployeeUnkid As Integer = -1
            Dim intDependantBeneficiaryUnkid As Integer = -1

            'Shani (19-Oct-2016) -- Start
            'Enhancement - Add Gender In Depandent Importation Screen
            Dim intGender As Integer = -1
            'Shani (19-Oct-2016) -- End

            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim isEmployeeApprove As Boolean = False
                isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                'Gajanan [17-April-2019] -- End

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                        'Gajanan [27-May-2019] -- Start
                    Else
                        dtRow.Item("EmployeeId") = intEmployeeUnkid
                        'Gajanan [27-May-2019] -- End
                    End If
                End If

                '------------------------- RELATION
                If dtRow.Item("relation").ToString.Trim.Length > 0 Then
                    intRelationUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.RELATIONS, dtRow.Item("relation").ToString.Trim)
                    If intRelationUnkid <= 0 Then
                        If Not objCMaster Is Nothing Then objCMaster = Nothing
                        objCMaster = New clsCommon_Master

                        objCMaster._Code = dtRow.Item("relation").ToString
                        objCMaster._Name = dtRow.Item("relation").ToString.Trim
                        objCMaster._Isactive = True
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.RELATIONS

                        If objCMaster.Insert() Then
                            intRelationUnkid = objCMaster._Masterunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Code & "/" & objCMaster._Name & ":" & objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                'S.SANDEEP [ 04 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                '------------------------- COUNTRY
                'If dtRow.Item("Country").ToString.Trim.Length > 0 Then
                '    intCountryId = objCountry.GetCountryUnkId(dtRow.Item("country").ToString.Trim)
                '    If intCountryId <= 0 Then
                '        If Not objCountry Is Nothing Then objCountry = Nothing
                '        objCountry = New clsMasterData
                '        intCountryId = objCountry.InsertCountry(dtRow.Item("country").ToString.Trim)
                '    End If
                'End If

                ''------------------------- STATE
                'If dtRow.Item("State").ToString.Trim.Length > 0 Then
                '    intStateId = objState.GetStateUnkId(dtRow.Item("State").ToString.Trim)
                '    If intStateId <= 0 Then
                '        If Not objState Is Nothing Then objState = Nothing
                '        objState = New clsstate_master
                '        objState._Code = dtRow.Item("State").ToString.Trim
                '        objState._Countryunkid = intCountryId
                '        objState._Isactive = True
                '        objState._Name = dtRow.Item("State").ToString.Trim
                '        If objState.Insert Then
                '            intStateId = objState._Stateunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objState._Code & "/" & objState._Name & ":" & objState._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '        End If
                '    End If
                'End If

                ''------------------------- CITY
                'If dtRow.Item("City").ToString.Trim.Length > 0 Then
                '    intCityId = objCity.GetCityUnkId(dtRow.Item("City").ToString.Trim)
                '    If intCityId <= 0 Then
                '        If Not objCity Is Nothing Then objCity = Nothing
                '        objCity = New clscity_master
                '        objCity._Code = dtRow.Item("city").ToString.Trim
                '        objCity._Countryunkid = intCountryId
                '        objCity._Isactive = True
                '        objCity._Name = dtRow.Item("city").ToString.Trim
                '        objCity._Stateunkid = intStateId
                '        If objCity.Insert Then
                '            intCityId = objCity._Cityunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objCity._Code & "/" & objCity._Name & ":" & objCity._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '        End If
                '    End If
                'End If

                ''------------------------- PINCODE
                'If dtRow.Item("Zipcode").ToString.Trim.Length > 0 Then
                '    intPincodeId = objPincode.GetZipCodeUnkId(dtRow.Item("Zipcode").ToString.Trim)
                '    If intPincodeId <= 0 Then
                '        If Not objPincode Is Nothing Then objPincode = Nothing
                '        objPincode = New clszipcode_master
                '        objPincode._Cityunkid = intCityId
                '        objPincode._Countryunkid = intCountryId
                '        objPincode._Isactive = True
                '        objPincode._Stateunkid = intStateId
                '        objPincode._Zipcode_Code = dtRow.Item("Zipcode").ToString.Trim
                '        objPincode._Zipcode_No = dtRow.Item("Zipcode").ToString.Trim
                '        If objPincode.Insert Then
                '            intPincodeId = objPincode._Zipcodeunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objPincode._Zipcode_Code & "/" & objPincode._Zipcode_No & ":" & objPincode._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                '            dtRow.Item("objStatus") = 2
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '        End If
                '    End If
                'End If

                ''------------------------- NATIONALITY
                'If dtRow.Item("Nationality").ToString.Length > 0 Then
                '    intNationality = objCountry.GetCountryUnkId(dtRow.Item("Nationality").ToString.Trim)
                '    If intCountryId <= 0 Then
                '        If Not objCountry Is Nothing Then objCountry = Nothing
                '        objCountry = New clsMasterData
                '        intNationality = objCountry.InsertCountry(dtRow.Item("Nationality").ToString.Trim)
                '    End If
                'End If
                'S.SANDEEP [ 04 APRIL 2012 ] -- END



                'Gajanan [27-May-2019] -- Start              
                '------------------------- EffectiveDate

                If dtRow.Item("EffectiveDate").ToString.Trim.Length > 0 Then
                    If IsDate(dtRow.Item("EffectiveDate")) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    ElseIf eZeeDate.convertDate(CDate(dtRow.Item("EffectiveDate"))) > eZeeDate.convertDate(DateAndTime.Today.Date) Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Effective date should not be greater than current date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If


                End If
                'Gajanan [27-May-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                    If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, intEmployeeUnkid, Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = objApprovalData._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If
                    'Gajanan [17-April-2019] -- End

                    Dim intOperationType As Integer = 0 : Dim strMsg As String = ""


                    'Gajanan [10-June-2019] -- Start      

                    'If objADependantBeneficiaryTran.isExist(dtRow.Item("Dpndtfirstname").ToString.Trim, dtRow.Item("Dpndtfirstname").ToString.Trim, dtRow.Item("Dpndtmiddlename").ToString().Trim(), _
                    '                                        dtRow.Item("IdNo").ToString.Trim, intEmployeeUnkid, "", Nothing, True, intOperationType) = True Then

                    If objADependantBeneficiaryTran.isExist(dtRow.Item("Dpndtfirstname").ToString.Trim, dtRow.Item("Dpndtlastname").ToString.Trim, dtRow.Item("Dpndtmiddlename").ToString().Trim(), _
                                                            dtRow.Item("IdNo").ToString.Trim, intEmployeeUnkid, "", Nothing, True, intOperationType) = True Then
                        'Gajanan [10-June-2019] -- End


                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED

                                If CBool(dtRow.Item("isbeneficiary").ToString.Trim) = True Then
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
                                End If


                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If CBool(dtRow.Item("isbeneficiary").ToString.Trim) = True Then
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
                                End If

                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If CBool(dtRow.Item("isbeneficiary").ToString.Trim) = True Then
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage("frmDependantsAndBeneficiaries", 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
                                End If

                        End Select

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = strMsg
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For

                    End If



                End If

                'Gajanan [17-April-2019] -- End



                objDependantBeneficiaryTran = New clsDependants_Beneficiary_tran

                intDependantBeneficiaryUnkid = objDependantBeneficiaryTran.GetDependantUnkid(intEmployeeUnkid, _
                                                                                             dtRow.Item("Dpndtfirstname").ToString.Trim, _
                                                                                             dtRow.Item("Dpndtlastname").ToString.Trim)

                If intDependantBeneficiaryUnkid > 0 Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Dependant Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                Else
                    'If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Else
                    '    objDependantBeneficiaryTran._Address = dtRow.Item("Address").ToString.Trim
                    '    Try
                    '        If dtRow.Item("birthdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("birthdate")) Then
                    '            objDependantBeneficiaryTran._Birthdate = CDate(dtRow.Item("birthdate"))
                    '        End If
                    '    Catch ex As Exception
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Finally
                    '    End Try


                    '    'Shani (19-Oct-2016) -- Start
                    '    'Enhancement - Add Gender In Depandent Importation Screen
                    '    Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                    '        'S.SANDEEP [13-AUG-2018] -- START
                    '        'ISSUE/ENHANCEMENT : {#0002445|ARUTI-283}
                    '        'Case Language.getMessage("clsMasterData", 73, "Male").ToUpper
                    '        '    intGender = 1
                    '        'Case Language.getMessage("clsMasterData", 74, "Female").ToUpper
                    '        '    intGender = 2
                    '        'Case Else
                    '        '    intGender = 0
                    '        Case "MALE", "M"
                    '            intGender = 1
                    '        Case "FEMALE", "F"
                    '            intGender = 2
                    '        Case Else
                    '            intGender = 0
                    '            'S.SANDEEP [13-AUG-2018] -- END
                    '    End Select

                    '    'Shani (19-Oct-2016) -- End


                    '    objDependantBeneficiaryTran._Cityunkid = intCityId
                    '    objDependantBeneficiaryTran._Countryunkid = intCountryId
                    '    objDependantBeneficiaryTran._Employeeunkid = intEmployeeUnkid
                    '    objDependantBeneficiaryTran._First_Name = dtRow.Item("Dpndtfirstname").ToString.Trim
                    '    objDependantBeneficiaryTran._Identify_No = dtRow.Item("IdNo").ToString.Trim
                    '    objDependantBeneficiaryTran._Isdependant = CBool(dtRow.Item("isbeneficiary").ToString.Trim)
                    '    objDependantBeneficiaryTran._Last_Name = dtRow.Item("Dpndtlastname").ToString.Trim
                    '    objDependantBeneficiaryTran._Mobile_No = dtRow.Item("Mobile").ToString.Trim
                    '    objDependantBeneficiaryTran._Nationalityunkid = intNationality
                    '    objDependantBeneficiaryTran._Relationunkid = intRelationUnkid
                    '    objDependantBeneficiaryTran._Stateunkid = intStateId
                    '    objDependantBeneficiaryTran._Telephone_No = dtRow.Item("Telephone").ToString.Trim
                    '    objDependantBeneficiaryTran._Userunkid = User._Object._Userunkid
                    '    objDependantBeneficiaryTran._Zipcodeunkid = intPincodeId

                    '    'Shani (19-Oct-2016) -- Start
                    '    'Enhancement - Add Gender In Depandent Importation Screen
                    '    objDependantBeneficiaryTran._Gender = intGender
                    '    'Shani (19-Oct-2016) -- End

                    '    'Pinkal (20-Jul-2018) -- Start
                    '    'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.
                    '    objDependantBeneficiaryTran._Middle_Name = dtRow.Item("Dpndtmiddlename").ToString().Trim()
                    '    'Pinkal (20-Jul-2018) -- End


                    '    If objDependantBeneficiaryTran.Insert(Nothing, Nothing) Then
                    '        dtRow.Item("image") = imgAccept
                    '        dtRow.Item("message") = ""
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Success")
                    '        dtRow.Item("objStatus") = 1
                    '        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    '    Else
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objDependantBeneficiaryTran._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '    End If


                    If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                        objADependantBeneficiaryTran = New clsDependant_beneficiaries_approval_tran

                        objADependantBeneficiaryTran._Audittype = enAuditType.ADD
                        objADependantBeneficiaryTran._Audituserunkid = User._Object._Userunkid


                        objADependantBeneficiaryTran._Address = dtRow.Item("Address").ToString.Trim
                        Try
                            If dtRow.Item("birthdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("birthdate")) Then
                                objADependantBeneficiaryTran._Birthdate = CDate(dtRow.Item("birthdate"))
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Finally
                        End Try

                        Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                            Case "MALE", "M"
                                intGender = 1
                            Case "FEMALE", "F"
                                intGender = 2
                            Case Else
                                intGender = 0
                        End Select

                        objADependantBeneficiaryTran._Cityunkid = intCityId
                        objADependantBeneficiaryTran._Countryunkid = intCountryId
                        objADependantBeneficiaryTran._Employeeunkid = intEmployeeUnkid
                        objADependantBeneficiaryTran._First_Name = dtRow.Item("Dpndtfirstname").ToString.Trim
                        objADependantBeneficiaryTran._Identify_No = dtRow.Item("IdNo").ToString.Trim
                        objADependantBeneficiaryTran._Isdependant = CBool(dtRow.Item("isbeneficiary").ToString.Trim)
                        objADependantBeneficiaryTran._Last_Name = dtRow.Item("Dpndtlastname").ToString.Trim
                        objADependantBeneficiaryTran._Mobile_No = dtRow.Item("Mobile").ToString.Trim
                        objADependantBeneficiaryTran._Nationalityunkid = intNationality
                        objADependantBeneficiaryTran._Relationunkid = intRelationUnkid
                        objADependantBeneficiaryTran._Stateunkid = intStateId
                        objADependantBeneficiaryTran._Telephone_No = dtRow.Item("Telephone").ToString.Trim
                        objADependantBeneficiaryTran._Zipcodeunkid = intPincodeId
                        objADependantBeneficiaryTran._Gender = intGender
                        objADependantBeneficiaryTran._Middle_Name = dtRow.Item("Dpndtmiddlename").ToString().Trim()


                        'Gajanan [27-May-2019] -- Start              
                        objADependantBeneficiaryTran._Effective_date = CDate(dtRow.Item("EffectiveDate"))
                        objADependantBeneficiaryTran._ImagePath = ""
                        'Gajanan [27-May-2019] -- End


                        objADependantBeneficiaryTran._Isvoid = False
                        objADependantBeneficiaryTran._Tranguid = Guid.NewGuid.ToString()
                        objADependantBeneficiaryTran._Transactiondate = Now
                        objADependantBeneficiaryTran._Approvalremark = ""
                        objADependantBeneficiaryTran._Isweb = False
                        objADependantBeneficiaryTran._Isfinal = False
                        objADependantBeneficiaryTran._Ip = getIP()
                        objADependantBeneficiaryTran._Host = getHostName()
                        objADependantBeneficiaryTran._Form_Name = mstrModuleName
                        objADependantBeneficiaryTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        objADependantBeneficiaryTran._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED


                        If objADependantBeneficiaryTran.Insert(Nothing, Nothing) Then
                            dtRow.Item("image") = imgAccept
                            dtRow.Item("message") = ""
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Success")
                            dtRow.Item("objStatus") = 1
                            objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                            'Gajanan [27-May-2019] -- Start
                            If EmpList.Contains(intEmployeeUnkid) = False Then
                                EmpList.Add(intEmployeeUnkid)
                            End If

                            'Dim ExtraFilter As String = "employeeunkid= " & objADependantBeneficiaryTran._Employeeunkid & " and first_name = '" & objADependantBeneficiaryTran._First_Name & "' and last_name ='" & objADependantBeneficiaryTran._Last_Name & "' and relationunkid =" & objADependantBeneficiaryTran._Relationunkid & ""
                            'Dim ExtraFilterForQuery As String = "employeeunkid= " & objADependantBeneficiaryTran._Employeeunkid & " and first_name = '" & objADependantBeneficiaryTran._First_Name & "' and last_name ='" & objADependantBeneficiaryTran._Last_Name & "' and relationunkid =" & objADependantBeneficiaryTran._Relationunkid & ""

                            'If objADependantBeneficiaryTran._Middle_Name.Trim.Length > 0 Then
                            '    ExtraFilter &= "and middle_name ='" & objADependantBeneficiaryTran._Middle_Name & "'"
                            '    ExtraFilterForQuery &= "and middle_name ='" & objADependantBeneficiaryTran._Middle_Name & "'"

                            'End If

                            'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                            '                          ConfigParameter._Object._UserAccessModeSetting, _
                            '                          Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                            '                          CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                            '                          enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                            '                          User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                            '                          User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, 0, intEmployeeUnkid, , , _
                            '                          ExtraFilter, Nothing, False, , ExtraFilterForQuery, Nothing)

                            'Gajanan [27-May-2019] -- End

                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objDependantBeneficiaryTran._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If


                        


                    Else
                    objDependantBeneficiaryTran._Address = dtRow.Item("Address").ToString.Trim
                    Try
                        If dtRow.Item("birthdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("birthdate")) Then
                            objDependantBeneficiaryTran._Birthdate = CDate(dtRow.Item("birthdate"))
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Finally
                    End Try

                    Select Case dtRow.Item("Gender").ToString.Trim.ToUpper
                        Case "MALE", "M"
                            intGender = 1
                        Case "FEMALE", "F"
                            intGender = 2
                        Case Else
                            intGender = 0
                    End Select
                    


                    objDependantBeneficiaryTran._Cityunkid = intCityId
                    objDependantBeneficiaryTran._Countryunkid = intCountryId
                    objDependantBeneficiaryTran._Employeeunkid = intEmployeeUnkid
                    objDependantBeneficiaryTran._First_Name = dtRow.Item("Dpndtfirstname").ToString.Trim
                    objDependantBeneficiaryTran._Identify_No = dtRow.Item("IdNo").ToString.Trim
                    objDependantBeneficiaryTran._Isdependant = CBool(dtRow.Item("isbeneficiary").ToString.Trim)
                    objDependantBeneficiaryTran._Last_Name = dtRow.Item("Dpndtlastname").ToString.Trim
                    objDependantBeneficiaryTran._Mobile_No = dtRow.Item("Mobile").ToString.Trim
                    objDependantBeneficiaryTran._Nationalityunkid = intNationality
                    objDependantBeneficiaryTran._Relationunkid = intRelationUnkid
                    objDependantBeneficiaryTran._Stateunkid = intStateId
                    objDependantBeneficiaryTran._Telephone_No = dtRow.Item("Telephone").ToString.Trim
                    objDependantBeneficiaryTran._Userunkid = User._Object._Userunkid
                    objDependantBeneficiaryTran._Zipcodeunkid = intPincodeId
                    objDependantBeneficiaryTran._Gender = intGender
                    objDependantBeneficiaryTran._Middle_Name = dtRow.Item("Dpndtmiddlename").ToString().Trim()

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    'objDependantBeneficiaryTran._Effective_date = dtpEffectiveDate.Value
                    'Sohail (18 May 2019) -- End
                        'Gajanan [27-May-2019] -- Start              
                        objDependantBeneficiaryTran._Effective_date = CDate(dtRow.Item("EffectiveDate"))
                        'Gajanan [27-May-2019] -- End

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    With objDependantBeneficiaryTran
                        objDependantBeneficiaryTran._WebFormName = mstrModuleName
                        objDependantBeneficiaryTran._Loginemployeeunkid = 0
                        objDependantBeneficiaryTran._WebClientIP = getIP()
                        objDependantBeneficiaryTran._WebHostName = getHostName()
                        objDependantBeneficiaryTran._Isweb = False
                        objDependantBeneficiaryTran._CompanyUnkid = Company._Object._Companyunkid
                        objDependantBeneficiaryTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    'Sohail (18 May 2019) -- End


                    If objDependantBeneficiaryTran.Insert(Nothing, Nothing) Then
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    Else
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objDependantBeneficiaryTran._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    End If
                    End If
                End If
            Next

            'Gajanan [27-May-2019] -- Start
            If DependantApprovalFlowVal Is Nothing Then
                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 8, "Fail") & "'").Count > 0 Then
                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 8, "Fail") & "'"
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                   CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                                   enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                    End If
                End If
            End If
            'Gajanan [27-May-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    dtTable.Columns.Remove("CName")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText" , Me.eZeeWizImportEmp.CancelText)
			Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText" , Me.eZeeWizImportEmp.NextText)
			Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText" , Me.eZeeWizImportEmp.BackText)
			Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText" , Me.eZeeWizImportEmp.FinishText)
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblPincode.Text = Language._Object.getCaption(Me.lblPincode.Name, Me.lblPincode.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblBirthDate.Text = Language._Object.getCaption(Me.lblBirthDate.Name, Me.lblBirthDate.Text)
			Me.lblDepFirstName.Text = Language._Object.getCaption(Me.lblDepFirstName.Name, Me.lblDepFirstName.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblDepLastName.Text = Language._Object.getCaption(Me.lblDepLastName.Name, Me.lblDepLastName.Text)
			Me.lblMessage2.Text = Language._Object.getCaption(Me.lblMessage2.Name, Me.lblMessage2.Text)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)
			Me.lblIsBeneficiary.Text = Language._Object.getCaption(Me.lblIsBeneficiary.Name, Me.lblIsBeneficiary.Text)
			Me.lblTelephone.Text = Language._Object.getCaption(Me.lblTelephone.Name, Me.lblTelephone.Text)
			Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.Name, Me.lblSurname.Text)
			Me.lblFirsName.Text = Language._Object.getCaption(Me.lblFirsName.Name, Me.lblFirsName.Text)
			Me.lblIdNo.Text = Language._Object.getCaption(Me.lblIdNo.Name, Me.lblIdNo.Text)
			Me.lblNationality.Text = Language._Object.getCaption(Me.lblNationality.Name, Me.lblNationality.Text)
			Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
            Me.lblDepMiddleName.Text = Language._Object.getCaption(Me.lblDepMiddleName.Name, Me.lblDepMiddleName.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data to.")
			Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Dependant(s).")
			Language.setMessage(mstrModuleName, 4, "Dependant Firstname cannot be blank. Please set Dependant Firstname in order to import Dependant(s).")
			Language.setMessage(mstrModuleName, 5, "Dependant Lastname cannot be blan. Please set Dependant Lastname in order to import Dependant(s).")
			Language.setMessage(mstrModuleName, 6, "Relation cannot be blank. Please set Relation in order to import Dependant(s).")
			Language.setMessage(mstrModuleName, 7, "Beneficiary cannot be blank. Please set Beneficary in order to import Dependant(s).")
			Language.setMessage(mstrModuleName, 8, "Fail")
			Language.setMessage(mstrModuleName, 9, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 10, "Invalid Date")
			Language.setMessage(mstrModuleName, 11, "Dependant Already Exist.")
			Language.setMessage(mstrModuleName, 12, "Success")
			Language.setMessage(mstrModuleName, 13, "Please check the selected file column 'isbeneficary' should only be in 'True' Or 'False' Format. Please modify it and try to import again.")
			Language.setMessage(mstrModuleName, 14, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 15, "Gender data in file is not vaild.")
            Language.setMessage(mstrModuleName, 16, "Effective date should not be greater than current date.")
            Language.setMessage(mstrModuleName, 17, "Invalid Date")
            Language.setMessage(mstrModuleName, 41, " Fields From File.")
            Language.setMessage(mstrModuleName, 42, "Please select different Field.")
            Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
            Language.setMessage(mstrModuleName, 50, "is already Mapped with")
            Language.setMessage("frmDependantsAndBeneficiaries", 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
            Language.setMessage("frmDependantsAndBeneficiaries", 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
            Language.setMessage("frmDependantsAndBeneficiaries", 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
            Language.setMessage("frmDependantsAndBeneficiaries", 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
            Language.setMessage("frmDependantsAndBeneficiaries", 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
            Language.setMessage("frmDependantsAndBeneficiaries", 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
            Language.setMessage(mstrModuleName, 7, "Fail")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class