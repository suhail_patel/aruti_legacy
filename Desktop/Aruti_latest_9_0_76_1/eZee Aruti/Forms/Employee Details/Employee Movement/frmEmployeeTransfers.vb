﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeTransfers

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeTransfers"
    Private objETransfers As clsemployee_transfer_tran
    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private objATransfers As clsTransfer_Approval_Tran
    'S.SANDEEP [20-JUN-2018] -- END
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mdtAppointmentDate As Date = Nothing
    Private mstrEmployeeCode As String = String.Empty
    Private xOldAlloc As New DataSet
    Private xCurrentAllocation As Dictionary(Of Integer, String)
    Private mintEmployeeUnkid As Integer = 0
    'S.SANDEEP [09-AUG-2018] -- START
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    'S.SANDEEP [09-AUG-2018] -- END

    'S.SANDEEP [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'S.SANDEEP [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGrp As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objClassGrp As New clsClassGroup
        Dim objCMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If



            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB

            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                        User._Object._Userunkid, _
            '                                        FinancialYear._Object._YearUnkid, _
            '                                        Company._Object._Companyunkid, _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        ConfigParameter._Object._UserAccessModeSetting, _
            '                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END

            ''S.SANDEEP [04 JUN 2015] -- END
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Emp")
            '    .SelectedValue = 0
            '    .Text = ""
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End

            dsCombos = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
                .SelectedValue = 0
            End With

            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
                .SelectedValue = 0
            End With

            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With

            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With

            RemoveHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("ClassGrp")
                .SelectedValue = 0
            End With
            AddHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            objEmployee = Nothing : objStation = Nothing
            objDeptGrp = Nothing : objDepartment = Nothing
            objSectionGrp = Nothing : objSection = Nothing
            objUnitGroup = Nothing : objUnit = Nothing
            objTeam = Nothing : objClassGrp = Nothing
            objCMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            dsData = objETransfers.GetList("List", CInt(cboEmployee.SelectedValue))

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)

            'S.SANDEEP |17-JAN-2019| -- START
            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.Int32)
                .ColumnName = "operationtypeid"
                .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
            End With
            dsData.Tables(0).Columns.Add(dcol)

            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "OperationType"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)
            'S.SANDEEP |17-JAN-2019| -- END

            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim dsPending As New DataSet
                dsPending = objATransfers.GetList("List", CInt(cboEmployee.SelectedValue))
                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsData.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            dgvHistory.AutoGenerateColumns = False
            dgcolhChangeDate.DataPropertyName = "EffDate"
            dgcolhBranch.DataPropertyName = "branch"
            dgcolhDepartmentGrp.DataPropertyName = "deptgroup"
            dgcolhDepartment.DataPropertyName = "dept"
            dgcolhSecGroup.DataPropertyName = "secgroup"
            dgcolhSection.DataPropertyName = "section"
            dgcolhUnitGrp.DataPropertyName = "unitname"
            dgcolhUnit.DataPropertyName = "unit"
            dgcolhTeam.DataPropertyName = "team"
            dgcolhClsGrp.DataPropertyName = "classgrp"
            dgcolhClass.DataPropertyName = "class"
            dgcolhReason.DataPropertyName = "CReason"
            objdgcolhtransferunkid.DataPropertyName = "transferunkid"
            objdgcolhFromEmp.DataPropertyName = "isfromemployee"
            objdgcolhAppointdate.DataPropertyName = "adate"
            objdgcolhRehiretranunkid.DataPropertyName = "rehiretranunkid"
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objdgcolhtranguid.DataPropertyName = "tranguid"
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            objdgcolhOperationTypeId.DataPropertyName = "operationtypeid"
            objdgcolhOperationType.DataPropertyName = "OperationType"
            'S.SANDEEP |17-JAN-2019| -- END

            dgvHistory.DataSource = dsData.Tables(0)

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Transfers()
        Try
            Dim dsTransfer As New DataSet
            dsTransfer = objETransfers.Get_Current_Allocation(Now.Date, CInt(cboEmployee.SelectedValue))
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                cboStation.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("stationunkid")
                cboDepartmentGrp.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("deptgroupunkid")
                cboDepartment.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("departmentunkid")
                cboSectionGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectiongroupunkid")
                cboSections.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectionunkid")
                cboUnitGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitgroupunkid")
                cboUnits.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitunkid")
                cboTeams.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("teamunkid")
                cboClassGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classgroupunkid")
                cboClass.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classunkid")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Transfers", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Valid_Transfer() As Boolean
        Try
            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboStation.SelectedValue) <= 0 AndAlso _
               CInt(cboDepartmentGrp.SelectedValue) <= 0 AndAlso _
               CInt(cboDepartment.SelectedValue) <= 0 AndAlso _
               CInt(cboSectionGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboSections.SelectedValue) <= 0 AndAlso _
               CInt(cboUnitGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboUnits.SelectedValue) <= 0 AndAlso _
               CInt(cboTeams.SelectedValue) <= 0 AndAlso _
               CInt(cboClassGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboClass.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Allocations information are mandatory.Please select atleast one Allocation to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            'S.SANDEEP [18-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Department Was not Mandatory}
            If CInt(cboDepartment.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Department is mandatory information. Please select Department to continue."), enMsgBoxStyle.Information)
                Return False
            End If
            'S.SANDEEP [18-May-2018] -- END

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If



            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objETransfers.isExist(dtpEffectiveDate.Value.Date, CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                   CInt(cboDepartment.SelectedValue), CInt(cboSectionGroup.SelectedValue), CInt(cboSections.SelectedValue), _
                   CInt(cboUnitGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboTeams.SelectedValue), _
                   CInt(cboClassGroup.SelectedValue), CInt(cboClass.SelectedValue), CInt(cboEmployee.SelectedValue), 0) Then
                eZeeMsgBox.Show(Language.getMessage("clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), enMsgBoxStyle.Information)
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End


            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim strMsg As String = String.Empty
                Dim objPMovement As New clsEmployeeMovmentApproval
                'S.SANDEEP [18-SEP-2018] -- START                
                'strMsg = objPMovement.IsTransferCategorizationApprovers(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), CInt(cboDepartment.SelectedValue), CInt(cboSectionGroup.SelectedValue), CInt(cboSections.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboTeams.SelectedValue), CInt(cboClassGroup.SelectedValue), CInt(cboClass.SelectedValue), -1, -1)
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, CInt(cboEmployee.SelectedValue), Nothing)
                'S.SANDEEP [18-SEP-2018] -- END
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing


                'S.SANDEEP |16-JAN-2019| -- START
                If objETransfers.isExist(dtpEffectiveDate.Value.Date, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, CInt(cboEmployee.SelectedValue), mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_transfer_tran", 1, "Sorry, transfer information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objETransfers.Get_Current_Allocation(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("stationunkid")) = CInt(cboStation.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")) = CInt(cboDepartmentGrp.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("departmentunkid")) = CInt(cboDepartment.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")) = CInt(cboSectionGroup.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("sectionunkid")) = CInt(cboSections.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")) = CInt(cboUnitGroup.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("unitunkid")) = CInt(cboUnits.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("teamunkid")) = CInt(cboTeams.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("classgroupunkid")) = CInt(cboClassGroup.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("classunkid")) = CInt(cboClass.SelectedValue) Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                dsList = Nothing

                If objETransfers.isExist(dtpEffectiveDate.Value.Date, CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                   CInt(cboDepartment.SelectedValue), CInt(cboSectionGroup.SelectedValue), CInt(cboSections.SelectedValue), _
                   CInt(cboUnitGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboTeams.SelectedValue), CInt(cboClassGroup.SelectedValue), CInt(cboClass.SelectedValue), CInt(cboEmployee.SelectedValue), mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'S.SANDEEP [20-JUN-2018] -- END

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.


            If ConfigParameter._Object._CreateADUserFromEmpMst Then
                Dim objAttribute As New clsADAttribute_mapping
                Dim dtTable As DataTable = objAttribute.GetList("List", Company._Object._Companyunkid, True, Nothing, "attributename = '" & clsADAttribute_mapping.enAttributes.Transfer_Allocation.ToString().Replace("_", " ") & "'")

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                    Dim mintAllocationId As Integer = CInt(dtTable.Rows(0)("mappingunkid"))

                    Select Case mintAllocationId

                        Case CInt(enAllocation.BRANCH)

                            If CInt(cboStation.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblBranch.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboStation.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.DEPARTMENT_GROUP)

                            If CInt(cboDepartmentGrp.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblDepartmentGroup.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboDepartmentGrp.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.DEPARTMENT)

                            If CInt(cboDepartment.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblDepartment.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboDepartment.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.SECTION_GROUP)

                            If CInt(cboSectionGroup.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblSectionGroup.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboSectionGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.SECTION)

                            If CInt(cboSections.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblSection.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboSections.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.UNIT_GROUP)

                            If CInt(cboUnitGroup.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblUnitGroup.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboUnitGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.UNIT)

                            If CInt(cboUnits.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblUnits.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboUnits.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.TEAM)

                            If CInt(cboTeams.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblTeam.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboTeams.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.CLASS_GROUP)

                            If CInt(cboClassGroup.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblClassGroup.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboClassGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.CLASSES)

                            If CInt(cboClass.SelectedValue) <= 0 Then
                                eZeeMsgBox.Show(lblClass.Text & " " & Language.getMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cboClass.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                    End Select

                End If
                objAttribute = Nothing
            End If
            'Pinkal (18-Mar-2021) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valid_Transfer", mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objETransfers._Effectivedate = dtpEffectiveDate.Value
            'objETransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objETransfers._Stationunkid = CInt(cboStation.SelectedValue)
            'objETransfers._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
            'objETransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
            'objETransfers._Sectiongroupunkid = CInt(cboSectionGroup.SelectedValue)
            'objETransfers._Sectionunkid = CInt(cboSections.SelectedValue)
            'objETransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
            'objETransfers._Unitunkid = CInt(cboUnits.SelectedValue)
            'objETransfers._Teamunkid = CInt(cboTeams.SelectedValue)
            'objETransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            'objETransfers._Classunkid = CInt(cboClass.SelectedValue)
            'objETransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objETransfers._Isvoid = False
            'objETransfers._Statusunkid = 0
            'objETransfers._Userunkid = User._Object._Userunkid
            'objETransfers._Voiddatetime = Nothing
            'objETransfers._Voidreason = ""
            'objETransfers._Voiduserunkid = -1

            'S.SANDEEP [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

            'S.SANDEEP |17-JAN-2019| -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                'S.SANDEEP |17-JAN-2019| -- END

                'S.SANDEEP [15-NOV-2018] -- END
            objETransfers._Effectivedate = dtpEffectiveDate.Value
            objETransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objETransfers._Stationunkid = CInt(cboStation.SelectedValue)
            objETransfers._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
            objETransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
            objETransfers._Sectiongroupunkid = CInt(cboSectionGroup.SelectedValue)
            objETransfers._Sectionunkid = CInt(cboSections.SelectedValue)
            objETransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
            objETransfers._Unitunkid = CInt(cboUnits.SelectedValue)
            objETransfers._Teamunkid = CInt(cboTeams.SelectedValue)
            objETransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            objETransfers._Classunkid = CInt(cboClass.SelectedValue)
            objETransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            objETransfers._Isvoid = False
            objETransfers._Statusunkid = 0
            objETransfers._Userunkid = User._Object._Userunkid
            objETransfers._Voiddatetime = Nothing
            objETransfers._Voidreason = ""
            objETransfers._Voiduserunkid = -1
            Else

                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                objATransfers = New clsTransfer_Approval_Tran
                'Gajanan [11-Dec-2019] -- End

                objATransfers._Audittype = enAuditType.ADD
                objATransfers._Audituserunkid = User._Object._Userunkid
                objATransfers._Effectivedate = dtpEffectiveDate.Value
                objATransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objATransfers._Stationunkid = CInt(cboStation.SelectedValue)
                objATransfers._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
                objATransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
                objATransfers._Sectiongroupunkid = CInt(cboSectionGroup.SelectedValue)
                objATransfers._Sectionunkid = CInt(cboSections.SelectedValue)
                objATransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
                objATransfers._Unitunkid = CInt(cboUnits.SelectedValue)
                objATransfers._Teamunkid = CInt(cboTeams.SelectedValue)
                objATransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
                objATransfers._Classunkid = CInt(cboClass.SelectedValue)
                objATransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objATransfers._Isvoid = False
                objATransfers._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objATransfers._Voiddatetime = Nothing
                objATransfers._Voidreason = ""
                objATransfers._Voiduserunkid = -1
                objATransfers._Tranguid = Guid.NewGuid.ToString()
                objATransfers._Transactiondate = Now
                objATransfers._Remark = ""
                objATransfers._Rehiretranunkid = 0
                objATransfers._Mappingunkid = 0
                objATransfers._Isweb = False
                objATransfers._Isfinal = False
                objATransfers._Ip = getIP()
                objATransfers._Hostname = getHostName()
                objATransfers._Form_Name = mstrModuleName
                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objATransfers._Transferunkid = mintTransactionId
                    objATransfers._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objATransfers._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If

            'S.SANDEEP [20-JUN-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue()
        Try
            dtpEffectiveDate.Value = objETransfers._Effectivedate
            cboEmployee.SelectedValue = objETransfers._Employeeunkid
            cboStation.SelectedValue = objETransfers._Stationunkid
            cboDepartmentGrp.SelectedValue = objETransfers._Deptgroupunkid
            cboDepartment.SelectedValue = objETransfers._Departmentunkid
            cboSectionGroup.SelectedValue = objETransfers._Sectiongroupunkid
            cboSections.SelectedValue = objETransfers._Sectionunkid
            cboUnitGroup.SelectedValue = objETransfers._Unitgroupunkid
            cboUnits.SelectedValue = objETransfers._Unitunkid
            cboTeams.SelectedValue = objETransfers._Teamunkid
            cboClassGroup.SelectedValue = objETransfers._Classgroupunkid
            cboClass.SelectedValue = objETransfers._Classunkid
            cboChangeReason.SelectedValue = objETransfers._Changereasonunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            cboStation.SelectedValue = 0
            cboDepartmentGrp.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboSectionGroup.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnitGroup.SelectedValue = 0
            cboUnits.SelectedValue = 0
            cboTeams.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboChangeReason.SelectedValue = 0
            txtDate.Text = ""
            pnlData.Visible = False
            mdtAppointmentDate = Nothing

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            mintTransactionId = -1
            'Gajanan [11-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetAllocationForMail()
        Try
            xCurrentAllocation = New Dictionary(Of Integer, String)
            '''''''''''' BRANCH
            If CInt(cboStation.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.BRANCH, cboStation.Text)
            Else
                xCurrentAllocation.Add(enAllocation.BRANCH, "&nbsp;")
            End If

            '''''''''''' DEPARTMENT GROUP
            If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.DEPARTMENT_GROUP, cboDepartmentGrp.Text)
            Else
                xCurrentAllocation.Add(enAllocation.DEPARTMENT_GROUP, "&nbsp;")
            End If

            '''''''''''' DEPARTMENT
            If CInt(cboDepartment.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.DEPARTMENT, cboDepartment.Text)
            Else
                xCurrentAllocation.Add(enAllocation.DEPARTMENT, "&nbsp;")
            End If

            '''''''''''' SECTION GROUP
            If CInt(cboSectionGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.SECTION_GROUP, cboSectionGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.SECTION_GROUP, "&nbsp;")
            End If

            '''''''''''' SECTION
            If CInt(cboSections.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.SECTION, cboSections.Text)
            Else
                xCurrentAllocation.Add(enAllocation.SECTION, "&nbsp;")
            End If

            '''''''''''' UNIT GROUP
            If CInt(cboUnitGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.UNIT_GROUP, cboUnitGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.UNIT_GROUP, "&nbsp;")
            End If

            '''''''''''' UNIT
            If CInt(cboUnits.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.UNIT, cboUnits.Text)
            Else
                xCurrentAllocation.Add(enAllocation.UNIT, "&nbsp;")
            End If

            '''''''''''' TEAM
            If CInt(cboTeams.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.TEAM, cboTeams.Text)
            Else
                xCurrentAllocation.Add(enAllocation.TEAM, "&nbsp;")
            End If

            '''''''''''' CLASS GROUP
            If CInt(cboClassGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.CLASS_GROUP, cboClassGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.CLASS_GROUP, "&nbsp;")
            End If

            '''''''''''' CLASSES
            If CInt(cboClass.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.CLASSES, cboClass.Text)
            Else
                xCurrentAllocation.Add(enAllocation.CLASSES, "&nbsp;")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetAllocationForMail", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToChangeEmpTransfers

            'S.SANDEEP |17-JAN-2019| -- START
            'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditTransferEmployeeDetails
            If User._Object.Privilege._AllowToEditTransferEmployeeDetails = False Then
                objdgcolhEdit.Image = imgBlank
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteTransferEmployeeDetails
            'S.SANDEEP [09-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#292}
            objdgcolhViewPending.Visible = Not ConfigParameter._Object._SkipEmployeeMovementApprovalFlow
            'S.SANDEEP [09-AUG-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End


    'Private Function Set_Notification_Allocation(ByVal StrUserName As String) As String
    '    Dim StrMessage As New System.Text.StringBuilder
    '    Dim blnFlag As Boolean = False
    '    Try
    '        StrMessage.Append("<HTML><BODY>")
    '        StrMessage.Append(vbCrLf)
    '        If ConfigParameter._Object._Notify_Allocation.Trim.Length > 0 Then
    '            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
    '            StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
    '            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & cboEmployee.Text & "</b></span></p>")
    '            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
    '            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & mstrEmployeeCode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
    '            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")
    '            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Effective Date") & "</span></b></TD>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & dtpEffectiveDate.Value.ToShortDateString & "</span></b></TD>")
    '            StrMessage.Append("</TR>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("</TABLE>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<BR>")
    '            StrMessage.Append("<TABLE border = '1' WIDTH = '90%'>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 8, "Allocations") & "</span></b></TD>")
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 9, "Old Allocation") & "</span></b></TD>")
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 10, "New Allocation") & "</span></b></TD>")
    '            StrMessage.Append(vbCrLf)
    '            StrMessage.Append("</TR>")
    '            For Each sId As String In ConfigParameter._Object._Notify_Allocation.Split(CChar("||"))(0).Split(CChar(","))
    '                Select Case CInt(sId)
    '                    Case enAllocation.BRANCH
    '                        Dim ObjStation As New clsStation
    '                        If CInt(cboStation.SelectedValue) > 0 Then
    '                            ObjStation._Stationunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("stationunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 430, "Branch") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjStation._Name.Trim = "", "&nbsp;", ObjStation._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboStation.SelectedValue) <= 0, "&nbsp;", cboStation.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        Dim ObjDG As New clsDepartmentGroup
    '                        If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
    '                            ObjDG._Deptgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("deptgroupunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 429, "Department Group") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjDG._Name.Trim = "", "&nbsp;", ObjDG._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboDepartmentGrp.SelectedValue) <= 0, "&nbsp;", cboDepartmentGrp.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.DEPARTMENT
    '                        Dim ObjDept As New clsDepartment
    '                        If CInt(cboDepartment.SelectedValue) > 0 Then
    '                            ObjDept._Departmentunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("departmentunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 428, "Department") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjDept._Name.Trim = "", "&nbsp;", ObjDept._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboDepartment.SelectedValue) <= 0, "&nbsp;", cboDepartment.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.SECTION_GROUP
    '                        Dim ObjSecGrp As New clsSectionGroup
    '                        If CInt(cboSectionGroup.SelectedValue) > 0 Then
    '                            ObjSecGrp._Sectiongroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("sectiongroupunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 427, "Section Group") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjSecGrp._Name.Trim = "", "&nbsp;", ObjSecGrp._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboSectionGroup.SelectedValue) <= 0, "&nbsp;", cboSectionGroup.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.SECTION
    '                        Dim ObjSec As New clsSections
    '                        If CInt(cboSections.SelectedValue) > 0 Then
    '                            ObjSec._Sectionunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("sectionunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 426, "Section") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjSec._Name.Trim = "", "&nbsp;", ObjSec._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboSections.SelectedValue) <= 0, "&nbsp;", cboSections.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.UNIT_GROUP
    '                        Dim ObjUG As New clsUnitGroup
    '                        If CInt(cboUnitGroup.SelectedValue) > 0 Then
    '                            ObjUG._Unitgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("unitgroupunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 425, "Unit Group") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjUG._Name.Trim = "", "&nbsp;", ObjUG._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboUnitGroup.SelectedValue) <= 0, "&nbsp;", cboUnitGroup.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.UNIT
    '                        Dim ObjUnit As New clsUnits
    '                        If CInt(cboUnits.SelectedValue) > 0 Then
    '                            ObjUnit._Unitunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("unitunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 424, "Unit") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjUnit._Name.Trim = "", "&nbsp;", ObjUnit._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboUnits.SelectedValue) <= 0, "&nbsp;", cboUnits.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.TEAM
    '                        Dim ObjTeam As New clsTeams
    '                        If CInt(cboTeams.SelectedValue) > 0 Then
    '                            ObjTeam._Teamunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("teamunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 423, "Team") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjTeam._Name.Trim = "", "&nbsp;", ObjTeam._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboTeams.SelectedValue) <= 0, "&nbsp;", cboTeams.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.CLASS_GROUP
    '                        Dim ObjCG As New clsClassGroup
    '                        If CInt(cboClassGroup.SelectedValue) > 0 Then
    '                            ObjCG._Classgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("classgroupunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 420, "Class Group") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjCG._Name.Trim = "", "&nbsp;", ObjCG._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboClassGroup.SelectedValue) <= 0, "&nbsp;", cboClassGroup.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                    Case enAllocation.CLASSES
    '                        Dim ObjClass As New clsClass
    '                        If CInt(cboClass.SelectedValue) > 0 Then
    '                            ObjClass._Classesunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("classunkid"))
    '                            StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
    '                            StrMessage.Append(vbCrLf)
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 419, "Classes") & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjClass._Name.Trim = "", "&nbsp;", ObjClass._Name).ToString & "</span></TD>")
    '                            StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(CInt(cboClass.SelectedValue) <= 0, "&nbsp;", cboClass.Text).ToString & "</span></TD>")
    '                            StrMessage.Append("</TR>" & vbCrLf)
    '                            blnFlag = True
    '                        End If
    '                End Select
    '            Next
    '        End If
    '        StrMessage.Append("</TABLE>")
    '        StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '        StrMessage.Append("</span></p>")
    '        StrMessage.Append("</BODY></HTML>")

    '        If blnFlag = False Then
    '            StrMessage = StrMessage.Remove(0, StrMessage.Length)
    '        End If

    '        Return StrMessage.ToString

    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "Set_Notification_Allocation", mstrModuleName)
    '        Return ""
    '    End Try
    'End Function

    'Private Sub Email_Sending()
    '    Try
    '        If ConfigParameter._Object._Notify_Allocation.Trim.Length > 0 Then

    '            Dim xOldDate As Date = DateAdd(DateInterval.Day, -1, dtpEffectiveDate.Value.Date)
    '            xOldAlloc = objETransfers.Get_Current_Allocation(xOldDate, CInt(cboEmployee.SelectedValue))
    '            If xOldAlloc.Tables(0).Rows.Count <= 0 Then Exit Sub

    '            Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
    '            For Each sId As String In ConfigParameter._Object._Notify_Allocation.Split(CChar("||"))(2).Split(CChar(","))
    '                objUsr._Userunkid = CInt(sId)
    '                StrMessage = Set_Notification_Allocation(objUsr._Firstname & " " & objUsr._Lastname)
    '                gobjEmailList.Add(New clsEmailCollection(objUsr._Email, _
    '                                                         Language.getMessage(mstrModuleName, 6, "Notification of Changes in Employee Master file"), _
    '                                                         StrMessage, _
    '                                                         "", _
    '                                                         0, _
    '                                                         "", _
    '                                                         "", _
    '                                                         User._Object._Userunkid, _
    '                                                         enLogin_Mode.DESKTOP, _
    '                                                         clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT, _
    '                                                         objUsr._Firstname & " " & objUsr._Lastname))
    '            Next
    '            objUsr = Nothing
    '        End If
    '        trd = New Thread(AddressOf Send_Notification)
    '        trd.IsBackground = True
    '        trd.Start()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Email_Sending", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub Send_Notification()
    '    Try
    '        If gobjEmailList.Count > 0 Then
    '            Dim objSendMail As New clsSendMail
    '            For Each obj In gobjEmailList
    '                objSendMail._ToEmail = obj._EmailTo
    '                objSendMail._Subject = obj._Subject
    '                objSendMail._Message = obj._Message
    '                objSendMail._Form_Name = obj._Form_Name
    '                objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
    '                objSendMail._OperationModeId = obj._OperationModeId
    '                objSendMail._UserUnkid = obj._UserUnkid
    '                objSendMail._SenderAddress = obj._SenderAddress
    '                objSendMail._ModuleRefId = obj._ModuleRefId
    '                Try
    '                    objSendMail.SendMail()
    '                Catch ex As Exception
    '                End Try
    '            Next
    '            gobjEmailList.Clear()
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
    '    Finally
    '        If gobjEmailList.Count > 0 Then
    '            gobjEmailList.Clear()
    '        End If
    '    End Try
    'End Sub


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End


#End Region

#Region " Form's Events "

    Private Sub frmEmployeeTransfers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objETransfers = New clsemployee_transfer_tran
        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        objATransfers = New clsTransfer_Approval_Tran
        'S.SANDEEP [20-JUN-2018] -- END
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                'cboEmployee.Enabled = False
                'objbtnSearchEmployee.Enabled = False
                'Gajanan [11-Dec-2019] -- End
            End If
            pnlData.Visible = False
            'S.SANDEEP [14 APR 2015] -- START
            objlblCaption.Visible = False
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblPendingData.Visible = False
            'S.SANDEEP [20-JUN-2018] -- END


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            'btnSave.Enabled = User._Object.Privilege._AllowToChangeEmpTransfers
            'Shani (08-Dec-2016) -- End
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeTransfers_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_transfer_tran.SetMessages()
            objfrm._Other_ModuleNames = "clshremployee_transfer_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Valid_Transfer() = False Then Exit Sub
            Call SetValue()


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            Dim objfrmMovementMigration As New frmMovementMigration
            Dim DtMigration As DataTable = Nothing
            'Gajanan [23-SEP-2019] -- End

            If mAction = enAction.EDIT_ONE Then
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
                '    blnFlag = objETransfers.Update()
                'Else
                '    blnFlag = objETransfers.Insert()

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objETransfers.Update(Company._Object._Companyunkid)

                'S.SANDEEP |17-JAN-2019| -- START
                'blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)


                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, False, CInt(cboEmployee.SelectedValue)) Then

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                            blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False)
                            'Pinkal (12-Oct-2020) -- End


                    End If
                    Else

                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                        blnFlag = objETransfers.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False)
                        'Pinkal (12-Oct-2020) -- End


                    End If
                    'Gajanan [26-OCT-2019] -- End

                   
                    'Gajanan [23-SEP-2019] -- End
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED)
                    

                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, True, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, False, DtMigration, True)
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, True, CInt(cboEmployee.SelectedValue)) Then


                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                            'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                            '                                           , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                            blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                                                                       , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                       , Nothing, False, DtMigration, True)


                            'Pinkal (12-Oct-2020) -- End

                    End If
                    Else


                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                        'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                        '                                            , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                        blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                                                     , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                     , Nothing, False, DtMigration, True)

                        'Pinkal (12-Oct-2020) -- End

                    End If

                    'Gajanan [26-OCT-2019] -- End


                    'Gajanan [23-SEP-2019] -- End

                    If blnFlag = False AndAlso objATransfers._Message <> "" Then
                        eZeeMsgBox.Show(objATransfers._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Pinkal (18-Aug-2018) -- End
            Else
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objETransfers.Insert(Company._Object._Companyunkid)
                ''Pinkal (21-Dec-2015) -- Start
                ''Enhancement - Working on Employee Benefit changes given By Rutta.
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                '    Dim objFrm As New frmEmployeeBenefitCoverage
                '    objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                'End If
                ''Pinkal (21-Dec-2015) -- End
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'blnFlag = objETransfers.Insert(Company._Object._Companyunkid)


                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    'Gajanan [23-SEP-2019] -- Start    
                    '    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    '    'blnFlag = objETransfers.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                    '    blnFlag = objETransfers.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                    '    'Gajanan [23-SEP-2019] -- End
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'blnFlag = objETransfers.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                            blnFlag = objETransfers.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                            'Pinkal (12-Oct-2020) -- End
                    End If
                    Else

                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'blnFlag = objETransfers.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                        blnFlag = objETransfers.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration, False, False)
                        'Pinkal (12-Oct-2020) -- End
                    End If
                    'Gajanan [26-OCT-2019] -- End
                    'Pinkal (18-Aug-2018) -- End


                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    If blnFlag Then
                        'Gajanan [26-OCT-2019] -- End
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmEmployeeBenefitCoverage
                    objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                End If
                    End If

                Else

                    'S.SANDEEP |17-JAN-2019| -- START
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED)
                    

                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    'If objfrmMovementMigration.displayDialog(DtMigration, 0, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing, False, DtMigration, True)
                    'End If
                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, 0, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then


                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                            'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                            '                                           , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)
                            blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                                                                      , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                      , Nothing, False, DtMigration, True)
                            'Pinkal (12-Oct-2020) -- End

                    End If
                    Else


                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                        'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                        '                                           , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                        blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                                                                  , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                  , Nothing, False, DtMigration, True)


                        'Pinkal (12-Oct-2020) -- End

                    End If
                    'Gajanan [26-OCT-2019] -- End

                    'Gajanan [23-SEP-2019] -- End

                    If blnFlag = False AndAlso objATransfers._Message <> "" Then
                        eZeeMsgBox.Show(objATransfers._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If
                'S.SANDEEP [20-JUN-2018] -- END

                'S.SANDEEP [10-MAY-2017] -- END

            End If
            If blnFlag = False AndAlso objETransfers._Message <> "" Then
                eZeeMsgBox.Show(objETransfers._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else

                'Gajanan [26-OCT-2019] -- Start    
                'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                If blnFlag Then
                    'Gajanan [26-OCT-2019] -- End

                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Call SetAllocationForMail()
                ''Sohail (30 Nov 2017) -- Start
                ''SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                ''Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date)
                'Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date, Company._Object._Companyunkid)
                ''Sohail (30 Nov 2017) -- End
                ''Call Email_Sending()
                'Call Set_Transfers()
                'S.SANDEEP [15-NOV-2018] -- START
                'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                'S.SANDEEP |17-JAN-2019| -- START
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
                    'S.SANDEEP |17-JAN-2019| -- END

                    'S.SANDEEP [15-NOV-2018] -- END
                Call SetAllocationForMail()
                Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date, Company._Object._Companyunkid)
                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END
                    objPMovement = Nothing
                End If
                Call Set_Transfers()
                'S.SANDEEP [20-JUN-2018] -- END
            End If
            End If
            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'S.SANDEEP [15-NOV-2018] -- START

            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END

            'S.SANDEEP [15-NOV-2018] -- END
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
                                                                                                        objbtnSearchEmployee.Click, _
                                                                                                        objbtnSearchBranch.Click, _
                                                                                                        objbtnSearchDeptGrp.Click, _
                                                                                                        objbtnSearchDepartment.Click, _
                                                                                                        objbtnSearchSecGroup.Click, _
                                                                                                        objbtnSearchSection.Click, _
                                                                                                        objbtnSearchClass.Click, _
                                                                                                        objbtnSearchClassGrp.Click, _
                                                                                                        objbtnSearchTeam.Click, _
                                                                                                        objbtnSearchUnits.Click, _
                                                                                                        objbtnSearchUnitGrp.Click, _
                                                                                                        objSearchReason.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchBranch.Name.ToUpper
                    xCbo = cboStation
                Case objbtnSearchDeptGrp.Name.ToUpper
                    xCbo = cboDepartmentGrp
                Case objbtnSearchDepartment.Name.ToUpper
                    xCbo = cboDepartment
                Case objbtnSearchSecGroup.Name.ToUpper
                    xCbo = cboSectionGroup
                Case objbtnSearchSection.Name.ToUpper
                    xCbo = cboSections
                Case objbtnSearchClass.Name.ToUpper
                    xCbo = cboClass
                Case objbtnSearchClassGrp.Name.ToUpper
                    xCbo = cboClassGroup
                Case objbtnSearchTeam.Name.ToUpper
                    xCbo = cboTeams
                Case objbtnSearchUnits.Name.ToUpper
                    xCbo = cboUnits
                Case objbtnSearchUnitGrp.Name.ToUpper
                    xCbo = cboUnitGroup
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.TRANSFERS, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString

            FillEmployeeCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Combobox Event(s) "


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (09-Apr-2015) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                End If
                'S.SANDEEP [14 APR 2015] -- START
                objlblCaption.Visible = False
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True
                'S.SANDEEP [04-AUG-2017] -- END

                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                If mAction = enAction.EDIT_ONE Then
                    If objETransfers._Transferunkid > 0 Then objETransfers._Transferunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If
                'S.SANDEEP [07-Feb-2018] -- END

                Call ClearControls()
                Call Fill_Grid()
                Call Set_Transfers()
                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = CInt(cboEmployee.SelectedValue)
                    If objEmp._Empl_Enddate <> Nothing Then
                        If objEmp._Empl_Enddate <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_From_Date <> Nothing Then
                        If objEmp._Termination_From_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_To_Date <> Nothing Then
                        If objEmp._Termination_To_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    objEmp = Nothing
                End If
                'S.SANDEEP [04-AUG-2017] -- END
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboStation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStation.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboStation.SelectedValue), "DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDepartmentGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartmentGrp.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDepartmentGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Department")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartmentGrp_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSectionGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSectionGroup.SelectedValue), "Section", True)
                With cboSections
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Section")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSectionGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then

                'S.SANDEEP [13 APR 2015] -- START
                'Dim dsCombos As New DataSet
                'Dim objUnit As New clsUnits
                'dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                'With cboUnits
                '    .ValueMember = "unitunkid"
                '    .DisplayMember = "name"
                '    .DataSource = dsCombos.Tables("Unit")
                'End With

                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet

                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSections.SelectedValue))
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                'S.SANDEEP [13 APR 2015] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                With cboUnits
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Unit")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnitGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnits.SelectedValue))
                With cboTeams
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objclass As New clsClass
            dsCombos = objclass.getComboList("Classgrp", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Classgrp")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            mdtAppointmentDate = Nothing
            txtDate.Text = ""
            pnlData.Visible = False
            Select Case e.ColumnIndex
                'S.SANDEEP [09-AUG-2018] -- START
                Case objdgcolhViewPending.Index
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhViewPending.Index).Value Is imgInfo Then
                        Dim frm As New frmViewMovementApproval
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        'S.SANDEEP |17-JAN-2019| -- START
                        'frm.displayDialog(User._Object._Userunkid, 0, 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, Nothing, False, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                        'If mAction = enAction.EDIT_ONE Then
                        '    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                        'Else
                        '    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'End If



                        frm.displayDialog(User._Object._Userunkid, 0, 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, Nothing, False, CType(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhOperationTypeId.Index).Value, clsEmployeeMovmentApproval.enOperationType), "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'S.SANDEEP |17-JAN-2019| -- END

                        If frm IsNot Nothing Then frm.Dispose()
                        Exit Sub
                    End If
                    'S.SANDEEP [09-AUG-2018] -- END

                Case objdgcolhEdit.Index

                    'S.SANDEEP |17-JAN-2019| -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhtransferunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value) & " AND " & objdgcolhtranguid.DataPropertyName & "= ''")
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    mAction = enAction.EDIT_ONE
                    'S.SANDEEP [14 APR 2015] -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [14 APR 2015] -- END
                    objETransfers._Transferunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value)

                    'S.SANDEEP [15-NOV-2018] -- START
                    mintTransactionId = objETransfers._Transferunkid
                    'S.SANDEEP [15-NOV-2018] -- END

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objATransfers.isExist(Nothing, objATransfers._Employeeunkid, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP |17-JAN-2019| -- END
                    End If


                    Call SetEditValue()
                    If CBool(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhFromEmp.Index).Value) = True Then
                        mdtAppointmentDate = eZeeDate.convertDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAppointdate.Index).Value.ToString)
                        txtDate.Text = mdtAppointmentDate.Date.ToShortDateString
                        pnlData.Visible = True
                    End If
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objATransfers.isExist(Nothing, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value))
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objETransfers._Isvoid = True
                    objETransfers._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objETransfers._Voidreason = xStrVoidReason
                    objETransfers._Voiduserunkid = User._Object._Userunkid
                    objETransfers._Userunkid = User._Object._Userunkid

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), Company._Object._Companyunkid) = False Then
                    'If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst) = False Then
                    '    'Pinkal (18-Aug-2018) -- End
                    '    'If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value)) = False Then
                    '    'S.SANDEEP [10-MAY-2017] -- END
                    '    If objETransfers._Message <> "" Then
                    '        eZeeMsgBox.Show(objETransfers._Message, enMsgBoxStyle.Information)
                    '    End If
                    '    Exit Sub
                    'End If


                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    Dim objfrmMovementMigration As New frmMovementMigration
                    Dim DtMigration As DataTable = Nothing
                    'Gajanan [23-SEP-2019] -- End

                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        objATransfers._Isvoid = True
                        objATransfers._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objATransfers._Voidreason = xStrVoidReason
                        objATransfers._Voiduserunkid = -1
                        objATransfers._Audituserunkid = User._Object._Userunkid
                        objATransfers._Isweb = False
                        objATransfers._Ip = getIP()
                        objATransfers._Hostname = getHostName()
                        objATransfers._Form_Name = mstrModuleName
                        objATransfers._Audituserunkid = User._Object._Userunkid




                        'Gajanan [23-SEP-2019] -- Start    
                        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                        If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.DELETED, False, CInt(cboEmployee.SelectedValue)) Then
                            'Gajanan [23-SEP-2019] -- End

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objATransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, True, False) = False Then
                            If objATransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid _
                                                            , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName, Nothing, DtMigration, True, False) = False Then
                                'Pinkal (12-Oct-2020) -- End

                            If objATransfers._Message <> "" Then
                                eZeeMsgBox.Show(objATransfers._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                        End If

                    Else

                        'Gajanan [23-SEP-2019] -- Start    
                        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                        If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.DELETED, False, CInt(cboEmployee.SelectedValue)) Then


                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, False, False) = False Then
                            If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst _
                                                              , FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False) = False Then
                                'Pinkal (12-Oct-2020) -- End


                                'Gajanan [23-SEP-2019] -- End
                        'If objETransfers.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhtransferunkid.Index).Value)) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                        If objETransfers._Message <> "" Then
                            eZeeMsgBox.Show(objETransfers._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    End If
                    End If

                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows
                'objdgcolhtranguid


                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                '    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                    xdgvr.DefaultCellStyle.BackColor = Color.PowderBlue
                    xdgvr.DefaultCellStyle.ForeColor = Color.Black
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [09-AUG-2018] -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).Value = imgInfo
                    'S.SANDEEP [09-AUG-2018] -- END
                    lblPendingData.Visible = True

                    'S.SANDEEP |17-JAN-2019| -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).ToolTipText = xdgvr.Cells(objdgcolhOperationType.Index).Value.ToString
                    If CInt(xdgvr.Cells(objdgcolhOperationTypeId.Index).Value) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgView
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                Else
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END


                'S.SANDEEP [14 APR 2015] -- START
                If CInt(xdgvr.Cells(objdgcolhRehiretranunkid.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    objlblCaption.Visible = True
                End If
                'S.SANDEEP [14 APR 2015] -- END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbTransferInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTransferInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.gbTransferInformation.Text = Language._Object.getCaption(Me.gbTransferInformation.Name, Me.gbTransferInformation.Text)
            Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblChangeReason.Text = Language._Object.getCaption(Me.lblChangeReason.Name, Me.lblChangeReason.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
            Me.dgcolhDepartmentGrp.HeaderText = Language._Object.getCaption(Me.dgcolhDepartmentGrp.Name, Me.dgcolhDepartmentGrp.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhSecGroup.HeaderText = Language._Object.getCaption(Me.dgcolhSecGroup.Name, Me.dgcolhSecGroup.HeaderText)
            Me.dgcolhSection.HeaderText = Language._Object.getCaption(Me.dgcolhSection.Name, Me.dgcolhSection.HeaderText)
            Me.dgcolhUnitGrp.HeaderText = Language._Object.getCaption(Me.dgcolhUnitGrp.Name, Me.dgcolhUnitGrp.HeaderText)
            Me.dgcolhUnit.HeaderText = Language._Object.getCaption(Me.dgcolhUnit.Name, Me.dgcolhUnit.HeaderText)
            Me.dgcolhTeam.HeaderText = Language._Object.getCaption(Me.dgcolhTeam.Name, Me.dgcolhTeam.HeaderText)
            Me.dgcolhClsGrp.HeaderText = Language._Object.getCaption(Me.dgcolhClsGrp.Name, Me.dgcolhClsGrp.HeaderText)
            Me.dgcolhClass.HeaderText = Language._Object.getCaption(Me.dgcolhClass.Name, Me.dgcolhClass.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Allocations information are mandatory.Please select atleast one Allocation to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")
            Language.setMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?")
			Language.setMessage(mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process.")
			Language.setMessage(mstrModuleName, 8, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process.")
			Language.setMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination.")
			Language.setMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date.")
			Language.setMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration.")
			Language.setMessage(mstrModuleName, 100, "Sorry, Department is mandatory information. Please select Department to continue.")
			Language.setMessage("clsemployee_transfer_tran", 1, "Sorry, transfer information is already present for the selected effective date.")
			Language.setMessage("clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class