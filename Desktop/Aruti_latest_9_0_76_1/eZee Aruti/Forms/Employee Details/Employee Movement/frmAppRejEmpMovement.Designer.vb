﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppRejEmpMovement
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAppRejEmpMovement))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtApprover = New System.Windows.Forms.TextBox
        Me.txtLevel = New System.Windows.Forms.TextBox
        Me.lblLevel = New System.Windows.Forms.Label
        Me.lblLoggedInUser = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.btnShowMyReport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhicheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhename = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEffDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdeptgroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhsecgroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhsection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhunitgrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhunit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhteam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhclassgrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhclass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdDate1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdDate2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhwork_permit_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhissue_place = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDispValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboMovement = New System.Windows.Forms.ComboBox
        Me.lblMovement = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.cboOperationType = New System.Windows.Forms.ComboBox
        Me.lblSelectOperationType = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefemailFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtApprover)
        Me.gbFilterCriteria.Controls.Add(Me.txtLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoggedInUser)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 100
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(792, 65)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.SystemColors.Info
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.Location = New System.Drawing.Point(434, 34)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(192, 21)
        Me.txtApprover.TabIndex = 109
        '
        'txtLevel
        '
        Me.txtLevel.BackColor = System.Drawing.SystemColors.Info
        Me.txtLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLevel.Location = New System.Drawing.Point(684, 34)
        Me.txtLevel.Name = "txtLevel"
        Me.txtLevel.ReadOnly = True
        Me.txtLevel.Size = New System.Drawing.Size(96, 21)
        Me.txtLevel.TabIndex = 1
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(632, 37)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(46, 15)
        Me.lblLevel.TabIndex = 108
        Me.lblLevel.Text = "Level"
        '
        'lblLoggedInUser
        '
        Me.lblLoggedInUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoggedInUser.Location = New System.Drawing.Point(363, 37)
        Me.lblLoggedInUser.Name = "lblLoggedInUser"
        Me.lblLoggedInUser.Size = New System.Drawing.Size(65, 15)
        Me.lblLoggedInUser.TabIndex = 107
        Me.lblLoggedInUser.Text = "Approver"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(336, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 105
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(77, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(253, 21)
        Me.cboEmployee.TabIndex = 104
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(10, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(61, 15)
        Me.lblEmployee.TabIndex = 103
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(767, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 95
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(743, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 94
        Me.objbtnSearch.TabStop = False
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(25, 383)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(68, 15)
        Me.lblRemark.TabIndex = 106
        Me.lblRemark.Text = "Remark"
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(99, 380)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(705, 45)
        Me.txtRemark.TabIndex = 105
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.btnShowMyReport)
        Me.objefemailFooter.Controls.Add(Me.btnApprove)
        Me.objefemailFooter.Controls.Add(Me.btnReject)
        Me.objefemailFooter.Controls.Add(Me.btnClose)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 431)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(816, 55)
        Me.objefemailFooter.TabIndex = 107
        '
        'btnShowMyReport
        '
        Me.btnShowMyReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShowMyReport.BackColor = System.Drawing.Color.White
        Me.btnShowMyReport.BackgroundImage = CType(resources.GetObject("btnShowMyReport.BackgroundImage"), System.Drawing.Image)
        Me.btnShowMyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnShowMyReport.BorderColor = System.Drawing.Color.Empty
        Me.btnShowMyReport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnShowMyReport.FlatAppearance.BorderSize = 0
        Me.btnShowMyReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShowMyReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowMyReport.ForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnShowMyReport.GradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowMyReport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.Location = New System.Drawing.Point(212, 13)
        Me.btnShowMyReport.Name = "btnShowMyReport"
        Me.btnShowMyReport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowMyReport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.Size = New System.Drawing.Size(94, 30)
        Me.btnShowMyReport.TabIndex = 86
        Me.btnShowMyReport.Text = "&My Report"
        Me.btnShowMyReport.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(12, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(94, 30)
        Me.btnApprove.TabIndex = 85
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(112, 13)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(94, 30)
        Me.btnReject.TabIndex = 85
        Me.btnReject.Text = "&Reject"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(710, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(19, 116)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 108
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhicheck, Me.dgcolhecode, Me.dgcolhename, Me.dgcolhEffDate, Me.dgcolhbranch, Me.dgcolhdeptgroup, Me.dgcolhdept, Me.dgcolhsecgroup, Me.dgcolhsection, Me.dgcolhunitgrp, Me.dgcolhunit, Me.dgcolhteam, Me.dgcolhclassgrp, Me.dgcolhclass, Me.dgcolhCReason, Me.dgcolhJobGroup, Me.dgcolhJob, Me.dgcolhdDate1, Me.dgcolhdDate2, Me.dgcolhwork_permit_no, Me.dgcolhissue_place, Me.dgcolhIDate, Me.dgcolhExDate, Me.dgcolhCountry, Me.dgcolhDispValue})
        Me.dgvData.Location = New System.Drawing.Point(12, 110)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(792, 264)
        Me.dgvData.TabIndex = 109
        '
        'objdgcolhicheck
        '
        Me.objdgcolhicheck.Frozen = True
        Me.objdgcolhicheck.HeaderText = ""
        Me.objdgcolhicheck.Name = "objdgcolhicheck"
        Me.objdgcolhicheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhicheck.Width = 25
        '
        'dgcolhecode
        '
        Me.dgcolhecode.HeaderText = "Code"
        Me.dgcolhecode.Name = "dgcolhecode"
        Me.dgcolhecode.ReadOnly = True
        Me.dgcolhecode.Width = 80
        '
        'dgcolhename
        '
        Me.dgcolhename.HeaderText = "Employee"
        Me.dgcolhename.Name = "dgcolhename"
        Me.dgcolhename.ReadOnly = True
        Me.dgcolhename.Width = 150
        '
        'dgcolhEffDate
        '
        Me.dgcolhEffDate.HeaderText = "Effective Date"
        Me.dgcolhEffDate.Name = "dgcolhEffDate"
        Me.dgcolhEffDate.ReadOnly = True
        '
        'dgcolhbranch
        '
        Me.dgcolhbranch.HeaderText = "Branch"
        Me.dgcolhbranch.Name = "dgcolhbranch"
        Me.dgcolhbranch.ReadOnly = True
        '
        'dgcolhdeptgroup
        '
        Me.dgcolhdeptgroup.HeaderText = "Department Group"
        Me.dgcolhdeptgroup.Name = "dgcolhdeptgroup"
        Me.dgcolhdeptgroup.ReadOnly = True
        '
        'dgcolhdept
        '
        Me.dgcolhdept.HeaderText = "Department"
        Me.dgcolhdept.Name = "dgcolhdept"
        Me.dgcolhdept.ReadOnly = True
        '
        'dgcolhsecgroup
        '
        Me.dgcolhsecgroup.HeaderText = "Section Group"
        Me.dgcolhsecgroup.Name = "dgcolhsecgroup"
        Me.dgcolhsecgroup.ReadOnly = True
        '
        'dgcolhsection
        '
        Me.dgcolhsection.HeaderText = "Section"
        Me.dgcolhsection.Name = "dgcolhsection"
        Me.dgcolhsection.ReadOnly = True
        '
        'dgcolhunitgrp
        '
        Me.dgcolhunitgrp.HeaderText = "Unit Group"
        Me.dgcolhunitgrp.Name = "dgcolhunitgrp"
        Me.dgcolhunitgrp.ReadOnly = True
        '
        'dgcolhunit
        '
        Me.dgcolhunit.HeaderText = "Unit"
        Me.dgcolhunit.Name = "dgcolhunit"
        Me.dgcolhunit.ReadOnly = True
        '
        'dgcolhteam
        '
        Me.dgcolhteam.HeaderText = "Team"
        Me.dgcolhteam.Name = "dgcolhteam"
        Me.dgcolhteam.ReadOnly = True
        '
        'dgcolhclassgrp
        '
        Me.dgcolhclassgrp.HeaderText = "Class Group"
        Me.dgcolhclassgrp.Name = "dgcolhclassgrp"
        Me.dgcolhclassgrp.ReadOnly = True
        '
        'dgcolhclass
        '
        Me.dgcolhclass.HeaderText = "Class"
        Me.dgcolhclass.Name = "dgcolhclass"
        Me.dgcolhclass.ReadOnly = True
        '
        'dgcolhCReason
        '
        Me.dgcolhCReason.HeaderText = "Reason"
        Me.dgcolhCReason.Name = "dgcolhCReason"
        Me.dgcolhCReason.ReadOnly = True
        '
        'dgcolhJobGroup
        '
        Me.dgcolhJobGroup.HeaderText = "Job Group"
        Me.dgcolhJobGroup.Name = "dgcolhJobGroup"
        Me.dgcolhJobGroup.ReadOnly = True
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        '
        'dgcolhdDate1
        '
        Me.dgcolhdDate1.HeaderText = "Date1"
        Me.dgcolhdDate1.Name = "dgcolhdDate1"
        Me.dgcolhdDate1.ReadOnly = True
        '
        'dgcolhdDate2
        '
        Me.dgcolhdDate2.HeaderText = "Date2"
        Me.dgcolhdDate2.Name = "dgcolhdDate2"
        Me.dgcolhdDate2.ReadOnly = True
        '
        'dgcolhwork_permit_no
        '
        Me.dgcolhwork_permit_no.HeaderText = "Permit No."
        Me.dgcolhwork_permit_no.Name = "dgcolhwork_permit_no"
        Me.dgcolhwork_permit_no.ReadOnly = True
        '
        'dgcolhissue_place
        '
        Me.dgcolhissue_place.HeaderText = "Issue Place"
        Me.dgcolhissue_place.Name = "dgcolhissue_place"
        Me.dgcolhissue_place.ReadOnly = True
        '
        'dgcolhIDate
        '
        Me.dgcolhIDate.HeaderText = "Issue Date"
        Me.dgcolhIDate.Name = "dgcolhIDate"
        Me.dgcolhIDate.ReadOnly = True
        '
        'dgcolhExDate
        '
        Me.dgcolhExDate.HeaderText = "Expiry Date"
        Me.dgcolhExDate.Name = "dgcolhExDate"
        Me.dgcolhExDate.ReadOnly = True
        '
        'dgcolhCountry
        '
        Me.dgcolhCountry.HeaderText = "Country"
        Me.dgcolhCountry.Name = "dgcolhCountry"
        Me.dgcolhCountry.ReadOnly = True
        '
        'dgcolhDispValue
        '
        Me.dgcolhDispValue.HeaderText = "CostCenter"
        Me.dgcolhDispValue.Name = "dgcolhDispValue"
        Me.dgcolhDispValue.ReadOnly = True
        '
        'cboMovement
        '
        Me.cboMovement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMovement.DropDownWidth = 400
        Me.cboMovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMovement.FormattingEnabled = True
        Me.cboMovement.Location = New System.Drawing.Point(394, 83)
        Me.cboMovement.Name = "cboMovement"
        Me.cboMovement.Size = New System.Drawing.Size(177, 21)
        Me.cboMovement.TabIndex = 110
        '
        'lblMovement
        '
        Me.lblMovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMovement.Location = New System.Drawing.Point(290, 86)
        Me.lblMovement.Name = "lblMovement"
        Me.lblMovement.Size = New System.Drawing.Size(98, 15)
        Me.lblMovement.TabIndex = 113
        Me.lblMovement.Text = "Select Movement"
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(12, 83)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(272, 21)
        Me.txtSearch.TabIndex = 110
        '
        'cboOperationType
        '
        Me.cboOperationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperationType.DropDownWidth = 400
        Me.cboOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperationType.FormattingEnabled = True
        Me.cboOperationType.Location = New System.Drawing.Point(673, 83)
        Me.cboOperationType.Name = "cboOperationType"
        Me.cboOperationType.Size = New System.Drawing.Size(131, 21)
        Me.cboOperationType.TabIndex = 114
        '
        'lblSelectOperationType
        '
        Me.lblSelectOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectOperationType.Location = New System.Drawing.Point(577, 86)
        Me.lblSelectOperationType.Name = "lblSelectOperationType"
        Me.lblSelectOperationType.Size = New System.Drawing.Size(90, 15)
        Me.lblSelectOperationType.TabIndex = 115
        Me.lblSelectOperationType.Text = "Operation Type"
        '
        'frmAppRejEmpMovement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 486)
        Me.Controls.Add(Me.lblSelectOperationType)
        Me.Controls.Add(Me.cboOperationType)
        Me.Controls.Add(Me.lblMovement)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.cboMovement)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.objefemailFooter)
        Me.Controls.Add(Me.lblRemark)
        Me.Controls.Add(Me.txtRemark)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppRejEmpMovement"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appove/Reject Employee Movement"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefemailFooter.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtApprover As System.Windows.Forms.TextBox
    Friend WithEvents txtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents lblLoggedInUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnShowMyReport As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents cboMovement As System.Windows.Forms.ComboBox
    Friend WithEvents lblMovement As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhicheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEffDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhbranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdeptgroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhsecgroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhsection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhunitgrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhunit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhteam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhclassgrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhclass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdDate1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdDate2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhwork_permit_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhissue_place As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDispValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboOperationType As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectOperationType As System.Windows.Forms.Label
End Class
