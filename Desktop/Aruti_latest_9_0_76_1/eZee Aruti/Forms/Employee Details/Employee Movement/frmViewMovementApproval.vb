﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmViewMovementApproval

#Region " Private Varaibles "

    Private objApprovalTran As New clsEmployeeMovmentApproval
    Private ReadOnly mstrModuleName As String = "frmViewMovementApproval"
    Private mintUserId As Integer
    Private mintPriority As Integer
    Private mintPrivilegeId As Integer
    Private meFillType As clsEmployeeMovmentApproval.enMovementType
    Private meDtType As enEmp_Dates_Transaction
    Private mblnIsResidentPermit As Boolean
    'S.SANDEEP [09-AUG-2018] -- START
    Private mstrFilterString As String = String.Empty
    Private mblnFromApprovalScreen As Boolean = True
    'S.SANDEEP [09-AUG-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    Private meOperation As clsEmployeeMovmentApproval.enOperationType
    'S.SANDEEP |17-JAN-2019| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUserId As Integer, _
                                  ByVal intPriority As Integer, _
                                  ByVal intPrivilegeId As Integer, _
                                  ByVal eFillType As clsEmployeeMovmentApproval.enMovementType, _
                                  ByVal eDtType As enEmp_Dates_Transaction, _
                                  ByVal blnIsResidentPermit As Boolean, _
                                  ByVal eOperationType As clsEmployeeMovmentApproval.enOperationType, _
                                  Optional ByVal strFilter As String = "", _
                                  Optional ByVal blnFromApprovalScreen As Boolean = True) As Boolean 'S.SANDEEP [09-AUG-2018] -- START -- END
        'S.SANDEEP |17-JAN-2019| -- START {eOperationType} -- END
        Try
            mintUserId = intUserId
            mintPriority = intPriority
            mintPrivilegeId = intPrivilegeId
            meFillType = eFillType
            meDtType = eDtType
            mblnIsResidentPermit = blnIsResidentPermit
            'S.SANDEEP |17-JAN-2019| -- START
            meOperation = eOperationType
            'S.SANDEEP |17-JAN-2019| -- END
            mstrFilterString = strFilter 'S.SANDEEP [09-AUG-2018] -- START -- END
            mblnFromApprovalScreen = blnFromApprovalScreen 'S.SANDEEP [09-AUG-2018] -- START -- END
            If mblnFromApprovalScreen = False Then
                Me.Text = Language.getMessage(mstrModuleName, 1, "Approval Status")
            Else
                Me.Text = Language.getMessage(mstrModuleName, 2, "My Report")
            End If
            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillGrid()
        Try
            'S.SANDEEP |17-JAN-2019| -- START
            'Dim dt As DataTable = objApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, mintPrivilegeId, meFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, meDtType, mblnIsResidentPermit, Nothing, mintPriority, True, mstrFilterString) 'S.SANDEEP [09-AUG-2018] -- START {mstrFilterString} -- END
            Dim dt As DataTable = objApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, mintPrivilegeId, meFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, meDtType, mblnIsResidentPermit, meOperation, Nothing, mintPriority, True, mstrFilterString)
            'S.SANDEEP |17-JAN-2019| -- END


            'S.SANDEEP [09-AUG-2018] -- START
            'dt = New DataView(dt, "priority <= " & mintPriority, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            If mblnFromApprovalScreen Then
            dt = New DataView(dt, "priority <= " & mintPriority, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If
            'S.SANDEEP [09-AUG-2018] -- END


            SetGridColums()

            dgvData.AutoGenerateColumns = False

            objdgcolhicheck.DataPropertyName = "icheck"
            dgcolhecode.DataPropertyName = "ecode"
            dgcolhename.DataPropertyName = "ename"
            dgcolhEffDate.DataPropertyName = "EffDate"
            dgcolhCReason.DataPropertyName = "CReason"

            dgcolhApprover.DataPropertyName = "username"
            dgcolhLevel.DataPropertyName = "levelname"
            dgcolhStatus.DataPropertyName = "iStatus"

            dgcolhbranch.DataPropertyName = "branch"
            dgcolhdeptgroup.DataPropertyName = "deptgroup"
            dgcolhdept.DataPropertyName = "dept"
            dgcolhsecgroup.DataPropertyName = "secgroup"
            dgcolhsection.DataPropertyName = "section"
            dgcolhunitgrp.DataPropertyName = "unitgrp"
            dgcolhunit.DataPropertyName = "unit"
            dgcolhteam.DataPropertyName = "team"
            dgcolhclassgrp.DataPropertyName = "classgrp"
            dgcolhclass.DataPropertyName = "class"

            dgcolhJobGroup.DataPropertyName = "JobGroup"
            dgcolhJob.DataPropertyName = "Job"

            dgcolhdDate1.DataPropertyName = "dDate1"
            dgcolhdDate2.DataPropertyName = "dDate2"

            dgcolhwork_permit_no.DataPropertyName = "work_permit_no"
            dgcolhissue_place.DataPropertyName = "issue_place"
            dgcolhIDate.DataPropertyName = "IDate"
            dgcolhExDate.DataPropertyName = "EDate"
            dgcolhCountry.DataPropertyName = "Country"

            dgcolhDispValue.DataPropertyName = "DispValue"

            dgvData.DataSource = dt

            Select Case meFillType
                Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                    dgcolhdDate1.Visible = True
                    dgcolhdDate2.Visible = False
                    dgcolhdDate1.HeaderText = Language.getMessage("frmAppRejEmpMovement", 102, "Confirmation Date")

                Case clsEmployeeMovmentApproval.enMovementType.PROBATION
                    dgcolhdDate1.Visible = True
                    dgcolhdDate2.Visible = True
                    dgcolhdDate1.HeaderText = Language.getMessage("frmAppRejEmpMovement", 100, "Start Date")
                    dgcolhdDate2.HeaderText = Language.getMessage("frmAppRejEmpMovement", 101, "End Date")

                Case clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    dgcolhdDate1.Visible = True
                    dgcolhdDate2.Visible = False
                    dgcolhdDate1.HeaderText = Language.getMessage("frmAppRejEmpMovement", 103, "Retirement Date")

                Case clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    dgcolhdDate1.Visible = True
                    dgcolhdDate2.Visible = True
                    dgcolhdDate1.HeaderText = Language.getMessage("frmAppRejEmpMovement", 100, "Start Date")
                    dgcolhdDate2.HeaderText = Language.getMessage("frmAppRejEmpMovement", 101, "End Date")

                Case clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    dgcolhdDate1.Visible = True
                    dgcolhdDate2.Visible = True
                    dgcolhdDate1.HeaderText = Language.getMessage("frmAppRejEmpMovement", 104, "EOC Date")
                    dgcolhdDate2.HeaderText = Language.getMessage("frmAppRejEmpMovement", 105, "Leaving Date")

                Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                    dgcolhDispValue.Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                    dgcolhJobGroup.Visible = True
                    dgcolhJob.Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    dgcolhwork_permit_no.Visible = True
                    dgcolhissue_place.Visible = True
                    dgcolhIDate.Visible = True
                    dgcolhExDate.Visible = True
                    dgcolhCountry.Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                    dgcolhbranch.Visible = True
                    dgcolhdeptgroup.Visible = True
                    dgcolhdept.Visible = True
                    dgcolhsecgroup.Visible = True
                    dgcolhsection.Visible = True
                    dgcolhunitgrp.Visible = True
                    dgcolhunit.Visible = True
                    dgcolhteam.Visible = True
                    dgcolhclassgrp.Visible = True
                    dgcolhclass.Visible = True

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColums()
        Try
            dgcolhdDate1.Visible = False : dgcolhdDate2.Visible = False
            dgcolhDispValue.Visible = False : dgcolhJobGroup.Visible = False
            dgcolhJob.Visible = False : dgcolhwork_permit_no.Visible = False
            dgcolhissue_place.Visible = False : dgcolhIDate.Visible = False
            dgcolhExDate.Visible = False : dgcolhCountry.Visible = False
            dgcolhbranch.Visible = False : dgcolhdeptgroup.Visible = False
            dgcolhdept.Visible = False : dgcolhsecgroup.Visible = False
            dgcolhsection.Visible = False : dgcolhunitgrp.Visible = False
            dgcolhunit.Visible = False : dgcolhteam.Visible = False
            dgcolhclassgrp.Visible = False : dgcolhclass.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColums", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmViewMovementApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprovalTran = New clsEmployeeMovmentApproval
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmViewMovementApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeMovmentApproval.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeMovmentApproval"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            'Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.dgcolhecode.HeaderText = Language._Object.getCaption(Me.dgcolhecode.Name, Me.dgcolhecode.HeaderText)
            Me.dgcolhename.HeaderText = Language._Object.getCaption(Me.dgcolhename.Name, Me.dgcolhename.HeaderText)
            Me.dgcolhEffDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffDate.Name, Me.dgcolhEffDate.HeaderText)
            Me.dgcolhbranch.HeaderText = Language._Object.getCaption(Me.dgcolhbranch.Name, Me.dgcolhbranch.HeaderText)
            Me.dgcolhdeptgroup.HeaderText = Language._Object.getCaption(Me.dgcolhdeptgroup.Name, Me.dgcolhdeptgroup.HeaderText)
            Me.dgcolhdept.HeaderText = Language._Object.getCaption(Me.dgcolhdept.Name, Me.dgcolhdept.HeaderText)
            Me.dgcolhsecgroup.HeaderText = Language._Object.getCaption(Me.dgcolhsecgroup.Name, Me.dgcolhsecgroup.HeaderText)
            Me.dgcolhsection.HeaderText = Language._Object.getCaption(Me.dgcolhsection.Name, Me.dgcolhsection.HeaderText)
            Me.dgcolhunitgrp.HeaderText = Language._Object.getCaption(Me.dgcolhunitgrp.Name, Me.dgcolhunitgrp.HeaderText)
            Me.dgcolhunit.HeaderText = Language._Object.getCaption(Me.dgcolhunit.Name, Me.dgcolhunit.HeaderText)
            Me.dgcolhteam.HeaderText = Language._Object.getCaption(Me.dgcolhteam.Name, Me.dgcolhteam.HeaderText)
            Me.dgcolhclassgrp.HeaderText = Language._Object.getCaption(Me.dgcolhclassgrp.Name, Me.dgcolhclassgrp.HeaderText)
            Me.dgcolhclass.HeaderText = Language._Object.getCaption(Me.dgcolhclass.Name, Me.dgcolhclass.HeaderText)
            Me.dgcolhCReason.HeaderText = Language._Object.getCaption(Me.dgcolhCReason.Name, Me.dgcolhCReason.HeaderText)
            Me.dgcolhJobGroup.HeaderText = Language._Object.getCaption(Me.dgcolhJobGroup.Name, Me.dgcolhJobGroup.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhdDate1.HeaderText = Language._Object.getCaption(Me.dgcolhdDate1.Name, Me.dgcolhdDate1.HeaderText)
            Me.dgcolhdDate2.HeaderText = Language._Object.getCaption(Me.dgcolhdDate2.Name, Me.dgcolhdDate2.HeaderText)
            Me.dgcolhwork_permit_no.HeaderText = Language._Object.getCaption(Me.dgcolhwork_permit_no.Name, Me.dgcolhwork_permit_no.HeaderText)
            Me.dgcolhissue_place.HeaderText = Language._Object.getCaption(Me.dgcolhissue_place.Name, Me.dgcolhissue_place.HeaderText)
            Me.dgcolhIDate.HeaderText = Language._Object.getCaption(Me.dgcolhIDate.Name, Me.dgcolhIDate.HeaderText)
            Me.dgcolhExDate.HeaderText = Language._Object.getCaption(Me.dgcolhExDate.Name, Me.dgcolhExDate.HeaderText)
            Me.dgcolhCountry.HeaderText = Language._Object.getCaption(Me.dgcolhCountry.Name, Me.dgcolhCountry.HeaderText)
            Me.dgcolhDispValue.HeaderText = Language._Object.getCaption(Me.dgcolhDispValue.Name, Me.dgcolhDispValue.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhLevel.HeaderText = Language._Object.getCaption(Me.dgcolhLevel.Name, Me.dgcolhLevel.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmAppRejEmpMovement", 100, "Start Date")
            Language.setMessage("frmAppRejEmpMovement", 101, "End Date")
            Language.setMessage("frmAppRejEmpMovement", 102, "Confirmation Date")
            Language.setMessage("frmAppRejEmpMovement", 103, "Retirement Date")
            Language.setMessage("frmAppRejEmpMovement", 104, "EOC Date")
            Language.setMessage("frmAppRejEmpMovement", 105, "Leaving Date")
            Language.setMessage(mstrModuleName, 1, "Approval Status")
            Language.setMessage(mstrModuleName, 2, "My Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class