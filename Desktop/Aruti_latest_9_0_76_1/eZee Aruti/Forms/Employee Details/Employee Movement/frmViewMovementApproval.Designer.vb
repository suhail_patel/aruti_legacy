﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewMovementApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewMovementApproval))
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhicheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhename = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEffDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdeptgroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhsecgroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhsection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhunitgrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhunit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhteam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhclassgrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhclass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdDate1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhdDate2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhwork_permit_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhissue_place = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDispValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefemailFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.btnClose)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 413)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(849, 55)
        Me.objefemailFooter.TabIndex = 10
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(743, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhicheck, Me.dgcolhecode, Me.dgcolhename, Me.dgcolhEffDate, Me.dgcolhbranch, Me.dgcolhdeptgroup, Me.dgcolhdept, Me.dgcolhsecgroup, Me.dgcolhsection, Me.dgcolhunitgrp, Me.dgcolhunit, Me.dgcolhteam, Me.dgcolhclassgrp, Me.dgcolhclass, Me.dgcolhCReason, Me.dgcolhJobGroup, Me.dgcolhJob, Me.dgcolhdDate1, Me.dgcolhdDate2, Me.dgcolhwork_permit_no, Me.dgcolhissue_place, Me.dgcolhIDate, Me.dgcolhExDate, Me.dgcolhCountry, Me.dgcolhDispValue, Me.dgcolhApprover, Me.dgcolhLevel, Me.dgcolhStatus})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(849, 413)
        Me.dgvData.TabIndex = 110
        '
        'objdgcolhicheck
        '
        Me.objdgcolhicheck.Frozen = True
        Me.objdgcolhicheck.HeaderText = ""
        Me.objdgcolhicheck.Name = "objdgcolhicheck"
        Me.objdgcolhicheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhicheck.Visible = False
        Me.objdgcolhicheck.Width = 25
        '
        'dgcolhecode
        '
        Me.dgcolhecode.HeaderText = "Code"
        Me.dgcolhecode.Name = "dgcolhecode"
        Me.dgcolhecode.ReadOnly = True
        Me.dgcolhecode.Width = 80
        '
        'dgcolhename
        '
        Me.dgcolhename.HeaderText = "Employee"
        Me.dgcolhename.Name = "dgcolhename"
        Me.dgcolhename.ReadOnly = True
        Me.dgcolhename.Width = 150
        '
        'dgcolhEffDate
        '
        Me.dgcolhEffDate.HeaderText = "Effective Date"
        Me.dgcolhEffDate.Name = "dgcolhEffDate"
        Me.dgcolhEffDate.ReadOnly = True
        '
        'dgcolhbranch
        '
        Me.dgcolhbranch.HeaderText = "Branch"
        Me.dgcolhbranch.Name = "dgcolhbranch"
        Me.dgcolhbranch.ReadOnly = True
        '
        'dgcolhdeptgroup
        '
        Me.dgcolhdeptgroup.HeaderText = "Department Group"
        Me.dgcolhdeptgroup.Name = "dgcolhdeptgroup"
        Me.dgcolhdeptgroup.ReadOnly = True
        '
        'dgcolhdept
        '
        Me.dgcolhdept.HeaderText = "Department"
        Me.dgcolhdept.Name = "dgcolhdept"
        Me.dgcolhdept.ReadOnly = True
        '
        'dgcolhsecgroup
        '
        Me.dgcolhsecgroup.HeaderText = "Section Group"
        Me.dgcolhsecgroup.Name = "dgcolhsecgroup"
        Me.dgcolhsecgroup.ReadOnly = True
        '
        'dgcolhsection
        '
        Me.dgcolhsection.HeaderText = "Section"
        Me.dgcolhsection.Name = "dgcolhsection"
        Me.dgcolhsection.ReadOnly = True
        '
        'dgcolhunitgrp
        '
        Me.dgcolhunitgrp.HeaderText = "Unit Group"
        Me.dgcolhunitgrp.Name = "dgcolhunitgrp"
        Me.dgcolhunitgrp.ReadOnly = True
        '
        'dgcolhunit
        '
        Me.dgcolhunit.HeaderText = "Unit"
        Me.dgcolhunit.Name = "dgcolhunit"
        Me.dgcolhunit.ReadOnly = True
        '
        'dgcolhteam
        '
        Me.dgcolhteam.HeaderText = "Team"
        Me.dgcolhteam.Name = "dgcolhteam"
        Me.dgcolhteam.ReadOnly = True
        '
        'dgcolhclassgrp
        '
        Me.dgcolhclassgrp.HeaderText = "Class Group"
        Me.dgcolhclassgrp.Name = "dgcolhclassgrp"
        Me.dgcolhclassgrp.ReadOnly = True
        '
        'dgcolhclass
        '
        Me.dgcolhclass.HeaderText = "Class"
        Me.dgcolhclass.Name = "dgcolhclass"
        Me.dgcolhclass.ReadOnly = True
        '
        'dgcolhCReason
        '
        Me.dgcolhCReason.HeaderText = "Reason"
        Me.dgcolhCReason.Name = "dgcolhCReason"
        Me.dgcolhCReason.ReadOnly = True
        '
        'dgcolhJobGroup
        '
        Me.dgcolhJobGroup.HeaderText = "Job Group"
        Me.dgcolhJobGroup.Name = "dgcolhJobGroup"
        Me.dgcolhJobGroup.ReadOnly = True
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        '
        'dgcolhdDate1
        '
        Me.dgcolhdDate1.HeaderText = "Date1"
        Me.dgcolhdDate1.Name = "dgcolhdDate1"
        Me.dgcolhdDate1.ReadOnly = True
        '
        'dgcolhdDate2
        '
        Me.dgcolhdDate2.HeaderText = "Date2"
        Me.dgcolhdDate2.Name = "dgcolhdDate2"
        Me.dgcolhdDate2.ReadOnly = True
        '
        'dgcolhwork_permit_no
        '
        Me.dgcolhwork_permit_no.HeaderText = "Permit No."
        Me.dgcolhwork_permit_no.Name = "dgcolhwork_permit_no"
        Me.dgcolhwork_permit_no.ReadOnly = True
        '
        'dgcolhissue_place
        '
        Me.dgcolhissue_place.HeaderText = "Issue Place"
        Me.dgcolhissue_place.Name = "dgcolhissue_place"
        Me.dgcolhissue_place.ReadOnly = True
        '
        'dgcolhIDate
        '
        Me.dgcolhIDate.HeaderText = "Issue Date"
        Me.dgcolhIDate.Name = "dgcolhIDate"
        Me.dgcolhIDate.ReadOnly = True
        '
        'dgcolhExDate
        '
        Me.dgcolhExDate.HeaderText = "Expiry Date"
        Me.dgcolhExDate.Name = "dgcolhExDate"
        Me.dgcolhExDate.ReadOnly = True
        '
        'dgcolhCountry
        '
        Me.dgcolhCountry.HeaderText = "Country"
        Me.dgcolhCountry.Name = "dgcolhCountry"
        Me.dgcolhCountry.ReadOnly = True
        '
        'dgcolhDispValue
        '
        Me.dgcolhDispValue.HeaderText = "CostCenter"
        Me.dgcolhDispValue.Name = "dgcolhDispValue"
        Me.dgcolhDispValue.ReadOnly = True
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        '
        'dgcolhLevel
        '
        Me.dgcolhLevel.HeaderText = "Level"
        Me.dgcolhLevel.Name = "dgcolhLevel"
        Me.dgcolhLevel.ReadOnly = True
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        '
        'frmViewMovementApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 468)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.objefemailFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewMovementApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.objefemailFooter.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhicheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEffDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhbranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdeptgroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhsecgroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhsection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhunitgrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhunit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhteam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhclassgrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhclass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdDate1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhdDate2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhwork_permit_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhissue_place As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDispValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
