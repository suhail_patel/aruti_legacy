﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSalaryIncrementAnniversary
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSalaryIncrementAnniversary))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbMonthAssignment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radConfirmationMonth = New System.Windows.Forms.RadioButton
        Me.btnApplytoChecked = New eZee.Common.eZeeLightButton(Me.components)
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.nudDefinedMonth = New System.Windows.Forms.NumericUpDown
        Me.radDefinedMonth = New System.Windows.Forms.RadioButton
        Me.radAppointmentMonth = New System.Windows.Forms.RadioButton
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.pnlSalaryAnniversary = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvSalaryAnniversary = New System.Windows.Forms.DataGridView
        Me.objcolhCheckAll = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhEmployeeUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAppointmentDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAnniversaryMonth = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhNewEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhNewAnniversaryMonth = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.chkNeverAssignedOnly = New System.Windows.Forms.CheckBox
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.lblToMonth = New System.Windows.Forms.Label
        Me.lblFromMonth = New System.Windows.Forms.Label
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.lblToConfirm = New System.Windows.Forms.Label
        Me.dtpToConfirm = New System.Windows.Forms.DateTimePicker
        Me.lblFromConfirm = New System.Windows.Forms.Label
        Me.dtpFromConfirm = New System.Windows.Forms.DateTimePicker
        Me.lblToAppointment = New System.Windows.Forms.Label
        Me.dtpToAppointment = New System.Windows.Forms.DateTimePicker
        Me.lblFromAppointment = New System.Windows.Forms.Label
        Me.dtpFromAppointment = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbMonthAssignment.SuspendLayout()
        CType(Me.nudDefinedMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSalaryAnniversary.SuspendLayout()
        CType(Me.dgvSalaryAnniversary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbMonthAssignment)
        Me.pnlMain.Controls.Add(Me.pnlSalaryAnniversary)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(766, 582)
        Me.pnlMain.TabIndex = 0
        '
        'gbMonthAssignment
        '
        Me.gbMonthAssignment.BorderColor = System.Drawing.Color.Black
        Me.gbMonthAssignment.Checked = False
        Me.gbMonthAssignment.CollapseAllExceptThis = False
        Me.gbMonthAssignment.CollapsedHoverImage = Nothing
        Me.gbMonthAssignment.CollapsedNormalImage = Nothing
        Me.gbMonthAssignment.CollapsedPressedImage = Nothing
        Me.gbMonthAssignment.CollapseOnLoad = False
        Me.gbMonthAssignment.Controls.Add(Me.radConfirmationMonth)
        Me.gbMonthAssignment.Controls.Add(Me.btnApplytoChecked)
        Me.gbMonthAssignment.Controls.Add(Me.dtpEffectiveDate)
        Me.gbMonthAssignment.Controls.Add(Me.nudDefinedMonth)
        Me.gbMonthAssignment.Controls.Add(Me.radDefinedMonth)
        Me.gbMonthAssignment.Controls.Add(Me.radAppointmentMonth)
        Me.gbMonthAssignment.Controls.Add(Me.lblGradeGroup)
        Me.gbMonthAssignment.ExpandedHoverImage = Nothing
        Me.gbMonthAssignment.ExpandedNormalImage = Nothing
        Me.gbMonthAssignment.ExpandedPressedImage = Nothing
        Me.gbMonthAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMonthAssignment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMonthAssignment.HeaderHeight = 25
        Me.gbMonthAssignment.HeaderMessage = ""
        Me.gbMonthAssignment.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMonthAssignment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMonthAssignment.HeightOnCollapse = 0
        Me.gbMonthAssignment.LeftTextSpace = 0
        Me.gbMonthAssignment.Location = New System.Drawing.Point(12, 135)
        Me.gbMonthAssignment.Name = "gbMonthAssignment"
        Me.gbMonthAssignment.OpenHeight = 90
        Me.gbMonthAssignment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMonthAssignment.ShowBorder = True
        Me.gbMonthAssignment.ShowCheckBox = False
        Me.gbMonthAssignment.ShowCollapseButton = False
        Me.gbMonthAssignment.ShowDefaultBorderColor = True
        Me.gbMonthAssignment.ShowDownButton = False
        Me.gbMonthAssignment.ShowHeader = True
        Me.gbMonthAssignment.Size = New System.Drawing.Size(741, 66)
        Me.gbMonthAssignment.TabIndex = 13
        Me.gbMonthAssignment.Temp = 0
        Me.gbMonthAssignment.Text = "Anniversary Month Assignment"
        Me.gbMonthAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radConfirmationMonth
        '
        Me.radConfirmationMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radConfirmationMonth.Location = New System.Drawing.Point(321, 35)
        Me.radConfirmationMonth.Name = "radConfirmationMonth"
        Me.radConfirmationMonth.Size = New System.Drawing.Size(123, 17)
        Me.radConfirmationMonth.TabIndex = 153
        Me.radConfirmationMonth.Text = "Confirmation Month"
        Me.radConfirmationMonth.UseVisualStyleBackColor = True
        '
        'btnApplytoChecked
        '
        Me.btnApplytoChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApplytoChecked.BackColor = System.Drawing.Color.White
        Me.btnApplytoChecked.BackgroundImage = CType(resources.GetObject("btnApplytoChecked.BackgroundImage"), System.Drawing.Image)
        Me.btnApplytoChecked.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApplytoChecked.BorderColor = System.Drawing.Color.Empty
        Me.btnApplytoChecked.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApplytoChecked.FlatAppearance.BorderSize = 0
        Me.btnApplytoChecked.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApplytoChecked.ForeColor = System.Drawing.Color.Black
        Me.btnApplytoChecked.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApplytoChecked.GradientForeColor = System.Drawing.Color.Black
        Me.btnApplytoChecked.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplytoChecked.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApplytoChecked.Location = New System.Drawing.Point(618, 29)
        Me.btnApplytoChecked.Name = "btnApplytoChecked"
        Me.btnApplytoChecked.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplytoChecked.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApplytoChecked.Size = New System.Drawing.Size(109, 30)
        Me.btnApplytoChecked.TabIndex = 151
        Me.btnApplytoChecked.Text = "&Apply to Cheked"
        Me.btnApplytoChecked.UseVisualStyleBackColor = True
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(93, 33)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(93, 21)
        Me.dtpEffectiveDate.TabIndex = 150
        '
        'nudDefinedMonth
        '
        Me.nudDefinedMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDefinedMonth.Location = New System.Drawing.Point(553, 33)
        Me.nudDefinedMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudDefinedMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDefinedMonth.Name = "nudDefinedMonth"
        Me.nudDefinedMonth.Size = New System.Drawing.Size(50, 21)
        Me.nudDefinedMonth.TabIndex = 14
        Me.nudDefinedMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDefinedMonth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'radDefinedMonth
        '
        Me.radDefinedMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDefinedMonth.Location = New System.Drawing.Point(450, 35)
        Me.radDefinedMonth.Name = "radDefinedMonth"
        Me.radDefinedMonth.Size = New System.Drawing.Size(97, 17)
        Me.radDefinedMonth.TabIndex = 145
        Me.radDefinedMonth.Text = "Defined Month"
        Me.radDefinedMonth.UseVisualStyleBackColor = True
        '
        'radAppointmentMonth
        '
        Me.radAppointmentMonth.Checked = True
        Me.radAppointmentMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointmentMonth.Location = New System.Drawing.Point(192, 35)
        Me.radAppointmentMonth.Name = "radAppointmentMonth"
        Me.radAppointmentMonth.Size = New System.Drawing.Size(123, 17)
        Me.radAppointmentMonth.TabIndex = 144
        Me.radAppointmentMonth.TabStop = True
        Me.radAppointmentMonth.Text = "Appointment Month"
        Me.radAppointmentMonth.UseVisualStyleBackColor = True
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(79, 15)
        Me.lblGradeGroup.TabIndex = 29
        Me.lblGradeGroup.Text = "Effective Date"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSalaryAnniversary
        '
        Me.pnlSalaryAnniversary.Controls.Add(Me.chkSelectAll)
        Me.pnlSalaryAnniversary.Controls.Add(Me.dgvSalaryAnniversary)
        Me.pnlSalaryAnniversary.Location = New System.Drawing.Point(13, 207)
        Me.pnlSalaryAnniversary.Name = "pnlSalaryAnniversary"
        Me.pnlSalaryAnniversary.Size = New System.Drawing.Size(741, 313)
        Me.pnlSalaryAnniversary.TabIndex = 12
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(9, 10)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 14
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvSalaryAnniversary
        '
        Me.dgvSalaryAnniversary.AllowUserToAddRows = False
        Me.dgvSalaryAnniversary.AllowUserToDeleteRows = False
        Me.dgvSalaryAnniversary.BackgroundColor = System.Drawing.Color.White
        Me.dgvSalaryAnniversary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSalaryAnniversary.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheckAll, Me.objcolhEmployeeUnkId, Me.colhEmployeeCode, Me.colhEmployeeName, Me.colhAppointmentDate, Me.colhEffectiveDate, Me.colhAnniversaryMonth, Me.colhNewEffectiveDate, Me.colhNewAnniversaryMonth})
        Me.dgvSalaryAnniversary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSalaryAnniversary.Location = New System.Drawing.Point(0, 0)
        Me.dgvSalaryAnniversary.Name = "dgvSalaryAnniversary"
        Me.dgvSalaryAnniversary.RowHeadersVisible = False
        Me.dgvSalaryAnniversary.Size = New System.Drawing.Size(741, 313)
        Me.dgvSalaryAnniversary.TabIndex = 0
        '
        'objcolhCheckAll
        '
        DataGridViewCellStyle1.NullValue = False
        Me.objcolhCheckAll.DefaultCellStyle = DataGridViewCellStyle1
        Me.objcolhCheckAll.HeaderText = ""
        Me.objcolhCheckAll.Name = "objcolhCheckAll"
        Me.objcolhCheckAll.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhCheckAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhCheckAll.Width = 30
        '
        'objcolhEmployeeUnkId
        '
        Me.objcolhEmployeeUnkId.HeaderText = "Employee Id"
        Me.objcolhEmployeeUnkId.Name = "objcolhEmployeeUnkId"
        Me.objcolhEmployeeUnkId.ReadOnly = True
        Me.objcolhEmployeeUnkId.Visible = False
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.HeaderText = "Emp. Code"
        Me.colhEmployeeCode.Name = "colhEmployeeCode"
        Me.colhEmployeeCode.ReadOnly = True
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployeeName.HeaderText = "Employee Name"
        Me.colhEmployeeName.Name = "colhEmployeeName"
        Me.colhEmployeeName.ReadOnly = True
        '
        'colhAppointmentDate
        '
        Me.colhAppointmentDate.HeaderText = "Appointment Date"
        Me.colhAppointmentDate.Name = "colhAppointmentDate"
        Me.colhAppointmentDate.ReadOnly = True
        Me.colhAppointmentDate.Visible = False
        Me.colhAppointmentDate.Width = 120
        '
        'colhEffectiveDate
        '
        Me.colhEffectiveDate.HeaderText = "Effective Date"
        Me.colhEffectiveDate.Name = "colhEffectiveDate"
        Me.colhEffectiveDate.ReadOnly = True
        Me.colhEffectiveDate.Width = 110
        '
        'colhAnniversaryMonth
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhAnniversaryMonth.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhAnniversaryMonth.HeaderText = "Anniversary Month"
        Me.colhAnniversaryMonth.Name = "colhAnniversaryMonth"
        Me.colhAnniversaryMonth.ReadOnly = True
        Me.colhAnniversaryMonth.Width = 130
        '
        'colhNewEffectiveDate
        '
        Me.colhNewEffectiveDate.HeaderText = "New Effec. Date"
        Me.colhNewEffectiveDate.Name = "colhNewEffectiveDate"
        Me.colhNewEffectiveDate.ReadOnly = True
        '
        'colhNewAnniversaryMonth
        '
        Me.colhNewAnniversaryMonth.HeaderText = "New Anniv. Month"
        Me.colhNewAnniversaryMonth.Name = "colhNewAnniversaryMonth"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.chkNeverAssignedOnly)
        Me.gbFilterCriteria.Controls.Add(Me.nudToMonth)
        Me.gbFilterCriteria.Controls.Add(Me.nudFromMonth)
        Me.gbFilterCriteria.Controls.Add(Me.lblToMonth)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromMonth)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblToConfirm)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToConfirm)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromConfirm)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromConfirm)
        Me.gbFilterCriteria.Controls.Add(Me.lblToAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromAppointment)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(741, 117)
        Me.gbFilterCriteria.TabIndex = 11
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(604, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 306
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'chkNeverAssignedOnly
        '
        Me.chkNeverAssignedOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNeverAssignedOnly.Location = New System.Drawing.Point(87, 90)
        Me.chkNeverAssignedOnly.Name = "chkNeverAssignedOnly"
        Me.chkNeverAssignedOnly.Size = New System.Drawing.Size(136, 17)
        Me.chkNeverAssignedOnly.TabIndex = 157
        Me.chkNeverAssignedOnly.Text = "Never Assigned Only"
        Me.chkNeverAssignedOnly.UseVisualStyleBackColor = True
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(590, 87)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudToMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(50, 21)
        Me.nudToMonth.TabIndex = 155
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudToMonth.Value = New Decimal(New Integer() {12, 0, 0, 0})
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(382, 87)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudFromMonth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(50, 21)
        Me.nudFromMonth.TabIndex = 154
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudFromMonth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblToMonth
        '
        Me.lblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToMonth.Location = New System.Drawing.Point(511, 90)
        Me.lblToMonth.Name = "lblToMonth"
        Me.lblToMonth.Size = New System.Drawing.Size(73, 15)
        Me.lblToMonth.TabIndex = 153
        Me.lblToMonth.Text = "And"
        Me.lblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFromMonth
        '
        Me.lblFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromMonth.Location = New System.Drawing.Point(229, 90)
        Me.lblFromMonth.Name = "lblFromMonth"
        Me.lblFromMonth.Size = New System.Drawing.Size(147, 15)
        Me.lblFromMonth.TabIndex = 152
        Me.lblFromMonth.Text = "Appointment month between"
        Me.lblFromMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.Checked = False
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(87, 60)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpAsOnDate.TabIndex = 149
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(8, 63)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(73, 15)
        Me.lblAsOnDate.TabIndex = 148
        Me.lblAsOnDate.Text = "As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToConfirm
        '
        Me.lblToConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToConfirm.Location = New System.Drawing.Point(511, 63)
        Me.lblToConfirm.Name = "lblToConfirm"
        Me.lblToConfirm.Size = New System.Drawing.Size(73, 15)
        Me.lblToConfirm.TabIndex = 147
        Me.lblToConfirm.Text = "And"
        Me.lblToConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToConfirm
        '
        Me.dtpToConfirm.Checked = False
        Me.dtpToConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToConfirm.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToConfirm.Location = New System.Drawing.Point(590, 60)
        Me.dtpToConfirm.Name = "dtpToConfirm"
        Me.dtpToConfirm.ShowCheckBox = True
        Me.dtpToConfirm.Size = New System.Drawing.Size(117, 21)
        Me.dtpToConfirm.TabIndex = 146
        '
        'lblFromConfirm
        '
        Me.lblFromConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromConfirm.Location = New System.Drawing.Point(229, 63)
        Me.lblFromConfirm.Name = "lblFromConfirm"
        Me.lblFromConfirm.Size = New System.Drawing.Size(147, 15)
        Me.lblFromConfirm.TabIndex = 145
        Me.lblFromConfirm.Text = "Confirmation date between"
        Me.lblFromConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromConfirm
        '
        Me.dtpFromConfirm.Checked = False
        Me.dtpFromConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromConfirm.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromConfirm.Location = New System.Drawing.Point(382, 60)
        Me.dtpFromConfirm.Name = "dtpFromConfirm"
        Me.dtpFromConfirm.ShowCheckBox = True
        Me.dtpFromConfirm.Size = New System.Drawing.Size(117, 21)
        Me.dtpFromConfirm.TabIndex = 144
        '
        'lblToAppointment
        '
        Me.lblToAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToAppointment.Location = New System.Drawing.Point(511, 35)
        Me.lblToAppointment.Name = "lblToAppointment"
        Me.lblToAppointment.Size = New System.Drawing.Size(73, 15)
        Me.lblToAppointment.TabIndex = 143
        Me.lblToAppointment.Text = "And"
        Me.lblToAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToAppointment
        '
        Me.dtpToAppointment.Checked = False
        Me.dtpToAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToAppointment.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToAppointment.Location = New System.Drawing.Point(590, 33)
        Me.dtpToAppointment.Name = "dtpToAppointment"
        Me.dtpToAppointment.ShowCheckBox = True
        Me.dtpToAppointment.Size = New System.Drawing.Size(117, 21)
        Me.dtpToAppointment.TabIndex = 142
        '
        'lblFromAppointment
        '
        Me.lblFromAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromAppointment.Location = New System.Drawing.Point(229, 38)
        Me.lblFromAppointment.Name = "lblFromAppointment"
        Me.lblFromAppointment.Size = New System.Drawing.Size(147, 15)
        Me.lblFromAppointment.TabIndex = 141
        Me.lblFromAppointment.Text = "Appointment date between"
        Me.lblFromAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromAppointment
        '
        Me.dtpFromAppointment.Checked = False
        Me.dtpFromAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromAppointment.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromAppointment.Location = New System.Drawing.Point(382, 33)
        Me.dtpFromAppointment.Name = "dtpFromAppointment"
        Me.dtpFromAppointment.ShowCheckBox = True
        Me.dtpFromAppointment.Size = New System.Drawing.Size(117, 21)
        Me.dtpFromAppointment.TabIndex = 14
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(87, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(136, 21)
        Me.cboEmployee.TabIndex = 30
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 29
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(714, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 139
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(691, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 138
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 527)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(766, 55)
        Me.objFooter.TabIndex = 13
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(554, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 120
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(657, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeSalaryIncrementAnniversary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(766, 582)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSalaryIncrementAnniversary"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Employee Salary Increment Anniversary"
        Me.pnlMain.ResumeLayout(False)
        Me.gbMonthAssignment.ResumeLayout(False)
        CType(Me.nudDefinedMonth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSalaryAnniversary.ResumeLayout(False)
        Me.pnlSalaryAnniversary.PerformLayout()
        CType(Me.dgvSalaryAnniversary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlSalaryAnniversary As System.Windows.Forms.Panel
    Friend WithEvents dgvSalaryAnniversary As System.Windows.Forms.DataGridView
    Friend WithEvents dtpFromAppointment As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents lblToConfirm As System.Windows.Forms.Label
    Friend WithEvents dtpToConfirm As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromConfirm As System.Windows.Forms.Label
    Friend WithEvents dtpFromConfirm As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToAppointment As System.Windows.Forms.Label
    Friend WithEvents dtpToAppointment As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromAppointment As System.Windows.Forms.Label
    Friend WithEvents lblToMonth As System.Windows.Forms.Label
    Friend WithEvents lblFromMonth As System.Windows.Forms.Label
    Friend WithEvents gbMonthAssignment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents radAppointmentMonth As System.Windows.Forms.RadioButton
    Friend WithEvents radDefinedMonth As System.Windows.Forms.RadioButton
    Friend WithEvents nudDefinedMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnApplytoChecked As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhEmployeeUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAppointmentDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAnniversaryMonth As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhNewEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhNewAnniversaryMonth As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkNeverAssignedOnly As System.Windows.Forms.CheckBox
    Friend WithEvents radConfirmationMonth As System.Windows.Forms.RadioButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
