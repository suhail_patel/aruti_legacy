﻿Option Strict On

#Region " Imports "

Imports System.Globalization
Imports Aruti.Data
Imports System.IO
Imports eZeeCommonLib

#End Region

Public Class frmPeoplesoftMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPeoplesoftMapping"
    Private mblnProcessFailed As Boolean = False
    Dim mintCompanySelectedValue As Integer = 0
    Dim mintTZSSelectedValue As Integer = 0
    Dim mintUSDSelectedValue As Integer = 0
    Dim objPSoftImport As New clsPeoplesoftImport

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objShift As New clsshift_master
        Dim objCommon As New clsCommon_Master
        Dim objHead As New clsTransactionHead
        Dim objGradeGroup As New clsGradeGroup
        Dim dsCombo As DataSet
        Try
            dsCombo = objShift.getListForCombo("Shift", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Shift")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentityType
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objHead.getComboList("Head", True, , , enTypeOf.Salary)
            dsCombo = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            With cboTZSSalary
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Head")
                .SelectedValue = 0
            End With

            With cboUSDSalary
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Head").Copy
                .SelectedValue = 0
            End With

            dsCombo = objGradeGroup.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "List")
            With cboSalaryChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objShift = Nothing
            objCommon = Nothing
            objHead = Nothing
            objGradeGroup = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPeoplesoftMapping_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPSoftImport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftMapping_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeoplesoftMapping_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.I Then
                'Call btnImport.PerformClick()
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{Tab}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftMapping_KeyDown", mstrModuleName)
        End Try

    End Sub

    Private Sub frmPeoplesoftMapping_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            Dim objHead As New clsTransactionHead
            Dim dsHead As DataSet = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 ", False, False, False, 2)
            If dsHead.Tables(0).Rows.Count > 0 Then
                chkAssignDefaulTranHeads.Checked = True
                chkAssignDefaulTranHeads.Visible = True
            Else
                chkAssignDefaulTranHeads.Checked = False
                chkAssignDefaulTranHeads.Visible = False
            End If
            'Sohail (18 Feb 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeoplesoftMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsPeoplesoftImport"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try

            If CInt(cboShift.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Shift.", enMsgBoxStyle.Information)
                cboShift.Focus()
                Exit Try
            ElseIf CInt(cboIdentityType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Identity Type.", enMsgBoxStyle.Information)
                cboIdentityType.Focus()
                Exit Try
            ElseIf CInt(cboTZSSalary.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Salary Head for TZS.", enMsgBoxStyle.Information)
                cboTZSSalary.Focus()
                Exit Try
            ElseIf CInt(cboUSDSalary.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Salary Head for USD.", enMsgBoxStyle.Information)
                cboUSDSalary.Focus()
                Exit Try
            ElseIf CInt(cboGradeGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Grade Group.", enMsgBoxStyle.Information)
                cboGradeGroup.Focus()
                Exit Try
            ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Grade.", enMsgBoxStyle.Information)
                cboGrade.Focus()
                Exit Try
            ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Grade Level.", enMsgBoxStyle.Information)
                cboGradeLevel.Focus()
                Exit Try
            ElseIf CInt(cboSalaryChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show("Please select Salary Change Reason.", enMsgBoxStyle.Information)
                cboSalaryChangeReason.Focus()
                Exit Try
            End If

            If CInt(cboTZSSalary.SelectedValue) = CInt(cboUSDSalary.SelectedValue) Then
                If (MessageBox.Show("TZS Salary head and USD Salary head are same. Ar you sure you want to continue?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)) = Windows.Forms.DialogResult.No Then
                    cboUSDSalary.Focus()
                    Exit Try
                End If

            End If


            If eZeeMsgBox.Show("You are about to import data. Are you sure you want to import? ", CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then



                objPSoftImport._ShiftUnkID = CInt(cboShift.SelectedValue)
                objPSoftImport._ShiftName = cboShift.Text

                objPSoftImport._IdentityTypeUnkID = CInt(cboIdentityType.SelectedValue)
                objPSoftImport._IdentityTypeName = cboIdentityType.Text

                objPSoftImport._TZSSalaryUnkID = CInt(cboTZSSalary.SelectedValue)
                objPSoftImport._TZSSalaryName = cboTZSSalary.Text
                objPSoftImport._TZSSalaryHeadTypeID = CInt(CType(cboTZSSalary.SelectedItem, DataRowView).Item("trnheadtype_id"))

                objPSoftImport._USDSalaryUnkID = CInt(cboUSDSalary.SelectedValue)
                objPSoftImport._USDSalaryName = cboUSDSalary.Text
                objPSoftImport._USDSalaryHeadTypeID = CInt(CType(cboUSDSalary.SelectedItem, DataRowView).Item("trnheadtype_id"))

                objPSoftImport._GradeGroupUnkID = CInt(cboGradeGroup.SelectedValue)
                objPSoftImport._GradeGroupName = cboGradeGroup.Text

                objPSoftImport._GradeUnkID = CInt(cboGrade.SelectedValue)
                objPSoftImport._GradeName = cboGrade.Text

                objPSoftImport._GradeLevelUnkID = CInt(cboGradeLevel.SelectedValue)
                objPSoftImport._GradeLevelName = cboGradeLevel.Text

                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                objPSoftImport._AssignDefaulTranHeads = chkAssignDefaulTranHeads.Checked
                'Sohail (18 Feb 2019) -- End

                objBgWorker.RunWorkerAsync()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOK_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim objGrade As New clsGrade
        Dim dsCombo As DataSet
        Try
            dsCombo = objGrade.getComboList("List", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objGrade = Nothing
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim objGradeLevel As New clsGradeLevel
        Dim dsCombo As DataSet
        Try
            dsCombo = objGradeLevel.getComboList("List", True)
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        Finally
            objGradeLevel = Nothing
        End Try
    End Sub

#End Region

#Region " Background Worker Events "

    Private Sub objBgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objBgWorker.DoWork
        Try


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPSoftImport.Import_Inbound(mintCompanySelectedValue, mintTZSSelectedValue, mintUSDSelectedValue, SaveDialog.FileName, objBgWorker)
            objPSoftImport.Import_Inbound(mintCompanySelectedValue, mintTZSSelectedValue, mintUSDSelectedValue, SaveDialog.FileName, objBgWorker, ConfigParameter._Object._EmployeeAsOnDate, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End


        Catch ex As Exception
            mblnProcessFailed = True
            DisplayError.Show("-1", ex.Message, "objBgWorker_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub objBgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objBgWorker.ProgressChanged
        Try
            txtFile.Text = objPSoftImport.gstrFileName
            txtFile.SelectionStart = txtFile.Text.Length
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & objPSoftImport.gintTotalEmployees & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBgWorker_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objBgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objBgWorker.RunWorkerCompleted
        Try
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                MsgBox(e.Error.ToString, MsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True OrElse objPSoftImport.gblnIsImported = False Then

            Else
                Dim frm As New frmPeoplesoftImport
                frm._DataTable = objPSoftImport._Data_Table
                frm._DependantTable = objPSoftImport._Dependant_Table
                If frm.displayDialog() = True Then
                    eZeeMsgBox.Show("Data Imported Successfully.", enMsgBoxStyle.Information)
                End If
                objlblProgress.Text = ""
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBgWorker_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOK.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOK.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.lblIdentityType.Text = Language._Object.getCaption(Me.lblIdentityType.Name, Me.lblIdentityType.Text)
			Me.lblTZSSalary.Text = Language._Object.getCaption(Me.lblTZSSalary.Name, Me.lblTZSSalary.Text)
			Me.lblUSDSalary.Text = Language._Object.getCaption(Me.lblUSDSalary.Name, Me.lblUSDSalary.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblSalaryChangeReason.Text = Language._Object.getCaption(Me.lblSalaryChangeReason.Name, Me.lblSalaryChangeReason.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.Name, Me.btnOK.Text)
			Me.lblFile.Text = Language._Object.getCaption(Me.lblFile.Name, Me.lblFile.Text)
			Me.chkAssignDefaulTranHeads.Text = Language._Object.getCaption(Me.chkAssignDefaulTranHeads.Name, Me.chkAssignDefaulTranHeads.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

