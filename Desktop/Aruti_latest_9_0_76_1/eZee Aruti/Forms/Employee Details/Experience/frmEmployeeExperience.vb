﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmEmployeeExperience

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeExperience"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objJobExperience As clsJobExperience_tran
    Private mintExperienceTranUnkid As Integer = -1
    Private mintSeletedEmpId As Integer = -1


    'Pinkal (09-Jul-2012) -- Start
    'Enhancement : TRA Changes
    Private mintEmployeeIDFromEmp As Integer = -1
    Private mstrJobFromEmp As String = ""
    'Pinkal (09-Jul-2012) -- End



    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAExperienceTran As clsJobExperience_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim ExperienceApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmJobHistory_ExperienceList)))
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End


    'Gajanan [21-June-2019] -- Start      
    Private OldData As clsJobExperience_tran
    'Gajanan [21-June-2019] -- End



    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private mdtExperienceDocument As DataTable
    Private objDocument As clsScan_Attach_Documents
    Private mstrFolderName As String = ""
    'Gajanan [5-Dec-2019] -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeID As Integer = -1, Optional ByVal mstrJob As String = "") As Boolean
        Try
            mintExperienceTranUnkid = intUnkId
            menAction = eAction


            'Pinkal (09-Jul-2012) -- Start
            'Enhancement : TRA Changes
            mintEmployeeIDFromEmp = intEmployeeID
            mstrJobFromEmp = mstrJob
            'Pinkal (09-Jul-2012) -- End


            Me.ShowDialog()

            intUnkId = mintExperienceTranUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'cboJobs.BackColor = GUI.ColorComp
            txtOldJob.BackColor = GUI.ColorComp
            txtOtherBenefits.BackColor = GUI.ColorOptional
            txtSign.BackColor = GUI.ColorOptional
            'Anjan (02 Mar 2012)-End 


            txtAddress.BackColor = GUI.ColorOptional
            txtCompany.BackColor = GUI.ColorComp
            txtContactNo.BackColor = GUI.ColorOptional
            txtContactPerson.BackColor = GUI.ColorOptional
            txtGrossPay.BackColor = GUI.ColorOptional
            txtLeaveReason.BackColor = GUI.ColorOptional
            txtMemoField.BackColor = GUI.ColorOptional
            txtNote.BackColor = GUI.ColorOptional
            txtSupervisor.BackColor = GUI.ColorOptional
            lvBenefitHoldings.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAddress.Text = objJobExperience._Address
            txtCompany.Text = objJobExperience._Company
            txtContactNo.Text = objJobExperience._Contact_No
            txtContactPerson.Text = objJobExperience._Contact_Person
            cboEmployee.SelectedValue = objJobExperience._Employeeunkid
            If objJobExperience._End_Date <> Nothing Then
                dtpEndDate.Value = objJobExperience._End_Date
            Else
                dtpEndDate.Checked = False
            End If
            chkCanContatEmployee.Checked = objJobExperience._Iscontact_Previous

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'cboJobs.SelectedValue = objJobExperience._Jobunkid
            txtOldJob.Text = objJobExperience._Job
            txtOtherBenefits.Text = objJobExperience._OtherBenefit
            txtSign.Text = objJobExperience._CurrencySign
            'Anjan (02 Mar 2012)-End 



            'Anjan (11 May 2011)-Start
            'txtGrossPay.Decimal = objJobExperience._Last_Pay
            txtGrossPay.Decimal = CDec(Format(CDec(objJobExperience._Last_Pay), GUI.fmtCurrency)) 'Anjan (02 Mar 2012)
            'Anjan (11 May 2011)-End 

            txtLeaveReason.Text = objJobExperience._Leave_Reason
            txtMemoField.Text = objJobExperience._Memo
            txtNote.Text = objJobExperience._Remark
            If objJobExperience._Start_Date <> Nothing Then
                dtpStartDate.Value = objJobExperience._Start_Date
            Else
                dtpStartDate.Checked = False
            End If
            txtSupervisor.Text = objJobExperience._Supervisor

            If objJobExperience._PreviousBenefit.Length > 0 Then
                Dim strBenefitHoldings() As String = Nothing
                strBenefitHoldings = objJobExperience._PreviousBenefit.Split(CChar(","))
                For i As Integer = 0 To strBenefitHoldings.Length - 1
                    For j As Integer = 0 To lvBenefitHoldings.Items.Count - 1
                        If CInt(lvBenefitHoldings.Items(j).Tag) = CInt(strBenefitHoldings(i)) Then
                            lvBenefitHoldings.Items(j).Checked = True
                            Exit For
                        End If
                    Next
                Next
            End If


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            mdtExperienceDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.JOBHISTORYS, mintExperienceTranUnkid, ConfigParameter._Object._Document_Path)
            If menAction <> enAction.EDIT_ONE Then
                If mdtExperienceDocument IsNot Nothing Then mdtExperienceDocument.Rows.Clear()
            End If
            'Gajanan [5-Dec-2019] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'objJobExperience._Address = txtAddress.Text
            'objJobExperience._Company = txtCompany.Text
            'objJobExperience._Contact_No = txtContactNo.Text
            'objJobExperience._Contact_Person = txtContactPerson.Text
            'objJobExperience._Employeeunkid = CInt(cboEmployee.SelectedValue)

            ''Anjan (17 Apr 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            ''objJobExperience._End_Date = dtpEndDate.Value
            'If dtpEndDate.Checked = True Then
            '    objJobExperience._End_Date = dtpEndDate.Value
            'Else
            '    objJobExperience._End_Date = Nothing
            'End If
            ''Anjan (17 Apr 2012)-End 


            'objJobExperience._Iscontact_Previous = chkCanContatEmployee.Checked


            ''Anjan (02 Mar 2012)-Start
            ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            ''objJobExperience._Jobunkid = CInt(cboJobs.SelectedValue)
            'objJobExperience._Job = txtOldJob.Text
            'objJobExperience._OtherBenefit = txtOtherBenefits.Text
            'objJobExperience._CurrencySign = txtSign.Text
            ''Anjan (02 Mar 2012)-End 



            ''Anjan (11 May 2011)-Start
            ''objJobExperience._Last_Pay = txtGrossPay.Decimal
            'objJobExperience._Last_Pay = txtGrossPay.Decimal
            ''Anjan (11 May 2011)-End 


            'objJobExperience._Leave_Reason = txtLeaveReason.Text
            'objJobExperience._Memo = txtMemoField.Text
            'objJobExperience._Remark = txtNote.Text
            'objJobExperience._Start_Date = dtpStartDate.Value
            'objJobExperience._Supervisor = txtSupervisor.Text
            'If mintExperienceTranUnkid = -1 Then
            '    objJobExperience._Userunkid = User._Object._Userunkid
            '    objJobExperience._Isvoid = False
            '    objJobExperience._Voidatetime = Nothing
            '    objJobExperience._Voidreason = ""
            '    objJobExperience._Voiduserunkid = -1
            'Else
            '    objJobExperience._Userunkid = User._Object._Userunkid
            '    objJobExperience._Isvoid = objJobExperience._Isvoid
            '    objJobExperience._Voidatetime = objJobExperience._Voidatetime
            '    objJobExperience._Voidreason = objJobExperience._Voidreason
            '    objJobExperience._Voiduserunkid = objJobExperience._Voiduserunkid
            'End If

            'If lvBenefitHoldings.CheckedItems.Count > 0 Then
            '    objJobExperience._PreviousBenefit = ""
            '    For i As Integer = 0 To lvBenefitHoldings.CheckedItems.Count - 1
            '        If objJobExperience._PreviousBenefit.Length <= 0 Then
            '            objJobExperience._PreviousBenefit = CStr(lvBenefitHoldings.CheckedItems(i).Tag)
            '        Else
            '            objJobExperience._PreviousBenefit &= "," & CStr(lvBenefitHoldings.CheckedItems(i).Tag)
            '        End If
            '    Next
            'Else
            '    objJobExperience._PreviousBenefit = ""
            'End If



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If ExperienceApprovalFlowVal <> Nothing AndAlso ExperienceApprovalFlowVal.Length > 0 Or mintTransactionId > 0 Then

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If ExperienceApprovalFlowVal Is Nothing Then
            If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End

                objAExperienceTran._Audittype = enAuditType.ADD
                objAExperienceTran._Audituserunkid = User._Object._Userunkid
                objAExperienceTran._Address = txtAddress.Text
                objAExperienceTran._Company = txtCompany.Text
                objAExperienceTran._Contact_No = txtContactNo.Text
                objAExperienceTran._Contact_Person = txtContactPerson.Text
                objAExperienceTran._Employeeunkid = CInt(cboEmployee.SelectedValue)

                If dtpEndDate.Checked = True Then
                    objAExperienceTran._End_Date = dtpEndDate.Value
                Else
                    objAExperienceTran._End_Date = Nothing
                End If

                objAExperienceTran._Iscontact_Previous = chkCanContatEmployee.Checked

                objAExperienceTran._Old_Job = txtOldJob.Text
                objAExperienceTran._Otherbenefit = txtOtherBenefits.Text
                objAExperienceTran._Currencysign = txtSign.Text

                objAExperienceTran._Last_Pay = txtGrossPay.Decimal

                objAExperienceTran._Leave_Reason = txtLeaveReason.Text
                objAExperienceTran._Memo = txtMemoField.Text
                objAExperienceTran._Remark = txtNote.Text
                objAExperienceTran._Start_Date = dtpStartDate.Value
                objAExperienceTran._Supervisor = txtSupervisor.Text

                objAExperienceTran._Isvoid = False
                objAExperienceTran._Tranguid = Guid.NewGuid.ToString()
                objAExperienceTran._Transactiondate = Now
                objAExperienceTran._Approvalremark = ""
                objAExperienceTran._Isweb = False
                objAExperienceTran._Isfinal = False
                objAExperienceTran._Ip = getIP()
                objAExperienceTran._Host = getHostName()
                objAExperienceTran._Form_Name = mstrModuleName
                objAExperienceTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                If menAction <> enAction.EDIT_ONE Then
                    objAExperienceTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objAExperienceTran._Experiencetranunkid = -1
                Else
                    objAExperienceTran._Experiencetranunkid = mintExperienceTranUnkid
                    objAExperienceTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If

                If lvBenefitHoldings.CheckedItems.Count > 0 Then
                    objAExperienceTran._PreviousBenefitPlan = ""
                    For i As Integer = 0 To lvBenefitHoldings.CheckedItems.Count - 1
                        If objAExperienceTran._PreviousBenefitPlan.Length <= 0 Then
                            objAExperienceTran._PreviousBenefitPlan = CStr(lvBenefitHoldings.CheckedItems(i).Tag)
                        Else
                            objAExperienceTran._PreviousBenefitPlan &= "," & CStr(lvBenefitHoldings.CheckedItems(i).Tag)
                        End If
                    Next
                Else
                    objAExperienceTran._PreviousBenefitPlan = ""
                End If
            Else
            objJobExperience._Address = txtAddress.Text
            objJobExperience._Company = txtCompany.Text
            objJobExperience._Contact_No = txtContactNo.Text
            objJobExperience._Contact_Person = txtContactPerson.Text
            objJobExperience._Employeeunkid = CInt(cboEmployee.SelectedValue)

            If dtpEndDate.Checked = True Then
                objJobExperience._End_Date = dtpEndDate.Value
            Else
                objJobExperience._End_Date = Nothing
            End If


            objJobExperience._Iscontact_Previous = chkCanContatEmployee.Checked


            objJobExperience._Job = txtOldJob.Text
            objJobExperience._OtherBenefit = txtOtherBenefits.Text
            objJobExperience._CurrencySign = txtSign.Text

            objJobExperience._Last_Pay = txtGrossPay.Decimal


            objJobExperience._Leave_Reason = txtLeaveReason.Text
            objJobExperience._Memo = txtMemoField.Text
            objJobExperience._Remark = txtNote.Text
            objJobExperience._Start_Date = dtpStartDate.Value
            objJobExperience._Supervisor = txtSupervisor.Text
            If mintExperienceTranUnkid = -1 Then
                objJobExperience._Userunkid = User._Object._Userunkid
                objJobExperience._Isvoid = False
                objJobExperience._Voidatetime = Nothing
                objJobExperience._Voidreason = ""
                objJobExperience._Voiduserunkid = -1
            Else
                objJobExperience._Userunkid = User._Object._Userunkid
                objJobExperience._Isvoid = objJobExperience._Isvoid
                objJobExperience._Voidatetime = objJobExperience._Voidatetime
                objJobExperience._Voidreason = objJobExperience._Voidreason
                objJobExperience._Voiduserunkid = objJobExperience._Voiduserunkid
            End If

            If lvBenefitHoldings.CheckedItems.Count > 0 Then
                objJobExperience._PreviousBenefit = ""
                For i As Integer = 0 To lvBenefitHoldings.CheckedItems.Count - 1
                    If objJobExperience._PreviousBenefit.Length <= 0 Then
                        objJobExperience._PreviousBenefit = CStr(lvBenefitHoldings.CheckedItems(i).Tag)
                    Else
                        objJobExperience._PreviousBenefit &= "," & CStr(lvBenefitHoldings.CheckedItems(i).Tag)
                    End If
                Next
            Else
                objJobExperience._PreviousBenefit = ""
            End If
            End If
            'Gajanan [17-DEC-2018] -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objJobs As New clsJobs
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)


            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                       mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            objCMaster = Nothing
            'Gajanan [5-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim objBenefitPlan As New clsbenefitplan_master
        Try
            dsList = objBenefitPlan.GetList("Benefit")
            Dim lvItem As ListViewItem
            lvBenefitHoldings.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables("Benefit").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("benefitplanname").ToString
                lvItem.Tag = dtRow.Item("benefitplanunkid")

                lvBenefitHoldings.Items.Add(lvItem)

                lvItem = Nothing
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience
	'Private Function IsValid() As Boolean
    Private Function IsValid(Optional ByVal IsFromAttachment As Boolean = False) As Boolean
    'Gajanan [5-Dec-2019] -- End
        Try

            'Gajanan [21-June-2019] -- Start      
            Dim blnIsChange As Boolean = True



            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            'If IsNothing(OldData) = False Then
            If IsFromAttachment = False AndAlso IsNothing(OldData) = False Then
                'Gajanan [5-Dec-2019] -- End
                If OldData._Company = txtCompany.Text.Trim AndAlso _
                   OldData._Address = txtAddress.Text.Trim AndAlso _
                   OldData._Start_Date = dtpStartDate.Value AndAlso _
                   OldData._End_Date = dtpEndDate.Value AndAlso _
                   OldData._Job = txtOldJob.Text.Trim AndAlso _
                   OldData._Supervisor = txtSupervisor.Text.Trim AndAlso _
                   OldData._Remark = txtNote.Text.Trim AndAlso _
                   OldData._OtherBenefit = txtOtherBenefits.Text.Trim AndAlso _
                   OldData._Last_Pay = txtGrossPay.Decimal AndAlso _
                   OldData._CurrencySign = txtSign.Text.Trim AndAlso _
                   OldData._Iscontact_Previous = chkCanContatEmployee.Checked AndAlso _
                   OldData._Contact_Person = txtContactPerson.Text.Trim AndAlso _
                   OldData._Contact_No = txtContactNo.Text.Trim AndAlso _
                   OldData._Memo = txtMemoField.Text.Trim AndAlso _
                   OldData._Leave_Reason = txtLeaveReason.Text.Trim Then
                    blnIsChange = False
                Else
                    OldData = Nothing
                End If
            End If


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            'If blnIsChange = False Then
            If blnIsChange = False AndAlso IsFromAttachment = False Then
                'Gajanan [5-Dec-2019] -- End
                Dim xPreviousBenefit As List(Of String)
                xPreviousBenefit = OldData._PreviousBenefit.Split(CChar(",")).ToList()

                If lvBenefitHoldings.CheckedItems.Count > 0 Then

                    For i As Integer = 0 To lvBenefitHoldings.CheckedItems.Count - 1
                        If xPreviousBenefit.Contains(CStr(lvBenefitHoldings.CheckedItems(i).Tag)) = False Then
                            blnIsChange = True
                            Exit For
                        End If
                    Next
                Else
                    objAExperienceTran._PreviousBenefitPlan = ""
                End If
            End If

            
            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            If ConfigParameter._Object._JobExperienceDocsAttachmentMandatory AndAlso IsFromAttachment = False Then
                If mdtExperienceDocument Is Nothing OrElse mdtExperienceDocument.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtExperienceDocument) = False AndAlso CInt(mdtExperienceDocument.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If
            'Gajanan [5-Dec-2019] -- End



            If blnIsChange = False AndAlso menAction = enAction.EDIT_ONE Then
                objJobExperience = Nothing
                OldData = Nothing
                Me.Close()
                Return False
            End If

            'Gajanan [21-June-2019] -- End


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If txtCompany.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Company Name cannot be blank. Company Name is compulsory information."), enMsgBoxStyle.Information)
                txtCompany.Focus()
                Return False
            End If

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If CInt(cboJobs.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Job is compulsory information. Please select Job to continue."), enMsgBoxStyle.Information)
            '    cboJobs.Focus()
            '    Return False
            'End If
            If txtOldJob.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Job is compulsory information. Please assign Job to continue."), enMsgBoxStyle.Information)
                txtOldJob.Focus()
                Return False
            End If
            'Anjan (02 Mar 2012)-End 



            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES, DISCUSSED WITH MR. RUTTA ON DATE : 08/12/2011 - MAKE IT OPTIONAL FOR IMPORTATION
            'If txtLeaveReason.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Leaving Reason cannot be blank. Leaving Reason is compulsory information."), enMsgBoxStyle.Information)
            '    txtLeaveReason.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If dtpStartDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Start Date is compulsory information. Please select Start Date to continue."), enMsgBoxStyle.Information)
                dtpStartDate.Focus()
                Return False
            End If

            'Anjan (17 Apr 2012)-Start (SANDEEP SHARMA)
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If dtpEndDate.Checked = False Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "End Date is compulsory information. Please select End Date to continue."), enMsgBoxStyle.Information)
            '    dtpEndDate.Focus()
            '    Return False
            'End If
            'Anjan (17 Apr 2012)-End 



            'Sandeep [ 14 Aug 2010 ] -- Start
            If dtpEndDate.Checked = True And dtpStartDate.Checked = True Then
                If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "End date cannot be less then or equal to start date."), enMsgBoxStyle.Information)
                    dtpEndDate.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 14 Aug 2010 ] -- End 

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'If ExperienceApprovalFlowVal Is Nothing Then
            If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmQualificationsList, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Return False
                End If

                'Gajanan [17-April-2019] -- End

                If objJobExperience.isExist(CInt(cboEmployee.SelectedValue), txtCompany.Text.Trim, CDate(IIf(dtpStartDate.Checked = True, dtpStartDate.Value.Date, Nothing)), CDate(IIf(dtpEndDate.Checked = True, dtpEndDate.Value.Date, Nothing)), txtOldJob.Text.Trim, mintExperienceTranUnkid) = True Then
                    eZeeMsgBox.Show(Language.getMessage("clsJobExperience_tran", 1, "Particular Job History is already entered for selected employee. Please enter new information."), enMsgBoxStyle.Information)
                    Return False
                End If
                Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                If objAExperienceTran.isExist(CInt(cboEmployee.SelectedValue), txtCompany.Text.Trim, CDate(IIf(dtpStartDate.Checked = True, dtpStartDate.Value.Date, Nothing)), CDate(IIf(dtpEndDate.Checked = True, dtpEndDate.Value.Date, Nothing)), txtOldJob.Text, -1, "", Nothing, False, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 15, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 17, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 19, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 20, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End
            
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function


    Private Sub SetVisibility()

        Try
            objbtnAddBenefit.Enabled = User._Object.Privilege._AddBenefitPlan

            'Gajanan [17-DEC-2018] -- Start
            If mintExperienceTranUnkid > 0 Then
                cboEmployee.Enabled = False
            Else
                cboEmployee.Enabled = True
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " " & "<b>" & " " & objEmployee._Employeecode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'Gajanan (21 Nov 2018) -- Start
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                'Gajanan (21 Nov 2018) -- End
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 8, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 9, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 10, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.EXPERIENCES

                            If objJobExperience._Company.Trim <> txtCompany.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 11, "Company") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objJobExperience._Company.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtCompany.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._Job.Trim <> txtOldJob.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "Job") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objJobExperience._Job.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOldJob.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._Start_Date.Date <> dtpStartDate.Value.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 13, "Start Date") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objJobExperience._Start_Date <> Nothing, objJobExperience._Start_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(dtpStartDate.Checked, dtpStartDate.Value.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._End_Date.Date <> dtpEndDate.Value.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "End Date") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objJobExperience._End_Date <> Nothing, objJobExperience._End_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(dtpEndDate.Checked, dtpEndDate.Value.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 6, "Notification of Changes in Employee Experience(s).")
                            objSendMail._Message = dicNotification(sKey)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 7, "Notifications to newly added Employee Experience(s).")
                            Dim sMsg As String = dicNotification(sKey)
                            'Set_Notification(User._Object._Firstname.Trim & " " & User._Object._Lastname.Trim)
                            objSendMail._Message = sMsg
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END



    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtExperienceDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtExperienceDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.JOBHISTORYS
                dRow("form_name") = mstrForm_Name
                dRow("userunkid") = User._Object._Userunkid
                dRow("transactionunkid") = mintExperienceTranUnkid

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Date.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                dRow("filesize_kb") = f.Length / 1024
                'SHANI (16 JUL 2015) -- End 
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtExperienceDocument.Rows.Add(dRow)
            Call FillQualificationAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillQualificationAttachment()
        Dim dtView As DataView
        Try
            If mdtExperienceDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtExperienceDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvExperience.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvExperience.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillQualificationAttachment", mstrModuleName)
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End

#End Region

#Region " Form's Events "
    Private Sub frmEmployeeExperience_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobExperience = Nothing
    End Sub

    Private Sub frmEmployeeExperience_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeExperience_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeExperience_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objJobExperience = New clsJobExperience_tran
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objAExperienceTran = New clsJobExperience_approval_tran
        'Gajanan [17-DEC-2018] -- End


        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End


        'Gajanan [5-Dec-2019] -- Start   
        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
        objDocument = New clsScan_Attach_Documents
        'Gajanan [5-Dec-2019] -- End

        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            Call FillList()

            If menAction = enAction.EDIT_ONE Then
                objJobExperience._Experiencetranunkid = mintExperienceTranUnkid
                'Sandeep [ 17 Aug 2010 ] -- Start
                cboEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                objbtnSearchEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- End 
                'Sandeep [ 17 Aug 2010 ] -- End 


                'Gajanan [21-June-2019] -- Start      
                OldData = objJobExperience
                'Gajanan [21-June-2019] -- End

            End If

            Call GetValue()


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            Call FillQualificationAttachment()
            'Gajanan [5-Dec-2019] -- End

            'Pinkal (09-Jul-2012) -- Start
            'Enhancement : TRA Changes
            If mintEmployeeIDFromEmp > 0 AndAlso mstrJobFromEmp.Trim.Length > 0 Then
                cboEmployee.Enabled = False
                cboEmployee.SelectedValue = mintEmployeeIDFromEmp
                objbtnSearchEmployee.Enabled = False
                txtOldJob.ReadOnly = True
                txtOldJob.Text = mstrJobFromEmp
            End If
            'Pinkal (09-Jul-2012) -- End


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeIDFromEmp > 0 Then
                cboEmployee.Enabled = False
                cboEmployee.SelectedValue = mintEmployeeIDFromEmp
                objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString
            'Gajanan [5-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExperience_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobExperience_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsJobExperience_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub


            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dicNotification = New Dictionary(Of String, String)
            If menAction = enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        'S.SANDEEP [12-DEC-2018] -- START
                        'StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        'dicNotification.Add(objUsr._Email, StrMessage)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        dicNotification.Add(objUsr._Email, StrMessage)
                        End If
                        'S.SANDEEP [12-DEC-2018] -- END
                    Next
                    objUsr = Nothing
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        'S.SANDEEP [12-DEC-2018] -- START
                        'StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        'dicNotification.Add(objUsr._Email, StrMessage)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        dicNotification.Add(objUsr._Email, StrMessage)
                        End If
                        'S.SANDEEP [12-DEC-2018] -- END
                    Next
                    objUsr = Nothing
                End If
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END



            Call SetValue()

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim ExtraFilter As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and company = '" & txtCompany.Text & "' and old_job = '" & txtOldJob.Text & "' "
            Dim ExtraFilterForQuery As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and company = '" & txtCompany.Text & "' and old_job = '" & txtOldJob.Text & "' "

            'If dtpEndDate.Checked = True Then
            '    ExtraFilter &= " and end_date_fc = '" + eZeeDate.convertDate(dtpEndDate.Value.Date).ToString() + "' "
            '    ExtraFilterForQuery &= " and convert(varchar(8), cast(end_date as datetime), 112)   =  '" + eZeeDate.convertDate(dtpEndDate.Value.Date).ToString() + "' "
            'End If

            If dtpStartDate.Checked = True Then
                ExtraFilter &= " and start_date_fc = '" + eZeeDate.convertDate(dtpStartDate.Value.Date).ToString() + "' "
                ExtraFilterForQuery &= " and convert(varchar(8), cast(start_date as datetime), 112)   =  '" + eZeeDate.convertDate(dtpStartDate.Value.Date).ToString() + "' "
            End If



            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            If mdtExperienceDocument.Rows.Count > 0 Then
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()

                Dim strError As String = ""

                For Each dRow As DataRow In mdtExperienceDocument.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                        Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                        If blnIsIISInstalled Then

                            If clsFileUploadDownload.UploadFile(CStr(dRow("localpath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                dRow("fileuniquename") = strFileName
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Copy(CStr(dRow("localpath")), strDocLocalPath, True)
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strDocLocalPath
                                dRow.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then

                    End If
                Next
            End If
            'Gajanan [5-Dec-2019] -- End


            'Gajanan [17-April-2019] -- End
            If menAction = enAction.EDIT_ONE Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objJobExperience.Update()

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If ExperienceApprovalFlowVal Is Nothing AndAlso mintExperienceTranUnkid > 0 Then
                If ExperienceApprovalFlowVal Is Nothing AndAlso mintExperienceTranUnkid > 0 AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End


                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    'blnFlag = objAExperienceTran.Insert(Company._Object._Companyunkid)
                    blnFlag = objAExperienceTran.Insert(Company._Object._Companyunkid, Nothing, False, mdtExperienceDocument)
                    'Gajanan [5-Dec-2019] -- End

                    If blnFlag = False AndAlso objAExperienceTran._Message <> "" Then
                        eZeeMsgBox.Show(objAExperienceTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Else
                        If blnFlag <> False Then
                            objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                             enScreenName.frmJobHistory_ExperienceList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                             User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                             User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, , _
                                                             cboEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, _
                                                             clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)
                        End If
                        'Gajanan [17-April-2019] -- End

                    End If
                Else

                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    'blnFlag = objJobExperience.Update()
                    blnFlag = objJobExperience.Update(Nothing, mdtExperienceDocument)
                    'Gajanan [5-Dec-2019] -- End

                End If
                'Gajanan [17-DEC-2018] -- End
            Else


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objJobExperience.Insert()
                If ExperienceApprovalFlowVal Is Nothing AndAlso mintExperienceTranUnkid <= 0 AndAlso isEmployeeApprove = True Then

                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    'blnFlag = objAExperienceTran.Insert(Company._Object._Companyunkid)
                    blnFlag = objAExperienceTran.Insert(Company._Object._Companyunkid, Nothing, False, mdtExperienceDocument)
                    'Gajanan [5-Dec-2019] -- End
                    If blnFlag = False AndAlso objAExperienceTran._Message <> "" Then
                        eZeeMsgBox.Show(objAExperienceTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    Else
                        If blnFlag <> False Then
                            objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                             enScreenName.frmJobHistory_ExperienceList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                             User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                             User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                             , cboEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, _
                                                             clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery, Nothing)

                        End If
                        'Gajanan [17-April-2019] -- End

                    End If
            Else

                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    'blnFlag = objJobExperience.Insert()
                    blnFlag = objJobExperience.Insert(Nothing, mdtExperienceDocument)
                    'Gajanan [5-Dec-2019] -- End
            End If
                'Gajanan [17-DEC-2018] -- End
            End If

            If blnFlag = False And objJobExperience._Message <> "" Then
                eZeeMsgBox.Show(objJobExperience._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objJobExperience = Nothing
                    objJobExperience = New clsJobExperience_tran
                    Call GetValue()

                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    Call FillQualificationAttachment()
                    'Gajanan [5-Dec-2019] -- End

                    cboEmployee.Focus()
                Else
                    mintExperienceTranUnkid = objJobExperience._Experiencetranunkid
                    Me.Close()
                End If
            End If

            If mintSeletedEmpId <> -1 Then
                cboEmployee.SelectedValue = mintSeletedEmpId
            End If


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeIDFromEmp > 0 Then
                cboEmployee.SelectedValue = mintEmployeeIDFromEmp
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END


            Call FillList()

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [09-AUG-2018] -- START
            'If blnFlag = True Then
            '   trd = New Thread(AddressOf Send_Notification)
            '   trd.IsBackground = True
            '   trd.Start()
            'End If
            If ConfigParameter._Object._SkipEmployeeApprovalFlow = False Then
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            End If
            'S.SANDEEP [09-AUG-2018] -- END

            'S.SANDEEP [ 18 SEP 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If IsValid(True) = False Then Exit Sub

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Exit Sub
            End If

            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Call AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddAttachment_Click :", mstrModuleName)
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End

#End Region

#Region " Controls "
    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSeletedEmpId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 15 DEC 2010 ] -- Start
    'Issue : Mr. Rutta's Comment
    Private Sub objbtnAddBenefit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBenefit.Click
        Dim frm As New frmBenefitplan_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBenefit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 15 DEC 2010 ] -- End 

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(cboEmployee.SelectedValue) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private Sub dgvExperiencee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvExperience.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor

            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvExperience.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtExperienceDocument.Select("scanattachtranunkid = " & CInt(dgvExperience.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtExperienceDocument.Select("GUID = '" & dgvExperience.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 25, "Are you sure, you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call FillQualificationAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtExperienceDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualification_CellContentClick :", mstrModuleName)
            'SHANI (27 JUL 2015) -- Start
            'Enhancement - 
        Finally
            Cursor.Current = Cursors.Default
            'SHANI (27 JUL 2015) -- End 
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbEmployeeExperience.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeExperience.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOtherDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOtherDetails.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLeavingReason.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeavingReason.ForeColor = GUI._eZeeContainerHeaderForeColor


          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddAttachment.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddAttachment.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbEmployeeExperience.Text = Language._Object.getCaption(Me.gbEmployeeExperience.Name, Me.gbEmployeeExperience.Text)
            Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.Name, Me.lblInstitution.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblSupervisor.Text = Language._Object.getCaption(Me.lblSupervisor.Name, Me.lblSupervisor.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnJobInfo.Text = Language._Object.getCaption(Me.lnJobInfo.Name, Me.lnJobInfo.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.tabpJobInfo.Text = Language._Object.getCaption(Me.tabpJobInfo.Name, Me.tabpJobInfo.Text)
            Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
            Me.tabpOtherDetails.Text = Language._Object.getCaption(Me.tabpOtherDetails.Name, Me.tabpOtherDetails.Text)
            Me.gbOtherDetails.Text = Language._Object.getCaption(Me.gbOtherDetails.Name, Me.gbOtherDetails.Text)
            Me.lblLastGrossPay.Text = Language._Object.getCaption(Me.lblLastGrossPay.Name, Me.lblLastGrossPay.Text)
            Me.chkCanContatEmployee.Text = Language._Object.getCaption(Me.chkCanContatEmployee.Name, Me.chkCanContatEmployee.Text)
            Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.Name, Me.lblContactNo.Text)
            Me.lblContacPerson.Text = Language._Object.getCaption(Me.lblContacPerson.Name, Me.lblContacPerson.Text)
            Me.gbLeavingReason.Text = Language._Object.getCaption(Me.gbLeavingReason.Name, Me.gbLeavingReason.Text)
            Me.tabpBenefit.Text = Language._Object.getCaption(Me.tabpBenefit.Name, Me.tabpBenefit.Text)
            Me.tabpOtherBenefit.Text = Language._Object.getCaption(Me.tabpOtherBenefit.Name, Me.tabpOtherBenefit.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.TabPage1.Text = Language._Object.getCaption(Me.TabPage1.Name, Me.TabPage1.Text)
			Me.btnAddAttachment.Text = Language._Object.getCaption(Me.btnAddAttachment.Name, Me.btnAddAttachment.Text)
			Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
			Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
			Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Company Name cannot be blank. Company Name is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Job is compulsory information. Please assign Job to continue.")
            Language.setMessage(mstrModuleName, 4, "Start Date is compulsory information. Please select Start Date to continue.")
            Language.setMessage(mstrModuleName, 5, "End date cannot be less then or equal to start date.")
            Language.setMessage(mstrModuleName, 6, "Notification of Changes in Employee Experience(s).")
            Language.setMessage(mstrModuleName, 7, "Notifications to newly added Employee Experience(s).")
            Language.setMessage(mstrModuleName, 8, "Field")
            Language.setMessage(mstrModuleName, 9, "Old Value")
            Language.setMessage(mstrModuleName, 10, "New Value")
            Language.setMessage(mstrModuleName, 11, "Company")
            Language.setMessage(mstrModuleName, 12, "Job")
            Language.setMessage(mstrModuleName, 13, "Start Date")
            Language.setMessage(mstrModuleName, 14, "End Date")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 16, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 17, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 18, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 20, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 21, "Document Type is compulsory information. Please select Document Type to continue.")
			Language.setMessage(mstrModuleName, 25, "Are you sure, you want to delete this attachment?")
			Language.setMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
			Language.setMessage(mstrModuleName, 27, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 28, "sorry, file you are trying to access does not exists on Aruti self service application folder.")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage("clsJobExperience_tran", 1, "Particular Job History is already entered for selected employee. Please enter new information.")
			Language.setMessage(mstrModuleName, 6, "Selected information is already present for particular employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class