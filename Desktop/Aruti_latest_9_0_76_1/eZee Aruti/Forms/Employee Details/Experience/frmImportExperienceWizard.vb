﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportExperienceWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImportExperienceWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing 'Sohail (23 Dec 2011)

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAExperienceTran As clsJobExperience_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim ExperienceApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmJobHistory_ExperienceList)))
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End
#End Region

#Region " Form's Events "
    Private Sub frmImportExperienceWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objAExperienceTran = New clsJobExperience_approval_tran
            objApprovalData = New clsEmployeeDataApproval
            'Gajanan [17-April-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportDependantWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    'S.SANDEEP [19-JUL-2018] -- START
                                    Case cboFIRSTNAME.Name.ToUpper, cboSURNAME.Name.ToUpper, cboLEAVING_REASON.Name.ToUpper, cboEND_DATE.Name.ToUpper
                                        'Case "CBOFIRSTNAME", "CBOLASTNAME", "CBOLEAVEREASON", "CBOENDDATE" 'S.SANDEEP [ 27 APRIL 2012 CBOENDDATE ] -- START -- END
                                        'S.SANDEEP [19-JUL-2018] -- END
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboCOMPANY.Items.Add(dtColumns.ColumnName)
                cboEMPLOYEE_CODE.Items.Add(dtColumns.ColumnName)
                cboEND_DATE.Items.Add(dtColumns.ColumnName)
                cboFIRSTNAME.Items.Add(dtColumns.ColumnName)
                cboJOB.Items.Add(dtColumns.ColumnName)
                cboSURNAME.Items.Add(dtColumns.ColumnName)
                cboLEAVING_REASON.Items.Add(dtColumns.ColumnName)
                cboSTART_DATE.Items.Add(dtColumns.ColumnName)


                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                If dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 2, "EMPLOYEE_CODE") Then
                    dtColumns.Caption = "EMPLOYEE_CODE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 3, "FIRSTNAME") Then
                    dtColumns.Caption = "FIRSTNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 4, "SURNAME") Then
                    dtColumns.Caption = "SURNAME"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 5, "COMPANY") Then
                    dtColumns.Caption = "COMPANY"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 6, "START_DATE") Then
                    dtColumns.Caption = "START_DATE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 7, "END_DATE") Then
                    dtColumns.Caption = "END_DATE"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 8, "LEAVING_REASON") Then
                    dtColumns.Caption = "LEAVING_REASON"
                ElseIf dtColumns.ColumnName = Language.getMessage("clsJobExperience_tran", 9, "JOB") Then
                    dtColumns.Caption = "JOB"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ECode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Firstname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Surname", System.Type.GetType("System.String"))


            'Gajanan [27-May-2019] -- Start              
            'mdt_ImportData_Others.Columns.Add("Company", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Job", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Startdate", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("EndDate", System.Type.GetType("System.String"))
            'mdt_ImportData_Others.Columns.Add("Reason", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("Company", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Company")
            mdt_ImportData_Others.Columns.Add("Job", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Job")
            mdt_ImportData_Others.Columns.Add("Startdate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Startdate")
            mdt_ImportData_Others.Columns.Add("EndDate", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "EndDate")
            mdt_ImportData_Others.Columns.Add("Reason", System.Type.GetType("System.String")).ExtendedProperties.Add("col", "Reason")
            'Gajanan [27-May-2019] -- End



            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            'Sohail (23 Dec 2011) -- Start
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'Sohail (23 Dec 2011) -- End

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData_Others.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEMPLOYEE_CODE.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ECode") = dtRow.Item(cboEMPLOYEE_CODE.Text).ToString.Trim


                If cboFIRSTNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Firstname") = dtRow.Item(cboFIRSTNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Firstname") = ""
                End If

                If cboSURNAME.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Surname") = dtRow.Item(cboSURNAME.Text).ToString.Trim
                Else
                    drNewRow.Item("Surname") = ""
                End If

                If cboCOMPANY.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Company") = dtRow.Item(cboCOMPANY.Text).ToString.Trim
                Else
                    drNewRow.Item("Company") = ""
                End If

                If cboJOB.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Job") = dtRow.Item(cboJOB.Text).ToString.Trim
                Else
                    drNewRow.Item("Job") = ""
                End If

                If cboSTART_DATE.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Startdate") = dtRow.Item(cboSTART_DATE.Text).ToString.Trim
                Else
                    drNewRow.Item("Startdate") = ""
                End If

                If cboEND_DATE.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("EndDate") = dtRow.Item(cboEND_DATE.Text).ToString.Trim
                Else
                    drNewRow.Item("EndDate") = ""
                End If

                If cboLEAVING_REASON.Text.ToString.Trim.Length > 0 Then
                    drNewRow.Item("Reason") = dtRow.Item(cboLEAVING_REASON.Text).ToString.Trim
                Else
                    drNewRow.Item("Reason") = ""
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                'Sohail (23 Dec 2011) -- Start
                drNewRow.Item("objStatus") = ""
                'Sohail (23 Dec 2011) -- End

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ECode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                dgData.AutoGenerateColumns = False
                'Sohail (23 Dec 2011) -- Start
                'dgData.DataSource = mdt_ImportData_Others
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
                'Sohail (23 Dec 2011) -- End
            End If

            Call Import_Data()

            ezWait.Active = False 'Sohail (23 Dec 2011)
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEMPLOYEE_CODE.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboCOMPANY.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Company cannot be blank. Please set Company in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboJOB.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Job cannot be blank. Please set Job in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboSTART_DATE.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Start date cannot be blank. Please set Start date in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If .Item(cboEndDate.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "End date cannot be blank. Please set End date in order to import Employee Experience(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objJob As New clsJobs
            Dim objEMaster As New clsEmployee_Master
            Dim objExp As clsJobExperience_tran

            Dim intJobId As Integer = -1
            Dim intEmployeeUnkid As Integer = -1
            Dim intExperienceUnkid As Integer = -1
            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [22-Feb-2019] -- End



                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                'Gajanan [17-April-2019] -- End


                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ECode").ToString.Trim.Length > 0 Then
                    intEmployeeUnkid = objEMaster.GetEmployeeUnkid("", dtRow.Item("ECode").ToString.Trim)
                    If intEmployeeUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        'Sohail (23 Dec 2011) -- Start
                        dtRow.Item("objStatus") = 2
                        'Sohail (23 Dec 2011) -- End
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                        'Gajanan [27-May-2019] -- Start
                    Else
                        dtRow.Item("EmployeeId") = intEmployeeUnkid
                        'Gajanan [27-May-2019] -- End
                    End If
                End If


                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                ''------------------------------ JOB
                'If dtRow.Item("Job").ToString.Trim.Length > 0 Then
                '    intJobId = objJob.GetJobUnkId(dtRow.Item("Job").ToString.Trim)
                '    If intJobId <= 0 Then
                '        If Not objJob Is Nothing Then objJob = Nothing
                '        objJob = New clsJobs

                '        objJob._Create_Date = ConfigParameter._Object._CurrentDateAndTime
                '        objJob._Isactive = True
                '        objJob._Job_Code = dtRow.Item("job").ToString.Trim.ToUpper
                '        objJob._Job_Level = 0
                '        objJob._Jobgradeunkid = 0
                '        objJob._Jobgroupunkid = 0
                '        objJob._Jobunitunkid = 0
                '        objJob._Report_Tounkid = 0
                '        objJob._Userunkid = User._Object._Userunkid
                '        objJob._Job_Name = dtRow.Item("Job").ToString.Trim
                '        objJob._Jobsectionunkid = 0

                '        If objJob.Insert Then
                '            intJobId = objJob._Jobunkid
                '        Else
                '            dtRow.Item("image") = imgError
                '            dtRow.Item("message") = objJob._Job_Code & "/" & objJob._Job_Name & ":" & objJob._Message
                '            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                '            'Sohail (23 Dec 2011) -- Start
                '            dtRow.Item("objStatus") = 2
                '            'Sohail (23 Dec 2011) -- End
                '            objError.Text = CStr(Val(objError.Text) + 1)
                '            Continue For
                '            intJobId = -1
                '        End If
                '    End If
                'End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Try
                    If dtRow.Item("Startdate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("Startdate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                Catch ex As Exception
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End Try

                Try
                    If dtRow.Item("EndDate").ToString.Trim.Length > 0 Then
                        If IsDate(dtRow.Item("EndDate")) = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                Catch ex As Exception
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End Try


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If ExperienceApprovalFlowVal Is Nothing Then
                If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'objAExperienceTran = New clsJobExperience_approval_tran

                    If objApprovalData.IsApproverPresent(enScreenName.frmJobHistory_ExperienceList, FinancialYear._Object._DatabaseName, _
                                                          ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                          FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                          User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, intEmployeeUnkid, Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)

                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = objApprovalData._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If
                    'Gajanan [17-April-2019] -- End

                    Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                    If objAExperienceTran.isExist(intEmployeeUnkid, dtRow("Company").ToString(), CDate(dtRow.Item("Startdate")), CDate(dtRow.Item("EndDate")), dtRow("Job").ToString(), -1, "", Nothing, False, intOperationType) = True Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                strMsg = Language.getMessage(mstrModuleName, 103, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                strMsg = Language.getMessage(mstrModuleName, 104, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                strMsg = Language.getMessage(mstrModuleName, 105, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                        End Select

                        'Gajanan |30-MAR-2019| -- START
                        dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = strMsg
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 2
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Continue For
                        'Gajanan |30-MAR-2019| -- END

                    End If

                    'Gajanan |30-MAR-2019| -- START
                    'dtRow.Item("image") = imgError
                    'dtRow.Item("message") = strMsg
                    'dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    'dtRow.Item("objStatus") = 2
                    'objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    'Continue For
                    'Gajanan |30-MAR-2019| -- END
                End If
                'Gajanan [17-DEC-2018] -- End

                objExp = New clsJobExperience_tran

                intExperienceUnkid = objExp.GetExperienceDataUnkid(intEmployeeUnkid, dtRow.Item("Company").ToString.Trim)

                If intExperienceUnkid > 0 Then
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Employee Experience Already Exist.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    'Sohail (23 Dec 2011) -- Start
                    dtRow.Item("objStatus") = 0
                    Continue For
                    'Sohail (23 Dec 2011) -- End
                Else
                    '    objExp._Company = dtRow.Item("Company").ToString.Trim
                    '    objExp._Employeeunkid = intEmployeeUnkid
                    '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    '    'ENHANCEMENT : TRA CHANGES
                    '    'objExp._Jobunkid = intJobId
                    '    objExp._Job = dtRow.Item("Job").ToString.Trim
                    '    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    '    objExp._Leave_Reason = dtRow.Item("Reason").ToString.Trim

                    '    Try
                    '        If dtRow.Item("Startdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("Startdate")) Then
                    '            objExp._Start_Date = CDate(dtRow.Item("Startdate"))
                    '        End If
                    '    Catch ex As Exception
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    '        'Sohail (23 Dec 2011) -- Start
                    '        dtRow.Item("objStatus") = 2
                    '        'Sohail (23 Dec 2011) -- End
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Finally
                    '    End Try

                    '    Try
                    '        If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                    '            objExp._End_Date = CDate(dtRow.Item("EndDate"))
                    '            'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    '            'ENHANCEMENT : TRA CHANGES
                    '        Else
                    '            objExp._End_Date = Nothing
                    '            'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    '        End If
                    '    Catch ex As Exception
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    '        'Sohail (23 Dec 2011) -- Start
                    '        dtRow.Item("objStatus") = 2
                    '        'Sohail (23 Dec 2011) -- End
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Finally
                    '    End Try
                    'End If

                    ''S.SANDEEP [ 08 NOV 2012 ] -- START
                    ''ENHANCEMENT : TRA CHANGES
                    'objExp._Userunkid = User._Object._Userunkid
                    ''S.SANDEEP [ 08 NOV 2012 ] -- END

                    'If objExp.Insert Then
                    '    dtRow.Item("image") = imgAccept
                    '    dtRow.Item("message") = ""
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Success")
                    '    'Sohail (23 Dec 2011) -- Start
                    '    dtRow.Item("objStatus") = 1
                    '    'Sohail (23 Dec 2011) -- End
                    '    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    'Else
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = objExp._Message
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    '    'Sohail (23 Dec 2011) -- Start
                    '    dtRow.Item("objStatus") = 2
                    '    'Sohail (23 Dec 2011) -- End
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    'End If

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    'If ExperienceApprovalFlowVal Is Nothing Then
                    If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End
                        objAExperienceTran = New clsJobExperience_approval_tran
                        objAExperienceTran._Audittype = enAuditType.ADD
                        objAExperienceTran._Audituserunkid = User._Object._Userunkid
                        objAExperienceTran._Company = dtRow.Item("Company").ToString.Trim
                        objAExperienceTran._Employeeunkid = intEmployeeUnkid
                        objAExperienceTran._Old_Job = dtRow.Item("Job").ToString.Trim
                        objAExperienceTran._Leave_Reason = dtRow.Item("Reason").ToString.Trim
                        Try
                            If dtRow.Item("Startdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("Startdate")) Then
                                objAExperienceTran._Start_Date = CDate(dtRow.Item("Startdate"))
                            Else
                                objAExperienceTran._Start_Date = Nothing
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try

                        Try
                            If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                                objAExperienceTran._End_Date = CDate(dtRow.Item("EndDate"))
                            Else
                                objAExperienceTran._End_Date = Nothing
                            End If
                        Catch ex As Exception
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End Try

                        objAExperienceTran._Isvoid = False
                        objAExperienceTran._Tranguid = Guid.NewGuid.ToString()
                        objAExperienceTran._Transactiondate = Now
                        objAExperienceTran._Approvalremark = ""
                        objAExperienceTran._Isweb = False
                        objAExperienceTran._Isfinal = False
                        objAExperienceTran._Ip = getIP()
                        objAExperienceTran._Host = getHostName()
                        objAExperienceTran._Form_Name = mstrModuleName
                        objAExperienceTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        'Gajanan [27-May-2019] -- Start              
                        objAExperienceTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                        'Gajanan [27-May-2019] -- End

                    Else
                    objExp._Company = dtRow.Item("Company").ToString.Trim
                    objExp._Employeeunkid = intEmployeeUnkid
                    objExp._Job = dtRow.Item("Job").ToString.Trim
                    objExp._Leave_Reason = dtRow.Item("Reason").ToString.Trim
                    Try
                        If dtRow.Item("Startdate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("Startdate")) Then
                            objExp._Start_Date = CDate(dtRow.Item("Startdate"))
                            Else
                                objExp._Start_Date = Nothing
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try

                    Try
                        If dtRow.Item("EndDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("EndDate")) Then
                            objExp._End_Date = CDate(dtRow.Item("EndDate"))
                        Else
                            objExp._End_Date = Nothing
                        End If
                    Catch ex As Exception
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Date")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End Try
                        objExp._Userunkid = User._Object._Userunkid
                End If

                    Dim blnFlag As Boolean = False

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    'If ExperienceApprovalFlowVal Is Nothing Then
                    If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                        'Gajanan [22-Feb-2019] -- End
                        blnFlag = objAExperienceTran.Insert(Company._Object._Companyunkid)
                        If blnFlag = False AndAlso objAExperienceTran._Message <> "" Then
                            dtRow.Item("message") = objAExperienceTran._Message

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Else
                            'Gajanan [27-May-2019] -- Start
                            If EmpList.Contains(intEmployeeUnkid) = False Then
                                EmpList.Add(intEmployeeUnkid)
                            End If


                            'Dim ExtraFilter As String = "employeeunkid= " & intEmployeeUnkid & " and company = '" & objAExperienceTran._Company & "' and old_job = '" & objAExperienceTran._Old_Job & "' "
                            'Dim ExtraFilterForQuery As String = "employeeunkid= " & intEmployeeUnkid & " and company = '" & objAExperienceTran._Company & "' and old_job = '" & objAExperienceTran._Old_Job & "' "

                            'If dtRow.Item("StartDate").ToString.Trim.Length > 0 AndAlso IsDate(dtRow.Item("StartDate")) Then
                            '    ExtraFilter &= " and start_date_fc = '" + eZeeDate.convertDate(objAExperienceTran._Start_Date).ToString() + "' "
                            '    ExtraFilterForQuery &= " and convert(varchar(8), cast(start_date as datetime), 112)   =  '" + eZeeDate.convertDate(objAExperienceTran._Start_Date).ToString() + "' "
                            'End If

                            'objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                            '                                 ConfigParameter._Object._UserAccessModeSetting, _
                            '                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                            '                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                            '                                 enScreenName.frmJobHistory_ExperienceList, ConfigParameter._Object._EmployeeAsOnDate, _
                            '                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                            '                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , _
                            '                                 intEmployeeUnkid, , , ExtraFilter, Nothing, False, _
                            '                                 clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)
                            'Gajanan [17-April-2019] -- End




                            'Gajanan [27-May-2019] -- End

                        End If
                    Else
                        blnFlag = objExp.Insert()
                        If blnFlag = False AndAlso objExp._Message <> "" Then dtRow.Item("message") = objExp._Message
                    End If

                    If blnFlag Then
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 101, "Success")
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                        'Gajanan |30-MAR-2019| -- START
                        dtRow.Item("message") = objExp._Message
                        'Gajanan |30-MAR-2019| -- END
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If
                End If


                'Gajanan [27-May-2019] -- Start              
                Application.DoEvents()
                'Gajanan [27-May-2019] -- End


            Next

            'Gajanan [27-May-2019] -- Start
            If ExperienceApprovalFlowVal Is Nothing Then
                If mdt_ImportData_Others.Select("status <> '" & Language.getMessage(mstrModuleName, 8, "Fail") & "'").Count > 0 Then
                    If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                        Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                        mdt_ImportData_Others.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 8, "Fail") & "'"
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                                   Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                   CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                                   enScreenName.frmJobHistory_ExperienceList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                   User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                   User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                                   User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                                   ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData_Others.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                    End If
                End If
            End If
            'Gajanan [27-May-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (23 Dec 2011) -- Start
    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Dec 2011) -- End


    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblLeavingReason.Text = Language._Object.getCaption(Me.lblLeavingReason.Name, Me.lblLeavingReason.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
			Language.setMessage("clsJobExperience_tran", 3, "FIRSTNAME")
			Language.setMessage("clsJobExperience_tran", 4, "SURNAME")
			Language.setMessage("clsJobExperience_tran", 5, "COMPANY")
			Language.setMessage("clsJobExperience_tran", 6, "START_DATE")
			Language.setMessage("clsJobExperience_tran", 7, "END_DATE")
			Language.setMessage("clsJobExperience_tran", 8, "LEAVING_REASON")
			Language.setMessage("clsJobExperience_tran", 9, "JOB")
			Language.setMessage(mstrModuleName, 10, "Invalid Date")
			Language.setMessage(mstrModuleName, 11, "Employee Experience Already Exist.")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 101, "Success")
            Language.setMessage(mstrModuleName, 103, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
            Language.setMessage(mstrModuleName, 104, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 105, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
			Language.setMessage("clsJobExperience_tran", 2, "EMPLOYEE_CODE")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 4, "Company cannot be blank. Please set Company in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 5, "Job cannot be blank. Please set Job in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 6, "Start date cannot be blank. Please set Start date in order to import Employee Experience(s).")
            Language.setMessage(mstrModuleName, 7, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 8, "Fail")
            Language.setMessage(mstrModuleName, 9, "Employee Not Found.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class