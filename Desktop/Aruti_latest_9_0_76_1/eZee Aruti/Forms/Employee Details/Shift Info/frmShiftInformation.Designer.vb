﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShiftInformation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShiftInformation))
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Sunday")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Monday")
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Tuesday")
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Wednesday")
        Dim ListViewItem12 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Thursday")
        Dim ListViewItem13 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Friday")
        Dim ListViewItem14 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Saturday")
        Me.pnlShiftInformation = New System.Windows.Forms.Panel
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbShiftDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtShift = New eZee.TextBox.AlphanumericTextBox
        Me.txtBreakHours = New eZee.TextBox.NumericTextBox
        Me.txtShiftHours = New eZee.TextBox.NumericTextBox
        Me.lblBreakHours = New System.Windows.Forms.Label
        Me.lblShiftHours = New System.Windows.Forms.Label
        Me.lblShift = New System.Windows.Forms.Label
        Me.lvWorkingDays = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.eZeeWorkDays = New eZee.Common.eZeeLine
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.eZeeBreakInfo = New eZee.Common.eZeeLine
        Me.eZeeShiftInfo = New eZee.Common.eZeeLine
        Me.pnlShiftInformation.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbShiftDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlShiftInformation
        '
        Me.pnlShiftInformation.Controls.Add(Me.EZeeFooter1)
        Me.pnlShiftInformation.Controls.Add(Me.eZeeHeader)
        Me.pnlShiftInformation.Controls.Add(Me.gbShiftDetails)
        Me.pnlShiftInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlShiftInformation.Location = New System.Drawing.Point(0, 0)
        Me.pnlShiftInformation.Name = "pnlShiftInformation"
        Me.pnlShiftInformation.Size = New System.Drawing.Size(524, 327)
        Me.pnlShiftInformation.TabIndex = 0
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 272)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(524, 55)
        Me.EZeeFooter1.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(419, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 70
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = "Description"
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(524, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Shift Details"
        '
        'gbShiftDetails
        '
        Me.gbShiftDetails.BorderColor = System.Drawing.Color.Black
        Me.gbShiftDetails.Checked = False
        Me.gbShiftDetails.CollapseAllExceptThis = False
        Me.gbShiftDetails.CollapsedHoverImage = Nothing
        Me.gbShiftDetails.CollapsedNormalImage = Nothing
        Me.gbShiftDetails.CollapsedPressedImage = Nothing
        Me.gbShiftDetails.CollapseOnLoad = False
        Me.gbShiftDetails.Controls.Add(Me.txtShift)
        Me.gbShiftDetails.Controls.Add(Me.txtBreakHours)
        Me.gbShiftDetails.Controls.Add(Me.txtShiftHours)
        Me.gbShiftDetails.Controls.Add(Me.lblBreakHours)
        Me.gbShiftDetails.Controls.Add(Me.lblShiftHours)
        Me.gbShiftDetails.Controls.Add(Me.lblShift)
        Me.gbShiftDetails.Controls.Add(Me.lvWorkingDays)
        Me.gbShiftDetails.Controls.Add(Me.eZeeWorkDays)
        Me.gbShiftDetails.Controls.Add(Me.objStLine)
        Me.gbShiftDetails.Controls.Add(Me.eZeeBreakInfo)
        Me.gbShiftDetails.Controls.Add(Me.eZeeShiftInfo)
        Me.gbShiftDetails.ExpandedHoverImage = Nothing
        Me.gbShiftDetails.ExpandedNormalImage = Nothing
        Me.gbShiftDetails.ExpandedPressedImage = Nothing
        Me.gbShiftDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftDetails.HeaderHeight = 25
        Me.gbShiftDetails.HeightOnCollapse = 0
        Me.gbShiftDetails.LeftTextSpace = 0
        Me.gbShiftDetails.Location = New System.Drawing.Point(12, 66)
        Me.gbShiftDetails.Name = "gbShiftDetails"
        Me.gbShiftDetails.OpenHeight = 222
        Me.gbShiftDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftDetails.ShowBorder = True
        Me.gbShiftDetails.ShowCheckBox = False
        Me.gbShiftDetails.ShowCollapseButton = False
        Me.gbShiftDetails.ShowDefaultBorderColor = True
        Me.gbShiftDetails.ShowDownButton = False
        Me.gbShiftDetails.ShowHeader = True
        Me.gbShiftDetails.Size = New System.Drawing.Size(501, 197)
        Me.gbShiftDetails.TabIndex = 1
        Me.gbShiftDetails.Temp = 0
        Me.gbShiftDetails.Text = "Shift Information"
        Me.gbShiftDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShift
        '
        Me.txtShift.Flags = 0
        Me.txtShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShift.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtShift.Location = New System.Drawing.Point(108, 59)
        Me.txtShift.Name = "txtShift"
        Me.txtShift.ReadOnly = True
        Me.txtShift.Size = New System.Drawing.Size(149, 21)
        Me.txtShift.TabIndex = 71
        '
        'txtBreakHours
        '
        Me.txtBreakHours.AllowNegative = False
        Me.txtBreakHours.DigitsInGroup = 0
        Me.txtBreakHours.Flags = 65536
        Me.txtBreakHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBreakHours.Location = New System.Drawing.Point(108, 145)
        Me.txtBreakHours.MaxDecimalPlaces = 6
        Me.txtBreakHours.MaxWholeDigits = 21
        Me.txtBreakHours.Name = "txtBreakHours"
        Me.txtBreakHours.Prefix = ""
        Me.txtBreakHours.RangeMax = 1.7976931348623157E+308
        Me.txtBreakHours.RangeMin = -1.7976931348623157E+308
        Me.txtBreakHours.ReadOnly = True
        Me.txtBreakHours.Size = New System.Drawing.Size(100, 21)
        Me.txtBreakHours.TabIndex = 69
        '
        'txtShiftHours
        '
        Me.txtShiftHours.AllowNegative = False
        Me.txtShiftHours.DigitsInGroup = 0
        Me.txtShiftHours.Flags = 65536
        Me.txtShiftHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShiftHours.Location = New System.Drawing.Point(108, 86)
        Me.txtShiftHours.MaxDecimalPlaces = 6
        Me.txtShiftHours.MaxWholeDigits = 21
        Me.txtShiftHours.Name = "txtShiftHours"
        Me.txtShiftHours.Prefix = ""
        Me.txtShiftHours.RangeMax = 1.7976931348623157E+308
        Me.txtShiftHours.RangeMin = -1.7976931348623157E+308
        Me.txtShiftHours.ReadOnly = True
        Me.txtShiftHours.Size = New System.Drawing.Size(100, 21)
        Me.txtShiftHours.TabIndex = 68
        '
        'lblBreakHours
        '
        Me.lblBreakHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBreakHours.Location = New System.Drawing.Point(27, 148)
        Me.lblBreakHours.Name = "lblBreakHours"
        Me.lblBreakHours.Size = New System.Drawing.Size(75, 15)
        Me.lblBreakHours.TabIndex = 65
        Me.lblBreakHours.Text = "Minutes"
        Me.lblBreakHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShiftHours
        '
        Me.lblShiftHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftHours.Location = New System.Drawing.Point(27, 89)
        Me.lblShiftHours.Name = "lblShiftHours"
        Me.lblShiftHours.Size = New System.Drawing.Size(75, 15)
        Me.lblShiftHours.TabIndex = 63
        Me.lblShiftHours.Text = "Hours"
        Me.lblShiftHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(27, 62)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(75, 15)
        Me.lblShift.TabIndex = 62
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvWorkingDays
        '
        Me.lvWorkingDays.CheckBoxes = True
        Me.lvWorkingDays.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lvWorkingDays.Enabled = False
        Me.lvWorkingDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvWorkingDays.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem8.StateImageIndex = 0
        ListViewItem9.StateImageIndex = 0
        ListViewItem10.StateImageIndex = 0
        ListViewItem11.StateImageIndex = 0
        ListViewItem12.StateImageIndex = 0
        ListViewItem13.StateImageIndex = 0
        ListViewItem14.StateImageIndex = 0
        Me.lvWorkingDays.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11, ListViewItem12, ListViewItem13, ListViewItem14})
        Me.lvWorkingDays.Location = New System.Drawing.Point(294, 59)
        Me.lvWorkingDays.Name = "lvWorkingDays"
        Me.lvWorkingDays.Size = New System.Drawing.Size(193, 128)
        Me.lvWorkingDays.TabIndex = 61
        Me.lvWorkingDays.UseCompatibleStateImageBehavior = False
        Me.lvWorkingDays.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 180
        '
        'eZeeWorkDays
        '
        Me.eZeeWorkDays.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eZeeWorkDays.Location = New System.Drawing.Point(291, 36)
        Me.eZeeWorkDays.Name = "eZeeWorkDays"
        Me.eZeeWorkDays.Size = New System.Drawing.Size(193, 23)
        Me.eZeeWorkDays.TabIndex = 60
        Me.eZeeWorkDays.Text = "Working Days"
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(263, 36)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(22, 151)
        Me.objStLine.TabIndex = 59
        '
        'eZeeBreakInfo
        '
        Me.eZeeBreakInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eZeeBreakInfo.Location = New System.Drawing.Point(16, 119)
        Me.eZeeBreakInfo.Name = "eZeeBreakInfo"
        Me.eZeeBreakInfo.Size = New System.Drawing.Size(241, 23)
        Me.eZeeBreakInfo.TabIndex = 2
        Me.eZeeBreakInfo.Text = "Break Information"
        '
        'eZeeShiftInfo
        '
        Me.eZeeShiftInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eZeeShiftInfo.Location = New System.Drawing.Point(16, 36)
        Me.eZeeShiftInfo.Name = "eZeeShiftInfo"
        Me.eZeeShiftInfo.Size = New System.Drawing.Size(241, 23)
        Me.eZeeShiftInfo.TabIndex = 1
        Me.eZeeShiftInfo.Text = "Shift Information"
        '
        'frmShiftInformation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 327)
        Me.Controls.Add(Me.pnlShiftInformation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShiftInformation"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Shift Details"
        Me.pnlShiftInformation.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbShiftDetails.ResumeLayout(False)
        Me.gbShiftDetails.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlShiftInformation As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbShiftDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents eZeeBreakInfo As eZee.Common.eZeeLine
    Friend WithEvents eZeeShiftInfo As eZee.Common.eZeeLine
    Friend WithEvents eZeeWorkDays As eZee.Common.eZeeLine
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents lvWorkingDays As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblBreakHours As System.Windows.Forms.Label
    Friend WithEvents lblShiftHours As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtBreakHours As eZee.TextBox.NumericTextBox
    Friend WithEvents txtShiftHours As eZee.TextBox.NumericTextBox
    Friend WithEvents txtShift As eZee.TextBox.AlphanumericTextBox
End Class
