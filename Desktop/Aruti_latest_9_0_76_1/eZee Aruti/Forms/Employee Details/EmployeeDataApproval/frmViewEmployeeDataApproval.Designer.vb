﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewEmployeeDataApproval
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewEmployeeDataApproval))
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.lblotherqualificationnote = New System.Windows.Forms.Label
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboOperationType = New System.Windows.Forms.ComboBox
        Me.lblSelectOperationType = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn62 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn63 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn64 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn65 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn66 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn67 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn68 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn69 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn70 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn71 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn72 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn73 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn74 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn75 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn76 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn77 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn78 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn79 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn80 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn81 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn82 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn83 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn84 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn85 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn86 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn87 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn88 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn89 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn90 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn91 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn92 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn93 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn94 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn95 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn96 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn97 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn98 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn99 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn100 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn101 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn102 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhicheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQualifyGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAwardDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAwardToDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstitute = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompany = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSupervisor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRefreeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTelNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbirthdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdentifyNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSrNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdentityNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdIssuePlace = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdIssueDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdDLClass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddress1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddress2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressState = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressZipCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressProvince = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressRoad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressEstate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressProvince1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressRoad1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressChiefdom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressVillage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressTown = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressTel_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressPlotNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressAltNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressFax = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhmembershipname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhmembershipno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhperiod_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhissue_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhstart_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhexpiry_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMemRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCertificateNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthTown1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthVillage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthState = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthWard = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthVillage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthChiefdom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhComplexion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBloodGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEyeColor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNationality = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEthinCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReligion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHair = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaritalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExtraTel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaritalDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllergies = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisabilities = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSportsHobbies = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVoidReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhqualificationgroupunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhqualificationunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdepedantunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefemailFooter.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(698, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.lblotherqualificationnote)
        Me.objefemailFooter.Controls.Add(Me.picStayView)
        Me.objefemailFooter.Controls.Add(Me.btnClose)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 344)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(804, 55)
        Me.objefemailFooter.TabIndex = 111
        '
        'lblotherqualificationnote
        '
        Me.lblotherqualificationnote.AutoSize = True
        Me.lblotherqualificationnote.ForeColor = System.Drawing.Color.Blue
        Me.lblotherqualificationnote.Location = New System.Drawing.Point(391, 22)
        Me.lblotherqualificationnote.Name = "lblotherqualificationnote"
        Me.lblotherqualificationnote.Size = New System.Drawing.Size(216, 13)
        Me.lblotherqualificationnote.TabIndex = 117
        Me.lblotherqualificationnote.Text = "Note: This color indicates other qualification"
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(3, 20)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 115
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhicheck, Me.dgcolhSkillCategory, Me.dgcolhSkill, Me.dgcolhDescription, Me.dgcolhQualifyGroup, Me.dgcolhQualification, Me.dgcolhAwardDate, Me.dgcolhAwardToDate, Me.dgcolhInstitute, Me.dgcolhRefNo, Me.dgcolhCompany, Me.dgcolhJob, Me.dgcolhStartDate, Me.dgcolhEndDate, Me.dgcolhSupervisor, Me.dgcolhRemark, Me.dgcolhRefreeName, Me.dgcolhCountry, Me.dgcolhIdNo, Me.dgcolhEmail, Me.dgcolhTelNo, Me.dgcolhMobile, Me.dgcolhDBName, Me.dgcolhRelation, Me.dgcolhbirthdate, Me.dgcolhIdentifyNo, Me.dgcolhGender, Me.dgcolhIdType, Me.dgcolhSrNo, Me.dgcolhIdentityNo, Me.dgcolhIdCountry, Me.dgcolhIdIssuePlace, Me.dgcolhIdIssueDate, Me.dgcolhIdExpiryDate, Me.dgcolhIdDLClass, Me.dgcolhAddressType, Me.dgcolhAddress1, Me.dgcolhAddress2, Me.dgcolhFirstname, Me.dgcolhLastname, Me.dgcolhAddressCountry, Me.dgcolhAddressState, Me.dgcolhAddressCity, Me.dgcolhAddressZipCode, Me.dgcolhAddressProvince, Me.dgcolhAddressRoad, Me.dgcolhAddressEstate, Me.dgcolhAddressProvince1, Me.dgcolhAddressRoad1, Me.dgcolhAddressChiefdom, Me.dgcolhAddressVillage, Me.dgcolhAddressTown, Me.dgcolhAddressMobile, Me.dgcolhAddressTel_no, Me.dgcolhAddressPlotNo, Me.dgcolhAddressAltNo, Me.dgcolhAddressEmail, Me.dgcolhAddressFax, Me.dgcolhCategory, Me.dgcolhmembershipname, Me.dgcolhmembershipno, Me.dgcolhperiod_name, Me.dgcolhissue_date, Me.dgcolhstart_date, Me.dgcolhexpiry_date, Me.dgcolhMemRemark, Me.dgcolhStatus, Me.objdgcolhIsGrp, Me.dgcolhBirthCountry, Me.dgcolhBirthCity, Me.dgcolhBirthCertificateNo, Me.dgcolhBirthTown1, Me.dgcolhBirthVillage1, Me.dgcolhBirthState, Me.dgcolhBirthWard, Me.dgcolhBirthVillage, Me.dgcolhBirthChiefdom, Me.dgcolhComplexion, Me.dgcolhBloodGroup, Me.dgcolhEyeColor, Me.dgcolhNationality, Me.dgcolhEthinCity, Me.dgcolhReligion, Me.dgcolhHair, Me.dgcolhMaritalStatus, Me.dgcolhExtraTel, Me.dgcolhLanguage1, Me.dgcolhLanguage2, Me.dgcolhLanguage3, Me.dgcolhLanguage4, Me.dgcolhHeight, Me.dgcolhWeight, Me.dgcolhMaritalDate, Me.dgcolhAllergies, Me.dgcolhDisabilities, Me.dgcolhSportsHobbies, Me.dgcolhVoidReason, Me.dgcolhLevel, Me.dgcolhApprover, Me.objdgcolhEmpid, Me.objcolhqualificationgroupunkid, Me.objcolhqualificationunkid, Me.objcolhdepedantunkid})
        Me.dgvData.Location = New System.Drawing.Point(12, 77)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(780, 261)
        Me.dgvData.TabIndex = 116
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboOperationType)
        Me.gbFilterCriteria.Controls.Add(Me.lblSelectOperationType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 8)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 67
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(780, 63)
        Me.gbFilterCriteria.TabIndex = 117
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOperationType
        '
        Me.cboOperationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperationType.DropDownWidth = 400
        Me.cboOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperationType.FormattingEnabled = True
        Me.cboOperationType.Location = New System.Drawing.Point(90, 33)
        Me.cboOperationType.Name = "cboOperationType"
        Me.cboOperationType.Size = New System.Drawing.Size(187, 21)
        Me.cboOperationType.TabIndex = 144
        '
        'lblSelectOperationType
        '
        Me.lblSelectOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectOperationType.Location = New System.Drawing.Point(3, 36)
        Me.lblSelectOperationType.Name = "lblSelectOperationType"
        Me.lblSelectOperationType.Size = New System.Drawing.Size(90, 15)
        Me.lblSelectOperationType.TabIndex = 143
        Me.lblSelectOperationType.Text = "Operation Type"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Skill Category"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Skill"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Qualification Group"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Award Date"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Award To Date"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Institute"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Ref. No."
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "End Date"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Supervisor"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Refree Name"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Id. No."
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Tel. No."
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "Mobile"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Level"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "objcolhqualificationgroupunkid"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "objcolhqualificationunkid"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "objcolhdepedantunkid"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "Address Type"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "Address1"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Address2"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.HeaderText = "State"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "City"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "ZipCode"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "Province"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.HeaderText = "Road"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.HeaderText = "Estate"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.HeaderText = "Province1"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.HeaderText = "Road1"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.HeaderText = "Chiefdom"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "Village"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.HeaderText = "Town"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.HeaderText = "Mobile"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.HeaderText = "Telephone Number"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.HeaderText = "Plot Number"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.HeaderText = "Alternet Number"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.HeaderText = "Fax"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.HeaderText = "Membership Category"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        Me.DataGridViewTextBoxColumn58.ReadOnly = True
        Me.DataGridViewTextBoxColumn58.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.HeaderText = "Membership Name"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        Me.DataGridViewTextBoxColumn59.ReadOnly = True
        Me.DataGridViewTextBoxColumn59.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.HeaderText = "Membership No."
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        Me.DataGridViewTextBoxColumn60.ReadOnly = True
        Me.DataGridViewTextBoxColumn60.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.HeaderText = "Period Name"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        Me.DataGridViewTextBoxColumn61.ReadOnly = True
        Me.DataGridViewTextBoxColumn61.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn62
        '
        Me.DataGridViewTextBoxColumn62.HeaderText = "Issue Date"
        Me.DataGridViewTextBoxColumn62.Name = "DataGridViewTextBoxColumn62"
        Me.DataGridViewTextBoxColumn62.ReadOnly = True
        Me.DataGridViewTextBoxColumn62.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn63
        '
        Me.DataGridViewTextBoxColumn63.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn63.Name = "DataGridViewTextBoxColumn63"
        Me.DataGridViewTextBoxColumn63.ReadOnly = True
        Me.DataGridViewTextBoxColumn63.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn64
        '
        Me.DataGridViewTextBoxColumn64.HeaderText = "Expiry Date"
        Me.DataGridViewTextBoxColumn64.Name = "DataGridViewTextBoxColumn64"
        Me.DataGridViewTextBoxColumn64.ReadOnly = True
        Me.DataGridViewTextBoxColumn64.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn65
        '
        Me.DataGridViewTextBoxColumn65.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn65.Name = "DataGridViewTextBoxColumn65"
        Me.DataGridViewTextBoxColumn65.ReadOnly = True
        Me.DataGridViewTextBoxColumn65.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn66
        '
        Me.DataGridViewTextBoxColumn66.HeaderText = "Level"
        Me.DataGridViewTextBoxColumn66.Name = "DataGridViewTextBoxColumn66"
        '
        'DataGridViewTextBoxColumn67
        '
        Me.DataGridViewTextBoxColumn67.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn67.Name = "DataGridViewTextBoxColumn67"
        '
        'DataGridViewTextBoxColumn68
        '
        Me.DataGridViewTextBoxColumn68.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn68.Name = "DataGridViewTextBoxColumn68"
        '
        'DataGridViewTextBoxColumn69
        '
        Me.DataGridViewTextBoxColumn69.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn69.Name = "DataGridViewTextBoxColumn69"
        Me.DataGridViewTextBoxColumn69.Visible = False
        '
        'DataGridViewTextBoxColumn70
        '
        Me.DataGridViewTextBoxColumn70.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn70.Name = "DataGridViewTextBoxColumn70"
        '
        'DataGridViewTextBoxColumn71
        '
        Me.DataGridViewTextBoxColumn71.HeaderText = "City"
        Me.DataGridViewTextBoxColumn71.Name = "DataGridViewTextBoxColumn71"
        '
        'DataGridViewTextBoxColumn72
        '
        Me.DataGridViewTextBoxColumn72.HeaderText = "Certificate No."
        Me.DataGridViewTextBoxColumn72.Name = "DataGridViewTextBoxColumn72"
        '
        'DataGridViewTextBoxColumn73
        '
        Me.DataGridViewTextBoxColumn73.HeaderText = "Town1"
        Me.DataGridViewTextBoxColumn73.Name = "DataGridViewTextBoxColumn73"
        '
        'DataGridViewTextBoxColumn74
        '
        Me.DataGridViewTextBoxColumn74.HeaderText = "Village1"
        Me.DataGridViewTextBoxColumn74.Name = "DataGridViewTextBoxColumn74"
        '
        'DataGridViewTextBoxColumn75
        '
        Me.DataGridViewTextBoxColumn75.HeaderText = "State"
        Me.DataGridViewTextBoxColumn75.Name = "DataGridViewTextBoxColumn75"
        '
        'DataGridViewTextBoxColumn76
        '
        Me.DataGridViewTextBoxColumn76.HeaderText = "Ward"
        Me.DataGridViewTextBoxColumn76.Name = "DataGridViewTextBoxColumn76"
        '
        'DataGridViewTextBoxColumn77
        '
        Me.DataGridViewTextBoxColumn77.HeaderText = "Village"
        Me.DataGridViewTextBoxColumn77.Name = "DataGridViewTextBoxColumn77"
        '
        'DataGridViewTextBoxColumn78
        '
        Me.DataGridViewTextBoxColumn78.HeaderText = "Chiefdom"
        Me.DataGridViewTextBoxColumn78.Name = "DataGridViewTextBoxColumn78"
        '
        'DataGridViewTextBoxColumn79
        '
        Me.DataGridViewTextBoxColumn79.HeaderText = "Complexion"
        Me.DataGridViewTextBoxColumn79.Name = "DataGridViewTextBoxColumn79"
        '
        'DataGridViewTextBoxColumn80
        '
        Me.DataGridViewTextBoxColumn80.HeaderText = "Blood Group"
        Me.DataGridViewTextBoxColumn80.Name = "DataGridViewTextBoxColumn80"
        '
        'DataGridViewTextBoxColumn81
        '
        Me.DataGridViewTextBoxColumn81.HeaderText = "Eye Color"
        Me.DataGridViewTextBoxColumn81.Name = "DataGridViewTextBoxColumn81"
        '
        'DataGridViewTextBoxColumn82
        '
        Me.DataGridViewTextBoxColumn82.HeaderText = "Nationality"
        Me.DataGridViewTextBoxColumn82.Name = "DataGridViewTextBoxColumn82"
        '
        'DataGridViewTextBoxColumn83
        '
        Me.DataGridViewTextBoxColumn83.HeaderText = "EthinCity"
        Me.DataGridViewTextBoxColumn83.Name = "DataGridViewTextBoxColumn83"
        '
        'DataGridViewTextBoxColumn84
        '
        Me.DataGridViewTextBoxColumn84.HeaderText = "Religion"
        Me.DataGridViewTextBoxColumn84.Name = "DataGridViewTextBoxColumn84"
        '
        'DataGridViewTextBoxColumn85
        '
        Me.DataGridViewTextBoxColumn85.HeaderText = "Hair Color"
        Me.DataGridViewTextBoxColumn85.Name = "DataGridViewTextBoxColumn85"
        '
        'DataGridViewTextBoxColumn86
        '
        Me.DataGridViewTextBoxColumn86.HeaderText = "MaritalStatus"
        Me.DataGridViewTextBoxColumn86.Name = "DataGridViewTextBoxColumn86"
        '
        'DataGridViewTextBoxColumn87
        '
        Me.DataGridViewTextBoxColumn87.HeaderText = "Extra Telephone"
        Me.DataGridViewTextBoxColumn87.Name = "DataGridViewTextBoxColumn87"
        '
        'DataGridViewTextBoxColumn88
        '
        Me.DataGridViewTextBoxColumn88.HeaderText = "Language1"
        Me.DataGridViewTextBoxColumn88.Name = "DataGridViewTextBoxColumn88"
        '
        'DataGridViewTextBoxColumn89
        '
        Me.DataGridViewTextBoxColumn89.HeaderText = "Language2"
        Me.DataGridViewTextBoxColumn89.Name = "DataGridViewTextBoxColumn89"
        '
        'DataGridViewTextBoxColumn90
        '
        Me.DataGridViewTextBoxColumn90.HeaderText = "Language3"
        Me.DataGridViewTextBoxColumn90.Name = "DataGridViewTextBoxColumn90"
        '
        'DataGridViewTextBoxColumn91
        '
        Me.DataGridViewTextBoxColumn91.HeaderText = "Language4"
        Me.DataGridViewTextBoxColumn91.Name = "DataGridViewTextBoxColumn91"
        '
        'DataGridViewTextBoxColumn92
        '
        Me.DataGridViewTextBoxColumn92.HeaderText = "Height"
        Me.DataGridViewTextBoxColumn92.Name = "DataGridViewTextBoxColumn92"
        '
        'DataGridViewTextBoxColumn93
        '
        Me.DataGridViewTextBoxColumn93.HeaderText = "Weight"
        Me.DataGridViewTextBoxColumn93.Name = "DataGridViewTextBoxColumn93"
        '
        'DataGridViewTextBoxColumn94
        '
        Me.DataGridViewTextBoxColumn94.HeaderText = "Marital Date"
        Me.DataGridViewTextBoxColumn94.Name = "DataGridViewTextBoxColumn94"
        '
        'DataGridViewTextBoxColumn95
        '
        Me.DataGridViewTextBoxColumn95.HeaderText = "Allergies"
        Me.DataGridViewTextBoxColumn95.Name = "DataGridViewTextBoxColumn95"
        '
        'DataGridViewTextBoxColumn96
        '
        Me.DataGridViewTextBoxColumn96.HeaderText = "Disabilities"
        Me.DataGridViewTextBoxColumn96.Name = "DataGridViewTextBoxColumn96"
        '
        'DataGridViewTextBoxColumn97
        '
        Me.DataGridViewTextBoxColumn97.HeaderText = "Sports & Hobbies"
        Me.DataGridViewTextBoxColumn97.Name = "DataGridViewTextBoxColumn97"
        '
        'DataGridViewTextBoxColumn98
        '
        Me.DataGridViewTextBoxColumn98.HeaderText = "Void Reason"
        Me.DataGridViewTextBoxColumn98.Name = "DataGridViewTextBoxColumn98"
        '
        'DataGridViewTextBoxColumn99
        '
        Me.DataGridViewTextBoxColumn99.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn99.Name = "DataGridViewTextBoxColumn99"
        Me.DataGridViewTextBoxColumn99.Visible = False
        '
        'DataGridViewTextBoxColumn100
        '
        Me.DataGridViewTextBoxColumn100.HeaderText = "objcolhqualificationgroupunkid"
        Me.DataGridViewTextBoxColumn100.Name = "DataGridViewTextBoxColumn100"
        Me.DataGridViewTextBoxColumn100.Visible = False
        '
        'DataGridViewTextBoxColumn101
        '
        Me.DataGridViewTextBoxColumn101.HeaderText = "objcolhqualificationunkid"
        Me.DataGridViewTextBoxColumn101.Name = "DataGridViewTextBoxColumn101"
        Me.DataGridViewTextBoxColumn101.Visible = False
        '
        'DataGridViewTextBoxColumn102
        '
        Me.DataGridViewTextBoxColumn102.HeaderText = "objcolhdepedantunkid"
        Me.DataGridViewTextBoxColumn102.Name = "DataGridViewTextBoxColumn102"
        Me.DataGridViewTextBoxColumn102.Visible = False
        '
        'objdgcolhicheck
        '
        Me.objdgcolhicheck.HeaderText = ""
        Me.objdgcolhicheck.Name = "objdgcolhicheck"
        Me.objdgcolhicheck.Visible = False
        '
        'dgcolhSkillCategory
        '
        Me.dgcolhSkillCategory.HeaderText = "Skill Category"
        Me.dgcolhSkillCategory.Name = "dgcolhSkillCategory"
        Me.dgcolhSkillCategory.ReadOnly = True
        Me.dgcolhSkillCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSkill
        '
        Me.dgcolhSkill.HeaderText = "Skill"
        Me.dgcolhSkill.Name = "dgcolhSkill"
        Me.dgcolhSkill.ReadOnly = True
        Me.dgcolhSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDescription
        '
        Me.dgcolhDescription.HeaderText = "Description"
        Me.dgcolhDescription.Name = "dgcolhDescription"
        Me.dgcolhDescription.ReadOnly = True
        Me.dgcolhDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQualifyGroup
        '
        Me.dgcolhQualifyGroup.HeaderText = "Qualification Group"
        Me.dgcolhQualifyGroup.Name = "dgcolhQualifyGroup"
        Me.dgcolhQualifyGroup.ReadOnly = True
        Me.dgcolhQualifyGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQualification
        '
        Me.dgcolhQualification.HeaderText = "Qualification"
        Me.dgcolhQualification.Name = "dgcolhQualification"
        Me.dgcolhQualification.ReadOnly = True
        Me.dgcolhQualification.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAwardDate
        '
        Me.dgcolhAwardDate.HeaderText = "Award Date"
        Me.dgcolhAwardDate.Name = "dgcolhAwardDate"
        Me.dgcolhAwardDate.ReadOnly = True
        Me.dgcolhAwardDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAwardToDate
        '
        Me.dgcolhAwardToDate.HeaderText = "Award To Date"
        Me.dgcolhAwardToDate.Name = "dgcolhAwardToDate"
        Me.dgcolhAwardToDate.ReadOnly = True
        Me.dgcolhAwardToDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInstitute
        '
        Me.dgcolhInstitute.HeaderText = "Institute"
        Me.dgcolhInstitute.Name = "dgcolhInstitute"
        Me.dgcolhInstitute.ReadOnly = True
        Me.dgcolhInstitute.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Ref. No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCompany
        '
        Me.dgcolhCompany.HeaderText = "Company"
        Me.dgcolhCompany.Name = "dgcolhCompany"
        Me.dgcolhCompany.ReadOnly = True
        Me.dgcolhCompany.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStartDate
        '
        Me.dgcolhStartDate.HeaderText = "Start Date"
        Me.dgcolhStartDate.Name = "dgcolhStartDate"
        Me.dgcolhStartDate.ReadOnly = True
        Me.dgcolhStartDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEndDate
        '
        Me.dgcolhEndDate.HeaderText = "End Date"
        Me.dgcolhEndDate.Name = "dgcolhEndDate"
        Me.dgcolhEndDate.ReadOnly = True
        Me.dgcolhEndDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSupervisor
        '
        Me.dgcolhSupervisor.HeaderText = "Supervisor"
        Me.dgcolhSupervisor.Name = "dgcolhSupervisor"
        Me.dgcolhSupervisor.ReadOnly = True
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRefreeName
        '
        Me.dgcolhRefreeName.HeaderText = "Refree Name"
        Me.dgcolhRefreeName.Name = "dgcolhRefreeName"
        Me.dgcolhRefreeName.ReadOnly = True
        Me.dgcolhRefreeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCountry
        '
        Me.dgcolhCountry.HeaderText = "Country"
        Me.dgcolhCountry.Name = "dgcolhCountry"
        Me.dgcolhCountry.ReadOnly = True
        Me.dgcolhCountry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdNo
        '
        Me.dgcolhIdNo.HeaderText = "Id. No."
        Me.dgcolhIdNo.Name = "dgcolhIdNo"
        Me.dgcolhIdNo.ReadOnly = True
        Me.dgcolhIdNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTelNo
        '
        Me.dgcolhTelNo.HeaderText = "Tel. No."
        Me.dgcolhTelNo.Name = "dgcolhTelNo"
        Me.dgcolhTelNo.ReadOnly = True
        Me.dgcolhTelNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMobile
        '
        Me.dgcolhMobile.HeaderText = "Mobile"
        Me.dgcolhMobile.Name = "dgcolhMobile"
        Me.dgcolhMobile.ReadOnly = True
        Me.dgcolhMobile.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDBName
        '
        Me.dgcolhDBName.HeaderText = "Name"
        Me.dgcolhDBName.Name = "dgcolhDBName"
        '
        'dgcolhRelation
        '
        Me.dgcolhRelation.HeaderText = "Relation"
        Me.dgcolhRelation.Name = "dgcolhRelation"
        '
        'dgcolhbirthdate
        '
        Me.dgcolhbirthdate.HeaderText = "Birth Date"
        Me.dgcolhbirthdate.Name = "dgcolhbirthdate"
        '
        'dgcolhIdentifyNo
        '
        Me.dgcolhIdentifyNo.HeaderText = "Identify No."
        Me.dgcolhIdentifyNo.Name = "dgcolhIdentifyNo"
        '
        'dgcolhGender
        '
        Me.dgcolhGender.HeaderText = "Gender"
        Me.dgcolhGender.Name = "dgcolhGender"
        '
        'dgcolhIdType
        '
        Me.dgcolhIdType.HeaderText = "Identity Type"
        Me.dgcolhIdType.Name = "dgcolhIdType"
        Me.dgcolhIdType.ReadOnly = True
        '
        'dgcolhSrNo
        '
        Me.dgcolhSrNo.HeaderText = "Serial No."
        Me.dgcolhSrNo.Name = "dgcolhSrNo"
        Me.dgcolhSrNo.ReadOnly = True
        Me.dgcolhSrNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdentityNo
        '
        Me.dgcolhIdentityNo.HeaderText = "Identity No."
        Me.dgcolhIdentityNo.Name = "dgcolhIdentityNo"
        Me.dgcolhIdentityNo.ReadOnly = True
        Me.dgcolhIdentityNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdCountry
        '
        Me.dgcolhIdCountry.HeaderText = "Country"
        Me.dgcolhIdCountry.Name = "dgcolhIdCountry"
        Me.dgcolhIdCountry.ReadOnly = True
        Me.dgcolhIdCountry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdIssuePlace
        '
        Me.dgcolhIdIssuePlace.HeaderText = "Issue Place"
        Me.dgcolhIdIssuePlace.Name = "dgcolhIdIssuePlace"
        Me.dgcolhIdIssuePlace.ReadOnly = True
        Me.dgcolhIdIssuePlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdIssueDate
        '
        Me.dgcolhIdIssueDate.HeaderText = "Issue Date"
        Me.dgcolhIdIssueDate.Name = "dgcolhIdIssueDate"
        Me.dgcolhIdIssueDate.ReadOnly = True
        Me.dgcolhIdIssueDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdExpiryDate
        '
        Me.dgcolhIdExpiryDate.HeaderText = "Expiry Date"
        Me.dgcolhIdExpiryDate.Name = "dgcolhIdExpiryDate"
        Me.dgcolhIdExpiryDate.ReadOnly = True
        Me.dgcolhIdExpiryDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdDLClass
        '
        Me.dgcolhIdDLClass.HeaderText = "DL Class"
        Me.dgcolhIdDLClass.Name = "dgcolhIdDLClass"
        Me.dgcolhIdDLClass.ReadOnly = True
        Me.dgcolhIdDLClass.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAddressType
        '
        Me.dgcolhAddressType.HeaderText = "Address Type"
        Me.dgcolhAddressType.Name = "dgcolhAddressType"
        '
        'dgcolhAddress1
        '
        Me.dgcolhAddress1.HeaderText = "Address1"
        Me.dgcolhAddress1.Name = "dgcolhAddress1"
        '
        'dgcolhAddress2
        '
        Me.dgcolhAddress2.HeaderText = "Address2"
        Me.dgcolhAddress2.Name = "dgcolhAddress2"
        '
        'dgcolhFirstname
        '
        Me.dgcolhFirstname.HeaderText = "First Name"
        Me.dgcolhFirstname.Name = "dgcolhFirstname"
        '
        'dgcolhLastname
        '
        Me.dgcolhLastname.HeaderText = "Last Name"
        Me.dgcolhLastname.Name = "dgcolhLastname"
        '
        'dgcolhAddressCountry
        '
        Me.dgcolhAddressCountry.HeaderText = "Country"
        Me.dgcolhAddressCountry.Name = "dgcolhAddressCountry"
        '
        'dgcolhAddressState
        '
        Me.dgcolhAddressState.HeaderText = "State"
        Me.dgcolhAddressState.Name = "dgcolhAddressState"
        '
        'dgcolhAddressCity
        '
        Me.dgcolhAddressCity.HeaderText = "City"
        Me.dgcolhAddressCity.Name = "dgcolhAddressCity"
        '
        'dgcolhAddressZipCode
        '
        Me.dgcolhAddressZipCode.HeaderText = "ZipCode"
        Me.dgcolhAddressZipCode.Name = "dgcolhAddressZipCode"
        '
        'dgcolhAddressProvince
        '
        Me.dgcolhAddressProvince.HeaderText = "Province"
        Me.dgcolhAddressProvince.Name = "dgcolhAddressProvince"
        '
        'dgcolhAddressRoad
        '
        Me.dgcolhAddressRoad.HeaderText = "Road"
        Me.dgcolhAddressRoad.Name = "dgcolhAddressRoad"
        '
        'dgcolhAddressEstate
        '
        Me.dgcolhAddressEstate.HeaderText = "Estate"
        Me.dgcolhAddressEstate.Name = "dgcolhAddressEstate"
        '
        'dgcolhAddressProvince1
        '
        Me.dgcolhAddressProvince1.HeaderText = "Province1"
        Me.dgcolhAddressProvince1.Name = "dgcolhAddressProvince1"
        '
        'dgcolhAddressRoad1
        '
        Me.dgcolhAddressRoad1.HeaderText = "Road1"
        Me.dgcolhAddressRoad1.Name = "dgcolhAddressRoad1"
        '
        'dgcolhAddressChiefdom
        '
        Me.dgcolhAddressChiefdom.HeaderText = "Chiefdom"
        Me.dgcolhAddressChiefdom.Name = "dgcolhAddressChiefdom"
        '
        'dgcolhAddressVillage
        '
        Me.dgcolhAddressVillage.HeaderText = "Village"
        Me.dgcolhAddressVillage.Name = "dgcolhAddressVillage"
        '
        'dgcolhAddressTown
        '
        Me.dgcolhAddressTown.HeaderText = "Town"
        Me.dgcolhAddressTown.Name = "dgcolhAddressTown"
        '
        'dgcolhAddressMobile
        '
        Me.dgcolhAddressMobile.HeaderText = "Mobile"
        Me.dgcolhAddressMobile.Name = "dgcolhAddressMobile"
        '
        'dgcolhAddressTel_no
        '
        Me.dgcolhAddressTel_no.HeaderText = "Telephone Number"
        Me.dgcolhAddressTel_no.Name = "dgcolhAddressTel_no"
        '
        'dgcolhAddressPlotNo
        '
        Me.dgcolhAddressPlotNo.HeaderText = "Plot Number"
        Me.dgcolhAddressPlotNo.Name = "dgcolhAddressPlotNo"
        '
        'dgcolhAddressAltNo
        '
        Me.dgcolhAddressAltNo.HeaderText = "Alternet Number"
        Me.dgcolhAddressAltNo.Name = "dgcolhAddressAltNo"
        '
        'dgcolhAddressEmail
        '
        Me.dgcolhAddressEmail.HeaderText = "Email"
        Me.dgcolhAddressEmail.Name = "dgcolhAddressEmail"
        '
        'dgcolhAddressFax
        '
        Me.dgcolhAddressFax.HeaderText = "Fax"
        Me.dgcolhAddressFax.Name = "dgcolhAddressFax"
        '
        'dgcolhCategory
        '
        Me.dgcolhCategory.HeaderText = "Membership Category"
        Me.dgcolhCategory.Name = "dgcolhCategory"
        Me.dgcolhCategory.ReadOnly = True
        Me.dgcolhCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhmembershipname
        '
        Me.dgcolhmembershipname.HeaderText = "Membership Name"
        Me.dgcolhmembershipname.Name = "dgcolhmembershipname"
        Me.dgcolhmembershipname.ReadOnly = True
        Me.dgcolhmembershipname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhmembershipno
        '
        Me.dgcolhmembershipno.HeaderText = "Membership No."
        Me.dgcolhmembershipno.Name = "dgcolhmembershipno"
        Me.dgcolhmembershipno.ReadOnly = True
        Me.dgcolhmembershipno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhperiod_name
        '
        Me.dgcolhperiod_name.HeaderText = "Period Name"
        Me.dgcolhperiod_name.Name = "dgcolhperiod_name"
        Me.dgcolhperiod_name.ReadOnly = True
        Me.dgcolhperiod_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhissue_date
        '
        Me.dgcolhissue_date.HeaderText = "Issue Date"
        Me.dgcolhissue_date.Name = "dgcolhissue_date"
        Me.dgcolhissue_date.ReadOnly = True
        Me.dgcolhissue_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhstart_date
        '
        Me.dgcolhstart_date.HeaderText = "Start Date"
        Me.dgcolhstart_date.Name = "dgcolhstart_date"
        Me.dgcolhstart_date.ReadOnly = True
        Me.dgcolhstart_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhexpiry_date
        '
        Me.dgcolhexpiry_date.HeaderText = "Expiry Date"
        Me.dgcolhexpiry_date.Name = "dgcolhexpiry_date"
        Me.dgcolhexpiry_date.ReadOnly = True
        Me.dgcolhexpiry_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMemRemark
        '
        Me.dgcolhMemRemark.HeaderText = "Remark"
        Me.dgcolhMemRemark.Name = "dgcolhMemRemark"
        Me.dgcolhMemRemark.ReadOnly = True
        Me.dgcolhMemRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'dgcolhBirthCountry
        '
        Me.dgcolhBirthCountry.HeaderText = "Country"
        Me.dgcolhBirthCountry.Name = "dgcolhBirthCountry"
        '
        'dgcolhBirthCity
        '
        Me.dgcolhBirthCity.HeaderText = "City"
        Me.dgcolhBirthCity.Name = "dgcolhBirthCity"
        '
        'dgcolhBirthCertificateNo
        '
        Me.dgcolhBirthCertificateNo.HeaderText = "Certificate No."
        Me.dgcolhBirthCertificateNo.Name = "dgcolhBirthCertificateNo"
        '
        'dgcolhBirthTown1
        '
        Me.dgcolhBirthTown1.HeaderText = "Town1"
        Me.dgcolhBirthTown1.Name = "dgcolhBirthTown1"
        '
        'dgcolhBirthVillage1
        '
        Me.dgcolhBirthVillage1.HeaderText = "Village1"
        Me.dgcolhBirthVillage1.Name = "dgcolhBirthVillage1"
        '
        'dgcolhBirthState
        '
        Me.dgcolhBirthState.HeaderText = "State"
        Me.dgcolhBirthState.Name = "dgcolhBirthState"
        '
        'dgcolhBirthWard
        '
        Me.dgcolhBirthWard.HeaderText = "Ward"
        Me.dgcolhBirthWard.Name = "dgcolhBirthWard"
        '
        'dgcolhBirthVillage
        '
        Me.dgcolhBirthVillage.HeaderText = "Village"
        Me.dgcolhBirthVillage.Name = "dgcolhBirthVillage"
        '
        'dgcolhBirthChiefdom
        '
        Me.dgcolhBirthChiefdom.HeaderText = "Chiefdom"
        Me.dgcolhBirthChiefdom.Name = "dgcolhBirthChiefdom"
        '
        'dgcolhComplexion
        '
        Me.dgcolhComplexion.HeaderText = "Complexion"
        Me.dgcolhComplexion.Name = "dgcolhComplexion"
        '
        'dgcolhBloodGroup
        '
        Me.dgcolhBloodGroup.HeaderText = "Blood Group"
        Me.dgcolhBloodGroup.Name = "dgcolhBloodGroup"
        '
        'dgcolhEyeColor
        '
        Me.dgcolhEyeColor.HeaderText = "Eye Color"
        Me.dgcolhEyeColor.Name = "dgcolhEyeColor"
        '
        'dgcolhNationality
        '
        Me.dgcolhNationality.HeaderText = "Nationality"
        Me.dgcolhNationality.Name = "dgcolhNationality"
        '
        'dgcolhEthinCity
        '
        Me.dgcolhEthinCity.HeaderText = "EthinCity"
        Me.dgcolhEthinCity.Name = "dgcolhEthinCity"
        '
        'dgcolhReligion
        '
        Me.dgcolhReligion.HeaderText = "Religion"
        Me.dgcolhReligion.Name = "dgcolhReligion"
        '
        'dgcolhHair
        '
        Me.dgcolhHair.HeaderText = "Hair Color"
        Me.dgcolhHair.Name = "dgcolhHair"
        '
        'dgcolhMaritalStatus
        '
        Me.dgcolhMaritalStatus.HeaderText = "MaritalStatus"
        Me.dgcolhMaritalStatus.Name = "dgcolhMaritalStatus"
        '
        'dgcolhExtraTel
        '
        Me.dgcolhExtraTel.HeaderText = "Extra Telephone"
        Me.dgcolhExtraTel.Name = "dgcolhExtraTel"
        '
        'dgcolhLanguage1
        '
        Me.dgcolhLanguage1.HeaderText = "Language1"
        Me.dgcolhLanguage1.Name = "dgcolhLanguage1"
        '
        'dgcolhLanguage2
        '
        Me.dgcolhLanguage2.HeaderText = "Language2"
        Me.dgcolhLanguage2.Name = "dgcolhLanguage2"
        '
        'dgcolhLanguage3
        '
        Me.dgcolhLanguage3.HeaderText = "Language3"
        Me.dgcolhLanguage3.Name = "dgcolhLanguage3"
        '
        'dgcolhLanguage4
        '
        Me.dgcolhLanguage4.HeaderText = "Language4"
        Me.dgcolhLanguage4.Name = "dgcolhLanguage4"
        '
        'dgcolhHeight
        '
        Me.dgcolhHeight.HeaderText = "Height"
        Me.dgcolhHeight.Name = "dgcolhHeight"
        '
        'dgcolhWeight
        '
        Me.dgcolhWeight.HeaderText = "Weight"
        Me.dgcolhWeight.Name = "dgcolhWeight"
        '
        'dgcolhMaritalDate
        '
        Me.dgcolhMaritalDate.HeaderText = "Marital Date"
        Me.dgcolhMaritalDate.Name = "dgcolhMaritalDate"
        '
        'dgcolhAllergies
        '
        Me.dgcolhAllergies.HeaderText = "Allergies"
        Me.dgcolhAllergies.Name = "dgcolhAllergies"
        '
        'dgcolhDisabilities
        '
        Me.dgcolhDisabilities.HeaderText = "Disabilities"
        Me.dgcolhDisabilities.Name = "dgcolhDisabilities"
        '
        'dgcolhSportsHobbies
        '
        Me.dgcolhSportsHobbies.HeaderText = "Sports & Hobbies"
        Me.dgcolhSportsHobbies.Name = "dgcolhSportsHobbies"
        '
        'dgcolhVoidReason
        '
        Me.dgcolhVoidReason.HeaderText = "Void Reason"
        Me.dgcolhVoidReason.Name = "dgcolhVoidReason"
        '
        'dgcolhLevel
        '
        Me.dgcolhLevel.HeaderText = "Level"
        Me.dgcolhLevel.Name = "dgcolhLevel"
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        '
        'objdgcolhEmpid
        '
        Me.objdgcolhEmpid.HeaderText = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Name = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Visible = False
        '
        'objcolhqualificationgroupunkid
        '
        Me.objcolhqualificationgroupunkid.HeaderText = "objcolhqualificationgroupunkid"
        Me.objcolhqualificationgroupunkid.Name = "objcolhqualificationgroupunkid"
        Me.objcolhqualificationgroupunkid.Visible = False
        '
        'objcolhqualificationunkid
        '
        Me.objcolhqualificationunkid.HeaderText = "objcolhqualificationunkid"
        Me.objcolhqualificationunkid.Name = "objcolhqualificationunkid"
        Me.objcolhqualificationunkid.Visible = False
        '
        'objcolhdepedantunkid
        '
        Me.objcolhdepedantunkid.HeaderText = "objcolhdepedantunkid"
        Me.objcolhdepedantunkid.Name = "objcolhdepedantunkid"
        Me.objcolhdepedantunkid.Visible = False
        '
        'frmViewEmployeeDataApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(804, 399)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.objefemailFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewEmployeeDataApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmViewEmployeeDataApproval"
        Me.objefemailFooter.ResumeLayout(False)
        Me.objefemailFooter.PerformLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents lblotherqualificationnote As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblSelectOperationType As System.Windows.Forms.Label
    Friend WithEvents cboOperationType As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhicheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQualifyGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAwardDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAwardToDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstitute As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompany As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSupervisor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRefreeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTelNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhbirthdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdentifyNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSrNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdentityNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdIssuePlace As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdIssueDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdDLClass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddress1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddress2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressState As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressZipCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressProvince As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressRoad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressEstate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressProvince1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressRoad1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressChiefdom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressVillage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressTown As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressTel_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressPlotNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressAltNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressFax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhmembershipname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhmembershipno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhperiod_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhissue_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhstart_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhexpiry_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMemRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCertificateNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthTown1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthVillage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthState As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthVillage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthChiefdom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhComplexion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBloodGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEyeColor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNationality As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEthinCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReligion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHair As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaritalStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExtraTel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaritalDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllergies As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisabilities As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSportsHobbies As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVoidReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhqualificationgroupunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhqualificationunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdepedantunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn62 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn63 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn64 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn65 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn66 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn67 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn68 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn69 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn70 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn71 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn72 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn73 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn74 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn75 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn76 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn77 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn78 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn79 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn80 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn81 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn82 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn83 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn84 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn85 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn86 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn87 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn88 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn89 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn90 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn91 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn92 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn93 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn94 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn95 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn96 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn97 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn98 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn99 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn100 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn101 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn102 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
