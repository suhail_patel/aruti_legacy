﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Memberships
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elMemberships = New eZee.Common.eZeeLine
        Me.lvMembershipInfo = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhDCategory = New System.Windows.Forms.ColumnHeader
        Me.colhMCategory = New System.Windows.Forms.ColumnHeader
        Me.colhMembership = New System.Windows.Forms.ColumnHeader
        Me.colhMNumber = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.elIdentities = New eZee.Common.eZeeLine
        Me.pnlIdentityInfo = New System.Windows.Forms.Panel
        Me.lvIdendtifyInformation = New System.Windows.Forms.ListView
        Me.colhIdType = New System.Windows.Forms.ColumnHeader
        Me.colhIdSerialNo = New System.Windows.Forms.ColumnHeader
        Me.colhIdNo = New System.Windows.Forms.ColumnHeader
        Me.colhIssueCountry = New System.Windows.Forms.ColumnHeader
        Me.pnlIdentityInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'elMemberships
        '
        Me.elMemberships.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elMemberships.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMemberships.Location = New System.Drawing.Point(3, 9)
        Me.elMemberships.Name = "elMemberships"
        Me.elMemberships.Size = New System.Drawing.Size(575, 17)
        Me.elMemberships.TabIndex = 320
        Me.elMemberships.Text = "Employee Memberships"
        Me.elMemberships.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvMembershipInfo
        '
        Me.lvMembershipInfo.BackColorOnChecked = True
        Me.lvMembershipInfo.ColumnHeaders = Nothing
        Me.lvMembershipInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhDCategory, Me.colhMCategory, Me.colhMembership, Me.colhMNumber, Me.colhRemark})
        Me.lvMembershipInfo.CompulsoryColumns = ""
        Me.lvMembershipInfo.FullRowSelect = True
        Me.lvMembershipInfo.GridLines = True
        Me.lvMembershipInfo.GroupingColumn = Nothing
        Me.lvMembershipInfo.HideSelection = False
        Me.lvMembershipInfo.Location = New System.Drawing.Point(5, 32)
        Me.lvMembershipInfo.MinColumnWidth = 50
        Me.lvMembershipInfo.MultiSelect = False
        Me.lvMembershipInfo.Name = "lvMembershipInfo"
        Me.lvMembershipInfo.OptionalColumns = ""
        Me.lvMembershipInfo.ShowMoreItem = False
        Me.lvMembershipInfo.ShowSaveItem = False
        Me.lvMembershipInfo.ShowSelectAll = True
        Me.lvMembershipInfo.ShowSizeAllColumnsToFit = True
        Me.lvMembershipInfo.Size = New System.Drawing.Size(570, 165)
        Me.lvMembershipInfo.Sortable = True
        Me.lvMembershipInfo.TabIndex = 321
        Me.lvMembershipInfo.UseCompatibleStateImageBehavior = False
        Me.lvMembershipInfo.View = System.Windows.Forms.View.Details
        '
        'objcolhDCategory
        '
        Me.objcolhDCategory.Tag = "objcolhDCategory"
        Me.objcolhDCategory.Text = ""
        Me.objcolhDCategory.Width = 0
        '
        'colhMCategory
        '
        Me.colhMCategory.Tag = "colhMCategory"
        Me.colhMCategory.Text = "Mem. Category"
        Me.colhMCategory.Width = 130
        '
        'colhMembership
        '
        Me.colhMembership.Tag = "colhMembership"
        Me.colhMembership.Text = "Membership"
        Me.colhMembership.Width = 130
        '
        'colhMNumber
        '
        Me.colhMNumber.Tag = "colhMNumber"
        Me.colhMNumber.Text = "Mem. Number"
        Me.colhMNumber.Width = 130
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 175
        '
        'elIdentities
        '
        Me.elIdentities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elIdentities.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elIdentities.Location = New System.Drawing.Point(3, 202)
        Me.elIdentities.Name = "elIdentities"
        Me.elIdentities.Size = New System.Drawing.Size(575, 17)
        Me.elIdentities.TabIndex = 322
        Me.elIdentities.Text = "Employee Identites"
        Me.elIdentities.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlIdentityInfo
        '
        Me.pnlIdentityInfo.Controls.Add(Me.lvIdendtifyInformation)
        Me.pnlIdentityInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlIdentityInfo.Location = New System.Drawing.Point(6, 224)
        Me.pnlIdentityInfo.Name = "pnlIdentityInfo"
        Me.pnlIdentityInfo.Size = New System.Drawing.Size(569, 182)
        Me.pnlIdentityInfo.TabIndex = 323
        '
        'lvIdendtifyInformation
        '
        Me.lvIdendtifyInformation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhIdType, Me.colhIdSerialNo, Me.colhIdNo, Me.colhIssueCountry})
        Me.lvIdendtifyInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvIdendtifyInformation.FullRowSelect = True
        Me.lvIdendtifyInformation.GridLines = True
        Me.lvIdendtifyInformation.Location = New System.Drawing.Point(0, 0)
        Me.lvIdendtifyInformation.Name = "lvIdendtifyInformation"
        Me.lvIdendtifyInformation.Size = New System.Drawing.Size(569, 182)
        Me.lvIdendtifyInformation.TabIndex = 0
        Me.lvIdendtifyInformation.UseCompatibleStateImageBehavior = False
        Me.lvIdendtifyInformation.View = System.Windows.Forms.View.Details
        '
        'colhIdType
        '
        Me.colhIdType.Tag = "colhIdType"
        Me.colhIdType.Text = "Identity Type"
        Me.colhIdType.Width = 150
        '
        'colhIdSerialNo
        '
        Me.colhIdSerialNo.Tag = "colhIdSerialNo"
        Me.colhIdSerialNo.Text = "Serial No"
        Me.colhIdSerialNo.Width = 115
        '
        'colhIdNo
        '
        Me.colhIdNo.Tag = "colhIdNo"
        Me.colhIdNo.Text = "Identity No"
        Me.colhIdNo.Width = 184
        '
        'colhIssueCountry
        '
        Me.colhIssueCountry.Tag = "colhIssueCountry"
        Me.colhIssueCountry.Text = "Country"
        Me.colhIssueCountry.Width = 115
        '
        'objfrmDiary_Memberships
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.pnlIdentityInfo)
        Me.Controls.Add(Me.elIdentities)
        Me.Controls.Add(Me.lvMembershipInfo)
        Me.Controls.Add(Me.elMemberships)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Memberships"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlIdentityInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elMemberships As eZee.Common.eZeeLine
    Friend WithEvents lvMembershipInfo As eZee.Common.eZeeListView
    Friend WithEvents objcolhDCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembership As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents elIdentities As eZee.Common.eZeeLine
    Friend WithEvents pnlIdentityInfo As System.Windows.Forms.Panel
    Friend WithEvents lvIdendtifyInformation As System.Windows.Forms.ListView
    Friend WithEvents colhIdType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdSerialNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIssueCountry As System.Windows.Forms.ColumnHeader
End Class
