﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Discipline

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Discipline_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Dates_Load", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dtTable As DataTable
        Dim ObjDFile As New clsDiscipline_File
        Try
            dtTable = ObjDFile.GetData_ForDiary(mintEmployeeId, "Diary")

            dgvData.AutoGenerateColumns = False

            dgcolhStep.DataPropertyName = "Column1"
            dgcolhActionTaken.DataPropertyName = "Column2"
            dgcolhAStartDate.DataPropertyName = "Column3"
            dgcolhAEndDate.DataPropertyName = "Column4"
            dgcolhStatus.DataPropertyName = "Column5"
            dgcolhAppSatatus.DataPropertyName = "Column6"
            dgcolhAppDate.DataPropertyName = "Column7"
            dgcolhShow.DataPropertyName = "Column11"

            objdgcolhIsGrp.DataPropertyName = "Column8"
            objdgcolhGrpId.DataPropertyName = "Column9"
            objdgcolhIncident.DataPropertyName = "Column10"

            dgvData.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value = "-"
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCheck.Index).Style = objdgvsCollapseHeader
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value Is "-" Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = "+"
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 8
                        If dgvData.Rows(e.RowIndex).Cells(dgcolhShow.Index).Value.ToString <> "" Then
                            rtbIncident.Text = dgvData.Rows(e.RowIndex).Cells(objdgcolhIncident.Index).Value.ToString
                            gbIncident.Visible = True
                            gbIncident.BringToFront()
                            dgvData.Enabled = False
                        End If
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lblClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblClose.Click
        Try
            rtbIncident.Text = ""
            gbIncident.Visible = False
            dgvData.Enabled = True
            dgvData.BringToFront()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lblClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbIncident.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbIncident.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elDiscipline.Text = Language._Object.getCaption(Me.elDiscipline.Name, Me.elDiscipline.Text)
			Me.gbIncident.Text = Language._Object.getCaption(Me.gbIncident.Name, Me.gbIncident.Text)
			Me.lblClose.Text = Language._Object.getCaption(Me.lblClose.Name, Me.lblClose.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.dgcolhStep.HeaderText = Language._Object.getCaption(Me.dgcolhStep.Name, Me.dgcolhStep.HeaderText)
			Me.dgcolhActionTaken.HeaderText = Language._Object.getCaption(Me.dgcolhActionTaken.Name, Me.dgcolhActionTaken.HeaderText)
			Me.dgcolhAStartDate.HeaderText = Language._Object.getCaption(Me.dgcolhAStartDate.Name, Me.dgcolhAStartDate.HeaderText)
			Me.dgcolhAEndDate.HeaderText = Language._Object.getCaption(Me.dgcolhAEndDate.Name, Me.dgcolhAEndDate.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
			Me.dgcolhAppSatatus.HeaderText = Language._Object.getCaption(Me.dgcolhAppSatatus.Name, Me.dgcolhAppSatatus.HeaderText)
			Me.dgcolhAppDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppDate.Name, Me.dgcolhAppDate.HeaderText)
			Me.dgcolhShow.HeaderText = Language._Object.getCaption(Me.dgcolhShow.Name, Me.dgcolhShow.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class