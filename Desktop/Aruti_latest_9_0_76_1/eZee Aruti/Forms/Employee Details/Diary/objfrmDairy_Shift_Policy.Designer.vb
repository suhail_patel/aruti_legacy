﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDairy_Shift_Policy
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.elShift = New eZee.Common.eZeeLine
        Me.elPolicy = New eZee.Common.eZeeLine
        Me.dgvAssignedShift = New System.Windows.Forms.DataGridView
        Me.dgcolhEffective_Date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShiftType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShiftName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvPolicy = New System.Windows.Forms.DataGridView
        Me.dgcolhPEffective_Date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPolicy = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvAssignedShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPolicy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'elShift
        '
        Me.elShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.elShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elShift.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elShift.Location = New System.Drawing.Point(3, 9)
        Me.elShift.Name = "elShift"
        Me.elShift.Size = New System.Drawing.Size(575, 17)
        Me.elShift.TabIndex = 311
        Me.elShift.Text = "Shift"
        Me.elShift.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'elPolicy
        '
        Me.elPolicy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.elPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPolicy.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPolicy.Location = New System.Drawing.Point(3, 210)
        Me.elPolicy.Name = "elPolicy"
        Me.elPolicy.Size = New System.Drawing.Size(575, 17)
        Me.elPolicy.TabIndex = 312
        Me.elPolicy.Text = "Policy"
        Me.elPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvAssignedShift
        '
        Me.dgvAssignedShift.AllowUserToAddRows = False
        Me.dgvAssignedShift.AllowUserToDeleteRows = False
        Me.dgvAssignedShift.AllowUserToResizeRows = False
        Me.dgvAssignedShift.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAssignedShift.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAssignedShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAssignedShift.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhEffective_Date, Me.dgcolhShiftType, Me.dgcolhShiftName})
        Me.dgvAssignedShift.Location = New System.Drawing.Point(12, 29)
        Me.dgvAssignedShift.Name = "dgvAssignedShift"
        Me.dgvAssignedShift.ReadOnly = True
        Me.dgvAssignedShift.RowHeadersVisible = False
        Me.dgvAssignedShift.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAssignedShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssignedShift.Size = New System.Drawing.Size(557, 178)
        Me.dgvAssignedShift.TabIndex = 313
        '
        'dgcolhEffective_Date
        '
        Me.dgcolhEffective_Date.HeaderText = "Effective Date"
        Me.dgcolhEffective_Date.Name = "dgcolhEffective_Date"
        Me.dgcolhEffective_Date.ReadOnly = True
        '
        'dgcolhShiftType
        '
        Me.dgcolhShiftType.HeaderText = "Shift Type"
        Me.dgcolhShiftType.Name = "dgcolhShiftType"
        Me.dgcolhShiftType.ReadOnly = True
        Me.dgcolhShiftType.Width = 120
        '
        'dgcolhShiftName
        '
        Me.dgcolhShiftName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhShiftName.HeaderText = "Shift"
        Me.dgcolhShiftName.Name = "dgcolhShiftName"
        Me.dgcolhShiftName.ReadOnly = True
        '
        'dgvPolicy
        '
        Me.dgvPolicy.AllowUserToAddRows = False
        Me.dgvPolicy.AllowUserToDeleteRows = False
        Me.dgvPolicy.AllowUserToResizeRows = False
        Me.dgvPolicy.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvPolicy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvPolicy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPolicy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhPEffective_Date, Me.dgcolhPolicy})
        Me.dgvPolicy.Location = New System.Drawing.Point(8, 230)
        Me.dgvPolicy.Name = "dgvPolicy"
        Me.dgvPolicy.ReadOnly = True
        Me.dgvPolicy.RowHeadersVisible = False
        Me.dgvPolicy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvPolicy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPolicy.Size = New System.Drawing.Size(557, 176)
        Me.dgvPolicy.TabIndex = 314
        '
        'dgcolhPEffective_Date
        '
        Me.dgcolhPEffective_Date.HeaderText = "Effective Date"
        Me.dgcolhPEffective_Date.Name = "dgcolhPEffective_Date"
        Me.dgcolhPEffective_Date.ReadOnly = True
        '
        'dgcolhPolicy
        '
        Me.dgcolhPolicy.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhPolicy.HeaderText = "Policy"
        Me.dgcolhPolicy.Name = "dgcolhPolicy"
        Me.dgcolhPolicy.ReadOnly = True
        '
        'objfrmDairy_Shift_Policy
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.dgvPolicy)
        Me.Controls.Add(Me.dgvAssignedShift)
        Me.Controls.Add(Me.elPolicy)
        Me.Controls.Add(Me.elShift)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "objfrmDairy_Shift_Policy"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.dgvAssignedShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPolicy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elShift As eZee.Common.eZeeLine
    Friend WithEvents elPolicy As eZee.Common.eZeeLine
    Friend WithEvents dgvAssignedShift As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhEffective_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShiftType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShiftName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPolicy As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhPEffective_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPolicy As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
