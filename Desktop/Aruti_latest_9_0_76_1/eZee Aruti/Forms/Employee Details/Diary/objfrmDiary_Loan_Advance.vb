﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Loan_Advance

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Loan_Advance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvLoanAdvance.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDairy_Loan_Advance_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsLoanAdvance As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim objLoan_Advance As New clsLoan_Advance
        Try

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsLoanAdvance = objLoan_Advance.GetList("Loan")
            dsLoanAdvance = objLoan_Advance.GetList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    "Loan")
            'Nilay (10-Oct-2015) -- End

            If mintEmployeeId > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(mintEmployeeId) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsLoanAdvance.Tables("Loan"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsLoanAdvance.Tables("Loan")
            End If

            lvLoanAdvance.Items.Clear()
            Dim lvItem As ListViewItem = Nothing
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("VocNo").ToString
                lvItem.SubItems.Add(dtRow.Item("empcode").ToString)
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.SubItems.Add(dtRow.Item("loancode").ToString)
                lvItem.SubItems.Add(dtRow.Item("LoanScheme").ToString)
                lvItem.SubItems.Add(dtRow.Item("Loan_Advance").ToString)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount").ToString)))
                lvItem.SubItems.Add(dtRow.Item("installments").ToString) 'Nilay (15-Dec-2015) -- [Intallments] Replaced by [installments]
                lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                Select Case CInt(dtRow.Item("loan_statusunkid"))
                    Case 1 'InProgress
                        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 96, "In Progress"))
                    Case 2 'OnHold
                        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 97, "On Hold"))
                    Case 3 'WrittenOff
                        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 98, "Written Off"))
                    Case 4 'Completed
                        lvItem.SubItems.Add(Language.getMessage("clsMasterData", 100, "Completed"))
                End Select
                lvItem.SubItems.Add(dtRow.Item("isloan").ToString)
                lvItem.SubItems.Add(dtRow.Item("loan_statusunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                lvItem.SubItems.Add(dtRow.Item("empcode").ToString)
                lvItem.SubItems.Add(dtRow.Item("loancode").ToString)
                If CBool(dtRow.Item("isbrought_forward")) = True Then lvItem.ForeColor = Color.Gray

                lvItem.Tag = dtRow.Item("loanadvancetranunkid")

                lvLoanAdvance.Items.Add(lvItem)
            Next

            lvLoanAdvance.GridLines = False
            lvLoanAdvance.GroupingColumn = colhLoan_Advance
            lvLoanAdvance.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elLoanAdvance.Text = Language._Object.getCaption(Me.elLoanAdvance.Name, Me.elLoanAdvance.Text)
			Me.colhVocNo.Text = Language._Object.getCaption(CStr(Me.colhVocNo.Tag), Me.colhVocNo.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhLoanCode.Text = Language._Object.getCaption(CStr(Me.colhLoanCode.Tag), Me.colhLoanCode.Text)
			Me.colhLoanScheme.Text = Language._Object.getCaption(CStr(Me.colhLoanScheme.Tag), Me.colhLoanScheme.Text)
			Me.colhLoan_Advance.Text = Language._Object.getCaption(CStr(Me.colhLoan_Advance.Tag), Me.colhLoan_Advance.Text)
			Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
			Me.colhInstallments.Text = Language._Object.getCaption(CStr(Me.colhInstallments.Tag), Me.colhInstallments.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.colhIsLoan.Text = Language._Object.getCaption(CStr(Me.colhIsLoan.Tag), Me.colhIsLoan.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 96, "In Progress")
			Language.setMessage("clsMasterData", 97, "On Hold")
			Language.setMessage("clsMasterData", 98, "Written Off")
			Language.setMessage("clsMasterData", 100, "Completed")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class