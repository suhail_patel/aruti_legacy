﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Banks

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Banks_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            lvEmpBankList.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objfrmDairy_Banks_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dtTable As New DataTable
        Dim dsList As New DataSet
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim lvItem As ListViewItem
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objEmpBankTran.GetList("EmployeeBank")
            dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", True, , , )
            'Sohail (21 Aug 2015) -- End
            dtTable = New DataView(dsList.Tables("EmployeeBank"), "employeeunkid = '" & mintEmployeeId & "'", "", DataViewRowState.CurrentRows).ToTable
            lvEmpBankList.Items.Clear()
            For Each dRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("BankGrp").ToString
                lvItem.SubItems.Add(dRow.Item("BranchName").ToString)
                lvItem.SubItems.Add(dRow.Item("AccName").ToString)
                lvItem.SubItems.Add(dRow.Item("accountno").ToString)
                lvItem.SubItems.Add(Format(CDec(dRow.Item("percentage")), "###.#0").ToString)

                lvEmpBankList.Items.Add(lvItem)
            Next
            lvEmpBankList.GroupingColumn = objcolhBankGroup
            lvEmpBankList.DisplayGroups(True)
            lvEmpBankList.GridLines = False
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmpBanks.Text = Language._Object.getCaption(Me.elEmpBanks.Name, Me.elEmpBanks.Text)
			Me.colhBankBranch.Text = Language._Object.getCaption(CStr(Me.colhBankBranch.Tag), Me.colhBankBranch.Text)
			Me.colhAccType.Text = Language._Object.getCaption(CStr(Me.colhAccType.Tag), Me.colhAccType.Text)
			Me.colhAccNo.Text = Language._Object.getCaption(CStr(Me.colhAccNo.Tag), Me.colhAccNo.Text)
			Me.colhPercentage.Text = Language._Object.getCaption(CStr(Me.colhPercentage.Tag), Me.colhPercentage.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class