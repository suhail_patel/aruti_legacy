﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmEmployee_Movement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(objfrmEmployee_Movement))
        Me.elTrininEnrolment = New eZee.Common.eZeeLine
        Me.mnuJobLog = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cpHeader = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lvEmployeeMovementLog = New eZee.Common.eZeeVirtualListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhBranch = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhUser = New System.Windows.Forms.ColumnHeader
        Me.colhUnit = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhGradeLevel = New System.Windows.Forms.ColumnHeader
        Me.colhScale = New System.Windows.Forms.ColumnHeader
        Me.colhClass = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.colhDeptGrp = New System.Windows.Forms.ColumnHeader
        Me.colhSectionGrp = New System.Windows.Forms.ColumnHeader
        Me.colhUnitGrp = New System.Windows.Forms.ColumnHeader
        Me.colhTeam = New System.Windows.Forms.ColumnHeader
        Me.colhGradeGrp = New System.Windows.Forms.ColumnHeader
        Me.colhJobGrp = New System.Windows.Forms.ColumnHeader
        Me.colhClassGrp = New System.Windows.Forms.ColumnHeader
        Me.colhAllocationReason = New System.Windows.Forms.ColumnHeader
        Me.colhAppointedDate = New System.Windows.Forms.ColumnHeader
        Me.colhConfirmationDate = New System.Windows.Forms.ColumnHeader
        Me.colhSuspensionFrom = New System.Windows.Forms.ColumnHeader
        Me.colhSuspensionTo = New System.Windows.Forms.ColumnHeader
        Me.colhProbationFrom = New System.Windows.Forms.ColumnHeader
        Me.colhProbationTo = New System.Windows.Forms.ColumnHeader
        Me.colhEOCDate = New System.Windows.Forms.ColumnHeader
        Me.colhLeavingDate = New System.Windows.Forms.ColumnHeader
        Me.colhRetirementDate = New System.Windows.Forms.ColumnHeader
        Me.colhReinstatementDate = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elTrininEnrolment
        '
        Me.elTrininEnrolment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elTrininEnrolment.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elTrininEnrolment.Location = New System.Drawing.Point(2, 6)
        Me.elTrininEnrolment.Name = "elTrininEnrolment"
        Me.elTrininEnrolment.Size = New System.Drawing.Size(575, 17)
        Me.elTrininEnrolment.TabIndex = 309
        Me.elTrininEnrolment.Text = "Employee Movement"
        Me.elTrininEnrolment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'mnuJobLog
        '
        Me.mnuJobLog.Name = "mnuJobLog"
        Me.mnuJobLog.Size = New System.Drawing.Size(61, 4)
        '
        'cpHeader
        '
        Me.cpHeader.Name = "mnuJobLog"
        Me.cpHeader.Size = New System.Drawing.Size(61, 4)
        '
        'lvEmployeeMovementLog
        '
        Me.lvEmployeeMovementLog.ColumnHeaders = Nothing
        Me.lvEmployeeMovementLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhDate, Me.colhBranch, Me.colhDepartment, Me.colhSection, Me.colhJob, Me.colhUser, Me.colhUnit, Me.colhGrade, Me.colhGradeLevel, Me.colhScale, Me.colhClass, Me.colhCostCenter, Me.colhDeptGrp, Me.colhSectionGrp, Me.colhUnitGrp, Me.colhTeam, Me.colhGradeGrp, Me.colhJobGrp, Me.colhClassGrp, Me.colhAllocationReason, Me.colhAppointedDate, Me.colhConfirmationDate, Me.colhSuspensionFrom, Me.colhSuspensionTo, Me.colhProbationFrom, Me.colhProbationTo, Me.colhEOCDate, Me.colhLeavingDate, Me.colhRetirementDate, Me.colhReinstatementDate})
        Me.lvEmployeeMovementLog.CompulsoryColumns = resources.GetString("lvEmployeeMovementLog.CompulsoryColumns")
        Me.lvEmployeeMovementLog.ContextMenuStrip = Me.mnuJobLog
        Me.lvEmployeeMovementLog.ContextMenuStripHeader = Me.cpHeader
        Me.lvEmployeeMovementLog.FullRowSelect = True
        Me.lvEmployeeMovementLog.GridLines = True
        Me.lvEmployeeMovementLog.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmployeeMovementLog.HideSelection = False
        Me.lvEmployeeMovementLog.Location = New System.Drawing.Point(5, 26)
        Me.lvEmployeeMovementLog.LVItems = Nothing
        Me.lvEmployeeMovementLog.MinColumnWidth = 50
        Me.lvEmployeeMovementLog.MultiSelect = False
        Me.lvEmployeeMovementLog.Name = "lvEmployeeMovementLog"
        Me.lvEmployeeMovementLog.OptionalColumns = ""
        Me.lvEmployeeMovementLog.OwnerDraw = True
        Me.lvEmployeeMovementLog.ShowMoreItem = False
        Me.lvEmployeeMovementLog.ShowSaveItem = False
        Me.lvEmployeeMovementLog.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeMovementLog.Size = New System.Drawing.Size(572, 380)
        Me.lvEmployeeMovementLog.TabIndex = 310
        Me.lvEmployeeMovementLog.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeMovementLog.View = System.Windows.Forms.View.Details
        Me.lvEmployeeMovementLog.VirtualMode = True
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 110
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 90
        '
        'colhBranch
        '
        Me.colhBranch.Tag = "colhBranch"
        Me.colhBranch.Text = "Branch"
        Me.colhBranch.Width = 93
        '
        'colhDepartment
        '
        Me.colhDepartment.DisplayIndex = 4
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 110
        '
        'colhSection
        '
        Me.colhSection.DisplayIndex = 6
        Me.colhSection.Tag = "colhSection"
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 110
        '
        'colhJob
        '
        Me.colhJob.DisplayIndex = 15
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 100
        '
        'colhUser
        '
        Me.colhUser.DisplayIndex = 19
        Me.colhUser.Tag = "colhUser"
        Me.colhUser.Text = "User"
        Me.colhUser.Width = 100
        '
        'colhUnit
        '
        Me.colhUnit.DisplayIndex = 8
        Me.colhUnit.Tag = "colhUnit"
        Me.colhUnit.Text = "Unit"
        Me.colhUnit.Width = 90
        '
        'colhGrade
        '
        Me.colhGrade.DisplayIndex = 11
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade"
        Me.colhGrade.Width = 100
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.DisplayIndex = 12
        Me.colhGradeLevel.Tag = "colhGradeLevel"
        Me.colhGradeLevel.Text = "Grade Level"
        Me.colhGradeLevel.Width = 100
        '
        'colhScale
        '
        Me.colhScale.DisplayIndex = 13
        Me.colhScale.Tag = "colhScale"
        Me.colhScale.Text = "Scale"
        Me.colhScale.Width = 100
        '
        'colhClass
        '
        Me.colhClass.DisplayIndex = 17
        Me.colhClass.Tag = "colhClass"
        Me.colhClass.Text = "Class"
        Me.colhClass.Width = 100
        '
        'colhCostCenter
        '
        Me.colhCostCenter.DisplayIndex = 18
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 100
        '
        'colhDeptGrp
        '
        Me.colhDeptGrp.DisplayIndex = 3
        Me.colhDeptGrp.Tag = "colhDeptGrp"
        Me.colhDeptGrp.Text = "Department Group"
        Me.colhDeptGrp.Width = 110
        '
        'colhSectionGrp
        '
        Me.colhSectionGrp.DisplayIndex = 5
        Me.colhSectionGrp.Tag = "colhSectionGrp"
        Me.colhSectionGrp.Text = "Section Group"
        Me.colhSectionGrp.Width = 110
        '
        'colhUnitGrp
        '
        Me.colhUnitGrp.DisplayIndex = 7
        Me.colhUnitGrp.Tag = "colhUnitGrp"
        Me.colhUnitGrp.Text = "Unit Group"
        Me.colhUnitGrp.Width = 110
        '
        'colhTeam
        '
        Me.colhTeam.DisplayIndex = 9
        Me.colhTeam.Tag = "colhTeam"
        Me.colhTeam.Text = "Team"
        Me.colhTeam.Width = 110
        '
        'colhGradeGrp
        '
        Me.colhGradeGrp.DisplayIndex = 10
        Me.colhGradeGrp.Tag = "colhGradeGrp"
        Me.colhGradeGrp.Text = "Grade Group"
        Me.colhGradeGrp.Width = 110
        '
        'colhJobGrp
        '
        Me.colhJobGrp.DisplayIndex = 14
        Me.colhJobGrp.Tag = "colhJobGrp"
        Me.colhJobGrp.Text = "Job Group"
        Me.colhJobGrp.Width = 110
        '
        'colhClassGrp
        '
        Me.colhClassGrp.DisplayIndex = 16
        Me.colhClassGrp.Tag = "colhClassGrp"
        Me.colhClassGrp.Text = "Class Group"
        Me.colhClassGrp.Width = 110
        '
        'colhAllocationReason
        '
        Me.colhAllocationReason.Tag = "colhAllocationReason"
        Me.colhAllocationReason.Text = "Allocation Reason"
        Me.colhAllocationReason.Width = 90
        '
        'colhAppointedDate
        '
        Me.colhAppointedDate.Tag = "colhAppointedDate"
        Me.colhAppointedDate.Text = "Appointed Date"
        Me.colhAppointedDate.Width = 80
        '
        'colhConfirmationDate
        '
        Me.colhConfirmationDate.Tag = "colhConfirmationDate"
        Me.colhConfirmationDate.Text = "Confirmation Date"
        Me.colhConfirmationDate.Width = 80
        '
        'colhSuspensionFrom
        '
        Me.colhSuspensionFrom.Tag = "colhSuspensionFrom"
        Me.colhSuspensionFrom.Text = "Suspension Date From"
        Me.colhSuspensionFrom.Width = 80
        '
        'colhSuspensionTo
        '
        Me.colhSuspensionTo.Tag = "colhSuspensionTo"
        Me.colhSuspensionTo.Text = "Suspension Date To"
        Me.colhSuspensionTo.Width = 80
        '
        'colhProbationFrom
        '
        Me.colhProbationFrom.Tag = "colhProbationFrom"
        Me.colhProbationFrom.Text = "Probation Date From"
        Me.colhProbationFrom.Width = 80
        '
        'colhProbationTo
        '
        Me.colhProbationTo.Tag = "colhProbationTo"
        Me.colhProbationTo.Text = "Probation Date To"
        Me.colhProbationTo.Width = 80
        '
        'colhEOCDate
        '
        Me.colhEOCDate.Tag = "colhEOCDate"
        Me.colhEOCDate.Text = "EOC Date"
        Me.colhEOCDate.Width = 80
        '
        'colhLeavingDate
        '
        Me.colhLeavingDate.Tag = "colhLeavingDate"
        Me.colhLeavingDate.Text = "Leaving Date"
        Me.colhLeavingDate.Width = 80
        '
        'colhRetirementDate
        '
        Me.colhRetirementDate.Tag = "colhRetirementDate"
        Me.colhRetirementDate.Text = "Retirement Date"
        Me.colhRetirementDate.Width = 80
        '
        'colhReinstatementDate
        '
        Me.colhReinstatementDate.Tag = "colhReinstatementDate"
        Me.colhReinstatementDate.Text = "Reinstatement Date"
        Me.colhReinstatementDate.Width = 80
        '
        'objfrmEmployee_Movement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvEmployeeMovementLog)
        Me.Controls.Add(Me.elTrininEnrolment)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmEmployee_Movement"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elTrininEnrolment As eZee.Common.eZeeLine
    Friend WithEvents mnuJobLog As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cpHeader As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lvEmployeeMovementLog As eZee.Common.eZeeVirtualListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDeptGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSectionGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnitGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTeam As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClassGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAllocationReason As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAppointedDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhConfirmationDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSuspensionFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSuspensionTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhProbationFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhProbationTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEOCDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeavingDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRetirementDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhReinstatementDate As System.Windows.Forms.ColumnHeader
End Class
