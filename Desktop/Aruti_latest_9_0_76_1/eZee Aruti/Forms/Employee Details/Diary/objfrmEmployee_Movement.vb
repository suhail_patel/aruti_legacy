﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmEmployee_Movement

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmEmployee_Movement_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvEmployeeMovementLog.CreateHeaderItems()
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmEmployee_Movement_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim intIndex As Integer = 0
        Try
            dsList = objEmployee.GetMovement_Log(mintEmployeeId, "Log")

            lvEmployeeMovementLog.LVItems = Nothing
            If dsList.Tables(0).Rows.Count > 0 Then
                lvEmployeeMovementLog.LVItems = New ListViewItem(dsList.Tables(0).Rows.Count - 1) {}
            End If

            Dim lvItem As ListViewItem

            lvEmployeeMovementLog.Items.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("Branch").ToString)
                lvItem.SubItems.Add(dtRow.Item("Dept").ToString)
                lvItem.SubItems.Add(dtRow.Item("Section").ToString)
                lvItem.SubItems.Add(dtRow.Item("Job").ToString)
                lvItem.SubItems.Add(dtRow.Item("UserName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Unit").ToString)
                lvItem.SubItems.Add(dtRow.Item("Grade").ToString)
                lvItem.SubItems.Add(dtRow.Item("GLevel").ToString)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Scale")), GUI.fmtCurrency).ToString)
                lvItem.SubItems.Add(dtRow.Item("Class").ToString)
                lvItem.SubItems.Add(dtRow.Item("CostCenter").ToString)

                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("DeptGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("SectionGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("UnitGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("Team").ToString)
                lvItem.SubItems.Add(dtRow.Item("GradeGrp").ToString)
                lvItem.SubItems.Add(dtRow.Item("JobGroup").ToString)
                lvItem.SubItems.Add(dtRow.Item("ClassGrp").ToString)
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow.Item("AllocationReason").ToString)
                'Anjan (02 Mar 2012)-End 

                'S.SANDEEP [ 17 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Appointed").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("ConfDt").ToString).ToShortDateString)

                If dtRow.Item("SFD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("SFD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("STD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("STD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("PFD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("PFD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("PTD").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("PTD").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("EOC").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EOC").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                If dtRow.Item("Leaving").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Leaving").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If

                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Retirement").ToString).ToShortDateString)

                If dtRow.Item("Reinstatement").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Reinstatement").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                'S.SANDEEP [ 17 DEC 2012 ] -- END


                lvEmployeeMovementLog.LVItems(intIndex) = lvItem
                intIndex += 1
                lvItem = Nothing
            Next

            If intIndex > 0 Then
                lvEmployeeMovementLog.VirtualListSize = intIndex
            Else
                lvEmployeeMovementLog.VirtualListSize = 0
            End If

            'Sohail (15 May 2018) -- Start
            'CCK Enhancement - Ref # 179 : On loan approval screen on MSS, not all approvers should see the basic salary of the staff. in 72.1.
            If User._Object.Privilege._AllowTo_View_Scale = False Then
                lvEmployeeMovementLog.Columns(colhScale.Index).Width = 0
            End If
            'Sohail (15 May 2018) -- End

            lvEmployeeMovementLog.Invalidate()
            lvEmployeeMovementLog.Reset()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elTrininEnrolment.Text = Language._Object.getCaption(Me.elTrininEnrolment.Name, Me.elTrininEnrolment.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhBranch.Text = Language._Object.getCaption(CStr(Me.colhBranch.Tag), Me.colhBranch.Text)
			Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
			Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
			Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
			Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
			Me.colhUnit.Text = Language._Object.getCaption(CStr(Me.colhUnit.Tag), Me.colhUnit.Text)
			Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
			Me.colhGradeLevel.Text = Language._Object.getCaption(CStr(Me.colhGradeLevel.Tag), Me.colhGradeLevel.Text)
			Me.colhScale.Text = Language._Object.getCaption(CStr(Me.colhScale.Tag), Me.colhScale.Text)
			Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
			Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
			Me.colhDeptGrp.Text = Language._Object.getCaption(CStr(Me.colhDeptGrp.Tag), Me.colhDeptGrp.Text)
			Me.colhSectionGrp.Text = Language._Object.getCaption(CStr(Me.colhSectionGrp.Tag), Me.colhSectionGrp.Text)
			Me.colhUnitGrp.Text = Language._Object.getCaption(CStr(Me.colhUnitGrp.Tag), Me.colhUnitGrp.Text)
			Me.colhTeam.Text = Language._Object.getCaption(CStr(Me.colhTeam.Tag), Me.colhTeam.Text)
			Me.colhGradeGrp.Text = Language._Object.getCaption(CStr(Me.colhGradeGrp.Tag), Me.colhGradeGrp.Text)
			Me.colhJobGrp.Text = Language._Object.getCaption(CStr(Me.colhJobGrp.Tag), Me.colhJobGrp.Text)
			Me.colhClassGrp.Text = Language._Object.getCaption(CStr(Me.colhClassGrp.Tag), Me.colhClassGrp.Text)
			Me.colhAllocationReason.Text = Language._Object.getCaption(CStr(Me.colhAllocationReason.Tag), Me.colhAllocationReason.Text)
			Me.colhAppointedDate.Text = Language._Object.getCaption(CStr(Me.colhAppointedDate.Tag), Me.colhAppointedDate.Text)
			Me.colhConfirmationDate.Text = Language._Object.getCaption(CStr(Me.colhConfirmationDate.Tag), Me.colhConfirmationDate.Text)
			Me.colhSuspensionFrom.Text = Language._Object.getCaption(CStr(Me.colhSuspensionFrom.Tag), Me.colhSuspensionFrom.Text)
			Me.colhSuspensionTo.Text = Language._Object.getCaption(CStr(Me.colhSuspensionTo.Tag), Me.colhSuspensionTo.Text)
			Me.colhProbationFrom.Text = Language._Object.getCaption(CStr(Me.colhProbationFrom.Tag), Me.colhProbationFrom.Text)
			Me.colhProbationTo.Text = Language._Object.getCaption(CStr(Me.colhProbationTo.Tag), Me.colhProbationTo.Text)
			Me.colhEOCDate.Text = Language._Object.getCaption(CStr(Me.colhEOCDate.Tag), Me.colhEOCDate.Text)
			Me.colhLeavingDate.Text = Language._Object.getCaption(CStr(Me.colhLeavingDate.Tag), Me.colhLeavingDate.Text)
			Me.colhRetirementDate.Text = Language._Object.getCaption(CStr(Me.colhRetirementDate.Tag), Me.colhRetirementDate.Text)
			Me.colhReinstatementDate.Text = Language._Object.getCaption(CStr(Me.colhReinstatementDate.Tag), Me.colhReinstatementDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class