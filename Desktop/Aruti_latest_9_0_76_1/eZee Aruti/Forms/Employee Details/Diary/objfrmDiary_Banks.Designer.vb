﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Banks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmpBanks = New eZee.Common.eZeeLine
        Me.lvEmpBankList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhBankGroup = New System.Windows.Forms.ColumnHeader
        Me.colhBankBranch = New System.Windows.Forms.ColumnHeader
        Me.colhAccType = New System.Windows.Forms.ColumnHeader
        Me.colhAccNo = New System.Windows.Forms.ColumnHeader
        Me.colhPercentage = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elEmpBanks
        '
        Me.elEmpBanks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmpBanks.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmpBanks.Location = New System.Drawing.Point(3, 9)
        Me.elEmpBanks.Name = "elEmpBanks"
        Me.elEmpBanks.Size = New System.Drawing.Size(575, 17)
        Me.elEmpBanks.TabIndex = 321
        Me.elEmpBanks.Text = "Employee Banks"
        Me.elEmpBanks.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvEmpBankList
        '
        Me.lvEmpBankList.BackColorOnChecked = False
        Me.lvEmpBankList.ColumnHeaders = Nothing
        Me.lvEmpBankList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhBankGroup, Me.colhBankBranch, Me.colhAccType, Me.colhAccNo, Me.colhPercentage})
        Me.lvEmpBankList.CompulsoryColumns = ""
        Me.lvEmpBankList.FullRowSelect = True
        Me.lvEmpBankList.GridLines = True
        Me.lvEmpBankList.GroupingColumn = Nothing
        Me.lvEmpBankList.HideSelection = False
        Me.lvEmpBankList.Location = New System.Drawing.Point(6, 29)
        Me.lvEmpBankList.MinColumnWidth = 50
        Me.lvEmpBankList.MultiSelect = False
        Me.lvEmpBankList.Name = "lvEmpBankList"
        Me.lvEmpBankList.OptionalColumns = ""
        Me.lvEmpBankList.ShowMoreItem = False
        Me.lvEmpBankList.ShowSaveItem = False
        Me.lvEmpBankList.ShowSelectAll = True
        Me.lvEmpBankList.ShowSizeAllColumnsToFit = True
        Me.lvEmpBankList.Size = New System.Drawing.Size(572, 374)
        Me.lvEmpBankList.Sortable = False
        Me.lvEmpBankList.TabIndex = 322
        Me.lvEmpBankList.UseCompatibleStateImageBehavior = False
        Me.lvEmpBankList.View = System.Windows.Forms.View.Details
        '
        'objcolhBankGroup
        '
        Me.objcolhBankGroup.Tag = "objcolhBankGroup"
        Me.objcolhBankGroup.Text = "Bank Group"
        Me.objcolhBankGroup.Width = 0
        '
        'colhBankBranch
        '
        Me.colhBankBranch.Tag = "colhBankBranch"
        Me.colhBankBranch.Text = "Bank Branch"
        Me.colhBankBranch.Width = 190
        '
        'colhAccType
        '
        Me.colhAccType.Tag = "colhAccType"
        Me.colhAccType.Text = "Acc Type"
        Me.colhAccType.Width = 155
        '
        'colhAccNo
        '
        Me.colhAccNo.Tag = "colhAccNo"
        Me.colhAccNo.Text = "Acc No."
        Me.colhAccNo.Width = 162
        '
        'colhPercentage
        '
        Me.colhPercentage.Tag = "colhPercentage"
        Me.colhPercentage.Text = "(%)"
        Me.colhPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objfrmDiary_Banks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvEmpBankList)
        Me.Controls.Add(Me.elEmpBanks)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "objfrmDiary_Banks"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elEmpBanks As eZee.Common.eZeeLine
    Friend WithEvents lvEmpBankList As eZee.Common.eZeeListView
    Friend WithEvents objcolhBankGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBankBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPercentage As System.Windows.Forms.ColumnHeader
End Class
