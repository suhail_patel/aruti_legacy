﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeReferencesList

#Region " Private Varaibles "
    Private objReferences As clsEmployee_Ref_Tran
    Private ReadOnly mstrModuleName As String = "frmEmployeeReferencesList"
#End Region

#Region " Private Functions "
    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objCommon.getComboList(CType(CInt(30), clsCommon_Master.enCommonMaster), True, "Ref")
            With cboReference
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Ref")
            End With
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, )
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            dsList = objReferences.GetList("RefList")

            If txtReferenceNo.Text.Trim <> "" Then
                StrSearching &= "AND RefNo LIKE '%" & txtReferenceNo.Text.Trim & "%'" & " "
            End If

            If CInt(cboReference.SelectedValue) > 0 Then
                StrSearching &= "AND RefId = " & CInt(cboReference.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("RefList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("RefList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            End If

            lvReferencesList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("RefName").ToString
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.SubItems.Add(dtRow.Item("RefNo").ToString)
                lvItem.SubItems.Add(dtRow.Item("Remark").ToString)

                lvItem.Tag = dtRow.Item("RefTranId")

                lvReferencesList.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvReferencesList.Items.Count > 5 Then
                colhRemark.Width = 335 - 20
            Else
                colhRemark.Width = 335
            End If

            colhReference.Width += colhEmployee.Width
            colhEmployee.Width = 0
            lvReferencesList.GroupingColumn = colhEmployee
            lvReferencesList.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeReferences
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeReferences
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeReferences

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmAssetsRegisterList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objReferences = New clsEmployee_Ref_Tran
        Try


            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()
            Call SetVisibility()

            Call FillCombo()

            'Call FillList()


            If lvReferencesList.Items.Count > 0 Then lvReferencesList.Items(0).Selected = True
            lvReferencesList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssetsRegisterList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvReferencesList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmAssetsRegisterList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmAssetsRegisterList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objReferences = Nothing
    End Sub
#End Region

#Region " Buton's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvReferencesList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvReferencesList.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvReferencesList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                objReferences.Delete(CInt(lvReferencesList.SelectedItems(0).Tag), True, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), 1, "Testing")
                lvReferencesList.SelectedItems(0).Remove()

                If lvReferencesList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvReferencesList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvReferencesList.Items.Count - 1
                    lvReferencesList.Items(intSelectedIndex).Selected = True
                    lvReferencesList.EnsureVisible(intSelectedIndex)
                ElseIf lvReferencesList.Items.Count <> 0 Then
                    lvReferencesList.Items(intSelectedIndex).Selected = True
                    lvReferencesList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvReferencesList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvReferencesList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvReferencesList.Select()
            Exit Sub
        End If
        Dim frm As New frmEmployeeReferences
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvReferencesList.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(CInt(lvReferencesList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvReferencesList.Items(intSelectedIndex).Selected = True
            lvReferencesList.EnsureVisible(intSelectedIndex)
            lvReferencesList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeReferences
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboReference.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            txtReferenceNo.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


          
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblReferencesNo.Text = Language._Object.getCaption(Me.lblReferencesNo.Name, Me.lblReferencesNo.Text)
            Me.lblReference.Text = Language._Object.getCaption(Me.lblReference.Name, Me.lblReference.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhReference.Text = Language._Object.getCaption(CStr(Me.colhReference.Tag), Me.colhReference.Text)
            Me.colhReferenceNo.Text = Language._Object.getCaption(CStr(Me.colhReferenceNo.Tag), Me.colhReferenceNo.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class