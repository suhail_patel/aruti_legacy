﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveApprover_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveApprover_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApproversInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkExternalApprover = New System.Windows.Forms.CheckBox
        Me.lnkMapLeaveType = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.LblUser = New System.Windows.Forms.Label
        Me.lblApproverName = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddLevel = New eZee.Common.eZeeGradientButton
        Me.gbSelectedEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlSelectedEmployee = New System.Windows.Forms.Panel
        Me.chkAllSelectedEmployee = New System.Windows.Forms.CheckBox
        Me.lvSelectedEmployee = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhApprover = New System.Windows.Forms.ColumnHeader
        Me.colhDept = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhGUID = New System.Windows.Forms.ColumnHeader
        Me.colhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.cboFilterJob = New System.Windows.Forms.ComboBox
        Me.lblFilterjob = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.cboFilterSection = New System.Windows.Forms.ComboBox
        Me.lblFilterSection = New System.Windows.Forms.Label
        Me.cboeFilterDept = New System.Windows.Forms.ComboBox
        Me.lblFilterDepartment = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSelectedEmployee = New eZee.Common.eZeeGradientButton
        Me.txtSearchedEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.chkEmployeeAll = New System.Windows.Forms.CheckBox
        Me.lvEmployee = New System.Windows.Forms.ListView
        Me.ColhEmployeecode = New System.Windows.Forms.ColumnHeader
        Me.ColhEmp = New System.Windows.Forms.ColumnHeader
        Me.objcolhDepartment = New System.Windows.Forms.ColumnHeader
        Me.objColhSection = New System.Windows.Forms.ColumnHeader
        Me.objColhJob = New System.Windows.Forms.ColumnHeader
        Me.objcolhDepartmentID = New System.Windows.Forms.ColumnHeader
        Me.objColhSectionID = New System.Windows.Forms.ColumnHeader
        Me.objColhJobID = New System.Windows.Forms.ColumnHeader
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboApproveLevel = New System.Windows.Forms.ComboBox
        Me.lblApprovalTo = New System.Windows.Forms.Label
        Me.cboApprovalTo = New System.Windows.Forms.ComboBox
        Me.lblApproveLevel = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApproversInfo.SuspendLayout()
        Me.gbSelectedEmployee.SuspendLayout()
        Me.pnlSelectedEmployee.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApproversInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.lblJob)
        Me.pnlMainInfo.Controls.Add(Me.cboDepartment)
        Me.pnlMainInfo.Controls.Add(Me.cboJob)
        Me.pnlMainInfo.Controls.Add(Me.lblDepartment)
        Me.pnlMainInfo.Controls.Add(Me.lblSection)
        Me.pnlMainInfo.Controls.Add(Me.cboSection)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(923, 533)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbApproversInfo
        '
        Me.gbApproversInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproversInfo.Checked = False
        Me.gbApproversInfo.CollapseAllExceptThis = False
        Me.gbApproversInfo.CollapsedHoverImage = Nothing
        Me.gbApproversInfo.CollapsedNormalImage = Nothing
        Me.gbApproversInfo.CollapsedPressedImage = Nothing
        Me.gbApproversInfo.CollapseOnLoad = False
        Me.gbApproversInfo.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.gbApproversInfo.Controls.Add(Me.chkExternalApprover)
        Me.gbApproversInfo.Controls.Add(Me.lnkMapLeaveType)
        Me.gbApproversInfo.Controls.Add(Me.objbtnSearchUser)
        Me.gbApproversInfo.Controls.Add(Me.cboUser)
        Me.gbApproversInfo.Controls.Add(Me.LblUser)
        Me.gbApproversInfo.Controls.Add(Me.lblApproverName)
        Me.gbApproversInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbApproversInfo.Controls.Add(Me.txtName)
        Me.gbApproversInfo.Controls.Add(Me.objbtnAddLevel)
        Me.gbApproversInfo.Controls.Add(Me.gbSelectedEmployee)
        Me.gbApproversInfo.Controls.Add(Me.gbEmployee)
        Me.gbApproversInfo.Controls.Add(Me.cboApproveLevel)
        Me.gbApproversInfo.Controls.Add(Me.lblApprovalTo)
        Me.gbApproversInfo.Controls.Add(Me.cboApprovalTo)
        Me.gbApproversInfo.Controls.Add(Me.lblApproveLevel)
        Me.gbApproversInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproversInfo.ExpandedHoverImage = Nothing
        Me.gbApproversInfo.ExpandedNormalImage = Nothing
        Me.gbApproversInfo.ExpandedPressedImage = Nothing
        Me.gbApproversInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproversInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproversInfo.HeaderHeight = 25
        Me.gbApproversInfo.HeaderMessage = ""
        Me.gbApproversInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproversInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproversInfo.HeightOnCollapse = 0
        Me.gbApproversInfo.LeftTextSpace = 0
        Me.gbApproversInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbApproversInfo.Name = "gbApproversInfo"
        Me.gbApproversInfo.OpenHeight = 300
        Me.gbApproversInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproversInfo.ShowBorder = True
        Me.gbApproversInfo.ShowCheckBox = False
        Me.gbApproversInfo.ShowCollapseButton = False
        Me.gbApproversInfo.ShowDefaultBorderColor = True
        Me.gbApproversInfo.ShowDownButton = False
        Me.gbApproversInfo.ShowHeader = True
        Me.gbApproversInfo.Size = New System.Drawing.Size(923, 478)
        Me.gbApproversInfo.TabIndex = 0
        Me.gbApproversInfo.Temp = 0
        Me.gbApproversInfo.Text = "Approvers Info"
        Me.gbApproversInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExternalApprover
        '
        Me.chkExternalApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExternalApprover.Location = New System.Drawing.Point(65, 36)
        Me.chkExternalApprover.Name = "chkExternalApprover"
        Me.chkExternalApprover.Size = New System.Drawing.Size(247, 17)
        Me.chkExternalApprover.TabIndex = 302
        Me.chkExternalApprover.Text = "Make External Approver"
        Me.chkExternalApprover.UseVisualStyleBackColor = True
        '
        'lnkMapLeaveType
        '
        Me.lnkMapLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.lnkMapLeaveType.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkMapLeaveType.Location = New System.Drawing.Point(234, 141)
        Me.lnkMapLeaveType.Name = "lnkMapLeaveType"
        Me.lnkMapLeaveType.Size = New System.Drawing.Size(111, 15)
        Me.lnkMapLeaveType.TabIndex = 241
        Me.lnkMapLeaveType.TabStop = True
        Me.lnkMapLeaveType.Text = "Map Leave Type"
        Me.lnkMapLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(207, 138)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 299
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 200
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(65, 138)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(136, 21)
        Me.cboUser.TabIndex = 218
        '
        'LblUser
        '
        Me.LblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUser.Location = New System.Drawing.Point(8, 141)
        Me.LblUser.Name = "LblUser"
        Me.LblUser.Size = New System.Drawing.Size(51, 15)
        Me.LblUser.TabIndex = 219
        Me.LblUser.Text = "User"
        '
        'lblApproverName
        '
        Me.lblApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverName.Location = New System.Drawing.Point(8, 87)
        Me.lblApproverName.Name = "lblApproverName"
        Me.lblApproverName.Size = New System.Drawing.Size(51, 15)
        Me.lblApproverName.TabIndex = 2
        Me.lblApproverName.Text = "Name"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(317, 84)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 93
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(65, 84)
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(247, 21)
        Me.txtName.TabIndex = 1
        '
        'objbtnAddLevel
        '
        Me.objbtnAddLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLevel.BorderSelected = False
        Me.objbtnAddLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLevel.Location = New System.Drawing.Point(207, 111)
        Me.objbtnAddLevel.Name = "objbtnAddLevel"
        Me.objbtnAddLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLevel.TabIndex = 216
        '
        'gbSelectedEmployee
        '
        Me.gbSelectedEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.Checked = True
        Me.gbSelectedEmployee.CollapseAllExceptThis = False
        Me.gbSelectedEmployee.CollapsedHoverImage = Nothing
        Me.gbSelectedEmployee.CollapsedNormalImage = Nothing
        Me.gbSelectedEmployee.CollapsedPressedImage = Nothing
        Me.gbSelectedEmployee.CollapseOnLoad = False
        Me.gbSelectedEmployee.Controls.Add(Me.pnlSelectedEmployee)
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterJob)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterjob)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine3)
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.cboeFilterDept)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterDepartment)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnReset)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnSearch)
        Me.gbSelectedEmployee.Controls.Add(Me.btnDelete)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine2)
        Me.gbSelectedEmployee.ExpandedHoverImage = Nothing
        Me.gbSelectedEmployee.ExpandedNormalImage = Nothing
        Me.gbSelectedEmployee.ExpandedPressedImage = Nothing
        Me.gbSelectedEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSelectedEmployee.HeaderHeight = 25
        Me.gbSelectedEmployee.HeaderMessage = ""
        Me.gbSelectedEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSelectedEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.HeightOnCollapse = 0
        Me.gbSelectedEmployee.LeftTextSpace = 0
        Me.gbSelectedEmployee.Location = New System.Drawing.Point(352, 33)
        Me.gbSelectedEmployee.Name = "gbSelectedEmployee"
        Me.gbSelectedEmployee.OpenHeight = 300
        Me.gbSelectedEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSelectedEmployee.ShowBorder = True
        Me.gbSelectedEmployee.ShowCheckBox = False
        Me.gbSelectedEmployee.ShowCollapseButton = False
        Me.gbSelectedEmployee.ShowDefaultBorderColor = True
        Me.gbSelectedEmployee.ShowDownButton = False
        Me.gbSelectedEmployee.ShowHeader = True
        Me.gbSelectedEmployee.Size = New System.Drawing.Size(561, 439)
        Me.gbSelectedEmployee.TabIndex = 1
        Me.gbSelectedEmployee.Temp = 0
        Me.gbSelectedEmployee.Text = "Selected Employee"
        Me.gbSelectedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSelectedEmployee
        '
        Me.pnlSelectedEmployee.AutoSize = True
        Me.pnlSelectedEmployee.Controls.Add(Me.chkAllSelectedEmployee)
        Me.pnlSelectedEmployee.Controls.Add(Me.lvSelectedEmployee)
        Me.pnlSelectedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSelectedEmployee.Location = New System.Drawing.Point(2, 26)
        Me.pnlSelectedEmployee.Name = "pnlSelectedEmployee"
        Me.pnlSelectedEmployee.Size = New System.Drawing.Size(556, 356)
        Me.pnlSelectedEmployee.TabIndex = 2
        '
        'chkAllSelectedEmployee
        '
        Me.chkAllSelectedEmployee.Location = New System.Drawing.Point(8, 5)
        Me.chkAllSelectedEmployee.Name = "chkAllSelectedEmployee"
        Me.chkAllSelectedEmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkAllSelectedEmployee.TabIndex = 17
        Me.chkAllSelectedEmployee.UseVisualStyleBackColor = True
        '
        'lvSelectedEmployee
        '
        Me.lvSelectedEmployee.BackColorOnChecked = True
        Me.lvSelectedEmployee.CheckBoxes = True
        Me.lvSelectedEmployee.ColumnHeaders = Nothing
        Me.lvSelectedEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhApprover, Me.colhDept, Me.colhSection, Me.colhJob, Me.colhEmployee, Me.colhGUID, Me.colhemployeeunkid})
        Me.lvSelectedEmployee.CompulsoryColumns = ""
        Me.lvSelectedEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvSelectedEmployee.FullRowSelect = True
        Me.lvSelectedEmployee.GridLines = True
        Me.lvSelectedEmployee.GroupingColumn = Nothing
        Me.lvSelectedEmployee.HideSelection = False
        Me.lvSelectedEmployee.Location = New System.Drawing.Point(0, 0)
        Me.lvSelectedEmployee.MinColumnWidth = 50
        Me.lvSelectedEmployee.MultiSelect = False
        Me.lvSelectedEmployee.Name = "lvSelectedEmployee"
        Me.lvSelectedEmployee.OptionalColumns = ""
        Me.lvSelectedEmployee.ShowMoreItem = False
        Me.lvSelectedEmployee.ShowSaveItem = False
        Me.lvSelectedEmployee.ShowSelectAll = True
        Me.lvSelectedEmployee.ShowSizeAllColumnsToFit = True
        Me.lvSelectedEmployee.Size = New System.Drawing.Size(556, 356)
        Me.lvSelectedEmployee.Sortable = True
        Me.lvSelectedEmployee.TabIndex = 0
        Me.lvSelectedEmployee.UseCompatibleStateImageBehavior = False
        Me.lvSelectedEmployee.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Tag = "colhCheck"
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 25
        '
        'colhApprover
        '
        Me.colhApprover.Text = "Approver"
        Me.colhApprover.Width = 0
        '
        'colhDept
        '
        Me.colhDept.Text = "Department"
        Me.colhDept.Width = 95
        '
        'colhSection
        '
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 80
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 185
        '
        'colhEmployee
        '
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 160
        '
        'colhGUID
        '
        Me.colhGUID.Tag = "colhGUID"
        Me.colhGUID.Text = "GUID"
        Me.colhGUID.Width = 0
        '
        'colhemployeeunkid
        '
        Me.colhemployeeunkid.Tag = "colhemployeeunkid"
        Me.colhemployeeunkid.Text = "Employeeunkid"
        Me.colhemployeeunkid.Width = 0
        '
        'cboFilterJob
        '
        Me.cboFilterJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterJob.DropDownWidth = 300
        Me.cboFilterJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterJob.FormattingEnabled = True
        Me.cboFilterJob.Location = New System.Drawing.Point(432, 33)
        Me.cboFilterJob.Name = "cboFilterJob"
        Me.cboFilterJob.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterJob.TabIndex = 108
        Me.cboFilterJob.Visible = False
        '
        'lblFilterjob
        '
        Me.lblFilterjob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterjob.Location = New System.Drawing.Point(381, 36)
        Me.lblFilterjob.Name = "lblFilterjob"
        Me.lblFilterjob.Size = New System.Drawing.Size(48, 15)
        Me.lblFilterjob.TabIndex = 107
        Me.lblFilterjob.Text = "Job"
        Me.lblFilterjob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterjob.Visible = False
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(11, 58)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(542, 11)
        Me.objelLine3.TabIndex = 105
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterSection
        '
        Me.cboFilterSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterSection.DropDownWidth = 300
        Me.cboFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterSection.FormattingEnabled = True
        Me.cboFilterSection.Location = New System.Drawing.Point(266, 33)
        Me.cboFilterSection.Name = "cboFilterSection"
        Me.cboFilterSection.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterSection.TabIndex = 104
        Me.cboFilterSection.Visible = False
        '
        'lblFilterSection
        '
        Me.lblFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterSection.Location = New System.Drawing.Point(202, 36)
        Me.lblFilterSection.Name = "lblFilterSection"
        Me.lblFilterSection.Size = New System.Drawing.Size(58, 15)
        Me.lblFilterSection.TabIndex = 103
        Me.lblFilterSection.Text = "Section"
        Me.lblFilterSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterSection.Visible = False
        '
        'cboeFilterDept
        '
        Me.cboeFilterDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboeFilterDept.DropDownWidth = 300
        Me.cboeFilterDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboeFilterDept.FormattingEnabled = True
        Me.cboeFilterDept.Location = New System.Drawing.Point(89, 33)
        Me.cboeFilterDept.Name = "cboeFilterDept"
        Me.cboeFilterDept.Size = New System.Drawing.Size(104, 21)
        Me.cboeFilterDept.TabIndex = 102
        Me.cboeFilterDept.Visible = False
        '
        'lblFilterDepartment
        '
        Me.lblFilterDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblFilterDepartment.Name = "lblFilterDepartment"
        Me.lblFilterDepartment.Size = New System.Drawing.Size(75, 15)
        Me.lblFilterDepartment.TabIndex = 101
        Me.lblFilterDepartment.Text = "Department"
        Me.lblFilterDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterDepartment.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(534, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 71
        Me.objbtnReset.TabStop = False
        Me.objbtnReset.Visible = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(510, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        Me.objbtnSearch.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(464, 400)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(89, 29)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(6, 391)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(547, 6)
        Me.objelLine2.TabIndex = 11
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.lnkAllocation)
        Me.gbEmployee.Controls.Add(Me.objbtnSelectedEmployee)
        Me.gbEmployee.Controls.Add(Me.txtSearchedEmployee)
        Me.gbEmployee.Controls.Add(Me.objAlloacationReset)
        Me.gbEmployee.Controls.Add(Me.chkEmployeeAll)
        Me.gbEmployee.Controls.Add(Me.lvEmployee)
        Me.gbEmployee.Controls.Add(Me.objelLine1)
        Me.gbEmployee.Controls.Add(Me.btnAdd)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(11, 169)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(335, 301)
        Me.gbEmployee.TabIndex = 0
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(224, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 15)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnSelectedEmployee
        '
        Me.objbtnSelectedEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSelectedEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSelectedEmployee.BorderSelected = False
        Me.objbtnSelectedEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSelectedEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSelectedEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSelectedEmployee.Location = New System.Drawing.Point(210, 267)
        Me.objbtnSelectedEmployee.Name = "objbtnSelectedEmployee"
        Me.objbtnSelectedEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSelectedEmployee.TabIndex = 239
        '
        'txtSearchedEmployee
        '
        Me.txtSearchedEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchedEmployee.Flags = 0
        Me.txtSearchedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchedEmployee.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchedEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchedEmployee.Location = New System.Drawing.Point(8, 267)
        Me.txtSearchedEmployee.Name = "txtSearchedEmployee"
        Me.txtSearchedEmployee.ReadOnly = True
        Me.txtSearchedEmployee.Size = New System.Drawing.Size(198, 21)
        Me.txtSearchedEmployee.TabIndex = 218
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(311, 2)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 237
        '
        'chkEmployeeAll
        '
        Me.chkEmployeeAll.AutoSize = True
        Me.chkEmployeeAll.BackColor = System.Drawing.Color.Transparent
        Me.chkEmployeeAll.Location = New System.Drawing.Point(16, 5)
        Me.chkEmployeeAll.Name = "chkEmployeeAll"
        Me.chkEmployeeAll.Size = New System.Drawing.Size(78, 17)
        Me.chkEmployeeAll.TabIndex = 16
        Me.chkEmployeeAll.Text = "Select All"
        Me.chkEmployeeAll.UseVisualStyleBackColor = False
        '
        'lvEmployee
        '
        Me.lvEmployee.CheckBoxes = True
        Me.lvEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColhEmployeecode, Me.ColhEmp, Me.objcolhDepartment, Me.objColhSection, Me.objColhJob, Me.objcolhDepartmentID, Me.objColhSectionID, Me.objColhJobID})
        Me.lvEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmployee.FullRowSelect = True
        Me.lvEmployee.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmployee.HideSelection = False
        Me.lvEmployee.Location = New System.Drawing.Point(2, 26)
        Me.lvEmployee.Name = "lvEmployee"
        Me.lvEmployee.Size = New System.Drawing.Size(331, 219)
        Me.lvEmployee.TabIndex = 12
        Me.lvEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEmployee.View = System.Windows.Forms.View.Details
        '
        'ColhEmployeecode
        '
        Me.ColhEmployeecode.Tag = "ColhEmployeecode"
        Me.ColhEmployeecode.Text = "Employee Code"
        Me.ColhEmployeecode.Width = 100
        '
        'ColhEmp
        '
        Me.ColhEmp.Tag = "ColhEmp"
        Me.ColhEmp.Text = "Employee"
        Me.ColhEmp.Width = 210
        '
        'objcolhDepartment
        '
        Me.objcolhDepartment.Tag = "objcolhDepartment"
        Me.objcolhDepartment.Text = "Department"
        Me.objcolhDepartment.Width = 0
        '
        'objColhSection
        '
        Me.objColhSection.Tag = "objColhSection"
        Me.objColhSection.Text = "Section"
        Me.objColhSection.Width = 0
        '
        'objColhJob
        '
        Me.objColhJob.Tag = "objColhJob"
        Me.objColhJob.Text = "Job"
        Me.objColhJob.Width = 0
        '
        'objcolhDepartmentID
        '
        Me.objcolhDepartmentID.Tag = "objcolhDepartmentID"
        Me.objcolhDepartmentID.Text = "DepartmentID"
        Me.objcolhDepartmentID.Width = 0
        '
        'objColhSectionID
        '
        Me.objColhSectionID.Tag = "objColhSectionID"
        Me.objColhSectionID.Text = "SectionID"
        Me.objColhSectionID.Width = 0
        '
        'objColhJobID
        '
        Me.objColhJobID.Tag = "objColhJobID"
        Me.objColhJobID.Text = "JobID"
        Me.objColhJobID.Width = 0
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(8, 254)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(315, 6)
        Me.objelLine1.TabIndex = 10
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(237, 263)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(89, 29)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'cboApproveLevel
        '
        Me.cboApproveLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproveLevel.DropDownWidth = 200
        Me.cboApproveLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproveLevel.FormattingEnabled = True
        Me.cboApproveLevel.Location = New System.Drawing.Point(65, 111)
        Me.cboApproveLevel.Name = "cboApproveLevel"
        Me.cboApproveLevel.Size = New System.Drawing.Size(136, 21)
        Me.cboApproveLevel.TabIndex = 2
        '
        'lblApprovalTo
        '
        Me.lblApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalTo.Location = New System.Drawing.Point(8, 206)
        Me.lblApprovalTo.Name = "lblApprovalTo"
        Me.lblApprovalTo.Size = New System.Drawing.Size(67, 15)
        Me.lblApprovalTo.TabIndex = 98
        Me.lblApprovalTo.Text = "Approval To"
        Me.lblApprovalTo.Visible = False
        '
        'cboApprovalTo
        '
        Me.cboApprovalTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovalTo.FormattingEnabled = True
        Me.cboApprovalTo.Location = New System.Drawing.Point(81, 203)
        Me.cboApprovalTo.Name = "cboApprovalTo"
        Me.cboApprovalTo.Size = New System.Drawing.Size(165, 21)
        Me.cboApprovalTo.TabIndex = 6
        Me.cboApprovalTo.Visible = False
        '
        'lblApproveLevel
        '
        Me.lblApproveLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproveLevel.Location = New System.Drawing.Point(8, 114)
        Me.lblApproveLevel.Name = "lblApproveLevel"
        Me.lblApproveLevel.Size = New System.Drawing.Size(51, 15)
        Me.lblApproveLevel.TabIndex = 95
        Me.lblApproveLevel.Text = "Level"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 478)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(923, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(711, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(814, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(20, 123)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(67, 15)
        Me.lblJob.TabIndex = 223
        Me.lblJob.Text = "Job"
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(93, 66)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(165, 21)
        Me.cboDepartment.TabIndex = 3
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(93, 120)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(165, 21)
        Me.cboJob.TabIndex = 5
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(20, 69)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(67, 15)
        Me.lblDepartment.TabIndex = 219
        Me.lblDepartment.Text = "Department"
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(20, 96)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(67, 15)
        Me.lblSection.TabIndex = 221
        Me.lblSection.Text = "Section"
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(93, 93)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(165, 21)
        Me.cboSection.TabIndex = 4
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(65, 59)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(247, 17)
        Me.chkIncludeInactiveEmp.TabIndex = 304
        Me.chkIncludeInactiveEmp.Text = "Include Inactive Employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'frmLeaveApprover_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(923, 533)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveApprover_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Leave Approver "
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApproversInfo.ResumeLayout(False)
        Me.gbApproversInfo.PerformLayout()
        Me.gbSelectedEmployee.ResumeLayout(False)
        Me.gbSelectedEmployee.PerformLayout()
        Me.pnlSelectedEmployee.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployee.ResumeLayout(False)
        Me.gbEmployee.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbApproversInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblApproverName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblApproveLevel As System.Windows.Forms.Label
    Friend WithEvents cboApproveLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovalTo As System.Windows.Forms.Label
    Friend WithEvents cboApprovalTo As System.Windows.Forms.ComboBox
    Friend WithEvents gbSelectedEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents pnlSelectedEmployee As System.Windows.Forms.Panel
    Friend WithEvents lvSelectedEmployee As eZee.Common.eZeeListView
    Friend WithEvents colhApprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDept As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents cboFilterSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterSection As System.Windows.Forms.Label
    Friend WithEvents cboeFilterDept As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterDepartment As System.Windows.Forms.Label
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents lvEmployee As System.Windows.Forms.ListView
    Friend WithEvents ColhEmployeecode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkEmployeeAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkAllSelectedEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents colhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboFilterJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterjob As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents ColhEmp As System.Windows.Forms.ColumnHeader
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDepartmentID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSectionID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhJobID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSelectedEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSearchedEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents LblUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkMapLeaveType As System.Windows.Forms.LinkLabel
    Friend WithEvents chkExternalApprover As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
End Class
