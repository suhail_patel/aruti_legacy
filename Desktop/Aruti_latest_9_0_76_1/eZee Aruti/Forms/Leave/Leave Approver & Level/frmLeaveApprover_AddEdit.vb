﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 6

Public Class frmLeaveApprover_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLeaveApprover_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLeaveApprover As clsleaveapprover_master
    Private objLeaveApproverTran As clsleaveapprover_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintApproverUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mstrJobunkid As String = ""
    Private mstrSectionunkid As String = ""
    Private mstrDeprtmentunkid As String = ""
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtSearchEmployee As DataTable = Nothing
    Private mdtLeaveTypeMapping As DataTable = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintApproverUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintApproverUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeaveApprover_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveApprover = New clsleaveapprover_master
        objLeaveApproverTran = New clsleaveapprover_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objLeaveApprover._Approverunkid = mintApproverUnkid
                objLeaveApproverTran._Approverunkid = mintApproverUnkid


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                'objLeaveApproverTran.GetApproverTran(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mintApproverUnkid, objLeaveApprover._Departmentunkid _
                '                                                                                        , objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid)

                objLeaveApproverTran.GetApproverTran(FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mintApproverUnkid, objLeaveApprover._Departmentunkid _
                                                                                                    , objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid, chkIncludeInactiveEmp.Checked)

                'Pinkal (12-Oct-2020) -- End


                objbtnSearchEmployee.Enabled = False

                'S.SANDEEP [30 JAN 2016] -- START
                chkExternalApprover.Enabled = False
                'S.SANDEEP [30 JAN 2016] -- END

            End If
            mdtTran = objLeaveApproverTran._DataList
            Call SetVisibility()
            FillCombo()
            GetValue()
            txtName.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApprover_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApprover_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApprover_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApprover_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveApprover_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveApprover_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveapprover_master.SetMessages()
            clsleaveapprover_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveapprover_master,clsleaveapprover_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region "Button's Event"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then Exit Sub

            If lvEmployee.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployee.Select()
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            Dim dtRow As DataRow
            Dim count As Integer = 0

            For i As Integer = 0 To lvEmployee.CheckedItems.Count - 1
                count = CInt(mdtTran.Compute("count(employeeunkid)", "employeeunkid=" & CInt(lvEmployee.CheckedItems(i).Tag)))
                If count > 0 Then
                    Dim dr() As DataRow = Nothing
                    dr = mdtTran.Select("employeeunkid=" & CInt(lvEmployee.CheckedItems(i).Tag))
                    If dr.Length > 0 Then
                        dr(0)("approverunkid") = mintApproverUnkid
                        dr(0)("approvername") = txtName.Text.Trim
                        dr(0)("AUD") = "A"
                    End If
                Else
                    dtRow = mdtTran.NewRow
                    dtRow("leaveapprovertranunkid") = -1
                    dtRow("approverunkid") = mintApproverUnkid
                    dtRow("approvername") = txtName.Text.Trim
                    dtRow("employeeunkid") = CInt(lvEmployee.CheckedItems(i).Tag)
                    dtRow("name") = lvEmployee.CheckedItems(i).SubItems(ColhEmp.Index).Text.Trim

                    'Pinkal (28-Apr-2020) -- Start
                    'Optimization  - Working on Process Optimization and performance for require module.	
                    'dtRow("departmentunkid") = lvEmployee.CheckedItems(i).SubItems(objcolhDepartmentID.Index).Text.Trim
                    'dtRow("departmentname") = lvEmployee.CheckedItems(i).SubItems(objcolhDepartment.Index).Text.Trim
                    'dtRow("sectionunkid") = lvEmployee.CheckedItems(i).SubItems(objColhSectionID.Index).Text.Trim
                    'dtRow("sectionname") = lvEmployee.CheckedItems(i).SubItems(objColhSection.Index).Text.Trim
                    'dtRow("jobunkid") = lvEmployee.CheckedItems(i).SubItems(objColhJobID.Index).Text.Trim
                    'dtRow("jobname") = lvEmployee.CheckedItems(i).SubItems(objColhJob.Index).Text.Trim
                    dtRow("departmentunkid") = "-1"
                    dtRow("departmentname") = lvEmployee.CheckedItems(i).SubItems(objcolhDepartment.Index).Text.Trim
                    dtRow("sectionunkid") = "-1"
                    dtRow("sectionname") = lvEmployee.CheckedItems(i).SubItems(objColhSection.Index).Text.Trim
                    dtRow("jobunkid") = "-1"
                    dtRow("jobname") = lvEmployee.CheckedItems(i).SubItems(objColhJob.Index).Text.Trim
                    'Pinkal (28-Apr-2020) -- End


                    dtRow("AUD") = "A"
                    dtRow("GUID") = Guid.NewGuid.ToString
                    mdtTran.Rows.Add(dtRow)
                End If
            Next

            SelectedEmplyeeList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvSelectedEmployee.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), enMsgBoxStyle.Information)
                lvSelectedEmployee.Select()
                Exit Sub
            End If
            Dim drTemp As DataRow() = Nothing

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor

                Dim objLeaveForm As New clsleaveform

                For i As Integer = 0 To lvSelectedEmployee.CheckedItems.Count - 1

                    If CInt(lvSelectedEmployee.CheckedItems(i).Tag) = -1 Then
                        drTemp = mdtTran.Select("GUID = '" & lvSelectedEmployee.CheckedItems(i).SubItems(colhGUID.Index).Text & "'")
                    Else

                        If objLeaveForm.GetApproverPendingLeaveFormCount(objLeaveApprover._Approverunkid, lvSelectedEmployee.CheckedItems(i).SubItems(colhemployeeunkid.Index).Text) <= 0 Then
                            drTemp = mdtTran.Select("employeeunkid = " & CInt(lvSelectedEmployee.CheckedItems(i).SubItems(colhemployeeunkid.Index).Text))
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Employee has Pending Leave Application Form.You cannot delete this employee."), enMsgBoxStyle.Information)
                            Exit For
                        End If

                    End If

                    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                    End If

                Next
            End If

            SelectedEmplyeeList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() = False Then Exit Sub

            Dim count As Integer = CInt(mdtTran.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtTran.Rows.Count = 0 Or count = 0 Or lvSelectedEmployee.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                lvEmployee.Select()
                Exit Sub
            End If

            If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                If mdtLeaveTypeMapping Is Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please map leave type for this approver."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If


            SetValue()
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLeaveApprover.Update(mdtTran)
            Else
                blnFlag = objLeaveApprover.Insert(mdtTran)
            End If


            If blnFlag = False And objLeaveApprover._Message <> "" Then
                eZeeMsgBox.Show(objLeaveApprover._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLeaveApprover = Nothing
                    objLeaveApprover = New clsleaveapprover_master
                    objLeaveApproverTran = Nothing
                    objLeaveApproverTran = New clsleaveapprover_Tran
                    chkAllSelectedEmployee.Checked = False
                    chkEmployeeAll.Checked = False
                    lvEmployee.Items.Clear()
                    mstrEmployeeIDs = ""
                    mstrAdvanceFilter = ""
                    GetValue()
                    cboApproveLevel.Focus()
                    'S.SANDEEP [30 JAN 2016] -- START
                    chkExternalApprover.Checked = False
                    'S.SANDEEP [30 JAN 2016] -- END
                Else
                    mintApproverUnkid = objLeaveApprover._Approverunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            chkAllSelectedEmployee.Checked = False
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLevel.Click
        Dim frm As New frmLeaveApprover_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim objfrmLeaveApproverLevel_AddEdit As New frmLeaveApproverLevel_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objApproverLevel As New clsapproverlevel_master
            If objfrmLeaveApproverLevel_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objApproverLevel.getListForCombo("List", True)
                With cboApproveLevel
                    .ValueMember = "levelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboApproveLevel.SelectedValue = intRefId
            End If
            cboApproveLevel.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try

            'S.SANDEEP [30 JAN 2016] -- START
            ''Sohail (06 Jan 2012) -- Start
            ''TRA - ENHANCEMENT
            ''dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
            ''                                     CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)))

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''If menAction = enAction.EDIT_ONE Then
            ''    dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
            ''                                         CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)), , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            ''Else
            ''    dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
            ''                                         CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)), , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            ''End If
            'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, _
            '                                     0, CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue))
            ''S.SANDEEP [04 JUN 2015] -- END
            If chkExternalApprover.Checked = False Then
                Dim blnInActiveEmp As Boolean = False
                If menAction <> enAction.EDIT_ONE Then
                    blnInActiveEmp = False
                Else
                    blnInActiveEmp = True
                End If

                dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, blnInActiveEmp, "Employee", False, _
                                                     0, CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue))

                'Pinkal (06-Jan-2016) -- End

                'S.SANDEEP [04 JUN 2015] -- END


                objfrm.DataSource = dsList.Tables("Employee")
                objfrm.ValueMember = "employeeunkid"
                objfrm.DisplayMember = "employeename"
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog() Then
                    txtName.Text = objfrm.SelectedText
                    txtName.Tag = objfrm.SelectedValue

                    'S.SANDEEP [30 JAN 2016] -- START
                    'mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag))
                    mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)
                    'S.SANDEEP [30 JAN 2016] -- END

                    FillEmployeeList()

                    Dim objOption As New clsPassowdOptions
                    If objOption._IsEmployeeAsUser Then
                        Dim objUser As New clsUserAddEdit
                        Dim mintUserID As Integer = objUser.Return_UserId(CInt(objfrm.SelectedValue), Company._Object._Companyunkid)
                        Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
                        If drRow.Length > 0 Then
                            cboUser.SelectedValue = mintUserID
                        Else
                            cboUser.SelectedValue = 0
                        End If

                    End If
                End If
            Else
                Dim objUser As New clsUserAddEdit

                dsList = objUser.GetExternalApproverList("List", _
                                                         Company._Object._Companyunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "264")
                objUser = Nothing

                objfrm.DataSource = dsList.Tables("List")
                objfrm.ValueMember = "userunkid"
                objfrm.DisplayMember = "Name"
                objfrm.CodeMember = "username"
                If objfrm.DisplayDialog() Then
                    txtName.Text = objfrm.SelectedText
                    txtName.Tag = objfrm.SelectedValue
                    'S.SANDEEP [30 JAN 2016] -- START
                    'mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag))
                    mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)
                    'S.SANDEEP [30 JAN 2016] -- END
                    FillEmployeeList()
                End If
                objUser = Nothing
            End If
            'S.SANDEEP [30 JAN 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchedEmployee.Text = ""
            FillEmployeeList()
            lvEmployee_ItemChecked(sender, New ItemCheckedEventArgs(Nothing))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSelectedEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSelectedEmployee.Click
        Try
            If mdtSearchEmployee Is Nothing Then Exit Sub

            Dim objfrm As New frmCommonSearch
            objfrm.DataSource = mdtSearchEmployee
            objfrm.ValueMember = "employeeunkid"

            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'objfrm.DisplayMember = "name"
            'objfrm.CodeMember = "employeecode"

            'Pinkal (16-Jul-2020) -- Start
            'Bug Benchmark Distributors Ltd: 0004813- Error Message on Assignng Leave Approver.
            'objfrm.DisplayMember = "Employee Name"


            'Pinkal (12-May-2021)-- Start
            'Scania Face Integration Enhancement  -  Working on Scania ZKTeco Face Integration with TnA Device.
            'objfrm.DisplayMember = "EmployeeName"
            ' objfrm.CodeMember = "code"

            If mdtSearchEmployee.Columns.Contains("EmployeeName") = False Then
                mdtSearchEmployee.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ColumnName = "EmployeeName"
            End If

            If mdtSearchEmployee.Columns.Contains("code") = False Then
                mdtSearchEmployee.Columns(Language.getMessage("clsEmployee_Master", 42, "Code")).ColumnName = "code"
            End If


            objfrm.DisplayMember = "EmployeeName"
            objfrm.CodeMember = "code"
            'Pinkal (12-May-2021) -- End
            'Pinkal (16-Jul-2020) -- End
            'Pinkal (28-Apr-2020) -- End

            
            If objfrm.DisplayDialog() Then
                txtSearchedEmployee.Text = objfrm.SelectedText
                lvEmployee.SelectedIndices.Clear()
                Dim lvItem As ListViewItem
                lvItem = lvEmployee.FindItemWithText(objfrm.SelectedText, True, 0, False)
                If lvItem IsNot Nothing Then
                    lvEmployee.Focus()
                    lvEmployee.TopItem = lvItem
                    lvItem.Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSelectedEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboUser.DataSource, DataTable)
            With cboUser
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "CheckBox Event"

    Private Sub chkEmployeeAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmployeeAll.CheckedChanged
        Try
            'For i As Integer = 0 To lvEmployee.Items.Count - 1
            '    lvEmployee.Items(i).Checked = chkEmployeeAll.Checked
            'Next

            'Pinkal (10-Mar-2011) -- Start

            If lvEmployee.Items.Count = 0 Then Exit Sub
            SetOperation(chkEmployeeAll.Checked)

            'Pinkal (10-Mar-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEmployeeAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkAllSelectedEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllSelectedEmployee.CheckedChanged
        Try
            If lvSelectedEmployee.Items.Count = 0 Then Exit Sub
            SetSelectedEmployeeOperation(chkAllSelectedEmployee.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAllSelectedEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            cboApproveLevel.BackColor = GUI.ColorComp
            cboDepartment.BackColor = GUI.ColorComp
            cboSection.BackColor = GUI.ColorComp
            cboJob.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            cboUser.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboApproveLevel.SelectedValue = objLeaveApprover._Levelunkid

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objLeaveApprover._leaveapproverunkid
            'txtName.Text = objEmployee._Firstname + " " + objEmployee._Surname
            'txtName.Tag = objEmployee._Employeeunkid

            'S.SANDEEP [30 JAN 2016] -- START 
            If objLeaveApprover._Isexternalapprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objLeaveApprover._leaveapproverunkid
                Dim sName As String = objUser._Firstname + " " + objUser._Lastname
                txtName.Text = CStr(IIf(sName.Trim = "", objUser._Username, sName))
                txtName.Tag = objUser._Userunkid
                objUser = Nothing
            Else
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeaveApprover._leaveapproverunkid
                txtName.Text = objEmployee._Firstname + " " + objEmployee._Surname
                txtName.Tag = objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            End If
            'S.SANDEEP [30 JAN 2016] -- END 


            'S.SANDEEP [04 JUN 2015] -- END

            mdtTran = objLeaveApproverTran._DataList


            'S.SANDEEP [30 JAN 2016] -- START
            chkExternalApprover.Checked = objLeaveApprover._Isexternalapprover
            'S.SANDEEP [30 JAN 2016] -- END

            If menAction = enAction.EDIT_ONE Then

                'S.SANDEEP [30 JAN 2016] -- START
                'mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag))
                mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), objLeaveApprover._Isexternalapprover)
                'S.SANDEEP [30 JAN 2016] -- END

                FillEmployeeList()
                cboUser.SelectedValue = objLeaveApprover._MapUserId
                Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
                mdtLeaveTypeMapping = objLeaveTypeMapping.GetLeaveTypeForMapping(mintApproverUnkid)
                objLeaveTypeMapping = Nothing
            Else
                cboUser.SelectedValue = 0
            End If


            SelectedEmplyeeList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLeaveApprover._Levelunkid = CInt(cboApproveLevel.SelectedValue)
            objLeaveApprover._leaveapproverunkid = CInt(txtName.Tag)
            objLeaveApprover._Departmentunkid = -1
            objLeaveApprover._Sectionunkid = -1
            objLeaveApprover._Jobunkid = -1
            'S.SANDEEP [30 JAN 2016] -- START
            'objLeaveApprover._MapUserId = CInt(cboUser.SelectedValue)
            If chkExternalApprover.Checked = True Then
                objLeaveApprover._MapUserId = CInt(txtName.Tag)
            Else
                objLeaveApprover._MapUserId = CInt(cboUser.SelectedValue)
            End If
            'S.SANDEEP [30 JAN 2016] -- END
            objLeaveApprover._dtLeaveTypeMapping = mdtLeaveTypeMapping
            objLeaveApprover._Userumkid = User._Object._Userunkid

            'S.SANDEEP [30 JAN 2016] -- START
            objLeaveApprover._Isexternalapprover = chkExternalApprover.Checked
            'S.SANDEEP [30 JAN 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR APPROVER LEVEL
            Dim objApproverLevel As New clsapproverlevel_master
            dsFill = objApproverLevel.getListForCombo("Level", True)
            cboApproveLevel.ValueMember = "levelunkid"
            cboApproveLevel.DisplayMember = "name"
            cboApproveLevel.DataSource = dsFill.Tables("Level")

            'PRIVILEGE NO 264 :- ALLOW TO APPROVE LEAVE
            Dim mblnAdUser As Boolean = False
            Dim objUser As New clsUserAddEdit
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Dim objOption As New clsPassowdOptions
            'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            'If drOption.Length > 0 Then
            '    If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            'End If

            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, Company._Object._Companyunkid, 264)

            'Nilay (01-Mar-2016) -- Start
            'dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, 264)
            dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(264), FinancialYear._Object._YearUnkid, True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            cboUser.DisplayMember = "name"
            cboUser.ValueMember = "userunkid"
            cboUser.DataSource = dsFill.Tables("User")

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'objOption = Nothing
            'S.SANDEEP [10 AUG 2015] -- END
            objUser = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        Dim dtEmployee As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try
            Dim objEmployee As New clsEmployee_Master



            If chkExternalApprover.Checked = False Then
                If Not txtName.Tag Is Nothing And CInt(txtName.Tag) > 0 Then
                    'Pinkal (28-Apr-2020) -- Start
                    'Optimization  - Working on Process Optimization and performance for require module.	
                    'strSearch &= "AND employeeunkid <> " & CInt(txtName.Tag) & " "
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(txtName.Tag) & " "
                    'Pinkal (28-Apr-2020) -- End
                End If
            End If



            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	

            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    strSearch &= "AND " & mstrAdvanceFilter.Replace("ADF.", "") & " "
            'End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter & " "
            End If


            ''If mstrEmployeeIDs.Trim.Length > 0 Then
            ''    strSearch &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            ''End If
            'Pinkal (28-Apr-2020) -- End



            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'Dim blnInActiveEmp As Boolean = False
            'If menAction <> enAction.EDIT_ONE Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If
            Dim blnInActiveEmp As Boolean = chkIncludeInactiveEmp.Checked
            'Pinkal (12-Oct-2020) -- End

          


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	

            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 blnInActiveEmp, _
            '                                 "Employee", _
            '                                ConfigParameter._Object._ShowFirstAppointmentDate, , , , False, False)

            Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & _
                                                           "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Job

            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                      , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                      , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                      , True, blnInActiveEmp, "Employee", -1, False, strSearch, ConfigParameter._Object._ShowFirstAppointmentDate _
                                                                                      , False, False, True, Nothing, True)

            If mstrEmployeeIDs.Trim.Length > 0 Then
                dtEmployee = New DataView(dsEmployee.Tables(0), "employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = dsEmployee.Tables(0)
            End If

            'dtEmployee = New DataView(dsEmployee.Tables(0), strSearch, "", DataViewRowState.CurrentRows).ToTable
            'RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

            'Pinkal (28-Apr-2020) -- End


            'Pinkal (22-May-2021)-- Start
            'bug : Resolved bug In NMB.
            'Pinkal (16-Jul-2020) -- Start
            'Bug Benchmark Distributors Ltd: 0004813- Error Message on Assignng Leave Approver.
            'If dtEmployee IsNot Nothing AndAlso dtEmployee.Columns.Contains("Employee Name") Then
            '    dtEmployee.Columns("Employee Name").ColumnName = "EmployeeName"
            'End If
            'Pinkal (16-Jul-2020) -- End
            'Pinkal (22-May-2021)-- End


            mdtSearchEmployee = dtEmployee

            lvEmployee.Items.Clear()
            If Not dtEmployee Is Nothing Then
                Dim lvItem As ListViewItem

                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	

                'For Each drRow As DataRow In dtEmployee.Rows
                '    lvItem = New ListViewItem
                '    lvItem.Tag = drRow("employeeunkid")
                '    lvItem.Text = drRow("employeecode").ToString
                '    lvItem.SubItems.Add(drRow("name").ToString)
                '    lvItem.SubItems.Add(drRow("DeptName").ToString)
                '    lvItem.SubItems.Add(drRow("section").ToString)
                '    lvItem.SubItems.Add(drRow("job_name").ToString)
                '    lvItem.SubItems.Add(drRow("departmentunkid").ToString)
                '    lvItem.SubItems.Add(drRow("sectionunkid").ToString)
                '    lvItem.SubItems.Add(drRow("jobunkid").ToString)
                '    lvEmployee.Items.Add(lvItem)
                'Next
                Dim lvArray As New List(Of ListViewItem)
                lvEmployee.BeginUpdate()
                For Each drRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = drRow("employeeunkid")

                    'Pinkal (22-May-2021)-- Start
                    'bug : Resolved bug In NMB.
                    'lvItem.Text = drRow("code").ToString
                    'lvItem.SubItems.Add(drRow("EmployeeName").ToString)
                    'lvItem.SubItems.Add(drRow("Department").ToString)
                    'lvItem.SubItems.Add(drRow("section").ToString)
                    'lvItem.SubItems.Add(drRow("Job").ToString)
                    lvItem.Text = drRow(Language.getMessage("clsEmployee_Master", 42, "Code")).ToString
                    lvItem.SubItems.Add(drRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString)
                    lvItem.SubItems.Add(drRow(Language.getMessage("clsEmployee_Master", 120, "Department")).ToString)
                    lvItem.SubItems.Add(drRow(Language.getMessage("clsEmployee_Master", 198, "Section")).ToString)
                    lvItem.SubItems.Add(drRow(Language.getMessage("clsEmployee_Master", 118, "Job")).ToString)
                    'Pinkal (22-May-2021) -- End


                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add("-1")
                    lvItem.SubItems.Add("-1")
                    lvArray.Add(lvItem)
                Next

                RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
                lvEmployee.Items.AddRange(lvArray.ToArray)
                AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

                If lvEmployee.Items.Count > 20 Then
                    ColhEmp.Width = 210 - 18
                Else
                    ColhEmp.Width = 210
                End If

                lvArray = Nothing
                lvEmployee.EndUpdate()

            End If



            If lvEmployee.Items.Count > 0 Then
                chkEmployeeAll.Enabled = True
            Else
                chkEmployeeAll.Checked = False
                chkEmployeeAll.Enabled = False
            End If

            'AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

            'Pinkal (28-Apr-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [30 JAN 2016] -- START
    'Private Sub FillDepartmentTreeView()
    '    Dim objDepartment As New clsDepartment
    '    Dim objSection As New clsSections
    '    Dim objJob As New clsJobs
    '    Try
    '        tvLevels.Nodes.Clear()

    '        'FOR DEPARTMENT
    '        Dim dsDept As DataSet = objDepartment.getComboList("Department", False)
    '        Dim dtDept As DataTable = Nothing
    '        If CInt(cboDepartment.SelectedValue) > 0 Then
    '            dtDept = New DataView(dsDept.Tables("Department"), "departmentunkid=" & CInt(cboDepartment.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '        ElseIf CInt(cboDepartment.SelectedValue) = -1 Then
    '            dtDept = dsDept.Tables("Department")
    '        End If

    '        'FOR SECTION
    '        Dim dsSection As DataSet = objSection.GetList("Section")
    '        Dim dtSection As DataTable = Nothing
    '        If CInt(cboDepartment.SelectedValue) > 0 Or CInt(cboSection.SelectedValue) > 0 Then
    '            dtSection = New DataView(dsSection.Tables("Section"), "departmentunkid=" & CInt(cboDepartment.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '            If CInt(cboSection.SelectedValue) > 0 Then dtSection = New DataView(dtSection, "sectionunkid=" & CInt(cboSection.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '        ElseIf CInt(cboDepartment.SelectedValue) = -1 Then
    '            dtSection = dsSection.Tables("Section")
    '        End If

    '        'FOR JOBS
    '        Dim dsJob As DataSet = objJob.GetList("Job")
    '        Dim dtjob As DataTable = Nothing
    '        If CInt(cboDepartment.SelectedValue) > 0 Then
    '            If CInt(cboJob.SelectedValue) > 0 Then
    '                dtjob = New DataView(dsJob.Tables("job"), "jobunkid=" & CInt(cboJob.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtjob = dsJob.Tables("Job")
    '            End If

    '        ElseIf CInt(cboDepartment.SelectedValue) = -1 Or CInt(cboSection.SelectedValue) = -1 Then
    '            dtjob = dsJob.Tables("Job")
    '        End If


    '        If dtDept Is Nothing Then Exit Sub


    '        For Each drDepartment As DataRow In dtDept.Rows
    '            Dim tvNodeDepartment As TreeNode
    '            tvNodeDepartment = tvLevels.Nodes.Add(drDepartment.Item("name").ToString())
    '            tvNodeDepartment.Tag = CInt(drDepartment.Item("departmentunkid"))

    '            If dtSection Is Nothing Then Continue For

    '            For Each drRow As DataRow In dtSection.Rows
    '                If CInt(drDepartment.Item("departmentunkid").ToString()) = CInt(drRow("departmentunkid").ToString) Then
    '                    tvNodeDepartment.Nodes.Add(drRow("sectionunkid").ToString, drRow("name").ToString)
    '                    tvNodeDepartment.Nodes(drRow("sectionunkid").ToString).Tag = drRow("sectionunkid").ToString

    '                    If dtjob Is Nothing Then Continue For

    '                    For Each drJob As DataRow In dtjob.Rows
    '                        If CInt(drRow.Item("sectionunkid").ToString()) = CInt(drJob("jobsectionunkid").ToString) Then
    '                            tvNodeDepartment.Nodes(drRow("sectionunkid").ToString).Nodes.Add(drJob("jobunkid").ToString, drJob("jobname").ToString)
    '                            tvNodeDepartment.Nodes(drRow("sectionunkid").ToString).Nodes(drJob("jobunkid").ToString).Tag = drJob("jobunkid").ToString
    '                        End If
    '                    Next

    '                End If
    '            Next
    '        Next
    '        tvLevels.ExpandAll()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillDepartmentTreeView", mstrModuleName)
    '    Finally
    '        objDepartment = Nothing
    '    End Try
    'End Sub
    'S.SANDEEP [30 JAN 2016] -- END



    Private Sub SelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Dim dtEmployee As DataTable = Nothing
        Try

            strSearch = "AND AUD <> 'D' "


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'If CInt(cboeFilterDept.SelectedValue) > 0 Then
            '    strSearch &= "AND departmentunkid=" & CInt(cboeFilterDept.SelectedValue) & " "
            'End If
            'If CInt(cboFilterSection.SelectedValue) > 0 Then
            '    strSearch &= "AND sectionunkid=" & CInt(cboFilterSection.SelectedValue)
            'End If
            'If CInt(cboFilterJob.SelectedValue) > 0 Then
            '    strSearch &= "AND jobunkid=" & CInt(cboFilterJob.SelectedValue)
            'End If
            'Pinkal (28-Apr-2020) -- End

           


            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(mdtTran, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = mdtTran
            End If


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'RemoveHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
            'Pinkal (28-Apr-2020) -- End



            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	


            If Not dtEmployee Is Nothing Then
                Dim lvItem As ListViewItem
                lvSelectedEmployee.Items.Clear()
                Dim lvArray As New List(Of ListViewItem)
                lvSelectedEmployee.BeginUpdate()
                For Each drRow As DataRow In dtEmployee.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = ""
                    lvItem.Tag = CInt(drRow("leaveapprovertranunkid"))
                    lvItem.SubItems.Add(drRow("approvername").ToString)
                    lvItem.SubItems.Add(drRow("departmentname").ToString)
                    lvItem.SubItems.Add(drRow("sectionname").ToString)
                    lvItem.SubItems.Add(drRow("jobname").ToString)
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("GUID").ToString)
                    lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                    'lvSelectedEmployee.Items.Add(lvItem)
                    lvArray.Add(lvItem)
                Next
                RemoveHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
                lvSelectedEmployee.Items.AddRange(lvArray.ToArray)
                AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked

                lvArray = Nothing
                lvSelectedEmployee.EndUpdate()
            End If

            If lvSelectedEmployee.Items.Count > 0 Then
                chkAllSelectedEmployee.Enabled = True
            Else
                chkAllSelectedEmployee.Checked = False
                chkAllSelectedEmployee.Enabled = False
            End If

            'AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked

            'Pinkal (28-Apr-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SelectedEmplyeeList", mstrModuleName)
        Finally
            dtEmployee.Dispose()
        End Try

    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboApproveLevel.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Approver Level is compulsory information.Please Select Approver Level."), enMsgBoxStyle.Information)
                cboApproveLevel.Select()
                Return False


            ElseIf txtName.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Approver Name cannot be blank. Approver Name is required information."), enMsgBoxStyle.Information)
                txtName.Select()
                Return False


            ElseIf CInt(cboUser.SelectedValue) <= 0 Then
                'S.SANDEEP [30 JAN 2016] -- START
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "User is compulsory information.Please map user to this approver."), enMsgBoxStyle.Information)
                'cboUser.Select()
                'Return False
                If chkExternalApprover.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "User is compulsory information.Please map user to this approver."), enMsgBoxStyle.Information)
                    cboUser.Select()
                    Return False
                End If
                'S.SANDEEP [30 JAN 2016] -- END

            ElseIf ConfigParameter._Object._IsLeaveApprover_ForLeaveType = True AndAlso (lnkMapLeaveType.Enabled = False And menAction <> enAction.EDIT_ONE) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Leave Type Mapping is compulsory information.Please map leave type to this approver."), enMsgBoxStyle.Information)
                lnkMapLeaveType.Focus()
                Return False

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetVisibility()
        Try
            objbtnAddLevel.Enabled = User._Object.Privilege._AddLeaveApproverLevel
            cboUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser
            objbtnSearchUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser
            lnkMapLeaveType.Enabled = User._Object.Privilege._AllowtoMapLeaveType
            lnkMapLeaveType.Visible = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvEmployee.Items
                RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub SetSelectedEmployeeOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvSelectedEmployee.Items
                RemoveHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetSelectedEmployeeOperation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployee.ItemChecked
        Try
            RemoveHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
            If lvEmployee.CheckedItems.Count <= 0 Then
                chkEmployeeAll.CheckState = CheckState.Unchecked

            ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
                chkEmployeeAll.CheckState = CheckState.Indeterminate

            ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
                chkEmployeeAll.CheckState = CheckState.Checked

            End If
            AddHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasters_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvSelectedEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSelectedEmployee.ItemChecked
        Try
            RemoveHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
            If lvSelectedEmployee.CheckedItems.Count <= 0 Then
                chkAllSelectedEmployee.CheckState = CheckState.Unchecked

            ElseIf lvSelectedEmployee.CheckedItems.Count < lvSelectedEmployee.Items.Count Then
                chkAllSelectedEmployee.CheckState = CheckState.Indeterminate

            ElseIf lvSelectedEmployee.CheckedItems.Count = lvSelectedEmployee.Items.Count Then
                chkAllSelectedEmployee.CheckState = CheckState.Checked

            End If
            AddHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSelectedEmployee_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkMapLeaveType_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkMapLeaveType.LinkClicked
        Try
            If txtName.Text.Trim.Length > 0 AndAlso CInt(cboApproveLevel.SelectedValue) > 0 Then
                Dim objFrm As New frmApprover_leavetypeMapping
                objFrm._EmployeeName = txtName.Text.Trim
                objFrm._ApproverLevel = cboApproveLevel.Text
                If mdtLeaveTypeMapping IsNot Nothing AndAlso mdtLeaveTypeMapping.Rows.Count > 0 Then
                    objFrm._dtLeaveTypeMapping = mdtLeaveTypeMapping
                End If
                objFrm.displayDialog(-1, enAction.ADD_ONE, mintApproverUnkid)
                mdtLeaveTypeMapping = objFrm._dtLeaveTypeMapping
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Name or Level to do futher operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkMapLeaveType_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [30 JAN 2016] -- START
#Region " Checkbox Event "

    Private Sub chkExternalApprover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            LblUser.Visible = Not chkExternalApprover.Checked
            cboUser.Visible = Not chkExternalApprover.Checked
            objbtnSearchUser.Visible = Not chkExternalApprover.Checked
            If chkExternalApprover.Checked = False Then
                cboUser.SelectedValue = 0
            End If
            If menAction <> enAction.EDIT_ONE Then
                txtName.Text = ""
                txtName.Tag = Nothing
                mstrEmployeeIDs = ""
                lvEmployee.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalApprover_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (12-Oct-2020) -- Start
    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
    Private Sub chkIncludeInactiveEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveEmp.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            If txtName.Tag IsNot Nothing AndAlso CInt(txtName.Tag) > 0 Then
                lvEmployee.SuspendLayout()
                FillEmployeeList()
                lvEmployee.ResumeLayout()
            End If
            If menAction = enAction.EDIT_ONE Then

                objLeaveApprover._Approverunkid = mintApproverUnkid
                objLeaveApproverTran._Approverunkid = mintApproverUnkid


                objLeaveApproverTran.GetApproverTran(FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mintApproverUnkid, objLeaveApprover._Departmentunkid _
                                                                                                    , objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid, chkIncludeInactiveEmp.Checked)

                mdtTran = objLeaveApproverTran._DataList.Copy()

                lvSelectedEmployee.SuspendLayout()
                SelectedEmplyeeList()
                lvSelectedEmployee.ResumeLayout()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIncludeInactiveEmp_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Pinkal (12-Oct-2020) -- End


#End Region
    'S.SANDEEP [30 JAN 2016] -- END




  
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbApproversInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApproversInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSelectedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSelectedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbApproversInfo.Text = Language._Object.getCaption(Me.gbApproversInfo.Name, Me.gbApproversInfo.Text)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.Name, Me.lblApproverName.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.Name, Me.lblApproveLevel.Text)
            Me.lblApprovalTo.Text = Language._Object.getCaption(Me.lblApprovalTo.Name, Me.lblApprovalTo.Text)
            Me.gbSelectedEmployee.Text = Language._Object.getCaption(Me.gbSelectedEmployee.Name, Me.gbSelectedEmployee.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
            Me.colhDept.Text = Language._Object.getCaption(CStr(Me.colhDept.Tag), Me.colhDept.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.lblFilterSection.Text = Language._Object.getCaption(Me.lblFilterSection.Name, Me.lblFilterSection.Text)
            Me.lblFilterDepartment.Text = Language._Object.getCaption(Me.lblFilterDepartment.Name, Me.lblFilterDepartment.Text)
            Me.ColhEmployeecode.Text = Language._Object.getCaption(CStr(Me.ColhEmployeecode.Tag), Me.ColhEmployeecode.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.chkEmployeeAll.Text = Language._Object.getCaption(Me.chkEmployeeAll.Name, Me.chkEmployeeAll.Text)
            Me.chkAllSelectedEmployee.Text = Language._Object.getCaption(Me.chkAllSelectedEmployee.Name, Me.chkAllSelectedEmployee.Text)
            Me.colhGUID.Text = Language._Object.getCaption(CStr(Me.colhGUID.Tag), Me.colhGUID.Text)
            Me.colhemployeeunkid.Text = Language._Object.getCaption(CStr(Me.colhemployeeunkid.Tag), Me.colhemployeeunkid.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.lblFilterjob.Text = Language._Object.getCaption(Me.lblFilterjob.Name, Me.lblFilterjob.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.ColhEmp.Text = Language._Object.getCaption(CStr(Me.ColhEmp.Tag), Me.ColhEmp.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.Name, Me.LblUser.Text)
            Me.lnkMapLeaveType.Text = Language._Object.getCaption(Me.lnkMapLeaveType.Name, Me.lnkMapLeaveType.Text)
            Me.chkExternalApprover.Text = Language._Object.getCaption(Me.chkExternalApprover.Name, Me.chkExternalApprover.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Approver Level is compulsory information.Please Select Approver Level.")
            Language.setMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee.")
            Language.setMessage(mstrModuleName, 5, "This Employee has Pending Leave Application Form.You cannot delete this employee.")
            Language.setMessage(mstrModuleName, 6, "Approver Name cannot be blank. Approver Name is required information.")
            Language.setMessage(mstrModuleName, 7, "Please Select Name or Level to do futher operation.")
            Language.setMessage(mstrModuleName, 8, "Please map leave type for this approver.")
            Language.setMessage(mstrModuleName, 9, "User is compulsory information.Please map user to this approver.")
            Language.setMessage(mstrModuleName, 10, "Leave Type Mapping is compulsory information.Please map leave type to this approver.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class