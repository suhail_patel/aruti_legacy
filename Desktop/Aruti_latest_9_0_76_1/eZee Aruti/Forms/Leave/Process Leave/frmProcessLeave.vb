﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.Threading
Imports System.IO

'Last Message Index = 3

Public Class frmProcessLeave

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmProcessLeave"
    Private mblnCancel As Boolean = True
    Private objPendingLeave As clspendingleave_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Friend mintPendingLeaveUnkid As Integer = -1
    Private mintStausunkid As Integer = -1
    Private mdtTran As DataTable
    Private mintApproverEmpID As Integer = -1    'Pinkal (29-Sep-2011) -- Start    ' Issue :- Add One Paramerter For Passing Leave Approver Employee UNK ID to the Process Leave For Fill Approver Combo With Specific Approver
    Friend mintLeavetypeunkid As Integer
    Friend mdtStartdate As DateTime
    Friend mdtenddate As DateTime


    'Pinkal (03-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private dtExpense As DataTable
    'Pinkal (03-APR-2012) -- End



    'Pinkal (19-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mintPriority As Integer = -1
    'Pinkal (19-APR-2012) -- End


    'Pinkal (15-Jul-2013) -- Start
    'Enhancement : TRA Changes
    Private dtFraction As DataTable
    'Pinkal (15-Jul-2013) -- End


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes
    Dim mintClaimRequestMasterId As Integer = 0
    'Pinkal (06-Mar-2014) -- End


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    Private trd As Thread
    'Pinkal (03-Nov-2014) -- End

    'S.SANDEEP [30 JAN 2016] -- START
    Private mblnIsExternalApprover As Boolean = False
    'S.SANDEEP [30 JAN 2016] -- END


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Dim mdtClaimAttachment As DataTable = Nothing
    Dim objClaimPendingMst As clsclaim_request_master
    'Pinkal (10-Jan-2017) -- End

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private intEligibleExpenseDays As Integer = 0
    'Pinkal (29-Sep-2017) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnIsLvTypePaid As Boolean = False
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnConsiderExpense As Boolean = True
    'Pinkal (03-May-2019) -- End

#End Region

    Public Property _Priority() As Integer
        Get
            Return _Priority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property


#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal intFormunkid As Integer, ByVal mstrFormNo As String, _
                                              ByVal intemployeeunkid As Integer, ByVal mstrEmployeeName As String, ByVal intStatusunkid As Integer, _
                                              ByVal mdtApplydate As DateTime, ByVal mdtMaxApprovaldate As DateTime, ByVal eAction As enAction, _
                                              ByVal intApproverEmpID As Integer, ByVal blnIsExternalApprover As Boolean) As Boolean 'S.SANDEEP [30 JAN 2016] -- START {blnIsExternalApprover} -- END
        Try
            mintPendingLeaveUnkid = intUnkId
            menAction = eAction
            txtFormNo.Tag = intFormunkid
            txtFormNo.Text = mstrFormNo
            mintStausunkid = intStatusunkid

            txtEmployee.Tag = intemployeeunkid
            txtEmployee.Text = mstrEmployeeName
            dtpApplyDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpApplyDate.Enabled = False

            'S.SANDEEP [30 JAN 2016] -- START
            mblnIsExternalApprover = blnIsExternalApprover
            'S.SANDEEP [30 JAN 2016] -- END

            mintApproverEmpID = intApproverEmpID

            Me.ShowDialog()

            intUnkId = mintPendingLeaveUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmProcessLeave_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPendingLeave = New clspendingleave_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            If mintPendingLeaveUnkid > 0 Then
                objPendingLeave._Pendingleavetranunkid = mintPendingLeaveUnkid
            End If

            GetEmployeeValue()
            FillCombo()
            GetValue()
            txtFormNo.Select()

            If ConfigParameter._Object._IsArutiDemo = False Then
                cboAllowance.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            LblReliever.Enabled = ConfigParameter._Object._SetRelieverAsMandatoryForApproval
            cboReliever.Enabled = ConfigParameter._Object._SetRelieverAsMandatoryForApproval
            objbtnSearchReliever.Enabled = ConfigParameter._Object._SetRelieverAsMandatoryForApproval
            'Pinkal (01-Oct-2018) -- End

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            chkIncludeAllStaff.Enabled = ConfigParameter._Object._SetRelieverAsMandatoryForApproval
            'Pinkal (03-May-2019) -- End



            dtpStartdate.Value = mdtStartdate.Date
            dtpEnddate.Value = mdtenddate.Date
            Dim objLeaveform As New clsleaveform
            Dim objlvtype As New clsleavetype_master
            objLeaveform._Formunkid = CInt(txtFormNo.Tag)
            objlvtype._Leavetypeunkid = objLeaveform._Leavetypeunkid
            lnkShowScanDocuementList.Visible = objlvtype._IsCheckDocOnLeaveForm

            objlblLeaveDate.Text = ConfigParameter._Object._CurrentDateAndTime.Date.ToShortDateString
            GetBalanceInfo()


            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            If ConfigParameter._Object._AllowLvApplicationApplyForBackDates = False Then
                dtpStartdate.MinDate = mdtStartdate.Date
            End If
            'Pinkal (25-Mar-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessLeave_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessLeave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessLeave_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessLeave_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'btnSave_Click(sender, e)
                btnApprove_Click(sender, e)
                'Pinkal (25-May-2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProcessLeave_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProcessLeave_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPendingLeave = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspendingleave_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clspendingleave_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboAllowance_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllowance.SelectedIndexChanged
        Dim objEmpEd As clsEarningDeduction = Nothing
        Dim dsEmpEd As DataSet = Nothing
        Dim objTranHead As New clsTransactionHead
        Try
            Dim dtAllowance As DataTable = CType(cboAllowance.DataSource, DataTable)
            If cboAllowance.SelectedIndex > 0 Then
                objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dtAllowance.Rows(cboAllowance.SelectedIndex).Item("tranheadunkid").ToString)
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    txtAllowance.Enabled = True
                Else
                    txtAllowance.Enabled = False
                End If
            Else
                txtAllowance.Enabled = False
            End If

            txtAllowance.Enabled = False

            objEmpEd = New clsEarningDeduction
            dsEmpEd = objEmpEd.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpED", True, txtEmployee.Tag.ToString, , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            txtAllowance.Text = "0.00"
            If Not dsEmpEd Is Nothing And dsEmpEd.Tables("EmpED").Rows.Count > 0 Then
                Dim drRow() As DataRow = dsEmpEd.Tables("EmpED").Select("tranheadunkid=" & CInt(cboAllowance.SelectedValue))
                If drRow.Length > 0 Then
                    txtAllowance.Text = drRow(0)("amount").ToString
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllowance_SelectedIndexChanged", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click, btnReSchedule.Click
        Dim blnFlag As Boolean = False
        Try

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            Dim xStatusId As Integer = 2

            If CType(sender, Button).Name.ToUpper() = btnApprove.Name.ToUpper() Then
                xStatusId = 1
            ElseIf CType(sender, Button).Name.ToUpper() = btnReject.Name.ToUpper() Then
                xStatusId = 3
            ElseIf CType(sender, Button).Name.ToUpper() = btnReSchedule.Name.ToUpper() Then
                xStatusId = 4
            End If
            'Pinkal (25-May-2019) -- End


            If CInt(cboApprover.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Approver is compulsory information.Please Select Approver."), enMsgBoxStyle.Information)
                cboApprover.Focus()
                Exit Sub

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboStatus.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Status is compulsory information.Please Select Status."), enMsgBoxStyle.Information)
                '    cboStatus.Focus()
                '    Exit Sub
            End If
            'Pinkal (25-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If (CInt(cboStatus.SelectedIndex) = 3 Or CInt(cboStatus.SelectedIndex) = 4) And txtRemark.Text.Trim.Length = 0 Then
            If (xStatusId = 3 OrElse xStatusId = 4) AndAlso txtRemark.Text.Trim.Length = 0 Then
                'Pinkal (25-May-2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Remark cannot be blank. Remark is required information."), enMsgBoxStyle.Information)
                txtRemark.Focus()
                Exit Sub
            End If

            If dtpStartdate.Value.Date > dtpEnddate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Proper Start Date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dtpStartdate.Select()
                Exit Sub
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If ConfigParameter._Object._SetRelieverAsMandatoryForApproval AndAlso CInt(cboReliever.SelectedValue) <= 0 AndAlso CInt(cboStatus.SelectedValue) = 1 Then   'Check Only When Status is only approved.
            If ConfigParameter._Object._SetRelieverAsMandatoryForApproval AndAlso CInt(cboReliever.SelectedValue) <= 0 AndAlso xStatusId = 1 Then   'Check Only When Status is only approved.
                'Pinkal (25-May-2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Reliever is compulsory information.Please select reliever to approve/reject the leave application."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboReliever.Select()
                Exit Sub
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnIsLvTypePaid Then
                Dim mdecTotalDays As Decimal = 0
                If dtFraction IsNot Nothing Then
                    mdecTotalDays = dtFraction.AsEnumerable().Where(Function(row) row.Field(Of String)("AUD") <> "D").Sum(Function(row) row.Field(Of Decimal)("dayfraction"))
                End If

                If mintLvformMinDays <> 0 And mintLvformMinDays > mdecTotalDays Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Language.getMessage(mstrModuleName, 25, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                If mintLvformMaxDays <> 0 And mintLvformMaxDays < mdecTotalDays Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Language.getMessage(mstrModuleName, 25, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            'If mintClaimRequestMasterId <= 0 Then
            '    Dim objclmreq As New clsclaim_request_master
            '    mintClaimRequestMasterId = objclmreq.GetClaimRequestMstIDFromLeaveForm(CInt(txtFormNo.Tag))
            '    objclmreq = Nothing
            'End If
            If dtExpense IsNot Nothing AndAlso dtExpense.Rows.Count > 0 Then

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If (CInt(cboStatus.SelectedValue)) = 1 AndAlso dtExpense.Rows.Count > 0 Then
                If xStatusId = 1 AndAlso dtExpense.Rows.Count > 0 Then
                    'Pinkal (25-May-2019) -- End
                    If IsValidExpense_Eligility() = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'Pinkal (29-Sep-2017) -- End

            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sending Email(s) process is in progress from other module. Please wait for some time."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim objMaster As New clsMasterData

            If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue Then

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                'Dim mintPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date)
                Dim mintPeriodId As Integer = 0
                If mblnConsiderLeaveOnTnAPeriod Then
                    mintPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date)
                Else
                    mintPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date, FinancialYear._Object._YearUnkid, 0, False, True, Nothing, True)
                End If
                'Pinkal (01-Jan-2019) -- End


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If mintPeriodId > 0 AndAlso CInt(cboStatus.SelectedValue) = 1 Then
                If mintPeriodId > 0 AndAlso xStatusId = 1 Then
                    'Pinkal (25-May-2019) -- End
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId
                    If objPeriod._Statusid = enStatusType.Close Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't approve this leave for this employee.Reason: Leave dates are falling in a period which is already closed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    ElseIf objPeriod._Statusid = enStatusType.Open Then

                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo = True Then
                            Dim objTnALeaveTran As New clsTnALeaveTran

                            Dim mdtTnADate As DateTime = Nothing

                            'Pinkal (01-Jan-2019) -- Start
                            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                            Dim mintModuleRefence As enModuleReference
                            If mblnConsiderLeaveOnTnAPeriod Then
                            If objPeriod._TnA_EndDate.Date < dtpStartdate.Value.Date Then
                                mdtTnADate = dtpStartdate.Value.Date
                            Else
                                mdtTnADate = objPeriod._TnA_EndDate.Date
                            End If
                                mintModuleRefence = enModuleReference.TnA
                            Else
                                If objPeriod._End_Date.Date < dtpStartdate.Value.Date Then
                                    mdtTnADate = dtpStartdate.Value.Date
                                Else
                                    mdtTnADate = objPeriod._End_Date.Date
                                End If
                                mintModuleRefence = enModuleReference.Payroll
                            End If

                            'If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), txtEmployee.Tag.ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            '    Exit Sub
                            'End If

                            If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), txtEmployee.Tag.ToString(), mdtTnADate.Date, mintModuleRefence) Then
                                'Sohail (19 Apr 2019) -- Start
                                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 27, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Dim objFrm As New frmProcessPayroll
                                    objFrm.ShowDialog()
                                End If
                                'Sohail (19 Apr 2019) -- End
                                Exit Sub
                            End If

                            'Pinkal (01-Jan-2019) -- End

                        End If

                    End If

                End If

            End If


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 2 Then
            If xStatusId = 2 Then
                'Pinkal (25-May-2019) -- End


                'Pinkal (03-Jan-2020) -- Start
                'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                'Dim dsStatusList As DataSet = objMaster.getLeaveStatusList("Status")
                Dim dsStatusList As DataSet = objMaster.getLeaveStatusList("Status", ConfigParameter._Object._ApplicableLeaveStatus)
                'Pinkal (03-Jan-2020) -- End


                Dim drRow() As DataRow = dsStatusList.Tables(0).Select("statusunkid = " & objPendingLeave._Statusunkid)
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't change ") & drRow(0)("Name").ToString() _
                                    & " " & Language.getMessage(mstrModuleName, 8, "status to Pending status."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If

            'Pinkal (06-Jan-2016) -- End

            If dtFraction IsNot Nothing Then
                Dim drRow() As DataRow = dtFraction.Select("AUD <> 'D'")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You cannot approve this leave form.Reason : As these days are falling in holiday(s) and weekend."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            'If ConfigParameter._Object._IsAutomaticIssueOnFinalApproval Then

            '    If GetPromptForBalance() = False Then
            '        Exit Sub
            '    End If

            'Else

            '    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso CInt(cboStatus.SelectedValue) = 1 Then
            '        Dim dsList As DataSet = Nothing
            '        Dim objLeaveIssueTran As New clsleaveissue_Tran
            '        Dim objbalance As New clsleavebalance_tran
            '        Dim objlvForm As New clsleaveform
            '        objlvForm._Formunkid = CInt(txtFormNo.Tag)


            '        dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                                , True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing)

            '    End If

            'End If


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If GetPromptForBalance() = False Then Exit Sub
            If GetPromptForBalance(xStatusId) = False Then Exit Sub
            'Pinkal (25-May-2019) -- End



            'Pinkal (08-Oct-2018) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            If xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                    Dim objPaymentTran As New clsPayment_tran
                    Dim objPeriod As New clsMasterData

                    'Pinkal (01-Jan-2019) -- Start
                    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                    'Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date)
                    Dim intPeriodId As Integer = 0
                    If mblnConsiderLeaveOnTnAPeriod Then
                        intPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date)
                    Else
                        intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartdate.Value.Date, FinancialYear._Object._YearUnkid, 0, False, True, Nothing, True)
                    End If

                    'Dim mdtTnAStartdate As Date = Nothing
                    'If intPeriodId > 0 Then
                    '    Dim objTnAPeriod As New clscommom_period_Tran
                    '    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    'End If

                    'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(txtEmployee.Tag), mdtTnAStartdate, intPeriodId, dtpStartdate.Value.Date) > 0 Then

                    Dim mdtTnAStartdate As Date = Nothing
                    If intPeriodId > 0 Then
                        Dim objTnAPeriod As New clscommom_period_Tran
                        If mblnConsiderLeaveOnTnAPeriod Then
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        Else
                            objTnAPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                            mdtTnAStartdate = objTnAPeriod._Start_Date
                        End If
                        objTnAPeriod = Nothing
                    End If

                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(txtEmployee.Tag), mdtTnAStartdate, intPeriodId, dtpStartdate.Value.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                        'Pinkal (01-Jan-2019) -- End
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?  "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If

                    End If

                End If

            End If


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If SetValue() = False Then Exit Sub
            If SetValue(xStatusId) = False Then Exit Sub
            'Pinkal (25-May-2019) -- End


            If mintPendingLeaveUnkid > 0 Then

                blnFlag = objPendingLeave.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._IsIncludeInactiveEmp.ToString(), ConfigParameter._Object._EmployeeAsOnDate _
                                                                , FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                                , ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), ConfigParameter._Object._LeaveBalanceSetting _
                                                                , ConfigParameter._Object._IsAutomaticIssueOnFinalApproval.ToString(), ConfigParameter._Object._SkipEmployeeMovementApprovalFlow, ConfigParameter._Object._CreateADUserFromEmpMst)
                'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
            Else

                blnFlag = objPendingLeave.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp.ToString(), ConfigParameter._Object._EmployeeAsOnDate)

            End If

            If blnFlag = False And objPendingLeave._Message <> "" Then
                eZeeMsgBox.Show(objPendingLeave._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then

                Dim objNet As New clsNetConnectivity
                If objNet._Conected Then
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = CInt(txtFormNo.Tag)

                    Dim objLeaveType As New clsleavetype_master
                    objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid
                    objPendingLeave.SendMailToApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                            , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                            , CInt(txtEmployee.Tag), txtFormNo.Text.Trim, mintPriority, objPendingLeave._Statusunkid _
                                                                            , objLeaveType._Leavename, "", enLogin_Mode.DESKTOP, 0)

                    If objLeaveForm._Statusunkid <> 2 Then    'Pinkal (01-Feb-2014) -- Start  And objLeaveForm._Statusunkid <> 7

                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeaveForm._Employeeunkid
                        objLeaveForm._EmployeeCode = objEmp._Employeecode
                        objLeaveForm._EmployeeFirstName = objEmp._Firstname
                        objLeaveForm._EmployeeMiddleName = objEmp._Othername
                        objLeaveForm._EmployeeSurName = objEmp._Surname
                        objLeaveForm._EmpMail = objEmp._Email

                        If objLeaveForm._Statusunkid = 1 Then
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objLeaveForm.SendMailToUser(objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), txtFormNo.Text.Trim, objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date, "", "", , enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                            objLeaveForm.SendMailToUser(objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), txtFormNo.Text.Trim, objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date, Company._Object._Companyunkid, "", "", , enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If



                        'Pinkal (03-May-2019) -- Start
                        'Enhancement - Working on Leave UAT Changes for NMB.

                        '*************** START TO SEND RELIEVER EMAIL NOTIFICATION************************
                        If ConfigParameter._Object._SetRelieverAsMandatoryForApproval AndAlso objLeaveForm._Statusunkid = 7 AndAlso objPendingLeave._RelieverEmpId > 0 Then

                            'Pinkal (25-May-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            'objLeaveForm.SendMailToReliever(Company._Object._Companyunkid, objPendingLeave._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                            '                                                , objLeaveForm._Formunkid, objLeaveForm._Formno, txtEmployee.Text.Trim(), objPendingLeave._Startdate, objPendingLeave._Enddate _
                            '                                                , User._Object._Userunkid, enLogin_Mode.DESKTOP)

                            objLeaveForm.SendMailToReliever(Company._Object._Companyunkid, objPendingLeave._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                                                                            , objLeaveForm._Formunkid, objLeaveForm._Formno, objEmp._Firstname & " " & objEmp._Surname, objPendingLeave._Startdate, objPendingLeave._Enddate _
                                                                            , User._Object._Userunkid, objLeaveForm._Statusunkid, "", enLogin_Mode.DESKTOP)

                            'Pinkal (25-May-2019) -- End


                        End If

                        '*************** END TO SEND RELIEVER EMAIL NOTIFICATION************************

                        'Pinkal (03-May-2019) -- End

                        If objEmp._Email.Trim.Length <= 0 Then GoTo CloseForm

                        If objLeaveForm._Statusunkid = 1 OrElse objLeaveForm._Statusunkid = 7 Then
                            Dim objfrmsendmail As New frmSendMail
                            Dim strPath As String = ""
                            If mintClaimRequestMasterId > 0 Then
                                If ConfigParameter._Object._IsAutomaticIssueOnFinalApproval AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then

                                    strPath = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                       , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                                       , ConfigParameter._Object._UserAccessModeSetting, txtFormNo.Text.Trim, CInt(txtEmployee.Tag), objPendingLeave._Formunkid, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, True)

                                ElseIf ConfigParameter._Object._IsAutomaticIssueOnFinalApproval AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval = False Then

                                    strPath = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                       , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                                       , ConfigParameter._Object._UserAccessModeSetting, txtFormNo.Text.Trim, CInt(txtEmployee.Tag), objPendingLeave._Formunkid, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, False)

                                Else

                                    strPath = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                       , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                                       , ConfigParameter._Object._UserAccessModeSetting, txtFormNo.Text.Trim, CInt(txtEmployee.Tag), objPendingLeave._Formunkid, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, False)
                                End If
                            Else

                                strPath = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                       , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                                       , ConfigParameter._Object._UserAccessModeSetting, txtFormNo.Text.Trim, CInt(txtEmployee.Tag), objPendingLeave._Formunkid, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, True)

                            End If


                            'Pinkal (01-Apr-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.
                            'objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", mintClaimRequestMasterId)
                            objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date _
                                                                               , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP, 0 _
                                                                               , User._Object._Userunkid, "", mintClaimRequestMasterId, objLeaveForm._Formno)
                            'Pinkal (01-Apr-2019) -- End



                        Else

                            'Pinkal (25-May-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            'If CInt(cboStatus.SelectedValue) = 3 OrElse CInt(cboStatus.SelectedValue) = 4 Then
                            If xStatusId = 3 OrElse xStatusId = 4 Then
                                'Pinkal (25-May-2019) -- End

                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                'objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", "", "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, txtRemark.Text.Trim, mintClaimRequestMasterId)
                                objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date _
                                                                                    , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", "", "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid _
                                                                                    , txtRemark.Text.Trim, mintClaimRequestMasterId, objLeaveForm._Formno)
                                'Pinkal (01-Apr-2019) -- End


                            Else

                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                'objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", "", "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", mintClaimRequestMasterId)
                                objLeaveForm.SendMailToEmployee(CInt(txtEmployee.Tag), objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date _
                                                                                  , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", "", "", enLogin_Mode.DESKTOP _
                                                                                  , 0, User._Object._Userunkid, "", mintClaimRequestMasterId, objLeaveForm._Formno)
                                'Pinkal (01-Apr-2019) -- End


                            End If

                        End If

                        If ConfigParameter._Object._IsAutomaticIssueOnFinalApproval AndAlso ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Length > 0 AndAlso objPendingLeave._Statusunkid = 7 Then
                            objMaster = New clsMasterData
                            gobjEmailList = New List(Of clsEmailCollection)
                            Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                            Dim objEmployee As New clsEmployee_Master
                            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)

                            For Each sId As String In ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Split(CChar(","))
                                If sId.Trim = "" Then Continue For
                                Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(sId), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
                                If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                                    Dim mblnFlag As Boolean = False
                                    For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
                                        Dim drRow() As DataRow = Nothing
                                        Select Case CInt(AID)

                                            Case enAllocation.DEPARTMENT
                                                drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                                                If drRow.Length > 0 Then
                                                    mblnFlag = True
                                                Else
                                                    mblnFlag = False
                                                    Exit For
                                                End If

                                            Case enAllocation.JOBS
                                                drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
                                                If drRow.Length > 0 Then
                                                    mblnFlag = True
                                                Else
                                                    mblnFlag = False
                                                    Exit For
                                                End If

                                            Case enAllocation.CLASS_GROUP
                                                drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                                If drRow.Length > 0 Then
                                                    mblnFlag = True
                                                Else
                                                    mblnFlag = False
                                                    Exit For
                                                End If

                                            Case enAllocation.CLASSES
                                                drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
                                                If drRow.Length > 0 Then
                                                    mblnFlag = True
                                                Else
                                                    mblnFlag = False
                                                    Exit For
                                                End If

                                            Case Else
                                                mblnFlag = False
                                        End Select

                                    Next

                                    If mblnFlag Then
                                        Dim objLeaveIssueMaster As New clsleaveissue_master

                                        StrMessage = objLeaveIssueMaster.SetNotificationForIssuedUser(objUsr._Firstname & " " & objUsr._Lastname, txtEmployee.Text, objLeaveForm._Formno, objLeaveType._Leavename, objPendingLeave._Startdate.Date, objPendingLeave._Enddate.Date)

                                        'Pinkal (02--Feb-2018) -- Start
                                        'Bug - SUPPORT CCK 0001973 | Bug comes when user tries to approve leave.
                                        'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 13, "Issued Leave Application form notification"), StrMessage, "", 0, "", "", 0, 0, 0, ""))
                                        'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Language.getMessage(mstrModuleName, 13, "Issued Leave Application form notification"), StrMessage, "", 0, "", "", User._Object._Userunkid, 0, enModuleReference.Leave, Company._Object._Senderaddress))
                                        If sId.Trim.Length > 0 Then

                                            For Each uid As String In sId.Trim.Split(CChar(","))
                                                objUsr._Userunkid = CInt(uid)
                                                Dim objSendMail As New clsSendMail
                                                objSendMail._ToEmail = objUsr._Email
                                                objSendMail._Subject = Language.getMessage(mstrModuleName, 13, "Issued Leave Application form notification")
                                                objSendMail._Message = StrMessage
                                                objSendMail._Form_Name = ""
                                                objSendMail._LogEmployeeUnkid = -1
                                                objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                                                objSendMail._UserUnkid = objUsr._Userunkid
                                                objSendMail._SenderAddress = Company._Object._Senderaddress
                                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                                                Try
                                                    Dim intCUnkId As Integer = 0
                                                    Dim arr(1) As Object
                                                    arr(0) = Company._Object._Companyunkid
                                                    intCUnkId = Convert.ToInt32(arr(0))
                                                    If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                                                        Continue For
                                                    End If
                                                Catch ex As Exception
                                                End Try
                                            Next
                                        End If
                                        'Pinkal (02--Feb-2018) -- End
                                        objLeaveIssueMaster = Nothing
                                    End If

                                End If

                            Next

                        End If
                        'trd = New Thread(AddressOf Send_Notification)
                        'trd.IsBackground = True
                        'Pinkal (02--Feb-2018) -- Start
                        'Bug - SUPPORT CCK 0001973 | Bug comes when user tries to approve leave.
                        'Dim arr(1) As Object
                        'arr(0) = Company._Object._Companyunkid
                        'trd.Start(arr)
                        'trd.Start()
                        'Thread.Sleep(5000)
                        'Pinkal (02--Feb-2018) -- End

                    End If

                End If

CloseForm:      mblnCancel = False
                mintPendingLeaveUnkid = objPendingLeave._Pendingleavetranunkid
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboApprover.DataSource, DataTable)
            dtList = New DataView(dtList, "employeeunkid<>" & CInt(txtEmployee.Tag), "", DataViewRowState.CurrentRows).ToTable
            With cboApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTranHeads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHeads.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboAllowance.DataSource, DataTable)
            dtList = New DataView(dtList, "tranheadunkid <> 0", "", DataViewRowState.CurrentRows).ToTable
            With cboAllowance
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHeads_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private Sub objbtnSearchReliever_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReliever.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboReliever.DataSource, DataTable)
            With cboReliever
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReliever_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (01-Oct-2018) -- End


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            txtFormNo.BackColor = GUI.ColorComp
            txtEmployee.BackColor = GUI.ColorComp
            cboApprover.BackColor = GUI.ColorComp
            cboAllowance.BackColor = GUI.ColorComp

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'cboStatus.BackColor = GUI.ColorComp
            txtLeaveType.BackColor = GUI.ColorComp
            'Pinkal (25-May-2019) -- End

            txtDepartment.BackColor = GUI.ColorComp
            txtReportTo.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objPendingLeave._Pendingleavetranunkid > 0 Then

                If objPendingLeave._Approvaldate <> Nothing Then

                    If objPendingLeave._Approverunkid > 0 Then
                        cboApprover.SelectedValue = objPendingLeave._Approverunkid
                    End If
                    txtAllowance.Text = objPendingLeave._Amount.ToString
                    cboAllowance.SelectedValue = objPendingLeave._Transactionheadunkid

                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    'cboStatus.SelectedValue = objPendingLeave._Statusunkid
                    'Pinkal (25-May-2019) -- End

                    txtRemark.Text = objPendingLeave._Remark

                    RemoveHandler dtpStartdate.ValueChanged, AddressOf dtpStartdate_ValueChanged
                    RemoveHandler dtpEnddate.ValueChanged, AddressOf dtpStartdate_ValueChanged

                    If objPendingLeave._Startdate <> Nothing Then dtpStartdate.Value = objPendingLeave._Startdate
                    If objPendingLeave._Enddate <> Nothing Then dtpEnddate.Value = objPendingLeave._Enddate

                    AddHandler dtpStartdate.ValueChanged, AddressOf dtpStartdate_ValueChanged
                    AddHandler dtpEnddate.ValueChanged, AddressOf dtpStartdate_ValueChanged

                    dtpStartdate_ValueChanged(New Object(), New EventArgs())

                End If


                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If ConfigParameter._Object._SetRelieverAsMandatoryForApproval Then
                    cboReliever.SelectedValue = objPendingLeave._RelieverEmpId
                End If
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                Dim objLvType As New clsleavetype_master
                objLvType._Leavetypeunkid = mintLeavetypeunkid
                mblnIsLvTypePaid = objLvType._IsPaid
                mintLvformMinDays = objLvType._LVDaysMinLimit
                mintLvformMaxDays = objLvType._LVDaysMaxLimit
                'Pinkal (08-Oct-2018) -- End
                
                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = objLvType._ConsiderLeaveOnTnAPeriod
                'Pinkal (01-Jan-2019) -- End


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                mblnConsiderExpense = objLvType._ConsiderExpense
                lnkShowLeaveExpense.Visible = mblnConsiderExpense
                'Pinkal (03-May-2019) -- End


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'objLvType = Nothing
                If objLvType._IsStopApplyAcrossFYELC Then

                    If objLvType._IsShortLeave = False Then

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                            dtpStartdate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                            If dtpStartdate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                                dtpStartdate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                            End If

                            dtpEnddate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                            If dtpEnddate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                                dtpEnddate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                            End If

                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                            Dim objAccrueBalance As New clsleavebalance_tran
                            Dim dsList As DataSet = objAccrueBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                 , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & mintLeavetypeunkid & " AND lvleavebalance_tran.isvoid = 0", Nothing)
                            objAccrueBalance = Nothing

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                                Dim mdtLvELCBalanceStartDate As DateTime = CDate(dsList.Tables(0).Rows(0)("startdate")).Date
                                Dim mdtLvELCBalanceEndDate As DateTime = CDate(dsList.Tables(0).Rows(0)("enddate")).Date

                                If mdtLvELCBalanceStartDate <> Nothing AndAlso mdtLvELCBalanceEndDate <> Nothing Then

                                    dtpStartdate.MinDate = mdtLvELCBalanceStartDate.Date
                                    dtpStartdate.MaxDate = mdtLvELCBalanceEndDate.Date

                                    dtpEnddate.MinDate = mdtLvELCBalanceStartDate.Date
                                    dtpEnddate.MaxDate = mdtLvELCBalanceEndDate.Date

                                End If

                            End If
                            dsList.Clear()
                            dsList = Nothing
                        End If

            Else
                        dtpStartdate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        If dtpStartdate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                            dtpStartdate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                        End If

                        dtpEnddate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        If dtpEnddate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                            dtpEnddate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                        End If
                    End If
                End If


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                txtLeaveType.Text = objLvType._Leavename
                'Pinkal (25-May-2019) -- End


                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                If objLvType._DoNotApplyForFutureDates Then
                    dtpApplyDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStartdate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpEnddate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If
                'Pinkal (05-Jun-2020) -- End



                objLvType = Nothing
                'Pinkal (26-Feb-2019) -- End
            Else
                Dim objFrm As New clsleaveform
                objFrm._Formunkid = CInt(txtFormNo.Tag)
                dtpStartdate.MinDate = objFrm._Startdate
                dtpStartdate.MaxDate = objFrm._Returndate
                dtpStartdate.Value = objFrm._Startdate

                dtpEnddate.MinDate = objFrm._Startdate
                dtpEnddate.MaxDate = objFrm._Returndate
                dtpEnddate.Value = objFrm._Returndate

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    'Private Function SetValue() As Boolean
    Private Function SetValue(ByVal xStatusId As Integer) As Boolean
        'Pinkal (25-May-2019) -- End
        Try
            objPendingLeave._Formunkid = CInt(txtFormNo.Tag)
            objPendingLeave._Employeeunkid = CInt(txtEmployee.Tag)
            objPendingLeave._Approverunkid = CInt(cboApprover.SelectedValue)
            objPendingLeave._Startdate = dtpStartdate.Value
            objPendingLeave._Enddate = dtpEnddate.Value
            objPendingLeave._Approvaldate = dtpApplyDate.Value
            objPendingLeave._Transactionheadunkid = CInt(cboAllowance.SelectedValue)
            objPendingLeave._Amount = CDec(IIf(txtAllowance.Text <> "", txtAllowance.Text, 0))

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'objPendingLeave._Statusunkid = CInt(cboStatus.SelectedValue)
            objPendingLeave._Statusunkid = xStatusId
            'Pinkal (25-May-2019) -- End
            objPendingLeave._Remark = txtRemark.Text.Trim()
            objPendingLeave._PaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If ConfigParameter._Object._SetRelieverAsMandatoryForApproval Then
                objPendingLeave._RelieverEmpId = CInt(cboReliever.SelectedValue)
            End If
            'Pinkal (01-Oct-2018) -- End


            Dim objClaimRequestMst As New clsclaim_request_master
            mintClaimRequestMasterId = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(txtFormNo.Tag))
            objPendingLeave._ClaimRequestMasterId = mintClaimRequestMasterId
            objClaimRequestMst = Nothing


            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.

            If dtExpense Is Nothing AndAlso objPendingLeave._ClaimRequestMasterId > 0 Then

                Dim objExpApproverTran As New clsclaim_request_approval_tran
                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                    dtExpense = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                                         , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enExpenseType.EXP_LEAVE _
                                                                                                         , False, True, objPendingLeave._Approvertranunkid, "", objPendingLeave._ClaimRequestMasterId).Tables(0)
                Else
                    dtExpense = objExpApproverTran.GetApproverExpesneList("List", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                                        , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, enExpenseType.EXP_LEAVE _
                                                                                                        , False, True, -1, "", objPendingLeave._ClaimRequestMasterId).Tables(0)
                End If

                objExpApproverTran = Nothing


                Dim drRow() As DataRow = dtExpense.Select("AUD = '' ")
                If drRow.Length > 0 Then
                    For Each dr As DataRow In drRow
                        dr("AUD") = "U"
                    Next
                End If

            End If

            'Pinkal (10-Jan-2017) -- End

            If dtExpense IsNot Nothing AndAlso dtExpense.Columns.Contains("crapprovaltranunkid") Then
                dtExpense.Columns.Remove("crapprovaltranunkid")
            End If



            objPendingLeave._DtExpense = dtExpense
            objPendingLeave._Userunkid = User._Object._Userunkid
            objPendingLeave._DtFraction = dtFraction


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            If xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                    objPendingLeave._DayFraction = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD<> 'D'"))
                Else
                    objPendingLeave._DayFraction = 0
                End If

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboStatus.SelectedValue) = 3 Or CInt(cboStatus.SelectedValue) = 4 Then    'REJECT OR RESCHEDULE
            ElseIf xStatusId = 3 OrElse xStatusId = 4 Then    'REJECT OR RESCHEDULE
                'Pinkal (25-May-2019) -- End
                For Each dr As DataRow In objPendingLeave._DtFraction.Rows
                    dr("AUD") = "D"
                Next
                objPendingLeave._DtFraction.AcceptChanges()
                objPendingLeave._DayFraction = 0
            End If

            objPendingLeave._YearId = FinancialYear._Object._YearUnkid

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'objPendingLeave._VisiblelId = CInt(cboStatus.SelectedValue)
            objPendingLeave._VisiblelId = xStatusId
            'Pinkal (25-May-2019) -- End

            objPendingLeave._CompanyID = CInt(Company._Object._Companyunkid)
            objPendingLeave._LoginMode = enLogin_Mode.DESKTOP
            objPendingLeave._ArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If objClaimPendingMst IsNot Nothing AndAlso CInt(cboStatus.SelectedValue) = 1 Then
            If objClaimPendingMst IsNot Nothing AndAlso xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                objPendingLeave._dtClaimAttchment = mdtClaimAttachment
                objPendingLeave._ObjClaimRequestMst = objClaimPendingMst

                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strError As String = ""
                Dim mstrFolderName As String = ""

                If mdtClaimAttachment IsNot Nothing Then
                    strError = ""
                    mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.CLAIM_REQUEST).Tables(0).Rows(0)("Name").ToString
                    For Each dRow As DataRow In mdtClaimAttachment.Rows
                        If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                            Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            If blnIsIISInstalled Then
                                If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Return False
                                Else
                                    Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                    dRow("fileuniquename") = strFileName
                                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                        strPath += "/"
                                    End If
                                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                    dRow("filepath") = strPath
                                    dRow.AcceptChanges()
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                        Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                    End If
                                    File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                                    dRow("fileuniquename") = strFileName
                                    dRow("filepath") = strDocLocalPath
                                    dRow.AcceptChanges()
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                            End If

                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then

                            Dim strFileName As String = dRow("fileuniquename").ToString
                            If blnIsIISInstalled Then
                                'Gajanan [8-April-2019] -- Start
                                'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                    'Gajanan [8-April-2019] -- End
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Return False
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If File.Exists(strDocLocalPath) Then
                                        File.Delete(strDocLocalPath)
                                    End If
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                            End If
                        End If
                    Next
                End If

            End If
            'Pinkal (10-Jan-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFilter As DataTable = Nothing
        Try
            'FOR APPROVER
            Dim objEmployee As New clsEmployee_Master
            ' EVERY TIME INCLUDE ALL (ACTIVE/INACTIVE) EMPLOYEE SO DON"T GET CHANGED THE GETEMPLOYEELIST PARAMETER

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, True, "Approver", True)

            Dim objEmployeeApprover As New clsleaveapprover_master
            dtFilter = objEmployeeApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                               , User._Object._Userunkid, -1, mintApproverEmpID, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), , mblnIsExternalApprover) 'S.SANDEEP [30 JAN 2016] -- START {mblnIsExternalApprover} -- END

            cboApprover.ValueMember = "employeeunkid"
            cboApprover.DisplayMember = "employeename"
            cboApprover.DataSource = dtFilter



            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If ConfigParameter._Object._SetRelieverAsMandatoryForApproval Then
                FillReliver()
            End If
            'Pinkal (01-Oct-2018) -- End

            RemoveHandler cboAllowance.SelectedIndexChanged, AddressOf cboAllowance_SelectedIndexChanged

            'FOR ALLOWANCE
            dsFill = Nothing : dtFilter = Nothing
            Dim objTransactionhead As New clsTransactionHead
            dsFill = objTransactionhead.getComboList(FinancialYear._Object._DatabaseName, "Allowance", True, , , enTypeOf.Allowance)
            cboAllowance.ValueMember = "tranheadunkid"
            cboAllowance.DisplayMember = "Name"
            cboAllowance.DataSource = dsFill.Tables("Allowance")

            AddHandler cboAllowance.SelectedIndexChanged, AddressOf cboAllowance_SelectedIndexChanged



            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'FOR STATUS
            'dsFill = Nothing
            'Dim objStaus As New clsMasterData
            'Dim dtStatus As DataTable = Nothing
            'dsFill = objStaus.getLeaveStatusList("Status", True)
            'cboStatus.ValueMember = "statusunkid"
            'cboStatus.DisplayMember = "name"

            'If mintStausunkid = 7 Then
            '    dtStatus = New DataView(dsFill.Tables(0), "statusunkid not in (1,2)", "", DataViewRowState.CurrentRows).ToTable
            'ElseIf mintStausunkid <> 7 Then
            '    dtStatus = New DataView(dsFill.Tables(0), "statusunkid not in (6,7)", "", DataViewRowState.CurrentRows).ToTable
            'End If

            'cboStatus.DataSource = dtStatus
            'cboStatus.SelectedValue = mintStausunkid
            'Pinkal (25-May-2019) -- End


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.

            Dim xApproveLocation As Point = btnApprove.Location
            Dim xRejectLocation As Point = btnReject.Location
            Dim xReScheduleLocation As Point = btnReSchedule.Location

            dsFill = Nothing
            Dim objMaster As New clsMasterData
            dsFill = objMaster.getLeaveStatusList("Status", "", False, False, False)
            Dim dRow() As DataRow = dsFill.Tables(0).Select("statusunkid not in (" & ConfigParameter._Object._ApplicableLeaveStatus & ")", "")
            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If CInt(dRow(i)("statusunkid")) = 1 Then
                        btnApprove.Visible = False
                    ElseIf CInt(dRow(i)("statusunkid")) = 3 Then
                        btnReject.Visible = False
                        If btnApprove.Visible Then
                            If btnReSchedule.Visible = False Then
                                btnApprove.Location = xReScheduleLocation
                            Else
                                btnApprove.Location = xRejectLocation
                            End If
                        End If
                    ElseIf CInt(dRow(i)("statusunkid")) = 4 Then
                        btnReSchedule.Visible = False
                        If btnReject.Visible Then btnReject.Location = xReScheduleLocation
                        If btnApprove.Visible Then
                            If btnReject.Visible Then
                                btnReject.Location = xReScheduleLocation
                                btnApprove.Location = xRejectLocation
                            Else
                                btnApprove.Location = xReScheduleLocation
                            End If
                        End If

                    End If
                Next
            End If
            'Pinkal (03-Mar-2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployeeValue()
        Try
            'FOR DEPARTMENT
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)

            Dim objdept As New clsDepartment
            objdept._Departmentunkid = objEmployee._Departmentunkid
            txtDepartment.Text = objdept._Name


            'FOR REPORT TO

            Dim objJob As New clsJobs
            objJob._Jobunkid = objEmployee._Jobunkid

            objJob._Jobunkid = objJob._Report_Tounkid
            Dim objReportTo As New clsReportingToEmployee

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'objReportTo._EmployeeUnkid = CInt(txtEmployee.Tag)
            objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(txtEmployee.Tag)
            'Pinkal (18-Aug-2018) -- End

            Dim dt As DataTable = objReportTo._RDataTable
            Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
            If DefaultReportList.Count > 0 Then
                txtReportTo.Text = DefaultReportList(0).Item("ename").ToString
                'Pinkal (17-Nov-2017) -- Start
                'Enhancement -  issue # 0001563 Windsor Reporting To Issue on Leave Approval Process.
                'Else
                '    txtReportTo.Text = objJob._Job_Name
                'Pinkal (17-Nov-2017) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDepartment", mstrModuleName)
        End Try
    End Sub

    Private Sub GetBalanceInfo()
        Try
            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim dsList As DataSet = Nothing

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(mintLeavetypeunkid), txtEmployee.Tag.ToString)
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(mintLeavetypeunkid), txtEmployee.Tag.ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)
                'Pinkal (18-Nov-2016) -- End


            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                objLeaveAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLeaveAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(mintLeavetypeunkid), txtEmployee.Tag.ToString, Nothing, Nothing, True, True)
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CStr(mintLeavetypeunkid), txtEmployee.Tag.ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
                'Pinkal (18-Nov-2016) -- End
            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objlblLastYearAccruedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                objlblLastYearIssuedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                objlblAccruedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                objlblBalanceValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")


                'Pinkal (24-Aug-2016) -- Start
                'Enhancement - Removing 2 columns(Leave Accrue Upto Last Year AND Leave Issued Upto Last Year) From Whole Leave Module AS Per Rutta's Request For VFT
                'objlblIssuedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                objlblIssuedToDateValue.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                objlblLeaveBFvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                objlblTotalAdjustment.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                'Pinkal (24-Aug-2016) -- End


                'Pinkal (15-Apr-2019) -- Start
                'Enhancement - Working on Leave & CR Changes for NMB.

                'Pinkal (16-May-2019) -- Start
                'Bug [0003821] - Leave balance as on date displayed on leave approval screen not accurate.
                'objlblBalanceAsonDateValue.Text = CDec(CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount"))) - CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")))).ToString("#0.00")
                objlblBalanceAsonDateValue.Text = CDec(CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment"))) - CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
                'Pinkal (16-May-2019) -- End


                'Pinkal (15-Apr-2019) -- End


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    'Private Function GetPromptForBalance() As Boolean
    Private Function GetPromptForBalance(ByVal xStatusId As Integer) As Boolean
        'Pinkal (25-May-2019) -- End
        Dim dsList As DataSet = Nothing
        Dim objLeaveIssueTran As New clsleaveissue_Tran
        Dim objbalance As New clsleavebalance_tran
        Dim objlvForm As New clsleaveform
        Dim objLeaveType As New clsleavetype_master
        Dim mdblCurrentLeavedays As Decimal = 0
        Try


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            If xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                    mdblCurrentLeavedays = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD<> 'D'"))
                Else
                    mdblCurrentLeavedays = 0
                End If

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboStatus.SelectedValue) = 3 Or CInt(cboStatus.SelectedValue) = 4 Then    'REJECT OR RESCHEDULE
            ElseIf xStatusId = 3 OrElse xStatusId = 4 Then    'REJECT OR RESCHEDULE
                'Pinkal (25-May-2019) -- End

                For Each dr As DataRow In dtFraction.Rows
                    dr("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
                mdblCurrentLeavedays = 0
            End If

            objlvForm._Formunkid = CInt(txtFormNo.Tag)
            objLeaveType._Leavetypeunkid = objlvForm._Leavetypeunkid

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                     , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), False, False, False, "", Nothing)
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objbalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(txtEmployee.Tag), True, True, False, "", Nothing)
            End If


            Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid=" & objLeaveType._Leavetypeunkid)

            Dim mdtStartDate As Date = Nothing
            Dim mdtEndDate As Date = Nothing

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso drRow.Length > 0 Then
                mdtStartDate = CDate(drRow(0)("startdate")).Date
                mdtEndDate = CDate(drRow(0)("enddate")).Date
            End If

            Dim mdblIssue As Decimal = 0
            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid, FinancialYear._Object._YearUnkid, Nothing, mdtStartDate, mdtEndDate)
            Else
                mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid, FinancialYear._Object._YearUnkid)
            End If


            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then

                    'Pinkal (02-Jan-2018) -- Start
                    'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                    'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                    'Control to be put on application/not just issuing. employee to be warned and stopped.

                    'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    '    Return True
                    'Else
                    '    Return False
                    'End If
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.OkOnly + enMsgBoxStyle.Information, enMsgBoxStyle))
                    Return False
                    'Pinkal (02-Jan-2018) -- End

                End If


            ElseIf objLeaveType._DeductFromLeaveTypeunkid <= 0 AndAlso objLeaveType._IsPaid = False Then

                If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If

            End If

            If drRow.Length > 0 Then

                If CBool(drRow(0)("isshortleave")) = False Then

                    If CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance AndAlso CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then


                        'Pinkal (08-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                        If CBool(drRow(0)("ispaid")) Then
                            If CDec(drRow(0)("maxnegative_limit")) <> 0 AndAlso CDec(drRow(0)("maxnegative_limit")) > CDec(CDec(drRow(0)("accrue_amount")) - (CDec(drRow(0)("issue_amount")) + mdblCurrentLeavedays)) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Return False
                            End If
                        End If
                        'Pinkal (08-Oct-2018) -- End


                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Return True
                            Else
                                Return False
                            End If

                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False

                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then
                        Dim mdecAsonDateAccrue As Decimal = 0
                        Dim mdecAsonDateBalance As Decimal = 0

                        'Pinkal (06-Apr-2018) -- Start
                        'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
                        'objbalance.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                        '                                             , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid _
                        '                                             , dtpEnddate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, mdecAsonDateAccrue, mdecAsonDateBalance)

                        objbalance.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                                     , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), objLeaveType._Leavetypeunkid _
                                                                     , dtpEnddate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                                                                     , mdecAsonDateAccrue, mdecAsonDateBalance, FinancialYear._Object._YearUnkid)

                        'Pinkal (06-Apr-2018) -- End


                        If mdecAsonDateBalance < mdblCurrentLeavedays Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If

                    End If


                ElseIf CBool(drRow(0)("isshortleave")) Then

                    If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), enMsgBoxStyle.Information)
                        Return False
                    End If

                End If

            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPromptForBalance", mstrModuleName)
        End Try
    End Function

    'Pinkal (02--Feb-2018) -- Start
    'Bug - SUPPORT CCK 0001973 | Bug comes when user tries to approve leave.

    Private Sub Send_Notification(ByVal intCompanyUnkid As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        Dim intCUnkId As Integer = 0
                        If TypeOf intCompanyUnkid Is Integer Then
                            intCUnkId = Convert.ToInt32(intCompanyUnkid)
                        Else
                            Dim arr() As Object = CType(intCompanyUnkid, Object())
                            intCUnkId = Convert.ToInt32(arr(0))
                        End If
                        If objSendMail.SendMail(intCUnkId).ToString.Length > 0 Then
                            Continue For
                        End If
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    'Pinkal (02--Feb-2018) -- End

    Private Sub GetAsOnDateAccrue()
        Try
            Dim objLvAccrue As New clsleavebalance_tran
            Dim mdecAsonDateAccrue As Decimal = 0
            Dim mdecAsonDateBalance As Decimal = 0

            'Pinkal (06-Apr-2018) -- Start
            'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
            'objLvAccrue.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
            '                                             , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), mintLeavetypeunkid _
            '                                             , dtpEnddate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, mdecAsonDateAccrue, mdecAsonDateBalance)

            objLvAccrue.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                         , ConfigParameter._Object._LeaveBalanceSetting, CInt(txtEmployee.Tag), mintLeavetypeunkid _
                                                         , dtpEnddate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                                                         , mdecAsonDateAccrue, mdecAsonDateBalance, FinancialYear._Object._YearUnkid)

            'Pinkal (06-Apr-2018) -- End

            objAsonDateAccrue.Text = mdecAsonDateAccrue.ToString()
            objAsonDateBalance.Text = mdecAsonDateBalance.ToString()
            objLvAccrue = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAsOnDateAccrue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private Function IsValidExpense_Eligility() As Boolean
        Dim blnFlag As Boolean = False
        Try
            If txtFormNo.Tag IsNot Nothing AndAlso CInt(txtFormNo.Tag) > 0 Then
                Dim objlvform As New clsleaveform
                objlvform._Formunkid = CInt(txtFormNo.Tag)
                Dim objlvtype As New clsleavetype_master
                objlvtype._Leavetypeunkid = objlvform._Leavetypeunkid
                If objlvtype._EligibilityOnAfter_Expense > 0 Then
                    intEligibleExpenseDays = objlvtype._EligibilityOnAfter_Expense
                    If CDec(objNoofDays.Tag) >= objlvtype._EligibilityOnAfter_Expense Then
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidExpense_Eligility", mstrModuleName)
        Finally
        End Try
    End Function
    'Pinkal (29-Sep-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private Sub FillReliver()
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()).Date) = CInt(txtEmployee.Tag)


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.

            Dim mstrSearching As String = ""

            If objPendingLeave._RelieverEmpId > 0 Then
                mstrSearching = "AND (hremployee_master.employeeunkid <> " & CInt(txtEmployee.Tag) & " "
            Else
                mstrSearching = "AND hremployee_master.employeeunkid <> " & CInt(txtEmployee.Tag) & " "
            End If


            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If chkIncludeAllStaff.Checked = False Then

            If ConfigParameter._Object._RelieverAllocation.ToString().Length > 0 Then

                Dim arAllocations() As String = ConfigParameter._Object._RelieverAllocation.ToString().Split(CChar(","))
                If arAllocations.Length > 0 Then

                    For i As Integer = 0 To arAllocations.Length - 1

                        Select Case CType(arAllocations(i), enAllocation)

                            Case enAllocation.BRANCH

            If objEmployee._Stationunkid > 0 Then
                mstrSearching &= "AND T.stationunkid = " & objEmployee._Stationunkid & " "
            End If

                            Case enAllocation.DEPARTMENT_GROUP

            If objEmployee._Deptgroupunkid > 0 Then
                mstrSearching &= "AND T.deptgroupunkid = " & objEmployee._Deptgroupunkid & " "
            End If

                            Case enAllocation.DEPARTMENT

            If objEmployee._Departmentunkid > 0 Then
                mstrSearching &= "AND T.departmentunkid = " & objEmployee._Departmentunkid & " "
            End If

                            Case enAllocation.SECTION_GROUP

            If objEmployee._Sectiongroupunkid > 0 Then
                mstrSearching &= "AND T.sectiongroupunkid = " & objEmployee._Sectiongroupunkid & " "
            End If

                            Case enAllocation.SECTION

            If objEmployee._Sectionunkid > 0 Then
                mstrSearching &= "AND T.sectionunkid = " & objEmployee._Sectionunkid & " "
            End If

                            Case enAllocation.UNIT_GROUP

            If objEmployee._Unitgroupunkid > 0 Then
                mstrSearching &= "AND T.unitgroupunkid = " & objEmployee._Unitgroupunkid & " "
            End If

                            Case enAllocation.UNIT

            If objEmployee._Unitunkid > 0 Then
                mstrSearching &= "AND T.unitunkid = " & objEmployee._Unitunkid & " "
            End If

                            Case enAllocation.TEAM

            If objEmployee._Teamunkid > 0 Then
                mstrSearching &= "AND T.teamunkid = " & objEmployee._Teamunkid & " "
            End If

                            Case enAllocation.JOB_GROUP

            If objEmployee._Jobgroupunkid > 0 Then
                mstrSearching &= "AND J.jobgroupunkid = " & objEmployee._Jobgroupunkid & " "
            End If

                            Case enAllocation.JOBS

            If objEmployee._Jobunkid > 0 Then
                mstrSearching &= "AND J.jobunkid = " & objEmployee._Jobunkid & " "
            End If

                            Case enAllocation.CLASS_GROUP

            If objEmployee._Classgroupunkid > 0 Then
                mstrSearching &= "AND T.classgroupunkid = " & objEmployee._Classgroupunkid & " "
            End If

                            Case enAllocation.CLASSES

            If objEmployee._Classunkid > 0 Then
                mstrSearching &= "AND T.classunkid = " & objEmployee._Classunkid & " "
            End If

                            Case enAllocation.COST_CENTER

            If objEmployee._Costcenterunkid > 0 Then
                mstrSearching &= "AND C.costcenterunkid = " & objEmployee._Costcenterunkid & " "
            End If

                        End Select

                    Next

                End If

            End If

            End If

            If objPendingLeave._RelieverEmpId > 0 Then
                mstrSearching &= ") OR hremployee_master.employeeunkid in (" & objPendingLeave._RelieverEmpId & ") "
            End If


            'Pinkal (03-May-2019) -- End


            'If objEmployee._Gradegroupunkid > 0 Then
            '    mstrSearching &= "AND G.gradegroupunkid = " & objEmployee._Gradegroupunkid & " "
            'End If

            'If objEmployee._Gradeunkid > 0 Then
            '    mstrSearching &= "AND G.gradeunkid = " & objEmployee._Gradeunkid & " "
            'End If

            'If objEmployee._Gradelevelunkid > 0 Then
            '    mstrSearching &= "AND G.gradelevelunkid = " & objEmployee._Gradelevelunkid & " "
            'End If

            'Pinkal (13-Mar-2019) -- End


            If mstrSearching.Trim.Length > 0 Then
                mstrSearching = mstrSearching.Substring(3)
            End If

            Dim dsEmpList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Reliver", True, 0, , , , , , , , , , , , , , mstrSearching, , False)

            With cboReliever
                .ValueMember = "employeeunkid"
                .DisplayMember = "EmpCodeName"
                .DataSource = dsEmpList.Tables(0)
                .SelectedIndex = 0
            End With

            objEmployee = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReliver", mstrModuleName)
        End Try
    End Sub

    'Pinkal (01-Oct-2018) -- End


#End Region

#Region "LinkLabel Event"

    Private Sub lnkShowLeaveForm_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowLeaveForm.LinkClicked
        Try
            Dim objFrm As New frmLeaveForm_AddEdit
            objFrm._IsFromLeaveProcess = True
            objFrm.cboEmployee.Enabled = False
            objFrm.cboLeaveType.Enabled = False
            objFrm.btnSave.Visible = False
            objFrm.lnkLeaveDayCount.Visible = False
            objFrm.lvLeaveExpense.Visible = False
            objFrm.objbtnAddLeaveType.Visible = False
            objFrm.objbtnSearchEmployee.Visible = False
            objFrm.lnkCopyEmpAddress.Visible = False
            objFrm.dtpApplyDate.Enabled = False
            objFrm.dtpStartDate.Enabled = False
            objFrm.dtpEndDate.Enabled = False
            objFrm.txtFormNo.ReadOnly = True
            objFrm.txtLeaveAddress.ReadOnly = True
            objFrm.txtLeaveReason.ReadOnly = True

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            objFrm.lnkScanDocuements.Visible = False
            'Pinkal (06-Jan-2016) -- End

            objFrm.displayDialog(CInt(txtFormNo.Tag), enAction.EDIT_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowLeaveForm_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkShowLeaveExpense_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowLeaveExpense.LinkClicked
        Try
            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            If IsValidExpense_Eligility() = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (29-Sep-2017) -- End

            Dim objClaimRequestMst As New clsclaim_request_master
            mintClaimRequestMasterId = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(txtFormNo.Tag))

            Dim objFrmExpense As New frmExpenseApproval
            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                objClaimRequestMst._Crmasterunkid = mintClaimRequestMasterId

                Dim objClaimProcess As New clsclaim_process_Tran
                Dim dsClaimProcess As DataSet = objClaimProcess.GetList("Process", True, 1, "cmclaim_process_tran.crmasterunkid = " & mintClaimRequestMasterId)
                If dsClaimProcess IsNot Nothing AndAlso dsClaimProcess.Tables(0).Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already posted to Payroll."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                dsClaimProcess.Clear()
                dsClaimProcess = Nothing
                objClaimProcess = Nothing

                If objClaimRequestMst._Statusunkid = 1 OrElse objClaimRequestMst._Statusunkid = 2 Then
                    Dim objleaveapprover As New clsleaveapprover_master
                    Dim objApproveLevel As New clsapproverlevel_master
                    objleaveapprover._Approverunkid = objPendingLeave._Approvertranunkid
                    objApproveLevel._Levelunkid = objleaveapprover._Levelunkid


                    Dim objExpenseApproval As New clsclaim_request_approval_tran
                    Dim dsApprovalList As DataSet = objExpenseApproval.GetApproverExpesneList("Approval", False, ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                                                                                                                                        , FinancialYear._Object._DatabaseName, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                                                                        , enExpenseType.EXP_LEAVE, False, True, -1, "", mintClaimRequestMasterId)

                    Dim drRow() As DataRow = dsApprovalList.Tables(0).Select("statusunkid = 1 AND crpriority = " & objApproveLevel._Priority)
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already approved."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    dsApprovalList.Clear()
                    dsApprovalList = Nothing
                    objExpenseApproval = Nothing
                    If dtExpense IsNot Nothing AndAlso dtExpense.Rows.Count > 0 Then
                        objFrmExpense._dtExpense = dtExpense
                    End If


                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                    'objFrmExpense.displayDialog(mintClaimRequestMasterId, objPendingLeave._Approvertranunkid, objPendingLeave._Approverunkid, objApproveLevel._Levelunkid, True)
                    objFrmExpense.displayDialog(mintClaimRequestMasterId, objPendingLeave._Approvertranunkid, objPendingLeave._Approverunkid, objApproveLevel._Levelunkid, mblnIsExternalApprover, True)
                    'Pinkal (01-Mar-2016) -- End

                    dtExpense = objFrmExpense._dtExpense

                Else

                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                    'dtExpense = Nothing
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "There is no Leave expense data for this leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    'Exit Sub

                    Dim objFrmClaimMst As New frmClaims_RequestAddEdit
                    objClaimPendingMst = New clsclaim_request_master

                    objFrmClaimMst._EmployeeId = CInt(txtEmployee.Tag)
                    objFrmClaimMst._LeaveTypeId = mintLeavetypeunkid
                    objFrmClaimMst._ExpenseCategoryID = enExpenseType.EXP_LEAVE
                    objFrmClaimMst._IsFromLeave = True
                    objFrmClaimMst._Leavedate = dtpApplyDate.Value.Date
                    objFrmClaimMst._LeaveFormID = CInt(txtFormNo.Tag)
                    objFrmClaimMst._dtExpense = dtExpense
                    objFrmClaimMst._dtAttchment = mdtClaimAttachment

                    objFrmClaimMst.displayDialog(-1, enAction.ADD_ONE, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, objClaimPendingMst, True)

                    'Dim objapprover As New clsleaveapprover_master
                    'Dim dtApprover As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                                                     , -1, CInt(txtEmployee.Tag), -1, CInt(IIf(objClaimPendingMst._LeaveApproverForLeaveType, mintLeavetypeunkid, -1)) _
                    '                                                                     , objClaimPendingMst._LeaveApproverForLeaveType.ToString(), Nothing)

                    'objapprover = Nothing

                    Dim dtApprover As DataTable = objPendingLeave.GetEmployeeApproverListWithPriority(CInt(txtEmployee.Tag), CInt(txtFormNo.Tag), Nothing).Tables(0)
                    objClaimPendingMst._dtLeaveApprover = New DataView(dtApprover, "priority >= " & mintPriority, "priority", DataViewRowState.CurrentRows).ToTable()
                    mdtClaimAttachment = objFrmClaimMst._dtAttchment
                    dtExpense = objFrmClaimMst._dtExpense

                    'Pinkal (10-Jan-2017) -- End


                End If

            Else

                Dim objClaimApprover As New clsclaim_request_approval_tran
                Dim mdtTable As DataTable = objClaimApprover.GetEmployeeLastApprovedExpenseDetail(mintClaimRequestMasterId)
                If mdtTable IsNot Nothing AndAlso mdtTable.Rows.Count > 0 Then
                    If objFrmExpense.displayDialog(mintClaimRequestMasterId, CInt(mdtTable.Rows(0)("crapproverunkid")), CInt(mdtTable.Rows(0)("approveremployeeunkid")), CInt(mdtTable.Rows(0)("crlevelunkid")), mblnIsExternalApprover, True) Then
                        dtExpense = objFrmExpense._dtExpense
                    End If
                Else
                    dtExpense = Nothing
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "There is no Leave expense data for this leave form."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            End If

            objFrmExpense = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowLeaveExpense_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkLeaveDayCount_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkLeaveDayCount.LinkClicked


        If dtpStartdate.Value.Date > dtpEnddate.Value.Date Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Proper Start Date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            dtpStartdate.Select()
            Exit Sub
        End If

        Dim objfrmLeaveFraction As New frmLeaveDay_fraction
        Dim objLeaveFraction As New clsleaveday_fraction
        objfrmLeaveFraction._Startdate = dtpStartdate.Value.Date
        objfrmLeaveFraction._Enddate = dtpEnddate.Value.Date
        objfrmLeaveFraction._EmployeeId = CInt(txtEmployee.Tag)
        objfrmLeaveFraction._LeaveTypeId = mintLeavetypeunkid


        If objPendingLeave._Statusunkid = 1 Then
            objfrmLeaveFraction._ApproverId = objPendingLeave._Approverunkid
        Else
            Dim objleaveapprover As New clsleaveapprover_master
            Dim objApproveLevel As New clsapproverlevel_master
            objleaveapprover._Approverunkid = objPendingLeave._Approvertranunkid
            objApproveLevel._Levelunkid = objleaveapprover._Levelunkid
            Dim mstrSearch As String = "lvpendingleave_tran.approverunkid <> " & objPendingLeave._Approverunkid & " AND  lvpendingleave_tran.formunkid = " & objPendingLeave._Formunkid

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim dsPedingList As DataSet = objPendingLeave.GetList("PendingProcess", True, "", "", "", -1, mstrSearch)
            Dim dsPedingList As DataSet = objPendingLeave.GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                , True, mstrSearch)
            'Pinkal (24-Aug-2015) -- End


            Dim drRow() As DataRow = dsPedingList.Tables(0).Select("priority < " & objApproveLevel._Priority)
            If drRow.Length > 0 Then
                Dim mintPriority As Integer = CInt(dsPedingList.Tables(0).Compute("Max(priority)", "statusunkid=1"))
                For i As Integer = 0 To drRow.Length - 1
                    If CInt(drRow(i)("priority")) = mintPriority AndAlso CInt(drRow(i)("statusunkid")) = 1 Then
                        objfrmLeaveFraction._ApproverId = CInt(drRow(i)("approverunkid"))
                        Exit For
                    End If
                Next
            Else
                objfrmLeaveFraction._ApproverId = -1
            End If
        End If

        Dim dtTable As DataTable = New DataView(dtFraction, "AUD = 'D'", "", DataViewRowState.CurrentRows).ToTable

        objfrmLeaveFraction.mblnFromProcessPending = True
        If objfrmLeaveFraction.displayDialog(-1, CInt(txtFormNo.Tag), enAction.EDIT_ONE) Then
            dtFraction = objfrmLeaveFraction._dtFraction
            For Each dr As DataRow In dtFraction.Rows
                If objfrmLeaveFraction._ApproverId <> objPendingLeave._Approverunkid AndAlso dr("AUD").ToString().Trim <> "D" Then
                    dr("AUD") = "A"
                Else
                    If dr("AUD").ToString().Trim = "" AndAlso dr.RowState = DataRowState.Modified Then
                        dr("AUD") = "U"
                    End If

                End If
            Next

            ' START IMPORT DELETED ALL ROWS FROM DTTABLE TO DTFRACTION WHEN OLD ROWS IS ALREADY IN DATABASE TO VOID
            For Each dr As DataRow In dtTable.Rows
                Dim drDeleteRow() As DataRow = dtFraction.Select("leavedate ='" & CDate(dr("leavedate")) & "' AND AUD = 'D' AND dayfraction = " & CDec(dr("dayfraction")))
                If drDeleteRow.Length <= 0 Then
                    dtFraction.ImportRow(dr)
                End If
            Next
            ' END IMPORT DELETED ALL ROWS FROM DTTABLE TO DTFRACTION WHEN OLD ROWS IS ALREADY IN DATABASE TO VOID

            dtFraction.AcceptChanges()


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            Dim mdecDays As Decimal = 0
            If dtFraction IsNot Nothing And dtFraction.Rows.Count > 0 Then
                If IsDBNull(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & objfrmLeaveFraction._ApproverId)) = False Then
                    mdecDays = CDec(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & objfrmLeaveFraction._ApproverId))
                End If
            End If

            objNoofDays.Text = Language.getMessage(mstrModuleName, 12, "Total Days : ") & mdecDays
            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            objNoofDays.Tag = mdecDays
            'Pinkal (29-Sep-2017) -- End

            'Pinkal (06-Jan-2016) -- End

        End If

    End Sub

    Private Sub lnkShowScanDocuementList_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowScanDocuementList.LinkClicked
        Try
            Dim objForm As New frmPreviewDocuments
            If User._Object._Isrighttoleft = True Then
                objForm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objForm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objForm)
            End If


            Dim mstrTranId As String = ""
            Dim objScanAttach As New clsScan_Attach_Documents
            Dim dtTable As DataTable = objScanAttach.GetAttachmentTranunkIds(CInt(txtEmployee.Tag) _
                                                                               , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module _
                                                                               , CInt(txtFormNo.Tag))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTable.Rows
                    mstrTranId &= CInt(dRow("scanattachtranunkid")) & ","
                Next
            End If

            If mstrTranId.Trim.Length > 0 Then
                mstrTranId = mstrTranId.Trim.Substring(0, mstrTranId.Trim.Length - 1)
            End If

            objForm.displayDialog(mstrTranId.ToString)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowScanDocuementList_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Private Sub dtpStartdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartdate.ValueChanged, dtpEnddate.ValueChanged
        Try
            Dim objfrmLeaveFraction As New frmLeaveDay_fraction
            Dim objLeaveFraction As New clsleaveday_fraction
            dtFraction = objLeaveFraction.GetList("List", objPendingLeave._Formunkid, True, -1, mintApproverEmpID).Tables(0)

            If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count <= 0 Then
                Dim objleaveapprover As New clsleaveapprover_master
                Dim objApproveLevel As New clsapproverlevel_master
                objleaveapprover._Approverunkid = objPendingLeave._Approvertranunkid
                objApproveLevel._Levelunkid = objleaveapprover._Levelunkid
                Dim mstrSearch As String = "lvpendingleave_tran.approverunkid <> " & objPendingLeave._Approverunkid & " AND  lvpendingleave_tran.formunkid = " & objPendingLeave._Formunkid

                Dim dsPedingList As DataSet = objPendingLeave.GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                     , True, mstrSearch)

                Dim dRow() As DataRow = dsPedingList.Tables(0).Select("priority < " & objApproveLevel._Priority)
                If dRow.Length > 0 Then
                    Dim mintPriority As Integer = CInt(dsPedingList.Tables(0).Compute("Max(priority)", "statusunkid=1"))
                    For i As Integer = 0 To dRow.Length - 1
                        If CInt(dRow(i)("priority")) = mintPriority AndAlso CInt(dRow(i)("statusunkid")) = 1 Then
                            objfrmLeaveFraction._ApproverId = CInt(dRow(i)("approverunkid"))
                            Exit For
                        End If
                    Next
                End If
            Else
                objfrmLeaveFraction._ApproverId = mintApproverEmpID
            End If

            dtFraction = objLeaveFraction.GetList("List", objPendingLeave._Formunkid, True, -1, objfrmLeaveFraction._ApproverId).Tables(0)

            If objPendingLeave._Startdate.Date <> dtpStartdate.Value.Date Or objPendingLeave._Enddate.Date <> dtpEnddate.Value.Date Then
                Dim objFrmFraction As New frmLeaveDay_fraction
                objFrmFraction.mblnFromProcessPending = True
                objFrmFraction._Startdate = dtpStartdate.Value.Date
                objFrmFraction._Enddate = dtpEnddate.Value.Date
                objFrmFraction._EmployeeId = CInt(txtEmployee.Tag)
                objFrmFraction._LeaveTypeId = mintLeavetypeunkid
                objFrmFraction._dtFraction = dtFraction
                objFrmFraction.mintFormunkid = objPendingLeave._Formunkid
                objFrmFraction.GenerateDays()
            End If

            Dim drRow As DataRow() = dtFraction.Select("leavedate < '" & dtpStartdate.Value.Date & "' AND AUD <> 'D'")

            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If

            For Each dr As DataRow In dtFraction.Rows
                If objfrmLeaveFraction._ApproverId <> objPendingLeave._Approverunkid AndAlso dr("AUD").ToString().Trim <> "D" Then
                    dr("AUD") = "A"
                Else
                    If dr("AUD").ToString().Trim = "" AndAlso dr.RowState = DataRowState.Modified Then
                        dr("AUD") = "U"
                    End If

                End If
                dr("Formunkid") = objPendingLeave._Formunkid
                dr("approverunkid") = objfrmLeaveFraction._ApproverId

            Next


            Dim mdecDays As Decimal = 0
            If dtFraction IsNot Nothing And dtFraction.Rows.Count > 0 Then
                If IsDBNull(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & objfrmLeaveFraction._ApproverId)) = False Then
                    mdecDays = CDec(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & objfrmLeaveFraction._ApproverId))
                End If
            End If

            objNoofDays.Text = Language.getMessage(mstrModuleName, 12, "Total Days : ") & mdecDays
            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            objNoofDays.Tag = mdecDays
            'Pinkal (29-Sep-2017) -- End


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            GetAsOnDateAccrue()
            'Pinkal (18-Nov-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartdate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.

#Region "Checkbox Events"

    Private Sub chkIncludeAllStaff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeAllStaff.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor()
            FillReliver()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIncludeAllStaff_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

    'Pinkal (03-May-2019) -- End


    'Pinkal (05-Jun-2020) -- Start
    'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
    Private Sub ExtraWebMessage()
        Language.getMessage(mstrModuleName, 28, "Please select proper")
        Language.getMessage(mstrModuleName, 29, "should be between")
        Language.getMessage(mstrModuleName, 30, "Sorry, As per company setting you are not allow to set leave application start date to previous date.")
        Language.getMessage(mstrModuleName, 31, "Sorry, As per company setting you are not allow to set leave application end date to previous date.")
        Language.getMessage(mstrModuleName, 32, "Sorry, you cannot approve/reject this leave application.Reason:As Per setting configured on leave type master you can not approve/reject for future date(s).")
    End Sub
    'Pinkal (05-Jun-2020) -- End
 
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbProcessLeaveInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbProcessLeaveInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnReSchedule.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReSchedule.GradientForeColor = GUI._ButttonFontColor

			Me.btnReject.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReject.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbProcessLeaveInfo.Text = Language._Object.getCaption(Me.gbProcessLeaveInfo.Name, Me.gbProcessLeaveInfo.Text)
            Me.lnLeaveBalance.Text = Language._Object.getCaption(Me.lnLeaveBalance.Name, Me.lnLeaveBalance.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblToDateAccrued.Text = Language._Object.getCaption(Me.lblToDateAccrued.Name, Me.lblToDateAccrued.Text)
            Me.lblTotalIssuedToDate.Text = Language._Object.getCaption(Me.lblTotalIssuedToDate.Name, Me.lblTotalIssuedToDate.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
            Me.lblReportTo.Text = Language._Object.getCaption(Me.lblReportTo.Name, Me.lblReportTo.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblApprovalDate.Text = Language._Object.getCaption(Me.lblApprovalDate.Name, Me.lblApprovalDate.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblAs.Text = Language._Object.getCaption(Me.lblAs.Name, Me.lblAs.Text)
            Me.lblAllowance.Text = Language._Object.getCaption(Me.lblAllowance.Name, Me.lblAllowance.Text)
            Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Language._Object.getCaption(Me.lblStartdate.Name, Me.lblStartdate.Text)
            Me.lnkShowLeaveForm.Text = Language._Object.getCaption(Me.lnkShowLeaveForm.Name, Me.lnkShowLeaveForm.Text)
            Me.lnkShowLeaveExpense.Text = Language._Object.getCaption(Me.lnkShowLeaveExpense.Name, Me.lnkShowLeaveExpense.Text)
            Me.lnkLeaveDayCount.Text = Language._Object.getCaption(Me.lnkLeaveDayCount.Name, Me.lnkLeaveDayCount.Text)
            Me.lnkShowScanDocuementList.Text = Language._Object.getCaption(Me.lnkShowScanDocuementList.Name, Me.lnkShowScanDocuementList.Text)
            Me.lblLastYearAccrued.Text = Language._Object.getCaption(Me.lblLastYearAccrued.Name, Me.lblLastYearAccrued.Text)
            Me.lblLastYearIssued.Text = Language._Object.getCaption(Me.lblLastYearIssued.Name, Me.lblLastYearIssued.Text)
            Me.LblTotalAdjustment.Text = Language._Object.getCaption(Me.LblTotalAdjustment.Name, Me.LblTotalAdjustment.Text)
            Me.LblLeaveBF.Text = Language._Object.getCaption(Me.LblLeaveBF.Name, Me.LblLeaveBF.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.LblAsonDateAccrue.Text = Language._Object.getCaption(Me.LblAsonDateAccrue.Name, Me.LblAsonDateAccrue.Text)
            Me.lblAsonDateBalance.Text = Language._Object.getCaption(Me.lblAsonDateBalance.Name, Me.lblAsonDateBalance.Text)
            Me.LblReliever.Text = Language._Object.getCaption(Me.LblReliever.Name, Me.LblReliever.Text)
	    Me.LblBalanceAsonDate.Text = Language._Object.getCaption(Me.LblBalanceAsonDate.Name, Me.LblBalanceAsonDate.Text)
			Me.chkIncludeAllStaff.Text = Language._Object.getCaption(Me.chkIncludeAllStaff.Name, Me.chkIncludeAllStaff.Text)
			Me.btnReSchedule.Text = Language._Object.getCaption(Me.btnReSchedule.Name, Me.btnReSchedule.Text)
			Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
			Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Approver is compulsory information.Please Select Approver.")
            Language.setMessage(mstrModuleName, 3, "Remark cannot be blank. Remark is required information.")
            Language.setMessage(mstrModuleName, 4, "Please Select Proper Start Date.")
            Language.setMessage(mstrModuleName, 5, "You can't approve this leave for this employee.Reason: Leave dates are falling in a period which is already closed.")
            Language.setMessage(mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling.")
            Language.setMessage(mstrModuleName, 7, "You can't change")
            Language.setMessage(mstrModuleName, 8, "status to Pending status.")
            Language.setMessage(mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?")
            Language.setMessage(mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form.")
            Language.setMessage(mstrModuleName, 11, "You cannot approve this leave form.Reason : As these days are falling in holiday(s) and weekend.")
            Language.setMessage(mstrModuleName, 12, "Total Days :")
            Language.setMessage(mstrModuleName, 13, "Issued Leave Application form notification")
            Language.setMessage(mstrModuleName, 14, "Sending Email(s) process is in progress from other module. Please wait for some time.")
            Language.setMessage(mstrModuleName, 15, "There is no Leave expense data for this leave form.")
            Language.setMessage(mstrModuleName, 17, "You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already posted to Payroll.")
            Language.setMessage(mstrModuleName, 18, "You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already approved.")
            Language.setMessage(mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?")
            Language.setMessage(mstrModuleName, 20, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type.")
            Language.setMessage(mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is")
            Language.setMessage(mstrModuleName, 22, "Reliever is compulsory information.Please select reliever to approve/reject the leave application.")
            Language.setMessage(mstrModuleName, 23, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum")
 	    Language.setMessage(mstrModuleName, 24, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum")
 	    Language.setMessage(mstrModuleName, 25, "day(s) to be applied for this leave type.")
	    Language.setMessage(mstrModuleName, 26, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type.")
			Language.setMessage(mstrModuleName, 27, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 28, "Please select proper")
			Language.setMessage(mstrModuleName, 29, "should be between")
			Language.setMessage(mstrModuleName, 30, "Sorry, As per company setting you are not allow to set leave application start date to previous date.")
			Language.setMessage(mstrModuleName, 31, "Sorry, As per company setting you are not allow to set leave application end date to previous date.")
			Language.setMessage(mstrModuleName, 32, "Sorry, you cannot approve/reject this leave application.Reason:As Per setting configured on leave type master you can not approve/reject for future date(s).")
            Language.setMessage(mstrModuleName, 36, "Document path does not exist.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class