﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 8

Public Class frmProcessLeaveList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmProcessLeaveList"
    Private objpending As clspendingleave_Tran
    Dim objMapping As clsapprover_Usermapping
    Dim objApprover As clsleaveapprover_master
    Dim dsPedingList As DataSet = Nothing
    Private mstrAdvanceFilter As String = ""

    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    Private mstrEmployeeIDs As String = ""
    'Pinkal (03-Nov-2014) -- End

#End Region

#Region " Private Methods "

    Private Sub FillList()

        Dim strSearching As String = ""
        Dim dtLeaveForm As New DataTable
        Try

            If User._Object.Privilege._AllowToViewLeaveProcessList = True Then

                Me.Cursor = Cursors.WaitCursor

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching = "AND lvpendingleave_tran.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLeaveType.SelectedValue) > 0 Then

                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'strSearching = "AND lvleaveform.leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " "
                    strSearching &= "AND lvleaveform.leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " "
                    'Pinkal (15-Mar-2019) -- End
                End If

                If txtFormNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND lvleaveform.FormNo like '%" & txtFormNo.Text.Trim & "%' "
                End If

                If dtpApplyDate.Checked Then
                    strSearching &= "AND lvleaveform.applydate='" & eZeeDate.convertDate(dtpApplyDate.Value) & "' "
                End If

                If dtpStartDate.Checked Then
                    strSearching &= "AND lvpendingleave_tran.startdate >='" & eZeeDate.convertDate(dtpStartDate.Value) & "' "
                End If

                If dtpEndDate.Checked Then
                    strSearching &= "AND lvpendingleave_tran.enddate <='" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
                End If

                strSearching &= "AND lvpendingleave_tran.visibleid <> -1 "


                If CInt(cboStatus.SelectedValue) > 0 Then
                    If CInt(cboStatus.SelectedValue) = 6 Then
                        strSearching &= "AND lvpendingleave_tran.iscancelform = 1"
                    Else
                        strSearching &= "AND lvpendingleave_tran.iscancelform = 0 AND lvleaveform.statusunkid = " & CInt(cboStatus.SelectedValue)
                    End If
                End If


                If mstrAdvanceFilter.Length > 0 Then
                    strSearching &= "AND " & mstrAdvanceFilter
                End If

                If strSearching.Trim.Length > 0 Then
                    strSearching = strSearching.Trim.Substring(3)
                End If


                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'dsPedingList = objpending.GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                                                  , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, strSearching)

                dsPedingList = objpending.GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                  , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, strSearching _
                                                                  , Nothing, chkIncludeClosedFYTransactions.Checked, CInt(cboEmployee.SelectedValue))
                'Pinkal (15-Mar-2019) -- End

                strSearching = ""

                'START FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

                If dsPedingList.Tables("PendingProcess").Rows.Count > 0 Then
                    Dim drRow As DataRow() = dsPedingList.Tables("PendingProcess").Select("mapuserunkid = " & CInt(User._Object._Userunkid), "priority desc")
                    If drRow.Length > 0 Then
                        Dim objmapuser As New clsapprover_Usermapping
                        objmapuser.GetData(enUserType.Approver, CInt(drRow(0)("leaveapproverunkid")), , )
                        objApprover._Approverunkid = objmapuser._Approverunkid
                    End If
                End If

                Dim objApproverLevel As New clsapproverlevel_master
                objApproverLevel._Levelunkid = objApprover._Levelunkid

                'END FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

                If chkMyApprovals.Checked AndAlso objApproverLevel._Levelunkid > 0 Then
                    strSearching &= "AND mapuserunkid = " & CInt(User._Object._Userunkid)
                Else
                    RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
                    chkMyApprovals.Checked = False
                    AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If chkIncludeClosedFYTransactions.Checked = False Then
                    If mstrEmployeeIDs.Trim.Length > 0 Then
                        strSearching &= "AND employeeunkid  in (" & mstrEmployeeIDs & ")"
                    End If
                    End If
                    'Pinkal (15-Mar-2019) -- End
                End If

                If objApproverLevel._Levelunkid > 0 Then
                    If strSearching.Length > 0 Then
                        strSearching = strSearching.Substring(3)
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), strSearching, "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "", "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    End If

                Else
                    If strSearching.Length > 0 Then
                        strSearching = strSearching.Substring(3)
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), strSearching, "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "", "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                RemoveHandler lvLeaveProcessList.SelectedIndexChanged, AddressOf lvLeaveProcessList_SelectedIndexChanged

                Dim mstrStaus As String = ""
                Dim mintLeaveFormunkid As Integer = 0
                Dim dList As DataTable = Nothing

                Dim lvItem As ListViewItem

                lvLeaveProcessList.Items.Clear()
                For Each drRow As DataRow In dtLeaveForm.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("formno").ToString()
                    lvItem.Tag = drRow("pendingleavetranunkid")
                    lvItem.SubItems.Add(drRow("employeecode").ToString)
                    lvItem.SubItems.Add(drRow("employeename").ToString)
                    lvItem.SubItems.Add(drRow("approvername").ToString & " - " & drRow("levelname").ToString)
                    lvItem.SubItems.Add(drRow("leavename").ToString)
                    If IsDBNull(drRow.Item("startdate")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("startdate").ToString).ToShortDateString)
                    End If

                    If IsDBNull(drRow.Item("enddate")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("enddate").ToString).ToShortDateString)
                    End If
                    lvItem.SubItems.Add(Math.Round(CDec(drRow("days")), 2).ToString)
                    mstrStaus = ""

                    If mintLeaveFormunkid <> CInt(drRow("formunkid").ToString()) Then
                        dList = New DataView(dsPedingList.Tables(0), "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND formunkid = " & CInt(drRow("formunkid").ToString()), "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                        mintLeaveFormunkid = CInt(drRow("formunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then

                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            'Pinkal (16-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                            'Pinkal (16-May-2019) -- End

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 1 Then

                                    'Pinkal (16-May-2019) -- Start
                                    'Enhancement - Working on Leave UAT Changes for NMB.
                                    'mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & drRow("approvername").ToString()
                                    mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & " " & info1.ToTitleCase(drRow("approvername").ToString().ToLower())
                                    'Pinkal (16-May-2019) -- End
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 2 Then

                                    If CInt(dr(i)("statusunkid")) = 1 Then
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & dr(i)("approvername").ToString()
                                        mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & " " & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                        'Pinkal (03-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then

                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & dr(i)("approvername").ToString()
                                        mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & " " & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 4 Then
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & dr(i)("approvername").ToString()
                                        mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & " " & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 7 Then
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & dr(i)("IssueUserName").ToString()
                                        mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & " " & info1.ToTitleCase(dr(i)("IssueUserName").ToString().ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For
                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    'Pinkal (16-May-2019) -- Start
                                    'Enhancement - Working on Leave UAT Changes for NMB.
                                    'mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & drRow("approvername").ToString()
                                    mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & " " & info1.ToTitleCase(drRow("approvername").ToString().ToLower())
                                    'Pinkal (16-May-2019) -- End
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 4 Then
                                    'Pinkal (16-May-2019) -- Start
                                    'Enhancement - Working on Leave UAT Changes for NMB.
                                    'mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & drRow("approvername").ToString()
                                    mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & " " & info1.ToTitleCase(drRow("approvername").ToString().ToLower())
                                    'Pinkal (16-May-2019) -- End
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 7 Then
                                    'Pinkal (16-May-2019) -- Start
                                    'Enhancement - Working on Leave UAT Changes for NMB.
                                    'mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & drRow("IssueUserName").ToString()
                                    mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & " " & info1.ToTitleCase(drRow("IssueUserName").ToString().ToLower())
                                    'Pinkal (16-May-2019) -- End
                                    Exit For

                                End If

                            Next

                        End If


                    End If

                    If mstrStaus <> "" Then
                        lvItem.SubItems.Add(mstrStaus)
                    Else
                        lvItem.SubItems.Add(drRow("status").ToString)
                    End If

                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("approvaldate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                    lvItem.SubItems.Add(drRow("statusunkid").ToString)
                    lvItem.SubItems.Add(drRow("formunkid").ToString)
                    lvItem.SubItems.Add(drRow("leavetypeunkid").ToString)
                    lvItem.SubItems.Add(drRow("approverunkid").ToString)
                    lvItem.SubItems.Add(drRow("leaveissueunkid").ToString)
                    lvItem.SubItems.Add(drRow("mapuserunkid").ToString)
                    lvItem.SubItems.Add(drRow("mapapproverunkid").ToString)
                    lvItem.SubItems.Add(drRow("iscancelform").ToString)
                    lvItem.SubItems.Add(drRow("leaveapproverunkid").ToString)
                    lvItem.SubItems.Add(drRow("priority").ToString)
                    lvItem.SubItems.Add(drRow("isexternalapprover").ToString)

                    lvLeaveProcessList.Items.Add(lvItem)
                Next

                lvLeaveProcessList.GridLines = False
                lvLeaveProcessList.GroupingColumn = colhFormNo
                lvLeaveProcessList.DisplayGroups(True)


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'If lvLeaveProcessList.Items.Count > 3 Then
                '    colhStatus.Width = 185 - 16
                'Else
                '    colhStatus.Width = 185
                'End If
                If lvLeaveProcessList.Items.Count > 3 Then
                    colhStatus.Width = 200 - 16
                Else
                    colhStatus.Width = 200
                End If
                'Pinkal (03-May-2019) -- End

                

                AddHandler lvLeaveProcessList.SelectedIndexChanged, AddressOf lvLeaveProcessList_SelectedIndexChanged
                Me.Cursor = Cursors.Default

            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsFill = objApprover.GetEmployeeFromUser(User._Object._Userunkid, User._Object.Privilege._AllowtoApproveLeave, User._Object.Privilege._AllowIssueLeave)
            dsFill = objApprover.GetEmployeeFromUser(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                            , ConfigParameter._Object._IsIncludeInactiveEmp, User._Object._Userunkid, User._Object.Privilege._AllowtoApproveLeave, User._Object.Privilege._AllowIssueLeave)
            'Pinkal (24-Aug-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsFill.Tables(0)
            End With


            'Pinkal (03-Nov-2014) -- Start
            'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
            Dim lstIDs As List(Of String) = (From p In dsFill.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))
            'Pinkal (03-Nov-2014) -- End



            'FOR STATUS
            dsFill = Nothing
            Dim objMaster As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsFill = objMaster.getLeaveStatusList("Status", True)
            dsFill = objMaster.getLeaveStatusList("Status", ConfigParameter._Object._ApplicableLeaveStatus, True)
            'Pinkal (03-Jan-2020) -- End

            cboStatus.ValueMember = "statusunkid"
            cboStatus.DisplayMember = "name"
            cboStatus.DataSource = dsFill.Tables("Status")


            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.getListForCombo("LeaveType", True)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            cboLeaveType.DataSource = dsFill.Tables("LeaveType")


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnIssueLeave.Enabled = User._Object.Privilege._AllowIssueLeave
            btnEdit.Enabled = User._Object.Privilege._AllowChangeLeaveFormStatus
            btnViewDiary.Enabled = User._Object.Privilege._AllowToViewDiary
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmProcessLeaveList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objpending = New clspendingleave_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpApplyDate.Checked = False

            dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpStartDate.Checked = False

            dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpEndDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpEndDate.Checked = False

            objMapping = New clsapprover_Usermapping
            objMapping.GetData(enUserType.Approver, , User._Object._Userunkid, )

            objApprover = New clsleaveapprover_master
            objApprover._Approverunkid = objMapping._Approverunkid


            'S.SANDEEP [30 JAN 2016] -- START
            'Dim objEmployee As New clsEmployee_Master

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''objEmployee._Employeeunkid = objApprover._leaveapproverunkid
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objApprover._leaveapproverunkid
            ''S.SANDEEP [04 JUN 2015] -- END
            'txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Surname
            Dim dsAppr As New DataSet
            dsAppr = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , objApprover._Approverunkid)
            If dsAppr.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = CStr(dsAppr.Tables("List").Rows(0)("name"))
            End If
            dsAppr.Dispose()

            'S.SANDEEP [30 JAN 2016] -- END


            FillCombo()
            'FillList()


            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsAutomaticIssueOnFinalApproval Then
                btnIssueLeave.Visible = False
                btnViewDiary.Location = btnIssueLeave.Location
            Else
                btnIssueLeave.Visible = True
            End If

            'Pinkal (01-Feb-2014) -- End


            RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            chkMyApprovals.Checked = True
            AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged


            lvLeaveProcessList.GridLines = False

            If lvLeaveProcessList.Items.Count > 0 Then lvLeaveProcessList.Items(0).Selected = True
            lvLeaveProcessList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveFormList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspendingleave_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clspendingleave_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnShowRemark_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowRemark.Click
        Try
            If lvLeaveProcessList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveProcessList.Select()
                Exit Sub
            End If
            gbFilterCriteria.Enabled = False
            lvLeaveProcessList.Enabled = False
            objFooter.Enabled = False
            txtRemarks.Text = ""
            gbRemark.Visible = True
            gbFilterCriteria.BringToFront()
            objpending._Pendingleavetranunkid = CInt(lvLeaveProcessList.SelectedItems(0).Tag)
            txtRemarks.Text = objpending._Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShowRemark_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRemarkClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemarkClose.Click
        Try
            gbFilterCriteria.SendToBack()
            gbRemark.Visible = False
            gbFilterCriteria.Enabled = True
            objFooter.Enabled = True
            lvLeaveProcessList.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRemarkClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Dim ObjFrm As New frmProcessLeave
            If lvLeaveProcessList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveProcessList.Select()
                Exit Sub
            End If

            If User._Object._Userunkid <> CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhUserunkid.Index).Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You can't Edit this Leave detail. Reason: You are logged in into another user login account."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If CBool(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhIsCancelForm.Index).Text) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You can't Edit this Leave detail. Reason: This Leave is already Cancelled."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 7 Then 'FOR ISSUED
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already Issued."), enMsgBoxStyle.Information)
                Exit Sub
            End If



            'START FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT

            'Pinkal (09-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

            'Dim dtList As DataTable = New DataView(dsPedingList.Tables("PendingProcess"), "employeeunkid = " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
            '                                        & " AND formunkid = " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text) _
            '                                        & " AND approverunkid <> " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text), "", DataViewRowState.CurrentRows).ToTable

            Dim dtList As DataTable = New DataView(dsPedingList.Tables("PendingProcess"), "employeeunkid = " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                                    & " AND formunkid = " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text) _
                                                 & " AND leaveapproverunkid <> " & CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveApproverunkid.Index).Text), "", DataViewRowState.CurrentRows).ToTable

            'Pinkal (09-Jul-2014) -- End

            If dtList.Rows.Count > 0 Then
                Dim objapproverlevel As New clsapproverlevel_master
                objapproverlevel._Levelunkid = objApprover._Levelunkid
                For i As Integer = 0 To dtList.Rows.Count - 1


                    If objapproverlevel._Priority > CInt(dtList.Rows(i)("Priority")) Then

                        'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                        Dim dList As DataTable = New DataView(dtList, "levelunkid = " & CInt(dtList.Rows(i)("levelunkid")) & " AND (statusunkid = 1 or statusunkid = 7)", "", DataViewRowState.CurrentRows).ToTable

                        If dList.Rows.Count > 0 Then Continue For

                        'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                        If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit this Leave detail. Reason: This Leave approval is still pending."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 4 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 7 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already Issued."), enMsgBoxStyle.Information)
                            Exit Sub


                        End If

                    ElseIf objapproverlevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then

                        If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Leave detail. Reason: This Leave approval is already approved."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 4 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), enMsgBoxStyle.Information)
                            Exit Sub

                        ElseIf CInt(dtList.Rows(i)("statusunkid")) = 7 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already Issued."), enMsgBoxStyle.Information)
                            Exit Sub

                        End If

                    End If

                Next

            End If

            'END FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT

            If CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date _
             And objApprover._leaveapproverunkid = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text) Then

                If CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then   'FOR APPROVED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Leave detail. Reason: This Leave is already approved."), enMsgBoxStyle.Information)
                    Exit Sub

                ElseIf CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 7 Then 'FOR ISSUED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already Issued."), enMsgBoxStyle.Information)
                    Exit Sub


                    'Pinkal (15-Oct-2014) -- Start
                    'Enhancement -  AKFTZ LEAVE APPROVER PROBLEM FOR VISIBILITY IN LEAVE PROCESS PENDING TRAN

                ElseIf CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), enMsgBoxStyle.Information)
                    Exit Sub

                ElseIf CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 4 Then 'FOR RE-SCHEDULED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), enMsgBoxStyle.Information)
                    Exit Sub

                    'Pinkal (15-Oct-2014) -- End

                End If



            ElseIf CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date > ConfigParameter._Object._CurrentDateAndTime.Date _
             And objApprover._leaveapproverunkid = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text) Then

                If CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), enMsgBoxStyle.Information)
                    Exit Sub
                ElseIf CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 4 Then 'FOR RE-SCHEDULED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If


            ObjFrm.mintLeavetypeunkid = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
            ObjFrm.mdtStartdate = CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhStartDate.Index).Text)
            ObjFrm.mdtenddate = CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhEndDate.Index).Text)
            ObjFrm._Priority = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhPriority.Index).Text)

            If ObjFrm.displayDialog(CInt(lvLeaveProcessList.SelectedItems(0).Tag), _
                CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), lvLeaveProcessList.SelectedItems(0).SubItems(colhFormNo.Index).Text, _
                CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), lvLeaveProcessList.SelectedItems(0).SubItems(colhEmployee.Index).Text, _
                CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text), _
                CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhApplyDate.Index).Text), _
                CDate(lvLeaveProcessList.SelectedItems(0).SubItems(colhEndDate.Index).Text), enAction.ADD_ONE, _
                CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text), _
                CBool(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhisexternalapprover.Index).Text)) Then
                'S.SANDEEP [30 JAN 2016] -- START {CBool(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhisexternalapprover.Index).Text)} -- END

                FillList()

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnIssueLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIssueLeave.Click
        Try
            Dim ObjFrm As New frmLeaveIssue_AddEdit()

            ObjFrm.gbLeaveIssue.Visible = True
            ObjFrm.pnlLeaveIssueInfo.Visible = True

            ObjFrm.gbLeaveViewer.Visible = False
            ObjFrm.pnlLeaveViewer.Visible = False

            If lvLeaveProcessList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveProcessList.Select()
                Exit Sub
            End If


            Dim dsPendingApprover As DataSet = objpending.GetEmployeeApproverListWithPriority(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text))


            'Pinkal (15-Jul-2013) -- Start
            'Enhancement : TRA Changes
            'Dim drRow() As DataRow = dsPendingApprover.Tables(0).Select("priority = MAX(priority)")
            Dim drRow() As DataRow = dsPendingApprover.Tables(0).Select("priority = MAX(priority) AND statusunkid = 1")
            'Pinkal (15-Jul-2013) -- End

            If drRow.Length > 0 Then
                ObjFrm.mintPedingProcessunkid = CInt(drRow(0)("pendingleavetranunkid"))
            End If


            'Pinkal (09-Jan-2014) -- Start
            'Enhancement : Oman Changes

            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)

            If objLeaveType._IsCheckDocOnLeaveIssue Then
                Dim objDoc As New clsScan_Attach_Documents
                Dim dtDoc As DataTable = objDoc.GetAttachmentTranunkIds(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                                                                                       , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module, CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text))

                If dtDoc Is Nothing OrElse dtDoc.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You cannot issue this leave form.Reason: Document attachment is mandatory for this leave type. Please attach document in order to perform transaction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            End If

            'Pinkal (09-Jan-2014) -- End





            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If (ObjFrm.displayDialog(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text), False, enAction.ADD_ONE, CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), _
            '    CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text), _
            '    lvLeaveProcessList.SelectedItems(0).SubItems(colhEmployee.Index).Text)) Then


            If (ObjFrm.displayDialog(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text), False, enAction.ADD_ONE _
                                               , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                               , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text) _
                                               , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text) _
                                               , lvLeaveProcessList.SelectedItems(0).SubItems(colhEmployee.Index).Text _
                                               , CBool(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhisexternalapprover.Index).Text))) Then

                'Pinkal (01-Mar-2016) -- End

                lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text = ObjFrm.mintLeaveIssueUnkid.ToString

                If ObjFrm.mintLeaveIssueUnkid > 0 Then


                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                    'objpending.UpdatePendingFormStatus(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), _
                    '                                   CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), _
                    '                                  CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text), _
                    '                                  CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveApproverunkid.Index).Text))

                    objpending.UpdatePendingFormStatus(FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                            , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                            , ConfigParameter._Object._EmployeeAsOnDate.ToString(), ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text) _
                                                                            , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                                                            , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhapproverunkid.Index).Text) _
                                                                            , CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveApproverunkid.Index).Text))


                    'Pinkal (24-Aug-2015) -- End

                End If

                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuIssueLeave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If chkIncludeClosedFYTransactions.Checked Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                    cboEmployee.Select()
                    Exit Sub
                ElseIf CInt(cboLeaveType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                    cboLeaveType.Select()
                    Exit Sub
                End If
            End If
            'Pinkal (15-Mar-2019) -- End

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0
            txtFormNo.Text = ""

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then

                dtpApplyDate.Value = FinancialYear._Object._Database_End_Date.Date
                dtpStartDate.Value = FinancialYear._Object._Database_End_Date.Date
                dtpEndDate.Value = FinancialYear._Object._Database_End_Date.Date

            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then

                dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

            End If

            dtpApplyDate.Checked = False
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
            cboStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            chkIncludeClosedFYTransactions.Checked = False
            'Pinkal (15-Mar-2019) -- End

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnViewDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDiary.Click
        Dim frm As New frmEmployeeDiary
        Try
            If lvLeaveProcessList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveProcessList.Select()
                Exit Sub
            End If
            frm.displayDialog(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnViewDiary_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            frm._Hr_EmployeeTable_Alias = "h1"
            'Pinkal (06-May-2014) -- End

            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview Event"

    Private Sub lvLeaveProcessList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLeaveProcessList.SelectedIndexChanged
        Dim dsList As DataSet = Nothing
        Try
            If lvLeaveProcessList.SelectedItems.Count > 0 Then


                'START FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT

                dsList = objMapping.GetList("Mapping", enUserType.Approver)

                If dsList.Tables("Mapping").Rows.Count > 0 Then
                    Dim dtList As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid= " & CInt(User._Object._Userunkid), "", DataViewRowState.CurrentRows).ToTable
                    If dtList.Rows.Count > 0 Then
                        objApprover._Approverunkid = CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhmapApproverunkid.Index).Text)
                    End If
                End If

                'END FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT

                If lvLeaveProcessList.SelectedItems.Count > 0 Then

                    If CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Or CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 4 Then
                        btnEdit.Enabled = False
                    Else
                        btnEdit.Enabled = True
                    End If

                End If


                Dim objapproverlevel As New clsapproverlevel_master
                objapproverlevel._Levelunkid = objApprover._Levelunkid

                Call SetVisibility()

                'START FOR CHECK WHETHER FORM IS APPROVED BY HIGH AUTHORITY OR NOT


                Dim dsEmpApprover As DataSet = objpending.GetEmployeeApproverListWithPriority(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), -1)
                Dim dsPendingApprover As DataSet = objpending.GetEmployeeApproverListWithPriority(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text))

                If dsEmpApprover.Tables(0).Rows.Count <> dsPendingApprover.Tables(0).Rows.Count Then

                    'START ISSUE BUTTON IS ONLY ENABLE WHEN LEAVE FORM IS APPROVE

                    If User._Object.Privilege._AllowIssueLeave = True Then


                        'Pinkal (06-Mar-2014) -- Start
                        'Enhancement : Oman Changes [given error everytime when dsPendingApprover is not having row]

                        'If objpending.GetApproverMaxPriorityStatusId(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text),CInt(dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")) = 1 _
                        '                   AndAlso CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text) <= 0 Then
                        '    btnIssueLeave.Enabled = True
                        'Else
                        '    btnIssueLeave.Enabled = False
                        'End If

                        If objpending.GetApproverMaxPriorityStatusId(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), _
                                                                  CInt(IIf(IsDBNull(dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")), -1, dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")))) = 1 _
                                           AndAlso CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text) <= 0 Then
                            btnIssueLeave.Enabled = True
                        Else
                            btnIssueLeave.Enabled = False
                        End If

                        'Pinkal (06-Mar-2014) -- End

                    End If

                    'END ISSUE BUTTON IS ONLY ENABLE WHEN LEAVE FORM IS APPROVE

                Else

                    'START ISSUE BUTTON IS ONLY ENABLE WHEN LEAVE FORM IS APPROVE

                    If User._Object.Privilege._AllowIssueLeave = True Then


                        'Pinkal (06-Mar-2014) -- Start
                        'Enhancement : Oman Changes [given error everytime when dsPendingApprover is not having row]

                        'If objpending.GetApproverMaxPriorityStatusId(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), CInt(dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1"))) = 1 _
                        '                    AndAlso CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text) <= 0 Then
                        '    btnIssueLeave.Enabled = True
                        'Else
                        '    btnIssueLeave.Enabled = False
                        'End If

                        If objpending.GetApproverMaxPriorityStatusId(CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhFormunkid.Index).Text), CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), _
                                                                CInt(IIf(IsDBNull(dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")), -1, dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")))) = 1 _
                                            AndAlso CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhLeaveIssueunkid.Index).Text) <= 0 Then
                            btnIssueLeave.Enabled = True
                        Else
                            btnIssueLeave.Enabled = False
                        End If

                        'Pinkal (06-Mar-2014) -- End

                    End If

                    'END ISSUE BUTTON IS ONLY ENABLE WHEN LEAVE FORM IS APPROVE

                End If

                'END FOR CHECK WHETHER FORM IS APPROVED BY HIGH AUTHORITY OR NOT


                If CInt(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) <> 2 Then
                    btnShowRemark.Enabled = True
                Else
                    btnShowRemark.Enabled = False
                End If


                If CBool(lvLeaveProcessList.SelectedItems(0).SubItems(objcolhIsCancelForm.Index).Text) = True Then
                    btnIssueLeave.Enabled = False
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveProcessList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            FillList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnIssueLeave.GradientBackColor = GUI._ButttonBackColor
            Me.btnIssueLeave.GradientForeColor = GUI._ButttonFontColor

            Me.btnShowRemark.GradientBackColor = GUI._ButttonBackColor
            Me.btnShowRemark.GradientForeColor = GUI._ButttonFontColor

            Me.btnRemarkClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnRemarkClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnViewDiary.GradientBackColor = GUI._ButttonBackColor
            Me.btnViewDiary.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhFormNo.Text = Language._Object.getCaption(CStr(Me.colhFormNo.Tag), Me.colhFormNo.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colLeaveType.Text = Language._Object.getCaption(CStr(Me.colLeaveType.Tag), Me.colLeaveType.Text)
            Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
            Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
            Me.colhLeaveDays.Text = Language._Object.getCaption(CStr(Me.colhLeaveDays.Tag), Me.colhLeaveDays.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhApplyDate.Text = Language._Object.getCaption(CStr(Me.colhApplyDate.Tag), Me.colhApplyDate.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblApplyDate.Text = Language._Object.getCaption(Me.lblApplyDate.Name, Me.lblApplyDate.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.btnIssueLeave.Text = Language._Object.getCaption(Me.btnIssueLeave.Name, Me.btnIssueLeave.Text)
            Me.btnShowRemark.Text = Language._Object.getCaption(Me.btnShowRemark.Name, Me.btnShowRemark.Text)
            Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)
            Me.btnRemarkClose.Text = Language._Object.getCaption(Me.btnRemarkClose.Name, Me.btnRemarkClose.Text)
            Me.btnViewDiary.Text = Language._Object.getCaption(Me.btnViewDiary.Name, Me.btnViewDiary.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)
            Me.colhEmployeeCode.Text = Language._Object.getCaption(CStr(Me.colhEmployeeCode.Tag), Me.colhEmployeeCode.Text)
			Me.chkIncludeClosedFYTransactions.Text = Language._Object.getCaption(Me.chkIncludeClosedFYTransactions.Name, Me.chkIncludeClosedFYTransactions.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Leave Form Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "You can't Edit this Leave detail. Reason: You are logged in into another user login account.")
            Language.setMessage(mstrModuleName, 3, "You can't Edit this Leave detail. Reason: This Leave approval is still pending.")
            Language.setMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected.")
            Language.setMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled.")
            Language.setMessage(mstrModuleName, 6, "You can't Edit this Leave detail. Reason: This Leave approval is already approved.")
            Language.setMessage(mstrModuleName, 7, "You can't Edit this Leave detail. Reason: This Leave is already approved.")
            Language.setMessage(mstrModuleName, 8, "Please select Leave Form from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already Issued.")
            Language.setMessage(mstrModuleName, 10, "You can't Edit this Leave detail. Reason: This Leave is already Cancelled.")
            Language.setMessage(mstrModuleName, 11, "Approved By :-")
            Language.setMessage(mstrModuleName, 12, "Rejected By :-")
            Language.setMessage(mstrModuleName, 13, "Re-Scheduled By :-")
            Language.setMessage(mstrModuleName, 14, "Issued By :-")
            Language.setMessage(mstrModuleName, 15, "You cannot issue this leave form.Reason: Document attachment is mandatory for this leave type. Please attach document in order to perform transaction.")
			Language.setMessage(mstrModuleName, 16, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 17, "Leave Type is compulsory information.Please Select Leave Type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

