﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProcessLeave
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProcessLeave))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbProcessLeaveInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeAllStaff = New System.Windows.Forms.CheckBox
        Me.cboReliever = New System.Windows.Forms.ComboBox
        Me.LblReliever = New System.Windows.Forms.Label
        Me.objbtnSearchReliever = New eZee.Common.eZeeGradientButton
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.lblApprover = New System.Windows.Forms.Label
        Me.lblApprovalDate = New System.Windows.Forms.Label
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.dtpApplyDate = New System.Windows.Forms.DateTimePicker
        Me.objAsonDateAccrue = New System.Windows.Forms.Label
        Me.LblAsonDateAccrue = New System.Windows.Forms.Label
        Me.objbtnSearchApprover = New eZee.Common.eZeeGradientButton
        Me.objAsonDateBalance = New System.Windows.Forms.Label
        Me.lblAllowance = New System.Windows.Forms.Label
        Me.lblAsonDateBalance = New System.Windows.Forms.Label
        Me.cboAllowance = New System.Windows.Forms.ComboBox
        Me.lnkShowScanDocuementList = New System.Windows.Forms.LinkLabel
        Me.objNoofDays = New System.Windows.Forms.Label
        Me.lnkLeaveDayCount = New System.Windows.Forms.LinkLabel
        Me.txtAllowance = New eZee.TextBox.NumericTextBox
        Me.lnkShowLeaveExpense = New System.Windows.Forms.LinkLabel
        Me.lblValue = New System.Windows.Forms.Label
        Me.txtReportTo = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchTranHeads = New eZee.Common.eZeeGradientButton
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblStartdate = New System.Windows.Forms.Label
        Me.lnkShowLeaveForm = New System.Windows.Forms.LinkLabel
        Me.dtpStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblReportTo = New System.Windows.Forms.Label
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.dtpEnddate = New System.Windows.Forms.DateTimePicker
        Me.txtFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.objlblLeaveDate = New System.Windows.Forms.Label
        Me.lblAs = New System.Windows.Forms.Label
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.lnLeaveBalance = New eZee.Common.eZeeLine
        Me.objTlbLeaveBalance = New System.Windows.Forms.TableLayoutPanel
        Me.objlblBalanceAsonDateValue = New System.Windows.Forms.Label
        Me.LblBalanceAsonDate = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.LblTotalAdjustment = New System.Windows.Forms.Label
        Me.lblTotalIssuedToDate = New System.Windows.Forms.Label
        Me.lblToDateAccrued = New System.Windows.Forms.Label
        Me.LblLeaveBF = New System.Windows.Forms.Label
        Me.objlblBalanceValue = New System.Windows.Forms.Label
        Me.objlblTotalAdjustment = New System.Windows.Forms.Label
        Me.objlblIssuedToDateValue = New System.Windows.Forms.Label
        Me.objlblAccruedToDateValue = New System.Windows.Forms.Label
        Me.objlblLeaveBFvalue = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnReSchedule = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblLastYearAccrued = New System.Windows.Forms.Label
        Me.objlblLastYearIssuedValue = New System.Windows.Forms.Label
        Me.objlblLastYearAccruedValue = New System.Windows.Forms.Label
        Me.lblLastYearIssued = New System.Windows.Forms.Label
        Me.txtLeaveType = New eZee.TextBox.AlphanumericTextBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbProcessLeaveInfo.SuspendLayout()
        Me.objTlbLeaveBalance.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbProcessLeaveInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.objlblLastYearIssuedValue)
        Me.pnlMainInfo.Controls.Add(Me.objlblLastYearAccruedValue)
        Me.pnlMainInfo.Controls.Add(Me.lblLastYearIssued)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(683, 429)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbProcessLeaveInfo
        '
        Me.gbProcessLeaveInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProcessLeaveInfo.Checked = False
        Me.gbProcessLeaveInfo.CollapseAllExceptThis = False
        Me.gbProcessLeaveInfo.CollapsedHoverImage = Nothing
        Me.gbProcessLeaveInfo.CollapsedNormalImage = Nothing
        Me.gbProcessLeaveInfo.CollapsedPressedImage = Nothing
        Me.gbProcessLeaveInfo.CollapseOnLoad = False
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtLeaveType)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblLeaveType)
        Me.gbProcessLeaveInfo.Controls.Add(Me.chkIncludeAllStaff)
        Me.gbProcessLeaveInfo.Controls.Add(Me.cboReliever)
        Me.gbProcessLeaveInfo.Controls.Add(Me.LblReliever)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objbtnSearchReliever)
        Me.gbProcessLeaveInfo.Controls.Add(Me.cboApprover)
        Me.gbProcessLeaveInfo.Controls.Add(Me.EZeeLine1)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblApprover)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblApprovalDate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblRemark)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtRemark)
        Me.gbProcessLeaveInfo.Controls.Add(Me.dtpApplyDate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objAsonDateAccrue)
        Me.gbProcessLeaveInfo.Controls.Add(Me.LblAsonDateAccrue)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objbtnSearchApprover)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objAsonDateBalance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblAllowance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblAsonDateBalance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.cboAllowance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lnkShowScanDocuementList)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objNoofDays)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lnkLeaveDayCount)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtAllowance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lnkShowLeaveExpense)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblValue)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtReportTo)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objbtnSearchTranHeads)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblDepartment)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblStartdate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lnkShowLeaveForm)
        Me.gbProcessLeaveInfo.Controls.Add(Me.dtpStartdate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblReportTo)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblEnddate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtDepartment)
        Me.gbProcessLeaveInfo.Controls.Add(Me.dtpEnddate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtFormNo)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblFormNo)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objlblLeaveDate)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblAs)
        Me.gbProcessLeaveInfo.Controls.Add(Me.txtEmployee)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lblEmployee)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objLine1)
        Me.gbProcessLeaveInfo.Controls.Add(Me.lnLeaveBalance)
        Me.gbProcessLeaveInfo.Controls.Add(Me.objTlbLeaveBalance)
        Me.gbProcessLeaveInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProcessLeaveInfo.ExpandedHoverImage = Nothing
        Me.gbProcessLeaveInfo.ExpandedNormalImage = Nothing
        Me.gbProcessLeaveInfo.ExpandedPressedImage = Nothing
        Me.gbProcessLeaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProcessLeaveInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProcessLeaveInfo.HeaderHeight = 25
        Me.gbProcessLeaveInfo.HeaderMessage = ""
        Me.gbProcessLeaveInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProcessLeaveInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProcessLeaveInfo.HeightOnCollapse = 0
        Me.gbProcessLeaveInfo.LeftTextSpace = 0
        Me.gbProcessLeaveInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbProcessLeaveInfo.Name = "gbProcessLeaveInfo"
        Me.gbProcessLeaveInfo.OpenHeight = 300
        Me.gbProcessLeaveInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProcessLeaveInfo.ShowBorder = True
        Me.gbProcessLeaveInfo.ShowCheckBox = False
        Me.gbProcessLeaveInfo.ShowCollapseButton = False
        Me.gbProcessLeaveInfo.ShowDefaultBorderColor = True
        Me.gbProcessLeaveInfo.ShowDownButton = False
        Me.gbProcessLeaveInfo.ShowHeader = True
        Me.gbProcessLeaveInfo.Size = New System.Drawing.Size(683, 379)
        Me.gbProcessLeaveInfo.TabIndex = 6
        Me.gbProcessLeaveInfo.Temp = 0
        Me.gbProcessLeaveInfo.Text = "Process Leave Info"
        Me.gbProcessLeaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeAllStaff
        '
        Me.chkIncludeAllStaff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeAllStaff.Location = New System.Drawing.Point(449, 167)
        Me.chkIncludeAllStaff.Name = "chkIncludeAllStaff"
        Me.chkIncludeAllStaff.Size = New System.Drawing.Size(192, 17)
        Me.chkIncludeAllStaff.TabIndex = 298
        Me.chkIncludeAllStaff.Text = "Include All Staff"
        Me.chkIncludeAllStaff.UseVisualStyleBackColor = True
        '
        'cboReliever
        '
        Me.cboReliever.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReliever.DropDownWidth = 350
        Me.cboReliever.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReliever.FormattingEnabled = True
        Me.cboReliever.Location = New System.Drawing.Point(449, 189)
        Me.cboReliever.Name = "cboReliever"
        Me.cboReliever.Size = New System.Drawing.Size(195, 21)
        Me.cboReliever.TabIndex = 294
        '
        'LblReliever
        '
        Me.LblReliever.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReliever.Location = New System.Drawing.Point(356, 192)
        Me.LblReliever.Name = "LblReliever"
        Me.LblReliever.Size = New System.Drawing.Size(88, 15)
        Me.LblReliever.TabIndex = 295
        Me.LblReliever.Text = "Reliever"
        Me.LblReliever.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchReliever
        '
        Me.objbtnSearchReliever.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReliever.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReliever.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReliever.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReliever.BorderSelected = False
        Me.objbtnSearchReliever.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReliever.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReliever.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReliever.Location = New System.Drawing.Point(650, 189)
        Me.objbtnSearchReliever.Name = "objbtnSearchReliever"
        Me.objbtnSearchReliever.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReliever.TabIndex = 296
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(449, 215)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(195, 21)
        Me.cboApprover.TabIndex = 3
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(16, 246)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(323, 10)
        Me.EZeeLine1.TabIndex = 292
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(356, 218)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(88, 15)
        Me.lblApprover.TabIndex = 244
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprovalDate
        '
        Me.lblApprovalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalDate.Location = New System.Drawing.Point(356, 299)
        Me.lblApprovalDate.Name = "lblApprovalDate"
        Me.lblApprovalDate.Size = New System.Drawing.Size(88, 15)
        Me.lblApprovalDate.TabIndex = 245
        Me.lblApprovalDate.Text = "Approval Date"
        Me.lblApprovalDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(20, 310)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(65, 15)
        Me.lblRemark.TabIndex = 255
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(91, 310)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(249, 61)
        Me.txtRemark.TabIndex = 10
        '
        'dtpApplyDate
        '
        Me.dtpApplyDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Checked = False
        Me.dtpApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplyDate.Location = New System.Drawing.Point(449, 296)
        Me.dtpApplyDate.Name = "dtpApplyDate"
        Me.dtpApplyDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpApplyDate.TabIndex = 4
        '
        'objAsonDateAccrue
        '
        Me.objAsonDateAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objAsonDateAccrue.Location = New System.Drawing.Point(181, 208)
        Me.objAsonDateAccrue.Name = "objAsonDateAccrue"
        Me.objAsonDateAccrue.Size = New System.Drawing.Size(67, 17)
        Me.objAsonDateAccrue.TabIndex = 291
        Me.objAsonDateAccrue.Text = "0.00"
        Me.objAsonDateAccrue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblAsonDateAccrue
        '
        Me.LblAsonDateAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAsonDateAccrue.Location = New System.Drawing.Point(20, 208)
        Me.LblAsonDateAccrue.Name = "LblAsonDateAccrue"
        Me.LblAsonDateAccrue.Size = New System.Drawing.Size(155, 16)
        Me.LblAsonDateAccrue.TabIndex = 290
        Me.LblAsonDateAccrue.Text = "Leave Accrue As on End Date"
        '
        'objbtnSearchApprover
        '
        Me.objbtnSearchApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApprover.BorderSelected = False
        Me.objbtnSearchApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApprover.Location = New System.Drawing.Point(650, 215)
        Me.objbtnSearchApprover.Name = "objbtnSearchApprover"
        Me.objbtnSearchApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApprover.TabIndex = 264
        Me.objbtnSearchApprover.Visible = False
        '
        'objAsonDateBalance
        '
        Me.objAsonDateBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objAsonDateBalance.Location = New System.Drawing.Point(181, 230)
        Me.objAsonDateBalance.Name = "objAsonDateBalance"
        Me.objAsonDateBalance.Size = New System.Drawing.Size(67, 17)
        Me.objAsonDateBalance.TabIndex = 289
        Me.objAsonDateBalance.Text = "0.00"
        Me.objAsonDateBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAllowance
        '
        Me.lblAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllowance.Location = New System.Drawing.Point(356, 326)
        Me.lblAllowance.Name = "lblAllowance"
        Me.lblAllowance.Size = New System.Drawing.Size(88, 15)
        Me.lblAllowance.TabIndex = 266
        Me.lblAllowance.Text = "Allowance"
        Me.lblAllowance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAsonDateBalance
        '
        Me.lblAsonDateBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsonDateBalance.Location = New System.Drawing.Point(20, 231)
        Me.lblAsonDateBalance.Name = "lblAsonDateBalance"
        Me.lblAsonDateBalance.Size = New System.Drawing.Size(155, 16)
        Me.lblAsonDateBalance.TabIndex = 288
        Me.lblAsonDateBalance.Text = "Leave Balance As on End Date"
        '
        'cboAllowance
        '
        Me.cboAllowance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllowance.Enabled = False
        Me.cboAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllowance.FormattingEnabled = True
        Me.cboAllowance.Location = New System.Drawing.Point(449, 323)
        Me.cboAllowance.Name = "cboAllowance"
        Me.cboAllowance.Size = New System.Drawing.Size(195, 21)
        Me.cboAllowance.TabIndex = 5
        '
        'lnkShowScanDocuementList
        '
        Me.lnkShowScanDocuementList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowScanDocuementList.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowScanDocuementList.Location = New System.Drawing.Point(159, 282)
        Me.lnkShowScanDocuementList.Name = "lnkShowScanDocuementList"
        Me.lnkShowScanDocuementList.Size = New System.Drawing.Size(133, 17)
        Me.lnkShowScanDocuementList.TabIndex = 286
        Me.lnkShowScanDocuementList.TabStop = True
        Me.lnkShowScanDocuementList.Text = "Show Scan Documents"
        Me.lnkShowScanDocuementList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkShowScanDocuementList.Visible = False
        '
        'objNoofDays
        '
        Me.objNoofDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objNoofDays.Location = New System.Drawing.Point(553, 271)
        Me.objNoofDays.Name = "objNoofDays"
        Me.objNoofDays.Size = New System.Drawing.Size(127, 17)
        Me.objNoofDays.TabIndex = 284
        Me.objNoofDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkLeaveDayCount
        '
        Me.lnkLeaveDayCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkLeaveDayCount.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkLeaveDayCount.Location = New System.Drawing.Point(159, 261)
        Me.lnkLeaveDayCount.Name = "lnkLeaveDayCount"
        Me.lnkLeaveDayCount.Size = New System.Drawing.Size(133, 17)
        Me.lnkLeaveDayCount.TabIndex = 260
        Me.lnkLeaveDayCount.TabStop = True
        Me.lnkLeaveDayCount.Text = "Leave Day Fraction"
        '
        'txtAllowance
        '
        Me.txtAllowance.AllowNegative = True
        Me.txtAllowance.BackColor = System.Drawing.SystemColors.Window
        Me.txtAllowance.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAllowance.DigitsInGroup = 0
        Me.txtAllowance.Enabled = False
        Me.txtAllowance.Flags = 0
        Me.txtAllowance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAllowance.Location = New System.Drawing.Point(449, 350)
        Me.txtAllowance.MaxDecimalPlaces = 6
        Me.txtAllowance.MaxWholeDigits = 21
        Me.txtAllowance.Name = "txtAllowance"
        Me.txtAllowance.Prefix = ""
        Me.txtAllowance.RangeMax = 1.7976931348623157E+308
        Me.txtAllowance.RangeMin = -1.7976931348623157E+308
        Me.txtAllowance.Size = New System.Drawing.Size(101, 21)
        Me.txtAllowance.TabIndex = 6
        Me.txtAllowance.Text = "0"
        Me.txtAllowance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lnkShowLeaveExpense
        '
        Me.lnkShowLeaveExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowLeaveExpense.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowLeaveExpense.Location = New System.Drawing.Point(20, 282)
        Me.lnkShowLeaveExpense.Name = "lnkShowLeaveExpense"
        Me.lnkShowLeaveExpense.Size = New System.Drawing.Size(133, 17)
        Me.lnkShowLeaveExpense.TabIndex = 282
        Me.lnkShowLeaveExpense.TabStop = True
        Me.lnkShowLeaveExpense.Text = "Show Leave Expense"
        Me.lnkShowLeaveExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblValue
        '
        Me.lblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.Location = New System.Drawing.Point(356, 353)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(88, 15)
        Me.lblValue.TabIndex = 271
        Me.lblValue.Text = "Value"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReportTo
        '
        Me.txtReportTo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReportTo.Flags = 0
        Me.txtReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReportTo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReportTo.Location = New System.Drawing.Point(449, 140)
        Me.txtReportTo.Name = "txtReportTo"
        Me.txtReportTo.ReadOnly = True
        Me.txtReportTo.Size = New System.Drawing.Size(195, 21)
        Me.txtReportTo.TabIndex = 9
        '
        'objbtnSearchTranHeads
        '
        Me.objbtnSearchTranHeads.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHeads.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHeads.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHeads.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHeads.BorderSelected = False
        Me.objbtnSearchTranHeads.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHeads.Enabled = False
        Me.objbtnSearchTranHeads.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHeads.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHeads.Location = New System.Drawing.Point(650, 324)
        Me.objbtnSearchTranHeads.Name = "objbtnSearchTranHeads"
        Me.objbtnSearchTranHeads.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHeads.TabIndex = 272
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(356, 115)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(88, 15)
        Me.lblDepartment.TabIndex = 247
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartdate
        '
        Me.lblStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartdate.Location = New System.Drawing.Point(356, 244)
        Me.lblStartdate.Name = "lblStartdate"
        Me.lblStartdate.Size = New System.Drawing.Size(88, 15)
        Me.lblStartdate.TabIndex = 277
        Me.lblStartdate.Text = "Start Date"
        Me.lblStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkShowLeaveForm
        '
        Me.lnkShowLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowLeaveForm.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShowLeaveForm.Location = New System.Drawing.Point(20, 261)
        Me.lnkShowLeaveForm.Name = "lnkShowLeaveForm"
        Me.lnkShowLeaveForm.Size = New System.Drawing.Size(133, 17)
        Me.lnkShowLeaveForm.TabIndex = 281
        Me.lnkShowLeaveForm.TabStop = True
        Me.lnkShowLeaveForm.Text = "Show Leave Form"
        '
        'dtpStartdate
        '
        Me.dtpStartdate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Checked = False
        Me.dtpStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartdate.Location = New System.Drawing.Point(449, 241)
        Me.dtpStartdate.Name = "dtpStartdate"
        Me.dtpStartdate.Size = New System.Drawing.Size(101, 21)
        Me.dtpStartdate.TabIndex = 276
        '
        'lblReportTo
        '
        Me.lblReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportTo.Location = New System.Drawing.Point(356, 142)
        Me.lblReportTo.Name = "lblReportTo"
        Me.lblReportTo.Size = New System.Drawing.Size(88, 15)
        Me.lblReportTo.TabIndex = 248
        Me.lblReportTo.Text = "Report To"
        Me.lblReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(356, 271)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(88, 15)
        Me.lblEnddate.TabIndex = 279
        Me.lblEnddate.Text = "End Date"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(449, 113)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(195, 21)
        Me.txtDepartment.TabIndex = 8
        '
        'dtpEnddate
        '
        Me.dtpEnddate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnddate.Checked = False
        Me.dtpEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnddate.Location = New System.Drawing.Point(449, 268)
        Me.dtpEnddate.Name = "dtpEnddate"
        Me.dtpEnddate.Size = New System.Drawing.Size(101, 21)
        Me.dtpEnddate.TabIndex = 278
        '
        'txtFormNo
        '
        Me.txtFormNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtFormNo.Flags = 0
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFormNo.Location = New System.Drawing.Point(449, 33)
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.ReadOnly = True
        Me.txtFormNo.Size = New System.Drawing.Size(195, 21)
        Me.txtFormNo.TabIndex = 1
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(356, 35)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(88, 15)
        Me.lblFormNo.TabIndex = 274
        Me.lblFormNo.Text = "Application No"
        Me.lblFormNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblLeaveDate
        '
        Me.objlblLeaveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLeaveDate.Location = New System.Drawing.Point(255, 32)
        Me.objlblLeaveDate.Name = "objlblLeaveDate"
        Me.objlblLeaveDate.Size = New System.Drawing.Size(79, 13)
        Me.objlblLeaveDate.TabIndex = 261
        Me.objlblLeaveDate.Text = "31/12/2010"
        '
        'lblAs
        '
        Me.lblAs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAs.Location = New System.Drawing.Point(211, 31)
        Me.lblAs.Name = "lblAs"
        Me.lblAs.Size = New System.Drawing.Size(34, 15)
        Me.lblAs.TabIndex = 259
        Me.lblAs.Text = "As"
        Me.lblAs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployee
        '
        Me.txtEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(449, 86)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(195, 21)
        Me.txtEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(356, 88)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(88, 15)
        Me.lblEmployee.TabIndex = 243
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(345, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(10, 351)
        Me.objLine1.TabIndex = 242
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'lnLeaveBalance
        '
        Me.lnLeaveBalance.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnLeaveBalance.Location = New System.Drawing.Point(8, 30)
        Me.lnLeaveBalance.Name = "lnLeaveBalance"
        Me.lnLeaveBalance.Size = New System.Drawing.Size(195, 17)
        Me.lnLeaveBalance.TabIndex = 3
        Me.lnLeaveBalance.Text = "Leave Balance"
        '
        'objTlbLeaveBalance
        '
        Me.objTlbLeaveBalance.BackColor = System.Drawing.SystemColors.Control
        Me.objTlbLeaveBalance.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objTlbLeaveBalance.ColumnCount = 2
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.22581!))
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.77419!))
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblBalanceAsonDateValue, 1, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblBalanceAsonDate, 0, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblAmount, 1, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblDescription, 0, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblBalance, 0, 6)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblTotalAdjustment, 0, 4)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblTotalIssuedToDate, 0, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblToDateAccrued, 0, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblLeaveBF, 0, 1)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblBalanceValue, 1, 6)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblTotalAdjustment, 1, 4)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblIssuedToDateValue, 1, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblAccruedToDateValue, 1, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblLeaveBFvalue, 1, 1)
        Me.objTlbLeaveBalance.Location = New System.Drawing.Point(21, 52)
        Me.objTlbLeaveBalance.Name = "objTlbLeaveBalance"
        Me.objTlbLeaveBalance.RowCount = 7
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.Size = New System.Drawing.Size(319, 149)
        Me.objTlbLeaveBalance.TabIndex = 2
        '
        'objlblBalanceAsonDateValue
        '
        Me.objlblBalanceAsonDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalanceAsonDateValue.Location = New System.Drawing.Point(236, 106)
        Me.objlblBalanceAsonDateValue.Name = "objlblBalanceAsonDateValue"
        Me.objlblBalanceAsonDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblBalanceAsonDateValue.TabIndex = 17
        Me.objlblBalanceAsonDateValue.Text = "0"
        Me.objlblBalanceAsonDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblBalanceAsonDate
        '
        Me.LblBalanceAsonDate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblBalanceAsonDate.Location = New System.Drawing.Point(4, 106)
        Me.LblBalanceAsonDate.Name = "LblBalanceAsonDate"
        Me.LblBalanceAsonDate.Size = New System.Drawing.Size(225, 20)
        Me.LblBalanceAsonDate.TabIndex = 16
        Me.LblBalanceAsonDate.Text = "Balance As on Date"
        Me.LblBalanceAsonDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAmount
        '
        Me.lblAmount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAmount.Location = New System.Drawing.Point(236, 1)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(79, 20)
        Me.lblAmount.TabIndex = 1
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescription
        '
        Me.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDescription.Location = New System.Drawing.Point(4, 1)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(225, 20)
        Me.lblDescription.TabIndex = 0
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBalance
        '
        Me.lblBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblBalance.Location = New System.Drawing.Point(4, 127)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(225, 21)
        Me.lblBalance.TabIndex = 10
        Me.lblBalance.Text = "Total Balance"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblTotalAdjustment
        '
        Me.LblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblTotalAdjustment.Location = New System.Drawing.Point(4, 85)
        Me.LblTotalAdjustment.Name = "LblTotalAdjustment"
        Me.LblTotalAdjustment.Size = New System.Drawing.Size(225, 20)
        Me.LblTotalAdjustment.TabIndex = 12
        Me.LblTotalAdjustment.Text = "Total Adjustment"
        Me.LblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalIssuedToDate
        '
        Me.lblTotalIssuedToDate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTotalIssuedToDate.Location = New System.Drawing.Point(4, 64)
        Me.lblTotalIssuedToDate.Name = "lblTotalIssuedToDate"
        Me.lblTotalIssuedToDate.Size = New System.Drawing.Size(225, 20)
        Me.lblTotalIssuedToDate.TabIndex = 8
        Me.lblTotalIssuedToDate.Text = "Total Issued To Date"
        Me.lblTotalIssuedToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToDateAccrued
        '
        Me.lblToDateAccrued.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblToDateAccrued.Location = New System.Drawing.Point(4, 43)
        Me.lblToDateAccrued.Name = "lblToDateAccrued"
        Me.lblToDateAccrued.Size = New System.Drawing.Size(225, 20)
        Me.lblToDateAccrued.TabIndex = 4
        Me.lblToDateAccrued.Text = "Total Accrued To Date"
        Me.lblToDateAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblLeaveBF
        '
        Me.LblLeaveBF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblLeaveBF.Location = New System.Drawing.Point(4, 22)
        Me.LblLeaveBF.Name = "LblLeaveBF"
        Me.LblLeaveBF.Size = New System.Drawing.Size(225, 20)
        Me.LblLeaveBF.TabIndex = 14
        Me.LblLeaveBF.Text = "Leave BF"
        Me.LblLeaveBF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblBalanceValue
        '
        Me.objlblBalanceValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalanceValue.Location = New System.Drawing.Point(236, 127)
        Me.objlblBalanceValue.Name = "objlblBalanceValue"
        Me.objlblBalanceValue.Size = New System.Drawing.Size(79, 21)
        Me.objlblBalanceValue.TabIndex = 11
        Me.objlblBalanceValue.Text = "0"
        Me.objlblBalanceValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblTotalAdjustment
        '
        Me.objlblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblTotalAdjustment.Location = New System.Drawing.Point(236, 85)
        Me.objlblTotalAdjustment.Name = "objlblTotalAdjustment"
        Me.objlblTotalAdjustment.Size = New System.Drawing.Size(79, 20)
        Me.objlblTotalAdjustment.TabIndex = 13
        Me.objlblTotalAdjustment.Text = "0"
        Me.objlblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblIssuedToDateValue
        '
        Me.objlblIssuedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblIssuedToDateValue.Location = New System.Drawing.Point(236, 64)
        Me.objlblIssuedToDateValue.Name = "objlblIssuedToDateValue"
        Me.objlblIssuedToDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblIssuedToDateValue.TabIndex = 9
        Me.objlblIssuedToDateValue.Text = "0"
        Me.objlblIssuedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblAccruedToDateValue
        '
        Me.objlblAccruedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAccruedToDateValue.Location = New System.Drawing.Point(236, 43)
        Me.objlblAccruedToDateValue.Name = "objlblAccruedToDateValue"
        Me.objlblAccruedToDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblAccruedToDateValue.TabIndex = 5
        Me.objlblAccruedToDateValue.Text = "0"
        Me.objlblAccruedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblLeaveBFvalue
        '
        Me.objlblLeaveBFvalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblLeaveBFvalue.Location = New System.Drawing.Point(236, 22)
        Me.objlblLeaveBFvalue.Name = "objlblLeaveBFvalue"
        Me.objlblLeaveBFvalue.Size = New System.Drawing.Size(79, 20)
        Me.objlblLeaveBFvalue.TabIndex = 15
        Me.objlblLeaveBFvalue.Text = "0"
        Me.objlblLeaveBFvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnReSchedule)
        Me.objFooter.Controls.Add(Me.btnReject)
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.lblLastYearAccrued)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 379)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(683, 50)
        Me.objFooter.TabIndex = 20
        '
        'btnReSchedule
        '
        Me.btnReSchedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReSchedule.BackColor = System.Drawing.Color.White
        Me.btnReSchedule.BackgroundImage = CType(resources.GetObject("btnReSchedule.BackgroundImage"), System.Drawing.Image)
        Me.btnReSchedule.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReSchedule.BorderColor = System.Drawing.Color.Empty
        Me.btnReSchedule.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReSchedule.FlatAppearance.BorderSize = 0
        Me.btnReSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReSchedule.ForeColor = System.Drawing.Color.Black
        Me.btnReSchedule.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReSchedule.GradientForeColor = System.Drawing.Color.Black
        Me.btnReSchedule.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReSchedule.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReSchedule.Location = New System.Drawing.Point(471, 10)
        Me.btnReSchedule.Name = "btnReSchedule"
        Me.btnReSchedule.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReSchedule.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReSchedule.Size = New System.Drawing.Size(97, 30)
        Me.btnReSchedule.TabIndex = 24
        Me.btnReSchedule.Text = "Re-Schedu&le"
        Me.btnReSchedule.UseVisualStyleBackColor = True
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(368, 10)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(97, 30)
        Me.btnReject.TabIndex = 23
        Me.btnReject.Text = "&Reject"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(265, 10)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 22
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(574, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblLastYearAccrued
        '
        Me.lblLastYearAccrued.Location = New System.Drawing.Point(8, 13)
        Me.lblLastYearAccrued.Name = "lblLastYearAccrued"
        Me.lblLastYearAccrued.Size = New System.Drawing.Size(225, 20)
        Me.lblLastYearAccrued.TabIndex = 21
        Me.lblLastYearAccrued.Text = "Total Accrued Upto Last Year"
        Me.lblLastYearAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLastYearAccrued.Visible = False
        '
        'objlblLastYearIssuedValue
        '
        Me.objlblLastYearIssuedValue.Location = New System.Drawing.Point(721, 172)
        Me.objlblLastYearIssuedValue.Name = "objlblLastYearIssuedValue"
        Me.objlblLastYearIssuedValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblLastYearIssuedValue.TabIndex = 7
        Me.objlblLastYearIssuedValue.Text = "0"
        Me.objlblLastYearIssuedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblLastYearIssuedValue.Visible = False
        '
        'objlblLastYearAccruedValue
        '
        Me.objlblLastYearAccruedValue.Location = New System.Drawing.Point(721, 106)
        Me.objlblLastYearAccruedValue.Name = "objlblLastYearAccruedValue"
        Me.objlblLastYearAccruedValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblLastYearAccruedValue.TabIndex = 3
        Me.objlblLastYearAccruedValue.Text = "0"
        Me.objlblLastYearAccruedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblLastYearAccruedValue.Visible = False
        '
        'lblLastYearIssued
        '
        Me.lblLastYearIssued.Location = New System.Drawing.Point(721, 132)
        Me.lblLastYearIssued.Name = "lblLastYearIssued"
        Me.lblLastYearIssued.Size = New System.Drawing.Size(225, 20)
        Me.lblLastYearIssued.TabIndex = 6
        Me.lblLastYearIssued.Text = "Total Issued Upto Last Year"
        Me.lblLastYearIssued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLastYearIssued.Visible = False
        '
        'txtLeaveType
        '
        Me.txtLeaveType.BackColor = System.Drawing.SystemColors.Window
        Me.txtLeaveType.Flags = 0
        Me.txtLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLeaveType.Location = New System.Drawing.Point(449, 59)
        Me.txtLeaveType.Name = "txtLeaveType"
        Me.txtLeaveType.ReadOnly = True
        Me.txtLeaveType.Size = New System.Drawing.Size(195, 21)
        Me.txtLeaveType.TabIndex = 300
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(356, 61)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(88, 15)
        Me.lblLeaveType.TabIndex = 301
        Me.lblLeaveType.Text = "Leave Type"
        Me.lblLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmProcessLeave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 429)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProcessLeave"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Process Pending Leave"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbProcessLeaveInfo.ResumeLayout(False)
        Me.gbProcessLeaveInfo.PerformLayout()
        Me.objTlbLeaveBalance.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbProcessLeaveInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnLeaveBalance As eZee.Common.eZeeLine
    Friend WithEvents objTlbLeaveBalance As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents objlblAccruedToDateValue As System.Windows.Forms.Label
    Friend WithEvents lblToDateAccrued As System.Windows.Forms.Label
    Friend WithEvents objlblIssuedToDateValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalIssuedToDate As System.Windows.Forms.Label
    Friend WithEvents objlblBalanceValue As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblReportTo As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblApprovalDate As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtReportTo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpApplyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblAs As System.Windows.Forms.Label
    Friend WithEvents objlblLeaveDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents cboAllowance As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllowance As System.Windows.Forms.Label
    Friend WithEvents txtAllowance As eZee.TextBox.NumericTextBox
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTranHeads As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents dtpStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartdate As System.Windows.Forms.Label
    Friend WithEvents lnkShowLeaveForm As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkShowLeaveExpense As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkLeaveDayCount As System.Windows.Forms.LinkLabel
    Friend WithEvents objNoofDays As System.Windows.Forms.Label
    Friend WithEvents lnkShowScanDocuementList As System.Windows.Forms.LinkLabel
    Friend WithEvents lblLastYearAccrued As System.Windows.Forms.Label
    Friend WithEvents objlblLastYearIssuedValue As System.Windows.Forms.Label
    Friend WithEvents objlblLastYearAccruedValue As System.Windows.Forms.Label
    Friend WithEvents lblLastYearIssued As System.Windows.Forms.Label
    Friend WithEvents LblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents objlblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents LblLeaveBF As System.Windows.Forms.Label
    Friend WithEvents objlblLeaveBFvalue As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents objAsonDateAccrue As System.Windows.Forms.Label
    Friend WithEvents LblAsonDateAccrue As System.Windows.Forms.Label
    Friend WithEvents objAsonDateBalance As System.Windows.Forms.Label
    Friend WithEvents lblAsonDateBalance As System.Windows.Forms.Label
    Friend WithEvents cboReliever As System.Windows.Forms.ComboBox
    Friend WithEvents LblReliever As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchReliever As eZee.Common.eZeeGradientButton
    Friend WithEvents LblBalanceAsonDate As System.Windows.Forms.Label
    Friend WithEvents objlblBalanceAsonDateValue As System.Windows.Forms.Label
    Friend WithEvents chkIncludeAllStaff As System.Windows.Forms.CheckBox
    Friend WithEvents btnReSchedule As eZee.Common.eZeeLightButton
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents txtLeaveType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
End Class
