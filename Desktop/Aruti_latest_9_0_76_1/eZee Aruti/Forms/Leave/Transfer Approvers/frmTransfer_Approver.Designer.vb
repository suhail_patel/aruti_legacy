﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransfer_Approver
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransfer_Approver))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.tabNewApprover = New System.Windows.Forms.TabControl
        Me.tbpMigratedEmployee = New System.Windows.Forms.TabPage
        Me.txtNewApproverMigratedEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objchkNewCheck = New System.Windows.Forms.CheckBox
        Me.dgNewApproverMigratedEmp = New System.Windows.Forms.DataGridView
        Me.objColhdgMigratedSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhdgMigratedEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgMigratedEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tbpAssignEmployee = New System.Windows.Forms.TabPage
        Me.dgNewApproverAssignEmp = New System.Windows.Forms.DataGridView
        Me.colhdgAssignEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgAssignEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtNewApproverAssignEmp = New eZee.TextBox.AlphanumericTextBox
        Me.txtOldSearchEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.objbtnSearchOldLevel = New eZee.Common.eZeeGradientButton
        Me.cboOldLevel = New System.Windows.Forms.ComboBox
        Me.objbtnSearchOldApprover = New eZee.Common.eZeeGradientButton
        Me.pnlOldEmp = New System.Windows.Forms.Panel
        Me.objchkOldCheck = New System.Windows.Forms.CheckBox
        Me.dgOldApproverEmp = New System.Windows.Forms.DataGridView
        Me.objcolhdgSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhdgEmployeecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlNewEmp = New System.Windows.Forms.Panel
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnUnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblNewLevel = New System.Windows.Forms.Label
        Me.lblNewApprover = New System.Windows.Forms.Label
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.objbtnSearchNewApprover = New eZee.Common.eZeeGradientButton
        Me.cboOldApprover = New System.Windows.Forms.ComboBox
        Me.objbtnSearchNewLevel = New eZee.Common.eZeeGradientButton
        Me.cboNewApprover = New System.Windows.Forms.ComboBox
        Me.cboNewLevel = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkShowInactiveApprovers = New System.Windows.Forms.CheckBox
        Me.chkShowInActiveEmployees = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.tabNewApprover.SuspendLayout()
        Me.tbpMigratedEmployee.SuspendLayout()
        CType(Me.dgNewApproverMigratedEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAssignEmployee.SuspendLayout()
        CType(Me.dgNewApproverAssignEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOldEmp.SuspendLayout()
        CType(Me.dgOldApproverEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(763, 517)
        Me.pnlMain.TabIndex = 0
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.chkShowInActiveEmployees)
        Me.gbInfo.Controls.Add(Me.chkShowInactiveApprovers)
        Me.gbInfo.Controls.Add(Me.objAlloacationReset)
        Me.gbInfo.Controls.Add(Me.lnkAllocation)
        Me.gbInfo.Controls.Add(Me.tabNewApprover)
        Me.gbInfo.Controls.Add(Me.txtOldSearchEmployee)
        Me.gbInfo.Controls.Add(Me.Label1)
        Me.gbInfo.Controls.Add(Me.objbtnSearchOldLevel)
        Me.gbInfo.Controls.Add(Me.cboOldLevel)
        Me.gbInfo.Controls.Add(Me.objbtnSearchOldApprover)
        Me.gbInfo.Controls.Add(Me.pnlOldEmp)
        Me.gbInfo.Controls.Add(Me.pnlNewEmp)
        Me.gbInfo.Controls.Add(Me.objbtnAssign)
        Me.gbInfo.Controls.Add(Me.objbtnUnAssign)
        Me.gbInfo.Controls.Add(Me.lblNewLevel)
        Me.gbInfo.Controls.Add(Me.lblNewApprover)
        Me.gbInfo.Controls.Add(Me.lblCaption1)
        Me.gbInfo.Controls.Add(Me.objbtnSearchNewApprover)
        Me.gbInfo.Controls.Add(Me.cboOldApprover)
        Me.gbInfo.Controls.Add(Me.objbtnSearchNewLevel)
        Me.gbInfo.Controls.Add(Me.cboNewApprover)
        Me.gbInfo.Controls.Add(Me.cboNewLevel)
        Me.gbInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(763, 462)
        Me.gbInfo.TabIndex = 111
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Approver Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(332, 109)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 239
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(247, 112)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 237
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tabNewApprover
        '
        Me.tabNewApprover.Controls.Add(Me.tbpMigratedEmployee)
        Me.tabNewApprover.Controls.Add(Me.tbpAssignEmployee)
        Me.tabNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabNewApprover.Location = New System.Drawing.Point(410, 112)
        Me.tabNewApprover.Name = "tabNewApprover"
        Me.tabNewApprover.SelectedIndex = 0
        Me.tabNewApprover.Size = New System.Drawing.Size(342, 344)
        Me.tabNewApprover.TabIndex = 0
        '
        'tbpMigratedEmployee
        '
        Me.tbpMigratedEmployee.Controls.Add(Me.txtNewApproverMigratedEmp)
        Me.tbpMigratedEmployee.Controls.Add(Me.objchkNewCheck)
        Me.tbpMigratedEmployee.Controls.Add(Me.dgNewApproverMigratedEmp)
        Me.tbpMigratedEmployee.Location = New System.Drawing.Point(4, 22)
        Me.tbpMigratedEmployee.Name = "tbpMigratedEmployee"
        Me.tbpMigratedEmployee.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpMigratedEmployee.Size = New System.Drawing.Size(334, 318)
        Me.tbpMigratedEmployee.TabIndex = 0
        Me.tbpMigratedEmployee.Text = "Migrated Employee"
        Me.tbpMigratedEmployee.UseVisualStyleBackColor = True
        '
        'txtNewApproverMigratedEmp
        '
        Me.txtNewApproverMigratedEmp.Flags = 0
        Me.txtNewApproverMigratedEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewApproverMigratedEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtNewApproverMigratedEmp.Location = New System.Drawing.Point(1, 3)
        Me.txtNewApproverMigratedEmp.Name = "txtNewApproverMigratedEmp"
        Me.txtNewApproverMigratedEmp.Size = New System.Drawing.Size(329, 21)
        Me.txtNewApproverMigratedEmp.TabIndex = 126
        '
        'objchkNewCheck
        '
        Me.objchkNewCheck.AutoSize = True
        Me.objchkNewCheck.Location = New System.Drawing.Point(8, 32)
        Me.objchkNewCheck.Name = "objchkNewCheck"
        Me.objchkNewCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkNewCheck.TabIndex = 119
        Me.objchkNewCheck.UseVisualStyleBackColor = True
        '
        'dgNewApproverMigratedEmp
        '
        Me.dgNewApproverMigratedEmp.AllowUserToAddRows = False
        Me.dgNewApproverMigratedEmp.AllowUserToDeleteRows = False
        Me.dgNewApproverMigratedEmp.AllowUserToResizeColumns = False
        Me.dgNewApproverMigratedEmp.AllowUserToResizeRows = False
        Me.dgNewApproverMigratedEmp.BackgroundColor = System.Drawing.Color.White
        Me.dgNewApproverMigratedEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgNewApproverMigratedEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objColhdgMigratedSelect, Me.colhdgMigratedEmpCode, Me.colhdgMigratedEmp})
        Me.dgNewApproverMigratedEmp.Location = New System.Drawing.Point(1, 26)
        Me.dgNewApproverMigratedEmp.Name = "dgNewApproverMigratedEmp"
        Me.dgNewApproverMigratedEmp.RowHeadersVisible = False
        Me.dgNewApproverMigratedEmp.Size = New System.Drawing.Size(330, 289)
        Me.dgNewApproverMigratedEmp.TabIndex = 120
        '
        'objColhdgMigratedSelect
        '
        Me.objColhdgMigratedSelect.HeaderText = ""
        Me.objColhdgMigratedSelect.Name = "objColhdgMigratedSelect"
        Me.objColhdgMigratedSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objColhdgMigratedSelect.Width = 25
        '
        'colhdgMigratedEmpCode
        '
        Me.colhdgMigratedEmpCode.FillWeight = 145.3488!
        Me.colhdgMigratedEmpCode.HeaderText = "Employee Code"
        Me.colhdgMigratedEmpCode.Name = "colhdgMigratedEmpCode"
        Me.colhdgMigratedEmpCode.ReadOnly = True
        Me.colhdgMigratedEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgMigratedEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgMigratedEmpCode.Width = 125
        '
        'colhdgMigratedEmp
        '
        Me.colhdgMigratedEmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhdgMigratedEmp.FillWeight = 54.65117!
        Me.colhdgMigratedEmp.HeaderText = "Employee"
        Me.colhdgMigratedEmp.Name = "colhdgMigratedEmp"
        Me.colhdgMigratedEmp.ReadOnly = True
        Me.colhdgMigratedEmp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgMigratedEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'tbpAssignEmployee
        '
        Me.tbpAssignEmployee.Controls.Add(Me.dgNewApproverAssignEmp)
        Me.tbpAssignEmployee.Controls.Add(Me.txtNewApproverAssignEmp)
        Me.tbpAssignEmployee.Location = New System.Drawing.Point(4, 22)
        Me.tbpAssignEmployee.Name = "tbpAssignEmployee"
        Me.tbpAssignEmployee.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpAssignEmployee.Size = New System.Drawing.Size(334, 318)
        Me.tbpAssignEmployee.TabIndex = 1
        Me.tbpAssignEmployee.Text = "Assigned Employee"
        Me.tbpAssignEmployee.UseVisualStyleBackColor = True
        '
        'dgNewApproverAssignEmp
        '
        Me.dgNewApproverAssignEmp.AllowUserToAddRows = False
        Me.dgNewApproverAssignEmp.AllowUserToDeleteRows = False
        Me.dgNewApproverAssignEmp.AllowUserToResizeColumns = False
        Me.dgNewApproverAssignEmp.AllowUserToResizeRows = False
        Me.dgNewApproverAssignEmp.BackgroundColor = System.Drawing.Color.White
        Me.dgNewApproverAssignEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgNewApproverAssignEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhdgAssignEmpCode, Me.colhdgAssignEmployee})
        Me.dgNewApproverAssignEmp.Location = New System.Drawing.Point(1, 26)
        Me.dgNewApproverAssignEmp.Name = "dgNewApproverAssignEmp"
        Me.dgNewApproverAssignEmp.RowHeadersVisible = False
        Me.dgNewApproverAssignEmp.Size = New System.Drawing.Size(330, 292)
        Me.dgNewApproverAssignEmp.TabIndex = 128
        '
        'colhdgAssignEmpCode
        '
        Me.colhdgAssignEmpCode.FillWeight = 145.3488!
        Me.colhdgAssignEmpCode.HeaderText = "Employee Code"
        Me.colhdgAssignEmpCode.Name = "colhdgAssignEmpCode"
        Me.colhdgAssignEmpCode.ReadOnly = True
        Me.colhdgAssignEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgAssignEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgAssignEmpCode.Width = 125
        '
        'colhdgAssignEmployee
        '
        Me.colhdgAssignEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhdgAssignEmployee.FillWeight = 54.65117!
        Me.colhdgAssignEmployee.HeaderText = "Employee"
        Me.colhdgAssignEmployee.Name = "colhdgAssignEmployee"
        Me.colhdgAssignEmployee.ReadOnly = True
        Me.colhdgAssignEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgAssignEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtNewApproverAssignEmp
        '
        Me.txtNewApproverAssignEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtNewApproverAssignEmp.Flags = 0
        Me.txtNewApproverAssignEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewApproverAssignEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtNewApproverAssignEmp.Location = New System.Drawing.Point(1, 3)
        Me.txtNewApproverAssignEmp.Name = "txtNewApproverAssignEmp"
        Me.txtNewApproverAssignEmp.Size = New System.Drawing.Size(329, 21)
        Me.txtNewApproverAssignEmp.TabIndex = 127
        '
        'txtOldSearchEmployee
        '
        Me.txtOldSearchEmployee.Flags = 0
        Me.txtOldSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOldSearchEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtOldSearchEmployee.Location = New System.Drawing.Point(9, 109)
        Me.txtOldSearchEmployee.Name = "txtOldSearchEmployee"
        Me.txtOldSearchEmployee.Size = New System.Drawing.Size(234, 21)
        Me.txtOldSearchEmployee.TabIndex = 124
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 15)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOldLevel
        '
        Me.objbtnSearchOldLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOldLevel.BorderSelected = False
        Me.objbtnSearchOldLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOldLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchOldLevel.Image = CType(resources.GetObject("objbtnSearchOldLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchOldLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOldLevel.Location = New System.Drawing.Point(333, 82)
        Me.objbtnSearchOldLevel.Name = "objbtnSearchOldLevel"
        Me.objbtnSearchOldLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOldLevel.TabIndex = 121
        '
        'cboOldLevel
        '
        Me.cboOldLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldLevel.DropDownWidth = 300
        Me.cboOldLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldLevel.FormattingEnabled = True
        Me.cboOldLevel.Location = New System.Drawing.Point(89, 82)
        Me.cboOldLevel.Name = "cboOldLevel"
        Me.cboOldLevel.Size = New System.Drawing.Size(236, 21)
        Me.cboOldLevel.TabIndex = 120
        '
        'objbtnSearchOldApprover
        '
        Me.objbtnSearchOldApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOldApprover.BorderSelected = False
        Me.objbtnSearchOldApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOldApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchOldApprover.Image = CType(resources.GetObject("objbtnSearchOldApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchOldApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOldApprover.Location = New System.Drawing.Point(333, 55)
        Me.objbtnSearchOldApprover.Name = "objbtnSearchOldApprover"
        Me.objbtnSearchOldApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOldApprover.TabIndex = 118
        '
        'pnlOldEmp
        '
        Me.pnlOldEmp.Controls.Add(Me.objchkOldCheck)
        Me.pnlOldEmp.Controls.Add(Me.dgOldApproverEmp)
        Me.pnlOldEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOldEmp.Location = New System.Drawing.Point(9, 133)
        Me.pnlOldEmp.Name = "pnlOldEmp"
        Me.pnlOldEmp.Size = New System.Drawing.Size(345, 320)
        Me.pnlOldEmp.TabIndex = 108
        '
        'objchkOldCheck
        '
        Me.objchkOldCheck.AutoSize = True
        Me.objchkOldCheck.Location = New System.Drawing.Point(7, 6)
        Me.objchkOldCheck.Name = "objchkOldCheck"
        Me.objchkOldCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkOldCheck.TabIndex = 118
        Me.objchkOldCheck.UseVisualStyleBackColor = True
        '
        'dgOldApproverEmp
        '
        Me.dgOldApproverEmp.AllowUserToAddRows = False
        Me.dgOldApproverEmp.AllowUserToDeleteRows = False
        Me.dgOldApproverEmp.AllowUserToResizeColumns = False
        Me.dgOldApproverEmp.AllowUserToResizeRows = False
        Me.dgOldApproverEmp.BackgroundColor = System.Drawing.Color.White
        Me.dgOldApproverEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgOldApproverEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhdgSelect, Me.colhdgEmployeecode, Me.colhdgEmployee})
        Me.dgOldApproverEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgOldApproverEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgOldApproverEmp.Name = "dgOldApproverEmp"
        Me.dgOldApproverEmp.RowHeadersVisible = False
        Me.dgOldApproverEmp.Size = New System.Drawing.Size(345, 320)
        Me.dgOldApproverEmp.TabIndex = 119
        '
        'objcolhdgSelect
        '
        Me.objcolhdgSelect.HeaderText = ""
        Me.objcolhdgSelect.Name = "objcolhdgSelect"
        Me.objcolhdgSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhdgSelect.Width = 25
        '
        'colhdgEmployeecode
        '
        Me.colhdgEmployeecode.FillWeight = 145.3488!
        Me.colhdgEmployeecode.HeaderText = "Employee Code"
        Me.colhdgEmployeecode.Name = "colhdgEmployeecode"
        Me.colhdgEmployeecode.ReadOnly = True
        Me.colhdgEmployeecode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgEmployeecode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgEmployeecode.Width = 125
        '
        'colhdgEmployee
        '
        Me.colhdgEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhdgEmployee.FillWeight = 54.65117!
        Me.colhdgEmployee.HeaderText = "Employee"
        Me.colhdgEmployee.Name = "colhdgEmployee"
        Me.colhdgEmployee.ReadOnly = True
        Me.colhdgEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhdgEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'pnlNewEmp
        '
        Me.pnlNewEmp.Location = New System.Drawing.Point(410, 109)
        Me.pnlNewEmp.Name = "pnlNewEmp"
        Me.pnlNewEmp.Size = New System.Drawing.Size(345, 347)
        Me.pnlNewEmp.TabIndex = 108
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnAssign.Location = New System.Drawing.Point(361, 242)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 115
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'objbtnUnAssign
        '
        Me.objbtnUnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnUnAssign.BackgroundImage = CType(resources.GetObject("objbtnUnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnUnAssign.Location = New System.Drawing.Point(361, 288)
        Me.objbtnUnAssign.Name = "objbtnUnAssign"
        Me.objbtnUnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnAssign.TabIndex = 116
        Me.objbtnUnAssign.UseVisualStyleBackColor = True
        '
        'lblNewLevel
        '
        Me.lblNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewLevel.Location = New System.Drawing.Point(408, 85)
        Me.lblNewLevel.Name = "lblNewLevel"
        Me.lblNewLevel.Size = New System.Drawing.Size(77, 15)
        Me.lblNewLevel.TabIndex = 114
        Me.lblNewLevel.Text = "Level"
        Me.lblNewLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNewApprover
        '
        Me.lblNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewApprover.Location = New System.Drawing.Point(408, 58)
        Me.lblNewApprover.Name = "lblNewApprover"
        Me.lblNewApprover.Size = New System.Drawing.Size(77, 15)
        Me.lblNewApprover.TabIndex = 113
        Me.lblNewApprover.Text = "To Approver"
        Me.lblNewApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption1
        '
        Me.lblCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption1.Location = New System.Drawing.Point(6, 58)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(80, 15)
        Me.lblCaption1.TabIndex = 110
        Me.lblCaption1.Text = "From Approver"
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchNewApprover
        '
        Me.objbtnSearchNewApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchNewApprover.BorderSelected = False
        Me.objbtnSearchNewApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchNewApprover.Image = CType(resources.GetObject("objbtnSearchNewApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchNewApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchNewApprover.Location = New System.Drawing.Point(733, 54)
        Me.objbtnSearchNewApprover.Name = "objbtnSearchNewApprover"
        Me.objbtnSearchNewApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchNewApprover.TabIndex = 9
        '
        'cboOldApprover
        '
        Me.cboOldApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldApprover.DropDownWidth = 300
        Me.cboOldApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldApprover.FormattingEnabled = True
        Me.cboOldApprover.Location = New System.Drawing.Point(89, 55)
        Me.cboOldApprover.Name = "cboOldApprover"
        Me.cboOldApprover.Size = New System.Drawing.Size(238, 21)
        Me.cboOldApprover.TabIndex = 2
        '
        'objbtnSearchNewLevel
        '
        Me.objbtnSearchNewLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchNewLevel.BorderSelected = False
        Me.objbtnSearchNewLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchNewLevel.Image = CType(resources.GetObject("objbtnSearchNewLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchNewLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchNewLevel.Location = New System.Drawing.Point(733, 81)
        Me.objbtnSearchNewLevel.Name = "objbtnSearchNewLevel"
        Me.objbtnSearchNewLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchNewLevel.TabIndex = 8
        '
        'cboNewApprover
        '
        Me.cboNewApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewApprover.DropDownWidth = 300
        Me.cboNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewApprover.FormattingEnabled = True
        Me.cboNewApprover.Location = New System.Drawing.Point(491, 55)
        Me.cboNewApprover.Name = "cboNewApprover"
        Me.cboNewApprover.Size = New System.Drawing.Size(236, 21)
        Me.cboNewApprover.TabIndex = 2
        '
        'cboNewLevel
        '
        Me.cboNewLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewLevel.DropDownWidth = 300
        Me.cboNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewLevel.FormattingEnabled = True
        Me.cboNewLevel.Location = New System.Drawing.Point(491, 82)
        Me.cboNewLevel.Name = "cboNewLevel"
        Me.cboNewLevel.Size = New System.Drawing.Size(236, 21)
        Me.cboNewLevel.TabIndex = 2
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 462)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(763, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(553, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(96, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(655, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(96, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.FillWeight = 145.3488!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.FillWeight = 54.65117!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.FillWeight = 145.3488!
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 125
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.FillWeight = 54.65117!
        Me.DataGridViewTextBoxColumn4.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.FillWeight = 145.3488!
        Me.DataGridViewTextBoxColumn5.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 125
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.FillWeight = 54.65117!
        Me.DataGridViewTextBoxColumn6.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'chkShowInactiveApprovers
        '
        Me.chkShowInactiveApprovers.BackColor = System.Drawing.Color.Transparent
        Me.chkShowInactiveApprovers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInactiveApprovers.Location = New System.Drawing.Point(89, 31)
        Me.chkShowInactiveApprovers.Name = "chkShowInactiveApprovers"
        Me.chkShowInactiveApprovers.Size = New System.Drawing.Size(199, 17)
        Me.chkShowInactiveApprovers.TabIndex = 241
        Me.chkShowInactiveApprovers.Text = "Show Inactive Approvers"
        Me.chkShowInactiveApprovers.UseVisualStyleBackColor = False
        '
        'chkShowInActiveEmployees
        '
        Me.chkShowInActiveEmployees.BackColor = System.Drawing.Color.Transparent
        Me.chkShowInActiveEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInActiveEmployees.Location = New System.Drawing.Point(491, 31)
        Me.chkShowInActiveEmployees.Name = "chkShowInActiveEmployees"
        Me.chkShowInActiveEmployees.Size = New System.Drawing.Size(199, 17)
        Me.chkShowInActiveEmployees.TabIndex = 242
        Me.chkShowInActiveEmployees.Text = "Show Inactive Employees"
        Me.chkShowInActiveEmployees.UseVisualStyleBackColor = False
        '
        'frmTransfer_Approver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 517)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransfer_Approver"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Transfer Leave Approvers"
        Me.pnlMain.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.tabNewApprover.ResumeLayout(False)
        Me.tbpMigratedEmployee.ResumeLayout(False)
        Me.tbpMigratedEmployee.PerformLayout()
        CType(Me.dgNewApproverMigratedEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAssignEmployee.ResumeLayout(False)
        Me.tbpAssignEmployee.PerformLayout()
        CType(Me.dgNewApproverAssignEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOldEmp.ResumeLayout(False)
        Me.pnlOldEmp.PerformLayout()
        CType(Me.dgOldApproverEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboNewApprover As System.Windows.Forms.ComboBox
    Friend WithEvents cboNewLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboOldApprover As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchNewApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchNewLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlNewEmp As System.Windows.Forms.Panel
    Friend WithEvents pnlOldEmp As System.Windows.Forms.Panel
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lblNewLevel As System.Windows.Forms.Label
    Friend WithEvents lblNewApprover As System.Windows.Forms.Label
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objchkNewCheck As System.Windows.Forms.CheckBox
    Friend WithEvents objchkOldCheck As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchOldApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents dgOldApproverEmp As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchOldLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOldLevel As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdgSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhdgEmployeecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtOldSearchEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabNewApprover As System.Windows.Forms.TabControl
    Friend WithEvents tbpMigratedEmployee As System.Windows.Forms.TabPage
    Friend WithEvents tbpAssignEmployee As System.Windows.Forms.TabPage
    Friend WithEvents txtNewApproverMigratedEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgNewApproverMigratedEmp As System.Windows.Forms.DataGridView
    Friend WithEvents txtNewApproverAssignEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dgNewApproverAssignEmp As System.Windows.Forms.DataGridView
    Friend WithEvents colhdgAssignEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgAssignEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhdgMigratedSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhdgMigratedEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgMigratedEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents chkShowInactiveApprovers As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowInActiveEmployees As System.Windows.Forms.CheckBox
End Class
