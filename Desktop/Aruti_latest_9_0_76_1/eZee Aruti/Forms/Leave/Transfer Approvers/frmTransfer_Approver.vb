﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmTransfer_Approver

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmTransfer_Approver"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dvOldApproveEmp As DataView
    Private dvNewApprovAssignEmp As DataView
    Private dvNewApprovMigratedEmp As DataView
    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
    Private mstrAdvanceFilter As String = String.Empty
    'Nilay (18-Mar-2015) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region "Form's Event"

    Private Sub frmTransfer_Approver_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransfer_Approver_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objApprover As New clsleaveapprover_master


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
            '                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                                 , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                                                 , True, True, True, False, -1, Nothing, "", "")

            Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                               , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                               , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, chkShowInactiveApprovers.Checked, True, False, -1, Nothing, "", "")

            'Pinkal (14-May-2020) -- End



            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

            Dim dr As DataRow = dtTable.NewRow
            dr("leaveapproverunkid") = 0
            dr("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            cboOldApprover.DisplayMember = "name"
            cboOldApprover.ValueMember = "leaveapproverunkid"
            cboOldApprover.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillOldApproverEmployeeList()
        Try
            If dvOldApproveEmp IsNot Nothing Then

                dgOldApproverEmp.AutoGenerateColumns = False
                dgOldApproverEmp.DataSource = dvOldApproveEmp
                objcolhdgSelect.DataPropertyName = "select"
                colhdgEmployeecode.DataPropertyName = "employeecode"
                colhdgEmployee.DataPropertyName = "employeename"

            End If



            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            If dgNewApproverMigratedEmp.RowCount > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Pinkal (14-May-2020) -- End


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillOldApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillNewApproverAssignEmployeeList()
        Try
            If dvNewApprovAssignEmp IsNot Nothing Then

                dgNewApproverAssignEmp.AutoGenerateColumns = False
                dgNewApproverAssignEmp.DataSource = dvNewApprovAssignEmp
                colhdgAssignEmpCode.DataPropertyName = "employeecode"
                colhdgAssignEmployee.DataPropertyName = "employeename"
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillOldApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillNewApproverMigratedEmployeeList()
        Try
            If dvNewApprovMigratedEmp IsNot Nothing Then

                dgNewApproverMigratedEmp.AutoGenerateColumns = False
                dgNewApproverMigratedEmp.DataSource = dvNewApprovMigratedEmp
                objColhdgMigratedSelect.DataPropertyName = "select"
                colhdgMigratedEmpCode.DataPropertyName = "employeecode"
                colhdgMigratedEmp.DataPropertyName = "employeename"
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            If dgNewApproverMigratedEmp.RowCount > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Pinkal (14-May-2020) -- End


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillNewApproverMigratedEmployeeList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [30 JAN 2016] -- START
    Private Sub GetLeaveApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer, ByVal blnExternalApprover As Boolean)
        'Private Sub GetLeaveApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer)
        'S.SANDEEP [30 JAN 2016] -- END
        Try
            Dim objLeaveApprover As New clsleaveapprover_master
            'S.SANDEEP [30 JAN 2016] -- START
            'Dim dsList As DataSet = objLeaveApprover.GetLevelFromLeaveApprover(True, intApproverID)
            Dim dsList As DataSet = objLeaveApprover.GetLevelFromLeaveApprover(intApproverID, blnExternalApprover, True)
            'S.SANDEEP [30 JAN 2016] -- END

            If CType(sender, ComboBox).Name = "cboNewApprover" Then
                cboNewLevel.DisplayMember = "levelname"
                cboNewLevel.ValueMember = "levelunkid"
                cboNewLevel.DataSource = dsList.Tables(0)

            ElseIf CType(sender, ComboBox).Name = "cboOldApprover" Then
                cboOldLevel.DisplayMember = "levelname"
                cboOldLevel.ValueMember = "levelunkid"
                cboOldLevel.DataSource = dsList.Tables(0)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxOldApproverValue()
        Try
            Dim drRow As DataRow() = dvOldApproveEmp.ToTable.Select("select = true")

            RemoveHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged
            'Nilay (18-Mar-2015) -- Start
            'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
            'If drRow.Length <= 0 Then
            '    objchkOldCheck.CheckState = CheckState.Unchecked
            'ElseIf drRow.Length < dgOldApproverEmp.Rows.Count Then
            '    objchkOldCheck.CheckState = CheckState.Indeterminate
            'ElseIf drRow.Length = dgOldApproverEmp.Rows.Count Then
            '    objchkOldCheck.CheckState = CheckState.Checked
            'End If
            If drRow.Length <= 0 Then
                objchkOldCheck.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvOldApproveEmp.Table.Rows.Count Then
                objchkOldCheck.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvOldApproveEmp.Table.Rows.Count Then
                objchkOldCheck.CheckState = CheckState.Checked
            End If
            dvOldApproveEmp.Table.AcceptChanges()
            'Nilay (18-Mar-2015) -- End
            AddHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxOldApproverValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetCheckBoxNewApproverValue()
        Try
            Dim drRow As DataRow() = dvNewApprovMigratedEmp.ToTable.Select("select = true")

            RemoveHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged

            'Nilay (18-Mar-2015) -- Start
            'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
            'If drRow.Length <= 0 Then
            '    objchkNewCheck.CheckState = CheckState.Unchecked
            'ElseIf drRow.Length < dgNewApproverMigratedEmp.Rows.Count Then
            '    objchkNewCheck.CheckState = CheckState.Indeterminate
            'ElseIf drRow.Length = dgNewApproverMigratedEmp.Rows.Count Then
            '    objchkNewCheck.CheckState = CheckState.Checked
            'End If
            If drRow.Length <= 0 Then
                objchkNewCheck.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvNewApprovMigratedEmp.Table.Rows.Count Then
                objchkNewCheck.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvNewApprovMigratedEmp.Table.Rows.Count Then
                objchkNewCheck.CheckState = CheckState.Checked
            End If
            dvNewApprovMigratedEmp.Table.AcceptChanges()
            'Nilay (18-Mar-2015) -- End
            AddHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxNewApproverValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchOldApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldApprover.DataSource, DataTable)
            With cboOldApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewApprover.DataSource, DataTable)
            With cboNewApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewLevel.DataSource, DataTable)
            With cboNewLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOldLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldLevel.DataSource, DataTable)
            With cboOldLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try

            If dvOldApproveEmp Is Nothing Then
                GoTo warning
            End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Exit Sub
            End If

            Dim drRow() As DataRow = dvOldApproveEmp.Table.Select("select = true")

            If drRow.Length <= 0 Then
warning:        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee to migrate."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim blnFlag As Boolean = False
            Dim blnPendingFlag As Boolean = False
            If dgOldApproverEmp.RowCount <= 0 Then Exit Sub
            Dim dtMigratedEmployee As DataTable = Nothing



            If drRow.Length > 0 Then


                'Pinkal (01-Feb-2014) -- Start
                'Enhancement : TRA Changes
                Dim dtGetExistData As DataTable = Nothing
                'Pinkal (01-Feb-2014) -- End


                For i As Integer = 0 To drRow.Length - 1


                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes [ COMMENT FROM MR. ANDREW FOR REMVOING ON 01-JAN-2013]

                    'Dim objLeaveForm As New clsleaveform
                    'If objLeaveForm.GetApproverPendingLeaveFormCount(CInt(cboOldApprover.Tag), drRow(i)("employeeunkid").ToString()) > 0 Then
                    '    blnPendingFlag = True
                    '    Continue For
                    'End If

                    'Pinkal (18-Dec-2012) -- End


                    If dvNewApprovAssignEmp IsNot Nothing AndAlso dvNewApprovAssignEmp.Table.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dvNewApprovAssignEmp.Table.Select("employeeunkid=" & drRow(i)("employeeunkid").ToString())
                        If dRow.Length > 0 Then
                            blnFlag = True

                            'Pinkal (01-Feb-2014) -- Start
                            'Enhancement : TRA Changes

                            If dtGetExistData Is Nothing Then
                                dtGetExistData = dvNewApprovAssignEmp.Table().Clone
                            End If
                            dtGetExistData.ImportRow(drRow(i))
                            Continue For
                            'Pinkal (01-Feb-2014) -- End

                        End If

                    End If

                    drRow(i)("select") = False


                    'Pinkal (19-Mar-2015) -- Start
                    'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.
                    If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) Then Continue For
                    'Pinkal (19-Mar-2015) -- End

                    If dvNewApprovMigratedEmp Is Nothing Then
                        dtMigratedEmployee = dvOldApproveEmp.Table().Clone
                        dtMigratedEmployee.ImportRow(drRow(i))
                        dvNewApprovMigratedEmp = dtMigratedEmployee.DefaultView
                    Else
                        'Pinkal (22-Jun-2017) -- Start
                        'Enhancement -Bug Solved For Claim Approver Migration .
                        Dim dr As DataRow() = dvNewApprovMigratedEmp.Table.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
                        If dr.Length <= 0 Then
                        dvNewApprovMigratedEmp.Table.ImportRow(drRow(i))
                    End If
                        'Pinkal (22-Jun-2017) -- End
                    End If

                    dvOldApproveEmp.Table.Rows.Remove(drRow(i))
                Next

                'dvOldApproveEmp.Table.AcceptChanges()
                objchkOldCheck.Checked = False

                If blnFlag Then

                    'Pinkal (18-Jun-2013) -- Start
                    'Enhancement : TRA Changes
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the checked employee is already binded with the selected approver and will not be added to the migration list."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Some of the checked employee is already linked with the selected approver."), enMsgBoxStyle.Information)
                    'Pinkal (18-Jun-2013) -- End


                    'Pinkal (01-Feb-2014) -- Start
                    'Enhancement : TRA Changes

                    dtGetExistData.Rows.Clear()
                    dtGetExistData = Nothing

                    'Pinkal (01-Feb-2014) -- End

                    'ElseIf blnPendingFlag Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You cannot Migrate this approver to another approver.Reason : Some of the employee(s) have pending leave form for this approver."), enMsgBoxStyle.Information)
                End If

                FillNewApproverMigratedEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try

            If dvNewApprovMigratedEmp Is Nothing Then
                GoTo Warning
            End If

            Dim drRow() As DataRow = dvNewApprovMigratedEmp.Table.Select("select = true")

            If drRow.Length <= 0 Then
Warning:        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dgNewApproverMigratedEmp.RowCount <= 0 Then Exit Sub
            Dim dtOldApproverEmployee As DataTable = Nothing
            If drRow.Length > 0 Then
                'Nilay (18-Mar-2015) -- Start
                'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
                Call objAlloacationReset.PerformClick()
                'Nilay (18-Mar-2015) -- End
                For i As Integer = 0 To drRow.Length - 1

                    drRow(i)("select") = False

                    If dvOldApproveEmp Is Nothing Then
                        dtOldApproverEmployee = dvNewApprovMigratedEmp.Table().Clone
                        dtOldApproverEmployee.ImportRow(drRow(i))
                        dvOldApproveEmp = dtOldApproverEmployee.DefaultView
                    Else
                        dvOldApproveEmp.Table.ImportRow(drRow(i))
                    End If

                    dvNewApprovMigratedEmp.Table.Rows.Remove(drRow(i))
                Next
                dvOldApproveEmp.Table.AcceptChanges()
                objchkNewCheck.Checked = False
                FillOldApproverEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select From Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Exit Sub

            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Exit Sub

            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Exit Sub

            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Exit Sub

            ElseIf dgNewApproverMigratedEmp.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no employee(s) for migration."), enMsgBoxStyle.Information)
                dgNewApproverMigratedEmp.Focus()
                Exit Sub

            End If


            Dim objApproverTran As New clsleaveapprover_Tran
            Me.Cursor = Cursors.WaitCursor


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If objApproverTran.Migration_Insert(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), dvNewApprovMigratedEmp.Table() _
            ', CInt(cboNewApprover.Tag), CInt(cboNewApprover.SelectedValue), ConfigParameter._Object._PaymentApprovalwithLeaveApproval) = False Then
            If objApproverTran.Migration_Insert(FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), dvNewApprovMigratedEmp.Table() _
                                                            , CInt(cboNewApprover.Tag), CInt(cboNewApprover.SelectedValue), ConfigParameter._Object._PaymentApprovalwithLeaveApproval, chkShowInActiveEmployees.Checked) = False Then
                'Pinkal (12-Oct-2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Problem in Approver Migration."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver Migration done successfully."), enMsgBoxStyle.Information)
                cboOldApprover.SelectedIndex = 0
                cboOldLevel.SelectedIndex = 0
                cboNewApprover.SelectedIndex = 0
                cboNewLevel.SelectedIndex = 0
                cboOldApprover.Select()
                dvOldApproveEmp.Table.Clear()
                dvNewApprovAssignEmp.Table.Clear()
                dvNewApprovMigratedEmp.Table.Clear()
                FillCombo()

                'Pinkal (14-May-2020) -- Start
                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                chkShowInactiveApprovers.Checked = False
                chkShowInActiveEmployees.Checked = False
                chkShowInActiveEmployees.Enabled = True
                'Pinkal (14-May-2020) -- End

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            txtOldSearchEmployee.Text = ""
            mstrAdvanceFilter = ""
            If dvOldApproveEmp IsNot Nothing Then
                dvOldApproveEmp.RowFilter = mstrAdvanceFilter
            End If
            'dvOldApproveEmp.Table.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (18-Mar-2015) -- End


#End Region

#Region "ComboBox Event"

    Private Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            Dim objLeaveApprover As New clsleaveapprover_master



            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            'Dim dsList As DataSet = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
            '                                                                         , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                                         , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
            '                                                                     , True, True, True, False, -1, Nothing _
            '                                                                         , "", "")

            Dim dsList As DataSet = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                      , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                      , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                     , True, chkShowInactiveApprovers.Checked, True, False, -1, Nothing, "", "")

            'Pinkal (14-May-2020) -- End




            Dim dtTable As DataTable = Nothing
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                dtTable = dsList.Tables(0)
            Else
                Dim strApproverUnkids As String = ""
                strApproverUnkids = objLeaveApprover.GetLeaveApproverUnkid(CInt(cboOldApprover.SelectedValue), CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                dtTable = New DataView(dsList.Tables(0), "approverunkid NOT IN(" & strApproverUnkids & ")", "", DataViewRowState.CurrentRows).ToTable()
            End If

            dtTable = dtTable.DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

            Dim dr As DataRow = dtTable.NewRow
            dr("leaveapproverunkid") = 0
            dr("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            cboNewApprover.DisplayMember = "name"
            cboNewApprover.ValueMember = "leaveapproverunkid"
            cboNewApprover.DataSource = dtTable


            GetLeaveApproverLevel(sender, CInt(cboOldApprover.SelectedValue), CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboOldLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Try
            Dim objLeaveApprover As New clsleaveapprover_master


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'Dim dsOldApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue) _
            '                                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

            Dim dsOldApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(FinancialYear._Object._DatabaseName, CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue) _
                                                                                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                                                     , CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")), chkShowInActiveEmployees.Checked)
            'Pinkal (14-May-2020) -- End



            dvOldApproveEmp = dsOldApproverEmp.Tables(0).DefaultView
            dvOldApproveEmp.RowFilter = mstrAdvanceFilter
            objchkOldCheck.Checked = False
            FillOldApproverEmployeeList()

            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'If dsOldApproverEmp IsNot Nothing AndAlso dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
            '    cboOldApprover.Tag = CInt(dsOldApproverEmp.Tables(0).Rows(0)("approverunkid"))
            'Else
            '    cboOldApprover.Tag = Nothing
            'End If

            If dsOldApproverEmp IsNot Nothing Then
                If dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
                cboOldApprover.Tag = CInt(dsOldApproverEmp.Tables(0).Rows(0)("approverunkid"))
            Else
                    cboOldApprover.Tag = CInt(objLeaveApprover.GetApproverID(CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue)))
                End If
            Else
                cboOldApprover.Tag = Nothing
            End If
            'Pinkal (14-May-2020) -- End

           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            'S.SANDEEP [30 JAN 2016] -- START
            'GetLeaveApproverLevel(sender, CInt(cboNewApprover.SelectedValue))
            GetLeaveApproverLevel(sender, CInt(cboNewApprover.SelectedValue), CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            'S.SANDEEP [30 JAN 2016] -- END


            'Pinkal (19-Mar-2015) -- Start
            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

            'Dim objLeaveApprover As New clsleaveapprover_master
            'Dim dsNewApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(CInt(cboNewApprover.SelectedValue), -1)
            'dvNewApprovAssignEmp = dsNewApproverEmp.Tables(0).DefaultView
            'objchkNewCheck.Checked = False
            'FillNewApproverAssignEmployeeList()

            'If CInt(cboNewApprover.SelectedValue) <= 0 Then
            '    If dvNewApprovMigratedEmp IsNot Nothing Then dvNewApprovMigratedEmp.Table.Rows.Clear()
            'End If

            'Pinkal (19-Mar-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNewLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        Try
            Dim objLeaveApprover As New clsleaveapprover_master


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            'Dim dsNewApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue) _
            '                                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

            Dim dsNewApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(FinancialYear._Object._DatabaseName, CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue) _
                                                                                                                                      , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                                                      , CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")), chkShowInActiveEmployees.Checked)

            'Pinkal (14-May-2020) -- End



            dvNewApprovAssignEmp = dsNewApproverEmp.Tables(0).DefaultView
            objchkNewCheck.Checked = False
            FillNewApproverAssignEmployeeList()

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                If dvNewApprovMigratedEmp IsNot Nothing Then dvNewApprovMigratedEmp.Table.Rows.Clear()
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'If dsNewApproverEmp IsNot Nothing AndAlso dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
            '    cboNewApprover.Tag = CInt(dsNewApproverEmp.Tables(0).Rows(0)("approverunkid"))
            'Else
            '    cboNewApprover.Tag = Nothing
            'End If
            If dsNewApproverEmp IsNot Nothing Then
                If dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
                cboNewApprover.Tag = CInt(dsNewApproverEmp.Tables(0).Rows(0)("approverunkid"))
            Else
                    cboNewApprover.Tag = CInt(objLeaveApprover.GetApproverID(CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue)))
                End If
            Else
                cboNewApprover.Tag = Nothing
            End If
            'Pinkal (14-May-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkOldCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldCheck.CheckedChanged
        Try
            If dvOldApproveEmp.Table.Rows.Count > 0 Then
                'Pinkal (15-Jun-2015) -- Start
                'Enhancement - WORKED ON ALLOCATION PROBLEM WHEN MIGRATING APPROVERS IN FINCA TANZANIA
                'For Each dr As DataRowView In dvOldApproveEmp
                '    RemoveHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                '    dr("select") = objchkOldCheck.Checked
                '    AddHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                'Next

                For Each dr As DataRowView In dvOldApproveEmp
                    RemoveHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                    dr("select") = objchkOldCheck.Checked
                    AddHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                Next

                'Pinkal (15-Jun-2015) -- End

                dvOldApproveEmp.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkNewCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewCheck.CheckedChanged
        Try
            If dvNewApprovMigratedEmp.Table.Rows.Count > 0 Then
                'Pinkal (15-Jun-2015) -- Start
                'Enhancement - WORKED ON ALLOCATION PROBLEM WHEN MIGRATING APPROVERS IN FINCA TANZANIA.
                'For Each dr As DataRow In dvNewApprovMigratedEmp.Table.Rows
                '    RemoveHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                '    dr("select") = objchkNewCheck.Checked
                '    AddHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                'Next
                For Each dr As DataRowView In dvNewApprovMigratedEmp
                    RemoveHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                    dr("select") = objchkNewCheck.Checked
                    AddHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                Next
                'Pinkal (15-Jun-2015) -- End
                dvNewApprovMigratedEmp.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (14-May-2020) -- Start
    'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

    Private Sub chkShowInactiveApprovers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInactiveApprovers.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            chkShowInActiveEmployees.Checked = False
            chkShowInActiveEmployees.Enabled = True
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInactiveApprovers_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub chkShowInActiveEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployees.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            cboOldLevel_SelectedIndexChanged(cboOldLevel, New EventArgs())
            cboNewLevel_SelectedIndexChanged(cboNewLevel, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInActiveEmployees_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Pinkal (14-May-2020) -- End


#End Region

#Region "DataGridView Event"

    Private Sub dgOldApproverEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOldApproverEmp.CellContentClick, dgOldApproverEmp.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhdgSelect.Index Then

                If dgOldApproverEmp.IsCurrentCellDirty Then
                    dgOldApproverEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxOldApproverValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldApproverEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

    Private Sub dgNewApproverMigratedEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgNewApproverMigratedEmp.CellContentClick, dgNewApproverMigratedEmp.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objColhdgMigratedSelect.Index Then

                If dgNewApproverMigratedEmp.IsCurrentCellDirty Then
                    dgNewApproverMigratedEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxNewApproverValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgNewApproverMigratedEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtOldSearchEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOldSearchEmployee.TextChanged
        Try
            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If dvOldApproveEmp Is Nothing Then Exit Sub
            'Pinkal (01-Feb-2014) -- End
            dvOldApproveEmp.RowFilter = "employeecode like '%" & txtOldSearchEmployee.Text.Trim & "%' or employeename like '%" & txtOldSearchEmployee.Text.Trim & "%'"
            'Nilay (18-Mar-2015) -- Start
            'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvOldApproveEmp.RowFilter &= "AND " & mstrAdvanceFilter
            End If
            'Nilay (18-Mar-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtOldSearchEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverAssignEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverAssignEmp.TextChanged
        Try

            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If dvNewApprovAssignEmp Is Nothing Then Exit Sub
            'Pinkal (01-Feb-2014) -- End

            dvNewApprovAssignEmp.RowFilter = "employeecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverAssignEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverAssignEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverMigratedEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverMigratedEmp.TextChanged
        Try

            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If dvNewApprovMigratedEmp Is Nothing Then Exit Sub
            'Pinkal (01-Feb-2014) -- End

            dvNewApprovMigratedEmp.RowFilter = "employeecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverMigratedEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
#Region "Link Allocation"
    Private Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "From Leave Approver is compulsory information.Please select From Approver."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Exit Sub
            End If
            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Level is compulsory information.Please select Level."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Exit Sub
            End If

            Dim frm As New frmAdvanceSearch

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (15-Jun-2015) -- Start
            'Enhancement - WORKED ON ALLOCATION PROBLEM WHEN MIGRATING APPROVERS IN FINCA TANZANIA.
            objchkOldCheck.Checked = False
            'Pinkal (15-Jun-2015) -- End
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString.Replace("ADF.", "")
                dvOldApproveEmp.RowFilter = mstrAdvanceFilter
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_Click", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (18-Mar-2015) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUnAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblNewLevel.Text = Language._Object.getCaption(Me.lblNewLevel.Name, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Language._Object.getCaption(Me.lblNewApprover.Name, Me.lblNewApprover.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.colhdgEmployeecode.HeaderText = Language._Object.getCaption(Me.colhdgEmployeecode.Name, Me.colhdgEmployeecode.HeaderText)
            Me.colhdgEmployee.HeaderText = Language._Object.getCaption(Me.colhdgEmployee.Name, Me.colhdgEmployee.HeaderText)
            Me.tbpMigratedEmployee.Text = Language._Object.getCaption(Me.tbpMigratedEmployee.Name, Me.tbpMigratedEmployee.Text)
            Me.tbpAssignEmployee.Text = Language._Object.getCaption(Me.tbpAssignEmployee.Name, Me.tbpAssignEmployee.Text)
            Me.colhdgAssignEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmpCode.Name, Me.colhdgAssignEmpCode.HeaderText)
            Me.colhdgAssignEmployee.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmployee.Name, Me.colhdgAssignEmployee.HeaderText)
            Me.colhdgMigratedEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmpCode.Name, Me.colhdgMigratedEmpCode.HeaderText)
            Me.colhdgMigratedEmp.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmp.Name, Me.colhdgMigratedEmp.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.chkShowInactiveApprovers.Text = Language._Object.getCaption(Me.chkShowInactiveApprovers.Name, Me.chkShowInactiveApprovers.Text)
			Me.chkShowInActiveEmployees.Text = Language._Object.getCaption(Me.chkShowInActiveEmployees.Name, Me.chkShowInActiveEmployees.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s).")
            Language.setMessage(mstrModuleName, 4, "Please check atleast one employee to migrate.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Some of the checked employee is already linked with the selected approver.")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one employee to unassign.")
            Language.setMessage(mstrModuleName, 7, "Problem in Approver Migration.")
            Language.setMessage(mstrModuleName, 8, "Approver Migration done successfully.")
            Language.setMessage(mstrModuleName, 9, "Please Select From Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 10, "There is no employee(s) for migration.")
            Language.setMessage(mstrModuleName, 11, "From Leave Approver is compulsory information.Please select From Approver.")
            Language.setMessage(mstrModuleName, 12, "Level is compulsory information.Please select Level.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class