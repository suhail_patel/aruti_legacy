﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmHoliday_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmHoliday_AddEdit"
    Private mblnCancel As Boolean = True
    Private objholidayMaster As clsholiday_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintHolidayMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintHolidayMasterUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintHolidayMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmHoliday_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objholidayMaster = New clsholiday_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()

            'Pinkal (19-Jan-2016) -- Start
            'Enhancement - Working on Changes in close Year For Voltamp problem for transfering employee Assigned shift.
            ' dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date
            Dim objMstPeriod As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim mintPeriodId As Integer = objMstPeriod.getFirstPeriodID(enModuleReference.Payroll,FinancialYear._Object._YearUnkid, 0 )
            dtpApplyDate.MinDate = objPeriod.GetTnA_StartDate(mintPeriodId, enModuleReference.Payroll)
            objPeriod = Nothing
            objMstPeriod = Nothing
            'Pinkal (19-Jan-2016) -- End


            'Pinkal (09-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
            'dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date
            'Pinkal (09-Jul-2014) -- End

            If menAction = enAction.EDIT_ONE Then
                objholidayMaster._Holidayunkid = mintHolidayMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboLeaveYear.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHoliday_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHoliday_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHoliday_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHoliday_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHoliday_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHoliday_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objholidayMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsholiday_master.SetMessages()
            objfrm._Other_ModuleNames = "clsholiday_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Dropdown's Event"

    'Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveYear.SelectedIndexChanged
    '    Try
    '        'FillPeriod()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            'If CInt(cboLeaveYear.SelectedValue) = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
            '    cboLeaveYear.Focus()
                '    Exit Sub

            'Pinkal (21-Jul-2014) -- End

            If Trim(txtHolidayName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Holiday Name cannot be blank. Holiday Name is required information."), enMsgBoxStyle.Information)
                txtHolidayName.Focus()
                Exit Sub
            ElseIf objlblColor.BackColor.ToArgb = 0 Or objlblColor.BackColor = Color.Empty Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Color is compulsory information. Please Select Color."), enMsgBoxStyle.Information)
                objbtnFillColor.Select()
                Exit Sub
            Else
                'START CHECK FOR COLOR
                Dim objMaster As New clsMasterData
                If objMaster.IsColorExist(objlblColor.BackColor.ToArgb, mintHolidayMasterUnkid) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Color is used by another transaction.Please select other color for this holiday."), enMsgBoxStyle.Information)
                    objbtnFillColor.Select()
                    Exit Sub
                End If
                'END CHECK FOR COLOR
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objholidayMaster.Update()
            Else
                blnFlag = objholidayMaster.Insert()
            End If

            If blnFlag = False And objholidayMaster._Message <> "" Then
                eZeeMsgBox.Show(objholidayMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objholidayMaster = Nothing
                    objholidayMaster = New clsholiday_master
                    Call GetValue()
                    cboLeaveYear.Select()
                Else
                    mintHolidayMasterUnkid = objholidayMaster._Holidayunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            Call objFrmLangPopup.displayDialog(txtHolidayName.Text.Trim, objholidayMaster._Holidayname1, objholidayMaster._Holidayname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnFillColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnFillColor.Click
        If ColorDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
        objlblColor.BackColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboLeaveYear.BackColor = GUI.ColorComp
            cboLeaveYear.BackColor = GUI.ColorComp
            txtHolidayName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'cboLeaveYear.SelectedValue = objholidayMaster._Yearunkid
            'Pinkal (21-Jul-2014) -- End

            If objholidayMaster._Holidayunkid > 0 Then
                dtpApplyDate.Value = objholidayMaster._Holidaydate
            End If

            txtHolidayName.Text = objholidayMaster._Holidayname
            If CInt(HolidayType.Recurrent) = objholidayMaster._Holidaytypeid Then
                radRecurrent.Checked = True
            ElseIf CInt(HolidayType.OnceEffect) = objholidayMaster._Holidaytypeid Then
                radOnceEffect.Checked = True
            End If
            objlblColor.BackColor = Color.FromArgb(objholidayMaster._Color)
            txtDescription.Text = objholidayMaster._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'objholidayMaster._Yearunkid = CInt(cboLeaveYear.SelectedValue)
            'Pinkal (21-Jul-2014) -- End

            objholidayMaster._Holidaydate = dtpApplyDate.Value
            objholidayMaster._Holidayname = txtHolidayName.Text.Trim
            If radRecurrent.Checked Then
                objholidayMaster._Holidaytypeid = CInt(HolidayType.Recurrent)
            ElseIf radOnceEffect.Checked Then
                objholidayMaster._Holidaytypeid = CInt(HolidayType.OnceEffect)
            End If
            objholidayMaster._Color = objlblColor.BackColor.ToArgb
            objholidayMaster._Description = txtDescription.Text.Trim

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsYear As New DataSet
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            'dsYear = objMaster.getComboListPAYYEAR("Year", True)
            'With cboLeaveYear
            '    .ValueMember = "Id"
            '    .DisplayMember = "NAME"
            '    .DataSource = dsYear.Tables("Year")
            '    .SelectedValue = 0
            'End With

            'Pinkal (21-Jul-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbHolidayInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbHolidayInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbHolidayInfo.Text = Language._Object.getCaption(Me.gbHolidayInfo.Name, Me.gbHolidayInfo.Text)
			Me.lblLeaveYear.Text = Language._Object.getCaption(Me.lblLeaveYear.Name, Me.lblLeaveYear.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblColor.Text = Language._Object.getCaption(Me.lblColor.Name, Me.lblColor.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.radOnceEffect.Text = Language._Object.getCaption(Me.radOnceEffect.Name, Me.radOnceEffect.Text)
			Me.radRecurrent.Text = Language._Object.getCaption(Me.radRecurrent.Name, Me.radRecurrent.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year.")
			Language.setMessage(mstrModuleName, 2, "Holiday Name cannot be blank. Holiday Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Color is compulsory information. Please Select Color.")
			Language.setMessage(mstrModuleName, 4, "This Color is used by another transaction.Please select other color for this holiday.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class