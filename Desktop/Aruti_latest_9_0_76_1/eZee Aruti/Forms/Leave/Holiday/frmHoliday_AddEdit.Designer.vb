﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHoliday_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHoliday_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbHolidayInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.radOnceEffect = New System.Windows.Forms.RadioButton
        Me.radRecurrent = New System.Windows.Forms.RadioButton
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnFillColor = New eZee.Common.eZeeGradientButton
        Me.lblColor = New System.Windows.Forms.Label
        Me.objlblColor = New System.Windows.Forms.Label
        Me.txtHolidayName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.dtpApplyDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.cboLeaveYear = New System.Windows.Forms.ComboBox
        Me.lblLeaveYear = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.Panel1.SuspendLayout()
        Me.gbHolidayInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbHolidayInfo)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.cboLeaveYear)
        Me.Panel1.Controls.Add(Me.lblLeaveYear)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(399, 273)
        Me.Panel1.TabIndex = 0
        '
        'gbHolidayInfo
        '
        Me.gbHolidayInfo.BorderColor = System.Drawing.Color.Black
        Me.gbHolidayInfo.Checked = False
        Me.gbHolidayInfo.CollapseAllExceptThis = False
        Me.gbHolidayInfo.CollapsedHoverImage = Nothing
        Me.gbHolidayInfo.CollapsedNormalImage = Nothing
        Me.gbHolidayInfo.CollapsedPressedImage = Nothing
        Me.gbHolidayInfo.CollapseOnLoad = False
        Me.gbHolidayInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbHolidayInfo.Controls.Add(Me.radOnceEffect)
        Me.gbHolidayInfo.Controls.Add(Me.radRecurrent)
        Me.gbHolidayInfo.Controls.Add(Me.lblDescription)
        Me.gbHolidayInfo.Controls.Add(Me.txtDescription)
        Me.gbHolidayInfo.Controls.Add(Me.objbtnFillColor)
        Me.gbHolidayInfo.Controls.Add(Me.lblColor)
        Me.gbHolidayInfo.Controls.Add(Me.objlblColor)
        Me.gbHolidayInfo.Controls.Add(Me.txtHolidayName)
        Me.gbHolidayInfo.Controls.Add(Me.lblName)
        Me.gbHolidayInfo.Controls.Add(Me.dtpApplyDate)
        Me.gbHolidayInfo.Controls.Add(Me.lblDate)
        Me.gbHolidayInfo.ExpandedHoverImage = Nothing
        Me.gbHolidayInfo.ExpandedNormalImage = Nothing
        Me.gbHolidayInfo.ExpandedPressedImage = Nothing
        Me.gbHolidayInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHolidayInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHolidayInfo.HeaderHeight = 25
        Me.gbHolidayInfo.HeaderMessage = ""
        Me.gbHolidayInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbHolidayInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHolidayInfo.HeightOnCollapse = 0
        Me.gbHolidayInfo.LeftTextSpace = 0
        Me.gbHolidayInfo.Location = New System.Drawing.Point(11, 9)
        Me.gbHolidayInfo.Name = "gbHolidayInfo"
        Me.gbHolidayInfo.OpenHeight = 300
        Me.gbHolidayInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHolidayInfo.ShowBorder = True
        Me.gbHolidayInfo.ShowCheckBox = False
        Me.gbHolidayInfo.ShowCollapseButton = False
        Me.gbHolidayInfo.ShowDefaultBorderColor = True
        Me.gbHolidayInfo.ShowDownButton = False
        Me.gbHolidayInfo.ShowHeader = True
        Me.gbHolidayInfo.Size = New System.Drawing.Size(374, 200)
        Me.gbHolidayInfo.TabIndex = 9
        Me.gbHolidayInfo.Temp = 0
        Me.gbHolidayInfo.Text = "Holiday Info"
        Me.gbHolidayInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(339, 83)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 7
        '
        'radOnceEffect
        '
        Me.radOnceEffect.Checked = True
        Me.radOnceEffect.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOnceEffect.Location = New System.Drawing.Point(213, 57)
        Me.radOnceEffect.Name = "radOnceEffect"
        Me.radOnceEffect.Size = New System.Drawing.Size(112, 17)
        Me.radOnceEffect.TabIndex = 3
        Me.radOnceEffect.TabStop = True
        Me.radOnceEffect.Text = "Once Effect"
        Me.radOnceEffect.UseVisualStyleBackColor = True
        '
        'radRecurrent
        '
        Me.radRecurrent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radRecurrent.Location = New System.Drawing.Point(95, 57)
        Me.radRecurrent.Name = "radRecurrent"
        Me.radRecurrent.Size = New System.Drawing.Size(112, 17)
        Me.radRecurrent.TabIndex = 2
        Me.radRecurrent.Text = "Recurrent"
        Me.radRecurrent.UseVisualStyleBackColor = True
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(9, 133)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(79, 15)
        Me.lblDescription.TabIndex = 235
        Me.lblDescription.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(94, 128)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(269, 61)
        Me.txtDescription.TabIndex = 6
        '
        'objbtnFillColor
        '
        Me.objbtnFillColor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnFillColor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnFillColor.BorderSelected = False
        Me.objbtnFillColor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnFillColor.Image = Global.Aruti.Main.My.Resources.Resources.Misc_16
        Me.objbtnFillColor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnFillColor.Location = New System.Drawing.Point(340, 107)
        Me.objbtnFillColor.Name = "objbtnFillColor"
        Me.objbtnFillColor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnFillColor.TabIndex = 8
        '
        'lblColor
        '
        Me.lblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColor.Location = New System.Drawing.Point(10, 108)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(79, 15)
        Me.lblColor.TabIndex = 232
        Me.lblColor.Text = "Color"
        '
        'objlblColor
        '
        Me.objlblColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColor.Location = New System.Drawing.Point(95, 105)
        Me.objlblColor.Name = "objlblColor"
        Me.objlblColor.Size = New System.Drawing.Size(239, 18)
        Me.objlblColor.TabIndex = 5
        '
        'txtHolidayName
        '
        Me.txtHolidayName.Flags = 0
        Me.txtHolidayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHolidayName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHolidayName.Location = New System.Drawing.Point(94, 79)
        Me.txtHolidayName.Name = "txtHolidayName"
        Me.txtHolidayName.Size = New System.Drawing.Size(239, 21)
        Me.txtHolidayName.TabIndex = 4
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(9, 83)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(79, 15)
        Me.lblName.TabIndex = 229
        Me.lblName.Text = "Name"
        '
        'dtpApplyDate
        '
        Me.dtpApplyDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplyDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplyDate.Location = New System.Drawing.Point(94, 31)
        Me.dtpApplyDate.Name = "dtpApplyDate"
        Me.dtpApplyDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpApplyDate.TabIndex = 1
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(9, 33)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(79, 15)
        Me.lblDate.TabIndex = 226
        Me.lblDate.Text = "Date"
        '
        'cboLeaveYear
        '
        Me.cboLeaveYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveYear.FormattingEnabled = True
        Me.cboLeaveYear.Location = New System.Drawing.Point(93, 39)
        Me.cboLeaveYear.Name = "cboLeaveYear"
        Me.cboLeaveYear.Size = New System.Drawing.Size(112, 21)
        Me.cboLeaveYear.TabIndex = 1
        Me.cboLeaveYear.Visible = False
        '
        'lblLeaveYear
        '
        Me.lblLeaveYear.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveYear.Location = New System.Drawing.Point(8, 42)
        Me.lblLeaveYear.Name = "lblLeaveYear"
        Me.lblLeaveYear.Size = New System.Drawing.Size(79, 15)
        Me.lblLeaveYear.TabIndex = 224
        Me.lblLeaveYear.Text = "Leave Year"
        Me.lblLeaveYear.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 218)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(399, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(187, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(290, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmHoliday_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 273)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHoliday_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Holiday"
        Me.Panel1.ResumeLayout(False)
        Me.gbHolidayInfo.ResumeLayout(False)
        Me.gbHolidayInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbHolidayInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLeaveYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveYear As System.Windows.Forms.Label
    Friend WithEvents dtpApplyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtHolidayName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents objlblColor As System.Windows.Forms.Label
    Friend WithEvents objbtnFillColor As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents radOnceEffect As System.Windows.Forms.RadioButton
    Friend WithEvents radRecurrent As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
End Class
