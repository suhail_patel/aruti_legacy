﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmHolidaysList

#Region "Private Variable"

    Private objHolidayMaster As clsholiday_master
    Private ReadOnly mstrModuleName As String = "frmHolidaysList"

#End Region

#Region "Form's Event"

    Private Sub frmHolidaysList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objHolidayMaster = New clsholiday_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date
            'Pinkal (21-Jul-2014) -- End


            FillCombo()
            fillList()
            If lvHolidayList.Items.Count > 0 Then lvHolidayList.Items(0).Selected = True
            lvHolidayList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidaysList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidaysList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvHolidayList.Focused Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidaysList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidaysList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objHolidayMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsholiday_master.SetMessages()
            objfrm._Other_ModuleNames = "clsholiday_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveYear.SelectedIndexChanged
        Try
            FillPeriod()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmHoliday_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvHolidayList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Holiday from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvHolidayList.Select()
            Exit Sub
        End If
        Dim objfrmHoliday_AddEdit As New frmHoliday_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvHolidayList.SelectedItems(0).Index
            If objfrmHoliday_AddEdit.displayDialog(CInt(lvHolidayList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frmAssignCategory_AddEdit = Nothing
            If lvHolidayList.Items.Count > 0 Then
                lvHolidayList.Items(intSelectedIndex).Selected = True
                lvHolidayList.EnsureVisible(intSelectedIndex)
            End If
            lvHolidayList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmHoliday_AddEdit IsNot Nothing Then objfrmHoliday_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvHolidayList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Holiday from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvHolidayList.Select()
            Exit Sub
        End If
        If objHolidayMaster.isUsed(CInt(lvHolidayList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Holiday. Reason: This Holiday is in use."), enMsgBoxStyle.Information) '?2
            lvHolidayList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvHolidayList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Holiday?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objHolidayMaster.Delete(CInt(lvHolidayList.SelectedItems(0).Tag))
                lvHolidayList.SelectedItems(0).Remove()

                If lvHolidayList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvHolidayList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvHolidayList.Items.Count - 1
                    lvHolidayList.Items(intSelectedIndex).Selected = True
                    lvHolidayList.EnsureVisible(intSelectedIndex)
                ElseIf lvHolidayList.Items.Count <> 0 Then
                    lvHolidayList.Items(intSelectedIndex).Selected = True
                    lvHolidayList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvHolidayList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLeaveYear.SelectedIndex = 0
            cboLeavePeriod.SelectedIndex = 0


            'Pinkal (03-Jan-2011) -- START

            If FinancialYear._Object._Database_End_Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpApplyDate.Value = FinancialYear._Object._Database_End_Date.Date
            ElseIf FinancialYear._Object._Database_End_Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            'Pinkal (03-Jan-2011) -- END

            dtpApplyDate.Checked = False
            txtHolidayName.Text = ""
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsHolidayList As New DataSet
        Dim strSearching As String = ""
        Dim dtHolidayList As New DataTable
        Try

            If User._Object.Privilege._AllowToViewHolidayList = True Then    'Pinkal (02-Jul-2012) -- Start

                dsHolidayList = objHolidayMaster.GetList("Holiday")

                If dtpApplyDate.Checked = True Then
                    strSearching = "AND holidaydate ='" & eZeeDate.convertDate(dtpApplyDate.Value) & "' "
                End If


                If CInt(cboLeaveYear.SelectedValue) > 0 Then
                    strSearching &= "AND yearunkid =" & CInt(cboLeaveYear.SelectedValue) & " "
                End If

                If CInt(cboLeavePeriod.SelectedValue) > 0 Then
                    strSearching &= "AND periodunkid =" & CInt(cboLeavePeriod.SelectedValue) & " "
                End If


                If txtHolidayName.Text.Trim.Length > 0 Then
                    strSearching &= "AND holidayname like '%" & txtHolidayName.Text.Trim & "%' "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtHolidayList = New DataView(dsHolidayList.Tables("Holiday"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtHolidayList = dsHolidayList.Tables("Holiday")
                End If


                Dim lvItem As ListViewItem

                lvHolidayList.Items.Clear()
                For Each drRow As DataRow In dtHolidayList.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("yearname").ToString
                    lvItem.Tag = drRow("holidayunkid")
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(drRow("holidayname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvHolidayList.Items.Add(lvItem)
                Next

                If lvHolidayList.Items.Count > 10 Then
                    colhDescription.Width = 307 - 18
                Else
                    colhDescription.Width = 307
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsHolidayList.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()

        'Sandeep [ 09 Oct 2010 ] -- Start
        'Issues Reported by Vimal
        Dim objMaster As New clsMasterData
        Dim dsYear As New DataSet
        'Sandeep [ 09 Oct 2010 ] -- End 

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'Dim dsYear As DataSet = objHolidayMaster.getListForCombo("Year", True)
            'cboLeaveYear.DisplayMember = "name"
            'cboLeaveYear.ValueMember = "yearunkid"
            'cboLeaveYear.DataSource = dsYear.Tables(0)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsYear = objMaster.getComboListPAYYEAR("Year", True)
            dsYear = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboLeaveYear
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsYear.Tables("Year")
                .SelectedValue = 0
            End With
            'Sandeep [ 09 Oct 2010 ] -- End 

        Catch ex As Exception
            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sandeep [ 09 Oct 2010 ] -- End 
        End Try
    End Sub

    Private Sub FillPeriod()
        Try
            Dim objPeriod As New clscommom_period_Tran
            Dim dsPeriod As DataSet = Nothing
            If CInt(cboLeaveYear.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Leave), CInt(cboLeaveYear.SelectedValue), "Period", False)
                dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Leave), CInt(cboLeaveYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Leave), CInt(cboLeaveYear.SelectedValue), "Period", True)
                dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Leave), CInt(cboLeaveYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
                'Sohail (21 Aug 2015) -- End
            End If
            cboLeavePeriod.ValueMember = "periodunkid"
            cboLeavePeriod.DisplayMember = "name"
            cboLeavePeriod.DataSource = dsPeriod.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddHolidays
            btnEdit.Enabled = User._Object.Privilege._EditHolidays
            btnDelete.Enabled = User._Object.Privilege._DeleteHolidays
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLeaveYear.Text = Language._Object.getCaption(Me.lblLeaveYear.Name, Me.lblLeaveYear.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.colhLeaveYear.Text = Language._Object.getCaption(CStr(Me.colhLeaveYear.Tag), Me.colhLeaveYear.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.lblLeavePeriod.Text = Language._Object.getCaption(Me.lblLeavePeriod.Name, Me.lblLeavePeriod.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Holiday from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Holiday. Reason: This Holiday is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Holiday?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class