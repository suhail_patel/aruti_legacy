﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

'Last Message Index = 6

Public Class frmImportAccrueLeave

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportAccrueLeave"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Dim objAccrue As clsleavebalance_tran
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmImportAccrueLeave_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objAccrue = New clsleavebalance_tran
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'objIExcel = New ExcelData
            'S.SANDEEP [12-Jan-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAccrueLeave_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillGirdView()
        Try

            If dsList IsNot Nothing Then

                dsList.Tables(0).Columns.Add("ischeck", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("accruesettingId", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("accruesetting", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("error", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("leavetypeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("ispaid", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("isshortleave", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("image", Type.GetType("System.Object"))


                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objLeave As New clsleavetype_master

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        dsList.Tables(0).Rows(i)("ischeck") = False
                        dsList.Tables(0).Rows(i)("accruesettingId") = -1
                        dsList.Tables(0).Rows(i)("accruesetting") = ""
                        dsList.Tables(0).Rows(i)("error") = ""
                        dsList.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1).Clone
                        dsList.Tables(0).Rows(i)("employeeunkid") = objEmp.GetEmployeeUnkid("", dsList.Tables(0).Rows(i)("employeecode").ToString.Trim)
                        dsList.Tables(0).Rows(i)("leavetypeunkid") = objLeave.GetLeaveTypeUnkId(dsList.Tables(0).Rows(i)("leavename").ToString.Trim)
                        objLeave._Leavetypeunkid = CInt(dsList.Tables(0).Rows(i)("leavetypeunkid"))
                        dsList.Tables(0).Rows(i)("ispaid") = objLeave._IsPaid
                        dsList.Tables(0).Rows(i)("isshortleave") = objLeave._IsShortLeave


                        'Pinkal (16-Dec-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                        If objLeave._IsShortLeave = False Then

                            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                                If dsList.Tables(0).Rows(i)("accrue_amount").ToString = "" Then
                                    dsList.Tables(0).Rows(i)("accrue_amount") = 0
                                End If
                            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                                If dsList.Tables(0).Rows(i)("monthly_accrue").ToString = "" Then
                                    dsList.Tables(0).Rows(i)("monthly_accrue") = 0
                                End If
                            End If

                        Else
                            If dsList.Tables(0).Rows(i)("accrue_amount").ToString = "" Then
                                dsList.Tables(0).Rows(i)("accrue_amount") = 0
                            End If
                        End If

                        'Pinkal (16-Dec-2016) -- End

                        If dsList.Tables(0).Rows(i)("Forward_amount").ToString = "" Then
                            dsList.Tables(0).Rows(i)("Forward_amount") = 0
                        End If


                        'Pinkal (15-Nov-2013) -- Start
                        'Enhancement : Oman Changes

                        If dsList.Tables(0).Rows(i)("eligibilityafter").ToString = "" Then
                            dsList.Tables(0).Rows(i)("eligibilityafter") = 0
                        End If

                        If dsList.Tables(0).Columns.Contains("CFamount") AndAlso dsList.Tables(0).Rows(i)("CFamount").ToString = "" Then
                            dsList.Tables(0).Rows(i)("CFamount") = 0
                        End If

                        If dsList.Tables(0).Columns.Contains("isnoaction") AndAlso dsList.Tables(0).Rows(i)("isnoaction").ToString = "" Then
                            dsList.Tables(0).Rows(i)("isnoaction") = False
                        End If

                        If dsList.Tables(0).Columns.Contains("consecutivedays") AndAlso dsList.Tables(0).Rows(i)("consecutivedays").ToString = "" Then
                            dsList.Tables(0).Rows(i)("consecutivedays") = 0
                        End If

                        If dsList.Tables(0).Columns.Contains("occurrence") AndAlso dsList.Tables(0).Rows(i)("occurrence").ToString = "" Then
                            dsList.Tables(0).Rows(i)("occurrence") = 0
                        End If

                        If dsList.Tables(0).Columns.Contains("islifetime") AndAlso dsList.Tables(0).Rows(i)("islifetime").ToString = "" Then
                            dsList.Tables(0).Rows(i)("islifetime") = False
                        End If

                        'Pinkal (15-Nov-2013) -- End


                    Next

                End If

            End If

            DgAccrueLeave.AutoGenerateColumns = False

            DgAccrueLeave.DataSource = dsList.Tables(0)

            colhImage.DataPropertyName = "image"
            colhEmployeecode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employee"
            colhLeave.DataPropertyName = "leavename"
            colhStartdate.DataPropertyName = "startdate"
            colhEnddate.DataPropertyName = "enddate"

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                colhAccrue_amount.DataPropertyName = "accrue_amount"
                dgcolhMonthlyAccrue.Visible = False
            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                dgcolhMonthlyAccrue.DataPropertyName = "monthly_accrue"
                colhAccrue_amount.Visible = False
            End If
            'Pinkal (16-Dec-2016) -- End


            colhForwardAmt.DataPropertyName = "Forward_amount"
            objcolhEmployeeId.DataPropertyName = "employeeunkid"
            objcolhLeavetypeId.DataPropertyName = "leavetypeunkid"
            objcolhIscheck.DataPropertyName = "ischeck"
            objColhAccrueSettingId.DataPropertyName = "accruesettingId"
            colhAccrueSetting.DataPropertyName = "accruesetting"
            colhMessage.DataPropertyName = "error"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'dsList = objIExcel.Import(txtFilePath.Text)
                dsList = OpenXML_Import(txtFilePath.Text)
                'S.SANDEEP [12-Jan-2018] -- END



                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                Dim strQuery As String = ""
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    dsList.Tables(0).Columns(i).ColumnName = dsList.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & dsList.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = dsList.Tables(0).Select(strQuery)

                If drRow.Length > 0 Then

                    For j As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(j))
                    Next
                    dsList.Tables(0).AcceptChanges()

                End If

                'Pinkal (12-Oct-2011) -- End

                FillGirdView()
                Cursor = Cursors.Default
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try


            Dim blnError As Boolean = False


            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            'If radIssuebal.Checked = False And radExceedBal.Checked = False And radDonotIssue.Checked = False Then
            If radIssuebal.Checked = False AndAlso radExceedBal.Checked = False AndAlso radDonotIssue.Checked = False AndAlso radDonotIssueAsonDate.Checked = False Then
                'Pinkal (16-Dec-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Select Leave Accrue Setting."), enMsgBoxStyle.Information)
                Exit Sub

            ElseIf radApplytoAll.Checked = False And radApplytoChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Option to set the value."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If DgAccrueLeave.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to import accrue leave."), enMsgBoxStyle.Information)
                Exit Sub
            Else

                Dim DrRow As DataRow() = dsList.Tables(0).Select("accruesettingId <= 0")
                If DrRow.Length = dsList.Tables(0).Rows.Count Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Set Accrue setting for employee(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


                If chkShortLeave.Checked AndAlso (dsList.Tables(0).Columns.Contains("consecutivedays") = False OrElse dsList.Tables(0).Columns.Contains("occurrence") = False OrElse dsList.Tables(0).Columns.Contains("islifetime") = False) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You can't import this file.Reason : This is not Short Leave File Format.Please check file format."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                ElseIf chkShortLeave.Checked = False AndAlso (dsList.Tables(0).Columns.Contains("CFamount") = False OrElse dsList.Tables(0).Columns.Contains("isnoaction") = False) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You can't import this file.Reason : This is not Accrued Leave File Format.Please check file format."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If


                Dim objAccrue As clsleavebalance_tran = Nothing

                Dim intIndex As Integer = 0
                For Each dr As DataRow In dsList.Tables(0).Rows

                    If CInt(dr("accruesettingId")) <= 0 Then Continue For

                    objAccrue = New clsleavebalance_tran

                    If IsDBNull(dr("startdate")) Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ErrorText = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("enddate")) = False AndAlso (CDate(dr("enddate")) < CDate(dr("startdate"))) Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ErrorText = Language.getMessage(mstrModuleName, 3, "Invalid Start Date.")
                        DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhStartdate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 3, "Invalid Start Date.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("employeeunkid")) <= 0 Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployeecode.Index).ErrorText = Language.getMessage(mstrModuleName, 4, "Invalid Employee Code.")
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployeecode.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhEmployeecode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 4, "Invalid Employee Code.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    ElseIf CInt(dr("leavetypeunkid")) <= 0 Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ErrorText = Language.getMessage(mstrModuleName, 5, "Invalid Leave Name.")
                        DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Leave Name.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For

                    End If


                    'Pinkal (16-Dec-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                    If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                        If dsList.Tables(0).Columns.Contains("monthly_accrue") Then
                            If CDec(dr("monthly_accrue")) <= 0 Then
                                DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ErrorText = Language.getMessage(mstrModuleName, 18, "Monthlty Accrue is compulsory information.")
                                DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhLeave.Index).ErrorText
                                dr("error") = Language.getMessage(mstrModuleName, 18, "Monthlty Accrue is compulsory information.")
                                dr("Image") = imgError
                                intIndex += 1
                                Continue For
                            End If
                        End If

                    End If

                    'Pinkal (16-Dec-2016) -- End



                    objAccrue._Employeeunkid = CInt(dr("employeeunkid"))
                    objAccrue._Leavetypeunkid = CInt(dr("leavetypeunkid"))

                    'START FOR CHECK WHETHER EMPLOYEE ISSUE LEAVE OR NOT
                    Dim objIssue As New clsleaveissue_Tran
                    Dim IsssueCount As Decimal = objIssue.GetEmployeeIssueDaysCount(objAccrue._Employeeunkid, objAccrue._Leavetypeunkid, FinancialYear._Object._YearUnkid)

                    If IsssueCount > 0 Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployee.Index).ErrorText = Language.getMessage(mstrModuleName, 6, "This Employee has been issued leave.So this employee cannot be imported.")
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployee.Index).ToolTipText = DgAccrueLeave.Rows(intIndex).Cells(colhEmployee.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 6, "This Employee has been issued leave.So this employee cannot be imported.")
                        dr("Image") = imgError
                        intIndex += 1
                        Continue For
                    End If

                    'END FOR CHECK WHETHER EMPLOYEE ISSUE LEAVE OR NOT



                    objAccrue._Yearunkid = FinancialYear._Object._YearUnkid
                    objAccrue._Ispaid = CBool(dr("ispaid"))

                    'Pinkal (16-Dec-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                    'objAccrue._ActualAmount = CDec(dr("accrue_amount"))
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objAccrue._Employeeunkid
                    'Pinkal (16-Dec-2016) -- End

                    objAccrue._IsshortLeave = CBool(dr("isshortleave"))


                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                        If CDate(dr("startdate")).Date > FinancialYear._Object._Database_Start_Date Then
                            objAccrue._Startdate = CDate(dr("startdate")).Date
                        Else
                            objAccrue._Startdate = FinancialYear._Object._Database_Start_Date
                        End If

                        If IsDBNull(dr("enddate")) Then
                            If objAccrue._Ispaid = False Then
                                objAccrue._Enddate = Nothing
                            Else
                                objAccrue._Enddate = FinancialYear._Object._Database_End_Date
                            End If

                        Else

                            If CDate(dr("enddate")).Date < FinancialYear._Object._Database_End_Date Then
                                objAccrue._Enddate = CDate(dr("enddate")).Date
                            Else
                                objAccrue._Enddate = FinancialYear._Object._Database_End_Date
                            End If

                        End If

                        objAccrue._IsOpenELC = False
                        objAccrue._IsELC = False

                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then


                        'Pinkal (16-Dec-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                        'Dim objEmployee As New clsEmployee_Master
                        'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objAccrue._Employeeunkid
                        'Pinkal (16-Dec-2016) -- End

                        If IsDBNull(dr("startdate")) Then

                            If objEmployee._Appointeddate.Date <> Nothing Then
                                objAccrue._Startdate = objEmployee._Appointeddate.Date
                            End If
                            If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                                objAccrue._Startdate = objEmployee._Reinstatementdate.Date
                            End If

                        Else
                            objAccrue._Startdate = CDate(dr("startdate")).Date
                        End If


                        If IsDBNull(dr("enddate")) Then

                            If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < objAccrue._Startdate.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                                objAccrue._Enddate = objEmployee._Termination_To_Date
                            Else
                                objAccrue._Enddate = objAccrue._Startdate.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                            End If

                        Else
                            objAccrue._Enddate = CDate(dr("enddate")).Date
                        End If

                        objAccrue._IsOpenELC = True
                        objAccrue._IsELC = True


                    End If

                    If chkShortLeave.Checked = False Then

                        Dim intDiff As Integer = 0

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                            'Pinkal (16-Dec-2016) -- Start
                            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                                objAccrue._Daily_Amount = CDec(dr("accrue_amount")) / CInt(DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date.AddDays(1)))

                                If objAccrue._Enddate = Nothing Then
                                    intDiff = CInt(DateDiff(DateInterval.Day, objAccrue._Startdate, FinancialYear._Object._Database_End_Date.AddDays(1)))
                                Else
                                    intDiff = CInt(DateDiff(DateInterval.Day, objAccrue._Startdate, objAccrue._Enddate.AddDays(1)))
                                End If

                            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                                objAccrue._Daily_Amount = 0

                                If objAccrue._Enddate = Nothing Then
                                    intDiff = CInt(DateDiff(DateInterval.Month, objAccrue._Startdate, FinancialYear._Object._Database_End_Date.AddDays(1)))
                                Else
                                    intDiff = CInt(DateDiff(DateInterval.Month, objAccrue._Startdate, objAccrue._Enddate.AddDays(1)))
                                End If

                            End If


                            'Pinkal (16-Dec-2016) -- End


                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then


                            'Pinkal (16-Dec-2016) -- Start
                            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then

                                objAccrue._Daily_Amount = CDec(dr("accrue_amount")) / CInt(DateDiff(DateInterval.Day, objAccrue._Startdate, objAccrue._Enddate.AddDays(1)))

                                If objAccrue._Enddate = Nothing Then
                                    intDiff = CInt(DateDiff(DateInterval.Day, objAccrue._Startdate.Date, objAccrue._Startdate.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1)))
                                Else
                                    intDiff = CInt(DateDiff(DateInterval.Day, objAccrue._Startdate, objAccrue._Enddate.AddDays(1)))
                                End If

                            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                                objAccrue._Daily_Amount = 0

                                If objAccrue._Enddate = Nothing Then
                                    intDiff = CInt(DateDiff(DateInterval.Month, objAccrue._Startdate, objAccrue._Startdate.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1)))
                                Else
                                    intDiff = CInt(DateDiff(DateInterval.Month, objAccrue._Startdate, objAccrue._Enddate.AddDays(1)))
                                End If

                            End If
                            'Pinkal (16-Dec-2016) -- End

                        End If


                        'Pinkal (16-Dec-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                        If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                            objAccrue._AccrueAmount = objAccrue._Daily_Amount * intDiff
                            objAccrue._Monthly_Accrue = 0
                            objAccrue._ActualAmount = CDec(dr("accrue_amount"))

                        ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
CheckMonthDiff:
                                    'Pinkal (16-Feb-2017) -- Start
                                    'Enhancement - Worked on ASP Monthly Leave Accrue Bug.
                                    '  If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > objAccrue._Startdate.Date.Day Then
                                    If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > objAccrue._Enddate.Date.Day Then
                                        'Pinkal (16-Feb-2017) -- End
                                        intDiff = intDiff - 1
                                    End If
                                End If
                            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                GoTo CheckMonthDiff
                            End If

                            objAccrue._AccrueAmount = CDec(dr("monthly_accrue")) * intDiff

                            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                objAccrue._ActualAmount = CDec(dr("monthly_accrue")) * CInt(DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date.AddDays(1)))

                            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                objAccrue._ActualAmount = CDec(dr("monthly_accrue")) * CInt(DateDiff(DateInterval.Month, objAccrue._Startdate.Date, objAccrue._Startdate.Date.AddMonths(ConfigParameter._Object._ELCmonths)))
                            End If

                            objAccrue._Monthly_Accrue = CDec(dr("monthly_accrue"))

                        End If

                        'objAccrue._AccrueAmount += CInt(dr("Forward_amount"))
                        objAccrue._AccrueAmount += CDec(dr("Forward_amount"))
                        'Pinkal (16-Dec-2016) -- End


                        objAccrue._LeaveBF = CDec(dr("Forward_amount"))
                        objAccrue._UptoLstYr_AccrueAmout = objAccrue._UptoLstYr_AccrueAmout + objAccrue._AccrueAmount
                        objAccrue._Remaining_Balance = objAccrue._AccrueAmount
                        objAccrue._IsNoAction = CBool(dr("isnoaction"))

                        If objAccrue._IsNoAction Then
                            objAccrue._CFlvAmount = 0
                        Else
                            objAccrue._CFlvAmount = CDec(dr("CFamount"))
                        End If

                    Else

                        '===============================================
                        ' IN SHORT LEAVE CONSIDER FORWARD AMOUNT AS ISSUE AMOUNT
                        '===============================================

                        Dim msblIssuamount As Decimal = CDec(IIf(CDec(dr("Forward_amount")) < 0, CDec(dr("Forward_amount")) * (-1), CDec(dr("Forward_amount"))))

                        objAccrue._AccrueAmount = CDec(dr("accrue_amount"))

                        'Pinkal (29-Jan-2021)-- Start
                        'Bug - Worked on Bug Resolved for SPPS.
                        objAccrue._ActualAmount = CDec(dr("accrue_amount"))
                        'Pinkal (29-Jan-2021) -- End

                        objAccrue._UptoLstYr_AccrueAmout = CDec(dr("accrue_amount"))
                        objAccrue._IssueAmount = msblIssuamount
                        objAccrue._UptoLstYr_IssueAmout = msblIssuamount
                        objAccrue._Remaining_Balance = objAccrue._AccrueAmount - objAccrue._IssueAmount
                        objAccrue._Consicutivedays = CInt(dr("consecutivedays"))
                        objAccrue._Occurance = CInt(dr("occurrence"))

                        If CBool(dr("islifetime")) Then
                            objAccrue._Optiondid = enLeaveFreq_Factors.LVF_LIFE_TIME
                        Else
                            objAccrue._Optiondid = enLeaveFreq_Factors.LVF_FINANCIAL_YEAR
                        End If

                    End If

                    objAccrue._EligibilityAfter = CInt(dr("eligibilityafter"))
                    objAccrue._Userunkid = User._Object._Userunkid
                    objAccrue._AccrueSetting = CInt(dr("accruesettingId"))

                    'Pinkal (26-May-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE,ADVANCE FILTER,ANALYSIS BY AND USER ACCESS.
                    objAccrue._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                    'Pinkal (26-May-2015) -- End


                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                    'objAccrue.Insert()
                    objAccrue.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True)
                    'Pinkal (24-Aug-2015) -- End



                    If objAccrue._Message <> "" Then
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployee.Index).ErrorText = objAccrue._Message
                        DgAccrueLeave.Rows(intIndex).Cells(colhEmployee.Index).ToolTipText = objAccrue._Message
                        dr("error") = objAccrue._Message
                        dr("Image") = imgError
                        blnError = True
                        Continue For
                    End If

                    dr("Image") = imgAccept
                    intIndex += 1
                Next

                Dim drError As DataRow() = dsList.Tables(0).Select("error <> '' ")

                If drError.Length > 0 Then

                    If drError.Length < dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Some Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub

                    ElseIf drError.Length = dsList.Tables(0).Rows.Count Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Data did not import due to error."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub

                    End If
                End If

                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Data successfully imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtFilePath.Text = ""
                    dsList.Tables(0).Rows.Clear()
                    If chkShortLeave.Checked Then
                        chkShortLeave.Checked = False
                        radIssuebal.Checked = False
                    End If
                End If

                DgAccrueLeave.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnImportData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click
        Try


            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            'If radIssuebal.Checked = False And radExceedBal.Checked = False And radDonotIssue.Checked = False Then
            If radIssuebal.Checked = False AndAlso radExceedBal.Checked = False AndAlso radDonotIssue.Checked = False AndAlso radDonotIssueAsonDate.Checked = False Then
                'Pinkal (16-Dec-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Select Leave Accrue Setting."), enMsgBoxStyle.Information)
                Exit Sub

            ElseIf radApplytoAll.Checked = False And radApplytoChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Option to set the value."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If DgAccrueLeave.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no data to set the accrue Setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If radApplytoAll.Checked Then

                If dsList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsList.Tables(0).Rows

                        If radIssuebal.Checked Then

                            dr("accruesettingId") = CInt(enAccrueSetting.Issue_Balance)
                            dr("accruesetting") = radIssuebal.Text

                        ElseIf radExceedBal.Checked Then

                            dr("accruesettingId") = CInt(enAccrueSetting.Exceeding_balance_Unpaid)
                            dr("accruesetting") = radExceedBal.Text

                        ElseIf radDonotIssue.Checked Then
                            dr("accruesettingId") = CInt(enAccrueSetting.Donot_Issue_On_Exceeding_Balance)
                            dr("accruesetting") = radDonotIssue.Text


                            'Pinkal (16-Dec-2016) -- Start
                            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                        ElseIf radDonotIssueAsonDate.Checked Then
                            dr("accruesettingId") = CInt(enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date)
                            dr("accruesetting") = radDonotIssueAsonDate.Text
                            'Pinkal (16-Dec-2016) -- End

                        End If

                    Next

                End If

            ElseIf radApplytoChecked.Checked Then

                Dim drRow As DataRow() = dsList.Tables(0).Select("ischeck = true")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please check atleast one Employee to assign accrue setting."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                For Each dr As DataRow In drRow

                    If radIssuebal.Checked Then

                        dr("accruesettingId") = CInt(enAccrueSetting.Issue_Balance)
                        dr("accruesetting") = radIssuebal.Text

                    ElseIf radExceedBal.Checked Then

                        dr("accruesettingId") = CInt(enAccrueSetting.Exceeding_balance_Unpaid)
                        dr("accruesetting") = radExceedBal.Text

                    ElseIf radDonotIssue.Checked Then

                        dr("accruesettingId") = CInt(enAccrueSetting.Donot_Issue_On_Exceeding_Balance)
                        dr("accruesetting") = radDonotIssue.Text

                        'Pinkal (16-Dec-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                    ElseIf radDonotIssueAsonDate.Checked Then
                        dr("accruesettingId") = CInt(enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date)
                        dr("accruesetting") = radDonotIssueAsonDate.Text
                        'Pinkal (16-Dec-2016) -- End

                    End If
                    dr("ischeck") = False
                Next

            End If
            dsList.Tables(0).AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                'Dim dsList As DataSet = objAccrue.GetImportFileStructure(False)
                Dim dsList As DataSet = objAccrue.GetImportFileStructure(False, ConfigParameter._Object._LeaveAccrueTenureSetting)
                'Pinkal (16-Dec-2016) -- End

                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'objIExcel.Export(objSave.FileName, dsList)
                OpenXML_Export(objSave.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    'Pinkal (09-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If DgAccrueLeave.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dsList IsNot Nothing Then
                Dim dvGriddata As DataView = dsList.Tables(0).DefaultView
                dvGriddata.RowFilter = "error <>''"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("ischeck")
                        dtTable.Columns.Remove("accruesettingId")
                        dtTable.Columns.Remove("employeeunkid")
                        dtTable.Columns.Remove("leavetypeunkid")
                        dtTable.Columns.Remove("ispaid")
                        dtTable.Columns.Remove("isshortleave")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Leave Balance") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (09-Nov-2013) -- End


    'Pinkal (15-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Private Sub btnGetShortLeaveFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetShortLeaveFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                'Dim dsList As DataSet = objAccrue.GetImportFileStructure(True)
                Dim dsList As DataSet = objAccrue.GetImportFileStructure(True, ConfigParameter._Object._LeaveAccrueTenureSetting)
                'Pinkal (16-Dec-2016) -- End

                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'objIExcel.Export(objSave.FileName, dsList)
                OpenXML_Export(objSave.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END

            End If
        Catch ex As Exception
            DisplayError.Show("1-", ex.Message, "btnGetShortLeaveFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-Nov-2013) -- End



#End Region

#Region "CheckBox Event"

    Private Sub chkShortLeave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShortLeave.CheckedChanged
        Try
            If chkShortLeave.Checked Then
                radIssuebal.Checked = True
                radIssuebal.Enabled = False
                radExceedBal.Enabled = False
                radDonotIssue.Enabled = False

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                radDonotIssueAsonDate.Enabled = False
                'Pinkal (16-Dec-2016) -- End

            Else
                radIssuebal.Checked = False
                radIssuebal.Enabled = True
                radExceedBal.Enabled = True
                radDonotIssue.Enabled = True
                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                radDonotIssueAsonDate.Enabled = True
                'Pinkal (16-Dec-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShortLeave_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.Name, Me.btnGetFileFormat.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.radExceedBal.Text = Language._Object.getCaption(Me.radExceedBal.Name, Me.radExceedBal.Text)
            Me.radIssuebal.Text = Language._Object.getCaption(Me.radIssuebal.Name, Me.radIssuebal.Text)
            Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
            Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
            Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.chkShortLeave.Text = Language._Object.getCaption(Me.chkShortLeave.Name, Me.chkShortLeave.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.radDonotIssue.Text = Language._Object.getCaption(Me.radDonotIssue.Name, Me.radDonotIssue.Text)
            Me.btnGetShortLeaveFileFormat.Text = Language._Object.getCaption(Me.btnGetShortLeaveFileFormat.Name, Me.btnGetShortLeaveFileFormat.Text)
            Me.radDonotIssueAsonDate.Text = Language._Object.getCaption(Me.radDonotIssueAsonDate.Name, Me.radDonotIssueAsonDate.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeecode.HeaderText = Language._Object.getCaption(Me.colhEmployeecode.Name, Me.colhEmployeecode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhLeave.HeaderText = Language._Object.getCaption(Me.colhLeave.Name, Me.colhLeave.HeaderText)
            Me.colhAccrueSetting.HeaderText = Language._Object.getCaption(Me.colhAccrueSetting.Name, Me.colhAccrueSetting.HeaderText)
            Me.colhStartdate.HeaderText = Language._Object.getCaption(Me.colhStartdate.Name, Me.colhStartdate.HeaderText)
            Me.colhEnddate.HeaderText = Language._Object.getCaption(Me.colhEnddate.Name, Me.colhEnddate.HeaderText)
            Me.colhAccrue_amount.HeaderText = Language._Object.getCaption(Me.colhAccrue_amount.Name, Me.colhAccrue_amount.HeaderText)
            Me.dgcolhMonthlyAccrue.HeaderText = Language._Object.getCaption(Me.dgcolhMonthlyAccrue.Name, Me.dgcolhMonthlyAccrue.HeaderText)
            Me.colhForwardAmt.HeaderText = Language._Object.getCaption(Me.colhForwardAmt.Name, Me.colhForwardAmt.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to import accrue leave.")
            Language.setMessage(mstrModuleName, 2, "Start date is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Invalid Start Date.")
            Language.setMessage(mstrModuleName, 4, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 5, "Invalid Leave Name.")
            Language.setMessage(mstrModuleName, 6, "This Employee has been issued leave.So this employee cannot be imported.")
            Language.setMessage(mstrModuleName, 7, "Data successfully imported.")
            Language.setMessage(mstrModuleName, 8, "Please Select Leave Accrue Setting.")
            Language.setMessage(mstrModuleName, 9, "Please Select Option to set the value.")
            Language.setMessage(mstrModuleName, 10, "There is no data to set the accrue Setting.")
            Language.setMessage(mstrModuleName, 11, "Please check atleast one Employee to assign accrue setting.")
            Language.setMessage(mstrModuleName, 12, "Some Data did not import due to error.")
            Language.setMessage(mstrModuleName, 13, "Data did not import due to error.")
            Language.setMessage(mstrModuleName, 14, "Please Set Accrue setting for employee(s).")
            Language.setMessage(mstrModuleName, 15, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 16, "You can't import this file.Reason : This is not Short Leave File Format.Please check file format.")
            Language.setMessage(mstrModuleName, 17, "You can't import this file.Reason : This is not Accrued Leave File Format.Please check file format.")
            Language.setMessage(mstrModuleName, 18, "Monthlty Accrue is compulsory information.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class