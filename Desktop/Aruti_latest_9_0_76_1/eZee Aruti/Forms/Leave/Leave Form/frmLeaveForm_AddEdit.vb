﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

'Last Message Index = 12

Public Class frmLeaveForm_AddEdit

#Region "Property"

    Public Property _IsFromLeaveProcess() As Boolean
        Get
            Return isFromLeaveProcess
        End Get
        Set(ByVal value As Boolean)
            isFromLeaveProcess = value
        End Set
    End Property

#End Region

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLeaveForm_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLeaveForm As clsleaveform
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLeaveFormUnkid As Integer = -1
    Private dtFraction As DataTable
    Private dtExpense As DataTable
    Private isFromLeaveProcess As Boolean = False
    Private objClaimRequestMst As clsclaim_request_master = Nothing
    Private mintClaimRequestMasterId As Integer = -1
    Private mdtAttachement As DataTable = Nothing

    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mstrFolderName As String = ""
    Dim mdtClaimAttachment As DataTable = Nothing

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private intEligibleExpenseDays As Integer = 0
    'Pinkal (29-Sep-2017) -- End

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnLvTypeSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End

    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnIsLvTypePaid As Boolean = False
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    'Pinkal (20-Nov-2018) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    'Pinkal (07-Mar-2019) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnConsiderExpense As Boolean = True
    'Pinkal (03-May-2019) -- End


    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Dim mstrP2PToken As String = ""
    'Pinkal (29-Aug-2019) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLeaveFormUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintLeaveFormUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeaveForm_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveForm = New clsleaveform

        'Pinkal (06-Jan-2016) -- Start
        'Enhancement - Working on Changes in SS for Leave Module.
        objClaimRequestMst = New clsclaim_request_master
        'Pinkal (06-Jan-2016) -- End

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            SetProcessVisiblity()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objLeaveForm._Formunkid = mintLeaveFormUnkid
            End If
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                LblELCStart.Visible = True
                LblELCStart.Text = ""
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                LblELCStart.Visible = False
            End If

            FillCombo()
            GetValue()
            cboEmployee.Select()

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            'mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LEAVEFORMS).Tables(0).Rows(0)("Name").ToString
            'Shani (20-Aug-2016) -- End
            'SHANI (16 JUL 2015) -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveForm_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveForm_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveForm_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveForm_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveForm_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveForm_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveForm = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveform.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveform"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLeaveType.Click
        Dim frm As New frmLeaveType_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objLeaveType As New clsleavetype_master
                dsList = objLeaveType.GetList("LVTYP", True)
                With cboLeaveType
                    .ValueMember = "leavetypeunkid"
                    .DisplayMember = "leavename"

                    Dim drRow As DataRow = dsList.Tables("LVTYP").NewRow
                    drRow("leavetypeunkid") = 0
                    drRow("leavename") = Language.getMessage(mstrModuleName, 8, "Select")
                    dsList.Tables("LVTYP").Rows.InsertAt(drRow, 0)

                    .DataSource = dsList.Tables("LVTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objLeaveType = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf ConfigParameter._Object._LeaveFormNoType = 0 And txtFormNo.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Form No cannot be blank. Form No is required information."), enMsgBoxStyle.Information)
                txtFormNo.Select()
                Exit Sub
            ElseIf CInt(cboLeaveType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Select()
                Exit Sub


                'Pinkal (13-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf dtpApplyDate.Value > dtpStartDate.Value Then
            ElseIf dtpApplyDate.Value > dtpStartDate.Value AndAlso ConfigParameter._Object._AllowLvApplicationApplyForBackDates = False Then
                'Pinkal (13-Jun-2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Apply date should not be greater than Start date."), enMsgBoxStyle.Information)
                Exit Sub

            ElseIf lnkScanDocuements.Visible AndAlso mdtAttachement Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Scan/Document(s) is compulsory information.Please attach Scan/Document(s) as per this leave type setting."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            Dim objemployee As New clsEmployee_Master
            objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)



            'Pinkal (25-May-2019) -- End



            If dtExpense IsNot Nothing AndAlso dtExpense.Rows.Count > 0 Then
                If IsValidExpense_Eligility() = False AndAlso dtExpense.Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim sMsg As String = String.Empty
            sMsg = objLeaveForm.IsValid_Form(CInt(cboEmployee.SelectedValue), _
                                      CInt(cboLeaveType.SelectedValue), _
                                      dtpStartDate.Value.Date, _
                                      dtpEndDate.Value.Date, _
                                      CInt((DateDiff(DateInterval.Day, dtpStartDate.Value.Date, dtpEndDate.Value.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpEndDate.Value.Date)).ToString()))

            If sMsg <> "" Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Exit Sub
            End If
            sMsg = ""


            If dtpEndDate.Checked Then
                sMsg = objLeaveForm.IsValid_SickLeaveForm(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpEndDate.Value.Date, mintLeaveFormUnkid)
            Else
                sMsg = objLeaveForm.IsValid_SickLeaveForm(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, Nothing, mintLeaveFormUnkid)
            End If

            If sMsg <> "" Then
                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                objLeaveForm.LeaveForm_Alert(CInt(cboEmployee.SelectedValue), dtpStartDate.Value.Date, dtpEndDate.Value.Date, mintLeaveFormUnkid)
            Else
                objLeaveForm.LeaveForm_Alert(CInt(cboEmployee.SelectedValue), dtpStartDate.Value.Date, Nothing, mintLeaveFormUnkid)
            End If

            If objLeaveForm._Message <> "" Then
                eZeeMsgBox.Show(objLeaveForm._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CheckForPeriod() Then


                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.

                If mblnLvTypeSkipApproverFlow = False Then

                Dim objapprover As New clsleaveapprover_master
                Dim dtList As DataTable = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                         , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                         , -1, CInt(cboEmployee.SelectedValue), -1, -1, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString())

                If dtList.Rows.Count = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Assign Approver to this employee and also map approver to system user."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If dtList.Rows.Count > 0 Then
                    Dim objUsermapping As New clsapprover_Usermapping
                    Dim isUserExist As Boolean = False
                    For Each dr As DataRow In dtList.Rows
                        objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                        If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                    Next

                    If isUserExist = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Map this employee's Approver to system user."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If

                If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                    dtList = objapprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                  , -1, CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString())

                    If dtList.Rows.Count = 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Map this Leave type to this employee's Approver(s)."), enMsgBoxStyle.Information)
                        Exit Sub

                    End If

                End If

                End If

                'Pinkal (01-Oct-2018) -- End


                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

                If objLeaveType._Gender > 0 Then

                    If objLeaveType._Gender <> objemployee._Gender Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "This leave type is invalid for selected employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        cboLeaveType.Select()
                        Exit Sub
                    End If

                End If

                'Pinkal (29-Sep-2017) -- Start
                'Bug - Solved Leave Eligility On Leave Application for PACRA (REF-ID # 86).

                'If objLeaveType._EligibilityAfter > 0 Then

                '    If DateDiff(DateInterval.Day, objemployee._Appointeddate.Date, dtpStartDate.Value.Date) <= objLeaveType._EligibilityAfter Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave type master."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '        cboLeaveType.Select()
                '        Exit Sub
                '    End If
                'End If

                Dim objAccruetran As New clsleavebalance_tran

                If ConfigParameter._Object._IsAutomaticIssueOnFinalApproval Then

                    If objLeaveType._IsPaid And objLeaveType._IsAccrueAmount Then

                        Dim drAccrue() As DataRow = Nothing

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing).Tables(0).Select(" leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing).Tables(0).Select("employeeunkid =" & CInt(cboEmployee.SelectedValue) & " AND leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        End If

                        If drAccrue.Length = 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Enter Accrue amount for this paid leave."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If


                    ElseIf objLeaveType._IsPaid And objLeaveType._IsAccrueAmount = False And objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                        Dim drAccrue() As DataRow = Nothing

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing).Tables(0).Select("employeeunkid =" & CInt(cboEmployee.SelectedValue) & " AND leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing).Tables(0).Select("employeeunkid =" & CInt(cboEmployee.SelectedValue) & " AND leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        End If

                        If drAccrue.Length = 0 Then
                            objLeaveType._Leavetypeunkid = objLeaveType._DeductFromLeaveTypeunkid
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "This particular leave is mapped with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 20, " , which does not have the accrue amount. Please add accrue amount for ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 21, " inorder to issue leave."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                    ElseIf objLeaveType._IsPaid And objLeaveType._IsShortLeave Then

                        Dim drShortLeave() As DataRow = Nothing

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            drShortLeave = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND isshortleave = 1 " & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                        End If


                        If drShortLeave.Length = 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                    End If


                End If


                Dim drEligibility() As DataRow = Nothing
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    drEligibility = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing).Tables(0).Select(" leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    drEligibility = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing).Tables(0).Select("employeeunkid =" & CInt(cboEmployee.SelectedValue) & " AND leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " AND yearunkid = " & FinancialYear._Object._YearUnkid)
                End If

                If drEligibility.Length > 0 Then

                    Dim mdtDate As Date = Nothing
                    If objemployee._Reinstatementdate <> Nothing Then
                        If objemployee._Appointeddate.Date < objemployee._Reinstatementdate.Date AndAlso objemployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                            mdtDate = objemployee._Reinstatementdate.Date
                        Else
                            mdtDate = objemployee._Appointeddate.Date
                        End If
                    Else
                        mdtDate = objemployee._Appointeddate.Date
                    End If

                    If DateDiff(DateInterval.Day, mdtDate.Date, dtpStartDate.Value.Date) <= CInt(drEligibility(0)("eligibilityafter")) AndAlso CInt(drEligibility(0)("eligibilityafter")) <> 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave balance."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        cboLeaveType.Select()
                        Exit Sub
                    End If
                End If
                drEligibility = Nothing

                'Pinkal (29-Sep-2017) -- End


                Dim mblnIssued As Boolean = False

                If objLeaveForm._Statusunkid = 7 Then  ' FOR STATUS = ISSUED 
                    mblnIssued = True
                End If


                'Pinkal (03-Jan-2014) -- Start
                'Enhancement : Oman Changes  AS PER MR.ANDREW'S COMMENT

                ' INORDER TO SET BUSINESS FLOW AND CONTROL BALANCE OF LEAVE WE HAVE TO PUT THIS RESTRICTION ON LEAVE AS MANY COMPANY ARE NOT ISSUING LEAVE REGULARLY.
                ' IF ANYBODY WILL COME FOR THIS CHANGES,WE DON'T CHANGE THIS CONCEPT.

                Dim intStatusId As Integer = objLeaveForm.GetEmployeeLastLeaveFormStatus(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), mintLeaveFormUnkid)

                If intStatusId = 2 OrElse intStatusId = 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot apply for this leave now as your previous leave form has not been issued yet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                'Pinkal (03-Jan-2014) -- End

                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                    Dim mdtTnAStartdate As Date = Nothing
                    Dim objPaymentTran As New clsPayment_tran
                    Dim objMaster As New clsMasterData


                    'Pinkal (01-Jan-2019) -- Start
                    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                    'Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartDate.Value.Date)
                    Dim intPeriodId As Integer = 0
                    If mblnConsiderLeaveOnTnAPeriod Then
                        intPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartDate.Value.Date)
                    Else
                        intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.Value.Date, FinancialYear._Object._YearUnkid, 0, False, True, Nothing, True)
                    End If

                    If intPeriodId > 0 Then
                        Dim objTnAPeriod As New clscommom_period_Tran
                        If mblnConsiderLeaveOnTnAPeriod Then
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        Else
                            objTnAPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                            mdtTnAStartdate = objTnAPeriod._Start_Date
                        End If
                        objTnAPeriod = Nothing
                    End If

                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, dtpStartDate.Value.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to apply for this leave ? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If

                    'Pinkal (01-Jan-2019) -- End

                End If


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If mblnIsLvTypePaid Then
                    If mintLvformMinDays <> 0 And mintLvformMinDays > CDec(objNoofDays.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Sorry, you cannot apply for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Language.getMessage(mstrModuleName, 43, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    If mintLvformMaxDays <> 0 And mintLvformMaxDays < CDec(objNoofDays.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Sorry, you cannot apply for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Language.getMessage(mstrModuleName, 43, "day(s) to be applied for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If
                'Pinkal (08-Oct-2018) -- End


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim mstrMsg As String = checkforBlockLeaveValidation()
                If mstrMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(mstrMsg.Trim(), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                'Pinkal (26-Feb-2019) -- End



                If SetValue() = False Then Exit Sub


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'If mblnLvTypeSkipApproverFlow = False Then
                If mblnLvTypeSkipApproverFlow = False AndAlso mblnConsiderExpense Then
                    'Pinkal (03-May-2019) -- End
                If dtExpense Is Nothing OrElse dtExpense.Rows.Count <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Are you sure you want to save leave form without any leave expense ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                ElseIf dtExpense.Rows.Count > 0 Then
                    Dim drRow() As DataRow = dtExpense.Select("AUD <> 'D'")
                    If drRow.Length <= 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Are you sure you want to save leave form without any leave expense ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If
                End If
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "You have applied ") & cboLeaveType.Text & " " & Language.getMessage(mstrModuleName, 26, "leave for") & _
                                              " " & objNoofDays.Text & " " & Language.getMessage(mstrModuleName, 27, "days , from") & " " & _
                                            dtpStartDate.Value.Date & " " & Language.getMessage(mstrModuleName, 28, "to") & " " & dtpEndDate.Value.Date & ". " & _
                                            Language.getMessage(mstrModuleName, 29, "Are you sure of the number of days and selected dates? If yes click YES to save the application, otherwise click NO to make corrections.") _
                                             , CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then

                    Exit Sub
                End If

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strError As String = ""
                If mdtAttachement IsNot Nothing Then
                    mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LEAVEFORMS).Tables(0).Rows(0)("Name").ToString
                    For Each dRow As DataRow In mdtAttachement.Rows
                        If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                            Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            If blnIsIISInstalled Then
                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError) = False Then
                                If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                    'Shani(24-Aug-2015) -- End

                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                Else
                                    Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                    dRow("fileuniquename") = strFileName
                                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                        strPath += "/"
                                    End If
                                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                    dRow("filepath") = strPath
                                    dRow.AcceptChanges()
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                        Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                    End If
                                    File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                                    dRow("fileuniquename") = strFileName
                                    dRow("filepath") = strDocLocalPath
                                    dRow.AcceptChanges()
                                Else
                                    'Pinkal (06-Jan-2016) -- Start
                                    'Enhancement - Working on Changes in SS for Leave Module.
                                    'eZeeMsgBox.Show("Configuration Path not Exist.", enMsgBoxStyle.Information)
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    'Pinkal (06-Jan-2016) -- End
                                    Exit Sub
                                End If
                            End If
                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFileName As String = dRow("fileuniquename").ToString
                            If blnIsIISInstalled Then

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                                'Gajanan [8-April-2019] -- Start
                                'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                    'Gajanan [8-April-2019] -- End
                                    'Shani(24-Aug-2015) -- End
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If File.Exists(strDocLocalPath) Then
                                        File.Delete(strDocLocalPath)
                                    End If
                                Else

                                    'Pinkal (06-Jan-2016) -- Start
                                    'Enhancement - Working on Changes in SS for Leave Module.
                                    'eZeeMsgBox.Show("Configuration Path not Exist.", enMsgBoxStyle.Information)
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    'Pinkal (06-Jan-2016) -- End
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
                objLeaveForm._dtAttachDocument = mdtAttachement
                'SHANI (16 JUL 2015) -- End 


                If mdtClaimAttachment IsNot Nothing Then
                    strError = ""
                    mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.CLAIM_REQUEST).Tables(0).Rows(0)("Name").ToString
                    For Each dRow As DataRow In mdtClaimAttachment.Rows
                        If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                            Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                            If blnIsIISInstalled Then
                                If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                Else
                                    Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                    dRow("fileuniquename") = strFileName
                                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                        strPath += "/"
                                    End If
                                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                    dRow("filepath") = strPath
                                    dRow.AcceptChanges()
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                        Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                    End If
                                    File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                                    dRow("fileuniquename") = strFileName
                                    dRow("filepath") = strDocLocalPath
                                    dRow.AcceptChanges()
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFileName As String = dRow("fileuniquename").ToString
                            If blnIsIISInstalled Then
                                'Gajanan [8-April-2019] -- Start
                                'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                    'Gajanan [8-April-2019] -- End
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If File.Exists(strDocLocalPath) Then
                                        File.Delete(strDocLocalPath)
                                    End If
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Document path does not exist."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
                objLeaveForm._dtClaimAttchment = mdtClaimAttachment

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If menAction = enAction.EDIT_ONE Then

                    'blnFlag = objLeaveForm.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                               , mblnIssued, dtFraction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), ConfigParameter._Object._LeaveBalanceSetting)

                    blnFlag = objLeaveForm.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                   , mblnIssued, dtFraction, ConfigParameter._Object._CurrentDateAndTime, mblnLvTypeSkipApproverFlow _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString() _
                                                                   , ConfigParameter._Object._LeaveBalanceSetting, ConfigParameter._Object._SkipEmployeeMovementApprovalFlow, ConfigParameter._Object._CreateADUserFromEmpMst)
                    'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                Else

                    'blnFlag = objLeaveForm.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                    '                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                            , ConfigParameter._Object._CurrentDateAndTime, dtFraction, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString())

                    blnFlag = objLeaveForm.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                           , ConfigParameter._Object._CurrentDateAndTime, mblnLvTypeSkipApproverFlow, ConfigParameter._Object._LeaveBalanceSetting _
                                                           , ConfigParameter._Object._UserAccessModeSetting, dtFraction, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), ConfigParameter._Object._SkipEmployeeMovementApprovalFlow, ConfigParameter._Object._CreateADUserFromEmpMst)
                    'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                End If
                'Pinkal (01-Oct-2018) -- End

                If blnFlag = False And objLeaveForm._Message <> "" Then
                    eZeeMsgBox.Show(objLeaveForm._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then


                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.


                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'If (menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE) AndAlso mblnIsClaimFormBudgetMandatory AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                    If (menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE) AndAlso mblnIsClaimFormBudgetMandatory _
                         AndAlso mblnIsHRExpense = False AndAlso ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                        'Pinkal (07-Mar-2019) -- End

                        If objClaimRequestMst._Claimrequestno.Trim.Length > 0 Then

                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            Dim objP2P As New clsCRP2PIntegration

                            Dim mstrP2PRequest As String = objClaimRequestMst.GenerateNewRequisitionRequestForP2P(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                                                                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                                                                                                                                                           , objClaimRequestMst._Employeeunkid, objClaimRequestMst._Claimrequestno, objClaimRequestMst._Transactiondate _
                                                                                                                                                                                                           , objClaimRequestMst._Claim_Remark, User._Object._Username, dtExpense, mdtClaimAttachment, objP2P)

                            'Pinkal (04-Feb-2019) -- End

                            Dim objMstData As New clsMasterData
                            Dim mstrGetData As String = ""
                            Dim mstrError As String = ""

                            'Pinkal (31-May-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            Dim mstrPostedData As String = ""
                            'If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P) = False Then

                            'Pinkal (29-Aug-2019) -- Start
                            'Enhancement NMB - Working on P2P Get Token Service URL.
                            'If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData) = False Then
                            If objMstData.GetSetP2PWebRequest(ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData, mstrToken:=mstrP2PToken) = False Then
                                'Pinkal (29-Aug-2019) -- End

                                '/* START TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.

                                If mdtClaimAttachment IsNot Nothing AndAlso mdtClaimAttachment.Rows.Count > 0 Then
                                    Dim dr As List(Of DataRow) = mdtClaimAttachment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").ToList()
                                    If dr.Count > 0 Then
                                        dr.ForEach(Function(x) UpdateDeleteAttachment(x, False, "D"))
                                    End If
                                End If

                                objClaimRequestMst._Isvoid = True
                                objClaimRequestMst._Voiduserunkid = User._Object._Userunkid
                                objClaimRequestMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                objClaimRequestMst._Voidreason = "Voided due to connection fail or error from P2P system."
                                If objClaimRequestMst.Delete(objClaimRequestMst._Crmasterunkid, User._Object._Userunkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid _
                                                                               , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                               , True, mdtClaimAttachment, True, Nothing, mblnIsClaimFormBudgetMandatory) = False Then
                                    eZeeMsgBox.Show(objClaimRequestMst._Message, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If

                                If mdtClaimAttachment IsNot Nothing AndAlso mdtClaimAttachment.Rows.Count > 0 Then
                                    Dim dr As List(Of DataRow) = mdtClaimAttachment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") = "D" AndAlso x.Field(Of Integer)("scanattachtranunkid") > 0).ToList()
                                    If dr.Count > 0 Then
                                        dr.ForEach(Function(x) UpdateDeleteAttachment(x, True, "A"))
                                    End If
                                End If
                                '/* END TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.
                                If eZeeMsgBox.Show(mstrError & Language.getMessage(mstrModuleName, 46, "Are you sure want to save this leave application without expense application ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then

                                    '/* START TO DELETE WHOLE LEAVE FORM WHEN P2P REQUEST IS NOT SUCCEED.
                                    objLeaveForm._Isvoid = True
                                    objLeaveForm._Voiduserunkid = User._Object._Userunkid
                                    objLeaveForm._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                                    objLeaveForm._Voidreason = "Voided due to connection fail or error from P2P system."
                                    If objLeaveForm.Delete(objLeaveForm._Formunkid, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                    , True, False, mdtAttachement, True, True, ConfigParameter._Object._LeaveBalanceSetting) = False Then
                                        eZeeMsgBox.Show(objLeaveForm._Message, enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                    '/* END TO DELETE WHOLE LEAVE FORM WHEN P2P REQUEST IS NOT SUCCEED.
                                    mblnCancel = False
                                    Me.Close()
                                End If
                            Else
                                '/* START TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                                'mstrGetData = "{""requisitionId"": ""NMBHR-011118-000012"",""message"": ""New requisition details saved successfully."",""status"": 200,""timestamp"": ""01-Nov-2018 12:58:59 PM""}"
                                'mstrGetData = "{""timeStamp"":""1550050679772"",""PmtReferenceNo"":""E02130219123759763"",""message"": ""saved sucessfully"",""status"": ""200""}"

                                If mstrGetData.Trim.Length > 0 Then
                                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)

                                    'Pinkal (31-May-2019) -- Start
                                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                    'If objClaimRequestMst.UpdateP2PResponseToClaimMst(dtTable, objClaimRequestMst._Crmasterunkid, User._Object._Userunkid) = False Then
                                    If objClaimRequestMst.UpdateP2PResponseToClaimMst(dtTable, objClaimRequestMst._Crmasterunkid, User._Object._Userunkid, mstrPostedData) = False Then
                                        'Pinkal (31-May-2019) -- End
                                        eZeeMsgBox.Show(objClaimRequestMst._Message, enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                                '/* END TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                            End If
                            objMstData = Nothing

                        End If

                    End If

                    'Pinkal (20-Nov-2018) -- End

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    objLeaveForm.GetData()
                    If menAction <> enAction.EDIT_ONE Then

                        If mblnLvTypeSkipApproverFlow = False Then
                        objLeaveForm.SendMailToApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                           , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                           , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                           , CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), objLeaveForm._Formunkid, objLeaveForm._Formno _
                                                                               , False, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), "", enLogin_Mode.DESKTOP, 0, False)

                        ElseIf mblnLvTypeSkipApproverFlow Then
                            SendEmailForSkipApproverFlow()
                    End If

                    ElseIf (menAction <> enAction.ADD_CONTINUE AndAlso menAction <> enAction.ADD_ONE) AndAlso mblnLvTypeSkipApproverFlow Then
                        SendEmailForSkipApproverFlow()
                    End If  'If menAction <> enAction.EDIT_ONE Then

                    'Pinkal (01-Oct-2018) -- End

                    dtFraction.Rows.Clear()
                    mblnCancel = False


                    If menAction = enAction.ADD_CONTINUE Then
                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Leave Application saved successfully."), enMsgBoxStyle.Information)
                        'Pinkal (07-Mar-2019) -- End
                        objLeaveForm = Nothing
                        objLeaveForm = New clsleaveform
                        GetValue()
                        cboEmployee.Select()
                    Else
                        'Pinkal (07-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 53, "Leave Application updated successfully."), enMsgBoxStyle.Information)
                        'Pinkal (07-Mar-2019) -- End
                        mintLeaveFormUnkid = objLeaveForm._Formunkid
                        Me.Close()
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "LinkedButton's Event"

    Private Sub lnkAccrueleave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAccrueleave.LinkClicked
        Try
            Dim objAccrue As New frmLeaveAccrue_AddEdit
            objAccrue.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue))


            If objAccrue.mintLeavebalalnceUnkid > 0 Then
                lnkAccrueleave.Visible = False

                dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                If objAccrue.dtpStopDate.Checked Then
                    dtpStartDate.MaxDate = objAccrue.dtpStopDate.Value
                End If

                dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date
                If objAccrue.dtpStopDate.Checked Then
                    dtpEndDate.MaxDate = objAccrue.dtpStopDate.Value
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAccrueleave_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkCopyEmpAddress_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyEmpAddress.LinkClicked
        Try
            If CInt(cboEmployee.SelectedValue) < 1 Then Exit Sub
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objState As New clsstate_master
            objState._Stateunkid = objEmployee._Domicile_Stateunkid

            Dim mstrCountry As String = ""
            Dim objCountry As New clsMasterData
            Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
            If dsCountry.Tables("List").Rows.Count > 0 Then
                mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
            End If

            Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                       IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                       IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                       IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                       mstrCountry

            txtLeaveAddress.Text = strAddress

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyEmpAddress_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkLeaveDayCount_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkLeaveDayCount.LinkClicked
        Try
            Dim objfrmLeaveFraction As New frmLeaveDay_fraction
            objfrmLeaveFraction._Startdate = dtpStartDate.Value.Date
            If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                objfrmLeaveFraction._Enddate = dtpEndDate.Value.Date
            ElseIf dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = False Then
                objfrmLeaveFraction._Enddate = dtpStartDate.Value.Date
            Else
                objfrmLeaveFraction._Enddate = dtpEndDate.Value.Date
            End If
            objfrmLeaveFraction._EmployeeId = CInt(cboEmployee.SelectedValue)
            objfrmLeaveFraction._LeaveTypeId = CInt(cboLeaveType.SelectedValue)

            If objfrmLeaveFraction.displayDialog(-1, mintLeaveFormUnkid, Me.menAction) Then
                dtFraction = objfrmLeaveFraction._dtFraction
                objNoofDays.Text = CDec(dtFraction.Compute("SUM(dayfraction)", "1=1")).ToString("#0.00")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkLeaveDayCount_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvLeaveExpense_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lvLeaveExpense.LinkClicked
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            End If

            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Focus()
                Exit Sub
            End If

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            If IsValidExpense_Eligility() = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (29-Sep-2017) -- End

            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            If objLeaveType._IsPaid = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry,You cannot apply leave expense for this leave type.Reason : This Leave Type is unpaid leave type."), enMsgBoxStyle.Information)
                cboLeaveType.Focus()
                Exit Sub
            End If
            objLeaveType = Nothing

            Dim objfrmExpense As New frmClaims_RequestAddEdit
            objfrmExpense._EmployeeId = CInt(cboEmployee.SelectedValue)
            objfrmExpense._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objfrmExpense._ExpenseCategoryID = enExpenseType.EXP_LEAVE
            objfrmExpense._IsFromLeave = True

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            'objfrmExpense._Leavedate = dtpApplyDate.Value.Date
            objfrmExpense._Leavedate = CDate(dtpApplyDate.Value & " " & Now.ToLongTimeString())
            'Pinkal (20-Nov-2018) -- End




            If Me.menAction = enAction.EDIT_ONE Then
                Dim objClaimMst As New clsclaim_request_master
                mintClaimRequestMasterId = objClaimMst.GetClaimRequestMstIDFromLeaveForm(mintLeaveFormUnkid)
                objClaimMst._Crmasterunkid = mintClaimRequestMasterId

                If objClaimMst._Statusunkid = 3 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "You cannot Edit Claim Expense Form.Reason: Claim Expense Form was already rejected."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
                objfrmExpense._LeaveFormID = mintLeaveFormUnkid
            End If

            If dtExpense IsNot Nothing AndAlso dtExpense.Rows.Count > 0 Then
                objfrmExpense._dtExpense = dtExpense.Copy
            End If

            If mdtClaimAttachment IsNot Nothing AndAlso mdtClaimAttachment.Rows.Count > 0 Then
                objfrmExpense._dtAttchment = mdtClaimAttachment
            End If

            If objfrmExpense.displayDialog(mintClaimRequestMasterId, Me.menAction, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, objClaimRequestMst, True) Then
                dtExpense = objfrmExpense._dtExpense
                mdtClaimAttachment = objfrmExpense._dtAttchment
                mblnIsClaimFormBudgetMandatory = objfrmExpense._blnIsClaimFormBudgetMandatory
                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpense = objfrmExpense._blnIsHRExpense
                'Pinkal (07-Mar-2019) -- End

                'Pinkal (29-Aug-2019) -- Start
                'Enhancement NMB - Working on P2P Get Token Service URL.
                mstrP2PToken = objfrmExpense._P2PToken
                'Pinkal (29-Aug-2019) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveExpense_LinkClicked", mstrModuleName)
        End Try
    End Sub


    'Pinkal (07-May-2015) -- Start
    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    Private Sub lnkScanDocuements_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkScanDocuements.LinkClicked
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If mdtAttachement IsNot Nothing Then frm._dtAttachment = mdtAttachement.Copy()
            frm._TransactionID = mintLeaveFormUnkid
            'Pinkal (06-Jan-2016) -- End

            'S.SANDEEP |26-APR-2019| -- START
            'If frm.displayDialog(Language.getMessage(mstrModuleName, 33, "Select Employee"), enImg_Email_RefId.Leave_Module _
            '                                                            , menAction, "-1", mstrModuleName _
            '                                                            , CInt(cboEmployee.SelectedValue), enScanAttactRefId.LEAVEFORMS, True) Then 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            If frm.displayDialog(Language.getMessage(mstrModuleName, 33, "Select Employee"), enImg_Email_RefId.Leave_Module _
                                                                        , menAction, "-1", mstrModuleName, True _
                                                                        , CInt(cboEmployee.SelectedValue), enScanAttactRefId.LEAVEFORMS, True) Then 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                'S.SANDEEP |26-APR-2019| -- END
                mdtAttachement = frm._dtAttachment
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkScanDocuements_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-May-2015) -- End


#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            txtFormNo.BackColor = GUI.ColorComp
            cboLeaveType.BackColor = GUI.ColorComp
            txtLeaveAddress.BackColor = GUI.ColorOptional
            txtLeaveAddress.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtFormNo.Text = objLeaveForm._Formno
            cboLeaveType.SelectedValue = objLeaveForm._Leavetypeunkid
            cboEmployee.SelectedValue = objLeaveForm._Employeeunkid

            If objLeaveForm._Formunkid > 0 Then
                dtpApplyDate.Value = objLeaveForm._Applydate
                dtpStartDate.Value = objLeaveForm._Startdate
                If objLeaveForm._Returndate <> Nothing Then
                    dtpEndDate.Value = objLeaveForm._Returndate
                End If


                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                Dim objAttchement As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END
                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " AND transactionunkid = " & mintLeaveFormUnkid, "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing

                mintClaimRequestMasterId = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintLeaveFormUnkid)
                objClaimRequestMst._Crmasterunkid = mintClaimRequestMasterId
                Dim objClaimTran As New clsclaim_request_tran
                objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId
                dtExpense = objClaimTran._DataTable
                objClaimTran = Nothing
                'Pinkal (06-Jan-2016) -- End


            Else
                If FinancialYear._Object._Database_End_Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpApplyDate.Value = FinancialYear._Object._Database_End_Date
                    dtpStartDate.Value = FinancialYear._Object._Database_End_Date
                    dtpEndDate.Value = FinancialYear._Object._Database_End_Date
                Else
                    dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

            End If

            If isFromLeaveProcess = False Then

                If objLeaveForm._Statusunkid = 1 Or objLeaveForm._Statusunkid = 7 Then
                    cboEmployee.Enabled = False
                    txtFormNo.ReadOnly = True
                    cboLeaveType.Enabled = False
                    objbtnSearchEmployee.Enabled = False
                    cboLeaveType.Enabled = False
                    objbtnAddLeaveType.Enabled = False
                Else
                    cboEmployee.Enabled = True
                    txtFormNo.ReadOnly = False
                    cboLeaveType.Enabled = True
                    objbtnSearchEmployee.Enabled = True
                    cboLeaveType.Enabled = True
                    objbtnAddLeaveType.Enabled = True
                End If

            End If


            txtLeaveAddress.Text = objLeaveForm._Addressonleave
            txtLeaveReason.Text = objLeaveForm._Remark

            If menAction = enAction.EDIT_ONE Then

            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'dtExpense = objLeaveForm._dtExpense
            'Pinkal (06-Jan-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try

            Dim objLeaveType As New clsleavetype_master

            If objLeaveForm._Applydate <> Nothing And objLeaveForm._Startdate <> Nothing And objLeaveForm._Returndate <> Nothing Then

                If objLeaveForm._Applydate.Date <> dtpApplyDate.Value.Date _
                    Or objLeaveForm._Startdate.Date <> dtpStartDate.Value.Date _
                    Or objLeaveForm._Returndate.Date <> dtpEndDate.Value.Date Then
                    objLeaveForm._Statusunkid = 2
                Else
                    Dim drRow As DataRow() = dtFraction.Select("AUD = 'U' or AUD = 'D'")
                    If drRow.Length > 0 Then
                        objLeaveForm._Statusunkid = 2
                    Else
                        objLeaveForm._Statusunkid = objLeaveForm._Statusunkid
                    End If

                End If
            Else
                objLeaveForm._Statusunkid = 2
            End If


            objLeaveForm._Startdate = dtpStartDate.Value
            If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                objLeaveForm._Returndate = dtpEndDate.Value
            ElseIf dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = False Then
                objLeaveForm._Returndate = Nothing
            Else
                objLeaveForm._Returndate = dtpEndDate.Value
            End If

            If dtFraction Is Nothing Then
                Dim objFrmFraction As New frmLeaveDay_fraction

                objFrmFraction._EmployeeId = CInt(cboEmployee.SelectedValue)
                objFrmFraction._LeaveTypeId = CInt(cboLeaveType.SelectedValue)

                Dim objLeaveFraction As New clsleaveday_fraction
                objFrmFraction._dtFraction = objLeaveFraction._dtfraction
                objFrmFraction._Startdate = dtpStartDate.Value.Date
                If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                    objFrmFraction._Enddate = dtpEndDate.Value.Date
                ElseIf dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = False Then
                    objFrmFraction._Enddate = dtpStartDate.Value.Date
                Else
                    objFrmFraction._Enddate = dtpEndDate.Value.Date
                End If
                objFrmFraction.GenerateDays()
                dtFraction = objFrmFraction._dtFraction
            Else

                Dim drRow As DataRow() = dtFraction.Select("leavedate < '" & dtpStartDate.Value.Date & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dtFraction.AcceptChanges()
                End If

                drRow = Nothing
                If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                    drRow = dtFraction.Select("leavedate >'" & dtpEndDate.Value.Date & "' AND AUD <> 'D'")
                ElseIf dtpEndDate.ShowCheckBox = False Then
                    drRow = dtFraction.Select("leavedate >'" & dtpEndDate.Value.Date & "' AND AUD <> 'D'")
                End If

                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dtFraction.AcceptChanges()
                End If


                'START FOR IN EDIT MODE, WHEN USER ADD DAYS THEN ADD TO DATATBLE

                Dim mintDays As Integer = 0
                Dim mdtdate As DateTime = Nothing
                If objLeaveForm._Returndate <> Nothing Then
                    mintDays = CInt(DateDiff(DateInterval.Day, objLeaveForm._Startdate.Date, objLeaveForm._Returndate.Date))
                Else
                    mintDays = CInt(DateDiff(DateInterval.Day, objLeaveForm._Startdate.Date, objLeaveForm._Startdate.Date))
                End If
                mdtdate = objLeaveForm._Startdate.Date

                Dim blnIssueonHoliday As Boolean = False
                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                blnIssueonHoliday = objLeaveType._Isissueonholiday

                Dim blnIssueonWeekend As Boolean = False
                blnIssueonWeekend = objLeaveType._Isissueonweekend

                For i As Integer = 0 To mintDays

                    If blnIssueonHoliday = False Then
                        Dim objEmpHoliday As New clsemployee_holiday
                        If objEmpHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), mdtdate.AddDays(i).Date).Rows.Count > 0 Then
                            Continue For
                        End If

                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : TRA Changes  [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
                        Dim objReccurent As New clsemployee_holiday
                        'Pinkal (27-Jun-2015) -- Start
                        'Enhancement - FROM NOW IN ELC EMPLOYEE CAN APPLY LEAVE BEYOND ELC IF EMPLOYEE HAD LEAVE BALANCE.DID FOR TRA AND REQUESTED BY RUTTA.
                        'For j As Integer = 0 To mintDays - 1
                        If objReccurent.GetEmployeeReCurrentHoliday(CInt(cboEmployee.SelectedValue), mdtdate.AddDays(i).Date) Then
                            Continue For
                        End If
                        'Next
                        'Pinkal (27-Jun-2015) -- End
                        'Pinkal (06-May-2014) -- End
                    End If

                    If blnIssueonWeekend = False Then

                        Dim objShift As New clsNewshift_master
                        Dim objShiftTran As New clsshift_tran
                        Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                        objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtdate.AddDays(i).Date, CInt(cboEmployee.SelectedValue))
                        objShiftTran.GetShiftTran(objShift._Shiftunkid)
                        If objShiftTran._dtShiftday IsNot Nothing Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1")
                            If drShift.Length > 0 Then
                                Continue For
                            End If
                        End If

                    End If


                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    'Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtdate.AddDays(i).Date & "'  AND (AUD <> 'D' or AUD <> '')")

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    'Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtdate.AddDays(i).Date & "'  AND (AUD <> 'D' and AUD <> '')")
                    Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & mdtdate.AddDays(i).Date & "'  AND AUD <> 'D' ")
                    'Pinkal (01-Oct-2018) -- End


                    'Pinkal (18-Nov-2016) -- End

                    If dtRow.Length = 0 Then
                        Dim dr As DataRow = dtFraction.NewRow()
                        dr("dayfractionunkid") = -1
                        dr("formunkid") = objLeaveForm._Formunkid
                        dr("leavedate") = mdtdate.AddDays(i)
                        dr("dayfraction") = 1
                        dr("GUID") = Guid.NewGuid.ToString()
                        dr("AUD") = "A"
                        dtFraction.Rows.InsertAt(dr, i)
                    End If
                Next
            End If

            'END FOR IN EDIT MODE, WHEN USER ADD DAYS THEN ADD TO DATATBLE

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            Dim objBudget As New clsBudgetEmp_timesheet()
            If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                If objBudget.GetEmpTotalWorkingHrsForTheDay(dtpStartDate.Value.Date, dtpEndDate.Value.Date, CInt(cboEmployee.SelectedValue)) > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "You cannot apply for this leave form.Reason : This employee is already applied for employee budget timesheet for this tenure."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    objBudget = Nothing
                    Return False
                End If
            End If
            objBudget = Nothing
            'Pinkal (13-Oct-2017) -- End


            objLeaveForm._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLeaveForm._Formno = txtFormNo.Text.Trim
            objLeaveForm._Applydate = dtpApplyDate.Value

            objLeaveForm._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            objLeaveForm._Addressonleave = txtLeaveAddress.Text.Trim
            objLeaveForm._Remark = txtLeaveReason.Text.Trim

            objLeaveForm._Userunkid = User._Object._Userunkid


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objLeaveForm._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            'If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "You cannot apply for this leave form.Reason : As these days are falling in holiday(s) and weekend."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Return False
            'End If
            If dtFraction IsNot Nothing Then
                If dtFraction.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "You cannot apply for this leave form.Reason : As these days are falling in holiday(s) and weekend."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If
            End If
            'Pinkal (01-Apr-2019) -- End



            'START FOR INSERT AND CHECK FOR SHORT LEAVE TYPE IN LEAVE BALANCE TRAN
            If objLeaveType._Leavetypeunkid <= 0 Then
                objLeaveType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            End If
            If objLeaveType._IsPaid And (objLeaveType._IsAccrueAmount = False OrElse objLeaveType._IsAccrueAmount) And objLeaveType._IsShortLeave Then

                Dim objAccruetran As New clsleavebalance_tran
                Dim drAccrue() As DataRow = Nothing

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))
                End If


                If drAccrue.Length = 0 Then

                    objAccruetran._Yearunkid = FinancialYear._Object._YearUnkid
                    objAccruetran._Employeeunkid = objLeaveForm._Employeeunkid
                    objAccruetran._Leavetypeunkid = objLeaveForm._Leavetypeunkid
                    objAccruetran._AccrueSetting = enAccrueSetting.Issue_Balance
                    objAccruetran._AccrueAmount = objLeaveType._MaxAmount
                    objAccruetran._UptoLstYr_AccrueAmout = objLeaveType._MaxAmount
                    objAccruetran._IsshortLeave = objLeaveType._IsShortLeave
                    objAccruetran._Ispaid = objLeaveType._IsPaid
                    objAccruetran._Startdate = FinancialYear._Object._Database_Start_Date
                    objAccruetran._Enddate = FinancialYear._Object._Database_End_Date
                    objAccruetran._Userunkid = User._Object._Userunkid
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        objAccruetran._IsOpenELC = True
                        objAccruetran._IsELC = True
                    End If

                    If objAccruetran.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True) = False Then
                        eZeeMsgBox.Show(objAccruetran._Message, enMsgBoxStyle.Information)
                        Return False
                    End If

                    Dim mdblAmount As Decimal = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                    If objLeaveType._MaxAmount < mdblAmount Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Return False
                    End If

                Else


                    'Pinkal (09-Jul-2013) -- Start
                    'Enhancement : TRA Changes  'FOR UPDATE SHORT LEAVE  BALANCE IN LEAVE ACCRUE BALANCE WHEN IT'S CHANGE IN MAX AMOUNT OF SHORT LEAVE TYPE IN LEAVE TYPE MASTER
                    Dim mdblAmount As Decimal = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                    If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Return False
                    End If


                    'Pinkal (09-Jul-2013) -- End  'FOR UPDATE SHORT LEAVE  BALANCE IN LEAVE ACCRUE BALANCE WHEN IT'S CHANGE IN MAX AMOUNT OF SHORT LEAVE TYPE IN LEAVE TYPE MASTER

                End If


            ElseIf objLeaveType._IsPaid And objLeaveType._IsAccrueAmount And objLeaveType._IsShortLeave = False Then


                'Pinkal (03-Jan-2014) -- Start
                'Enhancement : Oman Changes  AS PER MR.ANDREW'S COMMENT

                ' INORDER TO SET BUSINESS FLOW AND CONTROL BALANCE OF LEAVE WE HAVE TO PUT THIS RESTRICTION ON LEAVE AS MANY COMPANY ARE NOT ISSUING LEAVE REGULARLY.
                ' IF ANYBODY WILL COME FOR THIS CHANGES,WE DON'T CHANGE THIS CONCEPT.

                Dim objAccruetran As New clsleavebalance_tran
                Dim drAccrue() As DataRow = Nothing

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(cboLeaveType.SelectedValue))
                End If


                If drAccrue.Length > 0 Then

                    If CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                        Dim mdblAmount As Decimal = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False

                            'Pinkal (27-Jun-2015) -- Start
                            'Enhancement - FROM NOW IN ELC EMPLOYEE CAN APPLY LEAVE BEYOND ELC IF EMPLOYEE HAD LEAVE BALANCE.DID FOR TRA AND REQUESTED BY RUTTA.

                            'Else

                            'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso CDate(drAccrue(0)("enddate")).Date < dtpStartDate.Value.Date Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You cannot create this leave form.Reason :Employee Leave Cycle is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            '    Return False
                            'End If

                            'Pinkal (27-Jun-2015) -- End

                        End If

                    ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                        Dim mdblAmount As Decimal = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        If CDec(objAsonDateBalance.Text) < mdblAmount Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If


                        'Pinkal (08-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                    ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then

                        Dim mdblAmount As Decimal = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(CDec(drAccrue(0)("accrue_amount")) - (CDec(drAccrue(0)("issue_amount")) + mdblAmount)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                            'Pinkal (08-Oct-2018) -- End
                        End If

                    End If

                End If

                'Pinkal (02-Jan-2018) -- Start
                'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                'Control to be put on application/not just issuing. employee to be warned and stopped.

            ElseIf objLeaveType._DeductFromLeaveTypeunkid > 0 AndAlso objLeaveType._IsPaid AndAlso objLeaveType._IsAccrueAmount = False AndAlso objLeaveType._IsShortLeave = False Then

                Dim objAccruetran As New clsleavebalance_tran
                Dim drAccrue() As DataRow = Nothing


                'Pinkal (06-Aug-2019) -- Start
                'Defect [NMB] - Worked on Block Leave which didn't check Annual Leave Balance before annual balance exceeded and employee can able to apply leave application.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._Leavetypeunkid))
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._Leavetypeunkid))
                'End If

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    drAccrue = objAccruetran.GetList("AccrueTran", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "").Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid))
                End If


                Dim mdtStartDate As Date = Nothing
                Dim mdtEndDate As Date = Nothing

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso drAccrue.Length > 0 Then
                    mdtStartDate = CDate(drAccrue(0)("startdate")).Date
                    mdtEndDate = CDate(drAccrue(0)("enddate")).Date
                End If

                Dim mdblAmount As Decimal = 0

                If drAccrue.Length > 0 Then

                    If CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                        mdblAmount = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        If CDec(drAccrue(0)("accrue_amount")) < CDec(drAccrue(0)("issue_amount")) + mdblAmount Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If

                    ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                        mdblAmount = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        'Pinkal (11-Jun-2020) -- Start
                        'Bug  Freight Forwarders Ltd (0004733) - Leave Types Deducted from Annual Leave/Accrue Type leaves not Allowing user to Apply
                        ' If CDec(objAsonDateBalance.Text) < mdblAmount Then
                        Dim mdecAsonDateBalance As Decimal = 0
                        Dim mdecAsonDateAccrue As Decimal = 0
                        objAccruetran.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                                     , ConfigParameter._Object._LeaveBalanceSetting, CInt(cboEmployee.SelectedValue), CInt(objLeaveType._DeductFromLeaveTypeunkid) _
                                                                     , dtpEndDate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                                                                     , mdecAsonDateAccrue, mdecAsonDateBalance, FinancialYear._Object._YearUnkid)

                        'If CDec(objAsonDateBalance.Text) < mdblAmount Then
                        If mdecAsonDateBalance < mdblAmount Then
                            'Pinkal (11-Jun-2020) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If


                    ElseIf CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CDec(drAccrue(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then

                        mdblAmount = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))

                        If CDec(drAccrue(0)("maxnegative_limit")) <> 0 AndAlso CDec(drAccrue(0)("maxnegative_limit")) > CDec(CDec(drAccrue(0)("accrue_amount")) - (CDec(drAccrue(0)("issue_amount")) + mdblAmount)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Return False
                        End If

                    End If

                Else
                Dim mdblIssue As Decimal = 0
                Dim objLeaveIssueTran As New clsleaveissue_Tran
                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(cboEmployee.SelectedValue), objLeaveType._Leavetypeunkid, FinancialYear._Object._YearUnkid, Nothing, mdtStartDate, mdtEndDate)
                Else
                    mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(cboEmployee.SelectedValue), objLeaveType._Leavetypeunkid, FinancialYear._Object._YearUnkid)
                End If

                    mdblAmount = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                If objLeaveType._MaxAmount < (mdblIssue + mdblAmount) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If
                End If

                'Pinkal (06-Aug-2019) -- End

                'Pinkal (02-Jan-2018) -- End
            End If


            'END FOR INSERT AND CHECK FOR SHORT LEAVE TYPE IN LEAVE BALANCE TRAN

            objLeaveForm._dtExpense = dtExpense
            objLeaveForm._IsPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            objLeaveForm._ObjClaimRequestMst = objClaimRequestMst


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            'SHANI (16 JUL 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboLeaveType_SelectedIndexChanged
            RemoveHandler cboLeaveType.SelectedIndexChanged, AddressOf cboLeaveType_SelectedIndexChanged
            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, False)
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If
            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")

            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "leavename"


            Dim drRow As DataRow = dsFill.Tables("LeaveType").NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 8, "Select")
            dsFill.Tables("LeaveType").Rows.InsertAt(drRow, 0)

            cboLeaveType.DataSource = dsFill.Tables("LeaveType")
            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboLeaveType_SelectedIndexChanged
            AddHandler cboLeaveType.SelectedIndexChanged, AddressOf cboLeaveType_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetProcessVisiblity()
        Try
            If ConfigParameter._Object._LeaveFormNoType = 1 Then
                txtFormNo.Enabled = False
            Else
                txtFormNo.Enabled = True
            End If
            objbtnAddLeaveType.Enabled = User._Object.Privilege._AddLeaveType

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'dtpStartDate.MinDate = dtpApplyDate.Value.Date
            dtpApplyDate.Enabled = Not ConfigParameter._Object._AllowLvApplicationApplyForBackDates
            dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (13-Jun-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProcessVisiblity", mstrModuleName)
        End Try
    End Sub

    Private Function CheckForPeriod() As Boolean
        Try


            'Pinkal (23-Sep-2014) -- Start
            'Enhancement - [FINCA UGANDA]  IF THIS SETTIGN IS FALSE THEN IT WILL ALLOW TO APPLY LEAVE WHETHER PERIOD IS CLOSED. 
            If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue = False Then Return True
            'Pinkal (23-Sep-2014) -- End


            Dim dsList As DataSet = Nothing
            Dim objperiod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Close)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objperiod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            dsList = objperiod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            'Shani(24-Aug-2015) -- End

            'Sohail (21 Aug 2015) -- End

            If dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString) <= dtpApplyDate.Value.Date _
                    And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString) >= dtpApplyDate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select correct apply date. Reason:This date period is already closed."), enMsgBoxStyle.Information)
                        dtpApplyDate.Select()
                        Return False

                    ElseIf eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString) <= dtpStartDate.Value.Date _
                    And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString) >= dtpStartDate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select correct start date. Reason:This date period is already closed."), enMsgBoxStyle.Information)
                        dtpStartDate.Select()
                        Return False

                    ElseIf eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString) <= dtpEndDate.Value.Date _
                    And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString) >= dtpEndDate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select correct end date. Reason:This date period is already closed."), enMsgBoxStyle.Information)
                        dtpEndDate.Select()
                        Return False

                    End If

                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForPeriod", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Function GetNoOfHolidays(ByVal intLeaveTypeId As Integer, ByVal startdate As Date, ByVal enddate As Date) As Decimal
        Dim mdcDayCount As Decimal = 0
        Dim arHolidayDate() As String = Nothing
        Try
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = intLeaveTypeId
            If objLeaveType._Isissueonholiday = False Then
                mdcDayCount = objLeaveForm.GetNoOfHolidays(CInt(cboEmployee.SelectedValue), startdate, enddate, arHolidayDate, objLeaveType._Isissueonweekend, objLeaveType._IsLeaveHL_onWK)


                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

                Dim objReccurent As New clsemployee_holiday
                For i As Integer = 0 To CInt(DateDiff(DateInterval.Day, startdate, enddate.AddDays(1)) - 1)

                    'Pinkal (19-Jun-2014) -- Start
                    'Enhancement : TRA Changes Leave Enhancement  [Deducting 1 more Days in WHEN Leave is assign to employee and also recurrent]
                    If arHolidayDate.Contains(eZeeDate.convertDate(startdate.AddDays(i).Date)) = False Then
                        If objReccurent.GetEmployeeReCurrentHoliday(CInt(cboEmployee.SelectedValue), startdate.AddDays(i).Date) Then
                            mdcDayCount += 1
                        End If
                    End If
                    'Pinkal (19-Jun-2014) -- End
                Next

                'Pinkal (06-May-2014) -- End

            Else
                mdcDayCount = 0
            End If

            If objLeaveType._Isissueonweekend = False Then
                mdcDayCount += objLeaveForm.GetNoOfWeekend(CInt(cboEmployee.SelectedValue), startdate, arHolidayDate, enddate, objLeaveType._Isissueonholiday, objLeaveType._IsLeaveHL_onWK)
            Else
                mdcDayCount += 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNoOfHolidays", mstrModuleName)
        End Try
        Return mdcDayCount
    End Function

    Private Sub GetBalanceInfo()
        Try
            Dim objLvAccrue As New clsleavebalance_tran
            Dim dsList As DataSet = Nothing

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLvAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString)
                dsList = objLvAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)
                'Pinkal (18-Nov-2016) -- End


            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                objLvAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLvAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLvAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, Nothing, Nothing, True, True)
                dsList = objLvAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
                'Pinkal (18-Nov-2016) -- End


            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objBalance.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")
            End If

            Dim dsbalanceList As DataSet = Nothing

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)


            Dim mdtEndDate As DateTime = Nothing
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                'dsbalanceList = objLvAccrue.GetEmployeeBalanceData(CInt(cboLeaveType.SelectedValue).ToString(), CInt(cboEmployee.SelectedValue).ToString(), True, FinancialYear._Object._YearUnkid, False, False, False)
                'If dsbalanceList IsNot Nothing AndAlso dsbalanceList.Tables(0).Rows.Count > 0 Then
                '    If mdtEndDate > FinancialYear._Object._Database_End_Date.Date OrElse mdtEndDate = Nothing Then
                '        mdtEndDate = FinancialYear._Object._Database_End_Date.Date
                '    Else
                '        mdtEndDate = CDate(dsbalanceList.Tables(0).Rows(0)("enddate")).Date
                '    End If
                'Else
                '    mdtEndDate = FinancialYear._Object._Database_End_Date.Date
                'End If

                If objEmployee._Termination_From_Date.Date <> Nothing Then
                    mdtEndDate = objEmployee._Termination_From_Date.Date
                ElseIf objEmployee._Termination_To_Date.Date <> Nothing Then
                    mdtEndDate = objEmployee._Termination_To_Date.Date
                End If

                If mdtEndDate = Nothing OrElse mdtEndDate.Date > FinancialYear._Object._Database_End_Date.Date Then
                    mdtEndDate = FinancialYear._Object._Database_End_Date.Date
                End If

                dsList = objLvAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)

                'Pinkal (18-Nov-2016) -- End

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                dsbalanceList = objLvAccrue.GetEmployeeBalanceData(CInt(cboLeaveType.SelectedValue).ToString(), CInt(cboEmployee.SelectedValue).ToString(), True, FinancialYear._Object._YearUnkid, True, True, False)

                If dsbalanceList IsNot Nothing AndAlso dsbalanceList.Tables(0).Rows.Count > 0 Then
                    mdtEndDate = CDate(dsbalanceList.Tables(0).Rows(0)("enddate")).Date
                Else
                    If objEmployee._Termination_To_Date.Date <> Nothing Then
                        mdtEndDate = objEmployee._Termination_To_Date.Date
                    End If
                End If

                objLvAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLvAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date
                dsList = objLvAccrue.GetLeaveBalanceInfo(mdtEndDate.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
            End If

            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objLeaveAccrue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBalanceInfo", mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    'Private Sub GetAsOnDateAccrue(ByVal dtpDate As Date)
    '    Try
    '        Dim objLvAccrue As New clsleavebalance_tran
    '        Dim dsList As DataSet = Nothing
    '        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            dsList = objLvAccrue.GetLeaveBalanceInfo(dtpApplyDate.Value.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, )

    '        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            objLvAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
    '            objLvAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date
    '            dsList = objLvAccrue.GetLeaveBalanceInfo(dtpApplyDate.Value.Date, cboLeaveType.SelectedValue.ToString(), CInt(cboEmployee.SelectedValue).ToString, Nothing, Nothing, True, True)
    '        End If

    '        If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
    '            objAsonDateAccrue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
    '            'Pinkal (16-Jan-2016) -- Start
    '            'Enhancement - Working on Changes FOR CHAI BORA.
    '            'objAsonDateBalance.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) - (CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
    '            objAsonDateBalance.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) - (CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
    '            'Pinkal (16-Jan-2016) -- End
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetAsOnDateAccrue", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub GetAsOnDateAccrue()
        Try
            Dim objLvAccrue As New clsleavebalance_tran
            Dim mdecAsonDateAccrue As Decimal = 0
            Dim mdecAsonDateBalance As Decimal = 0

            'Pinkal (06-Apr-2018) -- Start
            'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].
            'objLvAccrue.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
            '                                             , ConfigParameter._Object._LeaveBalanceSetting, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
            '                                             , dtpEndDate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
            '                                             , mdecAsonDateAccrue, mdecAsonDateBalance)
            objLvAccrue.GetAsonDateAccrue(FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date _
                                                         , ConfigParameter._Object._LeaveBalanceSetting, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue) _
                                                         , dtpEndDate.Value.Date, ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth _
                                                        , mdecAsonDateAccrue, mdecAsonDateBalance, FinancialYear._Object._YearUnkid)
            'Pinkal (06-Apr-2018) -- End
            
            objAsonDateAccrue.Text = mdecAsonDateAccrue.ToString()
            objAsonDateBalance.Text = mdecAsonDateBalance.ToString()
            objLvAccrue = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAsOnDateAccrue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Nov-2016) -- End 

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private Function IsValidExpense_Eligility() As Boolean
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboLeaveType.SelectedValue) > 0 Then
                Dim objlvtype As New clsleavetype_master
                objlvtype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                If objlvtype._EligibilityOnAfter_Expense > 0 Then
                    intEligibleExpenseDays = objlvtype._EligibilityOnAfter_Expense
                    If CDec(objNoofDays.Text) >= objlvtype._EligibilityOnAfter_Expense Then
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If
            End If
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidExpense_Eligility", mstrModuleName)
        Finally
        End Try
    End Function
    'Pinkal (29-Sep-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private Function SendEmailForSkipApproverFlow() As Boolean
        Try
            Dim mblnSendEmail As Boolean = True
            If dtpEndDate.ShowCheckBox AndAlso dtpEndDate.Checked = False Then mblnSendEmail = False

            If mblnSendEmail Then

                '/* START SEND EMAIL TO DEFAULT REPORTING TO

                objLeaveForm.SendMailToApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                               , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                               , CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), objLeaveForm._Formunkid, objLeaveForm._Formno _
                                                               , False, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), "", enLogin_Mode.DESKTOP, 0, mblnLvTypeSkipApproverFlow)

                '/* END SEND EMAIL TO DEFAULT REPORTING TO


                Dim objfrmsendmail As New frmSendMail
                Dim strPath As String = ""

                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeaveForm._Employeeunkid

                objLeaveForm._EmployeeCode = objEmp._Employeecode
                objLeaveForm._EmployeeFirstName = objEmp._Firstname
                objLeaveForm._EmployeeMiddleName = objEmp._Othername
                objLeaveForm._EmployeeSurName = objEmp._Surname
                objLeaveForm._EmpMail = objEmp._Email

                objEmp = Nothing

                strPath = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                          , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True _
                                                                                          , ConfigParameter._Object._UserAccessModeSetting, objLeaveForm._Formno, objLeaveForm._Employeeunkid _
                                                                                          , objLeaveForm._Formunkid, ConfigParameter._Object._LeaveBalanceSetting, FinancialYear._Object._YearUnkid _
                                                                                          , FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, False)


                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                'objLeaveForm.SendMailToEmployee(objLeaveForm._Employeeunkid, cboLeaveType.SelectedText, dtpStartDate.Value.Date, dtpEndDate.Value.Date _
                '                                                    , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP _
                '                                                    , objLeaveForm._LoginEmployeeunkid, User._Object._Userunkid, "", 0)


                objLeaveForm.SendMailToEmployee(objLeaveForm._Employeeunkid, cboLeaveType.SelectedText, dtpStartDate.Value.Date, dtpEndDate.Value.Date _
                                                                    , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP _
                                                                    , objLeaveForm._LoginEmployeeunkid, User._Object._Userunkid, "", 0, objLeaveForm._Formno)

                'Pinkal (01-Apr-2019) -- End


                objfrmsendmail = Nothing

            End If  'If mblnSendEmail Then
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendEmailForSkipApproverFlow", mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateDeleteAttachment", mstrModuleName)
            Return False
        End Try
    End Function
    'Pinkal (20-Nov-2018) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function CheckforBlockLeaveValidation() As String
        Dim mstrMsg As String = ""
        Try
            Dim objLvType As New clsleavetype_master
            Dim mstrLeaveTypeIds As String = objLvType.GetDeductdToLeaveTypeIDs(CInt(cboLeaveType.SelectedValue))
            If mstrLeaveTypeIds.Trim.Length > 0 Then
                Dim ar() As String = mstrLeaveTypeIds.Trim.ToString().Split(CChar(","))
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        objLvType._Leavetypeunkid = CInt(ar(i))
                        If objLvType._IsBlockLeave = False Then Continue For
                        If objLeaveForm.IsLeaveForm_Exists(CInt(cboEmployee.SelectedValue), CInt(ar(i))) Then
                            If objLeaveForm.GetLeaveFormForBlockLeaveStatus(CInt(cboEmployee.SelectedValue), CInt(ar(i))) <> 7 Then 'Issue
                                'Pinkal (03-May-2019) -- Start
                                'Enhancement - Working on Leave UAT Changes for NMB.
                                'mstrMsg = Language.getMessage(mstrModuleName, 47, "You cannot apply leave form for this leave type.Reason : mandatory") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 48, "has not been applied or previously applied") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 49, "(s) are not issued.")
                                mstrMsg = Language.getMessage(mstrModuleName, 54, "Sorry ! You have to apply") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 55, "before applying this type of leave.")
                                'Pinkal (03-May-2019) -- End
                            End If
                        Else
                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.
                            'mstrMsg = Language.getMessage(mstrModuleName, 47, "You cannot apply leave form for this leave type.Reason : mandatory") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 48, "has not been applied or previously applied") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 49, "(s) are not issued.")
                            mstrMsg = Language.getMessage(mstrModuleName, 54, "Sorry ! You have to apply") & " " & objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 55, "before applying this type of leave.")
                            'Pinkal (03-May-2019) -- End
                            Exit For
                        End If
                    Next
                End If
            End If
            objLvType = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "checkforBlockLeaveValidation", mstrModuleName)
            mstrMsg = ex.Message
        End Try
        Return mstrMsg
    End Function

    'Pinkal (26-Feb-2019) -- End



#End Region

#Region "DatePicker's Event"

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged, dtpEndDate.ValueChanged
        Try
            dtpEndDate.MinDate = dtpStartDate.Value.Date

            If menAction <> enAction.EDIT_ONE AndAlso CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                If dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = False Then
                    objNoofDays.Text = (DateDiff(DateInterval.Day, dtpStartDate.Value.Date, dtpStartDate.Value.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpStartDate.Value.Date)).ToString()
                ElseIf dtpEndDate.ShowCheckBox = True AndAlso dtpEndDate.Checked = True Then
                    objNoofDays.Text = (DateDiff(DateInterval.Day, dtpStartDate.Value.Date, dtpEndDate.Value.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpEndDate.Value.Date)).ToString()
                Else
                    objNoofDays.Text = (DateDiff(DateInterval.Day, dtpStartDate.Value.Date, dtpEndDate.Value.Date.AddDays(1)) - GetNoOfHolidays(CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpEndDate.Value.Date)).ToString()
                End If
            ElseIf menAction = enAction.EDIT_ONE AndAlso CInt(cboLeaveType.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objLeaveFraction As New clsleaveday_fraction
                Dim mdclCount As Decimal = 0
                dtFraction = objLeaveFraction.GetList("List", mintLeaveFormUnkid, True).Tables(0)

                Dim objFrmFraction As New frmLeaveDay_fraction
                objFrmFraction._Startdate = dtpStartDate.Value.Date
                objFrmFraction._Enddate = dtpEndDate.Value.Date
                objFrmFraction._EmployeeId = CInt(cboEmployee.SelectedValue)
                objFrmFraction._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
                objFrmFraction._dtFraction = dtFraction
                objFrmFraction.GenerateDays()

                Dim drRow As DataRow() = dtFraction.Select("leavedate < '" & dtpStartDate.Value.Date & "'")

                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dtFraction.AcceptChanges()
                End If

                If Not IsDBNull(dtFraction.Compute("SUM(dayfraction)", "AUD<>'D'")) Then
                    mdclCount = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD<>'D'"))
                End If
                objNoofDays.Text = mdclCount.ToString("#0.00")

            ElseIf menAction <> enAction.EDIT_ONE AndAlso CInt(cboLeaveType.SelectedValue) <= 0 AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                objNoofDays.Text = "0.00"
            End If


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            GetAsOnDateAccrue()
            'Pinkal (18-Nov-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Nov-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    'Private Sub dtpApplyDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpApplyDate.ValueChanged
    '    Try
    '        GetAsOnDateAccrue()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dtpApplyDate_ValueChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (18-Nov-2016) -- End

#End Region

#Region "ComboBox's Event"

    Private Sub cboLeaveType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim dsList As DataSet = Nothing
        Try
            objBalance.Text = "0.00"
            objLeaveAccrue.Text = "0.00"
            objAsonDateAccrue.Text = "0.00"
            objAsonDateBalance.Text = "0.00"

            dtpStartDate.MinDate = CDate("01-Jan-1900")
            dtpEndDate.MinDate = CDate("01-Jan-1900")
            dtpApplyDate.MinDate = CDate("01-Jan-1900")

            dtpStartDate.MaxDate = CDate("01-Jan-9998")
            dtpEndDate.MaxDate = CDate("01-Jan-9998")
            dtpApplyDate.MaxDate = CDate("01-Jan-9998")


            If CInt(cboEmployee.SelectedValue) <= 0 OrElse CInt(cboLeaveType.SelectedValue) <= 0 Then Exit Sub
            Dim objEmployee As New clsEmployee_Master

            If CInt(cboEmployee.SelectedValue) > 0 Then

                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                If objEmployee._Appointeddate.Date < FinancialYear._Object._Database_Start_Date.Date Then


                ElseIf objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then

                    dtpStartDate.MinDate = objEmployee._Appointeddate.Date
                    dtpEndDate.MinDate = objEmployee._Appointeddate.Date
                    dtpApplyDate.MinDate = objEmployee._Appointeddate.Date

                End If

            End If

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            Dim objLType As New clsleavetype_master
            objLType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)

            Dim mintLeaveTypeId As Integer = objLType._Leavetypeunkid
            If objLType._DeductFromLeaveTypeunkid > 0 Then
                mintLeaveTypeId = objLType._DeductFromLeaveTypeunkid
            End If

            Dim mdtLvELCBalanceStartDate As Date = Nothing
            Dim mdtLvELCBalanceEndDate As Date = Nothing

            Dim objAccrueBalance As New clsleavebalance_tran

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                'dsList = objAccrueBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " AND lvleavebalance_tran.isvoid = 0", Nothing)
                dsList = objAccrueBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False, "lvleavebalance_tran.leavetypeunkid = " & mintLeaveTypeId & " AND lvleavebalance_tran.isvoid = 0", Nothing)
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                'dsList = objAccrueBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " AND lvleavebalance_tran.isvoid = 0", Nothing)
                dsList = objAccrueBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "lvleavebalance_tran.leavetypeunkid = " & mintLeaveTypeId & " AND lvleavebalance_tran.isvoid = 0", Nothing)
            End If

            LblELCStart.Text = ""

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    LblELCStart.Text = Language.getMessage(mstrModuleName, 16, "ELC Start From") & " : " & CDate(dsList.Tables(0).Rows(0)("startdate")).ToShortDateString & "  -  " & CDate(dsList.Tables(0).Rows(0)("enddate")).ToShortDateString
                    mdtLvELCBalanceStartDate = CDate(dsList.Tables(0).Rows(0)("startdate")).Date
                    mdtLvELCBalanceEndDate = CDate(dsList.Tables(0).Rows(0)("enddate")).Date
                End If

                GetBalanceInfo()


                If objEmployee._Termination_From_Date.Date <> Nothing OrElse objEmployee._Empl_Enddate.Date <> Nothing Then
                    If objEmployee._Termination_From_Date.Date <> Nothing AndAlso objEmployee._Termination_From_Date.Date < objEmployee._Termination_To_Date.Date Then
                        dtpStartDate.MaxDate = objEmployee._Termination_From_Date.Date
                        dtpEndDate.MaxDate = objEmployee._Termination_From_Date.Date
                    ElseIf objEmployee._Empl_Enddate.Date <> Nothing AndAlso objEmployee._Empl_Enddate.Date < objEmployee._Termination_To_Date.Date Then
                        dtpStartDate.MaxDate = objEmployee._Empl_Enddate.Date
                        dtpEndDate.MaxDate = objEmployee._Empl_Enddate.Date
                    End If
                Else
                    dtpStartDate.MaxDate = objEmployee._Termination_To_Date.Date
                    dtpEndDate.MaxDate = objEmployee._Termination_To_Date.Date
                End If

            End If

            If objLType._IsSickLeave = True Then
                dtpEndDate.Value = dtpStartDate.Value
                dtpEndDate.ShowCheckBox = True : dtpEndDate.Checked = False
            Else
                dtpEndDate.ShowCheckBox = False
            End If

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnLvTypeSkipApproverFlow = objLType._SkipApproverFlow
            lvLeaveExpense.Visible = Not mblnLvTypeSkipApproverFlow
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnIsLvTypePaid = objLType._IsPaid
            mintLvformMinDays = objLType._LVDaysMinLimit
            mintLvformMaxDays = objLType._LVDaysMaxLimit
            'Pinkal (08-Oct-2018) -- End

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            mblnConsiderLeaveOnTnAPeriod = objLType._ConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            mblnConsiderExpense = objLType._ConsiderExpense
            lvLeaveExpense.Visible = mblnConsiderExpense
            'Pinkal (03-May-2019) -- End


            mdtAttachement = Nothing
            If isFromLeaveProcess = False Then
                lnkScanDocuements.Visible = objLType._IsCheckDocOnLeaveForm
            End If


            If objLType._IsStopApplyAcrossFYELC Then

                If objLType._IsShortLeave = False Then

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                        dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        If dtpStartDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                            dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                        End If


                        dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        If dtpStartDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                            dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                        End If

                        dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        If dtpEndDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                            dtpEndDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                        End If

                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        If mdtLvELCBalanceStartDate <> Nothing AndAlso mdtLvELCBalanceEndDate <> Nothing Then

                            dtpApplyDate.MinDate = mdtLvELCBalanceStartDate.Date
                            dtpApplyDate.MaxDate = mdtLvELCBalanceEndDate.Date

                            dtpStartDate.MinDate = mdtLvELCBalanceStartDate.Date
                            dtpStartDate.MaxDate = mdtLvELCBalanceEndDate.Date

                            dtpEndDate.MinDate = mdtLvELCBalanceStartDate.Date
                            dtpEndDate.MaxDate = mdtLvELCBalanceEndDate.Date

                        End If

                    End If

                Else
                    dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                    If dtpStartDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                        dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                    End If


                    dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                    If dtpStartDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                        dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                    End If

                    dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                    If dtpEndDate.MaxDate > FinancialYear._Object._Database_End_Date.Date Then
                        dtpEndDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                    End If
                End If
            End If


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            If objLType._DoNotApplyForFutureDates = True Then
                If dtpApplyDate.MaxDate > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpApplyDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If
                If dtpStartDate.MaxDate > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpStartDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If
                If dtpEndDate.MaxDate > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    dtpEndDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If
            End If
            'Pinkal (05-Jun-2020) -- End


            mdtAttachement = Nothing
            dsList.Clear()
            dsList = Nothing
            objLType = Nothing
            objEmployee = Nothing

            dtpStartDate_ValueChanged(sender, e)

            'Pinkal (26-Feb-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveType_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList = Nothing
        End Try

    End Sub

#End Region


    'Pinkal (05-Jun-2020) -- Start
    'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
    Private Sub ExtraWebMessage()
        Language.getMessage(mstrModuleName, 56, "Sorry ! This leave type cannot overlap to next year.")
        Language.getMessage(mstrModuleName, 57, "Sorry, you cannot apply for this leave application.Reason:As Per setting configured on leave type master you can not apply for future date(s).")
    End Sub
    'Pinkal (05-Jun-2020) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbLeaveForm.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveForm.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbLeaveForm.Text = Language._Object.getCaption(Me.gbLeaveForm.Name, Me.gbLeaveForm.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblApplyDate.Text = Language._Object.getCaption(Me.lblApplyDate.Name, Me.lblApplyDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblReturnDate.Text = Language._Object.getCaption(Me.lblReturnDate.Name, Me.lblReturnDate.Text)
            Me.lblLeaveAddress.Text = Language._Object.getCaption(Me.lblLeaveAddress.Name, Me.lblLeaveAddress.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lnkAccrueleave.Text = Language._Object.getCaption(Me.lnkAccrueleave.Name, Me.lnkAccrueleave.Text)
            Me.lblDays.Text = Language._Object.getCaption(Me.lblDays.Name, Me.lblDays.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
            Me.lnkCopyEmpAddress.Text = Language._Object.getCaption(Me.lnkCopyEmpAddress.Name, Me.lnkCopyEmpAddress.Text)
            Me.lnkLeaveDayCount.Text = Language._Object.getCaption(Me.lnkLeaveDayCount.Name, Me.lnkLeaveDayCount.Text)
            Me.lvLeaveExpense.Text = Language._Object.getCaption(Me.lvLeaveExpense.Name, Me.lvLeaveExpense.Text)
            Me.LblELCStart.Text = Language._Object.getCaption(Me.LblELCStart.Name, Me.LblELCStart.Text)
            Me.LblLeaveAccrue.Text = Language._Object.getCaption(Me.LblLeaveAccrue.Name, Me.LblLeaveAccrue.Text)
            Me.LblAsonDateAccrue.Text = Language._Object.getCaption(Me.LblAsonDateAccrue.Name, Me.LblAsonDateAccrue.Text)
            Me.lblAsonDateBalance.Text = Language._Object.getCaption(Me.lblAsonDateBalance.Name, Me.lblAsonDateBalance.Text)
            Me.lnkScanDocuements.Text = Language._Object.getCaption(Me.lnkScanDocuements.Name, Me.lnkScanDocuements.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 2, "Form No cannot be blank. Form No is required information.")
            Language.setMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Apply date should not be greater than Start date.")
            Language.setMessage(mstrModuleName, 5, "Please Assign Approver to this employee and also map approver to system user.")
            Language.setMessage(mstrModuleName, 6, "Please Map this employee's Approver to system user.")
            Language.setMessage(mstrModuleName, 8, "Select")
            Language.setMessage(mstrModuleName, 9, "Please select correct start date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName, 10, "Please select correct end date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName, 11, "Please select correct apply date. Reason:This date period is already closed.")
            Language.setMessage(mstrModuleName, 12, "Please Map this Leave type to this employee's Approver(s).")
            Language.setMessage(mstrModuleName, 13, "This leave type is invalid for selected employee.")
            Language.setMessage(mstrModuleName, 14, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Language.setMessage(mstrModuleName, 16, "ELC Start From")
            Language.setMessage(mstrModuleName, 17, "Sorry, you cannot apply for this leave now as your previous leave form has not been issued yet.")
            Language.setMessage(mstrModuleName, 18, "Please Enter Accrue amount for this paid leave.")
            Language.setMessage(mstrModuleName, 19, "This particular leave is mapped with")
            Language.setMessage(mstrModuleName, 20, " , which does not have the accrue amount. Please add accrue amount for")
            Language.setMessage(mstrModuleName, 21, " inorder to issue leave.")
            Language.setMessage(mstrModuleName, 22, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency.")
            Language.setMessage(mstrModuleName, 23, "You cannot apply for this leave form.Reason : As these days are falling in holiday(s) and weekend.")
            Language.setMessage(mstrModuleName, 24, "Are you sure you want to save leave form without any leave expense ?")
            Language.setMessage(mstrModuleName, 25, "You have applied")
            Language.setMessage(mstrModuleName, 26, "leave for")
            Language.setMessage(mstrModuleName, 27, "days , from")
            Language.setMessage(mstrModuleName, 28, "to")
            Language.setMessage(mstrModuleName, 29, "Are you sure of the number of days and selected dates? If yes click YES to save the application, otherwise click NO to make corrections.")
            Language.setMessage(mstrModuleName, 30, "You cannot Edit Claim Expense Form.Reason: Claim Expense Form was already rejected.")
            Language.setMessage(mstrModuleName, 32, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to apply for this leave ?")
            Language.setMessage(mstrModuleName, 33, "Select Employee")
            Language.setMessage(mstrModuleName, 34, "Scan/Document(s) is compulsory information.Please attach Scan/Document(s) as per this leave type setting.")
            Language.setMessage(mstrModuleName, 35, "Sorry,You cannot apply leave expense for this leave type.Reason : This Leave Type is unpaid leave type.")
            Language.setMessage(mstrModuleName, 36, "Document path does not exist.")
            Language.setMessage(mstrModuleName, 37, "You cannot apply for this leave form.Reason : This Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Language.setMessage(mstrModuleName, 38, "You are not eligible to apply for this leave. Reason : Eligibility to apply for this Leave Type does not match with Eligibility days on leave balance.")
			Language.setMessage(mstrModuleName, 39, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is")
            Language.setMessage(mstrModuleName, 40, "You cannot apply for this leave form.Reason : This employee is already applied for employee budget timesheet for this tenure.")
  	    Language.setMessage(mstrModuleName, 41, "Sorry, you cannot apply for this leave type as it has been configured with setting for minimum")
	    Language.setMessage(mstrModuleName, 42, "Sorry, you cannot apply for this leave type as it has been configured with setting for maximum")
	    Language.setMessage(mstrModuleName, 43, "day(s) to be applied for this leave type.")
	    Language.setMessage(mstrModuleName, 44, "You cannot create this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type.")
			Language.setMessage(mstrModuleName, 46, "Are you sure want to save this leave application without expense application ?")
			Language.setMessage(mstrModuleName, 52, "Leave Application saved successfully.")
			Language.setMessage(mstrModuleName, 53, "Leave Application updated successfully.")
			Language.setMessage(mstrModuleName, 54, "Sorry ! You have to apply")
			Language.setMessage(mstrModuleName, 55, "before applying this type of leave.")
			Language.setMessage(mstrModuleName, 56, "Sorry ! This leave type cannot overlap to next year.")
			Language.setMessage(mstrModuleName, 57, "Sorry, you cannot apply for this leave application.Reason:As Per setting configured on leave type master you can not apply for future date(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class