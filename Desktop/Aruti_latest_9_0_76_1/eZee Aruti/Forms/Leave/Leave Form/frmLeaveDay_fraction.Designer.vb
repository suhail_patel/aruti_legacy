﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveDay_fraction
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveDay_fraction))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgLeaveFraction = New System.Windows.Forms.DataGridView
        Me.dgColhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFraction = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.BtnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgLeaveFraction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgLeaveFraction)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(331, 359)
        Me.pnlMain.TabIndex = 0
        '
        'dgLeaveFraction
        '
        Me.dgLeaveFraction.AllowUserToAddRows = False
        Me.dgLeaveFraction.AllowUserToDeleteRows = False
        Me.dgLeaveFraction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLeaveFraction.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgColhDate, Me.dgcolhFraction})
        Me.dgLeaveFraction.Location = New System.Drawing.Point(3, 3)
        Me.dgLeaveFraction.Name = "dgLeaveFraction"
        Me.dgLeaveFraction.RowHeadersVisible = False
        Me.dgLeaveFraction.RowHeadersWidth = 20
        Me.dgLeaveFraction.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgLeaveFraction.Size = New System.Drawing.Size(325, 295)
        Me.dgLeaveFraction.TabIndex = 1
        '
        'dgColhDate
        '
        Me.dgColhDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhDate.HeaderText = "Date"
        Me.dgColhDate.Name = "dgColhDate"
        Me.dgColhDate.ReadOnly = True
        Me.dgColhDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhFraction
        '
        Me.dgcolhFraction.AllowNegative = False
        Me.dgcolhFraction.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFraction.DecimalLength = 2
        DataGridViewCellStyle1.Format = "F2"
        Me.dgcolhFraction.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhFraction.HeaderText = "Fractions"
        Me.dgcolhFraction.Name = "dgcolhFraction"
        Me.dgcolhFraction.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhFraction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.BtnOk)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 304)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(331, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(235, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'BtnOk
        '
        Me.BtnOk.BackColor = System.Drawing.Color.White
        Me.BtnOk.BackgroundImage = CType(resources.GetObject("BtnOk.BackgroundImage"), System.Drawing.Image)
        Me.BtnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnOk.BorderColor = System.Drawing.Color.Empty
        Me.BtnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.BtnOk.FlatAppearance.BorderSize = 0
        Me.BtnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOk.ForeColor = System.Drawing.Color.Black
        Me.BtnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnOk.GradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.Location = New System.Drawing.Point(139, 13)
        Me.BtnOk.Name = "BtnOk"
        Me.BtnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.BtnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.BtnOk.Size = New System.Drawing.Size(90, 30)
        Me.BtnOk.TabIndex = 0
        Me.BtnOk.Text = "&Ok"
        Me.BtnOk.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmLeaveDay_fraction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 359)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveDay_fraction"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Day Count"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgLeaveFraction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents BtnOk As eZee.Common.eZeeLightButton
    Friend WithEvents dgLeaveFraction As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFraction As eZee.Common.DataGridViewNumericTextBoxColumn
End Class
