﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.IO

'Last Message Index = 6


Public Class frmLeaveFormList

#Region "Private Variable"

    Private objLeaveForm As clsleaveform
    Private ReadOnly mstrModuleName As String = "frmLeaveFormList"
    Dim mintApproverunkid As Integer = -1
    Private mstrAdvanceFilter As String = ""
    Public mstrEmployeeUnkids As String = ""
    Public mblnIsFromMail As Boolean = False
    Public mblnExport_Print As Boolean = False
    Public mstrLeaveFormIds As String = ""
    Public dsEmailListData As DataSet = Nothing

#End Region

#Region "Property"

    Public Property _EmployeeUnkids() As String
        Get
            Return mstrEmployeeUnkids
        End Get
        Set(ByVal value As String)
            mstrEmployeeUnkids = value
        End Set
    End Property

    Public WriteOnly Property _IsFromMail() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

    Public WriteOnly Property IsExport_Print() As Boolean
        Set(ByVal value As Boolean)
            mblnExport_Print = value
        End Set
    End Property

    Public ReadOnly Property LeaveFormIDs() As String
        Get
            Return mstrLeaveFormIds
        End Get
    End Property

    Public Property _Email_Data() As DataSet
        Get
            Return dsEmailListData
        End Get
        Set(ByVal value As DataSet)
            dsEmailListData = value
        End Set
    End Property

#End Region

#Region "Form's Event"

    Private Sub frmLeaveFormList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveForm = New clsleaveform
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Dim objMapping As clsapprover_Usermapping
            Dim objApprover As clsleaveapprover_master
            Language.setLanguage(Me.Name)

            dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date

            dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date

            dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpEndDate.MaxDate = FinancialYear._Object._Database_End_Date

            objMapping = New clsapprover_Usermapping
            objMapping.GetData(enUserType.Approver, , User._Object._Userunkid, )

            objApprover = New clsleaveapprover_master
            objApprover._Approverunkid = objMapping._Approverunkid
            mintApproverunkid = objApprover._Approverunkid

            'S.SANDEEP [30 JAN 2016] -- START
            'Dim objEmployee As New clsEmployee_Master

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''objEmployee._Employeeunkid = objApprover._leaveapproverunkid
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objApprover._leaveapproverunkid
            ''S.SANDEEP [04 JUN 2015] -- END


            'txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Surname
            'txtApprover.Tag = objMapping._Userunkid

            Dim dsAppr As New DataSet
            dsAppr = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , objApprover._Approverunkid)
            If dsAppr.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = CStr(dsAppr.Tables("List").Rows(0)("name"))
            End If
            txtApprover.Tag = objMapping._Userunkid
            dsAppr.Dispose()
            'S.SANDEEP [30 JAN 2016] -- END



            FillCombo()

            If lvLeaveFormList.Items.Count > 0 Then lvLeaveFormList_SelectedIndexChanged(sender, e)

            If lvLeaveFormList.Items.Count > 0 Then lvLeaveFormList.Items(0).Selected = True '
            objbtnSearch_Click(sender, e)

            If mblnIsFromMail = True Then
                lvLeaveFormList.CheckBoxes = True
                objColhCheck.Width = 25
                objChkAll.Visible = True
            Else
                objChkAll.Visible = False
                objColhCheck.Width = 0
                colhFormNo.Width = colhFormNo.Width + 25
                objEFooter.Visible = False
            End If

            lvLeaveFormList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveFormList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveFormList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvLeaveFormList.Focused Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveFormList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveFormList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveForm = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveform.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveform"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Menu's Event"

    Private Sub mnuLeaveAccrue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLeaveAccrue.Click
        Try
            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If


            Dim ObjFrm As New frmLeaveAccrue_AddEdit
            If (ObjFrm.displayDialog(-1, enAction.ADD_ONE, _
                CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), _
                CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text), _
                CInt(lvLeaveFormList.SelectedItems(0).Tag), CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhleaveissueunkid.Index).Text))) Then
                ' lvLeaveFormList.SelectedItems(0).SubItems(colhleaveissueunkid.Index).Text = ObjFrm.mintLeaveAccrueUnkid.ToString
                fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLeaveAccrue_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmLeaveForm_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvLeaveFormList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeaveFormList.Select()
            Exit Sub
        End If

        Dim objfrmLeaveForm_AddEdit As New frmLeaveForm_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeaveFormList.SelectedItems(0).Index
            If objfrmLeaveForm_AddEdit.displayDialog(CInt(lvLeaveFormList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmLeaveForm_AddEdit = Nothing
            lvLeaveFormList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeaveForm_AddEdit IsNot Nothing Then objfrmLeaveForm_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvLeaveFormList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeaveFormList.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeaveFormList.SelectedItems(0).Index


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim().Length > 0 Then
                Dim objClaimRequestMst As New clsclaim_request_master
                Dim mintClaimMstID As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(lvLeaveFormList.SelectedItems(0).Tag))
                objClaimRequestMst._Crmasterunkid = mintClaimMstID
                objClaimRequestMst = Nothing

                Dim objClaim As New clsclaim_request_tran
                objClaim._ClaimRequestMasterId = mintClaimMstID
                Dim dtTable As DataTable = objClaim._DataTable
                objClaim = Nothing
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                    If xBudgetMandatoryCount > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it."), CType(enMsgBoxStyle.Information, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If
            End If
            '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

            'Pinkal (20-Nov-2018) -- End


            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Leave Form?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLeaveForm._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objLeaveForm._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLeaveForm._Voiduserunkid = User._Object._Userunkid

                Dim objClaimRequestMst As New clsclaim_request_master
                Dim mintClaimMstID As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(lvLeaveFormList.SelectedItems(0).Tag))
                objClaimRequestMst._Crmasterunkid = mintClaimMstID
                objLeaveForm._ObjClaimRequestMst = objClaimRequestMst
              
                Dim objClaimTran As New clsclaim_request_tran
                Dim mdtAttachement As DataTable = Nothing
                objClaimTran._ClaimRequestMasterId = mintClaimMstID
                Dim dtExpense As DataTable = objClaimTran._DataTable
                objClaimTran = Nothing

                Dim objAttchement As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim))
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim), , , , , CBool(IIf(CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim) <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END

                Dim strTranIds As String = String.Join(",", dtExpense.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"

                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing
                For Each dRow As DataRow In mdtAttachement.Rows
                    dRow.Item("AUD") = "D"
                Next
                mdtAttachement.AcceptChanges()


                Dim mintLeaveTypeID As Integer = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
                Dim objlvType As New clsleavetype_master
                objlvType._Leavetypeunkid = mintLeaveTypeID
                If objlvType._IsCheckDocOnLeaveForm Then
                    Dim objScanAttach As New clsScan_Attach_Documents
                    Dim dtTable As DataTable = objScanAttach.GetAttachmentTranunkIds(CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim) _
                                                                                , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module _
                                                                                , CInt(lvLeaveFormList.SelectedItems(0).Tag))
                    objLeaveForm._dtAttachDocument = dtTable
                    dtTable = Nothing
                    objScanAttach = Nothing
                Else
                    objLeaveForm._dtAttachDocument = Nothing
                End If
                objlvType = Nothing

             
                If objLeaveForm.Delete(CInt(lvLeaveFormList.SelectedItems(0).Tag), FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True _
                                                , ConfigParameter._Object._IsIncludeInactiveEmp, mdtAttachement, True, True, ConfigParameter._Object._LeaveBalanceSetting) Then
             

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.

                    'objLeaveForm.SendMailToApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                    '                                                   , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                    '                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                    '                                                   , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                    '                                                   , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text) _
                    '                                                   , CInt(lvLeaveFormList.SelectedItems(0).Tag), lvLeaveFormList.SelectedItems(0).SubItems(colhFormNo.Index).Text _
                    '                                                   , True, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), "", enLogin_Mode.DESKTOP, 0)

                    Dim mstrDeleterName As String = User._Object._Firstname & " " & User._Object._Lastname

                    If mstrDeleterName.Trim.Length <= 0 Then
                        mstrDeleterName = User._Object._Username
                    End If

                    objLeaveForm.SendMailToApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                       , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                       , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                       , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                                                       , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text) _
                                                                       , CInt(lvLeaveFormList.SelectedItems(0).Tag), lvLeaveFormList.SelectedItems(0).SubItems(colhFormNo.Index).Text _
                                                                     , True, ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), "", enLogin_Mode.DESKTOP, 0, False, mstrDeleterName)


                    'Pinkal (01-Apr-2019) -- End

             
                    Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                    Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                    For Each dRow As DataRow In mdtAttachement.Rows
                        Dim strError As String = ""
                        If dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFileName As String = dRow("fileuniquename").ToString

                            If blnIsIISInstalled Then
                                'Hemant [8-April-2019] -- Start
                                If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                    'Hemant [8-April-2019] -- End
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            Else
                                If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                    Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                    If File.Exists(strDocLocalPath) Then
                                        File.Delete(strDocLocalPath)
                                    End If
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next

                End If

                lvLeaveFormList.SelectedItems(0).Remove()

                If lvLeaveFormList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvLeaveFormList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvLeaveFormList.Items.Count - 1
                    lvLeaveFormList.Items(intSelectedIndex).Selected = True
                    lvLeaveFormList.EnsureVisible(intSelectedIndex)
                ElseIf lvLeaveFormList.Items.Count <> 0 Then
                    lvLeaveFormList.Items(intSelectedIndex).Selected = True
                    lvLeaveFormList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvLeaveFormList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0
            txtFormNo.Text = ""

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then

                dtpApplyDate.Value = FinancialYear._Object._Database_End_Date.Date
                dtpStartDate.Value = FinancialYear._Object._Database_End_Date.Date
                dtpEndDate.Value = FinancialYear._Object._Database_End_Date.Date

            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then

                dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

            End If


            dtpApplyDate.Checked = False
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
            cboStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnEmailOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmailOk.Click
        Try
            If mblnExport_Print = False Then
                GetEmailLeaveForm()
            Else
                GetPrintExportLeaveForm()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsLeaveForm As New DataSet
        Dim strSearching As String = ""
        Dim dtLeaveForm As New DataTable
        Try

            If User._Object.Privilege._AllowToViewLeaveFormList = True Then


                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'dsLeaveForm = objLeaveForm.GetList("LeaveForm", True, mintApproverunkid, , , , , mstrAdvanceFilter)

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching = "AND lvleaveform.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveform.leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " "
                End If

                If txtFormNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND lvleaveform.FormNo like '%" & txtFormNo.Text.Trim & "%' "
                End If

                If dtpApplyDate.Checked Then
                    strSearching &= "AND lvleaveform.applydate='" & eZeeDate.convertDate(dtpApplyDate.Value) & "' "
                End If

                If dtpStartDate.Checked Then
                    strSearching &= "AND lvleaveform.startdate >='" & eZeeDate.convertDate(dtpStartDate.Value) & "' "
                End If

                If dtpEndDate.Checked Then
                    strSearching &= "AND lvleaveform.returndate <='" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveform.statusunkid = " & CInt(cboStatus.SelectedValue)
                End If



                'Pinkal (06-May-2014) -- Start
                'Enhancement : TRA Changes
                'If mstrAdvanceFilter.Length > 0 Then
                '    strSearching &= "AND " & mstrAdvanceFilter
                'End If
                'Pinkal (06-May-2014) -- End



                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtLeaveForm = New DataView(dsLeaveForm.Tables("LeaveForm"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtLeaveForm = dsLeaveForm.Tables("LeaveForm")
                'End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    strSearching &= "AND " & mstrAdvanceFilter
                End If

                dsLeaveForm = objLeaveForm.GetList("LeaveForm", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, mintApproverunkid, True, -1, strSearching)

                dtLeaveForm = dsLeaveForm.Tables("LeaveForm")

                'Pinkal (24-Aug-2015) -- End


                Dim lvItem As ListViewItem
                lvLeaveFormList.Items.Clear()


                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                Dim lvArray As New List(Of ListViewItem)
                lvLeaveFormList.BeginUpdate()
                'Pinkal (10-Jun-2020) -- End


                For Each drRow As DataRow In dtLeaveForm.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = ""
                    lvItem.SubItems.Add(drRow("formno").ToString())
                    lvItem.Tag = drRow("formunkid")
                    lvItem.SubItems.Add(drRow("employeecode").ToString)
                    lvItem.SubItems.Add(drRow("employeename").ToString)
                    lvItem.SubItems.Add(drRow("leavename").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("startdate").ToString).ToShortDateString)
                    If drRow("returndate").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("returndate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    lvItem.SubItems.Add(drRow("days").ToString)
                    If Not IsDBNull(drRow("Approved_StartDate")) Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Approved_StartDate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If

                    If Not IsDBNull(drRow("Approved_EndDate")) Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("Approved_EndDate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    If Not IsDBNull(drRow("Approved_Days")) Then
                        lvItem.SubItems.Add(Math.Round(CDec(drRow("Approved_Days")), 2).ToString())
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    lvItem.SubItems.Add(drRow("status").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("applydate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                    lvItem.SubItems.Add(drRow("statusunkid").ToString)
                    lvItem.SubItems.Add(drRow("leavetypeunkid").ToString)
                    lvItem.SubItems.Add(drRow("userunkid").ToString)
                    lvItem.SubItems.Add(drRow("leaveissueunkid").ToString)

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    lvItem.SubItems.Add(drRow("skipapproverflow").ToString())
                    'Pinkal (01-Oct-2018) -- End


                    'Pinkal (10-Jun-2020) -- Start
                    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                    'lvLeaveFormList.Items.Add(lvItem)
                    lvArray.Add(lvItem)
                    'Pinkal (10-Jun-2020) -- End

                Next

                If lvLeaveFormList.Items.Count > 16 Then
                    colhStatus.Width = 85 - 18
                Else
                    colhStatus.Width = 85
                End If


                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                RemoveHandler lvLeaveFormList.SelectedIndexChanged, AddressOf lvLeaveFormList_SelectedIndexChanged
                RemoveHandler lvLeaveFormList.ItemChecked, AddressOf lvLeaveFormList_ItemChecked

                lvLeaveFormList.Items.AddRange(lvArray.ToArray)

                AddHandler lvLeaveFormList.SelectedIndexChanged, AddressOf lvLeaveFormList_SelectedIndexChanged
                AddHandler lvLeaveFormList.ItemChecked, AddressOf lvLeaveFormList_ItemChecked

                'Pinkal (10-Jun-2020) -- End

                If lvLeaveFormList.Items.Count > 0 Then
                    lvLeaveFormList.Items(0).Selected = True
                End If


                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                lvArray.Clear()
                lvArray = Nothing
                lvLeaveFormList.EndUpdate()
                'Pinkal (10-Jun-2020) -- End


            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsLeaveForm.Dispose()
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, False)
            'End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END


            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")

            'FOR STATUS
            dsFill = Nothing
            Dim objMaster As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsFill = objMaster.getLeaveStatusList("Status", True)
            dsFill = objMaster.getLeaveStatusList("Status", ConfigParameter._Object._ApplicableLeaveStatus, True)
            'Pinkal (03-Jan-2020) -- End

            cboStatus.ValueMember = "statusunkid"
            cboStatus.DisplayMember = "name"
            cboStatus.DataSource = dsFill.Tables("Status")

            'FOR BYDEFAULT SET STATUS PENDING
            cboStatus.SelectedValue = 2

            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.getListForCombo("LeaveType", True)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            cboLeaveType.DataSource = dsFill.Tables("LeaveType")


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddLeaveForm
            btnDelete.Enabled = User._Object.Privilege._EditLeaveForm
            btnEdit.Enabled = User._Object.Privilege._EditLeaveForm
            mnuLeaveAccrue.Enabled = User._Object.Privilege._AllowLeaveAccrue
            mnuCancelLeaveForm.Enabled = User._Object.Privilege._AllowToCancelLeave
            mnuScanFormImages.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            btnPrint.Enabled = clsArutiReportClass.Is_Report_Assigned(enArutiReport.EmployeeLeaveForm)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub GetEmailLeaveForm()
        Dim objFields As New clsLetterFields
        Dim blnFlag As Boolean = False

        If lvLeaveFormList.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one Leave Form."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            For Each lvItem As ListViewItem In lvLeaveFormList.CheckedItems
                If mstrLeaveFormIds.Contains(lvItem.Tag.ToString) = False Then
                    mstrLeaveFormIds &= "," & lvItem.Tag.ToString
                End If
            Next

            If mstrLeaveFormIds.Length > 0 Then
                mstrLeaveFormIds = Mid(mstrLeaveFormIds, 2)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsEmailListData = objFields.GetEmployeeData(mstrLeaveFormIds, enImg_Email_RefId.Leave_Module, "DataTable")

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'dsEmailListData = objFields.GetEmployeeData(mstrLeaveFormIds, enImg_Email_RefId.Leave_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, "DataTable")
                dsEmailListData = objFields.GetEmployeeData(mstrLeaveFormIds, enImg_Email_RefId.Leave_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, Company._Object._DatabaseName, "DataTable")
                'Pinkal (10-Feb-2021) -- End


                'S.SANDEEP [04 JUN 2015] -- END

                If dsEmailListData.Tables("DataTable").Rows.Count > 0 Then
                    Dim dtRow() As DataRow = dsEmailListData.Tables("DataTable").Select("Email = ''")
                    If dtRow.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Some of the employees email addresses are blank. Do you wish to continue? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            For i As Integer = 0 To dtRow.Length - 1
                                dsEmailListData.Tables("DataTable").Rows.Remove(dtRow(i))
                            Next
                        End If
                    End If
                End If
            End If
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub GetPrintExportLeaveForm()
        Try
            Dim strNumber As String = ""
            Dim objLetterFields As New clsLetterFields
            Dim mstrGuests() As String
            mstrGuests = New String() {}
            Dim blnFlag As Boolean = False
            Dim intCnt As Integer = 0

            Dim iNDx As Integer = colhEmployee.Index

            If iNDx < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, The following columns are mandatory (Employee Name). Please add this columns inorder to print/export letter."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If lvLeaveFormList.CheckedItems.Count > 0 Then
                ReDim mstrGuests(lvLeaveFormList.CheckedItems.Count + 1)
                For i As Integer = 0 To lvLeaveFormList.CheckedItems.Count - 1
                    With lvLeaveFormList.CheckedItems(i)
                        If .Tag IsNot Nothing AndAlso .Tag.ToString <> "" Then
                            mstrGuests(intCnt) = .SubItems(iNDx).Text
                            If strNumber = "" Then
                                strNumber = .Tag.ToString
                            Else
                                strNumber = strNumber & " , " & .Tag.ToString
                            End If
                            intCnt += 1
                        End If
                    End With
                Next
                If strNumber <> "" Then
                    mstrLeaveFormIds = strNumber

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsEmailListData = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Leave_Module)

                    'S.SANDEEP |09-APR-2019| -- START
                    'dsEmailListData = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Leave_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsEmailListData = objLetterFields.GetEmployeeData(strNumber, enImg_Email_RefId.Leave_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                End If
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPrintExportLeaveForm", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "ListView Event"

    Private Sub lvLeaveFormList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLeaveFormList.SelectedIndexChanged
        Try
            If lvLeaveFormList.SelectedItems.Count > 0 Then

                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
                If objLeaveType._IsPaid Then
                    mnuLeaveAccrue.Enabled = True
                Else
                    mnuLeaveAccrue.Enabled = False
                End If

                If objLeaveType._IsShortLeave Then
                    mnuLeaveAccrue.Enabled = False
                Else
                    mnuLeaveAccrue.Enabled = True
                End If

                If CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 And _
                                CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date > ConfigParameter._Object._CurrentDateAndTime.Date Then

                    btnEdit.Enabled = False
                    btnDelete.Enabled = False

                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 And _
                                   CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then

                    btnEdit.Enabled = False
                    btnDelete.Enabled = False


                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 2 And _
                          CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True


                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 2 And _
                         CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhStartDate.Index).Text).Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then

                    If lvLeaveFormList.SelectedItems(0).SubItems(colhEndDate.Index).Text.Trim.Length <= 0 Then
                        btnEdit.Enabled = True
                        btnDelete.Enabled = True
                    Else
                        'Pinkal (03-Nov-2017) -- Start
                        'Enhancement - Ref id 90  Working on Allow to delete /edit leave form if is on past date, both user and employee should be able to delete this form for past dates if not a single approver has approved..
                        'btnEdit.Enabled = False
                        'btnDelete.Enabled = False
                        'Pinkal (03-Nov-2017) -- End
                    End If

                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False

                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 4 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False


                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 6 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False

                ElseIf CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 7 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False

                End If

                If CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 7 And _
                   (User._Object.Privilege._AllowToCancelLeave = True Or User._Object.Privilege._AllowToCancelPreviousDateLeave = True) Then
                    mnuCancelLeaveForm.Enabled = True
                Else
                    mnuCancelLeaveForm.Enabled = False
                End If


                If CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 2 Then
                    Dim objPending As New clspendingleave_Tran

                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                    'Dim dsPendingList As DataSet = objPending.GetList("Pending", True, "", "", "", -1, "lvpendingleave_tran.formunkid = " & CInt(lvLeaveFormList.SelectedItems(0).Tag) & _
                    '                                                  " AND lvpendingleave_tran.employeeunkid = " & CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) & _
                    '                                                  " AND lvpendingleave_tran.statusunkid <> 2 ")

                    Dim dsPendingList As DataSet = objPending.GetList("Pending", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                 , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                 , True, "lvpendingleave_tran.formunkid = " & CInt(lvLeaveFormList.SelectedItems(0).Tag) & _
                                                                      " AND lvpendingleave_tran.employeeunkid = " & CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) & _
                                                                      " AND lvpendingleave_tran.statusunkid <> 2 ")

                    'Pinkal (24-Aug-2015) -- End

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        btnEdit.Enabled = False
                        btnDelete.Enabled = False
                        'Pinkal (03-Nov-2017) -- Start
                        'Enhancement - Ref id 90  Working on Allow to delete /edit leave form if is on past date, both user and employee should be able to delete this form for past dates if not a single approver has approved..
                    Else
                        btnEdit.Enabled = True
                        btnDelete.Enabled = True
                        'Pinkal (03-Nov-2017) -- End
                    End If

                End If


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveFormList_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub lvLeaveFormList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveFormList.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvLeaveFormList.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvLeaveFormList.CheckedItems.Count < lvLeaveFormList.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvLeaveFormList.CheckedItems.Count = lvLeaveFormList.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveFormList_ItemChecked", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub mnuScanFormImages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanFormImages.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try

            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If


            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Leave_Module, enAction.ADD_ONE, "")

            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Leave_Module _
            '                                                          , enAction.ADD_ONE, lvLeaveFormList.SelectedItems(0).Tag.ToString(), mstrModuleName _
            '                                                          , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
            '                                                          , enScanAttactRefId.LEAVEFORMS) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END

            frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Leave_Module _
                                                                      , enAction.ADD_ONE, lvLeaveFormList.SelectedItems(0).Tag.ToString(), mstrModuleName, True _
                                                                      , CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text) _
                                                                      , enScanAttactRefId.LEAVEFORMS) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END

            'Pinkal (03-Jan-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanFormImages_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuCancelLeaveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCancelLeaveForm.Click
        Try
            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim().Length > 0 Then
                Dim objClaimRequestMst As New clsclaim_request_master
                Dim mintClaimMstID As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(lvLeaveFormList.SelectedItems(0).Tag))
                objClaimRequestMst._Crmasterunkid = mintClaimMstID
                objClaimRequestMst = Nothing

                Dim objClaim As New clsclaim_request_tran
                objClaim._ClaimRequestMasterId = mintClaimMstID
                Dim dtTable As DataTable = objClaim._DataTable
                objClaim = Nothing
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                    If xBudgetMandatoryCount > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot cancel this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it."), CType(enMsgBoxStyle.Information, enMsgBoxStyle))
                        Exit Sub
                    End If
                End If
            End If
            '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

            'Pinkal (20-Nov-2018) -- End



            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Matthew, 10:45 We discussed this yesterday. Remove the validation on cancelling leave. Only validate during application Yes, i was with Rutta and NMB guy]
            'Dim objLvType As New clsleavetype_master
            'objLvType._Leavetypeunkid = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
            'If objLvType._DeductFromLeaveTypeunkid > 0 Then
            '    objLvType._Leavetypeunkid = objLvType._DeductFromLeaveTypeunkid
            '    If objLeaveForm.IsLeaveForm_Exists(CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text), objLvType._Leavetypeunkid, True) Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You cannot cancel this leave form for this leave type.Reason : This leave type is configured as deducted from") & " " & _
            '                                                   objLvType._Leavename & " " & Language.getMessage(mstrModuleName, 16, "which has been already in Pending/approved/issued status."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            'objLvType = Nothing
            'Pinkal (13-Jun-2019) -- End


            'Pinkal (30-May-2020) -- Start
            'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
            If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo) AndAlso ConfigParameter._Object._AllowToCancelLeaveForClosedPeriod = False Then

                Dim objMaster As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran
                Dim mblnStartDateClose As Boolean = False
                Dim mblnEndDateClose As Boolean = False
                Dim mintStartPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhStartDate.Index).Text), FinancialYear._Object._YearUnkid, 0)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintStartPeriodId

                If objPeriod._Statusid = 2 Then mblnStartDateClose = True
                
                Dim mintEndPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(lvLeaveFormList.SelectedItems(0).SubItems(colhEndDate.Index).Text), FinancialYear._Object._YearUnkid, 0)

                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintEndPeriodId

                If objPeriod._Statusid = 2 Then mblnEndDateClose = True

                If mblnStartDateClose = True OrElse mblnEndDateClose = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You can not cancel this leave application.Reason: Leave Date(s) are fall in period which is already closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If
            'Pinkal (30-May-2020) -- End



            Dim objIssue As New clsleaveissue_Tran
            Dim dsList As DataSet = objIssue.GetList("List", True, CInt(lvLeaveFormList.SelectedItems(0).Tag))
            Dim drRow As DataRow() = Nothing
            If User._Object.Privilege._AllowToCancelPreviousDateLeave = False AndAlso User._Object.Privilege._AllowToCancelLeave = True Then
                drRow = dsList.Tables(0).Select("leavedate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' AND isvoid = 0")
            ElseIf User._Object.Privilege._AllowToCancelPreviousDateLeave = True AndAlso User._Object.Privilege._AllowToCancelLeave = False Then
                drRow = dsList.Tables(0).Select("leavedate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' AND isvoid = 0")
            ElseIf User._Object.Privilege._AllowToCancelPreviousDateLeave = True AndAlso User._Object.Privilege._AllowToCancelLeave = True Then
                drRow = dsList.Tables(0).Select("formunkid = " & CInt(lvLeaveFormList.SelectedItems(0).Tag) & " AND isvoid = 0")
            End If

            If drRow.Length > 0 Then
                Dim objCancelLeaveform As New frmCancel_leaveform
                objCancelLeaveform._Employeeunkid = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text)
                objCancelLeaveform._Formunkid = CInt(lvLeaveFormList.SelectedItems(0).Tag)
                objCancelLeaveform._LeaveTypeunkid = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
                objCancelLeaveform._DataList = dsList

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                objCancelLeaveform._SkipApproverFlow = CBool(lvLeaveFormList.SelectedItems(0).SubItems(objdgcolhSkipApproverFlow.Index).Text)
                'Pinkal (01-Oct-2018) -- End

                objCancelLeaveform.ShowDialog()
                fillList()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no Leave data to cancel the leave form."), enMsgBoxStyle.Information) '?1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCancelLeaveForm_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            frm._Hr_EmployeeTable_Alias = "h1"
            'Pinkal (06-May-2014) -- End

            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPreviewForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewForm.Click
        Try
            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If

            Dim objLeaveFormRpt As New ArutiReports.clsEmployeeLeaveForm(User._Object._Languageunkid, Company._Object._Companyunkid)

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            objLeaveFormRpt._LeaveFormId = CInt(lvLeaveFormList.SelectedItems(0).Tag)
            'Pinkal (06-Mar-2014) -- End

            objLeaveFormRpt._LeaveFormNo = lvLeaveFormList.SelectedItems(0).SubItems(colhFormNo.Index).Text.Trim.ToString()
            objLeaveFormRpt._EmployeeId = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim)
            objLeaveFormRpt._LeaveTypeId = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text.Trim())


            'Pinkal (27-May-2014) -- Start
            'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
            objLeaveFormRpt._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveFormRpt._YearId = FinancialYear._Object._YearUnkid
            'Pinkal (27-May-2014) -- End


            'Pinkal (09-Sep-2016) -- Start
            'Enhancement - Implemented VFT Changes For Leave Form Report.
            objLeaveFormRpt._Fin_StartDate = FinancialYear._Object._Database_Start_Date.Date
            objLeaveFormRpt._Fin_Enddate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (09-Sep-2016) -- End


            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objLeaveFormRpt._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objLeaveFormRpt._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (16-Dec-2016) -- End

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveFormRpt.generateReport(0, enPrintAction.Preview)
            objLeaveFormRpt.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                   , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                   , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, 0)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreviewForm_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPrintForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintForm.Click
        Try
            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If

            Dim objLeaveFormRpt As New ArutiReports.clsEmployeeLeaveForm(User._Object._Languageunkid, Company._Object._Companyunkid)

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            objLeaveFormRpt._LeaveFormId = CInt(lvLeaveFormList.SelectedItems(0).Tag)
            'Pinkal (06-Mar-2014) -- End

            objLeaveFormRpt._LeaveFormNo = lvLeaveFormList.SelectedItems(0).SubItems(colhFormNo.Index).Text.Trim.ToString()
            objLeaveFormRpt._EmployeeId = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim)
            objLeaveFormRpt._LeaveTypeId = CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text.Trim())
            'Pinkal (27-May-2014) -- Start
            'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
            objLeaveFormRpt._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveFormRpt._YearId = FinancialYear._Object._YearUnkid
            'Pinkal (27-May-2014) -- End

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objLeaveFormRpt._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objLeaveFormRpt._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (16-Dec-2016) -- End

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objLeaveFormRpt.generateReport(0, enPrintAction.Print)
            objLeaveFormRpt.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                  , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                  , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Print, enExportAction.None, 0)
            'Pinkal (24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintForm_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes

    Private Sub mnuPreviewScanDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewScanDoc.Click
        Dim objForm As New frmPreviewDocuments

        Try

            If lvLeaveFormList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvLeaveFormList.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                objForm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objForm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objForm)
            End If


            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

            Dim mstrTranId As String = ""
            Dim objScanAttach As New clsScan_Attach_Documents
            Dim dtTable As DataTable = objScanAttach.GetAttachmentTranunkIds(CInt(lvLeaveFormList.SelectedItems(0).SubItems(objcolhEmployeeunkid.Index).Text.Trim) _
                                                                               , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module _
                                                                               , CInt(lvLeaveFormList.SelectedItems(0).Tag))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTable.Rows
                    mstrTranId &= CInt(dRow("scanattachtranunkid")) & ","
                Next
            End If

            If mstrTranId.Trim.Length > 0 Then
                mstrTranId = mstrTranId.Trim.Substring(0, mstrTranId.Trim.Length - 1)
            End If

            objForm.displayDialog(mstrTranId.ToString)

            'Pinkal (07-May-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreviewScanDoc_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Jan-2014) -- End


#End Region

#Region "CheckBox Event"

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvLeaveFormList.ItemChecked, AddressOf lvLeaveFormList_ItemChecked
            For Each lvItem As ListViewItem In lvLeaveFormList.Items
                lvItem.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvLeaveFormList.ItemChecked, AddressOf lvLeaveFormList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


	
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnPrint.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPrint.GradientForeColor = GUI._ButttonFontColor

			Me.btnEClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmailOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmailOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
			Me.lblApplyDate.Text = Language._Object.getCaption(Me.lblApplyDate.Name, Me.lblApplyDate.Text)
			Me.lblReturnDate.Text = Language._Object.getCaption(Me.lblReturnDate.Name, Me.lblReturnDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhFormNo.Text = Language._Object.getCaption(CStr(Me.colhFormNo.Tag), Me.colhFormNo.Text)
			Me.colhApplyDate.Text = Language._Object.getCaption(CStr(Me.colhApplyDate.Tag), Me.colhApplyDate.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.mnuLeaveAccrue.Text = Language._Object.getCaption(Me.mnuLeaveAccrue.Name, Me.mnuLeaveAccrue.Text)
			Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.mnuPreviewForm.Text = Language._Object.getCaption(Me.mnuPreviewForm.Name, Me.mnuPreviewForm.Text)
			Me.mnuPrintForm.Text = Language._Object.getCaption(Me.mnuPrintForm.Name, Me.mnuPrintForm.Text)
			Me.mnuEmailForm.Text = Language._Object.getCaption(Me.mnuEmailForm.Name, Me.mnuEmailForm.Text)
			Me.mnuScanFormImages.Text = Language._Object.getCaption(Me.mnuScanFormImages.Name, Me.mnuScanFormImages.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.colLeaveType.Text = Language._Object.getCaption(CStr(Me.colLeaveType.Tag), Me.colLeaveType.Text)
			Me.colhLeaveDays.Text = Language._Object.getCaption(CStr(Me.colhLeaveDays.Tag), Me.colhLeaveDays.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.mnuCancelLeaveForm.Text = Language._Object.getCaption(Me.mnuCancelLeaveForm.Name, Me.mnuCancelLeaveForm.Text)
			Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
			Me.btnEmailOk.Text = Language._Object.getCaption(Me.btnEmailOk.Name, Me.btnEmailOk.Text)
			Me.colhApprovedStartDate.Text = Language._Object.getCaption(CStr(Me.colhApprovedStartDate.Tag), Me.colhApprovedStartDate.Text)
			Me.colhApprovedEndDate.Text = Language._Object.getCaption(CStr(Me.colhApprovedEndDate.Tag), Me.colhApprovedEndDate.Text)
			Me.colhApprovedDays.Text = Language._Object.getCaption(CStr(Me.colhApprovedDays.Tag), Me.colhApprovedDays.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.mnuPreviewScanDoc.Text = Language._Object.getCaption(Me.mnuPreviewScanDoc.Name, Me.mnuPreviewScanDoc.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Leave Form from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Leave Form?")
			Language.setMessage(mstrModuleName, 3, "Select Employee")
			Language.setMessage(mstrModuleName, 4, "There is no Leave data to cancel the leave form.")
			Language.setMessage(mstrModuleName, 5, "Please check atleast one Leave Form.")
			Language.setMessage(mstrModuleName, 6, "Some of the employees email addresses are blank. Do you wish to continue?")
			Language.setMessage(mstrModuleName, 7, "Sorry, The following columns are mandatory (Employee Name). Please add this columns inorder to print/export letter.")
			Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot cancel this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot delete this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it.")
			Language.setMessage(mstrModuleName, 15, "You cannot cancel this leave form for this leave type.Reason : This leave type is configured as deducted from")
			Language.setMessage(mstrModuleName, 16, "which has been already in Pending/approved/issued status.")
Language.setMessage(mstrModuleName, 17, "You can not cancel this leave application.Reason: Leave Date(s) are fall in period which is already closed.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class