﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text.RegularExpressions

Public Class frmLeaveAccrue_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLeaveAccrue_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLeaveAccrue As clsleavebalance_tran
    Private menAction As enAction = enAction.ADD_ONE
    Friend mintLeavebalalnceUnkid As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mintLeaveTypeunkid As Integer = -1
    Private mintFormunkid As Integer = -1
    Private mintIssueunkid As Integer = -1
    Private mdblTempAccrueamout As Double = 0
    Private mstrLeaveTypeID As String = "0"

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intLeavetypeunkid As Integer = -1, Optional ByVal intFormunkid As Integer = -1, Optional ByVal intIssueunkid As Integer = -1) As Boolean
        Try
            mintLeavebalalnceUnkid = intUnkId
            mintEmployeeunkid = intEmployeeunkid
            mintLeaveTypeunkid = intLeavetypeunkid
            mintFormunkid = intFormunkid
            mintIssueunkid = intIssueunkid
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintLeavebalalnceUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLeaveAccrue_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveAccrue = New clsleavebalance_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            setColor()
            FillCombo()

            dtpAsOnDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpAsOnDate.MaxDate = FinancialYear._Object._Database_End_Date
            If menAction = enAction.EDIT_ONE Then
                objLeaveAccrue._LeaveBalanceunkid = mintLeavebalalnceUnkid
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dtpStopDate.Enabled = False
            End If


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            Select Case ConfigParameter._Object._LeaveAccrueTenureSetting
                Case enLeaveAccrueTenureSetting.Yearly
                    lblAccrualAmount.Visible = True
                    LblMonthlyAccrue.Visible = False
                Case enLeaveAccrueTenureSetting.Monthly
                    LblMonthlyAccrue.Visible = True
                    lblAccrualAmount.Visible = False
            End Select
            'Pinkal (18-Nov-2016) -- End

            GetValue()
            cboLeaveYear.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveAccrue_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveAccrue_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveAccrue_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveAccrue_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            ElseIf e.KeyCode = Keys.Delete Then
                btnDelete_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveAccrue_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveAccrue_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeaveAccrue = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()



        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveYear.SelectedIndexChanged
        Try
            Call cboEmployee_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dtFill As DataTable = Nothing
        Try
            If cboEmployee.SelectedValue Is Nothing AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
            'FOR ISSUE LEAVE TYPE
            Dim objleavetype As New clsleavetype_master
            Dim dsFill As DataSet = objleavetype.GetListFromLeaveForm("LeaveType", CInt(cboEmployee.SelectedValue), True)
            cboIssueLeaveCode.ValueMember = "leavetypeunkid"
            cboIssueLeaveCode.DisplayMember = "name"


            If mintLeaveTypeunkid > 0 Then
                dtFill = New DataView(dsFill.Tables("LeaveType"), "leavetypeunkid=" & mintLeaveTypeunkid & " AND ispaid = 1 AND isaccrueamount = 1 AND isshortleave = false", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid = 1 AND isaccrueamount = 1 AND isshortleave = false", "", DataViewRowState.CurrentRows).ToTable
            End If

            mstrLeaveTypeID = ""

            If dtFill.Rows.Count = 0 Then
                Dim drrow As DataRow = dtFill.NewRow
                drrow("leavetypeunkid") = 0
                drrow("name") = "Select"
                dtFill.Rows.InsertAt(drrow, 0)
            End If

            cboIssueLeaveCode.DataSource = dtFill

            Dim objEmployee As New clsEmployee_Master

            If CInt(cboEmployee.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmployee._Employeeunkid(dtpAsOnDate.Value.Date) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    If objEmployee._Appointeddate.Date < FinancialYear._Object._Database_Start_Date.Date Then
                        dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        dtpStartDate.Value = FinancialYear._Object._Database_Start_Date.Date

                    ElseIf objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
                        dtpStartDate.MinDate = objEmployee._Appointeddate.Date
                        dtpStopDate.MinDate = objEmployee._Appointeddate.Date
                        dtpStartDate.Value = objEmployee._Appointeddate.Date

                    End If

                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                        dtpStartDate.MinDate = objEmployee._Reinstatementdate.Date
                        dtpStopDate.MinDate = objEmployee._Reinstatementdate.Date
                        dtpStartDate.Value = objEmployee._Reinstatementdate.Date
                    End If


                    If objEmployee._Termination_To_Date <> Nothing Then
                        dtpStopDate.Checked = True
                        dtpStopDate.Value = objEmployee._Termination_To_Date
                    Else
                        dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        dtpStopDate.Checked = False
                    End If

                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    dtpAsOnDate.MinDate = CDate("01-Jan-1900")
                    dtpAsOnDate.MaxDate = CDate("01-Jan-9998")

                    dtpAsOnDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                    dtpAsOnDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

                    If ConfigParameter._Object._CurrentDateAndTime.Date > FinancialYear._Object._Database_End_Date.Date Then
                        dtpAsOnDate.Value = FinancialYear._Object._Database_End_Date.Date
                    Else
                        dtpAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    End If

                    If objEmployee._Appointeddate.Date <> Nothing Then
                        dtpStartDate.MinDate = objEmployee._Appointeddate.Date
                        dtpStartDate.Value = objEmployee._Appointeddate.Date
                    End If

                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                        dtpStartDate.MinDate = objEmployee._Reinstatementdate.Date
                        dtpStartDate.Value = objEmployee._Reinstatementdate.Date
                    End If

                    If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < dtpStartDate.Value.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                        dtpStopDate.Value = objEmployee._Termination_To_Date
                    Else
                        dtpStopDate.Value = dtpStartDate.Value.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                    End If

                    dtpStopDate.Checked = True
                    dtpStopDate.Enabled = False


                End If

                radIssuebal.Checked = True
                txtAccrualAmount.Text = ""
                If cboLeaveCode.SelectedIndex > 0 Then cboLeaveCode.SelectedIndex = 0
            End If
            txtCfAmount.Decimal = 0
            txtEligibilityAfter.Decimal = 0

            chkNoAction.Checked = False


            FillAccrueList()
            'FOR CALLING SELECTEDINDEX CHANGED OF ISSUE LEAVE CODE
            If Not dsFill Is Nothing Then
                cboIssueLeaveCode_SelectedIndexChanged(sender, e)
                cboForm_SelectedIndexChanged(sender, e)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLeaveCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveCode.SelectedIndexChanged
        Try


            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrLeaveTypeID = ""
            'Pinkal (06-Feb-2013) -- End

            If CInt(cboLeaveCode.SelectedValue) = 0 Then
                cboLeaveCode.Tag = CInt(cboLeaveCode.SelectedValue)
                GetLeaveBalanceInfo()
                Exit Sub
            End If

            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)
            cboLeaveCode.Tag = objLeaveType._IsPaid



            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes



            Dim dFill As DataTable = objLeaveType.GetDeductdToLeaveType("List", CInt(cboEmployee.SelectedValue), True)

            For i As Integer = 0 To dFill.Rows.Count - 1
                If CInt(cboLeaveCode.SelectedValue) = CInt(dFill.Rows(i)("deductfromlvtypeunkid")) Then
                    'Pinkal (02-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'mstrLeaveTypeID &= CInt(cboLeaveCode.SelectedValue).ToString() & "," & dFill.Rows(i)("leavetypeunkid").ToString() & ","
                    mstrLeaveTypeID &= dFill.Rows(i)("leavetypeunkid").ToString() & ","
                    'Pinkal (02-Nov-2021) -- End
                Else
                    'Pinkal (01-Apr-2019) -- Start
                    'Bug [0003736] - leave remaining balance, Issued days and as on acrued days not displaying on leave Accrue screen reports are fine
                    'mstrLeaveTypeID &= CInt(cboLeaveCode.SelectedValue).ToString()
                    mstrLeaveTypeID = CInt(cboLeaveCode.SelectedValue).ToString()
                    'Pinkal (01-Apr-2019) -- End
                End If
            Next

            If mstrLeaveTypeID.Length > 0 AndAlso mstrLeaveTypeID.Trim <> "0" AndAlso mstrLeaveTypeID.Trim.Contains(",") Then

                'Pinkal (02-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'mstrLeaveTypeID = mstrLeaveTypeID.Substring(0, mstrLeaveTypeID.Length - 1)
                mstrLeaveTypeID &= CInt(cboLeaveCode.SelectedValue)
                'Pinkal (02-Nov-2021) -- End

            End If


            GetLeaveBalanceInfo()

            'Pinkal (06-Feb-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboIssueLeaveCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIssueLeaveCode.SelectedIndexChanged
        Try
            Dim objleaveissue As New clsleaveissue_master
            Dim dtFill As DataTable

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'Dim dsFill As DataSet = objleaveissue.GetFormNo(CInt(cboEmployee.SelectedValue), CInt(cboIssueLeaveCode.SelectedValue))
            Dim dsFill As DataSet = objleaveissue.GetFormNo(CInt(cboEmployee.SelectedValue), CInt(cboIssueLeaveCode.SelectedValue), ConfigParameter._Object._LeaveBalanceSetting)
            'Pinkal (21-Jul-2014) -- End


            cboForm.ValueMember = "formunkid"
            cboForm.DisplayMember = "formno"
            If mintFormunkid > 0 Then
                dtFill = New DataView(dsFill.Tables(0), "formunkid=" & mintFormunkid, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFill = dsFill.Tables(0)
            End If
            cboForm.DataSource = dtFill

            FillIssueList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIssueLeaveCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboForm_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboForm.SelectedIndexChanged
        Try
            If cboForm.SelectedValue Is Nothing Or CInt(cboForm.SelectedValue) <= 0 Then
                lstIssueLeave.Items.Clear()
                txtIssuemount.Text = "0"
                Exit Sub
            End If

            FillIssueList()

            ''FOR ALL EMPLOYEE
            GetLeaveIssueUnkId()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf CInt(cboLeaveCode.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Code is compulsory information.Please Select Leave Code."), enMsgBoxStyle.Information)
                cboLeaveCode.Select()
                Exit Sub
            ElseIf txtAccrualAmount.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Days cannot be blank. Days is required information."), enMsgBoxStyle.Information)
                txtAccrualAmount.Select()
                Exit Sub
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'Dim dsList As DataSet = objLeaveAccrue.GetList("List", True, False, CInt(cboEmployee.SelectedValue), True, True, False)
                Dim dsList As DataSet = objLeaveAccrue.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                        , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing)
                'Pinkal (24-Aug-2015) -- End
                Dim dtTable As DataTable = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(cboLeaveCode.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                    If CDate(dtTable.Rows(0)("enddate")).Date >= dtpStartDate.Value.Date OrElse CDate(dtTable.Rows(0)("enddate")).Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry,You cannot create new ELC as current ELC is already active."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If
            End If


            Dim objemployee As New clsEmployee_Master
            objemployee._Employeeunkid(dtpAsOnDate.Value.Date) = CInt(cboEmployee.SelectedValue)

            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)

            If objLeaveType._Gender <> 0 AndAlso objLeaveType._Gender <> objemployee._Gender Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "This leave type is invalid for selected employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboLeaveCode.Select()
                Exit Sub
            End If

            If txtCfAmount.Text = "" OrElse txtEligibilityAfter.Text = "" Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You have not set CF amount or leave eligibility days. 0 will be set for this employee.Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            SetValue()


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'blnFlag = objLeaveAccrue.Insert()

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'blnFlag = objLeaveAccrue.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                               , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
            '                                               , True, True)
            blnFlag = objLeaveAccrue.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                           , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                         , True, False)
            'Pinkal (06-Jan-2016) -- End
            'Pinkal (24-Aug-2015) -- End



            If blnFlag = False And objLeaveAccrue._Message <> "" Then
                eZeeMsgBox.Show(objLeaveAccrue._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLeaveAccrue = Nothing
                    objLeaveAccrue = New clsleavebalance_tran
                    GetValue()
                    cboLeaveCode.SelectedIndex = 0
                    cboLeaveYear.Select()
                Else
                    mintLeavebalalnceUnkid = objLeaveAccrue._LeaveBalanceunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If (lvAccrue.FocusedItem Is Nothing And lstIssueLeave.FocusedItem Is Nothing) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Leave from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf lvAccrue.SelectedItems.Count = 0 And lvAccrue.Focused Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Accure Leave from the list to perform further operation."), enMsgBoxStyle.Information)
                lvAccrue.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer = -1

            If lvAccrue.SelectedItems.Count <= 0 Then Exit Sub
            intSelectedIndex = lvAccrue.SelectedItems(0).Index
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this Accrued Leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLeaveAccrue._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objLeaveAccrue._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLeaveAccrue._Voiduserunkid = User._Object._Userunkid

                'Pinkal (02-Sep-2015) -- Start
                'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.
                'Dim blnFlag As Boolean = objLeaveAccrue.Delete(CInt(lvAccrue.SelectedItems(0).Tag), CInt(cboEmployee.SelectedValue))
                Dim blnFlag As Boolean = objLeaveAccrue.Delete(CInt(lvAccrue.SelectedItems(0).Tag), -1)

                If blnFlag = False And objLeaveAccrue._Message <> "" Then
                    eZeeMsgBox.Show(objLeaveAccrue._Message, enMsgBoxStyle.Information)
                Else

                    objLeaveAccrue = New clsleavebalance_tran

                    'Pinkal (01-Dec-2014) -- Start
                    'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
                    mintLeavebalalnceUnkid = -1
                    'Pinkal (01-Dec-2014) -- End

                    mdblTempAccrueamout = 0
                    cboLeaveCode.SelectedValue = 0
                    txtAccrualAmount.Text = ""
                    txtAccrualAmount.Tag = ""
                    cboLeaveCode.Select()


                    lvAccrue.SelectedItems(0).Remove()
                    If lvAccrue.Items.Count <= 0 Then
                        Exit Try
                    End If

                    If lvAccrue.Items.Count = intSelectedIndex Then
                        intSelectedIndex = lvAccrue.Items.Count - 1
                        lvAccrue.Items(intSelectedIndex).Selected = True
                        lvAccrue.EnsureVisible(intSelectedIndex)
                    ElseIf lvAccrue.Items.Count <> 0 Then
                        lvAccrue.Items(intSelectedIndex).Selected = True
                        lvAccrue.EnsureVisible(intSelectedIndex)
                    End If
                End If
            End If
            lvAccrue.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGlobalAssign.Click
        Try
            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            End If

            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes

            Dim ObjFrm As New frmGlobalLeaveAssign

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                If ObjFrm.displayDialog(-1, CInt(cboLeaveYear.SelectedValue), CInt(cboLeaveCode.SelectedValue), enAction.ADD_ONE) Then
                    FillAccrueList()
                End If

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                If ObjFrm.displayDialog(-1, CInt(cboLeaveYear.SelectedValue), -1, enAction.ADD_CONTINUE) Then
                    FillAccrueList()
                    cboLeaveCode.SelectedIndex = 0
                End If

            End If

            'Pinkal (06-Feb-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGlobalAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnDeleteByBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDeleteByBatch.Click
        Try
            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf lvAccrue.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no Accrued leave to delete for this employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim ObjFrm As New frmDeleteAccruedLeave
            ObjFrm.Text = "Delete Accrued Leave By Batch"
            'Sandeep [ 14 DEC 2010 ] -- Start
            'ObjFrm._Index = 2
            ObjFrm._Index = 1
            'Sandeep [ 14 DEC 2010 ] -- End 
            If ObjFrm.displayDialog(CInt(cboEmployee.SelectedValue), enAction.ADD_ONE) Then
                FillAccrueList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDeleteByBatch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnDeleteByLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDeleteByLeave.Click
        Try

            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf lvAccrue.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no Accrued leave to delete for this employee."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf lvAccrue.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Leave type to delete for this employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim ObjFrm As New frmDeleteAccruedLeave
            ObjFrm.Text = "Delete Accrued Leave By Leave"
            ObjFrm._Index = 2
            ObjFrm._LeaveBalanceId = CInt(lvAccrue.SelectedItems(0).Tag)
            If ObjFrm.displayDialog(CInt(cboEmployee.SelectedValue), enAction.ADD_ONE) Then
                FillAccrueList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDeleteByLeave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnDeleteByDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf lvAccrue.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no Accrued leave to delete for this employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim ObjFrm As New frmDeleteAccruedLeave
            ObjFrm.Text = "Delete Accrued Leave By Date"
            ObjFrm._Index = 1
            If ObjFrm.displayDialog(CInt(cboEmployee.SelectedValue), enAction.ADD_ONE) Then
                FillAccrueList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDeleteByDate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnImport.Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnExport.Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAddLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLeaveType.Click
        Dim frm As New frmLeaveType_AddEdit
        Dim intRefId As Integer = -1
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objLeaveType As New clsleavetype_master
                dsList = objLeaveType.GetList("LVTYP", True)
                With cboLeaveCode
                    .ValueMember = "leavetypeunkid"
                    .DisplayMember = "leavename"

                    Dim drRow As DataRow = dsList.Tables("LVTYP").NewRow
                    drRow("leavetypeunkid") = 0
                    drRow("leavename") = Language.getMessage(mstrModuleName, 11, "Select")
                    dsList.Tables("LVTYP").Rows.InsertAt(drRow, 0)

                    .DataSource = dsList.Tables("LVTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objLeaveType = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnDeleteByLeave_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnDeleteByLeave.MouseHover, objbtnDeleteByBatch.MouseHover

        Try
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "OBJBTNDELETEBYLEAVE"
                    objTpToolTip.Show(Language.getMessage(mstrModuleName, 12, "Delete Leave Type wise"), Me.objbtnDeleteByLeave, 1000)
                Case "OBJBTNDELETEBYBATCH"
                    objTpToolTip.Show(Language.getMessage(mstrModuleName, 13, "Delete Batch wise"), Me.objbtnDeleteByBatch, 1000)
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDeleteByLeave_MouseHover", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton's Event"

    Private Sub lnkIssueLeave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkIssueLeave.LinkClicked
        Try
            If CInt(cboLeaveYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year."), enMsgBoxStyle.Information)
                cboLeaveYear.Select()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf CInt(cboIssueLeaveCode.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Issue Leave Code is compulsory information.Please Select Issue Leave Code."), enMsgBoxStyle.Information)
                cboIssueLeaveCode.Select()
                Exit Sub
            ElseIf CInt(cboForm.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Form No is compulsory information.Please Select Form No."), enMsgBoxStyle.Information)
                cboForm.Select()
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkIssueLeave_LinkClicked", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub chkNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoAction.CheckedChanged
        Try
            If chkNoAction.Checked Then
                txtCfAmount.Enabled = False
                txtCfAmount.Decimal = 0
            Else
                txtCfAmount.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoAction_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 26 SEPT 2013 ] -- END

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboLeaveYear.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboLeaveCode.BackColor = GUI.ColorComp
            txtAccrualAmount.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If mintLeavebalalnceUnkid > 0 Then
                cboLeaveYear.SelectedValue = objLeaveAccrue._Yearunkid
                cboEmployee.SelectedValue = objLeaveAccrue._Employeeunkid
                cboLeaveCode_SelectedIndexChanged(Nothing, Nothing)
                txtAccrualAmount.Text = ""
                If enAccrueSetting.Issue_Balance = objLeaveAccrue._AccrueSetting Then
                    radIssuebal.Checked = True
                ElseIf enAccrueSetting.Exceeding_balance_Unpaid = objLeaveAccrue._AccrueSetting Then
                    radExceedBal.Checked = True
                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                ElseIf enAccrueSetting.Donot_Issue_On_Exceeding_Balance = objLeaveAccrue._AccrueSetting Then
                    radDonotIssue.Checked = True
                ElseIf enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date = objLeaveAccrue._AccrueSetting Then
                    radDonotIssueAsonDate.Checked = True
                    'Pinkal (18-Nov-2016) -- End
                End If
            Else
                If mintEmployeeunkid > 0 Then cboEmployee.SelectedValue = mintEmployeeunkid
                If mintFormunkid > 0 Then cboForm.SelectedValue = mintFormunkid
                If mintLeaveTypeunkid > 0 Then cboIssueLeaveCode.SelectedValue = mintLeaveTypeunkid
                txtAccrualAmount.Text = ""
                txtCfAmount.Text = ""
                txtEligibilityAfter.Text = ""
            End If
            FillAccrueList()
            FillIssueList()


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            nudMaxNegativeDays.Value = objLeaveAccrue._MaxNegativeDaysLimit
            'Pinkal (08-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Dim mdtStartdate As DateTime
        Dim mdtStopdate As DateTime
        Try
            objLeaveAccrue._LeaveBalanceunkid = mintLeavebalalnceUnkid
            objLeaveAccrue._Yearunkid = CInt(cboLeaveYear.SelectedValue)
            objLeaveAccrue._Employeeunkid = CInt(cboEmployee.SelectedValue)

            If radIssuebal.Checked = True Then
                objLeaveAccrue._AccrueSetting = enAccrueSetting.Issue_Balance
            ElseIf radExceedBal.Checked = True Then
                objLeaveAccrue._AccrueSetting = enAccrueSetting.Exceeding_balance_Unpaid
            ElseIf radDonotIssue.Checked = True Then
                objLeaveAccrue._AccrueSetting = enAccrueSetting.Donot_Issue_On_Exceeding_Balance

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            ElseIf radDonotIssueAsonDate.Checked = True Then
                objLeaveAccrue._AccrueSetting = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date
                'Pinkal (18-Nov-2016) -- End
            Else
                objLeaveAccrue._AccrueSetting = enAccrueSetting.No_Balance
            End If

            ''START FOR LEAVE ACCRUE 

            mdtStartdate = dtpStartDate.Value

            objLeaveAccrue._Startdate = dtpStartDate.Value
            objLeaveAccrue._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)
            objLeaveAccrue._Ispaid = CBool(cboLeaveCode.Tag)


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                objLeaveAccrue._ActualAmount = CDbl(txtAccrualAmount.Decimal)
                objLeaveAccrue._Monthly_Accrue = 0
            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                objLeaveAccrue._Monthly_Accrue = CDbl(txtAccrualAmount.Decimal)
            End If
            'Pinkal (18-Nov-2016) -- End


            objLeaveAccrue._CFlvAmount = CDec(IIf(txtCfAmount.Text.Trim.Length > 0, txtCfAmount.Text, 0))
            objLeaveAccrue._EligibilityAfter = CInt(IIf(txtEligibilityAfter.Text.Trim.Length > 0, txtEligibilityAfter.Text, 0))
            objLeaveAccrue._IsNoAction = chkNoAction.Checked

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(dtpAsOnDate.Value.Date) = CInt(cboEmployee.SelectedValue)

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                If dtpStartDate.Value.Date > FinancialYear._Object._Database_Start_Date Then
                    mdtStartdate = dtpStartDate.Value.Date
                Else
                    mdtStartdate = FinancialYear._Object._Database_Start_Date
                End If

                If dtpStopDate.Checked Then

                    If dtpStopDate.Value.Date < FinancialYear._Object._Database_End_Date Then
                        mdtStopdate = dtpStopDate.Value
                        objLeaveAccrue._Enddate = dtpStopDate.Value
                    Else
                        mdtStopdate = FinancialYear._Object._Database_End_Date
                        objLeaveAccrue._Enddate = dtpStopDate.Value
                    End If
                Else
                    mdtStopdate = FinancialYear._Object._Database_End_Date
                    objLeaveAccrue._Enddate = Nothing
                End If


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then

                    If mdblTempAccrueamout <> CDec(txtAccrualAmount.Text) AndAlso txtAccrualAmount.Text.Trim <> "" Then
                        objLeaveAccrue._Daily_Amount = CDec(txtAccrualAmount.Text.Trim) / CInt(DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date.AddDays(1)))
                    Else
                        objLeaveAccrue._Daily_Amount = CDec(txtAccrualAmount.Tag)
                    End If

                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    objLeaveAccrue._Daily_Amount = 0
                End If
                'Pinkal (18-Nov-2016) -- End

                objLeaveAccrue._IsOpenELC = False
                objLeaveAccrue._IsELC = False

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                mdtStartdate = dtpStartDate.Value.Date
                objLeaveAccrue._Enddate = dtpStopDate.Value.Date
                mdtStopdate = objLeaveAccrue._Enddate


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    objLeaveAccrue._Daily_Amount = CDec(txtAccrualAmount.Text.Trim) / CInt(DateDiff(DateInterval.Day, mdtStartdate, mdtStopdate.AddDays(1)))
                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    objLeaveAccrue._Daily_Amount = 0
                End If
                'Pinkal (18-Nov-2016) -- End

                objLeaveAccrue._IsOpenELC = True
                objLeaveAccrue._IsELC = True

            End If


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.


            Dim intDiff As Integer = 0
            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                intDiff = CInt(DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))
                objLeaveAccrue._AccrueAmount = objLeaveAccrue._Daily_Amount * intDiff

            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                intDiff = CInt(DateDiff(DateInterval.Month, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
CheckMonthDiff:

                        'Pinkal (16-Feb-2017) -- Start
                        'Enhancement - Worked on ASP Monthly Leave Accrue Bug.
                        'If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > mdtStartdate.Date.Day Then
                        If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > mdtStopdate.Date.Day Then
                            'Pinkal (16-Feb-2017) -- End
                            intDiff = intDiff - 1
                        End If
                    End If
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    GoTo CheckMonthDiff
                End If

                objLeaveAccrue._AccrueAmount = (txtAccrualAmount.Decimal) * intDiff

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    objLeaveAccrue._ActualAmount = (txtAccrualAmount.Decimal) * CInt(DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date.AddDays(1)))

                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    objLeaveAccrue._ActualAmount = (txtAccrualAmount.Decimal) * CInt(DateDiff(DateInterval.Month, objLeaveAccrue._Startdate.Date, objLeaveAccrue._Startdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1)))
                End If
                'Pinkal (16-Dec-2016) -- End


            End If
            'Pinkal (18-Nov-2016) -- End 

            Dim dsList As DataSet = Nothing
            Dim mstrFilter As String = ""
            mstrFilter = " AND yearunkid <>" & CInt(cboLeaveYear.SelectedValue)

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objLeaveAccrue.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                               , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, False, False, False, "", Nothing)

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objLeaveAccrue.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, -1, True, True, False, "", Nothing)
            End If


            Dim dtList As DataTable = New DataView(dsList.Tables("List"), "employeeunkid = " & CInt(cboEmployee.SelectedValue) & _
                                                   " AND leavetypeunkid = " & CInt(cboLeaveCode.SelectedValue) & mstrFilter, "", DataViewRowState.CurrentRows).ToTable

            If dtList.Rows.Count > 0 Then

                If CDec(dtList.Rows(0)("remaining_bal")) > CDec(dtList.Rows(0)("cfamount")) AndAlso CBool(dtList.Rows(0)("isnoaction")) = False Then
                    dtList.Rows(0)("remaining_bal") = CDec(dtList.Rows(0)("cfamount"))
                End If


                objLeaveAccrue._AccrueAmount += CDec(dtList.Rows(0)("remaining_bal"))
                objLeaveAccrue._UptoLstYr_AccrueAmout = CDec(dtList.Rows(0)("uptolstyr_accrueamt")) + objLeaveAccrue._AccrueAmount
                objLeaveAccrue._UptoLstYr_IssueAmout = CDec(dtList.Rows(0)("uptolstyr_issueamt"))
                objLeaveAccrue._LeaveBF = CDec(dtList.Rows(0)("remaining_bal"))

                'Pinkal (12-Jan-2016) -- Start
                'Enhancement - Changing Leave Adjustment to 0 WHEN User want to add new Leave Accrue.
                objLeaveAccrue._AdjustmentAmt = 0
                'Pinkal (12-Jan-2016) -- End

            Else

                Dim dtLeavebf As DataTable = New DataView(dsList.Tables("List"), "employeeunkid = " & CInt(cboEmployee.SelectedValue) & _
                                                  " AND leavetypeunkid = " & CInt(cboLeaveCode.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

                If dtLeavebf.Rows.Count > 0 Then

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        objLeaveAccrue._AccrueAmount += CDec(dtLeavebf.Rows(0)("leavebf"))
                        objLeaveAccrue._UptoLstYr_AccrueAmout = (CDec(dtLeavebf.Rows(0)("uptolstyr_accrueamt")) - CDec(dtLeavebf.Rows(0)("accrue_amount"))) + objLeaveAccrue._AccrueAmount
                        objLeaveAccrue._LeaveBF = CDec(dtLeavebf.Rows(0)("leavebf"))
                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        objLeaveAccrue._AccrueAmount += CDec(dtLeavebf.Rows(0)("remaining_bal"))
                        objLeaveAccrue._UptoLstYr_AccrueAmout = CDec(dtLeavebf.Rows(0)("uptolstyr_accrueamt")) + objLeaveAccrue._AccrueAmount
                        objLeaveAccrue._LeaveBF = CDec(dtLeavebf.Rows(0)("remaining_bal"))
                    End If
                    objLeaveAccrue._IssueAmount = 0
                    objLeaveAccrue._UptoLstYr_IssueAmout = CDec(dtLeavebf.Rows(0)("uptolstyr_issueamt"))
                    objLeaveAccrue._Remaining_Balance = objLeaveAccrue._AccrueAmount

                Else

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        objLeaveAccrue._UptoLstYr_AccrueAmout = objLeaveAccrue._AccrueAmount
                        objLeaveAccrue._Remaining_Balance = objLeaveAccrue._AccrueAmount

                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        dsList = objLeaveAccrue.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                        , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, True _
                                                                        , False, "", Nothing)


                        Dim dtBalance As DataTable = New DataView(dsList.Tables("List"), "leavetypeunkid = " & CInt(cboLeaveCode.SelectedValue) & " AND startdate = max(startdate)", "", DataViewRowState.CurrentRows).ToTable
                        If dtBalance IsNot Nothing AndAlso dtBalance.Rows.Count > 0 Then
                            objLeaveAccrue._AccrueAmount += CDec(dtBalance.Rows(0)("remaining_bal"))
                            objLeaveAccrue._UptoLstYr_AccrueAmout = CDec(dtBalance.Rows(0)("uptolstyr_accrueamt")) + objLeaveAccrue._AccrueAmount
                            objLeaveAccrue._UptoLstYr_IssueAmout = CDec(dtBalance.Rows(0)("uptolstyr_issueamt"))
                            objLeaveAccrue._LeaveBF = CDec(dtBalance.Rows(0)("remaining_bal"))
                        End If
                        objLeaveAccrue._Remaining_Balance = objLeaveAccrue._AccrueAmount

                    End If


                    'Pinkal (12-Jan-2016) -- Start
                    'Enhancement - Changing Leave Adjustment to 0 WHEN User want to add new Leave Accrue.
                    objLeaveAccrue._AdjustmentAmt = 0
                    'Pinkal (12-Jan-2016) -- End
                End If

            End If

            objLeaveAccrue._IsCloseFy = False
            objLeaveAccrue._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveAccrue._Userunkid = User._Object._Userunkid

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objLeaveAccrue._MaxNegativeDaysLimit = CInt(nudMaxNegativeDays.Value)
            'Pinkal (08-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try
            'FOR YEAR
            Dim objYear As New clsMasterData
            dsFill = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            cboLeaveYear.ValueMember = "Id"
            cboLeaveYear.DisplayMember = "name"
            cboLeaveYear.DataSource = dsFill.Tables("Year")

            'FOR EMPLOYEE
            dsFill = Nothing
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveAccrue_AddEdit))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True, _
                                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'Pinkal (14-Jun-2018) -- End



            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"

            If mintEmployeeunkid > 0 Then
                dtFill = New DataView(dsFill.Tables("Employee"), "employeeunkid=" & mintEmployeeunkid, "", DataViewRowState.CurrentRows).ToTable
                cboEmployee.DataSource = dtFill
            Else
                cboEmployee.DataSource = dsFill.Tables("Employee")
            End If



            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True, False)
            If mintLeaveTypeunkid > 0 Then
                dtFill = New DataView(dsFill.Tables("LeaveType"), "leavetypeunkid=" & mintLeaveTypeunkid & " AND ispaid=1 AND isaccrueamount = 1", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid=1 AND isaccrueamount = 1 ", "", DataViewRowState.CurrentRows).ToTable

                Dim drRow As DataRow = dtFill.NewRow
                drRow("leavetypeunkid") = 0
                drRow("leavename") = "Select"
                dtFill.Rows.InsertAt(drRow, 0)

            End If
            cboLeaveCode.ValueMember = "leavetypeunkid"
            cboLeaveCode.DisplayMember = "leavename"
            cboLeaveCode.DataSource = dtFill
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAccrueList()
        Dim dsAccrueList As DataSet = Nothing
        Dim dtAccrueList As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsAccrueList = objLeaveAccrue.GetList("AccrueList", True)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsAccrueList = objLeaveAccrue.GetList("AccrueList", True, False, -1, True, True)
                'End If

                'strSearch = "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " AND ispaid = 1 AND yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND isshortleave = 0 "

                'strSearch = strSearch.Substring(3)
                'dtAccrueList = New DataView(dsAccrueList.Tables("AccrueList"), strSearch, "", DataViewRowState.CurrentRows).ToTable


                'Pinkal (14-Jun-2018) -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveAccrue_AddEdit))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsAccrueList = objLeaveAccrue.GetList("AccrueList", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False _
                '                                                             , "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND lvleavebalance_tran.isshortleave = 0 ", Nothing)
                'ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsAccrueList = objLeaveAccrue.GetList("AccrueList", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False _
                '                                                             , "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND lvleavebalance_tran.isshortleave = 0 ", Nothing)
                'End If


                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    dsAccrueList = objLeaveAccrue.GetList("AccrueList", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                             , mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), False, False, False _
                                                                             , "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND lvleavebalance_tran.isshortleave = 0 ", Nothing)
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    dsAccrueList = objLeaveAccrue.GetList("AccrueList", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                             , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                             , mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(cboEmployee.SelectedValue), True, True, False _
                                                                             , "lvleavebalance_tran.ispaid = 1 AND lvleavebalance_tran.yearunkid = " & CInt(cboLeaveYear.SelectedValue) & " AND lvleavebalance_tran.isshortleave = 0 ", Nothing)
                End If

                dtAccrueList = New DataView(dsAccrueList.Tables("AccrueList"), "", "", DataViewRowState.CurrentRows).ToTable

                'Pinkal (24-Aug-2015) -- End


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    colhMonthlyAcrrue.Width = 90
                    colhActualAmount.Width = 0
                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    colhMonthlyAcrrue.Width = 0
                    colhActualAmount.Width = 90
                End If
                'Pinkal (18-Nov-2016) -- End


                Dim lvItem As ListViewItem
                lvAccrue.Items.Clear()
                For Each drRow As DataRow In dtAccrueList.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("leavename").ToString
                    lvItem.Tag = drRow("leavebalanceunkid")
                    lvItem.SubItems.Add(CDate(drRow("startdate").ToString).ToShortDateString)

                    If Not IsDBNull(drRow("enddate")) Then
                        lvItem.SubItems.Add(CDate(drRow("enddate")).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    lvItem.SubItems.Add(CDbl(drRow("actualamount")).ToString())
                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    lvItem.SubItems.Add(Math.Round(CDec(drRow("monthly_accrue")), 2).ToString)
                    'Pinkal (18-Nov-2016) -- End
                    lvItem.SubItems.Add(Math.Round(CDec(drRow("accrue_amount")), 2).ToString)
                    lvItem.SubItems.Add(drRow("accruesetting").ToString)
                    lvItem.SubItems.Add(drRow("daily_amount").ToString)
                    lvItem.SubItems.Add(drRow("leavetypeunkid").ToString)
                    lvItem.SubItems.Add(Format(CDec(drRow.Item("cfamount")), "####.#0"))
                    lvItem.SubItems.Add(CInt(drRow.Item("eligibilityafter")).ToString)
                    lvItem.SubItems.Add(CBool(drRow.Item("isnoaction")).ToString)

                    'Pinkal (08-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    lvItem.SubItems.Add(CInt(drRow.Item("maxnegative_limit")).ToString)
                    'Pinkal (08-Oct-2018) -- End

                    lvAccrue.Items.Add(lvItem)
                Next


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                'If lvAccrue.Items.Count > 16 Then
                '    colhTotLeave.Width = 80 - 18
                'Else
                '    colhTotLeave.Width = 80
                'End If

                If lvAccrue.Items.Count > 16 Then
                    colhTotLeave.Width = 80 - 18
                Else
                    colhTotLeave.Width = 80
                End If

                'Pinkal (18-Nov-2016) -- End

            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillAccrueList", mstrModuleName)
        Finally
            If Not dsAccrueList Is Nothing Then dsAccrueList.Dispose()
        End Try
    End Sub

    Private Sub FillIssueList()
        Dim objIssue As New clsleaveissue_Tran
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Dim strSearching As String = String.Empty
        Try


            If CInt(cboIssueLeaveCode.SelectedValue) > 0 Then
                dsFill = objIssue.GetList("IssueLeave", True)
                strSearching = "AND leavetypeunkid =" & CInt(cboIssueLeaveCode.SelectedValue) & " AND formunkid =" & CInt(cboForm.SelectedValue) & " AND ispaid = 1 "


                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtFill = New DataView(dsFill.Tables("IssueLeave"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtFill = dsFill.Tables("IssueLeave")
                End If

                Dim lvItem As ListViewItem

                lstIssueLeave.Items.Clear()
                If Not dtFill Is Nothing Then
                    For Each drRow As DataRow In dtFill.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = eZeeDate.convertDate(drRow("leavedate").ToString).ToShortDateString
                        lvItem.Tag = drRow("leaveissuetranunkid")
                        lvItem.SubItems.Add(drRow("leavename").ToString())
                        lvItem.SubItems.Add(drRow("formno").ToString())
                        lvItem.SubItems.Add(drRow("dayfraction").ToString())
                        lvItem.SubItems.Add(drRow("leaveissueunkid").ToString())
                        lvItem.SubItems.Add(drRow("leaveissuetranunkid").ToString())
                        lstIssueLeave.Items.Add(lvItem)
                    Next


                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    'If lstIssueLeave.Items.Count > 16 Then
                    '    colhIssueTotLeave.Width = 98 - 18
                    'Else
                    '    colhIssueTotLeave.Width = 98
                    'End If

                    If lstIssueLeave.Items.Count > 16 Then
                        colhIssueTotLeave.Width = 80 - 18
                    Else
                        colhIssueTotLeave.Width = 80
                    End If
                    'Pinkal (18-Nov-2016) -- End


                End If
            Else
                lstIssueLeave.Items.Clear()
            End If

            If dtFill IsNot Nothing AndAlso dtFill.Rows.Count > 0 Then
                txtIssuemount.Text = dtFill.Compute("SUM(dayfraction)", "1=1").ToString()
            Else
                txtIssuemount.Text = CStr(lstIssueLeave.Items.Count)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillIssueList", mstrModuleName)
        End Try
    End Sub

    Private Sub GetLeaveIssueUnkId()
        Try
            Dim dsFill As DataSet
            Dim dtFill As DataTable
            Dim objIssueleave As New clsleaveissue_master
            dsFill = objIssueleave.GetList("List", True)
            dtFill = New DataView(dsFill.Tables("List"), "employeeunkid=" & CInt(cboEmployee.SelectedValue) & " AND leavetypeunkid=" & CInt(cboIssueLeaveCode.SelectedValue) _
                     & " AND formunkid=" & CInt(cboForm.SelectedValue) & " AND leaveyearunkid=" & CInt(cboLeaveYear.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

            If Not dtFill Is Nothing Then
                If dtFill.Rows.Count > 0 Then
                    mintIssueunkid = CInt(dtFill.Rows(0)("leaveissueunkid"))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveIssueUnkId", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddLeaveType.Enabled = User._Object.Privilege._AddLeaveType
            btnGlobalAssign.Enabled = User._Object.Privilege._AllowLeaveAccrue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub GetLeaveBalanceInfo()
        Try
            If mstrLeaveTypeID.Trim = "" Or mstrLeaveTypeID = "0" Then
                mstrLeaveTypeID = CInt(cboLeaveCode.SelectedValue).ToString()
            End If
            Dim dsList As DataSet = Nothing

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(dtpAsOnDate.Value.Date, mstrLeaveTypeID, CStr(IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue, 0)) _
                                                                                  , ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)
                'Pinkal (18-Nov-2016) -- End



            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                objLeaveAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLeaveAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'dsList = objLeaveAccrue.GetLeaveBalanceInfo(dtpAsOnDate.Value.Date, mstrLeaveTypeID, CStr(IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue, 0)), Nothing, Nothing, True, True)
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(dtpAsOnDate.Value.Date, mstrLeaveTypeID, CStr(IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue, 0)) _
                                                                                    , ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
                'Pinkal (18-Nov-2016) -- End
            End If


            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objlblLastYearAccruedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                objlblLastYearIssuedValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                objlblAccruedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                objlblBalancevalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")

                'Pinkal (24-Aug-2016) -- Start
                'Enhancement - Removing 2 columns(Leave Accrue Upto Last Year AND Leave Issued Upto Last Year) From Whole Leave Module AS Per Rutta's Request For VFT
                ' objlblTodateIssuedvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                objlblTodateIssuedvalue.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                objlblLeaveBFvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                objlblTotalAdjustment.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                'Pinkal (24-Aug-2016) -- End

            Else
                objlblLastYearAccruedValue.Text = "0.00"
                objlblLastYearIssuedValue.Text = "0.00"
                objlblAccruedToDateValue.Text = "0.00"
                objlblTodateIssuedvalue.Text = "0.00"
                objlblBalancevalue.Text = "0.00"

                'Pinkal (24-Aug-2016) -- Start
                'Enhancement - Removing 2 columns(Leave Accrue Upto Last Year AND Leave Issued Upto Last Year) From Whole Leave Module AS Per Rutta's Request For VFT
                objlblLeaveBFvalue.Text = "0.00"
                objlblTotalAdjustment.Text = "0.00"
                'Pinkal (24-Aug-2016) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveBalanceInfo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview's Event"

    Private Sub lvAccrue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAccrue.SelectedIndexChanged
        Try

            If lvAccrue.SelectedItems.Count > 0 Then

                If CInt(lvAccrue.SelectedItems(0).SubItems(objcolhAccrueSetting.Index).Text) = enAccrueSetting.Issue_Balance Then
                    radIssuebal.Checked = True

                ElseIf CInt(lvAccrue.SelectedItems(0).SubItems(objcolhAccrueSetting.Index).Text) = enAccrueSetting.Exceeding_balance_Unpaid Then
                    radExceedBal.Checked = True

                ElseIf CInt(lvAccrue.SelectedItems(0).SubItems(objcolhAccrueSetting.Index).Text) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then
                    radDonotIssue.Checked = True

                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                ElseIf CInt(lvAccrue.SelectedItems(0).SubItems(objcolhAccrueSetting.Index).Text) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then
                    radDonotIssueAsonDate.Checked = True
                    'Pinkal (18-Nov-2016) -- End

                ElseIf CInt(lvAccrue.SelectedItems(0).SubItems(objcolhAccrueSetting.Index).Text) = enAccrueSetting.No_Balance Then

                End If

                dtpAsOnDate.MinDate = CDate("01-Jan-1900")
                dtpAsOnDate.MaxDate = CDate("01-Jan-9998")
                dtpStartDate.MinDate = CDate("01-Jan-1900")
                dtpStartDate.MaxDate = CDate("01-Jan-9998")
                dtpStopDate.MinDate = CDate("01-Jan-1900")
                dtpStopDate.MaxDate = CDate("01-Jan-9998")

                Dim objLeaveType As New clsleavetype_master
                Dim dFill As DataTable = objLeaveType.GetDeductdToLeaveType("List", CInt(cboEmployee.SelectedValue), True)
                mstrLeaveTypeID = ""
                For i As Integer = 0 To dFill.Rows.Count - 1
                    If CInt(lvAccrue.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text) = CInt(dFill.Rows(i)("deductfromlvtypeunkid")) Then
                        'Pinkal (02-Nov-2021)-- Start
                        'NMB Staff Requisition Approval Enhancements. 
                        'mstrLeaveTypeID &= CInt(lvAccrue.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text) & "," & dFill.Rows(i)("leavetypeunkid").ToString() & ","
                        mstrLeaveTypeID &= dFill.Rows(i)("leavetypeunkid").ToString() & ","
                        'Pinkal (02-Nov-2021) -- End
                    Else
                        mstrLeaveTypeID = lvAccrue.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text
                    End If
                Next

                If mstrLeaveTypeID.Length > 0 AndAlso mstrLeaveTypeID.Trim <> "0" AndAlso mstrLeaveTypeID.Trim.Contains(",") Then
                    'Pinkal (02-Nov-2021)-- Start
                    'NMB Staff Requisition Approval Enhancements. 
                    'mstrLeaveTypeID = mstrLeaveTypeID.Substring(0, mstrLeaveTypeID.Length - 1)
                    mstrLeaveTypeID &= CInt(lvAccrue.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)
                    'Pinkal (02-Nov-2021) -- End
                End If

                cboLeaveCode.SelectedValue = CInt(lvAccrue.SelectedItems(0).SubItems(objcolhLeavetypeunkid.Index).Text)

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(dtpAsOnDate.Value.Date) = CInt(cboEmployee.SelectedValue)

                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    txtAccrualAmount.Text = lvAccrue.SelectedItems(0).SubItems(colhTotLeave.Index).Text
                    mdblTempAccrueamout = CDec(txtAccrualAmount.Text)
                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    txtAccrualAmount.Text = lvAccrue.SelectedItems(0).SubItems(colhMonthlyAcrrue.Index).Text
                    mdblTempAccrueamout = 0
                End If
                'Pinkal (18-Nov-2016) -- End

                txtAccrualAmount.Tag = CDec(lvAccrue.SelectedItems(0).SubItems(objcolhDailyAmount.Index).Text)


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                nudMaxNegativeDays.Value = CInt(lvAccrue.SelectedItems(0).SubItems(objcolhMaxNegativeLimit.Index).Text)
                'Pinkal (08-Oct-2018) -- End


                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    If dtpStartDate.Value.Date > CDate(lvAccrue.SelectedItems(0).SubItems(colhStartdate.Index).Text).Date Then
                        dtpStartDate.MinDate = CDate(lvAccrue.SelectedItems(0).SubItems(colhStartdate.Index).Text).Date
                        dtpStartDate.MaxDate = CDate(lvAccrue.SelectedItems(0).SubItems(colhenddate.Index).Text).Date
                    End If


                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    If objEmployee._Appointeddate.Date < FinancialYear._Object._Database_Start_Date.Date Then
                        dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                        dtpStartDate.Value = FinancialYear._Object._Database_Start_Date.Date

                    ElseIf objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
                        dtpStartDate.MinDate = objEmployee._Appointeddate.Date
                        dtpStopDate.MinDate = objEmployee._Appointeddate.Date
                        dtpStartDate.Value = objEmployee._Appointeddate.Date
                    End If

                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                        dtpStartDate.MinDate = objEmployee._Reinstatementdate.Date
                        dtpStopDate.MinDate = objEmployee._Reinstatementdate.Date
                        dtpStartDate.Value = objEmployee._Reinstatementdate.Date
                    End If


                    If objEmployee._Termination_To_Date <> Nothing Then
                        dtpStopDate.Checked = True
                        dtpStopDate.Value = objEmployee._Termination_To_Date
                    Else
                        dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        dtpStopDate.Checked = False
                    End If

                    'Pinkal (18-Nov-2016) -- End

                End If

                dtpStartDate.Value = CDate(lvAccrue.SelectedItems(0).SubItems(colhStartdate.Index).Text).Date
                If lvAccrue.SelectedItems(0).SubItems(colhenddate.Index).Text <> "" And Not IsDBNull(lvAccrue.SelectedItems(0).SubItems(colhenddate.Index).Text) Then
                    dtpStopDate.Value = CDate(lvAccrue.SelectedItems(0).SubItems(colhenddate.Index).Text)
                    dtpStopDate.Checked = True
                Else
                    dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStopDate.Checked = False
                End If

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    dtpAsOnDate.MinDate = dtpStartDate.Value.Date
                    dtpAsOnDate.MaxDate = dtpStopDate.Value.Date
                    If ConfigParameter._Object._CurrentDateAndTime.Date > dtpStopDate.Value.Date Then
                        dtpAsOnDate.Value = dtpStopDate.Value.Date
                    Else
                        dtpAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    End If
                End If


                If CInt(lvAccrue.SelectedItems(0).Tag) > 0 Then
                    mintLeavebalalnceUnkid = CInt(lvAccrue.SelectedItems(0).Tag)
                Else
                    mintLeavebalalnceUnkid = -1
                End If

                txtCfAmount.Decimal = CDec(lvAccrue.SelectedItems(0).SubItems(objcolhCFAmt.Index).Text)
                txtEligibilityAfter.Decimal = CDec(lvAccrue.SelectedItems(0).SubItems(objcolhEDays.Index).Text)
                chkNoAction.Checked = CBool(lvAccrue.SelectedItems(0).SubItems(objcolhNoAction.Index).Text)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAccrue_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lstIssueLeave_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lstIssueLeave.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lstIssueLeave_ItemSelectionChanged", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtAccrualAmount_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 46 Then e.Handled = True
    End Sub

#End Region

#Region "DatePicker Event"

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            dtpStopDate.MinDate = dtpStartDate.Value.Date

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(dtpAsOnDate.Value.Date) = CInt(cboEmployee.SelectedValue)
                If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < dtpStartDate.Value.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                    dtpStopDate.Value = objEmployee._Termination_To_Date
                Else
                    dtpStopDate.Value = dtpStartDate.Value.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dtpAsOnDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpAsOnDate.ValueChanged, cboLeaveYear.SelectedValueChanged, cboEmployee.SelectedValueChanged, cboLeaveCode.SelectedValueChanged
        Try
            GetLeaveBalanceInfo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

#Region "RadioButton Events"

    Private Sub radIssuebal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radIssuebal.CheckedChanged, radExceedBal.CheckedChanged, radDonotIssue.CheckedChanged, radDonotIssueAsonDate.CheckedChanged
        Try
            If CType(sender, RadioButton).Name = radIssuebal.Name OrElse CType(sender, RadioButton).Name = radExceedBal.Name Then
                lblMaxNegativeLimit.Visible = True
                nudMaxNegativeDays.Visible = True
                nudMaxNegativeDays.Value = 0
            ElseIf CType(sender, RadioButton).Name = radDonotIssue.Name OrElse CType(sender, RadioButton).Name = radDonotIssueAsonDate.Name Then
                lblMaxNegativeLimit.Visible = False
                nudMaxNegativeDays.Visible = False
                nudMaxNegativeDays.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radIssuebal_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (08-Oct-2018) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbLeaveBalance.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveBalance.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbIssueList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbIssueList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAccrueList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAccrueList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnExport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnExport.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnDeleteByBatch.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnDeleteByBatch.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnDeleteByLeave.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnDeleteByLeave.GradientForeColor = GUI._ButttonFontColor

            Me.btnGlobalAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnGlobalAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblLeaveYear.Text = Language._Object.getCaption(Me.lblLeaveYear.Name, Me.lblLeaveYear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblAs.Text = Language._Object.getCaption(Me.lblAs.Name, Me.lblAs.Text)
            Me.lblLastYearAccrued.Text = Language._Object.getCaption(Me.lblLastYearAccrued.Name, Me.lblLastYearAccrued.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.gbLeaveBalance.Text = Language._Object.getCaption(Me.gbLeaveBalance.Name, Me.gbLeaveBalance.Text)
            Me.gbIssueList.Text = Language._Object.getCaption(Me.gbIssueList.Name, Me.gbIssueList.Text)
            Me.gbAccrueList.Text = Language._Object.getCaption(Me.gbAccrueList.Name, Me.gbAccrueList.Text)
            Me.lblAccrualAmount.Text = Language._Object.getCaption(Me.lblAccrualAmount.Name, Me.lblAccrualAmount.Text)
            Me.lblLeaveCode.Text = Language._Object.getCaption(Me.lblLeaveCode.Name, Me.lblLeaveCode.Text)
            Me.colhLeave.Text = Language._Object.getCaption(CStr(Me.colhLeave.Tag), Me.colhLeave.Text)
            Me.colhTotLeave.Text = Language._Object.getCaption(CStr(Me.colhTotLeave.Tag), Me.colhTotLeave.Text)
            Me.lblIssueLeave.Text = Language._Object.getCaption(Me.lblIssueLeave.Name, Me.lblIssueLeave.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblIssueLeaveCode.Text = Language._Object.getCaption(Me.lblIssueLeaveCode.Name, Me.lblIssueLeaveCode.Text)
            Me.colhIssueLeave.Text = Language._Object.getCaption(CStr(Me.colhIssueLeave.Tag), Me.colhIssueLeave.Text)
            Me.colhIssueStartDate.Text = Language._Object.getCaption(CStr(Me.colhIssueStartDate.Tag), Me.colhIssueStartDate.Text)
            Me.colhIssueFormNo.Text = Language._Object.getCaption(CStr(Me.colhIssueFormNo.Tag), Me.colhIssueFormNo.Text)
            Me.colhIssueTotLeave.Text = Language._Object.getCaption(CStr(Me.colhIssueTotLeave.Tag), Me.colhIssueTotLeave.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
            Me.Label11.Text = Language._Object.getCaption(Me.Label11.Name, Me.Label11.Text)
            Me.Label12.Text = Language._Object.getCaption(Me.Label12.Name, Me.Label12.Text)
            Me.lblLastYearIssued.Text = Language._Object.getCaption(Me.lblLastYearIssued.Name, Me.lblLastYearIssued.Text)
            Me.lblTodateIssued.Text = Language._Object.getCaption(Me.lblTodateIssued.Name, Me.lblTodateIssued.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.btnGlobalAssign.Text = Language._Object.getCaption(Me.btnGlobalAssign.Name, Me.btnGlobalAssign.Text)
            Me.lnkIssueLeave.Text = Language._Object.getCaption(Me.lnkIssueLeave.Name, Me.lnkIssueLeave.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.radDonotIssue.Text = Language._Object.getCaption(Me.radDonotIssue.Name, Me.radDonotIssue.Text)
            Me.radExceedBal.Text = Language._Object.getCaption(Me.radExceedBal.Name, Me.radExceedBal.Text)
            Me.radIssuebal.Text = Language._Object.getCaption(Me.radIssuebal.Name, Me.radIssuebal.Text)
            Me.lblToDateAccrued.Text = Language._Object.getCaption(Me.lblToDateAccrued.Name, Me.lblToDateAccrued.Text)
            Me.colhStartdate.Text = Language._Object.getCaption(CStr(Me.colhStartdate.Tag), Me.colhStartdate.Text)
            Me.colhenddate.Text = Language._Object.getCaption(CStr(Me.colhenddate.Tag), Me.colhenddate.Text)
            Me.colhActualAmount.Text = Language._Object.getCaption(CStr(Me.colhActualAmount.Tag), Me.colhActualAmount.Text)
            Me.LblEligibilityAfter.Text = Language._Object.getCaption(Me.LblEligibilityAfter.Name, Me.LblEligibilityAfter.Text)
            Me.LblCFAmount.Text = Language._Object.getCaption(Me.LblCFAmount.Name, Me.LblCFAmount.Text)
            Me.chkNoAction.Text = Language._Object.getCaption(Me.chkNoAction.Name, Me.chkNoAction.Text)
            Me.EZeeLine2.Text = Language._Object.getCaption(Me.EZeeLine2.Name, Me.EZeeLine2.Text)
            Me.LblLeaveBF.Text = Language._Object.getCaption(Me.LblLeaveBF.Name, Me.LblLeaveBF.Text)
            Me.LblTotalAdjustment.Text = Language._Object.getCaption(Me.LblTotalAdjustment.Name, Me.LblTotalAdjustment.Text)
            Me.LblMonthlyAccrue.Text = Language._Object.getCaption(Me.LblMonthlyAccrue.Name, Me.LblMonthlyAccrue.Text)
            Me.colhMonthlyAcrrue.Text = Language._Object.getCaption(CStr(Me.colhMonthlyAcrrue.Tag), Me.colhMonthlyAcrrue.Text)
            Me.radDonotIssueAsonDate.Text = Language._Object.getCaption(Me.radDonotIssueAsonDate.Name, Me.radDonotIssueAsonDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Leave Year is compulsory information.Please Select Leave Year.")
            Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "Leave Code is compulsory information.Please Select Leave Code.")
            Language.setMessage(mstrModuleName, 4, "Days cannot be blank. Days is required information.")
            Language.setMessage(mstrModuleName, 5, "Please select Leave from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Please select Accure Leave from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this Accrued Leave?")
            Language.setMessage(mstrModuleName, 8, "Issue Leave Code is compulsory information.Please Select Issue Leave Code.")
            Language.setMessage(mstrModuleName, 9, "There is no Accrued leave to delete for this employee.")
            Language.setMessage(mstrModuleName, 10, "Please select Leave type to delete for this employee.")
            Language.setMessage(mstrModuleName, 11, "Select")
            Language.setMessage(mstrModuleName, 12, "Delete Leave Type wise")
            Language.setMessage(mstrModuleName, 13, "Delete Batch wise")
            Language.setMessage(mstrModuleName, 14, "Form No is compulsory information.Please Select Form No.")
            Language.setMessage(mstrModuleName, 15, "This leave type is invalid for selected employee.")
            Language.setMessage(mstrModuleName, 16, "You have not set CF amount or leave eligibility days. 0 will be set for this employee.Do you wish to continue?")
            Language.setMessage(mstrModuleName, 17, "Sorry,You cannot create new ELC as current ELC is already active.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class