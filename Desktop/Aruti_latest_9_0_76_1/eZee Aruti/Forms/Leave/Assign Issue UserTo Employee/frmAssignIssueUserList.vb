﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAssignIssueUserList

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssignIssueUserList"
    Private objIssueUser As clsissueUser_Tran
    Private mstrAdvanceFilter As String = ""
#End Region

#Region "Form's Event"

    Private Sub frmAssignIssueUserList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objIssueUser = New clsissueUser_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Call OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            FillCombo()
            btnMigration.Visible = User._Object.Privilege._AllowToMigrateIssueUser
            cboIssueUser.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignIssueUserList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignIssueUserList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAssignIssueUser.Focused Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignIssueUserList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignIssueUserList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objIssueUser = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsissueUser_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsissueUser_Tran"
            objfrm.displayDialog(Me)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            SetLanguage()
            'Pinkal (10-Jul-2014) -- End

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmAssignIssueUser_Employee
            ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMigration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMigration.Click
        Try
            If lvAssignIssueUser.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvAssignIssueUser.Select()
                Exit Sub
            End If

            Dim objFrm As New frmAssignIssueUser_Employee
            If objFrm.displayDialog(CInt(lvAssignIssueUser.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                fillList()
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnMigration_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssignIssueUser.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Check Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssignIssueUser.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssignIssueUser.CheckedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objIssueUser._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objIssueUser._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objIssueUser._Voiduserunkid = User._Object._Userunkid
                For i As Integer = 0 To lvAssignIssueUser.CheckedItems.Count - 1
                    objIssueUser.Delete(CInt(lvAssignIssueUser.CheckedItems(0).Tag))
                    lvAssignIssueUser.CheckedItems(0).Remove()
                Next
                fillList()

                If lvAssignIssueUser.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAssignIssueUser.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAssignIssueUser.Items.Count - 1
                    lvAssignIssueUser.Items(intSelectedIndex).Selected = True
                    lvAssignIssueUser.EnsureVisible(intSelectedIndex)
                ElseIf lvAssignIssueUser.Items.Count <> 0 Then
                    lvAssignIssueUser.Items(intSelectedIndex).Selected = True
                    lvAssignIssueUser.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAssignIssueUser.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchIssueUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchIssueUser.Click
        Try
            Dim objfrm As New frmCommonSearch
            objfrm.DataSource = CType(cboIssueUser.DataSource, DataTable)
            objfrm.ValueMember = cboIssueUser.ValueMember
            objfrm.DisplayMember = cboIssueUser.DisplayMember
            If objfrm.DisplayDialog() Then
                cboIssueUser.SelectedValue = objfrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchIssueUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboIssueUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Issue User Or Employee to perform further operation."))
                Exit Sub
            End If
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboIssueUser.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            mstrAdvanceFilter = ""
            lvAssignIssueUser.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()

        Dim dsFill As DataSet = Nothing
        Dim mblnAdUser As Boolean = False
        Try

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables(0)

            'dsFill = objIssueUser.GetUserWithIssueLeavePrivilage("IssueUser", True)
            'cboIssueUser.ValueMember = "userunkid"
            'cboIssueUser.DisplayMember = "username"
            'cboIssueUser.DataSource = dsFill.Tables("IssueUser")

            Dim objUser As New clsUserAddEdit

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Dim objOption As New clsPassowdOptions
            'Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            'If drOption.Length > 0 Then
            '    If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            'End If

            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, Company._Object._Companyunkid, 264)

            'Nilay (01-Mar-2016) -- Start
            'dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, 264)
            dsFill = objUser.getNewComboList("User", , True, Company._Object._Companyunkid, CStr(264), FinancialYear._Object._YearUnkid, True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            cboIssueUser.DisplayMember = "name"
            cboIssueUser.ValueMember = "userunkid"
            cboIssueUser.DataSource = dsFill.Tables("User")

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'objOption = Nothing
            'S.SANDEEP [10 AUG 2015] -- END
            objUser = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsList As DataSet = Nothing
        Dim dtList As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try
            dsList = objIssueUser.GetList("IssueUser")

            If CInt(cboIssueUser.SelectedValue) > 0 Then
                strSearch &= "AND issueuserunkid =" & CInt(cboIssueUser.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtList = New DataView(dsList.Tables("IssueUser"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtList = dsList.Tables("IssueUser")
            End If

            Dim lvItem As ListViewItem
            lvAssignIssueUser.Items.Clear()
            For Each drRow As DataRow In dtList.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = drRow("issueusertranunkid")
                lvItem.SubItems.Add(drRow("issueuser").ToString)
                lvItem.SubItems.Add(drRow("employee").ToString)
                lvItem.SubItems.Add(drRow("department").ToString)
                lvItem.SubItems.Add(drRow("section").ToString)
                lvItem.SubItems.Add(drRow("job").ToString)
                lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                lvAssignIssueUser.Items.Add(lvItem)
            Next
            lvAssignIssueUser.GridLines = False
            lvAssignIssueUser.GroupingColumn = colhIssueUser
            lvAssignIssueUser.DisplayGroups(True)

            If lvAssignIssueUser.Items.Count > 14 Then
                colhJob.Width = 160 - 18
            Else
                colhJob.Width = 160
            End If

            If lvAssignIssueUser.Items.Count > 0 Then
                objCheckAll.Enabled = True
            Else
                objCheckAll.Checked = False
                objCheckAll.Enabled = False
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'btnNew.Enabled = User._Object.Privilege._AddEmployeeHolidays
            'btnMigration.Enabled = User._Object.Privilege._EditEmployeeHolidays
            'btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeHolidays
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetOperation(ByVal blnOperation As Boolean)
        Try
            For Each Item As ListViewItem In lvAssignIssueUser.Items
                RemoveHandler lvAssignIssueUser.ItemChecked, AddressOf lvAssignIssueUser_ItemChecked
                Item.Checked = blnOperation
                AddHandler lvAssignIssueUser.ItemChecked, AddressOf lvAssignIssueUser_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            If lvAssignIssueUser.Items.Count = 0 Then Exit Sub
            SetOperation(objCheckAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvAssignIssueUser_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssignIssueUser.ItemChecked
        Try
            RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
            If lvAssignIssueUser.CheckedItems.Count <= 0 Then
                objCheckAll.CheckState = CheckState.Unchecked

            ElseIf lvAssignIssueUser.CheckedItems.Count < lvAssignIssueUser.Items.Count Then
                objCheckAll.CheckState = CheckState.Indeterminate

            ElseIf lvAssignIssueUser.CheckedItems.Count = lvAssignIssueUser.Items.Count Then
                objCheckAll.CheckState = CheckState.Checked

            End If
            AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssignIssueUser_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnMigration.GradientBackColor = GUI._ButttonBackColor
            Me.btnMigration.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhIssueUser.Text = Language._Object.getCaption(CStr(Me.colhIssueUser.Tag), Me.colhIssueUser.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.colhcheck.Text = Language._Object.getCaption(CStr(Me.colhcheck.Tag), Me.colhcheck.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.LblIssueUser.Text = Language._Object.getCaption(Me.LblIssueUser.Name, Me.LblIssueUser.Text)
            Me.btnMigration.Text = Language._Object.getCaption(Me.btnMigration.Name, Me.btnMigration.Text)
            Me.colhSection.Text = Language._Object.getCaption(CStr(Me.colhSection.Tag), Me.colhSection.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Please Check Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee?")
            Language.setMessage(mstrModuleName, 4, "Please Select Issue User Or Employee to perform further operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class