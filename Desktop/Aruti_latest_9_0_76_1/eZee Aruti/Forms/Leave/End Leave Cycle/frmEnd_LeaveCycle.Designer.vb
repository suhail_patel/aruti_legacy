﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEnd_LeaveCycle
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEnd_LeaveCycle))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEdDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.dtpDateAsOn = New System.Windows.Forms.DateTimePicker
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.radRetirementDate = New System.Windows.Forms.RadioButton
        Me.radTerminationDate = New System.Windows.Forms.RadioButton
        Me.radEOCDate = New System.Windows.Forms.RadioButton
        Me.radProbationDate = New System.Windows.Forms.RadioButton
        Me.radSuspensionDate = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnEnd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilter.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.txtSearch)
        Me.pnlMain.Controls.Add(Me.objChkAll)
        Me.pnlMain.Controls.Add(Me.dgData)
        Me.pnlMain.Controls.Add(Me.gbFilter)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(715, 463)
        Me.pnlMain.TabIndex = 0
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(12, 83)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(691, 21)
        Me.txtSearch.TabIndex = 107
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(19, 115)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 14
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgData.ColumnHeadersHeight = 22
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhEcode, Me.dgcolhEmployee, Me.dgcolhJob, Me.dgcolhStDate, Me.dgcolhEdDate})
        Me.dgData.Location = New System.Drawing.Point(12, 110)
        Me.dgData.Name = "dgData"
        Me.dgData.RowHeadersVisible = False
        Me.dgData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgData.Size = New System.Drawing.Size(691, 292)
        Me.dgData.TabIndex = 13
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Employee Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhJob.Width = 190
        '
        'dgcolhStDate
        '
        Me.dgcolhStDate.HeaderText = "Start Date"
        Me.dgcolhStDate.Name = "dgcolhStDate"
        Me.dgcolhStDate.ReadOnly = True
        Me.dgcolhStDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhStDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEdDate
        '
        Me.dgcolhEdDate.HeaderText = "End Date"
        Me.dgcolhEdDate.Name = "dgcolhEdDate"
        Me.dgcolhEdDate.ReadOnly = True
        Me.dgcolhEdDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEdDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.btnReset)
        Me.gbFilter.Controls.Add(Me.dtpDateAsOn)
        Me.gbFilter.Controls.Add(Me.btnSearch)
        Me.gbFilter.Controls.Add(Me.radRetirementDate)
        Me.gbFilter.Controls.Add(Me.radTerminationDate)
        Me.gbFilter.Controls.Add(Me.radEOCDate)
        Me.gbFilter.Controls.Add(Me.radProbationDate)
        Me.gbFilter.Controls.Add(Me.radSuspensionDate)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(12, 12)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(691, 65)
        Me.gbFilter.TabIndex = 12
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Search Employee"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(666, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 241
        '
        'dtpDateAsOn
        '
        Me.dtpDateAsOn.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateAsOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateAsOn.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateAsOn.Location = New System.Drawing.Point(584, 34)
        Me.dtpDateAsOn.Name = "dtpDateAsOn"
        Me.dtpDateAsOn.Size = New System.Drawing.Size(94, 21)
        Me.dtpDateAsOn.TabIndex = 71
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(642, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 240
        '
        'radRetirementDate
        '
        Me.radRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radRetirementDate.Location = New System.Drawing.Point(469, 36)
        Me.radRetirementDate.Name = "radRetirementDate"
        Me.radRetirementDate.Size = New System.Drawing.Size(108, 17)
        Me.radRetirementDate.TabIndex = 70
        Me.radRetirementDate.Text = "Retirement Date"
        Me.radRetirementDate.UseVisualStyleBackColor = True
        '
        'radTerminationDate
        '
        Me.radTerminationDate.Checked = True
        Me.radTerminationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radTerminationDate.Location = New System.Drawing.Point(355, 36)
        Me.radTerminationDate.Name = "radTerminationDate"
        Me.radTerminationDate.Size = New System.Drawing.Size(108, 17)
        Me.radTerminationDate.TabIndex = 70
        Me.radTerminationDate.TabStop = True
        Me.radTerminationDate.Text = "Termination Date"
        Me.radTerminationDate.UseVisualStyleBackColor = True
        '
        'radEOCDate
        '
        Me.radEOCDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEOCDate.Location = New System.Drawing.Point(241, 36)
        Me.radEOCDate.Name = "radEOCDate"
        Me.radEOCDate.Size = New System.Drawing.Size(108, 17)
        Me.radEOCDate.TabIndex = 70
        Me.radEOCDate.Text = "End Of Contract"
        Me.radEOCDate.UseVisualStyleBackColor = True
        '
        'radProbationDate
        '
        Me.radProbationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radProbationDate.Location = New System.Drawing.Point(127, 36)
        Me.radProbationDate.Name = "radProbationDate"
        Me.radProbationDate.Size = New System.Drawing.Size(108, 17)
        Me.radProbationDate.TabIndex = 70
        Me.radProbationDate.Text = "Probation Date"
        Me.radProbationDate.UseVisualStyleBackColor = True
        '
        'radSuspensionDate
        '
        Me.radSuspensionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSuspensionDate.Location = New System.Drawing.Point(13, 36)
        Me.radSuspensionDate.Name = "radSuspensionDate"
        Me.radSuspensionDate.Size = New System.Drawing.Size(108, 17)
        Me.radSuspensionDate.TabIndex = 70
        Me.radSuspensionDate.Text = "Suspension Date"
        Me.radSuspensionDate.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnEnd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 408)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(715, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnEnd
        '
        Me.btnEnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEnd.BackColor = System.Drawing.Color.White
        Me.btnEnd.BackgroundImage = CType(resources.GetObject("btnEnd.BackgroundImage"), System.Drawing.Image)
        Me.btnEnd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEnd.BorderColor = System.Drawing.Color.Empty
        Me.btnEnd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEnd.FlatAppearance.BorderSize = 0
        Me.btnEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnd.ForeColor = System.Drawing.Color.Black
        Me.btnEnd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEnd.GradientForeColor = System.Drawing.Color.Black
        Me.btnEnd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEnd.Location = New System.Drawing.Point(503, 13)
        Me.btnEnd.Name = "btnEnd"
        Me.btnEnd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEnd.Size = New System.Drawing.Size(97, 30)
        Me.btnEnd.TabIndex = 6
        Me.btnEnd.Text = "&End"
        Me.btnEnd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(606, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEnd_LeaveCycle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 463)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEnd_LeaveCycle"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "End Leave Cycle"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilter.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEnd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radRetirementDate As System.Windows.Forms.RadioButton
    Friend WithEvents radTerminationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radEOCDate As System.Windows.Forms.RadioButton
    Friend WithEvents radProbationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radSuspensionDate As System.Windows.Forms.RadioButton
    Friend WithEvents dtpDateAsOn As System.Windows.Forms.DateTimePicker
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEdDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
End Class
