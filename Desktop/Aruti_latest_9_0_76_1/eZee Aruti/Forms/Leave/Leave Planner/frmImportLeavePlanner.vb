﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

Public Class frmImportLeavePlanner

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportLeavePlanner"
    Private objLeavePlanner As clsleaveplanner
    Private mstrAdvanceFilter As String = ""
    Private dsList As DataSet = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Form's Events "

    Private Sub frmImportLeavePlanner_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeavePlanner = New clsleaveplanner
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            btnOpenFile.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportLeavePlanner_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportLeavePlanner_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeavePlanner = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillGirdView()
        Try

            If dsList IsNot Nothing Then
                dsList.Tables(0).Columns.Add("ischecked", Type.GetType("System.Boolean"))
                dsList.Tables(0).Columns.Add("error", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("leavetypeunkid", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("image", Type.GetType("System.Object"))


                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objLeave As New clsleavetype_master

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        dsList.Tables(0).Rows(i)("ischecked") = True
                        dsList.Tables(0).Rows(i)("error") = ""
                        dsList.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1).Clone
                        dsList.Tables(0).Rows(i)("employeeunkid") = objEmp.GetEmployeeUnkid("", dsList.Tables(0).Rows(i)("Employeecode").ToString.Trim)
                        dsList.Tables(0).Rows(i)("leavetypeunkid") = objLeave.GetLeaveTypeUnkId(dsList.Tables(0).Rows(i)("LeaveType").ToString.Trim)
                    Next
                    dsList.AcceptChanges()
                End If

            End If

            dgLeavePlanner.AutoGenerateColumns = False

            dgLeavePlanner.DataSource = dsList.Tables(0)

            objdgcolhImage.DataPropertyName = "image"
            dgcolhEmployeecode.DataPropertyName = "Employeecode"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhLeaveType.DataPropertyName = "LeaveType"
            dgcolhStartdate.DataPropertyName = "Startdate"
            dgcolhEnddate.DataPropertyName = "Enddate"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            objdgcolhLeavetypeId.DataPropertyName = "leavetypeunkid"
            objdgcolhMessage.DataPropertyName = "error"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim objExcel As ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            ofdlgOpen.Filter = "Excel files(*.xlsx)|*.xlsx"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then

                Cursor = Cursors.WaitCursor
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'objExcel = New ExcelData
                'dsList = objExcel.Import(txtFilePath.Text)

                dsList = OpenXML_Import(txtFilePath.Text)
                'S.SANDEEP [12-Jan-2018] -- END

                Dim strQuery As String = ""
                For i As Integer = 0 To dsList.Tables(0).Columns.Count - 1
                    dsList.Tables(0).Columns(i).ColumnName = dsList.Tables(0).Columns(i).ColumnName.Replace(" ", "_")
                    strQuery &= " AND " & dsList.Tables(0).Columns(i).ColumnName & " IS NULL "
                Next

                If strQuery.Trim.Length > 0 Then
                    strQuery = strQuery.Trim.Substring(4, strQuery.Trim.Length - 4)
                End If

                Dim drRow As DataRow() = dsList.Tables(0).Select(strQuery)
                If drRow.Length > 0 Then
                    For j As Integer = 0 To drRow.Length - 1
                        dsList.Tables(0).Rows.Remove(drRow(j))
                    Next
                    dsList.Tables(0).AcceptChanges()
                End If

                Cursor = Cursors.Default
                FillGirdView()
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try
            Dim blnError As Boolean = False

            If dgLeavePlanner.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to import leave planner."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim intIndex As Integer = 0
                Dim objPlannerFraction As New clsplannerday_fraction

                For Each dr As DataRow In dsList.Tables(0).Rows
                    objLeavePlanner = New clsleaveplanner

                    If IsDBNull(dr("startdate")) Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhStartdate.Index).ErrorText = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhStartdate.Index).ToolTipText = dgLeavePlanner.Rows(intIndex).Cells(dgcolhStartdate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 2, "Start date is compulsory information.")
                        dr("Image") = imgError
                        blnError = True
                        intIndex += 1
                        Continue For

                    ElseIf IsDBNull(dr("enddate")) Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhEnddate.Index).ToolTipText = dgLeavePlanner.Rows(intIndex).Cells(dgcolhEnddate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 3, "End date is compulsory information.")
                        dr("Image") = imgError
                        intIndex += 1
                        blnError = True
                        Continue For

                    ElseIf IsDBNull(dr("enddate")) = False AndAlso (CDate(dr("enddate")) < CDate(dr("startdate"))) Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhStartdate.Index).ToolTipText = dgLeavePlanner.Rows(intIndex).Cells(dgcolhStartdate.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 4, "Invalid Start Date.")
                        dr("Image") = imgError
                        intIndex += 1
                        blnError = True
                        Continue For

                    ElseIf CInt(dr("employeeunkid")) <= 0 Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhEmployeecode.Index).ToolTipText = dgLeavePlanner.Rows(intIndex).Cells(dgcolhEmployeecode.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 5, "Invalid Employee Code.")
                        dr("Image") = imgError
                        intIndex += 1
                        blnError = True
                        Continue For

                    ElseIf CInt(dr("leavetypeunkid")) <= 0 Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhLeaveType.Index).ToolTipText = dgLeavePlanner.Rows(intIndex).Cells(dgcolhLeaveType.Index).ErrorText
                        dr("error") = Language.getMessage(mstrModuleName, 6, "Invalid Leave Type Name.")
                        dr("Image") = imgError
                        intIndex += 1
                        blnError = True
                        Continue For
                    End If

                    Dim dsPlanner As DataSet = dsList.Clone
                    dsPlanner.Tables(0).TableName = "Employee"

                    If dsPlanner IsNot Nothing AndAlso dsPlanner.Tables(0).Columns.Contains("image") Then
                        dsPlanner.Tables(0).Columns.Remove("image")
                    End If

                    dsPlanner.Tables(0).ImportRow(dr)

                    Dim xmlStr As String = GetXML(dsPlanner)
                    Dim dtEmployeeList As DataTable = objPlannerFraction.GetPlannerDayFraction(xmlStr, CInt(dr("leavetypeunkid")), CDate(dr("startdate")).Date, CDate(dr("enddate")).Date)


                    objLeavePlanner._Leavetypeunkid = CInt(dr("leavetypeunkid"))
                    objLeavePlanner._Startdate = CDate(dr("startdate")).Date
                    objLeavePlanner._Stopdate = CDate(dr("enddate")).Date
                    objLeavePlanner._Remarks = ""
                    objLeavePlanner._Userunkid = User._Object._Userunkid
                    objLeavePlanner._PlannerFraction = dtEmployeeList


                    Dim blnFlag As Boolean = objLeavePlanner.Insert(dsPlanner, CDate(dr("enddate")).Date)

                    If blnFlag = False And objLeavePlanner._Message <> "" Then
                        dgLeavePlanner.Rows(intIndex).Cells(dgcolhEmployeecode.Index).ToolTipText = objLeavePlanner._Message
                        dr("error") = objLeavePlanner._Message
                        dr("Image") = imgError
                        intIndex += 1
                        blnError = True
                        Continue For
                    End If

                    dr("Image") = imgAccept
                    intIndex += 1
                Next


                If blnError = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data successfully import."), enMsgBoxStyle.Information)
                    dsList.Tables(0).Rows.Clear()
                    dgLeavePlanner.DataSource = dsList.Tables(0)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Some Data did not import due to error."), enMsgBoxStyle.Information)
                    Dim dtTable As DataTable = New DataView(dsList.Tables(0), "error <> ''", "", DataViewRowState.CurrentRows).ToTable()
                    dgLeavePlanner.DataSource = dtTable
                    Exit Sub
                End If
            End If

            dgLeavePlanner.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImportData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try

            If dgLeavePlanner.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no data to show error(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dsList IsNot Nothing Then
                Dim dvGriddata As DataView = dsList.Tables(0).DefaultView
                dvGriddata.RowFilter = "error <>''"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("Ischecked")
                        dtTable.Columns.Remove("employeeunkid")
                        dtTable.Columns.Remove("leavetypeunkid")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Leave Planner") = True Then
                            Process.Start(savDialog.FileName)
                            dtTable.Rows.Clear()
                            dgLeavePlanner.DataSource = dtTable
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFormat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            Dim objSave As New SaveFileDialog
            objSave.Filter = "Excel files(*.xlsx)|*.xlsx"
            If objSave.ShowDialog = Windows.Forms.DialogResult.OK Then
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim objExcel As New ExcelData
                'Dim dsList As DataSet = objLeavePlanner.GetImportFileStructure()
                'objExcel.Export(objSave.FileName, dsList)
                'objExcel = Nothing
                Dim dsList As DataSet = objLeavePlanner.GetImportFileStructure()
                OpenXML_Export(objSave.FileName, dsList)
                'S.SANDEEP [12-Jan-2018] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetFileFormat_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.Name, Me.btnImportData.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.Name, Me.btnGetFileFormat.Text)
            Me.dgcolhEmployeecode.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeecode.Name, Me.dgcolhEmployeecode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhLeaveType.HeaderText = Language._Object.getCaption(Me.dgcolhLeaveType.Name, Me.dgcolhLeaveType.HeaderText)
            Me.dgcolhStartdate.HeaderText = Language._Object.getCaption(Me.dgcolhStartdate.Name, Me.dgcolhStartdate.HeaderText)
            Me.dgcolhEnddate.HeaderText = Language._Object.getCaption(Me.dgcolhEnddate.Name, Me.dgcolhEnddate.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to import leave planner.")
            Language.setMessage(mstrModuleName, 2, "Start date is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "End date is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Invalid Start Date.")
            Language.setMessage(mstrModuleName, 5, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 6, "Invalid Leave Type Name.")
            Language.setMessage(mstrModuleName, 7, "There is no data to show error(s).")
            Language.setMessage(mstrModuleName, 8, "Data successfully import.")
            Language.setMessage(mstrModuleName, 9, "Some Data did not import due to error.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class