﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

'Last Message Index = 2

Public Class frmLeaveSchedular

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLeaveSchedular"
    Dim dtInfo As New DataTable("LeaveInfo")
    Dim mdtCurrent As DateTime = Nothing
    Dim mintNoofDays As Integer = 0
    Dim mintTodayIndex As Integer = -1
    Dim DaysofWeek As DateTime()
    Dim strName As String()
    Dim dtFill As DataTable = Nothing

    Dim oldX As Integer
    Dim mintScrollPosition As Integer = 0
    Dim oldY As Integer
    Dim mintOldRowIndex As Integer = 0
    Dim mintOldStartIndex As Integer = 0
    Dim mintOldEndIndex As Integer = 0
    Dim mintNewRowINdex As Integer = 0
    Dim mintNewStartIndex As Integer = 0
    Dim mintNewEndIndex As Integer = 0
    Dim mintOldWidth As Integer = 0
    Dim mblnSelected As Boolean = False
    Dim mintLeavePlannerunkid As Integer = -1
    Dim mdtOldStartdate As DateTime = Nothing
    Dim mdtOldStopdate As DateTime = Nothing
    Dim mdtNewStartdate As DateTime = Nothing
    Dim mdtNewStopdate As DateTime = Nothing
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

#Region "Form's Method"

    Private Sub frmEmployeeDairy_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End

            'Pinkal (03-Jan-2011) -- Start

            'S.SANDEEP [ 09 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES (REMOVING FINANCIAL YEAR CONDITION)
            'dtpCalender.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpCalender.MaxDate = FinancialYear._Object._Database_End_Date
            'S.SANDEEP [ 09 APRIL 2012 ] -- END

            'Pinkal (03-Jan-2011) -- End

            FillCombo()
            FillLeaveType()
            objSearch_Click(sender, e)
            btnWeek_Click(sender, e)
            btnWeek.Selected = True
            cboDepartment.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDairy_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveSchedular_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveSchedular_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

           
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnWeek_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWeek.Click
        Try
            ' pnlViewCalender.Controls.Clear()

            'Pinkal (03-Jan-2011) -- Start

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                mdtCurrent = FinancialYear._Object._Database_End_Date.Date
                mintNoofDays = 1
            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                mdtCurrent = ConfigParameter._Object._CurrentDateAndTime.Date
            mintNoofDays = 7
            End If

            dtpCalender.SelectionStart = mdtCurrent

            'mdtCurrent = dtpCalender.SelectionStart.Date

            'Pinkal (03-Jan-2011) -- End

            btnMonthly.Selected = False
            dtpCalender_DateChanged(sender, New DateRangeEventArgs(dtpCalender.SelectionStart.Date, dtpCalender.SelectionStart.Date))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnWeek_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMonthly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMonthly.Click
        Try
            '  pnlViewCalender.Controls.Clear()
            mintNoofDays = Date.DaysInMonth(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month)
            Dim firstDayOfCurrentMonth As New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month, 1)
            mdtCurrent = firstDayOfCurrentMonth
            btnWeek.Selected = False
            Generate_Calender()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMonthly_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearch.Click
        Dim dsFill As DataSet = Nothing
        Dim strSearching As String = ""
        Try
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (06-Feb-2013) -- Start
            ''Enhancement : TRA Changes
            ''dsFill = objEmployee.GetList("Employee")
            'dsFill = objEmployee.GetList("Employee", False, True, , , , , , "isapproved = 1 ", False)
            ''Pinkal (06-Feb-2013) -- End



            'If CInt(cboDepartment.SelectedValue) > 0 Then
            '    strSearching &= "AND departmentunkid=" & CInt(cboDepartment.SelectedValue) & " "
            'End If

            'If CInt(cboSection.SelectedValue) > 0 Then
            '    strSearching &= "AND sectionunkid = " & CInt(cboSection.SelectedValue) & " "
            'End If

            'If CInt(cboJob.SelectedValue) > 0 Then
            '    strSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue)
            'End If

            ''Anjan (21 Nov 2011)-Start
            ''ENHANCEMENT : TRA COMMENTS
            'If mstrAdvanceFilter.Length > 0 Then
            '    strSearching &= "AND " & mstrAdvanceFilter
            'End If
            ''Anjan (21 Nov 2011)-End 

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    If strSearching.Length > 0 Then
            '        strSearching = strSearching.Substring(3)
            '        dtFill = New DataView(dsFill.Tables("Employee"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            '    Else
            '        dtFill = dsFill.Tables("Employee")
            '    End If

            'Else

            '    If strSearching.Length > 0 Then
            '        strSearching = strSearching.Substring(3)
            '        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & " AND isactive = 1", "", DataViewRowState.CurrentRows).ToTable
            '    Else
            '        dtFill = New DataView(dsFill.Tables("Employee"), " isactive = 1", "", DataViewRowState.CurrentRows).ToTable
            '    End If

            'End If
            ''Pinkal (24-Jun-2011) -- End
            


            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'If CInt(cboDepartment.SelectedValue) > 0 Then
            '    strSearching &= "AND ETRF.departmentunkid = '" & CInt(cboDepartment.SelectedValue) & "' "
            'End If

            'If CInt(cboSection.SelectedValue) > 0 Then
            '    strSearching &= "AND ETRF.sectionunkid = '" & CInt(cboSection.SelectedValue) & "' "
            'End If

            'If CInt(cboJob.SelectedValue) > 0 Then
            '    strSearching &= "AND ERECAT.jobunkid = '" & CInt(cboJob.SelectedValue) & "' "
            'End If

            'If mstrAdvanceFilter.Length > 0 Then
            '    strSearching &= "AND " & mstrAdvanceFilter
            'End If

            'If strSearching.Length > 0 Then
            '    strSearching = strSearching.Substring(3)
            'End If

            'dsFill = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, True, _
            '                             False, _
            '                             "Employee", _
            '                             ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching)

            'dtFill = dsFill.Tables(0)


            If CInt(cboDepartment.SelectedValue) > 0 Then
                strSearching &= "AND T.departmentunkid = '" & CInt(cboDepartment.SelectedValue) & "' "
            End If

            If CInt(cboSection.SelectedValue) > 0 Then
                strSearching &= "AND T.sectionunkid = '" & CInt(cboSection.SelectedValue) & "' "
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                strSearching &= "AND J.jobunkid = '" & CInt(cboJob.SelectedValue) & "' "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, False, "Employee", , , , , , , , , , , , , , , , strSearching)

            dtFill = dsFill.Tables(0)
            'S.SANDEEP [15 NOV 2016] -- END


            
            'S.SANDEEP [04 JUN 2015] -- END



            Generate_Calender()
            If dgvEmployeeCalender.SelectedCells.Count > 0 Then
                dgvEmployeeCalender_CellClick(sender, New DataGridViewCellEventArgs(dgvEmployeeCalender.SelectedCells(0).ColumnIndex, dgvEmployeeCalender.SelectedCells(0).RowIndex))
            Else
                lvUpcomingEvents.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objReset.Click
        Try
            If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedIndex = 0
            If cboSection.Items.Count > 0 Then cboSection.SelectedIndex = 0
            If cboJob.Items.Count > 0 Then cboJob.SelectedIndex = 0
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            objSearch_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub Generate_Calender()
        Try
            Dim dtRow As DataRow
            Dim mintFixedColumn As Integer = 1
            dgvEmployeeCalender.Rows.Clear()
            If dtFill Is Nothing Then Return
            If dtFill.Rows.Count = 0 Then Return

            Dim intDays As Integer = Date.DaysInMonth(dtpCalender.SelectionEnd.Year, dtpCalender.SelectionEnd.Month)
            DaysofWeek = New DateTime(intDays) {}
            strName = New String(intDays) {}


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE

            Dim intTotDays As Integer = Date.DaysInMonth(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month)
            For intDay As Integer = 1 To intDays
                If intDay <= intTotDays Then
                DaysofWeek(intDay) = eZeeDate.convertDate(dtpCalender.SelectionStart.Year & CInt(dtpCalender.SelectionStart.Month).ToString("0#") & intDay.ToString("0#"))

                    'Pinkal (02-Sep-2011) -- Start
                    strName(intDay) = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(DaysofWeek(intDay).DayOfWeek)
                    'Pinkal (02-Sep-2011) -- End
                strName(intDay) = strName(intDay).Substring(0, 3)
                End If
            Next

            'Pinkal (24-Jun-2011) -- End

            Dim pColumn As DataGridViewColumn
            Dim i As Integer
            Dim strTemp As String
            Dim pCell As clsMergeCell

            dgvEmployeeCalender.Columns.Clear()

            Dim HeaderStyle As New DataGridViewCellStyle

            HeaderStyle.BackColor = Color.SlateGray
            HeaderStyle.ForeColor = Color.White

            dgvEmployeeCalender.Columns.Add("objdgColEmployee", Language.getMessage(mstrModuleName, 3, "Employee"))
            pColumn = dgvEmployeeCalender.Columns("objdgColEmployee")
            pColumn.Frozen = True
            pColumn.ReadOnly = True
            pColumn.DefaultCellStyle = HeaderStyle
            pColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            pColumn.Width = 110
            pColumn.SortMode = DataGridViewColumnSortMode.NotSortable

            dtInfo.Columns.Clear()
            Dim dtcolRowIndex As New DataColumn("RowIndex")
            Dim dtcolStartIndex As New DataColumn("StartIndex")
            Dim dtcolEndIndex As New DataColumn("EndIndex")
            Dim dtcolTranUnkid As New DataColumn("LeavePlannerUnkid")
            dtInfo.Columns.Add(dtcolRowIndex)
            dtInfo.Columns.Add(dtcolStartIndex)
            dtInfo.Columns.Add(dtcolEndIndex)
            dtInfo.Columns.Add(dtcolTranUnkid)

            For i = 0 To mintNoofDays - 1
                strTemp = "objcol" + i.ToString()
                pColumn.Tag = mdtCurrent.Month & "," & mdtCurrent.Year

                'Pinkal (02-Sep-2011) -- Start

                'dgvEmployeeCalender.Columns.Add(strTemp, Format(mdtCurrent.AddDays(i), "d MMM yy") & " (" &  mdtCurrent.AddDays(i).DayOfWeek.ToString.Substring(0, 3) & ")")
                dgvEmployeeCalender.Columns.Add(strTemp, Format(mdtCurrent.AddDays(i), "d MMM yy") & " (" & CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(mdtCurrent.AddDays(i).DayOfWeek).ToString.Substring(0, 3) & ")")

                'Pinkal (02-Sep-2011) -- End

                pColumn = dgvEmployeeCalender.Columns(strTemp)
                pColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                pColumn.Width = 100
                pColumn.ReadOnly = True
                pColumn.HeaderCell.Style.BackColor = Color.SlateGray
                pColumn.HeaderCell.Style.ForeColor = Color.White
            Next

            dgvEmployeeCalender.Rows.Add(dtFill.Rows.Count)

            Dim intStartIndex As Integer = 0
            Dim intEndIndex As Integer = 0

            Dim objLeavePlanner As New clsleaveplanner

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", Not ConfigParameter._Object._IsIncludeInactiveEmp)
            Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True _
                                                                                    , True, "")
            'Pinkal (24-Aug-2015) -- End



            For j As Integer = 0 To dtFill.Rows.Count - 1

                'S.SANDEEP [15 NOV 2016] -- START
                'ENHANCEMENT : QUERY OPTIMIZATION
                'dgvEmployeeCalender.Rows(j).Cells(0).Value = dtFill.Rows(j)("firstname").ToString & " " & dtFill.Rows(j)("surname").ToString
                dgvEmployeeCalender.Rows(j).Cells(0).Value = dtFill.Rows(j)("employeename").ToString
                'S.SANDEEP [15 NOV 2016] -- END
                dgvEmployeeCalender.Rows(j).Tag = dtFill.Rows(j)("employeeunkid").ToString
                dgvEmployeeCalender.Rows(j).Resizable = DataGridViewTriState.False

                For c As Integer = 1 To dgvEmployeeCalender.Rows(j).Cells.Count - 1
                    dgvEmployeeCalender.Rows(j).Cells(c).Tag = mdtCurrent.AddDays(c - 1).Year & mdtCurrent.AddDays(c - 1).Month.ToString("0#") & mdtCurrent.AddDays(c - 1).Day.ToString("0#")
                Next

                'Dim objLeavePlanner As New clsleaveplanner
                ''Pinkal (24-Jun-2011) -- Start
                ''ISSUE : CHECK FOR ACTIVE EMPLOYEE
                ''Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", True)
                'Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", Not ConfigParameter._Object._IsIncludeInactiveEmp)
                ''Pinkal (24-Jun-2011) -- End

                Dim dtPlanner As DataTable = New DataView(dsFill.Tables("LeavePlanner"), "employeeunkid = " & CInt(dtFill.Rows(j)("employeeunkid").ToString), "", DataViewRowState.CurrentRows).ToTable

                For p As Integer = 0 To dtPlanner.Rows.Count - 1


                    For cell As Integer = 1 To dgvEmployeeCalender.Rows(j).Cells.Count - 1
                        If dgvEmployeeCalender.Rows(j).Cells(cell).Tag Is Nothing Then Continue For

                        pCell = New clsMergeCell

                        Dim intDiff As Integer = 0
                        dtRow = dtInfo.NewRow
                        If eZeeDate.convertDate(dgvEmployeeCalender.Rows(j).Cells(cell).Tag.ToString).ToShortDateString >= eZeeDate.convertDate(dtPlanner.Rows(p)("startdate").ToString()).AddDays(p).ToShortDateString _
                           Or eZeeDate.convertDate(dgvEmployeeCalender.Rows(j).Cells(cell).Tag.ToString).ToShortDateString <= eZeeDate.convertDate(dtPlanner.Rows(p)("stopdate").ToString()).ToShortDateString Then

                            intStartIndex = mintFixedColumn + CInt(DateDiff(DateInterval.Day, mdtCurrent, eZeeDate.convertDate(dtPlanner.Rows(p)("startdate").ToString()).Date))

                            If intStartIndex <= 0 Then intStartIndex = 1

                            If eZeeDate.convertDate(dgvEmployeeCalender.Rows(j).Cells(dgvEmployeeCalender.Rows(j).Cells.Count - 1).Tag.ToString).Date >= eZeeDate.convertDate(dtPlanner.Rows(p)("stopdate").ToString()).Date Then
                                intEndIndex = mintFixedColumn + CInt(DateDiff(DateInterval.Day, mdtCurrent, eZeeDate.convertDate(dtPlanner.Rows(p)("stopdate").ToString()).Date))
                            Else
                                intEndIndex = mintFixedColumn + CInt(DateDiff(DateInterval.Day, mdtCurrent, eZeeDate.convertDate(dgvEmployeeCalender.Rows(j).Cells(dgvEmployeeCalender.Rows(j).Cells.Count - 1).Tag.ToString).Date))
                            End If

                            
                            ' pCell.MakeMerge(dgvEmployeeCalender, j, intStartIndex, intEndIndex, Color.FromArgb(CInt(dtPlanner.Rows(p)("color"))), Color.White, dtPlanner.Rows(p)("statusname").ToString(), "", picStayView.Image)

                            'S.SANDEEP [15 NOV 2016] -- START
                            'ENHANCEMENT : QUERY OPTIMIZATION
                            'pCell.MakeMerge(dgvEmployeeCalender, j, intStartIndex, intEndIndex, Color.FromArgb(CInt(dtPlanner.Rows(p)("color"))), Color.White, dtPlanner.Rows(p)("leavename").ToString(), "", picStayView.Image)
                            Dim clrColor As Color = Color.White : Dim StrLeaveName As String = ""
                            If IsDBNull(dtPlanner.Rows(p)("color")) = False Then
                                clrColor = Color.FromArgb(CInt(dtPlanner.Rows(p)("color")))
                            End If
                            If IsDBNull(dtPlanner.Rows(p)("leavename")) = False Then
                                StrLeaveName = dtPlanner.Rows(p)("leavename").ToString()
                            End If
                            pCell.MakeMerge(dgvEmployeeCalender, j, intStartIndex, intEndIndex, clrColor, Color.White, StrLeaveName, "", picStayView.Image)
                            'S.SANDEEP [15 NOV 2016] -- END

                            dtRow("RowIndex") = j : dtRow("StartIndex") = intStartIndex : dtRow("EndIndex") = intEndIndex
                            dtRow("LeavePlannerUnkid") = CInt(dtPlanner.Rows(p)("leaveplannerunkid"))
                            dtInfo.Rows.Add(dtRow)

                            For c As Integer = 1 To dgvEmployeeCalender.Rows(j).Cells.Count - 1
                                dgvEmployeeCalender.Rows(j).Cells(c).Tag = mdtCurrent.AddDays(c - 1).Year & mdtCurrent.AddDays(c - 1).Month.ToString("0#") & mdtCurrent.AddDays(c - 1).Day.ToString("0#")
                            Next

                            Exit For
                        End If

                    Next

                Next

            Next
            dgvEmployeeCalender.ShowCellToolTips = True
            objlblSelectedArea.Visible = False
            objlblRight.Visible = False
            objlblLeft.Visible = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Calender", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR DEPARTMENT
            Dim objDepartment As New clsDepartment
            dsFill = objDepartment.getComboList("Department", True)
            cboDepartment.DataSource = dsFill.Tables("Department")
            cboDepartment.DisplayMember = "name"
            cboDepartment.ValueMember = "departmentunkid"

            'FOR SECTION
            dsFill = Nothing
            Dim objSection As New clsSections
            dsFill = objSection.getComboList("Section", True)
            cboSection.DataSource = dsFill.Tables("Section")
            cboSection.DisplayMember = "name"
            cboSection.ValueMember = "sectionunkid"

            'FOR JOBS
            dsFill = Nothing
            Dim objJob As New clsJobs
            dsFill = objJob.getComboList("Job", True)
            cboJob.DataSource = dsFill.Tables("Job")
            cboJob.DisplayMember = "name"
            cboJob.ValueMember = "jobunkid"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub UpdateLeavePlanner()
        Try
            Dim objLeavePlanner As New clsleaveplanner
            objLeavePlanner._Leaveplannerunkid = mintLeavePlannerunkid
            'If objLeavePlanner._Status = 6 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry you cannot perform this operation. Reason: Selected Employee Leave is already Canceled."), enMsgBoxStyle.Information)
            '    Exit Sub
            'Else

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to change this Employee Leave Period?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objLeavePlanner._Startdate = mdtNewStartdate
                objLeavePlanner._Stopdate = mdtNewStopdate

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objLeavePlanner.Update()
                objLeavePlanner.Update(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'S.SANDEEP [04 JUN 2015] -- END


                'Pinkal (04-Nov-2010) -- START

                If objLeavePlanner._Message <> "" Then
                    eZeeMsgBox.Show(objLeavePlanner._Message, enMsgBoxStyle.Information)
                Else
                    Generate_Calender()
                End If

                'Pinkal (04-Nov-2010) -- END


            End If
            ' End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateLeavePlanner", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLeaveType()
        '    Dim dsFill As DataSet = Nothing
        '    Dim lvItem As ListViewItem
        '    Try
        '        lvItem = Nothing
        '        Dim objLeaveType As New clsleavetype_master
        '        dsFill = objLeaveType.GetList("LeaveType", True)
        '        lstLeaveType.Items.Clear()
        '        For Each drRow As DataRow In dsFill.Tables("LeaveType").Rows
        '            lvItem = New ListViewItem
        '            lvItem.Text = drRow("leavename").ToString
        '            lvItem.Tag = drRow("leavetypeunkid")
        '            lstLeaveType.Items.Add(lvItem)
        '            If CInt(drRow("color")) >= 0 Then
        '                lstLeaveType.Items(lvItem.Index).ForeColor = Color.Black
        '            Else
        '                lstLeaveType.Items(lvItem.Index).BackColor = Color.FromArgb(CInt(drRow("color")))
        '                lstLeaveType.Items(lvItem.Index).ForeColor = Color.White
        '            End If
        '        Next
        '        If lstLeaveType.Items.Count > 8 Then
        '            colhLeaveType.Width = 170 - 20
        '        Else
        '            colhLeaveType.Width = 167
        '        End If
        '    Catch ex As Exception

        '    End Try
    End Sub

#End Region

#Region "Calendar's Event"

    Private Sub dtpCalender_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles dtpCalender.DateChanged
        Try
            If btnWeek.Selected Then
                mdtCurrent = dtpCalender.SelectionStart.Date

                'Pinkal (03-Jan-2011) -- Start


                'S.SANDEEP [ 09 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES (REMOVING FINANCIAL YEAR CONDITION)
                'If CInt(DateDiff(DateInterval.Day, mdtCurrent, FinancialYear._Object._Database_End_Date.Date)) > 6 Then
                '    mintNoofDays = 7
                'Else
                '    mintNoofDays = CInt(DateDiff(DateInterval.Day, mdtCurrent, FinancialYear._Object._Database_End_Date.Date.AddDays(1)))
                'End If
                mintNoofDays = CInt(DateDiff(DateInterval.Day, mdtCurrent, dtpCalender.SelectionStart.Date.AddDays(7)))
                'S.SANDEEP [ 09 APRIL 2012 ] -- END


                'Pinkal (03-Jan-2011) -- End

            ElseIf btnMonthly.Selected = True Then
                mdtCurrent = New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month, 1)
                mintNoofDays = Date.DaysInMonth(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month)
            End If
            Generate_Calender()
            If dgvEmployeeCalender.SelectedCells.Count > 0 Then
                dgvEmployeeCalender_CellClick(sender, New DataGridViewCellEventArgs(dgvEmployeeCalender.SelectedCells(0).ColumnIndex, dgvEmployeeCalender.SelectedCells(0).RowIndex))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpCalender_DateChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid's Event"

    Private Sub dgvEmployeeCalender_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployeeCalender.CellClick
        Try

            'Pinkal (27-oct-2010) ------------------- START

            Dim Firstdate As DateTime
            Dim LastDate As DateTime

            If dtpCalender.SelectionStart.Month = dtpCalender.MaxDate.Month Then
                Firstdate = New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month, 1)
                LastDate = New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month, Date.DaysInMonth(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.Month))
            Else
                Firstdate = New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.AddMonths(1).Month, 1)
                LastDate = New DateTime(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.AddMonths(1).Month, Date.DaysInMonth(dtpCalender.SelectionStart.Year, dtpCalender.SelectionStart.AddMonths(1).Month))
            End If

            'Pinkal (27-oct-2010) ------------------- END

            If e.RowIndex < 0 Then Return

            Dim objLeavePlanner As New clsleaveplanner

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", True)

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", Not ConfigParameter._Object._IsIncludeInactiveEmp)
            Dim dsFill As DataSet = objLeavePlanner.GetList("LeavePlanner", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True _
                                                                                    , True, "")
            'Pinkal (24-Aug-2015) -- End

            'Pinkal (24-Jun-2011) -- End


            Dim dtPlanner As DataTable = New DataView(dsFill.Tables("LeavePlanner"), "employeeunkid = " & CInt(dtFill.Rows(e.RowIndex)("employeeunkid").ToString) _
                                                    & " AND startdate >='" & eZeeDate.convertDate(Firstdate) & "' AND stopdate <='" _
                                                    & eZeeDate.convertDate(LastDate) & "'", "startdate asc", DataViewRowState.CurrentRows).ToTable

            lvUpcomingEvents.Items.Clear()
            Dim lvitem As ListViewItem

            If dtPlanner.Rows.Count = 0 Then Exit Sub
            For i As Integer = 0 To dtPlanner.Rows.Count - 1
                Dim intDiif As Integer = CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(dtPlanner.Rows(i)("startdate").ToString()).Date, eZeeDate.convertDate(dtPlanner.Rows(i)("stopdate").ToString()).AddDays(1).Date))
                For dt As Integer = 0 To intDiif - 1
                    lvitem = New ListViewItem
                    lvitem.Text = eZeeDate.convertDate(dtPlanner.Rows(i)("startdate").ToString()).AddDays(dt).ToShortDateString
                    lvUpcomingEvents.Items.Add(lvitem)
                Next
            Next
            If lvUpcomingEvents.Items.Count > 16 Then
                colhUpcomingEvents.Width = 169 - 20
            Else
                colhUpcomingEvents.Width = 169
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployeeCalender_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmployeeCalender_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvEmployeeCalender.MouseDown
        Try
            If e.Button = Windows.Forms.MouseButtons.Left Then

                For i As Integer = 0 To dtInfo.Rows.Count - 1

                    If IsDBNull(dtInfo.Rows(i).Item("rowindex")) Then Continue For

                    If dgvEmployeeCalender.HitTest(e.X, e.Y).RowIndex = CInt(dtInfo.Rows(i).Item("rowindex")) And dgvEmployeeCalender.HitTest(e.X, e.Y).ColumnIndex >= CInt(dtInfo.Rows(i).Item("startindex")) And dgvEmployeeCalender.HitTest(e.X, e.Y).ColumnIndex <= CInt(dtInfo.Rows(i).Item("endindex")) Then
                        mblnSelected = True
                        mintOldRowIndex = CInt(dtInfo.Rows(i).Item("RowIndex"))
                        mintOldStartIndex = CInt(dtInfo.Rows(i).Item("StartIndex"))
                        mintOldEndIndex = CInt(dtInfo.Rows(i).Item("EndIndex"))
                        mintLeavePlannerunkid = CInt(dtInfo.Rows(i).Item("LeavePlannerUnkid"))

                        Dim objPlanner As New clsleaveplanner
                        objPlanner._Leaveplannerunkid = mintLeavePlannerunkid
                        mdtOldStartdate = objPlanner._Startdate
                        mdtOldStopdate = objPlanner._Stopdate


                        dgvEmployeeCalender.MultiSelect = False


                        Dim objLeavePlanner As New clsleaveplanner
                        objLeavePlanner._Leaveplannerunkid = mintLeavePlannerunkid

                        'If objLeavePlanner._Status = 6 Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry you cannot perform this operation. Reason: Selected Employee Leave is already Canceled."), enMsgBoxStyle.Information)
                        '    mblnSelected = False
                        '    Exit Sub
                        'Else
                        Dim dtSelectedAreaMaxDate As Date = Nothing
                        Dim dtSelectedAreaStartdate As Date = Nothing
                        dtSelectedAreaStartdate = eZeeDate.convertDate(dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(mintOldStartIndex).Tag.ToString).Date
                        dtSelectedAreaMaxDate = eZeeDate.convertDate(dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(mintOldEndIndex).Tag.ToString).Date

                        If mdtOldStopdate.Date > dtSelectedAreaMaxDate.Date Or mdtOldStartdate.Date < dtSelectedAreaStartdate.Date Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot select this transaction. Please open this transaction and then perform appropriate operation. Do you want to open this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Dim objFrmLeavePlanner As New frmleaveplanner_AddEdit
                                objFrmLeavePlanner.displayDialog(mintLeavePlannerunkid, enAction.EDIT_ONE)
                                Generate_Calender()
                            End If
                            mblnSelected = False
                            Exit Sub
                        End If
                        ' End If


                        dgvEmployeeCalender.ClearSelection()
                        dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(mintOldStartIndex).Selected = True
                        Dim intWidth As Integer = 0

                        For j As Integer = mintOldStartIndex To mintOldEndIndex
                            intWidth += dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(j).Size.Width
                        Next

                        objlblSelectedArea.BringToFront()
                        objlblRight.BringToFront()
                        objlblLeft.BringToFront()


                        objlblSelectedArea.Visible = True
                        objlblSelectedArea.BorderStyle = BorderStyle.FixedSingle
                        If dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(mintOldStartIndex).Value IsNot Nothing Then objlblSelectedArea.Text = dgvEmployeeCalender.Rows(mintOldRowIndex).Cells(mintOldStartIndex).Value.ToString
                        objlblSelectedArea.Location = New Point(dgvEmployeeCalender.Location.X + dgvEmployeeCalender.GetCellDisplayRectangle(mintOldStartIndex, mintOldRowIndex, True).X, dgvEmployeeCalender.Location.Y + dgvEmployeeCalender.GetCellDisplayRectangle(mintOldStartIndex, mintOldRowIndex, True).Y)
                        objlblSelectedArea.Height = dgvEmployeeCalender.SelectedCells(0).Size.Height
                        objlblSelectedArea.Width = intWidth

                        objlblRight.Visible = True
                        objlblLeft.Visible = True
                        objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))
                        objlblLeft.Location = New Point(objlblSelectedArea.Location.X - objlblLeft.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblLeft.Size.Height / 2)))
                        oldX = e.X
                        oldY = e.Y

                        If intWidth <= 0 Then
                            objlblSelectedArea.Visible = False
                            objlblLeft.Visible = False
                            objlblRight.Visible = False
                        End If

                    Else
                        If mblnSelected = False Then
                            objlblSelectedArea.Visible = False
                            objlblLeft.Visible = False
                            objlblRight.Visible = False
                        End If
                    End If
                Next

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployeeCalender_MouseDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmployeeCalender_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvEmployeeCalender.MouseUp, objlblSelectedArea.MouseUp
        Try
            'START FOR GET FORCASTE LEAVE 
            dgvEmployeeCalender_CellClick(sender, New DataGridViewCellEventArgs(dgvEmployeeCalender.HitTest(e.X, e.Y).ColumnIndex, dgvEmployeeCalender.HitTest(e.X, e.Y).RowIndex))
            'END FOR GET FORCASTE LEAVE 

            If mblnSelected Then

                'Pinkal (16-Feb-2011) -- Start

                If objlblSelectedArea.Location.Y < 0 Then
                    mblnSelected = False
                    objlblSelectedArea.Visible = False
                    objlblLeft.Visible = False
                    objlblRight.Visible = False
                    Exit Sub
                End If

                If dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).RowIndex < 0 Then
                    mblnSelected = False
                    objlblSelectedArea.Visible = False
                    objlblLeft.Visible = False
                    objlblRight.Visible = False
                    Exit Sub
                End If

                'Pinkal (16-Feb-2011) -- End

                mintNewRowINdex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).RowIndex
                mintNewStartIndex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).ColumnIndex
                mintNewEndIndex = mintNewStartIndex + mintOldEndIndex - mintOldStartIndex
                dgvEmployeeCalender.MultiSelect = True



                If mintNewStartIndex = mintOldStartIndex And mintNewEndIndex = mintOldEndIndex And mintOldRowIndex = mintNewRowINdex Then
                    mblnSelected = False
                    'Exit Sub
                End If

                objlblSelectedArea.Location = New Point(dgvEmployeeCalender.Location.X + dgvEmployeeCalender.GetCellDisplayRectangle(mintNewStartIndex, mintNewRowINdex, True).X, dgvEmployeeCalender.Location.Y + dgvEmployeeCalender.GetCellDisplayRectangle(mintNewStartIndex, mintNewRowINdex, True).Y)
                objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))
                objlblLeft.Location = New Point(objlblSelectedArea.Location.X - objlblLeft.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblLeft.Size.Height / 2)))
                If sender.Equals(objlblSelectedArea) Then
                    objlblSelectedArea.Location = New Point(dgvEmployeeCalender.Location.X + dgvEmployeeCalender.GetCellDisplayRectangle(mintNewStartIndex, mintNewRowINdex, True).X, dgvEmployeeCalender.Location.Y + dgvEmployeeCalender.GetCellDisplayRectangle(mintNewStartIndex, mintNewRowINdex, True).Y)
                    objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))
                    objlblLeft.Location = New Point(objlblSelectedArea.Location.X - objlblLeft.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblLeft.Size.Height / 2)))

                    mdtNewStartdate = mdtOldStartdate.AddDays(mintNewStartIndex - mintOldStartIndex)
                    mdtNewStopdate = mdtOldStopdate.AddDays(mintNewEndIndex - mintOldEndIndex)

                    UpdateLeavePlanner()

                    objlblSelectedArea.Visible = False
                    objlblLeft.Visible = False
                    objlblRight.Visible = False
                End If
                mblnSelected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployeeCalender_MouseUp", mstrModuleName)
            objlblSelectedArea.Visible = False
        End Try
    End Sub

    Private Sub dgvEmployeeCalender_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvEmployeeCalender.Scroll
        Try
            objlblSelectedArea.Visible = False
            objlblLeft.Visible = False
            objlblRight.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployeeCalender_Scroll", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Label's Event"

    Private Sub objlblSelectedArea_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblSelectedArea.MouseDown, objlblRight.MouseDown, objlblLeft.MouseDown
        Try
            mblnSelected = True
            oldX = e.X
            oldY = e.Y
            mintOldWidth = objlblSelectedArea.Size.Width
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblSelectedArea_MouseDown", mstrModuleName)
        End Try
    End Sub

    Private Sub objlblSelectedArea_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblSelectedArea.MouseMove
        Try
            If mblnSelected Then
                objlblSelectedArea.Location = New Point(objlblSelectedArea.Location.X + e.X - oldX, objlblSelectedArea.Location.Y + e.Y - oldY)
                objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))
                objlblLeft.Location = New Point(objlblSelectedArea.Location.X - objlblLeft.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblLeft.Size.Height / 2)))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblSelectedArea_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub objlblLeft_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblLeft.MouseUp
        Try
            mintNewRowINdex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).RowIndex
            mintNewStartIndex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).ColumnIndex
            mintNewEndIndex = mintOldEndIndex

            Dim intWidth As Integer = 0
            intWidth = objlblSelectedArea.Size.Width + (objlblSelectedArea.Location.X - dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).ColumnX)

            objlblSelectedArea.Size = New Size(intWidth, objlblSelectedArea.Size.Height)
            objlblSelectedArea.Location = New Point(dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).ColumnX, objlblSelectedArea.Location.Y)
            objlblLeft.Location = New Point(objlblSelectedArea.Location.X - objlblLeft.Size.Width, objlblLeft.Location.Y)

            mdtNewStopdate = mdtOldStopdate

            'Pinkal (16-Feb-2011) -- Start

            If mintNewStartIndex < 0 Then mintNewStartIndex += 1

            'Pinkal (16-Feb-2011) -- End

            mdtNewStartdate = mdtOldStartdate.AddDays(-1 * (mintOldStartIndex - mintNewStartIndex))
            UpdateLeavePlanner()

            objlblSelectedArea.Visible = False
            objlblLeft.Visible = False
            objlblRight.Visible = False

            mblnSelected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblLeft_MouseUp", mstrModuleName)
        End Try
    End Sub

    Private Sub objlblLeft_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblLeft.MouseMove
        Try
            If mblnSelected = True Then
                If objlblSelectedArea.Size.Width - (e.X - oldX) <= objlblSelectedArea.MinimumSize.Width Or objlblSelectedArea.Size.Height <= objlblSelectedArea.MinimumSize.Height Then Exit Sub
                objlblLeft.Location = New Point(objlblLeft.Location.X + e.X - oldX, objlblLeft.Location.Y)
                objlblSelectedArea.Size = New Size(objlblSelectedArea.Size.Width - (e.X - oldX), objlblSelectedArea.Size.Height)
                objlblSelectedArea.Location = New Point(objlblLeft.Location.X + objlblLeft.Width, objlblSelectedArea.Location.Y)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblLeft_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub objlblRight_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblRight.MouseMove
        Try
            If mblnSelected = True Then
                objlblSelectedArea.Size = New Size(objlblSelectedArea.Size.Width + (e.X - oldX), objlblSelectedArea.Size.Height)
                objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblRight_MouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub objlblRight_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles objlblRight.MouseUp
        Try
            mintNewRowINdex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).RowIndex
            mintNewStartIndex = dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X, objlblSelectedArea.Location.Y).ColumnIndex

            mintNewEndIndex = CInt(IIf(dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y).ColumnIndex < 0, 0, dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y).ColumnIndex))

            Dim intWidth As Integer = 0
            If dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y).ColumnX > -1 Then
            intWidth = objlblSelectedArea.Size.Width + (dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y).ColumnX + dgvEmployeeCalender.Columns(dgvEmployeeCalender.HitTest(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y).ColumnIndex).Width - (objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width))
            End If
            objlblSelectedArea.Size = New Size(intWidth, objlblSelectedArea.Size.Height)
            objlblRight.Location = New Point(objlblSelectedArea.Location.X + objlblSelectedArea.Size.Width, objlblSelectedArea.Location.Y + CInt((objlblSelectedArea.Size.Height / 2)) - CInt((objlblRight.Size.Height / 2)))

            mdtNewStartdate = mdtOldStartdate

            ''Pinkal (16-Feb-2011) -- Start

            If mintNewEndIndex - mintOldEndIndex < 0 And mintNewEndIndex <= 0 Then mintNewEndIndex = mintOldEndIndex + 1

            'Pinkal (16-Feb-2011) -- End

            mdtNewStopdate = mdtOldStopdate.AddDays(mintNewEndIndex - mintOldEndIndex)

            UpdateLeavePlanner()

            objlblSelectedArea.Visible = False
            objlblLeft.Visible = False
            objlblRight.Visible = False
            mblnSelected = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objlblRight_MouseUp", mstrModuleName)
            objlblSelectedArea.Visible = False
            objlblLeft.Visible = False
            objlblRight.Visible = False
        End Try
    End Sub

#End Region

#Region "Listview's Event"

    Private Sub lstLeaveType_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs)
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lstLeaveType_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMonthCalender.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMonthCalender.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUpcomingThings.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUpcomingThings.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnMonthly.GradientBackColor = GUI._ButttonBackColor 
			Me.btnMonthly.GradientForeColor = GUI._ButttonFontColor

			Me.btnWeek.GradientBackColor = GUI._ButttonBackColor 
			Me.btnWeek.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMonthCalender.Text = Language._Object.getCaption(Me.gbMonthCalender.Name, Me.gbMonthCalender.Text)
			Me.btnMonthly.Text = Language._Object.getCaption(Me.btnMonthly.Name, Me.btnMonthly.Text)
			Me.btnWeek.Text = Language._Object.getCaption(Me.btnWeek.Name, Me.btnWeek.Text)
			Me.dtpCalender.Text = Language._Object.getCaption(Me.dtpCalender.Name, Me.dtpCalender.Text)
			Me.gbUpcomingThings.Text = Language._Object.getCaption(Me.gbUpcomingThings.Name, Me.gbUpcomingThings.Text)
			Me.colhUpcomingEvents.Text = Language._Object.getCaption(CStr(Me.colhUpcomingEvents.Tag), Me.colhUpcomingEvents.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lbljob.Text = Language._Object.getCaption(Me.lbljob.Name, Me.lbljob.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.lbldepartment.Text = Language._Object.getCaption(Me.lbldepartment.Name, Me.lbldepartment.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Are you sure you want to change this Employee Leave Period?")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot select this transaction. Please open this transaction and then perform appropriate operation. Do you want to open this transaction?")
			Language.setMessage(mstrModuleName, 3, "Employee")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


