﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 4

Public Class frmGlobalLeaveAssign

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmGlobalLeaveAssign"
    Private mblnCancel As Boolean = True
    Private objBatch As clsbatch_master
    Private menAction As enAction = enAction.ADD_ONE
    Friend mintBatchUnkid As Integer = -1
    Private mintYearunkid As Integer = -1
    Private mintLeavetypeunkid As Integer = -1
    Private mdtEmployee As DataTable = Nothing
    Private intTotalleave As Integer = 1
    Private mstrAdvanceFilter As String = ""
    Private dvEmployee As DataView
    Private dsEmployee As DataSet
    Private mdtDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal intYearunkid As Integer, ByVal intleavetypeunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBatchUnkid = intUnkId
            mintYearunkid = intYearunkid
            mintLeavetypeunkid = intleavetypeunkid
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintBatchUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmGlobalLeaveAssign_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBatch = New clsbatch_master
        Try
            Call Set_Logo(Me, gApplicationType)
            setColor()

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date

                dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpStopDate.MaxDate = FinancialYear._Object._Database_End_Date

                LblDueDate.Visible = False
                dtpDueDate.Visible = False
                rdNewEmployee.Visible = False
                rdDueDate.Visible = False
                LblELCLeaveType.Visible = False
                cboELCLeaveType.Visible = False

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                objbtnSearchLeave1.Visible = False
                radEmpWOAccrue.Checked = True
                'Pinkal (30-Jun-2016) -- End

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                LblDueDate.Visible = True
                dtpDueDate.Visible = True
                rdNewEmployee.Visible = True
                rdDueDate.Visible = True
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpDueDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpStartDate.Enabled = False
                dtpStopDate.Enabled = False

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                radEmpWOAccrue.Visible = False
                radShowAll.Visible = False
                'Pinkal (30-Jun-2016) -- End
            End If

            If menAction = enAction.EDIT_ONE Then
                objBatch._Batchunkid = mintBatchUnkid
            End If
            FillCombo()

            If dsEmployee Is Nothing Then
                'Pinkal (29-Jan-2019) -- Start
                'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
                'nudFromYear.Enabled = False
                'nudFromMonth.Enabled = False
                'nudFromYear.Value = 0
                'nudFromMonth.Value = 0
                pnlYear.Enabled = False
                'Pinkal (29-Jan-2019) -- End
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If

            txtCfAmount.Text = ""
            txtEligibilityAfter.Text = ""

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                rdDueDate_CheckedChanged(New Object(), New EventArgs())
            Else
                dgcolhAppliedLeave.Visible = False
                dgcolhApprovedLeave.Visible = False
                dgcolhIssuedLeave.Visible = False
                dgcolhRemaining_Bal.Visible = False
            End If


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                lblAccrualDays.Visible = True
                LblMonthlyAccrue.Visible = False
            ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                lblAccrualDays.Visible = False
                LblMonthlyAccrue.Visible = True
            End If
            'Pinkal (18-Nov-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalLeaveAssign_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalLeaveAssign_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalLeaveAssign_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalLeaveAssign_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnAssign_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalLeaveAssign_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalLeaveAssign_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objBatch = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Try
            If dgEmployee.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no employee to Accrue Leave."), enMsgBoxStyle.Information)
                dgEmployee.Select()
                Exit Sub

            ElseIf CInt(cboLeaveCode.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveCode.Select()
                Exit Sub

            ElseIf txtBatchNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Batch No cannot be blank. Batch No is required information."), enMsgBoxStyle.Information)
                txtBatchNo.Select()
                Exit Sub

            ElseIf txtAccrualAmount.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Days cannot be blank. Days is required information."), enMsgBoxStyle.Information)
                txtAccrualAmount.Select()
                Exit Sub

            ElseIf mdtEmployee IsNot Nothing Then
                Dim dr() As DataRow = mdtEmployee.Select("ischecked=True")
                If dr.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    dgEmployee.Select()
                    Exit Sub
                End If
            End If

            If txtCfAmount.Decimal > 0 Or txtEligibilityAfter.Decimal > 0 Or chkNoAction.Checked = True Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You have set CF amount or leave eligibility days or No action as checked. This will be applicable to all selected employee(s).") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            If txtCfAmount.Text.Trim = "" Or txtEligibilityAfter.Text.Trim = "" Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You have not set CF amount or leave eligibility days. 0 will be set to all selected employee(s).") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            'Pinkal (09-Dec-2020) -- Start
            'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
            'SetValue()
            'blnFlag = objBatch.Insert(mintYearunkid, mdtEmployee, CInt(txtAccrualAmount.Text.Trim), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Dim dtTable As DataTable = mdtEmployee.Copy()
            SetValue(dtTable)
            blnFlag = objBatch.Insert(mintYearunkid, dtTable, CInt(txtAccrualAmount.Text.Trim), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Pinkal (09-Dec-2020) -- End

            If blnFlag = False And objBatch._Message <> "" Then
                eZeeMsgBox.Show(objBatch._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                'Pinkal (09-Dec-2020) -- Start
                'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
                dtTable.Clear()
                dtTable = Nothing
                'Pinkal (09-Dec-2020) -- End
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBatch = Nothing
                    objBatch = New clsbatch_master
                    GetEmployee()
                    mdtEmployee.Clear()
                    txtBatchNo.Text = ""
                    txtAccrualAmount.Text = "0"
                    cboELCLeaveType.SelectedValue = 0
                    cboLeaveCode.SelectedValue = 0
                    chkSelectAll.Checked = False

                    If rdDueDate.Checked Then
                        cboELCLeaveType.Select()
                    ElseIf rdNewEmployee.Checked Then
                        cboLeaveCode.Select()
                    End If
                    chkNoAction.Checked = False
                    txtCfAmount.Text = ""
                    txtEligibilityAfter.Text = ""
                Else
                    mintBatchUnkid = objBatch._Batchunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            If radAppointedDate.Checked = True Then
                If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, To date cannot be less than From date"), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (nudFromYear.Value > 0 Or nudFromMonth.Value > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "From Condition is compulsory information.Please Select From Condition."), enMsgBoxStyle.Information)
                    cboFromcondition.Select()
                    Exit Sub
                ElseIf (nudToYear.Value > 0 Or nudToMonth.Value > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "To Condition is compulsory information.Please Select To Condition."), enMsgBoxStyle.Information)
                    cboTocondition.Select()
                    Exit Sub
                End If
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                If cboLeaveCode.SelectedValue Is Nothing OrElse CInt(cboLeaveCode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)

                    If rdDueDate.Checked Then
                        cboELCLeaveType.Select()
                    ElseIf rdNewEmployee.Checked Then
                        cboLeaveCode.Select()
                    End If

                    Exit Sub

                End If

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                If CInt(cboLeaveCode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                    cboLeaveCode.Select()
                    Exit Sub
                End If
                'Pinkal (30-Jun-2016) -- End
            End If


            GetEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
            Me.Cursor = Cursors.Default
            'Pinkal (14-Dec-2017) -- End
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGender.SelectedValue = 0
            nudFromYear.Value = 0
            nudFromMonth.Value = 0
            nudToYear.Value = 0
            nudToMonth.Value = 0
            cboFromcondition.SelectedValue = 0
            cboTocondition.SelectedValue = 0
            mstrAdvanceFilter = ""

            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            rdDueDate.Checked = True
            chkSelectAll.Checked = False
            'Pinkal (06-Feb-2013) -- End

            'Pinkal (29-Jan-2019) -- Start
            'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
            radAppointedDate.Checked = False
            radExpYear.Checked = False
            radProbationDate.Checked = False
            radConfirmationDate.Checked = False
            'Pinkal (29-Jan-2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLeave1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave1.Click, objbtnSearchLeave2.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchLeave1.Name.ToUpper
                    If cboELCLeaveType.DataSource IsNot Nothing Then
                        With frm
                            .ValueMember = cboELCLeaveType.ValueMember
                            .DisplayMember = cboELCLeaveType.DisplayMember
                            .CodeMember = "leavetypecode"
                            .DataSource = CType(cboELCLeaveType.DataSource, DataTable)
                        End With

                        If frm.DisplayDialog Then
                            cboELCLeaveType.SelectedValue = frm.SelectedValue
                        End If
                    End If
                Case objbtnSearchLeave2.Name.ToUpper
                    If cboLeaveCode.DataSource IsNot Nothing Then
                        With frm
                            .ValueMember = cboLeaveCode.ValueMember
                            .DisplayMember = cboLeaveCode.DisplayMember
                            .CodeMember = "leavetypecode"
                            .DataSource = CType(cboLeaveCode.DataSource, DataTable)
                        End With

                        If frm.DisplayDialog Then
                            cboLeaveCode.SelectedValue = frm.SelectedValue
                        End If
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "CheckBox's Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If mdtEmployee.Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboLeaveCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveCode.SelectedIndexChanged
        Try
            If CInt(cboLeaveCode.SelectedValue) = 0 Then Exit Sub
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)
            cboLeaveCode.Tag = objLeaveType._IsPaid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLeaveCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGender_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGender.SelectedIndexChanged
        Try
            chkSelectAll.Checked = False
            TxtSearch.Text = ""
            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            'GetEmployee()
            'Pinkal (30-Jun-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGender_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboLeaveCode.BackColor = GUI.ColorComp
            txtBatchNo.BackColor = GUI.ColorComp
            txtAccrualAmount.BackColor = GUI.ColorComp
            TxtSearch.BackColor = GUI.ColorOptional
            cboGender.BackColor = GUI.ColorOptional
            nudFromYear.BackColor = GUI.ColorOptional
            nudFromMonth.BackColor = GUI.ColorOptional
            nudToYear.BackColor = GUI.ColorOptional
            nudToMonth.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboTocondition.BackColor = GUI.ColorOptional

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                If rdDueDate.Checked Then
                    cboELCLeaveType.BackColor = GUI.ColorComp
                Else
                    cboELCLeaveType.BackColor = GUI.ColorOptional
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    'Pinkal (09-Dec-2020) -- Start
    'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal dtTable As DataTable)
        'Pinkal (09-Dec-2020) -- End
        Dim objEmployee As New clsEmployee_Master
        Try
            objBatch._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)
            objBatch._Batchno = txtBatchNo.Text.Trim
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                objBatch._Startdate = dtpStartDate.Value.Date
                objBatch._Enddate = CDate(IIf(dtpStopDate.Checked, dtpStopDate.Value.Date, Nothing))

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                objBatch._Startdate = Nothing
                objBatch._Enddate = Nothing
            End If
            objBatch._Userunkid = User._Object._Userunkid

            If radIssuebal.Checked Then
                objBatch._Accruesetting = enAccrueSetting.Issue_Balance
            ElseIf radExceedBal.Checked Then
                objBatch._Accruesetting = enAccrueSetting.Exceeding_balance_Unpaid
            ElseIf radDonotIssue.Checked Then
                objBatch._Accruesetting = enAccrueSetting.Donot_Issue_On_Exceeding_Balance

            ElseIf radDonotIssueAsonDate.Checked Then
                objBatch._Accruesetting = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date
            End If

            objBatch._CFlvAmount = CDec(IIf(txtCfAmount.Text.Trim.Length > 0, txtCfAmount.Text, 0))
            objBatch._EligibilityAfter = CInt(IIf(txtEligibilityAfter.Text.Trim.Length > 0, txtEligibilityAfter.Text, 0))
            objBatch._IsNoAction = chkNoAction.Checked


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objBatch._MaxNegativeDaysLimit = CInt(nudMaxNegativeDays.Value)
            'Pinkal (08-Oct-2018) -- End

            'Pinkal (09-Dec-2020) -- Start
            'Bug  -  Worked on Login and Forgot Password Issue in Oryx.

            'START FOR REMOVE ISSUED LEAVE EMPLOYEE

            Dim objIssue As New clsleaveissue_Tran

            'GetFilterEmployee()
            GetFilterEmployee(dtTable)

            'END FOR REMOVE ISSUED LEAVE EMPLOYEE


            'If mdtEmployee.Columns.Contains("yearunkid") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("yearunkid", Type.GetType("System.Int16")))
            'End If

            'If mdtEmployee.Columns.Contains("ispaid") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("ispaid", Type.GetType("System.Boolean")))
            'End If

            'If mdtEmployee.Columns.Contains("accrue_amount") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("accrue_amount", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("issue_amount") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("issue_amount", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("daily_amount") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("daily_amount", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("balance") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("balance", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("remaining_bal") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("remaining_bal", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("uptolstyr_accrueamt") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("uptolstyr_accrueamt", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("uptolstyr_issueamt") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("uptolstyr_issueamt", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("actualamount") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("actualamount", Type.GetType("System.Decimal")))
            'End If

            'If mdtEmployee.Columns.Contains("isshortleave") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("isshortleave", Type.GetType("System.Boolean")))
            'End If

            'If mdtEmployee.Columns.Contains("days") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("days", Type.GetType("System.Int16")))
            'End If

            'If mdtEmployee.Columns.Contains("isopenelc") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("isopenelc", Type.GetType("System.Boolean")))
            'End If

            'If mdtEmployee.Columns.Contains("iselc") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("iselc", Type.GetType("System.Boolean")))
            'End If

            'If mdtEmployee.Columns.Contains("LeaveBF") = False Then
            '    mdtEmployee.Columns.Add(New DataColumn("LeaveBF", Type.GetType("System.Decimal")))
            'End If


            'If mdtEmployee.Columns.Contains("isclosefy") = False Then
            '    mdtEmployee.Columns.Add("isclosefy", Type.GetType("System.Boolean"))
            'End If

            'If mdtEmployee.Columns.Contains("adj_remaining_bal") = False Then
            '    mdtEmployee.Columns.Add("adj_remaining_bal", Type.GetType("System.Decimal"))
            'End If

            'If mdtEmployee.Columns.Contains("monthly_accrue") = False Then
            '    mdtEmployee.Columns.Add("monthly_accrue", Type.GetType("System.Decimal"))
            'End If


            If dtTable.Columns.Contains("yearunkid") = False Then
                dtTable.Columns.Add(New DataColumn("yearunkid", Type.GetType("System.Int16")))
            End If

            If dtTable.Columns.Contains("ispaid") = False Then
                dtTable.Columns.Add(New DataColumn("ispaid", Type.GetType("System.Boolean")))
            End If

            If dtTable.Columns.Contains("accrue_amount") = False Then
                dtTable.Columns.Add(New DataColumn("accrue_amount", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("issue_amount") = False Then
                dtTable.Columns.Add(New DataColumn("issue_amount", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("daily_amount") = False Then
                dtTable.Columns.Add(New DataColumn("daily_amount", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("balance") = False Then
                dtTable.Columns.Add(New DataColumn("balance", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("remaining_bal") = False Then
                dtTable.Columns.Add(New DataColumn("remaining_bal", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("uptolstyr_accrueamt") = False Then
                dtTable.Columns.Add(New DataColumn("uptolstyr_accrueamt", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("uptolstyr_issueamt") = False Then
                dtTable.Columns.Add(New DataColumn("uptolstyr_issueamt", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("actualamount") = False Then
                dtTable.Columns.Add(New DataColumn("actualamount", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("isshortleave") = False Then
                dtTable.Columns.Add(New DataColumn("isshortleave", Type.GetType("System.Boolean")))
            End If

            If dtTable.Columns.Contains("days") = False Then
                dtTable.Columns.Add(New DataColumn("days", Type.GetType("System.Int16")))
            End If

            If dtTable.Columns.Contains("isopenelc") = False Then
                dtTable.Columns.Add(New DataColumn("isopenelc", Type.GetType("System.Boolean")))
            End If

            If dtTable.Columns.Contains("iselc") = False Then
                dtTable.Columns.Add(New DataColumn("iselc", Type.GetType("System.Boolean")))
            End If

            If dtTable.Columns.Contains("LeaveBF") = False Then
                dtTable.Columns.Add(New DataColumn("LeaveBF", Type.GetType("System.Decimal")))
            End If

            If dtTable.Columns.Contains("isclosefy") = False Then
                dtTable.Columns.Add("isclosefy", Type.GetType("System.Boolean"))
            End If

            If dtTable.Columns.Contains("adj_remaining_bal") = False Then
                dtTable.Columns.Add("adj_remaining_bal", Type.GetType("System.Decimal"))
            End If

            If dtTable.Columns.Contains("monthly_accrue") = False Then
                dtTable.Columns.Add("monthly_accrue", Type.GetType("System.Decimal"))
            End If

            'Pinkal (09-Dec-2020) -- End


            Dim mdtStartdate As DateTime
            Dim mdtStopdate As DateTime


            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                mdtStartdate = dtpStartDate.Value
                If dtpStopDate.Checked Then
                    mdtStopdate = dtpStopDate.Value
                Else
                    mdtStopdate = FinancialYear._Object._Database_End_Date
                End If

            End If


            'Pinkal (09-Dec-2020) -- Start
            'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
            'Dim drRow() As DataRow = mdtEmployee.Select("ischecked = True")
            Dim drRow() As DataRow = dtTable.Select("ischecked = True")
            'Pinkal (09-Dec-2020) -- End


            If drRow.Length <= 0 Then Exit Sub


            For i As Integer = 0 To drRow.Length - 1

                drRow(i)("yearunkid") = mintYearunkid
                drRow(i)("ispaid") = CBool(cboLeaveCode.Tag)
                drRow(i)("isclosefy") = False

                'START FOR GET EMPLOYEE APPOINT DATE
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(drRow(i)("employeeunkid"))

                'END FOR GET EMPLOYEE APPOINT DATE

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                    If rdNewEmployee.Checked Then

                        If objEmployee._Appointeddate.Date <> Nothing Then
                            mdtStartdate = objEmployee._Appointeddate.Date
                        End If

                        If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                            mdtStartdate = objEmployee._Reinstatementdate.Date
                        End If

                        If mdtStartdate.Date < FinancialYear._Object._Database_Start_Date Then
                            mdtStartdate = FinancialYear._Object._Database_Start_Date
                        End If

                        If objEmployee._Termination_To_Date.Date <> Nothing AndAlso (objEmployee._Termination_To_Date.Date < mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                            mdtStopdate = objEmployee._Termination_To_Date
                        Else
                            mdtStopdate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                        End If
                        drRow(i)("startdate") = mdtStartdate
                        drRow(i)("stopdate") = mdtStopdate

                    ElseIf rdDueDate.Checked Then

                        mdtStartdate = CDate(drRow(i)("stopdate")).AddDays(1).Date
                        mdtStopdate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                        drRow(i)("startdate") = mdtStartdate
                        drRow(i)("stopdate") = mdtStopdate

                    End If

                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                        drRow(i)("daily_amount") = CDec(txtAccrualAmount.Text.Trim) / DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1))
                    ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                        drRow(i)("daily_amount") = 0
                    End If
                    'Pinkal (18-Nov-2016) -- End


                    drRow(i)("isopenelc") = True
                    drRow(i)("iselc") = True


                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    mdtStartdate = dtpStartDate.Value.Date

                    If objEmployee._Appointeddate.Date <> Nothing AndAlso objEmployee._Appointeddate.Date > mdtStartdate.Date Then
                        mdtStartdate = objEmployee._Appointeddate.Date
                    End If

                    If objEmployee._Reinstatementdate.Date <> Nothing AndAlso (objEmployee._Reinstatementdate.Date > mdtStartdate.Date) Then
                        mdtStartdate = objEmployee._Reinstatementdate.Date
                    End If

                    If mdtStartdate.Date <= FinancialYear._Object._Database_Start_Date Then
                        mdtStartdate = FinancialYear._Object._Database_Start_Date
                    End If

                    If dtpStopDate.Checked Then
                        mdtStopdate = dtpStopDate.Value.Date

                        If objEmployee._Empl_Enddate <> Nothing AndAlso objEmployee._Empl_Enddate.Date < mdtStopdate.Date Then
                            mdtStopdate = objEmployee._Empl_Enddate.Date
                        End If

                        If objEmployee._Termination_From_Date <> Nothing AndAlso objEmployee._Termination_From_Date.Date < mdtStopdate.Date Then
                            mdtStopdate = objEmployee._Termination_From_Date.Date
                        End If

                        If objEmployee._Termination_To_Date.Date <> Nothing AndAlso objEmployee._Termination_To_Date < mdtStopdate.Date Then
                            mdtStopdate = objEmployee._Termination_To_Date
                        Else
                            mdtStopdate = mdtStopdate.Date
                        End If

                    Else
                        If objEmployee._Termination_To_Date.Date <> Nothing AndAlso objEmployee._Termination_To_Date < FinancialYear._Object._Database_End_Date.Date Then
                            mdtStopdate = objEmployee._Termination_To_Date
                        Else
                            mdtStopdate = FinancialYear._Object._Database_End_Date
                        End If
                    End If

                    drRow(i)("startdate") = mdtStartdate
                    drRow(i)("stopdate") = mdtStopdate
                    drRow(i)("isopenelc") = False
                    drRow(i)("iselc") = False


                    'Pinkal (18-Nov-2016) -- Start
                    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                    If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                        drRow(i)("daily_amount") = CDec(txtAccrualAmount.Text.Trim) / DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.AddDays(1))
                    ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                        drRow(i)("daily_amount") = 0
                    End If
                    'Pinkal (18-Nov-2016) -- End

                End If

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'Dim intDiff As Integer = CInt(DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))
                Dim intDiff As Integer = 0

                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    intDiff = CInt(DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))

                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    intDiff = CInt(DateDiff(DateInterval.Month, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
CheckMonthDiff:
                            'Pinkal (16-Feb-2017) -- Start
                            'Enhancement - Worked on ASP Monthly Leave Accrue Bug.
                            'If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > mdtStartdate.Date.Day Then
                            If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > mdtStopdate.Date.Day Then
                                'Pinkal (16-Feb-2017) -- End
                                intDiff = intDiff - 1
                            End If
                        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            GoTo CheckMonthDiff
                        End If
                    End If
                End If
                'Pinkal (18-Nov-2016) -- End


                Dim objBalance As New clsleavebalance_tran
                Dim dsList As DataSet = Nothing
                Dim mstrFilter As String = ""

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    dsList = objBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(drRow(i)("employeeunkid")), False, False, False, "", Nothing)
                    mstrFilter = " AND yearunkid <>" & mintYearunkid & " "

                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    dsList = objBalance.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                          , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                          , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, False, CInt(drRow(i)("employeeunkid")), True, True, False, "", Nothing)
                    mstrFilter = ""
                End If

                Dim dtList As DataTable = New DataView(dsList.Tables("List"), "leavetypeunkid = " & CInt(cboLeaveCode.SelectedValue) & mstrFilter, "", DataViewRowState.CurrentRows).ToTable


                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    drRow(i)("accrue_amount") = CDec(drRow(i)("daily_amount")) * intDiff
                    drRow(i)("monthly_accrue") = 0
                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then
                    drRow(i)("accrue_amount") = txtAccrualAmount.Decimal * intDiff
                    drRow(i)("monthly_accrue") = txtAccrualAmount.Decimal
                End If
                'Pinkal (18-Nov-2016) -- End



                If dtList.Rows.Count > 0 AndAlso mstrFilter.Trim.Length > 0 Then    'FOR  UPDATE LAST YEAR AMOUNT TO CURRENT YEAR AMOUNT
                    drRow(i)("accrue_amount") = CDec(drRow(i)("accrue_amount")) + CDec(dtList.Rows(0)("remaining_bal"))
                    drRow(i)("uptolstyr_accrueamt") = CDec(dtList.Rows(0)("uptolstyr_accrueamt")) + CDec(drRow(i)("accrue_amount"))
                    drRow(i)("uptolstyr_issueamt") = CDec(dtList.Rows(0)("uptolstyr_issueamt"))
                    drRow(i)("LeaveBF") = CDec(dtList.Rows(0)("remaining_bal"))
                    drRow(i)("remaining_bal") = CDec(drRow(i)("accrue_amount"))


                ElseIf dtList.Rows.Count <= 0 AndAlso mstrFilter.Trim.Length <= 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    drRow(i)("accrue_amount") = CDec(drRow(i)("accrue_amount")) + CDec(dsList.Tables(0).Rows(0)("leavebf"))
                    drRow(i)("uptolstyr_accrueamt") = (CDec(dsList.Tables(0).Rows(0)("uptolstyr_accrueamt")) - CDec(dsList.Tables(0).Rows(0)("accrue_amount"))) + CDec(drRow(i)("accrue_amount"))
                    drRow(i)("uptolstyr_issueamt") = CInt(dsList.Tables(0).Rows(0)("uptolstyr_issueamt"))
                    drRow(i)("remaining_bal") = CDec(drRow(i)("accrue_amount"))
                    drRow(i)("LeaveBF") = CDec(dsList.Tables(0).Rows(0)("leavebf"))

                ElseIf dtList.Rows.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso mstrFilter.Trim.Length <= 0 Then    'FOR  UPDATE CURRENT YEAR AMOUNT AGAIN


                    drRow(i)("accrue_amount") = CDec(drRow(i)("accrue_amount")) + CDec(dtList.Rows(0)("remaining_bal"))
                    drRow(i)("uptolstyr_accrueamt") = CDec(dtList.Rows(0)("uptolstyr_accrueamt")) + CDec(drRow(i)("accrue_amount"))
                    drRow(i)("uptolstyr_issueamt") = CDec(dtList.Rows(0)("uptolstyr_issueamt"))
                    drRow(i)("LeaveBF") = CDec(dtList.Rows(0)("remaining_bal"))
                    drRow(i)("remaining_bal") = CDec(drRow(i)("accrue_amount"))

                Else
                    drRow(i)("uptolstyr_accrueamt") = CDec(drRow(i)("accrue_amount"))
                    drRow(i)("uptolstyr_issueamt") = 0
                    drRow(i)("LeaveBF") = 0
                    drRow(i)("remaining_bal") = CDec(drRow(i)("accrue_amount"))

                End If

                drRow(i)("issue_amount") = 0
                drRow(i)("balance") = 0
                drRow(i)("days") = 0


                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    drRow(i)("actualamount") = CDec(txtAccrualAmount.Text.Trim)

                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        drRow(i)("actualamount") = (txtAccrualAmount.Decimal) * CInt(DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date.AddDays(1)))

                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        drRow(i)("actualamount") = (txtAccrualAmount.Decimal) * CInt(DateDiff(DateInterval.Month, mdtStartdate.Date, mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1)))
                    End If
                End If

                'Pinkal (16-Dec-2016) -- End



                drRow(i)("isshortleave") = False

                'Pinkal (12-Jan-2016) -- Start
                'Enhancement - Changing Leave Adjustment to 0 WHEN User want to add new Leave Accrue.
                drRow(i)("adj_remaining_bal") = 0
                'Pinkal (12-Jan-2016) -- End

            Next

            'Pinkal (09-Dec-2020) -- Start
            'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
            dtTable.AcceptChanges()
            'Pinkal (09-Dec-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)

            'S.SANDEEP [21 JUL 2016] -- START -- START
            'dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid=1 AND isshortleave = 0", "", DataViewRowState.CurrentRows).ToTable
            dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid=1 AND isshortleave = 0 AND isaccrueamount = 1", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [21 JUL 2016] -- START -- END

            'START FOR ADD SELECT ROW'
            Dim drRow As DataRow
            drRow = dtFill.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 8, "Select")
            dtFill.Rows.InsertAt(drRow, 0)
            'END FOR ADD SELECT ROW'

            cboLeaveCode.ValueMember = "leavetypeunkid"
            cboLeaveCode.DisplayMember = "leavename"
            cboLeaveCode.DataSource = dtFill


            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                RemoveHandler cboELCLeaveType.SelectedValueChanged, AddressOf cboELCLeaveType_SelectedValueChanged
                cboELCLeaveType.ValueMember = "leavetypeunkid"
                cboELCLeaveType.DisplayMember = "leavename"
                cboELCLeaveType.DataSource = dtFill.Copy
                cboLeaveCode.Enabled = False
                objbtnSearchLeave2.Enabled = False
                AddHandler cboELCLeaveType.SelectedValueChanged, AddressOf cboELCLeaveType_SelectedValueChanged
            End If


            If mintLeavetypeunkid > 0 Then cboLeaveCode.SelectedValue = mintLeavetypeunkid

            Dim objMaster As New clsMasterData
            dsFill = objMaster.getGenderList("Gender", True)
            cboGender.DisplayMember = "Name"
            cboGender.ValueMember = "id"
            cboGender.DataSource = dsFill.Tables(0)
            dsFill = Nothing
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsFill = objMaster.GetCondition(True)
            dsFill = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            cboFromcondition.DisplayMember = "Name"
            cboFromcondition.ValueMember = "id"
            cboFromcondition.DataSource = dsFill.Tables(0)

            cboTocondition.DisplayMember = "Name"
            cboTocondition.ValueMember = "id"
            cboTocondition.DataSource = dsFill.Tables(0).Copy
            dsFill = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Pinkal (09-Dec-2020) -- Start
    'Bug  -  Worked on Login and Forgot Password Issue in Oryx.
    'Private Sub GetFilterEmployee()
    Private Sub GetFilterEmployee(ByVal dtTable As DataTable)
        Try

            'If mdtEmployee.Rows.Count > 0 Then
            If dtTable.Rows.Count > 0 Then
                Dim objIssue As New clsleaveissue_Tran

                'Dim dr() As DataRow = mdtEmployee.Select("ischecked = True")
                Dim dr() As DataRow = dtTable.Select("ischecked = True")

                If dr.Length > 0 Then
                    For i As Integer = 0 To dr.Length - 1

                        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            Dim objLeaveAccrue As New clsleavebalance_tran
                            Dim dsList As DataSet = objLeaveAccrue.GetEmployeeBalanceData(CInt(cboLeaveCode.SelectedValue).ToString(), CInt(dr(i)("employeeunkid")).ToString(), True, FinancialYear._Object._YearUnkid, True, True, False)
                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                If dtpDueDate.Value.Date > CDate(dsList.Tables(0).Rows(0)("enddate")) Then Continue For
                            End If

                        End If
                        Dim IssueDays As Decimal = objIssue.GetEmployeeIssueDaysCount(CInt(dr(i)("employeeunkid")), CInt(cboLeaveCode.SelectedValue), FinancialYear._Object._YearUnkid, dtpDueDate.Value.Date)

                        If IssueDays > 0 Then
                            dr(i).Delete()
                        Else
                            Continue For
                        End If
                    Next
                    'mdtEmployee.AcceptChanges()
                    dtTable.AcceptChanges()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterEmployee", mstrModuleName)
        End Try
    End Sub
    'Pinkal (09-Dec-2020) -- End


    Private Sub GetEmployee()
        'Pinkal (14-Dec-2017) -- Start
        'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
        Me.Cursor = Cursors.WaitCursor
        'Pinkal (14-Dec-2017) -- End
        Try
            Dim strSearching As String = ""
            Dim objEmployee As New clsEmployee_Master
            Dim objAccrue As New clsleavebalance_tran

            ' FOR REINSTATEMENT DATE ADDED ONE PARAMETER IN EMPLOYEE MASTER GETLIST METHOD

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If radAppointedDate.Checked = True Then
            '    dsEmployee = objEmployee.GetList("Employee", False, True, dtpDate1.Value.Date, dtpDate2.Value.Date, -1, "", False, "isapproved = 1 ")
            'Else
            '    dsEmployee = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), -1, "", False, "isapproved = 1 ", True)
            'End If

            Dim dtDate1, dtDate2 As Date : Dim blnReinstatement As Boolean = False
            dtDate1 = Nothing : dtDate2 = Nothing

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            'If radAppointedDate.Checked = True Then
            '    dtDate1 = dtpDate1.Value.Date
            '    dtDate2 = dtpDate2.Value.Date
            'Else
            '    dtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            '    dtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            '    blnReinstatement = True
            'End If

            If radAppointedDate.Checked = True OrElse radConfirmationDate.Checked = True OrElse radProbationDate.Checked = True Then
                dtDate1 = dtpDate1.Value.Date
                dtDate2 = dtpDate2.Value.Date
            Else
                dtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                blnReinstatement = True
            End If

            'Pinkal (30-Jun-2016) -- End

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                If rdNewEmployee.Checked Then

                    'Pinkal (22-Mar-2016) -- Start
                    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                    'Dim mstrEmployeId As String = objAccrue.GetNewEmployeeForELC(CInt(cboLeaveCode.SelectedValue), FinancialYear._Object._YearUnkid)
                    Dim mstrEmployeId As String = objAccrue.GetNewEmployeeForELC(CInt(cboLeaveCode.SelectedValue), FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, ConfigParameter._Object._EmployeeAsOnDate)
                    'Pinkal (22-Mar-2016) -- End

                    If mstrEmployeId.Trim.Length > 0 Then
                        strSearching = "AND hremployee_master.employeeunkid in (" & mstrEmployeId & ") "
                    Else
                        strSearching = "AND hremployee_master.employeeunkid = 0"
                    End If

                    'Pinkal (24-Aug-2015) -- End

                ElseIf rdDueDate.Checked Then
                    Dim mstrEmployeId As String = objAccrue.GetEmployeeForDueDateELC(dtpDueDate.Value.Date, CInt(cboLeaveCode.SelectedValue), FinancialYear._Object._YearUnkid)
                    If mstrEmployeId.Trim.Length > 0 Then
                        strSearching = "AND hremployee_master.employeeunkid in (" & mstrEmployeId & ") "
                    Else
                        strSearching = "AND hremployee_master.employeeunkid = 0"
                    End If

                End If
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                strSearching &= "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
            End If

            If radExpYear.Checked = True Then
                If nudFromYear.Value > 0 Or nudFromMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If nudToYear.Value > 0 Or nudToMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.Text & " '" & eZeeDate.convertDate(mdtDate) & "' "
                End If
            End If


            'Pinkal (22-Oct-2018) -- Start
            'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
            If radAppointedDate.Checked = True Then
                strSearching &= "AND Convert(char(8),hremployee_master.appointeddate,112) BETWEEN '" & eZeeDate.convertDate(dtDate1) & "' AND '" & eZeeDate.convertDate(dtDate2) & "'"
            End If
            'Pinkal (22-Oct-2018) -- End


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            If radProbationDate.Checked Then
                strSearching &= "AND Convert(char(8),EPROB.probation_from_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),EPROB.probation_to_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            If radConfirmationDate.Checked Then
                strSearching &= "AND Convert(char(8),ECNF.confirmation_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),ECNF.confirmation_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveAccrue_AddEdit))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            If radEmpWOAccrue.Checked Then
                Dim mstrSearch As String = ""


                'Pinkal (29-Jan-2019) -- Start
                'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
                'mstrSearch = objAccrue.GetEmployeeIDsWithoutAccrue(ConfigParameter._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                                            , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._LeaveBalanceSetting _
                '                                                                                , CInt(cboLeaveCode.SelectedValue), True, "")

                If strSearching.Trim.Length > 0 Then
                    mstrSearch = objAccrue.GetEmployeeIDsWithoutAccrue(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                              , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._LeaveBalanceSetting _
                                                                                                  , CInt(cboLeaveCode.SelectedValue), True, strSearching.Substring(3))
                Else
                    mstrSearch = objAccrue.GetEmployeeIDsWithoutAccrue(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                            , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, ConfigParameter._Object._LeaveBalanceSetting _
                                                                                                , CInt(cboLeaveCode.SelectedValue), True, "")
                End If
                'Pinkal (29-Jan-2019) -- End


                If mstrSearch.Trim.Length > 0 Then
                    strSearching &= " AND hremployee_master.employeeunkid in (" & mstrSearch & ")"
                End If

            End If

            'Pinkal (30-Jun-2016) -- End


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If


            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.

            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 dtDate1, _
            '                                 dtDate2, _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                 "Employee", _
            '                                 ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching, blnReinstatement)

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_Job



            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .

            'dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, _
            '                                  FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 dtDate1, _
            '                                 dtDate2, _
            '                                  ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , strSearching)


            'Pinkal (29-Jan-2019) -- Start
            'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.

            'dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, _
            '                                  FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 dtDate1, _
            '                                dtDate2, _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                mblnOnlyApproved, False, "List", , , strSearching, False, _
            '                                False, False, mblnAddApprovalCondition)

            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, _
                                              FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                                                                          ConfigParameter._Object._UserAccessModeSetting, _
                                                                                          mblnOnlyApproved, False, "List", , , strSearching, False, _
                                                                                          False, False, mblnAddApprovalCondition)

            'Pinkal (29-Jan-2019) -- End

            'Pinkal (14-Jun-2018) -- End



            'Pinkal (14-Dec-2017) -- End


            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                If dsEmployee.Tables(0).Columns.Contains("appliedleave") = False Then
                    dsEmployee.Tables(0).Columns.Add("appliedleave", Type.GetType("System.Decimal"))
                    dsEmployee.Tables(0).Columns("appliedleave").DefaultValue = 0
                End If

                If dsEmployee.Tables(0).Columns.Contains("approvedleave") = False Then
                    dsEmployee.Tables(0).Columns.Add("approvedleave", Type.GetType("System.Decimal"))
                    dsEmployee.Tables(0).Columns("approvedleave").DefaultValue = 0
                End If

                If dsEmployee.Tables(0).Columns.Contains("issuedleave") = False Then
                    dsEmployee.Tables(0).Columns.Add("issuedleave", Type.GetType("System.Decimal"))
                    dsEmployee.Tables(0).Columns("issuedleave").DefaultValue = 0
                End If

                If dsEmployee.Tables(0).Columns.Contains("remaining_bal") = False Then
                    dsEmployee.Tables(0).Columns.Add("remaining_bal", Type.GetType("System.Decimal"))
                    dsEmployee.Tables(0).Columns("remaining_bal").DefaultValue = 0
                End If
            End If


            If dsEmployee.Tables(0).Columns.Contains("startdate") = False Then
                dsEmployee.Tables(0).Columns.Add("startdate", Type.GetType("System.DateTime"))
            End If

            If dsEmployee.Tables(0).Columns.Contains("stopdate") = False Then
                dsEmployee.Tables(0).Columns.Add("stopdate", Type.GetType("System.DateTime"))
            End If

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            'If radAppointedDate.Checked = True Then
            '    LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            'Else
            'LblValue.Text = mdtToDate.Date & " - " & mdtDate.Date
            'End If

            If radAppointedDate.Checked = True OrElse radConfirmationDate.Checked = True OrElse radProbationDate.Checked = True Then
                LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            Else
                LblValue.Text = mdtToDate.Date & " - " & mdtDate.Date
            End If

            'Pinkal (30-Jun-2016) -- End

            mdtEmployee = dsEmployee.Tables(0)
            Dim objLvForm As New clsleaveform
            Dim objLvIssuetran As New clsleaveissue_Tran

            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.

            ' Dim objLvBalance As New clsleavebalance_tran
            Dim mintCompany As Integer = FinancialYear._Object._Companyunkid
            Dim mintLeaveBalanceSetting As Integer = ConfigParameter._Object._LeaveBalanceSetting
            Dim mstrPreviousDBName As String = ""
            Dim mdtPrevFYStartDate As Date = Nothing
            Dim mdtPrevFYEndDate As Date = Nothing


            Dim objCompany As New clsCompany_Master
            Dim dsDatabaseList As DataTable = objCompany.Get_DataBaseList("List", False, False, False)
            Dim drRow() As DataRow = dsDatabaseList.Select("GrpId = " & FinancialYear._Object._Companyunkid & " AND isclosed = 1", "end_date DESC")
            If drRow.Length > 0 Then
                mstrPreviousDBName = drRow(0)("Database").ToString().Trim()
                mdtPrevFYStartDate = CDate(drRow(0)("start_date")).Date
                mdtPrevFYEndDate = CDate(drRow(0)("end_date")).Date
                    End If


            If mdtEmployee.Columns.Contains("ischecked") = False Then
                mdtEmployee.Columns.Add("ischecked", Type.GetType("System.Boolean"))
                mdtEmployee.Columns("ischecked").DefaultValue = False
                End If


            'For Each dr As DataRow In mdtEmployee.Rows
            '    dr("appointeddate") = eZeeDate.convertDate(dr("appointeddate").ToString()).ToShortDateString

            '    Dim mdtDate As Date = Nothing
            '    If mdtEmployee.Columns.Contains("startdate") Then
            '        mdtDate = objAccrue.GetEmployeeDates(CInt(dr("employeeunkid")), CInt(cboLeaveCode.SelectedValue), True)
            '        If mdtDate.Date <> Nothing Then
            '            dr("startdate") = mdtDate
            '        End If
            '    End If

            '    mdtDate = Nothing
            '    If mdtEmployee.Columns.Contains("stopdate") Then
            '        mdtDate = objAccrue.GetEmployeeDates(CInt(dr("employeeunkid")), CInt(cboLeaveCode.SelectedValue), False)
            '        If mdtDate <> Nothing Then
            '            dr("stopdate") = mdtDate
            '        End If
            '    End If

            '    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '        If rdNewEmployee.Checked Then
            '            dr("appliedleave") = 0
            '            dr("approvedleave") = 0
            '            dr("issuedleave") = 0
            '            dr("remaining_bal") = 0
            '        Else
            '            dr("appliedleave") = objLvForm.GetEmployeeTotalAppliedDays(mintYearunkid, CInt(dr("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(dr("startdate")), CDate(dr("stopdate")) _
            '                                                                                                    , mintCompany, FinancialYear._Object._Database_Start_Date.Date, mintLeaveBalanceSetting, mstrPreviousDBName, mdtPrevFYStartDate _
            '                                                                                                    , mdtPrevFYEndDate)
            '            dr("approvedleave") = objLvForm.GetEmployeeTotalApprovedDays(mintYearunkid, CInt(dr("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(dr("startdate")), CDate(dr("stopdate")) _
            '                                                                                                    , mintCompany, FinancialYear._Object._Database_Start_Date.Date, mintLeaveBalanceSetting, mstrPreviousDBName, mdtPrevFYStartDate _
            '                                                                                                    , mdtPrevFYEndDate)
            '            dr("issuedleave") = objLvIssuetran.GetEmployeeIssueDaysCount(CInt(dr("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), mintYearunkid, Nothing, CDate(dr("startdate")), CDate(dr("stopdate")))
            '            dr("remaining_bal") = objLvBalance.GetEmpRemainingBalForDueDateELC(mintYearunkid, CInt(dr("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(dr("stopdate")))
            '        End If
            '    End If
            'Next


            mdtEmployee.AsEnumerable().ToList().ForEach(Function(x) UpdateRow(x, objAccrue, objLvForm, objLvIssuetran, mintCompany, mintLeaveBalanceSetting, mstrPreviousDBName, mdtPrevFYStartDate, mdtPrevFYEndDate))

            'objLvBalance = Nothing
            objLvIssuetran = Nothing
            objLvForm = Nothing

            'Pinkal (14-Dec-2017) -- End

            SetDataSource()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee", mstrModuleName)
        Finally
            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
            Me.Cursor = Cursors.Default
            'Pinkal (14-Dec-2017) -- End
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            dvEmployee = mdtEmployee.DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgEmployee.DataSource = dvEmployee
            objSelect.DataPropertyName = "ischecked"

            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.
            'dgColhEmpCode.DataPropertyName = "Code"
            dgColhEmpCode.DataPropertyName = Language.getMessage("clsEmployee_Master", 42, "Code")
            'Pinkal (28-Oct-2021) -- End


            dgColhEmployee.DataPropertyName = "Employee Name"
            dgColhAppointdate.DataPropertyName = "Appointed Date"
            dgcolhJobTitle.DataPropertyName = "Job"

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If

            dgcolhAppliedLeave.DataPropertyName = "appliedleave"
            dgcolhApprovedLeave.DataPropertyName = "approvedleave"
            dgcolhIssuedLeave.DataPropertyName = "issuedleave"
            dgcolhRemaining_Bal.DataPropertyName = "remaining_bal"
            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 13, "Employee Count : ") & dvEmployee.Count

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    'Pinkal (14-Dec-2017) -- Start
    'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.

    Private Function UpdateRow(ByVal drRow As DataRow, ByVal objLvBalance As clsleavebalance_tran, ByVal objLvForm As clsleaveform, ByVal objLvIssuetran As clsleaveissue_Tran _
                                            , ByVal mintCompany As Integer, ByVal mintLeaveBalanceSetting As Integer, ByVal mstrPreviousDBName As String, ByVal mdtPrevFYStartDate As Date _
                                            , ByVal mdtPrevFYEndDate As Date) As Boolean
        Try

            drRow("ischecked") = False
            drRow("Appointed Date") = eZeeDate.convertDate(drRow("Appointed Date").ToString()).ToShortDateString()

            Dim mdtDate As Date = Nothing
            If mdtEmployee.Columns.Contains("startdate") Then
                mdtDate = objLvBalance.GetEmployeeDates(CInt(drRow("employeeunkid")), CInt(cboLeaveCode.SelectedValue), True)
                If mdtDate.Date <> Nothing Then
                    drRow("startdate") = mdtDate
                End If
            End If

            mdtDate = Nothing
            If mdtEmployee.Columns.Contains("stopdate") Then
                mdtDate = objLvBalance.GetEmployeeDates(CInt(drRow("employeeunkid")), CInt(cboLeaveCode.SelectedValue), False)
                If mdtDate <> Nothing Then
                    drRow("stopdate") = mdtDate
                End If
            End If

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                If rdNewEmployee.Checked Then
                    drRow("appliedleave") = 0
                    drRow("approvedleave") = 0
                    drRow("issuedleave") = 0
                    drRow("remaining_bal") = 0
                Else
                    drRow("appliedleave") = objLvForm.GetEmployeeTotalAppliedDays(mintYearunkid, CInt(drRow("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(drRow("startdate")), CDate(drRow("stopdate")) _
                                                                                                            , mintCompany, FinancialYear._Object._Database_Start_Date.Date, mintLeaveBalanceSetting, mstrPreviousDBName, mdtPrevFYStartDate _
                                                                                                            , mdtPrevFYEndDate)
                    drRow("approvedleave") = objLvForm.GetEmployeeTotalApprovedDays(mintYearunkid, CInt(drRow("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(drRow("startdate")), CDate(drRow("stopdate")) _
                                                                                                            , mintCompany, FinancialYear._Object._Database_Start_Date.Date, mintLeaveBalanceSetting, mstrPreviousDBName, mdtPrevFYStartDate _
                                                                                                            , mdtPrevFYEndDate)
                    drRow("issuedleave") = objLvIssuetran.GetEmployeeIssueDaysCount(CInt(drRow("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), mintYearunkid, Nothing, CDate(drRow("startdate")), CDate(drRow("stopdate")))
                    drRow("remaining_bal") = objLvBalance.GetEmpRemainingBalForDueDateELC(mintYearunkid, CInt(drRow("employeeunkid")), CInt(cboELCLeaveType.SelectedValue), CDate(drRow("stopdate")))
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRow", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (14-Dec-2017) -- End

#End Region

#Region "LinkButton Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            TxtSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objSelect.Index Then

                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtEmployee.AcceptChanges()
                End If

                SetCheckBoxValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub TxtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then

                'Pinkal (22-Oct-2018) -- Start
                'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
                ' dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR name like '%" & TxtSearch.Text.Trim & "%'"
                dvEmployee.RowFilter = "[" & Language.getMessage("clsEmployee_Master", 42, "Code") & "] like '%" & TxtSearch.Text.Trim & "%'  OR [" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "] like '%" & TxtSearch.Text.Trim & "%'"
                'Pinkal (22-Oct-2018) -- End

                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TxtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "NumericUpdown Event"

    Private Sub nudFromYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudFromYear.ValueChanged, nudFromMonth.Validated, nudFromMonth.ValueChanged, nudFromMonth.Validated
        Try
            mdtDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudFromYear.Value) * -1).AddMonths(CInt(nudFromMonth.Value) * -1)
            nudToYear.Minimum = nudFromYear.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudFromYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudToYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudToYear.ValueChanged, nudToYear.Validated, nudToMonth.ValueChanged, nudToMonth.Validated
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudToYear.Value) * -1).AddMonths(CInt(nudToMonth.Value) * -1)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudToYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "RadioButton Event"

    Private Sub rdDueDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDueDate.CheckedChanged
        Try
            If rdDueDate.Checked Then
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                rdNewEmployee.Checked = False
                dtpDueDate.Enabled = True
                If cboELCLeaveType.SelectedIndex >= 0 Then
                    cboELCLeaveType.SelectedIndex = 0
                    cboELCLeaveType.Enabled = True
                    cboLeaveCode.SelectedIndex = 0
                    cboLeaveCode.Enabled = False
                    objbtnSearchLeave2.Enabled = False
                    objbtnSearchLeave1.Enabled = True
                    dgcolhAppliedLeave.Visible = True
                    dgcolhApprovedLeave.Visible = True
                    dgcolhIssuedLeave.Visible = True
                    dgcolhRemaining_Bal.Visible = True
                End If
                setColor()
                If mdtEmployee IsNot Nothing Then
                    mdtEmployee.Rows.Clear()
                    LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 13, "Employee Count : ") & mdtEmployee.Rows.Count
                End If
                cboELCLeaveType.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdDueDate_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub rdNewEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdNewEmployee.CheckedChanged
        Try
            If rdNewEmployee.Checked Then
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                rdDueDate.Checked = False
                dtpDueDate.Enabled = False
                cboELCLeaveType.SelectedIndex = 0
                cboELCLeaveType.Enabled = False
                cboLeaveCode.SelectedIndex = 0
                cboLeaveCode.Enabled = True
                objbtnSearchLeave2.Enabled = True
                objbtnSearchLeave1.Enabled = False
                dgcolhAppliedLeave.Visible = False
                dgcolhApprovedLeave.Visible = False
                dgcolhIssuedLeave.Visible = False
                dgcolhRemaining_Bal.Visible = False
                setColor()
                If mdtEmployee IsNot Nothing Then
                    mdtEmployee.Rows.Clear()
                    LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 13, "Employee Count : ") & mdtEmployee.Rows.Count
                End If
                cboLeaveCode.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdNewEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radAppointedDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAppointedDate.CheckedChanged _
                                                                                                                                                                                        , radExpYear.CheckedChanged, radProbationDate.CheckedChanged _
                                                                                                                                                                                        , radConfirmationDate.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radAppointedDate.Name.ToUpper, radConfirmationDate.Name.ToUpper, radProbationDate.Name.ToUpper
                    If radAppointedDate.Checked = True OrElse radConfirmationDate.Checked = True OrElse radProbationDate.Checked = True Then
                        pnlAppDate.Enabled = True : pnlYear.Enabled = False
                        'Pinkal (29-Jan-2019) -- Start
                        'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
                        nudFromYear.Value = 0
                        nudFromMonth.Value = 0
                        cboFromcondition.SelectedValue = 0
                        nudToYear.Value = 0
                        nudToMonth.Value = 0
                        cboTocondition.SelectedValue = 0
                        'Pinkal (29-Jan-2019) -- End
                    End If
                Case radExpYear.Name.ToUpper
                    If radExpYear.Checked = True Then
                        pnlAppDate.Enabled = False : pnlYear.Enabled = True
                        'Pinkal (29-Jan-2019) -- Start
                        'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
                        dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        'Pinkal (29-Jan-2019) -- End
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAppointedDate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoAction.CheckedChanged
        Try
            If chkNoAction.Checked Then
                txtCfAmount.Enabled = False
                txtCfAmount.Decimal = 0
            Else
                txtCfAmount.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoAction_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private Sub radIssuebal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radIssuebal.CheckedChanged, radExceedBal.CheckedChanged _
                                                                                                                                                                            , radDonotIssue.CheckedChanged, radDonotIssueAsonDate.CheckedChanged

        Try

            If CType(sender, RadioButton).Name = radIssuebal.Name OrElse CType(sender, RadioButton).Name = radExceedBal.Name Then
                lblMaxNegativeLimit.Visible = True
                nudMaxNegativeDays.Visible = True
                nudMaxNegativeDays.Value = 0
            ElseIf CType(sender, RadioButton).Name = radDonotIssue.Name OrElse CType(sender, RadioButton).Name = radDonotIssueAsonDate.Name Then
                lblMaxNegativeLimit.Visible = False
                nudMaxNegativeDays.Visible = False
                nudMaxNegativeDays.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radIssuebal_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (08-Oct-2018) -- End


#End Region

#Region "Dropdown Event"

    Private Sub cboELCLeaveType_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboELCLeaveType.SelectedValueChanged
        Try
            If rdDueDate.Checked AndAlso cboELCLeaveType.SelectedValue IsNot Nothing Then
                cboLeaveCode.SelectedValue = cboELCLeaveType.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboELCLeaveType_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub


#End Region

    'Private Sub SetListOperation(ByVal blnOperation As Boolean)
    '    Try
    '        For Each Item As ListViewItem In lstList.Items
    '            RemoveHandler lstList.ItemChecked, AddressOf lstList_ItemChecked
    '            Item.Checked = blnOperation
    '            AddHandler lstList.ItemChecked, AddressOf lstList_ItemChecked
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetListOperation", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub SetGroupOperation(ByVal blnOperation As Boolean)
    '    Try
    '        For Each Item As ListViewItem In lstGroup.Items
    '            Item.Checked = blnOperation
    '        Next
    '        ControlVisibility()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetGroupOperation", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ControlVisibility()
    '    Try
    '        If lstGroup.Items.Count > 0 Then
    '            chkSelectallGroup.Enabled = True
    '        Else
    '            chkSelectallGroup.Checked = False
    '            chkSelectallGroup.Enabled = False
    '        End If

    '        If lstList.Items.Count > 0 Then
    '            chkSelectallList.Enabled = True
    '        Else
    '            chkSelectallList.Checked = False
    '            chkSelectallList.Enabled = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ControlVisibility", mstrModuleName)
    '    End Try
    'End Sub


    '#Region "Radio Button's Event"

    '    Private Sub rabEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstList.Items.Clear()
    '            If rabEmployee.Checked Then
    '                Dim objEmployee As New clsEmployee_Master


    '                'Pinkal (05-Mar-2012) -- Start
    '                'Enhancement : TRA Changes
    '                Dim dsFill As DataSet = objEmployee.GetEmployeeList("Employee", False, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , CInt(cboGender.SelectedValue))
    '                'Pinkal (05-Mar-2012) -- End

    '                If CInt(cboLeaveCode.SelectedValue) > 0 Then GetFilterEmployee(dsFill)
    '                If dsFill.Tables("Employee").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstList.Items.Clear()

    '                    RemoveHandler lstList.ItemChecked, AddressOf lstList_ItemChecked

    '                    For i As Integer = 0 To dsFill.Tables("Employee").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("Employee").Rows(i)("employeename").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("Employee").Rows(i)("employeeunkid"))
    '                        lstList.Items.Add(lvItem)
    '                    Next
    '                    AddHandler lstList.ItemChecked, AddressOf lstList_ItemChecked
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabEmployee_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabGrade.Checked Then
    '                Dim objGrade As New clsGradeGroup
    '                Dim dsFill As DataSet = objGrade.GetList("Grade", True)
    '                If dsFill.Tables("Grade").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("Grade").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("Grade").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("Grade").Rows(i)("gradegroupunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabGrade_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabSection_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstList.Items.Clear()
    '            If rabSection.Checked Then
    '                Dim objSection As New clsSections
    '                Dim dsFill As DataSet = objSection.GetList("Section", True)
    '                If dsFill.Tables("Section").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("Section").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("Section").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("Section").Rows(i)("sectionunkid"))
    '                        lstList.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabSection_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabCostCenter.Checked Then
    '                Dim objCostcenter As New clspayrollgroup_master
    '                Dim dsFill As DataSet = objCostcenter.GetList("CostCenter", True)
    '                Dim dtfill As DataTable = New DataView(dsFill.Tables("CostCenter"), "grouptype_id=" & PayrollGroupType.CostCenter, "", DataViewRowState.CurrentRows).ToTable
    '                If dsFill.Tables("CostCenter").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dtfill.Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dtfill.Rows(i)("groupname").ToString
    '                        lvItem.Tag = CInt(dtfill.Rows(i)("groupmasterunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabCostCenter_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabPayPoint_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstList.Items.Clear()
    '            If rabPayPoint.Checked Then
    '                Dim objPaypoint As New clspaypoint_master
    '                Dim dsFill As DataSet = objPaypoint.GetList("PayPoint", True)
    '                If dsFill.Tables("PayPoint").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("PayPoint").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("PayPoint").Rows(i)("paypointname").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("PayPoint").Rows(i)("paypointunkid"))
    '                        lstList.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabSection_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabDeptGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabDeptGroup.Checked Then
    '                Dim objDeptGroup As New clsDepartmentGroup
    '                Dim dsFill As DataSet = objDeptGroup.GetList("DeptGroup", True)
    '                If dsFill.Tables("DeptGroup").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("DeptGroup").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("DeptGroup").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("DeptGroup").Rows(i)("deptgroupunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabDeptGroup_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabSectionDept_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabSectionDept.Checked Then
    '                Dim objDepartment As New clsDepartment
    '                Dim dsFill As DataSet = objDepartment.GetList("Department", True)
    '                If dsFill.Tables("Department").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("Department").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("Department").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("Department").Rows(i)("departmentunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabSectionDept_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabJobnJobGrp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabJobnJobGrp.Checked Then
    '                Dim objJobGroup As New clsJobGroup
    '                Dim dsFill As DataSet = objJobGroup.GetList("JobGroup", True)
    '                If dsFill.Tables("JobGroup").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("JobGroup").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("JobGroup").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("JobGroup").Rows(i)("jobgroupunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabJobnJobGrp_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub rabClassnClassGrp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        Try
    '            lstGroup.Items.Clear()
    '            If rabClassnClassGrp.Checked Then
    '                Dim objClassGroup As New clsClassGroup
    '                Dim dsFill As DataSet = objClassGroup.GetList("ClassGroup", True)
    '                If dsFill.Tables("ClassGroup").Rows.Count > 0 Then
    '                    Dim lvItem As ListViewItem
    '                    lstGroup.Items.Clear()
    '                    For i As Integer = 0 To dsFill.Tables("ClassGroup").Rows.Count - 1
    '                        lvItem = New ListViewItem
    '                        lvItem.Text = dsFill.Tables("ClassGroup").Rows(i)("name").ToString
    '                        lvItem.Tag = CInt(dsFill.Tables("ClassGroup").Rows(i)("classgroupunkid"))
    '                        lstGroup.Items.Add(lvItem)
    '                    Next
    '                End If
    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "rabClassnClassGrp_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region



    '#Region "ListView's Event"


    '    'Pinkal (24-Jun-2011) -- Start
    '    'ISSUE : CHECK FOR ACTIVE EMPLOYEE

    '    Private Sub lstGroup_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Dim dtFill As DataTable = Nothing
    '        Dim strGroupId As String = String.Empty
    '        Try

    '            If e.Item.Tag Is Nothing Or e.Item.Tag.ToString() = "" Then Exit Sub

    '            RemoveHandler chkSelectallGroup.CheckedChanged, AddressOf chkSelectallGroup_CheckedChanged
    '            If lstGroup.CheckedItems.Count <= 0 Then
    '                chkSelectallGroup.CheckState = CheckState.Unchecked

    '            ElseIf lstGroup.CheckedItems.Count < lstGroup.Items.Count Then
    '                chkSelectallGroup.CheckState = CheckState.Indeterminate

    '            ElseIf lstGroup.CheckedItems.Count = lstGroup.Items.Count Then
    '                chkSelectallGroup.CheckState = CheckState.Checked

    '            End If
    '            AddHandler chkSelectallGroup.CheckedChanged, AddressOf chkSelectallGroup_CheckedChanged


    '            If lstGroup.CheckedItems.Count = 0 Then
    '                lstList.Items.Clear()
    '                ControlVisibility()
    '                Exit Sub
    '            End If


    '            For i As Integer = 0 To lstGroup.CheckedItems.Count - 1
    '                strGroupId &= "'" & lstGroup.CheckedItems(i).Tag.ToString & "',"
    '            Next
    '            strGroupId = strGroupId.Substring(0, strGroupId.Length - 1)


    '            'FOR GRADE GROUP
    '            If rabGrade.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objGrade As New clsGrade
    '                    'dsFill = objGrade.GetList("Grade", True)
    '                    If dsGrade Is Nothing Then
    '                        dsGrade = objGrade.GetList("Grade", True)
    '                    End If

    '                    dtFill = New DataView(dsGrade.Tables("Grade"), "gradegroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("name").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("gradeunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next

    '                    End If
    '                End If

    '                'FOR COST CENTER GROUP
    '            ElseIf rabCostCenter.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objCostCenter As New clscostcenter_master
    '                    chkSelectallList.Enabled = True
    '                    'dsFill = objCostCenter.GetList("CostCenter", True)
    '                    If dsCostCenter Is Nothing Then
    '                        dsCostCenter = objCostCenter.GetList("CostCenter", True)
    '                    End If

    '                    dtFill = New DataView(dsCostCenter.Tables("CostCenter"), "costcentergroupmasterunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("costcentername").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("costcenterunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next

    '                    End If
    '                Else
    '                    chkSelectallList.Enabled = False
    '                End If

    '                'FOR DEPARTMENT GROUP
    '            ElseIf rabDeptGroup.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objDepartment As New clsDepartment

    '                    ' dsFill = objDepartment.GetList("Department", True)
    '                    If dsDepartment Is Nothing Then
    '                        dsDepartment = objDepartment.GetList("Department", True)
    '                    End If

    '                    dtFill = New DataView(dsDepartment.Tables("Department"), "deptgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("name").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("departmentunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next

    '                    End If

    '                End If

    '                'FOR SECTION IN DEPARTMENT GROUP
    '            ElseIf rabSectionDept.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objSection As New clsSections

    '                    ' dsFill = objSection.GetList("Section", True)
    '                    If dsSectionDg Is Nothing Then
    '                        dsSectionDg = objSection.GetList("Section", True)
    '                    End If

    '                    dtFill = New DataView(dsSectionDg.Tables("Section"), "departmentunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("name").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("sectionunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next

    '                    End If

    '                End If

    '                'FOR JOB IN JOB GROUP
    '            ElseIf rabJobnJobGrp.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objJob As New clsJobs

    '                    '    dsFill = objJob.GetList("Job", True)
    '                    If dsJob Is Nothing Then
    '                        dsJob = objJob.GetList("Job", True)
    '                    End If

    '                    dtFill = New DataView(dsJob.Tables("Job"), "jobgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("JobName").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("jobunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next
    '                    End If

    '                End If

    '                'FOR CLASS IN CLASS GROUP
    '            ElseIf rabClassnClassGrp.Checked Then
    '                If strGroupId.Length > 0 Then
    '                    Dim objClass As New clsClass
    '                    'dsFill = objClass.GetList("Class", True)

    '                    If dsClass Is Nothing Then
    '                        dsClass = objClass.GetList("Class", True)
    '                    End If

    '                    dtFill = New DataView(dsClass.Tables("Class"), "classgroupunkid in (" & strGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    If dtFill.Rows.Count > 0 Then
    '                        Dim lvItem As ListViewItem
    '                        lstList.Items.Clear()
    '                        For i As Integer = 0 To dtFill.Rows.Count - 1
    '                            lvItem = New ListViewItem
    '                            lvItem.Text = dtFill.Rows(i)("name").ToString
    '                            lvItem.Tag = CInt(dtFill.Rows(i)("classesunkid"))
    '                            lstList.Items.Add(lvItem)
    '                        Next
    '                    End If

    '                End If

    '            End If
    '            ControlVisibility()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lstGroup_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Pinkal (24-Jun-2011) -- End

    '    Private Sub lstList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Try
    '            RemoveHandler chkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
    '            If lstList.CheckedItems.Count <= 0 Then
    '                chkSelectallList.CheckState = CheckState.Unchecked

    '            ElseIf lstList.CheckedItems.Count < lstList.Items.Count Then
    '                chkSelectallList.CheckState = CheckState.Indeterminate

    '            ElseIf lstList.CheckedItems.Count = lstList.Items.Count Then
    '                chkSelectallList.CheckState = CheckState.Checked

    '            End If
    '            AddHandler chkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lstList_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region










    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbLeaveInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbLeaveInfo.Text = Language._Object.getCaption(Me.gbLeaveInfo.Name, Me.gbLeaveInfo.Text)
            Me.lblLeaveCode.Text = Language._Object.getCaption(Me.lblLeaveCode.Name, Me.lblLeaveCode.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.lblAccrualDays.Text = Language._Object.getCaption(Me.lblAccrualDays.Name, Me.lblAccrualDays.Text)
            Me.lblBatchNo.Text = Language._Object.getCaption(Me.lblBatchNo.Name, Me.lblBatchNo.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.radExceedBal.Text = Language._Object.getCaption(Me.radExceedBal.Name, Me.radExceedBal.Text)
            Me.radIssuebal.Text = Language._Object.getCaption(Me.radIssuebal.Name, Me.radIssuebal.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.LblValue.Text = Language._Object.getCaption(Me.LblValue.Name, Me.LblValue.Text)
            Me.LblFrom.Text = Language._Object.getCaption(Me.LblFrom.Name, Me.LblFrom.Text)
            Me.LblMonth.Text = Language._Object.getCaption(Me.LblMonth.Name, Me.LblMonth.Text)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.LblToMonth.Text = Language._Object.getCaption(Me.LblToMonth.Name, Me.LblToMonth.Text)
            Me.LblToYear.Text = Language._Object.getCaption(Me.LblToYear.Name, Me.LblToYear.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.rdNewEmployee.Text = Language._Object.getCaption(Me.rdNewEmployee.Name, Me.rdNewEmployee.Text)
            Me.LblDueDate.Text = Language._Object.getCaption(Me.LblDueDate.Name, Me.LblDueDate.Text)
            Me.rdDueDate.Text = Language._Object.getCaption(Me.rdDueDate.Name, Me.rdDueDate.Text)
            Me.LblELCLeaveType.Text = Language._Object.getCaption(Me.LblELCLeaveType.Name, Me.LblELCLeaveType.Text)
            Me.eline1.Text = Language._Object.getCaption(Me.eline1.Name, Me.eline1.Text)
            Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.Name, Me.radExpYear.Text)
            Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.Name, Me.radAppointedDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.chkNoAction.Text = Language._Object.getCaption(Me.chkNoAction.Name, Me.chkNoAction.Text)
            Me.LblEligibilityAfter.Text = Language._Object.getCaption(Me.LblEligibilityAfter.Name, Me.LblEligibilityAfter.Text)
            Me.LblCFAmount.Text = Language._Object.getCaption(Me.LblCFAmount.Name, Me.LblCFAmount.Text)
            Me.radDonotIssue.Text = Language._Object.getCaption(Me.radDonotIssue.Name, Me.radDonotIssue.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblAccrueSetting.Text = Language._Object.getCaption(Me.lblAccrueSetting.Name, Me.lblAccrueSetting.Text)
            Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
            Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
            Me.dgColhAppointdate.HeaderText = Language._Object.getCaption(Me.dgColhAppointdate.Name, Me.dgColhAppointdate.HeaderText)
            Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)
            Me.dgcolhAppliedLeave.HeaderText = Language._Object.getCaption(Me.dgcolhAppliedLeave.Name, Me.dgcolhAppliedLeave.HeaderText)
            Me.dgcolhApprovedLeave.HeaderText = Language._Object.getCaption(Me.dgcolhApprovedLeave.Name, Me.dgcolhApprovedLeave.HeaderText)
            Me.dgcolhIssuedLeave.HeaderText = Language._Object.getCaption(Me.dgcolhIssuedLeave.Name, Me.dgcolhIssuedLeave.HeaderText)
            Me.dgcolhRemaining_Bal.HeaderText = Language._Object.getCaption(Me.dgcolhRemaining_Bal.Name, Me.dgcolhRemaining_Bal.HeaderText)
            Me.LblEmployeeCount.Text = Language._Object.getCaption(Me.LblEmployeeCount.Name, Me.LblEmployeeCount.Text)
            Me.radProbationDate.Text = Language._Object.getCaption(Me.radProbationDate.Name, Me.radProbationDate.Text)
            Me.radConfirmationDate.Text = Language._Object.getCaption(Me.radConfirmationDate.Name, Me.radConfirmationDate.Text)
            Me.radShowAll.Text = Language._Object.getCaption(Me.radShowAll.Name, Me.radShowAll.Text)
            Me.radEmpWOAccrue.Text = Language._Object.getCaption(Me.radEmpWOAccrue.Name, Me.radEmpWOAccrue.Text)
            Me.LblMonthlyAccrue.Text = Language._Object.getCaption(Me.LblMonthlyAccrue.Name, Me.LblMonthlyAccrue.Text)
            Me.radDonotIssueAsonDate.Text = Language._Object.getCaption(Me.radDonotIssueAsonDate.Name, Me.radDonotIssueAsonDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no employee to Accrue Leave.")
            Language.setMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName, 3, "Batch No cannot be blank. Batch No is required information.")
            Language.setMessage(mstrModuleName, 4, "Days cannot be blank. Days is required information.")
            Language.setMessage(mstrModuleName, 5, "Employee is compulsory information.Please check atleast one employee.")
            Language.setMessage(mstrModuleName, 6, "From Condition is compulsory information.Please Select From Condition.")
            Language.setMessage(mstrModuleName, 7, "To Condition is compulsory information.Please Select To Condition.")
            Language.setMessage(mstrModuleName, 8, "Select")
            Language.setMessage(mstrModuleName, 9, "Sorry, To date cannot be less than From date")
            Language.setMessage(mstrModuleName, 10, "You have set CF amount or leave eligibility days or No action as checked. This will be applicable to all selected employee(s).")
            Language.setMessage(mstrModuleName, 11, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 12, "You have not set CF amount or leave eligibility days. 0 will be set to all selected employee(s).")
            Language.setMessage(mstrModuleName, 13, "Employee Count :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class