﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprover_leavetypeMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApprover_leavetypeMapping))
        Me.gbLeavedetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblApprover = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objlblApproverLevel = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlLeaveType = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgLeaveType = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhPaidLeave = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objApproverFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLeaveCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLeaveType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLeaveTypeMappingunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeavedetail.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlLeaveType.SuspendLayout()
        CType(Me.dgLeaveType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objApproverFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbLeavedetail
        '
        Me.gbLeavedetail.BorderColor = System.Drawing.Color.Black
        Me.gbLeavedetail.Checked = False
        Me.gbLeavedetail.CollapseAllExceptThis = False
        Me.gbLeavedetail.CollapsedHoverImage = Nothing
        Me.gbLeavedetail.CollapsedNormalImage = Nothing
        Me.gbLeavedetail.CollapsedPressedImage = Nothing
        Me.gbLeavedetail.CollapseOnLoad = False
        Me.gbLeavedetail.Controls.Add(Me.objlblApprover)
        Me.gbLeavedetail.Controls.Add(Me.lblApprover)
        Me.gbLeavedetail.Controls.Add(Me.objlblApproverLevel)
        Me.gbLeavedetail.Controls.Add(Me.lblApproverLevel)
        Me.gbLeavedetail.ExpandedHoverImage = Nothing
        Me.gbLeavedetail.ExpandedNormalImage = Nothing
        Me.gbLeavedetail.ExpandedPressedImage = Nothing
        Me.gbLeavedetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavedetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavedetail.HeaderHeight = 25
        Me.gbLeavedetail.HeaderMessage = ""
        Me.gbLeavedetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavedetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavedetail.HeightOnCollapse = 0
        Me.gbLeavedetail.LeftTextSpace = 0
        Me.gbLeavedetail.Location = New System.Drawing.Point(10, 4)
        Me.gbLeavedetail.Name = "gbLeavedetail"
        Me.gbLeavedetail.OpenHeight = 300
        Me.gbLeavedetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavedetail.ShowBorder = True
        Me.gbLeavedetail.ShowCheckBox = False
        Me.gbLeavedetail.ShowCollapseButton = False
        Me.gbLeavedetail.ShowDefaultBorderColor = True
        Me.gbLeavedetail.ShowDownButton = False
        Me.gbLeavedetail.ShowHeader = True
        Me.gbLeavedetail.Size = New System.Drawing.Size(385, 88)
        Me.gbLeavedetail.TabIndex = 8
        Me.gbLeavedetail.Temp = 0
        Me.gbLeavedetail.Text = "Approver Detail"
        Me.gbLeavedetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApprover
        '
        Me.objlblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApprover.Location = New System.Drawing.Point(119, 37)
        Me.objlblApprover.Name = "objlblApprover"
        Me.objlblApprover.Size = New System.Drawing.Size(246, 14)
        Me.objlblApprover.TabIndex = 282
        Me.objlblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 37)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(104, 14)
        Me.lblApprover.TabIndex = 281
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApproverLevel
        '
        Me.objlblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApproverLevel.Location = New System.Drawing.Point(119, 63)
        Me.objlblApproverLevel.Name = "objlblApproverLevel"
        Me.objlblApproverLevel.Size = New System.Drawing.Size(246, 14)
        Me.objlblApproverLevel.TabIndex = 280
        Me.objlblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(8, 63)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(104, 14)
        Me.lblApproverLevel.TabIndex = 279
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 278)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(406, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(193, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(296, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlLeaveType
        '
        Me.pnlLeaveType.Controls.Add(Me.chkSelectAll)
        Me.pnlLeaveType.Controls.Add(Me.dgLeaveType)
        Me.pnlLeaveType.Location = New System.Drawing.Point(10, 95)
        Me.pnlLeaveType.Name = "pnlLeaveType"
        Me.pnlLeaveType.Size = New System.Drawing.Size(385, 177)
        Me.pnlLeaveType.TabIndex = 10
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 14)
        Me.chkSelectAll.TabIndex = 284
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgLeaveType
        '
        Me.dgLeaveType.AllowUserToAddRows = False
        Me.dgLeaveType.AllowUserToDeleteRows = False
        Me.dgLeaveType.AllowUserToResizeRows = False
        Me.dgLeaveType.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgLeaveType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLeaveType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhLeaveCode, Me.dgcolhLeaveType, Me.dgcolhPaidLeave, Me.objdgcolhLeaveTypeMappingunkid})
        Me.dgLeaveType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgLeaveType.Location = New System.Drawing.Point(0, 0)
        Me.dgLeaveType.Name = "dgLeaveType"
        Me.dgLeaveType.RowHeadersVisible = False
        Me.dgLeaveType.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgLeaveType.Size = New System.Drawing.Size(385, 177)
        Me.dgLeaveType.TabIndex = 0
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhPaidLeave
        '
        Me.dgcolhPaidLeave.HeaderText = "IsPaid"
        Me.dgcolhPaidLeave.Name = "dgcolhPaidLeave"
        Me.dgcolhPaidLeave.ReadOnly = True
        Me.dgcolhPaidLeave.Width = 50
        '
        'objApproverFooter
        '
        Me.objApproverFooter.BorderColor = System.Drawing.Color.Silver
        Me.objApproverFooter.Controls.Add(Me.btnOk)
        Me.objApproverFooter.Controls.Add(Me.btnCancel)
        Me.objApproverFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objApproverFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objApproverFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objApproverFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objApproverFooter.Location = New System.Drawing.Point(0, 223)
        Me.objApproverFooter.Name = "objApproverFooter"
        Me.objApproverFooter.Size = New System.Drawing.Size(406, 55)
        Me.objApproverFooter.TabIndex = 10
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(193, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(296, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Leave Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Leave Type"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = ""
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'dgcolhLeaveCode
        '
        Me.dgcolhLeaveCode.HeaderText = "Leave Code"
        Me.dgcolhLeaveCode.Name = "dgcolhLeaveCode"
        Me.dgcolhLeaveCode.ReadOnly = True
        Me.dgcolhLeaveCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhLeaveType
        '
        Me.dgcolhLeaveType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhLeaveType.HeaderText = "Leave Type"
        Me.dgcolhLeaveType.Name = "dgcolhLeaveType"
        Me.dgcolhLeaveType.ReadOnly = True
        Me.dgcolhLeaveType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhLeaveTypeMappingunkid
        '
        Me.objdgcolhLeaveTypeMappingunkid.HeaderText = ""
        Me.objdgcolhLeaveTypeMappingunkid.Name = "objdgcolhLeaveTypeMappingunkid"
        Me.objdgcolhLeaveTypeMappingunkid.ReadOnly = True
        Me.objdgcolhLeaveTypeMappingunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhLeaveTypeMappingunkid.Visible = False
        '
        'frmApprover_leavetypeMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 333)
        Me.Controls.Add(Me.pnlLeaveType)
        Me.Controls.Add(Me.objApproverFooter)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbLeavedetail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprover_leavetypeMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Type Mapping "
        Me.gbLeavedetail.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.pnlLeaveType.ResumeLayout(False)
        CType(Me.dgLeaveType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objApproverFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbLeavedetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblApprover As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objlblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnlLeaveType As System.Windows.Forms.Panel
    Friend WithEvents dgLeaveType As System.Windows.Forms.DataGridView
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhLeaveCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLeaveType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPaidLeave As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhLeaveTypeMappingunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objApproverFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
End Class
