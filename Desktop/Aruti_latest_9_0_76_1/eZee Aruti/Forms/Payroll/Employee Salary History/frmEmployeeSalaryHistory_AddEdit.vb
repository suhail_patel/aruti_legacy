﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.Web

#End Region

Public Class frmEmployeeSalaryHistory_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeSalaryHistory_AddEdit"
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mdtAppointmentDate As Date = Nothing
    Private objESalaryHistory As clsempsalary_history_tran
    Private mdtFirstOpenPeriodDate As Date = Nothing

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objGradeGrp As New clsGradeGroup
        Dim objCommon As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            Dim StrFilter As String = String.Empty
            StrFilter = " CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= '" & eZeeDate.convertDate(mdtFirstOpenPeriodDate) & "' "
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , StrFilter)

            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged
            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
                .EndUpdate()
            End With
            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged

            RemoveHandler cboChangeType.SelectedIndexChanged, AddressOf cboChangeType_SelectedIndexChanged
            dsCombos = objMaster.getComboListForSalaryChangeType("ChangeType")
            With cboChangeType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("ChangeType")
                .SelectedValue = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION
            End With
            AddHandler cboChangeType.SelectedIndexChanged, AddressOf cboChangeType_SelectedIndexChanged

            RemoveHandler cboGradeGroup.SelectedIndexChanged, AddressOf cboGradeGroup_SelectedIndexChanged
            dsCombos = objGradeGrp.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            AddHandler cboGradeGroup.SelectedIndexChanged, AddressOf cboGradeGroup_SelectedIndexChanged

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .BeginUpdate()
                .ValueMember = "masterunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("SalReason")
                .SelectedValue = 0
                .EndUpdate()
            End With


            dsCombos = objMaster.getComboListSalaryIncrementBy(True, "IncrMode")
            Dim dtTab As DataTable = New DataView(dsCombos.Tables("IncrMode"), "Id <> 2", "Id", DataViewRowState.CurrentRows).ToTable
            With cboIncrementBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTab
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing : objMaster = Nothing : objGradeGrp = Nothing : objCommon = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub GetScale()
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet = Nothing
        Try
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), "Scale")
            If CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), dtpPromotionDate.Value.Date, "Scale")
            Else
                dsList = objWages.getScaleInfo(CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), dtpIncrementdate.Value.Date, "Scale")
            End If
            'Sohail (27 Apr 2016) -- End
            If dsList.Tables("Scale").Rows.Count > 0 Then
                txtBasicScale.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), GUI.fmtCurrency)
                txtMaximum.Text = Format(dsList.Tables("Scale").Rows(0).Item("Maximum"), GUI.fmtCurrency)
                txtNewScale.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), GUI.fmtCurrency)
            Else
                txtNewScale.Text = Format(0, GUI.fmtCurrency)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetScale", mstrModuleName)
        Finally
            objWages = Nothing
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboChangeType.SelectedValue) = enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION Then
                If dtpPromotionDate.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Promotion is mandatory information. Please provide promotion date to continue."), enMsgBoxStyle.Information)
                    dtpPromotionDate.Focus()
                    Return False
                End If
            End If

            If chkChangeGrade.Checked = True Then
                If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Grade Group. Grade Group is mandatory information."), enMsgBoxStyle.Information)
                    cboGradeGroup.Focus()
                    Return False
                ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Grade. Grade is mandatory information."), enMsgBoxStyle.Information)
                    cboGrade.Focus()
                    Return False
                ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Grade Level. Grade Level is mandatory information."), enMsgBoxStyle.Information)
                    cboGradeLevel.Focus()
                    Return False
                End If
            End If

            If txtNewScale.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Scale is mandatory information. Please provide Scale to continue."), enMsgBoxStyle.Information)
                txtNewScale.Decimal = 0
                Return False
            End If

            If dtpIncrementdate.Value.Date <= CDate(txtDate.Tag).Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Change date cannot be less than or equal to employee appointment date."), enMsgBoxStyle.Information)
                dtpIncrementdate.Focus()
                Return False
            End If

            If dtpPromotionDate.Checked = True Then
                If dtpPromotionDate.Value.Date <= CDate(txtDate.Tag).Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Promotion date cannot be less than or equal to employee appointment date."), enMsgBoxStyle.Information)
                    dtpPromotionDate.Focus()
                    Return False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub GetValue()
        Try
            cboEmployee.SelectedValue = objESalaryHistory._Employeeunkid
            dtpIncrementdate.Value = objESalaryHistory._Incrementdate
            If objESalaryHistory._Promotion_Date <> Nothing Then
                dtpPromotionDate.Value = objESalaryHistory._Promotion_Date
                dtpPromotionDate.Checked = True
            End If
            chkChangeGrade.Checked = objESalaryHistory._Isgradechange
            If objESalaryHistory._Increment_Mode = enSalaryIncrementBy.Percentage Then
                cboIncrementBy.SelectedValue = 0 : cboIncrementBy.Enabled = False
            Else
                If chkChangeGrade.Checked = True AndAlso objESalaryHistory._Increment_Mode = 0 Then
                    cboIncrementBy.SelectedValue = enSalaryIncrementBy.Grade
                Else
                    cboIncrementBy.SelectedValue = objESalaryHistory._Increment_Mode
                End If
            End If
            cboReason.SelectedValue = objESalaryHistory._Reason_Id
            cboGradeGroup.SelectedValue = objESalaryHistory._Gradegroupunkid
            cboGrade.SelectedValue = objESalaryHistory._Gradeunkid
            cboGradeLevel.SelectedValue = objESalaryHistory._Gradelevelunkid
            txtNewScale.Decimal = CDec(Format(CDec(objESalaryHistory._Newscale), GUI.fmtCurrency))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objESalaryHistory._Approveruserunkid = objESalaryHistory._Approveruserunkid
            objESalaryHistory._Changetypeid = CInt(cboChangeType.SelectedValue)
            objESalaryHistory._Currentscale = objESalaryHistory._Currentscale
            objESalaryHistory._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objESalaryHistory._Gradegroupunkid = CInt(cboGradeGroup.SelectedValue)
            objESalaryHistory._Gradeunkid = CInt(cboGrade.SelectedValue)
            objESalaryHistory._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            objESalaryHistory._Increment = objESalaryHistory._Increment
            If cboIncrementBy.Enabled = True Then
                objESalaryHistory._Increment_Mode = CInt(cboIncrementBy.SelectedValue)
            End If
            objESalaryHistory._Isapproved = objESalaryHistory._Isapproved
            objESalaryHistory._Incrementdate = dtpIncrementdate.Value
            objESalaryHistory._Isfromemployee = objESalaryHistory._Isfromemployee
            objESalaryHistory._Isgradechange = chkChangeGrade.Checked
            objESalaryHistory._Isvoid = objESalaryHistory._Isvoid
            objESalaryHistory._Newscale = txtNewScale.Decimal
            objESalaryHistory._Percentage = objESalaryHistory._Percentage
            objESalaryHistory._Periodunkid = objESalaryHistory._Periodunkid
            If dtpPromotionDate.Checked = True Then
                objESalaryHistory._Promotion_Date = dtpPromotionDate.Value
            End If
            objESalaryHistory._Psoft_Syncdatetime = objESalaryHistory._Psoft_Syncdatetime
            objESalaryHistory._Reason_Id = CInt(cboReason.SelectedValue)
            objESalaryHistory._Rehiretranunkid = objESalaryHistory._Rehiretranunkid
            objESalaryHistory._Salaryincrementtranunkid = objESalaryHistory._Salaryincrementtranunkid
            objESalaryHistory._Userunkid = User._Object._Userunkid
            objESalaryHistory._Voiddatetime = objESalaryHistory._Voiddatetime
            objESalaryHistory._Voidreason = objESalaryHistory._Voidreason
            objESalaryHistory._Voiduserunkid = objESalaryHistory._Voiduserunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try
            dsData = objESalaryHistory.GetList("List", CInt(cboEmployee.SelectedValue))

            dgvHistory.AutoGenerateColumns = False

            dgcolhChangeDate.DataPropertyName = "incrementdate"
            dgcolhChangeDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhChangeType.DataPropertyName = "changetype"
            dgcolhChangeBy.DataPropertyName = "ChangeBy"
            dgcolhPromotionDate.DataPropertyName = "promotion_date"
            dgcolhPromotionDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhOldScale.DataPropertyName = "currentscale"
            dgcolhOldScale.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhNewScale.DataPropertyName = "newscale"
            dgcolhNewScale.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhGradeGroup.DataPropertyName = "GradeGroup"
            dgcolhGrade.DataPropertyName = "Grade"
            dgcolhGradeLevel.DataPropertyName = "GradeLevel"
            dgcolhReason.DataPropertyName = "changereason"
            objdgcolhSalaryIncrementUnkid.DataPropertyName = "salaryincrementtranunkid"
            objdgcolhEmpSalaryHistoryId.DataPropertyName = "empsalaryhistorytranunkid"
            objdgcolhRehireTranId.DataPropertyName = "rehiretranunkid"

            dgvHistory.DataSource = dsData.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            cboIncrementBy.SelectedValue = 0
            chkChangeGrade.Checked = False
            dtpPromotionDate.Checked = False
            cboReason.SelectedValue = 0
            cboGradeGroup.SelectedValue = 0
            cboGrade.SelectedValue = 0
            cboGradeLevel.SelectedValue = 0
            txtNewScale.Decimal = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetFirstOpenPeriodDate()
        Try
            Dim intFirstPeriodId As Integer = 0
            Dim objMaster As New clsMasterData
            intFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, 0, 0, True)
            objMaster = Nothing

            dtpIncrementdate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpIncrementdate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

            If intFirstPeriodId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstPeriodId
                mdtFirstOpenPeriodDate = objPeriod._Start_Date.Date.AddDays(-1)
                dtpIncrementdate.MaxDate = mdtFirstOpenPeriodDate
                objPeriod = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeSalaryHistory_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objESalaryHistory = New clsempsalary_history_tran
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            chkChangeGrade.Checked = False
            chkChangeGrade_CheckedChanged(New Object, New EventArgs)
            Call SetFirstOpenPeriodDate()
            Call FillCombo()
            objlblCaption.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryHistory_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsempsalary_history_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsempsalary_history_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()
            If mAction = enAction.EDIT_ONE Then
                blnFlag = objESalaryHistory.Update(ConfigParameter._Object._CurrentDateAndTime)
            Else
                blnFlag = objESalaryHistory.Insert(ConfigParameter._Object._CurrentDateAndTime)
            End If
            If blnFlag = False AndAlso objESalaryHistory._Message <> "" Then
                eZeeMsgBox.Show(objESalaryHistory._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()
                If cboEmployee.Enabled = False Then cboEmployee.Enabled = True
                If objbtnSearchEmployee.Enabled = False Then objbtnSearchEmployee.Enabled = True
                If dtpIncrementdate.Enabled = False Then dtpIncrementdate.Enabled = True
                If txtNewScale.ReadOnly = True Then txtNewScale.ReadOnly = False
            End If
            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.SALINC_REASON, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "Group")
                With cboReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReason.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboReason.ValueMember
                .DisplayMember = cboReason.DisplayMember
                .DataSource = CType(cboReason.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboReason.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboChangeType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChangeType.SelectedIndexChanged
        Try
            Select Case CInt(cboChangeType.SelectedValue)
                Case enSalaryChangeType.SIMPLE_SALARY_CHANGE
                    dtpPromotionDate.Checked = False : dtpPromotionDate.Enabled = False
                Case enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION
                    dtpPromotionDate.Checked = True : dtpPromotionDate.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChangeType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objGrade As New clsGrade
            dsList = objGrade.getComboList("List", True, CInt(cboGradeGroup.SelectedValue))
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objGradeLvl As New clsGradeLevel
            dsList = objGradeLvl.getComboList("List", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Try
            Call GetScale()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            dtpPromotionDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpPromotionDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date


            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                txtDate.Text = objEmp._Appointeddate.ToShortDateString
                txtDate.Tag = objEmp._Appointeddate.Date
                dtpPromotionDate.MinDate = objEmp._Appointeddate.Date.AddDays(-1)
                objEmp = Nothing
                Call Fill_Grid()
            Else
                dgvHistory.DataSource = Nothing
                txtDate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboIncrementBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIncrementBy.SelectedIndexChanged
        Try
            Select Case CInt(cboIncrementBy.SelectedValue)
                Case enSalaryIncrementBy.Grade
                    chkChangeGrade.Enabled = True
                Case Else
                    chkChangeGrade.Enabled = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIncrementBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub chkChangeGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChangeGrade.CheckedChanged
        Try
            cboGradeGroup.SelectedValue = 0
            cboGradeGroup.Enabled = chkChangeGrade.Checked
            cboGrade.Enabled = chkChangeGrade.Checked
            cboGradeLevel.Enabled = chkChangeGrade.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkChangeGrade_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    mAction = enAction.EDIT_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    objESalaryHistory._Empsalaryhistorytranunkid = CInt(dgvHistory.CurrentRow.Cells(objdgcolhEmpSalaryHistoryId.Index).Value)
                    Call GetValue()
                    cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False

                    dtpIncrementdate.Enabled = False : txtNewScale.ReadOnly = True
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub
                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.PAYROLL, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objESalaryHistory._Isvoid = True
                    objESalaryHistory._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objESalaryHistory._Voidreason = xStrVoidReason
                    objESalaryHistory._Voiduserunkid = User._Object._Userunkid
                    If objESalaryHistory.Delete(CInt(dgvHistory.CurrentRow.Cells(objdgcolhEmpSalaryHistoryId.Index).Value), ConfigParameter._Object._CurrentDateAndTime) = False Then
                        If objESalaryHistory._Message <> "" Then
                            eZeeMsgBox.Show(objESalaryHistory._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    Call Fill_Grid()
                Case Else
                    dtpIncrementdate.Enabled = True : txtNewScale.ReadOnly = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows
                If CInt(xdgvr.Cells(objdgcolhSalaryIncrementUnkid.Index).Value) > 0 Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    'S.SANDEEP [17 SEP 2016] -- START
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [17 SEP 2016] -- END
                End If
                If CInt(xdgvr.Cells(objdgcolhRehireTranId.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    objlblCaption.Visible = True
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbSalaryIncrementInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSalaryIncrementInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbSalaryIncrementInfo.Text = Language._Object.getCaption(Me.gbSalaryIncrementInfo.Name, Me.gbSalaryIncrementInfo.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblIncrementDate.Text = Language._Object.getCaption(Me.lblIncrementDate.Name, Me.lblIncrementDate.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblNewScaleGrade.Text = Language._Object.getCaption(Me.lblNewScaleGrade.Name, Me.lblNewScaleGrade.Text)
            Me.chkChangeGrade.Text = Language._Object.getCaption(Me.chkChangeGrade.Name, Me.chkChangeGrade.Text)
            Me.lblPromotionDate.Text = Language._Object.getCaption(Me.lblPromotionDate.Name, Me.lblPromotionDate.Text)
            Me.lblChangeType.Text = Language._Object.getCaption(Me.lblChangeType.Name, Me.lblChangeType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.lblIncrementBy.Text = Language._Object.getCaption(Me.lblIncrementBy.Name, Me.lblIncrementBy.Text)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhChangeType.HeaderText = Language._Object.getCaption(Me.dgcolhChangeType.Name, Me.dgcolhChangeType.HeaderText)
            Me.dgcolhChangeBy.HeaderText = Language._Object.getCaption(Me.dgcolhChangeBy.Name, Me.dgcolhChangeBy.HeaderText)
            Me.dgcolhPromotionDate.HeaderText = Language._Object.getCaption(Me.dgcolhPromotionDate.Name, Me.dgcolhPromotionDate.HeaderText)
            Me.dgcolhOldScale.HeaderText = Language._Object.getCaption(Me.dgcolhOldScale.Name, Me.dgcolhOldScale.HeaderText)
            Me.dgcolhNewScale.HeaderText = Language._Object.getCaption(Me.dgcolhNewScale.Name, Me.dgcolhNewScale.HeaderText)
            Me.dgcolhGradeGroup.HeaderText = Language._Object.getCaption(Me.dgcolhGradeGroup.Name, Me.dgcolhGradeGroup.HeaderText)
            Me.dgcolhGrade.HeaderText = Language._Object.getCaption(Me.dgcolhGrade.Name, Me.dgcolhGrade.HeaderText)
            Me.dgcolhGradeLevel.HeaderText = Language._Object.getCaption(Me.dgcolhGradeLevel.Name, Me.dgcolhGradeLevel.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Promotion is mandatory information. Please provide promotion date to continue.")
            Language.setMessage(mstrModuleName, 3, "Please select Grade Group. Grade Group is mandatory information.")
            Language.setMessage(mstrModuleName, 4, "Please select Grade. Grade is mandatory information.")
            Language.setMessage(mstrModuleName, 5, "Please select Grade Level. Grade Level is mandatory information.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Scale is mandatory information. Please provide Scale to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Change date cannot be less than or equal to employee appointment date.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Promotion date cannot be less than or equal to employee appointment date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class