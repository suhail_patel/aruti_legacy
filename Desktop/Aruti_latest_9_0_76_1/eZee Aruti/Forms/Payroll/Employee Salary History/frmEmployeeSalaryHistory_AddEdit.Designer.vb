﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSalaryHistory_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSalaryHistory_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbSalaryIncrementInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboIncrementBy = New System.Windows.Forms.ComboBox
        Me.lblIncrementBy = New System.Windows.Forms.Label
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.objbtnSearchReason = New eZee.Common.eZeeGradientButton
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.pnlViewData = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPromotionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOldScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNewScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSalaryIncrementUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpSalaryHistoryId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRehireTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboChangeType = New System.Windows.Forms.ComboBox
        Me.lblChangeType = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblPromotionDate = New System.Windows.Forms.Label
        Me.dtpPromotionDate = New System.Windows.Forms.DateTimePicker
        Me.chkChangeGrade = New System.Windows.Forms.CheckBox
        Me.lblNewScaleGrade = New System.Windows.Forms.Label
        Me.txtNewScale = New eZee.TextBox.NumericTextBox
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.dtpIncrementdate = New System.Windows.Forms.DateTimePicker
        Me.lblIncrementDate = New System.Windows.Forms.Label
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.lblGrade = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.txtMaximum = New eZee.TextBox.NumericTextBox
        Me.txtBasicScale = New eZee.TextBox.NumericTextBox
        Me.lblBasicScale = New System.Windows.Forms.Label
        Me.lblMaximum = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbSalaryIncrementInfo.SuspendLayout()
        Me.pnlViewData.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbSalaryIncrementInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.cboPayYear)
        Me.pnlMainInfo.Controls.Add(Me.lblPayYear)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(913, 566)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbSalaryIncrementInfo
        '
        Me.gbSalaryIncrementInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSalaryIncrementInfo.Checked = False
        Me.gbSalaryIncrementInfo.CollapseAllExceptThis = False
        Me.gbSalaryIncrementInfo.CollapsedHoverImage = Nothing
        Me.gbSalaryIncrementInfo.CollapsedNormalImage = Nothing
        Me.gbSalaryIncrementInfo.CollapsedPressedImage = Nothing
        Me.gbSalaryIncrementInfo.CollapseOnLoad = False
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtMaximum)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtBasicScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblBasicScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblMaximum)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboIncrementBy)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblIncrementBy)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblAppointmentdate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnSearchReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.pnlViewData)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.btnSave)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objelLine1)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblEmployee)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblPromotionDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.dtpPromotionDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.chkChangeGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblNewScaleGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.txtNewScale)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.objbtnAddReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblReason)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGradeLevel)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGrade)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.cboGradeGroup)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.dtpIncrementdate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblIncrementDate)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGradeGroup)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGradeLevel)
        Me.gbSalaryIncrementInfo.Controls.Add(Me.lblGrade)
        Me.gbSalaryIncrementInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbSalaryIncrementInfo.ExpandedHoverImage = Nothing
        Me.gbSalaryIncrementInfo.ExpandedNormalImage = Nothing
        Me.gbSalaryIncrementInfo.ExpandedPressedImage = Nothing
        Me.gbSalaryIncrementInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSalaryIncrementInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSalaryIncrementInfo.HeaderHeight = 25
        Me.gbSalaryIncrementInfo.HeaderMessage = ""
        Me.gbSalaryIncrementInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSalaryIncrementInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSalaryIncrementInfo.HeightOnCollapse = 0
        Me.gbSalaryIncrementInfo.LeftTextSpace = 0
        Me.gbSalaryIncrementInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbSalaryIncrementInfo.Name = "gbSalaryIncrementInfo"
        Me.gbSalaryIncrementInfo.OpenHeight = 182
        Me.gbSalaryIncrementInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSalaryIncrementInfo.ShowBorder = True
        Me.gbSalaryIncrementInfo.ShowCheckBox = False
        Me.gbSalaryIncrementInfo.ShowCollapseButton = False
        Me.gbSalaryIncrementInfo.ShowDefaultBorderColor = True
        Me.gbSalaryIncrementInfo.ShowDownButton = False
        Me.gbSalaryIncrementInfo.ShowHeader = True
        Me.gbSalaryIncrementInfo.Size = New System.Drawing.Size(913, 511)
        Me.gbSalaryIncrementInfo.TabIndex = 0
        Me.gbSalaryIncrementInfo.Temp = 0
        Me.gbSalaryIncrementInfo.Text = "Employee Salary History Information"
        Me.gbSalaryIncrementInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIncrementBy
        '
        Me.cboIncrementBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrementBy.FormattingEnabled = True
        Me.cboIncrementBy.Location = New System.Drawing.Point(106, 114)
        Me.cboIncrementBy.Name = "cboIncrementBy"
        Me.cboIncrementBy.Size = New System.Drawing.Size(313, 21)
        Me.cboIncrementBy.TabIndex = 273
        '
        'lblIncrementBy
        '
        Me.lblIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementBy.Location = New System.Drawing.Point(12, 117)
        Me.lblIncrementBy.Name = "lblIncrementBy"
        Me.lblIncrementBy.Size = New System.Drawing.Size(88, 15)
        Me.lblIncrementBy.TabIndex = 274
        Me.lblIncrementBy.Text = "Change By"
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(219, 63)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(64, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appt. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchReason
        '
        Me.objbtnSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReason.BorderSelected = False
        Me.objbtnSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReason.Location = New System.Drawing.Point(442, 162)
        Me.objbtnSearchReason.Name = "objbtnSearchReason"
        Me.objbtnSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReason.TabIndex = 20
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(289, 60)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(130, 21)
        Me.txtDate.TabIndex = 184
        '
        'pnlViewData
        '
        Me.pnlViewData.Controls.Add(Me.dgvHistory)
        Me.pnlViewData.Controls.Add(Me.cboChangeType)
        Me.pnlViewData.Controls.Add(Me.lblChangeType)
        Me.pnlViewData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlViewData.Location = New System.Drawing.Point(12, 193)
        Me.pnlViewData.Name = "pnlViewData"
        Me.pnlViewData.Size = New System.Drawing.Size(886, 312)
        Me.pnlViewData.TabIndex = 271
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhChangeDate, Me.dgcolhChangeType, Me.dgcolhChangeBy, Me.dgcolhPromotionDate, Me.dgcolhOldScale, Me.dgcolhNewScale, Me.dgcolhGradeGroup, Me.dgcolhGrade, Me.dgcolhGradeLevel, Me.dgcolhReason, Me.objdgcolhSalaryIncrementUnkid, Me.objdgcolhEmpSalaryHistoryId, Me.objdgcolhRehireTranId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvHistory.Size = New System.Drawing.Size(886, 312)
        Me.dgvHistory.TabIndex = 267
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.HeaderText = "Change Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhChangeType
        '
        Me.dgcolhChangeType.HeaderText = "Change Type"
        Me.dgcolhChangeType.Name = "dgcolhChangeType"
        Me.dgcolhChangeType.ReadOnly = True
        '
        'dgcolhChangeBy
        '
        Me.dgcolhChangeBy.HeaderText = "Change By"
        Me.dgcolhChangeBy.Name = "dgcolhChangeBy"
        Me.dgcolhChangeBy.ReadOnly = True
        '
        'dgcolhPromotionDate
        '
        Me.dgcolhPromotionDate.HeaderText = "Promotion Date"
        Me.dgcolhPromotionDate.Name = "dgcolhPromotionDate"
        Me.dgcolhPromotionDate.ReadOnly = True
        '
        'dgcolhOldScale
        '
        Me.dgcolhOldScale.HeaderText = "Current Scale"
        Me.dgcolhOldScale.Name = "dgcolhOldScale"
        Me.dgcolhOldScale.ReadOnly = True
        '
        'dgcolhNewScale
        '
        Me.dgcolhNewScale.HeaderText = "New Scale"
        Me.dgcolhNewScale.Name = "dgcolhNewScale"
        Me.dgcolhNewScale.ReadOnly = True
        '
        'dgcolhGradeGroup
        '
        Me.dgcolhGradeGroup.HeaderText = "Grade Group"
        Me.dgcolhGradeGroup.Name = "dgcolhGradeGroup"
        Me.dgcolhGradeGroup.ReadOnly = True
        '
        'dgcolhGrade
        '
        Me.dgcolhGrade.HeaderText = "Grade"
        Me.dgcolhGrade.Name = "dgcolhGrade"
        Me.dgcolhGrade.ReadOnly = True
        '
        'dgcolhGradeLevel
        '
        Me.dgcolhGradeLevel.HeaderText = "Grade Level"
        Me.dgcolhGradeLevel.Name = "dgcolhGradeLevel"
        Me.dgcolhGradeLevel.ReadOnly = True
        '
        'dgcolhReason
        '
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        '
        'objdgcolhSalaryIncrementUnkid
        '
        Me.objdgcolhSalaryIncrementUnkid.HeaderText = "objdgcolhSalaryIncrementUnkid"
        Me.objdgcolhSalaryIncrementUnkid.Name = "objdgcolhSalaryIncrementUnkid"
        Me.objdgcolhSalaryIncrementUnkid.ReadOnly = True
        Me.objdgcolhSalaryIncrementUnkid.Visible = False
        '
        'objdgcolhEmpSalaryHistoryId
        '
        Me.objdgcolhEmpSalaryHistoryId.HeaderText = "objdgcolhEmpSalaryHistoryId"
        Me.objdgcolhEmpSalaryHistoryId.Name = "objdgcolhEmpSalaryHistoryId"
        Me.objdgcolhEmpSalaryHistoryId.ReadOnly = True
        Me.objdgcolhEmpSalaryHistoryId.Visible = False
        '
        'objdgcolhRehireTranId
        '
        Me.objdgcolhRehireTranId.HeaderText = "objdgcolhRehireTranId"
        Me.objdgcolhRehireTranId.Name = "objdgcolhRehireTranId"
        Me.objdgcolhRehireTranId.ReadOnly = True
        Me.objdgcolhRehireTranId.Visible = False
        '
        'cboChangeType
        '
        Me.cboChangeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeType.FormattingEnabled = True
        Me.cboChangeType.Location = New System.Drawing.Point(91, 286)
        Me.cboChangeType.Name = "cboChangeType"
        Me.cboChangeType.Size = New System.Drawing.Size(44, 21)
        Me.cboChangeType.TabIndex = 4
        Me.cboChangeType.Visible = False
        '
        'lblChangeType
        '
        Me.lblChangeType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeType.Location = New System.Drawing.Point(55, 289)
        Me.lblChangeType.Name = "lblChangeType"
        Me.lblChangeType.Size = New System.Drawing.Size(30, 15)
        Me.lblChangeType.TabIndex = 3
        Me.lblChangeType.Text = "Change Type"
        Me.lblChangeType.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(801, 157)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 22
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 140)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(886, 10)
        Me.objelLine1.TabIndex = 268
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(877, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(106, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(765, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(88, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'lblPromotionDate
        '
        Me.lblPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPromotionDate.Location = New System.Drawing.Point(12, 90)
        Me.lblPromotionDate.Name = "lblPromotionDate"
        Me.lblPromotionDate.Size = New System.Drawing.Size(88, 15)
        Me.lblPromotionDate.TabIndex = 7
        Me.lblPromotionDate.Text = "Promotion Date"
        '
        'dtpPromotionDate
        '
        Me.dtpPromotionDate.Checked = False
        Me.dtpPromotionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPromotionDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPromotionDate.Location = New System.Drawing.Point(106, 87)
        Me.dtpPromotionDate.Name = "dtpPromotionDate"
        Me.dtpPromotionDate.ShowCheckBox = True
        Me.dtpPromotionDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpPromotionDate.TabIndex = 8
        '
        'chkChangeGrade
        '
        Me.chkChangeGrade.BackColor = System.Drawing.Color.Transparent
        Me.chkChangeGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkChangeGrade.Location = New System.Drawing.Point(630, 4)
        Me.chkChangeGrade.Name = "chkChangeGrade"
        Me.chkChangeGrade.Size = New System.Drawing.Size(111, 17)
        Me.chkChangeGrade.TabIndex = 11
        Me.chkChangeGrade.Text = "Change Grade"
        Me.chkChangeGrade.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkChangeGrade.UseVisualStyleBackColor = False
        '
        'lblNewScaleGrade
        '
        Me.lblNewScaleGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewScaleGrade.Location = New System.Drawing.Point(219, 90)
        Me.lblNewScaleGrade.Name = "lblNewScaleGrade"
        Me.lblNewScaleGrade.Size = New System.Drawing.Size(64, 15)
        Me.lblNewScaleGrade.TabIndex = 9
        Me.lblNewScaleGrade.Text = "New Scale"
        '
        'txtNewScale
        '
        Me.txtNewScale.AllowNegative = True
        Me.txtNewScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewScale.DigitsInGroup = 0
        Me.txtNewScale.Flags = 0
        Me.txtNewScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewScale.Location = New System.Drawing.Point(289, 87)
        Me.txtNewScale.MaxDecimalPlaces = 6
        Me.txtNewScale.MaxWholeDigits = 21
        Me.txtNewScale.Name = "txtNewScale"
        Me.txtNewScale.Prefix = ""
        Me.txtNewScale.RangeMax = 1.7976931348623157E+308
        Me.txtNewScale.RangeMin = -1.7976931348623157E+308
        Me.txtNewScale.Size = New System.Drawing.Size(130, 21)
        Me.txtNewScale.TabIndex = 10
        Me.txtNewScale.Text = "0"
        Me.txtNewScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(469, 162)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 21
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(106, 162)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(330, 21)
        Me.cboReason.TabIndex = 19
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(12, 165)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(88, 15)
        Me.lblReason.TabIndex = 18
        Me.lblReason.Text = "Reason"
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(517, 114)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(157, 21)
        Me.cboGradeLevel.TabIndex = 17
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(517, 87)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(157, 21)
        Me.cboGrade.TabIndex = 15
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(517, 60)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboGradeGroup.TabIndex = 13
        '
        'dtpIncrementdate
        '
        Me.dtpIncrementdate.Checked = False
        Me.dtpIncrementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIncrementdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIncrementdate.Location = New System.Drawing.Point(106, 60)
        Me.dtpIncrementdate.Name = "dtpIncrementdate"
        Me.dtpIncrementdate.Size = New System.Drawing.Size(107, 21)
        Me.dtpIncrementdate.TabIndex = 6
        '
        'lblIncrementDate
        '
        Me.lblIncrementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementDate.Location = New System.Drawing.Point(12, 63)
        Me.lblIncrementDate.Name = "lblIncrementDate"
        Me.lblIncrementDate.Size = New System.Drawing.Size(88, 15)
        Me.lblIncrementDate.TabIndex = 5
        Me.lblIncrementDate.Text = "Change Date"
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(432, 63)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(79, 15)
        Me.lblGradeGroup.TabIndex = 12
        Me.lblGradeGroup.Text = "Grade Group"
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(432, 117)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(79, 15)
        Me.lblGradeLevel.TabIndex = 16
        Me.lblGradeLevel.Text = "Grade Level"
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(432, 90)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(79, 15)
        Me.lblGrade.TabIndex = 14
        Me.lblGrade.Text = "Grade"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 511)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(913, 55)
        Me.objFooter.TabIndex = 1
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.Color.Orange
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Black
        Me.objlblCaption.Location = New System.Drawing.Point(12, 20)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(152, 17)
        Me.objlblCaption.TabIndex = 8
        Me.objlblCaption.Text = "Employee Rehired"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(804, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboPayYear
        '
        Me.cboPayYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(322, 82)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(42, 21)
        Me.cboPayYear.TabIndex = 0
        Me.cboPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(282, 85)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(60, 15)
        Me.lblPayYear.TabIndex = 162
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.Visible = False
        '
        'txtMaximum
        '
        Me.txtMaximum.AllowNegative = True
        Me.txtMaximum.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtMaximum.DigitsInGroup = 0
        Me.txtMaximum.Flags = 0
        Me.txtMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaximum.Location = New System.Drawing.Point(748, 114)
        Me.txtMaximum.MaxDecimalPlaces = 6
        Me.txtMaximum.MaxWholeDigits = 21
        Me.txtMaximum.Name = "txtMaximum"
        Me.txtMaximum.Prefix = ""
        Me.txtMaximum.RangeMax = 1.7976931348623157E+308
        Me.txtMaximum.RangeMin = -1.7976931348623157E+308
        Me.txtMaximum.ReadOnly = True
        Me.txtMaximum.Size = New System.Drawing.Size(123, 21)
        Me.txtMaximum.TabIndex = 277
        Me.txtMaximum.Text = "0.00"
        Me.txtMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBasicScale
        '
        Me.txtBasicScale.AllowNegative = True
        Me.txtBasicScale.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBasicScale.DigitsInGroup = 0
        Me.txtBasicScale.Flags = 0
        Me.txtBasicScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBasicScale.Location = New System.Drawing.Point(748, 87)
        Me.txtBasicScale.MaxDecimalPlaces = 6
        Me.txtBasicScale.MaxWholeDigits = 21
        Me.txtBasicScale.Name = "txtBasicScale"
        Me.txtBasicScale.Prefix = ""
        Me.txtBasicScale.RangeMax = 1.7976931348623157E+308
        Me.txtBasicScale.RangeMin = -1.7976931348623157E+308
        Me.txtBasicScale.ReadOnly = True
        Me.txtBasicScale.Size = New System.Drawing.Size(123, 21)
        Me.txtBasicScale.TabIndex = 276
        Me.txtBasicScale.Text = "0.00"
        Me.txtBasicScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBasicScale
        '
        Me.lblBasicScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicScale.Location = New System.Drawing.Point(680, 90)
        Me.lblBasicScale.Name = "lblBasicScale"
        Me.lblBasicScale.Size = New System.Drawing.Size(62, 15)
        Me.lblBasicScale.TabIndex = 279
        Me.lblBasicScale.Text = "Minimum"
        '
        'lblMaximum
        '
        Me.lblMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximum.Location = New System.Drawing.Point(680, 117)
        Me.lblMaximum.Name = "lblMaximum"
        Me.lblMaximum.Size = New System.Drawing.Size(62, 15)
        Me.lblMaximum.TabIndex = 278
        Me.lblMaximum.Text = "Maximum"
        '
        'frmEmployeeSalaryHistory_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 566)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSalaryHistory_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Salary History"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbSalaryIncrementInfo.ResumeLayout(False)
        Me.gbSalaryIncrementInfo.PerformLayout()
        Me.pnlViewData.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbSalaryIncrementInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblIncrementDate As System.Windows.Forms.Label
    Friend WithEvents dtpIncrementdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblNewScaleGrade As System.Windows.Forms.Label
    Friend WithEvents txtNewScale As eZee.TextBox.NumericTextBox
    Friend WithEvents chkChangeGrade As System.Windows.Forms.CheckBox
    Friend WithEvents lblPromotionDate As System.Windows.Forms.Label
    Friend WithEvents lblChangeType As System.Windows.Forms.Label
    Friend WithEvents dtpPromotionDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboChangeType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents pnlViewData As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboIncrementBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblIncrementBy As System.Windows.Forms.Label
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPromotionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOldScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNewScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSalaryIncrementUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpSalaryHistoryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRehireTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtMaximum As eZee.TextBox.NumericTextBox
    Friend WithEvents txtBasicScale As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBasicScale As System.Windows.Forms.Label
    Friend WithEvents lblMaximum As System.Windows.Forms.Label
End Class
