﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVendor_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVendor_AddEdit))
        Me.pnlVendor = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbVendorInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.cboZipcode = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblPostal = New System.Windows.Forms.Label
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.chkBenefitProvider = New System.Windows.Forms.CheckBox
        Me.txtCompany = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.txtLastname = New eZee.TextBox.AlphanumericTextBox
        Me.lblLastname = New System.Windows.Forms.Label
        Me.txtwebsite = New eZee.TextBox.AlphanumericTextBox
        Me.lblwebsite = New System.Windows.Forms.Label
        Me.txtcontactperson = New eZee.TextBox.AlphanumericTextBox
        Me.lblcontactperson = New System.Windows.Forms.Label
        Me.txtcontactno = New eZee.TextBox.AlphanumericTextBox
        Me.lblContactNo = New System.Windows.Forms.Label
        Me.txtaddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtaddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.txtFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlVendor.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbVendorInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlVendor
        '
        Me.pnlVendor.Controls.Add(Me.objFooter)
        Me.pnlVendor.Controls.Add(Me.gbVendorInformation)
        Me.pnlVendor.Controls.Add(Me.eZeeHeader)
        Me.pnlVendor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlVendor.Location = New System.Drawing.Point(0, 0)
        Me.pnlVendor.Name = "pnlVendor"
        Me.pnlVendor.Size = New System.Drawing.Size(462, 431)
        Me.pnlVendor.TabIndex = 3
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 381)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(462, 50)
        Me.objFooter.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(358, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 17
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(258, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 16
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbVendorInformation
        '
        Me.gbVendorInformation.BorderColor = System.Drawing.Color.Black
        Me.gbVendorInformation.Checked = False
        Me.gbVendorInformation.CollapseAllExceptThis = False
        Me.gbVendorInformation.CollapsedHoverImage = Nothing
        Me.gbVendorInformation.CollapsedNormalImage = Nothing
        Me.gbVendorInformation.CollapsedPressedImage = Nothing
        Me.gbVendorInformation.CollapseOnLoad = False
        Me.gbVendorInformation.Controls.Add(Me.objbtnAddGroup)
        Me.gbVendorInformation.Controls.Add(Me.cboZipcode)
        Me.gbVendorInformation.Controls.Add(Me.cboState)
        Me.gbVendorInformation.Controls.Add(Me.cboCity)
        Me.gbVendorInformation.Controls.Add(Me.lblCity)
        Me.gbVendorInformation.Controls.Add(Me.lblPostal)
        Me.gbVendorInformation.Controls.Add(Me.lblCountry)
        Me.gbVendorInformation.Controls.Add(Me.cboCountry)
        Me.gbVendorInformation.Controls.Add(Me.lblState)
        Me.gbVendorInformation.Controls.Add(Me.chkBenefitProvider)
        Me.gbVendorInformation.Controls.Add(Me.txtCompany)
        Me.gbVendorInformation.Controls.Add(Me.lblCompany)
        Me.gbVendorInformation.Controls.Add(Me.txtLastname)
        Me.gbVendorInformation.Controls.Add(Me.lblLastname)
        Me.gbVendorInformation.Controls.Add(Me.txtwebsite)
        Me.gbVendorInformation.Controls.Add(Me.lblwebsite)
        Me.gbVendorInformation.Controls.Add(Me.txtcontactperson)
        Me.gbVendorInformation.Controls.Add(Me.lblcontactperson)
        Me.gbVendorInformation.Controls.Add(Me.txtcontactno)
        Me.gbVendorInformation.Controls.Add(Me.lblContactNo)
        Me.gbVendorInformation.Controls.Add(Me.txtaddress2)
        Me.gbVendorInformation.Controls.Add(Me.txtaddress1)
        Me.gbVendorInformation.Controls.Add(Me.lblAddress)
        Me.gbVendorInformation.Controls.Add(Me.txtFirstName)
        Me.gbVendorInformation.Controls.Add(Me.lblFirstName)
        Me.gbVendorInformation.Controls.Add(Me.txtCode)
        Me.gbVendorInformation.Controls.Add(Me.lblCode)
        Me.gbVendorInformation.Controls.Add(Me.cboGroup)
        Me.gbVendorInformation.Controls.Add(Me.lblGroup)
        Me.gbVendorInformation.ExpandedHoverImage = Nothing
        Me.gbVendorInformation.ExpandedNormalImage = Nothing
        Me.gbVendorInformation.ExpandedPressedImage = Nothing
        Me.gbVendorInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVendorInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbVendorInformation.HeaderHeight = 25
        Me.gbVendorInformation.HeightOnCollapse = 0
        Me.gbVendorInformation.LeftTextSpace = 0
        Me.gbVendorInformation.Location = New System.Drawing.Point(9, 68)
        Me.gbVendorInformation.Name = "gbVendorInformation"
        Me.gbVendorInformation.OpenHeight = 300
        Me.gbVendorInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbVendorInformation.ShowBorder = True
        Me.gbVendorInformation.ShowCheckBox = False
        Me.gbVendorInformation.ShowCollapseButton = False
        Me.gbVendorInformation.ShowDefaultBorderColor = True
        Me.gbVendorInformation.ShowDownButton = False
        Me.gbVendorInformation.ShowHeader = True
        Me.gbVendorInformation.Size = New System.Drawing.Size(444, 306)
        Me.gbVendorInformation.TabIndex = 1
        Me.gbVendorInformation.Temp = 0
        Me.gbVendorInformation.Text = "Vendor"
        Me.gbVendorInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(229, 34)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 215
        '
        'cboZipcode
        '
        Me.cboZipcode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboZipcode.FormattingEnabled = True
        Me.cboZipcode.Location = New System.Drawing.Point(315, 222)
        Me.cboZipcode.Name = "cboZipcode"
        Me.cboZipcode.Size = New System.Drawing.Size(113, 21)
        Me.cboZipcode.TabIndex = 12
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(315, 195)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(113, 21)
        Me.cboState.TabIndex = 10
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(99, 222)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(124, 21)
        Me.cboCity.TabIndex = 11
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(8, 224)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(85, 16)
        Me.lblCity.TabIndex = 148
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostal
        '
        Me.lblPostal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostal.Location = New System.Drawing.Point(229, 224)
        Me.lblPostal.Name = "lblPostal"
        Me.lblPostal.Size = New System.Drawing.Size(73, 16)
        Me.lblPostal.TabIndex = 149
        Me.lblPostal.Text = "ZIP code"
        Me.lblPostal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 197)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(85, 16)
        Me.lblCountry.TabIndex = 152
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(99, 195)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(124, 21)
        Me.cboCountry.TabIndex = 9
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(229, 197)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(73, 16)
        Me.lblState.TabIndex = 150
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkBenefitProvider
        '
        Me.chkBenefitProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBenefitProvider.Location = New System.Drawing.Point(230, 62)
        Me.chkBenefitProvider.Name = "chkBenefitProvider"
        Me.chkBenefitProvider.Size = New System.Drawing.Size(203, 17)
        Me.chkBenefitProvider.TabIndex = 3
        Me.chkBenefitProvider.Text = "Benefit Provider"
        Me.chkBenefitProvider.UseVisualStyleBackColor = True
        '
        'txtCompany
        '
        Me.txtCompany.Flags = 0
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompany.Location = New System.Drawing.Point(99, 87)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(334, 21)
        Me.txtCompany.TabIndex = 4
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(8, 91)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(85, 15)
        Me.lblCompany.TabIndex = 47
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLastname
        '
        Me.txtLastname.Flags = 0
        Me.txtLastname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastname.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLastname.Location = New System.Drawing.Point(314, 114)
        Me.txtLastname.Name = "txtLastname"
        Me.txtLastname.Size = New System.Drawing.Size(119, 21)
        Me.txtLastname.TabIndex = 6
        '
        'lblLastname
        '
        Me.lblLastname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastname.Location = New System.Drawing.Point(229, 117)
        Me.lblLastname.Name = "lblLastname"
        Me.lblLastname.Size = New System.Drawing.Size(79, 15)
        Me.lblLastname.TabIndex = 45
        Me.lblLastname.Text = "Last Name"
        Me.lblLastname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwebsite
        '
        Me.txtwebsite.Flags = 0
        Me.txtwebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtwebsite.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtwebsite.Location = New System.Drawing.Point(99, 276)
        Me.txtwebsite.Name = "txtwebsite"
        Me.txtwebsite.Size = New System.Drawing.Size(334, 21)
        Me.txtwebsite.TabIndex = 15
        '
        'lblwebsite
        '
        Me.lblwebsite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblwebsite.Location = New System.Drawing.Point(8, 278)
        Me.lblwebsite.Name = "lblwebsite"
        Me.lblwebsite.Size = New System.Drawing.Size(85, 16)
        Me.lblwebsite.TabIndex = 42
        Me.lblwebsite.Text = "Website"
        Me.lblwebsite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactperson
        '
        Me.txtcontactperson.Flags = 0
        Me.txtcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactperson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactperson.Location = New System.Drawing.Point(99, 249)
        Me.txtcontactperson.Name = "txtcontactperson"
        Me.txtcontactperson.Size = New System.Drawing.Size(124, 21)
        Me.txtcontactperson.TabIndex = 13
        '
        'lblcontactperson
        '
        Me.lblcontactperson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcontactperson.Location = New System.Drawing.Point(8, 251)
        Me.lblcontactperson.Name = "lblcontactperson"
        Me.lblcontactperson.Size = New System.Drawing.Size(85, 16)
        Me.lblcontactperson.TabIndex = 40
        Me.lblcontactperson.Text = "Contact Person"
        Me.lblcontactperson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtcontactno
        '
        Me.txtcontactno.Flags = 0
        Me.txtcontactno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcontactno.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtcontactno.Location = New System.Drawing.Point(315, 249)
        Me.txtcontactno.Name = "txtcontactno"
        Me.txtcontactno.Size = New System.Drawing.Size(118, 21)
        Me.txtcontactno.TabIndex = 14
        '
        'lblContactNo
        '
        Me.lblContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactNo.Location = New System.Drawing.Point(229, 252)
        Me.lblContactNo.Name = "lblContactNo"
        Me.lblContactNo.Size = New System.Drawing.Size(79, 15)
        Me.lblContactNo.TabIndex = 38
        Me.lblContactNo.Text = "Contact No"
        Me.lblContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtaddress2
        '
        Me.txtaddress2.Flags = 0
        Me.txtaddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress2.Location = New System.Drawing.Point(99, 168)
        Me.txtaddress2.Name = "txtaddress2"
        Me.txtaddress2.Size = New System.Drawing.Size(334, 21)
        Me.txtaddress2.TabIndex = 8
        '
        'txtaddress1
        '
        Me.txtaddress1.Flags = 0
        Me.txtaddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress1.Location = New System.Drawing.Point(99, 141)
        Me.txtaddress1.Name = "txtaddress1"
        Me.txtaddress1.Size = New System.Drawing.Size(334, 21)
        Me.txtaddress1.TabIndex = 7
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(8, 144)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(85, 15)
        Me.lblAddress.TabIndex = 31
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFirstName
        '
        Me.txtFirstName.Flags = 0
        Me.txtFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstName.Location = New System.Drawing.Point(99, 114)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(124, 21)
        Me.txtFirstName.TabIndex = 5
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(8, 118)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(85, 15)
        Me.lblFirstName.TabIndex = 13
        Me.lblFirstName.Text = "First Name"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(99, 60)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(124, 21)
        Me.txtCode.TabIndex = 2
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 62)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(85, 15)
        Me.lblCode.TabIndex = 11
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(99, 33)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(124, 21)
        Me.cboGroup.TabIndex = 1
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(85, 15)
        Me.lblGroup.TabIndex = 1
        Me.lblGroup.Text = "Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(462, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Vendor Information"
        '
        'frmVendor_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 431)
        Me.Controls.Add(Me.pnlVendor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVendor_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Vendor"
        Me.pnlVendor.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbVendorInformation.ResumeLayout(False)
        Me.gbVendorInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlVendor As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbVendorInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents txtLastname As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLastname As System.Windows.Forms.Label
    Friend WithEvents txtwebsite As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblwebsite As System.Windows.Forms.Label
    Friend WithEvents txtcontactperson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblcontactperson As System.Windows.Forms.Label
    Friend WithEvents txtcontactno As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblContactNo As System.Windows.Forms.Label
    Friend WithEvents txtaddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtaddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents chkBenefitProvider As System.Windows.Forms.CheckBox
    Friend WithEvents cboZipcode As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblPostal As System.Windows.Forms.Label
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
End Class
