﻿Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmExchangeRate_AddEdit

    Private menAction As enAction = enAction.ADD_ONE
    Private mintExchangeRateUnkid As Integer = -1
    Private objExchangeRateData As clsExchangeRate

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES ; Date is to be Place
    Private mintOldCountry As Integer = -1
    Private mdblOldExRate1 As Double = 0
    Private mdblOldExRate2 As Double = 0
    Private mdtOldExDate As Date = Nothing
    'Private mdtExchangeDate As Date = Nothing 'Sohail (03 Sep 2012)
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    Private mblnCancel As Boolean = True
    'Sandeep | 04 JAN 2010 | -- Start
    Private mblnIsBaseCurrency As Boolean = False
    'Sandeep | 04 JAN 2010 | -- END 
    Private ReadOnly mstrModuleName As String = "frmExchangeRate_AddEdit"

#Region " Display Dialog "


    'Sandeep | 04 JAN 2010 | -- Start
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal blnIsBaseCurr As Boolean = False) As Boolean
        'Sandeep | 04 JAN 2010 | -- END 
        Try
            mintExchangeRateUnkid = intUnkId
            menAction = eAction
            mblnIsBaseCurrency = blnIsBaseCurr
            Me.ShowDialog()

            intUnkId = mintExchangeRateUnkid

            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub setColor()
        cboCountry.BackColor = GUI.ColorComp
        txtCurrencyName.BackColor = GUI.ColorComp
        txtCurrencySign.BackColor = GUI.ColorComp
        txtExchangeRate.BackColor = GUI.ColorComp
        txtExchangeRate1.BackColor = GUI.ColorComp
        txtExchangeRate2.BackColor = GUI.ColorComp

    End Sub

    Private Sub FillCombos()
        Dim objMasterData As New clsMasterData
        Dim dsCountry As DataSet = Nothing
        Try
            RemoveHandler cboCountry.SelectedIndexChanged, AddressOf cboCountry_SelectedIndexChanged
            dsCountry = objMasterData.getCountryList("CountryList")
            cboCountry.DisplayMember = "country_name"
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DataSource = dsCountry.Tables(0).Copy

            cboCurrencySignName.DisplayMember = "currency"
            cboCurrencySignName.ValueMember = "countryunkid"
            cboCurrencySignName.DataSource = dsCountry.Tables(0).Copy

            cboCurrencySign.DisplayMember = "currency"
            cboCurrencySign.ValueMember = "countryunkid"
            cboCurrencySign.DataSource = dsCountry.Tables(0).Copy
            AddHandler cboCountry.SelectedIndexChanged, AddressOf cboCountry_SelectedIndexChanged

            If Company._Object._Localization_Country > 0 Then
                cboCountry.SelectedValue = Company._Object._Localization_Country
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombos", mstrModuleName)
        Finally
            dsCountry.Dispose()
            objMasterData = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        cboCountry.SelectedValue = IIf(objExchangeRateData._Countryunkid > 0, objExchangeRateData._Countryunkid, 1)
        'Sohail (10 Jan 2020) -- Start
        'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
        'txtCurrencyName.Text = objExchangeRateData._Currency_Name
        'txtCurrencySign.Text = objExchangeRateData._Currency_Sign
        'Sohail (10 Jan 2020) -- End
        txtExchangeRate.Text = Format(CDec(objExchangeRateData._Exchange_Rate), "0.0##########")
        txtExchangeRate1.Text = Format(CDec(objExchangeRateData._Exchange_Rate1), "0.0##########")
        txtExchangeRate2.Text = Format(CDec(objExchangeRateData._Exchange_Rate2), "0.0##########")

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'mdtExchangeDate = objExchangeRateData._Exchange_Date
        If objExchangeRateData._Exchange_Date = Nothing Then
            dtpDateOfRate.Value = ConfigParameter._Object._CurrentDateAndTime
        Else
            dtpDateOfRate.Value = objExchangeRateData._Exchange_Date
        End If
        'Sohail (03 Sep 2012) -- End
        mdtOldExDate = objExchangeRateData._Exchange_Date
        mintOldCountry = objExchangeRateData._Countryunkid
        mdblOldExRate1 = CDbl(Format(CDbl(objExchangeRateData._Exchange_Rate1), "0.0##########"))
        mdblOldExRate2 = CDbl(Format(CDbl(objExchangeRateData._Exchange_Rate2), "0.0##########"))
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        If objExchangeRateData._Isbasecurrency Then

            'Varsha (13 Dec 2017) -- Start
            'Enhancement: Allow to change Base currency Decimal Place
            'txtExchangeRate1.ReadOnly = True
            'txtExchangeRate2.ReadOnly = True
            'txtExchangeRate.ReadOnly = True
            'Varsha (13 Dec 2017) -- End    

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            cboCountry.Enabled = False
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'Varsha (13 Dec 2017) -- Start
            'Enhancement: Allow to change Base currency Decimal Place
            txtExchangeRate1.Enabled = False
            txtExchangeRate2.Enabled = False
            txtExchangeRate.Enabled = False
            txtCurrencyName.Enabled = False
            txtCurrencySign.Enabled = False
            radCurrencySignPrefix.Enabled = False
            radCurrencySignSuffix.Enabled = False
            'Varsha (13 Dec 2017) -- End

        End If

        nudDecimalPlaces.Value = objExchangeRateData._Digits_After_Decimal
        If objExchangeRateData._Isprefix = False Then
            radCurrencySignSuffix.Checked = True
        Else
            radCurrencySignPrefix.Checked = True
        End If

    End Sub

    Private Sub SetValue()

        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES

        'S.SANDEEP [ 20 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
        'mdtExchangeDate = ConfigParameter._Object._CurrentDateAndTime.AddDays(1)
        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'mdtExchangeDate = ConfigParameter._Object._CurrentDateAndTime
        objExchangeRateData._Exchange_Date = dtpDateOfRate.Value
        'Sohail (03 Sep 2012) -- End
        'S.SANDEEP [ 20 MARCH 2012 ] -- END
        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'If objExchangeRateData._Isbasecurrency = True Then
        '    mdtExchangeDate = objExchangeRateData._Exchange_Date
        '    objExchangeRateData._Exchange_Date = mdtExchangeDate
        'Else
        '    If mdblOldExRate1 <> objExchangeRateData._Exchange_Rate1 Or mdblOldExRate2 <> txtExchangeRate2.Decimal Then
        '        objExchangeRateData._Exchange_Date = mdtExchangeDate
        '    Else
        '        mdtExchangeDate = objExchangeRateData._Exchange_Date
        '        objExchangeRateData._Exchange_Date = mdtExchangeDate
        '    End If
        'End If
        'Sohail (03 Sep 2012) -- End
        'S.SANDEEP [ 07 NOV 2011 ] -- END


        objExchangeRateData._Countryunkid = CInt(cboCountry.SelectedValue)
        objExchangeRateData._Currency_Name = txtCurrencyName.Text
        objExchangeRateData._Currency_Sign = txtCurrencySign.Text
        objExchangeRateData._Exchange_Rate = CDec(txtExchangeRate.NumericText)

        objExchangeRateData._Exchange_Rate1 = txtExchangeRate1.Decimal
        objExchangeRateData._Exchange_Rate2 = txtExchangeRate2.Decimal

        If radCurrencySignPrefix.Checked Then
            objExchangeRateData._Isprefix = True
        End If
        If radCurrencySignSuffix.Checked Then
            objExchangeRateData._Isprefix = False
        End If
        objExchangeRateData._Digits_After_Decimal = CInt(nudDecimalPlaces.Value)
    End Sub

    'S.SANDEEP [ 05 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function IsValid() As Boolean
        Try
            If CInt(cboCountry.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select a Country from the drop down."), enMsgBoxStyle.Information) '?1
                cboCountry.Select()
                Return False
            End If

            If Trim(txtCurrencyName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Currency Name cannot be blank. Currency Name is a compulsory information."), enMsgBoxStyle.Information) '?2
                txtCurrencyName.Select()
                Return False
            End If

            If Trim(txtCurrencySign.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency Sign cannot be blank. Currency Sign is a compulsory information."), enMsgBoxStyle.Information) '?2
                txtCurrencyName.Select()
                Return False
            End If

            If txtExchangeRate1.Decimal <= 0 OrElse _
                txtExchangeRate2.Decimal <= 0 OrElse _
                txtExchangeRate.Decimal <= 0 Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please specify Exchange Rate greater than zero."), enMsgBoxStyle.Information) '?3
                txtExchangeRate1.Select()
                Return False
            End If

            ''Varsha (13 Dec 2017) -- Start
            ''Enhancement: Allow to change Base currency Decimal Place
            'If menAction <> enAction.EDIT_ONE AndAlso dtpDateOfRate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date OrElse dtpDateOfRate.Value.Date < FinancialYear._Object._Database_Start_Date OrElse dtpDateOfRate.Value.Date > FinancialYear._Object._Database_End_Date Then
            If menAction <> enAction.EDIT_ONE AndAlso objExchangeRateData._Isbasecurrency = False AndAlso (dtpDateOfRate.Value.Date > ConfigParameter._Object._CurrentDateAndTime.Date OrElse dtpDateOfRate.Value.Date < FinancialYear._Object._Database_Start_Date OrElse dtpDateOfRate.Value.Date > FinancialYear._Object._Database_End_Date) Then
                '    'Varsha (13 Dec 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry! Date for Rate should be between current financial year start and end date and it should not be greater than current date."), enMsgBoxStyle.Information)
                dtpDateOfRate.Focus()
                Return False
            ElseIf menAction <> enAction.EDIT_ONE AndAlso CInt(cboCountry.SelectedValue) = Company._Object._Localization_Country Then
                Dim ds As DataSet = objExchangeRateData.getComboList("BaseCurrency", False, True)
                If ds.Tables("BaseCurrency").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not enter different rates for Base Currency."), enMsgBoxStyle.Information)
                    cboCountry.Focus()
                    Return False
                End If
                objExchangeRateData._Isbasecurrency = True
            End If

            Dim ObjMdata As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim iPeriod As Integer = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iPeriod = ObjMdata.getCurrentPeriodID(enModuleReference.Payroll, dtpDateOfRate.Value.Date, , FinancialYear._Object._YearUnkid)
            iPeriod = ObjMdata.getCurrentPeriodID(enModuleReference.Payroll, dtpDateOfRate.Value.Date, FinancialYear._Object._YearUnkid)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = iPeriod
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod
            'Sohail (21 Aug 2015) -- End

            'Varsha (13 Dec 2017) -- Start
            'Enhancement: Allow to change Base currency Decimal Place
            'If objPeriod._Statusid = enStatusType.Close Then
            If objPeriod._Statusid = enStatusType.Close AndAlso objExchangeRateData._Isbasecurrency = False Then
                'Varsha (13 Dec 2017) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot save this exchange rate of the selected date. Reason : Period is already closed for the selected date."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 05 FEB 2013 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmExchangeRate_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExchangeRateData = Nothing
    End Sub

    Private Sub frmExchangeRate_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmExchangeRate_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmExchangeRate_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objExchangeRateData = New clsExchangeRate
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            'objCurrency.Text = Parameter._Object.getBaseCurrencySign
            If Company._Object._Localization_Currency <> "" Then
                lblBasecurrency.Text = Company._Object._Localization_Currency
            End If

            'Call OtherSettings()
            Call setColor()

            Call FillCombos()
            radCurrencySignSuffix.Checked = True
            If menAction = enAction.EDIT_ONE Then
                objExchangeRateData._ExchangeRateunkid = mintExchangeRateUnkid
                dtpDateOfRate.Enabled = False 'Sohail (03 Sep 2012)
                Call GetValue()
            End If

            'Sohail (10 Jan 2020) -- Start
            'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
            txtCurrencyName.ReadOnly = True
            txtCurrencySign.ReadOnly = True
            'Sohail (10 Jan 2020) -- End

            cboCountry.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmExchangeRate_AddEdit_Load", mstrModuleName)
        End Try
    End Sub


    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExchangeRate.SetMessages()
            objfrm._Other_ModuleNames = "clsExchangeRate"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'S.SANDEEP [ 05 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If IsValid() = False Then Exit Sub
            'S.SANDEEP [ 05 FEB 2013 ] -- END

            Call SetValue()


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objExchangeRateData.Update()
            'Else
            '    blnFlag = objExchangeRateData.Insert()
            'End If
            If objExchangeRateData._Isbasecurrency = False Then
                If menAction = enAction.EDIT_ONE Then
                    'Sohail (03 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If objExchangeRateData.isExist(objExchangeRateData._Countryunkid, mdtExchangeDate) Then
                    If objExchangeRateData.isExist(objExchangeRateData._Countryunkid, dtpDateOfRate.Value) Then
                        'Sohail (03 Sep 2012) -- End
                        'S.SANDEEP [ 20 MARCH 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If mdblOldExRate1 <> objExchangeRateData._Exchange_Rate1 Or mdblOldExRate2 <> objExchangeRateData._Exchange_Rate2 Then
                        '    Dim strMessage As String = String.Empty
                        '    strMessage = Language.getMessage(mstrModuleName, 6, "The new Exchange rate is different from rate last define." & vbCrLf & " Do you wish to continue with this new rate?")
                        '    If eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
                        'End If
                        'Sohail (03 Sep 2012) -- End
                        'S.SANDEEP [ 20 MARCH 2012 ] -- END
                        blnFlag = objExchangeRateData.Update()
                    Else
                        'Sohail (03 Sep 2012) -- Start
                        'TRA - ENHANCEMENT
                        'If mintOldCountry = objExchangeRateData._Countryunkid Then
                        '    If mdblOldExRate1 <> objExchangeRateData._Exchange_Rate1 Or mdblOldExRate2 <> objExchangeRateData._Exchange_Rate2 Then
                        '        blnFlag = objExchangeRateData.Insert()
                        '        If blnFlag = True Then
                        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Exchange rate saved successfully."), enMsgBoxStyle.Information)
                        '        End If
                        '    Else
                        '        blnFlag = True
                        '    End If
                        'Else
                        '    blnFlag = True
                        'End If
                        blnFlag = objExchangeRateData.Insert()
                        If blnFlag = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Exchange rate was saved successfully."), enMsgBoxStyle.Information)
                        End If
                        'Sohail (03 Sep 2012) -- End
                    End If
                Else
                    blnFlag = objExchangeRateData.Insert()
                    If blnFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Exchange rate was saved successfully."), enMsgBoxStyle.Information)
                    End If
                End If
            Else
                'Sohail (11 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objExchangeRateData.Update()
                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objExchangeRateData.Update()
                Else
                    blnFlag = objExchangeRateData.Insert()
                End If
                'Sohail (11 Sep 2012) -- End
                'Me.Close()
                'Exit Sub
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'Sandeep | 04 JAN 2010 | -- Start
            If blnFlag = True Then
                If mblnIsBaseCurrency = True Then
                    'Sandeep [ 01 MARCH 2011 ] -- Start
                    'If objExchangeRateData._Digits_After_Decimal = 0 Then
                    '    GUI.fmtCurrency = "#,###,###,##0"
                    'Else
                    '    GUI.fmtCurrency = "#,###,###,##0." & New String("0", objExchangeRateData._Digits_After_Decimal)
                    'End If
                    If objExchangeRateData._Digits_After_Decimal = 0 Then
                        ConfigParameter._Object._Base_CurrencyId = mintExchangeRateUnkid
                        ConfigParameter._Object._CurrencyFormat = "#,###,###,###,###,###,###,###,###,##0"
                        ConfigParameter._Object.updateParam()
                        ConfigParameter._Object.Refresh()
                    Else
                        ConfigParameter._Object._Base_CurrencyId = mintExchangeRateUnkid
                        ConfigParameter._Object._CurrencyFormat = "#,###,###,###,###,###,###,###,###,##0." & New String("0", objExchangeRateData._Digits_After_Decimal)
                        ConfigParameter._Object.updateParam()
                        ConfigParameter._Object.Refresh()
                    End If
                    'Sandeep [ 01 MARCH 2011 ] -- End 
                End If
            End If
            GUI.fmtCurrency = ConfigParameter._Object._CurrencyFormat
            'Sandeep | 04 JAN 2010 | -- END 

            If blnFlag = False And objExchangeRateData._Message <> "" Then
                eZeeMsgBox.Show(objExchangeRateData._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objExchangeRateData = Nothing
                    objExchangeRateData = New clsExchangeRate
                    Call GetValue()
                    txtCurrencyName.Select()
                Else
                    mintExchangeRateUnkid = objExchangeRateData._ExchangeRateunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            cboCurrencySignName.SelectedValue = cboCountry.SelectedValue
            txtCurrencyName.Text = cboCurrencySignName.Text
            cboCurrencySign.SelectedValue = cboCountry.SelectedValue
            txtCurrencySign.Text = cboCurrencySign.Text
            If cboCountry.SelectedValue = Company._Object._Localization_Country Then
                txtExchangeRate1.Text = "1.000000000"
                txtExchangeRate2.Text = "1.000000000"
                txtExchangeRate.Text = "1.000000000"

                txtExchangeRate1.Enabled = False
                txtExchangeRate2.Enabled = False
                txtExchangeRate.Enabled = False
            Else
                txtExchangeRate1.Enabled = True
                txtExchangeRate2.Enabled = True
                txtExchangeRate.Enabled = True

            End If

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtCurrencySign_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrencySign.TextChanged
        objlblSign.Text = txtCurrencySign.Text
        If objExchangeRateData._Isbasecurrency Then
            txtExchangeRate1.Text = "1"
            txtExchangeRate2.Text = "1"
            lblBasecurrency.Text = txtCurrencySign.Text
            'ElseIf objExchangeRateData._Isbasecurrency = False Then
            '    lblBasecurrency.Text = txtCurrencySign.Text
        End If
    End Sub
#End Region


    Private Sub txtExchangeRate1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExchangeRate1.TextChanged, txtExchangeRate2.TextChanged
        Try
            Dim dblExchangeRate As Double = 0
            If txtExchangeRate1.Decimal <= 0 Then
                dblExchangeRate = txtExchangeRate2.Decimal
            Else
                dblExchangeRate = txtExchangeRate2.Decimal / txtExchangeRate1.Decimal
            End If
            txtExchangeRate.Text = dblExchangeRate.ToString
        Catch ex As Exception

        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbCurrencyInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCurrencyInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbCurrencyInfo.Text = Language._Object.getCaption(Me.gbCurrencyInfo.Name, Me.gbCurrencyInfo.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.lblSign.Text = Language._Object.getCaption(Me.lblSign.Name, Me.lblSign.Text)
            Me.lblBasecurrency.Text = Language._Object.getCaption(Me.lblBasecurrency.Name, Me.lblBasecurrency.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblDisplaySign.Text = Language._Object.getCaption(Me.lblDisplaySign.Name, Me.lblDisplaySign.Text)
            Me.lblDecimalPlace.Text = Language._Object.getCaption(Me.lblDecimalPlace.Name, Me.lblDecimalPlace.Text)
            Me.radCurrencySignSuffix.Text = Language._Object.getCaption(Me.radCurrencySignSuffix.Name, Me.radCurrencySignSuffix.Text)
            Me.radCurrencySignPrefix.Text = Language._Object.getCaption(Me.radCurrencySignPrefix.Name, Me.radCurrencySignPrefix.Text)
            Me.lblDateOfRate.Text = Language._Object.getCaption(Me.lblDateOfRate.Name, Me.lblDateOfRate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select a Country from the drop down.")
            Language.setMessage(mstrModuleName, 2, "Currency Name cannot be blank. Currency Name is a compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Please specify Exchange Rate greater than zero.")
            Language.setMessage(mstrModuleName, 4, "Currency Sign cannot be blank. Currency Sign is a compulsory information.")
            Language.setMessage(mstrModuleName, 5, "This Exchange rate was saved successfully.")
            Language.setMessage(mstrModuleName, 7, "Sorry! You can not enter different rates for Base Currency.")
            Language.setMessage(mstrModuleName, 8, "Sorry! Date for Rate should be between current financial year start and end date and it should not be greater than current date.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot save this exchange rate of the selected date. Reason : Period is already closed for the selected date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class