﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmPayslip_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPayslip_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objTnALeave As clsTnALeaveTran
    Private objPayroll As clsPayrollProcessTran
    Private mintTnALeaveUnkid As Integer = -1

    Private mdtPayPeriodEndDate As Date


    'Pinkal (15-Oct-2013) -- Start
    'Enhancement : TRA Changes
    Private mdtPayPeriodStartDate As Date
    'Pinkal (15-Oct-2013) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintTnALeaveUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintTnALeaveUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub GetValue()
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim objPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        'Sohail (21 Jun 2013) -- Start
        'TRA - ENHANCEMENT
        Dim objPPA As New clsPayActivity_Tran
        Dim dsPPA As DataSet
        'Sohail (21 Jun 2013) -- End
        'Sohail (12 Jul 2013) -- Start
        'TRA - ENHANCEMENT
        Dim decTotAmount As Decimal = 0
        Dim decTotNormHours As Decimal = 0
        Dim decTotOTHours As Decimal = 0
        Dim decTotNormAmt As Decimal = 0
        Dim decTotOTAmt As Decimal = 0
        'Sohail (12 Jul 2013) -- End

        Try
            With objTnALeave
                txtDate.Text = ._Processdate.ToShortDateString
                txtVoucherNo.Text = ._Voucherno
                'Sohail (04 Mar 2011) -- Start
                txtOpenBalance.Text = ._OpeningBalance.ToString(GUI.fmtCurrency)
                txtOpenBalance.Tag = ._OpeningBalance
                'Sohail (04 Mar 2011) -- End

                'Pay Year & Pay Period
                txtPayYear.Text = ""
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = ._Payperiodunkid
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = ._Payperiodunkid
                'Sohail (21 Aug 2015) -- End
                'Sohail (11 Sep 2010) -- Start
                'dsList = objYear.getComboListPAYYEAR("Year")
                dsList = objYear.Get_Database_Year_List("Year", True, Company._Object._Companyunkid)
                'Sohail (11 Sep 2010) -- End
                dtTable = New DataView(dsList.Tables("Year"), "yearunkid = " & objPeriod._Yearunkid & "", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    txtPayYear.Text = dtTable.Rows(0).Item("financialyear_name").ToString
                End If
                txtPayPeriod.Text = objPeriod._Period_Name
                'Sohail (11 Aug 2010) -- Start
                mdtPayPeriodEndDate = objPeriod._End_Date
                'Sohail (11 Aug 2010) -- End


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes
                mdtPayPeriodStartDate = objPeriod._Start_Date
                'Pinkal (15-Oct-2013) -- End


                'FOR EMPLOYEE DETAIL
                GetEmployeeDetail(._Employeeunkid)

                'PAYROLL DETAIL
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPayroll.GetList("Payroll", 0, 0, mintTnALeaveUnkid)
                dsList = objPayroll.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payroll", 0, 0, mintTnALeaveUnkid, True, "")
                'Sohail (21 Aug 2015) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'FillPayslipList(dsList)
                FillPayslipList(dsList, mdtPayPeriodEndDate)
                'S.SANDEEP [04 JUN 2015] -- END


                'Sohail (21 Jun 2013) -- Start
                '*** PAY ACTIVITY DETAIL  ***
                If ConfigParameter._Object._ApplyPayPerActivity = True Then
                    dsPPA = objPPA.GetList("PPA", ._Employeeunkid, ._Payperiodunkid, , , , True)
                    Dim row As List(Of DataRow) = (From tbl In dsPPA.Tables(0).AsEnumerable Order By tbl.Item("activityname"), tbl.Item("ADate") Select (tbl)).ToList
                    lvPPA.Items.Clear()
                    Dim lvItem As ListViewItem
                    For Each rw In row
                        lvItem = New ListViewItem

                        lvItem.Text = rw.Item("activityname").ToString

                        lvItem.SubItems.Add(rw.Item("employeename").ToString)
                        lvItem.SubItems.Add(rw.Item("costcentername").ToString)
                        lvItem.SubItems.Add(eZeeDate.convertDate(rw.Item("ADate").ToString).ToShortDateString)
                        lvItem.SubItems.Add(Format(rw.Item("activity_value"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(rw.Item("measure").ToString)
                        lvItem.SubItems.Add(Format(rw.Item("activity_rate"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("amount"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("normal_factor"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("ph_factor"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("normal_hrs"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("ot_hrs"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("normal_amount"), GUI.fmtCurrency))
                        lvItem.SubItems.Add(Format(rw.Item("ot_amount"), GUI.fmtCurrency))

                        'Sohail (12 Jul 2013) -- Start
                        'TRA - ENHANCEMENT
                        decTotAmount += CDec(Format(rw.Item("amount"), GUI.fmtCurrency))
                        decTotNormHours += CDec(Format(rw.Item("normal_hrs"), GUI.fmtCurrency))
                        decTotOTHours += CDec(Format(rw.Item("ot_hrs"), GUI.fmtCurrency))
                        decTotNormAmt += CDec(Format(rw.Item("normal_amount"), GUI.fmtCurrency))
                        decTotOTAmt += CDec(Format(rw.Item("ot_amount"), GUI.fmtCurrency))
                        'Sohail (12 Jul 2013) -- End

                        lvPPA.Items.Add(lvItem)
                    Next

                    'Sohail (12 Jul 2013) -- Start
                    '***   For Grand Total  ***
                    If lvPPA.Items.Count > 0 Then
                        lvItem = New ListViewItem
                        lvItem.UseItemStyleForSubItems = False

                        lvItem.Text = ""

                        lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))

                        lvItem.SubItems.Add(decTotAmount.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add(decTotNormHours.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add(decTotOTHours.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add(decTotNormAmt.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add(decTotOTAmt.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold))

                        lvPPA.Items.Add(lvItem)
                    End If
                    'Sohail (12 Jul 2013) -- End

                    lvPPA.GroupingColumn = colhPPA
                    lvPPA.DisplayGroups(True)

                    If lvPPA.Items.Count > 5 Then
                        colhPPAEmp.Width = 120 - 18
                    Else
                        colhPPAEmp.Width = 120
                    End If
                Else
                    tabPayslip.TabPages.Remove(tabpPPA)
                End If

                'Sohail (21 Jun 2013) -- End

                'FOR PAYROLL PERIOD WORK DETAIL
                txtDaysInPeriod.Text = objTnALeave._Daysinperiod.ToString
                txtPayableDay.Text = objTnALeave._Totalpayabledays.ToString(GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                txtHolidayInMonth.Text = objTnALeave._Paidholidays.ToString
                txtPayableHr.Text = CalculateTime(True, CInt(objTnALeave._Totalpayablehours)).ToString(GUI.fmtCurrency).Replace(".", ":") 'Sohail (01 Dec 2010)

                'FOR PAYROLL EMPLOYEE WORK DETAIL
                txtWorkedDays.Text = objTnALeave._Totaldaysworked.ToString
                txtWorkedHours.Text = CalculateTime(True, CInt(objTnALeave._Totalhoursworked)).ToString(GUI.fmtCurrency).Replace(".", ":") 'Sohail (01 Dec 2010)
                txtAbsent.Text = objTnALeave._Totalabsentorleave.ToString
                txtShortHours.Text = CalculateTime(True, CInt(objTnALeave._Totalshorthours)).ToString(GUI.fmtCurrency).Replace(".", ":") 'Sohail (01 Dec 2010)
                txtExtraHours.Text = CalculateTime(True, CInt(objTnALeave._Totalextrahours)).ToString(GUI.fmtCurrency).Replace(".", ":") 'Sohail (01 Dec 2010)
                'Sohail (16 Aug 2010) -- Start
                txtDaysOnHold.Text = objTnALeave._Totalonholddays.ToString
                'Sohail (16 Aug 2010) -- End
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            dsList = Nothing
            dtTable = Nothing
            objPeriod = Nothing
            objYear = Nothing
            objPPA = Nothing 'Sohail (21 Jun 2013)
        End Try
    End Sub

    Private Sub GetEmployeeDetail(ByVal intEmployeeunkid As Integer)
        Dim objEmployee As New clsEmployee_Master
        Dim objGradeGroup As New clsGradeGroup
        Dim objGrade As New clsGrade
        Dim objGradeLevel As New clsGradeLevel
        Dim objMaster As New clsMasterData
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet
        Try
            imgImageControl._FileName = ""

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = intEmployeeunkid
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmployeeunkid
            objEmployee._Employeeunkid(mdtPayPeriodEndDate) = intEmployeeunkid
            'Sohail (18 Apr 2016) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            txtEmployee.Text = Trim(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname)
            objGradeGroup._Gradegroupunkid = objEmployee._Gradegroupunkid
            txtGradeGroup.Text = objGradeGroup._Name
            objGrade._Gradeunkid = objEmployee._Gradeunkid
            txtGrade.Text = objGrade._Name
            objGradeLevel._Gradelevelunkid = objEmployee._Gradelevelunkid
            txtGradeLevel.Text = objGradeLevel._Name
            'Sohail (11 Aug 2010) -- Start
            'txtScale.Text = objEmployee._Scale.ToString
            'txtScale.Text = Format(objMaster.Get_Current_Scale(intEmployeeunkid, mdtPayPeriodEndDate), "#0.00")
            dsList = objMaster.Get_Current_Scale("Salary", intEmployeeunkid, mdtPayPeriodEndDate)
            If dsList.Tables("Salary").Rows.Count > 0 Then
                txtScale.Text = Format(dsList.Tables("Salary").Rows(0).Item("newscale"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                'Sohail (20 Nov 2014) -- Start
                'TRA ISSUE - Last grade were showing in previous period payslip as well due to picking from hremployeemaster.
                txtGradeGroup.Text = dsList.Tables("Salary").Rows(0).Item("GradeGroup").ToString
                txtGrade.Text = dsList.Tables("Salary").Rows(0).Item("Grade").ToString
                txtGradeLevel.Text = dsList.Tables("Salary").Rows(0).Item("GradeLevel").ToString
                'Sohail (20 Nov 2014) -- End
            Else
                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, "Scale")
                dsList = objWages.getScaleInfo(objEmployee._Gradeunkid, objEmployee._Gradelevelunkid, mdtPayPeriodEndDate, "Scale")
                'Sohail (27 Apr 2016) -- End
                If dsList.Tables("Scale").Rows.Count > 0 Then
                    txtScale.Text = Format(dsList.Tables("Scale").Rows(0).Item("Salary"), GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                Else
                    txtScale.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                End If
            End If

            'Sohail (11 Aug 2010) -- End

            'FOR EMPLOYEE TYPE
            Dim objCommon As New clsCommon_Master
            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
            txtType.Text = objCommon._Name

            'FOR EMPLOYEE SECTION
            Dim objSection As New clsSections
            objSection._Sectionunkid = objEmployee._Sectionunkid
            txtSection.Text = objSection._Name

            'FOR EMPLOYEE DEPARTMENT
            Dim objdepartment As New clsDepartment
            objdepartment._Departmentunkid = objEmployee._Departmentunkid
            txtDepartment.Text = objdepartment._Name

            'FOR EMPLOYEE UNIT
            Dim objUnit As New clsUnits
            objUnit._Unitunkid = objEmployee._Unitunkid
            txtUnit.Text = objUnit._Name

            'FOR EMPLOYEE IMAGE

            'Anjan (22 Feb 2011)-Start
            'Issue : On windows 2003 server this folder of MyPictures is not there , so it gives error.
            'imgImageControl._FilePath = My.Computer.FileSystem.SpecialDirectories.MyPictures
            imgImageControl._FilePath = ConfigParameter._Object._PhotoPath

            'Anjan (22 Feb 2011)-End
            imgImageControl._FileName = imgImageControl._FileName & "\" & objEmployee._ImagePath

            'Sohail (09 Oct 2010) -- Start


            'Pinkal (15-Oct-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objShift As New clsNewshift_master
            'objShift._Shiftunkid = objEmployee._Shiftunkid
            'txtShift.Text = objShift._Shiftname
            'objShift = Nothing

            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim mstrShift As String = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'Dim dtList As DataTable = objEmpShift.Get_List("List", mdtPayPeriodStartDate, mdtPayPeriodEndDate, objEmployee._Employeeunkid)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtList As DataTable = objEmpShift.Get_List(FinancialYear._Object._DatabaseName, _
            '                                               User._Object._Userunkid, _
            '                                               FinancialYear._Object._YearUnkid, _
            '                                               Company._Object._Companyunkid, _
            '                                               ConfigParameter._Object._UserAccessModeSetting, "List", _
            '                                               mdtPayPeriodStartDate, _
            '                                               mdtPayPeriodEndDate, _
            '                                               objEmployee._Employeeunkid)

            Dim dtList As DataTable = objEmpShift.Get_List(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           ConfigParameter._Object._UserAccessModeSetting, "List", _
                                                           mdtPayPeriodStartDate, _
                                                           mdtPayPeriodEndDate, _
                                                           objEmployee._Employeeunkid(mdtPayPeriodEndDate))
            'S.SANDEEP [04 JUN 2015] -- END
            
            'Shani(24-Aug-2015) -- End
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'If dtList.Select("effectivedate = '" & mdtPayPeriodStartDate & "' ").Length <= 0 Then
            If dtList.Select("effectivedate = '" & eZeeDate.convertDate(mdtPayPeriodStartDate) & "' ").Length <= 0 Then
                'Sohail (18 Apr 2016) -- End

                Dim dRow As DataRow = dtList.NewRow
                dRow.Item("effectivedate") = eZeeDate.convertDate(mdtPayPeriodStartDate)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dRow.Item("shiftunkid") = objEmpShift.GetEmployee_Current_ShiftId(mdtPayPeriodStartDate, objEmployee._Employeeunkid)
                dRow.Item("shiftunkid") = objEmpShift.GetEmployee_Current_ShiftId(mdtPayPeriodStartDate, objEmployee._Employeeunkid(mdtPayPeriodEndDate))
                'S.SANDEEP [04 JUN 2015] -- END

                Dim objShift As New clsNewshift_master
                objShift._Shiftunkid = CInt(dRow.Item("shiftunkid"))
                dRow.Item("shiftname") = objShift._Shiftname
                dtList.Rows.InsertAt(dRow, 0)
            End If


            Dim lvItem As ListViewItem = Nothing
            For Each drRow As DataRow In dtList.Rows
                If mstrShift.Trim.Contains(drRow("shiftname").ToString().Trim) = False Then
                    lvItem = New ListViewItem()
                    lvItem.Text = drRow("shiftname").ToString()
                    lvItem.Tag = drRow("shiftunkid").ToString()
                    lvShift.Items.Add(lvItem)
                    mstrShift &= drRow("shiftname").ToString().Trim & ","
                End If
            Next

            objEmpShift = Nothing

            'Pinkal (15-Oct-2013) -- End


            'Sohail (09 Oct 2010) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeDetail", mstrModuleName)
        Finally
            If objEmployee IsNot Nothing Then objEmployee = Nothing
            If objGrade IsNot Nothing Then objGrade = Nothing
            If objGradeGroup IsNot Nothing Then objGradeGroup = Nothing
            If objGradeLevel IsNot Nothing Then objGradeLevel = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub FillPayslipList(ByVal dsList As DataSet, ByVal mdtPayPeriodEndDate As DateTime)
        'Private Sub FillPayslipList(ByVal dsList As DataSet)
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objEmployee As New clsEmployee_Master
        'Sohail (18 Apr 2016) -- Start
        'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
        'Dim objCostCenter As New clscostcenter_master
        'Dim objEmpCostCenter As New clsemployee_costcenter_Tran
        'Sohail (18 Apr 2016) -- End

        Dim i As Integer = 1
        Dim mdecAddtion As Decimal = CDec(0.0) 'Sohail (11 May 2011)
        Dim mdecDeduction As Decimal = CDec(0.0) 'Sohail (11 May 2011)
        'Dim strCostCenterName As String = "" 'Sohail (18 Apr 2016)
        Dim decAmount As Decimal 'Sohail (11 May 2011)
        Try
            If dsList IsNot Nothing Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim lvitem As ListViewItem
                    lvPayslip.Items.Clear()

                    'Sohail (11 Sep 2010) -- Start

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = CInt(dsList.Tables(0).Rows(0).Item("employeeunkid").ToString)
                    objEmployee._Employeeunkid(mdtPayPeriodEndDate) = CInt(dsList.Tables(0).Rows(0).Item("employeeunkid").ToString)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (18 Apr 2016) -- Start
                    'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
                    'objCostCenter._Costcenterunkid = objEmployee._Costcenterunkid
                    'strCostCenterName = objCostCenter._Costcentername
                    'Sohail (18 Apr 2016) -- End
                    'Sohail (11 Sep 2010) -- End

                    For Each drRow As DataRow In dsList.Tables(0).Rows
                        lvitem = New ListViewItem()
                        lvitem.Text = i.ToString
                        lvitem.Tag = drRow("payrollprocesstranunkid").ToString()
                        'Sohail (03 Nov 2010) -- Start
                        'lvitem.SubItems.Add(drRow("trnheadname").ToString)
                        'lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString) 'Sohail (11 Sep 2010)
                        'Sohail (19 Nov 2010) -- Start
                        'Dim objBenefit As New clsBenefitCoverage_tran
                        'Dim ds As DataSet = objBenefit.GetList("Benefit", CInt(dsList.Tables(0).Rows(0).Item("employeeunkid").ToString), 0, CInt(drRow("tranheadunkid").ToString))
                        'If ds.Tables("Benefit").Rows.Count > 0 Then
                        '    lvitem.SubItems.Add(ds.Tables("Benefit").Rows(0).Item("BPlanName").ToString)
                        '    lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString)
                        'Else
                        '    lvitem.SubItems.Add(drRow("trnheadname").ToString)
                        '    lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString)
                        'End If
                        'Sohail (31 Dec 2018) -- Start
                        'NMB Enhancement - 76.1 - Displayed earning on payslip mapped from benefit should take the name of the transaction head.
                        'If CInt(drRow("tranheadunkid").ToString) > 0 Then
                        '    Dim objBenefit As New clsBenefitCoverage_tran
                        '    'S.SANDEEP [04 JUN 2015] -- START
                        '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '    'Dim ds As DataSet = objBenefit.GetList("Benefit", CInt(dsList.Tables(0).Rows(0).Item("employeeunkid").ToString), 0, CInt(drRow("tranheadunkid").ToString))
                        '    Dim ds As DataSet = objBenefit.GetList(FinancialYear._Object._DatabaseName, _
                        '                                           User._Object._Userunkid, _
                        '                                           FinancialYear._Object._YearUnkid, _
                        '                                           Company._Object._Companyunkid, _
                        '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                        '                                           ConfigParameter._Object._IsIncludeInactiveEmp, "Benefit", , _
                        '                                           CInt(dsList.Tables(0).Rows(0).Item("employeeunkid").ToString), 0, _
                        '                                           CInt(drRow("tranheadunkid").ToString))
                        '    'S.SANDEEP [04 JUN 2015] -- END
                        '    If ds.Tables("Benefit").Rows.Count > 0 Then
                        '        lvitem.SubItems.Add(ds.Tables("Benefit").Rows(0).Item("BPlanName").ToString)
                        '        lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString)
                        '    Else
                        '        lvitem.SubItems.Add(drRow("trnheadname").ToString)
                        '        lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString)
                        '    End If
                        '    ds = Nothing
                        '    objBenefit = Nothing
                        'Else
                        'Sohail (31 Dec 2018) -- End
                                lvitem.SubItems.Add(drRow("trnheadname").ToString)
                                lvitem.SubItems(colhTranHead.Index).Tag = CInt(drRow("tranheadunkid").ToString)
                        'End If 'Sohail (31 Dec 2018)
                        'Sohail (19 Nov 2010) -- End
                        'ds = Nothing
                        'objBenefit = Nothing
                        'Sohail (03 Nov 2010) -- End

                        'Sohail (11 Sep 2010) -- Start
                        'Sohail (13 Sep 2011) -- Start
                        'dsFill = objEmpCostCenter.GetList("EmpCC", True, CInt(drRow("employeeunkid").ToString), CInt(drRow("tranheadunkid").ToString))
                        'If CInt(drRow("tranheadunkid").ToString) > 0 AndAlso dsFill.Tables("EmpCC").Rows.Count > 0 Then
                        '    lvitem.SubItems.Add(dsFill.Tables("EmpCC").Rows(0).Item("costcentername").ToString)
                        'Else
                        '    lvitem.SubItems.Add(strCostCenterName)
                        'End If
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        'lvitem.SubItems.Add(drRow("costcentername").ToString)
                        lvitem.SubItems.Add(drRow("allocationbyname").ToString & " : " & drRow("allocationname").ToString)
                        'Sohail (07 Feb 2019) -- End
                        'Sohail (13 Sep 2011) -- End
                        'Sohail (11 Sep 2010) -- End

                        If drRow("add_deduct").ToString IsNot DBNull.Value And drRow("add_deduct") IsNot Nothing Then
                            decAmount = CDec(drRow("amount").ToString) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                            If CInt(drRow("add_deduct")) = enAddDeduct.Add Then
                                lvitem.SubItems.Add(CDec(drRow("amount")).ToString(GUI.fmtCurrency)) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                'Sohail (17 Aug 2012) -- Start
                                'TRA - ENHANCEMENT
                                'mdecAddtion = mdecAddtion + CDec(drRow("amount").ToString()) 'Sohail (11 May 2011)
                                mdecAddtion = mdecAddtion + CDec(Format(drRow("amount"), GUI.fmtCurrency))
                                'Sohail (17 Aug 2012) -- End

                            ElseIf CInt(drRow("add_deduct")) = enAddDeduct.Deduct Then
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                lvitem.SubItems.Add(CDec(drRow("amount")).ToString(GUI.fmtCurrency)) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                'Sohail (17 Aug 2012) -- Start
                                'TRA - ENHANCEMENT
                                'mdecDeduction = mdecDeduction + CDec(drRow("amount").ToString()) 'Sohail (11 May 2011)
                                mdecDeduction = mdecDeduction + CDec(Format(drRow("amount"), GUI.fmtCurrency))
                                'Sohail (17 Aug 2012) -- End

                            ElseIf CInt(drRow("add_deduct")) = enAddDeduct.Do_Nothing Then
                                'Sohail (09 Oct 2010) -- Start
                                'Continue For
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                                lvitem.SubItems.Add(CDec(drRow("amount")).ToString(GUI.fmtCurrency)) 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                                'Sohail (09 Oct 2010) -- End
                            End If
                        Else
                            lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                            lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                            'Sohail (16 Oct 2010) -- Start
                            lvitem.SubItems.Add(Format(0, GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                            decAmount = 0
                            'Sohail (16 Oct 2010) -- End
                        End If

                        'Sohail (16 Oct 2010) -- Start
                        'lvPayslip.Items.Add(lvitem)
                        'i += 1
                        If chkIgnoreZero.Checked = False Or decAmount <> 0 Then
                            lvPayslip.Items.Add(lvitem)
                            i += 1
                        End If
                        'Sohail (16 Oct 2010) -- End
                    Next

                    'FOR SUM OF ADDTION
                    txtTotaladdition.Text = mdecAddtion.ToString(GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                    txtTotaldeduction.Text = mdecDeduction.ToString(GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                    'Anjan 29 Dec 2010)-Start
                    txtNetsalary.Text = Rounding.BRound((mdecAddtion - mdecDeduction), ConfigParameter._Object._RoundOff_Type).ToString(GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                    'Anjan (29 Dec 2010)-End
                    txtNetSalaryWithOpenBal.Text = Rounding.BRound((CDec(txtOpenBalance.Tag) + mdecAddtion - mdecDeduction), ConfigParameter._Object._RoundOff_Type).ToString(GUI.fmtCurrency) 'Sohail (04 Mar 2011), 'Sohail (11 May 2011)

                    'Sohail (12 Aug 2010) -- Start
                    If lvPayslip.Items.Count > 9 Then 'Sohail (16 Oct 2010)
                        colhTranHead.Width = 180 - 18 'Sohail (11 Sep 2010)
                    Else
                        colhTranHead.Width = 180 'Sohail (11 Sep 2010)
                    End If
                    'Sohail (12 Aug 2010) -- End
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPayslipList", mstrModuleName)
        Finally
            objEmployee = Nothing
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'objCostCenter = Nothing
            'objEmpCostCenter = Nothing
            'Sohail (18 Apr 2016) -- End
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPayslip_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTnALeave = New clsTnALeaveTran
        objPayroll = New clsPayrollProcessTran
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            If menAction = enAction.EDIT_ONE Then
                objTnALeave._Tnaleavetranunkid(Nothing) = mintTnALeaveUnkid
            End If
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslip_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslip_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Call btnClose.PerformClick()
                Case Keys.P
                    If e.Control = True Then
                        Call btnPrint.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslip_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslip_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objTnALeave = Nothing
            objPayroll = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslip_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPrint_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (01 Dec 2010) -- Start
#Region " Listview Events "
    'Sohail (11 Sep 2010) -- Start
    Private Sub lvPayslip_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPayslip.SelectedIndexChanged

        Try
            txtTranHeadFormula.Text = "" 'Sohail (04 Mar 2011)
            txtTranHeadFormula.Visible = False 'Sohail (04 Mar 2011)
            If lvPayslip.SelectedItems.Count > 0 AndAlso CInt(lvPayslip.Items(lvPayslip.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag) > 0 Then
                'Sohail (12 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                'Dim objTranHead As New clsTransactionHead
                'objTranHead._Tranheadunkid = CInt(lvPayslip.Items(lvPayslip.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag)
                ''Sohail (04 Mar 2011) -- Start
                ''If objTranHead._Formula.Trim.Length > 0 Then txtTranHeadFormula.Text = "Tran. Head Formula : " & objTranHead._Formula 
                'If objTranHead._Formula.Trim.Length > 0 Then
                '    txtTranHeadFormula.Text = "Tran. Head Formula : " & objTranHead._Formula
                '    txtTranHeadFormula.Visible = True
                'End If
                ''Sohail (04 Mar 2011) -- End
                'objTranHead = Nothing
                Dim objFormulaSlab As New clsTranhead_formula_slab_Tran
                Dim dsList As DataSet = objFormulaSlab.GetList("List", CInt(lvPayslip.Items(lvPayslip.SelectedItems(0).Index).SubItems(colhTranHead.Index).Tag), mdtPayPeriodEndDate)
                If dsList.Tables("List").Rows.Count > 0 Then
                    txtTranHeadFormula.Text = "Tran. Head Formula : " & dsList.Tables("List").Rows(0).Item("formula").ToString
                    txtTranHeadFormula.Visible = True
                End If
                objFormulaSlab = Nothing
                'Sohail (12 Apr 2013) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayslip_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End
#End Region

#Region " CheckBox's Events "
    'Sohail (16 Oct 2010) -- Start
    Private Sub chkIgnoreZero_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIgnoreZero.CheckedChanged
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPayroll.GetList("Payroll", 0, 0, mintTnALeaveUnkid)
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'dsList = objPayroll.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payroll", 0, 0, mintTnALeaveUnkid, True, "")
            If chkIgnoreZero.Checked = True Then
                dsList = objPayroll.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payroll", 0, 0, mintTnALeaveUnkid, True, " prpayrollprocess_tran.amount <> 0 ")
            Else
            dsList = objPayroll.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payroll", 0, 0, mintTnALeaveUnkid, True, "")
            End If
            'Sohail (18 Apr 2016) -- End
            'Sohail (21 Aug 2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'FillPayslipList(dsList)
            FillPayslipList(dsList, mdtPayPeriodEndDate)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIgnoreZero_CheckedChanged", mstrModuleName)
        Finally
            dsList = Nothing
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

#End Region
    'Sohail (01 Dec 2010) -- End

#Region " Message List "

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbPayslipInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPayslipInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeinformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeinformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPeriodWorkDetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPeriodWorkDetail.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeWorkDetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeWorkDetail.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnPrint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrint.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbPayslipInfo.Text = Language._Object.getCaption(Me.gbPayslipInfo.Name, Me.gbPayslipInfo.Text)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPayyear.Text = Language._Object.getCaption(Me.lblPayyear.Name, Me.lblPayyear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.Name, Me.lblScale.Text)
            Me.gbEmployeeinformation.Text = Language._Object.getCaption(Me.gbEmployeeinformation.Name, Me.gbEmployeeinformation.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
            Me.lblEmployeeImage.Text = Language._Object.getCaption(Me.lblEmployeeImage.Name, Me.lblEmployeeImage.Text)
            Me.gbPeriodWorkDetail.Text = Language._Object.getCaption(Me.gbPeriodWorkDetail.Name, Me.gbPeriodWorkDetail.Text)
            Me.lblDaysinPeriod.Text = Language._Object.getCaption(Me.lblDaysinPeriod.Name, Me.lblDaysinPeriod.Text)
            Me.lblHolidays.Text = Language._Object.getCaption(Me.lblHolidays.Name, Me.lblHolidays.Text)
            Me.lblPayableDays.Text = Language._Object.getCaption(Me.lblPayableDays.Name, Me.lblPayableDays.Text)
            Me.lblPayableHours.Text = Language._Object.getCaption(Me.lblPayableHours.Name, Me.lblPayableHours.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpPayslip.Text = Language._Object.getCaption(Me.tabpPayslip.Name, Me.tabpPayslip.Text)
            Me.lblTotaladdition.Text = Language._Object.getCaption(Me.lblTotaladdition.Name, Me.lblTotaladdition.Text)
            Me.lblTotaldeduction.Text = Language._Object.getCaption(Me.lblTotaldeduction.Name, Me.lblTotaldeduction.Text)
            Me.lblNetsalary.Text = Language._Object.getCaption(Me.lblNetsalary.Name, Me.lblNetsalary.Text)
            Me.tabpEmployeeInfo.Text = Language._Object.getCaption(Me.tabpEmployeeInfo.Name, Me.tabpEmployeeInfo.Text)
            Me.gbEmployeeWorkDetail.Text = Language._Object.getCaption(Me.gbEmployeeWorkDetail.Name, Me.gbEmployeeWorkDetail.Text)
            Me.lblWorkedHr.Text = Language._Object.getCaption(Me.lblWorkedHr.Name, Me.lblWorkedHr.Text)
            Me.lblShortHour.Text = Language._Object.getCaption(Me.lblShortHour.Name, Me.lblShortHour.Text)
            Me.lblWorkedDays.Text = Language._Object.getCaption(Me.lblWorkedDays.Name, Me.lblWorkedDays.Text)
            Me.lblAbsent.Text = Language._Object.getCaption(Me.lblAbsent.Name, Me.lblAbsent.Text)
            Me.lblExtraHours.Text = Language._Object.getCaption(Me.lblExtraHours.Name, Me.lblExtraHours.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
            Me.imgImageControl.Text = Language._Object.getCaption(Me.imgImageControl.Name, Me.imgImageControl.Text)
            Me.colhSrNo.Text = Language._Object.getCaption(CStr(Me.colhSrNo.Tag), Me.colhSrNo.Text)
            Me.colhTranHead.Text = Language._Object.getCaption(CStr(Me.colhTranHead.Tag), Me.colhTranHead.Text)
            Me.colhAddition.Text = Language._Object.getCaption(CStr(Me.colhAddition.Tag), Me.colhAddition.Text)
            Me.colhDeduction.Text = Language._Object.getCaption(CStr(Me.colhDeduction.Tag), Me.colhDeduction.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblDaysOnHold.Text = Language._Object.getCaption(Me.lblDaysOnHold.Name, Me.lblDaysOnHold.Text)
            Me.colhCostCenter.Text = Language._Object.getCaption(CStr(Me.colhCostCenter.Tag), Me.colhCostCenter.Text)
            Me.colhInformational.Text = Language._Object.getCaption(CStr(Me.colhInformational.Tag), Me.colhInformational.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
            Me.lblOpenBalance.Text = Language._Object.getCaption(Me.lblOpenBalance.Name, Me.lblOpenBalance.Text)
            Me.lblNetSalaryWithOpenBal.Text = Language._Object.getCaption(Me.lblNetSalaryWithOpenBal.Name, Me.lblNetSalaryWithOpenBal.Text)
            Me.tabpPPA.Text = Language._Object.getCaption(Me.tabpPPA.Name, Me.tabpPPA.Text)
            Me.colhPPA.Text = Language._Object.getCaption(CStr(Me.colhPPA.Tag), Me.colhPPA.Text)
            Me.colhPPAEmp.Text = Language._Object.getCaption(CStr(Me.colhPPAEmp.Tag), Me.colhPPAEmp.Text)
            Me.colhPPADate.Text = Language._Object.getCaption(CStr(Me.colhPPADate.Tag), Me.colhPPADate.Text)
            Me.colhPPAValue.Text = Language._Object.getCaption(CStr(Me.colhPPAValue.Tag), Me.colhPPAValue.Text)
            Me.cplhPPAUoM.Text = Language._Object.getCaption(CStr(Me.cplhPPAUoM.Tag), Me.cplhPPAUoM.Text)
            Me.colhPPARate.Text = Language._Object.getCaption(CStr(Me.colhPPARate.Tag), Me.colhPPARate.Text)
            Me.colhPPAAmount.Text = Language._Object.getCaption(CStr(Me.colhPPAAmount.Tag), Me.colhPPAAmount.Text)
            Me.colhPPANormalFactor.Text = Language._Object.getCaption(CStr(Me.colhPPANormalFactor.Tag), Me.colhPPANormalFactor.Text)
            Me.colhPPAPHFactor.Text = Language._Object.getCaption(CStr(Me.colhPPAPHFactor.Tag), Me.colhPPAPHFactor.Text)
            Me.colhPPANormalHours.Text = Language._Object.getCaption(CStr(Me.colhPPANormalHours.Tag), Me.colhPPANormalHours.Text)
            Me.colhPPAOTHours.Text = Language._Object.getCaption(CStr(Me.colhPPAOTHours.Tag), Me.colhPPAOTHours.Text)
            Me.colhPPANormalAmount.Text = Language._Object.getCaption(CStr(Me.colhPPANormalAmount.Tag), Me.colhPPANormalAmount.Text)
            Me.colhPPAOTAmount.Text = Language._Object.getCaption(CStr(Me.colhPPAOTAmount.Tag), Me.colhPPAOTAmount.Text)
            Me.colhPPACCenter.Text = Language._Object.getCaption(CStr(Me.colhPPACCenter.Tag), Me.colhPPACCenter.Text)
            Me.colhShift.Text = Language._Object.getCaption(CStr(Me.colhShift.Tag), Me.colhShift.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

