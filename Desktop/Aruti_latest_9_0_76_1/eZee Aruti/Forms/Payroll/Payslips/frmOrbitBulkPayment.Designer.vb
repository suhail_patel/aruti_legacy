﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrbitBulkPayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrbitBulkPayment))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhiCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhblank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVoucherNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpBankBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpBankAcc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChequeNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompBankAccNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhrequestdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhreferenceno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhcustomer_number = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhprimaryactnumber = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolherror_description = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhemployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objlnkValue = New System.Windows.Forms.LinkLabel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlDate = New System.Windows.Forms.Panel
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblReqDateTo = New System.Windows.Forms.Label
        Me.lblReqDateFrom = New System.Windows.Forms.Label
        Me.lblRequestType = New System.Windows.Forms.Label
        Me.lblRequestMode = New System.Windows.Forms.Label
        Me.cboRequestType = New System.Windows.Forms.ComboBox
        Me.cboRequestMode = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.tblStatus = New System.Windows.Forms.TableLayoutPanel
        Me.lblProcessing = New System.Windows.Forms.Label
        Me.objlblValue = New System.Windows.Forms.Label
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.pbProgress_Report = New System.Windows.Forms.ProgressBar
        Me.btnPost = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnStopProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbgWorker = New System.ComponentModel.BackgroundWorker
        Me.pnlMain.SuspendLayout()
        Me.gbEmployee.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDate.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.tblStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbEmployee)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(882, 477)
        Me.pnlMain.TabIndex = 1
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbEmployee.Controls.Add(Me.objlnkValue)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(0, 70)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(871, 353)
        Me.gbEmployee.TabIndex = 314
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.Text = "Employee"
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(2, 26)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(869, 327)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(863, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(863, 295)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 22
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhiCheck, Me.objdgcolhblank, Me.dgcolhVoucherNo, Me.dgcolhEmployee, Me.dgcolhEmpBankBranch, Me.dgcolhEmpBankAcc, Me.dgcolhCurrency, Me.dgcolhChequeNo, Me.dgcolhCompBankAccNo, Me.dgcolhrequestdate, Me.dgcolhreferenceno, Me.dgcolhcustomer_number, Me.dgcolhprimaryactnumber, Me.dgcolherror_description, Me.objdgcolhemployeeunkid, Me.objdgcolhIsGroup})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(863, 295)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhiCheck
        '
        Me.objdgcolhiCheck.Frozen = True
        Me.objdgcolhiCheck.HeaderText = ""
        Me.objdgcolhiCheck.Name = "objdgcolhiCheck"
        Me.objdgcolhiCheck.Width = 25
        '
        'objdgcolhblank
        '
        Me.objdgcolhblank.HeaderText = ""
        Me.objdgcolhblank.Name = "objdgcolhblank"
        Me.objdgcolhblank.ReadOnly = True
        Me.objdgcolhblank.Width = 30
        '
        'dgcolhVoucherNo
        '
        Me.dgcolhVoucherNo.HeaderText = "VoucherNo"
        Me.dgcolhVoucherNo.Name = "dgcolhVoucherNo"
        Me.dgcolhVoucherNo.ReadOnly = True
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        '
        'dgcolhEmpBankBranch
        '
        Me.dgcolhEmpBankBranch.HeaderText = "Emp Bank Branch"
        Me.dgcolhEmpBankBranch.Name = "dgcolhEmpBankBranch"
        Me.dgcolhEmpBankBranch.ReadOnly = True
        '
        'dgcolhEmpBankAcc
        '
        Me.dgcolhEmpBankAcc.HeaderText = "Emp Bank Account No"
        Me.dgcolhEmpBankAcc.Name = "dgcolhEmpBankAcc"
        Me.dgcolhEmpBankAcc.ReadOnly = True
        '
        'dgcolhCurrency
        '
        Me.dgcolhCurrency.HeaderText = "Currency"
        Me.dgcolhCurrency.Name = "dgcolhCurrency"
        Me.dgcolhCurrency.ReadOnly = True
        '
        'dgcolhChequeNo
        '
        Me.dgcolhChequeNo.HeaderText = "ChequeNo"
        Me.dgcolhChequeNo.Name = "dgcolhChequeNo"
        Me.dgcolhChequeNo.ReadOnly = True
        '
        'dgcolhCompBankAccNo
        '
        Me.dgcolhCompBankAccNo.HeaderText = "Comp Bank Account No."
        Me.dgcolhCompBankAccNo.Name = "dgcolhCompBankAccNo"
        Me.dgcolhCompBankAccNo.ReadOnly = True
        '
        'dgcolhrequestdate
        '
        Me.dgcolhrequestdate.HeaderText = "Request Date/Time"
        Me.dgcolhrequestdate.Name = "dgcolhrequestdate"
        Me.dgcolhrequestdate.ReadOnly = True
        Me.dgcolhrequestdate.Width = 110
        '
        'dgcolhreferenceno
        '
        Me.dgcolhreferenceno.HeaderText = "Reference No"
        Me.dgcolhreferenceno.Name = "dgcolhreferenceno"
        Me.dgcolhreferenceno.ReadOnly = True
        '
        'dgcolhcustomer_number
        '
        Me.dgcolhcustomer_number.HeaderText = "Customer No."
        Me.dgcolhcustomer_number.Name = "dgcolhcustomer_number"
        Me.dgcolhcustomer_number.ReadOnly = True
        '
        'dgcolhprimaryactnumber
        '
        Me.dgcolhprimaryactnumber.HeaderText = "Account Number"
        Me.dgcolhprimaryactnumber.Name = "dgcolhprimaryactnumber"
        Me.dgcolhprimaryactnumber.ReadOnly = True
        '
        'dgcolherror_description
        '
        Me.dgcolherror_description.HeaderText = "Message"
        Me.dgcolherror_description.Name = "dgcolherror_description"
        Me.dgcolherror_description.ReadOnly = True
        Me.dgcolherror_description.Width = 200
        '
        'objdgcolhemployeeunkid
        '
        Me.objdgcolhemployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhemployeeunkid.Name = "objdgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.ReadOnly = True
        Me.objdgcolhemployeeunkid.Visible = False
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = ""
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'objlnkValue
        '
        Me.objlnkValue.BackColor = System.Drawing.Color.Transparent
        Me.objlnkValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkValue.ForeColor = System.Drawing.Color.White
        Me.objlnkValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkValue.LinkColor = System.Drawing.SystemColors.ControlText
        Me.objlnkValue.Location = New System.Drawing.Point(71, 317)
        Me.objlnkValue.Name = "objlnkValue"
        Me.objlnkValue.Size = New System.Drawing.Size(423, 17)
        Me.objlnkValue.TabIndex = 308
        Me.objlnkValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.pnlDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblRequestType)
        Me.gbFilterCriteria.Controls.Add(Me.lblRequestMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboRequestType)
        Me.gbFilterCriteria.Controls.Add(Me.cboRequestMode)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(882, 69)
        Me.gbFilterCriteria.TabIndex = 313
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Mandatory Info"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(528, 38)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(82, 16)
        Me.lblPeriod.TabIndex = 321
        Me.lblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(616, 36)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(193, 21)
        Me.cboPeriod.TabIndex = 320
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(844, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 318
        Me.objbtnReset.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(712, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(97, 15)
        Me.lnkAllocation.TabIndex = 305
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(818, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 317
        Me.objbtnSearch.TabStop = False
        '
        'pnlDate
        '
        Me.pnlDate.Controls.Add(Me.dtpEndDate)
        Me.pnlDate.Controls.Add(Me.dtpStartDate)
        Me.pnlDate.Controls.Add(Me.lblReqDateTo)
        Me.pnlDate.Controls.Add(Me.lblReqDateFrom)
        Me.pnlDate.Location = New System.Drawing.Point(818, 38)
        Me.pnlDate.Name = "pnlDate"
        Me.pnlDate.Size = New System.Drawing.Size(275, 25)
        Me.pnlDate.TabIndex = 38
        Me.pnlDate.Visible = False
        '
        'dtpEndDate
        '
        Me.dtpEndDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(227, 2)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpEndDate.TabIndex = 324
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(74, 2)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpStartDate.TabIndex = 322
        '
        'lblReqDateTo
        '
        Me.lblReqDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReqDateTo.Location = New System.Drawing.Point(189, 4)
        Me.lblReqDateTo.Name = "lblReqDateTo"
        Me.lblReqDateTo.Size = New System.Drawing.Size(32, 16)
        Me.lblReqDateTo.TabIndex = 323
        Me.lblReqDateTo.Text = "To"
        '
        'lblReqDateFrom
        '
        Me.lblReqDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReqDateFrom.Location = New System.Drawing.Point(3, 4)
        Me.lblReqDateFrom.Name = "lblReqDateFrom"
        Me.lblReqDateFrom.Size = New System.Drawing.Size(65, 16)
        Me.lblReqDateFrom.TabIndex = 321
        Me.lblReqDateFrom.Text = "From Date"
        '
        'lblRequestType
        '
        Me.lblRequestType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequestType.Location = New System.Drawing.Point(215, 38)
        Me.lblRequestType.Name = "lblRequestType"
        Me.lblRequestType.Size = New System.Drawing.Size(82, 16)
        Me.lblRequestType.TabIndex = 87
        Me.lblRequestType.Text = "Request Type"
        '
        'lblRequestMode
        '
        Me.lblRequestMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRequestMode.Location = New System.Drawing.Point(12, 38)
        Me.lblRequestMode.Name = "lblRequestMode"
        Me.lblRequestMode.Size = New System.Drawing.Size(82, 16)
        Me.lblRequestMode.TabIndex = 86
        Me.lblRequestMode.Text = "Request Mode"
        '
        'cboRequestType
        '
        Me.cboRequestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequestType.DropDownWidth = 180
        Me.cboRequestType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequestType.FormattingEnabled = True
        Me.cboRequestType.Location = New System.Drawing.Point(303, 36)
        Me.cboRequestType.Name = "cboRequestType"
        Me.cboRequestType.Size = New System.Drawing.Size(217, 21)
        Me.cboRequestType.TabIndex = 85
        '
        'cboRequestMode
        '
        Me.cboRequestMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRequestMode.DropDownWidth = 180
        Me.cboRequestMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRequestMode.FormattingEnabled = True
        Me.cboRequestMode.Location = New System.Drawing.Point(100, 36)
        Me.cboRequestMode.Name = "cboRequestMode"
        Me.cboRequestMode.Size = New System.Drawing.Size(109, 21)
        Me.cboRequestMode.TabIndex = 85
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.tblStatus)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.pbProgress_Report)
        Me.EZeeFooter1.Controls.Add(Me.btnPost)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnStopProcess)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 427)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(882, 50)
        Me.EZeeFooter1.TabIndex = 312
        '
        'tblStatus
        '
        Me.tblStatus.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tblStatus.ColumnCount = 2
        Me.tblStatus.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
        Me.tblStatus.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 370.0!))
        Me.tblStatus.Controls.Add(Me.lblProcessing, 0, 0)
        Me.tblStatus.Controls.Add(Me.objlblValue, 1, 0)
        Me.tblStatus.Location = New System.Drawing.Point(204, 13)
        Me.tblStatus.Name = "tblStatus"
        Me.tblStatus.RowCount = 1
        Me.tblStatus.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblStatus.Size = New System.Drawing.Size(464, 25)
        Me.tblStatus.TabIndex = 39
        Me.tblStatus.Visible = False
        '
        'lblProcessing
        '
        Me.lblProcessing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblProcessing.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessing.Location = New System.Drawing.Point(4, 1)
        Me.lblProcessing.Name = "lblProcessing"
        Me.lblProcessing.Size = New System.Drawing.Size(102, 23)
        Me.lblProcessing.TabIndex = 0
        Me.lblProcessing.Text = "Processed"
        Me.lblProcessing.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.Location = New System.Drawing.Point(113, 1)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(350, 23)
        Me.objlblValue.TabIndex = 1
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(23, 10)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 38
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'pbProgress_Report
        '
        Me.pbProgress_Report.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pbProgress_Report.Location = New System.Drawing.Point(0, 40)
        Me.pbProgress_Report.Name = "pbProgress_Report"
        Me.pbProgress_Report.Size = New System.Drawing.Size(882, 10)
        Me.pbProgress_Report.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress_Report.TabIndex = 36
        '
        'btnPost
        '
        Me.btnPost.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPost.BackColor = System.Drawing.Color.White
        Me.btnPost.BackgroundImage = CType(resources.GetObject("btnPost.BackgroundImage"), System.Drawing.Image)
        Me.btnPost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPost.BorderColor = System.Drawing.Color.Empty
        Me.btnPost.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPost.FlatAppearance.BorderSize = 0
        Me.btnPost.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPost.ForeColor = System.Drawing.Color.Black
        Me.btnPost.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPost.GradientForeColor = System.Drawing.Color.Black
        Me.btnPost.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPost.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPost.Location = New System.Drawing.Point(685, 11)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPost.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPost.Size = New System.Drawing.Size(90, 30)
        Me.btnPost.TabIndex = 33
        Me.btnPost.Text = "S&tart"
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(781, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnStopProcess
        '
        Me.btnStopProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStopProcess.BackColor = System.Drawing.Color.White
        Me.btnStopProcess.BackgroundImage = CType(resources.GetObject("btnStopProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnStopProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStopProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnStopProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStopProcess.Enabled = False
        Me.btnStopProcess.FlatAppearance.BorderSize = 0
        Me.btnStopProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStopProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStopProcess.ForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStopProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStopProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.Location = New System.Drawing.Point(23, 10)
        Me.btnStopProcess.Name = "btnStopProcess"
        Me.btnStopProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStopProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStopProcess.Size = New System.Drawing.Size(90, 30)
        Me.btnStopProcess.TabIndex = 37
        Me.btnStopProcess.Text = "&Stop"
        Me.btnStopProcess.UseVisualStyleBackColor = True
        Me.btnStopProcess.Visible = False
        '
        'objbgWorker
        '
        Me.objbgWorker.WorkerReportsProgress = True
        Me.objbgWorker.WorkerSupportsCancellation = True
        '
        'frmOrbitBulkPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(882, 477)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrbitBulkPayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmOrbitBulkPayment"
        Me.pnlMain.ResumeLayout(False)
        Me.gbEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDate.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.tblStatus.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objlnkValue As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents pnlDate As System.Windows.Forms.Panel
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReqDateTo As System.Windows.Forms.Label
    Friend WithEvents lblReqDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblRequestType As System.Windows.Forms.Label
    Friend WithEvents lblRequestMode As System.Windows.Forms.Label
    Friend WithEvents cboRequestType As System.Windows.Forms.ComboBox
    Friend WithEvents cboRequestMode As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents tblStatus As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblProcessing As System.Windows.Forms.Label
    Friend WithEvents objlblValue As System.Windows.Forms.Label
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents pbProgress_Report As System.Windows.Forms.ProgressBar
    Friend WithEvents btnPost As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnStopProcess As eZee.Common.eZeeLightButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents objdgcolhiCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhblank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVoucherNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpBankBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpBankAcc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChequeNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompBankAccNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhrequestdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhreferenceno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhcustomer_number As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhprimaryactnumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolherror_description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhemployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
