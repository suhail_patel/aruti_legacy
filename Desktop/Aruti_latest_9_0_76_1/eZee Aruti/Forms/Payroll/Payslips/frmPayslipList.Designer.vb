﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayslipList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayslipList))
        Me.gbPayslipList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeInactiveEmployee = New System.Windows.Forms.CheckBox
        Me.pnlPayslip = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvPayslip = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhTnALeaveID = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhPayYear = New System.Windows.Forms.ColumnHeader
        Me.colhPayPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhVoucher = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhTotalAmount = New System.Windows.Forms.ColumnHeader
        Me.colhPaidAmount = New System.Windows.Forms.ColumnHeader
        Me.colhBalAmount = New System.Windows.Forms.ColumnHeader
        Me.PnlPaysliplist = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.bdgUnpaid = New Aruti.Data.Badge
        Me.bdgTotalAuthorized = New Aruti.Data.Badge
        Me.bdgTotalPaid = New Aruti.Data.Badge
        Me.bdgTotalEmployee = New Aruti.Data.Badge
        Me.bdgProcessed = New Aruti.Data.Badge
        Me.bdgNotProcessed = New Aruti.Data.Badge
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.txtBalanceAmountTo = New eZee.TextBox.NumericTextBox
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.txtBalanceAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblAmountFrom = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalPayement = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalVoidPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.objSepPaymentApproval = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPaymentApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.objSepPaymentAuthorize = New System.Windows.Forms.ToolStripSeparator
        Me.mnuAuthorizePayment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidAuthorizedPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.objSepStatutoryPayment = New System.Windows.Forms.ToolStripSeparator
        Me.mnuStatutoryPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.lblUnProcessMsg = New System.Windows.Forms.Label
        Me.objlblUnProcessColor = New System.Windows.Forms.Label
        Me.lblOverDeduction = New System.Windows.Forms.Label
        Me.objlblColor = New System.Windows.Forms.Label
        Me.btnPrint = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnViewDetail = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.objEFooter = New eZee.Common.eZeeFooter
        Me.btnEmailClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.mnuOrbitBulkProcess = New System.Windows.Forms.ToolStripMenuItem
        Me.objSepOrbit = New System.Windows.Forms.ToolStripSeparator
        Me.gbPayslipList.SuspendLayout()
        Me.pnlPayslip.SuspendLayout()
        Me.PnlPaysliplist.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.objEFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbPayslipList
        '
        Me.gbPayslipList.BorderColor = System.Drawing.Color.Black
        Me.gbPayslipList.Checked = False
        Me.gbPayslipList.CollapseAllExceptThis = False
        Me.gbPayslipList.CollapsedHoverImage = Nothing
        Me.gbPayslipList.CollapsedNormalImage = Nothing
        Me.gbPayslipList.CollapsedPressedImage = Nothing
        Me.gbPayslipList.CollapseOnLoad = False
        Me.gbPayslipList.Controls.Add(Me.chkIncludeInactiveEmployee)
        Me.gbPayslipList.Controls.Add(Me.pnlPayslip)
        Me.gbPayslipList.ExpandedHoverImage = Nothing
        Me.gbPayslipList.ExpandedNormalImage = Nothing
        Me.gbPayslipList.ExpandedPressedImage = Nothing
        Me.gbPayslipList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayslipList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayslipList.HeaderHeight = 25
        Me.gbPayslipList.HeaderMessage = ""
        Me.gbPayslipList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayslipList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayslipList.HeightOnCollapse = 0
        Me.gbPayslipList.LeftTextSpace = 0
        Me.gbPayslipList.Location = New System.Drawing.Point(12, 186)
        Me.gbPayslipList.Name = "gbPayslipList"
        Me.gbPayslipList.OpenHeight = 300
        Me.gbPayslipList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayslipList.ShowBorder = True
        Me.gbPayslipList.ShowCheckBox = False
        Me.gbPayslipList.ShowCollapseButton = False
        Me.gbPayslipList.ShowDefaultBorderColor = True
        Me.gbPayslipList.ShowDownButton = False
        Me.gbPayslipList.ShowHeader = True
        Me.gbPayslipList.Size = New System.Drawing.Size(941, 303)
        Me.gbPayslipList.TabIndex = 1
        Me.gbPayslipList.Temp = 0
        Me.gbPayslipList.Text = "Payslip List"
        Me.gbPayslipList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeInactiveEmployee
        '
        Me.chkIncludeInactiveEmployee.AutoSize = True
        Me.chkIncludeInactiveEmployee.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeInactiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmployee.Location = New System.Drawing.Point(772, 4)
        Me.chkIncludeInactiveEmployee.Name = "chkIncludeInactiveEmployee"
        Me.chkIncludeInactiveEmployee.Size = New System.Drawing.Size(157, 17)
        Me.chkIncludeInactiveEmployee.TabIndex = 75
        Me.chkIncludeInactiveEmployee.Text = "Include Inactive Employees"
        Me.chkIncludeInactiveEmployee.UseVisualStyleBackColor = False
        '
        'pnlPayslip
        '
        Me.pnlPayslip.Controls.Add(Me.objchkSelectAll)
        Me.pnlPayslip.Controls.Add(Me.lvPayslip)
        Me.pnlPayslip.Location = New System.Drawing.Point(3, 27)
        Me.pnlPayslip.Name = "pnlPayslip"
        Me.pnlPayslip.Size = New System.Drawing.Size(935, 275)
        Me.pnlPayslip.TabIndex = 73
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvPayslip
        '
        Me.lvPayslip.BackColorOnChecked = False
        Me.lvPayslip.CheckBoxes = True
        Me.lvPayslip.ColumnHeaders = Nothing
        Me.lvPayslip.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhTnALeaveID, Me.colhEmpCode, Me.colhEmployee, Me.colhPayYear, Me.colhPayPeriod, Me.colhVoucher, Me.colhEndDate, Me.colhTotalAmount, Me.colhPaidAmount, Me.colhBalAmount})
        Me.lvPayslip.CompulsoryColumns = ""
        Me.lvPayslip.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPayslip.FullRowSelect = True
        Me.lvPayslip.GridLines = True
        Me.lvPayslip.GroupingColumn = Nothing
        Me.lvPayslip.HideSelection = False
        Me.lvPayslip.Location = New System.Drawing.Point(0, 0)
        Me.lvPayslip.MinColumnWidth = 50
        Me.lvPayslip.MultiSelect = False
        Me.lvPayslip.Name = "lvPayslip"
        Me.lvPayslip.OptionalColumns = ""
        Me.lvPayslip.ShowMoreItem = False
        Me.lvPayslip.ShowSaveItem = False
        Me.lvPayslip.ShowSelectAll = True
        Me.lvPayslip.ShowSizeAllColumnsToFit = True
        Me.lvPayslip.Size = New System.Drawing.Size(935, 275)
        Me.lvPayslip.Sortable = False
        Me.lvPayslip.TabIndex = 0
        Me.lvPayslip.UseCompatibleStateImageBehavior = False
        Me.lvPayslip.View = System.Windows.Forms.View.Details
        '
        'objcolhTnALeaveID
        '
        Me.objcolhTnALeaveID.Tag = "objcolhTnALeaveID"
        Me.objcolhTnALeaveID.Text = ""
        Me.objcolhTnALeaveID.Width = 30
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Emp. Code"
        Me.colhEmpCode.Width = 80
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 250
        '
        'colhPayYear
        '
        Me.colhPayYear.Tag = "colhPayYear"
        Me.colhPayYear.Text = "Pay Year"
        Me.colhPayYear.Width = 0
        '
        'colhPayPeriod
        '
        Me.colhPayPeriod.Tag = "colhPayPeriod"
        Me.colhPayPeriod.Text = "Pay Period"
        Me.colhPayPeriod.Width = 0
        '
        'colhVoucher
        '
        Me.colhVoucher.Tag = "colhVoucher"
        Me.colhVoucher.Text = "Voucher No."
        Me.colhVoucher.Width = 75
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 90
        '
        'colhTotalAmount
        '
        Me.colhTotalAmount.Tag = "colhTotalAmount"
        Me.colhTotalAmount.Text = "Total Amount"
        Me.colhTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTotalAmount.Width = 135
        '
        'colhPaidAmount
        '
        Me.colhPaidAmount.Tag = "colhPaidAmount"
        Me.colhPaidAmount.Text = "Paid Amount"
        Me.colhPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPaidAmount.Width = 135
        '
        'colhBalAmount
        '
        Me.colhBalAmount.Tag = "colhBalAmount"
        Me.colhBalAmount.Text = "Balance Amount"
        Me.colhBalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBalAmount.Width = 135
        '
        'PnlPaysliplist
        '
        Me.PnlPaysliplist.Controls.Add(Me.gbPayslipList)
        Me.PnlPaysliplist.Controls.Add(Me.gbFilterCriteria)
        Me.PnlPaysliplist.Controls.Add(Me.objFooter)
        Me.PnlPaysliplist.Controls.Add(Me.EZeeHeader)
        Me.PnlPaysliplist.Controls.Add(Me.objEFooter)
        Me.PnlPaysliplist.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlPaysliplist.Location = New System.Drawing.Point(0, 0)
        Me.PnlPaysliplist.Name = "PnlPaysliplist"
        Me.PnlPaysliplist.Size = New System.Drawing.Size(965, 549)
        Me.PnlPaysliplist.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.bdgUnpaid)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalAuthorized)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalPaid)
        Me.gbFilterCriteria.Controls.Add(Me.bdgTotalEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.bdgProcessed)
        Me.gbFilterCriteria.Controls.Add(Me.bdgNotProcessed)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.txtBalanceAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtBalanceAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayYear)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayYear)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(941, 114)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bdgUnpaid
        '
        Me.bdgUnpaid.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgUnpaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgUnpaid.FontSize_Caption = 8.25!
        Me.bdgUnpaid.FontSize_Number = 16.0!
        Me.bdgUnpaid.Height_Caption = 17
        Me.bdgUnpaid.Location = New System.Drawing.Point(330, 28)
        Me.bdgUnpaid.Name = "bdgUnpaid"
        Me.bdgUnpaid.Size = New System.Drawing.Size(103, 50)
        Me.bdgUnpaid.TabIndex = 328
        Me.bdgUnpaid.Text_Caption = "Unpaid"
        Me.bdgUnpaid.Text_Number = ""
        '
        'bdgTotalAuthorized
        '
        Me.bdgTotalAuthorized.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalAuthorized.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalAuthorized.FontSize_Caption = 8.25!
        Me.bdgTotalAuthorized.FontSize_Number = 16.0!
        Me.bdgTotalAuthorized.Height_Caption = 17
        Me.bdgTotalAuthorized.Location = New System.Drawing.Point(548, 28)
        Me.bdgTotalAuthorized.Name = "bdgTotalAuthorized"
        Me.bdgTotalAuthorized.Size = New System.Drawing.Size(103, 50)
        Me.bdgTotalAuthorized.TabIndex = 327
        Me.bdgTotalAuthorized.Text_Caption = "Authorized"
        Me.bdgTotalAuthorized.Text_Number = ""
        '
        'bdgTotalPaid
        '
        Me.bdgTotalPaid.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalPaid.FontSize_Caption = 8.25!
        Me.bdgTotalPaid.FontSize_Number = 16.0!
        Me.bdgTotalPaid.Height_Caption = 17
        Me.bdgTotalPaid.Location = New System.Drawing.Point(439, 28)
        Me.bdgTotalPaid.Name = "bdgTotalPaid"
        Me.bdgTotalPaid.Size = New System.Drawing.Size(103, 50)
        Me.bdgTotalPaid.TabIndex = 326
        Me.bdgTotalPaid.Text_Caption = "Paid"
        Me.bdgTotalPaid.Text_Number = ""
        '
        'bdgTotalEmployee
        '
        Me.bdgTotalEmployee.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgTotalEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgTotalEmployee.FontSize_Caption = 8.25!
        Me.bdgTotalEmployee.FontSize_Number = 16.0!
        Me.bdgTotalEmployee.Height_Caption = 17
        Me.bdgTotalEmployee.Location = New System.Drawing.Point(221, 28)
        Me.bdgTotalEmployee.Name = "bdgTotalEmployee"
        Me.bdgTotalEmployee.Size = New System.Drawing.Size(103, 50)
        Me.bdgTotalEmployee.TabIndex = 325
        Me.bdgTotalEmployee.Text_Caption = "Total Employees"
        Me.bdgTotalEmployee.Text_Number = ""
        '
        'bdgProcessed
        '
        Me.bdgProcessed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgProcessed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgProcessed.FontSize_Caption = 8.25!
        Me.bdgProcessed.FontSize_Number = 16.0!
        Me.bdgProcessed.Height_Caption = 17
        Me.bdgProcessed.Location = New System.Drawing.Point(112, 28)
        Me.bdgProcessed.Name = "bdgProcessed"
        Me.bdgProcessed.Size = New System.Drawing.Size(103, 50)
        Me.bdgProcessed.TabIndex = 324
        Me.bdgProcessed.Text_Caption = "Processed"
        Me.bdgProcessed.Text_Number = ""
        '
        'bdgNotProcessed
        '
        Me.bdgNotProcessed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.bdgNotProcessed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bdgNotProcessed.FontSize_Caption = 8.25!
        Me.bdgNotProcessed.FontSize_Number = 16.0!
        Me.bdgNotProcessed.Height_Caption = 17
        Me.bdgNotProcessed.Location = New System.Drawing.Point(3, 28)
        Me.bdgNotProcessed.Name = "bdgNotProcessed"
        Me.bdgNotProcessed.Size = New System.Drawing.Size(103, 50)
        Me.bdgNotProcessed.TabIndex = 323
        Me.bdgNotProcessed.Text_Caption = "Not Processed"
        Me.bdgNotProcessed.Text_Number = ""
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(803, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 305
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'txtBalanceAmountTo
        '
        Me.txtBalanceAmountTo.AllowNegative = True
        Me.txtBalanceAmountTo.BackColor = System.Drawing.Color.White
        Me.txtBalanceAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalanceAmountTo.DigitsInGroup = 0
        Me.txtBalanceAmountTo.Flags = 0
        Me.txtBalanceAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalanceAmountTo.Location = New System.Drawing.Point(763, 84)
        Me.txtBalanceAmountTo.MaxDecimalPlaces = 6
        Me.txtBalanceAmountTo.MaxWholeDigits = 21
        Me.txtBalanceAmountTo.Name = "txtBalanceAmountTo"
        Me.txtBalanceAmountTo.Prefix = ""
        Me.txtBalanceAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtBalanceAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtBalanceAmountTo.Size = New System.Drawing.Size(130, 21)
        Me.txtBalanceAmountTo.TabIndex = 11
        Me.txtBalanceAmountTo.Text = "0"
        Me.txtBalanceAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(641, 87)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(116, 15)
        Me.lblAmountTo.TabIndex = 300
        Me.lblAmountTo.Text = "Balance Amount To"
        '
        'txtBalanceAmountFrom
        '
        Me.txtBalanceAmountFrom.AllowNegative = True
        Me.txtBalanceAmountFrom.BackColor = System.Drawing.Color.White
        Me.txtBalanceAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalanceAmountFrom.DigitsInGroup = 0
        Me.txtBalanceAmountFrom.Flags = 0
        Me.txtBalanceAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalanceAmountFrom.Location = New System.Drawing.Point(412, 84)
        Me.txtBalanceAmountFrom.MaxDecimalPlaces = 6
        Me.txtBalanceAmountFrom.MaxWholeDigits = 21
        Me.txtBalanceAmountFrom.Name = "txtBalanceAmountFrom"
        Me.txtBalanceAmountFrom.Prefix = ""
        Me.txtBalanceAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtBalanceAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtBalanceAmountFrom.Size = New System.Drawing.Size(130, 21)
        Me.txtBalanceAmountFrom.TabIndex = 10
        Me.txtBalanceAmountFrom.Text = "0"
        Me.txtBalanceAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmountFrom
        '
        Me.lblAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountFrom.Location = New System.Drawing.Point(288, 87)
        Me.lblAmountFrom.Name = "lblAmountFrom"
        Me.lblAmountFrom.Size = New System.Drawing.Size(118, 15)
        Me.lblAmountFrom.TabIndex = 298
        Me.lblAmountFrom.Text = "Balance Amount From"
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(763, 30)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(130, 21)
        Me.cboPayPeriod.TabIndex = 1
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(53, 90)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(67, 15)
        Me.lblPayYear.TabIndex = 292
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.Visible = False
        '
        'cboPayYear
        '
        Me.cboPayYear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayYear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(126, 87)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(130, 21)
        Me.cboPayYear.TabIndex = 0
        Me.cboPayYear.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(899, 57)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(763, 57)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(130, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(690, 60)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(690, 32)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(67, 17)
        Me.lblPayPeriod.TabIndex = 73
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(914, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(891, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.lblUnProcessMsg)
        Me.objFooter.Controls.Add(Me.objlblUnProcessColor)
        Me.objFooter.Controls.Add(Me.lblOverDeduction)
        Me.objFooter.Controls.Add(Me.objlblColor)
        Me.objFooter.Controls.Add(Me.btnPrint)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnViewDetail)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 494)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(965, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(114, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 96
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayment, Me.mnuGlobalPayement, Me.mnuGlobalVoidPayment, Me.objSepPaymentApproval, Me.mnuPaymentApproval, Me.objSepPaymentAuthorize, Me.mnuAuthorizePayment, Me.mnuVoidAuthorizedPayment, Me.objSepStatutoryPayment, Me.mnuOrbitBulkProcess, Me.objSepOrbit, Me.mnuStatutoryPayment})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(209, 226)
        '
        'mnuPayment
        '
        Me.mnuPayment.Name = "mnuPayment"
        Me.mnuPayment.Size = New System.Drawing.Size(208, 22)
        Me.mnuPayment.Text = "&Payment"
        '
        'mnuGlobalPayement
        '
        Me.mnuGlobalPayement.Name = "mnuGlobalPayement"
        Me.mnuGlobalPayement.Size = New System.Drawing.Size(208, 22)
        Me.mnuGlobalPayement.Text = "&Global Payment"
        '
        'mnuGlobalVoidPayment
        '
        Me.mnuGlobalVoidPayment.Name = "mnuGlobalVoidPayment"
        Me.mnuGlobalVoidPayment.Size = New System.Drawing.Size(208, 22)
        Me.mnuGlobalVoidPayment.Text = "&Global Void Payment"
        '
        'objSepPaymentApproval
        '
        Me.objSepPaymentApproval.Name = "objSepPaymentApproval"
        Me.objSepPaymentApproval.Size = New System.Drawing.Size(205, 6)
        '
        'mnuPaymentApproval
        '
        Me.mnuPaymentApproval.Name = "mnuPaymentApproval"
        Me.mnuPaymentApproval.Size = New System.Drawing.Size(208, 22)
        Me.mnuPaymentApproval.Text = "Payment Approval"
        '
        'objSepPaymentAuthorize
        '
        Me.objSepPaymentAuthorize.Name = "objSepPaymentAuthorize"
        Me.objSepPaymentAuthorize.Size = New System.Drawing.Size(205, 6)
        '
        'mnuAuthorizePayment
        '
        Me.mnuAuthorizePayment.Name = "mnuAuthorizePayment"
        Me.mnuAuthorizePayment.Size = New System.Drawing.Size(208, 22)
        Me.mnuAuthorizePayment.Text = "Authorize Payment"
        '
        'mnuVoidAuthorizedPayment
        '
        Me.mnuVoidAuthorizedPayment.Name = "mnuVoidAuthorizedPayment"
        Me.mnuVoidAuthorizedPayment.Size = New System.Drawing.Size(208, 22)
        Me.mnuVoidAuthorizedPayment.Text = "Void Authorized Payment"
        '
        'objSepStatutoryPayment
        '
        Me.objSepStatutoryPayment.Name = "objSepStatutoryPayment"
        Me.objSepStatutoryPayment.Size = New System.Drawing.Size(205, 6)
        '
        'mnuStatutoryPayment
        '
        Me.mnuStatutoryPayment.Name = "mnuStatutoryPayment"
        Me.mnuStatutoryPayment.Size = New System.Drawing.Size(208, 22)
        Me.mnuStatutoryPayment.Text = "&Statutory Payment"
        '
        'lblUnProcessMsg
        '
        Me.lblUnProcessMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnProcessMsg.Location = New System.Drawing.Point(410, 31)
        Me.lblUnProcessMsg.Name = "lblUnProcessMsg"
        Me.lblUnProcessMsg.Size = New System.Drawing.Size(297, 15)
        Me.lblUnProcessMsg.TabIndex = 95
        Me.lblUnProcessMsg.Text = "UnProcessed Payslip"
        Me.lblUnProcessMsg.Visible = False
        '
        'objlblUnProcessColor
        '
        Me.objlblUnProcessColor.BackColor = System.Drawing.SystemColors.ControlDark
        Me.objlblUnProcessColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblUnProcessColor.Location = New System.Drawing.Point(355, 31)
        Me.objlblUnProcessColor.Name = "objlblUnProcessColor"
        Me.objlblUnProcessColor.Size = New System.Drawing.Size(49, 15)
        Me.objlblUnProcessColor.TabIndex = 94
        Me.objlblUnProcessColor.Visible = False
        '
        'lblOverDeduction
        '
        Me.lblOverDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverDeduction.Location = New System.Drawing.Point(410, 13)
        Me.lblOverDeduction.Name = "lblOverDeduction"
        Me.lblOverDeduction.Size = New System.Drawing.Size(297, 15)
        Me.lblOverDeduction.TabIndex = 93
        Me.lblOverDeduction.Text = "Over Deduction or Zero Balance"
        Me.lblOverDeduction.Visible = False
        '
        'objlblColor
        '
        Me.objlblColor.BackColor = System.Drawing.Color.Red
        Me.objlblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColor.Location = New System.Drawing.Point(355, 13)
        Me.objlblColor.Name = "objlblColor"
        Me.objlblColor.Size = New System.Drawing.Size(49, 15)
        Me.objlblColor.TabIndex = 86
        Me.objlblColor.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.White
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrint.BorderColor = System.Drawing.Color.Empty
        Me.btnPrint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.Black
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Location = New System.Drawing.Point(12, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Size = New System.Drawing.Size(96, 30)
        Me.btnPrint.TabIndex = 2
        Me.btnPrint.Text = "P&rint Payslip"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(230, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(58, 30)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.Visible = False
        '
        'btnViewDetail
        '
        Me.btnViewDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewDetail.BackColor = System.Drawing.Color.White
        Me.btnViewDetail.BackgroundImage = CType(resources.GetObject("btnViewDetail.BackgroundImage"), System.Drawing.Image)
        Me.btnViewDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewDetail.BorderColor = System.Drawing.Color.Empty
        Me.btnViewDetail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewDetail.FlatAppearance.BorderSize = 0
        Me.btnViewDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewDetail.ForeColor = System.Drawing.Color.Black
        Me.btnViewDetail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewDetail.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewDetail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDetail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDetail.Location = New System.Drawing.Point(753, 13)
        Me.btnViewDetail.Name = "btnViewDetail"
        Me.btnViewDetail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDetail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDetail.Size = New System.Drawing.Size(97, 30)
        Me.btnViewDetail.TabIndex = 0
        Me.btnViewDetail.Text = "&View Detail"
        Me.btnViewDetail.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(856, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = ""
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(965, 60)
        Me.EZeeHeader.TabIndex = 3
        Me.EZeeHeader.Title = "Pay Slips"
        '
        'objEFooter
        '
        Me.objEFooter.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.objEFooter.BorderColor = System.Drawing.Color.Silver
        Me.objEFooter.Controls.Add(Me.btnEmailClose)
        Me.objEFooter.Controls.Add(Me.btnEmailOk)
        Me.objEFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objEFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objEFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objEFooter.Location = New System.Drawing.Point(0, 494)
        Me.objEFooter.Name = "objEFooter"
        Me.objEFooter.Size = New System.Drawing.Size(965, 55)
        Me.objEFooter.TabIndex = 4
        Me.objEFooter.Visible = False
        '
        'btnEmailClose
        '
        Me.btnEmailClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailClose.BackColor = System.Drawing.Color.White
        Me.btnEmailClose.BackgroundImage = CType(resources.GetObject("btnEmailClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailClose.FlatAppearance.BorderSize = 0
        Me.btnEmailClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailClose.ForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Location = New System.Drawing.Point(856, 12)
        Me.btnEmailClose.Name = "btnEmailClose"
        Me.btnEmailClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailClose.TabIndex = 2
        Me.btnEmailClose.Text = "&Close"
        Me.btnEmailClose.UseVisualStyleBackColor = True
        '
        'btnEmailOk
        '
        Me.btnEmailOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailOk.BackColor = System.Drawing.Color.White
        Me.btnEmailOk.BackgroundImage = CType(resources.GetObject("btnEmailOk.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailOk.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailOk.FlatAppearance.BorderSize = 0
        Me.btnEmailOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailOk.ForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Location = New System.Drawing.Point(753, 12)
        Me.btnEmailOk.Name = "btnEmailOk"
        Me.btnEmailOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailOk.TabIndex = 1
        Me.btnEmailOk.Text = "&Ok"
        Me.btnEmailOk.UseVisualStyleBackColor = True
        '
        'mnuOrbitBulkProcess
        '
        Me.mnuOrbitBulkProcess.Name = "mnuOrbitBulkProcess"
        Me.mnuOrbitBulkProcess.Size = New System.Drawing.Size(208, 22)
        Me.mnuOrbitBulkProcess.Text = "&Orbit Bulk Payments"
        '
        'objSepOrbit
        '
        Me.objSepOrbit.Name = "objSepOrbit"
        Me.objSepOrbit.Size = New System.Drawing.Size(205, 6)
        '
        'frmPayslipList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(965, 549)
        Me.Controls.Add(Me.PnlPaysliplist)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayslipList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pay Slips"
        Me.gbPayslipList.ResumeLayout(False)
        Me.gbPayslipList.PerformLayout()
        Me.pnlPayslip.ResumeLayout(False)
        Me.pnlPayslip.PerformLayout()
        Me.PnlPaysliplist.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.objEFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPayslipList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlPayslip As System.Windows.Forms.Panel
    Friend WithEvents lvPayslip As eZee.Common.eZeeListView
    Friend WithEvents colhPayYear As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPayPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTotalAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPaidAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBalAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents PnlPaysliplist As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnPrint As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnViewDetail As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objcolhTnALeaveID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVoucher As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblAmountFrom As System.Windows.Forms.Label
    Friend WithEvents txtBalanceAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents txtBalanceAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objlblColor As System.Windows.Forms.Label
    Friend WithEvents lblOverDeduction As System.Windows.Forms.Label
    Friend WithEvents lblUnProcessMsg As System.Windows.Forms.Label
    Friend WithEvents objlblUnProcessColor As System.Windows.Forms.Label
    Friend WithEvents chkIncludeInactiveEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalPayement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalVoidPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSepPaymentApproval As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuAuthorizePayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVoidAuthorizedPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPaymentApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSepPaymentAuthorize As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objEFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEmailClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailOk As eZee.Common.eZeeLightButton
    Friend WithEvents objSepStatutoryPayment As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuStatutoryPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bdgUnpaid As Aruti.Data.Badge
    Friend WithEvents bdgTotalAuthorized As Aruti.Data.Badge
    Friend WithEvents bdgTotalPaid As Aruti.Data.Badge
    Friend WithEvents bdgTotalEmployee As Aruti.Data.Badge
    Friend WithEvents bdgProcessed As Aruti.Data.Badge
    Friend WithEvents bdgNotProcessed As Aruti.Data.Badge
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuOrbitBulkProcess As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSepOrbit As System.Windows.Forms.ToolStripSeparator
End Class
