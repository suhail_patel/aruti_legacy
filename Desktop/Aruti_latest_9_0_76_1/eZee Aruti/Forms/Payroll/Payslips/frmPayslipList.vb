﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmPayslipList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmPayslipList"
    Private intCurrPeriodID As Integer = 0

    'Sandeep [ 07 APRIL 2011 ] -- Start
    Private mstrEmployeeUnkids As String = String.Empty
    Private mstrPeriodUnkids As String = String.Empty
    Private dsEmailListData As New DataSet
    'Sandeep [ 07 APRIL 2011 ] -- End 
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Sohail (06 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (06 Mar 2012) -- End

    'Sohail (23 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartEPayslip As Date
    Private mdtPeriodEndEPayslip As Date
    'Sohail (23 Oct 2012) -- End
    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private mstrSearchText As String = ""
    'Sohail (26 Oct 2016) -- End
#End Region

    'Sandeep [ 07 APRIL 2011 ] -- Start
#Region " Properties "

    ''' <summary>
    '''  ONLY USED FOR SENDING E-PAYSLIPS. PLEASE DO NOT MODIFY IT.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _EmployeeUnkids() As String
        Get
            Return mstrEmployeeUnkids
        End Get
        Set(ByVal value As String)
            mstrEmployeeUnkids = value
        End Set
    End Property

    ''' <summary>
    '''  ONLY USED FOR SENDING E-PAYSLIPS. PLEASE DO NOT MODIFY IT.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _PeriodUnkids() As String
        Get
            Return mstrPeriodUnkids
        End Get
        Set(ByVal value As String)
            mstrPeriodUnkids = value
        End Set
    End Property

    ''' <summary>
    '''  ONLY USED FOR SENDING E-PAYSLIPS. PLEASE DO NOT MODIFY IT.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Email_Data() As DataSet
        Get
            Return dsEmailListData
        End Get
        Set(ByVal value As DataSet)
            dsEmailListData = value
        End Set
    End Property

    'Sohail (23 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    '''  ONLY USED FOR SENDING E-PAYSLIPS. PLEASE DO NOT MODIFY IT.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _PeriodStart_EPayslip() As Date
        Get
            Return mdtPeriodStartEPayslip
        End Get
    End Property

    ''' <summary>
    '''  ONLY USED FOR SENDING E-PAYSLIPS. PLEASE DO NOT MODIFY IT.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property _PeriodEnd_EPayslip() As Date
        Get
            Return mdtPeriodEndEPayslip
        End Get
    End Property
    'Sohail (23 Oct 2012) -- End

#End Region
    'Sandeep [ 07 APRIL 2011 ] -- End 

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'cboDepartment.BackColor = GUI.ColorOptional
            'cboGrade.BackColor = GUI.ColorOptional
            'cboSections.BackColor = GUI.ColorOptional
            'cboClass.BackColor = GUI.ColorOptional
            'cboCostCenter.BackColor = GUI.ColorOptional
            'cboPayPoint.BackColor = GUI.ColorOptional
            'Sohail (02 Jul 2014) -- End
            cboPayYear.BackColor = GUI.ColorOptional
            cboPayPeriod.BackColor = GUI.ColorOptional

            txtBalanceAmountFrom.BackColor = GUI.ColorOptional
            txtBalanceAmountTo.BackColor = GUI.ColorOptional

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'cboBranch.BackColor = GUI.ColorOptional 'Sohail (02 Jul 2014) 
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillComBo()
        Dim objYear As New clsMasterData
        'Sohail (02 Jul 2014) -- Start
        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objPayPoint As New clspaypoint_master
        'Sohail (02 Jul 2014) -- End
        Dim dsCombo As DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (06 Jan 2012)
        'S.SANDEEP [ 17 AUG 2011 ] -- START
        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
        'Dim objBranch As New clsStation 'Sohail (02 Jul 2014)
        'S.SANDEEP [ 17 AUG 2011 ] -- END 
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objYear.getComboListPAYYEAR("Year", True)
            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPayYear
                .BeginUpdate() 'Sohail (11 Sep 2010)
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Year")
                'Sohail (04 Mar 2011) -- Start
                'If .Items.Count > 0 Then .SelectedValue = 0
                If .Items.Count > 1 Then .SelectedIndex = 1
                'Sohail (04 Mar 2011) -- End
                .EndUpdate() 'Sohail (11 Sep 2010)
            End With

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmployee.GetEmployeeList("EmployeeList", True)
            'With cboEmployee
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombo.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With
            'Sohail (06 Jan 2012) -- End

            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'dsCombo = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsCombo = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsCombo = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsCombo = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsCombo = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsCombo.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            'dsCombo = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate() 'Sohail (11 Sep 2010)
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate() 'Sohail (11 Sep 2010)
            'End With

            ''S.SANDEEP [ 17 AUG 2011 ] -- START
            ''ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'dsCombo = objBranch.getComboList("List", True)
            'With cboBranch
            '    .BeginUpdate()
            '    .ValueMember = "stationunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("List")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With
            ''S.SANDEEP [ 17 AUG 2011 ] -- END 
            'Sohail (02 Jul 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComBo", mstrModuleName)
        Finally
            objYear = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objPayPoint = Nothing
            'Sohail (02 Jul 2014) -- End
            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'objBranch = Nothing 'Sohail (02 Jul 2014)
            'S.SANDEEP [ 17 AUG 2011 ] -- END 
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnOnlyProcessedEmployees As Boolean = False)
        'Sohail (08 Feb 2022) - [blnOnlyProcessedEmployees]
        Dim objTnALeave As New clsTnALeaveTran
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Dim strFilter As String = ""

        Try

            If User._Object.Privilege._AllowToViewPaySlipList = True Then   'Pinkal (02-Jul-2012) -- Start


                lvPayslip.Items.Clear()
                'Sohail (18 Dec 2010) -- Start
                objlblColor.Visible = False
                lblOverDeduction.Visible = False 'Sohail (04 Mar 2011)
                'Sohail (04 Mar 2011) -- Start
                objlblUnProcessColor.Visible = False
                lblUnProcessMsg.Visible = False
                'Sohail (04 Mar 2011) -- End
                'Sohail (18 Dec 2010) -- End

                'Sohail (11 Sep 2010) -- Start
                Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
                Cursor.Current = Cursors.WaitCursor 'Sohail (10 Feb 2012)
                lvPayslip.BeginUpdate()
                'Sohail (11 Sep 2010) -- End

                'S.SANDEEP [ 17 AUG 2011 ] -- START
                'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                'dsList = objTnALeave.GetList("TnA", CInt(cboEmployee.SelectedValue), _
                '                        CInt(cboDepartment.SelectedValue), _
                '                        CInt(cboSections.SelectedValue), _
                '                        0, _
                '                        CInt(cboGrade.SelectedValue), _
                '                        0, _
                '                        CInt(cboClass.SelectedValue), _
                '                        CInt(cboCostCenter.SelectedValue), _
                '                        0, _
                '                        0, _
                '                        CInt(cboPayPoint.SelectedValue), _
                '                        CInt(cboPayYear.SelectedValue), _
                '                        CInt(cboPayPeriod.SelectedValue), _
                '                        "prtnaleave_tran.payperiodunkid, hremployee_master.firstname")
                'Sohail (12 May 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objTnALeave.GetList("TnA", CInt(cboEmployee.SelectedValue), _
                '            CInt(cboDepartment.SelectedValue), _
                '            CInt(cboSections.SelectedValue), _
                '            0, _
                '            CInt(cboGrade.SelectedValue), _
                '                        CInt(cboBranch.SelectedValue), _
                '            CInt(cboClass.SelectedValue), _
                '            CInt(cboCostCenter.SelectedValue), _
                '            0, _
                '            0, _
                '            CInt(cboPayPoint.SelectedValue), _
                '            CInt(cboPayYear.SelectedValue), _
                '            CInt(cboPayPeriod.SelectedValue), _
                '            "prtnaleave_tran.payperiodunkid, hremployee_master.firstname", _
                '            chkIncludeInactiveEmployee.Checked, mstrAdvanceFilter) 'Sohail (12 Oct 2011) - [chkIncludeInactiveEmployee.Checked]
                'Sohail (02 Jul 2014) -- Start
                'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                'dsList = objTnALeave.GetList("TnA", CInt(cboEmployee.SelectedValue), _
                '        CInt(cboDepartment.SelectedValue), _
                '        CInt(cboSections.SelectedValue), _
                '        0, _
                '        CInt(cboGrade.SelectedValue), _
                '                    CInt(cboBranch.SelectedValue), _
                '        CInt(cboClass.SelectedValue), _
                '        CInt(cboCostCenter.SelectedValue), _
                '        0, _
                '        0, _
                '        CInt(cboPayPoint.SelectedValue), _
                '        CInt(cboPayYear.SelectedValue), _
                '        CInt(cboPayPeriod.SelectedValue), _
                '        "prtnaleave_tran.payperiodunkid, hremployee_master.firstname", _
                '        chkIncludeInactiveEmployee.Checked, mstrAdvanceFilter, True)

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTnALeave.GetList("TnA", CInt(cboEmployee.SelectedValue), _
                '        0, _
                '        0, _
                '        0, _
                '       0, _
                '       0, _
                '       0, _
                '       0, _
                '       0, _
                '       0, _
                '       0, _
                '        CInt(cboPayYear.SelectedValue), _
                '        CInt(cboPayPeriod.SelectedValue), _
                '        "prtnaleave_tran.payperiodunkid, hremployee_master.firstname", _
                '        chkIncludeInactiveEmployee.Checked, mstrAdvanceFilter, True)

                'S.SANDEEP [15 NOV 2016] -- START
                'ENHANCEMENT : QUERY OPTIMIZATION
                'dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, True, "TnA", CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid, hremployee_master.firstname", mstrAdvanceFilter, True)
                'Sohail (25 Jan 2017) -- Start
                'TRA Enhancement - 65.1 - QUERY OPTIMIZATION.
                'dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, True, "TnA", CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue), "prtnaleave_tran.payperiodunkid", mstrAdvanceFilter, True)
                'Sohail (20 Nov 2017) -- Start
                'Issue - 70.1 - The multi-part identifier [@EmpTab].employeename could bot be found.
                'dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, True, "TnA", CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue), "cfcommon_period_tran.end_date, [@EmpTab].employeename", mstrAdvanceFilter, True)
                dsList = objTnALeave.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmployee.Checked, True, "TnA", CInt(cboEmployee.SelectedValue), CInt(cboPayPeriod.SelectedValue), "cfcommon_period_tran.end_date, #EmpTab.employeename", mstrAdvanceFilter, True, , , blnOnlyProcessedEmployees)
                'Sohail (08 Feb 2022) - [blnOnlyProcessedEmployees]
                'Sohail (20 Nov 2017) -- End
                'Sohail (25 Jan 2017) -- End
                'S.SANDEEP [15 NOV 2016] -- END

                'Sohail (02 Jul 2014) -- End
                'Sohail (02 Jul 2014) -- End
                'Sohail (21 Aug 2015) -- End
                'Sohail (12 May 2012) -- End
                'S.SANDEEP [ 17 AUG 2011 ] -- END 


                If txtBalanceAmountFrom.TextLength = 0 Then txtBalanceAmountFrom.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                If txtBalanceAmountTo.TextLength = 0 Then txtBalanceAmountTo.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
                If txtBalanceAmountFrom.Decimal <> 0 Then 'Sohail (01 Dec 2010)
                    strFilter = "balanceamount >= " & txtBalanceAmountFrom.Decimal & " " 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                End If
                If txtBalanceAmountTo.Decimal <> 0 Then 'Sohail (01 Dec 2010)
                    If strFilter.Length > 0 Then strFilter &= "AND "
                    strFilter &= "balanceamount <= " & txtBalanceAmountTo.Decimal & " " 'Sohail (01 Dec 2010), 'Sohail (11 May 2011)
                End If

                If strFilter.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("TnA"), strFilter, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables("TnA")).ToTable
                End If


                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    'Sohail (03 Nov 2010) -- Start
                    'lvItem.Text = dtRow.Item("tnaleavetranunkid").ToString 'dsRow.Item("employeename").ToString
                    lvItem.Text = ""
                    'Sohail (03 Nov 2010) -- End
                    lvItem.Tag = dtRow.Item("tnaleavetranunkid").ToString

                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                    'Sohail (28 Jan 2012) -- End

                    lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
                    lvItem.SubItems(colhEmployee.Index).Tag = dtRow.Item("employeeunkid").ToString 'Sohail (04 Mar 2011)

                    lvItem.SubItems.Add(dtRow.Item("YearName").ToString)
                    lvItem.SubItems(colhPayYear.Index).Tag = dtRow.Item("yearunkid").ToString 'Sohail (04 Mar 2011)

                    lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                    lvItem.SubItems(colhPayPeriod.Index).Tag = dtRow.Item("payperiodunkid").ToString 'Sohail (04 Mar 2011)

                    lvItem.SubItems.Add(dtRow.Item("voucherno").ToString)
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("employee_enddate").ToString).ToShortDateString)
                    'Sohail (09 Oct 2019) -- End
                    'Sohail (04 Mar 2011) -- Start
                    'lvItem.SubItems.Add(Format(dtRow.Item("total_amount"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                    'lvItem.SubItems.Add(Format(cdec(dtRow.Item("total_amount")) - cdec(dtRow.Item("balanceamount")), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("total_amount")) + CDec(dtRow.Item("openingbalance")), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    lvItem.SubItems(colhTotalAmount.Index).Tag = CInt(dtRow.Item("daysinperiod").ToString) + CInt(dtRow.Item("totalpayabledays").ToString) 'Sohail (04 Mar 2011)
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("total_amount")) + CDec(dtRow.Item("openingbalance")) - CDec(dtRow.Item("balanceamount")), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                    'Sohail (04 Mar 2011) -- End
                    lvItem.SubItems.Add(Format(dtRow.Item("balanceamount"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)

                    'Sohail (18 Dec 2010) -- Start
                    'Sohail (04 Mar 2011) -- Start
                    If CDec(lvItem.SubItems(colhTotalAmount.Index).Tag) = 0 Then 'Sohail (11 May 2011)
                        lvItem.BackColor = System.Drawing.SystemColors.ControlDark
                        lvItem.ForeColor = System.Drawing.SystemColors.ControlText

                        objlblUnProcessColor.Visible = True
                        lblUnProcessMsg.Visible = True 'Sohail (04 Mar 2011)
                    ElseIf CDec(dtRow.Item("total_amount").ToString) + CDec(dtRow.Item("openingbalance")) <= 0 Then 'Sohail (11 May 2011)
                        'If cdec(dtRow.Item("total_amount").ToString) + cdec(dtRow.Item("openingbalance")) <= 0 Then 'Sohail (04 Mar 2011)
                        'Sohail (04 Mar 2011) -- End
                        'If cdec(dtRow.Item("total_amount").ToString) <= 0 Then 'Sohail (25 Jan 2011)
                        If (New clsPayment_tran).IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(lvItem.Tag), clsPayment_tran.enPaymentRefId.PAYSLIP) = False Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
                            'If (New clsPayment_tran).IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, CInt(lvItem.Tag)) = False Then
                            lvItem.BackColor = Color.Red
                            lvItem.ForeColor = Color.Yellow

                            objlblColor.Visible = True
                            lblOverDeduction.Visible = True 'Sohail (04 Mar 2011)
                        End If
                    End If
                    'Sohail (18 Dec 2010) -- End

                    'Sohail (11 Sep 2010) -- Start
                    'Sohail (18 Aug 2011) -- Start
                    'RemoveHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
                    'lvPayslip.Items.Add(lvItem) 'Sohail (03 Nov 2010)
                    'AddHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
                    'Sohail (18 Aug 2011) -- End
                    lvArray.Add(lvItem) 'Sohail (03 Nov 2010)
                    'Sohail (11 Sep 2010) -- End

                Next
                RemoveHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
                lvPayslip.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
                AddHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
                lvPayslip.GroupingColumn = colhPayPeriod
                lvPayslip.DisplayGroups(True)
                lvArray = Nothing

                If colhPaidAmount.Width > 0 Then 'Sohail (16 Oct 2019)
                If lvPayslip.Items.Count > 5 Then
                        colhEmployee.Width = 250 - 18
                Else
                        colhEmployee.Width = 250
                End If
                End If 'Sohail (16 Oct 2019)
                lvPayslip.GridLines = False 'Sohail (16 Oct 2010)
                'Sohail (16 Oct 2019) -- Start
                'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                If User._Object.Privilege._AllowToViewPaidAmount = False Then
                    colhEmployee.Width += colhBalAmount.Width + colhPaidAmount.Width + colhTotalAmount.Width
                    colhBalAmount.Width = 0
                    colhPaidAmount.Width = 0
                    colhTotalAmount.Width = 0
                End If
                'Sohail (16 Oct 2019) -- End
                lvPayslip.EndUpdate() 'Sohail (11 Sep 2010)
                Cursor.Current = Cursors.Default 'Sohail (10 Feb 2012)

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTnALeave = Nothing
            dsList = Nothing
            dtTable = Nothing
            Call objbtnSearch.ShowResult(CStr(lvPayslip.Items.Count))
        End Try
    End Sub

    'Sohail (03 Nov 2010) -- Start
    Private Sub CheckAllPayslip(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPayslip.Items
                RemoveHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvPayslip.ItemChecked, AddressOf lvPayslip_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Nov 2010) -- End

    Private Sub SetVisibility()

        Try
            btnViewDetail.Enabled = User._Object.Privilege._AllowViewPayslip
            btnPrint.Enabled = User._Object.Privilege._AllowPrintPayslip
            'Sohail (05 Nov 2011) -- Start
            'btnGlobalPayemnt.Enabled = User._Object.Privilege._AllowGlobalPayment
            'btnPayment.Enabled = User._Object.Privilege._AddPayment
            mnuGlobalPayement.Enabled = User._Object.Privilege._AllowGlobalPayment
            mnuPayment.Enabled = User._Object.Privilege._AddPayment
            mnuGlobalVoidPayment.Enabled = User._Object.Privilege._DeletePayment
            'Sohail (05 Nov 2011) -- End            
            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            mnuAuthorizePayment.Enabled = User._Object.Privilege._AllowAuthorizePayslipPayment
            mnuVoidAuthorizedPayment.Enabled = User._Object.Privilege._AllowVoidAuthorizedPayslipPayment
            'Sohail (02 Jul 2012) -- End
            'Sohail (13 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (27 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'mnuApprovePayment.Enabled = User._Object.Privilege._AllowToApprovePayment
            'mnuVoidApprovedPayment.Enabled = User._Object.Privilege._AllowToVoidApprovedPayment
            If User._Object.Privilege._AllowToApprovePayment = True OrElse User._Object.Privilege._AllowToVoidApprovedPayment = True Then
                mnuPaymentApproval.Enabled = True
            Else
                mnuPaymentApproval.Enabled = False
            End If
            'Sohail (27 Feb 2013) -- End
            'Sohail (13 Feb 2013) -- End

            'Sohail (27 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'mnuApprovePayment.Visible = ConfigParameter._Object._SetPayslipPaymentApproval
            'mnuVoidApprovedPayment.Visible = ConfigParameter._Object._SetPayslipPaymentApproval
            mnuPaymentApproval.Visible = ConfigParameter._Object._SetPayslipPaymentApproval
            'Sohail (27 Feb 2013) -- End
            objSepPaymentApproval.Visible = ConfigParameter._Object._SetPayslipPaymentApproval


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.

            If User._Object.Privilege._AllowToAddStatutoryPayment = False AndAlso User._Object.Privilege._AllowToEditStatutoryPayment = False AndAlso User._Object.Privilege._AllowToDeleteStatutoryPayment = False AndAlso User._Object.Privilege._AllowToViewStatutoryPaymentList = False Then

                mnuStatutoryPayment.Enabled = False
            Else
                mnuStatutoryPayment.Enabled = True
            End If

            'Varsha Rana (17-Oct-2017) -- End

            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            If btnViewDetail.Enabled = True Then
                btnViewDetail.Enabled = User._Object.Privilege._AllowToViewPaidAmount
            End If
            'Sohail (16 Oct 2019) -- End

            mnuOrbitBulkProcess.Visible = ConfigParameter._Object._IsOrbitIntegrated
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If chkIncludeInactiveEmployee.Checked = True Then
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True)
            'Else
            '    'Sohail (06 Mar 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    If CInt(cboPayPeriod.SelectedValue) > 0 Then
            '        'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)
            '    Else
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If
            '    'Sohail (06 Mar 2012) -- End
            'End If


            Dim EmpDate1 As Date = Nothing
            Dim EmpDate2 As Date = Nothing
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                EmpDate1 = mdtPeriodStartDate
                EmpDate2 = mdtPeriodEndDate
            Else
                EmpDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                EmpDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          EmpDate1, _
                                          EmpDate2, _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, chkIncludeInactiveEmployee.Checked, "Employee", True)

            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
            'Sohail (26 Oct 2016) -- Start
            'Enhancement - Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Sohail (26 Oct 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (06 Jan 2012) -- End

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 12, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpComboText", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End

    'Sohail (04 Jun 2018) -- Start
    'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
    Private Sub ShowCount()
        Dim objPayroll As New clsPayrollProcessTran
        Dim ds As DataSet
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then Exit Try
            'Sohail (18 Jun 2019) -- Start
            'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
            bdgNotProcessed.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgProcessed.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgTotalEmployee.Enabled = User._Object.Privilege._AllowProcessPayroll
            bdgUnpaid.Enabled = User._Object.Privilege._AllowGlobalPayment
            bdgTotalPaid.Enabled = User._Object.Privilege._DeletePayment
            bdgTotalAuthorized.Enabled = User._Object.Privilege._AllowAuthorizePayslipPayment
            'Sohail (18 Jun 2019) -- End

            ds = objPayroll.GetPayrollProcessCount("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboPayPeriod.SelectedValue), True, True, mstrAdvanceFilter, "", Nothing, True)
            'Sohail (15 Apr 2020) - [blnApplyEmpExemptionFilter]

            If ds.Tables(0).Rows.Count > 0 Then
                'Sohail (18 Jun 2019) -- Start
                'Eko Supreme Nigeria Ltd - Support issue # 0003911 - 74.1 : User able to see Payslip l Eko Supreme Nigeria Ltd.
                'bdgNotProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalNotProcessed").ToString
                'bdgProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalProcessed").ToString
                'bdgTotalEmployee.Text_Number = (CInt(ds.Tables(0).Rows(0)("TotalNotProcessed")) + CInt(ds.Tables(0).Rows(0)("TotalProcessed"))).ToString
                'bdgTotalPaid.Text_Number = ds.Tables(0).Rows(0)("TotalPaid").ToString
                'bdgTotalAuthorized.Text_Number = ds.Tables(0).Rows(0)("TotalAuthorized").ToString
                'bdgUnpaid.Text_Number = ds.Tables(0).Rows(0)("TotalUnpaid").ToString
                If bdgNotProcessed.Enabled = True Then bdgNotProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalNotProcessed").ToString
                If bdgProcessed.Enabled = True Then bdgProcessed.Text_Number = ds.Tables(0).Rows(0)("TotalProcessed").ToString
                If bdgTotalEmployee.Enabled = True Then bdgTotalEmployee.Text_Number = (CInt(ds.Tables(0).Rows(0)("TotalNotProcessed")) + CInt(ds.Tables(0).Rows(0)("TotalProcessed"))).ToString
                If bdgTotalPaid.Enabled = True Then bdgTotalPaid.Text_Number = ds.Tables(0).Rows(0)("TotalPaid").ToString
                If bdgTotalAuthorized.Enabled = True Then bdgTotalAuthorized.Text_Number = ds.Tables(0).Rows(0)("TotalAuthorized").ToString
                If bdgUnpaid.Enabled = True Then bdgUnpaid.Text_Number = ds.Tables(0).Rows(0)("TotalUnpaid").ToString
                'Sohail (18 Jun 2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ShowCount", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Jun 2018) -- End

#End Region

#Region " Form's Events "

    Private Sub frmPayslipList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
                    'Case Keys.Delete
                    '    btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayslipList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            txtBalanceAmountFrom.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
            txtBalanceAmountTo.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
            'Sohail (04 Mar 2011) -- Start
            'txtMessage.Text = "Please make payment first for Overdeduction or Zero Balance Payslip(s).If you do not make payment, " & _
            '                  "then deduction for Loan/Advance/Savings/Tax deduction will not be deducted from the Employee(s) Salary." 'Sohail (03 Mar 2011)
            lblOverDeduction.Text = Language.getMessage(mstrModuleName, 10, "Over Deduction or Zero Balance")
            lblUnProcessMsg.Text = Language.getMessage(mstrModuleName, 11, "UnProcessed Payslip")
            'Sohail (04 Mar 2011) -- End
            Call SetVisibility()
            Call SetColor()
            Call FillComBo()
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillEmployeeCombo()
            'Sohail (06 Jan 2012) -- End
            'Sohail (09 Oct 2010) -- Start
            Dim objMaster As New clsMasterData
            'Sohail (12 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (12 Apr 2013) -- End
            cboPayPeriod.SelectedValue = intCurrPeriodID
            objMaster = Nothing
            'Sohail (09 Oct 2010) -- End
            ' Call FillList()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayslipList_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "
    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "Period", True, , False) 'Sohail (04 Mar 2011)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, , False)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    'Sohail (06 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
                'Sohail (04 Jun 2018) -- Start
                'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
                Call ShowCount()
                'Sohail (04 Jun 2018) -- End
            End If
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (06 Mar 2012) -- End

    'Sohail (26 Oct 2016) -- Start
    'Enhancement - Searchable Dropdown
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Oct 2016) -- End
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayment.Click, btnOperations.Click 'btnPayment.Click
        If lvPayslip.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Slip from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPayslip.Select()
            Exit Sub
        End If
        Dim objPeriod As New clscommom_period_Tran 'Sohail (04 Mar 2011)

        Try

            'Sohail (04 Mar 2011) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(lvPayslip.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvPayslip.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot make payment for this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
                lvPayslip.Select()
                Exit Try
            ElseIf CInt(lvPayslip.SelectedItems(0).SubItems(colhTotalAmount.Index).Tag) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot make payment for this entry. Reason: Payroll Process is not done yet for the ") & lvPayslip.SelectedItems(0).SubItems(colhEmployee.Index).Text & Language.getMessage(mstrModuleName, 8, " Employee for the ") & lvPayslip.SelectedItems(0).SubItems(colhPayPeriod.Index).Text & Language.getMessage(mstrModuleName, 9, " Period."), enMsgBoxStyle.Information)
                lvPayslip.Select()
                Exit Try
            End If
            'Sohail (04 Mar 2011) -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayslip.SelectedItems(0).Index

            Dim objPaymentList As New frmPaymentList
            objPaymentList._PaymentType_Id = CInt(clsPayment_tran.enPayTypeId.PAYMENT)
            objPaymentList._Reference_Id = CInt(clsPayment_tran.enPaymentRefId.PAYSLIP)
            objPaymentList._Transaction_Id = CInt(lvPayslip.SelectedItems(0).Tag)
            'objPaymentList.xCallFrom = "Payslip"
            objPaymentList.ShowDialog()
            FillList()
            'Sohail (04 Jun 2018) -- Start
            'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
            Call ShowCount()
            'Sohail (04 Jun 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayment_Click", mstrModuleName)
            'Sohail (04 Mar 2011) -- Start
        Finally
            objPeriod = Nothing
            'Sohail (04 Mar 2011) -- End
        End Try
    End Sub

    Private Sub btnViewDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDetail.Click, lvPayslip.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._AllowViewPayslip = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        'Sohail (30 Jan 2020) -- Start
        'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
        If User._Object.Privilege._AllowToViewPaidAmount = False Then Exit Sub
        'Sohail (30 Jan 2020) -- End

        If lvPayslip.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Slip from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPayslip.Select()
            Exit Sub
        End If

        Dim frm As New frmPayslip_AddEdit

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayslip.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvPayslip.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvPayslip.Items(intSelectedIndex).Selected = True
            lvPayslip.EnsureVisible(intSelectedIndex)
            lvPayslip.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnViewDetail_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (16 Oct 2010) -- Start
    Private Sub mnuGlobalPayemnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalPayement.Click ' btnGlobalPayemnt.Click
        Dim objFrm As New frmPayslipGlobalPayment
        Try
            If objFrm.DisplayDialog(enAction.ADD_CONTINUE) = True Then
                Call FillList()
                'Sohail (04 Jun 2018) -- Start
                'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
                Call ShowCount()
                'Sohail (04 Jun 2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalPayemnt_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (16 Oct 2010) -- End

    'Sohail (05 Nov 2011) -- Start
    Private Sub mnuGlobalVoidPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalVoidPayment.Click
        Dim frm As New frmGlobalVoidPayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT) Then
                Call FillList()
                'Sohail (04 Jun 2018) -- Start
                'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
                Call ShowCount()
                'Sohail (04 Jun 2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalVoidPayment_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (05 Nov 2011) -- End


    'Vimal (15 Dec 2010) -- Start 
    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim strEmployeeId As String = ""
        Dim strPeriodId As String = ""
        Dim arrEmp() As String = {}
        Dim arrPeriod() As String = {}
        Dim frm As New ArutiReports.frmPaySlip

        Try
            If lvPayslip.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If lvPayslip.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvPayslip.CheckedItems.Count - 1
                    If Array.IndexOf(arrEmp, lvPayslip.CheckedItems(i).SubItems(colhEmployee.Index).Tag.ToString) <> -1 Then
                        Continue For
                    Else
                        ReDim Preserve arrEmp(arrEmp.Length)
                        arrEmp(arrEmp.Length - 1) = lvPayslip.CheckedItems(i).SubItems(colhEmployee.Index).Tag.ToString
                    End If
                Next
                strEmployeeId = String.Join(",", arrEmp)
                For j As Integer = 0 To lvPayslip.CheckedItems.Count - 1
                    If Array.IndexOf(arrPeriod, lvPayslip.CheckedItems(j).SubItems(colhPayPeriod.Index).Tag.ToString) <> -1 Then
                        Continue For
                    Else
                        ReDim Preserve arrPeriod(arrPeriod.Length)
                        arrPeriod(arrPeriod.Length - 1) = lvPayslip.CheckedItems(j).SubItems(colhPayPeriod.Index).Tag.ToString
                    End If
                Next
                strPeriodId = String.Join(",", arrPeriod)
            End If

            arrEmp = Nothing
            arrPeriod = Nothing

            frm._EmpIds = strEmployeeId
            frm._PeriodIds = strPeriodId
            frm._PaySlip = True


            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            'frm.Size = CType(New Point(799, 423), Drawing.Size)
            'frm.gbFilterCriteriaEmpPeriod.Location = New Point(9, 66)
            'frm.gbFilterCriteriaMembership.Location = New Point(400, 66)
            'frm.gbFilterCriteriaMessage.Location = New Point(400, 170)
            'frm.gbSortBy.Location = New Point(400, 267)
            'frm.objbtnSort.Location = New Point(348, 32)
            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPrint_Click", mstrModuleName)
        End Try
    End Sub
    'Vimal (15 Dec 2010) -- End

    Private Sub btnEmailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEmailOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailOk.Click
        Dim objFields As New clsLetterFields
        Dim blnFlag As Boolean = False
        Dim objPeriod As clscommom_period_Tran 'Sohail (23 Oct 2012)

        If lvPayslip.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check atleast one employee to send payslip."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'If ConfigParameter._Object._ExportReportPath.Trim.Length <= 0 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the export report path from Aruti Configuration -> Oprtions -> Path -> Export Report Path."), enMsgBoxStyle.Information)
        '    Exit Sub
        'End If

        Try
            For Each lvItem As ListViewItem In lvPayslip.CheckedItems
                'Sohail (06 Jan 2015) -- Start
                'Off Grid Enhancement - Payslip should be attached if payment is done and if there is a balance due to payment rounding.
                'If CDec(lvItem.SubItems(colhBalAmount.Index).Text) > 0 Then 'Sohail (11 May 2011)
                If CDec(lvItem.SubItems(colhPaidAmount.Index).Text) <= 0 Then
                    'Sohail (06 Jan 2015) -- End
                    If blnFlag = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Some of the employees salary is not paid. Do you want to continue? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            blnFlag = True
                            Exit Sub
                        Else
                            blnFlag = True
                            Continue For
                        End If
                    End If
                ElseIf CDec(lvItem.SubItems(colhPaidAmount.Index).Text) > 0 Then 'Sohail (11 May 2011)
                    'S.SANDEEP [ 28 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If mstrEmployeeUnkids.Contains(lvItem.SubItems(colhEmployee.Index).Tag.ToString) = False Then
                    '    mstrEmployeeUnkids &= "," & lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    'End If
                    'If mstrPeriodUnkids.Contains(lvItem.SubItems(colhPayPeriod.Index).Tag.ToString) = False Then
                    '    mstrPeriodUnkids &= "," & lvItem.SubItems(colhPayPeriod.Index).Tag.ToString
                    'End If

                    mstrEmployeeUnkids &= "," & lvItem.SubItems(colhEmployee.Index).Tag.ToString
                    mstrPeriodUnkids &= "," & lvItem.SubItems(colhPayPeriod.Index).Tag.ToString
                    'S.SANDEEP [ 28 FEB 2012 ] -- END

                    'Sohail (23 Oct 2012) -- Start
                    'TRA - ENHANCEMENT
                    objPeriod = New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
                    'Sohail (21 Aug 2015) -- End
                    If mdtPeriodStartEPayslip = Nothing OrElse mdtPeriodStartEPayslip > objPeriod._Start_Date Then
                        mdtPeriodStartEPayslip = objPeriod._Start_Date
                    End If
                    If mdtPeriodEndEPayslip = Nothing OrElse mdtPeriodEndEPayslip < objPeriod._End_Date Then
                        mdtPeriodEndEPayslip = objPeriod._End_Date
                    End If

                    'Sohail (23 Oct 2012) -- End
                End If
            Next

            If mstrEmployeeUnkids.Length > 0 Then
                mstrEmployeeUnkids = Mid(mstrEmployeeUnkids, 2)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsEmailListData = objFields.GetEmployeeData(mstrEmployeeUnkids, enImg_Email_RefId.Employee_Module, "DataTable")
                'Sohail (03 May 2019) -- Start
                'FOUR SEASONS BILILA Issue - ISSUE # 0003785 - 76.1 - Error "Object refrence not set" msg when sending salary slips.
                'dsEmailListData = objFields.GetEmployeeData(mstrEmployeeUnkids, enImg_Email_RefId.Employee_Module, mdtPeriodEndEPayslip, Company._Object._Companyunkid, "DataTable")
                dsEmailListData = objFields.GetEmployeeData(mstrEmployeeUnkids, enImg_Email_RefId.Employee_Module, mdtPeriodEndEPayslip, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, "DataTable")
                'Sohail (03 May 2019) -- End
                'S.SANDEEP [04 JUN 2015] -- END

                If dsEmailListData.Tables("DataTable").Rows.Count > 0 Then
                    Dim dtRow() As DataRow = dsEmailListData.Tables("DataTable").Select("Email = ''")
                    If dtRow.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Some of the employees email addresses are blank. Do you wish to continue? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            For i As Integer = 0 To dtRow.Length - 1
                                dsEmailListData.Tables("DataTable").Rows.Remove(dtRow(i))
                            Next
                        End If
                    End If
                End If
            End If

            If mstrPeriodUnkids.Length > 0 Then
                mstrPeriodUnkids = Mid(mstrPeriodUnkids, 2)
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuAuthorizePayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAuthorizePayment.Click
        Try
            Dim objFrm As New frmAuthorizeUnauthorizePayment
            objFrm.DisplayDialog(enAction.ADD_CONTINUE, True)
            objFrm = Nothing
            'Sohail (04 Jun 2018) -- Start
            'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
            Call ShowCount()
            'Sohail (04 Jun 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAuthorizePayment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuVoidAuthorizedPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuVoidAuthorizedPayment.Click
        Try
            Dim objFrm As New frmAuthorizeUnauthorizePayment
            objFrm.DisplayDialog(enAction.ADD_CONTINUE, False)
            objFrm = Nothing
            'Sohail (04 Jun 2018) -- Start
            'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
            Call ShowCount()
            'Sohail (04 Jun 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidAuthorizedPayment_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Jul 2012) -- End

    'Sohail (27 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    'Sohail (13 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    'Private Sub mnuApprovePayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuApprovePayment.Click
    '    Try
    '        Dim objFrm As New frmApproveDisapprovePayment
    '        objFrm.DisplayDialog(enAction.ADD_CONTINUE, True)
    '        objFrm = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuApprovePayment_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub mnuVoidApprovedPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuVoidApprovedPayment.Click
    '    Try
    '        Dim objFrm As New frmApproveDisapprovePayment
    '        objFrm.DisplayDialog(enAction.ADD_CONTINUE, False)
    '        objFrm = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuVoidApprovedPayment_Click", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (13 Feb 2013) -- End

    Private Sub mnuPaymentApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaymentApproval.Click
        Try
            Dim objFrm As New frmApproveDisapprovePaymentList
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPaymentApproval_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Feb 2013) -- End

    'Sohail (05 Aug 2014) -- Start
    'Enhancement - Statutory Payment Priviledge.
    Private Sub mnuStatutoryPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStatutoryPayment.Click
        Dim objFrm As New frmStatutoryPaymentList
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuStatutoryPayment_Click", mstrModuleName)
        Finally
            If objFrm IsNot Nothing Then objFrm.Dispose()
        End Try
    End Sub
    'Sohail (05 Aug 2014) -- End

    'Sohail (25 Apr 2020) -- Start
    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
    Private Sub mnuOrbitBulkProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOrbitBulkProcess.Click
        Dim frm As New frmOrbitBulkPayment
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuOrbitBulkProcess_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (25 Apr 2020) -- End

#End Region

#Region " TextBox's Events "
    'Private Sub txtBalanceAmountFrom_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBalanceAmountFrom.LostFocus
    '    Try
    '        If txtBalanceAmountFrom.TextLength = 0 Then txtBalanceAmountFrom.Text = "0"
    '        txtBalanceAmountFrom.Text = Format(txtBalanceAmountFrom.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtBalanceAmountFrom_LostFocus", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub txtBalanceAmountTo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBalanceAmountTo.LostFocus
    '    Try
    '        If txtBalanceAmountTo.TextLength = 0 Then txtBalanceAmountTo.Text = "0"
    '        txtBalanceAmountTo.Text = Format(txtBalanceAmountTo.Decimal, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtBalanceAmountTo_LostFocus", mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (11 May 2011) -- Start
    Private Sub txtBalanceAmountFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBalanceAmountFrom.TextChanged
        Try
            If txtBalanceAmountFrom.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtBalanceAmountFrom_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtBalanceAmountTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBalanceAmountTo.TextChanged
        Try
            If txtBalanceAmountTo.SelectionLength <= 0 Then Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtBalanceAmountTo_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 May 2011) -- End

#End Region

    'Sohail (03 Nov 2010) -- Start
#Region " Listview's Events "
    Private Sub lvPayslip_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPayslip.ItemChecked
        Try
            If lvPayslip.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPayslip.CheckedItems.Count < lvPayslip.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvPayslip.CheckedItems.Count = lvPayslip.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayslip_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllPayslip(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Oct 2011) -- Start
    Private Sub chkIncludeInactiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveEmployee.CheckedChanged
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillEmployeeCombo()
            'Sohail (06 Jan 2012) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2011) -- End
#End Region
    'Sohail (03 Nov 2010) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Jan 2012) -- End
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (06 Jan 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

            'Sohail (06 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'Call objbtnSearch.ShowResult(CStr(lvPayslip.Items.Count))
            'Sohail (06 Sep 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (02 Jul 2014) -- Start
            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'Sohail (02 Jul 2014) -- End
            'Sohail (04 Mar 2011) -- Start
            'If cboPayYear.Items.Count > 0 Then cboPayYear.SelectedValue = 0
            If cboPayYear.Items.Count > 1 Then cboPayYear.SelectedIndex = 1
            'Sohail (04 Mar 2011) -- End
            If cboPayPeriod.Items.Count > 0 Then cboPayPeriod.SelectedValue = 0

            txtBalanceAmountFrom.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)
            txtBalanceAmountTo.Text = Format(0, GUI.fmtCurrency) 'Sohail (01 Dec 2010)

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            'cboBranch.SelectedValue = 0 'Sohail (02 Jul 2014)
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            cboPayPeriod.SelectedValue = intCurrPeriodID 'Sohail (09 Oct 2010)
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 

    'Sohail (04 Jun 2018) -- Start
    'Internal Enhancement - Providing Summary count on process payroll screen and Payslip screen in 72.1.
    Private Sub bdgNotProcessed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgNotProcessed.Click
        Dim frm As New frmProcessPayroll
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            frm.ShowDialog()
            Call ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgNotProcessed_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub bdgProcessed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgProcessed.Click
        'Dim frm As New frmProcessPayroll 'Sohail (08 Feb 2022)
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'Sohail (08 Feb 2022) -- Start
            'Enhancement : OLD-548 : Give Option to apply Basic Salary Computation Setting on Flat Rate Heads.
            'frm.ShowDialog()
            'Call ShowCount()
            Call FillList(True)
            'Sohail ((08 Feb 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgProcessed_Click", mstrModuleName)
        Finally
            'If frm IsNot Nothing Then frm.Dispose() 'Sohail (08 Feb 2022)
        End Try
    End Sub

    Private Sub bdgTotalEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalEmployee.Click
        Dim frm As New frmProcessPayroll
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            frm.ShowDialog()
            Call ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub bdgUnpaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgUnpaid.Click
        Dim objFrm As New frmPayslipGlobalPayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If objFrm.DisplayDialog(enAction.ADD_CONTINUE) = True Then
                Call FillList()
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgUnpaid_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub bdgTotalPaid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalPaid.Click
        Dim frm As New frmGlobalVoidPayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT) Then
                Call FillList()
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalPaid_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub bdgTotalAuthorized_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bdgTotalAuthorized.Click
        Dim objFrm As New frmAuthorizeUnauthorizePayment
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If objFrm.DisplayDialog(enAction.ADD_CONTINUE, True) = True Then
                Call FillList()
                Call ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bdgTotalAuthorized_Click", mstrModuleName)
        Finally
            If objFrm IsNot Nothing Then objFrm.Dispose()
        End Try
    End Sub
    'Sohail (04 Jun 2018) -- End

#End Region

#Region " Message List "
    '1, "Please select Pay Slip from the list to perform further operation on it."
    '2, "Please Select atleast one Employee."
    '3, "Sorry, You can not make payment for this entry. Reason: Period is closed."
    '4, "Sorry, You can not make payment for this entry. Reason: Payroll Process is not done yet for the '" & lvPayslip.SelectedItems(0).SubItems(colhEmployee.Index).Text & "' Employee for the '" & lvPayslip.SelectedItems(0).SubItems(colhPayPeriod.Index).Text & "' Period."
#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPayslipList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPayslipList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnPrint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrint.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnViewDetail.GradientBackColor = GUI._ButttonBackColor
            Me.btnViewDetail.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmailClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmailOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbPayslipList.Text = Language._Object.getCaption(Me.gbPayslipList.Name, Me.gbPayslipList.Text)
            Me.colhPayYear.Text = Language._Object.getCaption(CStr(Me.colhPayYear.Tag), Me.colhPayYear.Text)
            Me.colhPayPeriod.Text = Language._Object.getCaption(CStr(Me.colhPayPeriod.Tag), Me.colhPayPeriod.Text)
            Me.colhTotalAmount.Text = Language._Object.getCaption(CStr(Me.colhTotalAmount.Tag), Me.colhTotalAmount.Text)
            Me.colhPaidAmount.Text = Language._Object.getCaption(CStr(Me.colhPaidAmount.Tag), Me.colhPaidAmount.Text)
            Me.colhBalAmount.Text = Language._Object.getCaption(CStr(Me.colhBalAmount.Tag), Me.colhBalAmount.Text)
            Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnViewDetail.Text = Language._Object.getCaption(Me.btnViewDetail.Name, Me.btnViewDetail.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title", Me.EZeeHeader.Title)
            Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message", Me.EZeeHeader.Message)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhVoucher.Text = Language._Object.getCaption(CStr(Me.colhVoucher.Tag), Me.colhVoucher.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblAmountFrom.Text = Language._Object.getCaption(Me.lblAmountFrom.Name, Me.lblAmountFrom.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
            Me.lblOverDeduction.Text = Language._Object.getCaption(Me.lblOverDeduction.Name, Me.lblOverDeduction.Text)
            Me.lblUnProcessMsg.Text = Language._Object.getCaption(Me.lblUnProcessMsg.Name, Me.lblUnProcessMsg.Text)
            Me.chkIncludeInactiveEmployee.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmployee.Name, Me.chkIncludeInactiveEmployee.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuPayment.Text = Language._Object.getCaption(Me.mnuPayment.Name, Me.mnuPayment.Text)
            Me.mnuGlobalPayement.Text = Language._Object.getCaption(Me.mnuGlobalPayement.Name, Me.mnuGlobalPayement.Text)
            Me.mnuGlobalVoidPayment.Text = Language._Object.getCaption(Me.mnuGlobalVoidPayment.Name, Me.mnuGlobalVoidPayment.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
            Me.mnuAuthorizePayment.Text = Language._Object.getCaption(Me.mnuAuthorizePayment.Name, Me.mnuAuthorizePayment.Text)
            Me.mnuVoidAuthorizedPayment.Text = Language._Object.getCaption(Me.mnuVoidAuthorizedPayment.Name, Me.mnuVoidAuthorizedPayment.Text)
            Me.mnuPaymentApproval.Text = Language._Object.getCaption(Me.mnuPaymentApproval.Name, Me.mnuPaymentApproval.Text)
            Me.btnEmailClose.Text = Language._Object.getCaption(Me.btnEmailClose.Name, Me.btnEmailClose.Text)
            Me.btnEmailOk.Text = Language._Object.getCaption(Me.btnEmailOk.Name, Me.btnEmailOk.Text)
            Me.mnuStatutoryPayment.Text = Language._Object.getCaption(Me.mnuStatutoryPayment.Name, Me.mnuStatutoryPayment.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Pay Slip from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Please Select atleast one Employee.")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot make payment for this entry. Reason: Period is closed.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot make payment for this entry. Reason: Payroll Process is not done yet for the")
            Language.setMessage(mstrModuleName, 5, "Some of the employees salary is not paid. Do you want to continue?")
            Language.setMessage(mstrModuleName, 6, "Some of the employees email addresses are blank. Do you wish to continue?")
            Language.setMessage(mstrModuleName, 7, "Please check atleast one employee to send payslip.")
            Language.setMessage(mstrModuleName, 8, " Employee for the")
            Language.setMessage(mstrModuleName, 9, " Period.")
            Language.setMessage(mstrModuleName, 10, "Over Deduction or Zero Balance")
            Language.setMessage(mstrModuleName, 11, "UnProcessed Payslip")
            Language.setMessage(mstrModuleName, 12, "Type to Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class