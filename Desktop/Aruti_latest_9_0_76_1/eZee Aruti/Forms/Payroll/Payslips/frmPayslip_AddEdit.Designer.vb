﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayslip_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayslip_AddEdit))
        Me.gbPayslipInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblOpenBalance = New System.Windows.Forms.Label
        Me.txtOpenBalance = New eZee.TextBox.AlphanumericTextBox
        Me.txtDate = New eZee.TextBox.AlphanumericTextBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.txtGradeLevel = New eZee.TextBox.AlphanumericTextBox
        Me.txtGrade = New eZee.TextBox.AlphanumericTextBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.txtGradeGroup = New eZee.TextBox.AlphanumericTextBox
        Me.txtScale = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.txtPayPeriod = New eZee.TextBox.AlphanumericTextBox
        Me.txtPayYear = New eZee.TextBox.AlphanumericTextBox
        Me.lblScale = New System.Windows.Forms.Label
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.lblPayyear = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtVoucherNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.gbEmployeeinformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvShift = New eZee.Common.eZeeListView(Me.components)
        Me.colhShift = New System.Windows.Forms.ColumnHeader
        Me.txtUnit = New eZee.TextBox.AlphanumericTextBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.imgImageControl = New eZee.Common.eZeeImageControl
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lblEmployeeImage = New System.Windows.Forms.Label
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtSection = New eZee.TextBox.AlphanumericTextBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.txtType = New eZee.TextBox.AlphanumericTextBox
        Me.lblType = New System.Windows.Forms.Label
        Me.lblShift = New System.Windows.Forms.Label
        Me.txtShift = New eZee.TextBox.AlphanumericTextBox
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPeriodWorkDetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtPayableHr = New System.Windows.Forms.TextBox
        Me.lblDaysinPeriod = New System.Windows.Forms.Label
        Me.lblHolidays = New System.Windows.Forms.Label
        Me.esLine1 = New eZee.Common.eZeeStraightLine
        Me.lblPayableDays = New System.Windows.Forms.Label
        Me.txtDaysInPeriod = New eZee.TextBox.NumericTextBox
        Me.lblPayableHours = New System.Windows.Forms.Label
        Me.txtPayableDay = New eZee.TextBox.NumericTextBox
        Me.txtHolidayInMonth = New eZee.TextBox.NumericTextBox
        Me.PnlPayList = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.btnPrint = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabPayslip = New System.Windows.Forms.TabControl
        Me.tabpPayslip = New System.Windows.Forms.TabPage
        Me.txtNetSalaryWithOpenBal = New eZee.TextBox.AlphanumericTextBox
        Me.lblNetSalaryWithOpenBal = New System.Windows.Forms.Label
        Me.txtTranHeadFormula = New System.Windows.Forms.TextBox
        Me.txtNetsalary = New eZee.TextBox.AlphanumericTextBox
        Me.txtTotaldeduction = New eZee.TextBox.AlphanumericTextBox
        Me.txtTotaladdition = New eZee.TextBox.AlphanumericTextBox
        Me.lvPayslip = New eZee.Common.eZeeListView(Me.components)
        Me.colhSrNo = New System.Windows.Forms.ColumnHeader
        Me.colhTranHead = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.colhAddition = New System.Windows.Forms.ColumnHeader
        Me.colhDeduction = New System.Windows.Forms.ColumnHeader
        Me.colhInformational = New System.Windows.Forms.ColumnHeader
        Me.lblTotaladdition = New System.Windows.Forms.Label
        Me.lblTotaldeduction = New System.Windows.Forms.Label
        Me.lblNetsalary = New System.Windows.Forms.Label
        Me.tabpEmployeeInfo = New System.Windows.Forms.TabPage
        Me.gbEmployeeWorkDetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDaysOnHold = New eZee.TextBox.NumericTextBox
        Me.lblDaysOnHold = New System.Windows.Forms.Label
        Me.txtWorkedHours = New System.Windows.Forms.TextBox
        Me.txtShortHours = New System.Windows.Forms.TextBox
        Me.txtExtraHours = New System.Windows.Forms.TextBox
        Me.esLine2 = New eZee.Common.eZeeStraightLine
        Me.lblWorkedHr = New System.Windows.Forms.Label
        Me.txtWorkedDays = New eZee.TextBox.NumericTextBox
        Me.lblShortHour = New System.Windows.Forms.Label
        Me.lblWorkedDays = New System.Windows.Forms.Label
        Me.txtAbsent = New eZee.TextBox.NumericTextBox
        Me.lblAbsent = New System.Windows.Forms.Label
        Me.lblExtraHours = New System.Windows.Forms.Label
        Me.tabpPPA = New System.Windows.Forms.TabPage
        Me.lvPPA = New eZee.Common.eZeeListView(Me.components)
        Me.colhPPA = New System.Windows.Forms.ColumnHeader
        Me.colhPPAEmp = New System.Windows.Forms.ColumnHeader
        Me.colhPPACCenter = New System.Windows.Forms.ColumnHeader
        Me.colhPPADate = New System.Windows.Forms.ColumnHeader
        Me.colhPPAValue = New System.Windows.Forms.ColumnHeader
        Me.cplhPPAUoM = New System.Windows.Forms.ColumnHeader
        Me.colhPPARate = New System.Windows.Forms.ColumnHeader
        Me.colhPPAAmount = New System.Windows.Forms.ColumnHeader
        Me.colhPPANormalFactor = New System.Windows.Forms.ColumnHeader
        Me.colhPPAPHFactor = New System.Windows.Forms.ColumnHeader
        Me.colhPPANormalHours = New System.Windows.Forms.ColumnHeader
        Me.colhPPAOTHours = New System.Windows.Forms.ColumnHeader
        Me.colhPPANormalAmount = New System.Windows.Forms.ColumnHeader
        Me.colhPPAOTAmount = New System.Windows.Forms.ColumnHeader
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.gbPayslipInfo.SuspendLayout()
        Me.gbEmployeeinformation.SuspendLayout()
        Me.gbPeriodWorkDetail.SuspendLayout()
        Me.PnlPayList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.tabPayslip.SuspendLayout()
        Me.tabpPayslip.SuspendLayout()
        Me.tabpEmployeeInfo.SuspendLayout()
        Me.gbEmployeeWorkDetail.SuspendLayout()
        Me.tabpPPA.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbPayslipInfo
        '
        Me.gbPayslipInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPayslipInfo.Checked = False
        Me.gbPayslipInfo.CollapseAllExceptThis = False
        Me.gbPayslipInfo.CollapsedHoverImage = Nothing
        Me.gbPayslipInfo.CollapsedNormalImage = Nothing
        Me.gbPayslipInfo.CollapsedPressedImage = Nothing
        Me.gbPayslipInfo.CollapseOnLoad = False
        Me.gbPayslipInfo.Controls.Add(Me.lblOpenBalance)
        Me.gbPayslipInfo.Controls.Add(Me.txtOpenBalance)
        Me.gbPayslipInfo.Controls.Add(Me.txtDate)
        Me.gbPayslipInfo.Controls.Add(Me.lblGrade)
        Me.gbPayslipInfo.Controls.Add(Me.lblGradeLevel)
        Me.gbPayslipInfo.Controls.Add(Me.txtGradeLevel)
        Me.gbPayslipInfo.Controls.Add(Me.txtGrade)
        Me.gbPayslipInfo.Controls.Add(Me.lblGradeGroup)
        Me.gbPayslipInfo.Controls.Add(Me.txtGradeGroup)
        Me.gbPayslipInfo.Controls.Add(Me.txtScale)
        Me.gbPayslipInfo.Controls.Add(Me.txtEmployee)
        Me.gbPayslipInfo.Controls.Add(Me.txtPayPeriod)
        Me.gbPayslipInfo.Controls.Add(Me.txtPayYear)
        Me.gbPayslipInfo.Controls.Add(Me.lblScale)
        Me.gbPayslipInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbPayslipInfo.Controls.Add(Me.lblPayyear)
        Me.gbPayslipInfo.Controls.Add(Me.lblEmployee)
        Me.gbPayslipInfo.Controls.Add(Me.txtVoucherNo)
        Me.gbPayslipInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbPayslipInfo.Controls.Add(Me.lblDate)
        Me.gbPayslipInfo.ExpandedHoverImage = Nothing
        Me.gbPayslipInfo.ExpandedNormalImage = Nothing
        Me.gbPayslipInfo.ExpandedPressedImage = Nothing
        Me.gbPayslipInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayslipInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayslipInfo.HeaderHeight = 25
        Me.gbPayslipInfo.HeaderMessage = ""
        Me.gbPayslipInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayslipInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayslipInfo.HeightOnCollapse = 0
        Me.gbPayslipInfo.LeftTextSpace = 0
        Me.gbPayslipInfo.Location = New System.Drawing.Point(6, 6)
        Me.gbPayslipInfo.Name = "gbPayslipInfo"
        Me.gbPayslipInfo.OpenHeight = 300
        Me.gbPayslipInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayslipInfo.ShowBorder = True
        Me.gbPayslipInfo.ShowCheckBox = False
        Me.gbPayslipInfo.ShowCollapseButton = False
        Me.gbPayslipInfo.ShowDefaultBorderColor = True
        Me.gbPayslipInfo.ShowDownButton = False
        Me.gbPayslipInfo.ShowHeader = True
        Me.gbPayslipInfo.Size = New System.Drawing.Size(726, 118)
        Me.gbPayslipInfo.TabIndex = 0
        Me.gbPayslipInfo.Temp = 0
        Me.gbPayslipInfo.Text = "Pay Slip Information"
        Me.gbPayslipInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOpenBalance
        '
        Me.lblOpenBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpenBalance.Location = New System.Drawing.Point(529, 90)
        Me.lblOpenBalance.Name = "lblOpenBalance"
        Me.lblOpenBalance.Size = New System.Drawing.Size(83, 16)
        Me.lblOpenBalance.TabIndex = 29
        Me.lblOpenBalance.Text = "Open. Balance"
        Me.lblOpenBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpenBalance
        '
        Me.txtOpenBalance.Flags = 0
        Me.txtOpenBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpenBalance.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOpenBalance.Location = New System.Drawing.Point(614, 88)
        Me.txtOpenBalance.Name = "txtOpenBalance"
        Me.txtOpenBalance.ReadOnly = True
        Me.txtOpenBalance.Size = New System.Drawing.Size(100, 21)
        Me.txtOpenBalance.TabIndex = 28
        Me.txtOpenBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDate
        '
        Me.txtDate.Flags = 0
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDate.Location = New System.Drawing.Point(97, 34)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(89, 21)
        Me.txtDate.TabIndex = 0
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(196, 90)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(45, 16)
        Me.lblGrade.TabIndex = 26
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(346, 90)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(74, 16)
        Me.lblGradeLevel.TabIndex = 25
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGradeLevel
        '
        Me.txtGradeLevel.Flags = 0
        Me.txtGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGradeLevel.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGradeLevel.Location = New System.Drawing.Point(421, 88)
        Me.txtGradeLevel.Name = "txtGradeLevel"
        Me.txtGradeLevel.ReadOnly = True
        Me.txtGradeLevel.Size = New System.Drawing.Size(100, 21)
        Me.txtGradeLevel.TabIndex = 8
        '
        'txtGrade
        '
        Me.txtGrade.Flags = 0
        Me.txtGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrade.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGrade.Location = New System.Drawing.Point(245, 88)
        Me.txtGrade.Name = "txtGrade"
        Me.txtGrade.ReadOnly = True
        Me.txtGrade.Size = New System.Drawing.Size(89, 21)
        Me.txtGrade.TabIndex = 4
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(6, 90)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(87, 16)
        Me.lblGradeGroup.TabIndex = 22
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGradeGroup
        '
        Me.txtGradeGroup.Flags = 0
        Me.txtGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGradeGroup.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGradeGroup.Location = New System.Drawing.Point(97, 88)
        Me.txtGradeGroup.Name = "txtGradeGroup"
        Me.txtGradeGroup.ReadOnly = True
        Me.txtGradeGroup.Size = New System.Drawing.Size(89, 21)
        Me.txtGradeGroup.TabIndex = 3
        '
        'txtScale
        '
        Me.txtScale.Flags = 0
        Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScale.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtScale.Location = New System.Drawing.Point(421, 61)
        Me.txtScale.Name = "txtScale"
        Me.txtScale.ReadOnly = True
        Me.txtScale.Size = New System.Drawing.Size(100, 21)
        Me.txtScale.TabIndex = 7
        Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEmployee
        '
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(97, 61)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(237, 21)
        Me.txtEmployee.TabIndex = 2
        '
        'txtPayPeriod
        '
        Me.txtPayPeriod.Flags = 0
        Me.txtPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayPeriod.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPayPeriod.Location = New System.Drawing.Point(614, 61)
        Me.txtPayPeriod.Name = "txtPayPeriod"
        Me.txtPayPeriod.ReadOnly = True
        Me.txtPayPeriod.Size = New System.Drawing.Size(100, 21)
        Me.txtPayPeriod.TabIndex = 6
        '
        'txtPayYear
        '
        Me.txtPayYear.Flags = 0
        Me.txtPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayYear.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPayYear.Location = New System.Drawing.Point(614, 34)
        Me.txtPayYear.Name = "txtPayYear"
        Me.txtPayYear.ReadOnly = True
        Me.txtPayYear.Size = New System.Drawing.Size(100, 21)
        Me.txtPayYear.TabIndex = 5
        '
        'lblScale
        '
        Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScale.Location = New System.Drawing.Point(346, 63)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(72, 16)
        Me.lblScale.TabIndex = 11
        Me.lblScale.Text = "Scale"
        Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(529, 63)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(80, 16)
        Me.lblPayPeriod.TabIndex = 9
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayyear
        '
        Me.lblPayyear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayyear.Location = New System.Drawing.Point(529, 36)
        Me.lblPayyear.Name = "lblPayyear"
        Me.lblPayyear.Size = New System.Drawing.Size(80, 16)
        Me.lblPayyear.TabIndex = 7
        Me.lblPayyear.Text = "Pay Year"
        Me.lblPayyear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(6, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(87, 16)
        Me.lblEmployee.TabIndex = 5
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.Flags = 0
        Me.txtVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucherNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucherNo.Location = New System.Drawing.Point(421, 34)
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.ReadOnly = True
        Me.txtVoucherNo.Size = New System.Drawing.Size(100, 21)
        Me.txtVoucherNo.TabIndex = 1
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(346, 36)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(72, 16)
        Me.lblVoucherNo.TabIndex = 3
        Me.lblVoucherNo.Text = "Voucher No."
        Me.lblVoucherNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(6, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(87, 16)
        Me.lblDate.TabIndex = 1
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeinformation
        '
        Me.gbEmployeeinformation.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeinformation.Checked = False
        Me.gbEmployeeinformation.CollapseAllExceptThis = False
        Me.gbEmployeeinformation.CollapsedHoverImage = Nothing
        Me.gbEmployeeinformation.CollapsedNormalImage = Nothing
        Me.gbEmployeeinformation.CollapsedPressedImage = Nothing
        Me.gbEmployeeinformation.CollapseOnLoad = False
        Me.gbEmployeeinformation.Controls.Add(Me.lvShift)
        Me.gbEmployeeinformation.Controls.Add(Me.txtUnit)
        Me.gbEmployeeinformation.Controls.Add(Me.lblUnit)
        Me.gbEmployeeinformation.Controls.Add(Me.lblDepartment)
        Me.gbEmployeeinformation.Controls.Add(Me.imgImageControl)
        Me.gbEmployeeinformation.Controls.Add(Me.EZeeStraightLine1)
        Me.gbEmployeeinformation.Controls.Add(Me.lblEmployeeImage)
        Me.gbEmployeeinformation.Controls.Add(Me.txtDepartment)
        Me.gbEmployeeinformation.Controls.Add(Me.txtSection)
        Me.gbEmployeeinformation.Controls.Add(Me.lblSection)
        Me.gbEmployeeinformation.Controls.Add(Me.txtType)
        Me.gbEmployeeinformation.Controls.Add(Me.lblType)
        Me.gbEmployeeinformation.ExpandedHoverImage = Nothing
        Me.gbEmployeeinformation.ExpandedNormalImage = Nothing
        Me.gbEmployeeinformation.ExpandedPressedImage = Nothing
        Me.gbEmployeeinformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeinformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeinformation.HeaderHeight = 25
        Me.gbEmployeeinformation.HeaderMessage = ""
        Me.gbEmployeeinformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeinformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeinformation.HeightOnCollapse = 0
        Me.gbEmployeeinformation.LeftTextSpace = 0
        Me.gbEmployeeinformation.Location = New System.Drawing.Point(6, 6)
        Me.gbEmployeeinformation.Name = "gbEmployeeinformation"
        Me.gbEmployeeinformation.OpenHeight = 300
        Me.gbEmployeeinformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeinformation.ShowBorder = True
        Me.gbEmployeeinformation.ShowCheckBox = False
        Me.gbEmployeeinformation.ShowCollapseButton = False
        Me.gbEmployeeinformation.ShowDefaultBorderColor = True
        Me.gbEmployeeinformation.ShowDownButton = False
        Me.gbEmployeeinformation.ShowHeader = True
        Me.gbEmployeeinformation.Size = New System.Drawing.Size(709, 171)
        Me.gbEmployeeinformation.TabIndex = 9
        Me.gbEmployeeinformation.Temp = 0
        Me.gbEmployeeinformation.Text = "Employee More Information"
        Me.gbEmployeeinformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvShift
        '
        Me.lvShift.BackColorOnChecked = True
        Me.lvShift.ColumnHeaders = Nothing
        Me.lvShift.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhShift})
        Me.lvShift.CompulsoryColumns = ""
        Me.lvShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvShift.FullRowSelect = True
        Me.lvShift.GridLines = True
        Me.lvShift.GroupingColumn = Nothing
        Me.lvShift.HideSelection = False
        Me.lvShift.Location = New System.Drawing.Point(333, 32)
        Me.lvShift.MinColumnWidth = 50
        Me.lvShift.MultiSelect = False
        Me.lvShift.Name = "lvShift"
        Me.lvShift.OptionalColumns = ""
        Me.lvShift.ShowMoreItem = False
        Me.lvShift.ShowSaveItem = False
        Me.lvShift.ShowSelectAll = True
        Me.lvShift.ShowSizeAllColumnsToFit = True
        Me.lvShift.Size = New System.Drawing.Size(214, 133)
        Me.lvShift.Sortable = True
        Me.lvShift.TabIndex = 172
        Me.lvShift.UseCompatibleStateImageBehavior = False
        Me.lvShift.View = System.Windows.Forms.View.Details
        '
        'colhShift
        '
        Me.colhShift.Tag = "colhShift"
        Me.colhShift.Text = "Shift"
        Me.colhShift.Width = 210
        '
        'txtUnit
        '
        Me.txtUnit.BackColor = System.Drawing.SystemColors.Window
        Me.txtUnit.Flags = 0
        Me.txtUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtUnit.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUnit.Location = New System.Drawing.Point(158, 113)
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.ReadOnly = True
        Me.txtUnit.Size = New System.Drawing.Size(134, 21)
        Me.txtUnit.TabIndex = 167
        Me.txtUnit.TabStop = False
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(8, 115)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(144, 16)
        Me.lblUnit.TabIndex = 165
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 88)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(144, 16)
        Me.lblDepartment.TabIndex = 164
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'imgImageControl
        '
        Me.imgImageControl._FileMode = True
        Me.imgImageControl._Image = Nothing
        Me.imgImageControl._ShowAddButton = True
        Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgImageControl._ShowDeleteButton = True
        Me.imgImageControl._ShowPreview = False
        Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
        Me.imgImageControl.Enabled = False
        Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgImageControl.Location = New System.Drawing.Point(556, 51)
        Me.imgImageControl.Name = "imgImageControl"
        Me.imgImageControl.Size = New System.Drawing.Size(144, 114)
        Me.imgImageControl.TabIndex = 162
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(317, 25)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(10, 140)
        Me.EZeeStraightLine1.TabIndex = 158
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lblEmployeeImage
        '
        Me.lblEmployeeImage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblEmployeeImage.Location = New System.Drawing.Point(557, 31)
        Me.lblEmployeeImage.Name = "lblEmployeeImage"
        Me.lblEmployeeImage.Size = New System.Drawing.Size(98, 13)
        Me.lblEmployeeImage.TabIndex = 157
        Me.lblEmployeeImage.Text = "Employee Image"
        Me.lblEmployeeImage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(158, 86)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(134, 21)
        Me.txtDepartment.TabIndex = 17
        Me.txtDepartment.TabStop = False
        '
        'txtSection
        '
        Me.txtSection.BackColor = System.Drawing.SystemColors.Window
        Me.txtSection.Flags = 0
        Me.txtSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtSection.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSection.Location = New System.Drawing.Point(158, 59)
        Me.txtSection.Name = "txtSection"
        Me.txtSection.ReadOnly = True
        Me.txtSection.Size = New System.Drawing.Size(134, 21)
        Me.txtSection.TabIndex = 16
        Me.txtSection.TabStop = False
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 63)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(144, 13)
        Me.lblSection.TabIndex = 24
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtType
        '
        Me.txtType.BackColor = System.Drawing.SystemColors.Window
        Me.txtType.Flags = 0
        Me.txtType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtType.Location = New System.Drawing.Point(158, 32)
        Me.txtType.Name = "txtType"
        Me.txtType.ReadOnly = True
        Me.txtType.Size = New System.Drawing.Size(134, 21)
        Me.txtType.TabIndex = 15
        Me.txtType.TabStop = False
        '
        'lblType
        '
        Me.lblType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblType.Location = New System.Drawing.Point(8, 36)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(144, 13)
        Me.lblType.TabIndex = 22
        Me.lblType.Text = "Employement Type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(224, 15)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(144, 16)
        Me.lblShift.TabIndex = 170
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblShift.Visible = False
        '
        'txtShift
        '
        Me.txtShift.BackColor = System.Drawing.SystemColors.Window
        Me.txtShift.Flags = 0
        Me.txtShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtShift.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtShift.Location = New System.Drawing.Point(374, 13)
        Me.txtShift.Name = "txtShift"
        Me.txtShift.ReadOnly = True
        Me.txtShift.Size = New System.Drawing.Size(134, 21)
        Me.txtShift.TabIndex = 169
        Me.txtShift.TabStop = False
        Me.txtShift.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(668, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbPeriodWorkDetail
        '
        Me.gbPeriodWorkDetail.BackColor = System.Drawing.Color.Transparent
        Me.gbPeriodWorkDetail.BorderColor = System.Drawing.Color.Black
        Me.gbPeriodWorkDetail.Checked = False
        Me.gbPeriodWorkDetail.CollapseAllExceptThis = False
        Me.gbPeriodWorkDetail.CollapsedHoverImage = Nothing
        Me.gbPeriodWorkDetail.CollapsedNormalImage = Nothing
        Me.gbPeriodWorkDetail.CollapsedPressedImage = Nothing
        Me.gbPeriodWorkDetail.CollapseOnLoad = False
        Me.gbPeriodWorkDetail.Controls.Add(Me.txtPayableHr)
        Me.gbPeriodWorkDetail.Controls.Add(Me.lblDaysinPeriod)
        Me.gbPeriodWorkDetail.Controls.Add(Me.lblHolidays)
        Me.gbPeriodWorkDetail.Controls.Add(Me.esLine1)
        Me.gbPeriodWorkDetail.Controls.Add(Me.lblPayableDays)
        Me.gbPeriodWorkDetail.Controls.Add(Me.txtDaysInPeriod)
        Me.gbPeriodWorkDetail.Controls.Add(Me.lblPayableHours)
        Me.gbPeriodWorkDetail.Controls.Add(Me.txtPayableDay)
        Me.gbPeriodWorkDetail.Controls.Add(Me.txtHolidayInMonth)
        Me.gbPeriodWorkDetail.ExpandedHoverImage = Nothing
        Me.gbPeriodWorkDetail.ExpandedNormalImage = Nothing
        Me.gbPeriodWorkDetail.ExpandedPressedImage = Nothing
        Me.gbPeriodWorkDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPeriodWorkDetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPeriodWorkDetail.HeaderHeight = 25
        Me.gbPeriodWorkDetail.HeaderMessage = ""
        Me.gbPeriodWorkDetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPeriodWorkDetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPeriodWorkDetail.HeightOnCollapse = 0
        Me.gbPeriodWorkDetail.LeftTextSpace = 0
        Me.gbPeriodWorkDetail.Location = New System.Drawing.Point(6, 183)
        Me.gbPeriodWorkDetail.Name = "gbPeriodWorkDetail"
        Me.gbPeriodWorkDetail.OpenHeight = 89
        Me.gbPeriodWorkDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.gbPeriodWorkDetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPeriodWorkDetail.ShowBorder = True
        Me.gbPeriodWorkDetail.ShowCheckBox = False
        Me.gbPeriodWorkDetail.ShowCollapseButton = False
        Me.gbPeriodWorkDetail.ShowDefaultBorderColor = True
        Me.gbPeriodWorkDetail.ShowDownButton = False
        Me.gbPeriodWorkDetail.ShowHeader = True
        Me.gbPeriodWorkDetail.Size = New System.Drawing.Size(709, 89)
        Me.gbPeriodWorkDetail.TabIndex = 132
        Me.gbPeriodWorkDetail.Temp = 0
        Me.gbPeriodWorkDetail.Text = "Period Work Detail"
        Me.gbPeriodWorkDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPayableHr
        '
        Me.txtPayableHr.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayableHr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayableHr.Location = New System.Drawing.Point(539, 59)
        Me.txtPayableHr.Name = "txtPayableHr"
        Me.txtPayableHr.ReadOnly = True
        Me.txtPayableHr.Size = New System.Drawing.Size(134, 21)
        Me.txtPayableHr.TabIndex = 174
        Me.txtPayableHr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDaysinPeriod
        '
        Me.lblDaysinPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblDaysinPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblDaysinPeriod.Name = "lblDaysinPeriod"
        Me.lblDaysinPeriod.Size = New System.Drawing.Size(144, 13)
        Me.lblDaysinPeriod.TabIndex = 1
        Me.lblDaysinPeriod.Text = "Days in Period"
        Me.lblDaysinPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHolidays
        '
        Me.lblHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblHolidays.Location = New System.Drawing.Point(8, 63)
        Me.lblHolidays.Name = "lblHolidays"
        Me.lblHolidays.Size = New System.Drawing.Size(144, 13)
        Me.lblHolidays.TabIndex = 5
        Me.lblHolidays.Text = "Paid Holidays (Paid Leave)"
        Me.lblHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'esLine1
        '
        Me.esLine1.BackColor = System.Drawing.Color.Transparent
        Me.esLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.esLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.esLine1.Location = New System.Drawing.Point(319, 28)
        Me.esLine1.Name = "esLine1"
        Me.esLine1.Size = New System.Drawing.Size(10, 55)
        Me.esLine1.TabIndex = 154
        Me.esLine1.Text = "EZeeStraightLine1"
        '
        'lblPayableDays
        '
        Me.lblPayableDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblPayableDays.Location = New System.Drawing.Point(389, 36)
        Me.lblPayableDays.Name = "lblPayableDays"
        Me.lblPayableDays.Size = New System.Drawing.Size(144, 13)
        Me.lblPayableDays.TabIndex = 7
        Me.lblPayableDays.Text = "Total Payable Days"
        Me.lblPayableDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDaysInPeriod
        '
        Me.txtDaysInPeriod.AllowNegative = True
        Me.txtDaysInPeriod.BackColor = System.Drawing.SystemColors.Window
        Me.txtDaysInPeriod.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDaysInPeriod.DigitsInGroup = 0
        Me.txtDaysInPeriod.Flags = 0
        Me.txtDaysInPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDaysInPeriod.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDaysInPeriod.Location = New System.Drawing.Point(158, 32)
        Me.txtDaysInPeriod.MaxDecimalPlaces = 6
        Me.txtDaysInPeriod.MaxWholeDigits = 21
        Me.txtDaysInPeriod.Name = "txtDaysInPeriod"
        Me.txtDaysInPeriod.Prefix = ""
        Me.txtDaysInPeriod.RangeMax = 1.7976931348623157E+308
        Me.txtDaysInPeriod.RangeMin = -1.7976931348623157E+308
        Me.txtDaysInPeriod.ReadOnly = True
        Me.txtDaysInPeriod.Size = New System.Drawing.Size(134, 21)
        Me.txtDaysInPeriod.TabIndex = 18
        Me.txtDaysInPeriod.Text = "0"
        Me.txtDaysInPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPayableHours
        '
        Me.lblPayableHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblPayableHours.Location = New System.Drawing.Point(389, 63)
        Me.lblPayableHours.Name = "lblPayableHours"
        Me.lblPayableHours.Size = New System.Drawing.Size(144, 13)
        Me.lblPayableHours.TabIndex = 9
        Me.lblPayableHours.Text = "Total Payable Hours"
        Me.lblPayableHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPayableDay
        '
        Me.txtPayableDay.AllowNegative = True
        Me.txtPayableDay.BackColor = System.Drawing.SystemColors.Window
        Me.txtPayableDay.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPayableDay.DigitsInGroup = 0
        Me.txtPayableDay.Flags = 0
        Me.txtPayableDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayableDay.Location = New System.Drawing.Point(539, 32)
        Me.txtPayableDay.MaxDecimalPlaces = 6
        Me.txtPayableDay.MaxWholeDigits = 21
        Me.txtPayableDay.Name = "txtPayableDay"
        Me.txtPayableDay.Prefix = ""
        Me.txtPayableDay.RangeMax = 1.7976931348623157E+308
        Me.txtPayableDay.RangeMin = -1.7976931348623157E+308
        Me.txtPayableDay.ReadOnly = True
        Me.txtPayableDay.Size = New System.Drawing.Size(134, 21)
        Me.txtPayableDay.TabIndex = 20
        Me.txtPayableDay.Text = "0"
        Me.txtPayableDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHolidayInMonth
        '
        Me.txtHolidayInMonth.AllowNegative = True
        Me.txtHolidayInMonth.BackColor = System.Drawing.SystemColors.Window
        Me.txtHolidayInMonth.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtHolidayInMonth.DigitsInGroup = 0
        Me.txtHolidayInMonth.Flags = 0
        Me.txtHolidayInMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHolidayInMonth.Location = New System.Drawing.Point(158, 59)
        Me.txtHolidayInMonth.MaxDecimalPlaces = 6
        Me.txtHolidayInMonth.MaxWholeDigits = 21
        Me.txtHolidayInMonth.Name = "txtHolidayInMonth"
        Me.txtHolidayInMonth.Prefix = ""
        Me.txtHolidayInMonth.RangeMax = 1.7976931348623157E+308
        Me.txtHolidayInMonth.RangeMin = -1.7976931348623157E+308
        Me.txtHolidayInMonth.ReadOnly = True
        Me.txtHolidayInMonth.Size = New System.Drawing.Size(134, 21)
        Me.txtHolidayInMonth.TabIndex = 19
        Me.txtHolidayInMonth.Text = "0"
        Me.txtHolidayInMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PnlPayList
        '
        Me.PnlPayList.Controls.Add(Me.eZeeHeader)
        Me.PnlPayList.Controls.Add(Me.objFooter)
        Me.PnlPayList.Controls.Add(Me.tabPayslip)
        Me.PnlPayList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlPayList.Location = New System.Drawing.Point(0, 0)
        Me.PnlPayList.Name = "PnlPayList"
        Me.PnlPayList.Size = New System.Drawing.Size(770, 549)
        Me.PnlPayList.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(770, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "View PaySlip"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.chkIgnoreZero)
        Me.objFooter.Controls.Add(Me.lblShift)
        Me.objFooter.Controls.Add(Me.btnPrint)
        Me.objFooter.Controls.Add(Me.txtShift)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 494)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(770, 55)
        Me.objFooter.TabIndex = 1
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.AutoSize = True
        Me.chkIgnoreZero.Location = New System.Drawing.Point(10, 13)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(145, 17)
        Me.chkIgnoreZero.TabIndex = 2
        Me.chkIgnoreZero.Text = "&Ignore Zero Value Heads"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.White
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrint.BorderColor = System.Drawing.Color.Empty
        Me.btnPrint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.Black
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Location = New System.Drawing.Point(572, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Size = New System.Drawing.Size(90, 30)
        Me.btnPrint.TabIndex = 0
        Me.btnPrint.Text = "&Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'tabPayslip
        '
        Me.tabPayslip.Controls.Add(Me.tabpPayslip)
        Me.tabPayslip.Controls.Add(Me.tabpEmployeeInfo)
        Me.tabPayslip.Controls.Add(Me.tabpPPA)
        Me.tabPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPayslip.Location = New System.Drawing.Point(10, 66)
        Me.tabPayslip.Name = "tabPayslip"
        Me.tabPayslip.SelectedIndex = 0
        Me.tabPayslip.Size = New System.Drawing.Size(747, 423)
        Me.tabPayslip.TabIndex = 0
        '
        'tabpPayslip
        '
        Me.tabpPayslip.Controls.Add(Me.txtNetSalaryWithOpenBal)
        Me.tabpPayslip.Controls.Add(Me.lblNetSalaryWithOpenBal)
        Me.tabpPayslip.Controls.Add(Me.txtTranHeadFormula)
        Me.tabpPayslip.Controls.Add(Me.txtNetsalary)
        Me.tabpPayslip.Controls.Add(Me.txtTotaldeduction)
        Me.tabpPayslip.Controls.Add(Me.txtTotaladdition)
        Me.tabpPayslip.Controls.Add(Me.lvPayslip)
        Me.tabpPayslip.Controls.Add(Me.lblTotaladdition)
        Me.tabpPayslip.Controls.Add(Me.lblTotaldeduction)
        Me.tabpPayslip.Controls.Add(Me.lblNetsalary)
        Me.tabpPayslip.Controls.Add(Me.gbPayslipInfo)
        Me.tabpPayslip.Location = New System.Drawing.Point(4, 22)
        Me.tabpPayslip.Name = "tabpPayslip"
        Me.tabpPayslip.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpPayslip.Size = New System.Drawing.Size(739, 397)
        Me.tabpPayslip.TabIndex = 0
        Me.tabpPayslip.Text = "Pay Slip Information"
        Me.tabpPayslip.UseVisualStyleBackColor = True
        '
        'txtNetSalaryWithOpenBal
        '
        Me.txtNetSalaryWithOpenBal.BackColor = System.Drawing.SystemColors.Window
        Me.txtNetSalaryWithOpenBal.Flags = 0
        Me.txtNetSalaryWithOpenBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetSalaryWithOpenBal.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNetSalaryWithOpenBal.Location = New System.Drawing.Point(310, 368)
        Me.txtNetSalaryWithOpenBal.Name = "txtNetSalaryWithOpenBal"
        Me.txtNetSalaryWithOpenBal.ReadOnly = True
        Me.txtNetSalaryWithOpenBal.Size = New System.Drawing.Size(144, 21)
        Me.txtNetSalaryWithOpenBal.TabIndex = 177
        Me.txtNetSalaryWithOpenBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNetSalaryWithOpenBal
        '
        Me.lblNetSalaryWithOpenBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetSalaryWithOpenBal.Location = New System.Drawing.Point(132, 370)
        Me.lblNetSalaryWithOpenBal.Name = "lblNetSalaryWithOpenBal"
        Me.lblNetSalaryWithOpenBal.Size = New System.Drawing.Size(172, 16)
        Me.lblNetSalaryWithOpenBal.TabIndex = 176
        Me.lblNetSalaryWithOpenBal.Text = "Net Salary with Opening Balance "
        Me.lblNetSalaryWithOpenBal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTranHeadFormula
        '
        Me.txtTranHeadFormula.BackColor = System.Drawing.SystemColors.Window
        Me.txtTranHeadFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTranHeadFormula.Location = New System.Drawing.Point(8, 314)
        Me.txtTranHeadFormula.Multiline = True
        Me.txtTranHeadFormula.Name = "txtTranHeadFormula"
        Me.txtTranHeadFormula.ReadOnly = True
        Me.txtTranHeadFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTranHeadFormula.Size = New System.Drawing.Size(446, 48)
        Me.txtTranHeadFormula.TabIndex = 175
        Me.txtTranHeadFormula.Visible = False
        '
        'txtNetsalary
        '
        Me.txtNetsalary.BackColor = System.Drawing.SystemColors.Window
        Me.txtNetsalary.Flags = 0
        Me.txtNetsalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetsalary.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNetsalary.Location = New System.Drawing.Point(588, 368)
        Me.txtNetsalary.Name = "txtNetsalary"
        Me.txtNetsalary.ReadOnly = True
        Me.txtNetsalary.Size = New System.Drawing.Size(144, 21)
        Me.txtNetsalary.TabIndex = 42
        Me.txtNetsalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotaldeduction
        '
        Me.txtTotaldeduction.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotaldeduction.Flags = 0
        Me.txtTotaldeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotaldeduction.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTotaldeduction.Location = New System.Drawing.Point(588, 341)
        Me.txtTotaldeduction.Name = "txtTotaldeduction"
        Me.txtTotaldeduction.ReadOnly = True
        Me.txtTotaldeduction.Size = New System.Drawing.Size(144, 21)
        Me.txtTotaldeduction.TabIndex = 41
        Me.txtTotaldeduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotaladdition
        '
        Me.txtTotaladdition.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotaladdition.Flags = 0
        Me.txtTotaladdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotaladdition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTotaladdition.Location = New System.Drawing.Point(588, 314)
        Me.txtTotaladdition.Name = "txtTotaladdition"
        Me.txtTotaladdition.ReadOnly = True
        Me.txtTotaladdition.Size = New System.Drawing.Size(144, 21)
        Me.txtTotaladdition.TabIndex = 40
        Me.txtTotaladdition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lvPayslip
        '
        Me.lvPayslip.BackColorOnChecked = True
        Me.lvPayslip.ColumnHeaders = Nothing
        Me.lvPayslip.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhSrNo, Me.colhTranHead, Me.colhCostCenter, Me.colhAddition, Me.colhDeduction, Me.colhInformational})
        Me.lvPayslip.CompulsoryColumns = ""
        Me.lvPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPayslip.FullRowSelect = True
        Me.lvPayslip.GridLines = True
        Me.lvPayslip.GroupingColumn = Nothing
        Me.lvPayslip.HideSelection = False
        Me.lvPayslip.Location = New System.Drawing.Point(8, 130)
        Me.lvPayslip.MinColumnWidth = 50
        Me.lvPayslip.MultiSelect = False
        Me.lvPayslip.Name = "lvPayslip"
        Me.lvPayslip.OptionalColumns = ""
        Me.lvPayslip.ShowMoreItem = False
        Me.lvPayslip.ShowSaveItem = False
        Me.lvPayslip.ShowSelectAll = True
        Me.lvPayslip.ShowSizeAllColumnsToFit = True
        Me.lvPayslip.Size = New System.Drawing.Size(725, 178)
        Me.lvPayslip.Sortable = True
        Me.lvPayslip.TabIndex = 1
        Me.lvPayslip.UseCompatibleStateImageBehavior = False
        Me.lvPayslip.View = System.Windows.Forms.View.Details
        '
        'colhSrNo
        '
        Me.colhSrNo.Tag = "colhSrNo"
        Me.colhSrNo.Text = "S.N."
        Me.colhSrNo.Width = 40
        '
        'colhTranHead
        '
        Me.colhTranHead.Tag = "colhTranHead"
        Me.colhTranHead.Text = "Particulars"
        Me.colhTranHead.Width = 180
        '
        'colhCostCenter
        '
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 80
        '
        'colhAddition
        '
        Me.colhAddition.Tag = "colhAddition"
        Me.colhAddition.Text = "Addition"
        Me.colhAddition.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAddition.Width = 140
        '
        'colhDeduction
        '
        Me.colhDeduction.Tag = "colhDeduction"
        Me.colhDeduction.Text = "Deduction"
        Me.colhDeduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhDeduction.Width = 140
        '
        'colhInformational
        '
        Me.colhInformational.Tag = "colhInformational"
        Me.colhInformational.Text = "Informational"
        Me.colhInformational.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInformational.Width = 140
        '
        'lblTotaladdition
        '
        Me.lblTotaladdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotaladdition.Location = New System.Drawing.Point(478, 316)
        Me.lblTotaladdition.Name = "lblTotaladdition"
        Me.lblTotaladdition.Size = New System.Drawing.Size(104, 16)
        Me.lblTotaladdition.TabIndex = 34
        Me.lblTotaladdition.Text = "Total Addition"
        Me.lblTotaladdition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotaldeduction
        '
        Me.lblTotaldeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotaldeduction.Location = New System.Drawing.Point(478, 343)
        Me.lblTotaldeduction.Name = "lblTotaldeduction"
        Me.lblTotaldeduction.Size = New System.Drawing.Size(104, 16)
        Me.lblTotaldeduction.TabIndex = 36
        Me.lblTotaldeduction.Text = "Total Deduction"
        Me.lblTotaldeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNetsalary
        '
        Me.lblNetsalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetsalary.Location = New System.Drawing.Point(478, 370)
        Me.lblNetsalary.Name = "lblNetsalary"
        Me.lblNetsalary.Size = New System.Drawing.Size(104, 16)
        Me.lblNetsalary.TabIndex = 38
        Me.lblNetsalary.Text = "Net Salary"
        Me.lblNetsalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpEmployeeInfo
        '
        Me.tabpEmployeeInfo.Controls.Add(Me.gbEmployeeWorkDetail)
        Me.tabpEmployeeInfo.Controls.Add(Me.gbEmployeeinformation)
        Me.tabpEmployeeInfo.Controls.Add(Me.gbPeriodWorkDetail)
        Me.tabpEmployeeInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpEmployeeInfo.Name = "tabpEmployeeInfo"
        Me.tabpEmployeeInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpEmployeeInfo.Size = New System.Drawing.Size(739, 397)
        Me.tabpEmployeeInfo.TabIndex = 1
        Me.tabpEmployeeInfo.Text = "Employee Information"
        Me.tabpEmployeeInfo.UseVisualStyleBackColor = True
        '
        'gbEmployeeWorkDetail
        '
        Me.gbEmployeeWorkDetail.BackColor = System.Drawing.Color.Transparent
        Me.gbEmployeeWorkDetail.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeWorkDetail.Checked = False
        Me.gbEmployeeWorkDetail.CollapseAllExceptThis = False
        Me.gbEmployeeWorkDetail.CollapsedHoverImage = Nothing
        Me.gbEmployeeWorkDetail.CollapsedNormalImage = Nothing
        Me.gbEmployeeWorkDetail.CollapsedPressedImage = Nothing
        Me.gbEmployeeWorkDetail.CollapseOnLoad = False
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtDaysOnHold)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblDaysOnHold)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtWorkedHours)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtShortHours)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtExtraHours)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.esLine2)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblWorkedHr)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtWorkedDays)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblShortHour)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblWorkedDays)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.txtAbsent)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblAbsent)
        Me.gbEmployeeWorkDetail.Controls.Add(Me.lblExtraHours)
        Me.gbEmployeeWorkDetail.ExpandedHoverImage = Nothing
        Me.gbEmployeeWorkDetail.ExpandedNormalImage = Nothing
        Me.gbEmployeeWorkDetail.ExpandedPressedImage = Nothing
        Me.gbEmployeeWorkDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeWorkDetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeWorkDetail.HeaderHeight = 25
        Me.gbEmployeeWorkDetail.HeaderMessage = ""
        Me.gbEmployeeWorkDetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeWorkDetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeWorkDetail.HeightOnCollapse = 0
        Me.gbEmployeeWorkDetail.LeftTextSpace = 0
        Me.gbEmployeeWorkDetail.Location = New System.Drawing.Point(6, 278)
        Me.gbEmployeeWorkDetail.Name = "gbEmployeeWorkDetail"
        Me.gbEmployeeWorkDetail.OpenHeight = 118
        Me.gbEmployeeWorkDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.gbEmployeeWorkDetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeWorkDetail.ShowBorder = True
        Me.gbEmployeeWorkDetail.ShowCheckBox = False
        Me.gbEmployeeWorkDetail.ShowCollapseButton = False
        Me.gbEmployeeWorkDetail.ShowDefaultBorderColor = True
        Me.gbEmployeeWorkDetail.ShowDownButton = False
        Me.gbEmployeeWorkDetail.ShowHeader = True
        Me.gbEmployeeWorkDetail.Size = New System.Drawing.Size(709, 113)
        Me.gbEmployeeWorkDetail.TabIndex = 133
        Me.gbEmployeeWorkDetail.Temp = 0
        Me.gbEmployeeWorkDetail.Text = "Employee Work Detail"
        Me.gbEmployeeWorkDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDaysOnHold
        '
        Me.txtDaysOnHold.AllowNegative = True
        Me.txtDaysOnHold.BackColor = System.Drawing.SystemColors.Window
        Me.txtDaysOnHold.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDaysOnHold.DigitsInGroup = 0
        Me.txtDaysOnHold.Flags = 0
        Me.txtDaysOnHold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDaysOnHold.Location = New System.Drawing.Point(158, 86)
        Me.txtDaysOnHold.MaxDecimalPlaces = 6
        Me.txtDaysOnHold.MaxWholeDigits = 21
        Me.txtDaysOnHold.Name = "txtDaysOnHold"
        Me.txtDaysOnHold.Prefix = ""
        Me.txtDaysOnHold.RangeMax = 1.7976931348623157E+308
        Me.txtDaysOnHold.RangeMin = -1.7976931348623157E+308
        Me.txtDaysOnHold.ReadOnly = True
        Me.txtDaysOnHold.Size = New System.Drawing.Size(134, 21)
        Me.txtDaysOnHold.TabIndex = 177
        Me.txtDaysOnHold.Text = "0"
        Me.txtDaysOnHold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDaysOnHold
        '
        Me.lblDaysOnHold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblDaysOnHold.Location = New System.Drawing.Point(8, 90)
        Me.lblDaysOnHold.Name = "lblDaysOnHold"
        Me.lblDaysOnHold.Size = New System.Drawing.Size(144, 13)
        Me.lblDaysOnHold.TabIndex = 176
        Me.lblDaysOnHold.Text = "Days On Hold"
        Me.lblDaysOnHold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWorkedHours
        '
        Me.txtWorkedHours.BackColor = System.Drawing.SystemColors.Window
        Me.txtWorkedHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkedHours.Location = New System.Drawing.Point(158, 59)
        Me.txtWorkedHours.Name = "txtWorkedHours"
        Me.txtWorkedHours.ReadOnly = True
        Me.txtWorkedHours.Size = New System.Drawing.Size(134, 21)
        Me.txtWorkedHours.TabIndex = 173
        Me.txtWorkedHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtShortHours
        '
        Me.txtShortHours.BackColor = System.Drawing.SystemColors.Window
        Me.txtShortHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShortHours.Location = New System.Drawing.Point(539, 86)
        Me.txtShortHours.Name = "txtShortHours"
        Me.txtShortHours.ReadOnly = True
        Me.txtShortHours.Size = New System.Drawing.Size(134, 21)
        Me.txtShortHours.TabIndex = 172
        Me.txtShortHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtExtraHours
        '
        Me.txtExtraHours.BackColor = System.Drawing.SystemColors.Window
        Me.txtExtraHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExtraHours.Location = New System.Drawing.Point(539, 59)
        Me.txtExtraHours.Name = "txtExtraHours"
        Me.txtExtraHours.ReadOnly = True
        Me.txtExtraHours.Size = New System.Drawing.Size(134, 21)
        Me.txtExtraHours.TabIndex = 171
        Me.txtExtraHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'esLine2
        '
        Me.esLine2.BackColor = System.Drawing.Color.Transparent
        Me.esLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.esLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.esLine2.Location = New System.Drawing.Point(319, 26)
        Me.esLine2.Name = "esLine2"
        Me.esLine2.Size = New System.Drawing.Size(10, 81)
        Me.esLine2.TabIndex = 155
        Me.esLine2.Text = "EZeeStraightLine1"
        '
        'lblWorkedHr
        '
        Me.lblWorkedHr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblWorkedHr.Location = New System.Drawing.Point(8, 63)
        Me.lblWorkedHr.Name = "lblWorkedHr"
        Me.lblWorkedHr.Size = New System.Drawing.Size(144, 13)
        Me.lblWorkedHr.TabIndex = 14
        Me.lblWorkedHr.Text = "Worked Hours"
        Me.lblWorkedHr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWorkedDays
        '
        Me.txtWorkedDays.AllowNegative = True
        Me.txtWorkedDays.BackColor = System.Drawing.SystemColors.Window
        Me.txtWorkedDays.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtWorkedDays.DigitsInGroup = 0
        Me.txtWorkedDays.Flags = 0
        Me.txtWorkedDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkedDays.Location = New System.Drawing.Point(158, 32)
        Me.txtWorkedDays.MaxDecimalPlaces = 6
        Me.txtWorkedDays.MaxWholeDigits = 21
        Me.txtWorkedDays.Name = "txtWorkedDays"
        Me.txtWorkedDays.Prefix = ""
        Me.txtWorkedDays.RangeMax = 1.7976931348623157E+308
        Me.txtWorkedDays.RangeMin = -1.7976931348623157E+308
        Me.txtWorkedDays.ReadOnly = True
        Me.txtWorkedDays.Size = New System.Drawing.Size(134, 21)
        Me.txtWorkedDays.TabIndex = 22
        Me.txtWorkedDays.Text = "0"
        Me.txtWorkedDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblShortHour
        '
        Me.lblShortHour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblShortHour.Location = New System.Drawing.Point(389, 88)
        Me.lblShortHour.Name = "lblShortHour"
        Me.lblShortHour.Size = New System.Drawing.Size(144, 13)
        Me.lblShortHour.TabIndex = 22
        Me.lblShortHour.Text = "Short Hours"
        Me.lblShortHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorkedDays
        '
        Me.lblWorkedDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblWorkedDays.Location = New System.Drawing.Point(8, 36)
        Me.lblWorkedDays.Name = "lblWorkedDays"
        Me.lblWorkedDays.Size = New System.Drawing.Size(144, 13)
        Me.lblWorkedDays.TabIndex = 12
        Me.lblWorkedDays.Text = "Worked Days"
        Me.lblWorkedDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAbsent
        '
        Me.txtAbsent.AllowNegative = True
        Me.txtAbsent.BackColor = System.Drawing.SystemColors.Window
        Me.txtAbsent.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAbsent.DigitsInGroup = 0
        Me.txtAbsent.Flags = 0
        Me.txtAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAbsent.Location = New System.Drawing.Point(539, 32)
        Me.txtAbsent.MaxDecimalPlaces = 6
        Me.txtAbsent.MaxWholeDigits = 21
        Me.txtAbsent.Name = "txtAbsent"
        Me.txtAbsent.Prefix = ""
        Me.txtAbsent.RangeMax = 1.7976931348623157E+308
        Me.txtAbsent.RangeMin = -1.7976931348623157E+308
        Me.txtAbsent.ReadOnly = True
        Me.txtAbsent.Size = New System.Drawing.Size(134, 21)
        Me.txtAbsent.TabIndex = 24
        Me.txtAbsent.Text = "0"
        Me.txtAbsent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAbsent
        '
        Me.lblAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblAbsent.Location = New System.Drawing.Point(389, 36)
        Me.lblAbsent.Name = "lblAbsent"
        Me.lblAbsent.Size = New System.Drawing.Size(144, 13)
        Me.lblAbsent.TabIndex = 17
        Me.lblAbsent.Text = "Absent (Unpaid Leave)"
        Me.lblAbsent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExtraHours
        '
        Me.lblExtraHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblExtraHours.Location = New System.Drawing.Point(389, 61)
        Me.lblExtraHours.Name = "lblExtraHours"
        Me.lblExtraHours.Size = New System.Drawing.Size(144, 13)
        Me.lblExtraHours.TabIndex = 19
        Me.lblExtraHours.Text = "Extra Hours"
        Me.lblExtraHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpPPA
        '
        Me.tabpPPA.Controls.Add(Me.lvPPA)
        Me.tabpPPA.Location = New System.Drawing.Point(4, 22)
        Me.tabpPPA.Name = "tabpPPA"
        Me.tabpPPA.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpPPA.Size = New System.Drawing.Size(739, 397)
        Me.tabpPPA.TabIndex = 2
        Me.tabpPPA.Text = "Pay Per Actvity"
        Me.tabpPPA.UseVisualStyleBackColor = True
        '
        'lvPPA
        '
        Me.lvPPA.BackColorOnChecked = True
        Me.lvPPA.ColumnHeaders = Nothing
        Me.lvPPA.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPPA, Me.colhPPAEmp, Me.colhPPACCenter, Me.colhPPADate, Me.colhPPAValue, Me.cplhPPAUoM, Me.colhPPARate, Me.colhPPAAmount, Me.colhPPANormalFactor, Me.colhPPAPHFactor, Me.colhPPANormalHours, Me.colhPPAOTHours, Me.colhPPANormalAmount, Me.colhPPAOTAmount})
        Me.lvPPA.CompulsoryColumns = ""
        Me.lvPPA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPPA.FullRowSelect = True
        Me.lvPPA.GridLines = True
        Me.lvPPA.GroupingColumn = Nothing
        Me.lvPPA.HideSelection = False
        Me.lvPPA.Location = New System.Drawing.Point(8, 6)
        Me.lvPPA.MinColumnWidth = 50
        Me.lvPPA.MultiSelect = False
        Me.lvPPA.Name = "lvPPA"
        Me.lvPPA.OptionalColumns = ""
        Me.lvPPA.ShowItemToolTips = True
        Me.lvPPA.ShowMoreItem = False
        Me.lvPPA.ShowSaveItem = False
        Me.lvPPA.ShowSelectAll = True
        Me.lvPPA.ShowSizeAllColumnsToFit = True
        Me.lvPPA.Size = New System.Drawing.Size(725, 385)
        Me.lvPPA.Sortable = True
        Me.lvPPA.TabIndex = 2
        Me.lvPPA.UseCompatibleStateImageBehavior = False
        Me.lvPPA.View = System.Windows.Forms.View.Details
        '
        'colhPPA
        '
        Me.colhPPA.Tag = "colhPPA"
        Me.colhPPA.Text = "Actvity"
        Me.colhPPA.Width = 0
        '
        'colhPPAEmp
        '
        Me.colhPPAEmp.Tag = "colhPPAEmp"
        Me.colhPPAEmp.Text = "Employee"
        Me.colhPPAEmp.Width = 120
        '
        'colhPPACCenter
        '
        Me.colhPPACCenter.Tag = "colhPPACCenter"
        Me.colhPPACCenter.Text = "CCenter"
        Me.colhPPACCenter.Width = 90
        '
        'colhPPADate
        '
        Me.colhPPADate.Tag = "colhPPADate"
        Me.colhPPADate.Text = "Activity Date"
        Me.colhPPADate.Width = 80
        '
        'colhPPAValue
        '
        Me.colhPPAValue.Tag = "colhPPAValue"
        Me.colhPPAValue.Text = "Unit"
        Me.colhPPAValue.Width = 90
        '
        'cplhPPAUoM
        '
        Me.cplhPPAUoM.Tag = "cplhPPAUoM"
        Me.cplhPPAUoM.Text = "UoM"
        Me.cplhPPAUoM.Width = 90
        '
        'colhPPARate
        '
        Me.colhPPARate.Tag = "colhPPARate"
        Me.colhPPARate.Text = "Rate"
        Me.colhPPARate.Width = 90
        '
        'colhPPAAmount
        '
        Me.colhPPAAmount.Tag = "colhPPAAmount"
        Me.colhPPAAmount.Text = "Amount"
        Me.colhPPAAmount.Width = 90
        '
        'colhPPANormalFactor
        '
        Me.colhPPANormalFactor.Tag = "colhPPANormalFactor"
        Me.colhPPANormalFactor.Text = "Normal Factor"
        Me.colhPPANormalFactor.Width = 90
        '
        'colhPPAPHFactor
        '
        Me.colhPPAPHFactor.Tag = "colhPPAPHFactor"
        Me.colhPPAPHFactor.Text = "PH Factor"
        Me.colhPPAPHFactor.Width = 90
        '
        'colhPPANormalHours
        '
        Me.colhPPANormalHours.Text = "Normal Hours"
        Me.colhPPANormalHours.Width = 90
        '
        'colhPPAOTHours
        '
        Me.colhPPAOTHours.Tag = "colhPPAOTHours"
        Me.colhPPAOTHours.Text = "OT Hours"
        Me.colhPPAOTHours.Width = 90
        '
        'colhPPANormalAmount
        '
        Me.colhPPANormalAmount.Tag = "colhPPANormalAmount"
        Me.colhPPANormalAmount.Text = "Normal Amount"
        Me.colhPPANormalAmount.Width = 90
        '
        'colhPPAOTAmount
        '
        Me.colhPPAOTAmount.Tag = "colhPPAOTAmount"
        Me.colhPPAOTAmount.Text = "OT Amount"
        Me.colhPPAOTAmount.Width = 90
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(8, 314)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(446, 48)
        Me.TextBox1.TabIndex = 175
        Me.TextBox1.Visible = False
        '
        'frmPayslip_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(770, 549)
        Me.Controls.Add(Me.PnlPayList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayslip_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View PaySlip"
        Me.gbPayslipInfo.ResumeLayout(False)
        Me.gbPayslipInfo.PerformLayout()
        Me.gbEmployeeinformation.ResumeLayout(False)
        Me.gbEmployeeinformation.PerformLayout()
        Me.gbPeriodWorkDetail.ResumeLayout(False)
        Me.gbPeriodWorkDetail.PerformLayout()
        Me.PnlPayList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.tabPayslip.ResumeLayout(False)
        Me.tabpPayslip.ResumeLayout(False)
        Me.tabpPayslip.PerformLayout()
        Me.tabpEmployeeInfo.ResumeLayout(False)
        Me.gbEmployeeWorkDetail.ResumeLayout(False)
        Me.gbEmployeeWorkDetail.PerformLayout()
        Me.tabpPPA.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPayslipInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtVoucherNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPayyear As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeinformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSection As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents txtType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblEmployeeImage As System.Windows.Forms.Label
    Friend WithEvents gbPeriodWorkDetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDaysinPeriod As System.Windows.Forms.Label
    Friend WithEvents lblHolidays As System.Windows.Forms.Label
    Friend WithEvents esLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblPayableDays As System.Windows.Forms.Label
    Friend WithEvents txtDaysInPeriod As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPayableHours As System.Windows.Forms.Label
    Friend WithEvents txtPayableDay As eZee.TextBox.NumericTextBox
    Friend WithEvents txtHolidayInMonth As eZee.TextBox.NumericTextBox
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents PnlPayList As System.Windows.Forms.Panel
    Friend WithEvents tabPayslip As System.Windows.Forms.TabControl
    Friend WithEvents tabpPayslip As System.Windows.Forms.TabPage
    Friend WithEvents lblTotaladdition As System.Windows.Forms.Label
    Friend WithEvents lblTotaldeduction As System.Windows.Forms.Label
    Friend WithEvents lblNetsalary As System.Windows.Forms.Label
    Friend WithEvents tabpEmployeeInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbEmployeeWorkDetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents esLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblWorkedHr As System.Windows.Forms.Label
    Friend WithEvents txtWorkedDays As eZee.TextBox.NumericTextBox
    Friend WithEvents lblShortHour As System.Windows.Forms.Label
    Friend WithEvents lblWorkedDays As System.Windows.Forms.Label
    Friend WithEvents txtAbsent As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAbsent As System.Windows.Forms.Label
    Friend WithEvents lblExtraHours As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnPrint As eZee.Common.eZeeLightButton
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents lvPayslip As eZee.Common.eZeeListView
    Friend WithEvents colhSrNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAddition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDeduction As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtPayYear As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtScale As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPayPeriod As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtGradeGroup As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents txtGradeLevel As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtGrade As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents txtDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUnit As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtShortHours As System.Windows.Forms.TextBox
    Friend WithEvents txtExtraHours As System.Windows.Forms.TextBox
    Friend WithEvents txtWorkedHours As System.Windows.Forms.TextBox
    Friend WithEvents txtPayableHr As System.Windows.Forms.TextBox
    Friend WithEvents lblDaysOnHold As System.Windows.Forms.Label
    Friend WithEvents txtDaysOnHold As eZee.TextBox.NumericTextBox
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInformational As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtShift As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotaldeduction As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTotaladdition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtNetsalary As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOpenBalance As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOpenBalance As System.Windows.Forms.Label
    Friend WithEvents txtTranHeadFormula As System.Windows.Forms.TextBox
    Friend WithEvents txtNetSalaryWithOpenBal As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNetSalaryWithOpenBal As System.Windows.Forms.Label
    Friend WithEvents tabpPPA As System.Windows.Forms.TabPage
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lvPPA As eZee.Common.eZeeListView
    Friend WithEvents colhPPA As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAEmp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPADate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents cplhPPAUoM As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPARate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPANormalFactor As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAPHFactor As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPANormalHours As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAOTHours As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPANormalAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPAOTAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPPACCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvShift As eZee.Common.eZeeListView
    Friend WithEvents colhShift As System.Windows.Forms.ColumnHeader
End Class
