﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuthorizeUnauthorizePayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAuthorizeUnauthorizePayment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPaymenttranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.gbPaymentModeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objTotalpaidVal = New System.Windows.Forms.Label
        Me.objCashpaidVal = New System.Windows.Forms.Label
        Me.objBankpaidVal = New System.Windows.Forms.Label
        Me.lblTotalPaid = New System.Windows.Forms.Label
        Me.lblCashPaid = New System.Windows.Forms.Label
        Me.lblbankPaid = New System.Windows.Forms.Label
        Me.gbAmountInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.txtTotalAmount = New eZee.TextBox.NumericTextBox
        Me.lblTotalAmount = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbPaymentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPmtVoucher = New System.Windows.Forms.ComboBox
        Me.lblPmtVoucher = New System.Windows.Forms.Label
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkPayrollVarianceReport = New System.Windows.Forms.LinkLabel
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkPayrollTotalVarianceReport = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPaymentModeInfo.SuspendLayout()
        Me.gbAmountInfo.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbPaymentInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.txtRemarks)
        Me.pnlMainInfo.Controls.Add(Me.lblRemarks)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentModeInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbAmountInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(731, 566)
        Me.pnlMainInfo.TabIndex = 1
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.Panel1)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(422, 381)
        Me.gbEmployeeList.TabIndex = 212
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 160
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.dgEmployee)
        Me.Panel1.Controls.Add(Me.txtSearchEmp)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(1, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(421, 353)
        Me.Panel1.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee, Me.dgcolhAmount, Me.objdgcolhAmount, Me.dgcolhCurrency, Me.objdgcolhPaymenttranunkid})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(418, 325)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhAmount.HeaderText = "Paid Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        '
        'objdgcolhAmount
        '
        Me.objdgcolhAmount.HeaderText = "objdgcolhAmount"
        Me.objdgcolhAmount.Name = "objdgcolhAmount"
        Me.objdgcolhAmount.ReadOnly = True
        Me.objdgcolhAmount.Visible = False
        '
        'dgcolhCurrency
        '
        Me.dgcolhCurrency.HeaderText = "Currency"
        Me.dgcolhCurrency.Name = "dgcolhCurrency"
        Me.dgcolhCurrency.ReadOnly = True
        Me.dgcolhCurrency.Width = 60
        '
        'objdgcolhPaymenttranunkid
        '
        Me.objdgcolhPaymenttranunkid.HeaderText = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.Name = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.ReadOnly = True
        Me.objdgcolhPaymenttranunkid.Visible = False
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(417, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.HideSelection = False
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtRemarks.Location = New System.Drawing.Point(442, 339)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(266, 74)
        Me.txtRemarks.TabIndex = 211
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(443, 320)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(149, 16)
        Me.lblRemarks.TabIndex = 210
        Me.lblRemarks.Text = "Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbPaymentModeInfo
        '
        Me.gbPaymentModeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentModeInfo.Checked = False
        Me.gbPaymentModeInfo.CollapseAllExceptThis = False
        Me.gbPaymentModeInfo.CollapsedHoverImage = Nothing
        Me.gbPaymentModeInfo.CollapsedNormalImage = Nothing
        Me.gbPaymentModeInfo.CollapsedPressedImage = Nothing
        Me.gbPaymentModeInfo.CollapseOnLoad = False
        Me.gbPaymentModeInfo.Controls.Add(Me.objelLine1)
        Me.gbPaymentModeInfo.Controls.Add(Me.objTotalpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.objCashpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.objBankpaidVal)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblTotalPaid)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblCashPaid)
        Me.gbPaymentModeInfo.Controls.Add(Me.lblbankPaid)
        Me.gbPaymentModeInfo.ExpandedHoverImage = Nothing
        Me.gbPaymentModeInfo.ExpandedNormalImage = Nothing
        Me.gbPaymentModeInfo.ExpandedPressedImage = Nothing
        Me.gbPaymentModeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentModeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentModeInfo.HeaderHeight = 25
        Me.gbPaymentModeInfo.HeaderMessage = ""
        Me.gbPaymentModeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentModeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentModeInfo.HeightOnCollapse = 0
        Me.gbPaymentModeInfo.LeftTextSpace = 0
        Me.gbPaymentModeInfo.Location = New System.Drawing.Point(440, 190)
        Me.gbPaymentModeInfo.Name = "gbPaymentModeInfo"
        Me.gbPaymentModeInfo.OpenHeight = 182
        Me.gbPaymentModeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentModeInfo.ShowBorder = True
        Me.gbPaymentModeInfo.ShowCheckBox = False
        Me.gbPaymentModeInfo.ShowCollapseButton = False
        Me.gbPaymentModeInfo.ShowDefaultBorderColor = True
        Me.gbPaymentModeInfo.ShowDownButton = False
        Me.gbPaymentModeInfo.ShowHeader = True
        Me.gbPaymentModeInfo.Size = New System.Drawing.Size(275, 113)
        Me.gbPaymentModeInfo.TabIndex = 206
        Me.gbPaymentModeInfo.Temp = 0
        Me.gbPaymentModeInfo.Text = "Payment Mode Summary"
        Me.gbPaymentModeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(3, 78)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(270, 6)
        Me.objelLine1.TabIndex = 269
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotalpaidVal
        '
        Me.objTotalpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTotalpaidVal.Location = New System.Drawing.Point(134, 88)
        Me.objTotalpaidVal.Name = "objTotalpaidVal"
        Me.objTotalpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objTotalpaidVal.TabIndex = 209
        Me.objTotalpaidVal.Text = "0"
        Me.objTotalpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objCashpaidVal
        '
        Me.objCashpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCashpaidVal.Location = New System.Drawing.Point(134, 57)
        Me.objCashpaidVal.Name = "objCashpaidVal"
        Me.objCashpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objCashpaidVal.TabIndex = 208
        Me.objCashpaidVal.Text = "0"
        Me.objCashpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objBankpaidVal
        '
        Me.objBankpaidVal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objBankpaidVal.Location = New System.Drawing.Point(134, 32)
        Me.objBankpaidVal.Name = "objBankpaidVal"
        Me.objBankpaidVal.Size = New System.Drawing.Size(64, 16)
        Me.objBankpaidVal.TabIndex = 207
        Me.objBankpaidVal.Text = "0"
        Me.objBankpaidVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPaid
        '
        Me.lblTotalPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaid.Location = New System.Drawing.Point(8, 88)
        Me.lblTotalPaid.Name = "lblTotalPaid"
        Me.lblTotalPaid.Size = New System.Drawing.Size(94, 16)
        Me.lblTotalPaid.TabIndex = 206
        Me.lblTotalPaid.Text = "Total Payment"
        Me.lblTotalPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCashPaid
        '
        Me.lblCashPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashPaid.Location = New System.Drawing.Point(8, 57)
        Me.lblCashPaid.Name = "lblCashPaid"
        Me.lblCashPaid.Size = New System.Drawing.Size(94, 16)
        Me.lblCashPaid.TabIndex = 204
        Me.lblCashPaid.Text = "Cash Payment"
        Me.lblCashPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblbankPaid
        '
        Me.lblbankPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblbankPaid.Location = New System.Drawing.Point(8, 32)
        Me.lblbankPaid.Name = "lblbankPaid"
        Me.lblbankPaid.Size = New System.Drawing.Size(94, 16)
        Me.lblbankPaid.TabIndex = 141
        Me.lblbankPaid.Text = "Bank Payment"
        Me.lblbankPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbAmountInfo
        '
        Me.gbAmountInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAmountInfo.Checked = False
        Me.gbAmountInfo.CollapseAllExceptThis = False
        Me.gbAmountInfo.CollapsedHoverImage = Nothing
        Me.gbAmountInfo.CollapsedNormalImage = Nothing
        Me.gbAmountInfo.CollapsedPressedImage = Nothing
        Me.gbAmountInfo.CollapseOnLoad = False
        Me.gbAmountInfo.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbAmountInfo.ExpandedHoverImage = Nothing
        Me.gbAmountInfo.ExpandedNormalImage = Nothing
        Me.gbAmountInfo.ExpandedPressedImage = Nothing
        Me.gbAmountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAmountInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAmountInfo.HeaderHeight = 25
        Me.gbAmountInfo.HeaderMessage = ""
        Me.gbAmountInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAmountInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAmountInfo.HeightOnCollapse = 0
        Me.gbAmountInfo.LeftTextSpace = 0
        Me.gbAmountInfo.Location = New System.Drawing.Point(12, 450)
        Me.gbAmountInfo.Name = "gbAmountInfo"
        Me.gbAmountInfo.OpenHeight = 300
        Me.gbAmountInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAmountInfo.ShowBorder = True
        Me.gbAmountInfo.ShowCheckBox = False
        Me.gbAmountInfo.ShowCollapseButton = False
        Me.gbAmountInfo.ShowDefaultBorderColor = True
        Me.gbAmountInfo.ShowDownButton = False
        Me.gbAmountInfo.ShowHeader = True
        Me.gbAmountInfo.Size = New System.Drawing.Size(422, 59)
        Me.gbAmountInfo.TabIndex = 3
        Me.gbAmountInfo.Temp = 0
        Me.gbAmountInfo.Text = "Total Amount"
        Me.gbAmountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.txtTotalAmount)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblTotalAmount)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(417, 31)
        Me.pnlAdvanceTaken.TabIndex = 0
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.AllowNegative = True
        Me.txtTotalAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalAmount.DigitsInGroup = 3
        Me.txtTotalAmount.Flags = 0
        Me.txtTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmount.Location = New System.Drawing.Point(187, 7)
        Me.txtTotalAmount.MaxDecimalPlaces = 6
        Me.txtTotalAmount.MaxWholeDigits = 21
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.Prefix = ""
        Me.txtTotalAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTotalAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTotalAmount.ReadOnly = True
        Me.txtTotalAmount.Size = New System.Drawing.Size(167, 21)
        Me.txtTotalAmount.TabIndex = 0
        Me.txtTotalAmount.Text = "0"
        Me.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalAmount
        '
        Me.lblTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalAmount.Location = New System.Drawing.Point(64, 9)
        Me.lblTotalAmount.Name = "lblTotalAmount"
        Me.lblTotalAmount.Size = New System.Drawing.Size(117, 16)
        Me.lblTotalAmount.TabIndex = 0
        Me.lblTotalAmount.Text = "Total Amount"
        Me.lblTotalAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(731, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Authorize Payment"
        '
        'gbPaymentInfo
        '
        Me.gbPaymentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.Checked = False
        Me.gbPaymentInfo.CollapseAllExceptThis = False
        Me.gbPaymentInfo.CollapsedHoverImage = Nothing
        Me.gbPaymentInfo.CollapsedNormalImage = Nothing
        Me.gbPaymentInfo.CollapsedPressedImage = Nothing
        Me.gbPaymentInfo.CollapseOnLoad = False
        Me.gbPaymentInfo.Controls.Add(Me.cboPmtVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.lblPmtVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbPaymentInfo.Controls.Add(Me.objbtnReset)
        Me.gbPaymentInfo.Controls.Add(Me.objbtnSearch)
        Me.gbPaymentInfo.Controls.Add(Me.cboCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.lblCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbPaymentInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbPaymentInfo.ExpandedHoverImage = Nothing
        Me.gbPaymentInfo.ExpandedNormalImage = Nothing
        Me.gbPaymentInfo.ExpandedPressedImage = Nothing
        Me.gbPaymentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentInfo.HeaderHeight = 25
        Me.gbPaymentInfo.HeaderMessage = ""
        Me.gbPaymentInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.HeightOnCollapse = 0
        Me.gbPaymentInfo.LeftTextSpace = 0
        Me.gbPaymentInfo.Location = New System.Drawing.Point(440, 66)
        Me.gbPaymentInfo.Name = "gbPaymentInfo"
        Me.gbPaymentInfo.OpenHeight = 182
        Me.gbPaymentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentInfo.ShowBorder = True
        Me.gbPaymentInfo.ShowCheckBox = False
        Me.gbPaymentInfo.ShowCollapseButton = False
        Me.gbPaymentInfo.ShowDefaultBorderColor = True
        Me.gbPaymentInfo.ShowDownButton = False
        Me.gbPaymentInfo.ShowHeader = True
        Me.gbPaymentInfo.Size = New System.Drawing.Size(275, 118)
        Me.gbPaymentInfo.TabIndex = 0
        Me.gbPaymentInfo.Temp = 0
        Me.gbPaymentInfo.Text = "Payment Info"
        Me.gbPaymentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPmtVoucher
        '
        Me.cboPmtVoucher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPmtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPmtVoucher.FormattingEnabled = True
        Me.cboPmtVoucher.Location = New System.Drawing.Point(102, 88)
        Me.cboPmtVoucher.Name = "cboPmtVoucher"
        Me.cboPmtVoucher.Size = New System.Drawing.Size(155, 21)
        Me.cboPmtVoucher.TabIndex = 316
        '
        'lblPmtVoucher
        '
        Me.lblPmtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPmtVoucher.Location = New System.Drawing.Point(8, 90)
        Me.lblPmtVoucher.Name = "lblPmtVoucher"
        Me.lblPmtVoucher.Size = New System.Drawing.Size(88, 16)
        Me.lblPmtVoucher.TabIndex = 317
        Me.lblPmtVoucher.Text = "Voucher #"
        Me.lblPmtVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(128, 4)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(92, 13)
        Me.lnkAdvanceFilter.TabIndex = 315
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(248, -2)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 314
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(225, -2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 313
        Me.objbtnSearch.TabStop = False
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(102, 61)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(155, 21)
        Me.cboCurrency.TabIndex = 5
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 63)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(88, 16)
        Me.lblCurrency.TabIndex = 204
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(102, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(155, 21)
        Me.cboPayPeriod.TabIndex = 1
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(88, 16)
        Me.lblPayPeriod.TabIndex = 141
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkPayrollTotalVarianceReport)
        Me.objFooter.Controls.Add(Me.lnkPayrollVarianceReport)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 511)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(731, 55)
        Me.objFooter.TabIndex = 2
        '
        'lnkPayrollVarianceReport
        '
        Me.lnkPayrollVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollVarianceReport.Location = New System.Drawing.Point(10, 33)
        Me.lnkPayrollVarianceReport.Name = "lnkPayrollVarianceReport"
        Me.lnkPayrollVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollVarianceReport.TabIndex = 316
        Me.lnkPayrollVarianceReport.TabStop = True
        Me.lnkPayrollVarianceReport.Text = "Show Payroll Variance Report"
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(11, 13)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 14
        Me.lblSearchEmp.Text = "Search Employee"
        Me.lblSearchEmp.Visible = False
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(451, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(165, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Authorize Payment"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(622, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkPayrollTotalVarianceReport
        '
        Me.lnkPayrollTotalVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollTotalVarianceReport.Location = New System.Drawing.Point(234, 33)
        Me.lnkPayrollTotalVarianceReport.Name = "lnkPayrollTotalVarianceReport"
        Me.lnkPayrollTotalVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollTotalVarianceReport.TabIndex = 317
        Me.lnkPayrollTotalVarianceReport.TabStop = True
        Me.lnkPayrollTotalVarianceReport.Text = "Show Payroll Total Variance Report"
        '
        'frmAuthorizeUnauthorizePayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 566)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAuthorizeUnauthorizePayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Authorize Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPaymentModeInfo.ResumeLayout(False)
        Me.gbAmountInfo.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.pnlAdvanceTaken.PerformLayout()
        Me.gbPaymentInfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbAmountInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents txtTotalAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalAmount As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbPaymentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPaymentModeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCashPaid As System.Windows.Forms.Label
    Friend WithEvents lblbankPaid As System.Windows.Forms.Label
    Friend WithEvents lblTotalPaid As System.Windows.Forms.Label
    Friend WithEvents objTotalpaidVal As System.Windows.Forms.Label
    Friend WithEvents objCashpaidVal As System.Windows.Forms.Label
    Friend WithEvents objBankpaidVal As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPmtVoucher As System.Windows.Forms.ComboBox
    Friend WithEvents lblPmtVoucher As System.Windows.Forms.Label
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPaymenttranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkPayrollVarianceReport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPayrollTotalVarianceReport As System.Windows.Forms.LinkLabel
End Class
