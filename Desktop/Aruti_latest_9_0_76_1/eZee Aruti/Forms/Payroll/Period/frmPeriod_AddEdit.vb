﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmPeriod_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmPeriod_AddEdit"
    Private mblnCancel As Boolean = True
    Private objPeriodMaster As clscommom_period_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPeriodMasterUnkid As Integer = -1
    Private mintModuleRefid As Integer = -1
    Private mblnisAssessment As Boolean = False
    'S.SANDEEP [05 SEP 2016] -- START
    'ENHANCEMENT : INCLUDE ALL YEAR {BY ANDREW}
    Private mdtfinStartDate As Date = Nothing
    Private mdtfinEndDate As Date = Nothing
    'S.SANDEEP [05 SEP 2016] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intModuleRefid As Integer, Optional ByVal isAssessment As Boolean = False) As Boolean
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If isAssessment = True Then
                Me.Name = "frmAssessment_Period_AddEdit"
            Else
                Me.Name = "frmPayroll_Period_AddEdit"
            End If
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            mintPeriodMasterUnkid = intUnkId
            mintModuleRefid = intModuleRefid
            mblnisAssessment = isAssessment

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If isAssessment Then
            '    Me.Size = CType(New Point(446, 392), Drawing.Size)
            '    gbPeriod.Size = CType(New Point(417, 232), Drawing.Size)
            '    lblStatus.Visible = True
            '    cboStatus.Visible = True
            'End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP |08-JAN-2019| -- START
            objpnlAssessmentDays.Visible = False
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP [01 DEC 2015] -- START
            If mblnisAssessment Then
                lblPrescribedIntRate.Visible = False
                txtPrescribedIntRate.Visible = False
                'S.SANDEEP |08-JAN-2019| -- START
                objpnlAssessmentDays.Visible = True
                Me.Size = CType(New Point(518, 470), Drawing.Size)
                gbPeriod.Size = CType(New Point(488, 316), Drawing.Size)

                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                    chkIsCmptPeriod.Visible = True
                Else
                    chkIsCmptPeriod.Visible = False
                End If
                'S.SANDEEP |09-FEB-2021| -- END
            Else
                Me.Size = CType(New Point(518, 411), Drawing.Size)
                gbPeriod.Size = CType(New Point(488, 258), Drawing.Size)
                'S.SANDEEP |08-JAN-2019| -- END
            End If
            'S.SANDEEP [01 DEC 2015] -- END


            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintPeriodMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmPeriod_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPeriodMaster = New clscommom_period_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            setColor()

            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.

            'Nilay (03-Oct-2016) -- Start
            'Enhancement : 63.1 - New Payslip Template 16 for INDEPENDENT BROADCASTING AUTHORITY.
            'If ConfigParameter._Object._AccountingSoftWare = enIntegration.Sun_Account Then
            '    lblCostomCode.Visible = True
            '    txtCustomCode.Visible = True
            'End If
            'Nilay (03-Oct-2016) -- End

            'Nilay (16-Apr-2016) -- End

            'Pinkal (25-Oct-2010) -- Start  According To Mr.Rutta's Comment

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriodMaster._Periodunkid = mintPeriodMasterUnkid
                objPeriodMaster._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodMasterUnkid
                'Sohail (21 Aug 2015) -- End
                'Sohail (24 Mar 2018) -- Start
                'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                If objPeriodMaster._Statusid = CInt(enStatusType.Close) Then
                    dtpStartdate.Enabled = False
                    dtpEnddate.Enabled = False
                    dtpTnAEnddate.Enabled = False
                End If
                'Sohail (24 Mar 2018) -- End
                If mblnisAssessment Then cboStatus.Enabled = True
            Else
                If mblnisAssessment Then cboStatus.Enabled = False
            End If

            'Pinkal (25-Oct-2010) -- End  According To Mr.Rutta's Comment

            FillCombo()
            GetValue()
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            'S.SANDEEP [01 DEC 2015] -- START
            'If ConfigParameter._Object._IsSeparateTnAPeriod = True Then
            '    lblTnAEnddate.Visible = True
            '    dtpTnAEnddate.Visible = True
            'Else
            '    lblTnAEnddate.Visible = False
            '    dtpTnAEnddate.Visible = False
            'End If

            If mblnisAssessment = False Then
                If ConfigParameter._Object._IsSeparateTnAPeriod = True Then
                    lblTnAEnddate.Visible = True
                    dtpTnAEnddate.Visible = True
                Else
                    lblTnAEnddate.Visible = False
                    dtpTnAEnddate.Visible = False
                End If
            Else
                lblTnAEnddate.Visible = False
                dtpTnAEnddate.Visible = False
            End If
            'S.SANDEEP [01 DEC 2015] -- END
            'Sohail (07 Jan 2014) -- End

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            If ConfigParameter._Object._AccountingSoftWare = CInt(enIntegration.FLEX_CUBE_JV) Then
                lblFlexCubeNMBJV_BatchCode.Visible = True
                txtFlexCubeNMBJV_BatchCode.Visible = True
            Else
                lblFlexCubeNMBJV_BatchCode.Visible = False
                txtFlexCubeNMBJV_BatchCode.Visible = False
            End If
            'Sohail (13 Dec 2018) -- End

            cboPayyear.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriod_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeriod_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriod_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeriod_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPeriod_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPeriod_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPeriodMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscommom_period_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clscommom_period_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 



#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboPayyear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboPayyear.Select()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period Code cannot be blank. Period Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period Name cannot be blank. Period Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            ElseIf dtpEnddate.Value.Date < dtpStartdate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period End Date must be Greater than Start Date.Please Select the Correct End Date."), enMsgBoxStyle.Information)
                dtpEnddate.Select()
                Exit Sub
                'Sohail (26 Nov 2010) -- Start

                'S.SANDEEP [05 SEP 2016] -- START
                'ENHANCEMENT : INCLUDE ALL YEAR {BY ANDREW}
                'ElseIf dtpStartdate.Value.Date < FinancialYear._Object._Database_Start_Date OrElse dtpEnddate.Value.Date > FinancialYear._Object._Database_End_Date Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period Start Date and End Date should be in between ") & FinancialYear._Object._Database_Start_Date.ToShortDateString & Language.getMessage(mstrModuleName, 6, " and ") & FinancialYear._Object._Database_End_Date.ToShortDateString & "", enMsgBoxStyle.Information)
                '    dtpEnddate.Select()
                '    Exit Sub
            ElseIf mblnisAssessment = True Then
                If dtpStartdate.Value.Date < mdtfinStartDate.Date OrElse dtpEnddate.Value.Date > mdtfinEndDate.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period Start Date and End Date should be between ") & mdtfinStartDate.ToShortDateString & Language.getMessage(mstrModuleName, 6, " and ") & mdtfinEndDate.ToShortDateString & "", enMsgBoxStyle.Information)
                    dtpEnddate.Select()
                    Exit Sub
                End If
                'S.SANDEEP |09-FEB-2021| -- START
                'ISSUE/ENHANCEMENT : Competencies Period Changes
                If menAction = enAction.EDIT_ONE Then
                    Dim objEvMst As New clsevaluation_analysis_master
                    Dim strMsg As String = ""
                    If chkIsCmptPeriod.Checked Then                        
                        strMsg = objEvMst.IsValidPeriodSelected(mintPeriodMasterUnkid, clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE)
                    ElseIf chkIsCmptPeriod.Checked = False Then
                        strMsg = objEvMst.IsValidPeriodSelected(mintPeriodMasterUnkid, clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD)
                    End If
                    If strMsg Is Nothing Then strMsg = ""                    
                    If strMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                        objEvMst = Nothing
                        Exit Sub
                    End If
                    objEvMst = Nothing
                End If                
                'S.SANDEEP |09-FEB-2021| -- END
            ElseIf mblnisAssessment = False Then
                If dtpStartdate.Value.Date < FinancialYear._Object._Database_Start_Date OrElse dtpEnddate.Value.Date > FinancialYear._Object._Database_End_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period Start Date and End Date should be between ") & FinancialYear._Object._Database_Start_Date.ToShortDateString & Language.getMessage(mstrModuleName, 6, " and ") & FinancialYear._Object._Database_End_Date.ToShortDateString & "", enMsgBoxStyle.Information)
                    dtpEnddate.Select()
                    Exit Sub
                End If
                'S.SANDEEP [05 SEP 2016] -- END

                'Sohail (26 Nov 2010) -- End
                'Sohail (10 Jul 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf txtConstantDays.Decimal <= 0 OrElse (txtConstantDays.Decimal > CInt(DateDiff(DateInterval.Day, dtpStartdate.Value.Date, dtpEnddate.Value.Date.AddDays(4)))) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Constant days should be between Total Days in Period."), enMsgBoxStyle.Information)
                txtConstantDays.Select()
                Exit Sub
                'Sohail (30 Oct 2015) -- Start
                'Enhancement - New Statutory Report I-TAX Form J Report and option for Prescribed Quarterly Interest Rate on Period Master for Kenya.
            ElseIf txtPrescribedIntRate.Decimal < 0 OrElse txtPrescribedIntRate.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Quarterly Prescribed Interest Rate should be between 0 and 100."), enMsgBoxStyle.Information)
                txtPrescribedIntRate.Select()
                Exit Sub
                'Sohail (30 Oct 2015) -- End
                'Sohail (07 Jan 2014) -- Start
                'Enhancement - Separate TnA Periods from Payroll Periods
            ElseIf ConfigParameter._Object._IsSeparateTnAPeriod = True AndAlso (dtpTnAEnddate.Value.Date > dtpEnddate.Value.Date OrElse dtpTnAEnddate.Value.Date < dtpStartdate.Value.Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, TnA End date should be in between Period Start Date and End Date."), enMsgBoxStyle.Information)
                dtpTnAEnddate.Select()
                Exit Sub
                'Sohail (07 Jan 2014) -- End
            ElseIf menAction = enAction.EDIT_ONE AndAlso mintModuleRefid = enModuleReference.Payroll AndAlso (CInt(txtConstantDays.Decimal) <> objPeriodMaster._Constant_Days OrElse dtpStartdate.Value.Date <> objPeriodMaster._Start_Date.Date OrElse dtpEnddate.Value.Date <> objPeriodMaster._End_Date.Date) Then
                Dim objTnA As New clsTnALeaveTran
                Dim objEmp As New clsEmployee_Master
                Dim dsList As DataSet
                Dim strEmpIDs As String = ""
                'Anjan [10 June 2015] -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEmp.GetEmployeeList("Employees", False, True, , , , , , , , , , , , objPeriodMaster._Start_Date, objPeriodMaster._End_Date, , , , , , True)

                dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               objPeriodMaster._Start_Date, _
                                               objPeriodMaster._End_Date, _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, True, "Employees", False)
                'Anjan [10 June 2015] -- End

                Dim arr() As String = (From p In dsList.Tables("Employees").AsEnumerable() Select (p.Item("employeeunkid").ToString)).ToArray
                strEmpIDs = String.Join(",", arr)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTnA.IsPayrollProcessDone(objPeriodMaster._Periodunkid, strEmpIDs, objPeriodMaster._End_Date) = True Then
                If objTnA.IsPayrollProcessDone(objPeriodMaster._Periodunkid(FinancialYear._Object._DatabaseName), strEmpIDs, objPeriodMaster._End_Date) = True Then
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Payroll Process is already Done for last date of this period. You can not change Start Date, End Date or Constant Days."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Payroll Process is already Done for last date of this period. You can not change Start Date, End Date or Constant Days.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 11, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    txtConstantDays.Select()
                    Exit Sub
                End If
                'Sohail (10 Jul 2013) -- End
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPeriodMaster.Update()
            Else
                blnFlag = objPeriodMaster.Insert()
            End If

            If blnFlag = False And objPeriodMaster._Message <> "" Then
                eZeeMsgBox.Show(objPeriodMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPeriodMaster = Nothing
                    objPeriodMaster = New clscommom_period_Tran
                    Call GetValue()
                    txtCode.Select()
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'mintPeriodMasterUnkid = objPeriodMaster._Periodunkid
                    mintPeriodMasterUnkid = objPeriodMaster._Periodunkid(FinancialYear._Object._DatabaseName)
                    'Sohail (21 Aug 2015) -- End
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboPayyear.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorComp
            txtConstantDays.BackColor = GUI.ColorComp 'Sohail (10 Jul 2013)
            'S.SANDEEP |08-JAN-2019| -- START
            nudDaysAfter.BackColor = GUI.ColorOptional
            nudDaysBefore.BackColor = GUI.ColorOptional
            'S.SANDEEP |08-JAN-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboPayyear.SelectedValue = objPeriodMaster._Yearunkid
            txtCode.Text = objPeriodMaster._Period_Code
            txtName.Text = objPeriodMaster._Period_Name
            txtDescription.Text = objPeriodMaster._Descrription
            txtPrescribedIntRate.Decimal = objPeriodMaster._Prescribed_interest_rate 'Sohail (30 Oct 2015)
            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            txtCustomCode.Text = objPeriodMaster._Sunjv_PeriodCode
            'Nilay (16-Apr-2016) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objPeriodMaster._Periodunkid > 0 Then
            If objPeriodMaster._Periodunkid(FinancialYear._Object._DatabaseName) > 0 Then
                'Sohail (21 Aug 2015) -- End
                dtpStartdate.Value = objPeriodMaster._Start_Date
                dtpEnddate.Value = objPeriodMaster._End_Date
                dtpTnAEnddate.Value = objPeriodMaster._TnA_EndDate 'Sohail (07 Jan 2014)
                'Sohail (26 Nov 2010) -- Start
                If menAction = enAction.EDIT_ONE Then
                    Dim objPayment As New clsPayment_tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'Dim ds As DataSet = objPayment.GetListByPeriod("Employee", clsPayment_tran.enPaymentRefId.PAYSLIP, objPeriodMaster._Periodunkid, clsPayment_tran.enPayTypeId.PAYMENT)
                    Dim ds As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriodMaster._Start_Date, objPeriodMaster._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", clsPayment_tran.enPaymentRefId.PAYSLIP, objPeriodMaster._Periodunkid(FinancialYear._Object._DatabaseName), clsPayment_tran.enPayTypeId.PAYMENT, , True)
                    'Sohail (21 Aug 2015) -- End
                    If ds.Tables("Employee").Rows.Count > 0 Then
                        dtpStartdate.MinDate = objPeriodMaster._Start_Date
                        dtpStartdate.MaxDate = objPeriodMaster._Start_Date
                        dtpEnddate.MinDate = objPeriodMaster._End_Date
                        dtpEnddate.MaxDate = objPeriodMaster._End_Date
                        'Sohail (07 Jan 2014) -- Start
                        'Enhancement - Separate TnA Periods from Payroll Periods
                        dtpTnAEnddate.MinDate = objPeriodMaster._TnA_EndDate
                        dtpTnAEnddate.MaxDate = objPeriodMaster._TnA_EndDate
                        'Sohail (07 Jan 2014) -- End
                    End If
                    ds = Nothing
                    objPayment = Nothing
                End If
                'Sohail (26 Nov 2010) -- End
            Else
                dtpStartdate.Value = Now.Date
                dtpEnddate.Value = Now.Date
                dtpTnAEnddate.Value = Now.Date 'Sohail (07 Jan 2014)
            End If

            txtConstantDays.Decimal = objPeriodMaster._Constant_Days 'Sohail (10 Jul 2013)

            'Anjan (16 Aug 2010)-Start
            'Issue : From here we cannot give status as anyone can close period while editing
            'If CInt(objPeriodMaster._Statusid) = CInt(enStatusType.Open) Then
            '    cboStatus.SelectedIndex = CInt(enStatusType.Open) - 1
            'ElseIf CInt(objPeriodMaster._Statusid) = CInt(enStatusType.Close) Then
            '    cboStatus.SelectedIndex = CInt(enStatusType.Close) - 1
            'End If
            'Anjan (16 Aug 2010)-End


            ' Pinkal (14 Sep 2010)------------------ Start

            If mblnisAssessment Then
                If CInt(objPeriodMaster._Statusid) = CInt(enStatusType.Open) Then
                    cboStatus.SelectedIndex = CInt(enStatusType.Open) - 1
                ElseIf CInt(objPeriodMaster._Statusid) = CInt(enStatusType.Close) Then
                    cboStatus.SelectedIndex = CInt(enStatusType.Close) - 1
                End If
            End If

            ' Pinkal (14 Sep 2010)------------------- End

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            txtFlexCubeNMBJV_BatchCode.Text = objPeriodMaster._FlexCube_BatchNo
            'Sohail (13 Dec 2018) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            nudDaysAfter.Value = CDec(IIf(objPeriodMaster._LockDaysAfter < 0, 0, objPeriodMaster._LockDaysAfter))
            nudDaysBefore.Value = CDec(IIf(objPeriodMaster._LockDaysBefore < 0, 0, objPeriodMaster._LockDaysBefore))
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            chkIsCmptPeriod.Checked = objPeriodMaster._Iscmptperiod
            'S.SANDEEP |09-FEB-2021| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPeriodMaster._Yearunkid = CInt(cboPayyear.SelectedValue)
            objPeriodMaster._Period_Code = txtCode.Text.Trim
            objPeriodMaster._Period_Name = txtName.Text.Trim
            objPeriodMaster._Descrription = txtDescription.Text.Trim
            objPeriodMaster._Start_Date = dtpStartdate.Value.Date
            objPeriodMaster._End_Date = dtpEnddate.Value.Date
            'Sohail (19 Nov 2010) -- Start
            objPeriodMaster._Userunkid = User._Object._Userunkid
            'Sohail (19 Nov 2010) -- End
            objPeriodMaster._Constant_Days = CInt(txtConstantDays.Decimal) 'Sohail (10 Jul 2013)
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If ConfigParameter._Object._IsSeparateTnAPeriod = True Then
                objPeriodMaster._TnA_EndDate = dtpTnAEnddate.Value.Date
            Else
                objPeriodMaster._TnA_EndDate = dtpEnddate.Value.Date
            End If
            'Sohail (07 Jan 2014) -- End

            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            If txtCustomCode.Text.Trim.Length <= 0 Then
                objPeriodMaster._Sunjv_PeriodCode = txtCode.Text.Trim
            Else
                objPeriodMaster._Sunjv_PeriodCode = txtCustomCode.Text.Trim
            End If
            'Nilay (16-Apr-2016) -- End

            'Anjan (16 Aug 2010)-Start
            'Issue : From here we cannot give status as anyone can close period while editing
            'If CInt(cboStatus.SelectedIndex + 1) = CInt(enStatusType.Open) Then
            '    objPeriodMaster._Statusid = CInt(enStatusType.Open)
            'ElseIf CInt(cboStatus.SelectedIndex + 1) = CInt(enStatusType.Close) Then
            '    objPeriodMaster._Statusid = CInt(enStatusType.Close)
            'End If
            'objPeriodMaster._Statusid = 1
            'Anjan (16 Aug 2010)-End


            ' Pinkal (14 Sep 2010)------------------ Start

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If mblnisAssessment Then
            '    If CInt(cboStatus.SelectedIndex + 1) = CInt(enStatusType.Open) Then
            '        objPeriodMaster._Statusid = CInt(enStatusType.Open)
            '    ElseIf CInt(cboStatus.SelectedIndex + 1) = CInt(enStatusType.Close) Then
            '        objPeriodMaster._Statusid = CInt(enStatusType.Close)
            '    End If
            'Else
            '    'S.SANDEEP [ 04 JULY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'objPeriodMaster._Statusid = 1
            '    If menAction <> enAction.EDIT_ONE Then
            'objPeriodMaster._Statusid = 1
            '    Else
            '        objPeriodMaster._Statusid = objPeriodMaster._Statusid
            '    End If
            '    'S.SANDEEP [ 04 JULY 2012 ] -- END
            'End If
            If menAction <> enAction.EDIT_ONE Then
                objPeriodMaster._Statusid = 1
            Else
                objPeriodMaster._Statusid = objPeriodMaster._Statusid
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            'S.SANDEEP [02 SEP 2015] -- START
            objPeriodMaster._IsAssessmentPeriod = mblnisAssessment
            'S.SANDEEP [02 SEP 2015] -- END

            ' Pinkal (14 Sep 2010)------------------- End

            'Sohail (30 Oct 2015) -- Start
            'Enhancement - New Statutory Report I-TAX Form J Report and option for Prescribed Quarterly Interest Rate on Period Master for Kenya.
            objPeriodMaster._Prescribed_interest_rate = txtPrescribedIntRate.Decimal
            'Sohail (30 Oct 2015) -- End

            objPeriodMaster._Total_Days = CInt(DateDiff(DateInterval.Day, dtpStartdate.Value, dtpEnddate.Value.AddDays(1)))

            'S.SANDEEP |08-JAN-2019| -- START
            objPeriodMaster._LockDaysAfter = CInt(IIf(CInt(nudDaysAfter.Value) < 0, 0, CInt(nudDaysAfter.Value)))
            objPeriodMaster._LockDaysBefore = CInt(IIf(CInt(nudDaysBefore.Value) < 0, 0, CInt(nudDaysBefore.Value)))
            'S.SANDEEP |08-JAN-2019| -- END


            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            objPeriodMaster._Iscmptperiod = chkIsCmptPeriod.Checked
            'S.SANDEEP |09-FEB-2021| -- END

            objPeriodMaster._Modulerefid = mintModuleRefid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()

        'Sandeep [ 21 Aug 2010 ] -- Start
        Dim objMasterData As New clsMasterData
        'Sandeep [ 21 Aug 2010 ] -- End 

        Try

            'For Status

            cboStatus.Items.Clear()
            cboStatus.Items.Add("Open")
            cboStatus.Items.Add("Close")
            cboStatus.SelectedIndex = 0

            'For Financial Year

            'Sandeep [ 21 Aug 2010 ] -- Start
            'Dim dsYear As DataSet = objPeriodMaster.getListForYearCombo("Year", True)
            'cboPayyear.ValueMember = "yearunkid"
            'cboPayyear.DisplayMember = "name"

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsYear As DataSet = objMasterData.getComboListPAYYEAR("Year", True)
            Dim dsYear As DataSet = objMasterData.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, mblnisAssessment)
            'S.SANDEEP [04 JUN 2015] -- END

            cboPayyear.ValueMember = "Id"
            cboPayyear.DisplayMember = "name"
            'Sandeep [ 21 Aug 2010 ] -- End 
            cboPayyear.DataSource = dsYear.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

    'Nilay (16-Apr-2016) -- Start
    'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
#Region "TextBox Events"
    Private Sub txtCode_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCode.Validated
        Try
            If txtCode.Text.Trim.Length > 0 Then
                txtCustomCode.Text = txtCode.Text.Trim
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtCode_Validated", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |08-JAN-2019| -- START
    Private Sub nudDaysAfter_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDaysAfter.ValueChanged
        Try
            txtDate1.Text = dtpStartdate.Value.Date.AddDays(nudDaysAfter.Value).ToShortDateString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDaysAfter_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub nudDaysBefore_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDaysBefore.ValueChanged
        Try
            txtDate2.Text = dtpEnddate.Value.Date.AddDays(-(nudDaysBefore.Value)).ToShortDateString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDaysBefore_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |08-JAN-2019| -- END

#End Region
    'Nilay (16-Apr-2016) -- End

    'Sohail (10 Jul 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Other Control's Events "
    Private Sub dtpStartdate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStartdate.ValueChanged, dtpEnddate.ValueChanged
        Try
            txtConstantDays.Decimal = CDec(DateDiff(DateInterval.Day, dtpStartdate.Value.Date, dtpEnddate.Value.Date.AddDays(1)))
            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If menAction <> enAction.EDIT_ONE Then
                dtpTnAEnddate.Value = dtpEnddate.Value
            End If
            'Sohail (07 Jan 2014) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            nudDaysAfter.Maximum = txtConstantDays.Decimal
            nudDaysBefore.Maximum = txtConstantDays.Decimal
            'S.SANDEEP |08-JAN-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartdate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [05 SEP 2016] -- START
    'ENHANCEMENT : INCLUDE ALL YEAR {BY ANDREW}
    Private Sub cboPayyear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPayyear.SelectedIndexChanged
        Try
            If mblnisAssessment Then
                If CInt(cboPayyear.SelectedValue) > 0 Then
                    Dim dsYear As New DataSet
                    Dim objFinYear As New clsCompany_Master
                    dsYear = objFinYear.GetFinancialYearList(Company._Object._Companyunkid, -1, "List", CInt(cboPayyear.SelectedValue))
                    If dsYear.Tables(0).Rows.Count > 0 Then
                        mdtfinStartDate = eZeeDate.convertDate(dsYear.Tables(0).Rows(0).Item("start_date").ToString).Date
                        mdtfinEndDate = eZeeDate.convertDate(dsYear.Tables(0).Rows(0).Item("end_date").ToString).Date
                    End If
                Else
                    mdtfinStartDate = FinancialYear._Object._Database_Start_Date
                    mdtfinEndDate = FinancialYear._Object._Database_End_Date
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayyear_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [05 SEP 2016] -- END


#End Region
    'Sohail (10 Jul 2013) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPeriod.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPeriod.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.gbPeriod.Text = Language._Object.getCaption(Me.gbPeriod.Name, Me.gbPeriod.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Language._Object.getCaption(Me.lblStartdate.Name, Me.lblStartdate.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.lblConstantDays.Text = Language._Object.getCaption(Me.lblConstantDays.Name, Me.lblConstantDays.Text)
            Me.lblTnAEnddate.Text = Language._Object.getCaption(Me.lblTnAEnddate.Name, Me.lblTnAEnddate.Text)
            Me.lblPrescribedIntRate.Text = Language._Object.getCaption(Me.lblPrescribedIntRate.Name, Me.lblPrescribedIntRate.Text)
            Me.lblCostomCode.Text = Language._Object.getCaption(Me.lblCostomCode.Name, Me.lblCostomCode.Text)
	    Me.lblFlexCubeNMBJV_BatchCode.Text = Language._Object.getCaption(Me.lblFlexCubeNMBJV_BatchCode.Name, Me.lblFlexCubeNMBJV_BatchCode.Text)
	    Me.lblDaysBefore.Text = Language._Object.getCaption(Me.lblDaysBefore.Name, Me.lblDaysBefore.Text)
  	    Me.lblDaysAfter.Text = Language._Object.getCaption(Me.lblDaysAfter.Name, Me.lblDaysAfter.Text)
		

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Year is compulsory information.Please Select Year.")
            Language.setMessage(mstrModuleName, 2, "Period Code cannot be blank. Period Code is required information.")
            Language.setMessage(mstrModuleName, 3, "Period Name cannot be blank. Period Name is required information.")
            Language.setMessage(mstrModuleName, 4, "Period End Date must be Greater than Start Date.Please Select the Correct End Date.")
            Language.setMessage(mstrModuleName, 5, "Period Start Date and End Date should be between")
            Language.setMessage(mstrModuleName, 6, " and")
            Language.setMessage(mstrModuleName, 7, "Constant days should be between Total Days in Period.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Payroll Process is already Done for last date of this period. You can not change Start Date, End Date or Constant Days.")
            Language.setMessage(mstrModuleName, 9, "Sorry, TnA End date should be in between Period Start Date and End Date.")
            Language.setMessage(mstrModuleName, 10, "Quarterly Prescribed Interest Rate should be between 0 and 100.")
			Language.setMessage(mstrModuleName, 11, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class