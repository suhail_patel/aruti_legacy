﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmApproveDisapprovePaymentList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapprovePaymentList"
    Private objPaymentApproval As clsPayment_approval_tran

    'Private mintTotalEmployeeCount As Integer = 0
    Private mintTotalPaidEmployee As Integer = 0
    Private mintCurrLevelPriority As Integer = -1

    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorOptional 'Sohail (18 Aug 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objApproverMap As New clspayment_approver_mapping
        Dim objPeriod As New clscommom_period_Tran
        'Dim objEmp As New clsEmployee_Master
        'Dim objPayment As New clsPayment_tran
        'Sohail (05 Mar 2014) -- Start
        'Enhancement - 
        Dim objMaster As New clsMasterData
        Dim intCurrPeriodId As Integer = 0
        'Sohail (05 Mar 2014) -- End
        Dim dsCombo As DataSet
        Try

            dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid)
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (05 Mar 2014)
            intCurrPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (05 Mar 2014)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                'Sohail (05 Mar 2014) -- Start
                'Enhancement - 
                '.SelectedIndex = 0
                If intCurrPeriodId > 0 Then
                    .SelectedValue = intCurrPeriodId
                Else
                .SelectedIndex = 0
                End If
                'Sohail (05 Mar 2014) -- End
            End With

            'Anjan [ 18 Oct 2013 ] -- Start
            'ENHANCEMENT : Recruitment TRA changes requested by Andrew
            'mintTotalEmployeeCount = objEmp.GetEmployeeCount(True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)

            'dsCombo = objPayment.GetListByPeriod("PaidEmp", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT)
            'mintTotalPaidEmployee = dsCombo.Tables("PaidEmp").Rows.Count
            'Anjan [ 18 Oct 2013 ] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objApproverMap = Nothing
            objPeriod = Nothing
            'objEmp = Nothing
            'objPayment = Nothing
        End Try
    End Sub


    'Sohail (18 Aug 2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    '    Private Sub FillList()
    '        Dim dsList As DataSet
    '        Dim dtTable As DataTable
    '        Dim intCurrPayUnkID As Integer = 0
    '        Dim intPrevPayUnkID As Integer = 0
    '        Dim intCurrPriority As Integer = 0
    '        Dim intPrevPriority As Integer = 0
    '        Dim intCurrUserID As Integer = 0
    '        Dim intPrevUserID As Integer = 0
    '        Try

    '            lvPaymentApprovalList.Items.Clear()

    '            dsList = objPaymentApproval.GetApproverStatus("Approval", CInt(cboPeriod.SelectedValue), mintCurrLevelPriority)
    '            dtTable = New DataView(dsList.Tables("Approval"), "1 = 2", "", DataViewRowState.CurrentRows).ToTable

    '            For Each dsRow As DataRow In dsList.Tables("Approval").Rows
    '                intCurrPayUnkID = CInt(dsRow.Item("paymenttranunkid"))
    '                intCurrPriority = CInt(dsRow.Item("priority"))

    '                If intPrevPayUnkID = intCurrPayUnkID AndAlso intPrevPriority < intCurrPriority Then
    '                    GoTo ContinueFor
    '                End If


    '                dtTable.ImportRow(dsRow)


    'ContinueFor:
    '                intPrevPayUnkID = CInt(dsRow.Item("paymenttranunkid"))
    '                intPrevPriority = CInt(dsRow.Item("priority"))
    '            Next

    '            Dim strFilter As String = ""

    '            'Sohail (06 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'dtTable = New DataView(dtTable, strFilter, "priority, auditusername", DataViewRowState.CurrentRows).ToTable
    '            dtTable = New DataView(dtTable, strFilter, "priority, approval_date, auditusername", DataViewRowState.CurrentRows).ToTable
    '            'Sohail (06 Mar 2013) -- End

    '            Dim lvItem As New ListViewItem
    '            Dim intCount As Integer = 0
    '            Dim intTotApproved As Integer
    '            Dim intTotDisapproved As Integer
    '            'Sohail (06 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim dtCurrApprovalDate As DateTime = Nothing
    '            Dim dtPrevApprovalDate As DateTime = Nothing
    '            'Sohail (06 Mar 2013) -- End
    '            Dim drRow() As DataRow
    '            intCurrPriority = 0
    '            intPrevPriority = 0

    '            For Each dtRow As DataRow In dtTable.Rows
    '                intCurrPriority = CInt(dtRow.Item("priority"))
    '                intCurrUserID = CInt(dtRow.Item("audituserunkid"))
    '                dtCurrApprovalDate = CDate(dtRow.Item("approval_date")) 'Sohail (06 Mar 2013)

    '                'Sohail (06 Mar 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'If intPrevPriority <> intCurrPriority OrElse intPrevUserID <> intCurrUserID Then
    '                If intPrevPriority <> intCurrPriority OrElse intPrevUserID <> intCurrUserID OrElse dtPrevApprovalDate <> dtCurrApprovalDate Then
    '                    'Sohail (06 Mar 2013) -- End

    '                    lvItem = New ListViewItem

    '                    lvItem.Text = dtRow.Item("levelname").ToString

    '                    lvItem.SubItems.Add(dtRow.Item("auditusername").ToString)
    '                    lvItem.SubItems.Add(dtRow.Item("priority").ToString)

    '                    lvItem.SubItems.Add(mintTotalEmployeeCount.ToString)

    '                    'Sohail (06 Mar 2013) -- Start
    '                    'RUTTA - Remove Total Paid Column and add approval_date
    '                    'lvItem.SubItems.Add(mintTotalPaidEmployee.ToString)
    '                    lvItem.SubItems.Add(CDate(dtRow.Item("approval_date")).ToString)
    '                    'Sohail (06 Mar 2013) -- End

    '                    'Sohail (06 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'drRow = dtTable.Select("priority = " & intCurrPriority & " AND audituserunkid = " & intCurrUserID & " AND statusunkid = 1 ")
    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND approval_date = '" & dtCurrApprovalDate & "' AND audituserunkid = " & intCurrUserID & " AND statusunkid = 1 ")
    '                    'Sohail (06 Mar 2013) -- End
    '                    lvItem.SubItems.Add(drRow.Length.ToString)

    '                    'Sohail (06 Mar 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'drRow = dtTable.Select("priority = " & intCurrPriority & " AND audituserunkid = " & intCurrUserID & " AND statusunkid = 2 ")
    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND approval_date = '" & dtCurrApprovalDate & "' AND audituserunkid = " & intCurrUserID & " AND statusunkid = 2 ")
    '                    'Sohail (06 Mar 2013) -- End
    '                    lvItem.SubItems.Add(drRow.Length.ToString)

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND statusunkid = 1 ")
    '                    intTotApproved = drRow.Length

    '                    drRow = dtTable.Select("priority = " & intCurrPriority & " AND statusunkid = 2 ")
    '                    intTotDisapproved = drRow.Length

    '                    lvItem.SubItems.Add((mintTotalPaidEmployee - (intTotApproved + intTotDisapproved)).ToString)

    '                    lvItem.SubItems.Add(dtRow.Item("remarks").ToString) 'Sohail (06 Mar 2013)

    '                    lvPaymentApprovalList.Items.Add(lvItem)
    '                Else

    '                    GoTo ContinueNext
    '                End If


    'ContinueNext:
    '                intPrevPriority = intCurrPriority
    '                intPrevUserID = intCurrUserID
    '                dtPrevApprovalDate = dtCurrApprovalDate 'Sohail (06 Mar 2013)
    '            Next

    '            lvPaymentApprovalList.GroupingColumn = colhLevel
    '            lvPaymentApprovalList.DisplayGroups(True)
    '            lvPaymentApprovalList.GridLines = False

    '            If lvPaymentApprovalList.Items.Count > 4 Then
    '                colhApprover.Width = 130 - 18
    '            Else
    '                colhApprover.Width = 130
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '        End Try
    '    End Sub
    Private Sub FillList()
        Dim dsList As DataSet
        Try

            Call objbtnSearch.ShowResult("0")
            lvPaymentApprovalList.Items.Clear()

            Dim strFilter As String = ""

            dsList = objPaymentApproval.GetApprovalStatus("Approval", CInt(cboPeriod.SelectedValue), mintCurrLevelPriority, , CInt(cboEmployee.SelectedValue))


            Dim lvItem As New ListViewItem

            For Each dtRow As DataRow In dsList.Tables("Approval").Rows


                    lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("period_name").ToString
                lvItem.Tag = CInt(dtRow.Item("periodunkid"))

                lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
                lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid"))

                lvItem.SubItems.Add(dtRow.Item("voucherno").ToString)
                lvItem.SubItems.Add(dtRow.Item("username").ToString)

                lvItem.SubItems.Add(dtRow.Item("levelname").ToString)
                lvItem.SubItems.Add(dtRow.Item("priority").ToString)

                lvItem.SubItems.Add(dtRow.Item("statusname").ToString)


                If CDate(dtRow.Item("approval_date")).Date = CDate("08-Aug-8888") Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(Format(CDate(dtRow.Item("approval_date")), "dd-MMM-yyyy HH:mm:ss").ToString)
                End If

                lvItem.SubItems.Add(dtRow.Item("approvedbyusername").ToString.Trim)

                lvItem.SubItems.Add(dtRow.Item("remarks").ToString)

                    lvPaymentApprovalList.Items.Add(lvItem)

            Next

            lvPaymentApprovalList.GroupingColumn = colhEmployee
            lvPaymentApprovalList.DisplayGroups(True)
            lvPaymentApprovalList.GridLines = False

            If lvPaymentApprovalList.Items.Count > 4 Then
                colhRemarks.Width = 125 - 18
            Else
                colhRemarks.Width = 125
            End If

            Call objbtnSearch.ShowResult((From p In dsList.Tables("Approval") Select (p.Item("paymenttranunkid").ToString)).Distinct().Count().ToString & " Payments ")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            Dim dtDate1, dtDate2 As Date
            If CInt(cboPeriod.SelectedValue) > 0 Then
                dtDate1 = mdtPeriodStartDate
                dtDate2 = mdtPeriodEndDate
            Else
                dtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  dtDate1, _
                                                  dtDate2, _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, False, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (18 Aug 2015) -- End

    Private Sub SetVisibility()

        Try
            btnApprove.Enabled = User._Object.Privilege._AllowToApprovePayment
            btnVoidApproved.Enabled = User._Object.Privilege._AllowToVoidApprovedPayment

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmApproveDisapprovePaymentList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapprovePaymentList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapprovePaymentList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapprovePaymentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentApproval = New clsPayment_approval_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            'Sohail (18 Aug 2015) -- Start
            'Enhancement - List all approvals with their status on Payment Approval List screen.
            Call FillEmployeeCombo()
            'Sohail (18 Aug 2015) -- End
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapprovePaymentList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTransactionHead.SetMessages()
            objfrm._Other_ModuleNames = "clsTransactionHead"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox's Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Dim objEmp As New clsEmployee_Master
        Dim objPayment As New clsPayment_tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date

                'Anjan [10 June 2015] -- Start
               'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS 
                'Anjan [ 18 Oct 2013 ] -- Start
                'ENHANCEMENT : Recruitment TRA changes requested by Andrew

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'mintTotalEmployeeCount = objEmp.GetEmployeeCount(True, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate, , , , True)
                'mintTotalEmployeeCount = objEmp.GetEmployeeCount(FinancialYear._Object._DatabaseName, _
                '                                                 User._Object._Userunkid, _
                '                                                 FinancialYear._Object._YearUnkid, _
                '                                                 Company._Object._Companyunkid, _
                '                                                 mdtPeriodStartDate, _
                '                                                 mdtPeriodEndDate, _
                '                                                 ConfigParameter._Object._UserAccessModeSetting, True, False)
                'S.SANDEEP [04 JUN 2015] -- END
               'Anjan [10 June 2015] -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPayment.GetListByPeriod("PaidEmp", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT)
                dsList = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "PaidEmp", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, , True)
                'Sohail (21 Aug 2015) -- End
                mintTotalPaidEmployee = dsList.Tables("PaidEmp").Rows.Count
            Else
                'mintTotalEmployeeCount = 0
                mintTotalPaidEmployee = 0
                'Anjan [ 18 Oct 2013 ] -- End

            End If
            'Sohail (18 Aug 2015) -- Start
            'Enhancement - List all approvals with their status on Payment Approval List screen.
            Call FillEmployeeCombo()
            'Sohail (18 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
            objEmp = Nothing
            objPayment = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim objFrm As New frmApproveDisapprovePayment
            If objFrm.DisplayDialog(enAction.ADD_CONTINUE, True) = True Then
                Call FillList()
            End If
            objFrm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnVoidApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoidApproved.Click
        Try
            Dim objFrm As New frmApproveDisapprovePayment
            If objFrm.DisplayDialog(enAction.ADD_CONTINUE, False) = True Then
                Call FillList()
            End If
            objFrm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoidApproved_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedIndex = 0
            cboEmployee.SelectedValue = 0 'Sohail (18 Aug 2015)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Aug 2015) -- Start
    'Enhancement - List all approvals with their status on Payment Approval List screen.
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (18 Aug 2015) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnVoidApproved.GradientBackColor = GUI._ButttonBackColor 
			Me.btnVoidApproved.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
			Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
			Me.colhPriority.Text = Language._Object.getCaption(CStr(Me.colhPriority.Tag), Me.colhPriority.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhApprovalDate.Text = Language._Object.getCaption(CStr(Me.colhApprovalDate.Tag), Me.colhApprovalDate.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnVoidApproved.Text = Language._Object.getCaption(Me.btnVoidApproved.Name, Me.btnVoidApproved.Text)
			Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
			Me.colhVNo.Text = Language._Object.getCaption(CStr(Me.colhVNo.Tag), Me.colhVNo.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.colhByUser.Text = Language._Object.getCaption(CStr(Me.colhByUser.Tag), Me.colhByUser.Text)
			Me.colhRemarks.Text = Language._Object.getCaption(CStr(Me.colhRemarks.Tag), Me.colhRemarks.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class