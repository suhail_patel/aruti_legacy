﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPaymentApproverMapping

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmPaymentApproverMapping"
    Private mblnCancel As Boolean = True
    Private objPaymentApprover As clspayment_approver_mapping
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPaymentApproverUnkid As Integer = -1
    Private mintSelectedLevelId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintPaymentApproverUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintPaymentApproverUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmPaymentApproverMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objPaymentApprover = New clspayment_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Fill_Combo()

            If menAction = enAction.EDIT_ONE Then
                objPaymentApprover._Paymentapproverunkid = mintPaymentApproverUnkid
            End If

            GetValue()
            cboLevel.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentApproverLevel_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmPaymentApproverMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            ElseIf e.KeyCode = Keys.Return Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentApproverLevel_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentApproverMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPaymentApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspayment_approver_mapping.SetMessages()
            objfrm._Other_ModuleNames = "clspayment_approver_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If iSValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objPaymentApprover.Update()
            Else
                blnFlag = objPaymentApprover.Insert()
            End If

            If blnFlag = False And objPaymentApprover._Message <> "" Then
                eZeeMsgBox.Show(objPaymentApprover._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPaymentApprover = Nothing
                    objPaymentApprover = New clspayment_approver_mapping
                    Call GetValue() : cboLevel.SelectedValue = mintSelectedLevelId
                Else
                    mintPaymentApproverUnkid = objPaymentApprover._Paymentapproverunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click, objbtnSearchLevel.Click

        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHLEVEL"
                    Call Common_Search(cboLevel)
                Case "OBJBTNSEARCHUSER"
                    Call Common_Search(cboApproverUser)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsPaymentApproverlevel_master
        Dim dsList As New DataSet
        Dim objOption As New clsPassowdOptions
        Try
            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            'dsList = objLevel.getListForCombo("List", True)
            If menAction = enAction.EDIT_ONE Then
            dsList = objLevel.getListForCombo("List", True)
            Else
                dsList = objLevel.getListForCombo("List", True, True)
            End If
            'Sohail (29 Oct 2014) -- End
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


            Dim intPrivilegeId As Integer = 790

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Select Case objOption._UserLogingModeId
            '    Case enAuthenticationMode.BASIC_AUTHENTICATION
            '        If objOption._IsEmployeeAsUser Then
            '            dsList = objUsr.getComboList("List", True, False, True, Company._Object._Companyunkid, intPrivilegeId, FinancialYear._Object._YearUnkid)
            '        Else
            '            dsList = objUsr.getComboList("List", True, False, , Company._Object._Companyunkid, intPrivilegeId)
            '        End If
            '    Case enAuthenticationMode.AD_BASIC_AUTHENTICATION, enAuthenticationMode.AD_SSO_AUTHENTICATION
            '        dsList = objUsr.getComboList("List", True, True, , Company._Object._Companyunkid, intPrivilegeId)
            'End Select

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId)
            dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId.ToString(), FinancialYear._Object._YearUnkid, False)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END
            

            With cboApproverUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboLevel.SelectedValue = objPaymentApprover._Levelunkid
            cboApproverUser.SelectedValue = objPaymentApprover._Userapproverunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPaymentApprover._Levelunkid = CInt(cboLevel.SelectedValue)
            objPaymentApprover._Userapproverunkid = CInt(cboApproverUser.SelectedValue)
            If menAction = enAction.EDIT_ONE Then
                objPaymentApprover._Isvoid = objPaymentApprover._Isvoid
                objPaymentApprover._Voiddatetime = objPaymentApprover._Voiddatetime
                objPaymentApprover._Voiduserunkid = objPaymentApprover._Voiduserunkid
            Else
                objPaymentApprover._Isvoid = False
                objPaymentApprover._Voiddatetime = Nothing
                objPaymentApprover._Voiduserunkid = -1
            End If
            objPaymentApprover._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function iSValid() As Boolean
        Try
            If CInt(cboLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Payment Approver Level is mandatory information. Please select Payment Approver Level."), enMsgBoxStyle.Information)
                cboLevel.Focus()
                Return False
            ElseIf CInt(cboApproverUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Payment Approver User is mandatory information. Please select Payment Approver User."), enMsgBoxStyle.Information)
                cboApproverUser.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iSValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Common_Search(ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cbo.DisplayMember
                .ValueMember = cbo.ValueMember
                .CodeMember = ""
                .DataSource = CType(cbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog = True Then
                cbo.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Common_Search", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Try
            If CInt(cboLevel.SelectedValue) > 0 Then mintSelectedLevelId = CInt(cboLevel.SelectedValue)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbApproverMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbApproverMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbApproverMapping.Text = Language._Object.getCaption(Me.gbApproverMapping.Name, Me.gbApproverMapping.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblApprLevel.Text = Language._Object.getCaption(Me.lblApprLevel.Name, Me.lblApprLevel.Text)
			Me.lblApprUser.Text = Language._Object.getCaption(Me.lblApprUser.Name, Me.lblApprUser.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Payment Approver Level is mandatory information. Please select Payment Approver Level.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Payment Approver User is mandatory information. Please select Payment Approver User.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class