﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmApproveDisapprovePayment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapprovePayment"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private objPaymentApproval As clsPayment_approval_tran

    'Nilay (10-Feb-2016) -- Start
    'Private mdtPayPeriodStartDate As DateTime
    'Private mdtPayPeriodEndDate As DateTime
    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End

    Private mdtTable As DataTable = Nothing
    Private mdecTotAmt As Decimal = 0

    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mdecBaseCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalFormatedAmt As Decimal

    Private mblnFromApprove As Boolean
    Private mdicCurrency As New Dictionary(Of Integer, String)
    Private mstrPaymentApprovalTranIDs As String = ""

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

    Private mstrAdvanceFilter As String = "" 'Sohail (18 Feb 2014)

#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal Action As enAction, ByVal blnFromApprove As Boolean) As Boolean
        Try
            menAction = Action
            mblnFromApprove = blnFromApprove

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods & Functions "
    Private Sub SetColor()
        Try
            cboPayPeriod.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional 'Sohail (27 Feb 2013)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objExRate As New clsExchangeRate
        Dim objPeriod As New clscommom_period_Tran
        Dim objApproverMap As New clspayment_approver_mapping
        Dim objApproverLevel As New clsPaymentApproverlevel_master
        Dim dsCombo As DataSet

        Try

            'Sohail (21 Jan 2014) -- Start
            'Enhancement - Oman
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Open, True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Open, False)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open, False)
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Jan 2014) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .SelectedValue = 0
            End With

            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            Dim intBaseCountryId As Integer = 0
            'Sohail (16 Oct 2019) -- End
            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    intBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                    'Sohail (16 Oct 2019) -- End
                End If

                With cboCurrency
                    .ValueMember = "countryunkid"
                    .DisplayMember = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    '.SelectedIndex = 0
                    If intBaseCountryId > 0 Then
                        .SelectedValue = intBaseCountryId
                    Else
                    .SelectedIndex = 0
                    End If
                    'Sohail (16 Oct 2019) -- End
                End With

                'Sohail (24 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'For Each dsRow As DataRow In dsCombo.Tables(0).Rows
                '    If mdicCurrency.ContainsKey(CInt(dsRow.Item("exchangerateunkid"))) = False Then
                '        mdicCurrency.Add(CInt(dsRow.Item("exchangerateunkid")), dsRow.Item("currency_sign").ToString)
                '    End If
                'Next
                For Each dsRow As DataRow In dsCombo.Tables(0).Rows
                    If mdicCurrency.ContainsKey(CInt(dsRow.Item("countryunkid"))) = False Then
                        mdicCurrency.Add(CInt(dsRow.Item("countryunkid")), dsRow.Item("currency_sign").ToString)
                    End If
                Next
                'Sohail (24 Jun 2013) -- End
            End If

            'Sohail (16 Apr 2015) -- Start
            'Aghakhan Issue -  inactive Level users also can approve payments.
            'dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid)
            dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid, " prpaymentapproverlevel_master.isinactive = 0 ")
            'Sohail (16 Apr 2015) -- End
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority)
                mintMinPriority = objApproverLevel.GetMinPriority()
                mintMaxPriority = objApproverLevel.GetMaxPriority()
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority)

                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                    objApproverName.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver").ToString
                    objApproverLevelVal.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver_level").ToString
                    objLevelPriorityVal.Text = mintCurrLevelPriority.ToString
                End If


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objExRate = Nothing
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        'Dim strFilter As String = ""
        Dim decTotAmt As Decimal = 0
        Dim mintBankPayment As Integer = 0
        Dim mintCashPayment As Integer = 0
        Dim intIsApproved As Integer
        Dim strEmpFilter As String = "" 'Sohail (18 Feb 2014)

        Try
            mstrPaymentApprovalTranIDs = ""
            lvEmployeeList.Items.Clear()
            txtTotalAmount.Text = Format(decTotAmt, GUI.fmtCurrency)


            If CInt(cboPayPeriod.SelectedValue) <= 0 OrElse CInt(cboCurrency.SelectedValue) <= 0 OrElse mintCurrLevelPriority < 0 Then
                'Sohail (20 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                objBankpaidVal.Text = mintBankPayment.ToString
                objCashpaidVal.Text = mintCashPayment.ToString
                objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString
                'Sohail (20 Feb 2013) -- End
                Exit Try
            End If

            Cursor.Current = Cursors.WaitCursor

            Dim lvItem As ListViewItem
            Dim lvArray As New List(Of ListViewItem)
            lvEmployeeList.BeginUpdate()

            If mblnFromApprove = True Then
                intIsApproved = 0
            Else
                intIsApproved = -1
            End If

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            strEmpFilter = mstrAdvanceFilter
            If cboPmtVoucher.SelectedIndex > 0 Then
                If strEmpFilter.Trim = "" Then
                    strEmpFilter &= " (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
                Else
                    strEmpFilter &= " AND (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
                End If
            End If
            'Sohail (18 Feb 2014) -- End

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            'dsList = objPaymentApproval.GetListForApproval("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mblnFromApprove, mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved)
            'Nilay (10-Feb-2016) -- Start
            'dsList = objPaymentApproval.GetListForApproval("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mblnFromApprove, mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved, strEmpFilter)
            dsList = objPaymentApproval.GetListForApproval(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           mdtPayPeriodStartDate, _
                                                           mdtPayPeriodEndDate, _
                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                           True, _
                                                           False, _
                                                           "List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, _
                                                           mblnFromApprove, mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, _
                                                           CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved, strEmpFilter)
            'Nilay (10-Feb-2016) -- End
            'Sohail (18 Feb 2014) -- End

            Dim intCount As Integer = dsList.Tables("List").Rows.Count
            Dim dtRow As DataRow
            For i As Integer = 0 To intCount - 1
                dtRow = dsList.Tables("List").Rows(i)
                lvItem = New ListViewItem

                With lvItem
                    .Text = ""
                    .Tag = dtRow.Item("paymentapprovaltranunkid").ToString

                    .SubItems.Add(dtRow.Item("employeecode").ToString)
                    .SubItems(colhCode.Index).Tag = dtRow.Item("paymenttranunkid").ToString 'paymenttranunkid

                    .SubItems.Add(dtRow.Item("EmpName").ToString)
                    .SubItems(colhName.Index).Tag = dtRow.Item("employeeunkid").ToString 'Sohail (20 Feb 2013)

                    .SubItems.Add(Format(CDec(dtRow.Item("expaidamt")), GUI.fmtCurrency))
                    .SubItems(colhAmount.Index).Tag = CDec(dtRow.Item("expaidamt"))
                    decTotAmt += CDec(dtRow.Item("expaidamt"))

                    'Sohail (21 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If mdicCurrency.ContainsKey(CInt(dtRow.Item("paidcurrencyid"))) = True Then
                    '    .SubItems.Add(mdicCurrency.Item(CInt(dtRow.Item("paidcurrencyid"))))
                    'Else
                    '    .SubItems.Add(mstrBaseCurrSign)
                    'End If
                    If mdicCurrency.ContainsKey(CInt(dtRow.Item("countryunkid"))) = True Then
                        .SubItems.Add(mdicCurrency.Item(CInt(dtRow.Item("countryunkid"))))
                    Else
                        .SubItems.Add(mstrBaseCurrSign)
                    End If
                    'Sohail (21 Jun 2013) -- End
                    .SubItems(colhCurrency.Index).Tag = CInt(dtRow.Item("paymentmode")) 'Sohail (20 Feb 2013) - [Payment Mode]

                End With

                'Sohail (20 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                If CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE Then
                    mintCashPayment += 1
                ElseIf CInt(dtRow.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.TRANSFER Then
                    mintBankPayment += 1
                End If
                'Sohail (20 Feb 2013) -- End

                lvArray.Add(lvItem)

            Next

            RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            lvEmployeeList.Items.AddRange(lvArray.ToArray)
            AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked

            If colhAmount.Width > 0 Then
                If lvEmployeeList.Items.Count > 19 Then 'Sohail (16 Oct 2019)
                colhName.Width = 120 - 18
            Else
                colhName.Width = 120
            End If
            End If 'Sohail (16 Oct 2019)
            'txtTotalAmount.Text = Format(decTotAmt, GUI.fmtCurrency) 'Sohail (10 Mar 2014)
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            If User._Object.Privilege._AllowToViewPaidAmount = False Then
                colhName.Width += colhAmount.Width
                colhAmount.Width = 0
                lvEmployeeList.Enabled = False
            End If
            'Sohail (16 Oct 2019) -- End
            lvEmployeeList.EndUpdate()

            'Sohail (20 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            objBankpaidVal.Text = mintBankPayment.ToString
            objCashpaidVal.Text = mintCashPayment.ToString
            objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString
            'Sohail (20 Feb 2013) -- End

            Cursor.Current = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                Return False
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Currency. Currency is mandatory information."), enMsgBoxStyle.Information)
                Return False
                'Sohail (27 Feb 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf lvEmployeeList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one employee to Approve / Void Approved payment."), enMsgBoxStyle.Information)
                Return False
                'ElseIf lvEmployeeList.Items.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry! There is no payment available to Approve / Void Approved."), enMsgBoxStyle.Information)
                '    Return False
                'Sohail (27 Feb 2013) -- End
            ElseIf mintCurrLevelPriority < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry! You can not not Approve / Disapprove Payment. Please contact Administrator."), enMsgBoxStyle.Information)
                Return False
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._Notify_Payroll_Users.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Return False
            End If
            'Sohail (10 Mar 2014) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "
    Private Sub frmApproveDisapprovePayment_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapprovePayment_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapprovePayment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapprovePayment_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapprovePayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentApproval = New clsPayment_approval_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            mdtTable = objPaymentApproval._DataTable

            If mblnFromApprove = True Then
                Me.Text = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                btnProcess.Text = Language.getMessage(mstrModuleName, 12, "Approve Payment")
                lblRemarks.Text = Language.getMessage(mstrModuleName, 13, "Approval Remarks") 'Sohail (27 Feb 2013)
            Else
                Me.Text = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                btnProcess.Text = Language.getMessage(mstrModuleName, 15, "Void Approved Payment")
                lblRemarks.Text = Language.getMessage(mstrModuleName, 16, "Disapproval Remarks") 'Sohail (27 Feb 2013)
            End If
            'Hemant (17 Oct 2019) -- Start
            lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (17 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapprovePayment_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayment_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayment_approval_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (06 Apr 2013) -- End

#End Region

#Region " ComboBox's Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPayment As New clsPayment_tran
                Dim dsCombo As DataSet
                dsCombo = objPayment.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, FinancialYear._Object._DatabaseName, CInt(cboPayPeriod.SelectedValue))
                With cboPmtVoucher
                    .ValueMember = "voucherno"
                    .DisplayMember = "voucherno"
                    .DataSource = dsCombo.Tables("Voucher")
                    .SelectedIndex = 0
                End With
                objPayment = Nothing
                'Nilay (10-Feb-2016) -- Start
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
                mdtPayPeriodStartDate = objPeriod._Start_Date
                mdtPayPeriodEndDate = objPeriod._End_Date
                objPeriod = Nothing
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Nilay (10-Feb-2016) -- End
            End If
            'Sohail (18 Feb 2014) -- End
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            lblTotalAmount.Text = "Total Amount (" & cboCurrency.Text & ")"
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Events "
    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        Dim decTotAmt As Decimal
        Try
            decTotAmt = 0

            If lvEmployeeList.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                '    If lvItem.Checked = True Then
                '        decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text)
                '    End If
                'Next
            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                '    If lvItem.Checked = True Then
                '        decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text)
                '    End If
                'Next
            End If

            'mdecTotAmt = decTotAmt
            mdecTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhAmount.Index).Text)).Sum() 'Sohail (10 Mar 2014)

            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
            '    decTotAmt = decTotAmt * txtCashPerc.Decimal / 100
            'End If
            txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency) 'Sohail (27 Feb 2013)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0

        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
            '    If lvItem.Checked = True Then
            '        decTotAmt += CDec(lvItem.SubItems(colhAmount.Index).Text)
            '    End If
            'Next
            'mdecTotAmt = decTotAmt
            mdecTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhAmount.Index).Text)).Sum() 'Sohail (10 Mar 2014)

            'If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
            '    decTotAmt = decTotAmt * txtCashPerc.Decimal / 100
            'End If
            txtTotalAmount.Text = Format(mdecTotAmt, GUI.fmtCurrency) 'Sohail (27 Feb 2013)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnResult As Boolean
        Dim dsRow As DataRow
        Dim intIsApproved As Integer = 0
        Dim strMsg As String
        Dim strEmpIDs As String = ""
        'Sohail (23 Feb 2013) -- Start
        'TRA - ENHANCEMENT
        Dim intBankPaid As Integer = 0
        Dim intCashPaid As Integer = 0
        Dim decBankPaid As Decimal = 0
        Dim decCashPaid As Decimal = 0
        'Sohail (23 Feb 2013) -- End

        Try
            If IsValid() = False Then Exit Sub

            mdtTable.Clear()

            If mblnFromApprove = True Then
                'Sohail (27 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                'strMsg = Language.getMessage(mstrModuleName, 7, "Are you sure you want to Approve listed Payment?")
                'Sohail (29 Mar 2016) -- Start
                'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
                'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                    'Sohail (29 Mar 2016) -- End
                    strMsg = Language.getMessage(mstrModuleName, 7, "Are you sure you want to Final Approve selected Payments?")
                Else
                    strMsg = Language.getMessage(mstrModuleName, 8, "Are you sure you want to Approve selected Payments?")
                End If
                'Sohail (27 Feb 2013) -- End

            Else
                strMsg = Language.getMessage(mstrModuleName, 9, "Are you sure you want to Disapprove selected Payments?")
            End If

            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            Dim mstrDisApprovalReason As String = String.Empty
            If mblnFromApprove = False Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If


                frm.displayDialog(enVoidCategoryType.PAYMENT_DISAPPROVAL, mstrDisApprovalReason)
                If mstrDisApprovalReason.Length <= 0 Then
                    Exit Try
                End If
            End If


            'Sohail (27 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                'For Each lvItem As ListViewItem In lvEmployeeList.Items
                'Sohail (27 Feb 2013) -- End
                dsRow = mdtTable.NewRow

                dsRow.Item("paymentapprovaltranunkid") = lvItem.Tag
                dsRow.Item("paymenttranunkid") = lvItem.SubItems(colhCode.Index).Tag 'paymenttranunkid

                dsRow.Item("levelunkid") = mintCurrLevelID
                dsRow.Item("priority") = mintCurrLevelPriority
                dsRow.Item("approval_date") = ConfigParameter._Object._CurrentDateAndTime

                If mblnFromApprove = True Then
                    dsRow.Item("statusunkid") = 1 'Approve
                    dsRow.Item("isvoid") = False
                Else
                    dsRow.Item("statusunkid") = 2 'DisApproved
                    dsRow.Item("isvoid") = True
                End If
                dsRow.Item("userunkid") = User._Object._Userunkid
                dsRow.Item("disapprovalreason") = mstrDisApprovalReason
                dsRow.Item("remarks") = txtRemarks.Text.Trim 'Sohail (27 Feb 2013)

                dsRow.Item("voiduserunkid") = -1
                dsRow.Item("voiddatetime") = DBNull.Value
                dsRow.Item("voidreason") = ""

                'Sohail (20 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                If strEmpIDs.Trim = "" Then
                    strEmpIDs = lvItem.SubItems(colhName.Index).Tag.ToString
                Else
                    strEmpIDs &= "," & lvItem.SubItems(colhName.Index).Tag.ToString
                End If
                'Sohail (20 Feb 2013) -- End

                'Sohail (23 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                If CInt(lvItem.SubItems(colhCurrency.Index).Tag) = enPaymentMode.CASH OrElse CInt(lvItem.SubItems(colhCurrency.Index).Tag) = enPaymentMode.CASH_AND_CHEQUE Then
                    intCashPaid += 1
                    decCashPaid += CDec(lvItem.SubItems(colhAmount.Index).Text)
                ElseIf CInt(lvItem.SubItems(colhCurrency.Index).Tag) = enPaymentMode.CHEQUE OrElse CInt(lvItem.SubItems(colhCurrency.Index).Tag) = enPaymentMode.TRANSFER Then
                    intBankPaid += 1
                    decBankPaid += CDec(lvItem.SubItems(colhAmount.Index).Text)
                End If
                'Sohail (23 Feb 2013) -- End
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                dsRow.Item("employeeunkid") = CInt(lvItem.SubItems(colhName.Index).Tag)
                dsRow.Item("Paymentmodeid") = CInt(lvItem.SubItems(colhCurrency.Index).Tag)
                dsRow.Item("expaidamt") = CDec(lvItem.SubItems(colhAmount.Index).Text)
                'Sohail (18 Feb 2014) -- End

                mdtTable.Rows.Add(dsRow)
            Next

            'Sohail (29 Mar 2016) -- Start
            'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
            'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso mblnFromApprove = True Then
            If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso mblnFromApprove = True Then
                'Sohail (29 Mar 2016) -- End
                intIsApproved = 1
                'Sohail (29 Mar 2016) -- Start
                'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
                'ElseIf mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso mblnFromApprove = False Then
            ElseIf mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso mblnFromApprove = False Then
                'Sohail (29 Mar 2016) -- End
                intIsApproved = 2
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnResult = objPaymentApproval.UpdatePaymentApproval(mdtTable, intIsApproved)

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'blnResult = objPaymentApproval.UpdatePaymentApproval(mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)

            'Nilay (25-Mar-2016) -- Start
            'blnResult = objPaymentApproval.UpdatePaymentApproval(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)
            blnResult = objPaymentApproval.UpdatePaymentApproval(FinancialYear._Object._DatabaseName, _
                                                                 User._Object._Userunkid, _
                                                                 FinancialYear._Object._YearUnkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 True, mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)
            'Nilay (25-Mar-2016) -- End

            'Sohail (15 Dec 2015) -- End


            'Sohail (21 Aug 2015) -- End

            If blnResult = False And objPaymentApproval._Message <> "" Then
                eZeeMsgBox.Show(objPaymentApproval._Message, enMsgBoxStyle.Information)
            Else
                Dim objPaymentTran As New clsPayment_tran
                If mblnFromApprove = True Then
                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency)

                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable)

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0)
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (17 Dec 2014) -- End
                    'S.SANDEEP [ 28 JAN 2014 ] -- END

                    'Sohail (18 Feb 2014) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Payment Approval process completed successfully."), enMsgBoxStyle.Information)
                Else
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable)
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0)
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mdtTable, enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (17 Dec 2014) -- End
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Void Approved Payment process completed successfully."), enMsgBoxStyle.Information)
                End If
                cboPayPeriod.SelectedValue = 0
                'Sohail (27 Feb 2013) -- Start
                'TRA - ENHANCEMENT
                txtRemarks.Text = ""
                mblnCancel = False
                'Sohail (27 Feb 2013) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
#Region " Other Controls Events "
    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboPmtVoucher.Items.Count > 0 Then cboPmtVoucher.SelectedIndex = 0
            mstrAdvanceFilter = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_Click", mstrModuleName)
        End Try
    End Sub

    'Hemant (17 Oct 2019) -- Start
    Private Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollVarianceReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Try
            Dim frm As New ArutiReports.frmPayrollTotalVariance
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkPayrollTotalVarianceReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (17 Oct 2019) -- End

#End Region
    'Sohail (18 Feb 2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbAmountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAmountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPaymentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPaymentApprovalnfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentApprovalnfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPaymentModeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPaymentModeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbAmountInfo.Text = Language._Object.getCaption(Me.gbAmountInfo.Name, Me.gbAmountInfo.Text)
            Me.lblTotalAmount.Text = Language._Object.getCaption(Me.lblTotalAmount.Name, Me.lblTotalAmount.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbPaymentInfo.Text = Language._Object.getCaption(Me.gbPaymentInfo.Name, Me.gbPaymentInfo.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCurrency.Text = Language._Object.getCaption(CStr(Me.colhCurrency.Tag), Me.colhCurrency.Text)
            Me.gbPaymentApprovalnfo.Text = Language._Object.getCaption(Me.gbPaymentApprovalnfo.Name, Me.gbPaymentApprovalnfo.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblLevelPriority.Text = Language._Object.getCaption(Me.lblLevelPriority.Name, Me.lblLevelPriority.Text)
            Me.gbPaymentModeInfo.Text = Language._Object.getCaption(Me.gbPaymentModeInfo.Name, Me.gbPaymentModeInfo.Text)
            Me.lblTotalPaid.Text = Language._Object.getCaption(Me.lblTotalPaid.Name, Me.lblTotalPaid.Text)
            Me.lblCashPaid.Text = Language._Object.getCaption(Me.lblCashPaid.Name, Me.lblCashPaid.Text)
            Me.lblbankPaid.Text = Language._Object.getCaption(Me.lblbankPaid.Name, Me.lblbankPaid.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
            Me.lblPmtVoucher.Text = Language._Object.getCaption(Me.lblPmtVoucher.Name, Me.lblPmtVoucher.Text)
            Me.lnkPayrollVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollVarianceReport.Name, Me.lnkPayrollVarianceReport.Text)
            Me.lnkPayrollTotalVarianceReport.Text = Language._Object.getCaption(Me.lnkPayrollTotalVarianceReport.Name, Me.lnkPayrollTotalVarianceReport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select Currency. Currency is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select atleast one employee to Approve / Void Approved payment.")
            Language.setMessage(mstrModuleName, 4, "Sorry! You can not not Approve / Disapprove Payment. Please contact Administrator.")
            Language.setMessage(mstrModuleName, 5, "Payment Approval process completed successfully.")
            Language.setMessage(mstrModuleName, 6, "Void Approved Payment process completed successfully.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to Final Approve selected Payments?")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to Approve selected Payments?")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want to Disapprove selected Payments?")
            Language.setMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 11, "Approve Payment")
            Language.setMessage(mstrModuleName, 12, "Approve Payment")
            Language.setMessage(mstrModuleName, 13, "Approval Remarks")
            Language.setMessage(mstrModuleName, 14, "Void Approved Payment")
            Language.setMessage(mstrModuleName, 15, "Void Approved Payment")
            Language.setMessage(mstrModuleName, 16, "Disapproval Remarks")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class