﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPaymentApproverLevelList

#Region "Private Variable"

    Private objApproverLevel As clsPaymentApproverlevel_master
    Private ReadOnly mstrModuleName As String = "frmPaymentApproverLevelList"

#End Region

#Region " Private Methods "

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Private Sub SetColor()
        Try
            cboLevel.BackColor = GUI.ColorOptional
            cboActiveInactive.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objApproverLevel.getListForCombo("Level", True)
            With cboLevel
                .DisplayMember = "Name"
                .ValueMember = "levelunkid"
                .DataSource = dsCombo.Tables("Level")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub
    'Sohail (29 Oct 2014) -- End

    Private Sub FillList()
        Dim dsList As DataSet
        Dim strFilter As String = "" 'Sohail (29 Oct 2014)
        Try

            If User._Object.Privilege._AllowToViewPaymentApproverLevelList = True Then


                'Sohail (29 Oct 2014) -- Start
                'AKF Enhancement - Inactive option on Payment Approver Level.
                'dsList = objApproverLevel.GetList("List")
                If CInt(cboLevel.SelectedValue) > 0 Then
                    strFilter = " AND levelunkid = " & CInt(cboLevel.SelectedValue) & " "
                End If
                If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
                    strFilter &= " AND isinactive = 1 "
                Else
                    strFilter &= " AND isinactive = 0 "
                End If
                If strFilter.Trim <> "" Then strFilter = strFilter.Substring(4)
                dsList = objApproverLevel.GetList("List", False, , strFilter)

                btnInactive.Visible = False
                btnActive.Visible = False
                'Sohail (29 Oct 2014) -- End

                Dim lvItem As ListViewItem

                lvApproverLevelList.Items.Clear()
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = drRow("levelcode").ToString
                    lvItem.Tag = drRow("levelunkid")

                    lvItem.SubItems.Add(drRow("levelname").ToString)
                    lvItem.SubItems.Add(drRow("priority").ToString)

                    lvApproverLevelList.Items.Add(lvItem)
                Next

                'Sohail (29 Oct 2014) -- Start
                'AKF Enhancement - Inactive option on Payment Approver Level.
                If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                    btnInactive.Visible = True
                ElseIf CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
                    btnActive.Visible = True
                End If
                'Sohail (29 Oct 2014) -- End

                If lvApproverLevelList.Items.Count > 16 Then
                    colhApproveLevelName.Width = 400 - 18
                Else
                    colhApproveLevelName.Width = 400
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            dsList = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddPaymentApproverLevel
            btnEdit.Enabled = User._Object.Privilege._AllowToEditPaymentApproverLevel
            btnDelete.Enabled = User._Object.Privilege._AllowToDeletePaymentApproverLevel
            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            btnActive.Enabled = User._Object.Privilege._AllowToSetPaymentApproverLevelActive
            btnInactive.Enabled = User._Object.Privilege._AllowToSetPaymentApproverLevelActive
            'Sohail (29 Oct 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Event "

    Private Sub frmPaymentApproverLevelList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApproverLevel = New clsPaymentApproverlevel_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            Call SetColor()
            Call FillCombo()
            'Sohail (29 Oct 2014) -- End

            FillList()

            If lvApproverLevelList.Items.Count > 0 Then lvApproverLevelList.Items(0).Selected = True
            lvApproverLevelList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentApproverLevelList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentApproverLevelList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentApproverLevelList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentApproverLevelList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPaymentApproverlevel_master.SetMessages()
            objfrm._Other_ModuleNames = "clsPaymentApproverlevel_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objPaymentApproverLevel_AddEdit As New frmPaymentApproverLevel_AddEdit
            If objPaymentApproverLevel_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        'Sohail (29 Oct 2014) -- Start
        'AKF Enhancement - Inactive option on Payment Approver Level.
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! You can not Edit this Approval Level. This Approval Level is Inactive."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        'Sohail (29 Oct 2014) -- End

        Dim objfrmPaymentApproverLevel_AddEdit As New frmPaymentApproverLevel_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverLevelList.SelectedItems(0).Index
            If objfrmPaymentApproverLevel_AddEdit.displayDialog(CInt(lvApproverLevelList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            objfrmPaymentApproverLevel_AddEdit = Nothing

            lvApproverLevelList.Items(intSelectedIndex).Selected = True
            lvApproverLevelList.EnsureVisible(intSelectedIndex)
            lvApproverLevelList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmPaymentApproverLevel_AddEdit IsNot Nothing Then objfrmPaymentApproverLevel_AddEdit.Dispose()
        End Try
    End Sub

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Private Sub btnInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInactive.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.INACTIVE Then
            Exit Sub
        End If
        Dim objMaster As New clsMasterData
        Dim intFirstOpenPeriodID As Integer = 0

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intFirstOpenPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            intFirstOpenPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            If intFirstOpenPeriodID > 0 Then
                If objApproverLevel.isUsedInOpenPeriod(CInt(lvApproverLevelList.SelectedItems(0).Tag), intFirstOpenPeriodID) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot inactivate this Approver Level. Reason: This Approver Level is in use in First Open Period."), enMsgBoxStyle.Information) '?2
                    lvApproverLevelList.Select()
                    Exit Try
                End If
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverLevelList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to make inactive this Approver Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            Dim frm As New frmReasonSelection
            Dim mstrVoidReason As String = String.Empty
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Sub
            End If

            objApproverLevel.MakeActiveInactive(False, CInt(lvApproverLevelList.SelectedItems(0).Tag), User._Object._Userunkid)

            lvApproverLevelList.SelectedItems(0).Remove()

            If lvApproverLevelList.Items.Count <= 0 Then
                Exit Try
            End If

            If lvApproverLevelList.Items.Count = intSelectedIndex Then
                intSelectedIndex = lvApproverLevelList.Items.Count - 1
                lvApproverLevelList.Items(intSelectedIndex).Selected = True
                lvApproverLevelList.EnsureVisible(intSelectedIndex)
            ElseIf lvApproverLevelList.Items.Count <> 0 Then
                lvApproverLevelList.Items(intSelectedIndex).Selected = True
                lvApproverLevelList.EnsureVisible(intSelectedIndex)
            End If
            lvApproverLevelList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInactive_Click", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub btnActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActive.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
            Exit Sub
        End If
        Dim blnResult As Boolean
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to make active this Approver Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            blnResult = objApproverLevel.MakeActiveInactive(True, CInt(lvApproverLevelList.SelectedItems(0).Tag), User._Object._Userunkid)

            If blnResult = False AndAlso objApproverLevel._Message <> "" Then
                eZeeMsgBox.Show(objApproverLevel._Message, enMsgBoxStyle.Information)
            Else
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLevel.SelectedValue = 0
            cboActiveInactive.SelectedValue = enTranHeadActiveInActive.ACTIVE
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Oct 2014) -- End

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvApproverLevelList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApproverLevelList.Select()
            Exit Sub
        End If
        If objApproverLevel.isUsed(CInt(lvApproverLevelList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), enMsgBoxStyle.Information) '?2
            lvApproverLevelList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvApproverLevelList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objApproverLevel.Delete(CInt(lvApproverLevelList.SelectedItems(0).Tag))
                lvApproverLevelList.SelectedItems(0).Remove()

                If lvApproverLevelList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvApproverLevelList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvApproverLevelList.Items.Count - 1
                    lvApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvApproverLevelList.EnsureVisible(intSelectedIndex)
                ElseIf lvApproverLevelList.Items.Count <> 0 Then
                    lvApproverLevelList.Items(intSelectedIndex).Selected = True
                    lvApproverLevelList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvApproverLevelList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboLevel
                objfrm.DataSource = CType(cboLevel.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "levelcode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (29 Oct 2014) -- End

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnActive.GradientBackColor = GUI._ButttonBackColor
            Me.btnActive.GradientForeColor = GUI._ButttonFontColor

            Me.btnInactive.GradientBackColor = GUI._ButttonBackColor
            Me.btnInactive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhApproveLevelCode.Text = Language._Object.getCaption(CStr(Me.colhApproveLevelCode.Tag), Me.colhApproveLevelCode.Text)
            Me.colhApproveLevelName.Text = Language._Object.getCaption(CStr(Me.colhApproveLevelName.Tag), Me.colhApproveLevelName.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhApproveLevelPriority.Text = Language._Object.getCaption(CStr(Me.colhApproveLevelPriority.Tag), Me.colhApproveLevelPriority.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblActiveInactive.Text = Language._Object.getCaption(Me.lblActiveInactive.Name, Me.lblActiveInactive.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.btnActive.Text = Language._Object.getCaption(Me.btnActive.Name, Me.btnActive.Text)
            Me.btnInactive.Text = Language._Object.getCaption(Me.btnInactive.Name, Me.btnInactive.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver Level from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver Level?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot inactivate this Approver Level. Reason: This Approver Level is in use in First Open Period.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to make inactive this Approver Level?")
            Language.setMessage(mstrModuleName, 6, "Sorry! You can not Edit this Approval Level. This Approval Level is Inactive.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to make active this Approver Level?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class