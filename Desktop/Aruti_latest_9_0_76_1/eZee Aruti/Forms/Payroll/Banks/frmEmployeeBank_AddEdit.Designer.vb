﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeBank_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeBank_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lnkCopySlab = New System.Windows.Forms.LinkLabel
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTransactionHead = New System.Windows.Forms.Panel
        Me.lvEmpBankList = New eZee.Common.eZeeListView(Me.components)
        Me.colhID = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhBankGrp = New System.Windows.Forms.ColumnHeader
        Me.colhBankBranch = New System.Windows.Forms.ColumnHeader
        Me.colhAccType = New System.Windows.Forms.ColumnHeader
        Me.colhAccNo = New System.Windows.Forms.ColumnHeader
        Me.colhPerc = New System.Windows.Forms.ColumnHeader
        Me.colhGUID = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhModeID = New System.Windows.Forms.ColumnHeader
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmpBankDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMode = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblPerc = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBankBranch = New eZee.Common.eZeeGradientButton
        Me.lblSalaryDistrib = New System.Windows.Forms.Label
        Me.objbtnSearchBankGroup = New eZee.Common.eZeeGradientButton
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.objbtnAddAccType = New eZee.Common.eZeeGradientButton
        Me.txtAccountNo = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddBranch = New eZee.Common.eZeeGradientButton
        Me.objbtnAddBankGroup = New eZee.Common.eZeeGradientButton
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblAccountType = New System.Windows.Forms.Label
        Me.lblBank = New System.Windows.Forms.Label
        Me.cboAccountType = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlTransactionHead.SuspendLayout()
        Me.gbEmpBankDetails.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lnkCopySlab)
        Me.pnlMainInfo.Controls.Add(Me.btnDelete)
        Me.pnlMainInfo.Controls.Add(Me.Panel1)
        Me.pnlMainInfo.Controls.Add(Me.btnEdit)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.btnAdd)
        Me.pnlMainInfo.Controls.Add(Me.gbEmpBankDetails)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(767, 459)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lnkCopySlab
        '
        Me.lnkCopySlab.Location = New System.Drawing.Point(658, 195)
        Me.lnkCopySlab.Name = "lnkCopySlab"
        Me.lnkCopySlab.Size = New System.Drawing.Size(106, 13)
        Me.lnkCopySlab.TabIndex = 109
        Me.lnkCopySlab.TabStop = True
        Me.lnkCopySlab.Text = "Copy Prev. Details"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(658, 162)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objbtnUp)
        Me.Panel1.Controls.Add(Me.objbtnDown)
        Me.Panel1.Controls.Add(Me.pnlTransactionHead)
        Me.Panel1.Location = New System.Drawing.Point(12, 218)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(743, 179)
        Me.Panel1.TabIndex = 1
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Main.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(646, 23)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 149
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.Aruti.Main.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(646, 52)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 150
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'pnlTransactionHead
        '
        Me.pnlTransactionHead.Controls.Add(Me.lvEmpBankList)
        Me.pnlTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTransactionHead.Location = New System.Drawing.Point(0, 0)
        Me.pnlTransactionHead.Name = "pnlTransactionHead"
        Me.pnlTransactionHead.Size = New System.Drawing.Size(642, 178)
        Me.pnlTransactionHead.TabIndex = 148
        '
        'lvEmpBankList
        '
        Me.lvEmpBankList.BackColorOnChecked = True
        Me.lvEmpBankList.ColumnHeaders = Nothing
        Me.lvEmpBankList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhID, Me.colhEmployee, Me.colhBankGrp, Me.colhBankBranch, Me.colhAccType, Me.colhAccNo, Me.colhPerc, Me.colhGUID, Me.colhPeriod, Me.colhModeID})
        Me.lvEmpBankList.CompulsoryColumns = ""
        Me.lvEmpBankList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmpBankList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmpBankList.FullRowSelect = True
        Me.lvEmpBankList.GridLines = True
        Me.lvEmpBankList.GroupingColumn = Nothing
        Me.lvEmpBankList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmpBankList.HideSelection = False
        Me.lvEmpBankList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmpBankList.MinColumnWidth = 50
        Me.lvEmpBankList.MultiSelect = False
        Me.lvEmpBankList.Name = "lvEmpBankList"
        Me.lvEmpBankList.OptionalColumns = ""
        Me.lvEmpBankList.ShowMoreItem = False
        Me.lvEmpBankList.ShowSaveItem = False
        Me.lvEmpBankList.ShowSelectAll = True
        Me.lvEmpBankList.ShowSizeAllColumnsToFit = True
        Me.lvEmpBankList.Size = New System.Drawing.Size(642, 178)
        Me.lvEmpBankList.Sortable = False
        Me.lvEmpBankList.TabIndex = 0
        Me.lvEmpBankList.UseCompatibleStateImageBehavior = False
        Me.lvEmpBankList.View = System.Windows.Forms.View.Details
        '
        'colhID
        '
        Me.colhID.Text = "Priority"
        Me.colhID.Width = 50
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhBankGrp
        '
        Me.colhBankGrp.Tag = "colhBankGrp"
        Me.colhBankGrp.Text = "Bank Group"
        Me.colhBankGrp.Width = 150
        '
        'colhBankBranch
        '
        Me.colhBankBranch.Tag = "colhBankBranch"
        Me.colhBankBranch.Text = "Bank Branch"
        Me.colhBankBranch.Width = 140
        '
        'colhAccType
        '
        Me.colhAccType.Tag = "colhAccType"
        Me.colhAccType.Text = "Account Type"
        Me.colhAccType.Width = 110
        '
        'colhAccNo
        '
        Me.colhAccNo.Tag = "colhAccNo"
        Me.colhAccNo.Text = "Account No"
        Me.colhAccNo.Width = 120
        '
        'colhPerc
        '
        Me.colhPerc.Tag = "colhPerc"
        Me.colhPerc.Text = "(%)"
        Me.colhPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPerc.Width = 65
        '
        'colhGUID
        '
        Me.colhGUID.Text = "GUID"
        Me.colhGUID.Width = 0
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = ""
        Me.colhPeriod.Width = 0
        '
        'colhModeID
        '
        Me.colhModeID.Tag = "colhModeID"
        Me.colhModeID.Text = "Mode"
        Me.colhModeID.Width = 0
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(658, 126)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(767, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Add / Edit Employee Bank"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(658, 90)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'gbEmpBankDetails
        '
        Me.gbEmpBankDetails.BorderColor = System.Drawing.Color.Black
        Me.gbEmpBankDetails.Checked = False
        Me.gbEmpBankDetails.CollapseAllExceptThis = False
        Me.gbEmpBankDetails.CollapsedHoverImage = Nothing
        Me.gbEmpBankDetails.CollapsedNormalImage = Nothing
        Me.gbEmpBankDetails.CollapsedPressedImage = Nothing
        Me.gbEmpBankDetails.CollapseOnLoad = False
        Me.gbEmpBankDetails.Controls.Add(Me.lblMode)
        Me.gbEmpBankDetails.Controls.Add(Me.cboMode)
        Me.gbEmpBankDetails.Controls.Add(Me.lblPerc)
        Me.gbEmpBankDetails.Controls.Add(Me.lblPeriod)
        Me.gbEmpBankDetails.Controls.Add(Me.cboPeriod)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnSearchBankBranch)
        Me.gbEmpBankDetails.Controls.Add(Me.lblSalaryDistrib)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnSearchBankGroup)
        Me.gbEmpBankDetails.Controls.Add(Me.txtPercentage)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnAddAccType)
        Me.gbEmpBankDetails.Controls.Add(Me.txtAccountNo)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnAddBranch)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnAddBankGroup)
        Me.gbEmpBankDetails.Controls.Add(Me.lblAccountNo)
        Me.gbEmpBankDetails.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmpBankDetails.Controls.Add(Me.lblAccountType)
        Me.gbEmpBankDetails.Controls.Add(Me.lblBank)
        Me.gbEmpBankDetails.Controls.Add(Me.cboAccountType)
        Me.gbEmpBankDetails.Controls.Add(Me.cboEmployee)
        Me.gbEmpBankDetails.Controls.Add(Me.cboBankBranch)
        Me.gbEmpBankDetails.Controls.Add(Me.cboBankGroup)
        Me.gbEmpBankDetails.Controls.Add(Me.lblBankGroup)
        Me.gbEmpBankDetails.Controls.Add(Me.lblEmployee)
        Me.gbEmpBankDetails.ExpandedHoverImage = Nothing
        Me.gbEmpBankDetails.ExpandedNormalImage = Nothing
        Me.gbEmpBankDetails.ExpandedPressedImage = Nothing
        Me.gbEmpBankDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmpBankDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmpBankDetails.HeaderHeight = 25
        Me.gbEmpBankDetails.HeaderMessage = ""
        Me.gbEmpBankDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmpBankDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmpBankDetails.HeightOnCollapse = 0
        Me.gbEmpBankDetails.LeftTextSpace = 0
        Me.gbEmpBankDetails.Location = New System.Drawing.Point(12, 66)
        Me.gbEmpBankDetails.Name = "gbEmpBankDetails"
        Me.gbEmpBankDetails.OpenHeight = 91
        Me.gbEmpBankDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmpBankDetails.ShowBorder = True
        Me.gbEmpBankDetails.ShowCheckBox = False
        Me.gbEmpBankDetails.ShowCollapseButton = False
        Me.gbEmpBankDetails.ShowDefaultBorderColor = True
        Me.gbEmpBankDetails.ShowDownButton = False
        Me.gbEmpBankDetails.ShowHeader = True
        Me.gbEmpBankDetails.Size = New System.Drawing.Size(640, 146)
        Me.gbEmpBankDetails.TabIndex = 0
        Me.gbEmpBankDetails.Temp = 0
        Me.gbEmpBankDetails.Text = "Employee Bank Details"
        Me.gbEmpBankDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMode
        '
        Me.lblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(327, 93)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(98, 15)
        Me.lblMode.TabIndex = 249
        Me.lblMode.Text = "Distribution Mode"
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(431, 90)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(167, 21)
        Me.cboMode.TabIndex = 6
        '
        'lblPerc
        '
        Me.lblPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerc.Location = New System.Drawing.Point(522, 120)
        Me.lblPerc.Name = "lblPerc"
        Me.lblPerc.Size = New System.Drawing.Size(32, 15)
        Me.lblPerc.TabIndex = 243
        Me.lblPerc.Text = "(%)"
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(82, 15)
        Me.lblPeriod.TabIndex = 252
        Me.lblPeriod.Text = "Effec. Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(96, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'objbtnSearchBankBranch
        '
        Me.objbtnSearchBankBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankBranch.BorderSelected = False
        Me.objbtnSearchBankBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankBranch.Location = New System.Drawing.Point(269, 114)
        Me.objbtnSearchBankBranch.Name = "objbtnSearchBankBranch"
        Me.objbtnSearchBankBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankBranch.TabIndex = 246
        '
        'lblSalaryDistrib
        '
        Me.lblSalaryDistrib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryDistrib.Location = New System.Drawing.Point(327, 120)
        Me.lblSalaryDistrib.Name = "lblSalaryDistrib"
        Me.lblSalaryDistrib.Size = New System.Drawing.Size(102, 15)
        Me.lblSalaryDistrib.TabIndex = 242
        Me.lblSalaryDistrib.Text = "Salary Distribution"
        '
        'objbtnSearchBankGroup
        '
        Me.objbtnSearchBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankGroup.BorderSelected = False
        Me.objbtnSearchBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankGroup.Location = New System.Drawing.Point(269, 87)
        Me.objbtnSearchBankGroup.Name = "objbtnSearchBankGroup"
        Me.objbtnSearchBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankGroup.TabIndex = 245
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = False
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 65536
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(431, 117)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(85, 21)
        Me.txtPercentage.TabIndex = 7
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnAddAccType
        '
        Me.objbtnAddAccType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAccType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAccType.BorderSelected = False
        Me.objbtnAddAccType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAccType.Image = CType(resources.GetObject("objbtnAddAccType.Image"), System.Drawing.Image)
        Me.objbtnAddAccType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAccType.Location = New System.Drawing.Point(604, 36)
        Me.objbtnAddAccType.Name = "objbtnAddAccType"
        Me.objbtnAddAccType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAccType.TabIndex = 239
        '
        'txtAccountNo
        '
        Me.txtAccountNo.Flags = 0
        Me.txtAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccountNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAccountNo.Location = New System.Drawing.Point(431, 63)
        Me.txtAccountNo.Name = "txtAccountNo"
        Me.txtAccountNo.Size = New System.Drawing.Size(167, 21)
        Me.txtAccountNo.TabIndex = 5
        '
        'objbtnAddBranch
        '
        Me.objbtnAddBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBranch.BorderSelected = False
        Me.objbtnAddBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBranch.Image = CType(resources.GetObject("objbtnAddBranch.Image"), System.Drawing.Image)
        Me.objbtnAddBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBranch.Location = New System.Drawing.Point(296, 114)
        Me.objbtnAddBranch.Name = "objbtnAddBranch"
        Me.objbtnAddBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBranch.TabIndex = 238
        '
        'objbtnAddBankGroup
        '
        Me.objbtnAddBankGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddBankGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddBankGroup.BorderSelected = False
        Me.objbtnAddBankGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddBankGroup.Image = CType(resources.GetObject("objbtnAddBankGroup.Image"), System.Drawing.Image)
        Me.objbtnAddBankGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddBankGroup.Location = New System.Drawing.Point(296, 87)
        Me.objbtnAddBankGroup.Name = "objbtnAddBankGroup"
        Me.objbtnAddBankGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddBankGroup.TabIndex = 237
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(327, 66)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(98, 15)
        Me.lblAccountNo.TabIndex = 228
        Me.lblAccountNo.Text = "Account No."
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(269, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'lblAccountType
        '
        Me.lblAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountType.Location = New System.Drawing.Point(327, 39)
        Me.lblAccountType.Name = "lblAccountType"
        Me.lblAccountType.Size = New System.Drawing.Size(98, 15)
        Me.lblAccountType.TabIndex = 226
        Me.lblAccountType.Text = "Account Type"
        '
        'lblBank
        '
        Me.lblBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBank.Location = New System.Drawing.Point(8, 120)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(82, 15)
        Me.lblBank.TabIndex = 227
        Me.lblBank.Text = "Branch"
        '
        'cboAccountType
        '
        Me.cboAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountType.FormattingEnabled = True
        Me.cboAccountType.Location = New System.Drawing.Point(431, 36)
        Me.cboAccountType.Name = "cboAccountType"
        Me.cboAccountType.Size = New System.Drawing.Size(167, 21)
        Me.cboAccountType.TabIndex = 4
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(96, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(167, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.Location = New System.Drawing.Point(96, 114)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(167, 21)
        Me.cboBankBranch.TabIndex = 3
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.Location = New System.Drawing.Point(96, 87)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(167, 21)
        Me.cboBankGroup.TabIndex = 2
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(8, 90)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(82, 15)
        Me.lblBankGroup.TabIndex = 220
        Me.lblBankGroup.Text = "Bank Group"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 60)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(82, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 404)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(767, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(555, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(658, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeBank_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 459)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeBank_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Employee Bank"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pnlTransactionHead.ResumeLayout(False)
        Me.gbEmpBankDetails.ResumeLayout(False)
        Me.gbEmpBankDetails.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbEmpBankDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtAccountNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents lblAccountType As System.Windows.Forms.Label
    Friend WithEvents cboAccountType As System.Windows.Forms.ComboBox
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAddAccType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddBankGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSalaryDistrib As System.Windows.Forms.Label
    Friend WithEvents lblPerc As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents pnlTransactionHead As System.Windows.Forms.Panel
    Friend WithEvents lvEmpBankList As eZee.Common.eZeeListView
    Friend WithEvents colhID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBankGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBankBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPerc As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchBankBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBankGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents colhModeID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkCopySlab As System.Windows.Forms.LinkLabel
End Class
