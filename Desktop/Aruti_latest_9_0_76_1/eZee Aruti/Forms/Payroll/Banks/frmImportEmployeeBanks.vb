﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportEmployeeBanks

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportEmployeeBanks"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing 'Sohail (23 Dec 2011)

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

    'Sohail (15 Mar 2018) -- Start
    'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    'Sohail (15 Mar 2018) -- End

#End Region

#Region " From's Events "

    Private Sub frmImportEmployeeBanks_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmImportEmployeeBanks_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " eZee Wizard "

    Private Sub eWizEmpBanks_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eWizEmpBanks.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eWizEmpBanks.Pages.IndexOf(ewpImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportMembership_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub eWizEmpBanks_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eWizEmpBanks.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eWizEmpBanks.Pages.IndexOf(ewpFileSelection)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case eWizEmpBanks.Pages.IndexOf(ewpMapping)
                    If e.NewIndex > e.OldIndex Then
                        'Sohail (25 Apr 2014) -- Start
                        'Enhancement - Employee Bank Details Period Wise.
                        If CInt(cboPeriod.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select effective period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            cboPeriod.Focus()
                            e.Cancel = True
                            Exit Sub
                        ElseIf CInt(cboMode.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select distribution mode."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            cboMode.Focus()
                            e.Cancel = True
                            Exit Sub
                        End If
                        'Sohail (25 Apr 2014) -- End
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                'Sohail (04 Mar 2013) -- Start
                                'TRA - ENHANCEMENT
                                'If CType(ctrl, ComboBox).Text = "" Then
                                If CType(ctrl, ComboBox).Text = "" AndAlso CType(ctrl, ComboBox).Name <> cboSortCode.Name Then
                                    'Sohail (04 Mar 2013) -- End
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    ctrl.Focus() 'Sohail (25 Apr 2014)
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case eWizEmpBanks.Pages.IndexOf(ewpImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : eWizEmpBanks_BeforeSwitchPages ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("displayname", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employeeid", System.Type.GetType("System.Int32"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            'Sohail (23 Dec 2011) -- Start
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))
            'Sohail (23 Dec 2011) -- End
            mdt_ImportData_Others.Columns.Add("sortcode", System.Type.GetType("System.String")) 'Sohail (04 Mar 2013)

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")

            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next

            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow
            Dim intEmpId As Integer = -1

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                If mdicEmpAdded.ContainsKey(dtRow.Item(cboEmployeeCode.Text).ToString.Trim) Then Continue For

                drNewRow = mdt_ImportData_Others.NewRow

                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'drNewRow.Item("displayname") = dtRow.Item(cboMode.Text).ToString.Trim & "  " & dtRow.Item(cboPeriod.Text).ToString.Trim
                drNewRow.Item("displayname") = ""
                'Sohail (25 Apr 2014) -- End
                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                intEmpId = objEmployee.GetEmployeeUnkid("", dtRow.Item(cboEmployeeCode.Text).ToString.Trim)
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                If intEmpId > 0 Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = intEmpId
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                    'S.SANDEEP [04 JUN 2015] -- END

                    drNewRow.Item("displayname") = objEmployee._Firstname & " " & objEmployee._Surname
                End If
                'Sohail (25 Apr 2014) -- End
                drNewRow.Item("employeeid") = intEmpId.ToString
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                'Sohail (23 Dec 2011) -- Start
                drNewRow.Item("objStatus") = ""
                'Sohail (23 Dec 2011) -- End

                'Sohail (04 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                If cboSortCode.Text.Trim <> "" Then
                    drNewRow.Item("sortcode") = dtRow.Item(cboSortCode.Text).ToString.Trim
                Else
                    drNewRow.Item("sortcode") = ""
                End If
                'Sohail (04 Mar 2013) -- End

                mdicEmpAdded.Add(dtRow.Item(cboEmployeeCode.Text).ToString.Trim, intEmpId)

                intEmpId = -1

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "displayname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objdgcolhEmployeeId.DataPropertyName = "employeeid"
                dgData.AutoGenerateColumns = False
                'Sohail (23 Dec 2011) -- Start
                'dgData.DataSource = mdt_ImportData_Others
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
                'Sohail (23 Dec 2011) -- End
            End If

            Call Import_Data()

            'Sohail (24 Nov 2020) -- Start
            'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
            If mdt_ImportData_Others.Select("objStatus = 3 ").Length > 0 Then
                Dim dtValidation As DataTable = New DataView(mdt_ImportData_Others, "objStatus = 3 ", "", DataViewRowState.CurrentRows).ToTable
                dtValidation.Columns("message").SetOrdinal(0)
                dtValidation.Columns("displayname").SetOrdinal(1)
                dtValidation.Columns("message").ColumnName = "Account No."
                dtValidation.Columns("displayname").ColumnName = "Employee Name"
                For i As Integer = 2 To dtValidation.Columns.Count - 1
                    dtValidation.Columns.RemoveAt(2)
                Next
                Dim objValid As New frmCommonValidationList
                If objValid.displayDialog(True, Language.getMessage(mstrModuleName, 22, "Some Account Nos. are already defined for some employees.") & vbCrLf & "  " & Language.getMessage(mstrModuleName, 23, "Do you want to import duplicate account no. for these employees?"), dtValidation) = False Then Exit Try

                Call Import_Data("objStatus = 3 ")
            End If
            'Sohail (24 Nov 2020) -- End

            ezWait.Active = False
            eWizEmpBanks.BackEnabled = False
            eWizEmpBanks.CancelText = "Finish"

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : CreateDataTable ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set the Employee Code to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'If .Item(cbofirstname.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Firstname cannot be blank. Please set the Firstname to import employee banks(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'If .Item(cboSurname.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Surname cannot be blank. Please set the Surname to import employee banks(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'Sohail (25 Apr 2014) -- End
                If .Item(cboAccountNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Account No. cannot be blank. Please set the Account No. to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboAccountType.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Account Type cannot be blank. Please set the Account Type to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboBankGroup.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Bank Group cannot be blank. Please set the Bank Group to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboBranch.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Branch cannot be blank. Please set the Branch to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                If .Item(cboPercentage.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Percentage cannot be blank. Please set the Percentage to import employee bank(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                Return True
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : CheckInvalidData  ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataCombo()
        'Sohail (25 Apr 2014) -- Start
        'Enhancement - Employee Bank Details Period Wise.
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        'Sohail (25 Apr 2014) -- End
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'If TypeOf ctrl Is ComboBox Then
                If TypeOf ctrl Is ComboBox AndAlso ctrl.Name <> cboPeriod.Name AndAlso ctrl.Name <> cboMode.Name Then
                    'Sohail (25 Apr 2014) -- End
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    'Sohail (25 Apr 2014) -- Start
                    'Enhancement - Employee Bank Details Period Wise.
                    'If TypeOf ctrl Is ComboBox Then
                    If TypeOf ctrl Is ComboBox AndAlso ctrl.Name <> cboPeriod.Name AndAlso ctrl.Name <> cboMode.Name Then
                        'Sohail (25 Apr 2014) -- End
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next
            Next

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.GetPaymentBy("Mode")
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Mode")
                .SelectedValue = 0
            End With
            'Sohail (25 Apr 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            'Sohail (25 Apr 2014) -- End
        End Try
    End Sub

    Private Sub Import_Data(Optional ByVal strFilter As String = "")
        'Sohail (24 Nov 2020) - [strFilter]
        Try
            btnFilter.Enabled = False

            'Sohail (24 Nov 2020) -- Start
            'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
            'If mdt_ImportData_Others.Rows.Count <= 0 Then
            If mdt_ImportData_Others.Select(strFilter).Length <= 0 Then
                'Sohail (24 Nov 2020) -- End
                Exit Sub
            End If

            Dim objBankGrp As New clspayrollgroup_master
            Dim objBranch As New clsbankbranch_master
            Dim objAccType As New clsBankAccType
            Dim objEmpBanks As clsEmployeeBanks
            Dim objEmp As New clsEmployee_Master 'Sohail (23 Dec 2011)
            Dim intBankGrpId As Integer = -1
            Dim intBranchId As Integer = -1
            Dim intAccTypeId As Integer = -1

            Dim mdtEmpBank As New DataTable

            'Sohail (24 Nov 2020) -- Start
            'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
            'For Each dtMRow As DataRow In mdt_ImportData_Others.Rows
            For Each dtMRow As DataRow In mdt_ImportData_Others.Select(strFilter)
                'Sohail (24 Nov 2020) -- End
                'Sohail (23 Dec 2011) -- Start 
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtMRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                'Sohail (23 Dec 2011) -- End

                objEmpBanks = New clsEmployeeBanks
                objEmpBanks._Employeeunkid = mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim)
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'Sohail (15 Mar 2018) -- Start
                'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                'objEmpBanks.GetEmployeeBank_Tran(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, True)
                objEmpBanks.GetEmployeeBank_Tran(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, True, True)
                'Sohail (15 Mar 2018) -- End
                'Sohail (07 May 2015) -- End
                mdtEmpBank = objEmpBanks._DataTable.Copy


                Dim dtFilter As DataTable = New DataView(mds_ImportData.Tables(0), "" & cboEmployeeCode.Text & " = '" & dtMRow.Item("employeecode").ToString.Trim & "'", "", DataViewRowState.CurrentRows).ToTable

                If dtFilter.Rows.Count > 0 Then

                    'Sohail (23 Dec 2011) -- Start
                    '*** Employee Exist or Not  ********
                    If objEmp.isExist(dtMRow.Item("employeecode").ToString.Trim) = False Then
                        dtMRow.Item("image") = imgError
                        dtMRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Employee Not Found.")
                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                        dtMRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                    'Sohail (23 Dec 2011) -- End

                    Dim intAssignedEmpId As Integer = -1
                    Dim intPriority As Integer = 1
                    'Sohail (15 Mar 2018) -- Start
                    'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                    Dim decCurrPerc As Decimal = 0
                    Dim intCurrPriority As Integer = 0

                    Dim dsEBank As DataSet = objEmpBanks.IsBankAssigned(CInt(cboPeriod.SelectedValue), mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim))

                    If dsEBank.Tables(0).Rows.Count > 0 Then
                        If CInt(cboMode.SelectedValue) = CInt(enPaymentBy.Percentage) Then
                            decCurrPerc = (From p In dsEBank.Tables(0) Where (CInt(p.Item("modeid")) = CInt(enPaymentBy.Percentage)) Select (CDec(p.Item("percentage")))).Sum()
                            intCurrPriority = (From p In dsEBank.Tables(0) Where (CInt(p.Item("modeid")) = CInt(enPaymentBy.Percentage)) Select (CInt(p.Item("priority")))).Max()
                        Else
                            intCurrPriority = (From p In dsEBank.Tables(0) Where (CInt(p.Item("modeid")) = CInt(enPaymentBy.Value)) Select (CInt(p.Item("priority")))).Max()
                        End If
                        intPriority = intCurrPriority + 1
                    End If
                    'Sohail (15 Mar 2018) -- End
                    For Each dtRow As DataRow In dtFilter.Rows
                        Dim dtEBRow As DataRow = mdtEmpBank.NewRow

                        'Sohail (25 Apr 2014) -- Start
                        'Enhancement - Employee Bank Details Period Wise.
                        'intAssignedEmpId = objEmpBanks.IsBankAssigned(mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim), _
                        '                                              dtRow.Item(cboBankGroup.Text).ToString.Trim, _
                        '                                              dtRow.Item(cboBranch.Text).ToString.Trim, _
                        '                                              dtRow.Item(cboAccountType.Text).ToString.Trim, _
                        '                                              dtRow.Item(cboAccountNo.Text).ToString.Trim)
                        'Sohail (15 Mar 2018) -- Start
                        'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                        'intAssignedEmpId = objEmpBanks.IsBankAssigned(CInt(cboPeriod.SelectedValue), mdicEmpAdded(dtMRow.Item("employeecode").ToString.Trim))
                        'Sohail (15 Mar 2018) -- End
                        'Sohail (25 Apr 2014) -- End

                        'Sohail (15 Mar 2018) -- Start
                        'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                        'If intAssignedEmpId <= 0 Then
                        If 1 = 1 Then
                            'Sohail (15 Mar 2018) -- End

                            'Sohail (15 Mar 2018) -- Start
                            'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                            If dsEBank.Tables(0).Rows.Count > 0 Then
                                If CInt(dsEBank.Tables(0).Rows(0)("modeid")) <> CInt(cboMode.SelectedValue) Then
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Sorry, You can use one Mode only in one Period.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                    dtMRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If

                            End If

                            If objEmpBanks.isExist(-1, dtRow.Item(cboAccountNo.Text).ToString.Trim, -1, CInt(cboPeriod.SelectedValue)) = True Then
                                dtMRow.Item("image") = imgError
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'dtMRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Sorry, This Account no. is already defined.")
                                dtMRow.Item("message") = dtRow.Item(cboAccountNo.Text).ToString.Trim & ": " & Language.getMessage(mstrModuleName, 21, "Sorry, This Account no. is already defined.")
                                'Sohail (24 Nov 2020) -- End
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'dtMRow.Item("objStatus") = 2
                                dtMRow.Item("objStatus") = 3 'This Account no. is already defined
                                'Sohail (24 Nov 2020) -- End
                                objError.Text = CStr(Val(objError.Text) + 1)
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'Continue For
                                If strFilter.Trim = "" OrElse strFilter.Contains("objStatus = 3") = False Then
                                Continue For
                                Else
                                    dtMRow.Item("message") = ""
                                End If
                                'Sohail (24 Nov 2020) -- End
                            End If

                            If mdtEmpBank.Select("accountno = '" & dtRow.Item(cboAccountNo.Text).ToString.Trim & "' AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " ").Length > 0 = True Then
                                dtMRow.Item("image") = imgError
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'dtMRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Sorry, This Account no. is already defined.")
                                dtMRow.Item("message") = dtRow.Item(cboAccountNo.Text).ToString.Trim & ": " & Language.getMessage(mstrModuleName, 21, "Sorry, This Account no. is already defined.")
                                'Sohail (24 Nov 2020) -- End
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'dtMRow.Item("objStatus") = 2
                                dtMRow.Item("objStatus") = 3 'This Account no. is already defined
                                'Sohail (24 Nov 2020) -- End
                                objError.Text = CStr(Val(objError.Text) + 1)
                                'Sohail (24 Nov 2020) -- Start
                                'FDRC Enhancement : # OLD-212 :  Allow importation of Same Bank Account number with warning to user.
                                'Continue For
                                If strFilter.Trim = "" OrElse strFilter.Contains("objStatus = 3") = False Then
                                Continue For
                                Else
                                    dtMRow.Item("message") = ""
                                End If
                                'Sohail (24 Nov 2020) -- End
                            End If
                            'Sohail (15 Mar 2018) -- End

                            '~~~~~~ CHECKING PERCENTAGE > 100 ~~~~~~| START
                            'Sohail (25 Apr 2014) -- Start
                            'Enhancement - Employee Bank Details Period Wise.
                            'Dim dblPercentage As Double = CDbl(dtFilter.Compute("SUM(" & cboPercentage.Text & ")", ""))
                            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage Then
                                Dim dblPercentage As Decimal = (From p In dtFilter Select (CDec(p.Item(cboPercentage.Text)))).Sum()
                                'Sohail (25 Apr 2014) -- End

                                'Sohail (04 Mar 2013) -- Start
                                'TRA - ENHANCEMENT
                                'If dblPercentage > 100 Then
                                'Sohail (15 Mar 2018) -- Start
                                'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                                'If dblPercentage <> 100 Then
                                If (dblPercentage + decCurrPerc) <> 100 Then
                                    'Sohail (15 Mar 2018) -- End
                                    'Sohail (04 Mar 2013) -- End
                                    dtMRow.Item("image") = imgError
                                    dtMRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Total Salary Percentage must be 100.")
                                    dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                    'Sohail (23 Dec 2011) -- Start
                                    dtMRow.Item("objStatus") = 2
                                    'Sohail (23 Dec 2011) -- End
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If

                            End If 'Sohail (25 Apr 2014)

                            '~~~~~~ CHECKING PERCENTAGE > 100 ~~~~~~| END

                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK GROUP START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>
                            If dtRow.Item(cboBankGroup.Text).ToString.Trim.Length > 0 Then
                                intBankGrpId = objBankGrp.GetPayrollGrpUnkId(enPayrollGroupType.Bank, dtRow.Item(cboBankGroup.Text).ToString.Trim)
                                If intBankGrpId <= 0 Then
                                    If Not objBankGrp Is Nothing Then objBankGrp = Nothing
                                    objBankGrp = New clspayrollgroup_master

                                    'objBankGrp._Groupalias = dtRow.Item(cboBankGroup.Text).ToString.Trim
                                    objBankGrp._Groupcode = dtRow.Item(cboBankGroup.Text).ToString.Trim
                                    objBankGrp._Description = ""
                                    objBankGrp._Groupname = dtRow.Item(cboBankGroup.Text).ToString.Trim
                                    objBankGrp._Groupname1 = objBankGrp._Groupname
                                    objBankGrp._Groupname2 = objBankGrp._Groupname
                                    objBankGrp._Grouptype_Id = enPayrollGroupType.Bank
                                    objBankGrp._Isactive = True

                                    If objBankGrp.Insert Then
                                        intBankGrpId = objBankGrp._Groupmasterunkid
                                    Else
                                        intBankGrpId = -1
                                        'Sohail (23 Dec 2011) -- Start
                                        dtMRow.Item("image") = imgError
                                        dtMRow.Item("message") = objBankGrp._Groupcode & "/" & objBankGrp._Groupname & ":" & objBankGrp._Message
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                        'Sohail (23 Dec 2011) -- End
                                    End If
                                End If
                            End If
                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK GROUP END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>

                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK BRANCH START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>
                            If dtRow.Item(cboBranch.Text).ToString.Trim.Length > 0 Then
                                intBranchId = objBranch.GetBankBranchUnkid(dtRow.Item(cboBranch.Text).ToString.Trim, intBankGrpId)
                                If intBranchId <= 0 Then
                                    If Not objBranch Is Nothing Then objBranch = Nothing
                                    objBranch = New clsbankbranch_master

                                    objBranch._Branchcode = dtRow.Item(cboBranch.Text).ToString.Trim
                                    objBranch._Branchname = dtRow.Item(cboBranch.Text).ToString.Trim
                                    objBranch._Bankgroupunkid = intBankGrpId
                                    objBranch._Isactive = True
                                    'Sohail (04 Mar 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    If cboSortCode.Text.Trim <> "" Then
                                        objBranch._Sortcode = dtRow.Item(cboSortCode.Text).ToString.Trim
                                    Else
                                        objBranch._Sortcode = ""
                                    End If
                                    'Sohail (04 Mar 2013) -- End

                                    If objBranch.Insert Then
                                        intBranchId = objBranch._Branchunkid
                                    Else
                                        intBranchId = -1
                                        'Sohail (23 Dec 2011) -- Start
                                        dtMRow.Item("image") = imgError
                                        dtMRow.Item("message") = objBranch._Branchcode & "/" & objBranch._Branchname & ":" & objBranch._Message
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                        'Sohail (23 Dec 2011) -- End
                                    End If
                                End If
                            End If
                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK BRANCH END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>

                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK ACCOUNT TYPE START ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>
                            If dtRow.Item(cboAccountType.Text).ToString.Trim.Length > 0 Then
                                intAccTypeId = objAccType.GetBankAccType(dtRow.Item(cboAccountType.Text).ToString.Trim)
                                If intAccTypeId <= 0 Then
                                    If Not objAccType Is Nothing Then objAccType = Nothing
                                    objAccType = New clsBankAccType

                                    'objAccType._Accounttype_Alias = dtRow.Item(cboAccountType.Text).ToString.Trim
                                    objAccType._Accounttype_Code = dtRow.Item(cboAccountType.Text).ToString.Trim
                                    objAccType._Accounttype_Name = dtRow.Item(cboAccountType.Text).ToString.Trim
                                    objAccType._Accounttype_Name1 = objAccType._Accounttype_Name
                                    objAccType._Accounttype_Name2 = objAccType._Accounttype_Name
                                    objAccType._Description = ""
                                    objAccType._Isactive = True

                                    If objAccType.Insert Then
                                        intAccTypeId = objAccType._Accounttypeunkid
                                    Else
                                        intAccTypeId = -1
                                        'Sohail (23 Dec 2011) -- Start
                                        dtMRow.Item("image") = imgError
                                        dtMRow.Item("message") = objAccType._Accounttype_Code & "/" & objAccType._Accounttype_Name & ":" & objAccType._Message
                                        dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                                        dtMRow.Item("objStatus") = 2
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        Continue For
                                        'Sohail (23 Dec 2011) -- End
                                    End If
                                End If
                            End If
                            '<~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ BANK ACCOUNT TYPE END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>

                            dtEBRow.Item("empbanktranunkid") = -1
                            dtEBRow.Item("employeeunkid") = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
                            dtEBRow.Item("bankgroupunkid") = intBankGrpId
                            dtEBRow.Item("branchunkid") = intBranchId
                            dtEBRow.Item("accounttypeunkid") = intAccTypeId
                            dtEBRow.Item("accountno") = dtRow.Item(cboAccountNo.Text).ToString.Trim
                            'Sohail (25 Apr 2014) -- Start
                            'Enhancement - Employee Bank Details Period Wise.
                            'dtEBRow.Item("percentage") = dtRow.Item(cboPercentage.Text).ToString.Trim
                            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage Then
                                dtEBRow.Item("percentage") = dtRow.Item(cboPercentage.Text).ToString.Trim
                                dtEBRow.Item("modeid") = enPaymentBy.Percentage
                                dtEBRow.Item("amount") = 0
                                'Sohail (15 Mar 2018) -- Start
                                'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
                                'dtEBRow.Item("priority") = 1
                                dtEBRow.Item("priority") = intPriority
                                'Sohail (15 Mar 2018) -- End
                            Else
                                dtEBRow.Item("percentage") = 0
                                dtEBRow.Item("modeid") = enPaymentBy.Value
                                dtEBRow.Item("amount") = dtRow.Item(cboPercentage.Text).ToString.Trim
                                dtEBRow.Item("priority") = intPriority
                            End If
                            intPriority += 1
                            dtEBRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                            'Sohail (25 Apr 2014) -- End
                            'Sohail (21 Apr 2014) -- Start
                            'Enhancement - Salary Distribution by Amount and bank priority.
                            'Sohail (25 Apr 2014) -- Start
                            'Enhancement - Employee Bank Details Period Wise.
                            'dtEBRow.Item("modeid") = enPaymentBy.Percentage
                            'dtEBRow.Item("amount") = 0
                            'dtEBRow.Item("priority") = 1
                            'Sohail (25 Apr 2014) -- End
                            'Sohail (21 Apr 2014) -- End
                            dtEBRow.Item("AUD") = "A"
                            dtEBRow.Item("GUID") = Guid.NewGuid.ToString

                            mdtEmpBank.Rows.Add(dtEBRow)

                            intAssignedEmpId = -1
                        Else
                            dtMRow.Item("image") = imgWarring
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Employee Bank Already Exists.")
                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                            'Sohail (23 Dec 2011) -- Start
                            dtMRow.Item("objStatus") = 0
                            'Sohail (23 Dec 2011) -- End
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        End If
                    Next
                    'If intAssignedEmpId <= 0 Then
                    'Sohail (25 Apr 2014) -- Start
                    'Enhancement - Employee Bank Details Period Wise.
                    'If mdtEmpBank.Rows.Count > 0 Then
                    If mdtEmpBank.Select("AUD <> ''").Length > 0 Then
                        'Sohail (25 Apr 2014) -- End
                        objEmpBanks._Employeeunkid = mdicEmpAdded(dtMRow("employeecode").ToString.Trim)
                        objEmpBanks._DataTable = mdtEmpBank
                        objEmpBanks._Userunkid = User._Object._Userunkid
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objEmpBanks.InsertUpdateDelete_EmpBanks Then
                        If objEmpBanks.InsertUpdateDelete_EmpBanks(ConfigParameter._Object._CurrentDateAndTime) Then
                            'Sohail (21 Aug 2015) -- End
                            If dtMRow.Item("message").ToString.Trim = "" Then
                                dtMRow.Item("image") = imgAccept
                                dtMRow.Item("message") = ""
                                dtMRow.Item("status") = Language.getMessage(mstrModuleName, 15, "Success")
                                'Sohail (23 Dec 2011) -- Start
                                dtMRow.Item("objStatus") = 1
                                'Sohail (23 Dec 2011) -- End
                                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                            End If
                        Else
                            dtMRow.Item("image") = imgError
                            dtMRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Invalid Data")
                            dtMRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Fail")
                            'Sohail (23 Dec 2011) -- Start
                            dtMRow.Item("objStatus") = 2
                            'Sohail (23 Dec 2011) -- End
                            objError.Text = CStr(Val(objError.Text) + 1)
                        End If
                    End If
                    'End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
#Region " Combobox Events "
    Private Sub cboEmployeeCode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboEmployeeCode.Validating, _
                                                                                                                             cboBankGroup.Validating, _
                                                                                                                             cboBranch.Validating, _
                                                                                                                             cboAccountType.Validating, _
                                                                                                                             cboAccountNo.Validating, _
                                                                                                                             cboPercentage.Validating, _
                                                                                                                             cboSortCode.Validating
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedIndex) > -1 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFiledMapping.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboPeriod.Name AndAlso t.Name <> cboMode.Name AndAlso CInt(t.SelectedIndex) = CInt(cbo.SelectedIndex))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, This Field is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployeeCode_Validating", mstrModuleName)
        End Try

    End Sub

    'Sohail (15 Mar 2018) -- Start
    'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
            Else
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Mar 2018) -- End

#End Region
    'Sohail (25 Apr 2014) -- End

    'Sohail (23 Dec 2011) -- Start
#Region " Controls Events "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                dvGriddata.RowFilter = "objStatus = 2"
                Dim dtTable As DataTable = dvGriddata.ToTable

                dtTable.Columns.Remove("image")
                dtTable.Columns.Remove("objstatus")
                dtTable.Columns.Remove("employeeid")

                If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (23 Dec 2011) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eWizEmpBanks.CancelText = Language._Object.getCaption(Me.eWizEmpBanks.Name & "_CancelText" , Me.eWizEmpBanks.CancelText)
			Me.eWizEmpBanks.NextText = Language._Object.getCaption(Me.eWizEmpBanks.Name & "_NextText" , Me.eWizEmpBanks.NextText)
			Me.eWizEmpBanks.BackText = Language._Object.getCaption(Me.eWizEmpBanks.Name & "_BackText" , Me.eWizEmpBanks.BackText)
			Me.eWizEmpBanks.FinishText = Language._Object.getCaption(Me.eWizEmpBanks.Name & "_FinishText" , Me.eWizEmpBanks.FinishText)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
            Me.lblMembershipNo.Text = Language._Object.getCaption(Me.lblMembershipNo.Name, Me.lblMembershipNo.Text)
            Me.lblBankBranch.Text = Language._Object.getCaption(Me.lblBankBranch.Name, Me.lblBankBranch.Text)
            Me.lblBanks.Text = Language._Object.getCaption(Me.lblBanks.Name, Me.lblBanks.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.lblSortCode.Text = Language._Object.getCaption(Me.lblSortCode.Name, Me.lblSortCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set the Employee Code to import employee bank(s).")
            Language.setMessage(mstrModuleName, 6, "Account No. cannot be blank. Please set the Account No. to import employee bank(s).")
            Language.setMessage(mstrModuleName, 7, "Account Type cannot be blank. Please set the Account Type to import employee bank(s).")
            Language.setMessage(mstrModuleName, 8, "Bank Group cannot be blank. Please set the Bank Group to import employee bank(s).")
            Language.setMessage(mstrModuleName, 9, "Branch cannot be blank. Please set the Branch to import employee bank(s).")
            Language.setMessage(mstrModuleName, 10, "Percentage cannot be blank. Please set the Percentage to import employee bank(s).")
            Language.setMessage(mstrModuleName, 11, "Employee Bank Already Exists.")
            Language.setMessage(mstrModuleName, 12, "Fail")
            Language.setMessage(mstrModuleName, 13, "Invalid Data")
            Language.setMessage(mstrModuleName, 14, "Total Salary Percentage must be 100.")
            Language.setMessage(mstrModuleName, 15, "Success")
            Language.setMessage(mstrModuleName, 16, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 17, "Please select effective period.")
            Language.setMessage(mstrModuleName, 18, "Please select distribution mode.")
            Language.setMessage(mstrModuleName, 19, "Sorry, This Field is already mapped.")
	    Language.setMessage(mstrModuleName, 20, "Sorry, You can use one Mode only in one Period.")
	    Language.setMessage(mstrModuleName, 21, "Sorry, This Account no. is already defined.")
			Language.setMessage(mstrModuleName, 22, "Some Account Nos. are already defined for some employees.")
			Language.setMessage(mstrModuleName, 23, "Do you want to import duplicate account no. for these employees?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class