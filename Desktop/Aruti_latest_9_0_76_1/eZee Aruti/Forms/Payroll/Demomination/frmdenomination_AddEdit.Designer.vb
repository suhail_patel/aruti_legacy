﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmdenomination_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmdenomination_AddEdit))
        Me.PnlCurrencydenomation = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbCurrencyDenomination = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.NumericTextBox
        Me.PnlCurrencydenomation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbCurrencyDenomination.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlCurrencydenomation
        '
        Me.PnlCurrencydenomation.Controls.Add(Me.objFooter)
        Me.PnlCurrencydenomation.Controls.Add(Me.gbCurrencyDenomination)
        Me.PnlCurrencydenomation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlCurrencydenomation.Location = New System.Drawing.Point(0, 0)
        Me.PnlCurrencydenomation.Name = "PnlCurrencydenomation"
        Me.PnlCurrencydenomation.Size = New System.Drawing.Size(280, 186)
        Me.PnlCurrencydenomation.TabIndex = 3
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 136)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(280, 50)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(176, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(76, 8)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbCurrencyDenomination
        '
        Me.gbCurrencyDenomination.BorderColor = System.Drawing.Color.Black
        Me.gbCurrencyDenomination.Checked = False
        Me.gbCurrencyDenomination.CollapseAllExceptThis = False
        Me.gbCurrencyDenomination.CollapsedHoverImage = Nothing
        Me.gbCurrencyDenomination.CollapsedNormalImage = Nothing
        Me.gbCurrencyDenomination.CollapsedPressedImage = Nothing
        Me.gbCurrencyDenomination.CollapseOnLoad = False
        Me.gbCurrencyDenomination.Controls.Add(Me.txtName)
        Me.gbCurrencyDenomination.Controls.Add(Me.lblName)
        Me.gbCurrencyDenomination.Controls.Add(Me.txtCode)
        Me.gbCurrencyDenomination.Controls.Add(Me.lblCode)
        Me.gbCurrencyDenomination.Controls.Add(Me.cboCurrency)
        Me.gbCurrencyDenomination.Controls.Add(Me.lblCurrency)
        Me.gbCurrencyDenomination.ExpandedHoverImage = Nothing
        Me.gbCurrencyDenomination.ExpandedNormalImage = Nothing
        Me.gbCurrencyDenomination.ExpandedPressedImage = Nothing
        Me.gbCurrencyDenomination.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCurrencyDenomination.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCurrencyDenomination.HeaderHeight = 25
        Me.gbCurrencyDenomination.HeightOnCollapse = 0
        Me.gbCurrencyDenomination.LeftTextSpace = 0
        Me.gbCurrencyDenomination.Location = New System.Drawing.Point(9, 11)
        Me.gbCurrencyDenomination.Name = "gbCurrencyDenomination"
        Me.gbCurrencyDenomination.OpenHeight = 300
        Me.gbCurrencyDenomination.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCurrencyDenomination.ShowBorder = True
        Me.gbCurrencyDenomination.ShowCheckBox = False
        Me.gbCurrencyDenomination.ShowCollapseButton = False
        Me.gbCurrencyDenomination.ShowDefaultBorderColor = True
        Me.gbCurrencyDenomination.ShowDownButton = False
        Me.gbCurrencyDenomination.ShowHeader = True
        Me.gbCurrencyDenomination.Size = New System.Drawing.Size(262, 118)
        Me.gbCurrencyDenomination.TabIndex = 1
        Me.gbCurrencyDenomination.Temp = 0
        Me.gbCurrencyDenomination.Text = "Currency Denomination"
        Me.gbCurrencyDenomination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 93)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 13)
        Me.lblName.TabIndex = 13
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(87, 60)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(157, 21)
        Me.txtCode.TabIndex = 12
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 64)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(68, 13)
        Me.lblCode.TabIndex = 11
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(87, 32)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(157, 21)
        Me.cboCurrency.TabIndex = 10
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 36)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(68, 13)
        Me.lblCurrency.TabIndex = 1
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.AllowNegative = False
        Me.txtName.DigitsInGroup = 0
        Me.txtName.Flags = 65536
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(87, 88)
        Me.txtName.MaxDecimalPlaces = 6
        Me.txtName.MaxWholeDigits = 21
        Me.txtName.Name = "txtName"
        Me.txtName.Prefix = ""
        Me.txtName.RangeMax = 1.7976931348623157E+308
        Me.txtName.RangeMin = -1.7976931348623157E+308
        Me.txtName.Size = New System.Drawing.Size(157, 21)
        Me.txtName.TabIndex = 16
        Me.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmdenomination_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 186)
        Me.Controls.Add(Me.PnlCurrencydenomation)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmdenomination_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Currency Denomination"
        Me.PnlCurrencydenomation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbCurrencyDenomination.ResumeLayout(False)
        Me.gbCurrencyDenomination.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlCurrencydenomation As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbCurrencyDenomination As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.NumericTextBox
End Class
