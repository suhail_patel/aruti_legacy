﻿Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmdenominationList

    Private objDenomination As clsDenomination
    'Hemant (19 Mar 2019) -- Start
    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
    'Private ReadOnly mstrModuleName As String = "frmDenomationList"
    Private ReadOnly mstrModuleName As String = "frmDenominationList"
    'Hemant (19 Mar 2019) -- End

#Region " Private Methods "

    Private Sub fillList()
        Dim dsDenomination As New DataSet
        Try

            If User._Object.Privilege._AllowToViewCurrencyDenominationList = True Then   'Pinkal (09-Jul-2012) -- Start

                'S.SANDEEP [ 23 JUNE 2011 ] -- START
                'ISSUE : EXCHANGE RATE CHANGES
                'dsDenomination = objDenomination.GetList("List")
                dsDenomination = objDenomination.GetList("List", True)
                'S.SANDEEP [ 23 JUNE 2011 ] -- END 



                Dim lvItem As ListViewItem


                'Sandeep [ 29 NOV 2010 ] -- Start
                Dim objExRate As New clsExchangeRate
                objExRate._ExchangeRateunkid = 1
                'Sandeep [ 29 NOV 2010 ] -- End 
                Dim strFmtCurrency As String = objExRate._fmtCurrency 'Sohail (10 Jul 2014)

                lvdenomation.Items.Clear()
                For Each drRow As DataRow In dsDenomination.Tables(0).Rows
                    lvItem = New ListViewItem
                    'Sohail (10 Jul 2014) -- Start
                    'Enhancement - Custom Language.
                    'lvItem.Text = drRow("currency_sign").ToString
                    objExRate = New clsExchangeRate
                    objExRate._ExchangeRateunkid = CInt(drRow("currency_sign"))
                    lvItem.Text = objExRate._Currency_Sign
                    'Sohail (10 Jul 2014) -- End

                    lvItem.Tag = drRow("denomunkid")
                    lvItem.SubItems.Add(drRow("denom_code").ToString)
                    'Sandeep [ 29 NOV 2010 ] -- Start
                    'lvItem.SubItems.Add(drRow("denomination").ToString)
                    'Sohail (10 Jul 2014) -- Start
                    'Enhancement - Custom Language.
                    'lvItem.SubItems.Add(Format(CDec(drRow("denomination")), objExRate._fmtCurrency)) 'Sohail (11 May 2011)
                    lvItem.SubItems.Add(Format(CDec(drRow("denomination")), strFmtCurrency))
                    'Sohail (10 Jul 2014) -- End
                    'Sandeep [ 29 NOV 2010 ] -- End 
                    lvdenomation.Items.Add(lvItem)
                Next

                If lvdenomation.Items.Count > 16 Then
                    colhCurrencydenoname.Width = 320 - 18
                Else
                    colhCurrencydenoname.Width = 320
                End If


                'Sandeep [ 29 NOV 2010 ] -- Start
                objExRate = Nothing
                'Sandeep [ 29 NOV 2010 ] -- End 


                'Call setAlternateColor(lvAirline)

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsDenomination.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddDenomination
            btnEdit.Enabled = User._Object.Privilege._EditDenomination
            btnDelete.Enabled = User._Object.Privilege._DeleteDenomination
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDenomination_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvdenomation.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmDenomination_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmDenomination_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDenomination = New clsDenomination
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.

            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()
            Call SetVisibility()
            Call fillList()

            If lvdenomation.Items.Count > 0 Then lvdenomation.Items(0).Selected = True
            lvdenomation.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAirlineList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDenomination_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'objAirline.Dispose()
        objDenomination = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDenomination.SetMessages()
            objfrm._Other_ModuleNames = "clsDenomination"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 



#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmdenomination_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvdenomation.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditDenomination = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvdenomation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Denomination from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvdenomation.Select()
            Exit Sub
        End If
        Dim frm As New frmdenomination_AddEdit
        Try

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvdenomation.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(CInt(lvdenomation.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvdenomation.Items(intSelectedIndex).Selected = True
            lvdenomation.EnsureVisible(intSelectedIndex)
            lvdenomation.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvdenomation.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Denomination from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvdenomation.Select()
            Exit Sub
        End If
        'Sohail (28 Dec 2010) -- Start
        If objDenomination.isUsed(CInt(lvdenomation.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Denomination. Reason : This Denomination is in use."), enMsgBoxStyle.Information) '?2
            lvdenomation.Select()
            Exit Sub
        End If
        'Sohail (28 Dec 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvdenomation.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Denomination?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objDenomination.Delete(CInt(lvdenomation.SelectedItems(0).Tag))
                lvdenomation.SelectedItems(0).Remove()

                If lvdenomation.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvdenomation.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvdenomation.Items.Count - 1
                    lvdenomation.Items(intSelectedIndex).Selected = True
                    lvdenomation.EnsureVisible(intSelectedIndex)
                ElseIf lvdenomation.Items.Count <> 0 Then
                    lvdenomation.Items(intSelectedIndex).Selected = True
                    lvdenomation.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvdenomation.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhCurrencydenocode.Text = Language._Object.getCaption(CStr(Me.colhCurrencydenocode.Tag), Me.colhCurrencydenocode.Text)
            Me.colhCurrencydenoname.Text = Language._Object.getCaption(CStr(Me.colhCurrencydenoname.Tag), Me.colhCurrencydenoname.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCurrency.Text = Language._Object.getCaption(CStr(Me.colhCurrency.Tag), Me.colhCurrency.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Denomination from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Denomination?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Denomination. Reason : This Denomination is in use.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class