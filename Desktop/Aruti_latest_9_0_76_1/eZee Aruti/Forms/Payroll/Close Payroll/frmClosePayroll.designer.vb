﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClosePayroll
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClosePayroll))
        Me.pnlClosepayroll = New System.Windows.Forms.Panel
        Me.gbClosePeriodInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblNoteNo2 = New System.Windows.Forms.Label
        Me.lblNote2 = New System.Windows.Forms.Label
        Me.lblNoteNo1 = New System.Windows.Forms.Label
        Me.lblNote1 = New System.Windows.Forms.Label
        Me.lblNote = New System.Windows.Forms.Label
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lnEmployeeList = New eZee.Common.eZeeLine
        Me.lvEmployee = New System.Windows.Forms.ListView
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.lblPayPeriodOpen = New System.Windows.Forms.Label
        Me.cboPayPeriodOpen = New System.Windows.Forms.ComboBox
        Me.cboPayYearClose = New System.Windows.Forms.ComboBox
        Me.lblPayYearClose = New System.Windows.Forms.Label
        Me.lblPayPeriodClose = New System.Windows.Forms.Label
        Me.cboPayPeriodClose = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnCloseYear = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClosePeriod = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbgwClosePayroll = New System.ComponentModel.BackgroundWorker
        Me.pnlClosepayroll.SuspendLayout()
        Me.gbClosePeriodInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlClosepayroll
        '
        Me.pnlClosepayroll.Controls.Add(Me.gbClosePeriodInfo)
        Me.pnlClosepayroll.Controls.Add(Me.objFooter)
        Me.pnlClosepayroll.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlClosepayroll.Location = New System.Drawing.Point(0, 0)
        Me.pnlClosepayroll.Name = "pnlClosepayroll"
        Me.pnlClosepayroll.Size = New System.Drawing.Size(745, 391)
        Me.pnlClosepayroll.TabIndex = 0
        '
        'gbClosePeriodInfo
        '
        Me.gbClosePeriodInfo.BorderColor = System.Drawing.Color.Black
        Me.gbClosePeriodInfo.Checked = False
        Me.gbClosePeriodInfo.CollapseAllExceptThis = False
        Me.gbClosePeriodInfo.CollapsedHoverImage = Nothing
        Me.gbClosePeriodInfo.CollapsedNormalImage = Nothing
        Me.gbClosePeriodInfo.CollapsedPressedImage = Nothing
        Me.gbClosePeriodInfo.CollapseOnLoad = False
        Me.gbClosePeriodInfo.Controls.Add(Me.lblNoteNo2)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblNote2)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblNoteNo1)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblNote1)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblNote)
        Me.gbClosePeriodInfo.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbClosePeriodInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbClosePeriodInfo.Controls.Add(Me.lnEmployeeList)
        Me.gbClosePeriodInfo.Controls.Add(Me.lvEmployee)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblPayPeriodOpen)
        Me.gbClosePeriodInfo.Controls.Add(Me.cboPayPeriodOpen)
        Me.gbClosePeriodInfo.Controls.Add(Me.cboPayYearClose)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblPayYearClose)
        Me.gbClosePeriodInfo.Controls.Add(Me.lblPayPeriodClose)
        Me.gbClosePeriodInfo.Controls.Add(Me.cboPayPeriodClose)
        Me.gbClosePeriodInfo.ExpandedHoverImage = Nothing
        Me.gbClosePeriodInfo.ExpandedNormalImage = Nothing
        Me.gbClosePeriodInfo.ExpandedPressedImage = Nothing
        Me.gbClosePeriodInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClosePeriodInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbClosePeriodInfo.HeaderHeight = 25
        Me.gbClosePeriodInfo.HeaderMessage = ""
        Me.gbClosePeriodInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbClosePeriodInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbClosePeriodInfo.HeightOnCollapse = 0
        Me.gbClosePeriodInfo.LeftTextSpace = 0
        Me.gbClosePeriodInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbClosePeriodInfo.Name = "gbClosePeriodInfo"
        Me.gbClosePeriodInfo.OpenHeight = 300
        Me.gbClosePeriodInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClosePeriodInfo.ShowBorder = True
        Me.gbClosePeriodInfo.ShowCheckBox = False
        Me.gbClosePeriodInfo.ShowCollapseButton = False
        Me.gbClosePeriodInfo.ShowDefaultBorderColor = True
        Me.gbClosePeriodInfo.ShowDownButton = False
        Me.gbClosePeriodInfo.ShowHeader = True
        Me.gbClosePeriodInfo.Size = New System.Drawing.Size(721, 318)
        Me.gbClosePeriodInfo.TabIndex = 168
        Me.gbClosePeriodInfo.Temp = 0
        Me.gbClosePeriodInfo.Text = "Period to be closed"
        Me.gbClosePeriodInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNoteNo2
        '
        Me.lblNoteNo2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoteNo2.ForeColor = System.Drawing.Color.Red
        Me.lblNoteNo2.Location = New System.Drawing.Point(8, 123)
        Me.lblNoteNo2.Name = "lblNoteNo2"
        Me.lblNoteNo2.Size = New System.Drawing.Size(28, 20)
        Me.lblNoteNo2.TabIndex = 184
        Me.lblNoteNo2.Text = "2."
        '
        'lblNote2
        '
        Me.lblNote2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote2.ForeColor = System.Drawing.Color.Red
        Me.lblNote2.Location = New System.Drawing.Point(42, 123)
        Me.lblNote2.Name = "lblNote2"
        Me.lblNote2.Size = New System.Drawing.Size(414, 79)
        Me.lblNote2.TabIndex = 183
        Me.lblNote2.Text = resources.GetString("lblNote2.Text")
        '
        'lblNoteNo1
        '
        Me.lblNoteNo1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoteNo1.ForeColor = System.Drawing.Color.Red
        Me.lblNoteNo1.Location = New System.Drawing.Point(8, 54)
        Me.lblNoteNo1.Name = "lblNoteNo1"
        Me.lblNoteNo1.Size = New System.Drawing.Size(28, 20)
        Me.lblNoteNo1.TabIndex = 182
        Me.lblNoteNo1.Text = "1."
        '
        'lblNote1
        '
        Me.lblNote1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote1.ForeColor = System.Drawing.Color.Red
        Me.lblNote1.Location = New System.Drawing.Point(42, 54)
        Me.lblNote1.Name = "lblNote1"
        Me.lblNote1.Size = New System.Drawing.Size(414, 65)
        Me.lblNote1.TabIndex = 181
        Me.lblNote1.Text = "It is recommended that you kindly check payslip report, all other payroll reports" & _
            " and all statutary reports in order to make sure that all payroll reports are sh" & _
            "owing correct data."
        '
        'lblNote
        '
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.ForeColor = System.Drawing.Color.Red
        Me.lblNote.Location = New System.Drawing.Point(8, 32)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(258, 18)
        Me.lblNote.TabIndex = 180
        Me.lblNote.Text = "Note:"
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Checked = True
        Me.chkCopyPreviousEDSlab.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(45, 291)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(258, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 178
        Me.chkCopyPreviousEDSlab.Text = "Copy Closing period ED Slab to Opening period"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(462, 27)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(10, 288)
        Me.EZeeStraightLine1.TabIndex = 176
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lnEmployeeList
        '
        Me.lnEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeList.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeList.Location = New System.Drawing.Point(486, 36)
        Me.lnEmployeeList.Name = "lnEmployeeList"
        Me.lnEmployeeList.Size = New System.Drawing.Size(221, 16)
        Me.lnEmployeeList.TabIndex = 175
        Me.lnEmployeeList.Text = "Unprocessed Employee List"
        '
        'lvEmployee
        '
        Me.lvEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName})
        Me.lvEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmployee.FullRowSelect = True
        Me.lvEmployee.GridLines = True
        Me.lvEmployee.Location = New System.Drawing.Point(489, 54)
        Me.lvEmployee.Name = "lvEmployee"
        Me.lvEmployee.Size = New System.Drawing.Size(218, 249)
        Me.lvEmployee.TabIndex = 174
        Me.lvEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEmployee.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 150
        '
        'lblPayPeriodOpen
        '
        Me.lblPayPeriodOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriodOpen.Location = New System.Drawing.Point(45, 261)
        Me.lblPayPeriodOpen.Name = "lblPayPeriodOpen"
        Me.lblPayPeriodOpen.Size = New System.Drawing.Size(114, 16)
        Me.lblPayPeriodOpen.TabIndex = 173
        Me.lblPayPeriodOpen.Text = "Period to be Opened"
        Me.lblPayPeriodOpen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriodOpen
        '
        Me.cboPayPeriodOpen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriodOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriodOpen.FormattingEnabled = True
        Me.cboPayPeriodOpen.Location = New System.Drawing.Point(162, 259)
        Me.cboPayPeriodOpen.Name = "cboPayPeriodOpen"
        Me.cboPayPeriodOpen.Size = New System.Drawing.Size(131, 21)
        Me.cboPayPeriodOpen.TabIndex = 172
        '
        'cboPayYearClose
        '
        Me.cboPayYearClose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYearClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYearClose.FormattingEnabled = True
        Me.cboPayYearClose.Location = New System.Drawing.Point(162, 205)
        Me.cboPayYearClose.Name = "cboPayYearClose"
        Me.cboPayYearClose.Size = New System.Drawing.Size(131, 21)
        Me.cboPayYearClose.TabIndex = 168
        '
        'lblPayYearClose
        '
        Me.lblPayYearClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYearClose.Location = New System.Drawing.Point(45, 207)
        Me.lblPayYearClose.Name = "lblPayYearClose"
        Me.lblPayYearClose.Size = New System.Drawing.Size(111, 16)
        Me.lblPayYearClose.TabIndex = 170
        Me.lblPayYearClose.Text = "Pay Year"
        Me.lblPayYearClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayPeriodClose
        '
        Me.lblPayPeriodClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriodClose.Location = New System.Drawing.Point(45, 234)
        Me.lblPayPeriodClose.Name = "lblPayPeriodClose"
        Me.lblPayPeriodClose.Size = New System.Drawing.Size(111, 16)
        Me.lblPayPeriodClose.TabIndex = 171
        Me.lblPayPeriodClose.Text = "Period to be Closed"
        Me.lblPayPeriodClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriodClose
        '
        Me.cboPayPeriodClose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriodClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriodClose.FormattingEnabled = True
        Me.cboPayPeriodClose.Location = New System.Drawing.Point(162, 232)
        Me.cboPayPeriodClose.Name = "cboPayPeriodClose"
        Me.cboPayPeriodClose.Size = New System.Drawing.Size(131, 21)
        Me.cboPayPeriodClose.TabIndex = 169
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.btnCloseYear)
        Me.objFooter.Controls.Add(Me.btnClosePeriod)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 336)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(745, 55)
        Me.objFooter.TabIndex = 4
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(143, 13)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(370, 30)
        Me.objlblProgress.TabIndex = 15
        '
        'btnCloseYear
        '
        Me.btnCloseYear.BackColor = System.Drawing.Color.White
        Me.btnCloseYear.BackgroundImage = CType(resources.GetObject("btnCloseYear.BackgroundImage"), System.Drawing.Image)
        Me.btnCloseYear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCloseYear.BorderColor = System.Drawing.Color.Empty
        Me.btnCloseYear.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCloseYear.FlatAppearance.BorderSize = 0
        Me.btnCloseYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCloseYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloseYear.ForeColor = System.Drawing.Color.Black
        Me.btnCloseYear.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCloseYear.GradientForeColor = System.Drawing.Color.Black
        Me.btnCloseYear.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCloseYear.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCloseYear.Location = New System.Drawing.Point(12, 13)
        Me.btnCloseYear.Name = "btnCloseYear"
        Me.btnCloseYear.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCloseYear.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCloseYear.Size = New System.Drawing.Size(122, 30)
        Me.btnCloseYear.TabIndex = 2
        Me.btnCloseYear.Text = "&Close Pay Year"
        Me.btnCloseYear.UseVisualStyleBackColor = True
        '
        'btnClosePeriod
        '
        Me.btnClosePeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClosePeriod.BackColor = System.Drawing.Color.White
        Me.btnClosePeriod.BackgroundImage = CType(resources.GetObject("btnClosePeriod.BackgroundImage"), System.Drawing.Image)
        Me.btnClosePeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClosePeriod.BorderColor = System.Drawing.Color.Empty
        Me.btnClosePeriod.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClosePeriod.FlatAppearance.BorderSize = 0
        Me.btnClosePeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClosePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClosePeriod.ForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClosePeriod.GradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePeriod.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.Location = New System.Drawing.Point(521, 13)
        Me.btnClosePeriod.Name = "btnClosePeriod"
        Me.btnClosePeriod.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePeriod.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.Size = New System.Drawing.Size(122, 30)
        Me.btnClosePeriod.TabIndex = 0
        Me.btnClosePeriod.Text = "&Close Pay Period"
        Me.btnClosePeriod.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(649, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(84, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbgwClosePayroll
        '
        Me.objbgwClosePayroll.WorkerReportsProgress = True
        Me.objbgwClosePayroll.WorkerSupportsCancellation = True
        '
        'frmClosePayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(745, 391)
        Me.Controls.Add(Me.pnlClosepayroll)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClosePayroll"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Close Period/Year"
        Me.pnlClosepayroll.ResumeLayout(False)
        Me.gbClosePeriodInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlClosepayroll As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClosePeriod As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnCloseYear As eZee.Common.eZeeLightButton
    Friend WithEvents gbClosePeriodInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPayPeriodOpen As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriodOpen As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayYearClose As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayYearClose As System.Windows.Forms.Label
    Friend WithEvents lblPayPeriodClose As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriodClose As System.Windows.Forms.ComboBox
    Friend WithEvents lnEmployeeList As eZee.Common.eZeeLine
    Friend WithEvents lvEmployee As System.Windows.Forms.ListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents lblNote1 As System.Windows.Forms.Label
    Friend WithEvents lblNoteNo1 As System.Windows.Forms.Label
    Friend WithEvents lblNoteNo2 As System.Windows.Forms.Label
    Friend WithEvents lblNote2 As System.Windows.Forms.Label
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents objbgwClosePayroll As System.ComponentModel.BackgroundWorker
End Class
