﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBatchEntry

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBatchEntry"
    Private mintEmployeeUnkID As Integer = -1

    Private mblnCancel As Boolean = True

    Private m_DataSource As DataTable
    Dim m_Dataview As New DataView

    Private mblnIsOverWrite As Boolean = False
    Private menAction As enAction = enAction.ADD_ONE 'Sohail (03 Nov 2010)
    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime

    Private mstrAdvanceFilter As String = "" 'Sohail (31 Aug 2012)
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    'Sohail (18 Jul 2017) -- End
#End Region

#Region " Property "
    Public Property DataSource() As DataTable
        Get
            Return m_DataSource
        End Get
        Set(ByVal value As DataTable)
            m_DataSource = value
        End Set
    End Property

    Public Property _IsOverWrite() As Boolean
        Get
            Return mblnIsOverWrite
        End Get
        Set(ByVal value As Boolean)
            mblnIsOverWrite = value
        End Set
    End Property
#End Region

#Region " Display Dialogue "
    'Sohail (11 Sep 2010) -- Start
    'Public Function DisplayDialog(ByVal intEmpUnkID As Integer) As Boolean
    Public Function DisplayDialog(ByVal eAction As enAction) As Boolean 'Sohail (03 Nov 2010)
        'Public Function DisplayDialog() As Boolean
        'Sohail (11 Sep 2010) -- End
        Try
            'mintEmployeeUnkID = intEmpUnkID
            menAction = eAction 'Sohail (03 Nov 2010)

            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        cboBatch.BackColor = GUI.ColorOptional
        With dgvBatch
            .Columns(colhTrnHead.Index).SortMode = DataGridViewColumnSortMode.Programmatic
            .Columns(colhAmount.Index).SortMode = DataGridViewColumnSortMode.Programmatic
            .Columns(objcolhCurrency.Index).SortMode = DataGridViewColumnSortMode.Programmatic
            .Columns(objcolhVendor.Index).SortMode = DataGridViewColumnSortMode.Programmatic

            .Columns(colhAmount.Index).DefaultCellStyle.BackColor = GUI.ColorComp
            .Columns(objcolhCurrency.Index).DefaultCellStyle.BackColor = GUI.ColorComp
            .Columns(objcolhVendor.Index).DefaultCellStyle.BackColor = GUI.ColorOptional
            'Sohail (01 Dec 2010) -- Start
            .Columns(colhAmount.Index).DefaultCellStyle.Format = GUI.fmtCurrency
            'Sohail (01 Dec 2010) -- End
        End With

        'Sohail (11 Sep 2010) -- Start
        cboEmployee.BackColor = GUI.ColorOptional
        'Sohail (30 Nov 2013) -- Start
        'Enhancement - Oman
        'cboDepartment.BackColor = GUI.ColorOptional
        'cboGrade.BackColor = GUI.ColorOptional
        'cboSections.BackColor = GUI.ColorOptional
        'cboClass.BackColor = GUI.ColorOptional
        'cboCostCenter.BackColor = GUI.ColorOptional
        'cboJob.BackColor = GUI.ColorOptional
        'cboPayPoint.BackColor = GUI.ColorOptional
        'cboUnit.BackColor = GUI.ColorOptional
        'cboGender.BackColor = GUI.ColorOptional 'Sohail (31 Aug 2012)
        'Sohail (30 Nov 2013) -- End
        'Sohail (11 Sep 2010) -- End
        cboPeriod.BackColor = GUI.ColorComp 'Sohail (13 Jan 2012)

    End Sub

    Private Sub FillCombo()
        Dim objBatch As New clsBatchTransaction
        Dim dsCombo As DataSet = Nothing

        Dim objExchange As New clsExchangeRate
        Dim objVendor As New clsvendor_master
        Dim dsList As DataSet
        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Mar 2012)
        'Sohail (30 Nov 2013) -- Start
        'Enhancement - Oman
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        'Dim objMaster As New clsMasterData 'Sohail (31 Aug 2012)
        'Sohail (30 Nov 2013) -- End
        Dim objPeriod As New clscommom_period_Tran 'Sohail (13 Jan 2012)

        Try
            dsCombo = objBatch.getComboList("Batch", True)
            With cboBatch
                .ValueMember = "batchtransactionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Batch")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsList = objExchange.getComboList("Currency", True)
            With objcolhCurrency
                .DataSource = dsList.Tables("Currency")
                .ValueMember = "exchangerateunkid"
                .DisplayMember = "currency_name"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            dsList = objVendor.getComborList("Vendor", True)
            With objcolhVendor
                .DataSource = dsList.Tables("Vendor")
                .ValueMember = "vendorunkid"
                .DisplayMember = "companyname"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            'Sohail (03 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            ''Sohail (11 Sep 2010) -- Start
            ''Sohail (06 Jan 2012) -- Start
            ''TRA - ENHANCEMENT
            ''dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            ''Sohail (06 Jan 2012) -- End
            'With cboEmployee
            '    .BeginUpdate()
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsList.Tables("EmployeeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With
            Call FillCombo_Employee()
            'Sohail (03 Mar 2012) -- End

            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            'dsList = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .BeginUpdate()
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("DepartmentList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .BeginUpdate()
            '    .ValueMember = "gradeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("GradeList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .BeginUpdate()
            '    .ValueMember = "sectionunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("SectionList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objClass.getComboList("ClassList", True)
            'With cboClass
            '    .BeginUpdate()
            '    .ValueMember = "classesunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ClassList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .BeginUpdate()
            '    .ValueMember = "CostCenterunkid"
            '    .DisplayMember = "CostCentername"
            '    .DataSource = dsList.Tables("CostCenterList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objJob.getComboList("JobList", True)
            'With cboJob
            '    .BeginUpdate()
            '    .ValueMember = "jobunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("JobList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .BeginUpdate()
            '    .ValueMember = "paypointunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("PayPointList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            'dsList = objUnit.getComboList("UnitList", True)
            'With cboUnit
            '    .BeginUpdate()
            '    .ValueMember = "unitunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("UnitList")
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            '    .EndUpdate()
            'End With

            ''Sohail (31 Aug 2012) -- Start
            ''TRA - ENHANCEMENT
            'dsCombo = objMaster.getGenderList("Gender", True)
            'With cboGender
            '    .ValueMember = "id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("Gender")
            'End With
            ''Sohail (31 Aug 2012) -- End
            'Sohail (30 Nov 2013) -- End
            'Sohail (11 Sep 2010) -- End

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .EndUpdate()
            End With
            'Sohail (13 Jan 2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBatch = Nothing
            objExchange = Nothing
            objVendor = Nothing
            'objEmployee = Nothing 'Sohail (03 Mar 2012)
            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'objMaster = Nothing 'Sohail (31 Aug 2012)
            'Sohail (30 Nov 2013) -- End
        End Try
    End Sub

    'Sohail (03 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Dim strFilter As String = "" 'Sohail (25 May 2012)
        Try

            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
            'Sohail (06 Jan 2016) -- Start
            'Issue - Newly hired employees were not coming if Include Inactive Employee parameter passes as TRUE.
            'If chkShowNewlyHiredEmployees.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= @startdate "
            If chkShowNewlyHiredEmployees.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= '" & eZeeDate.convertDate(mdtPayPeriodStartDate) & "' "
            'Sohail (06 Jan 2016) -- End
            'Sohail (31 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrAdvanceFilter <> "" Then
                If strFilter <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If
            'Sohail (31 Aug 2012) -- End
            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , CInt(cboGender.SelectedValue), , , strFilter) 'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList",  True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , 0, , , strFilter)

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "EmployeeList", True, , , , , , , , , , , , , 0, , strFilter)
            'Anjan [10 June 2015] -- End

            'Sohail (30 Nov 2013) -- End
            'Sohail (25 May 2012) -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillComboEmployee", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    'Sohail (11 Sep 2010) -- Start
    Private Sub FillList()
        Dim objEmployee As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Dim strFilter As String = "" 'Sohail (25 May 2012)
        'Sohail (14 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
        Dim strTempTableSelectQry As String = ""
        Dim strTempTableJoinQry As String = ""
        Dim strTempTableDropQry As String = ""
        'Sohail (14 Feb 2019) -- End

        Try
            'Sohail (25 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'lvEmployeeList.Items.Clear()
            objlblEmpCount.Text = "( 0 / 0 )"
            mintCount = 0
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            'Sohail (18 Jul 2017) -- End
            If CInt(cboPeriod.SelectedValue) <= 0 Then Exit Sub
            'Sohail (06 Jan 2016) -- Start
            'Issue - Newly hired employees were not coming if Include Inactive Employee parameter passes as TRUE.
            'If chkShowNewlyHiredEmployees.Checked = True Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= @startdate "
            If chkShowNewlyHiredEmployees.Checked = True Then strFilter = " CONVERT(CHAR(8), appointeddate, 112) >= '" & eZeeDate.convertDate(mdtPayPeriodStartDate) & "' "
            'Sohail (06 Jan 2016) -- End
            'Sohail (25 May 2012) -- End
            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            If chkShowReinstatedEmployees.Checked = True Then
                'Sohail (14 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Option to filter employees who has not been assigned selected head on Global Assign ED screen.
                'If strFilter.Trim <> "" Then
                '    strFilter &= " AND CONVERT(CHAR(8), reinstatement_date, 112) >= @startdate "
                'Else
                '    strFilter = " CONVERT(CHAR(8), reinstatement_date, 112) >= @startdate "
                'End If
                strTempTableSelectQry &= "SELECT DISTINCT hremployee_rehire_tran.employeeunkid " & _
                                         "INTO #tblRehire " & _
                                         "FROM   hremployee_rehire_tran " & _
                                        " WHERE isvoid = 0 " & _
                                                 "AND reinstatment_date IS NOT NULL " & _
                                                 "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                                                 "AND CONVERT(CHAR(8), reinstatment_date, 112) BETWEEN @startdate AND @enddate "

                strTempTableJoinQry &= "JOIN #tblRehire ON #tblRehire.employeeunkid = hremployee_master.employeeunkid "

                strTempTableDropQry &= "DROP TABLE #tblRehire "
                'Sohail (14 Feb 2019) -- End
            End If
            'Sohail (30 Nov 2013) -- End

            'Sohail (31 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrAdvanceFilter <> "" Then
                If strFilter <> "" Then
                    strFilter &= " AND " & mstrAdvanceFilter
                Else
                    strFilter = mstrAdvanceFilter
                End If
            End If
            'Sohail (31 Aug 2012) -- End

            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                        )
            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , CInt(cboDepartment.SelectedValue) _
            '                                          , CInt(cboSections.SelectedValue) _
            '                                          , CInt(cboUnit.SelectedValue) _
            '                                          , CInt(cboGrade.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboClass.SelectedValue) _
            '                                          , CInt(cboCostCenter.SelectedValue) _
            '                                          , 0 _
            '                                          , CInt(cboJob.SelectedValue) _
            '                                          , CInt(cboPayPoint.SelectedValue) _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , CInt(cboGender.SelectedValue), , _
            '                                          , strFilter)
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmployee.GetEmployeeList("Employee", False, True _
            '                                          , CInt(cboEmployee.SelectedValue) _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , 0 _
            '                                          , mdtPayPeriodStartDate, mdtPayPeriodEndDate, , 0, , _
            '                                          , strFilter)
            '                                         'Sohail (30 Nov 2013) -- End
            '                                         'Sohail (25 May 2012) - [strFilter]
            '                                         'Sohail (31 Aug 2012) - [CInt(cboGender.SelectedValue)]

            'Sohail (17 Mar 2017) -- Start
            'Issue - 65.1 - Inactive employees were coming due to TRUE passed in inactive-employee parameter.
            'dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               mdtPayPeriodStartDate, _
            '                               mdtPayPeriodEndDate, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, True, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , , strFilter)
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Employee", False, CInt(cboEmployee.SelectedValue), , , , , , , , , , , , , , strFilter _
                                            , strTempTableSelectQry:=strTempTableSelectQry _
                                            , strTempTableJoinQry:=strTempTableJoinQry _
                                            , strTempTableDropQry:=strTempTableDropQry _
                                            )
            'Sohail (17 Mar 2017) -- End
            'Anjan [10 June 2015] -- End

            'Sohail (06 Jan 2012) -- End

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'Dim lvItem As ListViewItem
            'Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)
            'lvEmployeeList.Items.Clear()
            'lvEmployeeList.BeginUpdate()
            mintTotalEmployee = dsEmployee.Tables("Employee").Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dsEmployee.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsEmployee.Tables(0).Columns.Add(dtCol)
            End If
            'Sohail (18 Jul 2017) -- End

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'For Each dtRow As DataRow In dsEmployee.Tables("Employee").Rows
            '    lvItem = New ListViewItem
            '    With lvItem
            '        .Text = ""
            '        .Tag = dtRow.Item("employeeunkid").ToString
            '        .SubItems.Add(dtRow.Item("employeecode").ToString)
            '        .SubItems.Add(dtRow.Item("employeename").ToString)
            '        'Sohail (03 Nov 2010) -- Start
            '        lvArray.Add(lvItem)
            '        'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        'lvEmployeeList.Items.Add(lvItem)
            '        'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            '        'Sohail (03 Nov 2010) -- End

            '    End With
            'Next
                    'RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'lvEmployeeList.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
                    'AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            'If lvEmployeeList.Items.Count > 15 Then
            '    colhName.Width = 193 - 18
            'Else
            '    colhName.Width = 193
            'End If
            dvEmployee = dsEmployee.Tables("Employee").DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "employeename"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, employeename "
            'Sohail (18 Jul 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmployee = Nothing
            dsEmployee = Nothing
            'lvEmployeeList.EndUpdate()'Sohail (18 Jul 2017)
            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
            'Call objbtnSearch.ShowResult(CStr(lvEmployeeList.Items.Count)) 'Sohail (07 Nov 2014)'Sohail (18 Jul 2017)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'For Each lvItem As ListViewItem In lvEmployeeList.Items
            '    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked 'Sohail (03 Nov 2010)
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
            'Sohail (18 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 Sep 2010) -- End


    Private Sub SetVisibility()

        Try
            btnInsert.Enabled = User._Object.Privilege._AllowAssignBatchtoEmployee

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = lblSearchEmp.Text
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End

#End Region

#Region " Form's Events "

    Private Sub frmBatchEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            'Call FillList() 'Sohail (11 Sep 2010)
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            Call SetDefaultSearchEmpText()
            'Sohail (18 Jul 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchEntry_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " ComboBox's Events "
    Private Sub cboBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objBatchTran As New clsBatchTransactionTran
        Dim objTranHead As New clsTransactionHead
        Dim dsList As New DataSet

        Try
            dsList = objBatchTran.getComboList(CInt(cboBatch.SelectedValue), "BatchHeads", False)
            m_Dataview = New DataView(dsList.Tables("BatchHeads"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows) 'Except Salary Heads

            dgvBatch.AutoGenerateColumns = False
            dgvBatch.DataSource = m_Dataview

            objcolhBatchID.DataPropertyName = "batchtransactionunkid"
            objcolhTranHeadID.DataPropertyName = "tranheadunkid"
            colhTrnHead.DataPropertyName = "Name"
            colhAmount.DataPropertyName = "Amount"
            objcolhCurrency.DataPropertyName = "currencyid"
            objcolhVendor.DataPropertyName = "vendorid"
            colhMedicalRefNo.DataPropertyName = "medicalrefno" 'Sohail (18 Jan 2012)
            'Sohail (01 Dec 2010) -- Start
            'Sohail (11 May 2011) -- Start
            'If GUI.fmtCurrency.IndexOf(".") = -1 Then
            '    colhAmount.DecimalLength = 0
            'Else
            '    colhAmount.DecimalLength = GUI.fmtCurrency.Length - (GUI.fmtCurrency.IndexOf(".") + 1)
            'End If
            colhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'Sohail (11 May 2011) -- End
            'Sohail (01 Dec 2010) -- End


            dgvBatch.Refresh()

            For i As Integer = 0 To dgvBatch.Rows.Count - 1
                With dgvBatch.Rows(i)

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = CInt(.Cells(objcolhTranHeadID.Index).Value)
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(.Cells(objcolhTranHeadID.Index).Value)
                    'Sohail (21 Aug 2015) -- End
                    If objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse _
                                   objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse _
                                   objTranHead._Calctype_Id = enCalcType.AsUserDefinedValue OrElse _
                                   objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OnAttendance OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OnHourWorked OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OverTimeHours OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NET_PAY OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TOTAL_DEDUCTION OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TOTAL_EARNING OrElse _
                                   objTranHead._Calctype_Id = enCalcType.ShortHours OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TAXABLE_EARNING_TOTAL OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NON_TAXABLE_EARNING_TOTAL OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NON_CASH_BENEFIT_TOTAL _
                                   Then

                        .Cells(colhAmount.Index).ReadOnly = True
                        .Cells(colhAmount.Index).Style.BackColor = GUI.ColorOptional 'Sohail (18 Jan 2012)
                    End If
                    .Cells(colhMedicalRefNo.Index).ReadOnly = False
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_SelectedIndexChanged", mstrModuleName)
        Finally
            objBatchTran = Nothing
            objTranHead = Nothing
        End Try
    End Sub
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPayPeriodStartDate = objPeriod._Start_Date
                mdtPayPeriodEndDate = objPeriod._End_Date
                Call FillCombo_Employee() 'Sohail (03 Mar 2012)
                Call FillList()
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPayPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (21 Aug 2015) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        Dim strMsg As String = ""
        Dim strHeadName As String = ""
        Dim dsList As DataSet = Nothing
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        Dim blnFlag As Boolean
        Dim strList As String = ""
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)

        Try
            If CInt(cboBatch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Batch."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If
            If dgvBatch.Rows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "No data found to insert."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                strMsg = Language.getMessage(mstrModuleName, 14, "Please select Period. Period is mandatory information.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            'Sohail (13 Jan 2012) -- End
            'Sohail (11 Sep 2010) -- Start
            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'If lvEmployeeList.CheckedItems.Count = 0 Then
            If dvEmployee.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Sohail (18 Jul 2017) -- End
                strMsg = Language.getMessage(mstrModuleName, 11, "Please select atleast one Employee from list.")
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                'Sohail (18 Jul 2017) -- Start
                'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                'lvEmployeeList.Focus()
                dgEmployee.Focus()
                'Sohail (18 Jul 2017) -- End
                Exit Sub
            End If
            'Sohail (11 Sep 2010) -- End

            'Sohail (16 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
            '    If strList.Length <= 0 Then
            '        strList &= CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    Else
            '        strList &= "," & CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    End If
            'Next

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'Dim selectedTags As List(Of String) = lvEmployeeList.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) x.Tag.ToString).ToList()
            Dim selectedTags As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToList
            'Sohail (18 Jul 2017) -- End
            strList = String.Join(", ", selectedTags.ToArray())

            If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strList, mdtPayPeriodEndDate) = True Then
                Cursor.Current = Cursors.Default
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 18, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (16 Oct 2012) -- End

            'Sohail (18 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If
            'Sohail (18 Jan 2012) -- End
            For i As Integer = 0 To dgvBatch.Rows.Count - 1
                With dgvBatch.Rows(i)
                    'Anjan (06 Jun 2011)-Start
                    'Issue : Rutta wanted on allow -ve ,0 amount on ED for flat rate. 
                    'If CInt(.Cells(colhAmount.Index).Value) <= 0 And .Cells(colhAmount.Index).ReadOnly = False Then
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Enter Amount. Amount is mandatory information."), enMsgBoxStyle.Information)
                    '.Cells(colhAmount.Index).Selected = True
                    'Exit Sub
                    'End If
                    'Anjan (06 Jun 2011)-End
                    'If CInt(.Cells(objcolhCurrency.Index).Value) <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Currency. Currency is mandatory information."), enMsgBoxStyle.Information)
                    '    .Cells(objcolhCurrency.Index).Selected = True
                    '    Exit Sub
                    'End If

                    'Sohail (22 Jul 2010) -- Start
                    dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(.Cells(objcolhTranHeadID.Index).Value), "TranHead")
                    For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                        With dsRow

                            'Sohail (16 May 2012) -- Start
                            'TRA - ENHANCEMENT
                            'Do not prompt message if default value has been set on transaction head formula (other tham Computed Value)
                            If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For
                            'Sohail (16 May 2012) -- End

                            'Sohail (11 Sep 2010) -- Start
                            'Sohail (18 Jul 2017) -- Start
                            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                            'For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                            '    mintEmployeeUnkID = CInt(lvItem.Tag)
                            For Each dtRow As DataRow In dvEmployee.Table.Select("IsChecked = 1")
                                mintEmployeeUnkID = CInt(dtRow("employeeunkid").ToString())
                                'Sohail (18 Jul 2017) -- End                            
                                'Sohail (11 Sep 2010) -- End
                                'Sohail (17 Jan 2012) -- Start
                                'TRA - ENHANCEMENT
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If objED.isExist(mintEmployeeUnkID, CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                                If objED.isExist(FinancialYear._Object._DatabaseName, mintEmployeeUnkID, CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    'If objED.isExist(mintEmployeeUnkID, CInt(.Item("tranheadunkid").ToString)) = False Then
                                    'Sohail (17 Jan 2012) -- End
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objTranHead._Tranheadunkid = CInt(.Item("tranheadunkid").ToString)
                                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(.Item("tranheadunkid").ToString)
                                    'Sohail (21 Aug 2015) -- End
                                    strHeadName = objTranHead._Trnheadname

                                    'Sohail (11 Sep 2010) -- Start
                                    'strMsg = Language.getMessage(mstrModuleName, 5, "The Formula of this Transaction Head contains " & strHeadName & " Tranasaction Head (type of Flate Rate)." & vbCrLf & "So please add " & strHeadName & " Transaction Head First.")
                                    'Sohail (14 Apr 2011) -- Start
                                    'strMsg = Language.getMessage(mstrModuleName, 5, "This Batch contains '" & strHeadName & "' Tranasaction Head (Type of Flate Rate) in the formula of '" & dgvBatch.Rows(i).Cells(colhTrnHead.Index).Value.ToString & "' Transaction Head." & vbCrLf & vbCrLf & "Please first assign this '" & strHeadName & "' Transaction Head to the selected Employee from Payroll -> Global Assign ED.")
                                    'Sohail (11 Sep 2010) -- End
                                    'eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                                    'Exit Sub
                                    strMsg = Language.getMessage(mstrModuleName, 3, "This Batch contains ") & strHeadName & Language.getMessage(mstrModuleName, 4, "Tranasaction Head (Type of Flat Rate) in the formula of ") & dgvBatch.Rows(i).Cells(colhTrnHead.Index).Value.ToString & Language.getMessage(mstrModuleName, 5, "Transaction Head.") & _
                                               vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "Please first assign this ") & strHeadName & Language.getMessage(mstrModuleName, 7, "Transaction Head to the selected Employee from Payroll -> Payroll Transactions -> Global Assign ED.") _
                                               & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 8, "If you do not assign ") & strHeadName & Language.getMessage(mstrModuleName, 9, "Transaction Head, its value will be considered as Zero.") _
                                               & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 17, " IF you Press YES then this will apply for all proceeding heads within this Batch and notification for them will not be prompted.") _
                                               & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 10, "Do you want to proceed?")
                                    If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                        Exit Sub
                                    Else
                                        'Sohail (26 Mar 2012) -- Start
                                        'TRA - ENHANCEMENT
                                        'Exit For
                                        GoTo DoProcess
                                        'Sohail (26 Mar 2012) -- End
                                    End If
                                    'Sohail (14 Apr 2011) -- End
                                End If
                            Next
                        End With
                    Next
                    'Sohail (22 Jul 2010) -- End
                End With
            Next

DoProcess:  'Sohail (26 Mar 2012)
            mblnIsOverWrite = chkOverwrite.Checked
            m_DataSource = m_Dataview.ToTable
            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dCol As DataColumn
            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = CInt(cboPeriod.SelectedValue)
            m_DataSource.Columns.Add(dCol)
            'Sohail (13 Jan 2012) -- End

            'Sohail (07 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            dCol = New DataColumn("start_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = eZeeDate.convertDate(mdtPayPeriodStartDate)
            m_DataSource.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = eZeeDate.convertDate(mdtPayPeriodEndDate)
            m_DataSource.Columns.Add(dCol)
            'Sohail (07 Mar 2012) -- End

            'Sohail (11 Sep 2010) -- Start
            'Sohail (16 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'For i As Integer = 0 To lvEmployeeList.CheckedItems.Count - 1
            '    If strList.Length <= 0 Then
            '        strList &= CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    Else
            '        strList &= "," & CInt(lvEmployeeList.CheckedItems(i).Tag)
            '    End If
            'Next
            'Sohail (16 Oct 2012) -- End

            'blnFlag = objED.InsertAllByDataTable(mintEmployeeUnkID, m_DataSource, mblnIsOverWrite)
            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'blnFlag = objED.InsertAllByDataTable(strList, m_DataSource, mblnIsOverWrite)
            'Sohail (07 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'blnFlag = objED.InsertAllByDataTable(strList, m_DataSource, mblnIsOverWrite, True)
            'Sohail (26 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            'blnFlag = objED.InsertAllByDataTable(strList, m_DataSource, mblnIsOverWrite, True, chkCopyPreviousEDSlab.Checked) 
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnFlag = objED.InsertAllByDataTable(strList, m_DataSource, mblnIsOverWrite, True, chkCopyPreviousEDSlab.Checked, , , chkOverwritePrevEDSlabHeads.Checked)
            'Sohail (15 Dec 2018) -- Start
            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
            'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strList, m_DataSource, mblnIsOverWrite, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
            'Sohail (25 Jun 2020) -- Start
            'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
            'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, strList, m_DataSource, mblnIsOverWrite, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
            blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, Nothing, strList, m_DataSource, mblnIsOverWrite, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "", , True)
            'Sohail (25 Jun 2020) -- End
            'Sohail (15 Dec 2018) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (26 Jul 2012) -- End
            'Sohail (07 Mar 2012) -- End
            'Sohail (13 Jan 2012) -- End
            'Sohail (11 Sep 2010) -- End
            Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)

            If blnFlag = False And objED._Message <> "" Then
                eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            If blnFlag = True Then
                If User._Object.Privilege._AllowToApproveEarningDeduction = False AndAlso chkOverwrite.Checked = True Then
                    Dim dtTable As New DataTable
                    dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("tranheadname", System.Type.GetType("System.String")).DefaultValue = ""
                    dtTable.Columns.Add("calctype_id", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0

                    Dim d_r() As DataRow = dvEmployee.Table.Select("IsChecked = 1 ")

                    For Each r As DataRow In d_r
                        For Each h As DataRow In m_DataSource.Rows

                            Dim dr As DataRow = dtTable.NewRow

                            dr.Item("employeeunkid") = r.Item("employeeunkid")
                            dr.Item("employeecode") = r.Item("employeecode")
                            dr.Item("employeename") = r.Item("employeename")
                            dr.Item("tranheadname") = h.Item("name")
                            dr.Item("calctype_id") = h.Item("calctype_id")
                            dr.Item("amount") = h.Item("amount")

                            dtTable.Rows.Add(dr)
                        Next
                    Next

                    Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, dtTable, CInt(cboPeriod.SelectedValue), GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                End If
            End If
            'Sohail (31 Oct 2019) -- End

            'Sohail (03 Nov 2010) -- Start
            'mblnCancel = False
            'btnClose.PerformClick()
            If blnFlag Then
                mblnCancel = False
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Process completed successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    Call CheckAllEmployee(False)
                    cboBatch.SelectedValue = 0
                    objchkSelectAll.Checked = False
                    cboBatch.Focus()
                Else
                    Me.Close()
                End If
            End If
            'Sohail (03 Nov 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnInsert_Click", mstrModuleName)
        Finally
            objED = Nothing
            objTranHead = Nothing
            objTrnFormula = Nothing
            dsList = Nothing
        End Try
    End Sub
#End Region

    'Sohail (11 Sep 2010) -- Start
#Region " Listview's Events "

    'Sohail (07 Nov 2014) -- Start
    'Enhancement - Sorting on System List View.
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub lvEmployeeList_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs)
    '    Try
    '        Dim new_sorting_column As ColumnHeader = lvEmployeeList.Columns(e.Column)

    '        Dim sort_order As System.Windows.Forms.SortOrder

    '        '*** To store each column sorting order (issue is some spaces are coming before column header text for image)
    '        'If new_sorting_column.ImageKey <> "" Then
    '        '    If new_sorting_column.ImageKey.ToString = ">" Then
    '        '        sort_order = SortOrder.Descending
    '        '    Else
    '        '        sort_order = SortOrder.Ascending
    '        '    End If
    '        'Else
    '        '    sort_order = SortOrder.Ascending
    '        'End If

    '        'If sort_order = SortOrder.Ascending Then
    '        '    new_sorting_column.ImageKey = ">"
    '        'Else
    '        '    new_sorting_column.ImageKey = "<"
    '        'End If
    '        If lvEmployeeList.Sorting = SortOrder.Descending Then
    '            sort_order = SortOrder.Ascending
    '        Else
            '        sort_order = SortOrder.Descending
            '    End If
    '        lvEmployeeList.Sorting = sort_order

    '        lvEmployeeList.ListViewItemSorter = New ListViewComparer(e.Column, sort_order)

    '        lvEmployeeList.Sort()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ColumnClick", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Jul 2017) -- End
    'Sohail (07 Nov 2014) -- End

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '    Try
    '        'Sohail (03 Nov 2010) -- Start
    '        'If lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '        '    chkSelectAll.Checked = True
    '        'ElseIf lvEmployeeList.CheckedItems.Count = 0 Then
    '        '    chkSelectAll.Checked = False
    '        'End If
    '        If lvEmployeeList.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '        'Sohail (03 Nov 2010) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Jul 2017) -- End
#End Region

#Region " GridView Events "

    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkShowNewlyHiredEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowNewlyHiredEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowNewlyHiredEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (25 May 2012) -- End

    'Sohail (30 Nov 2013) -- Start
    'Enhancement - Oman
    Private Sub chkShowReinstatedEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowReinstatedEmployees.CheckedChanged
        Try
            Call FillCombo_Employee()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowReinstatedEmployees_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Nov 2013) -- End

    'Sohail (26 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (26 Jul 2012) -- End
#End Region

    'Sohail (11 May 2011) -- Start
#Region " Datagridview Events "
    Private Sub dgvBatch_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBatch.DataError
        Try
            If e.ColumnIndex = dgvBatch.Columns(colhAmount.Index).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Enter correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBatch_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBatch_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBatch.EditingControlShowing
        Dim tb As TextBox
        Try
            If dgvBatch.CurrentCell.ColumnIndex = colhAmount.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress 'Sohail (12 Oct 2011)
                AddHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (18 Jan 2012) -- Start
                'TRA - ENHANCEMENT
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                'Sohail (18 Jan 2012) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBatch_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (11 May 2011) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Mar 2012)
        'Dim dsList As DataSet 'Sohail (03 Mar 2012)
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date) 'Sohail (03 Mar 2012)
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Sohail (03 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Sohail (03 Mar 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (03 Mar 2012)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboClass.Items.Count > 0 Then cboClass.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            'If cboUnit.Items.Count > 0 Then cboUnit.SelectedValue = 0
            'Sohail (30 Nov 2013) -- End
            mstrAdvanceFilter = "" 'Sohail (31 Aug 2012)

            'Sohail (18 Jul 2017) -- Start
            'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
            'lvEmployeeList.Items.Clear()
            dgEmployee.DataSource = Nothing
            'Sohail (18 Jul 2017) -- End
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (25 May 2012) -- Start
    'TRA - ENHANCEMENT
    'Sohail (18 Jul 2017) -- Start
    'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
    'Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvEmployeeList.Items.Count <= 0 Then Exit Sub
    '        lvEmployeeList.SelectedIndices.Clear()
    '        Dim lvFoundItem As ListViewItem = lvEmployeeList.FindItemWithText(txtSearchEmp.Text, True, 0, True)
    '        If lvFoundItem IsNot Nothing Then
    '            lvEmployeeList.TopItem = lvFoundItem
    '            lvFoundItem.Selected = True
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR employeename LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Jul 2017) -- End
    'Sohail (25 May 2012) -- End

    'Sohail (31 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (31 Aug 2012) -- End
#End Region
    'Sohail (11 Sep 2010) -- End

#Region " Message List "
    '1, "Please select Batch."
    '2, "No data found to insert."
    '3, "Please Enter Amount. Amount is mandatory information."
    '4, "Please Select Currency. Currency is mandatory information."
    '5, "This Batch contains '" & strHeadName & "' Tranasaction Head (Type of Flate Rate) in the formula of '" & dgvBatch.Rows(i).Cells(colhTrnHead.Index).Value.ToString & "' Transaction Head." & vbCrLf & vbCrLf & "Please first assign this '" & strHeadName & "' Transaction Head to the selected Employee from Payroll -> Payroll Transactions -> Global Assign ED." & vbCrLf & vbCrLf & "If you do not assign " & strHeadName & " Transaction Head, its value will be cosidered as Zero." & vbCrLf & vbCrLf & "Do you want to proceed?"
    '6, "Please select atleast one Employee from list."
    '7, "Process completed successfully."
    '8, "Please Enter Proper Amount."
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBatchEntry.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBatchEntry.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 



			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnInsert.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInsert.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbBatchEntry.Text = Language._Object.getCaption(Me.gbBatchEntry.Name, Me.gbBatchEntry.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.btnInsert.Text = Language._Object.getCaption(Me.btnInsert.Name, Me.btnInsert.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.chkOverwrite.Text = Language._Object.getCaption(Me.chkOverwrite.Name, Me.chkOverwrite.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhTrnHead.HeaderText = Language._Object.getCaption(Me.colhTrnHead.Name, Me.colhTrnHead.HeaderText)
			Me.colhAmount.HeaderText = Language._Object.getCaption(Me.colhAmount.Name, Me.colhAmount.HeaderText)
			Me.colhMedicalRefNo.HeaderText = Language._Object.getCaption(Me.colhMedicalRefNo.Name, Me.colhMedicalRefNo.HeaderText)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.chkShowNewlyHiredEmployees.Text = Language._Object.getCaption(Me.chkShowNewlyHiredEmployees.Name, Me.chkShowNewlyHiredEmployees.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.chkShowReinstatedEmployees.Text = Language._Object.getCaption(Me.chkShowReinstatedEmployees.Name, Me.chkShowReinstatedEmployees.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Batch.")
			Language.setMessage(mstrModuleName, 2, "No data found to insert.")
			Language.setMessage(mstrModuleName, 3, "This Batch contains")
			Language.setMessage(mstrModuleName, 4, "Tranasaction Head (Type of Flat Rate) in the formula of")
			Language.setMessage(mstrModuleName, 5, "Transaction Head.")
			Language.setMessage(mstrModuleName, 6, "Please first assign this")
			Language.setMessage(mstrModuleName, 7, "Transaction Head to the selected Employee from Payroll -> Payroll Transactions -> Global Assign ED.")
			Language.setMessage(mstrModuleName, 8, "If you do not assign")
			Language.setMessage(mstrModuleName, 9, "Transaction Head, its value will be considered as Zero.")
			Language.setMessage(mstrModuleName, 10, "Do you want to proceed?")
			Language.setMessage(mstrModuleName, 11, "Please select atleast one Employee from list.")
			Language.setMessage(mstrModuleName, 12, "Process completed successfully.")
			Language.setMessage(mstrModuleName, 13, "Please Enter correct Amount.")
			Language.setMessage(mstrModuleName, 14, "Please select Period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to assign earning deduction.")
			Language.setMessage(mstrModuleName, 16, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
			Language.setMessage(mstrModuleName, 17, " IF you Press YES then this will apply for all proceeding heads within this Batch and notification for them will not be prompted.")
            Language.setMessage(mstrModuleName, 18, "Do you want to void Payroll?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class