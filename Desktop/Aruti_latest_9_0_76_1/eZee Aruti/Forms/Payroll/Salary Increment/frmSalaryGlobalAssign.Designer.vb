﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalaryGlobalAssign
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSalaryGlobalAssign))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblGradeLevelChangedMsg = New System.Windows.Forms.Label
        Me.objlblGradeLevelChangedColor = New System.Windows.Forms.Label
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.txtTotNewScale = New System.Windows.Forms.TextBox
        Me.txtTotIncrAmt = New System.Windows.Forms.TextBox
        Me.txtTotCurrScale = New System.Windows.Forms.TextBox
        Me.lblTotCurrScale = New System.Windows.Forms.Label
        Me.lblTotIncrAmt = New System.Windows.Forms.Label
        Me.lblTotNewScale = New System.Windows.Forms.Label
        Me.lblMaxLevelReachedMsg = New System.Windows.Forms.Label
        Me.objlblMaxLevelReachedColor = New System.Windows.Forms.Label
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbSalaryIncrement = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpActualDate = New System.Windows.Forms.DateTimePicker
        Me.lblActualDate = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.lblIncrementBy = New System.Windows.Forms.Label
        Me.cboIncrementBy = New System.Windows.Forms.ComboBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.txtPercentage = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblReason = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.dtpIncrementdate = New System.Windows.Forms.DateTimePicker
        Me.lblIncrementDate = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhGradeGroup = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhLevel = New System.Windows.Forms.ColumnHeader
        Me.colhLastDate = New System.Windows.Forms.ColumnHeader
        Me.colhCurrScale = New System.Windows.Forms.ColumnHeader
        Me.colhIncrAmt = New System.Windows.Forms.ColumnHeader
        Me.colhNewScale = New System.Windows.Forms.ColumnHeader
        Me.colhMaxScale = New System.Windows.Forms.ColumnHeader
        Me.objcolhIncMode = New System.Windows.Forms.ColumnHeader
        Me.objcolhPercentage = New System.Windows.Forms.ColumnHeader
        Me.colhNewGradeGroup = New System.Windows.Forms.ColumnHeader
        Me.colhNewGrade = New System.Windows.Forms.ColumnHeader
        Me.colhNewGradeLevel = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbSalaryIncrement.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbSalaryIncrement)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(967, 560)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(967, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Global Assign Salary Change"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblGradeLevelChangedMsg)
        Me.objFooter.Controls.Add(Me.objlblGradeLevelChangedColor)
        Me.objFooter.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.objFooter.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.txtSearchEmp)
        Me.objFooter.Controls.Add(Me.txtTotNewScale)
        Me.objFooter.Controls.Add(Me.txtTotIncrAmt)
        Me.objFooter.Controls.Add(Me.txtTotCurrScale)
        Me.objFooter.Controls.Add(Me.lblTotCurrScale)
        Me.objFooter.Controls.Add(Me.lblTotIncrAmt)
        Me.objFooter.Controls.Add(Me.lblTotNewScale)
        Me.objFooter.Controls.Add(Me.lblMaxLevelReachedMsg)
        Me.objFooter.Controls.Add(Me.objlblMaxLevelReachedColor)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 461)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(967, 99)
        Me.objFooter.TabIndex = 3
        '
        'lblGradeLevelChangedMsg
        '
        Me.lblGradeLevelChangedMsg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblGradeLevelChangedMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevelChangedMsg.Location = New System.Drawing.Point(304, 35)
        Me.lblGradeLevelChangedMsg.Name = "lblGradeLevelChangedMsg"
        Me.lblGradeLevelChangedMsg.Size = New System.Drawing.Size(401, 15)
        Me.lblGradeLevelChangedMsg.TabIndex = 243
        Me.lblGradeLevelChangedMsg.Text = "Grade Level will be Changed"
        Me.lblGradeLevelChangedMsg.Visible = False
        '
        'objlblGradeLevelChangedColor
        '
        Me.objlblGradeLevelChangedColor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.objlblGradeLevelChangedColor.BackColor = System.Drawing.Color.Blue
        Me.objlblGradeLevelChangedColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblGradeLevelChangedColor.Location = New System.Drawing.Point(250, 35)
        Me.objlblGradeLevelChangedColor.Name = "objlblGradeLevelChangedColor"
        Me.objlblGradeLevelChangedColor.Size = New System.Drawing.Size(49, 15)
        Me.objlblGradeLevelChangedColor.TabIndex = 242
        Me.objlblGradeLevelChangedColor.Visible = False
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(12, 55)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(229, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 241
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(12, 33)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(229, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 240
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(11, 10)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(94, 13)
        Me.lblSearchEmp.TabIndex = 107
        Me.lblSearchEmp.Text = "Search Employee"
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(111, 6)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(188, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'txtTotNewScale
        '
        Me.txtTotNewScale.Location = New System.Drawing.Point(821, 30)
        Me.txtTotNewScale.Name = "txtTotNewScale"
        Me.txtTotNewScale.ReadOnly = True
        Me.txtTotNewScale.Size = New System.Drawing.Size(131, 21)
        Me.txtTotNewScale.TabIndex = 105
        Me.txtTotNewScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotIncrAmt
        '
        Me.txtTotIncrAmt.Location = New System.Drawing.Point(687, 30)
        Me.txtTotIncrAmt.Name = "txtTotIncrAmt"
        Me.txtTotIncrAmt.ReadOnly = True
        Me.txtTotIncrAmt.Size = New System.Drawing.Size(131, 21)
        Me.txtTotIncrAmt.TabIndex = 104
        Me.txtTotIncrAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotCurrScale
        '
        Me.txtTotCurrScale.Location = New System.Drawing.Point(550, 30)
        Me.txtTotCurrScale.Name = "txtTotCurrScale"
        Me.txtTotCurrScale.ReadOnly = True
        Me.txtTotCurrScale.Size = New System.Drawing.Size(131, 21)
        Me.txtTotCurrScale.TabIndex = 103
        Me.txtTotCurrScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotCurrScale
        '
        Me.lblTotCurrScale.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotCurrScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotCurrScale.Location = New System.Drawing.Point(550, 12)
        Me.lblTotCurrScale.Name = "lblTotCurrScale"
        Me.lblTotCurrScale.Size = New System.Drawing.Size(131, 15)
        Me.lblTotCurrScale.TabIndex = 102
        Me.lblTotCurrScale.Text = "Total Current Scale"
        Me.lblTotCurrScale.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblTotIncrAmt
        '
        Me.lblTotIncrAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotIncrAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotIncrAmt.Location = New System.Drawing.Point(687, 12)
        Me.lblTotIncrAmt.Name = "lblTotIncrAmt"
        Me.lblTotIncrAmt.Size = New System.Drawing.Size(131, 15)
        Me.lblTotIncrAmt.TabIndex = 101
        Me.lblTotIncrAmt.Text = "Total Increment Amount"
        Me.lblTotIncrAmt.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblTotNewScale
        '
        Me.lblTotNewScale.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotNewScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotNewScale.Location = New System.Drawing.Point(824, 12)
        Me.lblTotNewScale.Name = "lblTotNewScale"
        Me.lblTotNewScale.Size = New System.Drawing.Size(131, 15)
        Me.lblTotNewScale.TabIndex = 97
        Me.lblTotNewScale.Text = "Total Expected New Scale"
        Me.lblTotNewScale.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblMaxLevelReachedMsg
        '
        Me.lblMaxLevelReachedMsg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxLevelReachedMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLevelReachedMsg.Location = New System.Drawing.Point(304, 56)
        Me.lblMaxLevelReachedMsg.Name = "lblMaxLevelReachedMsg"
        Me.lblMaxLevelReachedMsg.Size = New System.Drawing.Size(401, 15)
        Me.lblMaxLevelReachedMsg.TabIndex = 96
        Me.lblMaxLevelReachedMsg.Text = "Expected New Scale will reach the level of Maximum Scale."
        '
        'objlblMaxLevelReachedColor
        '
        Me.objlblMaxLevelReachedColor.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.objlblMaxLevelReachedColor.BackColor = System.Drawing.SystemColors.ControlDark
        Me.objlblMaxLevelReachedColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblMaxLevelReachedColor.Location = New System.Drawing.Point(250, 56)
        Me.objlblMaxLevelReachedColor.Name = "objlblMaxLevelReachedColor"
        Me.objlblMaxLevelReachedColor.Size = New System.Drawing.Size(49, 15)
        Me.objlblMaxLevelReachedColor.TabIndex = 95
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(755, 57)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(97, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(858, 57)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbSalaryIncrement
        '
        Me.gbSalaryIncrement.BorderColor = System.Drawing.Color.Black
        Me.gbSalaryIncrement.Checked = False
        Me.gbSalaryIncrement.CollapseAllExceptThis = False
        Me.gbSalaryIncrement.CollapsedHoverImage = Nothing
        Me.gbSalaryIncrement.CollapsedNormalImage = Nothing
        Me.gbSalaryIncrement.CollapsedPressedImage = Nothing
        Me.gbSalaryIncrement.CollapseOnLoad = False
        Me.gbSalaryIncrement.Controls.Add(Me.dtpActualDate)
        Me.gbSalaryIncrement.Controls.Add(Me.lblActualDate)
        Me.gbSalaryIncrement.Controls.Add(Me.lnkAllocation)
        Me.gbSalaryIncrement.Controls.Add(Me.objbtnReset)
        Me.gbSalaryIncrement.Controls.Add(Me.objbtnSearch)
        Me.gbSalaryIncrement.Controls.Add(Me.objbtnAddReason)
        Me.gbSalaryIncrement.Controls.Add(Me.cboReason)
        Me.gbSalaryIncrement.Controls.Add(Me.lblIncrementBy)
        Me.gbSalaryIncrement.Controls.Add(Me.cboIncrementBy)
        Me.gbSalaryIncrement.Controls.Add(Me.lblPercentage)
        Me.gbSalaryIncrement.Controls.Add(Me.txtPercentage)
        Me.gbSalaryIncrement.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbSalaryIncrement.Controls.Add(Me.lblReason)
        Me.gbSalaryIncrement.Controls.Add(Me.cboEmployee)
        Me.gbSalaryIncrement.Controls.Add(Me.lblEmployee)
        Me.gbSalaryIncrement.Controls.Add(Me.dtpIncrementdate)
        Me.gbSalaryIncrement.Controls.Add(Me.lblIncrementDate)
        Me.gbSalaryIncrement.Controls.Add(Me.cboPayPeriod)
        Me.gbSalaryIncrement.Controls.Add(Me.lblPayPeriod)
        Me.gbSalaryIncrement.Controls.Add(Me.cboPayYear)
        Me.gbSalaryIncrement.Controls.Add(Me.lblPayYear)
        Me.gbSalaryIncrement.ExpandedHoverImage = Nothing
        Me.gbSalaryIncrement.ExpandedNormalImage = Nothing
        Me.gbSalaryIncrement.ExpandedPressedImage = Nothing
        Me.gbSalaryIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSalaryIncrement.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSalaryIncrement.HeaderHeight = 25
        Me.gbSalaryIncrement.HeaderMessage = ""
        Me.gbSalaryIncrement.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSalaryIncrement.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSalaryIncrement.HeightOnCollapse = 0
        Me.gbSalaryIncrement.LeftTextSpace = 0
        Me.gbSalaryIncrement.Location = New System.Drawing.Point(12, 66)
        Me.gbSalaryIncrement.Name = "gbSalaryIncrement"
        Me.gbSalaryIncrement.OpenHeight = 182
        Me.gbSalaryIncrement.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSalaryIncrement.ShowBorder = True
        Me.gbSalaryIncrement.ShowCheckBox = False
        Me.gbSalaryIncrement.ShowCollapseButton = False
        Me.gbSalaryIncrement.ShowDefaultBorderColor = True
        Me.gbSalaryIncrement.ShowDownButton = False
        Me.gbSalaryIncrement.ShowHeader = True
        Me.gbSalaryIncrement.Size = New System.Drawing.Size(943, 89)
        Me.gbSalaryIncrement.TabIndex = 0
        Me.gbSalaryIncrement.Temp = 0
        Me.gbSalaryIncrement.Text = "Salary Change Information"
        Me.gbSalaryIncrement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpActualDate
        '
        Me.dtpActualDate.Checked = False
        Me.dtpActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpActualDate.Location = New System.Drawing.Point(541, 34)
        Me.dtpActualDate.Name = "dtpActualDate"
        Me.dtpActualDate.ShowCheckBox = True
        Me.dtpActualDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpActualDate.TabIndex = 308
        '
        'lblActualDate
        '
        Me.lblActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActualDate.Location = New System.Drawing.Point(434, 37)
        Me.lblActualDate.Name = "lblActualDate"
        Me.lblActualDate.Size = New System.Drawing.Size(101, 15)
        Me.lblActualDate.TabIndex = 309
        Me.lblActualDate.Text = "Actual Date"
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(803, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 306
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(916, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(893, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(912, 61)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 250
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(743, 61)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(163, 21)
        Me.cboReason.TabIndex = 5
        '
        'lblIncrementBy
        '
        Me.lblIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementBy.Location = New System.Drawing.Point(8, 63)
        Me.lblIncrementBy.Name = "lblIncrementBy"
        Me.lblIncrementBy.Size = New System.Drawing.Size(81, 16)
        Me.lblIncrementBy.TabIndex = 246
        Me.lblIncrementBy.Text = "Change By"
        Me.lblIncrementBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIncrementBy
        '
        Me.cboIncrementBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrementBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrementBy.FormattingEnabled = True
        Me.cboIncrementBy.Location = New System.Drawing.Point(95, 61)
        Me.cboIncrementBy.Name = "cboIncrementBy"
        Me.cboIncrementBy.Size = New System.Drawing.Size(108, 21)
        Me.cboIncrementBy.TabIndex = 3
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(209, 64)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(101, 15)
        Me.lblPercentage.TabIndex = 243
        Me.lblPercentage.Text = "Percentage (%)"
        '
        'txtPercentage
        '
        Me.txtPercentage.AllowNegative = True
        Me.txtPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentage.DigitsInGroup = 0
        Me.txtPercentage.Flags = 0
        Me.txtPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentage.Location = New System.Drawing.Point(316, 61)
        Me.txtPercentage.MaxDecimalPlaces = 6
        Me.txtPercentage.MaxWholeDigits = 21
        Me.txtPercentage.Name = "txtPercentage"
        Me.txtPercentage.Prefix = ""
        Me.txtPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtPercentage.Size = New System.Drawing.Size(106, 21)
        Me.txtPercentage.TabIndex = 4
        Me.txtPercentage.Text = "0"
        Me.txtPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(912, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(655, 64)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(82, 15)
        Me.lblReason.TabIndex = 240
        Me.lblReason.Text = "Reason"
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(743, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(163, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(655, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(82, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'dtpIncrementdate
        '
        Me.dtpIncrementdate.Checked = False
        Me.dtpIncrementdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIncrementdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIncrementdate.Location = New System.Drawing.Point(316, 34)
        Me.dtpIncrementdate.Name = "dtpIncrementdate"
        Me.dtpIncrementdate.ShowCheckBox = True
        Me.dtpIncrementdate.Size = New System.Drawing.Size(106, 21)
        Me.dtpIncrementdate.TabIndex = 1
        '
        'lblIncrementDate
        '
        Me.lblIncrementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrementDate.Location = New System.Drawing.Point(209, 37)
        Me.lblIncrementDate.Name = "lblIncrementDate"
        Me.lblIncrementDate.Size = New System.Drawing.Size(101, 15)
        Me.lblIncrementDate.TabIndex = 227
        Me.lblIncrementDate.Text = "Change Date"
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(95, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(108, 21)
        Me.cboPayPeriod.TabIndex = 0
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(81, 16)
        Me.lblPayPeriod.TabIndex = 225
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayYear
        '
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(62, 87)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(37, 21)
        Me.cboPayYear.TabIndex = 0
        Me.cboPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(6, 89)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(50, 16)
        Me.lblPayYear.TabIndex = 223
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayYear.Visible = False
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 161)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(943, 296)
        Me.gbEmployeeList.TabIndex = 1
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(940, 269)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 17
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhCode, Me.colhName, Me.colhGradeGroup, Me.colhGrade, Me.colhLevel, Me.colhLastDate, Me.colhCurrScale, Me.colhIncrAmt, Me.colhNewScale, Me.colhMaxScale, Me.objcolhIncMode, Me.objcolhPercentage, Me.colhNewGradeGroup, Me.colhNewGrade, Me.colhNewGradeLevel})
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.HideSelection = False
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.Size = New System.Drawing.Size(940, 269)
        Me.lvEmployeeList.TabIndex = 0
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 50
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 150
        '
        'colhGradeGroup
        '
        Me.colhGradeGroup.Tag = "colhGradeGroup"
        Me.colhGradeGroup.Text = "Grade Group"
        Me.colhGradeGroup.Width = 120
        '
        'colhGrade
        '
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade"
        Me.colhGrade.Width = 80
        '
        'colhLevel
        '
        Me.colhLevel.Tag = "colhLevel"
        Me.colhLevel.Text = "Grade Level"
        Me.colhLevel.Width = 80
        '
        'colhLastDate
        '
        Me.colhLastDate.Tag = "colhLastDate"
        Me.colhLastDate.Text = "Last Incr. Date"
        Me.colhLastDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhLastDate.Width = 90
        '
        'colhCurrScale
        '
        Me.colhCurrScale.Tag = "colhCurrScale"
        Me.colhCurrScale.Text = "Current Scale"
        Me.colhCurrScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhCurrScale.Width = 110
        '
        'colhIncrAmt
        '
        Me.colhIncrAmt.Tag = "colhIncrAmt"
        Me.colhIncrAmt.Text = "Change Amount"
        Me.colhIncrAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhIncrAmt.Width = 100
        '
        'colhNewScale
        '
        Me.colhNewScale.Tag = "colhNewScal"
        Me.colhNewScale.Text = "Expect. New Scale"
        Me.colhNewScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhNewScale.Width = 110
        '
        'colhMaxScale
        '
        Me.colhMaxScale.Tag = "colhMaxScale"
        Me.colhMaxScale.Text = "Max. Scale"
        Me.colhMaxScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhMaxScale.Width = 120
        '
        'objcolhIncMode
        '
        Me.objcolhIncMode.Tag = "objcolhIncMode"
        Me.objcolhIncMode.Text = "Inc Mode"
        Me.objcolhIncMode.Width = 0
        '
        'objcolhPercentage
        '
        Me.objcolhPercentage.Tag = "objcolhPercentage"
        Me.objcolhPercentage.Text = "Percentage"
        Me.objcolhPercentage.Width = 0
        '
        'colhNewGradeGroup
        '
        Me.colhNewGradeGroup.Tag = "colhNewGradeGroup"
        Me.colhNewGradeGroup.Text = "New Grade Group"
        Me.colhNewGradeGroup.Width = 0
        '
        'colhNewGrade
        '
        Me.colhNewGrade.Tag = "colhNewGrade"
        Me.colhNewGrade.Text = "New Grade"
        Me.colhNewGrade.Width = 0
        '
        'colhNewGradeLevel
        '
        Me.colhNewGradeLevel.Tag = "colhNewGradeLevel"
        Me.colhNewGradeLevel.Text = "New Grade Level"
        Me.colhNewGradeLevel.Width = 100
        '
        'frmSalaryGlobalAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(967, 560)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSalaryGlobalAssign"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assign Salary Change"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.gbSalaryIncrement.ResumeLayout(False)
        Me.gbSalaryIncrement.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbSalaryIncrement As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvEmployeeList As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpIncrementdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIncrementDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents colhGradeGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCurrScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLastDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIncrAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMaxScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents txtPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents colhNewScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents objlblMaxLevelReachedColor As System.Windows.Forms.Label
    Friend WithEvents lblMaxLevelReachedMsg As System.Windows.Forms.Label
    Friend WithEvents cboIncrementBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblIncrementBy As System.Windows.Forms.Label
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTotNewScale As System.Windows.Forms.Label
    Friend WithEvents lblTotCurrScale As System.Windows.Forms.Label
    Friend WithEvents lblTotIncrAmt As System.Windows.Forms.Label
    Friend WithEvents txtTotCurrScale As System.Windows.Forms.TextBox
    Friend WithEvents txtTotIncrAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtTotNewScale As System.Windows.Forms.TextBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objcolhIncMode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPercentage As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents colhNewGradeGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNewGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNewGradeLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblGradeLevelChangedMsg As System.Windows.Forms.Label
    Friend WithEvents objlblGradeLevelChangedColor As System.Windows.Forms.Label
    Friend WithEvents dtpActualDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblActualDate As System.Windows.Forms.Label
End Class
