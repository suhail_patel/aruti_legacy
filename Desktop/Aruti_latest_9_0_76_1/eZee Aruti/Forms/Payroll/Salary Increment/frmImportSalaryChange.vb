﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Threading
Imports System.Web

#End Region

Public Class frmImportSalaryChange

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportSalaryChange"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private mintPeriodUnkId As Integer
    Private mdtIncrementDate As DateTime
    Private mintReasonUnkId As Integer
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private mdtFilteredTable As DataTable
    Private m_Dataview As DataView
    Private mintSuccessCount As Integer = 0
    Private trd As Thread
    Private mintChangeTypeId As Integer = 0
    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private mdtActualDate As Date
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhEmpCode.DataPropertyName = "EmpCode"
            dgcolhEmpName.DataPropertyName = "EmpName"
            dgcolhCurrScale.DataPropertyName = "currentscale"
            dgcolhIncrement.DataPropertyName = "Increment"
            dgcolhNewScale.DataPropertyName = "newscale"
            dgcolhRemark.DataPropertyName = "remark"
            dgcolhPromotionDate.DataPropertyName = "promotion_date"

            dgcolhCurrScale.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhIncrement.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhNewScale.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhPromotionDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkSelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Sub Send_Notification()
        Try
            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
                Dim objEmployee As New clsEmployee_Master
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodUnkId
                Dim strEmpList As String = String.Join(",", (From p In mdtFilteredTable Select (p.Item("employeeunkid").ToString)).ToArray)
                Dim dsEmp As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")", , )
                Dim strDeptIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "departmentunkid") Select (If(IsDBNull(p.Item("departmentunkid")), "-99", p.Item("departmentunkid")).ToString)).ToArray)
                Dim strJobIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "jobunkid") Select (If(IsDBNull(p.Item("jobunkid")), "-99", p.Item("jobunkid")).ToString)).ToArray)
                Dim strClassGrpIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classgroupunkid") Select (If(IsDBNull(p.Item("classgroupunkid")), "-99", p.Item("classgroupunkid")).ToString)).ToArray)
                Dim strClassIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classunkid") Select (If(IsDBNull(p.Item("classunkid")), "-99", p.Item("classunkid")).ToString)).ToArray)

                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                Dim dUList As New DataSet
                dUList = objUsr.Get_UserBy_PrivilegeId(671)
                If dUList.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dUList.Tables(0).Rows
                        Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(dRow.Item("UId")), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
                        If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                            Dim mblnFlag As Boolean = False
                            For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
                                Dim drRow() As DataRow = Nothing
                                Select Case CInt(AID)

                                    Case enAllocation.DEPARTMENT
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strDeptIDs & ") AND referenceunkid = " & enAllocation.DEPARTMENT)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.JOBS
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strJobIDs & ") AND referenceunkid = " & enAllocation.JOBS)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASS_GROUP
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassGrpIDs & ") AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case enAllocation.CLASSES
                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassIDs & ") AND referenceunkid = " & enAllocation.CLASSES)
                                        If drRow.Length > 0 Then
                                            mblnFlag = True
                                        Else
                                            mblnFlag = False
                                            Exit For
                                        End If

                                    Case Else
                                        mblnFlag = False
                                End Select
                            Next

                            If mblnFlag Then
                                Dim objSMail As New clsSendMail
                                StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), 0)
                                objSMail._ToEmail = CStr(dRow.Item("UEmail"))
                                objSMail._Subject = Language.getMessage(mstrModuleName, 1, "Notifications to Approve Employee Salary Change(s)")
                                objSMail._Message = StrMessage
                                objSMail._Form_Name = "" 'Please pass Form Name for WEB
                                objSMail._LogEmployeeUnkid = -1
                                objSMail._OperationModeId = enLogin_Mode.DESKTOP
                                objSMail._UserUnkid = User._Object._Userunkid
                                objSMail._SenderAddress = User._Object._Email
                                objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objSMail.SendMail()
                                objSMail.SendMail(Company._Object._Companyunkid)
                                'Sohail (30 Nov 2017) -- End
                                objSMail = Nothing
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal intUserID As Integer, ByVal intSalaryincrementtranunkid As Integer) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 11, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total <b>" & mintSuccessCount.ToString & "</b> salary changes have been added to the salary list by importation. </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 12, "This is to inform you that total") & " <b>" & mintSuccessCount.ToString & "</b> " & Language.getMessage(mstrModuleName, 13, "salary changes have been added to the salary list by importation.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End



            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b> </span></p>")
            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 14, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage(mstrModuleName, 15, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            Dim strLink As String
            strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|0|" & mintPeriodUnkId.ToString & "|0"))

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on the following link to Approve/Reject these changes.</span></p>")
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")

            StrMessage.Append(" " & Language.getMessage(mstrModuleName, 16, "Please click on the following link to Approve/Reject these changes.") & "</span></p>")
            StrMessage.Append(" " & "<a href='" & strLink & "'>" & strLink & "</a>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<br><br>")
            StrMessage.Append(vbCrLf)

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmImportSalaryChange_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSalaryIncrement.SetMessages()
            objfrm._Other_ModuleNames = "clsSalaryIncrement"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportSalaryChange_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmImportSalaryChange_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportSalaryChange_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False
        Dim objSalary As New clsSalaryIncrement
        Dim objPayment As New clsPayment_tran
        Dim objTnA As New clsTnALeaveTran
        Dim objPeriod As New clscommom_period_Tran
        Dim strEmpList As String = ""
        Try
            If mdtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you can not import this file. Reason :There is no transaction in this file to import."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            m_Dataview.RowFilter = "rowtypeid = 0 "
            mdtFilteredTable = m_Dataview.ToTable

            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you can not import this file. Reason :Either Employee Codes are not there in the system or transactions already exists."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            For Each dtRow As DataRow In mdtFilteredTable.Rows
                dsList = objSalary.getLastIncrement("LastIncr", CInt(dtRow.Item("employeeunkid")), True)
                If dsList.Tables("LastIncr").Rows.Count > 0 Then
                    If eZeeDate.convertDate(mdtIncrementDate) < dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Some salary changes are already done from another machine. Please re-select file to do further operation."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    If eZeeDate.convertDate(mdtActualDate) < dsList.Tables("LastIncr").Rows(0).Item("incrementdate").ToString Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Some salary changes are already done from another machine. Please re-select file to do further operation."), enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
            Next
            'Sohail (21 Jan 2020) -- End

            objSalary._Periodunkid = mintPeriodUnkId
            objSalary._Incrementdate = mdtIncrementDate
            objSalary._Userunkid = User._Object._Userunkid
            objSalary._Isfromemployee = False
            objSalary._Reason_Id = mintReasonUnkId
            objSalary._Changetypeid = mintChangeTypeId
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            objSalary._ActualDate = mdtActualDate
            'Sohail (21 Jan 2020) -- End
            'Sohail (02 Mar 2020) -- Start
            'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
            objSalary._Arrears_Countryid = Company._Object._Localization_Country
            'Sohail (02 Mar 2020) -- End

            If User._Object.Privilege._AllowToApproveSalaryChange = True Then
                objSalary._Isapproved = True
                objSalary._Approveruserunkid = User._Object._Userunkid
            Else
                objSalary._Isapproved = False
            End If

            Dim lstEmp As List(Of String) = (From p In mdtFilteredTable Select (p.Item("employeeunkid").ToString)).ToList
            strEmpList = String.Join(",", lstEmp.ToArray)

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodUnkId
            dsList = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, mintPeriodUnkId, clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, False)
            If dsList.Tables("Payment").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Payment of Salary for some of the selected employees is already done for this period. Please Void Payment for selected employee to give salary change."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If objTnA.IsPayrollProcessDone(mintPeriodUnkId, strEmpList, objPeriod._End_Date) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 17, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If

            'Sohail (24 Feb 2022) -- Start
            'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
            'blnFlag = objSalary.InsertAll(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtFilteredTable, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , True, "")
            blnFlag = objSalary.InsertAll(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtFilteredTable, True, ConfigParameter._Object._CurrentDateAndTime, , True, "")
            'Sohail ((24 Feb 2022) -- End

            mintSuccessCount = objSalary._SuccessCnt

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Salary Increment Process completed successfully."), enMsgBoxStyle.Information)
                objchkSelectAll.Checked = False

                mblnCancel = False
                Me.Close()
            End If

            If blnFlag = True Then
                If mintSuccessCount > 0 Then
                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Salary C") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGetFileFomat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetFileFomat.Click
        If ConfigParameter._Object._ExportDataPath = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim dlgSaveFile As New SaveFileDialog
        Dim strFilePath As String = String.Empty
        Dim objEd As New clsEarningDeduction
        Dim dsList As DataSet
        Try
            With dlgSaveFile
                .InitialDirectory = ConfigParameter._Object._ExportDataPath
                .Filter = "Excel files(*.xlsx)|*.xlsx"
                .FilterIndex = 0
                .OverwritePrompt = True

                If .ShowDialog = Windows.Forms.DialogResult.OK Then
                    strFilePath = .FileName

                    dsList = (New clsSalaryIncrement).GetImportFileFormat("Format")
                    'S.SANDEEP [12-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 0001843
                    'objIExcel.Export(strFilePath, dsList)
                    OpenXML_Export(strFilePath, dsList)
                    'S.SANDEEP [12-Jan-2018] -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File Exported Successfully."), enMsgBoxStyle.Information)
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetFileFomat_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other control's Events "

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try
            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
            'S.SANDEEP [27-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002224|#ARUTI-134}
            'ofdlgOpen.Filter = "Excel files (*.xlsx)|*.xlsx"" '|XML files (*.xml)|*.xml"
            ofdlgOpen.Filter = "Excel files (*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            'S.SANDEEP [27-Apr-2018] -- END
            ofdlgOpen.FilterIndex = 0

            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName

                Select Case ofdlgOpen.FilterIndex
                    Case 1, 2
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'dsList = objIExcel.Import(txtFilePath.Text)
                        dsList = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select

                Dim frm As New frmSalaryChangeMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = True
                    Exit Sub
                End If

                mdtTable = frm._DataTable
                mintPeriodUnkId = frm._PeriodId
                mdtIncrementDate = frm._IncrementDate
                mintReasonUnkId = frm._ReasonId
                mintChangeTypeId = frm._ChangeTypeId
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                mdtActualDate = frm._ActualDate
                'Sohail (21 Jan 2020) -- End

                m_Dataview = New DataView(mdtTable)

                m_Dataview.RowFilter = "rowtypeid <> 0 "
                If m_Dataview.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Some Salary Changes will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button."), enMsgBoxStyle.Information)
                End If

                m_Dataview.RowFilter = "rowtypeid = 0 "

                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowAll.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = ""

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowAll_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnGetFileFomat.GradientBackColor = GUI._ButttonBackColor 
            Me.btnGetFileFomat.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor 
            Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
            Me.btnGetFileFomat.Text = Language._Object.getCaption(Me.btnGetFileFomat.Name, Me.btnGetFileFomat.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
            Me.mnuShowUnSuccessful.Text = Language._Object.getCaption(Me.mnuShowUnSuccessful.Name, Me.mnuShowUnSuccessful.Text)
            Me.mnuShowAll.Text = Language._Object.getCaption(Me.mnuShowAll.Name, Me.mnuShowAll.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
            Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
            Me.dgcolhCurrScale.HeaderText = Language._Object.getCaption(Me.dgcolhCurrScale.Name, Me.dgcolhCurrScale.HeaderText)
            Me.dgcolhIncrement.HeaderText = Language._Object.getCaption(Me.dgcolhIncrement.Name, Me.dgcolhIncrement.HeaderText)
            Me.dgcolhNewScale.HeaderText = Language._Object.getCaption(Me.dgcolhNewScale.Name, Me.dgcolhNewScale.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
            Me.dgcolhPromotionDate.HeaderText = Language._Object.getCaption(Me.dgcolhPromotionDate.Name, Me.dgcolhPromotionDate.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Notifications to Approve Employee Salary Change(s)")
            Language.setMessage(mstrModuleName, 2, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 3, "File Exported Successfully.")
            Language.setMessage(mstrModuleName, 4, "Salary Increment Process completed successfully.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you can not import this file. Reason :Either Employee Codes are not there in the system or transactions already exists.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you can not import this file. Reason :There is no transaction in this file to import.")
            Language.setMessage(mstrModuleName, 7, "Please Tick atleast one transaction from list to Import.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Payment of Salary for some of the selected employees is already done for this period. Please Void Payment for selected employee to give salary change.")
            Language.setMessage(mstrModuleName, 10, "Some Salary Changes will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button.")
			Language.setMessage(mstrModuleName, 11, "Dear")
			Language.setMessage(mstrModuleName, 12, "This is to inform you that total")
			Language.setMessage(mstrModuleName, 13, "salary changes have been added to the salary list by importation.")
			Language.setMessage(mstrModuleName, 14, "from Machine")
			Language.setMessage(mstrModuleName, 15, "and IPAddress")
			Language.setMessage(mstrModuleName, 16, "Please click on the following link to Approve/Reject these changes.")
			Language.setMessage(mstrModuleName, 17, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 18, "Sorry, Some salary changes are already done from another machine. Please re-select file to do further operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'*************************************** S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION ************************************|
'
'
'
'
'
'Public Class frmImportSalaryChange

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmImportSalaryChange"
'    Private mblnCancel As Boolean = True
'    Private dsList As New DataSet
'    Private mdtTable As New DataTable
'    Private mintPeriodUnkId As Integer
'    Private mdtIncrementDate As DateTime
'    Private mintReasonUnkId As Integer
'    Private objIExcel As ExcelData
'    Private mdtFilteredTable As DataTable
'    Private m_Dataview As DataView

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mintSuccessCount As Integer = 0
'    Private trd As Thread
'    'S.SANDEEP [ 18 SEP 2012 ] -- END

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog() As Boolean
'        Try
'            Me.ShowDialog()
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try
'            txtFilePath.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillGirdView()
'        Try
'            dgvImportInfo.AutoGenerateColumns = False

'            objdgcolhCheck.DataPropertyName = "IsChecked"
'            dgcolhEmpCode.DataPropertyName = "EmpCode"
'            dgcolhEmpName.DataPropertyName = "EmpName"
'            dgcolhCurrScale.DataPropertyName = "currentscale"
'            dgcolhIncrement.DataPropertyName = "Increment"
'            dgcolhNewScale.DataPropertyName = "newscale"
'            dgcolhRemark.DataPropertyName = "remark" 'Sohail (10 Nov 2014)

'            dgcolhCurrScale.DefaultCellStyle.Format = GUI.fmtCurrency
'            dgcolhIncrement.DefaultCellStyle.Format = GUI.fmtCurrency
'            dgcolhNewScale.DefaultCellStyle.Format = GUI.fmtCurrency

'            dgvImportInfo.DataSource = m_Dataview
'            dgvImportInfo.Refresh()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
'        Try
'            If m_Dataview Is Nothing Then Exit Sub

'            For Each dtRow As DataRowView In m_Dataview
'                dtRow.Item("IsChecked") = objchkSelectAll.Checked
'            Next
'            dgvImportInfo.DataSource = m_Dataview
'            dgvImportInfo.Refresh()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 18 SEP 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub Send_Notification()
'        Try
'            If User._Object.Privilege._AllowToApproveSalaryChange = False Then
'                'Sohail (08 Mar 2016) -- Start
'                'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
'                Dim objEmployee As New clsEmployee_Master
'                Dim objPeriod As New clscommom_period_Tran
'                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodUnkId
'                Dim strEmpList As String = String.Join(",", (From p In mdtFilteredTable Select (p.Item("employeeunkid").ToString)).ToArray)
'                Dim dsEmp As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")", , )
'                Dim strDeptIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "departmentunkid") Select (If(IsDBNull(p.Item("departmentunkid")), "-99", p.Item("departmentunkid")).ToString)).ToArray)
'                Dim strJobIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "jobunkid") Select (If(IsDBNull(p.Item("jobunkid")), "-99", p.Item("jobunkid")).ToString)).ToArray)
'                Dim strClassGrpIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classgroupunkid") Select (If(IsDBNull(p.Item("classgroupunkid")), "-99", p.Item("classgroupunkid")).ToString)).ToArray)
'                Dim strClassIDs As String = String.Join(",", (From p In New DataView(dsEmp.Tables(0)).ToTable(True, "classunkid") Select (If(IsDBNull(p.Item("classunkid")), "-99", p.Item("classunkid")).ToString)).ToArray)
'                'Sohail (08 Mar 2016) -- End

'                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
'                Dim dUList As New DataSet
'                dUList = objUsr.Get_UserBy_PrivilegeId(671)
'                If dUList.Tables(0).Rows.Count > 0 Then
'                    For Each dRow As DataRow In dUList.Tables(0).Rows
'                        'Sohail (08 Mar 2016) -- Start
'                        'Issue - User access on On Salary change, All approvers are receiving Notifications even if employee with salary change not in his/her user access. #51 (KBC & Kenya Project Comments List.xls)
'                        'Dim objSMail As New clsSendMail
'                        ''Sohail (30 Dec 2013) -- Start
'                        ''Enhancement - Send link in salary change notification
'                        ''StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")))
'                        'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), 0)
'                        ''Sohail (30 Dec 2013) -- End
'                        'objSMail._ToEmail = CStr(dRow.Item("UEmail"))
'                        'objSMail._Subject = Language.getMessage(mstrModuleName, 1, "Notifications to Approve Employee Salary Change(s)")
'                        'objSMail._Message = StrMessage
'                        ''S.SANDEEP [ 28 JAN 2014 ] -- START
'                        ''Sohail (17 Dec 2014) -- Start
'                        ''Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
'                        ''objSMail._Form_Name = mstrModuleName
'                        'objSMail._Form_Name = "" 'Please pass Form Name for WEB
'                        ''Sohail (17 Dec 2014) -- End
'                        'objSMail._LogEmployeeUnkid = -1
'                        'objSMail._OperationModeId = enLogin_Mode.DESKTOP
'                        'objSMail._UserUnkid = User._Object._Userunkid
'                        'objSMail._SenderAddress = User._Object._Email
'                        'objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
'                        ''S.SANDEEP [ 28 JAN 2014 ] -- END
'                        'objSMail.SendMail()
'                        'objSMail = Nothing
'                        Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(dRow.Item("UId")), Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting)
'                        If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
'                            Dim mblnFlag As Boolean = False
'                            For Each AID As String In ConfigParameter._Object._UserAccessModeSetting.Trim.Split(CChar(","))
'                                Dim drRow() As DataRow = Nothing
'                                Select Case CInt(AID)

'                                    Case enAllocation.DEPARTMENT
'                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strDeptIDs & ") AND referenceunkid = " & enAllocation.DEPARTMENT)
'                                        If drRow.Length > 0 Then
'                                            mblnFlag = True
'                                        Else
'                                            mblnFlag = False
'                                            Exit For
'                                        End If

'                                    Case enAllocation.JOBS
'                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strJobIDs & ") AND referenceunkid = " & enAllocation.JOBS)
'                                        If drRow.Length > 0 Then
'                                            mblnFlag = True
'                                        Else
'                                            mblnFlag = False
'                                            Exit For
'                                        End If

'                                    Case enAllocation.CLASS_GROUP
'                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassGrpIDs & ") AND referenceunkid = " & enAllocation.CLASS_GROUP)
'                                        If drRow.Length > 0 Then
'                                            mblnFlag = True
'                                        Else
'                                            mblnFlag = False
'                                            Exit For
'                                        End If

'                                    Case enAllocation.CLASSES
'                                        drRow = dtUserAccess.Select("allocationunkid IN (" & strClassIDs & ") AND referenceunkid = " & enAllocation.CLASSES)
'                                        If drRow.Length > 0 Then
'                                            mblnFlag = True
'                                        Else
'                                            mblnFlag = False
'                                            Exit For
'                                        End If

'                                    Case Else
'                                        mblnFlag = False
'                                End Select
'                            Next

'                            If mblnFlag Then
'                                Dim objSMail As New clsSendMail
'                                StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), CInt(dRow.Item("UId")), 0)
'                                objSMail._ToEmail = CStr(dRow.Item("UEmail"))
'                                objSMail._Subject = Language.getMessage(mstrModuleName, 1, "Notifications to Approve Employee Salary Change(s)")
'                                objSMail._Message = StrMessage
'                                objSMail._Form_Name = "" 'Please pass Form Name for WEB
'                                objSMail._LogEmployeeUnkid = -1
'                                objSMail._OperationModeId = enLogin_Mode.DESKTOP
'                                objSMail._UserUnkid = User._Object._Userunkid
'                                objSMail._SenderAddress = User._Object._Email
'                                objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
'                                objSMail.SendMail()
'                                objSMail = Nothing
'                            End If
'                        End If
'                        'Sohail (08 Mar 2016) -- End
'                    Next
'                End If
'            End If
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
'        End Try
'    End Sub

'    Private Function Set_Notification_Approvals(ByVal StrUserName As String, ByVal intUserID As Integer, ByVal intSalaryincrementtranunkid As Integer) As String
'        'Sohail (30 Dec 2013) - [intUserID, intSalaryincrementtranunkid]
'        Dim StrMessage As New System.Text.StringBuilder
'        Try
'            StrMessage.Append("<HTML><BODY>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total <b>" & mintSuccessCount.ToString & "</b> salary changes have been added to the salary list by importation. </span></p>")
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b> </span></p>")
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
'            StrMessage.Append(vbCrLf)
'            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

'            'Sohail (30 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            'Sohail (30 Dec 2013) -- Start
'            'Enhancement - Send link in salary change notification
'            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please login to Main Aruti HR system to Approve/Reject this change.</span></p>")
'            Dim strLink As String
'            strLink = ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPg_SalaryIncrementList.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid.ToString & "|" & intUserID & "|0|" & mintPeriodUnkId.ToString & "|0"))
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please click on the following link to Approve/Reject these changes.</span></p>")
'            StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>")
'            StrMessage.Append("<br><br>")
'            StrMessage.Append(vbCrLf)
'            'Sohail (30 Dec 2013) -- End
'            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
'            StrMessage.Append("</span></p>")
'            StrMessage.Append("</BODY></HTML>")

'            Return StrMessage.ToString

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
'            Return ""
'        End Try
'    End Function
'    'S.SANDEEP [ 18 SEP 2012 ] -- END

'#End Region

'#Region " Form's Events "

'    Private Sub frmImportSalaryChange_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            'Sohail (10 Jul 2014) -- Start
'            'Enhancement - Custom Language.
'            clsSalaryIncrement.SetMessages()
'            objfrm._Other_ModuleNames = "clsSalaryIncrement"
'            'Sohail (10 Jul 2014) -- End
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmImportSalaryChange_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'    Private Sub frmImportSalaryChange_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objIExcel = New ExcelData
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            Language.setLanguage(Me.Name)
'            Call OtherSettings()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmImportSalaryChange_Load", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Buttons Events "
'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            mblnCancel = True
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
'        Dim blnFlag As Boolean = False
'        Dim objSalary As New clsSalaryIncrement
'        'Sohail (24 Sep 2012) -- Start
'        'TRA - ENHANCEMENT
'        Dim objPayment As New clsPayment_tran
'        Dim objTnA As New clsTnALeaveTran
'        Dim objPeriod As New clscommom_period_Tran
'        Dim strEmpList As String = ""
'        'Sohail (24 Sep 2012) -- End
'        Try
'            If mdtTable.Rows.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you can not import this file. Reason :There is no transaction in this file to import."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            m_Dataview.RowFilter = "rowtypeid = 0 "
'            mdtFilteredTable = m_Dataview.ToTable

'            If mdtFilteredTable.Rows.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you can not import this file. Reason :Either Employee Codes are not there in the system or transactions are already exist."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
'            If mdtFilteredTable.Rows.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            objSalary._Periodunkid = mintPeriodUnkId
'            objSalary._Incrementdate = mdtIncrementDate
'            objSalary._Userunkid = User._Object._Userunkid
'            'objSalary._Isgradechange = False 'Sohail (10 Nov 2014)
'            objSalary._Isfromemployee = False
'            objSalary._Reason_Id = mintReasonUnkId

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            If User._Object.Privilege._AllowToApproveSalaryChange = True Then
'                objSalary._Isapproved = True
'                objSalary._Approveruserunkid = User._Object._Userunkid
'            Else
'                objSalary._Isapproved = False
'            End If

'            'Sohail (10 Nov 2014) -- Start
'            'Enhancement - Change Grade option on Import Salary Change.
'            'For Each dtRow As DataRow In mdtFilteredTable.Rows
'            '    If strEmpList.Length <= 0 Then
'            '        strEmpList &= dtRow.Item("employeeunkid").ToString
'            '    Else
'            '        strEmpList &= "," & dtRow.Item("employeeunkid").ToString
'            '    End If
'            'Next
'            Dim lstEmp As List(Of String) = (From p In mdtFilteredTable Select (p.Item("employeeunkid").ToString)).ToList
'            strEmpList = String.Join(",", lstEmp.ToArray)
'            'Sohail (10 Nov 2014) -- End

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, mintPeriodUnkId, clsPayment_tran.enPayTypeId.PAYMENT, strEmpList)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodUnkId
'            dsList = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, mintPeriodUnkId, clsPayment_tran.enPayTypeId.PAYMENT, strEmpList, False)
'            'Sohail (21 Aug 2015) -- End
'            If dsList.Tables("Payment").Rows.Count > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Payment of Salary for some of the selected employees is already done for this period. Please Void Payment for selected employee to give salary change."), enMsgBoxStyle.Information)
'                Exit Try
'            End If


'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = mintPeriodUnkId
'            'Sohail (21 Aug 2015) -- End
'            If objTnA.IsPayrollProcessDone(mintPeriodUnkId, strEmpList, objPeriod._End_Date) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
'                Exit Try
'            End If
'            'Sohail (24 Sep 2012) -- End

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'blnFlag = objSalary.InsertAll(mdtFilteredTable)
'            blnFlag = objSalary.InsertAll(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtFilteredTable, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, , True, "")
'            'Sohail (21 Aug 2015) -- End

'            'S.SANDEEP [ 18 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mintSuccessCount = objSalary._SuccessCnt
'            'S.SANDEEP [ 18 SEP 2012 ] -- END

'            If blnFlag = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Salary Increment Process completed successfully."), enMsgBoxStyle.Information)
'                objchkSelectAll.Checked = False

'                mblnCancel = False
'                Me.Close()
'            End If

'            'S.SANDEEP [ 18 SEP 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If blnFlag = True Then
'                If mintSuccessCount > 0 Then
'                    trd = New Thread(AddressOf Send_Notification)
'                    trd.IsBackground = True
'                    trd.Start()
'                End If
'            End If
'            'S.SANDEEP [ 18 SEP 2012 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
'        Try
'            If m_Dataview Is Nothing Then Exit Sub

'            Dim savDialog As New SaveFileDialog
'            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx""
'            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
'                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Salary C") = True Then
'                    Process.Start(savDialog.FileName)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnGetFileFomat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetFileFomat.Click
'        If ConfigParameter._Object._ExportDataPath = "" Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        Dim dlgSaveFile As New SaveFileDialog
'        Dim strFilePath As String = String.Empty
'        Dim objEd As New clsEarningDeduction
'        Dim dsList As DataSet
'        Try
'            With dlgSaveFile
'                .InitialDirectory = ConfigParameter._Object._ExportDataPath
'                .Filter = "Excel files(*.xlsx)|*.xlsx""
'                .FilterIndex = 0
'                .OverwritePrompt = True

'                If .ShowDialog = Windows.Forms.DialogResult.OK Then
'                    strFilePath = .FileName

'                    dsList = (New clsSalaryIncrement).GetImportFileFormat("Format")

'                    objIExcel.Export(strFilePath, dsList)
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File Exported Successfully."), enMsgBoxStyle.Information)
'                End If
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnGetFileFomat_Click", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " CheckBox's Events "
'    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
'        Try
'            Call CheckAll(objchkSelectAll.Checked)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Other control's Events "
'    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
'        Dim ofdlgOpen As New OpenFileDialog
'        Dim ObjFile As FileInfo
'        Try
'            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
'            ofdlgOpen.Filter = "Excel files (*.xlsx)|*.xlsx"" '|XML files (*.xml)|*.xml"
'            ofdlgOpen.FilterIndex = 0

'            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
'                ObjFile = New FileInfo(ofdlgOpen.FileName)
'                txtFilePath.Text = ofdlgOpen.FileName

'                Select Case ofdlgOpen.FilterIndex
'                    Case 1, 2
'                        dsList = objIExcel.Import(txtFilePath.Text)
'                End Select

'                Dim frm As New frmSalaryChangeMapping
'                If frm.displayDialog(dsList) = False Then
'                    mblnCancel = True
'                    Exit Sub
'                End If

'                mdtTable = frm._DataTable
'                mintPeriodUnkId = frm._PeriodId
'                mdtIncrementDate = frm._IncrementDate
'                mintReasonUnkId = frm._ReasonId
'                m_Dataview = New DataView(mdtTable)
'                'Sohail (10 Nov 2014) -- Start
'                'Enhancement - Change Grade option on Import Salary Change.
'                m_Dataview.RowFilter = "rowtypeid <> 0 "
'                If m_Dataview.Count > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Some Salary Changes will not be imported. To Review Unsuccessful data Please click on Unsuccessful Data in operation menu button."), enMsgBoxStyle.Information)
'                End If
'                'Sohail (10 Nov 2014) -- End
'                m_Dataview.RowFilter = "rowtypeid = 0 "

'                Call FillGirdView()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
'        Try
'            btnImport.Enabled = True
'            If m_Dataview Is Nothing Then Exit Sub

'            m_Dataview.RowFilter = "rowtypeid = 0 "

'            For Each dtRow As DataRowView In m_Dataview
'                dtRow.Item("IsChecked") = False
'            Next

'            Call FillGirdView()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Sohail (10 Nov 2014) -- Start
'    'Enhancement - Change Grade option on Import Salary Change.
'    'Private Sub mnuEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEmployees.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 1 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuEmployees_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuHigherLastSalaryChangeDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHigherLastSalaryChangeDate.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 2 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuHigherLastSalaryChangeDate_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuSalaryChangeAlreadyDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSalaryChangeAlreadyDone.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 3 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuSalaryChangeAlreadyDone_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuIncrementNotGiven_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuIncrementNotGiven.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 4 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuIncrementNotGiven_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub mnuNewScaleExceedsMaximumScale_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNewScaleExceedsMaximumScale.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 5 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuNewScaleExceedsMaximumScale_Click", mstrModuleName)
'    '    End Try
'    'End Sub

'    ''Sohail (07 Sep 2013) -- Start
'    ''TRA - ENHANCEMENT
'    'Private Sub mnuLastIncrementNotApproved_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLastIncrementNotApproved.Click
'    '    Try
'    '        btnImport.Enabled = False
'    '        If m_Dataview Is Nothing Then Exit Sub

'    '        m_Dataview.RowFilter = "rowtypeid = 6 "

'    '        For Each dtRow As DataRowView In m_Dataview
'    '            dtRow.Item("IsChecked") = False
'    '        Next

'    '        Call FillGirdView()
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "mnuLastIncrementNotApproved_Click", mstrModuleName)
'    '    End Try
'    'End Sub
'    ''Sohail (07 Sep 2013) -- End

'    Private Sub mnuShowUnSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
'        Try
'            btnImport.Enabled = False
'            If m_Dataview Is Nothing Then Exit Sub

'            m_Dataview.RowFilter = "rowtypeid <> 0 "

'            For Each dtRow As DataRowView In m_Dataview
'                dtRow.Item("IsChecked") = False
'            Next

'            Call FillGirdView()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowAll.Click
'        Try
'            btnImport.Enabled = False
'            If m_Dataview Is Nothing Then Exit Sub

'            m_Dataview.RowFilter = ""

'            For Each dtRow As DataRowView In m_Dataview
'                dtRow.Item("IsChecked") = False
'            Next

'            Call FillGirdView()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuShowAll_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (10 Nov 2014) -- End

'#End Region


'End Class