﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSalaryIncrementList

#Region " Private Variable "

    Private objSalaryInc As clsSalaryIncrement
    Private ReadOnly mstrModuleName As String = "frmSalaryIncrementList"
    Private mstrAdvanceFilter As String = ""
    Private mblnCancel As Boolean = True
    Private mstrAppraisalFilter As String = ""
    Private mdicAppraisal As New Dictionary(Of Integer, Integer)
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private mstrSearchText As String = ""
    Private mdtView As DataView
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Display Dialog "
    Public Function DisplayDialog(Optional ByRef strAppraisalFilter As String = "", Optional ByVal dicAppraisal As Dictionary(Of Integer, Integer) = Nothing) As Boolean
        Try
            mstrAppraisalFilter = strAppraisalFilter
            mdicAppraisal = dicAppraisal

            Me.ShowDialog()

            strAppraisalFilter = mstrAppraisalFilter
            dicAppraisal = mdicAppraisal

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboPayYear.BackColor = GUI.ColorOptional
            cboPayPeriod.BackColor = GUI.ColorOptional
            cboSalaryChangeApprovalStatus.BackColor = GUI.ColorComp 'Sohail (24 Sep 2012)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeList", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("EmployeeList")
                .SelectedValue = 0 'Sohail (21 Jan 2020)
            End With
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            Call SetDefaultSearchText(cboEmployee)
            'Sohail (21 Jan 2020) -- End

            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            With cboPayYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Year")
            End With

            dsCombo = objMaster.getComboListForSalaryChangeApprovalStatus("SalaryApproval", True)
            With cboSalaryChangeApprovalStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsCombo.Tables("SalaryApproval"), "Id <> " & enSalaryChangeApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim strSearch As String = ""
        Try

            If User._Object.Privilege._AllowToViewSalaryChangeList = True Then

                Cursor.Current = Cursors.WaitCursor

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                dsList = objSalaryInc.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "SalaryInc", Nothing, Convert.ToInt32(cboEmployee.SelectedValue).ToString, CInt(cboPayPeriod.SelectedValue), CInt(cboPayYear.SelectedValue), "prsalaryincrement_tran.employeeunkid, prsalaryincrement_tran.incrementdate ", mstrAdvanceFilter, CInt(cboSalaryChangeApprovalStatus.SelectedValue), True)
                'Sohail (21 Jan 2020) -- End

                If mstrAppraisalFilter.Trim.Length > 0 Then
                    strSearch = mstrAppraisalFilter
                End If
                If strSearch.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("SalaryInc"), strSearch.Substring(4), "employeename", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables("SalaryInc"), "", "employeename", DataViewRowState.CurrentRows).ToTable
                End If

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'Dim lvItem As ListViewItem
                'Dim lvArray As New List(Of ListViewItem)
                'Sohail (21 Jan 2020) -- End

                'lvSalaryIncrement.Items.Clear() 'Sohail (21 Jan 2020)
                objchkSelectAll.Checked = False
                'lvSalaryIncrement.BeginUpdate() 'Sohail (21 Jan 2020)

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'For Each drRow As DataRow In dtTable.Rows
                '    lvItem = New ListViewItem
                '    lvItem.Text = ""
                '    lvItem.Tag = drRow("salaryincrementtranunkid").ToString
                '    lvItem.SubItems.Add(drRow("period_name").ToString)
                '    lvItem.SubItems(colhPayPeriod.Index).Tag = drRow("periodunkid").ToString
                '    lvItem.SubItems.Add(drRow("employeename").ToString)
                '    lvItem.SubItems(colhEmployeeName.Index).Tag = drRow("employeeunkid").ToString
                '    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("incrementdate").ToString).ToShortDateString)
                '    lvItem.SubItems.Add(Format(drRow("currentscale"), GUI.fmtCurrency))
                '    lvItem.SubItems.Add(Format(drRow("increment"), GUI.fmtCurrency))
                '    lvItem.SubItems.Add(Format(drRow("newscale"), GUI.fmtCurrency))
                '    lvItem.SubItems.Add(drRow("isfromemployee").ToString)
                '    If CBool(drRow.Item("isapproved")) = True Then
                '        lvItem.SubItems.Add("A")
                '        lvItem.SubItems(colhApprovePending.Index).Tag = "A"
                '    Else
                '        lvItem.SubItems.Add("P")
                '        lvItem.SubItems(colhApprovePending.Index).Tag = "P"
                '    End If
                '    lvItem.SubItems.Add(drRow("GradeGroup").ToString)
                '    lvItem.SubItems(colhGradeGroup.Index).Tag = CInt(drRow("gradegroupunkid"))

                '    lvItem.SubItems.Add(drRow("Grade").ToString)
                '    lvItem.SubItems(colhGrade.Index).Tag = CInt(drRow("gradeunkid"))

                '    lvItem.SubItems.Add(drRow("GradeLevel").ToString)
                '    lvItem.SubItems(colhGradeLevel.Index).Tag = CInt(drRow("gradelevelunkid"))
                '    lvArray.Add(lvItem)
                'Next
                'RemoveHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
                'lvSalaryIncrement.Items.AddRange(lvArray.ToArray)
                'AddHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
                'lvSalaryIncrement.GroupingColumn = colhEmployeeName
                'lvSalaryIncrement.DisplayGroups(True)
                'lvSalaryIncrement.GridLines = False

                'If lvSalaryIncrement.Items.Count > 4 Then
                '    colhIncrementAmt.Width = 107 - 18
                'Else
                '    colhIncrementAmt.Width = 107
                'End If
                'lvSalaryIncrement.EndUpdate()
                objdgcolhCheck.DataPropertyName = "IsChecked"
                objcolhIsGroup.DataPropertyName = "IsGrp"
                objcolhIncrementID.DataPropertyName = "salaryincrementtranunkid"
                colhPayPeriod.DataPropertyName = "period_name"
                objcolhPeriodUnkId.DataPropertyName = "periodunkid"
                colhEmployeeName.DataPropertyName = "employeename"
                objcolhEmployeeUnkId.DataPropertyName = "employeeunkid"
                colhIncrDate.DataPropertyName = "incrementdate"
                colhActualDate.DataPropertyName = "actualdate"
                colhScale.DataPropertyName = "currentscale"
                colhScale.DefaultCellStyle.Format = GUI.fmtCurrency
                colhIncrementAmt.DataPropertyName = "increment"
                colhIncrementAmt.DefaultCellStyle.Format = GUI.fmtCurrency
                colhNewScale.DataPropertyName = "newscale"
                colhNewScale.DefaultCellStyle.Format = GUI.fmtCurrency
                objcolhAuto.DataPropertyName = "isfromemployee"
                colhApprovePending.DataPropertyName = "isapproved"
                colhGradeGroup.DataPropertyName = "GradeGroup"
                objcolhGradegroupunkid.DataPropertyName = "gradegroupunkid"
                colhGrade.DataPropertyName = "Grade"
                objcolhGradeunkid.DataPropertyName = "gradeunkid"
                colhGradeLevel.DataPropertyName = "GradeLevel"
                objcolhGradelevelunkid.DataPropertyName = "gradelevelunkid"
                'Sohail (13 Feb 2020) -- Start
                'NMB Enhancement # : show employee code on salary change list.
                objcolhEmpCode.DataPropertyName = "employeecode"
                'Sohail (13 Feb 2020) -- End
                colhEmployeeName.Visible = False
                'Sohail (13 Feb 2020) -- Start
                'NMB Enhancement # : show employee code on salary change list.
                objcolhEmpCode.Visible = False
                'Sohail (13 Feb 2020) -- End

                mdtView = dsList.Tables(0).DefaultView

                With dgvSalaryIncrement
                    .AutoGenerateColumns = False
                    .DataSource = mdtView
                End With

                For Each dgvc As DataGridViewColumn In dgvSalaryIncrement.Columns
                    dgvc.SortMode = DataGridViewColumnSortMode.NotSortable
                Next
                'Sohail (21 Jan 2020) -- End

                btnApprove.Visible = False
                btnDisApprove.Visible = False
                If CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enSalaryChangeApprovalStatus.Approved AndAlso btnDisApprove.Enabled = True Then
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'btnDisApprove.Visible = True
                    'Sohail (21 Jan 2020) -- End
                ElseIf CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enSalaryChangeApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
                    btnApprove.Visible = True
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    btnDisApprove.Visible = True
                    'Sohail (21 Jan 2020) -- End
                End If

                Cursor.Current = Cursors.Default

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            'Sohail (20 Jan 2017) -- Start
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'objbtnSearch.ShowResult(lvSalaryIncrement.Items.Count.ToString)
            If mdtView IsNot Nothing Then
                Call objbtnSearch.ShowResult(mdtView.ToTable.Select("IsGrp = 0").Length.ToString)
            Else
                Call objbtnSearch.ShowResult("0")
            End If
            'Sohail (21 Jan 2020) -- End
            'Sohail (20 Jan 2017) -- End
            dsList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddSalaryIncrement
            btnEdit.Enabled = User._Object.Privilege._EditSalaryIncrement
            btnDelete.Enabled = User._Object.Privilege._DeleteSalaryIncrement
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowGlobalSalaryIncrement
            mnuImportSalaryChange.Enabled = User._Object.Privilege._AllowGlobalSalaryIncrement
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveSalaryChange
            btnDisApprove.Enabled = User._Object.Privilege._AllowToApproveSalaryChange

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            mnuSalaryHistroy.Enabled = User._Object.Privilege._AllowToHistoricalSalaryChange
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    'Private Sub CheckAll(ByVal blnCheckAll As Boolean)
    '    Try
    '        For Each lvItem As ListViewItem In lvSalaryIncrement.Items
    '            If CBool(lvItem.SubItems(colhAuto.Index).Text) = True Then Continue For
    '            RemoveHandler lvSalaryIncrement.ItemCheck, AddressOf lvSalaryIncrement_ItemCheck
    '            RemoveHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
    '            lvItem.Checked = blnCheckAll
    '            AddHandler lvSalaryIncrement.ItemCheck, AddressOf lvSalaryIncrement_ItemCheck
    '            AddHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CheckAllEdHeads", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 25, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " ComboBox's Events "

    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing

        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True)
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                mdtPeriodStart = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriodStart = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) < 0 Then Call SetDefaultSearchText(cbo)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End

#End Region

#Region " Form's Event "

    Private Sub frmSalaryIncrementList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSalaryInc = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryIncrementList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try

            Select Case e.KeyCode
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Escape
                    btnClose.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSalaryIncrementList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSalaryInc = New clsSalaryIncrement
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If lvSalaryIncrement.Items.Count > 0 Then lvSalaryIncrement.Items(0).Selected = True
            'lvSalaryIncrement.Select()
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSalaryIncrement.SetMessages()
            objfrm._Other_ModuleNames = "clsSalaryIncrement"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objSalary_AddEdit As New frmSalaryIncrement_AddEdit
            If objSalary_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, dgvSalaryIncrement.DoubleClick
        If User._Object.Privilege._EditSalaryIncrement = False Then Exit Sub
        Dim objTnA As New clsTnALeaveTran
        Dim objPayment As New clsPayment_tran
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim intTnAID As Integer
        'Sohail (06 Dec 2019) -- Start
        'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
        Dim blnFirstSalaryScale As Boolean = False
        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        'Dim intCurrentPeriodId As Integer = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
        Dim intCurrentPeriodId As Integer = 0
        'Sohail (21 Jan 2020) -- End
        'Sohail (06 Dec 2019) -- End
        Try
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If lvSalaryIncrement.SelectedItems.Count < 1 Then
            If dgvSalaryIncrement.SelectedRows.Count <= 0 OrElse CBool(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIsGroup.Index).Value) = True Then
                'Sohail (21 Jan 2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Try
            End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If CBool(lvSalaryIncrement.SelectedItems(0).SubItems(colhAuto.Index).Text) = True Then
            intCurrentPeriodId = CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value)
            If CBool(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhAuto.Index).Value) = True Then
                'Sohail (21 Jan 2020) -- End
                'Sohail (06 Dec 2019) -- Start
                'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Edit this entry. Reason: This is Auto Generated entry."), enMsgBoxStyle.Information)
                'lvSalaryIncrement.Select()
                'Exit Try
                Dim objMaster As New clsMasterData
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'intCurrentPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(lvSalaryIncrement.SelectedItems(0).SubItems(colhIncrDate.Index).Text), FinancialYear._Object._YearUnkid, enStatusType.Open, True, False, Nothing, False, True)
                intCurrentPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgvSalaryIncrement.SelectedRows(0).Cells(colhIncrDate.Index).FormattedValue), FinancialYear._Object._YearUnkid, enStatusType.Open, True, False, Nothing, False, True)
                'Sohail (21 Jan 2020) -- End
                If intCurrentPeriodId <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Edit this entry. Reason: This is Auto Generated entry and increment date period is not open."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                Exit Try
            End If
                blnFirstSalaryScale = True
                'Sohail (06 Dec 2019) -- End
            End If
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intCurrentPeriodId
            'Sohail (06 Dec 2019) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Try
            End If

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'If lvSalaryIncrement.SelectedItems(0).SubItems(colhApprovePending.Index).Tag.ToString = "A" Then
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If blnFirstSalaryScale = False AndAlso lvSalaryIncrement.SelectedItems(0).SubItems(colhApprovePending.Index).Tag.ToString = "A" Then
            '    'Sohail (06 Dec 2019) -- End
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! This Salary Change is already Approved. Please Disapprove this Salary Change to perform further operation."), enMsgBoxStyle.Information)
            '    lvSalaryIncrement.Select()
            '    Exit Try
            'End If
            'Sohail (21 Jan 2020) -- End

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If objTnA.IsPayrollProcessDone(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
            If objTnA.IsPayrollProcessDone(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value), dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value.ToString, objPeriod._End_Date) = True Then
                'Sohail (21 Jan 2020) -- End
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to edit salary change."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to edit salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 23, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), intCurrentPeriodId)
            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), intCurrentPeriodId)
            'Sohail (21 Jan 2020) -- End
            'Sohail (06 Dec 2019) -- End
            If intTnAID > 0 Then
                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open global void payment screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit this information. Reason: Payment of Salary is already done for this period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit this information. Reason: Payment of Salary is already done for this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 24, "Do you want to void Payment?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmGlobalVoidPayment
                        objFrm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT)
                    End If
                    'Sohail (19 Apr 2019) -- End
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If
            End If
            If mstrAppraisalFilter.Trim = "" Then
                'Sohail (06 Dec 2019) -- Start
                'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
                'If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), intCurrentPeriodId, enAppraisal_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                If objSalaryInc.isUsedInAppraisal(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), intCurrentPeriodId, enAppraisal_Modes.SALARY_INCREMENT, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value)) = True Then
                    'Sohail (21 Jan 2020) -- End
                    'Sohail (06 Dec 2019) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Appraisal."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If
                'Sohail (06 Dec 2019) -- Start
                'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
                'If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), intCurrentPeriodId, enTrainingAward_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                If objSalaryInc.isUsedInTrainingEnrollment(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), intCurrentPeriodId, enTrainingAward_Modes.SALARY_INCREMENT, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value)) = True Then
                    'Sohail (21 Jan 2020) -- End
                    'Sohail (06 Dec 2019) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Training Enrollment."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), True)
            dsList = objSalaryInc.getLastIncrement("Incr", CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), True)
            'Sohail (21 Jan 2020) -- End
            If dsList.Tables("Incr").Rows.Count > 0 Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If eZeeDate.convertDate(CDate(lvSalaryIncrement.SelectedItems(0).SubItems(colhIncrDate.Index).Text)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                If eZeeDate.convertDate(CDate(dgvSalaryIncrement.SelectedRows(0).Cells(colhIncrDate.Index).FormattedValue)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                    'Sohail (21 Jan 2020) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to edit selected salary change."), enMsgBoxStyle.Information)
                    Exit Try
                End If
            End If

            Dim objfrmSalaryIncrement As New frmSalaryIncrement_AddEdit

            Dim intSelectedIndex As Integer
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'intSelectedIndex = lvSalaryIncrement.SelectedItems(0).Index
            intSelectedIndex = dgvSalaryIncrement.SelectedRows(0).Index
            'Sohail (21 Jan 2020) -- End
            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # 19 : System should allow to edit first entry salary scale.
            'If objfrmSalaryIncrement.displayDialog(CInt(lvSalaryIncrement.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If objfrmSalaryIncrement.displayDialog(CInt(lvSalaryIncrement.SelectedItems(0).Tag), enAction.EDIT_ONE, , blnFirstSalaryScale) Then
            If objfrmSalaryIncrement.displayDialog(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value), enAction.EDIT_ONE, , blnFirstSalaryScale) Then
                'Sohail (21 Jan 2020) -- End
                'Sohail (06 Dec 2019) -- End
                Call FillList()
            End If
            objfrmSalaryIncrement = Nothing

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'lvSalaryIncrement.Items(intSelectedIndex).Selected = True
            'lvSalaryIncrement.EnsureVisible(intSelectedIndex)
            'lvSalaryIncrement.Select()
            'Sohail (21 Jan 2020) -- End
            If objfrmSalaryIncrement IsNot Nothing Then objfrmSalaryIncrement.Dispose()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            objTnA = Nothing
            objPayment = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objTnA As New clsTnALeaveTran
        Dim objPayment As New clsPayment_tran
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim intTnAID As Integer
        Try
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If lvSalaryIncrement.SelectedItems.Count < 1 Then
            If dgvSalaryIncrement.SelectedRows.Count <= 0 OrElse CBool(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIsGroup.Index).Value) = True Then
                'Sohail (21 Jan 2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Try
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If CBool(lvSalaryIncrement.SelectedItems(0).SubItems(colhAuto.Index).Text) = True Then
            If CBool(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhAuto.Index).Value) = True Then
                'Sohail (21 Jan 2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot Delete this entry. Reason: This is Auto Generated entry."), enMsgBoxStyle.Information)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Try
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value)
            'Sohail (21 Jan 2020) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Try
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If lvSalaryIncrement.SelectedItems(0).SubItems(colhApprovePending.Index).Tag.ToString = "A" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! This Salary Change is already Approved. Please Disapprove this Salary Change to perform further operation."), enMsgBoxStyle.Information)
            '    lvSalaryIncrement.Select()
            '    Exit Try
            'End If
            'Sohail (21 Jan 2020) -- End

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'If objTnA.IsPayrollProcessDone(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
            If objTnA.IsPayrollProcessDone(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value), dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value.ToString, objPeriod._End_Date) = True Then
                'Sohail (21 Jan 2020) -- End
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to void salary change."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to void salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 23, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value))
            'Sohail (21 Jan 2020) -- End
            If intTnAID > 0 Then
                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open global void payment screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Delete this information. Reason: Payment of Salary is already done for this period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Delete this information. Reason: Payment of Salary is already done for this period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 24, "Do you want to void Payment?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmGlobalVoidPayment
                        objFrm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT)
                    End If
                    'Sohail (19 Apr 2019) -- End
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If
            End If

            If mstrAppraisalFilter.Trim = "" Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                If objSalaryInc.isUsedInAppraisal(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value), enAppraisal_Modes.SALARY_INCREMENT, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value)) = True Then
                    'Sohail (21 Jan 2020) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Appraisal."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
                If objSalaryInc.isUsedInTrainingEnrollment(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value), enTrainingAward_Modes.SALARY_INCREMENT, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value)) = True Then
                    'Sohail (21 Jan 2020) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Training Enrollment."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Try
                End If
            End If

            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), True)
            dsList = objSalaryInc.getLastIncrement("Incr", CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), True)
            'Sohail (21 Jan 2020) -- End
            If dsList.Tables("Incr").Rows.Count > 0 Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If eZeeDate.convertDate(CDate(lvSalaryIncrement.SelectedItems(0).SubItems(colhIncrDate.Index).Text)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                If eZeeDate.convertDate(CDate(dgvSalaryIncrement.SelectedRows(0).Cells(colhIncrDate.Index).FormattedValue)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
                    'Sohail (21 Jan 2020) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to void selected salary change."), enMsgBoxStyle.Information)
                    Exit Try
                End If
            End If

            Dim intSelectedIndex As Integer
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'intSelectedIndex = lvSalaryIncrement.SelectedItems(0).Index
            intSelectedIndex = dgvSalaryIncrement.SelectedRows(0).Index
            'Sohail (21 Jan 2020) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Salary Increment Information?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'objSalaryInc.Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvSalaryIncrement.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
                'Sohail (24 Feb 2022) -- Start
                'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                'objSalaryInc.Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
                objSalaryInc.Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhIncrementID.Index).Value), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, True, ConfigParameter._Object._CurrentDateAndTime, True, "")
                'Sohail ((24 Feb 2022) -- End
                'Sohail (21 Jan 2020) -- End
                mblnCancel = False
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If mdicAppraisal.ContainsKey(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag)) = False Then
                'mdicAppraisal.Add(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
                If mdicAppraisal.ContainsKey(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value)) = False Then
                    mdicAppraisal.Add(CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhEmployeeUnkId.Index).Value), CInt(dgvSalaryIncrement.SelectedRows(0).Cells(objcolhPeriodUnkId.Index).Value))
                    'Sohail (21 Jan 2020) -- End
                End If

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.SelectedItems(0).Remove()

                'If lvSalaryIncrement.Items.Count <= 0 Then
                '    Exit Try
                'End If

                'If lvSalaryIncrement.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvSalaryIncrement.Items.Count - 1
                '    lvSalaryIncrement.Items(intSelectedIndex).Selected = True
                '    lvSalaryIncrement.EnsureVisible(intSelectedIndex)
                'ElseIf lvSalaryIncrement.Items.Count <> 0 Then
                '    lvSalaryIncrement.Items(intSelectedIndex).Selected = True
                '    lvSalaryIncrement.EnsureVisible(intSelectedIndex)
                'End If
                Call FillList()
                'Sohail (21 Jan 2020) -- End
                End If
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'lvSalaryIncrement.Select()
            dgvSalaryIncrement.Focus()
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objTnA = Nothing
            objPayment = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboEmployee
                objfrm.DataSource = CType(.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPayYear.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            If cboSalaryChangeApprovalStatus.Items.Count > 0 Then cboSalaryChangeApprovalStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisApprove.Click
        'Sohail (05 Mar 2013) -- Start
        'TRA - ENHANCEMENT
        Dim objTnA As New clsTnALeaveTran
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (05 Mar 2013) -- End
        Dim dsList As DataSet 'Sohail (19 Sep 2013)
        Dim strMsg As String = ""
        Dim blnIsApprove As Boolean
        Dim btn As Button = CType(sender, Button)
        If btn.Name = btnApprove.Name Then
            blnIsApprove = True
            strMsg = Language.getMessage(mstrModuleName, 11, "Are you sure you want to Approve selected Salary Change?")
        ElseIf btn.Name = btnDisApprove.Name Then
            blnIsApprove = False
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'strMsg = Language.getMessage(mstrModuleName, 12, "Are you sure you want to Dispprove selected Salary Change?")
            strMsg = Language.getMessage(mstrModuleName, 28, "By disapproving salary change, Selected salary change transactions will be voided.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Are you sure you want to Dispprove selected Salary Change?")
            'Sohail (21 Jan 2020) -- End
        End If
        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        'If lvSalaryIncrement.CheckedItems.Count <= 0 Then
        If mdtView.Table.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
            'Sohail (21 Jan 2020) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Salary Change to Approve/Disapprove."), enMsgBoxStyle.Information)
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'lvSalaryIncrement.Focus()
            dgvSalaryIncrement.Focus()
            'Sohail (21 Jan 2020) -- End
            Exit Sub
            'Sohail (05 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            '    Exit Sub
            'Sohail (05 Mar 2013) -- End
        End If

        'Sohail (05 Mar 2013) -- Start
        'TRA - ENHANCEMENT
        'Sohail (21 Jan 2020) -- Start
        'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
        'For Each lvItem As ListViewItem In lvSalaryIncrement.CheckedItems
        'If CBool(lvItem.SubItems(colhAuto.Index).Text) = True Then
        For Each dr As DataRow In mdtView.Table.Select("IsGrp = 0 AND IsChecked = 1")
            If CBool(dr.Item("isfromemployee")) = True Then
                'Sohail (21 Jan 2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Some of the checked entries are Auto Generated."), enMsgBoxStyle.Information)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'lvSalaryIncrement.Select()
                dgvSalaryIncrement.Focus()
                'Sohail (21 Jan 2020) -- End
                Exit Sub

            Else
                objPeriod = New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dr.Item("periodunkid"))
                'Sohail (21 Jan 2020) -- End
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Period is closed for some of the checked entries."), enMsgBoxStyle.Information)
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'lvSalaryIncrement.Select()
                    dgvSalaryIncrement.Focus()
                    'Sohail (21 Jan 2020) -- End
                    Exit Sub
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'ElseIf objTnA.IsPayrollProcessDone(CInt(lvItem.SubItems(colhPayPeriod.Index).Tag), lvItem.SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
                ElseIf objTnA.IsPayrollProcessDone(CInt(dr.Item("periodunkid")), dr.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then
                    'Sohail (21 Jan 2020) -- End
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 23, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                If btn.Name = btnDisApprove.Name Then
                    Dim intTnAID As Integer = 0
                    intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(dr.Item("employeeunkid")), CInt(dr.Item("periodunkid")))
                    If intTnAID > 0 Then
                        Dim objPayment As New clsPayment_tran
                        If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You cannot Disapprove some of the salary change. Reason: Payment of Salary is already done for this period for those employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 24, "Do you want to void Payment?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Dim objFrm As New frmGlobalVoidPayment
                                objFrm.displayDialog(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT)
                            End If
                            Exit Sub
                        End If
                    End If
                End If
                'Sohail (21 Jan 2020) -- End

                'Sohail (19 Sep 2013) -- Start
                'TRA - ENHANCEMENT
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'dsList = objSalaryInc.getLastIncrement("LastIncr", CInt(lvItem.SubItems(colhEmployeeName.Index).Tag), True)
                dsList = objSalaryInc.getLastIncrement("LastIncr", CInt(dr.Item("employeeunkid")), True)
                'Sohail (21 Jan 2020) -- End
                If dsList.Tables("LastIncr").Rows.Count > 0 Then
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'If CInt(dsList.Tables("LastIncr").Rows(0).Item("salaryincrementtranunkid")) <> CInt(lvItem.Tag) Then
                    If CInt(dsList.Tables("LastIncr").Rows(0).Item("salaryincrementtranunkid")) <> CInt(dr.Item("salaryincrementtranunkid")) Then
                        'Sohail (21 Jan 2020) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can Approve/Disapprove each employee's Last Salary Change only."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'Sohail (19 Sep 2013) -- End

            End If
        Next

        If (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            Exit Sub
        End If
        'Sohail (05 Mar 2013) -- End

        Dim blnResult As Boolean
        Dim blnRefresh As Boolean = False
        Try
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'For Each lvItem As ListViewItem In lvSalaryIncrement.CheckedItems

            '    objSalaryInc = New clsSalaryIncrement
            '    With objSalaryInc
            '        ._Salaryincrementtranunkid = CInt(lvItem.Tag)
            '        ._Isapproved = blnIsApprove
            '        ._Approveruserunkid = User._Object._Userunkid


            '        'Anjan (05 Dec 2012)-Start
            '        'ISSUE : grades were not getting updated to employee master from salary increment.
            '        'blnResult = .Update(False)
            '        'Sohail (21 Aug 2015) -- Start
            '        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '        'blnResult = .Update(True)
            '        blnResult = .Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , True, "")
            '        'Sohail (21 Aug 2015) -- End
            '        'Anjan (05 Dec 2012)-End 


            '        If blnResult = False AndAlso objSalaryInc._Message <> "" Then
            '            eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
            '            Exit Try
            '        End If
            '        blnRefresh = True
            '    End With
            'Next

            'If blnRefresh = True Then
            '    Call FillList()
            'End If
            Dim strRemarks As String = ""
            If btn.Name = btnDisApprove.Name Then
                Dim frm As New frmRemark
                frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 27, "Comments")

                If frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll) = False Then
                    Exit Sub
                End If
            End If

            Dim drRow() As DataRow = mdtView.ToTable.Select("IsGrp = 0 AND IsChecked = 1 ")
            For Each dr As DataRow In drRow
                objSalaryInc = New clsSalaryIncrement
                With objSalaryInc
                    ._Salaryincrementtranunkid = CInt(dr.Item("salaryincrementtranunkid"))
                    ._Isapproved = blnIsApprove
                    ._Approveruserunkid = User._Object._Userunkid

                    If btn.Name = btnApprove.Name Then
                        'Sohail (24 Feb 2022) -- Start
                        'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                        'blnResult = .Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , True, "")
                        blnResult = .Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, ConfigParameter._Object._CurrentDateAndTime, True, , True, "")
                        'Sohail ((24 Feb 2022) -- End
                    ElseIf btn.Name = btnDisApprove.Name Then
                        'Sohail (24 Feb 2022) -- Start
                        'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                        'blnResult = .Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(dr.Item("salaryincrementtranunkid")), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, strRemarks, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
                        blnResult = .Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(dr.Item("salaryincrementtranunkid")), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, strRemarks, True, ConfigParameter._Object._CurrentDateAndTime, True, "")
                        'Sohail ((24 Feb 2022) -- End
                    End If

                    If blnResult = False AndAlso objSalaryInc._Message <> "" Then
                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next

            If blnRefresh = True Then
                Call FillList()
            End If
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview's Events "

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    'Private Sub lvSalaryIncrement_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
    '    Try
    '        If CBool(lvSalaryIncrement.Items(e.Index).SubItems(colhAuto.Index).Text) = True AndAlso e.NewValue = CheckState.Checked Then
    '            e.NewValue = CheckState.Unchecked
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSalaryIncrement_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvSalaryIncrement_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        If lvSalaryIncrement.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvSalaryIncrement.CheckedItems.Count < lvSalaryIncrement.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvSalaryIncrement.CheckedItems.Count = lvSalaryIncrement.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSalaryIncrement_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (21 Jan 2020) -- End

#End Region

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
#Region " Datagridview's Events "

    Private Sub dgvSalaryIncrement_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSalaryIncrement.CellContentClick
        Try
            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then
                If Me.dgvSalaryIncrement.IsCurrentCellDirty Then
                    Me.dgvSalaryIncrement.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhIsGroup.Index).Value) = True Then
                    drRow = mdtView.Table.Select(objcolhEmployeeUnkId.DataPropertyName & " = '" & dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhEmployeeUnkId.Index).Value.ToString() & "' AND " & objcolhAuto.DataPropertyName & " = 0 ", "")
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(dgvSalaryIncrement.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                        Next
                    End If
                Else
                    If CBool(dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhAuto.Index).Value) = True Then
                        dgvSalaryIncrement.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = False
                        dgvSalaryIncrement.CancelEdit()
                    End If
                End If

                drRow = mdtView.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If mdtView.ToTable.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalaryIncrement_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSalaryIncrement_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvSalaryIncrement.DataError

    End Sub

    Private Sub dgvSalaryIncrement_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvSalaryIncrement.CellFormatting
        Try
            If CBool(dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhIsGroup.Index).Value) = False Then
                If e.ColumnIndex = colhIncrDate.Index Then
                    If IsDBNull(e.Value) = False Then 'Sohail (13 Feb 2020)
                    e.Value = eZeeDate.convertDate(e.Value.ToString).ToShortDateString
                    End If 'Sohail (13 Feb 2020)
                ElseIf e.ColumnIndex = colhActualDate.Index Then
                    If IsDBNull(e.Value) = False Then 'Sohail (13 Feb 2020)
                    e.Value = eZeeDate.convertDate(e.Value.ToString).ToShortDateString
                    End If 'Sohail (13 Feb 2020)
                ElseIf e.ColumnIndex = colhApprovePending.Index Then
                    If CBool(e.Value) = True Then
                        e.Value = "A"
                    Else
                        e.Value = "P"
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalaryIncrement_CellFormatting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSalaryIncrement_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvSalaryIncrement.CellPainting
        Try
            If e.RowIndex >= 0 AndAlso e.RowIndex < dgvSalaryIncrement.RowCount - 1 AndAlso CBool(dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhIsGroup.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = objdgColhBlank.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To dgvSalaryIncrement.Columns.Count - 1
                        totWidth += dgvSalaryIncrement.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    'Sohail (13 Feb 2020) -- Start
                    'NMB Enhancement # : show employee code on salary change list.
                    'e.Graphics.DrawString(CType(dgvSalaryIncrement.Rows(e.RowIndex).Cells(colhEmployeeName.Index).Value.ToString, String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    e.Graphics.DrawString(CType(dgvSalaryIncrement.Rows(e.RowIndex).Cells(colhEmployeeName.Index).Value.ToString & " : [" & dgvSalaryIncrement.Rows(e.RowIndex).Cells(objcolhEmpCode.Index).Value.ToString & "]", String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)
                    'Sohail (13 Feb 2020) -- End

                End If

                e.Handled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSalaryIncrement_CellPainting", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (21 Jan 2020) -- End

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'Call CheckAll(objchkSelectAll.Checked)
            If mdtView Is Nothing Then Exit Sub
            RemoveHandler dgvSalaryIncrement.CellContentClick, AddressOf dgvSalaryIncrement_CellContentClick
            For Each dr As DataRow In mdtView.Table.Select("isfromemployee = 0 ")
                dr.Item(objdgcolhCheck.DataPropertyName) = CBool(objchkSelectAll.CheckState)
            Next
            dgvSalaryIncrement.Refresh()
            AddHandler dgvSalaryIncrement.CellContentClick, AddressOf dgvSalaryIncrement_CellContentClick
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Menu Event(s) "

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Dim objFrm As New frmSalaryGlobalAssign
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.DisplayDialog() = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportSalaryChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportSalaryChange.Click
        Dim frm As New frmImportSalaryChange
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog() = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "File Imported Successfully."), enMsgBoxStyle.Information)
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportSalaryChange_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSalaryHistroy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSalaryHistroy.Click
        Dim frm As New frmEmployeeSalaryHistory_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSalaryHistroy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprove.GradientBackColor = GUI._ButttonBackColor 
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

			Me.btnDisApprove.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDisApprove.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblAccess.Text = Language._Object.getCaption(Me.lblAccess.Name, Me.lblAccess.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblSalaryChangeApprovalStatus.Text = Language._Object.getCaption(Me.lblSalaryChangeApprovalStatus.Name, Me.lblSalaryChangeApprovalStatus.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnDisApprove.Text = Language._Object.getCaption(Me.btnDisApprove.Name, Me.btnDisApprove.Text)
            Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)
            Me.mnuImportSalaryChange.Text = Language._Object.getCaption(Me.mnuImportSalaryChange.Name, Me.mnuImportSalaryChange.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuSalaryHistroy.Text = Language._Object.getCaption(Me.mnuSalaryHistroy.Name, Me.mnuSalaryHistroy.Text)
			Me.colhPayPeriod.HeaderText = Language._Object.getCaption(Me.colhPayPeriod.Name, Me.colhPayPeriod.HeaderText)
			Me.colhEmployeeName.HeaderText = Language._Object.getCaption(Me.colhEmployeeName.Name, Me.colhEmployeeName.HeaderText)
			Me.colhIncrDate.HeaderText = Language._Object.getCaption(Me.colhIncrDate.Name, Me.colhIncrDate.HeaderText)
			Me.colhActualDate.HeaderText = Language._Object.getCaption(Me.colhActualDate.Name, Me.colhActualDate.HeaderText)
			Me.colhScale.HeaderText = Language._Object.getCaption(Me.colhScale.Name, Me.colhScale.HeaderText)
			Me.colhIncrementAmt.HeaderText = Language._Object.getCaption(Me.colhIncrementAmt.Name, Me.colhIncrementAmt.HeaderText)
			Me.colhNewScale.HeaderText = Language._Object.getCaption(Me.colhNewScale.Name, Me.colhNewScale.HeaderText)
			Me.colhGradeGroup.HeaderText = Language._Object.getCaption(Me.colhGradeGroup.Name, Me.colhGradeGroup.HeaderText)
			Me.colhGrade.HeaderText = Language._Object.getCaption(Me.colhGrade.Name, Me.colhGrade.HeaderText)
			Me.colhGradeLevel.HeaderText = Language._Object.getCaption(Me.colhGradeLevel.Name, Me.colhGradeLevel.HeaderText)
			Me.colhApprovePending.HeaderText = Language._Object.getCaption(Me.colhApprovePending.Name, Me.colhApprovePending.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Salary Increment Information?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot Edit this information. Reason: Payment of Salary is already done for this period.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot Delete this information. Reason: Payment of Salary is already done for this period.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot Edit this entry. Reason: This is Auto Generated entry and increment date period is not open.")
            Language.setMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed.")
            Language.setMessage(mstrModuleName, 8, "Sorry, You cannot Delete this entry. Reason: This is Auto Generated entry.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Appraisal.")
            Language.setMessage(mstrModuleName, 10, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Training Enrollment.")
            Language.setMessage(mstrModuleName, 11, "Are you sure you want to Approve selected Salary Change?")
            Language.setMessage(mstrModuleName, 12, "Are you sure you want to Dispprove selected Salary Change?")
            Language.setMessage(mstrModuleName, 13, "Please Check atleast one Salary Change to Approve/Disapprove.")
            Language.setMessage(mstrModuleName, 14, "File Imported Successfully.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to void salary change.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to void selected salary change.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to edit selected salary change.")
            Language.setMessage(mstrModuleName, 18, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to edit salary change.")
            Language.setMessage(mstrModuleName, 20, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Some of the checked entries are Auto Generated.")
            Language.setMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Period is closed for some of the checked entries.")
            Language.setMessage(mstrModuleName, 22, "Sorry, You can Approve/Disapprove each employee's Last Salary Change only.")
			Language.setMessage(mstrModuleName, 23, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 24, "Do you want to void Payment?")
			Language.setMessage(mstrModuleName, 25, "Type to Search")
			Language.setMessage(mstrModuleName, 26, "Sorry, You cannot Disapprove some of the salary change. Reason: Payment of Salary is already done for this period for those employee.")
			Language.setMessage(mstrModuleName, 27, "Comments")
			Language.setMessage(mstrModuleName, 28, "By disapproving salary change, Selected salary change transactions will be voided.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'*************************************** S.SANDEEP [09 MAY 2016] - TRANSACTION SCREEN FOR PROMOTION ************************************|
'
'
'
'
'
'Public Class frmSalaryIncrementList

'#Region "Private Variable"

'    Private objSalaryInc As clsSalaryIncrement
'    Private ReadOnly mstrModuleName As String = "frmSalaryIncrementList"
'    'Anjan (21 Nov 2011)-Start
'    'ENHANCEMENT : TRA COMMENTS
'    Private mstrAdvanceFilter As String = ""
'    'Anjan (21 Nov 2011)-End 

'    'Sohail (10 Feb 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private mblnCancel As Boolean = True
'    Private mstrAppraisalFilter As String = ""
'    Private mdicAppraisal As New Dictionary(Of Integer, Integer)
'    'Sohail (10 Feb 2012) -- End

'    'Sohail (21 Aug 2015) -- Start
'    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'    Private mdtPeriodStart As Date = Nothing
'    Private mdtPeriodEnd As Date = Nothing
'    'Sohail (21 Aug 2015) -- End

'#End Region

'    'Sohail (10 Feb 2012) -- Start
'    'TRA - ENHANCEMENT
'#Region " Display Dialog "
'    Public Function DisplayDialog(Optional ByRef strAppraisalFilter As String = "", Optional ByVal dicAppraisal As Dictionary(Of Integer, Integer) = Nothing) As Boolean
'        Try
'            mstrAppraisalFilter = strAppraisalFilter
'            mdicAppraisal = dicAppraisal

'            Me.ShowDialog()

'            strAppraisalFilter = mstrAppraisalFilter
'            dicAppraisal = mdicAppraisal

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region
'    'Sohail (10 Feb 2012) -- End

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try
'            cboEmployee.BackColor = GUI.ColorOptional
'            cboPayYear.BackColor = GUI.ColorOptional
'            cboPayPeriod.BackColor = GUI.ColorOptional
'            cboSalaryChangeApprovalStatus.BackColor = GUI.ColorComp 'Sohail (24 Sep 2012)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim objEmployee As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim dsCombo As DataSet = Nothing
'        Try
'            'Anjan [10 June 2015] -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsCombo = objEmployee.GetEmployeeList("Employee", True)
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'            'Else
'            'dsCombo = objEmployee.GetEmployeeList("Employee", True)
'            'End If
'            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                           User._Object._Userunkid, _
'                                           FinancialYear._Object._YearUnkid, _
'                                           Company._Object._Companyunkid, _
'                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                           ConfigParameter._Object._UserAccessModeSetting, _
'                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeList", True)

'            'Sohail (06 Jan 2012) -- End
'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsCombo.Tables("EmployeeList")
'            End With

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objMaster.getComboListPAYYEAR("Year", True)
'            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboPayYear
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("Year")
'            End With

'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            dsCombo = objMaster.getComboListForSalaryChangeApprovalStatus("SalaryApproval", True)
'            With cboSalaryChangeApprovalStatus
'                .BeginUpdate()
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = New DataView(dsCombo.Tables("SalaryApproval"), "Id <> " & enSalaryChangeApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
'                If .Items.Count > 0 Then .SelectedIndex = 0
'                .EndUpdate()
'            End With
'            'Sohail (24 Sep 2012) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objEmployee = Nothing
'            objMaster = Nothing
'        End Try
'    End Sub

'    Private Sub FillList()
'        'Dim objEmployee As New clsEmployee_Master 'Sohail (03 Nov 2010)
'        Dim dsList As New DataSet
'        'Sohail (10 Feb 2012) -- Start
'        'TRA - ENHANCEMENT
'        Dim dtTable As DataTable
'        Dim strSearch As String = ""
'        'Sohail (10 Feb 2012) -- End
'        Try

'            If User._Object.Privilege._AllowToViewSalaryChangeList = True Then    'Pinkal (09-Jul-2012) -- Start

'                Cursor.Current = Cursors.WaitCursor 'Sohail (10 Feb 2012)

'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'dsList = objSalaryInc.GetList("SalaryInc", cboEmployee.SelectedValue.ToString, CInt(cboPayYear.SelectedValue), _
'                '                              CInt(cboPayPeriod.SelectedValue), "prsalaryincrement_tran.employeeunkid, prsalaryincrement_tran.incrementdate ", mstrAdvanceFilter, , , , , CInt(cboSalaryChangeApprovalStatus.SelectedValue))
'                dsList = objSalaryInc.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "SalaryInc", Nothing, cboEmployee.SelectedValue.ToString, CInt(cboPayPeriod.SelectedValue), CInt(cboPayYear.SelectedValue), "prsalaryincrement_tran.employeeunkid, prsalaryincrement_tran.incrementdate ", mstrAdvanceFilter, CInt(cboSalaryChangeApprovalStatus.SelectedValue))
'                'Sohail (21 Aug 2015) -- End
'                '                             'Sohail (24 Sep 2012) - [cboEDApprovalStatus.SelectedValue]

'                'Sohail (10 Feb 2012) -- Start
'                'TRA - ENHANCEMENT
'                If mstrAppraisalFilter.Trim.Length > 0 Then
'                    strSearch = mstrAppraisalFilter
'                End If
'                If strSearch.Trim.Length > 0 Then
'                    dtTable = New DataView(dsList.Tables("SalaryInc"), strSearch.Substring(4), "employeename", DataViewRowState.CurrentRows).ToTable
'                Else
'                    dtTable = New DataView(dsList.Tables("SalaryInc"), "", "employeename", DataViewRowState.CurrentRows).ToTable
'                End If
'                'Sohail (10 Feb 2012) -- End

'                'Anjan (21 Nov 2011)-Start
'                'ENHANCEMENT : TRA COMMENTS
'                'Dim dtTable As DataTable
'                'If mstrAdvanceFilter.Length > 0 Then
'                '    dtTable = New DataView(dsList.Tables(0), mstrAdvanceFilter, "", DataViewRowState.CurrentRows).ToTable
'                'Else
'                '    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
'                'End If

'                'Anjan (21 Nov 2011)-End 

'                Dim lvItem As ListViewItem
'                Dim lvArray As New List(Of ListViewItem) 'Sohail (03 Nov 2010)

'                lvSalaryIncrement.Items.Clear()
'                objchkSelectAll.Checked = False   'Sohail (05 Mar 2013)
'                lvSalaryIncrement.BeginUpdate() 'Sohail (03 Nov 2010)

'                'Anjan (21 Nov 2011)-Start
'                'ENHANCEMENT : TRA COMMENTS
'                'Sohail (10 Feb 2012) -- Start
'                'TRA - ENHANCEMENT
'                For Each drRow As DataRow In dtTable.Rows
'                    'For Each drRow As DataRow In dsList.Tables("SalaryInc").Rows
'                    'Sohail (10 Feb 2012) -- End
'                    'For Each drRow As DataRow In dtTable.Rows
'                    'Anjan (21 Nov 2011)-End 

'                    lvItem = New ListViewItem
'                    'Sohail (24 Sep 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    'lvItem.Text = drRow("salaryincrementtranunkid").ToString
'                    lvItem.Text = ""
'                    'Sohail (24 Sep 2012) -- End
'                    lvItem.Tag = drRow("salaryincrementtranunkid").ToString

'                    lvItem.SubItems.Add(drRow("period_name").ToString)
'                    lvItem.SubItems(colhPayPeriod.Index).Tag = drRow("periodunkid").ToString

'                    'Sohail (03 Nov 2010) -- Start
'                    'objEmployee._Employeeunkid = CInt(drRow("employeeunkid").ToString)
'                    'lvItem.SubItems.Add(objEmployee._Firstname & " " & objEmployee._Surname)
'                    lvItem.SubItems.Add(drRow("employeename").ToString)
'                    'Sohail (03 Nov 2010) -- End
'                    lvItem.SubItems(colhEmployeeName.Index).Tag = drRow("employeeunkid").ToString

'                    'Sohail (11 Sep 2010) -- Start
'                    'lvItem.SubItems.Add(CDate(drRow("incrementdate").ToString).ToShortDateString)
'                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("incrementdate").ToString).ToShortDateString)
'                    'Sohail (11 Sep 2010) -- End
'                    lvItem.SubItems.Add(Format(drRow("currentscale"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
'                    lvItem.SubItems.Add(Format(drRow("increment"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
'                    lvItem.SubItems.Add(Format(drRow("newscale"), GUI.fmtCurrency)) 'Sohail (01 Dec 2010)
'                    'Sohail (09 Oct 2010) -- Start
'                    'lvItem.SubItems.Add(drRow("isautogenerated").ToString)
'                    lvItem.SubItems.Add(drRow("isfromemployee").ToString)
'                    'Sohail (09 Oct 2010) -- End

'                    'Sohail (24 Sep 2012) -- Start
'                    'TRA - ENHANCEMENT
'                    If CBool(drRow.Item("isapproved")) = True Then
'                        lvItem.SubItems.Add("A")
'                        lvItem.SubItems(colhApprovePending.Index).Tag = "A" 'Sohail (05 Mar 2013)
'                    Else
'                        lvItem.SubItems.Add("P")
'                        lvItem.SubItems(colhApprovePending.Index).Tag = "P" 'Sohail (05 Mar 2013)
'                    End If
'                    'Sohail (24 Sep 2012) -- End

'                    'Sohail (10 Nov 2014) -- Start
'                    'Enhancement - Change Grade option on Import Salary Change.
'                    lvItem.SubItems.Add(drRow("GradeGroup").ToString)
'                    lvItem.SubItems(colhGradeGroup.Index).Tag = CInt(drRow("gradegroupunkid"))

'                    lvItem.SubItems.Add(drRow("Grade").ToString)
'                    lvItem.SubItems(colhGradeGroup.Index).Tag = CInt(drRow("gradeunkid"))

'                    lvItem.SubItems.Add(drRow("GradeLevel").ToString)
'                    lvItem.SubItems(colhGradeGroup.Index).Tag = CInt(drRow("gradelevelunkid"))
'                    'Sohail (10 Nov 2014) -- End

'                    'Sohail (03 Nov 2010) -- Start
'                    'lvSalaryIncrement.Items.Add(lvItem)
'                    lvArray.Add(lvItem)
'                    'Sohail (03 Nov 2010) -- End
'                Next
'                RemoveHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked 'Sohail (24 Sep 2012)
'                lvSalaryIncrement.Items.AddRange(lvArray.ToArray) 'Sohail (03 Nov 2010)
'                AddHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked 'Sohail (24 Sep 2012)
'                lvSalaryIncrement.GroupingColumn = colhEmployeeName
'                lvSalaryIncrement.DisplayGroups(True)
'                lvSalaryIncrement.GridLines = False 'Sohail (16 Oct 2010)

'                If lvSalaryIncrement.Items.Count > 4 Then
'                    colhIncrementAmt.Width = 107 - 18
'                Else
'                    colhIncrementAmt.Width = 107
'                End If
'                lvSalaryIncrement.EndUpdate() 'Sohail (03 Nov 2010)

'                'Sohail (24 Sep 2012) -- Start
'                'TRA - ENHANCEMENT
'                btnApprove.Visible = False
'                btnDisApprove.Visible = False
'                If CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enSalaryChangeApprovalStatus.Approved AndAlso btnDisApprove.Enabled = True Then
'                    btnDisApprove.Visible = True
'                ElseIf CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enSalaryChangeApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
'                    btnApprove.Visible = True
'                End If
'                'Sohail (24 Sep 2012) -- End

'                Cursor.Current = Cursors.Default 'Sohail (10 Feb 2012)

'            End If

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
'        Finally
'            dsList.Dispose()
'            'objEmployee = Nothing 'Sohail (03 Nov 2010)
'        End Try
'    End Sub

'    Private Sub SetVisibility()

'        Try
'            btnNew.Enabled = User._Object.Privilege._AddSalaryIncrement
'            btnEdit.Enabled = User._Object.Privilege._EditSalaryIncrement
'            btnDelete.Enabled = User._Object.Privilege._DeleteSalaryIncrement
'            'Sohail (24 Sep 2012) -- Start
'            'TRA - ENHANCEMENT
'            'btnGlobalAssign.Enabled = User._Object.Privilege._AllowGlobalSalaryIncrement
'            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowGlobalSalaryIncrement
'            mnuImportSalaryChange.Enabled = User._Object.Privilege._AllowGlobalSalaryIncrement
'            btnApprove.Enabled = User._Object.Privilege._AllowToApproveSalaryChange
'            btnDisApprove.Enabled = User._Object.Privilege._AllowToApproveSalaryChange
'            'Sohail (24 Sep 2012) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'    'Sohail (24 Sep 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
'        Try
'            For Each lvItem As ListViewItem In lvSalaryIncrement.Items
'                If CBool(lvItem.SubItems(colhAuto.Index).Text) = True Then Continue For
'                RemoveHandler lvSalaryIncrement.ItemCheck, AddressOf lvSalaryIncrement_ItemCheck
'                RemoveHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
'                lvItem.Checked = blnCheckAll
'                AddHandler lvSalaryIncrement.ItemCheck, AddressOf lvSalaryIncrement_ItemCheck
'                AddHandler lvSalaryIncrement.ItemChecked, AddressOf lvSalaryIncrement_ItemChecked
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CheckAllEdHeads", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (24 Sep 2012) -- End
'#End Region

'#Region " ComboBox's Events "
'    Private Sub cboPayYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayYear.SelectedIndexChanged 'Sohail (03 Nov 2010)
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsCombo As DataSet = Nothing

'        Try
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), "PayPeriod", True)
'            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboPayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True)
'            'Sohail (21 Aug 2015) -- End
'            With cboPayPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("PayPeriod")
'                If .Items.Count > 0 Then .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedIndexChanged", mstrModuleName)
'        Finally
'            objPeriod = Nothing
'        End Try
'    End Sub

'    'Sohail (21 Aug 2015) -- Start
'    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
'        Try
'            If CInt(cboPayPeriod.SelectedValue) > 0 Then
'                mdtPeriodStart = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
'                mdtPeriodStart = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
'            Else
'                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
'                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (21 Aug 2015) -- End

'#End Region

'#Region "Form's Event"

'    Private Sub frmSalaryIncrementList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            objSalaryInc = Nothing
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSalaryIncrementList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try

'            Select Case e.KeyCode
'                Case Keys.Delete
'                    btnDelete.PerformClick()
'                Case Keys.Escape
'                    btnClose.PerformClick()
'                Case Keys.Return
'                    SendKeys.Send("{TAB}")
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmSalaryIncrementList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objSalaryInc = New clsSalaryIncrement
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)

'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.

'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 

'            Call SetVisibility()
'            Call SetColor()
'            Call FillCombo()
'            'Call FillList()
'            If lvSalaryIncrement.Items.Count > 0 Then lvSalaryIncrement.Items(0).Selected = True
'            lvSalaryIncrement.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmSalaryIncrementList_Load", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsSalaryIncrement.SetMessages()
'            objfrm._Other_ModuleNames = "clsSalaryIncrement"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 
'#End Region

'#Region "Button's Event"

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Try
'            Dim objSalary_AddEdit As New frmSalaryIncrement_AddEdit
'            If objSalary_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
'                FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvSalaryIncrement.DoubleClick
'        'Sohail (24 Jun 2011) -- Start
'        If User._Object.Privilege._EditSalaryIncrement = False Then Exit Sub
'        'Sohail (24 Jun 2011) -- End
'        Dim objTnA As New clsTnALeaveTran
'        Dim objPayment As New clsPayment_tran
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsList As DataSet 'Sohail (29 Dec 2012) 
'        Dim intTnAID As Integer
'        Try
'            If lvSalaryIncrement.SelectedItems.Count < 1 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If
'            If CBool(lvSalaryIncrement.SelectedItems(0).SubItems(colhAuto.Index).Text) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Edit this entry. Reason: This is Auto Generated entry."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
'            'Sohail (21 Aug 2015) -- End
'            If objPeriod._Statusid = enStatusType.Close Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If

'            'Sohail (05 Mar 2013) -- Start
'            'TRA - ENHANCEMENT
'            If lvSalaryIncrement.SelectedItems(0).SubItems(colhApprovePending.Index).Tag.ToString = "A" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! This Salary Change is already Approved. Please Disapprove this Salary Change to perform further operation on it."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If

'            If objTnA.IsPayrollProcessDone(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to edit salary change."), enMsgBoxStyle.Information)
'                Exit Try
'            End If
'            'Sohail (05 Mar 2013) -- End

'            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
'            If intTnAID > 0 Then
'                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'                    'If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit this information. Reason: Payment of Salary is already done for this period."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'            End If
'            'Sohail (10 Feb 2012) -- Start
'            'TRA - ENHANCEMENT
'            If mstrAppraisalFilter.Trim = "" Then
'                'Sohail (29 Dec 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT) = True Then
'                If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
'                    'Sohail (29 Dec 2012) -- End
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Appraisal."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'                'Sohail (24 Feb 2012) -- Start
'                'TRA - ENHANCEMENT
'                'Sohail (29 Dec 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT) = True Then
'                If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
'                    'Sohail (29 Dec 2012) -- End
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Training Enrollment."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'                'Sohail (24 Feb 2012) -- End
'            End If
'            'Sohail (10 Feb 2012) -- End

'            'Sohail (29 Dec 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Sohail (07 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag))
'            dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), True)
'            'Sohail (07 Sep 2013) -- End
'            If dsList.Tables("Incr").Rows.Count > 0 Then
'                If eZeeDate.convertDate(CDate(lvSalaryIncrement.SelectedItems(0).SubItems(colhIncrDate.Index).Text)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to edit selected salary change."), enMsgBoxStyle.Information)
'                    Exit Try
'                End If
'            End If
'            'Sohail (29 Dec 2012) -- End

'            Dim objfrmSalaryIncrement As New frmSalaryIncrement_AddEdit

'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvSalaryIncrement.SelectedItems(0).Index
'            If objfrmSalaryIncrement.displayDialog(CInt(lvSalaryIncrement.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
'                Call FillList()
'            End If
'            objfrmSalaryIncrement = Nothing

'            lvSalaryIncrement.Items(intSelectedIndex).Selected = True
'            lvSalaryIncrement.EnsureVisible(intSelectedIndex)
'            lvSalaryIncrement.Select()
'            If objfrmSalaryIncrement IsNot Nothing Then objfrmSalaryIncrement.Dispose()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        Finally
'            objTnA = Nothing
'            objPayment = Nothing
'            objPeriod = Nothing
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Dim objTnA As New clsTnALeaveTran
'        Dim objPayment As New clsPayment_tran
'        Dim objPeriod As New clscommom_period_Tran
'        Dim dsList As DataSet 'Sohail (29 Dec 2012) 
'        Dim intTnAID As Integer
'        Try
'            If lvSalaryIncrement.SelectedItems.Count < 1 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If
'            'If objPayPointmaster.isUsed(CInt(lvState.SelectedItems(0).Tag)) Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Information. Reason: This Informaton is in use."), enMsgBoxStyle.Information) '?2
'            '    lvState.Select()
'            '    Exit Sub
'            'End If
'            If CBool(lvSalaryIncrement.SelectedItems(0).SubItems(colhAuto.Index).Text) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot Delete this entry. Reason: This is Auto Generated entry."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If
'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'objPeriod._Periodunkid = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
'            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
'            'Sohail (21 Aug 2015) -- End
'            If objPeriod._Statusid = enStatusType.Close Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If

'            'Sohail (05 Mar 2013) -- Start
'            'TRA - ENHANCEMENT
'            If lvSalaryIncrement.SelectedItems(0).SubItems(colhApprovePending.Index).Tag.ToString = "A" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry! This Salary Change is already Approved. Please Disapprove this Salary Change to perform further operation on it."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Try
'            End If
'            'Sohail (05 Mar 2013) -- End

'            'Sohail (29 Dec 2012) -- Start
'            'TRA - ENHANCEMENT
'            If objTnA.IsPayrollProcessDone(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to void salary change."), enMsgBoxStyle.Information)
'                Exit Try
'            End If
'            'Sohail (29 Dec 2012) -- End

'            intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
'            If intTnAID > 0 Then
'                If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START -- END
'                    'If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Delete this information. Reason: Payment of Salary is already done for this period."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'            End If
'            'Sohail (10 Feb 2012) -- Start
'            'TRA - ENHANCEMENT
'            If mstrAppraisalFilter.Trim = "" Then
'                'Sohail (29 Dec 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT) = True Then
'                If objSalaryInc.isUsedInAppraisal(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enAppraisal_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
'                    'Sohail (29 Dec 2012) -- End
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Appraisal."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'                'Sohail (24 Feb 2012) -- Start
'                'TRA - ENHANCEMENT
'                'Sohail (29 Dec 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT) = True Then
'                If objSalaryInc.isUsedInTrainingEnrollment(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), enTrainingAward_Modes.SALARY_INCREMENT, CInt(lvSalaryIncrement.SelectedItems(0).Tag)) = True Then
'                    'Sohail (29 Dec 2012) -- End
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot Edit/Delete this entry. Reason: It is used in Training Enrollment."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Try
'                End If
'                'Sohail (24 Feb 2012) -- End
'            End If
'            'Sohail (10 Feb 2012) -- End

'            'Sohail (29 Dec 2012) -- Start
'            'TRA - ENHANCEMENT
'            'Sohail (07 Sep 2013) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag))
'            dsList = objSalaryInc.getLastIncrement("Incr", CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), True)
'            'Sohail (07 Sep 2013) -- End
'            If dsList.Tables("Incr").Rows.Count > 0 Then
'                If eZeeDate.convertDate(CDate(lvSalaryIncrement.SelectedItems(0).SubItems(colhIncrDate.Index).Text)) < dsList.Tables("Incr").Rows(0).Item("incrementdate").ToString Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Last Increment Date is greater than selected increment date. Please Void the last increment first for selected employee to void selected salary change."), enMsgBoxStyle.Information)
'                    Exit Try
'                End If
'            End If
'            'Sohail (29 Dec 2012) -- End

'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvSalaryIncrement.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Salary Increment Information?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'                'Sandeep [ 16 Oct 2010 ] -- Start
'                'objSalaryInc.Void(CInt(lvSalaryIncrement.SelectedItems(0).Tag), User._Object._Userunkid, Now, "") 'Sohail (14 Oct 2010)
'                Dim frm As New frmReasonSelection
'                Dim mstrVoidReason As String = String.Empty
'                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
'                If mstrVoidReason.Length <= 0 Then
'                    Exit Sub
'                End If
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objSalaryInc.Void(CInt(lvSalaryIncrement.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
'                objSalaryInc.Void(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvSalaryIncrement.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
'                'Sohail (21 Aug 2015) -- End
'                'Sandeep [ 16 Oct 2010 ] -- End 

'                'Sohail (10 Feb 2012) -- Start
'                'TRA - ENHANCEMENT
'                mblnCancel = False
'                'Sohail (29 Dec 2012) -- Start
'                'TRA - ENHANCEMENT
'                'If mdicAppraisal.ContainsKey(0) = False Then
'                If mdicAppraisal.ContainsKey(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag)) = False Then
'                    'Sohail (29 Dec 2012) -- End
'                    mdicAppraisal.Add(CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhEmployeeName.Index).Tag), CInt(lvSalaryIncrement.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag))
'                End If
'                'Sohail (10 Feb 2012) -- End

'                lvSalaryIncrement.SelectedItems(0).Remove()

'                If lvSalaryIncrement.Items.Count <= 0 Then
'                    Exit Try
'                End If

'                If lvSalaryIncrement.Items.Count = intSelectedIndex Then
'                    intSelectedIndex = lvSalaryIncrement.Items.Count - 1
'                    lvSalaryIncrement.Items(intSelectedIndex).Selected = True
'                    lvSalaryIncrement.EnsureVisible(intSelectedIndex)
'                ElseIf lvSalaryIncrement.Items.Count <> 0 Then
'                    lvSalaryIncrement.Items(intSelectedIndex).Selected = True
'                    lvSalaryIncrement.EnsureVisible(intSelectedIndex)
'                End If
'            End If
'            lvSalaryIncrement.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        Finally
'            objTnA = Nothing
'            objPayment = Nothing
'            objPeriod = Nothing
'        End Try
'    End Sub

'    'Sohail (09 Oct 2010) -- Start
'    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
'        Dim objFrm As New frmSalaryGlobalAssign
'        Try
'            If objFrm.DisplayDialog() = True Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (09 Oct 2010) -- End

'    'Sohail (22 Aug 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private Sub mnuImportSalaryChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportSalaryChange.Click
'        Dim frm As New frmImportSalaryChange
'        Try
'            If frm.displayDialog() = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "File Imported Successfully."), enMsgBoxStyle.Information)
'                FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuImportSalaryChange_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (22 Aug 2012) -- End

'    'Sohail (24 Sep 2012) -- Start
'    'TRA - ENHANCEMENT
'    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisApprove.Click
'        'Sohail (05 Mar 2013) -- Start
'        'TRA - ENHANCEMENT
'        Dim objTnA As New clsTnALeaveTran
'        Dim objPeriod As New clscommom_period_Tran
'        'Sohail (05 Mar 2013) -- End
'        Dim dsList As DataSet 'Sohail (19 Sep 2013)
'        Dim strMsg As String = ""
'        Dim blnIsApprove As Boolean
'        Dim btn As Button = CType(sender, Button)
'        If btn.Name = btnApprove.Name Then
'            blnIsApprove = True
'            strMsg = Language.getMessage(mstrModuleName, 11, "Are you sure you want to Approve selected Salary Change?")
'        ElseIf btn.Name = btnDisApprove.Name Then
'            blnIsApprove = False
'            strMsg = Language.getMessage(mstrModuleName, 12, "Are you sure you want to Dispprove selected Salary Change?")
'        End If
'        If lvSalaryIncrement.CheckedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Salary Change to Approve/Disapprove."), enMsgBoxStyle.Information)
'            lvSalaryIncrement.Focus()
'            Exit Sub
'            'Sohail (05 Mar 2013) -- Start
'            'TRA - ENHANCEMENT
'            'ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
'            '    Exit Sub
'            'Sohail (05 Mar 2013) -- End
'        End If

'        'Sohail (05 Mar 2013) -- Start
'        'TRA - ENHANCEMENT
'        For Each lvItem As ListViewItem In lvSalaryIncrement.CheckedItems
'            If CBool(lvItem.SubItems(colhAuto.Index).Text) = True Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Some of the checked entries are Auto Generated."), enMsgBoxStyle.Information)
'                lvSalaryIncrement.Select()
'                Exit Sub

'            Else
'                objPeriod = New clscommom_period_Tran
'                'Sohail (21 Aug 2015) -- Start
'                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
'                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(colhPayPeriod.Index).Tag)
'                'Sohail (21 Aug 2015) -- End
'                If objPeriod._Statusid = enStatusType.Close Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Period is closed for some of the checked entries."), enMsgBoxStyle.Information)
'                    lvSalaryIncrement.Select()
'                    Exit Sub
'                ElseIf objTnA.IsPayrollProcessDone(CInt(lvItem.SubItems(colhPayPeriod.Index).Tag), lvItem.SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'                'Sohail (19 Sep 2013) -- Start
'                'TRA - ENHANCEMENT
'                dsList = objSalaryInc.getLastIncrement("LastIncr", CInt(lvItem.SubItems(colhEmployeeName.Index).Tag), True)
'                If dsList.Tables("LastIncr").Rows.Count > 0 Then
'                    If CInt(dsList.Tables("LastIncr").Rows(0).Item("salaryincrementtranunkid")) <> CInt(lvItem.Tag) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can Approve/Disapprove each employee's Last Salary Change only."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                End If
'                'Sohail (19 Sep 2013) -- End

'            End If
'        Next

'        If (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
'            Exit Sub
'        End If
'        'Sohail (05 Mar 2013) -- End

'        Dim blnResult As Boolean
'        Dim blnRefresh As Boolean = False
'        Try
'            For Each lvItem As ListViewItem In lvSalaryIncrement.CheckedItems

'                objSalaryInc = New clsSalaryIncrement
'                With objSalaryInc
'                    ._Salaryincrementtranunkid = CInt(lvItem.Tag)
'                    ._Isapproved = blnIsApprove
'                    ._Approveruserunkid = User._Object._Userunkid


'                    'Anjan (05 Dec 2012)-Start
'                    'ISSUE : grades were not getting updated to employee master from salary increment.
'                    'blnResult = .Update(False)
'                    'Sohail (21 Aug 2015) -- Start
'                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'                    'blnResult = .Update(True)
'                    blnResult = .Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, , True, "")
'                    'Sohail (21 Aug 2015) -- End
'                    'Anjan (05 Dec 2012)-End 


'                    If blnResult = False AndAlso objSalaryInc._Message <> "" Then
'                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
'                        Exit Try
'                    End If
'                    blnRefresh = True
'                End With
'            Next

'            If blnRefresh = True Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (24 Sep 2012) -- End
'#End Region

'    'Sohail (24 Sep 2012) -- Start
'    'TRA - ENHANCEMENT
'#Region " Listview's Events "
'    Private Sub lvSalaryIncrement_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvSalaryIncrement.ItemCheck
'        Try
'            If CBool(lvSalaryIncrement.Items(e.Index).SubItems(colhAuto.Index).Text) = True AndAlso e.NewValue = CheckState.Checked Then
'                e.NewValue = CheckState.Unchecked
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvSalaryIncrement_ItemCheck", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvSalaryIncrement_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSalaryIncrement.ItemChecked
'        Try
'            If lvSalaryIncrement.CheckedItems.Count <= 0 Then
'                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'                objchkSelectAll.CheckState = CheckState.Unchecked
'                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'            ElseIf lvSalaryIncrement.CheckedItems.Count < lvSalaryIncrement.Items.Count Then
'                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'                objchkSelectAll.CheckState = CheckState.Indeterminate
'                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'            ElseIf lvSalaryIncrement.CheckedItems.Count = lvSalaryIncrement.Items.Count Then
'                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'                objchkSelectAll.CheckState = CheckState.Checked
'                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvSalaryIncrement_ItemChecked", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " CheckBox's Events "
'    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
'        Try
'            Call CheckAll(objchkSelectAll.Checked)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub
'#End Region
'    'Sohail (24 Sep 2012) -- End

'#Region " Other Control's Events "
'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim objfrm As New frmCommonSearch
'        Dim objEmployee As New clsEmployee_Master
'        'Dim dsList As DataSet
'        Try
'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If
'            'Anjan (02 Sep 2011)-End 

'            'Anjan [10 June 2015] -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsList = objEmployee.GetEmployeeList("EmployeeList")
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'            'Else
'            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True)
'            'End If
'            'Sohail (06 Jan 2012) -- End
'            'Anjan [10 June 2015] -- End

'            With cboEmployee
'                objfrm.DataSource = CType(.DataSource, DataTable) 'Anjan [10 June 2015] -- End
'                objfrm.ValueMember = .ValueMember
'                objfrm.DisplayMember = .DisplayMember
'                objfrm.CodeMember = "employeecode"
'                If objfrm.DisplayDialog Then
'                    .SelectedValue = objfrm.SelectedValue
'                End If
'                .Focus()
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            objfrm = Nothing
'            objEmployee = Nothing
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboEmployee.SelectedValue = 0
'            cboPayYear.SelectedValue = 0
'            cboPayPeriod.SelectedValue = 0
'            If cboSalaryChangeApprovalStatus.Items.Count > 0 Then cboSalaryChangeApprovalStatus.SelectedIndex = 0 'Sohail (24 Sep 2012)
'            'Anjan (21 Nov 2011)-Start
'            'ENHANCEMENT : TRA COMMENTS
'            mstrAdvanceFilter = ""
'            'Anjan (21 Nov 2011)-End 
'            Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Anjan (21 Nov 2011)-Start
'    'ENHANCEMENT : TRA COMMENTS
'    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
'        Try
'            Dim frm As New frmAdvanceSearch
'            frm._Hr_EmployeeTable_Alias = "hremployee_master"
'            frm.ShowDialog()
'            mstrAdvanceFilter = frm._GetFilterString
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (21 Nov 2011)-End 
'#End Region

'#Region " Message List "
'    '1, "Please select Employee from the list to perform further operation on it."
'    '2, "Sorry, You cannot delete this Information. Reason: This Informaton is in use."
'    '3, "Are you sure you want to delete this Salary Increment Information?"
'    '4, "Sorry, You can not Edit this information. Reason: Payment of Salary is already done for this period."
'    '5, "Sorry, You can not Delete this information. Reason: Payment of Salary is already done for this period."
'    '6, "Sorry, You can not Delete this entry. Reason: This is Auto Generated entry."
'    '7, "Sorry, You can not Edit/Delete this entry. Reason: Period is closed."
'#End Region




'End Class