﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenerateYear_Wizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGenerateYear_Wizard))
        Me.pnlGenerateYear = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.wzGenerateNewYear = New eZee.Common.eZeeWizard
        Me.wpTransaction = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbLoan = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep6_Desc = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.rtbCloseYearLog = New System.Windows.Forms.RichTextBox
        Me.lvTransaction = New eZee.Common.eZeeListView(Me.components)
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeName = New System.Windows.Forms.ColumnHeader
        Me.colhHead = New System.Windows.Forms.ColumnHeader
        Me.colhHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhBalance = New System.Windows.Forms.ColumnHeader
        Me.wpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.pnlFinish = New System.Windows.Forms.Panel
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep7_Finish = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.wpLeave_BF = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbLeave_BF = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvLeave = New eZee.Common.eZeeListView(Me.components)
        Me.colhLvBalanceID = New System.Windows.Forms.ColumnHeader
        Me.colhLeaveTypeName = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhLeaveBalance = New System.Windows.Forms.ColumnHeader
        Me.colhCFLeaveAmount = New System.Windows.Forms.ColumnHeader
        Me.EZeeLine5 = New eZee.Common.eZeeLine
        Me.wpMasterInformation = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep4 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvApplicants = New eZee.Common.eZeeListView(Me.components)
        Me.colhApplicantCode = New System.Windows.Forms.ColumnHeader
        Me.colhApplicantName = New System.Windows.Forms.ColumnHeader
        Me.lblStep4_Desc = New System.Windows.Forms.Label
        Me.wpConfiguration = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep3 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep3_Desc = New System.Windows.Forms.Label
        Me.lvLoanSavings = New eZee.Common.eZeeListView(Me.components)
        Me.colhLoanPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhLoanEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhLoanEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhLoanHead = New System.Windows.Forms.ColumnHeader
        Me.colhLoanHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhLoanBalance = New System.Windows.Forms.ColumnHeader
        Me.wpYearinfo = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbstep2 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.dtpPeriodEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.dtpNewEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblNewEnddate = New System.Windows.Forms.Label
        Me.dtpNewStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblNewStartdate = New System.Windows.Forms.Label
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.dtpCurrEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblCurrEnddate = New System.Windows.Forms.Label
        Me.dtpCurrStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblCurrStartdate = New System.Windows.Forms.Label
        Me.eZeeLine1 = New eZee.Common.eZeeLine
        Me.picStep2 = New System.Windows.Forms.PictureBox
        Me.wpWelcome = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep1Message = New System.Windows.Forms.Label
        Me.picStep1 = New System.Windows.Forms.PictureBox
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbNewYearSteps = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlStep6 = New System.Windows.Forms.Panel
        Me.picSide_Step6_Check = New System.Windows.Forms.PictureBox
        Me.lblStep6 = New System.Windows.Forms.Label
        Me.pnlStep7 = New System.Windows.Forms.Panel
        Me.lblStep7 = New System.Windows.Forms.Label
        Me.pnlStep3 = New System.Windows.Forms.Panel
        Me.picSide_Step3_Check = New System.Windows.Forms.PictureBox
        Me.lblStep3 = New System.Windows.Forms.Label
        Me.pnlStep5 = New System.Windows.Forms.Panel
        Me.picSide_Step5_Check = New System.Windows.Forms.PictureBox
        Me.lblStep5 = New System.Windows.Forms.Label
        Me.pnlStep4 = New System.Windows.Forms.Panel
        Me.picSide_Step4_Check = New System.Windows.Forms.PictureBox
        Me.lblStep4 = New System.Windows.Forms.Label
        Me.pnlStep2 = New System.Windows.Forms.Panel
        Me.picSide_Step2_Check = New System.Windows.Forms.PictureBox
        Me.lblStep2 = New System.Windows.Forms.Label
        Me.pnlStep1 = New System.Windows.Forms.Panel
        Me.picSide_Step1_Check = New System.Windows.Forms.PictureBox
        Me.lblStep1 = New System.Windows.Forms.Label
        Me.objFooterSteps = New eZee.Common.eZeeFooter
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.objbgwCloseYear = New System.ComponentModel.BackgroundWorker
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.pnlGenerateYear.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.wzGenerateNewYear.SuspendLayout()
        Me.wpTransaction.SuspendLayout()
        Me.gbLoan.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.wpFinish.SuspendLayout()
        Me.pnlFinish.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpLeave_BF.SuspendLayout()
        Me.gbLeave_BF.SuspendLayout()
        Me.wpMasterInformation.SuspendLayout()
        Me.gbStep4.SuspendLayout()
        Me.wpConfiguration.SuspendLayout()
        Me.gbStep3.SuspendLayout()
        Me.wpYearinfo.SuspendLayout()
        Me.gbstep2.SuspendLayout()
        CType(Me.picStep2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpWelcome.SuspendLayout()
        Me.gbStep1.SuspendLayout()
        CType(Me.picStep1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbNewYearSteps.SuspendLayout()
        Me.pnlStep6.SuspendLayout()
        CType(Me.picSide_Step6_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep7.SuspendLayout()
        Me.pnlStep3.SuspendLayout()
        CType(Me.picSide_Step3_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep5.SuspendLayout()
        CType(Me.picSide_Step5_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep4.SuspendLayout()
        CType(Me.picSide_Step4_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep2.SuspendLayout()
        CType(Me.picSide_Step2_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep1.SuspendLayout()
        CType(Me.picSide_Step1_Check, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlGenerateYear
        '
        Me.pnlGenerateYear.Controls.Add(Me.Panel1)
        Me.pnlGenerateYear.Controls.Add(Me.gbNewYearSteps)
        Me.pnlGenerateYear.Controls.Add(Me.EZeeHeader)
        Me.pnlGenerateYear.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlGenerateYear.Location = New System.Drawing.Point(0, 0)
        Me.pnlGenerateYear.Name = "pnlGenerateYear"
        Me.pnlGenerateYear.Size = New System.Drawing.Size(800, 586)
        Me.pnlGenerateYear.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.wzGenerateNewYear)
        Me.Panel1.Location = New System.Drawing.Point(229, 68)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(559, 498)
        Me.Panel1.TabIndex = 7
        '
        'wzGenerateNewYear
        '
        Me.wzGenerateNewYear.Controls.Add(Me.wpTransaction)
        Me.wzGenerateNewYear.Controls.Add(Me.wpLeave_BF)
        Me.wzGenerateNewYear.Controls.Add(Me.wpMasterInformation)
        Me.wzGenerateNewYear.Controls.Add(Me.wpConfiguration)
        Me.wzGenerateNewYear.Controls.Add(Me.wpYearinfo)
        Me.wzGenerateNewYear.Controls.Add(Me.wpWelcome)
        Me.wzGenerateNewYear.Controls.Add(Me.wpFinish)
        Me.wzGenerateNewYear.Controls.Add(Me.objbuttonBack)
        Me.wzGenerateNewYear.Controls.Add(Me.objbuttonCancel)
        Me.wzGenerateNewYear.Controls.Add(Me.objbuttonNext)
        Me.wzGenerateNewYear.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzGenerateNewYear.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzGenerateNewYear.HeaderImage = Nothing
        Me.wzGenerateNewYear.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzGenerateNewYear.Location = New System.Drawing.Point(0, 0)
        Me.wzGenerateNewYear.Name = "wzGenerateNewYear"
        Me.wzGenerateNewYear.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpWelcome, Me.wpYearinfo, Me.wpConfiguration, Me.wpMasterInformation, Me.wpLeave_BF, Me.wpTransaction, Me.wpFinish})
        Me.wzGenerateNewYear.SaveEnabled = True
        Me.wzGenerateNewYear.SaveText = "Save && Finish"
        Me.wzGenerateNewYear.SaveVisible = False
        Me.wzGenerateNewYear.SetSaveIndexBeforeFinishIndex = False
        Me.wzGenerateNewYear.Size = New System.Drawing.Size(559, 498)
        Me.wzGenerateNewYear.TabIndex = 1
        Me.wzGenerateNewYear.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzGenerateNewYear.WelcomeImage = Nothing
        Me.wzGenerateNewYear.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wpTransaction
        '
        Me.wpTransaction.Controls.Add(Me.gbLoan)
        Me.wpTransaction.Location = New System.Drawing.Point(0, 0)
        Me.wpTransaction.Name = "wpTransaction"
        Me.wpTransaction.Size = New System.Drawing.Size(559, 450)
        Me.wpTransaction.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpTransaction.TabIndex = 14
        '
        'gbLoan
        '
        Me.gbLoan.BorderColor = System.Drawing.Color.Black
        Me.gbLoan.Checked = False
        Me.gbLoan.CollapseAllExceptThis = False
        Me.gbLoan.CollapsedHoverImage = Nothing
        Me.gbLoan.CollapsedNormalImage = Nothing
        Me.gbLoan.CollapsedPressedImage = Nothing
        Me.gbLoan.CollapseOnLoad = False
        Me.gbLoan.Controls.Add(Me.objlblProgress)
        Me.gbLoan.Controls.Add(Me.lblStep6_Desc)
        Me.gbLoan.Controls.Add(Me.Panel2)
        Me.gbLoan.ExpandedHoverImage = Nothing
        Me.gbLoan.ExpandedNormalImage = Nothing
        Me.gbLoan.ExpandedPressedImage = Nothing
        Me.gbLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoan.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbLoan.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoan.HeaderHeight = 25
        Me.gbLoan.HeaderMessage = ""
        Me.gbLoan.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoan.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoan.HeightOnCollapse = 0
        Me.gbLoan.LeftTextSpace = 0
        Me.gbLoan.Location = New System.Drawing.Point(0, 0)
        Me.gbLoan.Name = "gbLoan"
        Me.gbLoan.OpenHeight = 300
        Me.gbLoan.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoan.ShowBorder = True
        Me.gbLoan.ShowCheckBox = False
        Me.gbLoan.ShowCollapseButton = False
        Me.gbLoan.ShowDefaultBorderColor = True
        Me.gbLoan.ShowDownButton = False
        Me.gbLoan.ShowHeader = True
        Me.gbLoan.Size = New System.Drawing.Size(559, 455)
        Me.gbLoan.TabIndex = 6
        Me.gbLoan.Temp = 0
        Me.gbLoan.Text = "Transaction - Step 6 of 7"
        Me.gbLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep6_Desc
        '
        Me.lblStep6_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep6_Desc.Location = New System.Drawing.Point(10, 33)
        Me.lblStep6_Desc.Name = "lblStep6_Desc"
        Me.lblStep6_Desc.Size = New System.Drawing.Size(536, 35)
        Me.lblStep6_Desc.TabIndex = 157
        Me.lblStep6_Desc.Text = "Select Employee which you want to include in New Year Generation. Transaction shu" & _
            "old be Loan / Advance / Salary / Saving Scheme."
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.rtbCloseYearLog)
        Me.Panel2.Controls.Add(Me.lvTransaction)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(7, 78)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(542, 362)
        Me.Panel2.TabIndex = 158
        '
        'rtbCloseYearLog
        '
        Me.rtbCloseYearLog.Location = New System.Drawing.Point(111, 208)
        Me.rtbCloseYearLog.Name = "rtbCloseYearLog"
        Me.rtbCloseYearLog.ReadOnly = True
        Me.rtbCloseYearLog.Size = New System.Drawing.Size(297, 151)
        Me.rtbCloseYearLog.TabIndex = 4
        Me.rtbCloseYearLog.Text = ""
        Me.rtbCloseYearLog.Visible = False
        '
        'lvTransaction
        '
        Me.lvTransaction.BackColorOnChecked = False
        Me.lvTransaction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvTransaction.ColumnHeaders = Nothing
        Me.lvTransaction.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPeriod, Me.colhEmployeeCode, Me.colhEmployeeName, Me.colhHead, Me.colhHeadType, Me.colhBalance})
        Me.lvTransaction.CompulsoryColumns = ""
        Me.lvTransaction.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTransaction.FullRowSelect = True
        Me.lvTransaction.GridLines = True
        Me.lvTransaction.GroupingColumn = Nothing
        Me.lvTransaction.HideSelection = False
        Me.lvTransaction.Location = New System.Drawing.Point(0, 0)
        Me.lvTransaction.MinColumnWidth = 50
        Me.lvTransaction.MultiSelect = False
        Me.lvTransaction.Name = "lvTransaction"
        Me.lvTransaction.OptionalColumns = ""
        Me.lvTransaction.ShowMoreItem = False
        Me.lvTransaction.ShowSaveItem = False
        Me.lvTransaction.ShowSelectAll = True
        Me.lvTransaction.ShowSizeAllColumnsToFit = True
        Me.lvTransaction.Size = New System.Drawing.Size(542, 362)
        Me.lvTransaction.Sortable = False
        Me.lvTransaction.TabIndex = 3
        Me.lvTransaction.UseCompatibleStateImageBehavior = False
        Me.lvTransaction.View = System.Windows.Forms.View.Details
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.Tag = "colhEmployeeCode"
        Me.colhEmployeeCode.Text = "EmpCode"
        Me.colhEmployeeCode.Width = 0
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.Tag = "colhEmployeeName"
        Me.colhEmployeeName.Text = "Employee Name"
        Me.colhEmployeeName.Width = 150
        '
        'colhHead
        '
        Me.colhHead.Tag = "colhHead"
        Me.colhHead.Text = "Head"
        Me.colhHead.Width = 145
        '
        'colhHeadType
        '
        Me.colhHeadType.Tag = "colhHeadType"
        Me.colhHeadType.Text = "Head Type"
        Me.colhHeadType.Width = 85
        '
        'colhBalance
        '
        Me.colhBalance.Tag = "colhBalance"
        Me.colhBalance.Text = "Balance Amount"
        Me.colhBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBalance.Width = 160
        '
        'wpFinish
        '
        Me.wpFinish.Controls.Add(Me.pnlFinish)
        Me.wpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wpFinish.Name = "wpFinish"
        Me.wpFinish.Size = New System.Drawing.Size(559, 450)
        Me.wpFinish.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.wpFinish.TabIndex = 13
        '
        'pnlFinish
        '
        Me.pnlFinish.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlFinish.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFinish.Location = New System.Drawing.Point(0, 0)
        Me.pnlFinish.Name = "pnlFinish"
        Me.pnlFinish.Size = New System.Drawing.Size(559, 450)
        Me.pnlFinish.TabIndex = 0
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblStep7_Finish)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.PictureBox1)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(559, 455)
        Me.EZeeCollapsibleContainer1.TabIndex = 4
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Finish- Step 7 of 7"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep7_Finish
        '
        Me.lblStep7_Finish.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7_Finish.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep7_Finish.Location = New System.Drawing.Point(253, 69)
        Me.lblStep7_Finish.Name = "lblStep7_Finish"
        Me.lblStep7_Finish.Size = New System.Drawing.Size(291, 345)
        Me.lblStep7_Finish.TabIndex = 155
        Me.lblStep7_Finish.Text = "The Generate New Year Wizard Completed Successfully."
        Me.lblStep7_Finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.PictureBox1.Location = New System.Drawing.Point(13, 34)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(225, 406)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'wpLeave_BF
        '
        Me.wpLeave_BF.Controls.Add(Me.gbLeave_BF)
        Me.wpLeave_BF.Location = New System.Drawing.Point(0, 0)
        Me.wpLeave_BF.Name = "wpLeave_BF"
        Me.wpLeave_BF.Size = New System.Drawing.Size(559, 450)
        Me.wpLeave_BF.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpLeave_BF.TabIndex = 12
        '
        'gbLeave_BF
        '
        Me.gbLeave_BF.BorderColor = System.Drawing.Color.Black
        Me.gbLeave_BF.Checked = False
        Me.gbLeave_BF.CollapseAllExceptThis = False
        Me.gbLeave_BF.CollapsedHoverImage = Nothing
        Me.gbLeave_BF.CollapsedNormalImage = Nothing
        Me.gbLeave_BF.CollapsedPressedImage = Nothing
        Me.gbLeave_BF.CollapseOnLoad = False
        Me.gbLeave_BF.Controls.Add(Me.lvLeave)
        Me.gbLeave_BF.Controls.Add(Me.EZeeLine5)
        Me.gbLeave_BF.ExpandedHoverImage = Nothing
        Me.gbLeave_BF.ExpandedNormalImage = Nothing
        Me.gbLeave_BF.ExpandedPressedImage = Nothing
        Me.gbLeave_BF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeave_BF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbLeave_BF.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeave_BF.HeaderHeight = 25
        Me.gbLeave_BF.HeaderMessage = ""
        Me.gbLeave_BF.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeave_BF.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeave_BF.HeightOnCollapse = 0
        Me.gbLeave_BF.LeftTextSpace = 0
        Me.gbLeave_BF.Location = New System.Drawing.Point(0, 0)
        Me.gbLeave_BF.Name = "gbLeave_BF"
        Me.gbLeave_BF.OpenHeight = 300
        Me.gbLeave_BF.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeave_BF.ShowBorder = True
        Me.gbLeave_BF.ShowCheckBox = False
        Me.gbLeave_BF.ShowCollapseButton = False
        Me.gbLeave_BF.ShowDefaultBorderColor = True
        Me.gbLeave_BF.ShowDownButton = False
        Me.gbLeave_BF.ShowHeader = True
        Me.gbLeave_BF.Size = New System.Drawing.Size(559, 455)
        Me.gbLeave_BF.TabIndex = 5
        Me.gbLeave_BF.Temp = 0
        Me.gbLeave_BF.Text = "Leave B/F - Step 5 of 7"
        Me.gbLeave_BF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvLeave
        '
        Me.lvLeave.BackColorOnChecked = False
        Me.lvLeave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvLeave.ColumnHeaders = Nothing
        Me.lvLeave.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLvBalanceID, Me.colhLeaveTypeName, Me.colhEmployee, Me.colhLeaveBalance, Me.colhCFLeaveAmount})
        Me.lvLeave.CompulsoryColumns = ""
        Me.lvLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeave.FullRowSelect = True
        Me.lvLeave.GridLines = True
        Me.lvLeave.GroupingColumn = Nothing
        Me.lvLeave.HideSelection = False
        Me.lvLeave.Location = New System.Drawing.Point(43, 58)
        Me.lvLeave.MinColumnWidth = 50
        Me.lvLeave.MultiSelect = False
        Me.lvLeave.Name = "lvLeave"
        Me.lvLeave.OptionalColumns = ""
        Me.lvLeave.ShowMoreItem = False
        Me.lvLeave.ShowSaveItem = False
        Me.lvLeave.ShowSelectAll = True
        Me.lvLeave.ShowSizeAllColumnsToFit = True
        Me.lvLeave.Size = New System.Drawing.Size(502, 377)
        Me.lvLeave.Sortable = False
        Me.lvLeave.TabIndex = 35
        Me.lvLeave.UseCompatibleStateImageBehavior = False
        Me.lvLeave.View = System.Windows.Forms.View.Details
        '
        'colhLvBalanceID
        '
        Me.colhLvBalanceID.Tag = "colhLvBalanceID"
        Me.colhLvBalanceID.Text = "Balance"
        Me.colhLvBalanceID.Width = 0
        '
        'colhLeaveTypeName
        '
        Me.colhLeaveTypeName.Tag = "colhLeaveTypeName"
        Me.colhLeaveTypeName.Text = "Leave Type Name"
        Me.colhLeaveTypeName.Width = 0
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee Name"
        Me.colhEmployee.Width = 250
        '
        'colhLeaveBalance
        '
        Me.colhLeaveBalance.Tag = "colhLeaveBalance"
        Me.colhLeaveBalance.Text = "Paid Leave Balance"
        Me.colhLeaveBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhLeaveBalance.Width = 150
        '
        'colhCFLeaveAmount
        '
        Me.colhCFLeaveAmount.Tag = "colhCFLeaveAmount"
        Me.colhCFLeaveAmount.Text = "C.F Amount"
        Me.colhCFLeaveAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhCFLeaveAmount.Width = 100
        '
        'EZeeLine5
        '
        Me.EZeeLine5.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine5.Location = New System.Drawing.Point(12, 35)
        Me.EZeeLine5.Name = "EZeeLine5"
        Me.EZeeLine5.Size = New System.Drawing.Size(533, 20)
        Me.EZeeLine5.TabIndex = 33
        Me.EZeeLine5.Text = "List of Employee"
        '
        'wpMasterInformation
        '
        Me.wpMasterInformation.Controls.Add(Me.gbStep4)
        Me.wpMasterInformation.Location = New System.Drawing.Point(0, 0)
        Me.wpMasterInformation.Name = "wpMasterInformation"
        Me.wpMasterInformation.Size = New System.Drawing.Size(559, 450)
        Me.wpMasterInformation.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpMasterInformation.TabIndex = 11
        '
        'gbStep4
        '
        Me.gbStep4.BorderColor = System.Drawing.Color.Black
        Me.gbStep4.Checked = False
        Me.gbStep4.CollapseAllExceptThis = False
        Me.gbStep4.CollapsedHoverImage = Nothing
        Me.gbStep4.CollapsedNormalImage = Nothing
        Me.gbStep4.CollapsedPressedImage = Nothing
        Me.gbStep4.CollapseOnLoad = False
        Me.gbStep4.Controls.Add(Me.lvApplicants)
        Me.gbStep4.Controls.Add(Me.lblStep4_Desc)
        Me.gbStep4.ExpandedHoverImage = Nothing
        Me.gbStep4.ExpandedNormalImage = Nothing
        Me.gbStep4.ExpandedPressedImage = Nothing
        Me.gbStep4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep4.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep4.HeaderHeight = 25
        Me.gbStep4.HeaderMessage = ""
        Me.gbStep4.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep4.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep4.HeightOnCollapse = 0
        Me.gbStep4.LeftTextSpace = 0
        Me.gbStep4.Location = New System.Drawing.Point(0, 0)
        Me.gbStep4.Name = "gbStep4"
        Me.gbStep4.OpenHeight = 300
        Me.gbStep4.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep4.ShowBorder = True
        Me.gbStep4.ShowCheckBox = False
        Me.gbStep4.ShowCollapseButton = False
        Me.gbStep4.ShowDefaultBorderColor = True
        Me.gbStep4.ShowDownButton = False
        Me.gbStep4.ShowHeader = True
        Me.gbStep4.Size = New System.Drawing.Size(559, 455)
        Me.gbStep4.TabIndex = 4
        Me.gbStep4.Temp = 0
        Me.gbStep4.Text = "Eligible Applicants - Step 4 of 7"
        Me.gbStep4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvApplicants
        '
        Me.lvApplicants.BackColorOnChecked = False
        Me.lvApplicants.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvApplicants.ColumnHeaders = Nothing
        Me.lvApplicants.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhApplicantCode, Me.colhApplicantName})
        Me.lvApplicants.CompulsoryColumns = ""
        Me.lvApplicants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvApplicants.FullRowSelect = True
        Me.lvApplicants.GridLines = True
        Me.lvApplicants.GroupingColumn = Nothing
        Me.lvApplicants.HideSelection = False
        Me.lvApplicants.Location = New System.Drawing.Point(7, 78)
        Me.lvApplicants.MinColumnWidth = 50
        Me.lvApplicants.MultiSelect = False
        Me.lvApplicants.Name = "lvApplicants"
        Me.lvApplicants.OptionalColumns = ""
        Me.lvApplicants.ShowMoreItem = False
        Me.lvApplicants.ShowSaveItem = False
        Me.lvApplicants.ShowSelectAll = True
        Me.lvApplicants.ShowSizeAllColumnsToFit = True
        Me.lvApplicants.Size = New System.Drawing.Size(542, 362)
        Me.lvApplicants.Sortable = True
        Me.lvApplicants.TabIndex = 160
        Me.lvApplicants.UseCompatibleStateImageBehavior = False
        Me.lvApplicants.View = System.Windows.Forms.View.Details
        '
        'colhApplicantCode
        '
        Me.colhApplicantCode.Tag = "colhApplicantCode"
        Me.colhApplicantCode.Text = "Applicant Code"
        Me.colhApplicantCode.Width = 90
        '
        'colhApplicantName
        '
        Me.colhApplicantName.Tag = "colhApplicantName"
        Me.colhApplicantName.Text = "Applicant Name"
        Me.colhApplicantName.Width = 440
        '
        'lblStep4_Desc
        '
        Me.lblStep4_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep4_Desc.Location = New System.Drawing.Point(15, 34)
        Me.lblStep4_Desc.Name = "lblStep4_Desc"
        Me.lblStep4_Desc.Size = New System.Drawing.Size(527, 41)
        Me.lblStep4_Desc.TabIndex = 158
        Me.lblStep4_Desc.Text = "These all Masters are used to carry forward for preparation of the Next Payroll Y" & _
            "ear. "
        '
        'wpConfiguration
        '
        Me.wpConfiguration.Controls.Add(Me.gbStep3)
        Me.wpConfiguration.Location = New System.Drawing.Point(0, 0)
        Me.wpConfiguration.Name = "wpConfiguration"
        Me.wpConfiguration.Size = New System.Drawing.Size(559, 450)
        Me.wpConfiguration.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpConfiguration.TabIndex = 10
        '
        'gbStep3
        '
        Me.gbStep3.BorderColor = System.Drawing.Color.Black
        Me.gbStep3.Checked = False
        Me.gbStep3.CollapseAllExceptThis = False
        Me.gbStep3.CollapsedHoverImage = Nothing
        Me.gbStep3.CollapsedNormalImage = Nothing
        Me.gbStep3.CollapsedPressedImage = Nothing
        Me.gbStep3.CollapseOnLoad = False
        Me.gbStep3.Controls.Add(Me.lblStep3_Desc)
        Me.gbStep3.Controls.Add(Me.lvLoanSavings)
        Me.gbStep3.ExpandedHoverImage = Nothing
        Me.gbStep3.ExpandedNormalImage = Nothing
        Me.gbStep3.ExpandedPressedImage = Nothing
        Me.gbStep3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep3.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep3.HeaderHeight = 25
        Me.gbStep3.HeaderMessage = ""
        Me.gbStep3.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep3.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep3.HeightOnCollapse = 0
        Me.gbStep3.LeftTextSpace = 0
        Me.gbStep3.Location = New System.Drawing.Point(0, 0)
        Me.gbStep3.Name = "gbStep3"
        Me.gbStep3.OpenHeight = 300
        Me.gbStep3.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep3.ShowBorder = True
        Me.gbStep3.ShowCheckBox = False
        Me.gbStep3.ShowCollapseButton = False
        Me.gbStep3.ShowDefaultBorderColor = True
        Me.gbStep3.ShowDownButton = False
        Me.gbStep3.ShowHeader = True
        Me.gbStep3.Size = New System.Drawing.Size(559, 455)
        Me.gbStep3.TabIndex = 3
        Me.gbStep3.Temp = 0
        Me.gbStep3.Text = "Loan && Savings- Step 3 of 7"
        Me.gbStep3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep3_Desc
        '
        Me.lblStep3_Desc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep3_Desc.Location = New System.Drawing.Point(10, 35)
        Me.lblStep3_Desc.Name = "lblStep3_Desc"
        Me.lblStep3_Desc.Size = New System.Drawing.Size(536, 40)
        Me.lblStep3_Desc.TabIndex = 159
        Me.lblStep3_Desc.Text = "These all Masters are used to carry forward for preparation of the Next Payroll Y" & _
            "ear. "
        '
        'lvLoanSavings
        '
        Me.lvLoanSavings.BackColorOnChecked = False
        Me.lvLoanSavings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvLoanSavings.ColumnHeaders = Nothing
        Me.lvLoanSavings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLoanPeriod, Me.colhLoanEmpCode, Me.colhLoanEmpName, Me.colhLoanHead, Me.colhLoanHeadType, Me.colhLoanBalance})
        Me.lvLoanSavings.CompulsoryColumns = ""
        Me.lvLoanSavings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLoanSavings.FullRowSelect = True
        Me.lvLoanSavings.GridLines = True
        Me.lvLoanSavings.GroupingColumn = Nothing
        Me.lvLoanSavings.HideSelection = False
        Me.lvLoanSavings.Location = New System.Drawing.Point(7, 78)
        Me.lvLoanSavings.MinColumnWidth = 50
        Me.lvLoanSavings.MultiSelect = False
        Me.lvLoanSavings.Name = "lvLoanSavings"
        Me.lvLoanSavings.OptionalColumns = ""
        Me.lvLoanSavings.ShowMoreItem = False
        Me.lvLoanSavings.ShowSaveItem = False
        Me.lvLoanSavings.ShowSelectAll = True
        Me.lvLoanSavings.ShowSizeAllColumnsToFit = True
        Me.lvLoanSavings.Size = New System.Drawing.Size(542, 362)
        Me.lvLoanSavings.Sortable = False
        Me.lvLoanSavings.TabIndex = 36
        Me.lvLoanSavings.UseCompatibleStateImageBehavior = False
        Me.lvLoanSavings.View = System.Windows.Forms.View.Details
        '
        'colhLoanPeriod
        '
        Me.colhLoanPeriod.Tag = "colhLoanPeriod"
        Me.colhLoanPeriod.Text = "Period"
        Me.colhLoanPeriod.Width = 0
        '
        'colhLoanEmpCode
        '
        Me.colhLoanEmpCode.Tag = "colhLoanEmpCode"
        Me.colhLoanEmpCode.Text = "EmpCode"
        Me.colhLoanEmpCode.Width = 0
        '
        'colhLoanEmpName
        '
        Me.colhLoanEmpName.Tag = "colhLoanEmpName"
        Me.colhLoanEmpName.Text = "Employee"
        Me.colhLoanEmpName.Width = 150
        '
        'colhLoanHead
        '
        Me.colhLoanHead.Tag = "colhLoanHead"
        Me.colhLoanHead.Text = "Loan / Savings"
        Me.colhLoanHead.Width = 125
        '
        'colhLoanHeadType
        '
        Me.colhLoanHeadType.Tag = "colhLoanHeadType"
        Me.colhLoanHeadType.Text = "Head Type"
        Me.colhLoanHeadType.Width = 105
        '
        'colhLoanBalance
        '
        Me.colhLoanBalance.Tag = "colhLoanBalance"
        Me.colhLoanBalance.Text = "Balance Amoount"
        Me.colhLoanBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhLoanBalance.Width = 160
        '
        'wpYearinfo
        '
        Me.wpYearinfo.Controls.Add(Me.gbstep2)
        Me.wpYearinfo.Location = New System.Drawing.Point(0, 0)
        Me.wpYearinfo.Name = "wpYearinfo"
        Me.wpYearinfo.Size = New System.Drawing.Size(559, 450)
        Me.wpYearinfo.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpYearinfo.TabIndex = 9
        '
        'gbstep2
        '
        Me.gbstep2.BorderColor = System.Drawing.Color.Black
        Me.gbstep2.Checked = False
        Me.gbstep2.CollapseAllExceptThis = False
        Me.gbstep2.CollapsedHoverImage = Nothing
        Me.gbstep2.CollapsedNormalImage = Nothing
        Me.gbstep2.CollapsedPressedImage = Nothing
        Me.gbstep2.CollapseOnLoad = False
        Me.gbstep2.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbstep2.Controls.Add(Me.dtpPeriodEnddate)
        Me.gbstep2.Controls.Add(Me.lblEnddate)
        Me.gbstep2.Controls.Add(Me.lblName)
        Me.gbstep2.Controls.Add(Me.txtName)
        Me.gbstep2.Controls.Add(Me.lblCode)
        Me.gbstep2.Controls.Add(Me.txtCode)
        Me.gbstep2.Controls.Add(Me.EZeeLine3)
        Me.gbstep2.Controls.Add(Me.dtpNewEnddate)
        Me.gbstep2.Controls.Add(Me.lblNewEnddate)
        Me.gbstep2.Controls.Add(Me.dtpNewStartdate)
        Me.gbstep2.Controls.Add(Me.lblNewStartdate)
        Me.gbstep2.Controls.Add(Me.EZeeLine2)
        Me.gbstep2.Controls.Add(Me.dtpCurrEnddate)
        Me.gbstep2.Controls.Add(Me.lblCurrEnddate)
        Me.gbstep2.Controls.Add(Me.dtpCurrStartdate)
        Me.gbstep2.Controls.Add(Me.lblCurrStartdate)
        Me.gbstep2.Controls.Add(Me.eZeeLine1)
        Me.gbstep2.Controls.Add(Me.picStep2)
        Me.gbstep2.ExpandedHoverImage = Nothing
        Me.gbstep2.ExpandedNormalImage = Nothing
        Me.gbstep2.ExpandedPressedImage = Nothing
        Me.gbstep2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbstep2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbstep2.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbstep2.HeaderHeight = 25
        Me.gbstep2.HeaderMessage = ""
        Me.gbstep2.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbstep2.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbstep2.HeightOnCollapse = 0
        Me.gbstep2.LeftTextSpace = 0
        Me.gbstep2.Location = New System.Drawing.Point(0, 0)
        Me.gbstep2.Name = "gbstep2"
        Me.gbstep2.OpenHeight = 300
        Me.gbstep2.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbstep2.ShowBorder = True
        Me.gbstep2.ShowCheckBox = False
        Me.gbstep2.ShowCollapseButton = False
        Me.gbstep2.ShowDefaultBorderColor = True
        Me.gbstep2.ShowDownButton = False
        Me.gbstep2.ShowHeader = True
        Me.gbstep2.Size = New System.Drawing.Size(559, 455)
        Me.gbstep2.TabIndex = 2
        Me.gbstep2.Temp = 0
        Me.gbstep2.Text = "Year  Information- Step 2 of 7"
        Me.gbstep2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Checked = True
        Me.chkCopyPreviousEDSlab.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(272, 395)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(268, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 179
        Me.chkCopyPreviousEDSlab.Text = "Copy last period ED Slab to New year first period"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'dtpPeriodEnddate
        '
        Me.dtpPeriodEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPeriodEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPeriodEnddate.Location = New System.Drawing.Point(354, 347)
        Me.dtpPeriodEnddate.Name = "dtpPeriodEnddate"
        Me.dtpPeriodEnddate.Size = New System.Drawing.Size(98, 21)
        Me.dtpPeriodEnddate.TabIndex = 105
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(269, 349)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(69, 16)
        Me.lblEnddate.TabIndex = 104
        Me.lblEnddate.Text = "End Date"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(269, 322)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(79, 16)
        Me.lblName.TabIndex = 103
        Me.lblName.Text = "Period Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(354, 320)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(186, 21)
        Me.txtName.TabIndex = 100
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(269, 295)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(79, 16)
        Me.lblCode.TabIndex = 101
        Me.lblCode.Text = "Period Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(354, 293)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(98, 21)
        Me.txtCode.TabIndex = 99
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(262, 270)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(278, 20)
        Me.EZeeLine3.TabIndex = 23
        Me.EZeeLine3.Text = "New Year First Period Information"
        '
        'dtpNewEnddate
        '
        Me.dtpNewEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpNewEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpNewEnddate.Location = New System.Drawing.Point(354, 226)
        Me.dtpNewEnddate.Name = "dtpNewEnddate"
        Me.dtpNewEnddate.Size = New System.Drawing.Size(98, 21)
        Me.dtpNewEnddate.TabIndex = 21
        '
        'lblNewEnddate
        '
        Me.lblNewEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewEnddate.Location = New System.Drawing.Point(269, 230)
        Me.lblNewEnddate.Name = "lblNewEnddate"
        Me.lblNewEnddate.Size = New System.Drawing.Size(69, 15)
        Me.lblNewEnddate.TabIndex = 20
        Me.lblNewEnddate.Text = "End Date"
        Me.lblNewEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpNewStartdate
        '
        Me.dtpNewStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpNewStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpNewStartdate.Location = New System.Drawing.Point(354, 199)
        Me.dtpNewStartdate.Name = "dtpNewStartdate"
        Me.dtpNewStartdate.Size = New System.Drawing.Size(98, 21)
        Me.dtpNewStartdate.TabIndex = 19
        '
        'lblNewStartdate
        '
        Me.lblNewStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewStartdate.Location = New System.Drawing.Point(269, 203)
        Me.lblNewStartdate.Name = "lblNewStartdate"
        Me.lblNewStartdate.Size = New System.Drawing.Size(69, 15)
        Me.lblNewStartdate.TabIndex = 18
        Me.lblNewStartdate.Text = "Start Date"
        Me.lblNewStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(262, 176)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(278, 20)
        Me.EZeeLine2.TabIndex = 17
        Me.EZeeLine2.Text = "New Year Information"
        '
        'dtpCurrEnddate
        '
        Me.dtpCurrEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCurrEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCurrEnddate.Location = New System.Drawing.Point(354, 132)
        Me.dtpCurrEnddate.Name = "dtpCurrEnddate"
        Me.dtpCurrEnddate.Size = New System.Drawing.Size(98, 21)
        Me.dtpCurrEnddate.TabIndex = 16
        '
        'lblCurrEnddate
        '
        Me.lblCurrEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrEnddate.Location = New System.Drawing.Point(269, 136)
        Me.lblCurrEnddate.Name = "lblCurrEnddate"
        Me.lblCurrEnddate.Size = New System.Drawing.Size(69, 15)
        Me.lblCurrEnddate.TabIndex = 15
        Me.lblCurrEnddate.Text = "End Date"
        Me.lblCurrEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpCurrStartdate
        '
        Me.dtpCurrStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCurrStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCurrStartdate.Location = New System.Drawing.Point(354, 105)
        Me.dtpCurrStartdate.Name = "dtpCurrStartdate"
        Me.dtpCurrStartdate.Size = New System.Drawing.Size(98, 21)
        Me.dtpCurrStartdate.TabIndex = 14
        '
        'lblCurrStartdate
        '
        Me.lblCurrStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrStartdate.Location = New System.Drawing.Point(269, 109)
        Me.lblCurrStartdate.Name = "lblCurrStartdate"
        Me.lblCurrStartdate.Size = New System.Drawing.Size(69, 15)
        Me.lblCurrStartdate.TabIndex = 13
        Me.lblCurrStartdate.Text = "Start Date"
        Me.lblCurrStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeLine1
        '
        Me.eZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eZeeLine1.Location = New System.Drawing.Point(262, 82)
        Me.eZeeLine1.Name = "eZeeLine1"
        Me.eZeeLine1.Size = New System.Drawing.Size(278, 20)
        Me.eZeeLine1.TabIndex = 12
        Me.eZeeLine1.Text = "Current Year Information"
        '
        'picStep2
        '
        Me.picStep2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picStep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep2.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.picStep2.Location = New System.Drawing.Point(13, 34)
        Me.picStep2.Name = "picStep2"
        Me.picStep2.Size = New System.Drawing.Size(225, 406)
        Me.picStep2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picStep2.TabIndex = 1
        Me.picStep2.TabStop = False
        '
        'wpWelcome
        '
        Me.wpWelcome.Controls.Add(Me.gbStep1)
        Me.wpWelcome.Location = New System.Drawing.Point(0, 0)
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.Size = New System.Drawing.Size(559, 450)
        Me.wpWelcome.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpWelcome.TabIndex = 7
        '
        'gbStep1
        '
        Me.gbStep1.BorderColor = System.Drawing.Color.Black
        Me.gbStep1.Checked = False
        Me.gbStep1.CollapseAllExceptThis = False
        Me.gbStep1.CollapsedHoverImage = Nothing
        Me.gbStep1.CollapsedNormalImage = Nothing
        Me.gbStep1.CollapsedPressedImage = Nothing
        Me.gbStep1.CollapseOnLoad = False
        Me.gbStep1.Controls.Add(Me.lblStep1Message)
        Me.gbStep1.Controls.Add(Me.picStep1)
        Me.gbStep1.ExpandedHoverImage = Nothing
        Me.gbStep1.ExpandedNormalImage = Nothing
        Me.gbStep1.ExpandedPressedImage = Nothing
        Me.gbStep1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep1.HeaderHeight = 25
        Me.gbStep1.HeaderMessage = ""
        Me.gbStep1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep1.HeightOnCollapse = 0
        Me.gbStep1.LeftTextSpace = 0
        Me.gbStep1.Location = New System.Drawing.Point(0, 0)
        Me.gbStep1.Name = "gbStep1"
        Me.gbStep1.OpenHeight = 300
        Me.gbStep1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep1.ShowBorder = True
        Me.gbStep1.ShowCheckBox = False
        Me.gbStep1.ShowCollapseButton = False
        Me.gbStep1.ShowDefaultBorderColor = True
        Me.gbStep1.ShowDownButton = False
        Me.gbStep1.ShowHeader = True
        Me.gbStep1.Size = New System.Drawing.Size(559, 455)
        Me.gbStep1.TabIndex = 3
        Me.gbStep1.Temp = 0
        Me.gbStep1.Text = "Finish - Step 1 of 7"
        Me.gbStep1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep1Message
        '
        Me.lblStep1Message.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1Message.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep1Message.Location = New System.Drawing.Point(255, 53)
        Me.lblStep1Message.Name = "lblStep1Message"
        Me.lblStep1Message.Size = New System.Drawing.Size(291, 345)
        Me.lblStep1Message.TabIndex = 154
        Me.lblStep1Message.Text = "Welcome to Generate New Year Wizard....."
        Me.lblStep1Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picStep1
        '
        Me.picStep1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.picStep1.Location = New System.Drawing.Point(13, 34)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(225, 406)
        Me.picStep1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picStep1.TabIndex = 1
        Me.picStep1.TabStop = False
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(374, 388)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(534, 388)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(450, 388)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'gbNewYearSteps
        '
        Me.gbNewYearSteps.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbNewYearSteps.BackColor = System.Drawing.Color.WhiteSmoke
        Me.gbNewYearSteps.BorderColor = System.Drawing.Color.Black
        Me.gbNewYearSteps.Checked = False
        Me.gbNewYearSteps.CollapseAllExceptThis = False
        Me.gbNewYearSteps.CollapsedHoverImage = Nothing
        Me.gbNewYearSteps.CollapsedNormalImage = Nothing
        Me.gbNewYearSteps.CollapsedPressedImage = Nothing
        Me.gbNewYearSteps.CollapseOnLoad = False
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep6)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep7)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep3)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep5)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep4)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep2)
        Me.gbNewYearSteps.Controls.Add(Me.pnlStep1)
        Me.gbNewYearSteps.Controls.Add(Me.objFooterSteps)
        Me.gbNewYearSteps.ExpandedHoverImage = Nothing
        Me.gbNewYearSteps.ExpandedNormalImage = Nothing
        Me.gbNewYearSteps.ExpandedPressedImage = Nothing
        Me.gbNewYearSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbNewYearSteps.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbNewYearSteps.HeaderHeight = 25
        Me.gbNewYearSteps.HeaderMessage = ""
        Me.gbNewYearSteps.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbNewYearSteps.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbNewYearSteps.HeightOnCollapse = 0
        Me.gbNewYearSteps.LeftTextSpace = 0
        Me.gbNewYearSteps.Location = New System.Drawing.Point(7, 66)
        Me.gbNewYearSteps.Name = "gbNewYearSteps"
        Me.gbNewYearSteps.OpenHeight = 300
        Me.gbNewYearSteps.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbNewYearSteps.ShowBorder = True
        Me.gbNewYearSteps.ShowCheckBox = False
        Me.gbNewYearSteps.ShowCollapseButton = False
        Me.gbNewYearSteps.ShowDefaultBorderColor = True
        Me.gbNewYearSteps.ShowDownButton = False
        Me.gbNewYearSteps.ShowHeader = True
        Me.gbNewYearSteps.Size = New System.Drawing.Size(214, 502)
        Me.gbNewYearSteps.TabIndex = 6
        Me.gbNewYearSteps.Temp = 0
        Me.gbNewYearSteps.Text = "Generate New Year Steps"
        Me.gbNewYearSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep6
        '
        Me.pnlStep6.Controls.Add(Me.picSide_Step6_Check)
        Me.pnlStep6.Controls.Add(Me.lblStep6)
        Me.pnlStep6.Location = New System.Drawing.Point(3, 272)
        Me.pnlStep6.Name = "pnlStep6"
        Me.pnlStep6.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep6.TabIndex = 9
        '
        'picSide_Step6_Check
        '
        Me.picSide_Step6_Check.Image = CType(resources.GetObject("picSide_Step6_Check.Image"), System.Drawing.Image)
        Me.picSide_Step6_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step6_Check.Name = "picSide_Step6_Check"
        Me.picSide_Step6_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step6_Check.TabIndex = 160
        Me.picSide_Step6_Check.TabStop = False
        Me.picSide_Step6_Check.Visible = False
        '
        'lblStep6
        '
        Me.lblStep6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep6.Location = New System.Drawing.Point(32, 13)
        Me.lblStep6.Name = "lblStep6"
        Me.lblStep6.Size = New System.Drawing.Size(163, 16)
        Me.lblStep6.TabIndex = 159
        Me.lblStep6.Text = "6.  Transaction"
        Me.lblStep6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep7
        '
        Me.pnlStep7.Controls.Add(Me.lblStep7)
        Me.pnlStep7.Location = New System.Drawing.Point(3, 319)
        Me.pnlStep7.Name = "pnlStep7"
        Me.pnlStep7.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep7.TabIndex = 7
        '
        'lblStep7
        '
        Me.lblStep7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7.Location = New System.Drawing.Point(32, 13)
        Me.lblStep7.Name = "lblStep7"
        Me.lblStep7.Size = New System.Drawing.Size(163, 16)
        Me.lblStep7.TabIndex = 159
        Me.lblStep7.Text = "7.  Finish"
        Me.lblStep7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep3
        '
        Me.pnlStep3.Controls.Add(Me.picSide_Step3_Check)
        Me.pnlStep3.Controls.Add(Me.lblStep3)
        Me.pnlStep3.Location = New System.Drawing.Point(3, 131)
        Me.pnlStep3.Name = "pnlStep3"
        Me.pnlStep3.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep3.TabIndex = 6
        '
        'picSide_Step3_Check
        '
        Me.picSide_Step3_Check.Image = CType(resources.GetObject("picSide_Step3_Check.Image"), System.Drawing.Image)
        Me.picSide_Step3_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step3_Check.Name = "picSide_Step3_Check"
        Me.picSide_Step3_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step3_Check.TabIndex = 160
        Me.picSide_Step3_Check.TabStop = False
        Me.picSide_Step3_Check.Visible = False
        '
        'lblStep3
        '
        Me.lblStep3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep3.Location = New System.Drawing.Point(32, 13)
        Me.lblStep3.Name = "lblStep3"
        Me.lblStep3.Size = New System.Drawing.Size(163, 16)
        Me.lblStep3.TabIndex = 159
        Me.lblStep3.Text = "3.  Loan && Savings"
        Me.lblStep3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep5
        '
        Me.pnlStep5.Controls.Add(Me.picSide_Step5_Check)
        Me.pnlStep5.Controls.Add(Me.lblStep5)
        Me.pnlStep5.Location = New System.Drawing.Point(3, 225)
        Me.pnlStep5.Name = "pnlStep5"
        Me.pnlStep5.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep5.TabIndex = 5
        '
        'picSide_Step5_Check
        '
        Me.picSide_Step5_Check.Image = CType(resources.GetObject("picSide_Step5_Check.Image"), System.Drawing.Image)
        Me.picSide_Step5_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step5_Check.Name = "picSide_Step5_Check"
        Me.picSide_Step5_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step5_Check.TabIndex = 160
        Me.picSide_Step5_Check.TabStop = False
        Me.picSide_Step5_Check.Visible = False
        '
        'lblStep5
        '
        Me.lblStep5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep5.Location = New System.Drawing.Point(32, 13)
        Me.lblStep5.Name = "lblStep5"
        Me.lblStep5.Size = New System.Drawing.Size(163, 16)
        Me.lblStep5.TabIndex = 159
        Me.lblStep5.Text = "5.  Leave B/F"
        Me.lblStep5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep4
        '
        Me.pnlStep4.Controls.Add(Me.picSide_Step4_Check)
        Me.pnlStep4.Controls.Add(Me.lblStep4)
        Me.pnlStep4.Location = New System.Drawing.Point(3, 178)
        Me.pnlStep4.Name = "pnlStep4"
        Me.pnlStep4.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep4.TabIndex = 4
        '
        'picSide_Step4_Check
        '
        Me.picSide_Step4_Check.Image = CType(resources.GetObject("picSide_Step4_Check.Image"), System.Drawing.Image)
        Me.picSide_Step4_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step4_Check.Name = "picSide_Step4_Check"
        Me.picSide_Step4_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step4_Check.TabIndex = 160
        Me.picSide_Step4_Check.TabStop = False
        Me.picSide_Step4_Check.Visible = False
        '
        'lblStep4
        '
        Me.lblStep4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep4.Location = New System.Drawing.Point(32, 13)
        Me.lblStep4.Name = "lblStep4"
        Me.lblStep4.Size = New System.Drawing.Size(163, 16)
        Me.lblStep4.TabIndex = 159
        Me.lblStep4.Text = "4. Eligible Applicants"
        Me.lblStep4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep2
        '
        Me.pnlStep2.Controls.Add(Me.picSide_Step2_Check)
        Me.pnlStep2.Controls.Add(Me.lblStep2)
        Me.pnlStep2.Location = New System.Drawing.Point(3, 84)
        Me.pnlStep2.Name = "pnlStep2"
        Me.pnlStep2.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep2.TabIndex = 3
        '
        'picSide_Step2_Check
        '
        Me.picSide_Step2_Check.Image = CType(resources.GetObject("picSide_Step2_Check.Image"), System.Drawing.Image)
        Me.picSide_Step2_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step2_Check.Name = "picSide_Step2_Check"
        Me.picSide_Step2_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step2_Check.TabIndex = 160
        Me.picSide_Step2_Check.TabStop = False
        Me.picSide_Step2_Check.Visible = False
        '
        'lblStep2
        '
        Me.lblStep2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep2.Location = New System.Drawing.Point(32, 13)
        Me.lblStep2.Name = "lblStep2"
        Me.lblStep2.Size = New System.Drawing.Size(163, 16)
        Me.lblStep2.TabIndex = 159
        Me.lblStep2.Text = "2.  Year Information"
        Me.lblStep2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep1
        '
        Me.pnlStep1.Controls.Add(Me.picSide_Step1_Check)
        Me.pnlStep1.Controls.Add(Me.lblStep1)
        Me.pnlStep1.Location = New System.Drawing.Point(3, 37)
        Me.pnlStep1.Name = "pnlStep1"
        Me.pnlStep1.Size = New System.Drawing.Size(205, 41)
        Me.pnlStep1.TabIndex = 2
        '
        'picSide_Step1_Check
        '
        Me.picSide_Step1_Check.Image = Global.Aruti.Main.My.Resources.Resources.Check1_16
        Me.picSide_Step1_Check.Location = New System.Drawing.Point(10, 13)
        Me.picSide_Step1_Check.Name = "picSide_Step1_Check"
        Me.picSide_Step1_Check.Size = New System.Drawing.Size(16, 16)
        Me.picSide_Step1_Check.TabIndex = 160
        Me.picSide_Step1_Check.TabStop = False
        Me.picSide_Step1_Check.Visible = False
        '
        'lblStep1
        '
        Me.lblStep1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1.Location = New System.Drawing.Point(32, 13)
        Me.lblStep1.Name = "lblStep1"
        Me.lblStep1.Size = New System.Drawing.Size(163, 16)
        Me.lblStep1.TabIndex = 159
        Me.lblStep1.Text = "1.  Welcome"
        Me.lblStep1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooterSteps
        '
        Me.objFooterSteps.BorderColor = System.Drawing.Color.Silver
        Me.objFooterSteps.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooterSteps.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooterSteps.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooterSteps.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooterSteps.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooterSteps.Location = New System.Drawing.Point(0, 452)
        Me.objFooterSteps.Name = "objFooterSteps"
        Me.objFooterSteps.Size = New System.Drawing.Size(214, 50)
        Me.objFooterSteps.TabIndex = 1
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = "The Generate New Year Wizard will help you in the preparation of the Next Payroll" & _
            " Year."
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(800, 60)
        Me.EZeeHeader.TabIndex = 5
        Me.EZeeHeader.Title = "Generate New Year"
        '
        'objbgwCloseYear
        '
        Me.objbgwCloseYear.WorkerReportsProgress = True
        Me.objbgwCloseYear.WorkerSupportsCancellation = True
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(455, 48)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(94, 30)
        Me.objlblProgress.TabIndex = 16
        Me.objlblProgress.Visible = False
        '
        'frmGenerateYear_Wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 586)
        Me.Controls.Add(Me.pnlGenerateYear)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGenerateYear_Wizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Generate New Year Wizard"
        Me.pnlGenerateYear.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.wzGenerateNewYear.ResumeLayout(False)
        Me.wpTransaction.ResumeLayout(False)
        Me.gbLoan.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.wpFinish.ResumeLayout(False)
        Me.pnlFinish.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpLeave_BF.ResumeLayout(False)
        Me.gbLeave_BF.ResumeLayout(False)
        Me.wpMasterInformation.ResumeLayout(False)
        Me.gbStep4.ResumeLayout(False)
        Me.wpConfiguration.ResumeLayout(False)
        Me.gbStep3.ResumeLayout(False)
        Me.wpYearinfo.ResumeLayout(False)
        Me.gbstep2.ResumeLayout(False)
        Me.gbstep2.PerformLayout()
        CType(Me.picStep2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpWelcome.ResumeLayout(False)
        Me.gbStep1.ResumeLayout(False)
        CType(Me.picStep1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbNewYearSteps.ResumeLayout(False)
        Me.pnlStep6.ResumeLayout(False)
        CType(Me.picSide_Step6_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep7.ResumeLayout(False)
        Me.pnlStep3.ResumeLayout(False)
        CType(Me.picSide_Step3_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep5.ResumeLayout(False)
        CType(Me.picSide_Step5_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep4.ResumeLayout(False)
        CType(Me.picSide_Step4_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep2.ResumeLayout(False)
        CType(Me.picSide_Step2_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep1.ResumeLayout(False)
        CType(Me.picSide_Step1_Check, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlGenerateYear As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbNewYearSteps As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlStep6 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step6_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep6 As System.Windows.Forms.Label
    Friend WithEvents pnlStep7 As System.Windows.Forms.Panel
    Friend WithEvents lblStep7 As System.Windows.Forms.Label
    Friend WithEvents pnlStep3 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step3_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep3 As System.Windows.Forms.Label
    Friend WithEvents pnlStep5 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step5_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep5 As System.Windows.Forms.Label
    Friend WithEvents pnlStep4 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step4_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep4 As System.Windows.Forms.Label
    Friend WithEvents pnlStep2 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step2_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep2 As System.Windows.Forms.Label
    Friend WithEvents pnlStep1 As System.Windows.Forms.Panel
    Friend WithEvents picSide_Step1_Check As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep1 As System.Windows.Forms.Label
    Friend WithEvents objFooterSteps As eZee.Common.eZeeFooter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents wzGenerateNewYear As eZee.Common.eZeeWizard
    Friend WithEvents wpWelcome As eZee.Common.eZeeWizardPage
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents wpYearinfo As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStep1Message As System.Windows.Forms.Label
    Friend WithEvents picStep1 As System.Windows.Forms.PictureBox
    Friend WithEvents gbstep2 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents picStep2 As System.Windows.Forms.PictureBox
    Friend WithEvents dtpCurrStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrStartdate As System.Windows.Forms.Label
    Friend WithEvents eZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents dtpCurrEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrEnddate As System.Windows.Forms.Label
    Friend WithEvents dtpNewEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNewEnddate As System.Windows.Forms.Label
    Friend WithEvents dtpNewStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNewStartdate As System.Windows.Forms.Label
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents wpConfiguration As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep3 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wpMasterInformation As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep4 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wpLeave_BF As eZee.Common.eZeeWizardPage
    Friend WithEvents wpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents pnlFinish As System.Windows.Forms.Panel
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep7_Finish As System.Windows.Forms.Label
    Friend WithEvents gbLeave_BF As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wpTransaction As eZee.Common.eZeeWizardPage
    Friend WithEvents gbLoan As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeLine5 As eZee.Common.eZeeLine
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lvTransaction As eZee.Common.eZeeListView
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBalance As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblStep6_Desc As System.Windows.Forms.Label
    Friend WithEvents lblStep4_Desc As System.Windows.Forms.Label
    Friend WithEvents colhHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvLeave As eZee.Common.eZeeListView
    Friend WithEvents colhLvBalanceID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeaveBalance As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvLoanSavings As eZee.Common.eZeeListView
    Friend WithEvents lblStep3_Desc As System.Windows.Forms.Label
    Friend WithEvents colhLoanPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanBalance As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeaveTypeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvApplicants As eZee.Common.eZeeListView
    Friend WithEvents colhApplicantCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplicantName As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpPeriodEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents colhCFLeaveAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbgwCloseYear As System.ComponentModel.BackgroundWorker
    Friend WithEvents rtbCloseYearLog As System.Windows.Forms.RichTextBox
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
End Class
