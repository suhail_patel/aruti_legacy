﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Net

#End Region

Public Class frmUpdateEDT24ACBLoanData

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmUpdateEDT24ACBLoanData"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    'S.SANDEEP [12-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 0001843
    'Private objIExcel As ExcelData
    'S.SANDEEP [12-Jan-2018] -- END
    Private mdtFilteredTable As DataTable
    Private m_Dataview As DataView

    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    Private dvEmployee As DataView
    Private mintTotalEmployee As Integer = 0
    Private mintCount As Integer = 0
    Private mstrSearchEmpText As String = ""
    Private mstrSearchText As String = ""
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPayPeriod.BackColor = GUI.ColorComp
            cboBankGroup.BackColor = GUI.ColorOptional
            cboBankBranch.BackColor = GUI.ColorOptional
            txtLink.BackColor = GUI.ColorComp
            txtUserId.BackColor = GUI.ColorComp
            txtPassword.BackColor = GUI.ColorComp
            cboLoanBenefit.BackColor = GUI.ColorComp
            cboLoanInstallment.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objBankGrp As New clspayrollgroup_master
        Dim objHead As New clsTransactionHead
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable
        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Open)
            dtTable = New DataView(dsCombo.Tables("PayPeriod"), "start_date <= '" & eZeeDate.convertDate(DateTime.Today) & "'", "end_date", DataViewRowState.CurrentRows).ToTable
            'Sohail (04 Mar 2011) -- End

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboPayPeriod)

            dsCombo = objBankGrp.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("BankGrp")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboBankGroup)

            dsCombo = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , enCalcType.FlatRate_Others, , , , " typeof_id <> " & enTypeOf.Salary & " ")
            With cboLoanBenefit
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Head")
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboLoanBenefit)
            With cboLoanInstallment
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Head").Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboLoanInstallment)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objBankGrp = Nothing
            objHead = Nothing
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       mdtPayPeriodStartDate, _
                                       mdtPayPeriodEndDate, _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, False, "Employee", True, , , , , , , , , , , , , , True, mstrAdvanceFilter)

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
            Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub

    Private Sub FillList()
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable

        objlblEmpCount.Text = "( 0 / 0 )"
        mintCount = 0

        Try
            dgEmployee.DataSource = Nothing
            RemoveHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged
            Call SetDefaultSearchEmpText()
            AddHandler txtSearchEmp.TextChanged, AddressOf txtSearchEmp_TextChanged

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then Exit Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND premployee_bank_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboBankGroup.SelectedValue) > 0 Then
                StrSearching &= "AND cfpayrollgroup_master.groupmasterunkid = " & CInt(cboBankGroup.SelectedValue) & " "
            End If

            If CInt(cboBankBranch.SelectedValue) > 0 Then
                StrSearching &= "AND premployee_bank_tran.branchunkid = " & CInt(cboBankBranch.SelectedValue) & " "
            End If

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'If mstrAdvanceFilter.Length > 0 Then
            '    StrSearching &= "AND " & mstrAdvanceFilter
            'End If
            'Sohail (12 Oct 2021) -- End

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , StrSearching, , mdtPayPeriodEndDate, "EmpName, end_date DESC, BankGrp, BranchName")
            dsList = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , mstrAdvanceFilter, , mdtPayPeriodEndDate, "EmpName, end_date DESC, BankGrp, BranchName", StrSearching)
            'Sohail (12 Oct 2021) -- End
            dtTable = New DataView(dsList.Tables(0)).ToTable

            mintTotalEmployee = dtTable.Rows.Count
            objlblEmpCount.Text = "( 0 / " & mintTotalEmployee.ToString & " )"

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvEmployee = dtTable.DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgEcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            dgColhEmployeeCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "EmpName"
            dgColhBankGroup.DataPropertyName = "BankGrp"
            dgColhBankBranch.DataPropertyName = "BranchName"
            dgColhAccountNo.DataPropertyName = "accountno"
            objdgColhEmpBankTranUnkId.DataPropertyName = "empbanktranunkid"

            dgEmployee.DataSource = dvEmployee
            dvEmployee.Sort = "IsChecked DESC, EmpName "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhEmpCode.DataPropertyName = "employeecode"
            dgcolhEmpName.DataPropertyName = "employeename"
            dgcolhTranHead.DataPropertyName = "trnheadname"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhMedicalRefCode.DataPropertyName = "medicalrefno"
            dgcolhRemarks.DataPropertyName = "rowtype_remark"

            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

            objlblLoanDataCount.Text = "( " & m_Dataview.Count & " / " & (mintCount * 2).ToString & " )"

            RemoveHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            objchkISelectAll.Checked = False
            AddHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    'RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    'RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                    'AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    'AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                Next
                dvEmployee.ToTable.AcceptChanges()

                Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")
                mintCount = drRow.Length
                objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            If m_Dataview Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = objchkISelectAll.Checked
            Next
            dgvImportInfo.DataSource = m_Dataview
            dgvImportInfo.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchEmpText()
        Try
            mstrSearchEmpText = Language.getMessage(mstrModuleName, 1, "Type to Search Emp. Code, Emp. Name, Bank, Branch, A/C No.")
            With txtSearchEmp
                .ForeColor = Color.Gray
                .Text = mstrSearchEmpText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 2, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchEmpComboText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgEmployee.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            objlblEmpCount.Text = "( " & mintCount.ToString & " / " & mintTotalEmployee.ToString & " )"

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgEmployee.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Function IsValidated() As Boolean
        Try
            If txtLink.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter Link."), enMsgBoxStyle.Information)
                txtLink.Focus()
                Return False
            ElseIf txtUserId.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter User Id."), enMsgBoxStyle.Information)
                txtUserId.Focus()
                Return False
            ElseIf txtPassword.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Password."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False
            ElseIf CInt(cboLoanBenefit.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select head for Loan Benefit."), enMsgBoxStyle.Information)
                cboLoanBenefit.Focus()
                Return False
            ElseIf CInt(cboLoanInstallment.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select head for Loan Installment."), enMsgBoxStyle.Information)
                cboLoanInstallment.Focus()
                Return False
            ElseIf CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            ElseIf dvEmployee.Table.Select("IsChecked = 1 ").Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select atleast one employee."), enMsgBoxStyle.Information) 'Sohail (28 Dec 2010)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmImportUpdateFlatRateHeads_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'objIExcel = New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            picExpand.Tag = "Collapsed"

            Call FillCombo()

            txtLink.Text = "http://172.29.1.64:9763/services/T24001/getLoanInfo?"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportUpdateFlatRateHeads_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

            If CInt(cboPayPeriod.SelectedValue) < 0 Then Call SetDefaultSearchText(cboPayPeriod)

            Call FillEmployeeCombo()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim objBranch As New clsbankbranch_master
        Dim dsCombos As DataSet
        Try
            If CInt(cboBankGroup.SelectedValue) > 0 Then
                dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            Else
                dsCombos = objBranch.getListForCombo("Branch", True)
            End If
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                .SelectedValue = 0
            End With
            If CInt(cboBankGroup.SelectedValue) < 0 Then Call SetDefaultSearchText(cboBankGroup)
            Call SetDefaultSearchText(cboBankBranch)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objBranch = Nothing
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus _
                                                                                                , cboBankGroup.GotFocus _
                                                                                                , cboBankBranch.GotFocus _
                                                                                                , cboPayPeriod.GotFocus _
                                                                                                , cboLoanBenefit.GotFocus _
                                                                                                , cboLoanInstallment.GotFocus
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If

            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress _
                                                                                                                      , cboBankGroup.KeyPress _
                                                                                                                      , cboBankBranch.KeyPress _
                                                                                                                      , cboPayPeriod.KeyPress _
                                                                                                                      , cboLoanBenefit.KeyPress _
                                                                                                                      , cboLoanInstallment.KeyPress
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave _
                                                                                             , cboBankGroup.Leave _
                                                                                             , cboBankBranch.Leave _
                                                                                             , cboPayPeriod.Leave _
                                                                                             , cboLoanBenefit.Leave _
                                                                                             , cboLoanInstallment.Leave
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged _
                                                                                                            , cboBankBranch.SelectedIndexChanged _
                                                                                                            , cboLoanBenefit.SelectedIndexChanged _
                                                                                                            , cboLoanInstallment.SelectedIndexChanged
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) < 0 Then Call SetDefaultSearchText(cbo)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanInstallment_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboLoanBenefit.Validating _
                                                                                                                              , cboLoanInstallment.Validating

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbT24ACBLoanInfo.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanInstallment_Validating", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        Dim objED As New clsEarningDeduction
        Dim objHead As New clsTransactionHead
        Dim dtTable As DataTable
        Dim strLink As String
        Dim response As String = ""
        Dim strBenefitHeadName As String = ""
        Dim intBenefitHeadTypeId As Integer = 0
        Dim intBenefitHeadTypeOfId As Integer = 0
        Dim intBenefitHeadCalcTypeId As Integer = 0
        Dim intBenefitHeadComputeOnId As Integer = 0
        Dim decBenefitHeadAmt As Decimal = 0
        Dim strLoanContractID As String = ""
        Dim strInstHeadName As String = ""
        Dim intInstHeadTypeId As Integer = 0
        Dim intInstHeadTypeOfId As Integer = 0
        Dim intInstHeadCalcTypeId As Integer = 0
        Dim intInstHeadComputeOnId As Integer = 0
        Dim decInstHeadAmt As Decimal = 0
        Dim intCnt As Integer = 0
        Try
            If IsValidated() = False Then Exit Sub

            'URL format : http://172.29.1.64:9763/services/T24002/getLoanInfo?id=123&passcode=123&account=123
            strLink = txtLink.Text.Trim & "id=" & txtUserId.Text.Trim & "&passcode=" & txtPassword.Text & "&account="

            dtTable = dvEmployee.Table.Select("IsChecked = 1 ").CopyToDataTable.DefaultView.ToTable(True, "employeeunkid", "employeecode", "EmpName", "accountno")

            mdtTable = objED._DataSource

            objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboLoanBenefit.SelectedValue)
            strBenefitHeadName = objHead._Trnheadname
            intBenefitHeadTypeId = objHead._Trnheadtype_Id
            intBenefitHeadTypeOfId = objHead._Typeof_id
            intBenefitHeadCalcTypeId = objHead._Calctype_Id
            intBenefitHeadComputeOnId = objHead._Computeon_Id

            objHead = New clsTransactionHead
            objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboLoanInstallment.SelectedValue)
            strInstHeadName = objHead._Trnheadname
            intInstHeadTypeId = objHead._Trnheadtype_Id
            intInstHeadTypeOfId = objHead._Typeof_id
            intInstHeadCalcTypeId = objHead._Calctype_Id
            intInstHeadComputeOnId = objHead._Computeon_Id

            If m_Dataview IsNot Nothing Then m_Dataview.Table.Clear()

            objlblLoanDataCount.Text = "( 0 / " & (mintCount * 2).ToString & " )"
            Application.DoEvents()

            For Each dtRow As DataRow In dtTable.Rows
                decBenefitHeadAmt = 0
                decInstHeadAmt = 0
                strLoanContractID = ""

                Dim dr As DataRow = mdtTable.NewRow
                Dim dr2 As DataRow = mdtTable.NewRow

                Try

                    response = New WebClient().DownloadString(strLink & dtRow.Item("accountno").ToString)
                    dr.Item("rowtypeid") = 0 'Success
                    dr.Item("rowtype_remark") = ""

                    response = response.Replace("<loanBenefit />", "<loanBenefit>0</loanBenefit>").Replace("<loanBenefit/>", "<loanBenefit>0</loanBenefit>")
                    response = response.Replace("<loanInstallment />", "<loanInstallment>0</loanInstallment>").Replace("<loanInstallment/>", "<loanInstallment>0</loanInstallment>")
                    response = response.Replace("<loanBenefit></loanBenefit>", "<loanBenefit>0</loanBenefit>")
                    response = response.Replace("<loanInstallment></loanInstallment>", "<loanInstallment>0</loanInstallment>")

                    Dim ds As New DataSet
                    Try
                        ds.ReadXml(New IO.StringReader(response))
                        ''decBenefitHeadAmt = CDec(ds.Tables(0).Rows(0).Item("loanBenefit"))
                        ''decInstHeadAmt = CDec(ds.Tables(0).Rows(0).Item("loanInstallment"))
                        'Try
                        '    decBenefitHeadAmt = (From p In ds.Tables(0) Select (CDec(p.Item("loanBenefit")))).Sum
                        '    decInstHeadAmt = (From p In ds.Tables(0) Select (CDec(p.Item("loanInstallment")))).Sum
                        strLoanContractID = String.Join(", ", (From p In ds.Tables(0) Select (p.Item("loanContract").ToString)).ToArray)
                        'Catch ex As Exception

                        'End Try
                        For Each dsRow As DataRow In ds.Tables(0).Rows
                            Dim decB As Decimal = 0
                            Dim decI As Decimal = 0

                            Decimal.TryParse(dsRow.Item("loanBenefit").ToString, decB)
                            Decimal.TryParse(dsRow.Item("loanInstallment").ToString, decI)

                            decBenefitHeadAmt += decB
                            decInstHeadAmt += decI
                        Next
                        
                        If strLoanContractID.Contains("NO LOAN CONTRACT") = True Then
                            dr.Item("rowtypeid") = 2 'NO LOAN CONTRACT
                            dr.Item("rowtype_remark") = strLoanContractID
                            dr.Item("medicalrefno") = ""
                        Else
                            dr.Item("rowtypeid") = 0 'Success
                            dr.Item("rowtype_remark") = ""
                            dr.Item("medicalrefno") = strLoanContractID
                        End If
                    Catch ex As Exception
                        dr.Item("rowtypeid") = 4 'Invalid Data / XML
                        dr.Item("rowtype_remark") = ex.Message & "; " & strLoanContractID & " Or Invalid Data / XML " & response
                        dr.Item("medicalrefno") = ""
                    End Try

                Catch ex As Exception
                    response = ex.Message
                    dr.Item("rowtypeid") = 1 'Net Connectivity issue
                    dr.Item("rowtype_remark") = response & " Or Net Connectivity issue."
                    dr.Item("amount") = 0
                    dr.Item("medicalrefno") = ""
                End Try

                dr.Item("edunkid") = 0
                dr.Item("employeeunkid") = dtRow.Item("employeeunkid")
                dr.Item("employeecode") = dtRow.Item("employeecode")
                dr.Item("employeename") = dtRow.Item("EmpName")

                dr.Item("currencyId") = 0
                dr.Item("isvoid") = 0
                dr.Item("voiduserunkId") = -1
                dr.Item("voiddatetime") = DBNull.Value
                dr.Item("voidreason") = ""
                dr.Item("formula") = ""
                dr.Item("formulaid") = ""
                dr.Item("periodunkid") = CInt(cboPayPeriod.SelectedValue)
                dr.Item("period_name") = cboPayPeriod.Text
                dr.Item("start_date") = CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString
                dr.Item("end_date") = CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString
                If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                    dr.Item("isapproved") = True
                    dr.Item("approveruserunkid") = User._Object._Userunkid
                Else
                    dr.Item("isapproved") = False
                    dr.Item("approveruserunkid") = 0
                End If

                dr2.ItemArray = dr.ItemArray

                For i As Integer = 0 To 1
                    intCnt += 1
                    objlblLoanDataCount.Text = "( " & intCnt.ToString & " / " & (mintCount * 2).ToString & " )"
                    Application.DoEvents()

                    If i = 0 Then
                        Dim intEDUnkid As Integer = objED.GetEDUnkID(FinancialYear._Object._DatabaseName, CInt(dtRow.Item("employeeunkid")), CInt(cboLoanBenefit.SelectedValue), CInt(cboPayPeriod.SelectedValue))

                        If intEDUnkid > 0 Then
                            objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = intEDUnkid
                            dr.Item("edunkid") = intEDUnkid
                            'dr.Item("costcenterunkid") = objED._CostCenterUnkID
                            dr.Item("membershiptranunkid") = objED._MembershipTranUnkid
                        Else
                            dr.Item("costcenterunkid") = 0
                            dr.Item("membershiptranunkid") = 0
                            If CInt(dr.Item("rowtypeid")) = 0 Then
                                dr.Item("rowtypeid") = 3 'Failure
                                dr.Item("rowtype_remark") = strBenefitHeadName & " " & Language.getMessage(mstrModuleName, 11, "Head is not assigned to this employee.")
                            End If
                        End If
                        dr.Item("amount") = Format(decBenefitHeadAmt, GUI.fmtCurrency)

                        dr.Item("tranheadunkid") = CInt(cboLoanBenefit.SelectedValue)
                        dr.Item("trnheadname") = strBenefitHeadName
                        dr.Item("trnheadtype_id") = intBenefitHeadTypeId
                        dr.Item("trnheadtype") = ""
                        dr.Item("typeof_id") = intBenefitHeadTypeOfId
                        dr.Item("calctype_id") = intBenefitHeadCalcTypeId
                        dr.Item("computeon_id") = intBenefitHeadComputeOnId
                        If intBenefitHeadTypeId = enTranHeadType.EarningForEmployees Then
                            dr.Item("isdeduct") = 0
                        ElseIf intBenefitHeadTypeId = enTranHeadType.DeductionForEmployee OrElse intBenefitHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions Then
                            dr.Item("isdeduct") = 1
                        Else
                            dr.Item("isdeduct") = 0
                        End If
                    Else

                        Dim intEDUnkid As Integer = objED.GetEDUnkID(FinancialYear._Object._DatabaseName, CInt(dtRow.Item("employeeunkid")), CInt(cboLoanInstallment.SelectedValue), CInt(cboPayPeriod.SelectedValue))

                        If intEDUnkid > 0 Then
                            objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = intEDUnkid
                            dr2.Item("edunkid") = intEDUnkid
                            'dr.Item("costcenterunkid") = objED._CostCenterUnkID
                            dr2.Item("membershiptranunkid") = objED._MembershipTranUnkid
                        Else
                            dr2.Item("costcenterunkid") = 0
                            dr2.Item("membershiptranunkid") = 0

                            If CInt(dr2.Item("rowtypeid")) = 0 Then
                                dr2.Item("rowtypeid") = 3 'Failure
                                dr2.Item("rowtype_remark") = strInstHeadName & " " & Language.getMessage(mstrModuleName, 11, "Head is not assigned to this employee.")
                            End If
                        End If
                        dr2.Item("amount") = Format(decInstHeadAmt, GUI.fmtCurrency)

                        dr2.Item("tranheadunkid") = CInt(cboLoanInstallment.SelectedValue)
                        dr2.Item("trnheadname") = strInstHeadName
                        dr2.Item("trnheadtype_id") = intInstHeadTypeId
                        dr2.Item("trnheadtype") = ""
                        dr2.Item("typeof_id") = intInstHeadTypeOfId
                        dr2.Item("calctype_id") = intInstHeadCalcTypeId
                        dr2.Item("computeon_id") = intInstHeadComputeOnId
                        If intInstHeadTypeId = enTranHeadType.EarningForEmployees Then
                            dr2.Item("isdeduct") = 0
                        ElseIf intInstHeadTypeId = enTranHeadType.DeductionForEmployee OrElse intInstHeadTypeId = enTranHeadType.EmployeesStatutoryDeductions Then
                            dr2.Item("isdeduct") = 1
                        Else
                            dr2.Item("isdeduct") = 0
                        End If
                    End If


                    If i = 0 Then
                        mdtTable.Rows.Add(dr)
                    Else
                        mdtTable.Rows.Add(dr2)
                    End If


                Next
            Next

            m_Dataview = New DataView(mdtTable)
            m_Dataview.Sort = "rowtypeid, employeename "
            m_Dataview.RowFilter = "rowtypeid <> 0 AND rowtypeid <> 2 "

            If m_Dataview.Count > 0 Then
                btnImport.Enabled = False
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "To Show Successful data, Please click on Operations -> Show Successful data."))
            Else
                m_Dataview.RowFilter = "rowtypeid = 0"
            End If


            Call FillGirdView()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGetData_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click

        If mdtTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot import data. Reason :There is no transaction to import."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        m_Dataview.RowFilter = "rowtypeid = 0 "
        mdtFilteredTable = m_Dataview.ToTable

        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot import this file. Reason :Some transaction heads are not assigned to selected employees."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        mdtFilteredTable = New DataView(m_Dataview.ToTable, "rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
        If mdtFilteredTable.Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Tick atleast one transaction from list to Import."), enMsgBoxStyle.Information)
            Exit Sub
        End If


        Dim objED As clsEarningDeduction
        Dim objEmpCostCenter As clsemployee_costcenter_Tran
        Dim dsList As DataSet
        Dim blnResult As Boolean = False
        Dim objPeriod As clscommom_period_Tran

        Try
            If dvEmployee Is Nothing Then Exit Try

            If dvEmployee.Table.Select("IsChecked = 1 ").Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select atleast one employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Are you sure, you want to Update Loan Data to selected Employee(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
                Exit Try
            End If


            Cursor.Current = Cursors.WaitCursor

            For Each dtRow As DataRow In mdtFilteredTable.Rows
                objED = New clsEarningDeduction

                objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = CInt(dtRow.Item("edunkid"))
                If objED._Calctype_Id = enCalcType.FlatRate_Others Then
                    objED._Amount = CDec(dtRow.Item("amount"))
                End If
                objED._Userunkid = User._Object._Userunkid
                objED._MedicalRefNo = dtRow.Item("medicalrefno").ToString

                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = objED._Periodunkid

                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                    objED._Isapproved = True
                    objED._Approveruserunkid = User._Object._Userunkid
                Else
                    objED._Isapproved = False
                    objED._Approveruserunkid = -1
                End If
                'Sohail (02 Sep 2019) -- End

                blnResult = objED.Update(FinancialYear._Object._DatabaseName, False, Nothing, ConfigParameter._Object._CurrentDateAndTime)

                If blnResult = False AndAlso objED._Message <> "" Then
                    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If

                Dim blnExist As Boolean = False

                If CInt(dtRow.Item("costcenterunkid")) > 0 Then
                    blnExist = False
                    objEmpCostCenter = New clsemployee_costcenter_Tran
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    'dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CC", True, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("tranheadunkid")))
                    dsList = objEmpCostCenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "CC", True, CInt(dtRow.Item("employeeunkid")), CInt(dtRow.Item("tranheadunkid")), , objPeriod._End_Date)
                    'Sohail (29 Mar 2017) -- End

                    If dsList.Tables("CC").Rows.Count > 0 Then
                        objEmpCostCenter._Costcentertranunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables("CC").Rows(0).Item("costcentertranunkid").ToString)
                        blnExist = True
                    End If

                    objEmpCostCenter._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
                    objEmpCostCenter._Tranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                    objEmpCostCenter._Costcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    objEmpCostCenter._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                    'Sohail (29 Mar 2017) -- End
                    objEmpCostCenter._Userunkid = User._Object._Userunkid
                    If blnExist = True Then
                        blnResult = objEmpCostCenter.Update(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                    Else
                        'Sohail (07 Feb 2019) -- Start
                        'NMB Enhancement - 76.1 - Employee cost centre should be distributed on all allocations instead of only on cost centre.
                        objEmpCostCenter._AllocationbyId = enAllocation.COST_CENTER
                        'Sohail (07 Feb 2019) -- End
                        blnResult = objEmpCostCenter.Insert(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                    End If
                    If blnResult = False AndAlso objEmpCostCenter._Message <> "" Then
                        eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End If
            Next
            Cursor.Current = Cursors.Default

            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
            objED = Nothing
            objEmpCostCenter = Nothing
        End Try

    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If m_Dataview Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, m_Dataview.ToTable, "Import Employee Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub dgEmployee_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgEmployee.CurrentCellDirtyStateChanged
        Try
            If dgEmployee.IsCurrentCellDirty Then
                dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgEcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                Dim intCnt As Integer = m_Dataview.ToTable.Select("IsChecked = 1").Length

                RemoveHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                If intCnt <= 0 Then
                    objchkISelectAll.CheckState = CheckState.Unchecked
                ElseIf intCnt < dgvImportInfo.Rows.Count Then
                    objchkISelectAll.CheckState = CheckState.Indeterminate
                ElseIf intCnt = dgvImportInfo.Rows.Count Then
                    objchkISelectAll.CheckState = CheckState.Checked
                End If

                AddHandler objchkISelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvImportInfo_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvImportInfo.CurrentCellDirtyStateChanged
        Try
            If dgvImportInfo.IsCurrentCellDirty Then
                dgvImportInfo.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvImportInfo_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkISelectAll.CheckedChanged
        Try
            Call CheckAll(objchkISelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other control's Events "
    'Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
    '    Dim ofdlgOpen As New OpenFileDialog
    '    Dim ObjFile As IO.FileInfo
    '    Try
    '        ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath
    '        ofdlgOpen.Filter = "Excel files (*.xlsx)|*.xlsx"" '|XML files (*.xml)|*.xml"
    '        ofdlgOpen.FilterIndex = 0

    '        If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            ObjFile = New IO.FileInfo(ofdlgOpen.FileName)
    '            'txtFilePath.Text = ofdlgOpen.FileName

    '            Select Case ofdlgOpen.FilterIndex
    '                Case 1, 2
    '                    'dsList = objIExcel.Import(txtFilePath.Text)
    '            End Select
    '            Dim frm As New frmTransactionHeadMapping
    '            If frm.displayDialog(dsList) = False Then
    '                mblnCancel = True
    '                Exit Sub
    '            End If

    '            mdtTable = frm._DataTable
    '            m_Dataview = New DataView(mdtTable)
    '            m_Dataview.RowFilter = "rowtypeid = 0 "

    '            Call FillGirdView()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub txtSearchEmp_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.GotFocus
        Try
            With txtSearchEmp
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchEmpText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.Leave
        Try
            If txtSearchEmp.Text.Trim = "" Then
                Call SetDefaultSearchEmpText()
            End If
            'Call dgEmployee_SelectionChanged(dgEmployee, New System.EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If txtSearchEmp.Text.Trim = mstrSearchEmpText Then Exit Sub
            If dvEmployee IsNot Nothing Then
                'RemoveHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
                dvEmployee.RowFilter = "employeecode LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'  OR EmpName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR BankGrp LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR BranchName LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%' OR accountno LIKE '%" & txtSearchEmp.Text.Replace("'", "''") & "%'"
                dgEmployee.Refresh()
                'AddHandler dgEmployee.SelectionChanged, AddressOf dgEmployee_SelectionChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False 'Sohail (09 Oct 2010)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            If cboPayPeriod.Items.Count > 0 Then cboPayPeriod.SelectedValue = 0
            If cboBankGroup.Items.Count > 0 Then cboBankGroup.SelectedValue = 0
            If cboBankBranch.Items.Count > 0 Then cboBankBranch.SelectedValue = 0
            If cboLoanBenefit.Items.Count > 0 Then cboLoanBenefit.SelectedValue = 0
            If cboLoanInstallment.Items.Count > 0 Then cboLoanInstallment.SelectedValue = 0
            If txtSearchEmp.Text.Length > 0 Then txtSearchEmp.Text = ""

            mstrAdvanceFilter = ""

            dgEmployee.DataSource = Nothing
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 0 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowUnSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowUnSuccessful.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid <> 0 AND rowtypeid <> 2  "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowUnSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuShowNoLoanContract_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowNoLoanContract.Click
        Try
            btnImport.Enabled = False
            If m_Dataview Is Nothing Then Exit Sub

            m_Dataview.RowFilter = "rowtypeid = 2 "

            For Each dtRow As DataRowView In m_Dataview
                dtRow.Item("IsChecked") = False
            Next

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowNoLoanContract_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub picExpand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picExpand.Click
        Try
            If picExpand.Tag.ToString = "Collapsed" Then
                picExpand.Image = My.Resources.left_arrow_16
                gbEmployeeList.Width = 591
                picExpand.Tag = "Expanded"

            Else
                picExpand.Image = My.Resources.right_arrow_16
                gbEmployeeList.Width = 241
                picExpand.Tag = "Collapsed"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "picExpand_Click", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbT24ACBLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbT24ACBLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnHeadOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnHeadOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnGetData.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGetData.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnHeadOperations.Text = Language._Object.getCaption(Me.btnHeadOperations.Name, Me.btnHeadOperations.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
			Me.lblBankBranch.Text = Language._Object.getCaption(Me.lblBankBranch.Name, Me.lblBankBranch.Text)
			Me.gbT24ACBLoanInfo.Text = Language._Object.getCaption(Me.gbT24ACBLoanInfo.Name, Me.gbT24ACBLoanInfo.Text)
			Me.lblLoanInstallment.Text = Language._Object.getCaption(Me.lblLoanInstallment.Name, Me.lblLoanInstallment.Text)
			Me.lblLoanBenefit.Text = Language._Object.getCaption(Me.lblLoanBenefit.Name, Me.lblLoanBenefit.Text)
			Me.lblLink.Text = Language._Object.getCaption(Me.lblLink.Name, Me.lblLink.Text)
			Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
			Me.lblUserId.Text = Language._Object.getCaption(Me.lblUserId.Name, Me.lblUserId.Text)
			Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
			Me.mnuShowUnSuccessful.Text = Language._Object.getCaption(Me.mnuShowUnSuccessful.Name, Me.mnuShowUnSuccessful.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.dgColhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgColhEmployeeCode.Name, Me.dgColhEmployeeCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhBankGroup.HeaderText = Language._Object.getCaption(Me.dgColhBankGroup.Name, Me.dgColhBankGroup.HeaderText)
			Me.dgColhBankBranch.HeaderText = Language._Object.getCaption(Me.dgColhBankBranch.Name, Me.dgColhBankBranch.HeaderText)
			Me.dgColhAccountNo.HeaderText = Language._Object.getCaption(Me.dgColhAccountNo.Name, Me.dgColhAccountNo.HeaderText)
			Me.btnGetData.Text = Language._Object.getCaption(Me.btnGetData.Name, Me.btnGetData.Text)
			Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
			Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
			Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhMedicalRefCode.HeaderText = Language._Object.getCaption(Me.dgcolhMedicalRefCode.Name, Me.dgcolhMedicalRefCode.HeaderText)
			Me.dgcolhRemarks.HeaderText = Language._Object.getCaption(Me.dgcolhRemarks.Name, Me.dgcolhRemarks.HeaderText)
			Me.mnuShowNoLoanContract.Text = Language._Object.getCaption(Me.mnuShowNoLoanContract.Name, Me.mnuShowNoLoanContract.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Type to Search Emp. Code, Emp. Name, Bank, Branch, A/C No.")
			Language.setMessage(mstrModuleName, 2, "Type to Search")
			Language.setMessage(mstrModuleName, 3, "Please enter Link.")
			Language.setMessage(mstrModuleName, 4, "Please enter User Id.")
			Language.setMessage(mstrModuleName, 5, "Please enter Password.")
			Language.setMessage(mstrModuleName, 6, "Please select head for Loan Benefit.")
			Language.setMessage(mstrModuleName, 7, "Please select head for Loan Installment.")
			Language.setMessage(mstrModuleName, 8, "Please select Period.")
			Language.setMessage(mstrModuleName, 9, "Please select atleast one employee.")
			Language.setMessage(mstrModuleName, 10, "Sorry, This transaction head is already mapped.")
			Language.setMessage(mstrModuleName, 11, "Head is not assigned to this employee.")
			Language.setMessage(mstrModuleName, 12, "To Show Successful data, Please click on Operations -> Show Successful data.")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot import data. Reason :There is no transaction to import.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot import this file. Reason :Some transaction heads are not assigned to selected employees.")
			Language.setMessage(mstrModuleName, 15, "Please Tick atleast one transaction from list to Import.")
			Language.setMessage(mstrModuleName, 16, "Are you sure, you want to Update Loan Data to selected Employee(s)?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class