﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalAssignEarningDeduction
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalAssignEarningDeduction))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEarningDeduction = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboCopySelectedPeriodEDSlab = New System.Windows.Forms.ComboBox
        Me.chkCopySelectedPeriodEDSlab = New System.Windows.Forms.CheckBox
        Me.dtpCumulativeStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblCumulativeStartDate = New System.Windows.Forms.Label
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.txtMedicalRefNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblMedicalRefNo = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.radOverwrite = New System.Windows.Forms.RadioButton
        Me.objComputeOn = New System.Windows.Forms.ComboBox
        Me.radAdd = New System.Windows.Forms.RadioButton
        Me.radDontOverWrite = New System.Windows.Forms.RadioButton
        Me.objCalcType = New System.Windows.Forms.ComboBox
        Me.objTranHeadType = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblVendor = New System.Windows.Forms.Label
        Me.objlblCurrencyName = New System.Windows.Forms.Label
        Me.cboVendor = New System.Windows.Forms.ComboBox
        Me.lblCurrencyCountry = New System.Windows.Forms.Label
        Me.cboCurrencyCountry = New System.Windows.Forms.ComboBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.lblEffectiveFrom = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkNotAssignedHead = New System.Windows.Forms.CheckBox
        Me.chkShowReinstatedEmployees = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.chkShowNewlyHiredEmployees = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbgwGlobalAssignED = New System.ComponentModel.BackgroundWorker
        Me.dtpEDStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblEDStartDate = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbEarningDeduction.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbEarningDeduction)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(850, 572)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(292, 445)
        Me.gbEmployeeList.TabIndex = 0
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(122, 5)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(95, 16)
        Me.objlblEmpCount.TabIndex = 160
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(289, 416)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee})
        Me.dgEmployee.Location = New System.Drawing.Point(1, 28)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(286, 388)
        Me.dgEmployee.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(285, 21)
        Me.txtSearchEmp.TabIndex = 12
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(850, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Global Assign Transaction Head"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.lblSearchEmp)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(850, 55)
        Me.objFooter.TabIndex = 3
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(463, 21)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 15
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(9, 13)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(98, 13)
        Me.lblSearchEmp.TabIndex = 9
        Me.lblSearchEmp.Text = "Search Employee"
        Me.lblSearchEmp.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(638, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbEarningDeduction
        '
        Me.gbEarningDeduction.BorderColor = System.Drawing.Color.Black
        Me.gbEarningDeduction.Checked = False
        Me.gbEarningDeduction.CollapseAllExceptThis = False
        Me.gbEarningDeduction.CollapsedHoverImage = Nothing
        Me.gbEarningDeduction.CollapsedNormalImage = Nothing
        Me.gbEarningDeduction.CollapsedPressedImage = Nothing
        Me.gbEarningDeduction.CollapseOnLoad = False
        Me.gbEarningDeduction.Controls.Add(Me.dtpEDStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblEDStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.cboCopySelectedPeriodEDSlab)
        Me.gbEarningDeduction.Controls.Add(Me.chkCopySelectedPeriodEDSlab)
        Me.gbEarningDeduction.Controls.Add(Me.dtpCumulativeStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblCumulativeStartDate)
        Me.gbEarningDeduction.Controls.Add(Me.dtpStopDate)
        Me.gbEarningDeduction.Controls.Add(Me.lblStopDate)
        Me.gbEarningDeduction.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbEarningDeduction.Controls.Add(Me.txtAmount)
        Me.gbEarningDeduction.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbEarningDeduction.Controls.Add(Me.txtMedicalRefNo)
        Me.gbEarningDeduction.Controls.Add(Me.lblMedicalRefNo)
        Me.gbEarningDeduction.Controls.Add(Me.lblPeriod)
        Me.gbEarningDeduction.Controls.Add(Me.cboPeriod)
        Me.gbEarningDeduction.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbEarningDeduction.Controls.Add(Me.lblTypeOf)
        Me.gbEarningDeduction.Controls.Add(Me.cboTypeOf)
        Me.gbEarningDeduction.Controls.Add(Me.EZeeStraightLine2)
        Me.gbEarningDeduction.Controls.Add(Me.radOverwrite)
        Me.gbEarningDeduction.Controls.Add(Me.objComputeOn)
        Me.gbEarningDeduction.Controls.Add(Me.radAdd)
        Me.gbEarningDeduction.Controls.Add(Me.radDontOverWrite)
        Me.gbEarningDeduction.Controls.Add(Me.objCalcType)
        Me.gbEarningDeduction.Controls.Add(Me.objTranHeadType)
        Me.gbEarningDeduction.Controls.Add(Me.cboTrnHeadType)
        Me.gbEarningDeduction.Controls.Add(Me.lblVendor)
        Me.gbEarningDeduction.Controls.Add(Me.objlblCurrencyName)
        Me.gbEarningDeduction.Controls.Add(Me.cboVendor)
        Me.gbEarningDeduction.Controls.Add(Me.lblCurrencyCountry)
        Me.gbEarningDeduction.Controls.Add(Me.cboCurrencyCountry)
        Me.gbEarningDeduction.Controls.Add(Me.lblAmount)
        Me.gbEarningDeduction.Controls.Add(Me.cboTrnHead)
        Me.gbEarningDeduction.Controls.Add(Me.lblTrnHead)
        Me.gbEarningDeduction.Controls.Add(Me.lblEffectiveFrom)
        Me.gbEarningDeduction.ExpandedHoverImage = Nothing
        Me.gbEarningDeduction.ExpandedNormalImage = Nothing
        Me.gbEarningDeduction.ExpandedPressedImage = Nothing
        Me.gbEarningDeduction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEarningDeduction.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEarningDeduction.HeaderHeight = 25
        Me.gbEarningDeduction.HeaderMessage = ""
        Me.gbEarningDeduction.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEarningDeduction.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEarningDeduction.HeightOnCollapse = 0
        Me.gbEarningDeduction.LeftTextSpace = 0
        Me.gbEarningDeduction.Location = New System.Drawing.Point(310, 184)
        Me.gbEarningDeduction.Name = "gbEarningDeduction"
        Me.gbEarningDeduction.OpenHeight = 182
        Me.gbEarningDeduction.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEarningDeduction.ShowBorder = True
        Me.gbEarningDeduction.ShowCheckBox = False
        Me.gbEarningDeduction.ShowCollapseButton = False
        Me.gbEarningDeduction.ShowDefaultBorderColor = True
        Me.gbEarningDeduction.ShowDownButton = False
        Me.gbEarningDeduction.ShowHeader = True
        Me.gbEarningDeduction.Size = New System.Drawing.Size(528, 327)
        Me.gbEarningDeduction.TabIndex = 2
        Me.gbEarningDeduction.Temp = 0
        Me.gbEarningDeduction.Text = "Earning And Deduction Information"
        Me.gbEarningDeduction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCopySelectedPeriodEDSlab
        '
        Me.cboCopySelectedPeriodEDSlab.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCopySelectedPeriodEDSlab.DropDownWidth = 215
        Me.cboCopySelectedPeriodEDSlab.Enabled = False
        Me.cboCopySelectedPeriodEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCopySelectedPeriodEDSlab.FormattingEnabled = True
        Me.cboCopySelectedPeriodEDSlab.Location = New System.Drawing.Point(352, 162)
        Me.cboCopySelectedPeriodEDSlab.Name = "cboCopySelectedPeriodEDSlab"
        Me.cboCopySelectedPeriodEDSlab.Size = New System.Drawing.Size(116, 21)
        Me.cboCopySelectedPeriodEDSlab.TabIndex = 249
        '
        'chkCopySelectedPeriodEDSlab
        '
        Me.chkCopySelectedPeriodEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopySelectedPeriodEDSlab.Location = New System.Drawing.Point(335, 139)
        Me.chkCopySelectedPeriodEDSlab.Name = "chkCopySelectedPeriodEDSlab"
        Me.chkCopySelectedPeriodEDSlab.Size = New System.Drawing.Size(187, 17)
        Me.chkCopySelectedPeriodEDSlab.TabIndex = 248
        Me.chkCopySelectedPeriodEDSlab.Text = "Copy Selected Period ED Slab"
        Me.chkCopySelectedPeriodEDSlab.UseVisualStyleBackColor = True
        '
        'dtpCumulativeStartDate
        '
        Me.dtpCumulativeStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Checked = False
        Me.dtpCumulativeStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCumulativeStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCumulativeStartDate.Location = New System.Drawing.Point(123, 195)
        Me.dtpCumulativeStartDate.Name = "dtpCumulativeStartDate"
        Me.dtpCumulativeStartDate.ShowCheckBox = True
        Me.dtpCumulativeStartDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpCumulativeStartDate.TabIndex = 6
        '
        'lblCumulativeStartDate
        '
        Me.lblCumulativeStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCumulativeStartDate.Location = New System.Drawing.Point(8, 198)
        Me.lblCumulativeStartDate.Name = "lblCumulativeStartDate"
        Me.lblCumulativeStartDate.Size = New System.Drawing.Size(106, 15)
        Me.lblCumulativeStartDate.TabIndex = 245
        Me.lblCumulativeStartDate.Text = "Cumulat. Start Date"
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(123, 249)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpStopDate.TabIndex = 8
        '
        'lblStopDate
        '
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(8, 252)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(106, 15)
        Me.lblStopDate.TabIndex = 243
        Me.lblStopDate.Text = "Stop Date"
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(335, 192)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(187, 34)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 235
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous / Selected Period ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(123, 141)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(116, 21)
        Me.txtAmount.TabIndex = 4
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(335, 116)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(158, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 230
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'txtMedicalRefNo
        '
        Me.txtMedicalRefNo.Flags = 0
        Me.txtMedicalRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMedicalRefNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMedicalRefNo.Location = New System.Drawing.Point(123, 168)
        Me.txtMedicalRefNo.MaxLength = 50
        Me.txtMedicalRefNo.Name = "txtMedicalRefNo"
        Me.txtMedicalRefNo.Size = New System.Drawing.Size(172, 21)
        Me.txtMedicalRefNo.TabIndex = 5
        '
        'lblMedicalRefNo
        '
        Me.lblMedicalRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalRefNo.Location = New System.Drawing.Point(8, 171)
        Me.lblMedicalRefNo.Name = "lblMedicalRefNo"
        Me.lblMedicalRefNo.Size = New System.Drawing.Size(106, 15)
        Me.lblMedicalRefNo.TabIndex = 228
        Me.lblMedicalRefNo.Text = "Reference No."
        Me.lblMedicalRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(112, 14)
        Me.lblPeriod.TabIndex = 225
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(123, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(116, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = CType(resources.GetObject("objbtnSearchTranHead.Image"), System.Drawing.Image)
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(301, 114)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 222
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 90)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(112, 15)
        Me.lblTypeOf.TabIndex = 220
        Me.lblTypeOf.Text = "Transaction Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.DropDownWidth = 300
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(123, 87)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(172, 21)
        Me.cboTypeOf.TabIndex = 2
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(319, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(14, 316)
        Me.EZeeStraightLine2.TabIndex = 217
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'radOverwrite
        '
        Me.radOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOverwrite.Location = New System.Drawing.Point(335, 79)
        Me.radOverwrite.Name = "radOverwrite"
        Me.radOverwrite.Size = New System.Drawing.Size(158, 31)
        Me.radOverwrite.TabIndex = 7
        Me.radOverwrite.Text = "Overwrite selected head if Exist"
        Me.radOverwrite.UseVisualStyleBackColor = True
        '
        'objComputeOn
        '
        Me.objComputeOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objComputeOn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objComputeOn.FormattingEnabled = True
        Me.objComputeOn.Location = New System.Drawing.Point(275, 141)
        Me.objComputeOn.Name = "objComputeOn"
        Me.objComputeOn.Size = New System.Drawing.Size(43, 21)
        Me.objComputeOn.TabIndex = 148
        Me.objComputeOn.Visible = False
        '
        'radAdd
        '
        Me.radAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdd.Location = New System.Drawing.Point(335, 56)
        Me.radAdd.Name = "radAdd"
        Me.radAdd.Size = New System.Drawing.Size(126, 17)
        Me.radAdd.TabIndex = 8
        Me.radAdd.Text = "Add to Existing"
        Me.radAdd.UseVisualStyleBackColor = True
        '
        'radDontOverWrite
        '
        Me.radDontOverWrite.Checked = True
        Me.radDontOverWrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDontOverWrite.Location = New System.Drawing.Point(335, 33)
        Me.radDontOverWrite.Name = "radDontOverWrite"
        Me.radDontOverWrite.Size = New System.Drawing.Size(126, 17)
        Me.radDontOverWrite.TabIndex = 6
        Me.radDontOverWrite.TabStop = True
        Me.radDontOverWrite.Text = "Do not Overwrite"
        Me.radDontOverWrite.UseVisualStyleBackColor = True
        '
        'objCalcType
        '
        Me.objCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objCalcType.FormattingEnabled = True
        Me.objCalcType.Location = New System.Drawing.Point(252, 114)
        Me.objCalcType.Name = "objCalcType"
        Me.objCalcType.Size = New System.Drawing.Size(43, 21)
        Me.objCalcType.TabIndex = 147
        Me.objCalcType.Visible = False
        '
        'objTranHeadType
        '
        Me.objTranHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objTranHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTranHeadType.FormattingEnabled = True
        Me.objTranHeadType.Location = New System.Drawing.Point(252, 60)
        Me.objTranHeadType.Name = "objTranHeadType"
        Me.objTranHeadType.Size = New System.Drawing.Size(43, 21)
        Me.objTranHeadType.TabIndex = 146
        Me.objTranHeadType.Visible = False
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.DropDownWidth = 300
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(123, 60)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(172, 21)
        Me.cboTrnHeadType.TabIndex = 1
        '
        'lblVendor
        '
        Me.lblVendor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVendor.Location = New System.Drawing.Point(252, 173)
        Me.lblVendor.Name = "lblVendor"
        Me.lblVendor.Size = New System.Drawing.Size(54, 15)
        Me.lblVendor.TabIndex = 144
        Me.lblVendor.Text = "Vendor"
        Me.lblVendor.Visible = False
        '
        'objlblCurrencyName
        '
        Me.objlblCurrencyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCurrencyName.Location = New System.Drawing.Point(301, 171)
        Me.objlblCurrencyName.Name = "objlblCurrencyName"
        Me.objlblCurrencyName.Size = New System.Drawing.Size(50, 15)
        Me.objlblCurrencyName.TabIndex = 141
        Me.objlblCurrencyName.Text = "###"
        Me.objlblCurrencyName.Visible = False
        '
        'cboVendor
        '
        Me.cboVendor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVendor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVendor.FormattingEnabled = True
        Me.cboVendor.Location = New System.Drawing.Point(216, 141)
        Me.cboVendor.Name = "cboVendor"
        Me.cboVendor.Size = New System.Drawing.Size(116, 21)
        Me.cboVendor.TabIndex = 5
        Me.cboVendor.Visible = False
        '
        'lblCurrencyCountry
        '
        Me.lblCurrencyCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrencyCountry.Location = New System.Drawing.Point(254, 150)
        Me.lblCurrencyCountry.Name = "lblCurrencyCountry"
        Me.lblCurrencyCountry.Size = New System.Drawing.Size(52, 15)
        Me.lblCurrencyCountry.TabIndex = 140
        Me.lblCurrencyCountry.Text = "Country Curr."
        Me.lblCurrencyCountry.Visible = False
        '
        'cboCurrencyCountry
        '
        Me.cboCurrencyCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrencyCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrencyCountry.FormattingEnabled = True
        Me.cboCurrencyCountry.Location = New System.Drawing.Point(312, 147)
        Me.cboCurrencyCountry.Name = "cboCurrencyCountry"
        Me.cboCurrencyCountry.Size = New System.Drawing.Size(34, 21)
        Me.cboCurrencyCountry.TabIndex = 2
        Me.cboCurrencyCountry.Visible = False
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 144)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(112, 15)
        Me.lblAmount.TabIndex = 135
        Me.lblAmount.Text = "Amount"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.DropDownWidth = 300
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(123, 114)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(172, 21)
        Me.cboTrnHead.TabIndex = 3
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 117)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(112, 15)
        Me.lblTrnHead.TabIndex = 134
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'lblEffectiveFrom
        '
        Me.lblEffectiveFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveFrom.Location = New System.Drawing.Point(8, 63)
        Me.lblEffectiveFrom.Name = "lblEffectiveFrom"
        Me.lblEffectiveFrom.Size = New System.Drawing.Size(112, 15)
        Me.lblEffectiveFrom.TabIndex = 133
        Me.lblEffectiveFrom.Text = "Tran. Head Type"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkNotAssignedHead)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowReinstatedEmployees)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowNewlyHiredEmployees)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(310, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(528, 112)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Employee Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNotAssignedHead
        '
        Me.chkNotAssignedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNotAssignedHead.Location = New System.Drawing.Point(82, 87)
        Me.chkNotAssignedHead.Name = "chkNotAssignedHead"
        Me.chkNotAssignedHead.Size = New System.Drawing.Size(429, 21)
        Me.chkNotAssignedHead.TabIndex = 313
        Me.chkNotAssignedHead.Text = "Show Employees Not Assigned Selected Head in Selected Period"
        Me.chkNotAssignedHead.UseVisualStyleBackColor = True
        '
        'chkShowReinstatedEmployees
        '
        Me.chkShowReinstatedEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReinstatedEmployees.Location = New System.Drawing.Point(245, 33)
        Me.chkShowReinstatedEmployees.Name = "chkShowReinstatedEmployees"
        Me.chkShowReinstatedEmployees.Size = New System.Drawing.Size(277, 21)
        Me.chkShowReinstatedEmployees.TabIndex = 311
        Me.chkShowReinstatedEmployees.Text = "Show Only Reinstated Employees in selected Period"
        Me.chkShowReinstatedEmployees.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(345, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(106, 13)
        Me.lnkAllocation.TabIndex = 309
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Advance Filter"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkShowNewlyHiredEmployees
        '
        Me.chkShowNewlyHiredEmployees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowNewlyHiredEmployees.Location = New System.Drawing.Point(82, 60)
        Me.chkShowNewlyHiredEmployees.Name = "chkShowNewlyHiredEmployees"
        Me.chkShowNewlyHiredEmployees.Size = New System.Drawing.Size(291, 21)
        Me.chkShowNewlyHiredEmployees.TabIndex = 10
        Me.chkShowNewlyHiredEmployees.Text = "Show Only Newly Hired Employees in selected Period"
        Me.chkShowNewlyHiredEmployees.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(218, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(130, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(70, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(501, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(478, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objbgwGlobalAssignED
        '
        Me.objbgwGlobalAssignED.WorkerReportsProgress = True
        Me.objbgwGlobalAssignED.WorkerSupportsCancellation = True
        '
        'dtpEDStartDate
        '
        Me.dtpEDStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEDStartDate.Checked = False
        Me.dtpEDStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEDStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEDStartDate.Location = New System.Drawing.Point(123, 222)
        Me.dtpEDStartDate.Name = "dtpEDStartDate"
        Me.dtpEDStartDate.ShowCheckBox = True
        Me.dtpEDStartDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpEDStartDate.TabIndex = 7
        '
        'lblEDStartDate
        '
        Me.lblEDStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEDStartDate.Location = New System.Drawing.Point(8, 225)
        Me.lblEDStartDate.Name = "lblEDStartDate"
        Me.lblEDStartDate.Size = New System.Drawing.Size(106, 15)
        Me.lblEDStartDate.TabIndex = 251
        Me.lblEDStartDate.Text = "E. D. Start Date"
        '
        'frmGlobalAssignEarningDeduction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalAssignEarningDeduction"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assign Transaction Head"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbEarningDeduction.ResumeLayout(False)
        Me.gbEarningDeduction.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents gbEarningDeduction As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVendor As System.Windows.Forms.Label
    Friend WithEvents objlblCurrencyName As System.Windows.Forms.Label
    Friend WithEvents cboVendor As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrencyCountry As System.Windows.Forms.Label
    Friend WithEvents cboCurrencyCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents lblEffectiveFrom As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objTranHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents objCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents objComputeOn As System.Windows.Forms.ComboBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents radOverwrite As System.Windows.Forms.RadioButton
    Friend WithEvents radAdd As System.Windows.Forms.RadioButton
    Friend WithEvents radDontOverWrite As System.Windows.Forms.RadioButton
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents txtMedicalRefNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMedicalRefNo As System.Windows.Forms.Label
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowNewlyHiredEmployees As System.Windows.Forms.CheckBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents dtpCumulativeStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCumulativeStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents chkShowReinstatedEmployees As System.Windows.Forms.CheckBox
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents objbgwGlobalAssignED As System.ComponentModel.BackgroundWorker
    Friend WithEvents chkCopySelectedPeriodEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents cboCopySelectedPeriodEDSlab As System.Windows.Forms.ComboBox
    Friend WithEvents chkNotAssignedHead As System.Windows.Forms.CheckBox
    Friend WithEvents dtpEDStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEDStartDate As System.Windows.Forms.Label
End Class
