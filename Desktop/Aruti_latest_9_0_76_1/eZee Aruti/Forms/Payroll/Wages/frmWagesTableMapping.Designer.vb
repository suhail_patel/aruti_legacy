﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWagesTableMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWagesTableMapping))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.objlblASign7 = New System.Windows.Forms.Label
        Me.objlblASign6 = New System.Windows.Forms.Label
        Me.objlblASign5 = New System.Windows.Forms.Label
        Me.objlblASign4 = New System.Windows.Forms.Label
        Me.objlblASign3 = New System.Windows.Forms.Label
        Me.objlblASign2 = New System.Windows.Forms.Label
        Me.objlblASign1 = New System.Windows.Forms.Label
        Me.cboIncrement = New System.Windows.Forms.ComboBox
        Me.lblIncrement = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboMaximum = New System.Windows.Forms.ComboBox
        Me.lblMaximum = New System.Windows.Forms.Label
        Me.cboMidPoint = New System.Windows.Forms.ComboBox
        Me.cboMinimum = New System.Windows.Forms.ComboBox
        Me.cboGradeLevelCode = New System.Windows.Forms.ComboBox
        Me.cboGradeCode = New System.Windows.Forms.ComboBox
        Me.cboGradeGroupCode = New System.Windows.Forms.ComboBox
        Me.lblMidpoint = New System.Windows.Forms.Label
        Me.lblMinimum = New System.Windows.Forms.Label
        Me.lblGradeLevelCode = New System.Windows.Forms.Label
        Me.lblGradeCode = New System.Windows.Forms.Label
        Me.lblGradeGroupCode = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.lblMessage = New System.Windows.Forms.Label
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.txtRemark)
        Me.pnlMainInfo.Controls.Add(Me.gbFieldMapping)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(684, 353)
        Me.pnlMainInfo.TabIndex = 1
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(408, 12)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ReadOnly = True
        Me.txtRemark.Size = New System.Drawing.Size(264, 280)
        Me.txtRemark.TabIndex = 2
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign7)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign6)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign5)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign4)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign3)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign2)
        Me.gbFieldMapping.Controls.Add(Me.objlblASign1)
        Me.gbFieldMapping.Controls.Add(Me.cboIncrement)
        Me.gbFieldMapping.Controls.Add(Me.lblIncrement)
        Me.gbFieldMapping.Controls.Add(Me.lblPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboPeriod)
        Me.gbFieldMapping.Controls.Add(Me.cboMaximum)
        Me.gbFieldMapping.Controls.Add(Me.lblMaximum)
        Me.gbFieldMapping.Controls.Add(Me.cboMidPoint)
        Me.gbFieldMapping.Controls.Add(Me.cboMinimum)
        Me.gbFieldMapping.Controls.Add(Me.cboGradeLevelCode)
        Me.gbFieldMapping.Controls.Add(Me.cboGradeCode)
        Me.gbFieldMapping.Controls.Add(Me.cboGradeGroupCode)
        Me.gbFieldMapping.Controls.Add(Me.lblMidpoint)
        Me.gbFieldMapping.Controls.Add(Me.lblMinimum)
        Me.gbFieldMapping.Controls.Add(Me.lblGradeLevelCode)
        Me.gbFieldMapping.Controls.Add(Me.lblGradeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblGradeGroupCode)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(12, 12)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(390, 280)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.Location = New System.Drawing.Point(206, 251)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(166, 20)
        Me.lnkAutoMap.TabIndex = 61
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblASign7
        '
        Me.objlblASign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign7.ForeColor = System.Drawing.Color.Red
        Me.objlblASign7.Location = New System.Drawing.Point(15, 225)
        Me.objlblASign7.Name = "objlblASign7"
        Me.objlblASign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign7.TabIndex = 242
        Me.objlblASign7.Text = "*"
        Me.objlblASign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign6
        '
        Me.objlblASign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign6.ForeColor = System.Drawing.Color.Red
        Me.objlblASign6.Location = New System.Drawing.Point(15, 198)
        Me.objlblASign6.Name = "objlblASign6"
        Me.objlblASign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign6.TabIndex = 241
        Me.objlblASign6.Text = "*"
        Me.objlblASign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign5
        '
        Me.objlblASign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign5.ForeColor = System.Drawing.Color.Red
        Me.objlblASign5.Location = New System.Drawing.Point(15, 144)
        Me.objlblASign5.Name = "objlblASign5"
        Me.objlblASign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign5.TabIndex = 240
        Me.objlblASign5.Text = "*"
        Me.objlblASign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign4
        '
        Me.objlblASign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign4.ForeColor = System.Drawing.Color.Red
        Me.objlblASign4.Location = New System.Drawing.Point(15, 117)
        Me.objlblASign4.Name = "objlblASign4"
        Me.objlblASign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign4.TabIndex = 239
        Me.objlblASign4.Text = "*"
        Me.objlblASign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign3
        '
        Me.objlblASign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign3.ForeColor = System.Drawing.Color.Red
        Me.objlblASign3.Location = New System.Drawing.Point(15, 90)
        Me.objlblASign3.Name = "objlblASign3"
        Me.objlblASign3.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign3.TabIndex = 238
        Me.objlblASign3.Text = "*"
        Me.objlblASign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign2
        '
        Me.objlblASign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign2.ForeColor = System.Drawing.Color.Red
        Me.objlblASign2.Location = New System.Drawing.Point(15, 63)
        Me.objlblASign2.Name = "objlblASign2"
        Me.objlblASign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign2.TabIndex = 237
        Me.objlblASign2.Text = "*"
        Me.objlblASign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign1
        '
        Me.objlblASign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign1.ForeColor = System.Drawing.Color.Red
        Me.objlblASign1.Location = New System.Drawing.Point(15, 36)
        Me.objlblASign1.Name = "objlblASign1"
        Me.objlblASign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign1.TabIndex = 236
        Me.objlblASign1.Text = "*"
        Me.objlblASign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboIncrement
        '
        Me.cboIncrement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncrement.FormattingEnabled = True
        Me.cboIncrement.Location = New System.Drawing.Point(160, 223)
        Me.cboIncrement.Name = "cboIncrement"
        Me.cboIncrement.Size = New System.Drawing.Size(212, 21)
        Me.cboIncrement.TabIndex = 233
        '
        'lblIncrement
        '
        Me.lblIncrement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncrement.Location = New System.Drawing.Point(40, 225)
        Me.lblIncrement.Name = "lblIncrement"
        Me.lblIncrement.Size = New System.Drawing.Size(113, 16)
        Me.lblIncrement.TabIndex = 234
        Me.lblIncrement.Text = "Increment"
        Me.lblIncrement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(40, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(113, 14)
        Me.lblPeriod.TabIndex = 231
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(160, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(212, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'cboMaximum
        '
        Me.cboMaximum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMaximum.FormattingEnabled = True
        Me.cboMaximum.Location = New System.Drawing.Point(160, 196)
        Me.cboMaximum.Name = "cboMaximum"
        Me.cboMaximum.Size = New System.Drawing.Size(212, 21)
        Me.cboMaximum.TabIndex = 6
        '
        'lblMaximum
        '
        Me.lblMaximum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximum.Location = New System.Drawing.Point(40, 198)
        Me.lblMaximum.Name = "lblMaximum"
        Me.lblMaximum.Size = New System.Drawing.Size(113, 16)
        Me.lblMaximum.TabIndex = 12
        Me.lblMaximum.Text = "Maximum"
        Me.lblMaximum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMidPoint
        '
        Me.cboMidPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMidPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMidPoint.FormattingEnabled = True
        Me.cboMidPoint.Location = New System.Drawing.Point(160, 169)
        Me.cboMidPoint.Name = "cboMidPoint"
        Me.cboMidPoint.Size = New System.Drawing.Size(212, 21)
        Me.cboMidPoint.TabIndex = 5
        '
        'cboMinimum
        '
        Me.cboMinimum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMinimum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMinimum.FormattingEnabled = True
        Me.cboMinimum.Location = New System.Drawing.Point(160, 142)
        Me.cboMinimum.Name = "cboMinimum"
        Me.cboMinimum.Size = New System.Drawing.Size(212, 21)
        Me.cboMinimum.TabIndex = 4
        '
        'cboGradeLevelCode
        '
        Me.cboGradeLevelCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevelCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevelCode.FormattingEnabled = True
        Me.cboGradeLevelCode.Location = New System.Drawing.Point(160, 115)
        Me.cboGradeLevelCode.Name = "cboGradeLevelCode"
        Me.cboGradeLevelCode.Size = New System.Drawing.Size(212, 21)
        Me.cboGradeLevelCode.TabIndex = 3
        '
        'cboGradeCode
        '
        Me.cboGradeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeCode.FormattingEnabled = True
        Me.cboGradeCode.Location = New System.Drawing.Point(160, 88)
        Me.cboGradeCode.Name = "cboGradeCode"
        Me.cboGradeCode.Size = New System.Drawing.Size(212, 21)
        Me.cboGradeCode.TabIndex = 2
        '
        'cboGradeGroupCode
        '
        Me.cboGradeGroupCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroupCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroupCode.FormattingEnabled = True
        Me.cboGradeGroupCode.Location = New System.Drawing.Point(160, 61)
        Me.cboGradeGroupCode.Name = "cboGradeGroupCode"
        Me.cboGradeGroupCode.Size = New System.Drawing.Size(212, 21)
        Me.cboGradeGroupCode.TabIndex = 1
        '
        'lblMidpoint
        '
        Me.lblMidpoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMidpoint.Location = New System.Drawing.Point(40, 171)
        Me.lblMidpoint.Name = "lblMidpoint"
        Me.lblMidpoint.Size = New System.Drawing.Size(113, 16)
        Me.lblMidpoint.TabIndex = 5
        Me.lblMidpoint.Text = "Mid Point"
        Me.lblMidpoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMinimum
        '
        Me.lblMinimum.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinimum.Location = New System.Drawing.Point(40, 144)
        Me.lblMinimum.Name = "lblMinimum"
        Me.lblMinimum.Size = New System.Drawing.Size(113, 16)
        Me.lblMinimum.TabIndex = 4
        Me.lblMinimum.Text = "Minimum"
        Me.lblMinimum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeLevelCode
        '
        Me.lblGradeLevelCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevelCode.Location = New System.Drawing.Point(40, 117)
        Me.lblGradeLevelCode.Name = "lblGradeLevelCode"
        Me.lblGradeLevelCode.Size = New System.Drawing.Size(113, 16)
        Me.lblGradeLevelCode.TabIndex = 3
        Me.lblGradeLevelCode.Text = "Grade Level Code"
        Me.lblGradeLevelCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeCode
        '
        Me.lblGradeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeCode.Location = New System.Drawing.Point(40, 90)
        Me.lblGradeCode.Name = "lblGradeCode"
        Me.lblGradeCode.Size = New System.Drawing.Size(113, 16)
        Me.lblGradeCode.TabIndex = 2
        Me.lblGradeCode.Text = "Grade Code"
        Me.lblGradeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeGroupCode
        '
        Me.lblGradeGroupCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroupCode.Location = New System.Drawing.Point(40, 63)
        Me.lblGradeGroupCode.Name = "lblGradeGroupCode"
        Me.lblGradeGroupCode.Size = New System.Drawing.Size(113, 16)
        Me.lblGradeGroupCode.TabIndex = 1
        Me.lblGradeGroupCode.Text = "Grade Group Code"
        Me.lblGradeGroupCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.lblMessage)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 298)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(684, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.Color.Red
        Me.lblMessage.Location = New System.Drawing.Point(27, 22)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(0, 13)
        Me.lblMessage.TabIndex = 2
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(575, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(472, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'frmWagesTableMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 353)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWagesTableMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Wages Table Mapping"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbFieldMapping.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.objefFormFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboMaximum As System.Windows.Forms.ComboBox
    Friend WithEvents lblMaximum As System.Windows.Forms.Label
    Friend WithEvents cboMidPoint As System.Windows.Forms.ComboBox
    Friend WithEvents cboMinimum As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeLevelCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeGroupCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblMidpoint As System.Windows.Forms.Label
    Friend WithEvents lblMinimum As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevelCode As System.Windows.Forms.Label
    Friend WithEvents lblGradeCode As System.Windows.Forms.Label
    Friend WithEvents lblGradeGroupCode As System.Windows.Forms.Label
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents cboIncrement As System.Windows.Forms.ComboBox
    Friend WithEvents lblIncrement As System.Windows.Forms.Label
    Friend WithEvents objlblASign7 As System.Windows.Forms.Label
    Friend WithEvents objlblASign6 As System.Windows.Forms.Label
    Friend WithEvents objlblASign5 As System.Windows.Forms.Label
    Friend WithEvents objlblASign4 As System.Windows.Forms.Label
    Friend WithEvents objlblASign3 As System.Windows.Forms.Label
    Friend WithEvents objlblASign2 As System.Windows.Forms.Label
    Friend WithEvents objlblASign1 As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
End Class
