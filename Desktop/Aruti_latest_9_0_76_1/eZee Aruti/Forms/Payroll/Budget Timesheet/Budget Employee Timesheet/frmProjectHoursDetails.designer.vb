﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProjectHoursDetails
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProjectHoursDetails))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objLblActivityVal = New System.Windows.Forms.Label
        Me.LblActivity = New System.Windows.Forms.Label
        Me.objLblDonorGrantVal = New System.Windows.Forms.Label
        Me.LblDonorGrant = New System.Windows.Forms.Label
        Me.objLblProjectVal = New System.Windows.Forms.Label
        Me.LblProject = New System.Windows.Forms.Label
        Me.objLblPeriodVal = New System.Windows.Forms.Label
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.objLblEmpVal = New System.Windows.Forms.Label
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.dgvProjectHrDetails = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhParticulars = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvProjectHrDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objLblActivityVal)
        Me.pnlMain.Controls.Add(Me.LblActivity)
        Me.pnlMain.Controls.Add(Me.objLblDonorGrantVal)
        Me.pnlMain.Controls.Add(Me.LblDonorGrant)
        Me.pnlMain.Controls.Add(Me.objLblProjectVal)
        Me.pnlMain.Controls.Add(Me.LblProject)
        Me.pnlMain.Controls.Add(Me.objLblPeriodVal)
        Me.pnlMain.Controls.Add(Me.LblPeriod)
        Me.pnlMain.Controls.Add(Me.objLblEmpVal)
        Me.pnlMain.Controls.Add(Me.LblEmployee)
        Me.pnlMain.Controls.Add(Me.dgvProjectHrDetails)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(489, 366)
        Me.pnlMain.TabIndex = 0
        '
        'objLblActivityVal
        '
        Me.objLblActivityVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblActivityVal.Location = New System.Drawing.Point(104, 123)
        Me.objLblActivityVal.Name = "objLblActivityVal"
        Me.objLblActivityVal.Size = New System.Drawing.Size(370, 18)
        Me.objLblActivityVal.TabIndex = 16
        Me.objLblActivityVal.Text = "#Activity"
        '
        'LblActivity
        '
        Me.LblActivity.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActivity.Location = New System.Drawing.Point(12, 123)
        Me.LblActivity.Name = "LblActivity"
        Me.LblActivity.Size = New System.Drawing.Size(85, 18)
        Me.LblActivity.TabIndex = 15
        Me.LblActivity.Text = "Activity"
        '
        'objLblDonorGrantVal
        '
        Me.objLblDonorGrantVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblDonorGrantVal.Location = New System.Drawing.Point(104, 96)
        Me.objLblDonorGrantVal.Name = "objLblDonorGrantVal"
        Me.objLblDonorGrantVal.Size = New System.Drawing.Size(370, 18)
        Me.objLblDonorGrantVal.TabIndex = 14
        Me.objLblDonorGrantVal.Text = "#Donor/Grant"
        '
        'LblDonorGrant
        '
        Me.LblDonorGrant.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDonorGrant.Location = New System.Drawing.Point(12, 96)
        Me.LblDonorGrant.Name = "LblDonorGrant"
        Me.LblDonorGrant.Size = New System.Drawing.Size(85, 18)
        Me.LblDonorGrant.TabIndex = 13
        Me.LblDonorGrant.Text = "Donor/Grant"
        '
        'objLblProjectVal
        '
        Me.objLblProjectVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblProjectVal.Location = New System.Drawing.Point(104, 68)
        Me.objLblProjectVal.Name = "objLblProjectVal"
        Me.objLblProjectVal.Size = New System.Drawing.Size(370, 18)
        Me.objLblProjectVal.TabIndex = 12
        Me.objLblProjectVal.Text = "#Project"
        '
        'LblProject
        '
        Me.LblProject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProject.Location = New System.Drawing.Point(12, 68)
        Me.LblProject.Name = "LblProject"
        Me.LblProject.Size = New System.Drawing.Size(85, 18)
        Me.LblProject.TabIndex = 11
        Me.LblProject.Text = "Project"
        '
        'objLblPeriodVal
        '
        Me.objLblPeriodVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblPeriodVal.Location = New System.Drawing.Point(104, 40)
        Me.objLblPeriodVal.Name = "objLblPeriodVal"
        Me.objLblPeriodVal.Size = New System.Drawing.Size(370, 18)
        Me.objLblPeriodVal.TabIndex = 8
        Me.objLblPeriodVal.Text = "#Period"
        '
        'LblPeriod
        '
        Me.LblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(12, 40)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(85, 18)
        Me.LblPeriod.TabIndex = 7
        Me.LblPeriod.Text = "Period"
        '
        'objLblEmpVal
        '
        Me.objLblEmpVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLblEmpVal.Location = New System.Drawing.Point(104, 12)
        Me.objLblEmpVal.Name = "objLblEmpVal"
        Me.objLblEmpVal.Size = New System.Drawing.Size(370, 18)
        Me.objLblEmpVal.TabIndex = 6
        Me.objLblEmpVal.Text = "#Employee"
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(12, 12)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(85, 18)
        Me.LblEmployee.TabIndex = 5
        Me.LblEmployee.Text = "Employee"
        '
        'dgvProjectHrDetails
        '
        Me.dgvProjectHrDetails.AllowUserToAddRows = False
        Me.dgvProjectHrDetails.AllowUserToDeleteRows = False
        Me.dgvProjectHrDetails.AllowUserToResizeRows = False
        Me.dgvProjectHrDetails.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvProjectHrDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvProjectHrDetails.ColumnHeadersHeight = 25
        Me.dgvProjectHrDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhParticulars, Me.dgcolhHours})
        Me.dgvProjectHrDetails.Location = New System.Drawing.Point(0, 151)
        Me.dgvProjectHrDetails.Name = "dgvProjectHrDetails"
        Me.dgvProjectHrDetails.ReadOnly = True
        Me.dgvProjectHrDetails.RowHeadersVisible = False
        Me.dgvProjectHrDetails.RowTemplate.ReadOnly = True
        Me.dgvProjectHrDetails.Size = New System.Drawing.Size(488, 159)
        Me.dgvProjectHrDetails.TabIndex = 4
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 310)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(489, 56)
        Me.objFooter.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(384, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 450
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.HeaderText = "Hours"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 115
        '
        'dgcolhParticulars
        '
        Me.dgcolhParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgcolhParticulars.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhParticulars.Frozen = True
        Me.dgcolhParticulars.HeaderText = "Particulars"
        Me.dgcolhParticulars.Name = "dgcolhParticulars"
        Me.dgcolhParticulars.ReadOnly = True
        Me.dgcolhParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhParticulars.Width = 375
        '
        'dgcolhHours
        '
        Me.dgcolhHours.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhHours.HeaderText = "Hours"
        Me.dgcolhHours.Name = "dgcolhHours"
        Me.dgcolhHours.ReadOnly = True
        Me.dgcolhHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhHours.Width = 110
        '
        'frmProjectHoursDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 366)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProjectHoursDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Project & Activity Hours Details"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgvProjectHrDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvProjectHrDetails As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objLblEmpVal As System.Windows.Forms.Label
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents objLblPeriodVal As System.Windows.Forms.Label
    Friend WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents objLblProjectVal As System.Windows.Forms.Label
    Friend WithEvents LblProject As System.Windows.Forms.Label
    Friend WithEvents LblDonorGrant As System.Windows.Forms.Label
    Friend WithEvents objLblDonorGrantVal As System.Windows.Forms.Label
    Friend WithEvents objLblActivityVal As System.Windows.Forms.Label
    Friend WithEvents LblActivity As System.Windows.Forms.Label
    Friend WithEvents dgcolhParticulars As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHours As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
