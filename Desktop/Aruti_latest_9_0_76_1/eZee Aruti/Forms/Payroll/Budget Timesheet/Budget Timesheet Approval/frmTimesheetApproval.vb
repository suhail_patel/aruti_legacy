﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO
Imports System.Web.UI.WebControls
Imports ArutiReports 'Nilay (21 Mar 2017)

Public Class frmTimesheetApproval

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmTimesheetApproval"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrSearchText As String = ""
    Private mintPeriodID As Integer = 0
    Private mintApproverID As Integer = 0
    Private mintApproverEmployeeID As Integer = 0
    Private mstrApproverName As String = ""
    Private mintPriority As Integer = 0
    Private mintBudgetMstID As Integer = 0
    Private mintShiftWorkingHrs As Integer = 0
    Private mdecPercentageHrs As Decimal = 0
    Private mintActivityWorkingHrs As Integer = 0
    Private mstrDefaultWorkingHRs As String = ""
    Private mblnIsExternalApprover As Boolean = False
    Private objTimesheetApproval As clstsemptimesheet_approval
    Private mblnIsApproved As Boolean = False
    Private mdtSelectedData As DataTable = Nothing
    Private mblnLastColumn As Boolean = False
    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    Private mdtApprovalData As DataTable = Nothing
    'Nilay (10 Jan 2017) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Nilay (21 Mar 2017) -- End

#End Region

#Region " Display Dialog "

    'Nilay (02-Jan-2017) -- Start
    'Issue #33: Enhancement - Implementing Budget Employee Time Sheet

    'Public Function displayDialog(ByVal xPeriodID As Integer, ByVal xApproverID As Integer, ByVal xApproverEmployeeId As Integer, ByVal dtTable As DataTable, ByVal xApproverName As String _
    '                                          , ByVal xblnIsExternalApprover As Boolean, ByVal eAction As enAction) As Boolean
    ' ByVal xPriority As Integer,
    Public Function displayDialog(ByVal xPeriodID As Integer, ByVal xPriority As Integer, ByVal dtTable As DataTable) As Boolean

        Try

            mdtSelectedData = dtTable
            mintPeriodID = xPeriodID
            mintPriority = xPriority
            'mintApproverID = xApproverID
            'mintApproverEmployeeID = xApproverEmployeeId
            'mstrApproverName = xApproverName
            'mblnIsExternalApprover = xblnIsExternalApprover
            'menAction = eAction

            'Nilay (02-Jan-2017) -- End
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTimesheetApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objTimesheetApproval = New clstsemptimesheet_approval
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            GetValue()
            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            SetVisibility()
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApproval_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetApproval_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheetApproval_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clstsemptimesheet_approval.SetMessages()
            objfrm._Other_ModuleNames = "clstsemptimesheet_approval"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmTimesheetApproval_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date.Date, "List", True)

            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsList.Tables(0)
            cboPeriod.SelectedValue = 0
            objPeriod = Nothing
            dsList = Nothing

            Dim objBudgetMst As New clsBudget_MasterNew
            dsList = objBudgetMst.GetComboList("List", False, True, "allocationbyid = 0")
            If dsList IsNot Nothing Then mintBudgetMstID = CInt(dsList.Tables(0).Rows(0)("budgetunkid"))
            dsList = Nothing
            objBudgetMst = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodID

            'Pinkal (05-Jun-2017) -- Start
            'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .
            'mdtPeriodStart = objPeriod._Start_Date.Date
            'mdtPeriodEnd = objPeriod._End_Date.Date
            mdtPeriodStart = objPeriod._TnA_StartDate.Date
            mdtPeriodEnd = objPeriod._TnA_EndDate.Date
            'Pinkal (05-Jun-2017) -- End


            cboPeriod.SelectedValue = mintPeriodID

            dgvEmpTimesheet.AutoGenerateColumns = False
            dgcolhParticular.DataPropertyName = "Particulars"
            dgcolhActivityDate.DataPropertyName = "activitydate"
            dgcolhApprover.DataPropertyName = "Approver"
            dgcolhDonor.DataPropertyName = "fundname"
            dgcolhProject.DataPropertyName = "fundprojectname"
            dgcolhActivity.DataPropertyName = "activity_name"
            dgcolhHours.DataPropertyName = "activity_hrs"
            objdgcolhActivityHrsInMins.DataPropertyName = "activity_hrsinMins"
            objdgcolhActivityPercentage.DataPropertyName = "percentage"
            objdgcolhPeriodID.DataPropertyName = "periodunkid"
            objdgcolhEmployeeID.DataPropertyName = "employeeunkid"
            objdgcolhFundSourceID.DataPropertyName = "fundsourceunkid"
            objdgcolhProjectID.DataPropertyName = "projectcodeunkid"
            objdgcolhActivityID.DataPropertyName = "activityunkid"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhMappedUserId.DataPropertyName = "mapuserunkid"
            objdgcolhStatusID.DataPropertyName = "ApprovalStatusId"
            objdgcolhEmployeeName.DataPropertyName = "Employee"
            objdgcolhActivityDate.DataPropertyName = "activitydate"
            objdgcolhPriority.DataPropertyName = "priority"
            objdgcolhApproverEmployeeID.DataPropertyName = "approveremployeeunkid"
            objdgcolhEmpTimesheetID.DataPropertyName = "emptimesheetunkid"
            objdgcolhTimesheetApprovalID.DataPropertyName = "timesheetapprovalunkid"
            objdgcolhIsExternalApprover.DataPropertyName = "isexternalapprover"
            dgcolhTotalActivityHours.DataPropertyName = "TotalActivityHours"
            objdgcolhTotalActivityHoursInMin.DataPropertyName = "TotalActivityHoursInMin"
            objdgcolhShiftHoursInSec.DataPropertyName = "ShiftHoursInSec"


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 199)  Working on Additional column to show the description filled by the employee, on timesheet submission screen, view completed submit for approval and timesheet approval
            dgcolhEmpDescription.DataPropertyName = "description"
            'Pinkal (28-Mar-2018) -- End


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgcolhSubmissionRemark.DataPropertyName = "submission_remark"
            'Pinkal (28-Jul-2018) -- End


            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            objdgcolhIsHoliday.DataPropertyName = "isholiday"
            'Pinkal (13-Aug-2018) -- End

            dgvEmpTimesheet.DataSource = mdtSelectedData
            SetGridColor()

            If mdtSelectedData IsNot Nothing AndAlso mdtSelectedData.Rows.Count > 0 Then
                dgvEmpTimesheet.Focus()
                dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(1).Cells(dgcolhHours.Index)
                SendKeys.Send("{f2}")
            End If

            'GetEmployeeBudgetCodes()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]

            Dim dr = From p As DataGridViewRow In dgvEmpTimesheet.Rows.Cast(Of DataGridViewRow)() Where CBool(p.Cells(objdgcolhIsGrp.Index).Value) = True Select p
            dr.ToList.ForEach(Function(x) SetRowStyle(x, True))

            Dim drDays = From p As DataGridViewRow In dgvEmpTimesheet.Rows.Cast(Of DataGridViewRow)() _
                           Where CBool(p.Cells(objdgcolhIsHoliday.Index).Value) = True Select p
            drDays.ToList.ForEach(Function(x) SetRowStyle(x, False))


            'Pinkal (13-Aug-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (13-Aug-2018) -- Start
    'Enhancement - Changes For PACT [Ref #249,252]

    Public Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal isHeader As Boolean) As Boolean
        Try
            If isHeader Then
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            xRow.DefaultCellStyle = dgvcsHeader
            xRow.ReadOnly = True
            Else
                xRow.DefaultCellStyle.ForeColor = Color.Red
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRowStyle", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (13-Aug-2018) -- End

    Public Shadows Sub DoubleBuffered(ByVal dgv As DataGridView, ByVal setting As Boolean)
        Dim dgvType As Type = dgv.GetType()
        Dim pi As System.Reflection.PropertyInfo = dgvType.GetProperty("DoubleBuffered", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic)
        pi.SetValue(dgv, setting, Nothing)
    End Sub

    Private Sub Setvalue()
        Try
            objTimesheetApproval._Periodunkid = CInt(cboPeriod.SelectedValue)
            objTimesheetApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'objTimesheetApproval._ApprovalData = New DataView(CType(dgvEmpTimesheet.DataSource, DataTable), "isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
            mdtApprovalData = New DataView(CType(dgvEmpTimesheet.DataSource, DataTable), "IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable
            objTimesheetApproval._ApprovalData = mdtApprovalData
            'Nilay (10 Jan 2017) -- End

            objTimesheetApproval._Statusunkid = CInt(IIf(mblnIsApproved, 1, 3))
            objTimesheetApproval._Visibleunkid = CInt(IIf(mblnIsApproved, 1, 3))

            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
            objTimesheetApproval._Tsapproverunkid = mintApproverID
            'objTimesheetApproval._Approveremployeeunkid = mintApproverEmployeeID
            'objTimesheetApproval._Priority = mintPriority
            'Nilay (02-Jan-2017) -- End

            objTimesheetApproval._Description = txtRemarks.Text.Trim
            objTimesheetApproval._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Setvalue", mstrModuleName)
        End Try
    End Sub

    Public Function TextBoxTimeValidation(ByVal txt As MaskedTextBox) As Boolean
        Try
            If txt.Text.Contains(" ") OrElse txt.Text.Contains("_") Then
                txt.Text = txt.Text.Trim.Replace(" ", "0")
                txt.Text = txt.Text.Trim.Replace("_", "0")
            End If


            If txt.Text.Trim = ":" Then
                txt.Text = "00:00"
            ElseIf CDec(txt.Text.Trim.Replace(":", ".")) > 24 Then
Isvalid:
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Invalid working hrs.Please enter valid working hrs."), enMsgBoxStyle.Information)
                txt.Focus()
                Return False

            ElseIf txt.Text.Trim.Contains(":") Then
                If txt.Text.Trim.Contains(":60") Then

calFromHr:
                    If CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4) <= 24 Then
                        txt.Text = CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4).ToString("#00.00").Replace(".", ":")
                    Else
                        GoTo Isvalid
                    End If
                ElseIf txt.Text <> "" And txt.Text <> ":" Then
                    If txt.Text.Trim.IndexOf(":") = 0 And txt.Text.Trim.Contains(":60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf(":"), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":")) = ":" Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(IIf(txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1) = "", "0", txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1))) > 59 Then
                        GoTo calFromHr
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TextBoxTimeValidation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private Sub SetVisibility()
        Try
            objdgcolhShowDetails.Visible = ConfigParameter._Object._ShowBgTimesheetActivityHrsDetail
            dgcolhActivity.Visible = ConfigParameter._Object._ShowBgTimesheetActivity
            dgcolhProject.Visible = ConfigParameter._Object._ShowBgTimesheetActivityProject
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Pinkal (28-Jul-2018) -- End


    '    Private Sub GetEmployeeBudgetCodes()
    '        Try
    '            Dim objBudgetCodes As New clsBudgetcodes_master
    '            Dim dsList As DataSet = Nothing
    '            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboActivity.SelectedValue) > 0 AndAlso CInt(txtEmployee.Tag) > 0 Then
    '                dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), mintBudgetMstID, CInt(cboActivity.SelectedValue), CInt(txtEmployee.Tag))
    '                Dim objEmpshift As New clsEmployee_Shift_Tran
    '                Dim mintShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(dtpDate.Value.Date, CInt(txtEmployee.Tag))
    '                objEmpshift = Nothing
    '                Dim objShiftTran As New clsshift_tran
    '                objShiftTran.GetShiftTran(mintShiftID)
    '                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.Value.Date, FirstDayOfWeek.Sunday) - 1)
    '                If drRow.Length > 0 AndAlso (dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0) Then
    '                    mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
    '                    mdecPercentageHrs = CDec(dsList.Tables(0).Rows(0)("percentage"))
    '                    GetWorkingMins(mintShiftWorkingHrs, mdecPercentageHrs, "")
    '                Else
    '                    mintShiftWorkingHrs = 0
    '                    mdecPercentageHrs = 0
    '                    mintActivityWorkingHrs = 0
    '                    txtHours.Text = ""
    '                    mstrDefaultWorkingHRs = "00:00"
    '                    GoTo ResetValue
    '                End If
    '            Else
    'ResetValue:
    '                mintShiftWorkingHrs = 0
    '                mdecPercentageHrs = 0
    '                txtHours.Text = ""
    '            End If
    '            objBudgetCodes = Nothing
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "GetEmployeeBudgetCodes", mstrModuleName)
    '        End Try
    '    End Sub

    'Private Sub GetWorkingMins(ByVal xShiftWorkingHrs As Integer, ByVal xPercentageHrs As Decimal, Optional ByVal mstrWorkingHrs As String = "")
    '    Try
    '        If mstrWorkingHrs.Trim.Length <= 0 Then
    '            Dim mintActivityHrs As Integer = CInt(mintShiftWorkingHrs * mdecPercentageHrs / 100)
    '            mstrDefaultWorkingHRs = CDec(CalculateTime(True, mintActivityHrs)).ToString("#00.00").Replace(".", ":")
    '        Else
    '            Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
    '            Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
    '            mintActivityWorkingHrs = (mintHours * 60) + mintMins
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "Button Events"

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
            Dim objPeriod As New clscommom_period_Tran
            Dim blnFlag As Boolean

            objPeriod._Periodunkid(ConfigParameter._Object._DatabaseUserName) = CInt(cboPeriod.SelectedValue)

            If CBool(ConfigParameter._Object._NotAllowIncompleteTimesheet) = True Then

                'Pinkal (02-Jul-2018) -- Start
                'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                '                                                     objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                                                                     objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                            clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, _
                                                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                                            ConfigParameter._Object._AllowOverTimeToEmpTimesheet)


                'Pinkal (28-Jul-2018) -- End


                'Pinkal (02-Jul-2018) -- End




                If objBudgetTimesheet._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot perform Approve operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set."), enMsgBoxStyle.Information)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot perform Approve operation. Reason : your timesheet is incomplete, please complete it."), enMsgBoxStyle.Information)
                        'Pinkal (31-Oct-2017) -- End
                        Exit Sub
                    End If
                End If
            End If

            mblnIsApproved = True
            Setvalue()
            If objTimesheetApproval.UpdateGlobalApproval(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid _
                                                       , User._Object._Userunkid, Company._Object._Companyunkid _
                                                       , ConfigParameter._Object._IsIncludeInactiveEmp) = False Then

                eZeeMsgBox.Show(objTimesheetApproval._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee timesheet approved successfully."), enMsgBoxStyle.Information)
                mblnCancel = False
                Dim strEmployeeIDs As String = String.Join(",", mdtApprovalData.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)


                'Pinkal (22-Jul-2019) -- Start
                'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.

                'objBudgetTimesheet.Send_Notification_Approver(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                '                                          Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                              ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                '                                              mintPriority, mdtSelectedData, enLogin_Mode.DESKTOP, _
                '                                              0, User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, True)

                objBudgetTimesheet.Send_Notification_Approver(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                          Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                                                              mintPriority, mdtSelectedData, enLogin_Mode.DESKTOP, _
                                                                                        0, User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, False)

                'Pinkal (22-Jul-2019) -- End

                Dim objEmployee As New clsEmployee_Master
                Dim dsEmp As DataSet = Nothing
                Dim strFilter As String = "hremployee_master.employeeunkid IN(" & strEmployeeIDs & ")"
                dsEmp = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , , , , , , , , , , , , , , , strFilter)

                For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))
                    Dim intEmployeeID As Integer = CInt(strEmpID)
                    Dim dtApproverList As DataTable = Nothing
                    Dim objApprover As New clstsapprover_master

                    dtApproverList = objApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                                     Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                     ConfigParameter._Object._IsIncludeInactiveEmp, -1, strEmpID, , Nothing, , _
                                                                     "priority > " & mintPriority & " ")

                    If dtApproverList Is Nothing OrElse dtApproverList.Rows.Count > 0 Then Continue For

                    Dim strEmpTimesheetIDs As String = String.Join(",", mdtApprovalData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID) _
                                                                   .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                    Dim objEmpTimesheetReport As New clsEmpBudgetTimesheetReport(User._Object._Languageunkid, Company._Object._Companyunkid)


                    'Pinkal (22-Jul-2019) -- Start
                    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                    'objEmpTimesheetReport._EmployeeID = intEmployeeID
                    'Dim dRow() As DataRow = dsEmp.Tables("Emp").Select("employeeunkid=" & intEmployeeID)
                    'If dRow.Length > 0 Then
                    '    objEmpTimesheetReport._EmployeeName = dRow(0).Item("employeename").ToString
                    '    objEmpTimesheetReport._EmployeeCode = dRow(0).Item("employeecode").ToString
                    'End If
                    Dim dRow() As DataRow = dsEmp.Tables("Emp").Select("employeeunkid=" & intEmployeeID)
                    If dRow.Length > 0 Then
                        objEmpTimesheetReport._dtEmployee = dRow.CopyToDataTable()
                    End If
                    objEmpTimesheetReport._ShowSubmissionRemark = True
                    'Pinkal (22-Jul-2019) -- End

                    objEmpTimesheetReport._PeriodId = CInt(cboPeriod.SelectedValue)
                    objEmpTimesheetReport._PeriodName = cboPeriod.Text
                    objEmpTimesheetReport._StartDate = mdtPeriodStart
                    objEmpTimesheetReport._EndDate = mdtPeriodEnd
                    objEmpTimesheetReport._ViewReportInHTML = True
                    objEmpTimesheetReport._OpenAfterExport = False
                    objEmpTimesheetReport._ExportReportPath = mstrExportEPaySlipPath

                    objEmpTimesheetReport.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                ConfigParameter._Object._UserAccessModeSetting, True, True)

                    If objEmpTimesheetReport._FileNameAfterExported <> "" Then

                        objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, CStr(intEmployeeID), strEmpTimesheetIDs, mintPeriodID, _
                                                              clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, enLogin_Mode.DESKTOP, 0, _
                                                                      User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, , _
                                                                      mstrExportEPaySlipPath, objEmpTimesheetReport._FileNameAfterExported, False)
                    End If
                Next

                'Pinkal (04-May-2018) -- Start
                'Enhancement - Budget timesheet Enhancement for MST/THPS.
                'objBudgetTimesheet.StartSendingEmail(Company._Object._Companyunkid)
                'Pinkal (04-May-2018) -- End

                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            If txtRemarks.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is required information."), enMsgBoxStyle.Information)
                txtRemarks.Focus()
                Exit Sub
            End If

            Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
            Dim objPeriod As New clscommom_period_Tran
            Dim blnFlag As Boolean

            objPeriod._Periodunkid(ConfigParameter._Object._DatabaseUserName) = CInt(cboPeriod.SelectedValue)

            If CBool(ConfigParameter._Object._NotAllowIncompleteTimesheet) = True Then

                'Pinkal (02-Jul-2018) -- Start
                'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))



                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                '                                                    objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtSelectedData, CInt(objPeriod._Constant_Days), _
                                                                    objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                           clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, _
                                                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                                           ConfigParameter._Object._AllowOverTimeToEmpTimesheet)

                'Pinkal (28-Jul-2018) -- End

                'Pinkal (02-Jul-2018) -- End


                

                If objBudgetTimesheet._Message <> "" Then
                    eZeeMsgBox.Show(objBudgetTimesheet._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot perform Reject operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set."), enMsgBoxStyle.Information)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot perform Reject operation. Reason : your timesheet is incomplete, please complete it."), enMsgBoxStyle.Information)
                        'Pinkal (31-Oct-2017) -- End
                        Exit Sub
                    End If
                End If
            End If

            mblnIsApproved = False
            Setvalue()
            If objTimesheetApproval.UpdateGlobalApproval(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid _
                                                       , User._Object._Userunkid, Company._Object._Companyunkid _
                                                                        , ConfigParameter._Object._IsIncludeInactiveEmp) = False Then

                eZeeMsgBox.Show(objTimesheetApproval._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee timesheet rejected successfully."), enMsgBoxStyle.Information)

                Dim strEmployeeIDs As String = String.Join(",", mdtApprovalData.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                Dim strEmpTimesheetIDs As String = String.Join(",", mdtApprovalData.AsEnumerable().Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                objBudgetTimesheet.Send_Notification_Employee(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, strEmployeeIDs, strEmpTimesheetIDs, mintPeriodID, _
                                                              clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, enLogin_Mode.DESKTOP, 0, _
                                                              User._Object._Userunkid, ConfigParameter._Object._ArutiSelfServiceURL, txtRemarks.Text, , , False)

                mblnCancel = False

                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReject_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    'Nilay (15 Feb 2017) -- Start
    Private Sub dgvEmpTimesheet_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpTimesheet.CellEndEdit
        Try
            If e.RowIndex <= 0 Then Exit Sub
            If dgvEmpTimesheet.Rows.Count - 1 = dgvEmpTimesheet.CurrentRow.Index Then
            If dgvEmpTimesheet.CurrentCell.ColumnIndex = dgcolhHours.Index Then
                    SendKeys.Send("{TAB}")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellEndEdit", mstrModuleName)
        End Try
    End Sub
    'Nilay (15 Feb 2017) -- End

    Private Sub dgvEmpTimesheet_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dgvEmpTimesheet.ColumnWidthChanged
        Try
            Dim rIsgrp As Rectangle = dgvEmpTimesheet.DisplayRectangle
            rIsgrp.Height = CInt(dgvEmpTimesheet.ColumnHeadersHeight / 2)
            dgvEmpTimesheet.Invalidate(rIsgrp)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_ColumnWidthChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvEmpTimesheet.Scroll
        Try
            DoubleBuffered(dgvEmpTimesheet, True)
            dgvEmpTimesheet.PerformLayout()
            dgvEmpTimesheet.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvEmpTimesheet.Paint
        Try
            For i As Integer = 0 To dgvEmpTimesheet.Rows.Count - 1
                If CBool(dgvEmpTimesheet.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    Dim r1 As Rectangle = Nothing
                    Dim w2 As Integer = 0
                    Dim format As New StringFormat()
                    format.Alignment = StringAlignment.Near
                    format.LineAlignment = StringAlignment.Center
                    For j As Integer = 0 To dgvEmpTimesheet.ColumnCount - 7
                        If j = 0 Then r1 = dgvEmpTimesheet.GetCellDisplayRectangle(j, i, True)
                        w2 = dgvEmpTimesheet.GetCellDisplayRectangle(j + 1, i, True).Width
                        r1.Width = r1.Width + w2 - 1
                        e.Graphics.FillRectangle(New SolidBrush(Color.Gray), r1)
                    Next
                    e.Graphics.DrawString(dgvEmpTimesheet.Rows(i).Cells(dgcolhParticular.Index).Value.ToString(), New Font(dgvEmpTimesheet.DefaultCellStyle.Font, FontStyle.Bold), New SolidBrush(Color.White), r1, format)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_Paint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvEmpTimesheet.CellPainting
        Try
            If e.RowIndex < 0 Then Exit Sub
            If CBool(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) Then
                Dim r2 As Rectangle = e.CellBounds
                r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
                r2.Height = CInt(e.CellBounds.Height / 2)
                e.PaintBackground(r2, True)
                e.PaintContent(r2)
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvEmpTimesheet.CellValidating
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.FormattedValue.ToString().Trim.Length <= 0 Then Exit Sub

            If e.ColumnIndex = dgcolhHours.Index Then

                If dgvEmpTimesheet.IsCurrentCellDirty Then
                    dgvEmpTimesheet.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim objBudgetCodes As New clsBudgetcodes_master

                If CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value) > 0 Then

                    Dim txtHours As New MaskedTextBox
                    txtHours.Mask = dgcolhHours.Mask
                    txtHours.TextMaskFormat = MaskFormat.IncludePromptAndLiterals
                    txtHours.Text = dgvEmpTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()

                    If TextBoxTimeValidation(txtHours) = False Then
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Nilay (15 Feb 2017) -- Start
                    'If txtHours.MaskCompleted = False OrElse txtHours.MaskFull = False OrElse txtHours.Text = "00:00" Then
                    If txtHours.MaskCompleted = False OrElse txtHours.MaskFull = False Then
                        'Nilay (15 Feb 2017) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Hours is compulsory information.Please enter activity hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        txtHours.Focus()
                        e.Cancel = True
                        Exit Sub
                    End If

                    dgvEmpTimesheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = txtHours.Text

                    Dim objBudget As New clsBudget_MasterNew
                    Dim mintDefaultBudgetID As Integer = 0
                    Dim dsDefaultBudgetID As DataSet = objBudget.GetComboList("List", False, True, "", 0) 'Employee Wise Default BudgetID
                    If dsDefaultBudgetID IsNot Nothing AndAlso dsDefaultBudgetID.Tables(0).Rows.Count > 0 Then
                        mintDefaultBudgetID = CInt(dsDefaultBudgetID.Tables(0).Rows(0)("budgetunkid"))
                    End If

                    Dim mstrAssignedHrs As String = ""
                    Dim dsList As DataSet = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), mintDefaultBudgetID _
                                                                                       , CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityID.Index).Value) _
                                                                                       , CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))

                    Dim mstrWorkingHrs As String = txtHours.Text
                    Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
                    Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
                    mintActivityWorkingHrs = (mintHours * 60) + mintMins

                    'Nilay (07 Feb 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
                    'Dim objEmpshift As New clsEmployee_Shift_Tran
                    'Dim mintShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivityDate.Index).Value).Date, CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                    'objEmpshift = Nothing
                    'Dim objShiftTran As New clsshift_tran
                    'objShiftTran.GetShiftTran(mintShiftID)
                    'Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivityDate.Index).Value).Date, FirstDayOfWeek.Sunday) - 1)
                    'If drRow.Length > 0 AndAlso (dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0) Then
                    'Dim mintAssignShiftHrs As Integer = CInt(CInt(CInt(CInt(drRow(0)("workinghrsinsec")) * CDec(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityPercentage.Index).Value)) / 100) / 60)
                    'If ConfigParameter._Object._AllowOverTimeToEmpTimesheet = False Then
                    '    If mintAssignShiftHrs < mintActivityWorkingHrs Then
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This employee does not allow to do this activity more than define activity percentage."), enMsgBoxStyle.Information)
                    '        dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index)
                    '        e.Cancel = True
                    '        Exit Sub
                    '    End If
                    'End If
                    Dim strTotalActivityHours As String = CStr(dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value)
                    Dim intEmployeeID As Integer = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value)
                    Dim strdtActivityDate As String = eZeeDate.convertDate(CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivityDate.Index).Value).Date)
                    Dim intOldActivityWorkingHrsInMin As Integer = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityHrsInMins.Index).Value)
                    Dim intTotalActivityHoursInMin As Integer = CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhTotalActivityHoursInMin.Index).Value)



                    'objTimesheetApproval = New clstsemptimesheet_approval
                    'Dim intSumActivityHoursInMin As Integer = objTimesheetApproval.ComputeActivityHours(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmpTimesheetID.Index).Value), _
                    '                                                                                    CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), _
                    '                                                                                    CDate(dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivityDate.Index).Value).Date, _
                    '                                                                                    CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhPeriodID.Index).Value))
                    'Dim intComputeActivityHoursInMin = CInt(mdtSelectedData.Compute("SUM(activity_hrsinMins)", "IsGrp=0 AND employeeunkid=" & intEmployeeID & " AND ADate=" & strdtActivityDate & ""))

                    Dim intComputeActivityHoursInMin As Integer = intTotalActivityHoursInMin - intOldActivityWorkingHrsInMin + mintActivityWorkingHrs

                    If CBool(ConfigParameter._Object._AllowToExceedTimeAssignedToActivity) = True Then
                        If CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) = False Then
                            If CInt(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhShiftHoursInSec.Index).Value) / 60) < mintActivityWorkingHrs Then 'workinghrsinsec=shifthours
                                dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This employee does not allow to do this activity hours more than define shift hours."), enMsgBoxStyle.Information)
                                dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index)
                                'dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index).Value = CalculateTime(True, intOldActivityWorkingHrs * 60).ToString("#00.00").Replace(".", ":")
                                e.Cancel = True
                                Exit Sub
                            ElseIf intComputeActivityHoursInMin > CInt(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhShiftHoursInSec.Index).Value) / 60) Then
                                    dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Total activity hours cannot be greater than shift hours."), enMsgBoxStyle.Information)
                                    dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = strTotalActivityHours
                                    dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index)
                                'dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index).Value = CalculateTime(True, intOldActivityWorkingHrs * 60).ToString("#00.00").Replace(".", ":")
                                e.Cancel = True
                                    Exit Sub
                                End If
                        ElseIf CBool(ConfigParameter._Object._AllowOverTimeToEmpTimesheet) = True Then

                            If intComputeActivityHoursInMin >= (24 * 60) Then
                                dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#0.00").Replace(".", ":")
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Invalid Total Activity hours. Reason: Total Activity hours cannot greater than or equal to 24. "), enMsgBoxStyle.Information)
                                dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index)
                                'dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index).Value = CalculateTime(True, intOldActivityWorkingHrs * 60).ToString("#00.00").Replace(".", ":")
                                e.Cancel = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            Dim intAssignedShiftHrs As Integer = CInt(CInt(CInt(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhShiftHoursInSec.Index).Value) * CDec(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityPercentage.Index).Value)) / 100) / 60)
                            If intAssignedShiftHrs < mintActivityWorkingHrs Then
                                dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This employee does not allow to do this activity more than define activity percentage."), enMsgBoxStyle.Information)
                                dgvEmpTimesheet.CurrentCell = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index)
                                e.Cancel = True
                                'dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhHours.Index).Value = CalculateTime(True, intOldActivityWorkingHrs * 60).ToString("#00.00").Replace(".", ":")
                                Exit Sub
                            End If
                        End If
                    End If

                    dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityHrsInMins.Index).Value = mintActivityWorkingHrs
                    dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")

                    Dim lstRow As List(Of DataRow) = (From p In mdtSelectedData Where (CBool(p.Item("IsGrp")) = False AndAlso CInt(p.Item("employeeunkid")) = intEmployeeID _
                                                                                       AndAlso CDate(p.Item("activitydate")).Date = CDate(dgvEmpTimesheet.CurrentRow.Cells(dgcolhActivityDate.Index).Value).Date) Select p).ToList

                    For Each dsRow As DataRow In lstRow
                        dsRow.Item(dgvEmpTimesheet.Columns(dgcolhTotalActivityHours.Index).DataPropertyName) = dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhTotalActivityHours.Index).Value
                        dsRow.Item(dgvEmpTimesheet.Columns(objdgcolhTotalActivityHoursInMin.Index).DataPropertyName) = intComputeActivityHoursInMin
                    Next
                    mdtSelectedData.AcceptChanges()
                    CType(dgvEmpTimesheet.DataSource, DataTable).AcceptChanges()
                    'Nilay (07 Feb 2017) -- End
                End If
                objBudgetCodes = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEmpTimesheet.CurrentCellDirtyStateChanged
        Try
            If dgvEmpTimesheet.IsCurrentCellDirty Then
                dgvEmpTimesheet.CommitEdit(DataGridViewDataErrorContexts.Commit)
                dgvEmpTimesheet.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    Private Sub dgvEmpTimesheet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpTimesheet.CellContentClick, dgvEmpTimesheet.CellContentDoubleClick
        Try
            If e.ColumnIndex = objdgcolhShowDetails.Index Then

                Dim mintShiftID As Integer = 0
                Dim mintShiftWorkingHrs As Integer = 0
                Dim objEmpshift As New clsEmployee_Shift_Tran
                mintShiftID = objEmpshift.GetEmployee_Current_ShiftId(mdtPeriodStart, CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value))
                objEmpshift = Nothing

                Dim objShiftTran As New clsshift_tran
                objShiftTran.GetShiftTran(mintShiftID)
                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtPeriodStart.Date, Microsoft.VisualBasic.FirstDayOfWeek.Sunday) - 1)
                If drRow.Length > 0 Then
                    mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
                End If
                objShiftTran = Nothing

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

                Dim drPeriodRow As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                Dim mdtPStartDate As Date = Nothing
                Dim mdtPEndDate As Date = Nothing
                If drRow IsNot Nothing Then
                    mdtPStartDate = eZeeDate.convertDate(drPeriodRow("start_date").ToString())
                    mdtPEndDate = eZeeDate.convertDate(drPeriodRow("end_date").ToString())
                End If

                Dim objFrm As New frmProjectHoursDetails
                'objFrm.displayDialog(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeName.Index).Value.ToString(), cboPeriod.Text _
                '                                    , mdtPeriodStart, mdtPeriodEnd, dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhProject.Index).Value.ToString() _
                '                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhDonor.Index).Value.ToString(), CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityID.Index).Value) _
                '                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivity.Index).Value.ToString() _
                '                                    , Convert.ToDouble(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityPercentage.Index).Value), CType(dgvEmpTimesheet.DataSource, DataTable), True)

                objFrm.displayDialog(CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeID.Index).Value), dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhEmployeeName.Index).Value.ToString(), cboPeriod.Text _
                                                    , mdtPeriodStart, mdtPeriodEnd, dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhProject.Index).Value.ToString() _
                                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhDonor.Index).Value.ToString(), CInt(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityID.Index).Value) _
                                                    , dgvEmpTimesheet.Rows(e.RowIndex).Cells(dgcolhActivity.Index).Value.ToString() _
                                                , Convert.ToDouble(dgvEmpTimesheet.Rows(e.RowIndex).Cells(objdgcolhActivityPercentage.Index).Value), CType(dgvEmpTimesheet.DataSource, DataTable), True _
                                                , mdtPStartDate, mdtPEndDate)

                'Pinkal (16-May-2018) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- End


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private Sub dgvEmpTimesheet_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvEmpTimesheet.DataBindingComplete
        Try
            SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpTimesheet_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmpTimesheet_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEmpTimesheet.DataError

    End Sub

    'Pinkal (28-Jul-2018) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnReject.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.LblRemarks.Text = Language._Object.getCaption(Me.LblRemarks.Name, Me.LblRemarks.Text)
            Me.dgcolhParticular.HeaderText = Language._Object.getCaption(Me.dgcolhParticular.Name, Me.dgcolhParticular.HeaderText)
            Me.dgcolhActivityDate.HeaderText = Language._Object.getCaption(Me.dgcolhActivityDate.Name, Me.dgcolhActivityDate.HeaderText)
            Me.dgcolhActivity.HeaderText = Language._Object.getCaption(Me.dgcolhActivity.Name, Me.dgcolhActivity.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhDonor.HeaderText = Language._Object.getCaption(Me.dgcolhDonor.Name, Me.dgcolhDonor.HeaderText)
            Me.dgcolhProject.HeaderText = Language._Object.getCaption(Me.dgcolhProject.Name, Me.dgcolhProject.HeaderText)
			Me.dgcolhEmpDescription.HeaderText = Language._Object.getCaption(Me.dgcolhEmpDescription.Name, Me.dgcolhEmpDescription.HeaderText)
			Me.dgcolhSubmissionRemark.HeaderText = Language._Object.getCaption(Me.dgcolhSubmissionRemark.Name, Me.dgcolhSubmissionRemark.HeaderText)
            Me.dgcolhTotalActivityHours.HeaderText = Language._Object.getCaption(Me.dgcolhTotalActivityHours.Name, Me.dgcolhTotalActivityHours.HeaderText)
            Me.dgcolhHours.HeaderText = Language._Object.getCaption(Me.dgcolhHours.Name, Me.dgcolhHours.HeaderText)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Invalid working hrs.Please enter valid working hrs.")
            Language.setMessage(mstrModuleName, 2, "Hours is compulsory information.Please enter activity hours.")
            Language.setMessage(mstrModuleName, 3, "This employee does not allow to do this activity more than define activity percentage.")
            Language.setMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is required information.")
            Language.setMessage(mstrModuleName, 5, "Employee timesheet approved successfully.")
            Language.setMessage(mstrModuleName, 6, "Employee timesheet rejected successfully.")
            Language.setMessage(mstrModuleName, 7, "This employee does not allow to do this activity hours more than define shift hours.")
            Language.setMessage(mstrModuleName, 8, "Total activity hours cannot be greater than shift hours.")
            Language.setMessage(mstrModuleName, 9, "Invalid Total Activity hours. Reason: Total Activity hours cannot greater than or equal to 24.")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot perform Reject operation. Reason : your timesheet is incomplete, please complete it.")
            Language.setMessage(mstrModuleName, 13, "Sorry, you cannot perform Approve operation. Reason : your timesheet is incomplete, please complete it.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class