﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApproverMapping

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmApproverMapping"
    Private mblnCancel As Boolean = True
    Private objApprover As clsbudget_approver_mapping
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBudgetapproverunkid As Integer = -1
    Private mintSelectedLevelId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBudgetapproverunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBudgetapproverunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmApproverMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objApprover = New clsbudget_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()

            If menAction = enAction.EDIT_ONE Then
                objApprover._Budgetapproverunkid = mintBudgetapproverunkid
            End If

            GetValue()
            cboLevel.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproverMapping_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmApproverMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            ElseIf e.KeyCode = Keys.Return Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproverMapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproverMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbudget_approver_mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsbudget_approver_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If iSValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objApprover.Update()
            Else
                blnFlag = objApprover.Insert()
            End If

            If blnFlag = False And objApprover._Message <> "" Then
                eZeeMsgBox.Show(objApprover._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objApprover = Nothing
                    objApprover = New clsbudget_approver_mapping
                    Call GetValue() : cboLevel.SelectedValue = mintSelectedLevelId
                Else
                    mintBudgetapproverunkid = objApprover._Budgetapproverunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click, objbtnSearchLevel.Click

        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHLEVEL"
                    Call Common_Search(cboLevel)
                Case "OBJBTNSEARCHUSER"
                    Call Common_Search(cboApproverUser)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsbudgetapproverlevel_master
        Dim dsList As New DataSet
        Dim objOption As New clsPassowdOptions
        Try
            If menAction = enAction.EDIT_ONE Then
                dsList = objLevel.getListForCombo("List", True)
            Else
                dsList = objLevel.getListForCombo("List", True, True)
            End If
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


            'Dim intPrivilegeId As Integer = 1034 'Allow To Map Level To Approver
            Dim intPrivilegeId As Integer = 1041 'Allow To Approve Budget
            dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId.ToString(), FinancialYear._Object._YearUnkid, False)

            With cboApproverUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboLevel.SelectedValue = objApprover._Levelunkid
            cboApproverUser.SelectedValue = objApprover._Userapproverunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objApprover._Levelunkid = CInt(cboLevel.SelectedValue)
            objApprover._Userapproverunkid = CInt(cboApproverUser.SelectedValue)
            objApprover._Isvoid = False
            objApprover._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function iSValid() As Boolean
        Try
            If CInt(cboLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Approver Level is mandatory information. Please select Approver Level."), enMsgBoxStyle.Information)
                cboLevel.Focus()
                Return False
            ElseIf CInt(cboApproverUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Approver User is mandatory information. Please select Approver User."), enMsgBoxStyle.Information)
                cboApproverUser.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "iSValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Common_Search(ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cbo.DisplayMember
                .ValueMember = cbo.ValueMember
                .CodeMember = ""
                .DataSource = CType(cbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog = True Then
                cbo.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Common_Search", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Try
            If CInt(cboLevel.SelectedValue) > 0 Then mintSelectedLevelId = CInt(cboLevel.SelectedValue)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbApproverMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbApproverMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbApproverMapping.Text = Language._Object.getCaption(Me.gbApproverMapping.Name, Me.gbApproverMapping.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblApprLevel.Text = Language._Object.getCaption(Me.lblApprLevel.Name, Me.lblApprLevel.Text)
			Me.lblApprUser.Text = Language._Object.getCaption(Me.lblApprUser.Name, Me.lblApprUser.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Approver Level is mandatory information. Please select Approver Level.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Approver User is mandatory information. Please select Approver User.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class