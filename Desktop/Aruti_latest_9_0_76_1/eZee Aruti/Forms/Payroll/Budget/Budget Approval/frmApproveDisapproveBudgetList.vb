﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmApproveDisapproveBudgetList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveBudgetList"
    Private objBudgetApproval As clsBudget_approval_tran
    Private mdicAllocaions As New Dictionary(Of Integer, String)
    Private mdicApprovalStatus As New Dictionary(Of Integer, String)
    Private mdtTable As DataTable

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboBudget.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objBudget As New clsBudget_MasterNew
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try

            dsCombo = objBudget.GetComboList("Budget")
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Budget")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getApprovalStatus("Status", True, True, True, True, True, True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Status")
                .SelectedValue = enApprovalStatus.PENDING
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBudget = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objApproverMap As New clsbudget_approver_mapping
        Dim objApproverLevel As New clsbudgetapproverlevel_master
        Dim dsList As DataSet
        Dim dsCombo As DataSet
        Dim intCurrPayUnkID As Integer = 0
        Dim intPrevPayUnkID As Integer = 0
        Dim intCurrPriority As Integer = 0
        Dim intPrevPriority As Integer = 0
        Dim intCurrUserID As Integer = 0
        Dim intPrevUserID As Integer = 0
        Dim lvItem As New ListViewItem
        Dim strBudgetUnkId As String = ""
        Dim strFilter As String = ""
        Try


            If User._Object.Privilege._AllowToViewBudgetApprovals = False Then Exit Try

            dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid)
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority)
                mintMinPriority = objApproverLevel.GetMinPriority()
                mintMaxPriority = objApproverLevel.GetMaxPriority()
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority)

                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You do not have Approval Permission."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If CInt(cboBudget.SelectedValue) > 0 Then
                strBudgetUnkId = cboBudget.SelectedValue.ToString
            End If
            'If mintLowerLevelPriority >= 0 Then
            dsList = objBudgetApproval.GetList("List", User._Object._Userunkid, , strBudgetUnkId, , , True, CInt(cboStatus.SelectedValue), " bgbudget_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            'Else
            '    dsList = objBudgetApproval.GetList("List", User._Object._Userunkid, , strBudgetUnkId, , , True, CInt(cboStatus.SelectedValue), " bgbudget_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            'End If

            mdtTable = dsList.Tables("List")

            dgvBudgetApproval.AutoGenerateColumns = False

            colhBudgetCode.DataPropertyName = "budget_code"
            colhBudgetName.DataPropertyName = "budget_name"
            objcolhPayyearUnkid.DataPropertyName = "payyearunkid"
            colhPayyear.DataPropertyName = "payyear"
            objcolhViewById.DataPropertyName = "viewbyid"
            colhViewBy.DataPropertyName = "ViewBy"
            objcolhAllocationById.DataPropertyName = "allocationbyid"
            colhAllocationBy.DataPropertyName = "AllocationBy"
            objcolhPresentationmodeId.DataPropertyName = "presentationmodeid"
            colhPresentationmode.DataPropertyName = "PresentationMode"
            objcolhWhotoIncludeId.DataPropertyName = "whotoincludeid"
            colhWhotoInclude.DataPropertyName = "WhoToInclude"
            objcolhSalaryLevelId.DataPropertyName = "salarylevelid"
            colhSalaryLevel.DataPropertyName = "SalaryLevel"
            colhIsDefault.DataPropertyName = "isdefaultYesNo"
            objcolhIsDefault.DataPropertyName = "isdefault"
            colhLevel.DataPropertyName = "levelname"
            objcolhlevelunkid.DataPropertyName = "levelunkid"
            colhApprover.DataPropertyName = "approverusername"
            objcolhApproverunkid.DataPropertyName = "approveruserunkid"
            colhPriority.DataPropertyName = "priority"
            colhApprovalDate.DataPropertyName = "approval_date"
            colhApprovalDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern & " " & System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern
            colhApprovalDate.DefaultCellStyle.DataSourceNullValue = ""
            colhApprovalDate.DefaultCellStyle.NullValue = ""
            objcolhStatus.DataPropertyName = "statusunkid"
            colhStatus.DataPropertyName = "status"
            colhRemark.DataPropertyName = "remarks"
            objcolhBudgetUnkid.DataPropertyName = "budgetunkid"
            objcolhBudgetapprovaltranunkid.DataPropertyName = "budgetapprovaltranunkid"

            dgvBudgetApproval.DataSource = mdtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(dgvBudgetApproval.RowCount.ToString)
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnChangeStatus.Enabled = User._Object.Privilege._AllowToApproveBudget

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function IsValid() As Boolean
        Dim objApproverMap As New clsbudget_approver_mapping
        Dim objApproverLevel As New clsbudgetapproverlevel_master
        Dim objApproval As New clsBudget_approval_tran
        Dim objBudget As New clsBudget_MasterNew 'Sohail (01 Oct 2016)
        Dim mintLowerLevelPriority As Integer = -1
        Dim dsCombo As DataSet
        Try
            If dgvBudgetApproval.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Budget from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvBudgetApproval.Focus()
                Exit Function
            ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.REJECTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Budget. Reason: This Budget is already Rejected."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.CANCELLED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Budget. Reason: This Budget is already Cancelled."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.APPROVED AndAlso User._Object.Privilege._AllowToVoidApprovedBudget = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't Cancel this Budget. Reason: This Budget is already Approved and you don't have permission to Cancel Budget approval."), enMsgBoxStyle.Information)
                Exit Function
                'ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.APPROVED Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You can't Edit this Budget. Reason: This Budget is already Approved."), enMsgBoxStyle.Information)
                '    Exit Function
            ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.PENDING AndAlso CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.APPROVED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Budget. Reason: This Budget is already Approved."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf User._Object._Userunkid <> CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhApproverunkid.Index).Value) AndAlso CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.PENDING Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You can't Edit this Budget. Reason: You are logged in into another user login account."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value) = enApprovalStatus.PENDING AndAlso User._Object.Privilege._AllowToApproveBudget = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You can't Edit this Budget detail. Reason: You don't have permission to Approve Budget."), enMsgBoxStyle.Information)
                Exit Function
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            objBudget._Budgetunkid = CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value)
            If objBudget._Selected_Employees <> objBudget._Total_Employees Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "All allocations / employees must be included in this budget to set this budget as default budget."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (22 Nov 2016) -- End

            'Sohail (01 Oct 2016) -- Start
            'Enhancement - 63.1 - Don't allow to approve budget if total percentage is not 100.
            dsCombo = objBudget.GetTotalPercentage("Budget", CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), , " SUM(percentage) < 100")
            If dsCombo.Tables("Budget").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You can't Edit this Budget. Reason: Total Percentage should be 100 for all transactions for selected budget."), enMsgBoxStyle.Information)
                Exit Try
            End If
            'Sohail (01 Oct 2016) -- End

            dsCombo = objApproverMap.GetList("CurrPriority", User._Object._Userunkid)
            If dsCombo.Tables("CurrPriority").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("CurrPriority").Rows(0).Item("priority"))
            End If
            If mintCurrLevelPriority > CInt(dgvBudgetApproval.SelectedRows(0).Cells(colhPriority.Index).Value) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You can't Edit this Budget. Reason: You are logged in into another user login account."), enMsgBoxStyle.Information)
                Exit Function
            End If
            mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority)
            If mintLowerLevelPriority >= 0 Then
                dsCombo = objApproval.GetList("List", User._Object._Userunkid, , dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value.ToString, , mintLowerLevelPriority)

                If dsCombo.Tables("List").Rows.Count > 0 Then
                    If CInt(dsCombo.Tables("List").Rows(0).Item("statusunkid")) = 1 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "You can't Edit this Budget. Reason: This Budget approval is still pending for lower level."), enMsgBoxStyle.Information)
                        Exit Function
                    End If
                End If
            End If

            'Sohail (01 Mar 2017) -- Start
            'Enhancement - 65.1 - Export and Import option on Payroll Budget.
            If objBudget.isUsed(CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You can't Edit this Budget. Reason: This budget is in use on Budget Codes screen.."), enMsgBoxStyle.Information)
                Exit Function
            End If
            'Sohail (01 Mar 2017) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
            objApproverMap = Nothing
            objApproverLevel = Nothing
            objApproval = Nothing
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmApproveDisapproveBudgetList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudgetApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudgetList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveBudgetList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudgetList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveBudgetList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetApproval = New clsBudget_approval_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveBudgetList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_approval_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox's Events "

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            If IsValid() = False Then Exit Try

            Dim objFrm As New frmApproveDisapproveBudget
            If objFrm.DisplayDialog(enAction.ADD_ONE, CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetapprovaltranunkid.Index).Value), CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhBudgetUnkid.Index).Value), CInt(dgvBudgetApproval.SelectedRows(0).Cells(objcolhStatus.Index).Value)) = True Then
                Call FillList()
            End If
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboBudget.SelectedIndex = 0
            cboStatus.SelectedValue = enApprovalStatus.PENDING
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.dgvBudgetApproval.Text = Language._Object.getCaption(Me.dgvBudgetApproval.Name, Me.dgvBudgetApproval.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.colhBudgetCode.HeaderText = Language._Object.getCaption(Me.colhBudgetCode.Name, Me.colhBudgetCode.HeaderText)
            Me.colhBudgetName.HeaderText = Language._Object.getCaption(Me.colhBudgetName.Name, Me.colhBudgetName.HeaderText)
            Me.colhPayyear.HeaderText = Language._Object.getCaption(Me.colhPayyear.Name, Me.colhPayyear.HeaderText)
            Me.colhViewBy.HeaderText = Language._Object.getCaption(Me.colhViewBy.Name, Me.colhViewBy.HeaderText)
            Me.colhAllocationBy.HeaderText = Language._Object.getCaption(Me.colhAllocationBy.Name, Me.colhAllocationBy.HeaderText)
            Me.colhPresentationmode.HeaderText = Language._Object.getCaption(Me.colhPresentationmode.Name, Me.colhPresentationmode.HeaderText)
            Me.colhWhotoInclude.HeaderText = Language._Object.getCaption(Me.colhWhotoInclude.Name, Me.colhWhotoInclude.HeaderText)
            Me.colhSalaryLevel.HeaderText = Language._Object.getCaption(Me.colhSalaryLevel.Name, Me.colhSalaryLevel.HeaderText)
            Me.colhIsDefault.HeaderText = Language._Object.getCaption(Me.colhIsDefault.Name, Me.colhIsDefault.HeaderText)
            Me.colhLevel.HeaderText = Language._Object.getCaption(Me.colhLevel.Name, Me.colhLevel.HeaderText)
            Me.colhApprover.HeaderText = Language._Object.getCaption(Me.colhApprover.Name, Me.colhApprover.HeaderText)
            Me.colhPriority.HeaderText = Language._Object.getCaption(Me.colhPriority.Name, Me.colhPriority.HeaderText)
            Me.colhApprovalDate.HeaderText = Language._Object.getCaption(Me.colhApprovalDate.Name, Me.colhApprovalDate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhRemark.HeaderText = Language._Object.getCaption(Me.colhRemark.Name, Me.colhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Sorry, You do not have Approval Permission.")
            Language.setMessage(mstrModuleName, 4, "Please select Budget from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 5, "You can't Edit this Budget. Reason: This Budget is already Rejected.")
            Language.setMessage(mstrModuleName, 6, "You can't Edit this Budget. Reason: This Budget is already Cancelled.")
            Language.setMessage(mstrModuleName, 8, "You can't Cancel this Budget. Reason: This Budget is already Approved and you don't have permission to Cancel Budget approval.")
            Language.setMessage(mstrModuleName, 9, "You can't Edit this Budget. Reason: This Budget is already Approved.")
            Language.setMessage(mstrModuleName, 10, "You can't Edit this Budget. Reason: You are logged in into another user login account.")
            Language.setMessage(mstrModuleName, 11, "You can't Edit this Budget detail. Reason: You don't have permission to Approve Budget.")
            Language.setMessage(mstrModuleName, 12, "You can't Edit this Budget. Reason: You are logged in into another user login account.")
            Language.setMessage(mstrModuleName, 13, "You can't Edit this Budget. Reason: This Budget approval is still pending for lower level.")
            Language.setMessage(mstrModuleName, 14, "Sorry, You can't Edit this Budget. Reason: Total Percentage should be 100 for all transactions for selected budget.")
            Language.setMessage(mstrModuleName, 15, "You can't Edit this Budget. Reason: This budget is in use on Budget Codes screen..")
            Language.setMessage(mstrModuleName, 16, "All allocations / employees must be included in this budget to set this budget as default budget.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class