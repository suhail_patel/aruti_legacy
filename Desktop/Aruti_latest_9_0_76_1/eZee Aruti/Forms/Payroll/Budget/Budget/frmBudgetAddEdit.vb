﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

Public Class frmBudgetAddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBudgetAddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private mstrEmployeeIDs As String = ""
    Private mintBudgetUnkId As Integer = -1
    Private mdtTable As DataTable
    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private mdtDataView As DataView
    'Nilay (22 Nov 2016) -- End
    Private dsAllHeads As DataSet = Nothing
    Private dtViewHead As DataView
    Private blnIsDelKey As Boolean = False
    Private objDic As New Dictionary(Of Integer, Decimal)
    Private mdicFund As New Dictionary(Of Integer, Decimal)
    Private mdicFundCode As New Dictionary(Of Integer, String)

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    Private mstrAnalysis_CodeField As String = "" 'Sohail (01 Mar 2017)

    Private menAllocation As enAnalysisRef
    Private mstrAllocation As String = ""

    Private objBudget_Master As clsBudget_MasterNew
    Private objBudget_Tran As clsBudget_TranNew
    Private objBudgetPeriod As New clsBudgetperiod_Tran
    Private objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
    Private mstrPeriodIdList As String = ""
    Private mdicHeadMapping As New Dictionary(Of Integer, Integer)
    Private mstrAllocationTranUnkIDs As String = ""
    Private mstrPreviousPeriodDBName As String = ""
    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private mstrSearch As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrFilteredEmployeeIDs As String = String.Empty
    'Nilay (22 Nov 2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intBudgetunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBudgetUnkId = intBudgetunkid
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtBudgetCode.BackColor = GUI.ColorComp
            txtBudgetName.BackColor = GUI.ColorComp
            cboBudgetYear.BackColor = GUI.ColorComp
            cboViewBy.BackColor = GUI.ColorComp
            cboPresentation.BackColor = GUI.ColorComp
            cboWhoToInclude.BackColor = GUI.ColorComp
            cboSalaryLevel.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtBudgetCode.Text = objBudget_Master._Budget_Code
            txtBudgetName.Text = objBudget_Master._Budget_Name
            If menAction <> enAction.EDIT_ONE Then
                cboBudgetYear.SelectedValue = FinancialYear._Object._YearUnkid
                cboYear.SelectedValue = FinancialYear._Object._YearUnkid
                dtpBudgetDate.Value = FinancialYear._Object._Database_End_Date
            Else
                cboBudgetYear.SelectedValue = objBudget_Master._Payyearunkid
                cboYear.SelectedValue = objBudget_Master._PreviousPeriodyearunkid
                dtpBudgetDate.Value = objBudget_Master._Budget_date
            End If
            cboViewBy.SelectedValue = objBudget_Master._Viewbyid
            mintViewIdx = objBudget_Master._Allocationbyid
            cboPresentation.SelectedValue = objBudget_Master._Presentationmodeid
            cboWhoToInclude.SelectedValue = objBudget_Master._Whotoincludeid
            cboSalaryLevel.SelectedValue = objBudget_Master._Salarylevelid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBudget_Master._Budget_Code = txtBudgetCode.Text.Trim
            objBudget_Master._Budget_Name = txtBudgetName.Text.Trim
            objBudget_Master._Payyearunkid = CInt(cboBudgetYear.SelectedValue)
            objBudget_Master._PayyearName = cboBudgetYear.Text
            objBudget_Master._Viewbyid = CInt(cboViewBy.SelectedValue)
            objBudget_Master._Allocationbyid = mintViewIdx
            objBudget_Master._Presentationmodeid = CInt(cboPresentation.SelectedValue)
            objBudget_Master._Whotoincludeid = CInt(cboWhoToInclude.SelectedValue)
            objBudget_Master._Salarylevelid = CInt(cboSalaryLevel.SelectedValue)
            objBudget_Master._Budget_date = dtpBudgetDate.Value.Date
            objBudget_Master._PreviousPeriodyearunkid = CInt(cboYear.SelectedValue)
            objBudget_Master._Userunkid = User._Object._Userunkid
            objBudget_Master._Isvoid = False
            objBudget_Master._Voiduserunkid = -1
            objBudget_Master._Voiddatetime = Nothing
            objBudget_Master._Voidreason = ""
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            Dim arr() As String = objlblTotalEmplyees.Text.Split(CChar("/"))
            If arr.Count = 2 Then
                objBudget_Master._Selected_Employees = CInt(arr(0))
                objBudget_Master._Total_Employees = CInt(arr(1))
            Else
                objBudget_Master._Selected_Employees = 0
                objBudget_Master._Total_Employees = 0
            End If
            'Sohail (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try

            dsCombo = objMaster.Get_Database_Year_List("Year", True, Company._Object._Companyunkid)
            With cboYear
                .ValueMember = "yearunkid"
                .DisplayMember = "financialyear_name"
                .DataSource = dsCombo.Tables("Year")
                .SelectedValue = FinancialYear._Object._YearUnkid
            End With

            Dim dtYear As DataTable = dsCombo.Tables("Year").Copy
            Dim dr As DataRow = dtYear.NewRow
            dr.Item("yearunkid") = -99
            dr.Item("financialyear_name") = CInt(FinancialYear._Object._FinancialYear_Name.Split(CChar("-"))(0)) + 1 & "-" & CInt(FinancialYear._Object._FinancialYear_Name.Split(CChar("-"))(1)) + 1
            dtYear.Rows.Add(dr)

            With cboBudgetYear
                .ValueMember = "yearunkid"
                .DisplayMember = "financialyear_name"
                .DataSource = dtYear
                .SelectedValue = FinancialYear._Object._YearUnkid
            End With

            dsCombo = objMaster.getComboListForBudgetViewBy("ViewBy", True)
            With cboViewBy
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("ViewBy")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForBudgetPresentation("Presentation", True)
            With cboPresentation
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Presentation")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForBudgetSalaryLevel("SalaryLevel", True)
            With cboSalaryLevel
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("SalaryLevel")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                .SelectedValue = 0
            End With

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            Dim objFundProjectCode As New clsFundProjectCode
            dsCombo = objFundProjectCode.GetComboList("FundProjectCode", True)
            With cboProjectCode
                .ValueMember = "fundprojectcodeunkid"
                .DisplayMember = "fundprojectcode"
                .DataSource = dsCombo.Tables("FundProjectCode")
                .SelectedValue = 0
            End With
            objFundProjectCode = Nothing

            dsCombo = objMaster.GetCondition(False, True, True, False, False)
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = enComparison_Operator.GREATER_THAN
            End With
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillTranHeadList()
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objHead As New clsBudgetformula_master
        Dim dsList As DataSet
        Dim dsCombo As DataSet
        'Dim dtTable As DataTable
        'Dim lvItem As ListViewItem
        Try

            dsList = objBudgetHeadMapping.GetListByBudgetUnkId("HeadMapping", mintBudgetUnkId, ConfigParameter._Object._CurrentDateAndTime, , True)
            'dtTable = dsList.Tables("TranHead")
            'Dim dtCol As New DataColumn("", Type.GetType("System.Int32"))
            'dtCol.DefaultValue = 0
            'dtCol.AllowDBNull = False
            'dtTable.Columns.Add(dtCol)

            dgvTranHead.AutoGenerateColumns = False

            objcolhIsCheck.DataPropertyName = "IsChecked"
            objcolhTranheadUnkId.DataPropertyName = "tranheadunkid"
            colhTranHeadCode.DataPropertyName = "HeadCode"
            colhTranHeadName.DataPropertyName = "HeadName"
            objcolhFormulaMappingHead.DataPropertyName = "mappedformulaheadunkid"
            colhIsSalary.DataPropertyName = "IsSalary"
            objcolhFormulaid.DataPropertyName = "formulaid"
            objcolhFormula.DataPropertyName = "formula"
            colhHeadType.DataPropertyName = "trnheadtype_name"

            dsCombo = objHead.getComboList("TranHead", True)
            Dim dr() As DataRow = dsCombo.Tables(0).Select("Id = 0")
            If dr.Length > 0 Then
                dr(0).Item("name") = ""
                dsCombo.Tables(0).AcceptChanges()
            End If
            With colhFormulaMappingHead
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .DataPropertyName = "mappedformulaheadunkid"
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            End With

            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables("HeadMapping") Where (mdicHeadMapping.ContainsKey(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

            For Each dsRow As DataRow In lstRow
                dsRow.Item("IsChecked") = True
            Next
            dsList.Tables("HeadMapping").AcceptChanges()

            dtViewHead = New DataView(dsList.Tables("HeadMapping"))
            dtViewHead.Sort = "IsChecked DESC, HeadName"
            dgvTranHead.DataSource = dtViewHead


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTranHeadList", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriod.Items
                RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllPeriod", mstrModuleName)
        End Try
    End Sub

    Private Sub GridSetup()
        Dim objBudget As New clsBudget_MasterNew
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objFormulaHeads As New clsBudgetformula_head_tran
        Dim dsList As DataSet
        Dim dtTemp As New DataTable
        Dim dtcol As DataColumn
        Dim dCol As DataGridViewTextBoxColumn
        Dim dColChk As DataGridViewCheckBoxColumn
        Try

            If IsValidForGrid() = False Then Exit Try

            Cursor.Current = Cursors.WaitCursor
            With dgvBudget
                .AutoGenerateColumns = False
                .IgnoreFirstColumn = True

                If Not .DataSource Is Nothing Then .DataSource = Nothing
                .Columns.Clear()
                .Rows.Clear()

                dColChk = New DataGridViewCheckBoxColumn
                dColChk.Name = "colhIsChecked"
                dColChk.HeaderText = ""
                dColChk.SortMode = DataGridViewColumnSortMode.NotSortable
                dColChk.Width = 25
                dColChk.Frozen = True
                dColChk.Visible = True
                dColChk.ReadOnly = False
                dColChk.DataPropertyName = "IsChecked"
                .Columns.Add(dColChk)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhIsChecked")
                dtcol.DataType = System.Type.GetType("System.Boolean")
                dtTemp.Columns.Add(dtcol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhSrNo"
                dCol.HeaderText = "Sr. No."
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 30
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DataPropertyName = "ROWNO"
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhSrNo")
                dtcol.DataType = System.Type.GetType("System.Int32")
                dtTemp.Columns.Add(dtcol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhCollaps"
                dCol.HeaderText = ""
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 30
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                dCol.DataPropertyName = "Collapse"
                .Columns.Add(dCol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocationID"
                dCol.HeaderText = mstrReport_GroupName.Replace(" :", "")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "Id"
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhAllocationID")
                dtcol.DataType = System.Type.GetType("System.Int32")
                dtTemp.Columns.Add(dtcol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocation"
                dCol.HeaderText = mstrReport_GroupName.Replace(" :", "")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 180
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = "GName"
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhAllocation")
                dtcol.DataType = System.Type.GetType("System.String")
                dtTemp.Columns.Add(dtcol)

                If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Employee Then

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhEmpJobUnkID"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 22, "Job Title")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = True
                    dCol.Visible = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "empjobunkid"
                    .Columns.Add(dCol)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhEmpJob"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 22, "Job Title")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 120
                    dCol.Frozen = True
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "EmpJobTitle"
                    .Columns.Add(dCol)

                End If

                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhTranHeadID"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Transaction Head")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 0
                    dCol.Visible = False
                    dCol.Frozen = True
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "tranheadunkid"
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn("colhTranHeadID")
                    dtcol.DataType = System.Type.GetType("System.Int32")
                    dtTemp.Columns.Add(dtcol)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhTranHead"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Transaction Head")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 150
                    dCol.Visible = True
                    dCol.Frozen = True
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "trnheadname"
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn("colhTranHead")
                    dtcol.DataType = System.Type.GetType("System.String")
                    dtTemp.Columns.Add(dtcol)

                Else

                    For i As Integer = 1 To 2

                        'Adding DataGrid Columns
                        dCol = New DataGridViewTextBoxColumn
                        dCol.Name = "colhTranHeadID" & "_|" & i
                        If i = 1 Then
                            dCol.HeaderText = Language.getMessage(mstrModuleName, 19, "Basic Salary")
                        Else
                            dCol.HeaderText = Language.getMessage(mstrModuleName, 2, "Other Payroll Cost")
                        End If
                        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                        dCol.Width = 150
                        dCol.Frozen = False
                        dCol.ReadOnly = True
                        dCol.DataPropertyName = "pramount"
                        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                        dCol.DataPropertyName = "_|" & i
                        .Columns.Add(dCol)
                        'Adding DataTable Columns
                        dtcol = New DataColumn("colhTranHeadID" & "_|" & i)
                        dtcol.DataType = System.Type.GetType("System.Int32")
                        dtTemp.Columns.Add(dtcol)

                    Next

                End If

                'Adding DataGrid Columns
                'dCol = New DataGridViewTextBoxColumn
                'dCol.Name = "colhTranHeadType"
                'dCol.HeaderText = Language.getMessage(mstrModuleName, 8, "Transaction Head Type")
                'dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                'dCol.Width = 130
                'dCol.Frozen = True
                'dCol.ReadOnly = True
                'dCol.DataPropertyName = dCol.Name
                '.Columns.Add(dCol)
                ''Adding DataTable Columns
                'dtcol = New DataColumn("colhTranHeadType")
                'dtcol.DataType = System.Type.GetType("System.String")
                'dtTemp.Columns.Add(dtcol)

                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhPayrollAmount"
                    dCol.HeaderText = "Current Figure"
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 150
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "pramount"
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn(dCol.Name)
                    dtcol.DataType = System.Type.GetType("System.Decimal")
                    dtTemp.Columns.Add(dtcol)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhBudgetAmount"
                    dCol.HeaderText = "New Budget Figure"
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 150
                    dCol.Frozen = False
                    dCol.ReadOnly = False
                    dCol.DataPropertyName = "budgetamount"
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn(dCol.Name)
                    dtcol.DataType = System.Type.GetType("System.Decimal")
                    dtTemp.Columns.Add(dtcol)

                Else

                    For i As Integer = 1 To 2

                        'Adding DataGrid Columns
                        dCol = New DataGridViewTextBoxColumn
                        If i = 1 Then
                            dCol.Name = "colhBudgetSalaryAmount"
                            dCol.HeaderText = "New Budget Basic Salary Figure"
                            dCol.DataPropertyName = "budgetsalaryamount"
                        Else
                            dCol.Name = "colhBudgetOtherPayrollAmount"
                            dCol.HeaderText = "New Budget Other Payroll Cost Figure"
                            dCol.DataPropertyName = "budgetotherpayrollamount"
                        End If
                        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                        dCol.Width = 130
                        dCol.Frozen = False
                        dCol.ReadOnly = False
                        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                        .Columns.Add(dCol)
                        'Adding DataTable Columns
                        'dtcol = New DataColumn(dCol.Name)
                        'dtcol.DataType = System.Type.GetType("System.Decimal")
                        'dtTemp.Columns.Add(dtcol)
                    Next
                End If


                dsList = objFundProjectCode.GetList("FundProjectCode")
                mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                mdicFundCode = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                For Each dsRow As DataRow In dsList.Tables("FundProjectCode").Rows

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhFund" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = dsRow.Item("fundprojectcode").ToString & " (%)"
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = False
                    dCol.ReadOnly = False
                    dCol.DataPropertyName = "|_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn(dCol.Name)
                    dtcol.DataType = System.Type.GetType("System.Decimal")
                    dtTemp.Columns.Add(dtcol)

                Next

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "objisgroup"
                dCol.HeaderText = "isgroup"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 50
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.Visible = False
                dCol.DataPropertyName = dCol.Name
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("objisgroup")
                dtcol.DataType = System.Type.GetType("System.Boolean")
                dtTemp.Columns.Add(dtcol)

                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTotal"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 24, "Total")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "colhTotal"
                dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                .Columns.Add(dCol)
                'Sohail (22 Nov 2016) -- End

                mstrPeriodIdList = String.Join(",", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Order By p.SubItems(colh_Period_EndDate.Index).Tag.ToString Ascending Select (p.SubItems(colh_Period_PayPeriod.Index).Tag.ToString)).ToArray)

                'Get Heads List
                Dim strTranHeadIDList As String = String.Join(",", (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToArray)
                dsAllHeads = Nothing

                'Sohail (01 Mar 2017) -- Start
                'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
                'dsList = objBudget.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpBudgetDate.Value.Date, dtpBudgetDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, False, mintViewIdx, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetUnkId, mstrPreviousPeriodDBName, CInt(cboViewBy.SelectedValue), CInt(cboPresentation.SelectedValue), CInt(cboWhoToInclude.SelectedValue), "Budget", True, "", dsAllHeads)
                dsList = objBudget.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpBudgetDate.Value.Date, dtpBudgetDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, False, mintViewIdx, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, mintBudgetUnkId, mstrPreviousPeriodDBName, CInt(cboViewBy.SelectedValue), CInt(cboPresentation.SelectedValue), CInt(cboWhoToInclude.SelectedValue), "Budget", True, "", dsAllHeads, , mstrAnalysis_CodeField)
                'Sohail (01 Mar 2017) -- End

                mdtTable = dsList.Tables(0)

                Dim dicHeadMapping As Dictionary(Of Integer, Integer) = (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("mappedformulaheadunkid")) > 0) Select New With {.headid = CInt(p.Item("tranheadunkid")), .mappedheadid = CInt(p.Item("mappedformulaheadunkid"))}).ToDictionary(Function(x) x.headid, Function(y) y.mappedheadid)
                Dim dicFormulaMapping As Dictionary(Of Integer, String) = (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("mappedformulaheadunkid")) > 0) Select New With {.headid = CInt(p.Item("tranheadunkid")), .formulaid = p.Item("formulaid").ToString}).ToDictionary(Function(x) x.headid, Function(y) y.formulaid)

                'If menAction <> enAction.EDIT_ONE Then
                If menAction <> enAction.EDIT_ONE OrElse chkOverWrite.Checked = True Then
                    Dim lstRow As List(Of DataRow) = Nothing

                    'lstRow = (From p In dsAllHeads.Tables(0) Where (CInt(p.Item("jobunkid")) <= 0 AndAlso dicFormulaMapping.Keys.ToArray.Contains(CInt(p.Item("tranheadunkid")))) Select (p)).ToList
                    lstRow = (From p In dsAllHeads.Tables(0) Where (dicFormulaMapping.Keys.ToArray.Contains(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

                    For Each dsRow As DataRow In lstRow
                        objDic.Clear()

                        If dicFormulaMapping.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = True Then

                            If dicHeadMapping.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = True Then
                                Dim ds As DataSet = objFormulaHeads.GetList("Heads", CInt(dicHeadMapping.Item(CInt(dsRow.Item("tranheadunkid")))), ConfigParameter._Object._CurrentDateAndTime)

                                For Each dtRow As DataRow In ds.Tables("Heads").Rows
                                    If objDic.ContainsKey(CInt(dtRow.Item("tranheadunkid"))) = False Then
                                        If GetValue(CInt(dtRow.Item("tranheadunkid")), CInt(dsRow.Item("Id")), CInt(dsRow.Item("AllocationtranunkId")), dsAllHeads.Tables(0)) = True Then

                                        End If
                                    End If

                                Next

                                Dim strFormula As String = getExpression(dicFormulaMapping.Item(CInt(dsRow.Item("tranheadunkid"))), CInt(dsRow.Item("Id")), CInt(dsRow.Item("AllocationtranunkId")), dsAllHeads.Tables(0))

                                Dim c As New clsFomulaEvaluate

                                Dim decAmt As Decimal = c.Eval(strFormula)

                                dsRow.Item("budgetamount") = decAmt
                                dsRow.AcceptChanges()
                            End If

                        End If
                    Next
                    dsAllHeads.Tables(0).AcceptChanges()

                    lstRow = (From p In dsAllHeads.Tables(0) Where (dicFormulaMapping.Keys.ToArray.Contains(CInt(p.Item("tranheadunkid"))) = False) Select (p)).ToList
                    For Each dsRow As DataRow In lstRow
                        dsRow.Item("budgetamount") = 0
                        dsRow.AcceptChanges()
                    Next
                    dsAllHeads.Tables(0).AcceptChanges()
                End If

                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                    mdtTable = dsAllHeads.Tables(0)

                Else
                    'If menAction <> enAction.EDIT_ONE Then
                    If menAction <> enAction.EDIT_ONE OrElse chkOverWrite.Checked = True Then
                        For Each dtRow As DataRow In dsAllHeads.Tables(0).Rows 'Select("allocationtranunkid > 0 ")
                            Dim strId As String = dtRow.Item("Id").ToString
                            Dim strAllocationtranunkId As String = dtRow.Item("allocationtranunkid").ToString
                            dtRow.Item("budgetsalaryamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = True AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                            dtRow.Item("budgetotherpayrollamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = False AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                        Next
                        dsAllHeads.Tables(0).AcceptChanges()

                        For Each dtRow As DataRow In mdtTable.Rows 'Select("allocationtranunkid > 0 ")
                            Dim strId As String = dtRow.Item("Id").ToString
                            Dim strAllocationtranunkId As String = dtRow.Item("allocationtranunkid").ToString
                            dtRow.Item("budgetsalaryamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = True AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                            dtRow.Item("budgetotherpayrollamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = False AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                        Next
                        mdtTable.AcceptChanges()
                    End If
                End If

                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Prompt user before saving payroll budgeting if all %s are not set.
                Dim strExpression As String = ""
                For Each pair In mdicFund
                    strExpression &= " + [|_" & pair.Key.ToString & "]"
                Next
                If strExpression.Trim <> "" Then
                    mdtTable.Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
                End If
                'Sohail (22 Nov 2016) -- End

                Dim decTotal As Decimal = 0
                Dim decSalaryTotal As Decimal = 0
                Dim decOtherPayrollTotal As Decimal = 0
                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                    decTotal = (From p In mdtTable Select (CDec(p.Item("pramount")))).DefaultIfEmpty.Sum
                Else
                    decSalaryTotal = (From p In mdtTable Select (CDec(p.Item("_|1")))).DefaultIfEmpty.Sum
                    decOtherPayrollTotal = (From p In mdtTable Select (CDec(p.Item("_|2")))).DefaultIfEmpty.Sum
                End If

                Dim dr As DataRow = mdtTable.NewRow
                dr.Item("budgetunkid") = 0
                dr.Item("budget_code") = ""
                dr.Item("budget_name") = ""
                dr.Item("payyearunkid") = 0
                dr.Item("viewbyid") = 0
                dr.Item("allocationbyid") = 0
                dr.Item("presentationmodeid") = 0
                dr.Item("whotoincludeid") = 0
                dr.Item("salarylevelid") = 0
                dr.Item("allocationtranunkid") = 0
                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                    dr.Item("tranheadunkid") = 0
                    dr.Item("trnheadname") = "Grand Total"
                    dr.Item("amount") = 0
                    dr.Item("budgetamount") = 0
                    dr.Item("pramount") = CDec(Format(decTotal, GUI.fmtCurrency))
                    dr.Item("salaryamount") = 0
                    dr.Item("budgetsalaryamount") = 0
                    dr.Item("otherpayrollamount") = 0
                    dr.Item("budgetotherpayrollamount") = 0
                Else
                    dr.Item("_|1") = CDec(Format(decSalaryTotal, GUI.fmtCurrency))
                    dr.Item("_|2") = CDec(Format(decOtherPayrollTotal, GUI.fmtCurrency))
                    dr.Item("budgetsalaryamount") = 0
                    dr.Item("budgetotherpayrollamount") = 0
                End If
                dr.Item("Id") = -1
                dr.Item("GName") = ""
                dr.Item("IsChecked") = False

                mdtTable.Rows.Add(dr)

                Call UpdateBudgetTotal()

                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'dgvBudget.DataSource = mdtTable
                dgvBudget.DataSource = mdtDataView
                'Nilay (22 Nov 2016) -- End
            End With

            If mdtTable.Rows.Count > 1 Then
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Budget code and name disable only in Budget Edit mode only.
                'gbBudget.Enabled = False
                For Each ctrl As Control In gbBudget.Controls
                    If ctrl.Name = txtBudgetCode.Name OrElse ctrl.Name = txtBudgetName.Name OrElse ctrl.Name = "" Then
                        ctrl.Enabled = True
                    Else
                        ctrl.Enabled = False
                    End If
                Next
                'Sohail (22 Nov 2016) -- End
            End If

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            mdtDataView = mdtTable.DefaultView
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GridSetup", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub UpdateBudgetTotal()
        Dim objEmp As New clsEmployee_Master
        Try
            Dim decTotal As Decimal
            Dim decSalaryTotal As Decimal = 0
            Dim decOtherPayrollTotal As Decimal = 0

            If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                decTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetamount")))).DefaultIfEmpty.Sum
                mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetamount") = CDec(Format(decTotal, GUI.fmtCurrency))

                objlblBudgetTotal.Text = Format(decTotal, GUI.fmtCurrency)
                objlblOtherCostTotal.Text = Format(0, GUI.fmtCurrency)
                objlblGrandTotal.Text = Format(decTotal, GUI.fmtCurrency)
            Else
                decSalaryTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetsalaryamount")))).DefaultIfEmpty.Sum
                decOtherPayrollTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetotherpayrollamount")))).DefaultIfEmpty.Sum

                mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetsalaryamount") = CDec(Format(decSalaryTotal, GUI.fmtCurrency))
                mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetotherpayrollamount") = CDec(Format(decOtherPayrollTotal, GUI.fmtCurrency))

                objlblBudgetTotal.Text = Format(decSalaryTotal, GUI.fmtCurrency)
                objlblOtherCostTotal.Text = Format(decOtherPayrollTotal, GUI.fmtCurrency)
                objlblGrandTotal.Text = Format(decSalaryTotal + decOtherPayrollTotal, GUI.fmtCurrency)
            End If

            Dim intFundId As Integer
            Dim decFundTotal As Decimal = 0
            For Each pair In mdicFund
                intFundId = pair.Key
                decFundTotal = 0

                Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
                If lstRow.Count > 0 Then
                    For Each dRow As DataRow In lstRow

                        If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                            decFundTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                        Else
                            decFundTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                        End If
                    Next
                    mdtTable.Rows(mdtTable.Rows.Count - 1).Item("|_" & intFundId.ToString) = CDec(Format(decFundTotal, GUI.fmtCurrency))
                Else
                    mdtTable.Rows(mdtTable.Rows.Count - 1).Item("|_" & intFundId.ToString) = 0
                End If
            Next

            mdtTable.AcceptChanges()

            Dim strFilter As String = ""
            Dim decTotEmp As Decimal = objEmp.GetEmployeeCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpBudgetDate.Value.Date, dtpBudgetDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, False)
            Dim decNewEmp As Decimal = (From p In mdtTable Where (CInt(p.Item("allocationtranunkid")) < 0) Select New With {Key .name = p.Item("GName").ToString}).Distinct().Count()
            decTotEmp += decNewEmp
            Dim decSelectedEmp As Decimal = decTotEmp
            If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Allocation Then
                If mstrAnalysis_Join.Trim <> "" Then
                    strFilter = mstrAnalysis_Join.Substring(mstrAnalysis_Join.LastIndexOf("AND") + 3, mstrAnalysis_Join.Length - mstrAnalysis_Join.LastIndexOf("AND") - 3)
                    'S.SANDEEP [29 SEP 2016] -- START
                    'ENHANCEMENT : DEFINING ENUM FOR EMPLYOEE FEILD, LIST TAKING LONG TIME TO FILL
                    Dim StrCheck_Fields As String = String.Empty
                    StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Email & "," & clsEmployee_Master.EmpColEnum.Col_Status
                    If IsNumeric(ConfigParameter._Object._Checked_Fields.Split(CChar(","))(0)) = True Then
                        StrCheck_Fields = ConfigParameter._Object._Checked_Fields
                    End If
                    'S.SANDEEP [29 SEP 2016] -- END
                    Dim dsSelectedEmp As DataSet = objEmp.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpBudgetDate.Value.Date, dtpBudgetDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Emp", , , strFilter) 'S.SANDEEP [29 SEP 2016] -- START {StrCheck_Fields} -- END
                    decSelectedEmp = dsSelectedEmp.Tables("Emp").Rows.Count
                End If
            Else
                'Sohail (01 Nov 2017) -- Start
                'decSelectedEmp = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select New With {Key .name = p.Item("GName").ToString}).Distinct().Count()
                decSelectedEmp = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select New With {Key .name = p.Item("GName").ToString, Key .code = p.Item("GCode").ToString}).Distinct().Count()
                'Sohail (01 Nov 2017) -- End
            End If

            objlblTotalEmplyees.Text = CInt(decSelectedEmp).ToString & "/" & CInt(decTotEmp).ToString

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            mdtDataView = mdtTable.DefaultView
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateBudgetTotal", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Function GetValue(ByVal intFormulaHeadId As Integer, ByVal intAllocationID As Integer, ByVal intAllocationTranUnkId As Integer, ByRef dtTable As DataTable) As Boolean
        Dim objFormulaHeads As New clsBudgetformula_head_tran
        Try

            Dim ds As DataSet = objFormulaHeads.GetList("Heads", intFormulaHeadId, ConfigParameter._Object._CurrentDateAndTime)

            For Each dtRow As DataRow In ds.Tables("Heads").Rows
                If GetValue(CInt(dtRow.Item("tranheadunkid")), intAllocationID, intAllocationTranUnkId, mdtTable) = True Then
                    If objDic.ContainsKey(intFormulaHeadId) = False Then
                        objDic.Add(intFormulaHeadId, 0)
                    Else
                        objDic.Item(intFormulaHeadId) = 0
                    End If
                    Return True
                End If
            Next

            Dim objFormulaTran As New clsBudgetformula_Tran
            Dim dsFrml As DataSet = objFormulaTran.GetList("Formula", intFormulaHeadId, ConfigParameter._Object._CurrentDateAndTime)

            If dsFrml.Tables("Formula").Rows.Count > 0 Then
                Dim strFormula As String = getExpression(dsFrml.Tables("Formula").Rows(0).Item("functionid").ToString, intAllocationID, intAllocationTranUnkId, dtTable)
                Dim c As New clsFomulaEvaluate
                Dim decAmt As Decimal = c.Eval(strFormula)

                If objDic.ContainsKey(intFormulaHeadId) = False Then
                    objDic.Add(intFormulaHeadId, decAmt)
                Else
                    objDic.Item(intFormulaHeadId) = decAmt
                End If
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Function

    Private Function getExpression(ByVal strFormulaID As String, ByVal intAllocationID As Integer, ByVal intAllocationTranUnkId As Integer, ByVal dtTable As DataTable) As String
        Dim temp As String = ""
        Dim strExp As String = ""
        Dim strOperand As String = ""
        Dim blnEx As Boolean = False
        Dim blnSystemBracketOpen As Boolean = False
        Dim blnIsExempt As Boolean = False
        Dim strIFExp As String = ""
        Dim intIFOpenBracket As Integer = 0
        Dim intIFCloseBracket As Integer = 0
        Dim blnIFStarted As Boolean = False
        Dim decCondition1 As Decimal = 0
        Dim decCondition2 As Decimal = 0
        Dim decTRUEPart As Decimal = 0
        Dim decFALSEPart As Decimal = 0
        Dim strIFOperator As String = ""
        Dim intIFPart As Integer = 0
        Dim objFormula As New clsFomulaEvaluate

        Try

            For i As Integer = 1 To strFormulaID.Length

                If Mid(strFormulaID, i, 1) = "+" OrElse Mid(strFormulaID, i, 1) = "-" OrElse Mid(strFormulaID, i, 1) = "*" OrElse Mid(strFormulaID, i, 1) = "/" OrElse _
                    Mid(strFormulaID, i, 1) = "(" OrElse Mid(strFormulaID, i, 1) = ")" OrElse _
                    Mid(strFormulaID, i, 1) = ">" OrElse Mid(strFormulaID, i, 1) = ">=" OrElse Mid(strFormulaID, i, 1) = "<" OrElse Mid(strFormulaID, i, 1) = "<=" OrElse Mid(strFormulaID, i, 1) = "=" OrElse Mid(strFormulaID, i, 1) = "<>" OrElse Mid(strFormulaID, i, 1) = "," Then

                    If temp.Trim.Length > 0 Then
                        If blnIFStarted = True Then
                            strIFExp += strOperand & temp
                        Else
                            strExp += strOperand & temp
                        End If

                        If blnSystemBracketOpen = True Then
                            strExp += ")"
                            blnSystemBracketOpen = False
                        End If

                        strOperand = ""
                        blnEx = True
                    End If

                    temp = Trim(Mid(strFormulaID, i, 1))

                    If temp = "+" Then
                        If strOperand.Trim.Length > 0 Then
                            If blnIsExempt = True Then
                                strOperand = "("
                                blnIsExempt = False
                            Else
                                strOperand += "("
                            End If
                            blnSystemBracketOpen = True
                        End If
                        strOperand += temp
                    ElseIf temp = "-" Then
                        If strOperand.Trim.Length > 0 Then
                            If blnIsExempt = True Then
                                strOperand = "("
                                blnIsExempt = False
                            Else
                                strOperand += "("
                            End If
                            blnSystemBracketOpen = True
                        End If
                        strOperand += temp

                    ElseIf temp = "*" Then
                        If strOperand.Trim.Length > 0 Then
                            If blnIsExempt = True Then
                                strOperand = "("
                                blnIsExempt = False
                            Else
                                strOperand += "("
                            End If
                            blnSystemBracketOpen = True
                        End If
                        strOperand += temp
                    ElseIf temp = "/" Then
                        If strOperand.Trim.Length > 0 Then
                            If blnIsExempt = True Then
                                strOperand = "("
                                blnIsExempt = False
                            Else
                                strOperand += "("
                            End If
                            blnSystemBracketOpen = True
                        End If
                        strOperand += temp
                    ElseIf temp = "(" Then
                        blnEx = False
                        strOperand += temp

                        If blnIFStarted = True Then
                            intIFOpenBracket += 1
                            If intIFOpenBracket > 1 Then
                                strIFExp += strOperand
                            End If
                            strOperand = ""
                        Else
                            strExp += strOperand
                            strOperand = ""
                        End If


                    ElseIf temp = ")" Then
                        blnEx = True

                        If blnIFStarted = True Then
                            intIFCloseBracket += 1
                            If intIFOpenBracket = intIFCloseBracket Then
                                decFALSEPart = objFormula.Eval(strIFExp)
                                strIFExp = ""

                                Select Case strIFOperator
                                    Case ">"
                                        If decCondition1 > decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                    Case ">="
                                        If decCondition1 >= decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                    Case "<"
                                        If decCondition1 < decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                    Case "<="
                                        If decCondition1 <= decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                    Case "="
                                        If decCondition1 = decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                    Case "<>"
                                        If decCondition1 <> decCondition2 Then
                                            strExp += decTRUEPart.ToString
                                        Else
                                            strExp += decFALSEPart.ToString
                                        End If
                                End Select

                                blnIFStarted = False
                                intIFOpenBracket = 0
                                intIFCloseBracket = 0
                                intIFPart = 0
                                strIFOperator = ""
                                decCondition1 = 0
                                decCondition2 = 0
                                decTRUEPart = 0
                                decFALSEPart = 0
                            Else
                                strOperand += temp
                                strIFExp += strOperand
                            End If
                        Else
                            strOperand += temp
                            strExp += strOperand
                        End If
                        strOperand = strOperand.Replace("+", "").Replace("-", "").Replace("*", "").Replace("/", "")

                        strOperand = ""

                    ElseIf temp = ">" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = ">=" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = "<" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = "<=" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = "=" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = "<>" Then
                        strIFOperator &= temp
                        If strIFOperator.Trim.Length = 1 Then
                            decCondition1 = objFormula.Eval(strIFExp)
                        Else
                            strOperand = ""
                        End If
                        strIFExp = ""
                    ElseIf temp = "," Then
                        If intIFPart = 0 Then 'Condition
                            decCondition2 = objFormula.Eval(strIFExp)
                        ElseIf intIFPart = 1 Then 'TRUE
                            decTRUEPart = objFormula.Eval(strIFExp)
                        ElseIf intIFPart = 2 Then 'FALSE
                            decFALSEPart = objFormula.Eval(strIFExp)
                        End If
                        strIFExp = ""
                        intIFPart += 1

                    End If

                    temp = ""

                ElseIf Mid(strFormulaID, i, 1) <> "#" Then
                    temp += Trim(Mid(strFormulaID, i, 1))
                Else

                    If temp <> "" Then

                        Select Case temp.ToUpper

                            Case "IF"
                                strExp += strOperand
                                blnIFStarted = True
                                strOperand = ""

                            Case "BSL"

                                If temp.ToUpper = "BSL" Then
                                    Dim decAmt As Decimal = 0
                                    decAmt = (From p In dtTable Where (CBool(p.Item("IsSalary")) = True AndAlso CInt(p.Item("Id")) = intAllocationID AndAlso CInt(p.Item("AllocationtranunkId")) = intAllocationTranUnkId) Select (CDec(p.Item("pramount")))).Sum()
                                    If blnIFStarted = True Then
                                        strIFExp += strOperand & decAmt.ToString
                                        strOperand = ""
                                    Else
                                        strExp += strOperand & decAmt.ToString
                                        strOperand = ""
                                    End If

                                End If
                                blnEx = True

                            Case Else

                                If temp.Length >= 5 AndAlso temp.Substring(0, 4) = "BSL|" Then
                                    'If marrHeadID.Contains(CInt(temp.Substring(4))) = False Then
                                    '    If blnIFStarted = True Then
                                    '        strIFExp += strOperand & "0"
                                    '    Else
                                    '        strExp += strOperand & "0"
                                    '    End If
                                    'Else
                                    '    If blnIFStarted = True Then
                                    '        strIFExp += strOperand & mdecBasicScale
                                    '    Else
                                    '        strExp += strOperand & mdecBasicScale
                                    '    End If
                                    'End If
                                    Dim decAmt As Decimal = 0
                                    decAmt = (From p In dtTable Where (CBool(p.Item("IsSalary")) = True AndAlso CInt(p.Item("tranheadunkid")) = CInt(temp.Substring(4)) AndAlso CInt(p.Item("Id")) = intAllocationID AndAlso CInt(p.Item("AllocationtranunkId")) = intAllocationTranUnkId) Select (CDec(p.Item("pramount")))).Sum()
                                    If blnIFStarted = True Then
                                        strIFExp += strOperand & decAmt.ToString
                                    Else
                                        strExp += strOperand & decAmt.ToString
                                    End If
                                    strOperand = ""
                                    blnEx = True

                                Else

                                    If objDic.ContainsKey(CInt(temp)) = True Then
                                        '    'If objExemp.isExist(mintPayPeriodUnkID, mintEmployeeunkid, CInt(temp)) = False Then
                                        If blnIFStarted = True Then
                                            strIFExp += strOperand & objDic.Item(CInt(temp)).ToString
                                        Else
                                            strExp += strOperand & objDic.Item(CInt(temp)).ToString
                                        End If
                                        strOperand = ""
                                        blnEx = True
                                        '    'Else
                                        '    '    blnIsExempt = True 'Sohail (16 Dec 2011)
                                        '    'End If
                                    Else

                                        'If GetValue(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, CInt(temp), objDic, dtDatabase_Start_Date, dtDatabase_End_Date, intLeaveBalanceSetting, blnIsFin_Close) = False Then
                                        '    Return ""
                                        'Else
                                        'If objExemp.isExist(mintPayPeriodUnkID, mintEmployeeunkid, CInt(temp)) = False Then
                                        If GetValue(CInt(temp), intAllocationID, intAllocationTranUnkId, dtTable) = False Then
                                            Return ""
                                        Else
                                            If blnIFStarted = True Then
                                                strIFExp += strOperand & objDic.Item(CInt(temp)).ToString
                                            Else
                                                strExp += strOperand & objDic.Item(CInt(temp)).ToString
                                            End If
                                        End If
                                        strOperand = ""
                                        blnEx = True
                                        'Else
                                        '    blnIsExempt = True 'Sohail (16 Dec 2011)
                                        'End If
                                        'End If
                                    End If

                                End If



                        End Select
                        temp = ""
                    End If
                End If
            Next

            If temp.Trim.Length > 0 Then
                If blnIFStarted = True Then
                    strIFExp += strOperand & temp
                Else
                    strExp += strOperand & temp
                End If

                If blnSystemBracketOpen = True Then
                    If blnIFStarted = True Then
                        strIFExp += ")"
                    Else
                        strExp += ")"
                    End If
                    blnSystemBracketOpen = False
                End If
                strOperand = ""
                blnEx = True
            End If
            strOperand = strOperand.Replace("+", "").Replace("-", "").Replace("*", "").Replace("/", "")
            strExp += strOperand
            Return strExp.Replace("()", "")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Function IsValidForGrid() As Boolean
        Try
            If txtBudgetCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter Budget code."), enMsgBoxStyle.Information)
                txtBudgetCode.Focus()
                Return False
            ElseIf txtBudgetName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Budget name."), enMsgBoxStyle.Information)
                txtBudgetName.Focus()
                Return False
            ElseIf CInt(cboViewBy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Budget For."), enMsgBoxStyle.Information)
                cboViewBy.Focus()
                Return False
            ElseIf CInt(cboPresentation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Presentation."), enMsgBoxStyle.Information)
                cboPresentation.Focus()
                Return False
            ElseIf CInt(cboWhoToInclude.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Who to Include."), enMsgBoxStyle.Information)
                cboWhoToInclude.Focus()
                Return False
            ElseIf CInt(cboWhoToInclude.SelectedValue) = enBudgetWhoToInclude.AllAsPerManPowerPlan AndAlso cboSalaryLevel.Visible = True AndAlso CInt(cboSalaryLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Salary Level."), enMsgBoxStyle.Information)
                cboSalaryLevel.Focus()
                Return False
            ElseIf dtpBudgetDate.Value.Date < FinancialYear._Object._Database_Start_Date.Date OrElse dtpBudgetDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Budget date should be between Financial year start date and end date."), enMsgBoxStyle.Information)
                cboWhoToInclude.Focus()
                Return False
            ElseIf lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please tick atleast one previous period."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
            ElseIf dtViewHead.Table.Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please tick atleast one transaction head."), enMsgBoxStyle.Information)
                dgvTranHead.Focus()
                Return False
            ElseIf dtViewHead.Table.Select("IsChecked = 1 AND IsSalary = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please tick atleast one Salary Type transaction head."), enMsgBoxStyle.Information)
                dgvTranHead.Focus()
                Return False
            ElseIf mintViewIdx < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Allocation / Employee."), enMsgBoxStyle.Information)
                lnkSetAnalysis.Focus()
                Return False
                'Sohail (24 Feb 2022) -- Start
                'Issue :  : StartIndex cannot be less than zero.
            ElseIf mstrAnalysis_TableName.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Allocation / Employee."), enMsgBoxStyle.Information)
                lnkSetAnalysis.Focus()
                Return False
                'Sohail (24 Feb 2022) -- End
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidForGrid", mstrModuleName)
        End Try
    End Function

    'Sohail (01 Mar 2017) -- Start
    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
    Private Function IsValidForSave(Optional ByVal blnForExport As Boolean = False) As Boolean
        Try
            If txtBudgetCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter Budget code."), enMsgBoxStyle.Information)
                txtBudgetCode.Focus()
                Return False
            ElseIf txtBudgetName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Budget name."), enMsgBoxStyle.Information)
                txtBudgetName.Focus()
                Return False
            ElseIf mdtTable.Select("Id <> -1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Generate data first."), enMsgBoxStyle.Information)
                Return False
            End If

            If blnForExport = False Then
                If objBudget_Master._Budget_statusunkid = enApprovalStatus.APPROVED OrElse objBudget_Master._Budget_statusunkid = enApprovalStatus.REJECTED Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do save this budget. Reason : This budget is already approved/rejected."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidForSave", mstrModuleName)
        End Try
    End Function
    'Sohail (01 Mar 2017) -- End

    Private Sub cbo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            blnIsDelKey = False
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "formula_code"
                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                    CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = CInt(cbo.SelectedValue)
                    'dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = CInt(cbo.SelectedValue)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                    cbo.SelectedValue = 0
                    CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                    'dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
                End If
                'ElseIf CInt(AscW(e.KeyChar)) = 13 Then
                '    If dgvTranHead.CurrentRow.Index < dgvTranHead.RowCount Then
                '        dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index + 1).Cells(colhFormulaMappingHead.Index)
                '    Else
                '        dgvTranHead.CurrentRow.Cells(colhTranHeadName.Index).Selected = True
                '    End If
            ElseIf CInt(AscW(e.KeyChar)) = 8 Then 'Backspace
                cbo.Text = ""
                cbo.Tag = ""
                cbo.SelectedValue = 0
                CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                'dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
                blnIsDelKey = True
            Else
                cbo.Text = ""
                cbo.Tag = ""
                cbo.SelectedValue = 0
                CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                'dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cbo_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cbo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.Delete Then
                Dim cbo As ComboBox = CType(sender, ComboBox)
                If cbo.SelectionLength = Len(cbo.Text) Then
                    cbo.Text = ""
                    cbo.Tag = ""
                    cbo.SelectedValue = 0
                    CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                    'dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
                    dgvTranHead.CurrentCell.Value = 0
                    blnIsDelKey = True
                    'MsgBox(CType(sender, ComboBox).SelectedValue.ToString)
                    'MsgBox(dgvTranHead.CurrentCell.Value.ToString)
                    'MsgBox(dgvTranHead.CurrentCell.EditedFormattedValue.ToString)
                    'MsgBox(dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid").ToString)
                End If
            ElseIf e.KeyCode = Keys.Return Then
                If dgvTranHead.CurrentRow.Index < dgvTranHead.RowCount Then
                    CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = CInt(dgvTranHead.CurrentCell.Value)
                    dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index + 1).Cells(colhFormulaMappingHead.Index)
                Else
                    dgvTranHead.CurrentRow.Cells(colhTranHeadName.Index).Selected = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cbo_KeyDown", mstrModuleName)
        End Try
    End Sub

    'Private Sub cbo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    Try
    '        Dim cbo As ComboBox = CType(sender, ComboBox)
    '        cbo.DropDownStyle = ComboBoxStyle.Simple
    '        If (e.KeyValue >= 65 AndAlso e.KeyValue <= 90) Or (e.KeyValue >= 97 AndAlso e.KeyValue <= 122) Or (e.KeyValue >= 47 AndAlso e.KeyValue <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cbo.ValueMember
    '                .DisplayMember = cbo.DisplayMember
    '                .DataSource = CType(cbo.DataSource, DataTable)
    '                .CodeMember = "formula_code"
    '            End With

    '            Dim c As Char = Convert.ToChar(e.KeyValue)
    '            frm.TypedText = c.ToString

    '            If frm.DisplayDialog Then
    '                cbo.SelectedValue = frm.SelectedValue
    '                cbo.Tag = frm.SelectedAlias
    '            Else
    '                cbo.Text = ""
    '                cbo.Tag = ""
    '                cbo.SelectedValue = 0
    '                dgvTranHead.CurrentRow.Cells(colhFormulaMappingHead.Index).Value = 0
    '                dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
    '                dtViewHead.Table.AcceptChanges()
    '                dgvTranHead.Focus()
    '                dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index).Cells(colhFormulaMappingHead.Index)
    '                SendKeys.Send("{F2}")
    '            End If
    '            frm = Nothing
    '        ElseIf e.KeyValue = 13 Then
    '            If dgvTranHead.CurrentRow.Index < dgvTranHead.RowCount - 1 Then
    '                dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index + 1).Cells(colhFormulaMappingHead.Index)
    '            Else
    '                dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index).Cells(colhTranHeadName.Index)
    '            End If
    '            SendKeys.Send("{F2}")
    '        ElseIf e.KeyCode = Keys.ShiftKey OrElse e.KeyCode = Keys.Menu OrElse e.KeyCode = Keys.ControlKey OrElse _
    '                 e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Left OrElse _
    '                 e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Back Then
    '            cbo.DropDownStyle = ComboBoxStyle.Simple
    '        Else
    '            cbo.Text = ""
    '            cbo.Tag = ""
    '            cbo.SelectedValue = 0
    '            dgvTranHead.CurrentRow.Cells(colhFormulaMappingHead.Index).Value = 0
    '            dtViewHead.Table.Rows(dgvTranHead.CurrentRow.Index).Item("mappedformulaheadunkid") = 0
    '            dtViewHead.Table.AcceptChanges()
    '            dgvTranHead.Focus()
    '            dgvTranHead.CurrentCell = dgvTranHead.Rows(dgvTranHead.CurrentRow.Index).Cells(colhFormulaMappingHead.Index)
    '            SendKeys.Send("{F2}")
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cbo_KeyDown", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cbo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If cbo.SelectedValue IsNot Nothing AndAlso TypeOf cbo.SelectedValue Is System.Int32 Then

                dgvTranHead.CurrentRow.Cells(objcolhFormulaMappingHead.Index).Value = CInt(cbo.SelectedValue)

                If CInt(cbo.SelectedValue) <= 0 Then
                    cbo.Text = ""
                    cbo.Tag = ""
                    CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                    'dtViewHead.ToTable.Ro
                    'dgvTranHead.CurrentCell.Value = 0
                    'dgvTranHead.Refresh()
                    blnIsDelKey = True
                Else
                    blnIsDelKey = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cbo_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudget_Master = Nothing
            objBudget_Tran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetAddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetAddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_MasterNew.SetMessages()
            clsBudget_TranNew.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_MasterNew, clsBudget_TranNew"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmBudgetAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudget_Master = New clsBudget_MasterNew
        objBudget_Tran = New clsBudget_TranNew
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            btnClose.CausesValidation = False

            If menAction = enAction.EDIT_ONE Then
                objBudget_Master._Budgetunkid = mintBudgetUnkId
                cboBudgetYear.Enabled = False
                cboYear.Enabled = False
                cboViewBy.Enabled = False
                cboPresentation.Enabled = False
                cboWhoToInclude.Enabled = False
                cboSalaryLevel.Enabled = False
                'lnkSetAnalysis.Enabled = False

                mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetUnkId)
                mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetUnkId)
                mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetUnkId)

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Payroll Budget.
                If objBudget_Master._Budget_statusunkid = enApprovalStatus.APPROVED OrElse objBudget_Master._Budget_statusunkid = enApprovalStatus.REJECTED Then
                    btnSave.Visible = False
mnuResetBudget.Visible = False 'Sohail (29 Mar 2017)
                Else
                    btnSave.Visible = True
mnuResetBudget.Visible = True 'Sohail (29 Mar 2017)
                End If
                mnuImportPercentageAllocation.Visible = True
            Else
                mnuImportPercentageAllocation.Visible = False
                'Sohail (01 Mar 2017) -- End
            End If

            Call FillCombo()
            Call FillTranHeadList()

            Call GetValue()

            If menAction = enAction.EDIT_ONE Then

                'Sohail (01 Oct 2016) -- Start
                'Enhancement - 63.1 - Don't allow to approve budget if total percentage is not 100
                'If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Allocation Then
                '    Dim frm As New frmViewAnalysis
                '    frm.displayDialog(, mintViewIdx, mstrAllocationTranUnkIDs, True)
                '    mstrStringIds = frm._ReportBy_Ids
                '    mstrStringName = frm._ReportBy_Name
                '    mintViewIdx = frm._ViewIndex
                '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids
                '    If mintViewIdx = 0 Then mintViewIdx = -1

                '    mstrAnalysis_Fields = frm._Analysis_Fields
                '    mstrAnalysis_Join = frm._Analysis_Join
                '    mstrAnalysis_TableName = frm._Analysis_TableName
                '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
                '    mstrReport_GroupName = frm._Report_GroupName
                '    frm = Nothing

                'ElseIf CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Employee Then
                '    mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetUnkId)
                '    Dim frm As New frmEmpSelection
                '    frm._EmployeeAsOnStartDate = dtpBudgetDate.Value.Date
                '    frm._EmployeeAsOnEndDate = dtpBudgetDate.Value.Date
                '    frm.displayDialog(mstrEmployeeIDs, , True)
                '    mstrStringIds = frm._ReportBy_Ids
                '    mstrStringName = frm._ReportBy_Name
                '    mintViewIdx = frm._ViewIndex
                '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids

                '    mstrAnalysis_Fields = frm._Analysis_Fields
                '    mstrAnalysis_Join = frm._Analysis_Join
                '    mstrAnalysis_TableName = frm._Analysis_TableName
                '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
                '    mstrReport_GroupName = frm._Report_GroupName
                '    frm = Nothing
                'End If
                If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Allocation Then
                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
                    'frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtpBudgetDate.Value.Date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName)
                    frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, dtpBudgetDate.Value.Date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                    'Sohail (01 Mar 2017) -- End
                    If mintViewIdx = 0 Then mintViewIdx = -1
                ElseIf CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Employee Then
                    mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetUnkId)
                    'Sohail (01 Mar 2017) -- Start
                    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
                    'frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName)
                    frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                    'Sohail (01 Mar 2017) -- End
                    mintViewIdx = 0
                    mstrAllocationTranUnkIDs = mstrEmployeeIDs
                End If
                'Sohail (01 Oct 2016) -- End

                Call btnGenerate.PerformClick()
            End If

            txtBudgetCode.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetAddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        Try
            lvPeriod.Items.Clear()
            mstrPreviousPeriodDBName = CType(cboYear.SelectedItem, DataRowView).Item("database_name").ToString
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboYear.SelectedValue), CType(cboYear.SelectedItem, DataRowView).Item("database_name").ToString, eZeeDate.convertDate(CType(cboYear.SelectedItem, DataRowView).Item("start_date").ToString), "Period", False)

            For Each dtRow As DataRow In dsList.Tables("Period").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = CInt(dtRow.Item("yearunkid"))

                'lvItem.SubItems.Add(.Item("payyearunkid").ToString)
                'lvItem.SubItems(colh_Period_PayYear.Index).Tag = .Item("payyearunkid").ToString

                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems(colh_Period_PayPeriod.Index).Tag = dtRow.Item("periodunkid").ToString

                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("start_date").ToString).ToShortDateString)
                lvItem.SubItems(colh_Period_StartDate.Index).Tag = dtRow.Item("start_date").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("end_date").ToString).ToShortDateString)
                lvItem.SubItems(colh_Period_EndDate.Index).Tag = dtRow.Item("end_date").ToString

                RemoveHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
                If mstrPeriodIdList.Split(CChar(",")).Contains(dtRow.Item("periodunkid").ToString) = True Then
                    lvItem.Checked = True
                    lvPeriod.Items.Insert(Array.IndexOf(mstrPeriodIdList.Split(CChar(",")), dtRow.Item("periodunkid").ToString), lvItem)
                Else
                    lvItem.Checked = False
                    lvPeriod.Items.Add(lvItem)
                End If
                AddHandler lvPeriod.ItemChecked, AddressOf lvPeriod_ItemChecked
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            mintViewIdx = -1

            If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Employee Then
                dsCombo = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude", True, True)
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                lnkAllocation.Visible = True
                'Nilay (22 Nov 2016) -- End
            Else
                dsCombo = objMaster.getComboListForBudgetWhoToInclude("WhoToInclude", True, False)
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                lnkAllocation.Visible = False
                'Nilay (22 Nov 2016) -- End
            End If

            With cboWhoToInclude
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("WhoToInclude")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboViewBy_SelectedIndexChanged", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub cboPresentation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPresentation.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListForBudgetAffectedColumns("AffectedColumns", CType(CInt(cboPresentation.SelectedValue), enBudgetPresentation), True)
            With cboAffectedColumns
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AffectedColumns")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPresentation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboWhoToInclude_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboWhoToInclude.SelectedIndexChanged
        Try
            If CInt(cboWhoToInclude.SelectedValue) = enBudgetWhoToInclude.AllAsPerManPowerPlan Then
                cboSalaryLevel.Enabled = True
            Else
                cboSalaryLevel.SelectedValue = 0
                cboSalaryLevel.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboWhoToInclude_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            If dtViewHead Is Nothing Then Exit Try

            Dim strFilter As String = ""
            If CInt(cboTrnHeadType.SelectedValue) > 0 Then
                strFilter = " IsChecked = 1 OR trnheadtype_id = " & CInt(cboTrnHeadType.SelectedValue) & " "
            End If
            dtViewHead.RowFilter = strFilter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private Sub cboAffectedColumns_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAffectedColumns.SelectedIndexChanged
        Try
            If CInt(cboAffectedColumns.SelectedValue) > 0 AndAlso _
               CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Project_Code Then
                lblProjectCode.Enabled = True
                cboProjectCode.Enabled = True
                radPercentage.Checked = True
                radFixedAmount.Enabled = False
                txtValue.Decimal = 0
                chkAddToExisting.Enabled = False
            Else
                lblProjectCode.Enabled = False
                cboProjectCode.SelectedValue = 0
                cboProjectCode.Enabled = False
                radFixedAmount.Enabled = True
                txtValue.Decimal = 0
                chkAddToExisting.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAffectedColumns_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (22 Nov 2016) -- End

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If CInt(cboAffectedColumns.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select Affected Columns."), enMsgBoxStyle.Information)
                cboAffectedColumns.Focus()
                Exit Try
                'Sohail (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
            ElseIf CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Project_Code AndAlso CInt(cboProjectCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Please select Project code."), enMsgBoxStyle.Information)
                cboProjectCode.Focus()
                Exit Try
                'Sohail (22 Nov 2016) -- End
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'ElseIf radApplyToChecked.Checked = True AndAlso mdtTable.Select("IsChecked = 1 ").Length <= 0 Then
            ElseIf radApplyToChecked.Checked = True AndAlso mdtDataView.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Nilay (22 Nov 2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please tick atleast one transaction."), enMsgBoxStyle.Information)
                dgvBudget.Focus()
                Exit Try
            End If

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            If radPercentage.Checked = True Then
                'Sohail (07 Apr 2017) -- Start
                'Enhancement - 65.1 - Including heads and amount in Export and Import option on Budget Codes.
                'If CDec(txtValue.Decimal) > 100 Then
                If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Project_Code AndAlso CDec(txtValue.Decimal) > 100 Then
                    'Sohail (07 Apr 2017) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Pecentage cannot be greater than 100."), enMsgBoxStyle.Information)
                    txtValue.Focus()
                    Exit Sub
                End If
            End If
            'Nilay (22 Nov 2016) -- End

            Dim lstRow As List(Of DataRow)

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            'If radApplyToChecked.Checked = True Then
            '    lstRow = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
            'Else
            '    lstRow = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (p)).ToList
            'End If
            If radApplyToChecked.Checked = True Then
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
            Else
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1) Select (p)).ToList
            End If
            'Nilay (22 Nov 2016) -- End

            Cursor.Current = Cursors.WaitCursor

            For Each dtRow As DataRow In lstRow

                If chkAddToExisting.Checked = True Then
                    If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                        If radPercentage.Checked = True Then
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Transaction_Head Then
                                'dtRow.Item("budgetamount") = CDec(dtRow.Item("budgetamount")) + (CDec(dtRow.Item("budgetamount")) * txtValue.Decimal / 100)
                                dtRow.Item("budgetamount") = CDec(dtRow.Item("budgetamount")) + (CDec(dtRow.Item("pramount")) * txtValue.Decimal / 100)
                            End If
                        Else
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Transaction_Head Then
                                dtRow.Item("budgetamount") = CDec(dtRow.Item("budgetamount")) + txtValue.Decimal
                            End If
                        End If
                    Else
                        If radPercentage.Checked = True Then
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Basic_Salary OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                'dtRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount")) + (CDec(dtRow.Item("budgetsalaryamount")) * txtValue.Decimal / 100)
                                dtRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount")) + (CDec(dtRow.Item("_|1")) * txtValue.Decimal / 100)
                            End If
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Other_Payroll_Cost OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                'dtRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount")) + (CDec(dtRow.Item("budgetamount")) * txtValue.Decimal / 100)
                                dtRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount")) + (CDec(dtRow.Item("_|2")) * txtValue.Decimal / 100)
                            End If
                        Else
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Basic_Salary OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                dtRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount")) + txtValue.Decimal
                            End If
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Other_Payroll_Cost OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                dtRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount")) + txtValue.Decimal
                            End If
                        End If
                    End If
                Else
                    If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                        If radPercentage.Checked = True Then
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Transaction_Head Then
                                'dtRow.Item("budgetamount") = CDec(dtRow.Item("budgetamount")) * txtValue.Decimal / 100
                                dtRow.Item("budgetamount") = CDec(dtRow.Item("pramount")) * txtValue.Decimal / 100
                            End If
                        Else
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Transaction_Head Then
                                dtRow.Item("budgetamount") = txtValue.Decimal
                            End If
                        End If
                    Else
                        If radPercentage.Checked = True Then
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Basic_Salary OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                'dtRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount")) * txtValue.Decimal / 100
                                dtRow.Item("budgetsalaryamount") = CDec(dtRow.Item("_|1")) * txtValue.Decimal / 100
                            End If
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Other_Payroll_Cost OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                'dtRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount")) * txtValue.Decimal / 100
                                dtRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("_|2")) * txtValue.Decimal / 100
                            End If


                        Else
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Basic_Salary OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                dtRow.Item("budgetsalaryamount") = txtValue.Decimal
                            End If
                            If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Other_Payroll_Cost OrElse CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Both Then
                                dtRow.Item("budgetotherpayrollamount") = txtValue.Decimal
                            End If
                        End If
                    End If
                    'Nilay (22 Nov 2016) -- Start
                    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                    If radPercentage.Checked = True Then
                        If CInt(cboAffectedColumns.SelectedValue) = enBudgetAffectedColumns.Project_Code Then
                            Dim strProjectCodeCol As String = "|_" & CStr(cboProjectCode.SelectedValue)
                            If strProjectCodeCol.Trim.Length > 0 Then
                                dtRow.Item(strProjectCodeCol) = txtValue.Decimal
                            End If
                        End If
                    End If
                    'Nilay (22 Nov 2016) -- End
                End If

            Next
            mdtTable.AcceptChanges()

            Call UpdateBudgetTotal()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApply_Click", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click

        Try
            Call GridSetup()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGenerate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuResetBudget.Click 'btnReset.Click 'Sohail (01 Mar 2017) - [Export and Import option on Payroll Budget]
        Try
            If mdtTable Is Nothing Then Exit Sub 'Sohail (01 Mar 2017)
            If mdtTable IsNot Nothing Then mdtTable.Rows.Clear()
            gbBudget.Enabled = True
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Increase / decrease fund project code balance on activity adjustment as per comment given by andrew.
            For Each ctrl As Control In gbBudget.Controls
                If menAction = enAction.EDIT_ONE AndAlso (ctrl.Name = cboBudgetYear.Name OrElse ctrl.Name = cboYear.Name OrElse ctrl.Name = cboViewBy.Name OrElse ctrl.Name = cboPresentation.Name OrElse ctrl.Name = cboWhoToInclude.Name OrElse ctrl.Name = cboSalaryLevel.Name) Then
                    ctrl.Enabled = False
                Else
                    ctrl.Enabled = True
                End If
            Next
            'Sohail (22 Nov 2016) -- End

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            cboAffectedColumns.SelectedValue = 0
            txtValue.Decimal = 0
            'Nilay (22 Nov 2016) -- End

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            mdtDataView = mdtTable.DefaultView
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'Sohail (01 Mar 2017) -- Start
            'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
            'If txtBudgetCode.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter Budget code."), enMsgBoxStyle.Information)
            '    txtBudgetCode.Focus()
            '    Exit Try
            'ElseIf txtBudgetName.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter Budget name."), enMsgBoxStyle.Information)
            '    txtBudgetName.Focus()
            '    Exit Try
            '    'Sohail (22 Nov 2016) -- Start
            '    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
            '    'ElseIf dgvBudget.RowCount <= 0 Then
            'ElseIf mdtTable.Select("Id <> -1 ").Length <= 0 Then
            '    'Sohail (22 Nov 2016) -- End
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Generate data first."), enMsgBoxStyle.Information)
            '    Exit Try
            'End If

            ''Sohail (01 Mar 2017) -- Start
            ''Enhancement - 65.1 - Export and Import option on Payroll Budget.
            'If objBudget_Master._Budget_statusunkid = enApprovalStatus.APPROVED OrElse objBudget_Master._Budget_statusunkid = enApprovalStatus.REJECTED Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do save this budget. Reason : This budget is already approved/rejected."), enMsgBoxStyle.Information)
            '    Exit Try
            'End If
            ''Sohail (01 Mar 2017) -- End
            If IsValidForSave() = False Then Exit Try
            'Sohail (01 Mar 2017) -- End

            'Prompt user before saving payroll budgeting if all %s are not set

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            'Dim lst As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            'If lst.Count > 0 Then
            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Total Percentage should be 100 for all transactions to set this budget as default budget.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 25, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
            '        Exit Try
            '    End If
            'End If
            Dim lst As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) > 100) Select (p)).ToList
            If lst.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Total Percentage cannot be greater then 100."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim lst1 As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) < 100) Select (p)).ToList
            If lst1.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Total Percentage should be 100 for all transactions to set this budget as default budget.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 25, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If
            'Nilay (22 Nov 2016) -- End

            Dim intFundId As Integer
            Dim decTotal As Decimal = 0
            For Each pair In mdicFund
                intFundId = pair.Key
                decTotal = 0

                Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
                If lstRow.Count > 0 Then
                    For Each dRow As DataRow In lstRow

                        If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                            decTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                        Else
                            decTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                        End If
                    Next

                    If decTotal > pair.Value Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "New Budget Figure is exceeding to the Project Code Current Balance.") & vbCrLf & vbCrLf & mdicFundCode.Item(pair.Key).ToString & " New Budget Figure : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicFundCode.Item(pair.Key).ToString & " Current Balance : " & Format(pair.Value, GUI.fmtCurrency) & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 21, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Try
                        Else
                            Exit For
                        End If
                    End If
                End If
            Next

            Call SetValue()

            Dim dtHead As DataTable = New DataView(dtViewHead.Table, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            Dim dtBudget As DataTable = New DataView(mdtTable, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            Dim dtAllHead As DataTable = New DataView(dsAllHeads.Tables(0), "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strFundIDs() As String = (From p In mdtTable.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray
            If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.Summary Then
                For Each dtRow As DataRow In mdtTable.Rows
                    Dim dr() As DataRow = dtAllHead.Select("IsSalary = 1 AND Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                    For Each dRow As DataRow In dr
                        dRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount"))
                        dRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount"))
                        For Each col As String In strFundIDs
                            dRow.Item(col) = CDec(dtRow.Item(col))
                        Next
                    Next
                    dtAllHead.AcceptChanges()

                    dr = dtAllHead.Select("IsSalary = 0 AND Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                    For Each dRow As DataRow In dr
                        dRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount"))
                        dRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount"))
                        For Each col As String In strFundIDs
                            dRow.Item(col) = CDec(dtRow.Item(col))
                        Next
                    Next
                    dtAllHead.AcceptChanges()
                Next
            End If

            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - All employees or all depts must be ticked if allocation is dept to make it default budget.
            If objBudget_Master._Selected_Employees <> objBudget_Master._Total_Employees Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "All allocations / employees must be included in this budget to set this budget as default budget.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 25, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Try
                End If
            End If

            'Sohail (22 Nov 2016) -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (19 Apr 2017) -- Start
                'MST Enhancement - 66.1 - applying user access filter on payroll budget.
                'blnFlag = objBudget_Master.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
                Dim strBudgettranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgettranunkid").ToString)).Distinct().ToArray)
                blnFlag = objBudget_Master.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, strBudgettranUnkIds)
                'Sohail (19 Apr 2017) -- End
            Else
                blnFlag = objBudget_Master.InsertAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
            End If

            If blnFlag = False And objBudget_Master._Message <> "" Then
                eZeeMsgBox.Show(objBudget_Master._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBudget_Master = New clsBudget_MasterNew
                    Call GetValue()
                Else
                    mintBudgetUnkId = objBudget_Master._Budgetunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtBudgetName.Text, objBudget_Master._Budget_Name1, objBudget_Master._Budget_Name2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (26-Aug-2016) -- END

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If mdtDataView Is Nothing Then Exit Try 'Sohail (24 Feb 2022) - Issue : : Baylor-Malawi - Object reference error on search on Budget Add Edit screen
            mstrSearch = " Id <> -1 "
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
            'If txtTotalPercentage.Text.Trim <> "" Then
            If txtTotalPercentage.Decimal <> 0 Then
                'Sohail (22 Nov 2016) -- End
                mstrSearch &= "AND colhTotal " & cboCondition.Text & " " & txtTotalPercentage.Decimal & " "
            End If
            If mstrFilteredEmployeeIDs.Trim <> "" Then
                mstrSearch &= "AND allocationtranunkid IN (" & mstrFilteredEmployeeIDs & ") "
            End If

            mdtDataView.RowFilter = mstrSearch

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrFilteredEmployeeIDs = ""
            mdtDataView.RowFilter = ""
            txtTotalPercentage.Decimal = 0
            cboCondition.SelectedValue = enComparison_Operator.GREATER_THAN

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (22 Nov 2016) -- End

    'Sohail (01 Mar 2017) -- Start
    'Enhancement - 65.1 - Export and Import option on Payroll Budget.
    Private Sub mnuExportPercentageAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportPercentageAllocation.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsDataSet As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As FileInfo
        Try
            If mdtTable Is Nothing Then Exit Try

            If IsValidForSave(True) = False Then Exit Try
            'If ConfigParameter._Object._ExportDataPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 666, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'dlgSaveFile.InitialDirectory = ConfigParameter._Object._ExportDataPath
            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Execl files(*.xlsx)|*.xlsx"
            dlgSaveFile.FilterIndex = 2
            'path = ConfigParameter._Object._ExportDataPath & "\" & Company._Object._Code & "_"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(dlgSaveFile.FileName)
                If ObjFile.Exists = True Then ObjFile.Delete()

                strFilePath = dlgSaveFile.FileName
                'strFilePath = path & ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)

                'strFilePath &= ObjFile.Extension

                Dim strColList As List(Of String)
                 strColList = (From p In mdicFund Select ("|_" & p.Key.ToString)).ToList
                'If strColList.Trim <> "" Then
                '    strColList = Chr(34) & "," & strColList
                'End If
                'Sohail (07 Apr 2017) -- Start
                'Enhancement - 65.1 - Including heads and amount in Export and Import option on Budget Codes.
                'Dim a() As String = {"budgetunkid", "budget_code", "budget_name", "payyearunkid", "viewbyid", "allocationbyid", "presentationmodeid", "whotoincludeid", "salarylevelid", "budget_date", "isdefault", "allocationtranunkid", "Id", "GCode", "GName", "colhTotal"}
                'strColList.AddRange(a.ToList)

                'Dim dtBudget As DataTable = New DataView(mdtTable, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable(True, strColList.ToArray)
                'Dim dtCol As DataColumn
                Dim dtBudget As DataTable
                Dim d_R() As DataRow = dtViewHead.Table.Select("IsChecked = 1")
                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                    dtBudget = mdtTable.Clone

                    Dim dtCol As DataColumn
                    For Each dtRow As DataRow In d_R

                        dtCol = New DataColumn("OLD" & dtRow.Item("tranheadunkid").ToString, System.Type.GetType("System.Decimal"))
                        dtCol.DefaultValue = 0
                        dtBudget.Columns.Add(dtCol)

                        dtCol = New DataColumn("NEW" & dtRow.Item("tranheadunkid").ToString, System.Type.GetType("System.Decimal"))
                        dtCol.DefaultValue = 0
                        dtBudget.Columns.Add(dtCol)
                    Next

                    Dim strPrevKey As String = ""
                    Dim dr As DataRow = Nothing
                    For Each dtRow As DataRow In dsAllHeads.Tables(0).Rows
                        If CInt(dtRow.Item("Id")) = -1 AndAlso dtRow.Item("GName").ToString = "" Then
                            strPrevKey = dtRow.Item("Id").ToString & "_" & dtRow.Item("allocationtranunkid").ToString
                            Continue For
                        End If


                        If strPrevKey <> dtRow.Item("Id").ToString & "_" & dtRow.Item("allocationtranunkid").ToString Then
                            dr = dtBudget.NewRow

                            dr.ItemArray = dtRow.ItemArray

                            dtBudget.Rows.Add(dr)
                        End If

                        dtBudget.Rows(dtBudget.Rows.Count - 1).Item("OLD" & dtRow.Item("tranheadunkid").ToString) = CDec(dtRow.Item("amount"))
                        dtBudget.Rows(dtBudget.Rows.Count - 1).Item("NEW" & dtRow.Item("tranheadunkid").ToString) = CDec(dtRow.Item("budgetamount"))

                        strPrevKey = dtRow.Item("Id").ToString & "_" & dtRow.Item("allocationtranunkid").ToString
                    Next
                    dtBudget.AcceptChanges()
                Else
                    dtBudget = New DataView(mdtTable, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
                End If
                'Sohail (07 Apr 2017) -- End

                Dim intColIdx As Integer = -1

                intColIdx += 1
                dtBudget.Columns("budget_code").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Budget Code"

                intColIdx += 1
                dtBudget.Columns("budget_name").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Budget Name"

                intColIdx += 1
                dtBudget.Columns("GCode").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Emp / Allocation Code"

                intColIdx += 1
                dtBudget.Columns("GName").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhAllocationID").HeaderText

                If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                    For Each dtRow As DataRow In d_R

                        intColIdx += 1
                        dtBudget.Columns("OLD" & dtRow.Item("tranheadunkid").ToString).SetOrdinal(intColIdx)
                        dtBudget.Columns(intColIdx).ColumnName = dtRow.Item("HeadCode").ToString

                        intColIdx += 1
                        dtBudget.Columns("NEW" & dtRow.Item("tranheadunkid").ToString).SetOrdinal(intColIdx)
                        dtBudget.Columns(intColIdx).ColumnName = dtRow.Item("HeadCode").ToString & " " & "Budget"

                    Next
                Else
                    intColIdx += 1
                    dtBudget.Columns("_|1").SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = "Basic Salary"

                    intColIdx += 1
                    dtBudget.Columns("budgetsalaryamount").SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = "Basic Salary Budget"

                    intColIdx += 1
                    dtBudget.Columns("_|2").SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = "Other Payroll Cost"

                    intColIdx += 1
                    dtBudget.Columns("budgetotherpayrollamount").SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = "Other Payroll Cost Budget"

                End If

                Dim intFundId As Integer = 0
                For Each pair In mdicFundCode
                    intFundId = pair.Key

                    'Dim intActivityId As Integer = CInt(dgvBudget.Columns("colhActivity" & intFundId.ToString).Value)

                    intColIdx += 1
                    dtBudget.Columns("|_" & intFundId).SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhFund" & intFundId.ToString).HeaderText

                    'dtCol = New DataColumn("||__" & intFundId, System.Type.GetType("System.String"))
                    'dtCol.AllowDBNull = False
                    'dtCol.DefaultValue = ""
                    'dtBudget.Columns.Add(dtCol)

                    'intColIdx += 1
                    'dtBudget.Columns("||__" & intFundId).SetOrdinal(intColIdx)
                    'dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhFund" & intFundId.ToString).Tag.ToString & " " & dgvBudget.Columns("colhActivity" & intFundId.ToString).HeaderText
                Next

                'Dim strFundIDs() As String = (From p In dtBudget.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray

                intColIdx += 1
                dtBudget.Columns("colhTotal").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Total (%)"

                'For Each dtRow As DataRow In dtBudget.Rows
                '    For Each pair In mdicFundCode
                '        intFundId = pair.Key

                '        If mdicActivityCode.ContainsKey(CInt(dtRow.Item("||_" & intFundId))) = True Then
                '            dtRow.Item(mdicFundCode.Item(intFundId).ToString & " " & dgvBudget.Columns("colhActivity" & intFundId.ToString).HeaderText) = mdicActivityCode.Item(CInt(dtRow.Item("||_" & intFundId)))
                '        End If
                '    Next
                'Next

                For i As Integer = intColIdx + 1 To dtBudget.Columns.Count - 1
                    dtBudget.Columns.RemoveAt(intColIdx + 1)
                Next

                dsDataSet.Tables.Clear()
                dsDataSet.Tables.Add(dtBudget)
                If dsDataSet.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "There is no data to Export."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Select Case dlgSaveFile.FilterIndex
                    'Case 1   'CSV
                    '    Dim objCsvExport As New clsExportData
                    '    ObjFile = New FileInfo(strFilePath)
                    '    objCsvExport.ExportToCSV(dsDataSet.Tables(0), strFilePath)
                    Case 1 'XML
                        dsDataSet.WriteXml(strFilePath)
                    Case 2  'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsDataSet)
                        OpenXML_Export(strFilePath, dsDataSet)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "File Exported Successfully to Export Data Path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportPercentageAllocation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuImportPercentageAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportPercentageAllocation.Click
        Dim frm As New frmImportBudget
        Try
            If IsValidForSave() = False Then Exit Try

            'Sohail (07 Apr 2017) -- Start
            'Enhancement - 65.1 - Including heads and amount in Export and Import option on Budget Codes.
            'If frm.displayDialog() = True Then
            If frm.displayDialog(mintBudgetUnkId) = True Then
                'Sohail (07 Apr 2017) -- End
                Call GridSetup()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportPercentageAllocation_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 Mar 2017) -- End

#End Region

#Region " DataGridView Events "

    Private Sub dgvTranHead_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranHead.CellContentClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvTranHead.IsCurrentCellDirty Then
                Me.dgvTranHead.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTranHead_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranHead.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub

            Select Case e.ColumnIndex
                Case colhFormulaMappingHead.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvTranHead_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranHead.CellValidated
        Try
            If e.RowIndex = -1 Then Exit Sub

            Select Case e.ColumnIndex

                Case colhFormulaMappingHead.Index
                    dgvTranHead.EndEdit()
                    If blnIsDelKey = False AndAlso CInt(dgvTranHead.CurrentRow.Cells(colhFormulaMappingHead.Index).Value) > 0 Then
                        Dim objFormula As New clsBudgetformula_Tran
                        Dim ds As DataSet = objFormula.GetList("Formula", CInt(dgvTranHead.CurrentRow.Cells(colhFormulaMappingHead.Index).Value), ConfigParameter._Object._CurrentDateAndTime)

                        If ds.Tables("Formula").Rows.Count > 0 Then
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadCode") = ds.Tables("Formula").Rows(0).Item("formula_code").ToString
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadName") = ds.Tables("Formula").Rows(0).Item("formula_name").ToString
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formulaid") = ds.Tables("Formula").Rows(0).Item("functionid").ToString
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formula") = ds.Tables("Formula").Rows(0).Item("function").ToString
                        Else
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadCode") = ""
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadName") = ""
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formulaid") = ""
                            CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formula") = ""
                        End If
                    Else
                        CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("mappedformulaheadunkid") = 0
                        CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadCode") = ""
                        CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("MappedHeadName") = ""
                        CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formulaid") = ""
                        CType(dgvTranHead.CurrentRow.DataBoundItem, DataRowView).Item("Formula") = ""
                    End If

            End Select
            If e.ColumnIndex = colhFormulaMappingHead.Index Then


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_CellValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTranHead_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTranHead.CellValueChanged
        Try
            If e.RowIndex = -1 Then Exit Sub

            'If e.ColumnIndex = colhFormulaMappingHead.Index Then

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub dgvTranHead_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvTranHead.CurrentCellDirtyStateChanged
    '    Try
    '        If dgvTranHead.IsCurrentCellDirty Then
    '            'dgvTranHead.CommitEdit(DataGridViewDataErrorContexts.Commit)
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub dgvTranHead_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvTranHead.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTranHead_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTranHead.EditingControlShowing
        Try
            Select Case dgvTranHead.CurrentCell.ColumnIndex
                Case colhFormulaMappingHead.Index
                    If e.Control IsNot Nothing Then
                        Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                        cbo.DropDownStyle = ComboBoxStyle.DropDown
                        cbo.AutoCompleteMode = AutoCompleteMode.SuggestAppend

                        RemoveHandler cbo.KeyPress, AddressOf cbo_KeyPress
                        RemoveHandler cbo.KeyDown, AddressOf cbo_KeyDown
                        RemoveHandler cbo.SelectionChangeCommitted, AddressOf cbo_SelectionChangeCommitted
                        AddHandler cbo.KeyPress, AddressOf cbo_KeyPress
                        AddHandler cbo.KeyDown, AddressOf cbo_KeyDown
                        AddHandler cbo.SelectionChangeCommitted, AddressOf cbo_SelectionChangeCommitted
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTranHead_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBudget.CellClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If e.ColumnIndex > 0 AndAlso e.ColumnIndex <= 4 Then
                If dgvBudget.Rows(e.RowIndex).Cells("colhSrNo").FormattedValue.ToString <> "" Then
                    If dgvBudget.Rows(e.RowIndex).Cells("colhCollaps").Value.ToString = "-" Then
                        dgvBudget.Rows(e.RowIndex).Cells("colhCollaps").Value = "+"
                    Else
                        dgvBudget.Rows(e.RowIndex).Cells("colhCollaps").Value = "-"
                    End If

                    For i = e.RowIndex + 1 To dgvBudget.RowCount - 1
                        If CInt(dgvBudget.Rows(e.RowIndex).Cells("colhAllocationID").Value) = CInt(dgvBudget.Rows(i).Cells("colhAllocationID").Value) AndAlso dgvBudget.Rows(e.RowIndex).Cells("colhAllocation").Value.ToString = dgvBudget.Rows(i).Cells("colhAllocation").Value.ToString Then
                            If dgvBudget.Rows(i).Visible = False Then
                                dgvBudget.Rows(i).Visible = True
                            Else
                                dgvBudget.Rows(i).Visible = False
                            End If
                        Else
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBudget.CellContentClick
        Try
            If e.RowIndex = -1 Then Exit Sub


            'If Me.dgvBudget.IsCurrentCellDirty Then
            '    Me.dgvBudget.CommitEdit(DataGridViewDataErrorContexts.Commit)
            'End If

            'If e.ColumnIndex = objcolhIsCheck.Index Then
            '    Me.dgvBudget.CommitEdit(DataGridViewDataErrorContexts.Commit)
            '    dgvBudget.Refresh()
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvBudget.CellMouseMove
        Try
            If e.RowIndex = -1 Then Exit Sub

            If e.ColumnIndex > 0 AndAlso e.ColumnIndex <= 4 Then
                If dgvBudget.Rows(e.RowIndex).Cells("colhSrNo").FormattedValue.ToString <> "" Then
                    dgvBudget.Cursor = Cursors.Hand
                Else
                    dgvBudget.Cursor = Cursors.Default
                End If
            Else
                dgvBudget.Cursor = Cursors.Default
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellMouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvBudget.CellValidating
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CInt(dgvBudget.CurrentRow.Cells("colhAllocationId").Value) = -1 Then Exit Sub 'Last row Grand Total Row

            If dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHFUND") = True Then

                If e.FormattedValue.ToString = "" OrElse CDec(e.FormattedValue) < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Percentage should be between 0 and 100."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Try
                End If


                Dim lstCols As List(Of DataGridViewColumn) = (From p In dgvBudget.Columns.Cast(Of DataGridViewColumn)() Where (p.Name.ToUpper.StartsWith("COLHFUND") AndAlso p.Index <> e.ColumnIndex) Select (p)).ToList

                Dim decTot As Decimal = 0
                For Each dc As DataGridViewColumn In lstCols
                    decTot += CDec(CType(dgvBudget.Rows(e.RowIndex).DataBoundItem, DataRowView).Item(dc.DataPropertyName))
                Next
                If decTot + CDec(e.FormattedValue) > 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, All Fund Sources percentage should not be greater than 100."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Try
                End If

                If CDec(e.FormattedValue.ToString) <> CDec(dgvBudget.CurrentCell.Value) Then
                    Dim decOldValue As Decimal = CDec(dgvBudget.CurrentCell.Value)
                    Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                    For Each dsRow As DataRow In lstRow
                        dsRow.Item(dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).DataPropertyName) = CDec(e.FormattedValue.ToString)
                    Next
                    Call UpdateBudgetTotal()

                    Dim intFundId As Integer = CInt(dgvBudget.Columns(e.ColumnIndex).Name.Substring(8).ToString)
                    Dim decTotal As Decimal = 0
                    'For Each pair In mdicFund
                    'intFundId = pair.Key
                    decTotal = 0

                    lstRow = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
                    If lstRow.Count > 0 Then
                        For Each dRow As DataRow In lstRow

                            If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                                decTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                            Else
                                decTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                            End If
                        Next

                        If decTotal > CDec(mdicFund.Item(intFundId)) Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "New Budget Figure is exceeding to the Project Code Current Balance.") & vbCrLf & vbCrLf & mdicFundCode.Item(intFundId).ToString & " New Budget Figure : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicFundCode.Item(intFundId).ToString & " Current Balance : " & Format(CDec(mdicFund.Item(intFundId)), GUI.fmtCurrency) & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 21, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Dim lst_Row As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                                For Each dsRow As DataRow In lst_Row
                                    dsRow.Item(dgvBudget.Columns(e.ColumnIndex).DataPropertyName) = decOldValue
                                Next
                                Call UpdateBudgetTotal()
                                e.Cancel = True
                                Exit Try
                            Else
                                If e.RowIndex < dgvBudget.RowCount Then
                                    dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
                                End If
                                'Exit For
                            End If
                        End If
                    End If
                    'Next
                    'Sohail (08 Nov 2017) -- Start
                    'Issue - 70.1 - Index was out of range. Must be non-negative and less than the size of the collection. Parameter name: index.
                    'If e.RowIndex < dgvBudget.RowCount Then
                    If e.RowIndex < dgvBudget.RowCount - 1 Then
                        'Sohail (08 Nov 2017) -- End
                        dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
                    End If
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBudget.CellValueChanged
        Try
            If e.RowIndex = -1 Then Exit Sub

            If dgvBudget.Columns(e.ColumnIndex).Name.ToUpper = "COLHBUDGETAMOUNT" OrElse dgvBudget.Columns(e.ColumnIndex).Name.ToUpper = "COLHBUDGETSALARYAMOUNT" OrElse dgvBudget.Columns(e.ColumnIndex).Name.ToUpper = "COLHBUDGETOTHERPAYROLLAMOUNT" OrElse dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHFUND") Then
                If IsDBNull(dgvBudget.CurrentCell.Value) = True Then
                    dgvBudget.CurrentCell.Value = 0
                End If
                If dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHFUND") Then
                    Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                    For Each dsRow As DataRow In lstRow
                        dsRow.Item(dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).DataPropertyName) = CDec(dgvBudget.CurrentCell.Value)
                    Next
                    mdtTable.AcceptChanges()
                End If
                Call UpdateBudgetTotal()

                If e.RowIndex < dgvBudget.RowCount Then
                    dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
                Else
                    '    dgvBudget.CurrentRow.Cells("COLHBUDGETAMOUNT").Selected = True
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBudget.EditingControlShowing
        Try

            If dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).Name.ToUpper.StartsWith("COLHFUND") = True Then
                If e.Control IsNot Nothing Then
                    Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                    RemoveHandler txt.KeyPress, AddressOf tb_keypress
                    AddHandler txt.KeyPress, AddressOf tb_keypress
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvBudget.DataBindingComplete
        Try
            If dgvBudget.RowCount <= 0 Then Exit Try

            If CInt(dgvBudget.Rows(dgvBudget.RowCount - 1).Cells("colhAllocationID").Value) = -1 Then 'Grand Total
                dgvBudget.Rows(dgvBudget.RowCount - 1).ReadOnly = True
                dgvBudget.Rows(dgvBudget.RowCount - 1).DefaultCellStyle.BackColor = Color.Blue
                dgvBudget.Rows(dgvBudget.RowCount - 1).DefaultCellStyle.ForeColor = Color.White
                dgvBudget.Rows(dgvBudget.RowCount - 1).DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                dgvBudget.Rows(dgvBudget.RowCount - 1).Cells(0) = New DataGridViewTextBoxCell
                dgvBudget.Rows(dgvBudget.RowCount - 1).Cells(0).Style.ForeColor = dgvBudget.Rows(dgvBudget.RowCount - 1).DefaultCellStyle.BackColor
                dgvBudget.Rows(dgvBudget.RowCount - 1).Cells(0).Style.SelectionBackColor = Color.Blue
                dgvBudget.Rows(dgvBudget.RowCount - 1).Cells(0).Style.SelectionForeColor = Color.Blue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_DataBindingComplete", mstrModuleName)
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
        Finally
            objbtnSearch.ShowResult(mdtDataView.ToTable.Select("Id <> -1 ").Count.ToString)
            'Sohail (22 Nov 2016) -- End
        End Try
    End Sub

    Private Sub dgvBudget_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvBudget.CurrentCellDirtyStateChanged
        Try
            If dgvBudget.CurrentCell.ColumnIndex = 0 AndAlso dgvBudget.IsCurrentCellDirty Then
                dgvBudget.CommitEdit(DataGridViewDataErrorContexts.Commit)
                dgvBudget.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBudget.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_DataError", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Button Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Try
            If CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Allocation Then
                Dim blnOnlyJob As Boolean = False
                If CInt(cboWhoToInclude.SelectedValue) = enBudgetWhoToInclude.AllAsPerManPowerPlan Then
                    blnOnlyJob = True
                End If
                Dim frm As New frmViewAnalysis
                If frm.displayDialog(, mintViewIdx, mstrAllocationTranUnkIDs, False, blnOnlyJob) = True Then
                    mstrStringIds = frm._ReportBy_Ids
                    mstrStringName = frm._ReportBy_Name
                    mintViewIdx = frm._ViewIndex
                    mstrAllocationTranUnkIDs = frm._ReportBy_Ids
                    If mintViewIdx = 0 Then mintViewIdx = -1

                    mstrAnalysis_Fields = frm._Analysis_Fields
                    mstrAnalysis_Join = frm._Analysis_Join
                    mstrAnalysis_TableName = frm._Analysis_TableName
                    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
                    mstrReport_GroupName = frm._Report_GroupName
                    mstrAnalysis_CodeField = frm._Analysis_CodeField 'Sohail (01 Mar 2017)
                Else
                    If menAction <> enAction.EDIT_ONE Then
                        mstrStringIds = ""
                        mstrStringName = ""
                        mintViewIdx = 0
                        mstrAllocationTranUnkIDs = ""
                        If mintViewIdx = 0 Then mintViewIdx = -1

                        mstrAnalysis_Fields = ""
                        mstrAnalysis_Join = ""
                        mstrAnalysis_TableName = ""
                        mstrAnalysis_OrderBy = ""
                        mstrReport_GroupName = ""
                        mstrAnalysis_CodeField = "" 'Sohail (01 Mar 2017)
                    End If

                End If

                frm = Nothing

            ElseIf CInt(cboViewBy.SelectedValue) = enBudgetViewBy.Employee Then
                Dim frm As New frmEmpSelection
                frm._EmployeeAsOnStartDate = dtpBudgetDate.Value.Date
                frm._EmployeeAsOnEndDate = dtpBudgetDate.Value.Date
                If frm.displayDialog(mstrEmployeeIDs) = True Then
                    mstrStringIds = frm._ReportBy_Ids
                    mstrStringName = frm._ReportBy_Name
                    mintViewIdx = frm._ViewIndex
                    mstrAllocationTranUnkIDs = frm._ReportBy_Ids

                    mstrAnalysis_Fields = frm._Analysis_Fields
                    mstrAnalysis_Join = frm._Analysis_Join
                    mstrAnalysis_TableName = frm._Analysis_TableName
                    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
                    mstrReport_GroupName = frm._Report_GroupName
                    mstrAnalysis_CodeField = frm._Analysis_CodeField 'Sohail (01 Mar 2017)
                Else
                    If menAction <> enAction.EDIT_ONE Then
                        mstrStringIds = ""
                        mstrStringName = ""
                        mintViewIdx = 0
                        mstrAllocationTranUnkIDs = ""
                        mstrEmployeeIDs = ""
                        mstrAnalysis_Fields = ""
                        mstrAnalysis_Join = ""
                        mstrAnalysis_TableName = ""
                        mstrAnalysis_OrderBy = ""
                        mstrReport_GroupName = ""
                        mstrAnalysis_CodeField = "" 'Sohail (01 Mar 2017)
                    End If
                End If

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Budget For."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtSearchHead_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchHead.KeyDown

    End Sub
    Private Sub txtSearchHead_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchHead.TextChanged
        Try
            If txtSearchHead.Text.Trim <> "" Then
                dtViewHead.RowFilter = "HeadCode LIKE '%" & txtSearchHead.Text & "%' OR HeadName LIKE '%" & txtSearchHead.Text & "%'"
            Else
                dtViewHead.RowFilter = ""
            End If
            dgvTranHead.DataSource = dtViewHead
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchHead_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Private Sub objchkSelectAll_Budget_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll_Budget.CheckedChanged
        Try
            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            'For Each dtRow As DataRow In mdtTable.Rows
            '    dtRow.Item("IsChecked") = objchkSelectAll_Budget.Checked
            'Next
            For Each dtRow As DataRowView In mdtDataView
                dtRow.Item("IsChecked") = objchkSelectAll_Budget.Checked
            Next
            mdtDataView.Table.AcceptChanges()
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_Budget_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSelectAll_Period_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll_Period.CheckedChanged
        Try
            Call CheckAllPeriod(objchkSelectAll_Period.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_Period_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Listview's Events "
    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            If lvPeriod.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
                objchkSelectAll_Period.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                RemoveHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
                objchkSelectAll_Period.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                RemoveHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
                objchkSelectAll_Period.CheckState = CheckState.Checked
                AddHandler objchkSelectAll_Period.CheckedChanged, AddressOf objchkSelectAll_Period_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
#Region " Link Button's events "
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim frm As New frmAdvanceSearch

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, dtpBudgetDate.Value.Date, dtpBudgetDate.Value.Date, _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                     "Emp", , , , , , , , , , , , , , , , mstrAdvanceFilter)

                mstrFilteredEmployeeIDs = String.Join(",", dsList.Tables("Emp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (22 Nov 2016) -- End



    'Sohail (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
#Region " Other Control Events "

    Private Sub gbBudgetData_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBudgetData.HeaderClick
        Try
            If gbBudgetData.Size.Height = gbBudgetData.HeaderHeight Then

            Else
                gbBudget.Collapse()
                gbBudgetData.Location = New Point(gbBudgetData.Location.X, gbBudget.Location.Y + gbBudget.HeaderHeight + 5)
                gbBudgetData.OpenHeight = 493
                gbBudgetData.Height = 493
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBudgetData_HeaderClick", mstrModuleName)
        End Try
    End Sub

    Private Sub gbBudget_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBudget.HeaderClick
        Try
            If gbBudget.Size.Height = gbBudget.HeaderHeight Then
                gbBudgetData.Location = New Point(gbBudgetData.Location.X, gbBudget.Location.Y + gbBudget.HeaderHeight + 5)
                gbBudgetData.OpenHeight = 493
                gbBudgetData.Height = 493
            Else
                gbBudgetData.Location = New Point(gbBudgetData.Location.X, gbBudget.Location.Y + gbBudget.Size.Height + 5)
                gbBudgetData.OpenHeight = 293
                gbBudgetData.Height = 293
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBudget_HeaderClick", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (22 Nov 2016) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbBudget.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBudget.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBudgetData.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBudgetData.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnGenerate.GradientBackColor = GUI._ButttonBackColor
            Me.btnGenerate.GradientForeColor = GUI._ButttonFontColor

            Me.btnApply.GradientBackColor = GUI._ButttonBackColor
            Me.btnApply.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbBudget.Text = Language._Object.getCaption(Me.gbBudget.Name, Me.gbBudget.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.colh_Period_PayPeriod.Text = Language._Object.getCaption(CStr(Me.colh_Period_PayPeriod.Tag), Me.colh_Period_PayPeriod.Text)
            Me.colh_Period_StartDate.Text = Language._Object.getCaption(CStr(Me.colh_Period_StartDate.Tag), Me.colh_Period_StartDate.Text)
            Me.colh_Period_EndDate.Text = Language._Object.getCaption(CStr(Me.colh_Period_EndDate.Tag), Me.colh_Period_EndDate.Text)
            Me.lblWhoToInclude.Text = Language._Object.getCaption(Me.lblWhoToInclude.Name, Me.lblWhoToInclude.Text)
            Me.lblPresentation.Text = Language._Object.getCaption(Me.lblPresentation.Name, Me.lblPresentation.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
            Me.lblSalaryLevel.Text = Language._Object.getCaption(Me.lblSalaryLevel.Name, Me.lblSalaryLevel.Text)
            Me.btnGenerate.Text = Language._Object.getCaption(Me.btnGenerate.Name, Me.btnGenerate.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblBudgetYear.Text = Language._Object.getCaption(Me.lblBudgetYear.Name, Me.lblBudgetYear.Text)
            Me.cbOnScreenComputations.Text = Language._Object.getCaption(Me.cbOnScreenComputations.Name, Me.cbOnScreenComputations.Text)
            Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
            Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
            Me.chkAddToExisting.Text = Language._Object.getCaption(Me.chkAddToExisting.Name, Me.chkAddToExisting.Text)
            Me.radPercentage.Text = Language._Object.getCaption(Me.radPercentage.Name, Me.radPercentage.Text)
            Me.radFixedAmount.Text = Language._Object.getCaption(Me.radFixedAmount.Name, Me.radFixedAmount.Text)
            Me.lblAffectedColumns.Text = Language._Object.getCaption(Me.lblAffectedColumns.Name, Me.lblAffectedColumns.Text)
            Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
            Me.dgvBudget.Text = Language._Object.getCaption(Me.dgvBudget.Name, Me.dgvBudget.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.lblBudgetCode.Text = Language._Object.getCaption(Me.lblBudgetCode.Name, Me.lblBudgetCode.Text)
            Me.lblBudgetName.Text = Language._Object.getCaption(Me.lblBudgetName.Name, Me.lblBudgetName.Text)
            Me.chkOverWrite.Text = Language._Object.getCaption(Me.chkOverWrite.Name, Me.chkOverWrite.Text)
            Me.colhTranHeadCode.HeaderText = Language._Object.getCaption(Me.colhTranHeadCode.Name, Me.colhTranHeadCode.HeaderText)
            Me.colhTranHeadName.HeaderText = Language._Object.getCaption(Me.colhTranHeadName.Name, Me.colhTranHeadName.HeaderText)
            Me.colhHeadType.HeaderText = Language._Object.getCaption(Me.colhHeadType.Name, Me.colhHeadType.HeaderText)
            Me.colhFormulaMappingHead.HeaderText = Language._Object.getCaption(Me.colhFormulaMappingHead.Name, Me.colhFormulaMappingHead.HeaderText)
            Me.colhIsSalary.HeaderText = Language._Object.getCaption(Me.colhIsSalary.Name, Me.colhIsSalary.HeaderText)
            Me.lblBudgetDate.Text = Language._Object.getCaption(Me.lblBudgetDate.Name, Me.lblBudgetDate.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.Name, Me.lblGrandTotal.Text)
            Me.lblBudgetTotal.Text = Language._Object.getCaption(Me.lblBudgetTotal.Name, Me.lblBudgetTotal.Text)
            Me.lblOtherCostTotal.Text = Language._Object.getCaption(Me.lblOtherCostTotal.Name, Me.lblOtherCostTotal.Text)
            Me.lblTotalEmplyees.Text = Language._Object.getCaption(Me.lblTotalEmplyees.Name, Me.lblTotalEmplyees.Text)
            Me.gbBudgetData.Text = Language._Object.getCaption(Me.gbBudgetData.Name, Me.gbBudgetData.Text)
            Me.lblProjectCode.Text = Language._Object.getCaption(Me.lblProjectCode.Name, Me.lblProjectCode.Text)
            Me.lblTotalPercentage.Text = Language._Object.getCaption(Me.lblTotalPercentage.Name, Me.lblTotalPercentage.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.mnuResetBudget.Text = Language._Object.getCaption(Me.mnuResetBudget.Name, Me.mnuResetBudget.Text)
            Me.ToolStripSeparator1.Text = Language._Object.getCaption(Me.ToolStripSeparator1.Name, Me.ToolStripSeparator1.Text)
            Me.mnuExportPercentageAllocation.Text = Language._Object.getCaption(Me.mnuExportPercentageAllocation.Name, Me.mnuExportPercentageAllocation.Text)
            Me.mnuImportPercentageAllocation.Text = Language._Object.getCaption(Me.mnuImportPercentageAllocation.Name, Me.mnuImportPercentageAllocation.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Transaction Head")
            Language.setMessage(mstrModuleName, 2, "Other Payroll Cost")
            Language.setMessage(mstrModuleName, 3, "Please enter Budget code.")
            Language.setMessage(mstrModuleName, 4, "Please enter Budget name.")
            Language.setMessage(mstrModuleName, 5, "Please select Budget For.")
            Language.setMessage(mstrModuleName, 6, "Please select Presentation.")
            Language.setMessage(mstrModuleName, 7, "Please select Who to Include.")
            Language.setMessage(mstrModuleName, 8, "Please select Salary Level.")
            Language.setMessage(mstrModuleName, 9, "Please tick atleast one previous period.")
            Language.setMessage(mstrModuleName, 10, "Please tick atleast one transaction head.")
            Language.setMessage(mstrModuleName, 11, "Please tick atleast one Salary Type transaction head.")
            Language.setMessage(mstrModuleName, 12, "Please select Allocation / Employee.")
            Language.setMessage(mstrModuleName, 13, "Please select Affected Columns.")
            Language.setMessage(mstrModuleName, 14, "Please tick atleast one transaction.")
            Language.setMessage(mstrModuleName, 15, "Please Generate data first.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Percentage should be between 0 and 100.")
            Language.setMessage(mstrModuleName, 17, "Sorry, All Fund Sources percentage should not be greater than 100.")
            Language.setMessage(mstrModuleName, 18, "Please select Budget For.")
            Language.setMessage(mstrModuleName, 19, "Basic Salary")
            Language.setMessage(mstrModuleName, 20, "New Budget Figure is exceeding to the Project Code Current Balance.")
            Language.setMessage(mstrModuleName, 21, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 22, "Job Title")
            Language.setMessage(mstrModuleName, 23, "Sorry, Budget date should be between Financial year start date and end date.")
            Language.setMessage(mstrModuleName, 24, "Total")
            Language.setMessage(mstrModuleName, 25, "Do you want to continue?")
            Language.setMessage(mstrModuleName, 26, "All allocations / employees must be included in this budget to set this budget as default budget.")
            Language.setMessage(mstrModuleName, 27, "Total Percentage should be 100 for all transactions to set this budget as default budget.")
            Language.setMessage(mstrModuleName, 28, "Total Percentage cannot be greater then 100.")
            Language.setMessage(mstrModuleName, 29, "Pecentage cannot be greater than 100.")
            Language.setMessage(mstrModuleName, 30, "Please select Project code.")
            Language.setMessage(mstrModuleName, 31, "Sorry, You cannot do save this budget. Reason : This budget is already approved/rejected.")
            Language.setMessage(mstrModuleName, 32, "File Exported Successfully to Export Data Path.")
            Language.setMessage(mstrModuleName, 34, "There is no data to Export.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class