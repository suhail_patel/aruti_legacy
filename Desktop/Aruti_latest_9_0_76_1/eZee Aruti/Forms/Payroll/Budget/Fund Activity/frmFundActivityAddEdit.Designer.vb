﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundActivityAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundActivityAddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblActivityCode = New System.Windows.Forms.Label
        Me.lblActivityName = New System.Windows.Forms.Label
        Me.txtActivityCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtActivityName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.cboFundProjectCode = New System.Windows.Forms.ComboBox
        Me.lblFundProjectCode = New System.Windows.Forms.Label
        Me.objbtnSearchFundProjectCode = New eZee.Common.eZeeGradientButton
        Me.gbActivityInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtNotifyAmount = New eZee.TextBox.IntegerTextBox
        Me.lblNotifyAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.IntegerTextBox
        Me.lblCurrentBalance = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        Me.gbActivityInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 253)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(411, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(199, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(302, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblActivityCode
        '
        Me.lblActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityCode.Location = New System.Drawing.Point(10, 62)
        Me.lblActivityCode.Name = "lblActivityCode"
        Me.lblActivityCode.Size = New System.Drawing.Size(111, 17)
        Me.lblActivityCode.TabIndex = 3
        Me.lblActivityCode.Text = "Activity Code"
        '
        'lblActivityName
        '
        Me.lblActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityName.Location = New System.Drawing.Point(10, 89)
        Me.lblActivityName.Name = "lblActivityName"
        Me.lblActivityName.Size = New System.Drawing.Size(111, 17)
        Me.lblActivityName.TabIndex = 5
        Me.lblActivityName.Text = "Activity Name"
        '
        'txtActivityCode
        '
        Me.txtActivityCode.Flags = 0
        Me.txtActivityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActivityCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtActivityCode.Location = New System.Drawing.Point(127, 60)
        Me.txtActivityCode.MaxLength = 255
        Me.txtActivityCode.Name = "txtActivityCode"
        Me.txtActivityCode.Size = New System.Drawing.Size(239, 21)
        Me.txtActivityCode.TabIndex = 1
        '
        'txtActivityName
        '
        Me.txtActivityName.Flags = 0
        Me.txtActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtActivityName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtActivityName.Location = New System.Drawing.Point(127, 87)
        Me.txtActivityName.MaxLength = 255
        Me.txtActivityName.Name = "txtActivityName"
        Me.txtActivityName.Size = New System.Drawing.Size(239, 21)
        Me.txtActivityName.TabIndex = 2
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(372, 87)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 7
        '
        'cboFundProjectCode
        '
        Me.cboFundProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFundProjectCode.FormattingEnabled = True
        Me.cboFundProjectCode.Location = New System.Drawing.Point(127, 33)
        Me.cboFundProjectCode.Name = "cboFundProjectCode"
        Me.cboFundProjectCode.Size = New System.Drawing.Size(239, 21)
        Me.cboFundProjectCode.TabIndex = 0
        '
        'lblFundProjectCode
        '
        Me.lblFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundProjectCode.Location = New System.Drawing.Point(10, 35)
        Me.lblFundProjectCode.Name = "lblFundProjectCode"
        Me.lblFundProjectCode.Size = New System.Drawing.Size(111, 17)
        Me.lblFundProjectCode.TabIndex = 0
        Me.lblFundProjectCode.Text = "Fund Project Code"
        '
        'objbtnSearchFundProjectCode
        '
        Me.objbtnSearchFundProjectCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFundProjectCode.BorderSelected = False
        Me.objbtnSearchFundProjectCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchFundProjectCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFundProjectCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFundProjectCode.Location = New System.Drawing.Point(372, 33)
        Me.objbtnSearchFundProjectCode.Name = "objbtnSearchFundProjectCode"
        Me.objbtnSearchFundProjectCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFundProjectCode.TabIndex = 2
        '
        'gbActivityInformation
        '
        Me.gbActivityInformation.BorderColor = System.Drawing.Color.Black
        Me.gbActivityInformation.Checked = False
        Me.gbActivityInformation.CollapseAllExceptThis = False
        Me.gbActivityInformation.CollapsedHoverImage = Nothing
        Me.gbActivityInformation.CollapsedNormalImage = Nothing
        Me.gbActivityInformation.CollapsedPressedImage = Nothing
        Me.gbActivityInformation.CollapseOnLoad = False
        Me.gbActivityInformation.Controls.Add(Me.lblRemarks)
        Me.gbActivityInformation.Controls.Add(Me.txtRemarks)
        Me.gbActivityInformation.Controls.Add(Me.txtNotifyAmount)
        Me.gbActivityInformation.Controls.Add(Me.lblNotifyAmount)
        Me.gbActivityInformation.Controls.Add(Me.txtAmount)
        Me.gbActivityInformation.Controls.Add(Me.lblCurrentBalance)
        Me.gbActivityInformation.Controls.Add(Me.txtActivityName)
        Me.gbActivityInformation.Controls.Add(Me.objbtnSearchFundProjectCode)
        Me.gbActivityInformation.Controls.Add(Me.lblFundProjectCode)
        Me.gbActivityInformation.Controls.Add(Me.cboFundProjectCode)
        Me.gbActivityInformation.Controls.Add(Me.txtActivityCode)
        Me.gbActivityInformation.Controls.Add(Me.lblActivityName)
        Me.gbActivityInformation.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbActivityInformation.Controls.Add(Me.lblActivityCode)
        Me.gbActivityInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbActivityInformation.ExpandedHoverImage = Nothing
        Me.gbActivityInformation.ExpandedNormalImage = Nothing
        Me.gbActivityInformation.ExpandedPressedImage = Nothing
        Me.gbActivityInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbActivityInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbActivityInformation.HeaderHeight = 25
        Me.gbActivityInformation.HeaderMessage = ""
        Me.gbActivityInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbActivityInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbActivityInformation.HeightOnCollapse = 0
        Me.gbActivityInformation.LeftTextSpace = 0
        Me.gbActivityInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbActivityInformation.Name = "gbActivityInformation"
        Me.gbActivityInformation.OpenHeight = 300
        Me.gbActivityInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbActivityInformation.ShowBorder = True
        Me.gbActivityInformation.ShowCheckBox = False
        Me.gbActivityInformation.ShowCollapseButton = False
        Me.gbActivityInformation.ShowDefaultBorderColor = True
        Me.gbActivityInformation.ShowDownButton = False
        Me.gbActivityInformation.ShowHeader = True
        Me.gbActivityInformation.Size = New System.Drawing.Size(411, 253)
        Me.gbActivityInformation.TabIndex = 0
        Me.gbActivityInformation.Temp = 0
        Me.gbActivityInformation.Text = "Fund Activity Information"
        Me.gbActivityInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNotifyAmount
        '
        Me.txtNotifyAmount.AllowNegative = False
        Me.txtNotifyAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNotifyAmount.DigitsInGroup = 0
        Me.txtNotifyAmount.Flags = 65536
        Me.txtNotifyAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotifyAmount.Location = New System.Drawing.Point(127, 141)
        Me.txtNotifyAmount.MaxDecimalPlaces = 4
        Me.txtNotifyAmount.MaxWholeDigits = 21
        Me.txtNotifyAmount.Name = "txtNotifyAmount"
        Me.txtNotifyAmount.Prefix = ""
        Me.txtNotifyAmount.RangeMax = 2147483647
        Me.txtNotifyAmount.RangeMin = -2147483648
        Me.txtNotifyAmount.Size = New System.Drawing.Size(130, 21)
        Me.txtNotifyAmount.TabIndex = 4
        Me.txtNotifyAmount.Text = "0"
        Me.txtNotifyAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNotifyAmount
        '
        Me.lblNotifyAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotifyAmount.Location = New System.Drawing.Point(10, 141)
        Me.lblNotifyAmount.Name = "lblNotifyAmount"
        Me.lblNotifyAmount.Size = New System.Drawing.Size(111, 29)
        Me.lblNotifyAmount.TabIndex = 11
        Me.lblNotifyAmount.Text = "Notify me when Amounts falls below"
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = False
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 65536
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(127, 114)
        Me.txtAmount.MaxDecimalPlaces = 4
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 2147483647
        Me.txtAmount.RangeMin = -2147483648
        Me.txtAmount.ReadOnly = True
        Me.txtAmount.Size = New System.Drawing.Size(130, 21)
        Me.txtAmount.TabIndex = 3
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentBalance
        '
        Me.lblCurrentBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBalance.Location = New System.Drawing.Point(10, 118)
        Me.lblCurrentBalance.Name = "lblCurrentBalance"
        Me.lblCurrentBalance.Size = New System.Drawing.Size(111, 17)
        Me.lblCurrentBalance.TabIndex = 8
        Me.lblCurrentBalance.Text = "Current Balance"
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.HideSelection = False
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtRemarks.Location = New System.Drawing.Point(127, 168)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(239, 74)
        Me.txtRemarks.TabIndex = 5
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(10, 175)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(111, 17)
        Me.lblRemarks.TabIndex = 213
        Me.lblRemarks.Text = "Description"
        '
        'frmFundActivityAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 308)
        Me.Controls.Add(Me.gbActivityInformation)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundActivityAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Fund Activity"
        Me.objFooter.ResumeLayout(False)
        Me.gbActivityInformation.ResumeLayout(False)
        Me.gbActivityInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblActivityCode As System.Windows.Forms.Label
    Friend WithEvents lblActivityName As System.Windows.Forms.Label
    Friend WithEvents txtActivityCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtActivityName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFundProjectCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblFundProjectCode As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchFundProjectCode As eZee.Common.eZeeGradientButton
    Friend WithEvents gbActivityInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCurrentBalance As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtNotifyAmount As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblNotifyAmount As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
End Class
