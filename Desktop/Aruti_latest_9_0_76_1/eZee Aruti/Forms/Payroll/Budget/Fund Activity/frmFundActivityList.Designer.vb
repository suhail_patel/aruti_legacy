﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundActivityList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundActivityList))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchFundProjectCode = New eZee.Common.eZeeGradientButton
        Me.cboFundActivity = New System.Windows.Forms.ComboBox
        Me.LblFundActivity = New System.Windows.Forms.Label
        Me.objbtnSearchFundActivity = New eZee.Common.eZeeGradientButton
        Me.cboFundProjectCode = New System.Windows.Forms.ComboBox
        Me.lblFundProjectCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvFundActivity = New eZee.Common.eZeeListView(Me.components)
        Me.ColhFundProjectCode = New System.Windows.Forms.ColumnHeader
        Me.colhActivityCode = New System.Windows.Forms.ColumnHeader
        Me.colhActivityName = New System.Windows.Forms.ColumnHeader
        Me.colhCurrentBalance = New System.Windows.Forms.ColumnHeader
        Me.colhNotifyAmount = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchFundProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboFundActivity)
        Me.gbFilterCriteria.Controls.Add(Me.LblFundActivity)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchFundActivity)
        Me.gbFilterCriteria.Controls.Add(Me.cboFundProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblFundProjectCode)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(11, 8)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(628, 65)
        Me.gbFilterCriteria.TabIndex = 13
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(601, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(578, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchFundProjectCode
        '
        Me.objbtnSearchFundProjectCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundProjectCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFundProjectCode.BorderSelected = False
        Me.objbtnSearchFundProjectCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFundProjectCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFundProjectCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFundProjectCode.Location = New System.Drawing.Point(280, 34)
        Me.objbtnSearchFundProjectCode.Name = "objbtnSearchFundProjectCode"
        Me.objbtnSearchFundProjectCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFundProjectCode.TabIndex = 90
        '
        'cboFundActivity
        '
        Me.cboFundActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFundActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFundActivity.FormattingEnabled = True
        Me.cboFundActivity.Location = New System.Drawing.Point(420, 34)
        Me.cboFundActivity.Name = "cboFundActivity"
        Me.cboFundActivity.Size = New System.Drawing.Size(175, 21)
        Me.cboFundActivity.TabIndex = 300
        '
        'LblFundActivity
        '
        Me.LblFundActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFundActivity.Location = New System.Drawing.Point(335, 36)
        Me.LblFundActivity.Name = "LblFundActivity"
        Me.LblFundActivity.Size = New System.Drawing.Size(79, 17)
        Me.LblFundActivity.TabIndex = 301
        Me.LblFundActivity.Text = "Fund Activity"
        '
        'objbtnSearchFundActivity
        '
        Me.objbtnSearchFundActivity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFundActivity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundActivity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundActivity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFundActivity.BorderSelected = False
        Me.objbtnSearchFundActivity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFundActivity.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFundActivity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFundActivity.Location = New System.Drawing.Point(601, 34)
        Me.objbtnSearchFundActivity.Name = "objbtnSearchFundActivity"
        Me.objbtnSearchFundActivity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFundActivity.TabIndex = 299
        '
        'cboFundProjectCode
        '
        Me.cboFundProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFundProjectCode.FormattingEnabled = True
        Me.cboFundProjectCode.Location = New System.Drawing.Point(99, 34)
        Me.cboFundProjectCode.Name = "cboFundProjectCode"
        Me.cboFundProjectCode.Size = New System.Drawing.Size(175, 21)
        Me.cboFundProjectCode.TabIndex = 296
        '
        'lblFundProjectCode
        '
        Me.lblFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundProjectCode.Location = New System.Drawing.Point(13, 36)
        Me.lblFundProjectCode.Name = "lblFundProjectCode"
        Me.lblFundProjectCode.Size = New System.Drawing.Size(79, 17)
        Me.lblFundProjectCode.TabIndex = 297
        Me.lblFundProjectCode.Text = "Project Code"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 317)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(650, 55)
        Me.objFooter.TabIndex = 14
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(438, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(335, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(232, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(541, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvFundActivity
        '
        Me.lvFundActivity.BackColorOnChecked = True
        Me.lvFundActivity.ColumnHeaders = Nothing
        Me.lvFundActivity.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColhFundProjectCode, Me.colhActivityCode, Me.colhActivityName, Me.colhCurrentBalance, Me.colhNotifyAmount})
        Me.lvFundActivity.CompulsoryColumns = ""
        Me.lvFundActivity.FullRowSelect = True
        Me.lvFundActivity.GridLines = True
        Me.lvFundActivity.GroupingColumn = Nothing
        Me.lvFundActivity.HideSelection = False
        Me.lvFundActivity.Location = New System.Drawing.Point(11, 79)
        Me.lvFundActivity.MinColumnWidth = 50
        Me.lvFundActivity.MultiSelect = False
        Me.lvFundActivity.Name = "lvFundActivity"
        Me.lvFundActivity.OptionalColumns = ""
        Me.lvFundActivity.ShowMoreItem = False
        Me.lvFundActivity.ShowSaveItem = False
        Me.lvFundActivity.ShowSelectAll = True
        Me.lvFundActivity.ShowSizeAllColumnsToFit = True
        Me.lvFundActivity.Size = New System.Drawing.Size(628, 231)
        Me.lvFundActivity.Sortable = True
        Me.lvFundActivity.TabIndex = 15
        Me.lvFundActivity.UseCompatibleStateImageBehavior = False
        Me.lvFundActivity.View = System.Windows.Forms.View.Details
        '
        'ColhFundProjectCode
        '
        Me.ColhFundProjectCode.DisplayIndex = 1
        Me.ColhFundProjectCode.Tag = "ColhFundProjectCode"
        Me.ColhFundProjectCode.Text = "Fund ProjectCode"
        Me.ColhFundProjectCode.Width = 0
        '
        'colhActivityCode
        '
        Me.colhActivityCode.DisplayIndex = 0
        Me.colhActivityCode.Tag = "colhActivityCode"
        Me.colhActivityCode.Text = "Code"
        Me.colhActivityCode.Width = 100
        '
        'colhActivityName
        '
        Me.colhActivityName.Tag = "colhActivityName"
        Me.colhActivityName.Text = "Activity Name"
        Me.colhActivityName.Width = 250
        '
        'colhCurrentBalance
        '
        Me.colhCurrentBalance.Tag = "colhCurrentBalance"
        Me.colhCurrentBalance.Text = "Current Balance"
        Me.colhCurrentBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhCurrentBalance.Width = 170
        '
        'colhNotifyAmount
        '
        Me.colhNotifyAmount.Tag = "colhNotifyAmount"
        Me.colhNotifyAmount.Text = "Notify Amount"
        Me.colhNotifyAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhNotifyAmount.Width = 100
        '
        'frmFundActivityList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 372)
        Me.Controls.Add(Me.lvFundActivity)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundActivityList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Fund Activity  List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboFundProjectCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblFundProjectCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvFundActivity As eZee.Common.eZeeListView
    Friend WithEvents ColhFundProjectCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhActivityCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhActivityName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCurrentBalance As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchFundProjectCode As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchFundActivity As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFundActivity As System.Windows.Forms.ComboBox
    Friend WithEvents LblFundActivity As System.Windows.Forms.Label
    Friend WithEvents colhNotifyAmount As System.Windows.Forms.ColumnHeader
End Class
