﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmFundSources_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFundSources_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private objFundSource As clsFundSource_Master
    Private mintFundSourceunkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFundSourceunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFundSourceunkid = intFundSourceunkid
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Form's Events "

    Private Sub frmFundSources_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objFundSource = Nothing
    End Sub

    Private Sub frmFundSources_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmFundSources_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmFundSources_AddEdit_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundSource_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsFundSource_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmFundSources_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFundSource = New clsFundSource_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()

            If menAction = enAction.EDIT_ONE Then
                objFundSource._FundSourceunkid = mintFundSourceunkid
                Call GetValue()
            End If

            txtFundCode.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundSources_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtFundCode.BackColor = GUI.ColorComp
            txtFundName.BackColor = GUI.ColorComp
            txtProjectCode.BackColor = GUI.ColorComp
            txtCurrentCeilingBal.BackColor = GUI.ColorOptional
            txtNotifyAmount.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtFundCode.Text = objFundSource._FundCode
            txtFundName.Text = objFundSource._FundName
            txtProjectCode.Text = objFundSource._Project_Code
            txtCurrentCeilingBal.Text = Format(objFundSource._CurrentCeilingBalance, GUI.fmtCurrency)
            If menAction = enAction.EDIT_ONE Then
                dtExpiryDate.Value = objFundSource._FundExpiryDate.Date
            End If
            txtNotifyAmount.Decimal = objFundSource._Notify_Amount
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFundSource._FundCode = txtFundCode.Text.Trim
            objFundSource._FundName = txtFundName.Text.Trim
            objFundSource._Project_Code = txtProjectCode.Text.Trim
            objFundSource._CurrentCeilingBalance = txtCurrentCeilingBal.Decimal
            objFundSource._FundExpiryDate = dtExpiryDate.Value
            objFundSource._Notify_Amount = txtNotifyAmount.Decimal
            objFundSource._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If txtFundCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information)
                txtFundCode.Focus()
                Return False
            ElseIf txtFundName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Fund Name cannot be blank. Fund Name is required information."), enMsgBoxStyle.Information)
                txtFundName.Focus()
                Return False
                'ElseIf txtProjectCode.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Project Code cannot be blank. Project Code is required information."), enMsgBoxStyle.Information)
                '    txtProjectCode.Focus()
                '    Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFundSource.Update(ConfigParameter._Object._CurrentDateAndTime)
            Else
                blnFlag = objFundSource.Insert(ConfigParameter._Object._CurrentDateAndTime)
            End If

            If blnFlag = False And objFundSource._Message <> "" Then
                eZeeMsgBox.Show(objFundSource._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objFundSource = New clsFundSource_Master
                    txtFundCode.Text = "" : txtFundName.Text = "" : txtProjectCode.Text = ""
                    Call GetValue()
                    txtFundCode.Focus()
                Else
                    mintFundSourceunkid = objFundSource._FundSourceunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (26-Aug-2016) -- Start
    'For Other Language ADD Name1, Name2 
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtFundName.Text, objFundSource._FundName1, objFundSource._FundName2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (26-Aug-2016) -- END

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFundSources.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFundSources.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbFundSources.Text = Language._Object.getCaption(Me.gbFundSources.Name, Me.gbFundSources.Text)
			Me.lblFundCode.Text = Language._Object.getCaption(Me.lblFundCode.Name, Me.lblFundCode.Text)
			Me.lblCurrentCeilingBal.Text = Language._Object.getCaption(Me.lblCurrentCeilingBal.Name, Me.lblCurrentCeilingBal.Text)
			Me.lblFundName.Text = Language._Object.getCaption(Me.lblFundName.Name, Me.lblFundName.Text)
			Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
			Me.lblProjectCode.Text = Language._Object.getCaption(Me.lblProjectCode.Name, Me.lblProjectCode.Text)
			Me.lblNotifyAmount.Text = Language._Object.getCaption(Me.lblNotifyAmount.Name, Me.lblNotifyAmount.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Fund Name cannot be blank. Fund Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Project Code cannot be blank. Project Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class