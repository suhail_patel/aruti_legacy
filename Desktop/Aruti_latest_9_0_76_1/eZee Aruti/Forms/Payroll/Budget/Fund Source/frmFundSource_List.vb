﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmFundSource_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundSource_List"
    Private objFundSource As clsFundSource_Master

#End Region

#Region " Form's Events "

    Private Sub frmFundSource_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmFundSource_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundSource_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundSource_List_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvFundSource.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFundSource_List_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFundSource_List_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsFundSource_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsFundSource_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmFundSource_List_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmFundSource_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFundSource = New clsFundSource_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmFundSource_List_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            objFundSource = New clsFundSource_Master

            dsList = objFundSource.GetComboList("List", True)
            With cboFundName
                .ValueMember = "fundsourceunkid"
                .DisplayMember = "fundname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim mstrSearch As String = String.Empty
            Dim dsList As New DataSet
            objFundSource = New clsFundSource_Master

            If User._Object.Privilege._AllowToViewFundSource = False Then Exit Sub

            If CInt(cboFundName.SelectedValue) > 0 Then
                mstrSearch &= "AND bgfundsource_master.fundsourceunkid = " & CInt(cboFundName.SelectedValue) & " "
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Substring(3)
            End If

            dsList = objFundSource.GetList("List", mstrSearch)

            dgvFundSource.AutoGenerateColumns = False
            dgcolhFundCode.DataPropertyName = "fundcode"
            dgcolhFundName.DataPropertyName = "fundname"
            dgcolhProjectCode.DataPropertyName = "project_code"
            dgcolhCurrentCeilingBal.DataPropertyName = "currentceilingbalance"
            dgcolhCurrentCeilingBal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentCeilingBal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCurrentCeilingBal.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhExpiryDate.DataPropertyName = "fundexpirydate"
            dgcolhExpiryDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhNotifyAmount.DataPropertyName = "notify_amount"
            dgcolhNotifyAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNotifyAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNotifyAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            objdgcolhfundsourceunkid.DataPropertyName = "fundsourceunkid"

            dgvFundSource.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvFundSource.RowCount.ToString)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddFundSource
            btnEdit.Enabled = User._Object.Privilege._AllowToEditFundSource
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteFundSource

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmFundSources_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
                Call FillCombo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmFundSources_AddEdit
        Try
            If dgvFundSource.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundSource.Focus()
                Exit Sub
            End If

            If frm.displayDialog(CInt(dgvFundSource.SelectedRows(0).Cells(objdgcolhfundsourceunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Try
            If dgvFundSource.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvFundSource.Focus()
                Exit Sub
            End If

            If objFundSource.isUsed(CInt(dgvFundSource.SelectedRows(0).Cells(objdgcolhfundsourceunkid.Index).Value)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot Delete this Fund Source. Reason: This Fund Source is in use."), enMsgBoxStyle.Information)
                dgvFundSource.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to Delete this Fund Source?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objFundSource = New clsFundSource_Master

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objFundSource._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objFundSource._Isvoid = True
                objFundSource._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objFundSource._Voiduserunkid = User._Object._Userunkid

                If objFundSource.Delete(CInt(dgvFundSource.SelectedRows(0).Cells(objdgcolhfundsourceunkid.Index).Value), ConfigParameter._Object._CurrentDateAndTime) Then
                    Call FillList()
                    Call FillCombo()
                Else
                    If objFundSource._Message <> "" Then
                        eZeeMsgBox.Show(objFundSource._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboFundName.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchFund.Click
        Try
            Dim objfrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboFundName.ValueMember
                .DisplayMember = cboFundName.DisplayMember
                .DataSource = CType(cboFundName.DataSource, DataTable)
                .CodeMember = "fundcode"
            End With

            If objfrm.DisplayDialog Then
                cboFundName.SelectedValue = objfrm.SelectedValue
                cboFundName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFund_Click", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblFundName.Text = Language._Object.getCaption(Me.lblFundName.Name, Me.lblFundName.Text)
			Me.dgcolhFundCode.HeaderText = Language._Object.getCaption(Me.dgcolhFundCode.Name, Me.dgcolhFundCode.HeaderText)
			Me.dgcolhFundName.HeaderText = Language._Object.getCaption(Me.dgcolhFundName.Name, Me.dgcolhFundName.HeaderText)
			Me.dgcolhProjectCode.HeaderText = Language._Object.getCaption(Me.dgcolhProjectCode.Name, Me.dgcolhProjectCode.HeaderText)
			Me.dgcolhCurrentCeilingBal.HeaderText = Language._Object.getCaption(Me.dgcolhCurrentCeilingBal.Name, Me.dgcolhCurrentCeilingBal.HeaderText)
			Me.dgcolhExpiryDate.HeaderText = Language._Object.getCaption(Me.dgcolhExpiryDate.Name, Me.dgcolhExpiryDate.HeaderText)
			Me.dgcolhNotifyAmount.HeaderText = Language._Object.getCaption(Me.dgcolhNotifyAmount.Name, Me.dgcolhNotifyAmount.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operation.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to Delete this Fund Source?")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot Delete this Fund Source. Reason: This Fund Source is in use.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class