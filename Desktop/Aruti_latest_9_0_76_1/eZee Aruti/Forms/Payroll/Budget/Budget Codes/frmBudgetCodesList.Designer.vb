﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetCodesList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetCodesList))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvBudgetList = New System.Windows.Forms.DataGridView
        Me.objcolhBudgetCodesUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhBudgetUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBudgetCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBudgetName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPayyearUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPayyear = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhViewById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhViewBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAllocationById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAllocationBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPresentationmodeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPresentationmode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhWhotoIncludeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhWhotoInclude = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSalaryLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSalaryLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbEmployeeExempetion = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        CType(Me.dgvBudgetList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeExempetion.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 497)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(863, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(651, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(548, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(445, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(754, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvBudgetList
        '
        Me.dgvBudgetList.AllowUserToAddRows = False
        Me.dgvBudgetList.AllowUserToDeleteRows = False
        Me.dgvBudgetList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBudgetList.BackgroundColor = System.Drawing.Color.White
        Me.dgvBudgetList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudgetList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhBudgetCodesUnkid, Me.colhPeriod, Me.objcolhBudgetUnkid, Me.colhBudgetCode, Me.colhBudgetName, Me.objcolhPayyearUnkid, Me.colhPayyear, Me.objcolhViewById, Me.colhViewBy, Me.objcolhAllocationById, Me.colhAllocationBy, Me.objcolhPresentationmodeId, Me.colhPresentationmode, Me.objcolhWhotoIncludeId, Me.colhWhotoInclude, Me.objcolhSalaryLevelId, Me.colhSalaryLevel, Me.colhIsDefault, Me.objcolhIsDefault, Me.colhIsApproved, Me.objcolhIsApproved, Me.objcolhPeriodStatusId})
        Me.dgvBudgetList.Location = New System.Drawing.Point(12, 82)
        Me.dgvBudgetList.Name = "dgvBudgetList"
        Me.dgvBudgetList.RowHeadersVisible = False
        Me.dgvBudgetList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBudgetList.Size = New System.Drawing.Size(843, 409)
        Me.dgvBudgetList.TabIndex = 303
        '
        'objcolhBudgetCodesUnkid
        '
        Me.objcolhBudgetCodesUnkid.HeaderText = "BudgetCodesUnkid"
        Me.objcolhBudgetCodesUnkid.Name = "objcolhBudgetCodesUnkid"
        Me.objcolhBudgetCodesUnkid.ReadOnly = True
        Me.objcolhBudgetCodesUnkid.Visible = False
        '
        'colhPeriod
        '
        Me.colhPeriod.HeaderText = "Period"
        Me.colhPeriod.Name = "colhPeriod"
        Me.colhPeriod.ReadOnly = True
        '
        'objcolhBudgetUnkid
        '
        Me.objcolhBudgetUnkid.HeaderText = "BudgetUnkid"
        Me.objcolhBudgetUnkid.Name = "objcolhBudgetUnkid"
        Me.objcolhBudgetUnkid.ReadOnly = True
        Me.objcolhBudgetUnkid.Visible = False
        '
        'colhBudgetCode
        '
        Me.colhBudgetCode.HeaderText = "Budget Code"
        Me.colhBudgetCode.Name = "colhBudgetCode"
        Me.colhBudgetCode.ReadOnly = True
        Me.colhBudgetCode.Width = 70
        '
        'colhBudgetName
        '
        Me.colhBudgetName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhBudgetName.HeaderText = "Budget Name"
        Me.colhBudgetName.Name = "colhBudgetName"
        Me.colhBudgetName.ReadOnly = True
        '
        'objcolhPayyearUnkid
        '
        Me.objcolhPayyearUnkid.HeaderText = "PayyearUnkid"
        Me.objcolhPayyearUnkid.Name = "objcolhPayyearUnkid"
        Me.objcolhPayyearUnkid.ReadOnly = True
        Me.objcolhPayyearUnkid.Visible = False
        '
        'colhPayyear
        '
        Me.colhPayyear.HeaderText = "Pay Year"
        Me.colhPayyear.Name = "colhPayyear"
        Me.colhPayyear.ReadOnly = True
        '
        'objcolhViewById
        '
        Me.objcolhViewById.HeaderText = "ViewById"
        Me.objcolhViewById.Name = "objcolhViewById"
        Me.objcolhViewById.ReadOnly = True
        Me.objcolhViewById.Visible = False
        '
        'colhViewBy
        '
        Me.colhViewBy.HeaderText = "View By"
        Me.colhViewBy.Name = "colhViewBy"
        Me.colhViewBy.ReadOnly = True
        '
        'objcolhAllocationById
        '
        Me.objcolhAllocationById.HeaderText = "AllocationById"
        Me.objcolhAllocationById.Name = "objcolhAllocationById"
        Me.objcolhAllocationById.ReadOnly = True
        Me.objcolhAllocationById.Visible = False
        '
        'colhAllocationBy
        '
        Me.colhAllocationBy.HeaderText = "Allocation By"
        Me.colhAllocationBy.Name = "colhAllocationBy"
        Me.colhAllocationBy.ReadOnly = True
        '
        'objcolhPresentationmodeId
        '
        Me.objcolhPresentationmodeId.HeaderText = "PresentationmodeId"
        Me.objcolhPresentationmodeId.Name = "objcolhPresentationmodeId"
        Me.objcolhPresentationmodeId.ReadOnly = True
        Me.objcolhPresentationmodeId.Visible = False
        '
        'colhPresentationmode
        '
        Me.colhPresentationmode.HeaderText = "Presentation Mode"
        Me.colhPresentationmode.Name = "colhPresentationmode"
        Me.colhPresentationmode.ReadOnly = True
        '
        'objcolhWhotoIncludeId
        '
        Me.objcolhWhotoIncludeId.HeaderText = "WhotoIncludeId"
        Me.objcolhWhotoIncludeId.Name = "objcolhWhotoIncludeId"
        Me.objcolhWhotoIncludeId.ReadOnly = True
        Me.objcolhWhotoIncludeId.Visible = False
        '
        'colhWhotoInclude
        '
        Me.colhWhotoInclude.HeaderText = "Who To Include"
        Me.colhWhotoInclude.Name = "colhWhotoInclude"
        Me.colhWhotoInclude.ReadOnly = True
        Me.colhWhotoInclude.Width = 135
        '
        'objcolhSalaryLevelId
        '
        Me.objcolhSalaryLevelId.HeaderText = "SalaryLevelId"
        Me.objcolhSalaryLevelId.Name = "objcolhSalaryLevelId"
        Me.objcolhSalaryLevelId.ReadOnly = True
        Me.objcolhSalaryLevelId.Visible = False
        '
        'colhSalaryLevel
        '
        Me.colhSalaryLevel.HeaderText = "Salary Level"
        Me.colhSalaryLevel.Name = "colhSalaryLevel"
        Me.colhSalaryLevel.ReadOnly = True
        Me.colhSalaryLevel.Visible = False
        '
        'colhIsDefault
        '
        Me.colhIsDefault.HeaderText = "Is Default"
        Me.colhIsDefault.Name = "colhIsDefault"
        Me.colhIsDefault.ReadOnly = True
        Me.colhIsDefault.Visible = False
        Me.colhIsDefault.Width = 60
        '
        'objcolhIsDefault
        '
        Me.objcolhIsDefault.HeaderText = "IsDefault"
        Me.objcolhIsDefault.Name = "objcolhIsDefault"
        Me.objcolhIsDefault.ReadOnly = True
        Me.objcolhIsDefault.Visible = False
        '
        'colhIsApproved
        '
        Me.colhIsApproved.HeaderText = "Approved"
        Me.colhIsApproved.Name = "colhIsApproved"
        Me.colhIsApproved.ReadOnly = True
        Me.colhIsApproved.Visible = False
        Me.colhIsApproved.Width = 60
        '
        'objcolhIsApproved
        '
        Me.objcolhIsApproved.HeaderText = "Approved"
        Me.objcolhIsApproved.Name = "objcolhIsApproved"
        Me.objcolhIsApproved.ReadOnly = True
        Me.objcolhIsApproved.Visible = False
        '
        'objcolhPeriodStatusId
        '
        Me.objcolhPeriodStatusId.HeaderText = "period status"
        Me.objcolhPeriodStatusId.Name = "objcolhPeriodStatusId"
        Me.objcolhPeriodStatusId.ReadOnly = True
        Me.objcolhPeriodStatusId.Visible = False
        '
        'gbEmployeeExempetion
        '
        Me.gbEmployeeExempetion.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeExempetion.Checked = False
        Me.gbEmployeeExempetion.CollapseAllExceptThis = False
        Me.gbEmployeeExempetion.CollapsedHoverImage = Nothing
        Me.gbEmployeeExempetion.CollapsedNormalImage = Nothing
        Me.gbEmployeeExempetion.CollapsedPressedImage = Nothing
        Me.gbEmployeeExempetion.CollapseOnLoad = False
        Me.gbEmployeeExempetion.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeExempetion.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeExempetion.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeExempetion.Controls.Add(Me.lblPeriod)
        Me.gbEmployeeExempetion.ExpandedHoverImage = Nothing
        Me.gbEmployeeExempetion.ExpandedNormalImage = Nothing
        Me.gbEmployeeExempetion.ExpandedPressedImage = Nothing
        Me.gbEmployeeExempetion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeExempetion.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeExempetion.HeaderHeight = 25
        Me.gbEmployeeExempetion.HeaderMessage = ""
        Me.gbEmployeeExempetion.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeExempetion.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeExempetion.HeightOnCollapse = 0
        Me.gbEmployeeExempetion.LeftTextSpace = 0
        Me.gbEmployeeExempetion.Location = New System.Drawing.Point(12, 12)
        Me.gbEmployeeExempetion.Name = "gbEmployeeExempetion"
        Me.gbEmployeeExempetion.OpenHeight = 91
        Me.gbEmployeeExempetion.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeExempetion.ShowBorder = True
        Me.gbEmployeeExempetion.ShowCheckBox = False
        Me.gbEmployeeExempetion.ShowCollapseButton = False
        Me.gbEmployeeExempetion.ShowDefaultBorderColor = True
        Me.gbEmployeeExempetion.ShowDownButton = False
        Me.gbEmployeeExempetion.ShowHeader = True
        Me.gbEmployeeExempetion.Size = New System.Drawing.Size(843, 64)
        Me.gbEmployeeExempetion.TabIndex = 304
        Me.gbEmployeeExempetion.Temp = 0
        Me.gbEmployeeExempetion.Text = "Filter Criteria"
        Me.gbEmployeeExempetion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(811, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(788, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboPeriod
        '
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(116, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(192, 21)
        Me.cboPeriod.TabIndex = 0
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(102, 15)
        Me.lblPeriod.TabIndex = 85
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmBudgetCodesList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 552)
        Me.Controls.Add(Me.gbEmployeeExempetion)
        Me.Controls.Add(Me.dgvBudgetList)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetCodesList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Codes List"
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvBudgetList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeExempetion.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvBudgetList As System.Windows.Forms.DataGridView
    Friend WithEvents gbEmployeeExempetion As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objcolhBudgetCodesUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhBudgetUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBudgetCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBudgetName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPayyearUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPayyear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhViewById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhViewBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAllocationById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAllocationBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPresentationmodeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPresentationmode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhWhotoIncludeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhWhotoInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSalaryLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSalaryLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
