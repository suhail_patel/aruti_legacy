﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

Public Class frmBudgetCodes

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBudgetCodes"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBudgetCodesUnkId As Integer = 0
    Private objBudgetCode_Master As clsBudgetcodes_master

    Private mdicFund As New Dictionary(Of Integer, Decimal)
    Private mdicFundCode As New Dictionary(Of Integer, String)
    Private mdicActivity As New Dictionary(Of Integer, Decimal)
    Private mdicActivityCode As New Dictionary(Of Integer, String)
    Private mdtTable As DataTable
    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private mdtDataView As DataView
    'Nilay (22 Nov 2016) -- End
    Private dsAllHeads As DataSet = Nothing
    Private mintViewById As Integer
    Private mintPresentationModeId As Integer
    Private mdtBudgetDate As Date
    Private mstrPeriodIdList As String = ""
    Private mdicHeadMapping As New Dictionary(Of Integer, Integer)
    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
    Private strTranHeadIDList As String
    Private mstrPreviousPeriodDBName As String
    Private mintWhotoincludeid As Integer
    'Sohail (02 Sep 2016) -- End
    'Sohail (31 Jul 2019) -- Start
    'PACT Support Issue # 0002617 - 76.1 - Budget module should not allow to change activity percentage when budget timesheet is applied.
    Private mdtOld As DataTable
    'Sohail (31 Jul 2019) -- End

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private mstrSearch As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrFilteredEmployeeIDs As String = String.Empty
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Nilay (22 Nov 2016) -- End
    Private mstrAnalysis_CodeField As String = "" 'Sohail (01 Mar 2017)
    'Sohail (01 May 2021) -- Start
    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
    Private mblnProcessFailed As Boolean = False
    Private mintTotRecords As Integer = 0
    'Sohail (01 May 2021) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intBudgetCodesunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBudgetCodesUnkId = intBudgetCodesunkid
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboBudget.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objBudget As New clsBudget_MasterNew
        'Sohail (02 Sep 2016) -- Start
        'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (02 Sep 2016) -- End
        Dim dsCombo As DataSet
        Try
            'Sohail (02 Aug 2017) -- Start
            'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
            'dsCombo = objBudget.GetComboList("Budget", False, True)
            If menAction = enAction.EDIT_ONE Then
                dsCombo = objBudget.GetComboList("Budget", False, False)
            Else
            dsCombo = objBudget.GetComboList("Budget", False, True)
            End If
            'Sohail (02 Aug 2017) -- End
            With cboBudget
                .ValueMember = "budgetunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Budget")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
            If menAction = enAction.EDIT_ONE Then
                dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            Else
                'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
                dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
                'Sohail (01 May 2021) -- Start
                'IHI Issue : : Project codes are missing for some employees on editong budget codes.

                'Sohail (01 May 2021) -- End
            End If
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                'Sohail (01 May 2021) -- Start
                'IHI Issue : : Project codes are missing for some employees on editong budget codes.
                If menAction <> enAction.EDIT_ONE Then
                    .SelectedValue = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                Else
                If .Items.Count > 0 Then .SelectedIndex = 0
                End If
                'Sohail (01 May 2021) -- End
            End With
            'Sohail (02 Sep 2016) -- End

            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.GetCondition(False, True, True, False, False)
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = enComparison_Operator.GREATER_THAN
            End With

            Dim objFundProjectCode As New clsFundProjectCode
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'dsCombo = objFundProjectCode.GetComboList("FundProjectCode", True)
            dsCombo = objFundProjectCode.GetComboList("FundProjectCode", True, , True)
            'Sohail (13 Oct 2017) -- End
            With cboProjectCode
                .ValueMember = "fundprojectcodeunkid"
                .DisplayMember = "fundprojectcode"
                .DataSource = dsCombo.Tables("FundProjectCode")
                .SelectedValue = 0
            End With
            objFundProjectCode = Nothing
            'Nilay (22 Nov 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboBudget.SelectedValue = objBudgetCode_Master._Budgetunkid
            cboPeriod.SelectedValue = objBudgetCode_Master._PeriodUnkId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBudgetCode_Master._Budgetunkid = CInt(cboBudget.SelectedValue)
            objBudgetCode_Master._PeriodUnkId = CInt(cboPeriod.SelectedValue)
            objBudgetCode_Master._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub GridSetup()
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran
        Dim objFormulaHeads As New clsBudgetformula_head_tran
        Dim dsList As DataSet
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""
        Dim objActivity As New clsfundactivity_Tran
        Dim dsCombo As DataSet
        Dim dCol As DataGridViewTextBoxColumn
        Dim dColCombo As DataGridViewComboBoxColumn
        'Nilay (22 Nov 2016) -- Start
        'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
        Dim dColChk As DataGridViewCheckBoxColumn
        'Nilay (22 Nov 2016) -- End
        Try

            If CInt(cboBudget.SelectedValue) <= 0 Then Exit Try

            Cursor.Current = Cursors.WaitCursor
            mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(CInt(cboBudget.SelectedValue))
            mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(CInt(cboBudget.SelectedValue))
            mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(CInt(cboBudget.SelectedValue))

            objBudget._Budgetunkid = CInt(cboBudget.SelectedValue)
            mintViewIdx = objBudget._Allocationbyid
            mintViewById = objBudget._Viewbyid
            mintPresentationModeId = objBudget._Presentationmodeid
            mdtBudgetDate = objBudget._Budget_date.Date
            mstrPreviousPeriodDBName = objPeriod.GetDatabaseName(objBudget._PreviousPeriodyearunkid, Company._Object._Companyunkid)
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
            mintWhotoincludeid = objBudget._Whotoincludeid
            'Sohail (02 Sep 2016) -- End

            'Sohail (01 Oct 2016) -- Start
            'Enhancement - 63.1 - Don't allow to approve budget if total percentage is not 100
            'If CInt(mintViewById) = enBudgetViewBy.Allocation Then
            '    Dim frm As New frmViewAnalysis
            '    frm.displayDialog(, mintViewIdx, mstrAllocationTranUnkIDs, True)
            '    mstrStringIds = frm._ReportBy_Ids
            '    mstrStringName = frm._ReportBy_Name
            '    mintViewIdx = frm._ViewIndex
            '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids
            '    If mintViewIdx = 0 Then mintViewIdx = -1

            '    mstrAnalysis_Fields = frm._Analysis_Fields
            '    mstrAnalysis_Join = frm._Analysis_Join
            '    mstrAnalysis_TableName = frm._Analysis_TableName
            '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            '    mstrReport_GroupName = frm._Report_GroupName
            '    frm = Nothing

            'ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
            '    mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(CInt(cboBudget.SelectedValue))
            '    Dim frm As New frmEmpSelection
            '    frm._EmployeeAsOnStartDate = mdtBudgetDate
            '    frm._EmployeeAsOnEndDate = mdtBudgetDate
            '    frm.displayDialog(mstrEmployeeIDs, , True)
            '    mstrStringIds = frm._ReportBy_Ids
            '    mstrStringName = frm._ReportBy_Name
            '    mintViewIdx = frm._ViewIndex
            '    mstrAllocationTranUnkIDs = frm._ReportBy_Ids

            '    mstrAnalysis_Fields = frm._Analysis_Fields
            '    mstrAnalysis_Join = frm._Analysis_Join
            '    mstrAnalysis_TableName = frm._Analysis_TableName
            '    mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            '    mstrReport_GroupName = frm._Report_GroupName
            '    frm = Nothing
            'End If
            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate.Date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName)
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", mintViewIdx, mstrAllocationTranUnkIDs, mdtBudgetDate.Date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'Sohail (01 Mar 2017) -- End
                If mintViewIdx = 0 Then mintViewIdx = -1
            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(CInt(cboBudget.SelectedValue))
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName)
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)
                'Sohail (01 Mar 2017) -- End
                mintViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If
            'Sohail (01 Oct 2016) -- End


            With dgvBudget
                .DataSource = Nothing
                .AutoGenerateColumns = False
                .IgnoreFirstColumn = True
                .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing
                If .Columns.Count <= 0 Then
                    .ColumnHeadersHeight = .ColumnHeadersHeight * 2
                End If
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter

                If Not .DataSource Is Nothing Then .DataSource = Nothing
                .Columns.Clear()
                .Rows.Clear()

                'dColChk = New DataGridViewCheckBoxColumn
                'dColChk.Name = "colhIsChecked"
                'dColChk.HeaderText = ""
                'dColChk.SortMode = DataGridViewColumnSortMode.NotSortable
                'dColChk.Width = 25
                'dColChk.Frozen = True
                'dColChk.Visible = True
                'dColChk.ReadOnly = False
                'dColChk.DataPropertyName = "IsChecked"
                '.Columns.Add(dColChk)

                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                dColChk = New DataGridViewCheckBoxColumn
                dColChk.Name = "colhIsChecked"
                dColChk.HeaderText = ""
                dColChk.SortMode = DataGridViewColumnSortMode.NotSortable
                dColChk.Width = 25
                dColChk.Frozen = True
                dColChk.Visible = True
                dColChk.ReadOnly = False
                dColChk.DataPropertyName = "IsChecked"
                .Columns.Add(dColChk)
                'Nilay (22 Nov 2016) -- End

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhSrNo"
                dCol.HeaderText = "Sr. No."
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 30
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DataPropertyName = "ROWNO"
                .Columns.Add(dCol)


                'dCol = New DataGridViewTextBoxColumn
                'dCol.Name = "colhCollaps"
                'dCol.HeaderText = ""
                'dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                'dCol.Width = 30
                'dCol.Frozen = True
                'dCol.Visible = True
                'dCol.ReadOnly = True
                'dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                'dCol.DataPropertyName = "Collapse"
                '.Columns.Add(dCol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocationID"
                dCol.HeaderText = mstrReport_GroupName.Replace(" :", "")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "Id"
                .Columns.Add(dCol)


                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocation"
                dCol.HeaderText = mstrReport_GroupName.Replace(" :", "")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 180
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = "GName"
                .Columns.Add(dCol)


                If CInt(mintViewById) = enBudgetViewBy.Employee Then

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhEmpJobUnkID"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Job Title")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = True
                    dCol.Visible = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "empjobunkid"
                    .Columns.Add(dCol)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhEmpJob"
                    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Job Title")
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 120
                    dCol.Frozen = True
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = "EmpJobTitle"
                    .Columns.Add(dCol)

                End If

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhGrossSalary"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 2, "Gross Salary Budget")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 150
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "budgetamount"
                dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                .Columns.Add(dCol)

                'If CInt(mintPresentationModeId) = enBudgetPresentation.TransactionWise Then

                '    'Adding DataGrid Columns
                '    dCol = New DataGridViewTextBoxColumn
                '    dCol.Name = "colhTranHeadID"
                '    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Transaction Head")
                '    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '    dCol.Width = 0
                '    dCol.Visible = False
                '    dCol.Frozen = True
                '    dCol.ReadOnly = True
                '    dCol.DataPropertyName = "tranheadunkid"
                '    .Columns.Add(dCol)


                '    'Adding DataGrid Columns
                '    dCol = New DataGridViewTextBoxColumn
                '    dCol.Name = "colhTranHead"
                '    dCol.HeaderText = Language.getMessage(mstrModuleName, 1, "Transaction Head")
                '    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '    dCol.Width = 150
                '    dCol.Visible = True
                '    dCol.Frozen = True
                '    dCol.ReadOnly = True
                '    dCol.DataPropertyName = "trnheadname"
                '    .Columns.Add(dCol)


                'Else

                '    For i As Integer = 1 To 2

                '        'Adding DataGrid Columns
                '        dCol = New DataGridViewTextBoxColumn
                '        dCol.Name = "colhTranHeadID" & "_|" & i
                '        If i = 1 Then
                '            dCol.HeaderText = Language.getMessage(mstrModuleName, 19, "Basic Salary")
                '        Else
                '            dCol.HeaderText = Language.getMessage(mstrModuleName, 2, "Other Payroll Cost")
                '        End If
                '        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '        dCol.Width = 150
                '        dCol.Frozen = False
                '        dCol.ReadOnly = True
                '        dCol.DataPropertyName = "pramount"
                '        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                '        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '        dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                '        dCol.DataPropertyName = "_|" & i
                '        .Columns.Add(dCol)


                '    Next

                'End If


                'If CInt(mintPresentationModeId) = enBudgetPresentation.TransactionWise Then

                '    'Adding DataGrid Columns
                '    dCol = New DataGridViewTextBoxColumn
                '    dCol.Name = "colhPayrollAmount"
                '    dCol.HeaderText = "Current Figure"
                '    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '    dCol.Width = 150
                '    dCol.Frozen = False
                '    dCol.ReadOnly = True
                '    dCol.DataPropertyName = "pramount"
                '    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                '    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                '    .Columns.Add(dCol)


                '    'Adding DataGrid Columns
                '    dCol = New DataGridViewTextBoxColumn
                '    dCol.Name = "colhBudgetAmount"
                '    dCol.HeaderText = "New Budget Figure"
                '    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '    dCol.Width = 150
                '    dCol.Frozen = False
                '    dCol.ReadOnly = False
                '    dCol.DataPropertyName = "budgetamount"
                '    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                '    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                '    .Columns.Add(dCol)


                'Else

                '    For i As Integer = 1 To 2

                '        'Adding DataGrid Columns
                '        dCol = New DataGridViewTextBoxColumn
                '        If i = 1 Then
                '            dCol.Name = "colhBudgetSalaryAmount"
                '            dCol.HeaderText = "New Budget Basic Salary Figure"
                '            dCol.DataPropertyName = "budgetsalaryamount"
                '        Else
                '            dCol.Name = "colhBudgetOtherPayrollAmount"
                '            dCol.HeaderText = "New Budget Other Payroll Cost Figure"
                '            dCol.DataPropertyName = "budgetotherpayrollamount"
                '        End If
                '        dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                '        dCol.Width = 130
                '        dCol.Frozen = False
                '        dCol.ReadOnly = False
                '        dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                '        dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                '        dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                '        .Columns.Add(dCol)


                '    Next

                'End If

                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objActivity.GetList("Activity")
                dsList = objActivity.GetList("Activity", , True)
                'Sohail (13 Oct 2017) -- End
                mdicActivity = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("amount"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                mdicActivityCode = (From p In dsList.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                'Sohail (13 Oct 2017) -- Start
                'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                'dsList = objFundProjectCode.GetList("FundProjectCode")
                dsList = objFundProjectCode.GetList("FundProjectCode", , True)
                'Sohail (13 Oct 2017) -- End
                mdicFund = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Total = CDec(p.Item("currentceilingbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                mdicFundCode = (From p In dsList.Tables("FundProjectCode") Select New With {.Id = CInt(p.Item("fundprojectcodeunkid")), .Code = p.Item("fundprojectcode").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                For Each dsRow As DataRow In dsList.Tables("FundProjectCode").Rows

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colhFund" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = "Project Code (%)" 'dsRow.Item("project_code").ToString & " (%)"
                    dCol.Tag = dsRow.Item("fundprojectcode").ToString
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 100
                    dCol.Frozen = False
                    dCol.ReadOnly = False
                    dCol.DataPropertyName = "|_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)

                    'Sohail (13 Oct 2017) -- Start
                    'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                    'dsCombo = objActivity.GetComboList("Activity", True, CInt(dsRow.Item("fundprojectcodeunkid")))
                    dsCombo = objActivity.GetComboList("Activity", True, CInt(dsRow.Item("fundprojectcodeunkid")), True)
                    'Sohail (13 Oct 2017) -- End
                    Dim d_row() As DataRow = dsCombo.Tables(0).Select("fundactivityunkid = 0")
                    If d_row.Length > 0 Then
                        d_row(0).Item("activityname") = ""
                        dsCombo.Tables(0).AcceptChanges()
                    End If
                    dColCombo = New DataGridViewComboBoxColumn
                    dColCombo.Name = "colhActivity" & dsRow.Item("fundprojectcodeunkid").ToString
                    dColCombo.HeaderText = "Activity Code"
                    dColCombo.Tag = dsRow.Item("fundprojectcode").ToString
                    dColCombo.SortMode = DataGridViewColumnSortMode.NotSortable
                    dColCombo.Width = 100
                    dColCombo.Frozen = False
                    dColCombo.ReadOnly = False
                    dColCombo.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dColCombo.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dColCombo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    'dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    dColCombo.ValueMember = "fundactivityunkid"
                    'Nilay (22 Nov 2016) -- Start
                    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                    'dColCombo.DisplayMember = "activityname"
                    dColCombo.DisplayMember = "activitycode"
                    'Nilay (22 Nov 2016) -- End
                    dColCombo.DataSource = dsCombo.Tables(0)
                    dColCombo.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dColCombo.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    .Columns.Add(dColCombo)

                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "objcolhActivity" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderText = "Activity Code"
                    dCol.Tag = dsRow.Item("fundprojectcode").ToString
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 0
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.Visible = False
                    dCol.DataPropertyName = "||_" & dsRow.Item("fundprojectcodeunkid").ToString
                    dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomLeft
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    'dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                    .Columns.Add(dCol)
                Next

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "objisgroup"
                dCol.HeaderText = "isgroup"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 50
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.Visible = False
                dCol.DataPropertyName = dCol.Name
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns.Add(dCol)

                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTotal"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 3, "Total")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = "colhTotal"
                dCol.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.Format = GUI.fmtCurrency
                .Columns.Add(dCol)

                'mstrPeriodIdList = String.Join(",", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Order By p.SubItems(colh_Period_EndDate.Index).Tag.ToString Ascending Select (p.SubItems(colh_Period_PayPeriod.Index).Tag.ToString)).ToArray)

                'Get Heads List
                strTranHeadIDList = String.Join(",", (From p In mdicHeadMapping Select (p.Key.ToString)).ToArray)
                dsAllHeads = Nothing

                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'dsList = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", CInt(cboPeriod.SelectedValue), True, "", dsAllHeads, True)
                'Sohail (24 Mar 2017) -- Start
                'Enhancement - 65.1 - Active employee as on period start and end date instead of budget date.
                'dsList = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", CInt(cboPeriod.SelectedValue), True, "", dsAllHeads, True, mstrAnalysis_CodeField)
                dsList = objBudgetCodes.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, objBudget._Allocationbyid, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, CInt(objBudget._Whotoincludeid), "Budget", CInt(cboPeriod.SelectedValue), True, "", dsAllHeads, True, mstrAnalysis_CodeField)
                'Sohail (24 Mar 2017) -- End
                'Sohail (01 Mar 2017) -- End

                Dim strExpression As String = ""
                For Each pair In mdicFund
                    strExpression &= " + [|_" & pair.Key.ToString & "]"
                Next
                mdtTable = dsList.Tables(0).Clone
                Dim dtCol As New DataColumn("autono", System.Type.GetType("System.Int32"))
                dtCol.AllowDBNull = False
                dtCol.AutoIncrement = True
                dtCol.AutoIncrementSeed = 1
                dtCol.AutoIncrementStep = 1
                dtCol.Unique = True
                mdtTable.Columns.Add(dtCol)
                mdtTable.Columns("autono").SetOrdinal(0)
                mdtTable.Load(dsList.Tables(0).CreateDataReader)
                If strExpression.Trim <> "" Then
                    mdtTable.Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
                End If
                'mdtTable = dsList.Tables(0)
                

                '    Dim dicHeadMapping As Dictionary(Of Integer, Integer) = (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("mappedformulaheadunkid")) > 0) Select New With {.headid = CInt(p.Item("tranheadunkid")), .mappedheadid = CInt(p.Item("mappedformulaheadunkid"))}).ToDictionary(Function(x) x.headid, Function(y) y.mappedheadid)
                '    Dim dicFormulaMapping As Dictionary(Of Integer, String) = (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("mappedformulaheadunkid")) > 0) Select New With {.headid = CInt(p.Item("tranheadunkid")), .formulaid = p.Item("formulaid").ToString}).ToDictionary(Function(x) x.headid, Function(y) y.formulaid)

                '    If menAction <> enAction.EDIT_ONE OrElse chkOverWrite.Checked = True Then
                '        Dim lstRow As List(Of DataRow) = Nothing

                '        lstRow = (From p In dsAllHeads.Tables(0) Where (dicFormulaMapping.Keys.ToArray.Contains(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

                '        For Each dsRow As DataRow In lstRow
                '            objDic.Clear()

                '            If dicFormulaMapping.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = True Then

                '                If dicHeadMapping.ContainsKey(CInt(dsRow.Item("tranheadunkid"))) = True Then
                '                    Dim ds As DataSet = objFormulaHeads.GetList("Heads", CInt(dicHeadMapping.Item(CInt(dsRow.Item("tranheadunkid")))), ConfigParameter._Object._CurrentDateAndTime)

                '                    For Each dtRow As DataRow In ds.Tables("Heads").Rows
                '                        If objDic.ContainsKey(CInt(dtRow.Item("tranheadunkid"))) = False Then
                '                            If GetValue(CInt(dtRow.Item("tranheadunkid")), CInt(dsRow.Item("Id")), CInt(dsRow.Item("AllocationtranunkId")), dsAllHeads.Tables(0)) = True Then

                '                            End If
                '                        End If

                '                    Next

                '                    Dim strFormula As String = getExpression(dicFormulaMapping.Item(CInt(dsRow.Item("tranheadunkid"))), CInt(dsRow.Item("Id")), CInt(dsRow.Item("AllocationtranunkId")), dsAllHeads.Tables(0))

                '                    Dim c As New clsFomulaEvaluate

                '                    Dim decAmt As Decimal = c.Eval(strFormula)

                '                    dsRow.Item("budgetamount") = decAmt
                '                    dsRow.AcceptChanges()
                '                End If

                '            End If
                '        Next
                '        dsAllHeads.Tables(0).AcceptChanges()

                '        lstRow = (From p In dsAllHeads.Tables(0) Where (dicFormulaMapping.Keys.ToArray.Contains(CInt(p.Item("tranheadunkid"))) = False) Select (p)).ToList
                '        For Each dsRow As DataRow In lstRow
                '            dsRow.Item("budgetamount") = 0
                '            dsRow.AcceptChanges()
                '        Next
                '        dsAllHeads.Tables(0).AcceptChanges()
                '    End If

                If mintPresentationModeId = enBudgetPresentation.TransactionWise Then
                    'mdtTable = dsAllHeads.Tables(0)
                Else
                    '        If menAction <> enAction.EDIT_ONE OrElse chkOverWrite.Checked = True Then
                    '            For Each dtRow As DataRow In dsAllHeads.Tables(0).Rows 'Select("allocationtranunkid > 0 ")
                    '                Dim strId As String = dtRow.Item("Id").ToString
                    '                Dim strAllocationtranunkId As String = dtRow.Item("allocationtranunkid").ToString
                    '                dtRow.Item("budgetsalaryamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = True AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                    '                dtRow.Item("budgetotherpayrollamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = False AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                    '            Next
                    '            dsAllHeads.Tables(0).AcceptChanges()

                    '            For Each dtRow As DataRow In mdtTable.Rows 'Select("allocationtranunkid > 0 ")
                    '                Dim strId As String = dtRow.Item("Id").ToString
                    '                Dim strAllocationtranunkId As String = dtRow.Item("allocationtranunkid").ToString
                    '                dtRow.Item("budgetsalaryamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = True AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                    '                dtRow.Item("budgetotherpayrollamount") = (From p In dsAllHeads.Tables(0) Where (CBool(p.Item("IsSalary")) = False AndAlso p.Item("Id").ToString = strId AndAlso p.Item("allocationtranunkid").ToString = strAllocationtranunkId) Select (CDec(p.Item("budgetamount")))).Sum()
                    '            Next
                    '            mdtTable.AcceptChanges()
                    '        End If
                End If


                Dim decTotal As Decimal = 0
                Dim decSalaryTotal As Decimal = 0
                Dim decOtherPayrollTotal As Decimal = 0
                'If mintPresentationModeId = enBudgetPresentation.TransactionWise Then
                decTotal = (From p In mdtTable Select (CDec(p.Item("budgetamount")))).DefaultIfEmpty.Sum
                'Else
                'decSalaryTotal = (From p In mdtTable Select (CDec(p.Item("_|1")))).DefaultIfEmpty.Sum
                'decOtherPayrollTotal = (From p In mdtTable Select (CDec(p.Item("_|2")))).DefaultIfEmpty.Sum
                'End If

                'Dim dr As DataRow = mdtTable.NewRow
                'dr.Item("budgetunkid") = 0
                'dr.Item("budget_code") = ""
                'dr.Item("budget_name") = ""
                'dr.Item("payyearunkid") = 0
                'dr.Item("viewbyid") = 0
                'dr.Item("allocationbyid") = 0
                'dr.Item("presentationmodeid") = 0
                'dr.Item("whotoincludeid") = 0
                'dr.Item("salarylevelid") = 0
                'dr.Item("allocationtranunkid") = 0
                'dr.Item("budgetamount") = CDec(Format(decTotal, GUI.fmtCurrency))
                'dr.Item("Id") = -1
                'dr.Item("GName") = ""
                'dr.Item("IsChecked") = False

                'mdtTable.Rows.Add(dr)

                Call UpdateBudgetTotal()

                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'dgvBudget.DataSource = mdtTable
                objchkSelectAll_Budget.Visible = True
                mdtDataView = mdtTable.DefaultView
                dgvBudget.DataSource = mdtDataView
                'Nilay (22 Nov 2016) -- End
            End With

            'If mdtTable.Rows.Count > 1 Then
            '    gbBudget.Enabled = False
            'End If

            'Sohail (31 Jul 2019) -- Start
            'PACT Support Issue # 0002617 - 76.1 - Budget module should not allow to change activity percentage when budget timesheet is applied.
            mdtOld = mdtTable.Copy
            'Sohail (31 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GridSetup", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub cbo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If cbo.SelectedValue IsNot Nothing AndAlso TypeOf cbo.SelectedValue Is System.Int32 Then

                Dim intFundId As Integer = CInt(dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).Name.Substring(12).ToString)

                dgvBudget.CurrentRow.Cells(dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).Name).Value = CInt(cbo.SelectedValue)

                If CInt(cbo.SelectedValue) <= 0 Then
                    cbo.Text = ""
                    cbo.Tag = ""
                    'RemoveHandler dgvBudget.RowValidating, AddressOf dgvBudget_RowValidating 'Sohail (07 Apr 2017)
                    CType(dgvBudget.CurrentRow.DataBoundItem, DataRowView).Item("||_" & intFundId.ToString) = 0
                    mdtTable.AcceptChanges()
                    'Nilay (22 Nov 2016) -- Start
                    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                    mdtDataView = mdtTable.DefaultView
                    'Nilay (22 Nov 2016) -- End
                    dgvBudget.CurrentCell = dgvBudget.Rows(dgvBudget.CurrentRow.Index).Cells(dgvBudget.Columns("colhActivity" & intFundId.ToString).Index)
                    'AddHandler dgvBudget.RowValidating, AddressOf dgvBudget_RowValidating 'Sohail (07 Apr 2017)
                    'blnIsDelKey = True
                Else
                    'blnIsDelKey = False
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cbo_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub UpdateBudgetTotal()
        Dim objEmp As New clsEmployee_Master
        Try
            Dim decTotal As Decimal = 0
            Dim decSalaryTotal As Decimal = 0
            Dim decOtherPayrollTotal As Decimal = 0

            'If mintPresentationModeId = enBudgetPresentation.TransactionWise Then
            'decTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetamount")))).DefaultIfEmpty.Sum
            'mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetamount") = CDec(Format(decTotal, GUI.fmtCurrency))

            'objlblBudgetTotal.Text = Format(decTotal, GUI.fmtCurrency)
            'objlblOtherCostTotal.Text = Format(0, GUI.fmtCurrency)
            'objlblGrandTotal.Text = Format(decTotal, GUI.fmtCurrency)
            'Else
            'decSalaryTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetsalaryamount")))).DefaultIfEmpty.Sum
            'decOtherPayrollTotal = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select (CDec(p.Item("budgetotherpayrollamount")))).DefaultIfEmpty.Sum

            'mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetsalaryamount") = CDec(Format(decSalaryTotal, GUI.fmtCurrency))
            'mdtTable.Rows(mdtTable.Rows.Count - 1).Item("budgetotherpayrollamount") = CDec(Format(decOtherPayrollTotal, GUI.fmtCurrency))

            'objlblBudgetTotal.Text = Format(decSalaryTotal, GUI.fmtCurrency)
            'objlblOtherCostTotal.Text = Format(decOtherPayrollTotal, GUI.fmtCurrency)
            'objlblGrandTotal.Text = Format(decSalaryTotal + decOtherPayrollTotal, GUI.fmtCurrency)
            'End If

            'Dim intFundId As Integer
            'Dim decFundTotal As Decimal = 0
            'For Each pair In mdicFund
            '    intFundId = pair.Key
            '    decFundTotal = 0

            '    Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
            '    If lstRow.Count > 0 Then
            '        For Each dRow As DataRow In lstRow

            '            If mintPresentationModeId = enBudgetPresentation.TransactionWise Then
            '                decFundTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
            '            Else
            '                decFundTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
            '            End If
            '        Next
            '        mdtTable.Rows(mdtTable.Rows.Count - 1).Item("|_" & intFundId.ToString) = CDec(Format(decFundTotal, GUI.fmtCurrency))
            '    End If
            'Next

            'mdtTable.AcceptChanges()

            'Dim strFilter As String = ""
            'Dim decTotEmp As Decimal = objEmp.GetEmployeeCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False)
            'Dim decNewEmp As Decimal = (From p In mdtTable Where (CInt(p.Item("allocationtranunkid")) < 0) Select New With {Key .name = p.Item("GName").ToString}).Distinct().Count()
            'decTotEmp += decNewEmp
            'Dim decSelectedEmp As Decimal = decTotEmp
            'If mintViewById = enBudgetViewBy.Allocation Then
            '    If mstrAnalysis_Join.Trim <> "" Then
            '        strFilter = mstrAnalysis_Join.Substring(mstrAnalysis_Join.LastIndexOf("AND") + 3, mstrAnalysis_Join.Length - mstrAnalysis_Join.LastIndexOf("AND") - 3)
            '        Dim dsSelectedEmp As DataSet = objEmp.GetListForDynamicField(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "Emp", , , strFilter)
            '        decSelectedEmp = dsSelectedEmp.Tables("Emp").Rows.Count
            '    End If
            'Else
            '    decSelectedEmp = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1) Select New With {Key .name = p.Item("GName").ToString}).Distinct().Count()
            'End If

            'objlblTotalEmplyees.Text = CInt(decSelectedEmp).ToString & "/" & CInt(decTotEmp).ToString

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateBudgetTotal", mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'btnSave.Enabled = User._Object.Privilege._AllowToSaveBudgetCodes

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetCodes_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudgetCode_Master = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetCodes_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBudgetCodes_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudgetcodes_master.SetMessages()
            clsBudgetcodes_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudgetcodes_master, clsBudgetcodes_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmBudgetCodes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBudgetCode_Master = New clsBudgetcodes_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call SetVisibility()
            btnClose.CausesValidation = False

            Call FillCombo()

            If cboBudget.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, There is no Default budget. Please set Default budget from Budget List screen"), enMsgBoxStyle.Information)
                btnSave.Visible = False
            End If

            If menAction = enAction.EDIT_ONE Then
                objBudgetCode_Master._Budgetcodesunkid = mintBudgetCodesUnkId
                cboPeriod.Enabled = False
                'Sohail (02 Aug 2017) -- Start
                'Enhancement - 69.1 - New Report 'Monthly Employee Budget Variance Analysis Report'.
                cboBudget.Enabled = False
                'Sohail (02 Aug 2017) -- End
                lnkCopyPreviousPeriod.Enabled = False

                Call GetValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetCodes_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboBudget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBudget.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            Dim objPeriod As New clscommom_period_Tran
            Dim objBudget As New clsBudget_MasterNew
            If CInt(cboBudget.SelectedValue) > 0 Then
                objBudget._Budgetunkid = CInt(cboBudget.SelectedValue)
                mintViewById = CInt(objBudget._Viewbyid)
                If mintViewById = enBudgetViewBy.Employee Then
                    lnkAllocation.Visible = True
                Else
                    lnkAllocation.Visible = False
                End If
            Else
                lnkAllocation.Visible = False
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
            End If
            'Nilay (22 Nov 2016) -- End

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
            'Call GridSetup()
            If cboBudget.Items.Count > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
            Call GridSetup()
            End If
            'Sohail (02 Sep 2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBudget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private Sub cboProjectCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProjectCode.SelectedIndexChanged
        Try
            Dim objFundActivity As New clsfundactivity_Tran
            'Sohail (13 Oct 2017) -- Start
            'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
            'Dim dsList As DataSet = objFundActivity.GetComboList("Activity", True, CInt(cboProjectCode.SelectedValue))
            Dim dsList As DataSet = objFundActivity.GetComboList("Activity", True, CInt(cboProjectCode.SelectedValue), True)
            'Sohail (13 Oct 2017) -- End
            With cboActivityCode
                .ValueMember = "fundactivityunkid"
                .DisplayMember = "activitycode"
                .DataSource = dsList.Tables("Activity")
                .SelectedValue = 0
            End With
            objFundActivity = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboProjectCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (22 Nov 2016) -- End


#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        'Dim dsList As DataSet
        'Dim dtViewHead As DataView
        'Dim blnFlag As Boolean
        Try
            'Nilay (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            'Dim lst_Row As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            Dim lst_Row As List(Of DataRow) = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("colhTotal")) <> 100) Select (p)).ToList
            'Nilay (22 Nov 2016) -- End
            If lst_Row.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Total Percentage should be 100 for all transactions."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            Else
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                If objPeriod._Statusid = enStatusType.Close Then
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot change any transactions. Reason : Selected period is already closed."), enMsgBoxStyle.Information)
                    'Exit Try
                End If
            End If

            'Sohail (31 Jul 2019) -- Start
            'PACT Support Issue # 0002617 - 76.1 - Budget module should not allow to change activity percentage when budget timesheet is applied.
            If mintViewById = enBudgetViewBy.Employee AndAlso menAction = enAction.EDIT_ONE Then
                Dim dtNew As DataTable = mdtDataView.Table.Copy
                If mdtOld.Columns.Contains("IsChecked") = True Then mdtOld.Columns.Remove("IsChecked")
                If dtNew.Columns.Contains("IsChecked") = True Then dtNew.Columns.Remove("IsChecked")
                Dim drMerged() As DataRow = dtNew.AsEnumerable().Except(mdtOld.AsEnumerable, DataRowComparer.Default).ToArray
                Dim dtMerged As DataTable = Nothing
                If drMerged.Length > 0 Then
                    dtMerged = drMerged.CopyToDataTable

                    Dim strEmpIDs As String = String.Join(",", (From p In dtMerged Where (CInt(p.Item("Id")) <> -1) Select (p.Item("allocationtranunkid").ToString)).ToArray)

                    Dim objEmpTS As New clsBudgetEmp_timesheet
                    Dim ds As DataSet = objEmpTS.GetEmployeeTimesheetList(intPeriodID:=CInt(cboPeriod.SelectedValue) _
                                                                          , blnIsVoid:=False _
                                                                          , strFilter:=" ltbemployee_timesheet.employeeunkid IN (" & strEmpIDs & ") " _
                                                                          )

                    If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim dt As DataTable = New DataView(ds.Tables(0)).ToTable(True, "EmployeeName", "employeecode")
                        dt.Columns("EmployeeName").SetOrdinal(0)
                        dt.Columns("employeecode").SetOrdinal(1)
                        dt.Columns("EmployeeName").ColumnName = "Emp. Name"
                        dt.Columns("employeecode").ColumnName = "Emp. Code"
                        Dim objValid As New frmCommonValidationList
                        objValid.displayDialog(False, Language.getMessage(mstrModuleName, 21, "Sorry, Budget timesheet is already applied for some of the employees in selected period."), dt)
                        Exit Try
                    End If
                End If
            End If
            'Sohail (31 Jul 2019) -- End

            Dim intFundId As Integer = 0
            For i = 0 To dgvBudget.RowCount - 1

                For Each pair In mdicFund
                    intFundId = pair.Key

                    Dim intActivityId As Integer = CInt(dgvBudget.Rows(i).Cells("colhActivity" & intFundId.ToString).Value)

                    If CDec(dgvBudget.Rows(i).Cells("colhFund" & intFundId.ToString).Value) > 0 AndAlso intActivityId <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Activity. Activity is mandatory information when Percentage is greater than Zero."), enMsgBoxStyle.Information)
                        dgvBudget.ClearSelection()
                        dgvBudget.Rows(i).Cells("colhActivity" & intFundId.ToString).Selected = True
                        'Sohail (13 Oct 2017) -- Start
                        'CCBRT Enhancement - 70.1 - (Ref. Id - 110, 111) - for all Four reports the user to be able to select a range of consecutive periods so as to be able to see the total. This Feature will enable the user to select more than one default budget so as to get a consolidated report.
                        dgvBudget.FirstDisplayedScrollingRowIndex = dgvBudget.Rows(i).Index
                        dgvBudget.FirstDisplayedScrollingColumnIndex = dgvBudget.Columns("colhActivity" & intFundId.ToString).Index - 1
                        'Sohail (13 Oct 2017) -- End
                        Exit Try
                    End If
                Next
            Next

            'Sohail (01 May 2021) -- Start
            'IHI Issue : : Project codes are missing for some employees on editong budget codes.
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgw.RunWorkerAsync()
            'Call SetValue()
            ''Sohail (02 Sep 2016) -- End

            'dsList = objBudgetHeadMapping.GetListByBudgetUnkId("HeadMapping", CInt(cboBudget.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, , False)
            'Dim lstRow As List(Of DataRow) = (From p In dsList.Tables("HeadMapping") Where (mdicHeadMapping.ContainsKey(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

            'For Each dsRow As DataRow In lstRow
            '    dsRow.Item("IsChecked") = True
            'Next
            'dsList.Tables("HeadMapping").AcceptChanges()
            'dtViewHead = New DataView(dsList.Tables("HeadMapping"))

            'Dim dtHead As DataTable = New DataView(dtViewHead.Table, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            ''Nilay (22 Nov 2016) -- Start
            ''Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            ''Dim dtBudget As DataTable = New DataView(mdtTable, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            'Dim dtBudget As DataTable = New DataView(mdtDataView.Table, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            ''Nilay (22 Nov 2016) -- End
            'Dim dtAllHead As DataTable = New DataView(dsAllHeads.Tables(0), "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            ''Nilay (22 Nov 2016) -- Start
            ''Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            ''Dim strFundIDs() As String = (From p In mdtTable.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray
            'Dim strFundIDs() As String = (From p In mdtDataView.Table.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray
            ''Nilay (22 Nov 2016) -- End
            ''If objBudget_Master._Presentationmodeid = enBudgetPresentation.Summary Then

            ''Nilay (22 Nov 2016) -- Start
            ''Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
            ''For Each dtRow As DataRow In mdtTable.Rows
            'For Each dtRow As DataRow In mdtDataView.Table.Rows
            '    'Nilay (22 Nov 2016) -- End
            '    Dim dr() As DataRow = dtAllHead.Select("Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
            '    For Each dRow As DataRow In dr
            '        'dRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount"))
            '        'dRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount"))
            '        For Each col As String In strFundIDs
            '            dRow.Item(col) = CDec(dtRow.Item(col))
            '            dRow.Item(col.Replace("|_", "||_")) = CInt(dtRow.Item(col.Replace("|_", "||_")))
            '        Next
            '    Next
            '    dtAllHead.AcceptChanges()

            '    'dr = dtAllHead.Select("IsSalary = 0 AND Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
            '    'For Each dRow As DataRow In dr
            '    '    dRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount"))
            '    '    dRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount"))
            '    '    For Each col As String In strFundIDs
            '    '        dRow.Item(col) = CDec(dtRow.Item(col))
            '    '    Next
            '    'Next
            '    'dtAllHead.AcceptChanges()
            'Next
            ''End If

            'If menAction = enAction.EDIT_ONE Then
            '    'Sohail (19 Apr 2017) -- Start
            '    'MST Enhancement - 66.1 - applying user access filter on payroll budget.
            '    'blnFlag = objBudgetCode_Master.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
            '    'Sohail (03 May 2017) -- Start
            '    'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
            '    'Dim strBudgettranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgettranunkid").ToString)).Distinct().ToArray)
            '    'blnFlag = objBudgetCode_Master.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, strBudgettranUnkIds)
            '    Dim strBudgetCodestranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgetcodestranunkid").ToString)).Distinct().ToArray)
            '    'Sohail (01 May 2021) -- Start
            '    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
            '    'blnFlag = objBudgetCode_Master.UpdateAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, strBudgetCodestranUnkIds)
            '    blnFlag = objBudgetCode_Master.Save(dtAllHead, ConfigParameter._Object._CurrentDateAndTime, strBudgetCodestranUnkIds)
            '    'Sohail (01 May 2021) -- End
            '    'Sohail (03 May 2017) -- End
            '    'Sohail (19 Apr 2017) -- End
            'Else
            '    blnFlag = objBudgetCode_Master.InsertAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            'If blnFlag = False And objBudgetCode_Master._Message <> "" Then
            '    eZeeMsgBox.Show(objBudgetCode_Master._Message, enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If blnFlag = True Then
            '    mblnCancel = False
            '    'If menAction = enAction.ADD_CONTINUE Then
            '    '    objBudget_Master = New clsBudget_MasterNew
            '    '    Call GetValue()
            '    'Else
            '    'mintBudgetUnkId = objBudget_Master._Budgetunkid
            '    Me.Close()
            '    'End If
            'End If
            'Sohail (01 May 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        End Try
    End Sub

    'Sohail (01 May 2021) -- Start
    'IHI Issue : : Project codes are missing for some employees on editong budget codes.
    Private Sub objbgw_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgw.DoWork
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim dsList As DataSet
        Dim dtViewHead As DataView
        Dim blnFlag As Boolean
        mblnProcessFailed = False
        Try
            Me.ControlBox = False
            EnableControls(False)

            Call SetValue()

            dsList = objBudgetHeadMapping.GetListByBudgetUnkId("HeadMapping", CInt(cboBudget.SelectedValue), ConfigParameter._Object._CurrentDateAndTime, , False)
            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables("HeadMapping") Where (mdicHeadMapping.ContainsKey(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

            For Each dsRow As DataRow In lstRow
                dsRow.Item("IsChecked") = True
            Next
            dsList.Tables("HeadMapping").AcceptChanges()
            dtViewHead = New DataView(dsList.Tables("HeadMapping"))

            Dim dtHead As DataTable = New DataView(dtViewHead.Table, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            Dim dtBudget As DataTable = New DataView(mdtDataView.Table, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            Dim dtAllHead As DataTable = New DataView(dsAllHeads.Tables(0), "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strFundIDs() As String = (From p In mdtDataView.Table.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray
            mintTotRecords = dtAllHead.Rows.Count

            For Each dtRow As DataRow In mdtDataView.Table.Rows
                Dim dr() As DataRow = dtAllHead.Select("Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                For Each dRow As DataRow In dr
                    For Each col As String In strFundIDs
                        dRow.Item(col) = CDec(dtRow.Item(col))
                        dRow.Item(col.Replace("|_", "||_")) = CInt(dtRow.Item(col.Replace("|_", "||_")))
                    Next
                Next
                dtAllHead.AcceptChanges()

                'dr = dtAllHead.Select("IsSalary = 0 AND Id = " & CInt(dtRow.Item("Id")) & " AND allocationtranunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                'For Each dRow As DataRow In dr
                '    dRow.Item("budgetsalaryamount") = CDec(dtRow.Item("budgetsalaryamount"))
                '    dRow.Item("budgetotherpayrollamount") = CDec(dtRow.Item("budgetotherpayrollamount"))
                '    For Each col As String In strFundIDs
                '        dRow.Item(col) = CDec(dtRow.Item(col))
                '    Next
                'Next
                'dtAllHead.AcceptChanges()
            Next
            'End If

            Dim dtNew As DataTable = mdtDataView.Table.Copy
            If mdtOld.Columns.Contains("IsChecked") = True Then mdtOld.Columns.Remove("IsChecked")
            If dtNew.Columns.Contains("IsChecked") = True Then dtNew.Columns.Remove("IsChecked")
            'Dim lstEdit As List(Of DataRow) = (From p In mdtOld.AsEnumerable Join q In dtNew.AsEnumerable On p.Field(Of Integer)("autono") Equals q.Field(Of Integer)("autono") Where (Not p.ItemArray.SequenceEqual(q.ItemArray)) Select (p)).ToList

            If menAction = enAction.EDIT_ONE Then
                Dim strBudgetCodestranUnkIds As String = String.Join(",", (From p In dtAllHead Select (p.Item("budgetcodestranunkid").ToString)).Distinct().ToArray)
                blnFlag = objBudgetCode_Master.Save(dtAllHead, ConfigParameter._Object._CurrentDateAndTime, strBudgetCodestranUnkIds, objbgw)
            Else
                blnFlag = objBudgetCode_Master.InsertAll(dtAllHead, dtHead, mstrPeriodIdList, ConfigParameter._Object._CurrentDateAndTime, objbgw)
            End If

            If blnFlag = False And objBudgetCode_Master._Message <> "" Then
                Throw New Exception(objBudgetCode_Master._Message)
            End If

            'If blnFlag = True Then
            '    mblnCancel = False

            '    Me.Close()
            'End If

        Catch ex As Exception
            mblnProcessFailed = True
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
            DisplayError.Show("-1", ex.Message, "objbgw_DoWork", mstrModuleName)
        Finally
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()
        End Try
    End Sub

    Private Sub objbgw_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgw.ProgressChanged
        Try
            objlblProgress.Text = " [" & e.ProgressPercentage & " / " & mintTotRecords & "] "
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgw_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgw_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgw.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            clsApplicationIdleTimer.LastIdealTime.Start()

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then
            Else
                objlblProgress.Text = ""
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Process completed successfully."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgw_RunWorkerCompleted", mstrModuleName)

        Finally

            Call EnableControls(True)
            Me.ControlBox = True
        End Try

    End Sub

    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
            lst = objFooter.Controls.OfType(Of Control)() '.Where(Function(x) x.Name <> btnStop.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 May 2021) -- End

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If mdtDataView Is Nothing OrElse mdtDataView.Table.Rows.Count <= 0 Then Exit Sub

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please check atleast one either Apply To All OR Apply To Checked."), enMsgBoxStyle.Information)
                radApplyToAll.Focus()
                Exit Sub
            End If

            If chkPercentage.Checked = False AndAlso chkActivityCode.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please check atleast one either Percentage OR Activity Code."), enMsgBoxStyle.Information)
                chkPercentage.Focus()
                Exit Sub
            End If

            If CInt(cboProjectCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select Project Code."), enMsgBoxStyle.Information)
                cboProjectCode.Focus()
                Exit Sub
            End If

            If chkActivityCode.Checked = True AndAlso CInt(cboActivityCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Activity Code."), enMsgBoxStyle.Information)
                cboActivityCode.Focus()
                Exit Sub
            End If

            If chkPercentage.Checked = True AndAlso chkActivityCode.Checked = True Then
                If CInt(cboProjectCode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select Project Code."), enMsgBoxStyle.Information)
                    cboProjectCode.Focus()
                    Exit Sub
                ElseIf CInt(cboActivityCode.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Activity Code."), enMsgBoxStyle.Information)
                    cboActivityCode.Focus()
                    Exit Sub
                End If
            End If

            If (chkPercentage.Checked OrElse chkActivityCode.Checked) AndAlso radApplyToChecked.Checked = True AndAlso _
                mdtDataView.Table.Select("IsChecked=True").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please tick atleast one transaction."), enMsgBoxStyle.Information)
                dgvBudget.Focus()
                Exit Sub
            End If

            If chkPercentage.Checked = True Then
                If CDec(txtPercentage.Decimal) > 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Percentage cannot be greater than 100."), enMsgBoxStyle.Information)
                    txtPercentage.Focus()
                    Exit Sub
                End If
            End If

            Dim lstRow As List(Of DataRow)

            If radApplyToChecked.Checked = True Then
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CBool(p.Item("IsChecked")) = True) Select (p)).ToList
            Else
                lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1) Select (p)).ToList
            End If

            Dim strProjectCodeCol As String = String.Empty
            Dim strActivityCodeCol As String = String.Empty

            If chkPercentage.Checked = True AndAlso chkActivityCode.Checked = True Then
                strProjectCodeCol = "|_" & CStr(cboProjectCode.SelectedValue)
                strActivityCodeCol = "||_" & CStr(cboProjectCode.SelectedValue)
            ElseIf chkPercentage.Checked = True Then
                strProjectCodeCol = "|_" & CStr(cboProjectCode.SelectedValue)
            ElseIf chkActivityCode.Checked = True Then
                strActivityCodeCol = "||_" & CStr(cboProjectCode.SelectedValue)
            End If

            For Each dtRow As DataRow In lstRow
                If strProjectCodeCol.Trim.Length > 0 Then
                    dtRow.Item(strProjectCodeCol) = txtPercentage.Decimal
                End If
                If strActivityCodeCol.Trim.Length > 0 AndAlso CInt(cboActivityCode.SelectedValue) > 0 Then
                    dtRow.Item(strActivityCodeCol) = CInt(cboActivityCode.SelectedValue)
                End If
            Next

            mdtDataView.Table.AcceptChanges()
            Call UpdateBudgetTotal()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If mdtDataView Is Nothing Then Exit Sub 'Sohail (10 Feb 2017)

            mstrSearch = " Id <> -1 "
            'Sohail (22 Nov 2016) -- Start
            'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen.
            'If txtTotalPercentage.Text.Trim <> "" Then
            If txtTotalPercentage.Decimal <> 0 Then
                'Sohail (22 Nov 2016) -- End
                mstrSearch &= "AND colhTotal " & cboCondition.Text & " " & txtTotalPercentage.Decimal & " "
            End If
            If mstrFilteredEmployeeIDs.Trim <> "" Then
                mstrSearch &= "AND allocationtranunkid IN (" & mstrFilteredEmployeeIDs & ") "
            End If

            mdtDataView.RowFilter = mstrSearch

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If mdtDataView Is Nothing Then Exit Sub 'Sohail (10 Feb 2017)

            mstrFilteredEmployeeIDs = ""
            mdtDataView.RowFilter = ""
            txtTotalPercentage.Decimal = 0
            cboCondition.SelectedValue = enComparison_Operator.GREATER_THAN

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (22 Nov 2016) -- End

    'Sohail (01 Mar 2017) -- Start
    'Enhancement - 65.1 - Export and Import option on Budget Codes.
    Private Sub mnuExportPercentageAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportPercentageAllocation.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsDataSet As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As FileInfo
        Dim objActivity As New clsfundactivity_Tran
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            End If
            'If ConfigParameter._Object._ExportDataPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 666, "Please set the Export Data Path From Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'dlgSaveFile.InitialDirectory = ConfigParameter._Object._ExportDataPath
            dlgSaveFile.Filter = "XML files (*.xml)|*.xml|Excel files(*.xlsx)|*.xlsx"
            dlgSaveFile.FilterIndex = 2
            'path = ConfigParameter._Object._ExportDataPath & "\" & Company._Object._Code & "_"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(dlgSaveFile.FileName)
                If ObjFile.Exists = True Then ObjFile.Delete()

                strFilePath = dlgSaveFile.FileName
                'strFilePath = dlgSaveFile.FileName & ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)

                'strFilePath &= ObjFile.Extension

                'Dim dsActivity As DataSet = objActivity.GetComboList("Activity", True)
                'dsActivity.Tables(0).PrimaryKey = New DataColumn("fundactivityunkid")

                Dim dtBudget As DataTable = New DataView(mdtTable, "Id <> -1 AND GName <> '' ", "", DataViewRowState.CurrentRows).ToTable
                Dim dtCol As DataColumn

                dtCol = New DataColumn("period_code", System.Type.GetType("System.String"))
                dtCol.AllowDBNull = False
                dtCol.DefaultValue = CType(cboPeriod.SelectedItem, DataRowView).Item("code").ToString
                dtBudget.Columns.Add(dtCol)

                Dim intColIdx As Integer = -1

                intColIdx += 1
                dtBudget.Columns("period_code").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Period Code"

                intColIdx += 1
                dtBudget.Columns("budget_code").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Budget Code"

                intColIdx += 1
                dtBudget.Columns("budget_name").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Budget Name"

                intColIdx += 1
                dtBudget.Columns("GCode").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Emp / Allocation Code"

                intColIdx += 1
                dtBudget.Columns("GName").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhAllocationID").HeaderText

                intColIdx += 1
                dtBudget.Columns("budgetamount").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Gross Salary Budget"

                Dim intFundId As Integer = 0
                For Each pair In mdicFundCode
                    intFundId = pair.Key

                    'Dim intActivityId As Integer = CInt(dgvBudget.Columns("colhActivity" & intFundId.ToString).Value)

                    intColIdx += 1
                    dtBudget.Columns("|_" & intFundId).SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhFund" & intFundId.ToString).Tag.ToString & " (%)"

                    dtCol = New DataColumn("||__" & intFundId, System.Type.GetType("System.String"))
                    dtCol.AllowDBNull = False
                    dtCol.DefaultValue = ""
                    dtBudget.Columns.Add(dtCol)

                    intColIdx += 1
                    dtBudget.Columns("||__" & intFundId).SetOrdinal(intColIdx)
                    dtBudget.Columns(intColIdx).ColumnName = dgvBudget.Columns("colhFund" & intFundId.ToString).Tag.ToString & " " & dgvBudget.Columns("colhActivity" & intFundId.ToString).HeaderText
                Next

                'Dim strFundIDs() As String = (From p In dtBudget.Columns.Cast(Of DataColumn)() Where (p.ColumnName.StartsWith("|_")) Select (p.ToString)).ToArray

                intColIdx += 1
                dtBudget.Columns("colhTotal").SetOrdinal(intColIdx)
                dtBudget.Columns(intColIdx).ColumnName = "Total (%)"

                For Each dtRow As DataRow In dtBudget.Rows
                    For Each pair In mdicFundCode
                        intFundId = pair.Key

                        If mdicActivityCode.ContainsKey(CInt(dtRow.Item("||_" & intFundId))) = True Then
                            dtRow.Item(mdicFundCode.Item(intFundId).ToString & " " & dgvBudget.Columns("colhActivity" & intFundId.ToString).HeaderText) = mdicActivityCode.Item(CInt(dtRow.Item("||_" & intFundId)))
                        End If
                    Next
                Next

                For i As Integer = intColIdx + 1 To dtBudget.Columns.Count - 1
                    dtBudget.Columns.RemoveAt(intColIdx + 1)
                Next

                dsDataSet.Tables.Clear()
                dsDataSet.Tables.Add(dtBudget)
                If dsDataSet.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no data to Export."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Select Case dlgSaveFile.FilterIndex
                    'Case 1   'CSV
                    '    Dim objCsvExport As New clsExportData
                    '    ObjFile = New FileInfo(strFilePath)
                    '    objCsvExport.ExportToCSV(dsDataSet.Tables(0), strFilePath)
                    Case 1 'XML
                        dsDataSet.WriteXml(strFilePath)
                    Case 2  'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsDataSet)
                        OpenXML_Export(strFilePath, dsDataSet)
                        'S.SANDEEP [12-Jan-2018] -- END
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "File Exported Successfully to Export Data Path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportPercentageAllocation_Click", mstrModuleName)
        Finally
            ObjFile = Nothing
        End Try
    End Sub

    Private Sub mnuImportPercentageAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportPercentageAllocation.Click
        Dim frm As New frmImportBudgetCodes
        Try
            If frm.displayDialog() = True Then
                Call GridSetup()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportPercentageAllocation_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (01 Mar 2017) -- End
#End Region

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
#Region " Link Button's events "
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim frm As New frmAdvanceSearch

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                     "Emp", , , , , , , , , , , , , , , , mstrAdvanceFilter)

                mstrFilteredEmployeeIDs = String.Join(",", dsList.Tables("Emp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (22 Nov 2016) -- End

#Region " DataGridView Events "

    'Sohail (10 Feb 2017) -- Start
    Private Sub dgvBudget_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvBudget.DataBindingComplete
        Try
            If mdtDataView Is Nothing Then Exit Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_DataBindingComplete", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(mdtDataView.ToTable.Select("Id <> -1 ").Count.ToString)
        End Try
    End Sub
    'Sohail (10 Feb 2017) -- End

    Private Sub dgvBudget_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvBudget.EditingControlShowing
        Try
            If dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).Name.ToUpper.StartsWith("COLHACTIVITY") = True Then
                If e.Control IsNot Nothing Then
                    Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                    cbo.DropDownStyle = ComboBoxStyle.DropDown
                    cbo.AutoCompleteMode = AutoCompleteMode.SuggestAppend

                    'RemoveHandler cbo.KeyPress, AddressOf cbo_KeyPress
                    'RemoveHandler cbo.KeyDown, AddressOf cbo_KeyDown
                    RemoveHandler cbo.SelectionChangeCommitted, AddressOf cbo_SelectionChangeCommitted
                    'AddHandler cbo.KeyPress, AddressOf cbo_KeyPress
                    'AddHandler cbo.KeyDown, AddressOf cbo_KeyDown
                    AddHandler cbo.SelectionChangeCommitted, AddressOf cbo_SelectionChangeCommitted
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvBudget.CellValidating
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CInt(dgvBudget.CurrentRow.Cells("colhAllocationId").Value) = -1 Then Exit Sub 'Last row Grand Total Row

            If dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHFUND") = True Then

                If e.FormattedValue.ToString = "" OrElse CDec(e.FormattedValue) < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Percentage should be in between 0 to 100."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Try
                End If

                Dim lstCols As List(Of DataGridViewColumn) = (From p In dgvBudget.Columns.Cast(Of DataGridViewColumn)() Where (p.Name.ToUpper.StartsWith("COLHFUND") AndAlso p.Index <> e.ColumnIndex) Select (p)).ToList

                Dim decTot As Decimal = 0
                For Each dc As DataGridViewColumn In lstCols
                    decTot += CDec(CType(dgvBudget.CurrentRow.DataBoundItem, DataRowView).Item(dc.DataPropertyName))
                Next

                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'If decTot + CDec(e.FormattedValue) > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, All Fund Sources percentage should not be greater than 100."), enMsgBoxStyle.Information)
                '    e.Cancel = True
                '    Exit Try
                'End If
                'Nilay (22 Nov 2016) -- End

                If CDec(e.FormattedValue.ToString) <> CDec(dgvBudget.CurrentCell.Value) Then
                    Dim decOldValue As Decimal = CDec(dgvBudget.CurrentCell.Value)
                    'Nilay (22 Nov 2016) -- Start
                    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                    'Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                    Dim lstRow As List(Of DataRow) = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                    'Nilay (22 Nov 2016) -- End
                    For Each dsRow As DataRow In lstRow
                        dsRow.Item(dgvBudget.Columns(dgvBudget.CurrentCell.ColumnIndex).DataPropertyName) = CDec(e.FormattedValue.ToString)
                    Next
                    'Call UpdateBudgetTotal()

                    Dim intFundId As Integer = CInt(dgvBudget.Columns(e.ColumnIndex).Name.Substring(8).ToString)
                    Dim decTotal As Decimal = 0
                    'For Each pair In mdicFund
                    'intFundId = pair.Key
                    decTotal = 0

                    'Nilay (22 Nov 2016) -- Start
                    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                    'lstRow = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
                    lstRow = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CDec(p.Item("|_" & intFundId.ToString)) > 0) Select (p)).ToList
                    'Nilay (22 Nov 2016) -- End

                    If lstRow.Count > 0 Then
                        For Each dRow As DataRow In lstRow

                            'If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
                            decTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                            'Else
                            'decTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
                            'End If
                        Next

                        If decTotal > CDec(mdicFund.Item(intFundId)) Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "New Budget Figure is exceeding to the Project Code Current Balance.") & vbCrLf & vbCrLf & mdicFundCode.Item(intFundId).ToString & " New Budget Figure : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicFundCode.Item(intFundId).ToString & " Current Balance : " & Format(CDec(mdicFund.Item(intFundId)), GUI.fmtCurrency) & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 9, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                'Nilay (22 Nov 2016) -- Start
                                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                                'Dim lst_Row As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                                Dim lst_Row As List(Of DataRow) = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("ROWNO")) = CInt(dgvBudget.CurrentRow.Cells("colhSrNo").Value)) Select p).ToList
                                'Nilay (22 Nov 2016) -- End


                                For Each dsRow As DataRow In lst_Row
                                    dsRow.Item(dgvBudget.Columns(e.ColumnIndex).DataPropertyName) = decOldValue
                                Next
                                'Call UpdateBudgetTotal()
                    e.Cancel = True
                    Exit Try
                            Else
                                If CDec(e.FormattedValue.ToString) > 0 AndAlso CInt(dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value) <= 0 Then
                                    'dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex + 1)
                                    'If e.RowIndex < dgvBudget.RowCount - 1 Then
                                    '    dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex)
                                    'End If
                                Else
                                    'If e.RowIndex < dgvBudget.RowCount - 1 Then
                                    '    dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
                                    'End If
                                End If
                                'Exit For
                            End If
                        End If
                    End If
                    'Next
                End If

            ElseIf dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHACTIVITY") = True Then
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'If e.FormattedValue.ToString = "" AndAlso CDec(dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex - 1).Value) > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Activity. Activity is mandatory information when Percentage is greater than Zero."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If
                'Nilay (22 Nov 2016) -- End
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellValidating", mstrModuleName)
        End Try
    End Sub

    'Sohail (07 Apr 2017) -- Start
    'Enhancement - 65.1 - Including heads and amount in Export and Import option on Budget Codes.
    'Private Sub dgvBudget_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvBudget.RowValidating
    '    Dim intFundId As Integer
    '    Try
    '        For Each pair In mdicFund
    '            intFundId = pair.Key

    '            Dim intActivityId As Integer = CInt(dgvBudget.CurrentRow.Cells("colhActivity" & intFundId.ToString).Value)

    '            If CDec(dgvBudget.Rows(e.RowIndex).Cells("colhFund" & intFundId.ToString).Value) > 0 AndAlso intActivityId <= 0 Then
    '                'Nilay (22 Nov 2016) -- Start
    '                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    '                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Activity. Activity is mandatory information when Percentage is greater than Zero."), enMsgBoxStyle.Information)
    '                'dgvBudget.CurrentRow.Cells("colhActivity" & intFundId.ToString).Selected = True
    '                'e.Cancel = True
    '                'Exit Try
    '                'Nilay (22 Nov 2016) -- End

    '            ElseIf intActivityId > 0 Then


    '                Dim decTotal As Decimal = 0
    '                decTotal = 0

    '                'Nilay (22 Nov 2016) -- Start
    '                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
    '                'Dim lstRow As List(Of DataRow) = (From p In mdtTable Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("||_" & intFundId.ToString)) > 0) Select (p)).ToList
    '                Dim lstRow As List(Of DataRow) = (From p In mdtDataView.Table Where (CInt(p.Item("Id")) <> -1 AndAlso CInt(p.Item("||_" & intFundId.ToString)) > 0) Select (p)).ToList
    '                'Nilay (22 Nov 2016) -- End

    '                If lstRow.Count > 0 Then
    '                    For Each dRow As DataRow In lstRow

    '                        'If CInt(cboPresentation.SelectedValue) = enBudgetPresentation.TransactionWise Then
    '                        decTotal += CDec(dRow.Item("budgetamount")) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
    '                        'Else
    '                        'decTotal += (CDec(dRow.Item("budgetsalaryamount")) + CDec(dRow.Item("budgetotherpayrollamount"))) * CDec(dRow.Item("|_" & intFundId.ToString)) / 100
    '                        'End If
    '                    Next

    '                    If decTotal > CDec(mdicActivity.Item(intActivityId)) Then
    '                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "New Budget Figure is exceeding to the Activity Current Balance.") & vbCrLf & vbCrLf & mdicActivityCode.Item(intActivityId).ToString & " New Budget Figure : " & Format(decTotal, GUI.fmtCurrency) & vbCrLf & mdicActivityCode.Item(intActivityId).ToString & " Current Balance : " & Format(CDec(mdicActivity.Item(intActivityId)), GUI.fmtCurrency) & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 9, "Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
    '                            'Call UpdateBudgetTotal()
    '                            'e.Cancel = True
    '                            Exit Try
    '                        Else
    '                            'If CDec(e.FormattedValue.ToString) > 0 AndAlso CInt(dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value) <= 0 Then
    '                            '    dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex).Cells(e.ColumnIndex + 1)
    '                            'Else
    '                            If e.RowIndex < dgvBudget.RowCount - 1 Then
    '                                'dgvBudget.CurrentCell = dgvBudget.Rows(e.RowIndex + 1).Cells(e.ColumnIndex)
    '                            End If
    '                            'End If
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '                'Next

    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvBudget_RowValidating", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (07 Apr 2017) -- End

    Private Sub dgvBudget_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvBudget.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If dgvBudget.Columns(e.ColumnIndex).Name.ToUpper.StartsWith("COLHACTIVITY") Then
                SendKeys.Send("{F2}")
            End If
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvBudget.CellPainting
        Try
            If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
                Dim r2 As Rectangle = e.CellBounds
                r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
                r2.Height = CInt(e.CellBounds.Height / 2)
                e.PaintBackground(r2, True)
                e.PaintContent(r2)
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dgvBudget.ColumnWidthChanged
        Try
            Dim rtHeader As Rectangle = Me.dgvBudget.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvBudget.ColumnHeadersHeight / 2)
            Me.dgvBudget.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_ColumnWidthChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvBudget.Paint
        Try
            'Dim monthes As String() = {"January", "February", "March", "April"}
            If mdtTable Is Nothing Then Exit Sub

            Dim j As Integer = dgvBudget.Columns("colhFund" & mdicFund.ElementAt(0).Key.ToString).Index
            While j < dgvBudget.Columns("colhFund" & mdicFund.ElementAt(0).Key.ToString).Index + (mdicFund.Count * 3)
                Dim r1 As Rectangle = Me.dgvBudget.GetCellDisplayRectangle(j, -1, True)
                Dim w2 As Integer = Me.dgvBudget.GetCellDisplayRectangle(j + 1, -1, True).Width
                r1.X += 1
                r1.Y += 1
                r1.Width = r1.Width + w2 - 2
                r1.Height = CInt(r1.Height / 2 - 2)
                e.Graphics.FillRectangle(New SolidBrush(Me.dgvBudget.ColumnHeadersDefaultCellStyle.BackColor), r1)
                Dim format As New StringFormat()
                format.Alignment = StringAlignment.Center
                format.LineAlignment = StringAlignment.Center
                e.Graphics.DrawString(dgvBudget.Columns(j).Tag.ToString, Me.dgvBudget.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvBudget.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
                j += 3
            End While
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_Paint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvBudget.Scroll
        Try
            Dim rtHeader As Rectangle = Me.dgvBudget.DisplayRectangle
            rtHeader.Height = CInt(Me.dgvBudget.ColumnHeadersHeight / 2)
            Me.dgvBudget.Invalidate(rtHeader)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvBudget_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBudget.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBudget_DataError", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Link Lable Events "
    Private Sub lnkCopyPreviousPeriod_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyPreviousPeriod.LinkClicked
        Dim dsList As DataSet
        Try
            If CInt(cboBudget.SelectedValue) <= 0 Then Exit Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select period. Period is mandatory information."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            End If

            dsList = objBudgetCode_Master.GetPreviuosPeriodBudgetCodes("Period", CInt(cboPeriod.SelectedValue))

            If dsList.Tables("Period").Rows.Count > 0 Then
                'Sohail (01 Mar 2017) -- Start
                'Enhancement - 65.1 - Export and Import option on Budget Codes.
                'dsList = objBudgetCode_Master.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mintViewIdx, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, mintWhotoincludeid, "Budget", CInt(dsList.Tables("Period").Rows(0).Item("periodunkid")), True, "", dsAllHeads, True)
                'Sohail (03 May 2017) -- Start
                'Enhancement - 66.1 - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.
                'dsList = objBudgetCode_Master.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtBudgetDate, mdtBudgetDate, ConfigParameter._Object._UserAccessModeSetting, True, False, mintViewIdx, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, mintWhotoincludeid, "Budget", CInt(dsList.Tables("Period").Rows(0).Item("periodunkid")), True, "", dsAllHeads, True, mstrAnalysis_CodeField)
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables("Period").Rows(0).Item("periodunkid"))
                dsList = objBudgetCode_Master.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, mintViewIdx, mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_TableName, mstrAnalysis_OrderBy, mstrPeriodIdList, strTranHeadIDList, CInt(cboBudget.SelectedValue), mstrPreviousPeriodDBName, mintViewById, enBudgetPresentation.TransactionWise, mintWhotoincludeid, "Budget", CInt(dsList.Tables("Period").Rows(0).Item("periodunkid")), True, "", dsAllHeads, True, mstrAnalysis_CodeField)
                'Sohail (03 May 2017) -- End
                'Sohail (01 Mar 2017) -- End

                Dim strExpression As String = ""
                For Each pair In mdicFund
                    strExpression &= " + [|_" & pair.Key.ToString & "]"
                Next
                If strExpression.Trim <> "" Then
                    dsList.Tables(0).Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
                End If
                mdtTable = dsList.Tables(0)

                Call UpdateBudgetTotal()
                'Nilay (22 Nov 2016) -- Start
                'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
                'dgvBudget.DataSource = mdtTable
                mdtDataView = mdtTable.DefaultView
                dgvBudget.DataSource = mdtDataView
                'Nilay (22 Nov 2016) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyPreviousPeriod_LinkClicked", mstrModuleName)
        Finally

        End Try
    End Sub
#End Region

    'Nilay (22 Nov 2016) -- Start
    'Enhancement #24 -  65.1 - Userability and enhancement for budgeting screen and budget code screen
#Region " CheckBox's Events "
    Private Sub objchkSelectAll_Budget_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll_Budget.CheckedChanged
        Try
            For Each dtRow As DataRowView In mdtDataView
                dtRow.Item("IsChecked") = objchkSelectAll_Budget.Checked
            Next
            mdtDataView.Table.AcceptChanges()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_Budget_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkPercentage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPercentage.CheckedChanged
        Try
            If chkActivityCode.Checked Then txtPercentage.Decimal = 0
            txtPercentage.Enabled = chkPercentage.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkPercentage_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkActivityCode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkActivityCode.CheckedChanged
        Try
            If chkPercentage.Checked = False Then
                txtPercentage.Decimal = 0
                txtPercentage.Enabled = Not chkActivityCode.Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkActivityCode_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (22 Nov 2016) -- End





	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbBudget.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBudget.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor
    
            Me.btnApply.GradientBackColor = GUI._ButttonBackColor
            Me.btnApply.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

    
			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbBudget.Text = Language._Object.getCaption(Me.gbBudget.Name, Me.gbBudget.Text)
			Me.dgvBudget.Text = Language._Object.getCaption(Me.dgvBudget.Name, Me.dgvBudget.Text)
			Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lnkCopyPreviousPeriod.Text = Language._Object.getCaption(Me.lnkCopyPreviousPeriod.Name, Me.lnkCopyPreviousPeriod.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblTotalPercentage.Text = Language._Object.getCaption(Me.lblTotalPercentage.Name, Me.lblTotalPercentage.Text)
            Me.lblProjectCode.Text = Language._Object.getCaption(Me.lblProjectCode.Name, Me.lblProjectCode.Text)
            Me.lblActivityCode.Text = Language._Object.getCaption(Me.lblActivityCode.Name, Me.lblActivityCode.Text)
            Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
            Me.radApplyToChecked.Text = Language._Object.getCaption(Me.radApplyToChecked.Name, Me.radApplyToChecked.Text)
            Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.Name, Me.btnApply.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
            Me.chkActivityCode.Text = Language._Object.getCaption(Me.chkActivityCode.Name, Me.chkActivityCode.Text)
            Me.chkPercentage.Text = Language._Object.getCaption(Me.chkPercentage.Name, Me.chkPercentage.Text)
			Me.mnuExportPercentageAllocation.Text = Language._Object.getCaption(Me.mnuExportPercentageAllocation.Name, Me.mnuExportPercentageAllocation.Text)
			Me.mnuImportPercentageAllocation.Text = Language._Object.getCaption(Me.mnuImportPercentageAllocation.Name, Me.mnuImportPercentageAllocation.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Job Title")
			Language.setMessage(mstrModuleName, 2, "Gross Salary Budget")
			Language.setMessage(mstrModuleName, 3, "Total")
			Language.setMessage(mstrModuleName, 4, "Sorry, Total Percentage should be 100 for all transactions.")
			Language.setMessage(mstrModuleName, 5, "Sorry, There is no Default budget. Please set Default budget from Budget List screen")
			Language.setMessage(mstrModuleName, 6, "Sorry, Percentage should be in between 0 to 100.")
			Language.setMessage(mstrModuleName, 7, "Sorry, All Fund Sources percentage should not be greater than 100.")
			Language.setMessage(mstrModuleName, 8, "New Budget Figure is exceeding to the Activity Current Balance.")
			Language.setMessage(mstrModuleName, 9, "Do you want to continue?")
			Language.setMessage(mstrModuleName, 10, "Please select Activity. Activity is mandatory information when Percentage is greater than Zero.")
			Language.setMessage(mstrModuleName, 11, "New Budget Figure is exceeding to the Project Code Current Balance.")
			Language.setMessage(mstrModuleName, 12, "Please select period. Period is mandatory information.")
			Language.setMessage(mstrModuleName, 13, "Sorry, You cannot change any transactions. Reason : Selected period is already closed.")
            Language.setMessage(mstrModuleName, 14, "Please tick atleast one transaction.")
            Language.setMessage(mstrModuleName, 15, "Please check atleast one either Apply To All OR Apply To Checked.")
            Language.setMessage(mstrModuleName, 16, "Please check atleast one either Percentage OR Activity Code.")
            Language.setMessage(mstrModuleName, 17, "Please select Project Code.")
            Language.setMessage(mstrModuleName, 18, "Please select Activity Code.")
            Language.setMessage(mstrModuleName, 19, "Percentage cannot be greater than 100.")
	    Language.setMessage(mstrModuleName, 20, "File Exported Successfully to Export Data Path.")
			Language.setMessage(mstrModuleName, 21, "Sorry, Budget timesheet is already applied for some of the employees in selected period.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class