﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmGenerateTranHead

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGenerateTranHead"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTransactionUnkid As Integer = -1
    Private mdtFormula As DataTable

    Private objTranHead As clsTransactionHead
    Private objFormula As clsTranheadFormulaTran
    Private mdtTable As New DataTable
    Private mdtTaxSlab As DataTable

    'Sohail (23 Jan 2014) -- Start
    'Enhancement - Oman
    Private objFormulaSlabTran As clsTranhead_formula_slab_Tran
    Private mdtFormulaSlab As DataTable
    Private objFormulaCurrency As clsTranheadFormulaCurrencyTran
    Private mdtFormulaCurrency As DataTable
    Private objFormulaLeave As clsTranheadFormulaLeaveTran
    Private mdtFormulaLeave As DataTable
    Private objFormulaMeasurementUnit As clsTranhead_formula_measurement_unit_Tran
    Private mdtFormulaMeasurementUnit As DataTable
    Private objFormulaActivityUnit As clsTranhead_formula_activity_unit_Tran
    Private mdtFormulaActivityUnit As DataTable
    Private objFormulaMeasurementAmount As clsTranhead_formula_measurement_amount_Tran
    Private mdtFormulaMeasurementAmount As DataTable
    Private objFormulaActivityAmount As clsTranhead_formula_activity_amount_Tran
    Private mdtFormulaActivityAmount As DataTable
    Private objFormulaShift As clsTranheadFormulaShiftTran
    Private mdtFormulaShift As DataTable
    Private objFormulaCumulative As clsTranheadFormulaCumulativeTran
    Private mdtFormulaCumulative As DataTable
    'Sohail (23 Jan 2014) -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintTransactionUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintTransactionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            cboTrnHeadType.BackColor = GUI.ColorComp
            cboTypeOf.BackColor = GUI.ColorOptional
            cboCalcType.BackColor = GUI.ColorOptional
            cboFunction.BackColor = GUI.ColorComp
            cboHeadTypeId.BackColor = GUI.ColorComp
            cboTypeOfId.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objTranHead._FormulaDatasource = objFormula._Datasource
            'Sohail (23 Jan 2014) -- Start
            'Enhancement - Oman
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCurrency._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaCurrency._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaCurrencyDatasource = objFormulaCurrency._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaLeave._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaLeave._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaLeaveDatasource = objFormulaLeave._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaMeasurementUnit._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaMeasurementUnit._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaMeasurementUnitDatasource = objFormulaMeasurementUnit._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaActivityUnit._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaActivityUnit._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaActivityUnitDatasource = objFormulaActivityUnit._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaMeasurementAmount._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaMeasurementAmount._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaMeasurementAmountDatasource = objFormulaMeasurementAmount._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaActivityAmount._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaActivityAmount._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaActivityAmountDatasource = objFormulaActivityAmount._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaShift._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaShift._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaShiftDatasource = objFormulaShift._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaCumulative._Formulatranheadunkid = objTranHead._Tranheadunkid
            objFormulaCumulative._Formulatranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            objTranHead._FormulaCumulativeDatasource = objFormulaCumulative._Datasource
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objFormulaSlabTran._Tranheadunkid = objTranHead._Tranheadunkid
            objFormulaSlabTran._Tranheadunkid = objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName)
            'Sohail (21 Aug 2015) -- End
            mdtFormulaSlab = objFormulaSlabTran._Datasource
            mdtFormulaSlab = New DataView(mdtFormulaSlab, "", "end_date DESC", DataViewRowState.CurrentRows).ToTable
            objTranHead._FormulaPeriodWiseSlabDatasource = objFormulaSlabTran._Datasource
            'Sohail (23 Jan 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As New DataSet
        Dim dtTable As DataTable

        Try
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            dtTable = New DataView(dsCombo.Tables("HeadType"), "ID <> 0", "", DataViewRowState.CurrentRows).ToTable

            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            With cboHeadTypeId
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("HeadType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)
            With cboCalcTypeId
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("CalcType")
                If .Items.Count > 0 Then .SelectedValue = enCalcType.AsComputedValue
            End With
            'dsCombo = objTranHead.getComboList("TranHead", True, 0, 0, -1)
            'dtTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            'With cboCalcType
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dtTable
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With

            With cboFunction
                .Items.Clear()
                .Items.Add("Select Function")
                .Items.Add("Plus")
                .Items.Add("Minus")
                .Items.Add("Multiply")
                .Items.Add("Devide")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim strFilter As String
        Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
        Try
            lvTrnHeadList.Items.Clear()

            If CInt(cboTrnHeadType.SelectedValue) <= 0 Then Exit Try

            lvTrnHeadList.SuspendLayout()
            strFilter = "typeof_id <> " & enTypeOf.Salary & " AND calctype_id NOT IN (" & strDefaultHeadCalcType & ") "
            If CInt(cboTypeOf.SelectedValue) > 0 Then
                strFilter &= "AND typeof_id = " & CInt(cboTypeOf.SelectedValue) & " "
            End If
            If CInt(cboCalcType.SelectedValue) > 0 Then
                strFilter &= "AND calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
            End If
            dsList = objTranHead.GetList("TranHead", , CInt(cboTrnHeadType.SelectedValue), True)
            dtTable = New DataView(dsList.Tables("TranHead"), strFilter, "", DataViewRowState.CurrentRows).ToTable

            Dim lvItem As ListViewItem

            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = drRow("tranheadunkid")

                lvItem.SubItems.Add(drRow("trnheadcode").ToString)

                lvItem.SubItems.Add(drRow("trnheadname").ToString)

                lvTrnHeadList.Items.Add(lvItem)
            Next

            If lvTrnHeadList.Items.Count > 23 Then
                colhHeadName.Width = 230 - 18
            Else
                colhHeadName.Width = 230
            End If
            lvTrnHeadList.ResumeLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTrnHeadList.Items
                RemoveHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvTrnHeadList.ItemChecked, AddressOf lvTrnHeadList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            mdtTable.Columns.Clear()
            mdtTable.Rows.Clear()
            mdtTable.Columns.Add("checked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("tranid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("trancode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("tranname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("formula", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("formulaid", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("headtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("headtype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("typeofid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("typeof", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("calctypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("calctype", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        End Try
    End Sub

    Private Function SetData() As Boolean
        Dim objTranHead As New clsTransactionHead
        Dim strHeadCode As String
        Dim strHeadName As String
        Dim strFormula As String = ""
        Dim strFormulaID As String = ""
        Dim dtRow As DataRow
        Try
            Call CreateDataTable()
            If CInt(cboFunction.SelectedIndex) = 1 Then 'Plus
                strFormula = " + "
                strFormulaID = " + "
            ElseIf CInt(cboFunction.SelectedIndex) = 2 Then 'Minus
                strFormula = " - "
                strFormulaID = " - "
            ElseIf CInt(cboFunction.SelectedIndex) = 3 Then 'Multiply
                strFormula = " * "
                strFormulaID = " * "
            ElseIf CInt(cboFunction.SelectedIndex) = 4 Then 'Devide
                strFormula = " / "
                strFormulaID = " / "
            End If
            strFormula += txtFormula.Text
            strFormulaID += txtFormula.Tag.ToString

            For Each lvItem As ListViewItem In lvTrnHeadList.CheckedItems

                strHeadCode = ""
                strHeadName = ""

                '*** set max length of 50 Chars for Code and Name ***
                If radPrefix.Checked = True Then
                    strHeadCode = txtPrefixSuffix.Text.Trim & Strings.Left(lvItem.SubItems(colhHeadCode.Index).Text, (50 - txtPrefixSuffix.Text.Trim.Length)).Trim
                    'strHeadCode = txtPrefixSuffix.Text.Trim & strHeadCode
                    If chkApplySametoHeadName.Checked = True Then
                        strHeadName = txtPrefixSuffix.Text.Trim & Strings.Left(lvItem.SubItems(colhHeadName.Index).Text, (50 - txtPrefixSuffix.Text.Trim.Length)).Trim
                    End If
                ElseIf radSufffix.Checked = True Then
                    strHeadCode = Strings.Left(lvItem.SubItems(colhHeadCode.Index).Text, (50 - txtPrefixSuffix.Text.Trim.Length)).Trim & txtPrefixSuffix.Text.Trim
                    'strHeadCode += txtPrefixSuffix.Text.Trim
                    If chkApplySametoHeadName.Checked = True Then
                        strHeadName = Strings.Left(lvItem.SubItems(colhHeadName.Index).Text, (50 - txtPrefixSuffix.Text.Trim.Length)).Trim & txtPrefixSuffix.Text.Trim
                    End If
                End If

                If strHeadName.Trim = "" Then strHeadName = lvItem.SubItems(colhHeadName.Index).Text

                If gbFindReplace.Checked = True Then
                    strHeadName = Replace(strHeadName, txtFind.Text, txtReplace.Text, , , CompareMethod.Text)
                End If
                If radPrefix.Checked = True Then
                    strHeadName = Strings.Left(strHeadName, 50).Trim
                Else
                    strHeadName = Strings.Right(strHeadName, 50).Trim
                End If


                dtRow = mdtTable.NewRow

                dtRow.Item("checked") = False
                dtRow.Item("tranid") = CInt(lvItem.Tag)
                dtRow.Item("trancode") = strHeadCode
                dtRow.Item("tranname") = strHeadName
                If objTranHead.isExist(strHeadCode) = True Then
                    dtRow.Item("rowtypeid") = 1

                    lblCodeExist.Visible = True
                    lblCodeExistColor.Visible = True
                ElseIf objTranHead.isExist("", strHeadName) = True Then
                    dtRow.Item("rowtypeid") = 2

                    lblNameExist.Visible = True
                    lblNameExistColor.Visible = True
                Else
                    dtRow.Item("rowtypeid") = 0

                    '*** New formula ***
                    dtRow.Item("formula") = lvItem.SubItems(colhHeadName.Index).Text & strFormula
                    dtRow.Item("formulaid") = "#" & lvItem.Tag.ToString & "#" & strFormulaID

                    dtRow.Item("calctypeid") = CInt(cboCalcTypeId.SelectedValue)
                    dtRow.Item("calctype") = cboCalcTypeId.Text
                End If

                mdtTable.Rows.Add(dtRow)

            Next

            mdtTable = New DataView(mdtTable, "", "rowtypeid", DataViewRowState.CurrentRows).ToTable

            dgvNewTranHead.AutoGenerateColumns = False

            dgcolhCheck.DataPropertyName = "checked"
            dgcolhTranCode.DataPropertyName = "trancode"
            dgcolhTranName.DataPropertyName = "tranname"
            dgcolhFormula.DataPropertyName = "formula"
            dgcolhHeadType.DataPropertyName = "headtype"
            dgcolhTypeOF.DataPropertyName = "typeof"
            dgcolhCalctype.DataPropertyName = "calctype"
            dgcolhRowType.DataPropertyName = "rowtypeid"

            dgvNewTranHead.DataSource = mdtTable

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Function

    Private Function GenerateTranHeads() As Boolean
        Dim objSimpleSlab As New clsTranheadSlabTran
        Dim dtFormula As DataTable
        Dim dtSimpleSlab As DataTable
        'Sohail (21 Nov 2011) -- Start
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim intPeriodID As Integer
        'Sohail (21 Nov 2011) -- End

        Try
            Cursor.Current = Cursors.WaitCursor
            '******* Simple Slab ****
            Dim dRow As DataRow
            dtSimpleSlab = objSimpleSlab._Datasource
            dRow = dtSimpleSlab.NewRow
            With dRow
                .Item("tranheadslabtranunkid") = -1
                .Item("tranheadunkid") = -1
                .Item("amountupto") = 999999999.0
                .Item("slabtype") = enPaymentBy.Percentage
                .Item("valuebasis") = 100
                .Item("userunkid") = User._Object._Userunkid
                .Item("isvoid") = False
                .Item("voiduserunkid") = -1
                .Item("voiddatetime") = DBNull.Value
                .Item("voidreason") = ""
                .Item("GUID") = Guid.NewGuid.ToString
                .Item("AUD") = "A"
                'Sohail (21 Nov 2011) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'intPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date)
                intPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, FinancialYear._Object._YearUnkid)
                'S.SANDEEP [04 JUN 2015] -- END

                If intPeriodID <= 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", False)
                    dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
                    'Sohail (21 Aug 2015) -- End
                    If dsList.Tables("Period").Rows.Count > 0 Then
                        intPeriodID = CInt(dsList.Tables("Period").Rows(0).Item("periodunkid").ToString)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please First Create atleast One Period."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                .Item("periodunkid") = intPeriodID
                'Sohail (21 Nov 2011) -- End
            End With
            dtSimpleSlab.Rows.Add(dRow)

            Dim dr() As DataRow = mdtTable.Select("rowtypeid = 0")
            For Each dtRow As DataRow In dr
                objTranHead = New clsTransactionHead

                With objTranHead
                    '*** Add current trnsaction head unkid in common formula table ***
                    'Sohail (19 Jun 2017) -- Start
                    'Issue - 68.1 - Object reference error in Generate transaction head.
                    'dtFormula = mdtFormula
                    dtFormula = mdtFormula.Copy
                    'Sohail (19 Jun 2017) -- End
                    dRow = dtFormula.NewRow
                    dRow.Item("formulatranheadunkid") = -1
                    dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranid"))
                    'Sohail (19 Jun 2017) -- Start
                    'Issue - 68.1 - Object reference error in Generate transaction head.
                    dRow.Item("periodunkid") = intPeriodID
                    'Sohail (19 Jun 2017) -- End
                    dRow.Item("AUD") = "A"
                    dtFormula.Rows.Add(dRow)

                    ._Trnheadcode = dtRow.Item("trancode").ToString
                    ._Trnheadname = dtRow.Item("tranname").ToString
                    ._Trnheadtype_Id = CInt(dtRow.Item("headtypeid"))
                    ._Typeof_id = CInt(dtRow.Item("typeofid"))
                    ._Calctype_Id = CInt(dtRow.Item("calctypeid"))
                    ._Computeon_Id = enComputeOn.ComputeOnSpecifiedFormula
                    ._Formula = dtRow.Item("formula").ToString
                    ._Formulaid = dtRow.Item("formulaid").ToString
                    ._FormulaDatasource = dtFormula
                    ._Isappearonpayslip = True
                    ._Isrecurrent = True
                    ._Userunkid = User._Object._Userunkid
                    ._Isactive = True 'Sohail (09 Nov 2013)
                    'Hemant (23 Oct 2020) -- Start
                    ._CostCenterUnkId = 0
                    'Hemant (23 Oct 2020) -- End
                    'Sohail (03 Dec 2020) -- Start
                    'Freight Forwarders Kenya Ltd Enhancement : # OLD-51 : Provide rounding options at transaction head level.
                    ._RoundOffTypeId = 200 '200 := -0.005 <-> 0.005
                    'Sohail (03 Dec 2020) -- End
                    'Sohail (11 Dec 2020) -- Start
                    'Netis Togo Enhancement # OLD-218 : - Give option to Round Up/Round Down/Auto on transaction head next to Rounding OFF Type.
                    ._RoundOffModeId = enPaymentRoundingType.AUTOMATIC
                    'Sohail (11 Dec 2020) -- End
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If .Insert(CInt(dtRow.Item("calctypeid")), dtSimpleSlab) = False Then
                    If .Insert(CInt(dtRow.Item("calctypeid")), ConfigParameter._Object._CurrentDateAndTime, dtSimpleSlab) = False Then
                        'Sohail (21 Aug 2015) -- End
                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                End With
            Next
            Cursor.Current = Cursors.Default
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateTranHeads", mstrModuleName)
        Finally
            objSimpleSlab = Nothing
            'Sohail (21 Nov 2011) -- Start
            objMaster = Nothing
            objPeriod = Nothing
            'Sohail (21 Nov 2011) -- End
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmGenerateTranHead_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTranHead = Nothing
            objFormula = Nothing
            'Sohail (23 Jan 2014) -- Start
            'Enhancement - Oman
            objFormulaCurrency = Nothing
            objFormulaLeave = Nothing
            objFormulaMeasurementUnit = Nothing
            objFormulaActivityUnit = Nothing
            objFormulaMeasurementAmount = Nothing
            objFormulaActivityAmount = Nothing
            objFormulaShift = Nothing
            objFormulaCumulative = Nothing
            'Sohail (23 Jan 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateTranHead_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGenerateTranHead_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objTranHead = New clsTransactionHead
        objFormula = New clsTranheadFormulaTran
        'Sohail (23 Jan 2014) -- Start
        'Enhancement - Oman
        objFormulaCurrency = New clsTranheadFormulaCurrencyTran
        objFormulaLeave = New clsTranheadFormulaLeaveTran
        objFormulaSlabTran = New clsTranhead_formula_slab_Tran
        objFormulaMeasurementUnit = New clsTranhead_formula_measurement_unit_Tran
        objFormulaActivityUnit = New clsTranhead_formula_activity_unit_Tran
        objFormulaMeasurementAmount = New clsTranhead_formula_measurement_amount_Tran
        objFormulaActivityAmount = New clsTranhead_formula_activity_amount_Tran
        objFormulaShift = New clsTranheadFormulaShiftTran
        objFormulaCumulative = New clsTranheadFormulaCumulativeTran
        'Sohail (23 Jan 2014) -- End
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call setColor()
            lblCodeExist.Visible = False
            lblCodeExistColor.Visible = False
            lblNameExistColor.Visible = False
            lblNameExist.Visible = False

            lblCodeExist.Text = Language.getMessage(mstrModuleName, 12, "Tran. Head Code already Exists.")
            lblNameExist.Text = Language.getMessage(mstrModuleName, 13, "Tran. Head Name already Exists.")
            Call FillCombo()

            Call FillList()

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGenerateTranHead_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Combobox's Events "
    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
            txtFormula.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        'Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        'Sohail (28 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION
        Dim strDefaultHeadCalcType As String = enCalcType.NET_PAY & "," & enCalcType.TOTAL_EARNING & "," & enCalcType.TOTAL_DEDUCTION & "," & enCalcType.TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_TAXABLE_EARNING_TOTAL & "," & enCalcType.NON_CASH_BENEFIT_TOTAL
        'Sohail (28 Jan 2012) -- End
        Try
            Select Case CInt(cboTypeOf.SelectedValue)
                Case Is > 1
                    dsList = objMaster.getComboListCalcType("CalcType", "2") 'Sohail (28 Jan 2012)

                    If CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Earnings Then
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Other_Deductions_Emp Then
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    ElseIf CInt(cboTypeOf.SelectedValue) = enTypeOf.Informational Then
                        If menAction = enAction.EDIT_ONE Then
                            Select Case objTranHead._Calctype_Id
                                Case enCalcType.NET_PAY, enCalcType.TOTAL_EARNING, enCalcType.TOTAL_DEDUCTION
                                    dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & ") ", "", DataViewRowState.CurrentRows).ToTable
                                Case Else
                                    dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                            End Select
                        Else
                            dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                        End If
                    Else
                        dtTable = New DataView(dsList.Tables("CalcType"), "Id NOT IN (" & enCalcType.ShortHours & "," & enCalcType.OverTimeHours & "," & strDefaultHeadCalcType & ") ", "", DataViewRowState.CurrentRows).ToTable
                    End If
                Case Else 'Type of - Salary
                    dsList = objMaster.getComboListCalcType("CalcType", CInt(cboTypeOf.SelectedValue).ToString) 'Sohail (28 Jan 2012)
                    dtTable = New DataView(dsList.Tables("CalcType"), "", "", DataViewRowState.CurrentRows).ToTable
            End Select

            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTypeOf_SelectedIndexChanged", mstrModuleName)
        Finally
            'objTranHead = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboHeadTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboHeadTypeId.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable

        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboHeadTypeId.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOfId
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboHeadTypeId_SelectedIndexChanged", mstrModuleName)
        Finally
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub
#End Region

#Region " Buton's Events "

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If CInt(cboHeadTypeId.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head Type."), enMsgBoxStyle.Information)
                cboHeadTypeId.Focus()
                Exit Try
            ElseIf CInt(cboTypeOfId.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Transaction Head Type Of."), enMsgBoxStyle.Information)
                cboTypeOfId.Focus()
                Exit Try
            End If
            Dim dr() As DataRow = Nothing
            If radApplytoAll.Checked = True Then
                dr = mdtTable.Select("rowtypeid = 0")
            ElseIf radApplytoChecked.Checked = True Then
                dr = mdtTable.Select("checked = 1")
            End If
            If dr.Length > 0 Then
                For Each dtRow As DataRow In dr
                    dtRow.Item("headtypeid") = CInt(cboHeadTypeId.SelectedValue)
                    dtRow.Item("headtype") = cboHeadTypeId.Text
                    dtRow.Item("typeofid") = CInt(cboTypeOfId.SelectedValue)
                    dtRow.Item("typeof") = cboTypeOfId.Text
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub dgvNewTranHead_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvNewTranHead.CellBeginEdit
        Try
            If e.ColumnIndex = dgcolhCheck.Index AndAlso CInt(dgvNewTranHead.Rows(e.RowIndex).Cells(dgcolhRowType.Index).Value) = 1 Then
                e.Cancel = True
            ElseIf e.ColumnIndex = dgcolhCheck.Index AndAlso CInt(dgvNewTranHead.Rows(e.RowIndex).Cells(dgcolhRowType.Index).Value) = 2 Then
                e.Cancel = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvNewTranHead_CellBeginEdit", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvNewTranHead_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvNewTranHead.CellFormatting
        Try
            If CInt(dgvNewTranHead.Rows(e.RowIndex).Cells(dgcolhRowType.Index).Value) = 1 Then
                dgvNewTranHead.Rows(e.RowIndex).DefaultCellStyle.BackColor = System.Drawing.SystemColors.ControlDark
            ElseIf CInt(dgvNewTranHead.Rows(e.RowIndex).Cells(dgcolhRowType.Index).Value) = 2 Then
                dgvNewTranHead.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
            Else
                dgvNewTranHead.Rows(e.RowIndex).DefaultCellStyle.BackColor = System.Drawing.SystemColors.Window
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvNewTranHead_CellFormatting", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview's Events "

    Private Sub lvTrnHeadList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTrnHeadList.ItemChecked

        Try
            If lvTrnHeadList.CheckedItems.Count <= 0 Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Unchecked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTrnHeadList.CheckedItems.Count < lvTrnHeadList.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTrnHeadList.CheckedItems.Count = lvTrnHeadList.Items.Count Then
                RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                chkSelectAll.CheckState = CheckState.Checked
                AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrnHeadList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkDgCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDgCheckAll.CheckedChanged
        Try
            Dim dr() As DataRow = mdtTable.Select("rowtypeid = 0")
            If dr.Length > 0 Then
                For Each dtRow As DataRow In dr
                    dtRow.Item("checked") = chkDgCheckAll.Checked
                Next
                mdtTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub wzGenerateTranHead_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzGenerateTranHead.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wzGenerateTranHead.Pages.IndexOf(wpTranHeadList)
                    If e.NewIndex > e.OldIndex Then
                        If lvTrnHeadList.CheckedItems.Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one Transaction Head."), enMsgBoxStyle.Information)
                            lvTrnHeadList.Focus()
                            e.Cancel = True
                        End If
                    End If
                Case wzGenerateTranHead.Pages.IndexOf(wpConfiguration)
                    If e.NewIndex > e.OldIndex Then
                        If CInt(cboFunction.SelectedIndex) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Function."), enMsgBoxStyle.Information)
                            cboFunction.Focus()
                            e.Cancel = True
                        ElseIf txtFormula.Text.Trim = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Add Formula/Function/constant."), enMsgBoxStyle.Information)
                            objbtnAddComputation.Focus()
                            e.Cancel = True
                        ElseIf txtPrefixSuffix.Text.Trim = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Prefix or Suffix for New Transaction Heads Code."), enMsgBoxStyle.Information)
                            txtPrefixSuffix.Focus()
                            e.Cancel = True
                        ElseIf chkApplySametoHeadName.Checked = False AndAlso gbFindReplace.Checked = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Check Apply same setting to Tran. Head Name OR Check Find and Replace setting."), enMsgBoxStyle.Information)
                            chkApplySametoHeadName.Focus()
                            e.Cancel = True
                        ElseIf gbFindReplace.Checked = True AndAlso txtFind.Text.Trim = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter text to Find."), enMsgBoxStyle.Information)
                            txtFind.Focus()
                            e.Cancel = True
                        Else
                            If SetData() = False Then
                                e.Cancel = True
                            End If
                        End If
                    End If
                Case wzGenerateTranHead.Pages.IndexOf(wpNewTranHead)
                    If e.NewIndex > e.OldIndex Then
                        Dim dr() As DataRow = mdtTable.Select("typeofid = 0 AND rowtypeid = 0")
                        If dr.Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please Set Head Type and Type Of for all New Transaction Head."), enMsgBoxStyle.Information)
                            cboHeadTypeId.Focus()
                            e.Cancel = True
                        Else
                            If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Are you sure you want to generate all Transaction Heads?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                                e.Cancel = True
                            Else
                                If GenerateTranHeads() = False Then
                                    e.Cancel = True
                                Else
                                    mblnCancel = False
                                End If
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzGenerateTranHead_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboTrnHeadType.Items.Count > 0 Then cboTrnHeadType.SelectedIndex = 0
            If cboTypeOf.Items.Count > 0 Then cboTypeOf.SelectedIndex = 0
            If cboCalcType.Items.Count > 0 Then cboCalcType.SelectedIndex = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddComputation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddComputation.Click
        Dim objFrm As New frmAdditionalComputation_AddEdit
        Dim moTempAction As enAction
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            If CInt(cboTrnHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head Type."), enMsgBoxStyle.Information)
                cboTrnHeadType.Focus()
                Exit Try
            End If

            objFrm._Formula = txtFormula.Text
            If txtFormula.Tag Is Nothing Then
                objFrm._FormulaID = ""
            Else
                objFrm._FormulaID = txtFormula.Tag.ToString
            End If
            'Sohail (23 Jan 2014) -- Start
            'Enhancement - Oman
            Dim dt As DataTable
            dt = mdtFormulaSlab.Copy
            objFrm._DatasourcePeriodWiseSlab = dt
            'Sohail (23 Jan 2014) -- End
            If txtFormula.Text = "" Then
                moTempAction = enAction.ADD_ONE
                objFrm._Datasource = objTranHead._FormulaDatasource
                'Sohail (23 Jan 2014) -- Start
                'Enhancement - Oman
                objFrm._DatasourceCurrency = objTranHead._FormulaCurrencyDatasource
                objFrm._DatasourcePaidUnpaidLeave = objTranHead._FormulaLeaveDatasource
                objFrm._DatasourceMeasurementUnit = objTranHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTranHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTranHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTranHead._FormulaActivityAmountDatasource
                objFrm._DatasourceShift = objTranHead._FormulaShiftDatasource
                objFrm._DatasourceCumulative = objTranHead._FormulaCumulativeDatasource
                'Sohail (23 Jan 2014) -- End
            Else
                moTempAction = enAction.EDIT_ONE
                objFrm._Datasource = objTranHead._FormulaDatasource
                'Sohail (23 Jan 2014) -- Start
                'Enhancement - Oman
                objFrm._DatasourceCurrency = objTranHead._FormulaCurrencyDatasource
                objFrm._DatasourcePaidUnpaidLeave = objTranHead._FormulaLeaveDatasource
                objFrm._DatasourceMeasurementUnit = objTranHead._FormulaMeasurementUnitDatasource
                objFrm._DatasourceActivityUnit = objTranHead._FormulaActivityUnitDatasource
                objFrm._DatasourceMeasurementAmount = objTranHead._FormulaMeasurementAmountDatasource
                objFrm._DatasourceActivityAmount = objTranHead._FormulaActivityAmountDatasource
                objFrm._DatasourceShift = objTranHead._FormulaShiftDatasource
                objFrm._DatasourceCumulative = objTranHead._FormulaCumulativeDatasource
                'Sohail (23 Jan 2014) -- End
            End If

            If objFrm.displayDialog(-1, moTempAction, CInt(cboTrnHeadType.SelectedValue), 0, 0) Then
                txtFormula.Text = objFrm._Formula
                txtFormula.Tag = objFrm._FormulaID
                'Sohail (23 Jan 2014) -- Start
                'Enhancement - Oman
                'mdtFormula = objFrm._Datasource
                objTranHead._FormulaDatasource = objFrm._Datasource
                objTranHead._FormulaCurrencyDatasource = objFrm._DatasourceCurrency
                objTranHead._FormulaLeaveDatasource = objFrm._DatasourcePaidUnpaidLeave
                objTranHead._FormulaMeasurementUnitDatasource = objFrm._DatasourceMeasurementUnit
                objTranHead._FormulaActivityUnitDatasource = objFrm._DatasourceActivityUnit
                objTranHead._FormulaMeasurementAmountDatasource = objFrm._DatasourceMeasurementAmount
                objTranHead._FormulaActivityAmountDatasource = objFrm._DatasourceActivityAmount
                objTranHead._FormulaShiftDatasource = objFrm._DatasourceShift
                objTranHead._FormulaCumulativeDatasource = objFrm._DatasourceCumulative
                mdtFormulaSlab = objFrm._DatasourcePeriodWiseSlab
                objTranHead._FormulaPeriodWiseSlabDatasource = objFrm._DatasourcePeriodWiseSlab
                'Sohail (23 Jan 2014) -- End
                'Sohail (19 Jun 2017) -- Start
                'Issue - 68.1 - Object reference error in Generate transaction head.
                mdtFormula = objFrm._Datasource
                'Sohail (19 Jun 2017) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddComputation_Click", mstrModuleName)
        Finally
            objFrm = Nothing
        End Try
    End Sub
#End Region

#Region " Messages "
    '1, "Please select Transaction Head Type."
    '2, "Please select atleast one Transaction Head."
    '3, "Please select Function."
    '4, "Please Add Formula."
    '5, "Please enter Prefix or Suffix for New Transaction Heads Code."
    '6, "Please either Check Apply same setting to Tran. Head Name OR Check Find and Replace setting."
    '7, "Please enter text to Find."
    '8, "Please select Transaction Head Type."
    '9, "Please select Transaction Head Type Of."
    '10, "Please Set Head Type and Type for all New Transaction Head."
    '11, "Are you sure you want to generate all Transaction Heads?"
    '14, "Please First Create atleast One Period."
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPrefixSuffix.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPrefixSuffix.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFindReplace.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFindReplace.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbCommonFormula.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCommonFormula.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbTranHeadList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTranHeadList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSet.GradientBackColor = GUI._ButttonBackColor
            Me.btnSet.GradientForeColor = GUI._ButttonFontColor

            Me.EZeeLightButton1.GradientBackColor = GUI._ButttonBackColor
            Me.EZeeLightButton1.GradientForeColor = GUI._ButttonFontColor

            Me.EZeeLightButton2.GradientBackColor = GUI._ButttonBackColor
            Me.EZeeLightButton2.GradientForeColor = GUI._ButttonFontColor

            Me.EZeeLightButton3.GradientBackColor = GUI._ButttonBackColor
            Me.EZeeLightButton3.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wzGenerateTranHead.CancelText = Language._Object.getCaption(Me.wzGenerateTranHead.Name & "_CancelText", Me.wzGenerateTranHead.CancelText)
            Me.wzGenerateTranHead.NextText = Language._Object.getCaption(Me.wzGenerateTranHead.Name & "_NextText", Me.wzGenerateTranHead.NextText)
            Me.wzGenerateTranHead.BackText = Language._Object.getCaption(Me.wzGenerateTranHead.Name & "_BackText", Me.wzGenerateTranHead.BackText)
            Me.wzGenerateTranHead.FinishText = Language._Object.getCaption(Me.wzGenerateTranHead.Name & "_FinishText", Me.wzGenerateTranHead.FinishText)
            Me.lblFinish.Text = Language._Object.getCaption(Me.lblFinish.Name, Me.lblFinish.Text)
            Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
            Me.lblCodeExist.Text = Language._Object.getCaption(Me.lblCodeExist.Name, Me.lblCodeExist.Text)
            Me.lblCodeExistColor.Text = Language._Object.getCaption(Me.lblCodeExistColor.Name, Me.lblCodeExistColor.Text)
            Me.lblNameExist.Text = Language._Object.getCaption(Me.lblNameExist.Name, Me.lblNameExist.Text)
            Me.lblNameExistColor.Text = Language._Object.getCaption(Me.lblNameExistColor.Name, Me.lblNameExistColor.Text)
            Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
            Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
            Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
            Me.lblTypeOfId.Text = Language._Object.getCaption(Me.lblTypeOfId.Name, Me.lblTypeOfId.Text)
            Me.lblHeadTypeId.Text = Language._Object.getCaption(Me.lblHeadTypeId.Name, Me.lblHeadTypeId.Text)
            Me.chkDgCheckAll.Text = Language._Object.getCaption(Me.chkDgCheckAll.Name, Me.chkDgCheckAll.Text)
            Me.dgcolhCheck.HeaderText = Language._Object.getCaption(Me.dgcolhCheck.Name, Me.dgcolhCheck.HeaderText)
            Me.dgcolhTranCode.HeaderText = Language._Object.getCaption(Me.dgcolhTranCode.Name, Me.dgcolhTranCode.HeaderText)
            Me.dgcolhTranName.HeaderText = Language._Object.getCaption(Me.dgcolhTranName.Name, Me.dgcolhTranName.HeaderText)
            Me.dgcolhFormula.HeaderText = Language._Object.getCaption(Me.dgcolhFormula.Name, Me.dgcolhFormula.HeaderText)
            Me.dgcolhHeadType.HeaderText = Language._Object.getCaption(Me.dgcolhHeadType.Name, Me.dgcolhHeadType.HeaderText)
            Me.dgcolhTypeOF.HeaderText = Language._Object.getCaption(Me.dgcolhTypeOF.Name, Me.dgcolhTypeOF.HeaderText)
            Me.dgcolhCalctype.HeaderText = Language._Object.getCaption(Me.dgcolhCalctype.Name, Me.dgcolhCalctype.HeaderText)
            Me.dgcolhRowType.HeaderText = Language._Object.getCaption(Me.dgcolhRowType.Name, Me.dgcolhRowType.HeaderText)
            Me.EZeeLightButton1.Text = Language._Object.getCaption(Me.EZeeLightButton1.Name, Me.EZeeLightButton1.Text)
            Me.EZeeLightButton2.Text = Language._Object.getCaption(Me.EZeeLightButton2.Name, Me.EZeeLightButton2.Text)
            Me.EZeeLightButton3.Text = Language._Object.getCaption(Me.EZeeLightButton3.Name, Me.EZeeLightButton3.Text)
            Me.lnTranHeadCode.Text = Language._Object.getCaption(Me.lnTranHeadCode.Name, Me.lnTranHeadCode.Text)
            Me.lblCreateCommonFormula.Text = Language._Object.getCaption(Me.lblCreateCommonFormula.Name, Me.lblCreateCommonFormula.Text)
            Me.gbPrefixSuffix.Text = Language._Object.getCaption(Me.gbPrefixSuffix.Name, Me.gbPrefixSuffix.Text)
            Me.chkApplySametoHeadName.Text = Language._Object.getCaption(Me.chkApplySametoHeadName.Name, Me.chkApplySametoHeadName.Text)
            Me.radSufffix.Text = Language._Object.getCaption(Me.radSufffix.Name, Me.radSufffix.Text)
            Me.radPrefix.Text = Language._Object.getCaption(Me.radPrefix.Name, Me.radPrefix.Text)
            Me.lnTranHeadName.Text = Language._Object.getCaption(Me.lnTranHeadName.Name, Me.lnTranHeadName.Text)
            Me.gbFindReplace.Text = Language._Object.getCaption(Me.gbFindReplace.Name, Me.gbFindReplace.Text)
            Me.lblReplace.Text = Language._Object.getCaption(Me.lblReplace.Name, Me.lblReplace.Text)
            Me.lblFind.Text = Language._Object.getCaption(Me.lblFind.Name, Me.lblFind.Text)
            Me.gbCommonFormula.Text = Language._Object.getCaption(Me.gbCommonFormula.Name, Me.gbCommonFormula.Text)
            Me.lblFunction.Text = Language._Object.getCaption(Me.lblFunction.Name, Me.lblFunction.Text)
            Me.lblFormula.Text = Language._Object.getCaption(Me.lblFormula.Name, Me.lblFormula.Text)
            Me.lblHeadSelection.Text = Language._Object.getCaption(Me.lblHeadSelection.Name, Me.lblHeadSelection.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEffectiveFrom.Text = Language._Object.getCaption(Me.lblEffectiveFrom.Name, Me.lblEffectiveFrom.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.Name, Me.lblTypeOf.Text)
            Me.gbTranHeadList.Text = Language._Object.getCaption(Me.gbTranHeadList.Name, Me.gbTranHeadList.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.colhHeadCheck.Text = Language._Object.getCaption(CStr(Me.colhHeadCheck.Tag), Me.colhHeadCheck.Text)
            Me.colhHeadCode.Text = Language._Object.getCaption(CStr(Me.colhHeadCode.Tag), Me.colhHeadCode.Text)
            Me.colhHeadName.Text = Language._Object.getCaption(CStr(Me.colhHeadName.Tag), Me.colhHeadName.Text)
            Me.lblMessageWizard.Text = Language._Object.getCaption(Me.lblMessageWizard.Name, Me.lblMessageWizard.Text)
            Me.lblGenerateWizard.Text = Language._Object.getCaption(Me.lblGenerateWizard.Name, Me.lblGenerateWizard.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Transaction Head Type.")
            Language.setMessage(mstrModuleName, 2, "Please select atleast one Transaction Head.")
            Language.setMessage(mstrModuleName, 3, "Please select Function.")
            Language.setMessage(mstrModuleName, 4, "Please Add Formula/Function/constant.")
            Language.setMessage(mstrModuleName, 5, "Please enter Prefix or Suffix for New Transaction Heads Code.")
            Language.setMessage(mstrModuleName, 6, "Please Check Apply same setting to Tran. Head Name OR Check Find and Replace setting.")
            Language.setMessage(mstrModuleName, 7, "Please enter text to Find.")
            Language.setMessage(mstrModuleName, 9, "Please select Transaction Head Type Of.")
            Language.setMessage(mstrModuleName, 10, "Please Set Head Type and Type Of for all New Transaction Head.")
            Language.setMessage(mstrModuleName, 11, "Are you sure you want to generate all Transaction Heads?")
            Language.setMessage(mstrModuleName, 12, "Tran. Head Code is already Exist")
            Language.setMessage(mstrModuleName, 13, "Tran. Head Name is already Exist")
            Language.setMessage(mstrModuleName, 14, "Please First Create atleast One Period.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class