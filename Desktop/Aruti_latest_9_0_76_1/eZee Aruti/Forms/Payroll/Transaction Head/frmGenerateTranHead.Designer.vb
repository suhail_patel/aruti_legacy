﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGenerateTranHead
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGenerateTranHead))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.wzGenerateTranHead = New eZee.Common.eZeeWizard
        Me.wpTranHeadList = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblHeadSelection = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblEffectiveFrom = New System.Windows.Forms.Label
        Me.cboCalcType = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.gbTranHeadList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvTrnHeadList = New System.Windows.Forms.ListView
        Me.colhHeadCheck = New System.Windows.Forms.ColumnHeader
        Me.colhHeadCode = New System.Windows.Forms.ColumnHeader
        Me.colhHeadName = New System.Windows.Forms.ColumnHeader
        Me.wpWelcome = New eZee.Common.eZeeWizardPage(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblMessageWizard = New System.Windows.Forms.Label
        Me.lblGenerateWizard = New System.Windows.Forms.Label
        Me.wpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.pnlFinish = New System.Windows.Forms.Panel
        Me.lblFinish = New System.Windows.Forms.Label
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.wpNewTranHead = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboCalcTypeId = New System.Windows.Forms.ComboBox
        Me.lblCodeExist = New System.Windows.Forms.Label
        Me.lblCodeExistColor = New System.Windows.Forms.Label
        Me.lblNameExist = New System.Windows.Forms.Label
        Me.lblNameExistColor = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.btnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboTypeOfId = New System.Windows.Forms.ComboBox
        Me.lblTypeOfId = New System.Windows.Forms.Label
        Me.cboHeadTypeId = New System.Windows.Forms.ComboBox
        Me.lblHeadTypeId = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.chkDgCheckAll = New System.Windows.Forms.CheckBox
        Me.dgvNewTranHead = New System.Windows.Forms.DataGridView
        Me.dgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhTranCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFormula = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHeadType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTypeOF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCalctype = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRowType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeLightButton1 = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLightButton2 = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLightButton3 = New eZee.Common.eZeeLightButton(Me.components)
        Me.wpConfiguration = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lnTranHeadCode = New eZee.Common.eZeeLine
        Me.lblCreateCommonFormula = New System.Windows.Forms.Label
        Me.gbPrefixSuffix = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtPrefixSuffix = New eZee.TextBox.AlphanumericTextBox
        Me.chkApplySametoHeadName = New System.Windows.Forms.CheckBox
        Me.radSufffix = New System.Windows.Forms.RadioButton
        Me.radPrefix = New System.Windows.Forms.RadioButton
        Me.lnTranHeadName = New eZee.Common.eZeeLine
        Me.gbFindReplace = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtReplace = New eZee.TextBox.AlphanumericTextBox
        Me.txtFind = New eZee.TextBox.AlphanumericTextBox
        Me.lblReplace = New System.Windows.Forms.Label
        Me.lblFind = New System.Windows.Forms.Label
        Me.gbCommonFormula = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblFunction = New System.Windows.Forms.Label
        Me.cboFunction = New System.Windows.Forms.ComboBox
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.objbtnAddComputation = New eZee.Common.eZeeGradientButton
        Me.lblFormula = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.pnlMain.SuspendLayout()
        Me.wzGenerateTranHead.SuspendLayout()
        Me.wpTranHeadList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTranHeadList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.wpWelcome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpFinish.SuspendLayout()
        Me.pnlFinish.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpNewTranHead.SuspendLayout()
        Me.gbFileInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvNewTranHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wpConfiguration.SuspendLayout()
        Me.gbPrefixSuffix.SuspendLayout()
        Me.gbFindReplace.SuspendLayout()
        Me.gbCommonFormula.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.wzGenerateTranHead)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(809, 508)
        Me.pnlMain.TabIndex = 5
        '
        'wzGenerateTranHead
        '
        Me.wzGenerateTranHead.Controls.Add(Me.wpNewTranHead)
        Me.wzGenerateTranHead.Controls.Add(Me.wpConfiguration)
        Me.wzGenerateTranHead.Controls.Add(Me.wpFinish)
        Me.wzGenerateTranHead.Controls.Add(Me.wpTranHeadList)
        Me.wzGenerateTranHead.Controls.Add(Me.wpWelcome)
        Me.wzGenerateTranHead.Controls.Add(Me.objbuttonBack)
        Me.wzGenerateTranHead.Controls.Add(Me.objbuttonCancel)
        Me.wzGenerateTranHead.Controls.Add(Me.objbuttonNext)
        Me.wzGenerateTranHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzGenerateTranHead.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzGenerateTranHead.HeaderImage = Nothing
        Me.wzGenerateTranHead.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzGenerateTranHead.Location = New System.Drawing.Point(0, 0)
        Me.wzGenerateTranHead.Name = "wzGenerateTranHead"
        Me.wzGenerateTranHead.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpWelcome, Me.wpTranHeadList, Me.wpConfiguration, Me.wpNewTranHead, Me.wpFinish})
        Me.wzGenerateTranHead.SaveEnabled = True
        Me.wzGenerateTranHead.SaveText = "Save && Finish"
        Me.wzGenerateTranHead.SaveVisible = False
        Me.wzGenerateTranHead.SetSaveIndexBeforeFinishIndex = False
        Me.wzGenerateTranHead.Size = New System.Drawing.Size(809, 508)
        Me.wzGenerateTranHead.TabIndex = 246
        Me.wzGenerateTranHead.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzGenerateTranHead.WelcomeImage = Nothing
        Me.wzGenerateTranHead.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wpTranHeadList
        '
        Me.wpTranHeadList.Controls.Add(Me.lblHeadSelection)
        Me.wpTranHeadList.Controls.Add(Me.gbFilterCriteria)
        Me.wpTranHeadList.Controls.Add(Me.gbTranHeadList)
        Me.wpTranHeadList.Location = New System.Drawing.Point(0, 0)
        Me.wpTranHeadList.Name = "wpTranHeadList"
        Me.wpTranHeadList.Size = New System.Drawing.Size(809, 460)
        Me.wpTranHeadList.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpTranHeadList.TabIndex = 9
        '
        'lblHeadSelection
        '
        Me.lblHeadSelection.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadSelection.Location = New System.Drawing.Point(422, 153)
        Me.lblHeadSelection.Name = "lblHeadSelection"
        Me.lblHeadSelection.Size = New System.Drawing.Size(318, 131)
        Me.lblHeadSelection.TabIndex = 246
        Me.lblHeadSelection.Text = "Please Select Transaction Head(s) from the List in order to create new Transactio" & _
            "n Head(s). "
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblEffectiveFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTypeOf)
        Me.gbFilterCriteria.Controls.Add(Me.cboTypeOf)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(414, 6)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(353, 124)
        Me.gbFilterCriteria.TabIndex = 244
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Transaction Head Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(326, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(302, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'lblEffectiveFrom
        '
        Me.lblEffectiveFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveFrom.Location = New System.Drawing.Point(8, 36)
        Me.lblEffectiveFrom.Name = "lblEffectiveFrom"
        Me.lblEffectiveFrom.Size = New System.Drawing.Size(133, 15)
        Me.lblEffectiveFrom.TabIndex = 234
        Me.lblEffectiveFrom.Text = "Tran. Head Type"
        '
        'cboCalcType
        '
        Me.cboCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(147, 87)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(179, 21)
        Me.cboCalcType.TabIndex = 231
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(147, 33)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(179, 21)
        Me.cboTrnHeadType.TabIndex = 229
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(8, 90)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(133, 15)
        Me.lblCalcType.TabIndex = 232
        Me.lblCalcType.Text = "Calculation Type"
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 63)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(133, 15)
        Me.lblTypeOf.TabIndex = 236
        Me.lblTypeOf.Text = "Transaction Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(147, 60)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(179, 21)
        Me.cboTypeOf.TabIndex = 230
        '
        'gbTranHeadList
        '
        Me.gbTranHeadList.BorderColor = System.Drawing.Color.Black
        Me.gbTranHeadList.Checked = False
        Me.gbTranHeadList.CollapseAllExceptThis = False
        Me.gbTranHeadList.CollapsedHoverImage = Nothing
        Me.gbTranHeadList.CollapsedNormalImage = Nothing
        Me.gbTranHeadList.CollapsedPressedImage = Nothing
        Me.gbTranHeadList.CollapseOnLoad = False
        Me.gbTranHeadList.Controls.Add(Me.pnlEmployeeList)
        Me.gbTranHeadList.ExpandedHoverImage = Nothing
        Me.gbTranHeadList.ExpandedNormalImage = Nothing
        Me.gbTranHeadList.ExpandedPressedImage = Nothing
        Me.gbTranHeadList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTranHeadList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTranHeadList.HeaderHeight = 25
        Me.gbTranHeadList.HeaderMessage = ""
        Me.gbTranHeadList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTranHeadList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTranHeadList.HeightOnCollapse = 0
        Me.gbTranHeadList.LeftTextSpace = 0
        Me.gbTranHeadList.Location = New System.Drawing.Point(8, 6)
        Me.gbTranHeadList.Name = "gbTranHeadList"
        Me.gbTranHeadList.OpenHeight = 300
        Me.gbTranHeadList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTranHeadList.ShowBorder = True
        Me.gbTranHeadList.ShowCheckBox = False
        Me.gbTranHeadList.ShowCollapseButton = False
        Me.gbTranHeadList.ShowDefaultBorderColor = True
        Me.gbTranHeadList.ShowDownButton = False
        Me.gbTranHeadList.ShowHeader = True
        Me.gbTranHeadList.Size = New System.Drawing.Size(400, 443)
        Me.gbTranHeadList.TabIndex = 6
        Me.gbTranHeadList.Temp = 0
        Me.gbTranHeadList.Text = "Transaction Head List"
        Me.gbTranHeadList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.chkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvTrnHeadList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(3, 25)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(394, 417)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 17
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'lvTrnHeadList
        '
        Me.lvTrnHeadList.CheckBoxes = True
        Me.lvTrnHeadList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhHeadCheck, Me.colhHeadCode, Me.colhHeadName})
        Me.lvTrnHeadList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTrnHeadList.FullRowSelect = True
        Me.lvTrnHeadList.GridLines = True
        Me.lvTrnHeadList.Location = New System.Drawing.Point(0, 0)
        Me.lvTrnHeadList.Name = "lvTrnHeadList"
        Me.lvTrnHeadList.Size = New System.Drawing.Size(394, 417)
        Me.lvTrnHeadList.TabIndex = 0
        Me.lvTrnHeadList.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadList.View = System.Windows.Forms.View.Details
        '
        'colhHeadCheck
        '
        Me.colhHeadCheck.Text = ""
        Me.colhHeadCheck.Width = 30
        '
        'colhHeadCode
        '
        Me.colhHeadCode.Tag = "colhHeadCode"
        Me.colhHeadCode.Text = "Tran. Head Code"
        Me.colhHeadCode.Width = 130
        '
        'colhHeadName
        '
        Me.colhHeadName.Tag = "colhHeadName"
        Me.colhHeadName.Text = "Transaction Head"
        Me.colhHeadName.Width = 230
        '
        'wpWelcome
        '
        Me.wpWelcome.Controls.Add(Me.PictureBox1)
        Me.wpWelcome.Controls.Add(Me.lblMessageWizard)
        Me.wpWelcome.Controls.Add(Me.lblGenerateWizard)
        Me.wpWelcome.Location = New System.Drawing.Point(0, 0)
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.Size = New System.Drawing.Size(809, 460)
        Me.wpWelcome.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpWelcome.TabIndex = 7
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.PictureBox1.Location = New System.Drawing.Point(12, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(251, 444)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 247
        Me.PictureBox1.TabStop = False
        '
        'lblMessageWizard
        '
        Me.lblMessageWizard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessageWizard.Location = New System.Drawing.Point(337, 64)
        Me.lblMessageWizard.Name = "lblMessageWizard"
        Me.lblMessageWizard.Size = New System.Drawing.Size(384, 34)
        Me.lblMessageWizard.TabIndex = 236
        Me.lblMessageWizard.Text = "This wizard will help you in creating Transaction Heads with common formula based" & _
            " on selected Transaction Heads."
        '
        'lblGenerateWizard
        '
        Me.lblGenerateWizard.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGenerateWizard.Location = New System.Drawing.Point(336, 13)
        Me.lblGenerateWizard.Name = "lblGenerateWizard"
        Me.lblGenerateWizard.Size = New System.Drawing.Size(395, 29)
        Me.lblGenerateWizard.TabIndex = 235
        Me.lblGenerateWizard.Text = "Generate Transaction Head Wizard"
        '
        'wpFinish
        '
        Me.wpFinish.Controls.Add(Me.pnlFinish)
        Me.wpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wpFinish.Name = "wpFinish"
        Me.wpFinish.Size = New System.Drawing.Size(809, 460)
        Me.wpFinish.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.wpFinish.TabIndex = 13
        '
        'pnlFinish
        '
        Me.pnlFinish.Controls.Add(Me.lblFinish)
        Me.pnlFinish.Controls.Add(Me.PictureBox3)
        Me.pnlFinish.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlFinish.Location = New System.Drawing.Point(0, 0)
        Me.pnlFinish.Name = "pnlFinish"
        Me.pnlFinish.Size = New System.Drawing.Size(809, 460)
        Me.pnlFinish.TabIndex = 0
        '
        'lblFinish
        '
        Me.lblFinish.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinish.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblFinish.Location = New System.Drawing.Point(331, 37)
        Me.lblFinish.Name = "lblFinish"
        Me.lblFinish.Size = New System.Drawing.Size(395, 375)
        Me.lblFinish.TabIndex = 250
        Me.lblFinish.Text = "Generate Transaction Head Wizard Process Completed Successfully."
        Me.lblFinish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox3.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.PictureBox3.Location = New System.Drawing.Point(12, 6)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(251, 444)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 249
        Me.PictureBox3.TabStop = False
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(381, 336)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(549, 336)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(465, 336)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'wpNewTranHead
        '
        Me.wpNewTranHead.Controls.Add(Me.gbFileInfo)
        Me.wpNewTranHead.Controls.Add(Me.Panel2)
        Me.wpNewTranHead.Controls.Add(Me.EZeeLightButton1)
        Me.wpNewTranHead.Controls.Add(Me.EZeeLightButton2)
        Me.wpNewTranHead.Controls.Add(Me.EZeeLightButton3)
        Me.wpNewTranHead.Location = New System.Drawing.Point(0, 0)
        Me.wpNewTranHead.Name = "wpNewTranHead"
        Me.wpNewTranHead.Size = New System.Drawing.Size(809, 460)
        Me.wpNewTranHead.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpNewTranHead.TabIndex = 11
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.cboCalcTypeId)
        Me.gbFileInfo.Controls.Add(Me.lblCodeExist)
        Me.gbFileInfo.Controls.Add(Me.lblCodeExistColor)
        Me.gbFileInfo.Controls.Add(Me.lblNameExist)
        Me.gbFileInfo.Controls.Add(Me.lblNameExistColor)
        Me.gbFileInfo.Controls.Add(Me.objLine1)
        Me.gbFileInfo.Controls.Add(Me.objLine2)
        Me.gbFileInfo.Controls.Add(Me.radApplytoChecked)
        Me.gbFileInfo.Controls.Add(Me.radApplytoAll)
        Me.gbFileInfo.Controls.Add(Me.btnSet)
        Me.gbFileInfo.Controls.Add(Me.cboTypeOfId)
        Me.gbFileInfo.Controls.Add(Me.lblTypeOfId)
        Me.gbFileInfo.Controls.Add(Me.cboHeadTypeId)
        Me.gbFileInfo.Controls.Add(Me.lblHeadTypeId)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 3)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(785, 92)
        Me.gbFileInfo.TabIndex = 3
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "Set Head Type and Type of for Expected New Transaction Head"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCalcTypeId
        '
        Me.cboCalcTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcTypeId.FormattingEnabled = True
        Me.cboCalcTypeId.Location = New System.Drawing.Point(187, 59)
        Me.cboCalcTypeId.Name = "cboCalcTypeId"
        Me.cboCalcTypeId.Size = New System.Drawing.Size(52, 21)
        Me.cboCalcTypeId.TabIndex = 108
        Me.cboCalcTypeId.Visible = False
        '
        'lblCodeExist
        '
        Me.lblCodeExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodeExist.Location = New System.Drawing.Point(604, 38)
        Me.lblCodeExist.Name = "lblCodeExist"
        Me.lblCodeExist.Size = New System.Drawing.Size(174, 15)
        Me.lblCodeExist.TabIndex = 107
        Me.lblCodeExist.Text = "Tran. Head Code already Exists"
        '
        'lblCodeExistColor
        '
        Me.lblCodeExistColor.BackColor = System.Drawing.SystemColors.ControlDark
        Me.lblCodeExistColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodeExistColor.Location = New System.Drawing.Point(549, 38)
        Me.lblCodeExistColor.Name = "lblCodeExistColor"
        Me.lblCodeExistColor.Size = New System.Drawing.Size(49, 15)
        Me.lblCodeExistColor.TabIndex = 106
        '
        'lblNameExist
        '
        Me.lblNameExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNameExist.Location = New System.Drawing.Point(604, 57)
        Me.lblNameExist.Name = "lblNameExist"
        Me.lblNameExist.Size = New System.Drawing.Size(174, 15)
        Me.lblNameExist.TabIndex = 105
        Me.lblNameExist.Text = "Tran. Head Name already Exists"
        '
        'lblNameExistColor
        '
        Me.lblNameExistColor.BackColor = System.Drawing.Color.Red
        Me.lblNameExistColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNameExistColor.Location = New System.Drawing.Point(549, 57)
        Me.lblNameExistColor.Name = "lblNameExistColor"
        Me.lblNameExistColor.Size = New System.Drawing.Size(49, 15)
        Me.lblNameExistColor.TabIndex = 104
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(268, 36)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(19, 44)
        Me.objLine1.TabIndex = 16
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(422, 36)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(19, 46)
        Me.objLine2.TabIndex = 17
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(293, 57)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(123, 17)
        Me.radApplytoChecked.TabIndex = 15
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Checked = True
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(293, 34)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(123, 17)
        Me.radApplytoAll.TabIndex = 14
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        '
        'btnSet
        '
        Me.btnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSet.BackColor = System.Drawing.Color.White
        Me.btnSet.BackgroundImage = CType(resources.GetObject("btnSet.BackgroundImage"), System.Drawing.Image)
        Me.btnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSet.BorderColor = System.Drawing.Color.Empty
        Me.btnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSet.FlatAppearance.BorderSize = 0
        Me.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSet.ForeColor = System.Drawing.Color.Black
        Me.btnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSet.GradientForeColor = System.Drawing.Color.Black
        Me.btnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Location = New System.Drawing.Point(447, 41)
        Me.btnSet.Name = "btnSet"
        Me.btnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Size = New System.Drawing.Size(88, 30)
        Me.btnSet.TabIndex = 18
        Me.btnSet.Text = "&Set"
        Me.btnSet.UseVisualStyleBackColor = False
        '
        'cboTypeOfId
        '
        Me.cboTypeOfId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOfId.FormattingEnabled = True
        Me.cboTypeOfId.Location = New System.Drawing.Point(102, 61)
        Me.cboTypeOfId.Name = "cboTypeOfId"
        Me.cboTypeOfId.Size = New System.Drawing.Size(160, 21)
        Me.cboTypeOfId.TabIndex = 10
        '
        'lblTypeOfId
        '
        Me.lblTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOfId.Location = New System.Drawing.Point(8, 63)
        Me.lblTypeOfId.Name = "lblTypeOfId"
        Me.lblTypeOfId.Size = New System.Drawing.Size(88, 17)
        Me.lblTypeOfId.TabIndex = 9
        Me.lblTypeOfId.Text = "Type Of"
        Me.lblTypeOfId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHeadTypeId
        '
        Me.cboHeadTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadTypeId.FormattingEnabled = True
        Me.cboHeadTypeId.Location = New System.Drawing.Point(102, 34)
        Me.cboHeadTypeId.Name = "cboHeadTypeId"
        Me.cboHeadTypeId.Size = New System.Drawing.Size(160, 21)
        Me.cboHeadTypeId.TabIndex = 8
        '
        'lblHeadTypeId
        '
        Me.lblHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadTypeId.Location = New System.Drawing.Point(8, 36)
        Me.lblHeadTypeId.Name = "lblHeadTypeId"
        Me.lblHeadTypeId.Size = New System.Drawing.Size(88, 17)
        Me.lblHeadTypeId.TabIndex = 7
        Me.lblHeadTypeId.Text = "Head Type"
        Me.lblHeadTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkDgCheckAll)
        Me.Panel2.Controls.Add(Me.dgvNewTranHead)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(12, 101)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(785, 349)
        Me.Panel2.TabIndex = 2
        '
        'chkDgCheckAll
        '
        Me.chkDgCheckAll.AutoSize = True
        Me.chkDgCheckAll.Location = New System.Drawing.Point(8, 4)
        Me.chkDgCheckAll.Name = "chkDgCheckAll"
        Me.chkDgCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.chkDgCheckAll.TabIndex = 17
        Me.chkDgCheckAll.UseVisualStyleBackColor = True
        '
        'dgvNewTranHead
        '
        Me.dgvNewTranHead.AllowUserToAddRows = False
        Me.dgvNewTranHead.AllowUserToDeleteRows = False
        Me.dgvNewTranHead.AllowUserToResizeRows = False
        Me.dgvNewTranHead.BackgroundColor = System.Drawing.Color.White
        Me.dgvNewTranHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNewTranHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhCheck, Me.dgcolhTranCode, Me.dgcolhTranName, Me.dgcolhFormula, Me.dgcolhHeadType, Me.dgcolhTypeOF, Me.dgcolhCalctype, Me.dgcolhRowType})
        Me.dgvNewTranHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvNewTranHead.Location = New System.Drawing.Point(0, 0)
        Me.dgvNewTranHead.MultiSelect = False
        Me.dgvNewTranHead.Name = "dgvNewTranHead"
        Me.dgvNewTranHead.RowHeadersVisible = False
        Me.dgvNewTranHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNewTranHead.Size = New System.Drawing.Size(785, 349)
        Me.dgvNewTranHead.TabIndex = 6
        '
        'dgcolhCheck
        '
        Me.dgcolhCheck.HeaderText = ""
        Me.dgcolhCheck.Name = "dgcolhCheck"
        Me.dgcolhCheck.Width = 25
        '
        'dgcolhTranCode
        '
        Me.dgcolhTranCode.HeaderText = "New Tran. Code"
        Me.dgcolhTranCode.Name = "dgcolhTranCode"
        Me.dgcolhTranCode.ReadOnly = True
        Me.dgcolhTranCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranCode.Width = 95
        '
        'dgcolhTranName
        '
        Me.dgcolhTranName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhTranName.HeaderText = "New Tran. Head Name"
        Me.dgcolhTranName.Name = "dgcolhTranName"
        Me.dgcolhTranName.ReadOnly = True
        Me.dgcolhTranName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhFormula
        '
        Me.dgcolhFormula.HeaderText = "New Formula"
        Me.dgcolhFormula.Name = "dgcolhFormula"
        Me.dgcolhFormula.ReadOnly = True
        Me.dgcolhFormula.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFormula.Width = 190
        '
        'dgcolhHeadType
        '
        Me.dgcolhHeadType.HeaderText = "Head Type"
        Me.dgcolhHeadType.Name = "dgcolhHeadType"
        Me.dgcolhHeadType.ReadOnly = True
        Me.dgcolhHeadType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTypeOF
        '
        Me.dgcolhTypeOF.HeaderText = "Type Of"
        Me.dgcolhTypeOF.Name = "dgcolhTypeOF"
        Me.dgcolhTypeOF.ReadOnly = True
        Me.dgcolhTypeOF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCalctype
        '
        Me.dgcolhCalctype.HeaderText = "Calculation Type"
        Me.dgcolhCalctype.Name = "dgcolhCalctype"
        Me.dgcolhCalctype.ReadOnly = True
        Me.dgcolhCalctype.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRowType
        '
        Me.dgcolhRowType.HeaderText = "rowtype"
        Me.dgcolhRowType.Name = "dgcolhRowType"
        Me.dgcolhRowType.ReadOnly = True
        Me.dgcolhRowType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRowType.Visible = False
        '
        'EZeeLightButton1
        '
        Me.EZeeLightButton1.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton1.BackgroundImage = CType(resources.GetObject("EZeeLightButton1.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton1.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton1.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton1.Enabled = False
        Me.EZeeLightButton1.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton1.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton1.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton1.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.Location = New System.Drawing.Point(374, 388)
        Me.EZeeLightButton1.Name = "EZeeLightButton1"
        Me.EZeeLightButton1.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton1.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton1.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton1.TabIndex = 6
        Me.EZeeLightButton1.Text = "Back"
        Me.EZeeLightButton1.UseVisualStyleBackColor = False
        '
        'EZeeLightButton2
        '
        Me.EZeeLightButton2.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton2.BackgroundImage = CType(resources.GetObject("EZeeLightButton2.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton2.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.EZeeLightButton2.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton2.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton2.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton2.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.Location = New System.Drawing.Point(534, 388)
        Me.EZeeLightButton2.Name = "EZeeLightButton2"
        Me.EZeeLightButton2.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton2.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton2.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton2.TabIndex = 5
        Me.EZeeLightButton2.Text = "Cancel"
        Me.EZeeLightButton2.UseVisualStyleBackColor = False
        '
        'EZeeLightButton3
        '
        Me.EZeeLightButton3.BackColor = System.Drawing.Color.White
        Me.EZeeLightButton3.BackgroundImage = CType(resources.GetObject("EZeeLightButton3.BackgroundImage"), System.Drawing.Image)
        Me.EZeeLightButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.EZeeLightButton3.BorderColor = System.Drawing.Color.Empty
        Me.EZeeLightButton3.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.EZeeLightButton3.FlatAppearance.BorderSize = 0
        Me.EZeeLightButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EZeeLightButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLightButton3.ForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeLightButton3.GradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton3.HoverGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.Location = New System.Drawing.Point(450, 388)
        Me.EZeeLightButton3.Name = "EZeeLightButton3"
        Me.EZeeLightButton3.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.EZeeLightButton3.PressedGradientForeColor = System.Drawing.Color.Black
        Me.EZeeLightButton3.Size = New System.Drawing.Size(77, 29)
        Me.EZeeLightButton3.TabIndex = 4
        Me.EZeeLightButton3.Text = "Next"
        Me.EZeeLightButton3.UseVisualStyleBackColor = False
        '
        'wpConfiguration
        '
        Me.wpConfiguration.Controls.Add(Me.lnTranHeadCode)
        Me.wpConfiguration.Controls.Add(Me.lblCreateCommonFormula)
        Me.wpConfiguration.Controls.Add(Me.gbPrefixSuffix)
        Me.wpConfiguration.Controls.Add(Me.lnTranHeadName)
        Me.wpConfiguration.Controls.Add(Me.gbFindReplace)
        Me.wpConfiguration.Controls.Add(Me.gbCommonFormula)
        Me.wpConfiguration.Controls.Add(Me.PictureBox2)
        Me.wpConfiguration.Location = New System.Drawing.Point(0, 0)
        Me.wpConfiguration.Name = "wpConfiguration"
        Me.wpConfiguration.Size = New System.Drawing.Size(809, 460)
        Me.wpConfiguration.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpConfiguration.TabIndex = 10
        '
        'lnTranHeadCode
        '
        Me.lnTranHeadCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnTranHeadCode.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnTranHeadCode.Location = New System.Drawing.Point(322, 170)
        Me.lnTranHeadCode.Name = "lnTranHeadCode"
        Me.lnTranHeadCode.Size = New System.Drawing.Size(366, 20)
        Me.lnTranHeadCode.TabIndex = 261
        Me.lnTranHeadCode.Text = "New Transaction Head Code Setting"
        '
        'lblCreateCommonFormula
        '
        Me.lblCreateCommonFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreateCommonFormula.Location = New System.Drawing.Point(322, 17)
        Me.lblCreateCommonFormula.Name = "lblCreateCommonFormula"
        Me.lblCreateCommonFormula.Size = New System.Drawing.Size(392, 15)
        Me.lblCreateCommonFormula.TabIndex = 260
        Me.lblCreateCommonFormula.Text = "Create common Formula and define new transaction head name."
        '
        'gbPrefixSuffix
        '
        Me.gbPrefixSuffix.BorderColor = System.Drawing.Color.Black
        Me.gbPrefixSuffix.Checked = False
        Me.gbPrefixSuffix.CollapseAllExceptThis = False
        Me.gbPrefixSuffix.CollapsedHoverImage = Nothing
        Me.gbPrefixSuffix.CollapsedNormalImage = Nothing
        Me.gbPrefixSuffix.CollapsedPressedImage = Nothing
        Me.gbPrefixSuffix.CollapseOnLoad = False
        Me.gbPrefixSuffix.Controls.Add(Me.txtPrefixSuffix)
        Me.gbPrefixSuffix.Controls.Add(Me.chkApplySametoHeadName)
        Me.gbPrefixSuffix.Controls.Add(Me.radSufffix)
        Me.gbPrefixSuffix.Controls.Add(Me.radPrefix)
        Me.gbPrefixSuffix.ExpandedHoverImage = Nothing
        Me.gbPrefixSuffix.ExpandedNormalImage = Nothing
        Me.gbPrefixSuffix.ExpandedPressedImage = Nothing
        Me.gbPrefixSuffix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPrefixSuffix.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPrefixSuffix.HeaderHeight = 25
        Me.gbPrefixSuffix.HeaderMessage = ""
        Me.gbPrefixSuffix.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPrefixSuffix.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPrefixSuffix.HeightOnCollapse = 0
        Me.gbPrefixSuffix.LeftTextSpace = 0
        Me.gbPrefixSuffix.Location = New System.Drawing.Point(369, 193)
        Me.gbPrefixSuffix.Name = "gbPrefixSuffix"
        Me.gbPrefixSuffix.OpenHeight = 90
        Me.gbPrefixSuffix.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPrefixSuffix.ShowBorder = True
        Me.gbPrefixSuffix.ShowCheckBox = False
        Me.gbPrefixSuffix.ShowCollapseButton = False
        Me.gbPrefixSuffix.ShowDefaultBorderColor = True
        Me.gbPrefixSuffix.ShowDownButton = False
        Me.gbPrefixSuffix.ShowHeader = True
        Me.gbPrefixSuffix.Size = New System.Drawing.Size(354, 95)
        Me.gbPrefixSuffix.TabIndex = 259
        Me.gbPrefixSuffix.Temp = 0
        Me.gbPrefixSuffix.Text = "Prefix or Suffix to selected Heads"
        Me.gbPrefixSuffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPrefixSuffix
        '
        Me.txtPrefixSuffix.Flags = 0
        Me.txtPrefixSuffix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrefixSuffix.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPrefixSuffix.Location = New System.Drawing.Point(24, 60)
        Me.txtPrefixSuffix.MaxLength = 10
        Me.txtPrefixSuffix.Name = "txtPrefixSuffix"
        Me.txtPrefixSuffix.Size = New System.Drawing.Size(135, 21)
        Me.txtPrefixSuffix.TabIndex = 265
        '
        'chkApplySametoHeadName
        '
        Me.chkApplySametoHeadName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkApplySametoHeadName.Location = New System.Drawing.Point(184, 37)
        Me.chkApplySametoHeadName.Name = "chkApplySametoHeadName"
        Me.chkApplySametoHeadName.Size = New System.Drawing.Size(161, 33)
        Me.chkApplySametoHeadName.TabIndex = 264
        Me.chkApplySametoHeadName.Text = "Apply same setting to New Transaction Head Name"
        Me.chkApplySametoHeadName.UseVisualStyleBackColor = True
        '
        'radSufffix
        '
        Me.radSufffix.AutoSize = True
        Me.radSufffix.Checked = True
        Me.radSufffix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSufffix.Location = New System.Drawing.Point(106, 37)
        Me.radSufffix.Name = "radSufffix"
        Me.radSufffix.Size = New System.Drawing.Size(53, 17)
        Me.radSufffix.TabIndex = 260
        Me.radSufffix.TabStop = True
        Me.radSufffix.Text = "Suffix"
        Me.radSufffix.UseVisualStyleBackColor = True
        '
        'radPrefix
        '
        Me.radPrefix.AutoSize = True
        Me.radPrefix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radPrefix.Location = New System.Drawing.Point(24, 37)
        Me.radPrefix.Name = "radPrefix"
        Me.radPrefix.Size = New System.Drawing.Size(53, 17)
        Me.radPrefix.TabIndex = 259
        Me.radPrefix.Text = "Prefix"
        Me.radPrefix.UseVisualStyleBackColor = True
        '
        'lnTranHeadName
        '
        Me.lnTranHeadName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnTranHeadName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnTranHeadName.Location = New System.Drawing.Point(322, 306)
        Me.lnTranHeadName.Name = "lnTranHeadName"
        Me.lnTranHeadName.Size = New System.Drawing.Size(366, 20)
        Me.lnTranHeadName.TabIndex = 258
        Me.lnTranHeadName.Text = "New Transaction Head Name Setting"
        '
        'gbFindReplace
        '
        Me.gbFindReplace.BorderColor = System.Drawing.Color.Black
        Me.gbFindReplace.Checked = False
        Me.gbFindReplace.CollapseAllExceptThis = False
        Me.gbFindReplace.CollapsedHoverImage = Nothing
        Me.gbFindReplace.CollapsedNormalImage = Nothing
        Me.gbFindReplace.CollapsedPressedImage = Nothing
        Me.gbFindReplace.CollapseOnLoad = False
        Me.gbFindReplace.Controls.Add(Me.txtReplace)
        Me.gbFindReplace.Controls.Add(Me.txtFind)
        Me.gbFindReplace.Controls.Add(Me.lblReplace)
        Me.gbFindReplace.Controls.Add(Me.lblFind)
        Me.gbFindReplace.ExpandedHoverImage = Nothing
        Me.gbFindReplace.ExpandedNormalImage = Nothing
        Me.gbFindReplace.ExpandedPressedImage = Nothing
        Me.gbFindReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFindReplace.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFindReplace.HeaderHeight = 25
        Me.gbFindReplace.HeaderMessage = ""
        Me.gbFindReplace.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFindReplace.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFindReplace.HeightOnCollapse = 0
        Me.gbFindReplace.LeftTextSpace = 0
        Me.gbFindReplace.Location = New System.Drawing.Point(366, 329)
        Me.gbFindReplace.Name = "gbFindReplace"
        Me.gbFindReplace.OpenHeight = 90
        Me.gbFindReplace.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFindReplace.ShowBorder = True
        Me.gbFindReplace.ShowCheckBox = True
        Me.gbFindReplace.ShowCollapseButton = False
        Me.gbFindReplace.ShowDefaultBorderColor = True
        Me.gbFindReplace.ShowDownButton = False
        Me.gbFindReplace.ShowHeader = True
        Me.gbFindReplace.Size = New System.Drawing.Size(354, 94)
        Me.gbFindReplace.TabIndex = 257
        Me.gbFindReplace.Temp = 0
        Me.gbFindReplace.Text = "Find and replace in Selected Transaction Heads"
        Me.gbFindReplace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtReplace
        '
        Me.txtReplace.Flags = 0
        Me.txtReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReplace.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReplace.Location = New System.Drawing.Point(116, 60)
        Me.txtReplace.Name = "txtReplace"
        Me.txtReplace.Size = New System.Drawing.Size(220, 21)
        Me.txtReplace.TabIndex = 267
        '
        'txtFind
        '
        Me.txtFind.Flags = 0
        Me.txtFind.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFind.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFind.Location = New System.Drawing.Point(116, 33)
        Me.txtFind.Name = "txtFind"
        Me.txtFind.Size = New System.Drawing.Size(220, 21)
        Me.txtFind.TabIndex = 266
        '
        'lblReplace
        '
        Me.lblReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReplace.Location = New System.Drawing.Point(9, 63)
        Me.lblReplace.Name = "lblReplace"
        Me.lblReplace.Size = New System.Drawing.Size(102, 15)
        Me.lblReplace.TabIndex = 254
        Me.lblReplace.Text = "Replace"
        '
        'lblFind
        '
        Me.lblFind.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFind.Location = New System.Drawing.Point(8, 36)
        Me.lblFind.Name = "lblFind"
        Me.lblFind.Size = New System.Drawing.Size(102, 15)
        Me.lblFind.TabIndex = 253
        Me.lblFind.Text = "Find"
        '
        'gbCommonFormula
        '
        Me.gbCommonFormula.BorderColor = System.Drawing.Color.Black
        Me.gbCommonFormula.Checked = False
        Me.gbCommonFormula.CollapseAllExceptThis = False
        Me.gbCommonFormula.CollapsedHoverImage = Nothing
        Me.gbCommonFormula.CollapsedNormalImage = Nothing
        Me.gbCommonFormula.CollapsedPressedImage = Nothing
        Me.gbCommonFormula.CollapseOnLoad = False
        Me.gbCommonFormula.Controls.Add(Me.lblFunction)
        Me.gbCommonFormula.Controls.Add(Me.cboFunction)
        Me.gbCommonFormula.Controls.Add(Me.txtFormula)
        Me.gbCommonFormula.Controls.Add(Me.objbtnAddComputation)
        Me.gbCommonFormula.Controls.Add(Me.lblFormula)
        Me.gbCommonFormula.ExpandedHoverImage = Nothing
        Me.gbCommonFormula.ExpandedNormalImage = Nothing
        Me.gbCommonFormula.ExpandedPressedImage = Nothing
        Me.gbCommonFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCommonFormula.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCommonFormula.HeaderHeight = 25
        Me.gbCommonFormula.HeaderMessage = ""
        Me.gbCommonFormula.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCommonFormula.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCommonFormula.HeightOnCollapse = 0
        Me.gbCommonFormula.LeftTextSpace = 0
        Me.gbCommonFormula.Location = New System.Drawing.Point(369, 45)
        Me.gbCommonFormula.Name = "gbCommonFormula"
        Me.gbCommonFormula.OpenHeight = 90
        Me.gbCommonFormula.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCommonFormula.ShowBorder = True
        Me.gbCommonFormula.ShowCheckBox = False
        Me.gbCommonFormula.ShowCollapseButton = False
        Me.gbCommonFormula.ShowDefaultBorderColor = True
        Me.gbCommonFormula.ShowDownButton = False
        Me.gbCommonFormula.ShowHeader = True
        Me.gbCommonFormula.Size = New System.Drawing.Size(354, 98)
        Me.gbCommonFormula.TabIndex = 249
        Me.gbCommonFormula.Temp = 0
        Me.gbCommonFormula.Text = "Common Formula"
        Me.gbCommonFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFunction
        '
        Me.lblFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFunction.Location = New System.Drawing.Point(8, 36)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(80, 15)
        Me.lblFunction.TabIndex = 252
        Me.lblFunction.Text = "Function"
        '
        'cboFunction
        '
        Me.cboFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFunction.FormattingEnabled = True
        Me.cboFunction.Location = New System.Drawing.Point(94, 33)
        Me.cboFunction.Name = "cboFunction"
        Me.cboFunction.Size = New System.Drawing.Size(160, 21)
        Me.cboFunction.TabIndex = 251
        '
        'txtFormula
        '
        Me.txtFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormula.Location = New System.Drawing.Point(94, 60)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.ReadOnly = True
        Me.txtFormula.Size = New System.Drawing.Size(229, 21)
        Me.txtFormula.TabIndex = 253
        '
        'objbtnAddComputation
        '
        Me.objbtnAddComputation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddComputation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddComputation.BorderSelected = False
        Me.objbtnAddComputation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddComputation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddComputation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddComputation.Location = New System.Drawing.Point(329, 60)
        Me.objbtnAddComputation.Name = "objbtnAddComputation"
        Me.objbtnAddComputation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddComputation.TabIndex = 255
        '
        'lblFormula
        '
        Me.lblFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormula.Location = New System.Drawing.Point(9, 63)
        Me.lblFormula.Name = "lblFormula"
        Me.lblFormula.Size = New System.Drawing.Size(80, 15)
        Me.lblFormula.TabIndex = 254
        Me.lblFormula.Text = "Formula"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.PictureBox2.Location = New System.Drawing.Point(12, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(251, 444)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 248
        Me.PictureBox2.TabStop = False
        '
        'frmGenerateTranHead
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 508)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGenerateTranHead"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GenerateTransaction Head Wizard"
        Me.pnlMain.ResumeLayout(False)
        Me.wzGenerateTranHead.ResumeLayout(False)
        Me.wpTranHeadList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTranHeadList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.wpWelcome.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpFinish.ResumeLayout(False)
        Me.pnlFinish.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpNewTranHead.ResumeLayout(False)
        Me.gbFileInfo.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvNewTranHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wpConfiguration.ResumeLayout(False)
        Me.gbPrefixSuffix.ResumeLayout(False)
        Me.gbPrefixSuffix.PerformLayout()
        Me.gbFindReplace.ResumeLayout(False)
        Me.gbFindReplace.PerformLayout()
        Me.gbCommonFormula.ResumeLayout(False)
        Me.gbCommonFormula.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents wzGenerateTranHead As eZee.Common.eZeeWizard
    Friend WithEvents wpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents pnlFinish As System.Windows.Forms.Panel
    Friend WithEvents lblFinish As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents wpNewTranHead As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCalcTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodeExist As System.Windows.Forms.Label
    Friend WithEvents lblCodeExistColor As System.Windows.Forms.Label
    Friend WithEvents lblNameExist As System.Windows.Forms.Label
    Friend WithEvents lblNameExistColor As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents btnSet As eZee.Common.eZeeLightButton
    Friend WithEvents cboTypeOfId As System.Windows.Forms.ComboBox
    Friend WithEvents lblTypeOfId As System.Windows.Forms.Label
    Friend WithEvents cboHeadTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblHeadTypeId As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents chkDgCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvNewTranHead As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhTranCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFormula As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHeadType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTypeOF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCalctype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRowType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EZeeLightButton1 As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLightButton2 As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeLightButton3 As eZee.Common.eZeeLightButton
    Friend WithEvents wpConfiguration As eZee.Common.eZeeWizardPage
    Friend WithEvents lnTranHeadCode As eZee.Common.eZeeLine
    Friend WithEvents lblCreateCommonFormula As System.Windows.Forms.Label
    Friend WithEvents gbPrefixSuffix As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPrefixSuffix As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkApplySametoHeadName As System.Windows.Forms.CheckBox
    Friend WithEvents radSufffix As System.Windows.Forms.RadioButton
    Friend WithEvents radPrefix As System.Windows.Forms.RadioButton
    Friend WithEvents lnTranHeadName As eZee.Common.eZeeLine
    Friend WithEvents gbFindReplace As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtReplace As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFind As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblReplace As System.Windows.Forms.Label
    Friend WithEvents lblFind As System.Windows.Forms.Label
    Friend WithEvents gbCommonFormula As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents cboFunction As System.Windows.Forms.ComboBox
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents objbtnAddComputation As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFormula As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents wpTranHeadList As eZee.Common.eZeeWizardPage
    Friend WithEvents lblHeadSelection As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEffectiveFrom As System.Windows.Forms.Label
    Friend WithEvents cboCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents gbTranHeadList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvTrnHeadList As System.Windows.Forms.ListView
    Friend WithEvents colhHeadCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHeadCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhHeadName As System.Windows.Forms.ColumnHeader
    Friend WithEvents wpWelcome As eZee.Common.eZeeWizardPage
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblMessageWizard As System.Windows.Forms.Label
    Friend WithEvents lblGenerateWizard As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
End Class
