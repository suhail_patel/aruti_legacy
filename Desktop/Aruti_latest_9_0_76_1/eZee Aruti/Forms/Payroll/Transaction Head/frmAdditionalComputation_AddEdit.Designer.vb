﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdditionalComputation_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdditionalComputation_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbIFFunction = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnClearIFLogical2 = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClearValueFALSE = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClearValueTRUE = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClearIFLogical1 = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddIFLogical2 = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtIFLogical2 = New System.Windows.Forms.TextBox
        Me.cboComparision = New System.Windows.Forms.ComboBox
        Me.lblValueFALSE = New System.Windows.Forms.Label
        Me.lblValueTRUE = New System.Windows.Forms.Label
        Me.lblLogicalTest = New System.Windows.Forms.Label
        Me.btnAddValueFALSE = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtValueFALSE = New System.Windows.Forms.TextBox
        Me.btnAddValueTRUE = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtValueTRUE = New System.Windows.Forms.TextBox
        Me.btnAddIFLogical1 = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtIFLogical1 = New System.Windows.Forms.TextBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnIFCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnIFOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAddComputation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAnd = New System.Windows.Forms.Label
        Me.txtConstantValueFrom = New eZee.TextBox.NumericTextBox
        Me.cboSubFunction2 = New System.Windows.Forms.ComboBox
        Me.txtConstantValueTo = New eZee.TextBox.NumericTextBox
        Me.txtConstantValue = New eZee.TextBox.NumericTextBox
        Me.cboSubFunction = New System.Windows.Forms.ComboBox
        Me.cboCRExpense = New System.Windows.Forms.ComboBox
        Me.cboCommonMaster = New System.Windows.Forms.ComboBox
        Me.cboSalaryHead = New System.Windows.Forms.ComboBox
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFunction = New eZee.Common.eZeeGradientButton
        Me.cboActivity = New System.Windows.Forms.ComboBox
        Me.cboMeasurement = New System.Windows.Forms.ComboBox
        Me.lblSimpleSlabPeriod = New System.Windows.Forms.Label
        Me.cboEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.cboPaidUnpaidLeave = New System.Windows.Forms.ComboBox
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.cboDefaultValue = New System.Windows.Forms.ComboBox
        Me.lblDefaultValue = New System.Windows.Forms.Label
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.lblFormula = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.pnlComputation = New System.Windows.Forms.Panel
        Me.lvFormula = New eZee.Common.eZeeListView(Me.components)
        Me.colhFunction = New System.Windows.Forms.ColumnHeader
        Me.colhFormula = New System.Windows.Forms.ColumnHeader
        Me.colhDefaultValue = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhEndDate = New System.Windows.Forms.ColumnHeader
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.cboFunction = New System.Windows.Forms.ComboBox
        Me.lblFunction = New System.Windows.Forms.Label
        Me.lblTrnHead_Leave = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbIFFunction.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbAddComputation.SuspendLayout()
        Me.pnlComputation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbIFFunction)
        Me.pnlMainInfo.Controls.Add(Me.gbAddComputation)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(480, 520)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbIFFunction
        '
        Me.gbIFFunction.BackColor = System.Drawing.SystemColors.Control
        Me.gbIFFunction.BorderColor = System.Drawing.SystemColors.ActiveCaption
        Me.gbIFFunction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.gbIFFunction.Checked = False
        Me.gbIFFunction.CollapseAllExceptThis = False
        Me.gbIFFunction.CollapsedHoverImage = Nothing
        Me.gbIFFunction.CollapsedNormalImage = Nothing
        Me.gbIFFunction.CollapsedPressedImage = Nothing
        Me.gbIFFunction.CollapseOnLoad = False
        Me.gbIFFunction.Controls.Add(Me.btnClearIFLogical2)
        Me.gbIFFunction.Controls.Add(Me.btnClearValueFALSE)
        Me.gbIFFunction.Controls.Add(Me.btnClearValueTRUE)
        Me.gbIFFunction.Controls.Add(Me.btnClearIFLogical1)
        Me.gbIFFunction.Controls.Add(Me.btnAddIFLogical2)
        Me.gbIFFunction.Controls.Add(Me.txtIFLogical2)
        Me.gbIFFunction.Controls.Add(Me.cboComparision)
        Me.gbIFFunction.Controls.Add(Me.lblValueFALSE)
        Me.gbIFFunction.Controls.Add(Me.lblValueTRUE)
        Me.gbIFFunction.Controls.Add(Me.lblLogicalTest)
        Me.gbIFFunction.Controls.Add(Me.btnAddValueFALSE)
        Me.gbIFFunction.Controls.Add(Me.txtValueFALSE)
        Me.gbIFFunction.Controls.Add(Me.btnAddValueTRUE)
        Me.gbIFFunction.Controls.Add(Me.txtValueTRUE)
        Me.gbIFFunction.Controls.Add(Me.btnAddIFLogical1)
        Me.gbIFFunction.Controls.Add(Me.txtIFLogical1)
        Me.gbIFFunction.Controls.Add(Me.EZeeFooter1)
        Me.gbIFFunction.ExpandedHoverImage = Nothing
        Me.gbIFFunction.ExpandedNormalImage = Nothing
        Me.gbIFFunction.ExpandedPressedImage = Nothing
        Me.gbIFFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbIFFunction.GradientColor = System.Drawing.SystemColors.ControlLight
        Me.gbIFFunction.HeaderHeight = 25
        Me.gbIFFunction.HeaderMessage = ""
        Me.gbIFFunction.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbIFFunction.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbIFFunction.HeightOnCollapse = 0
        Me.gbIFFunction.LeftTextSpace = 0
        Me.gbIFFunction.Location = New System.Drawing.Point(10, 157)
        Me.gbIFFunction.Name = "gbIFFunction"
        Me.gbIFFunction.OpenHeight = 300
        Me.gbIFFunction.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbIFFunction.ShowBorder = True
        Me.gbIFFunction.ShowCheckBox = False
        Me.gbIFFunction.ShowCollapseButton = False
        Me.gbIFFunction.ShowDefaultBorderColor = True
        Me.gbIFFunction.ShowDownButton = False
        Me.gbIFFunction.ShowHeader = True
        Me.gbIFFunction.Size = New System.Drawing.Size(461, 245)
        Me.gbIFFunction.TabIndex = 1
        Me.gbIFFunction.Temp = 0
        Me.gbIFFunction.Text = "IF Function Arguments"
        Me.gbIFFunction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.gbIFFunction.Visible = False
        '
        'btnClearIFLogical2
        '
        Me.btnClearIFLogical2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClearIFLogical2.BackColor = System.Drawing.Color.White
        Me.btnClearIFLogical2.BackgroundImage = CType(resources.GetObject("btnClearIFLogical2.BackgroundImage"), System.Drawing.Image)
        Me.btnClearIFLogical2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClearIFLogical2.BorderColor = System.Drawing.Color.Empty
        Me.btnClearIFLogical2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClearIFLogical2.FlatAppearance.BorderSize = 0
        Me.btnClearIFLogical2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearIFLogical2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearIFLogical2.ForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClearIFLogical2.GradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical2.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearIFLogical2.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical2.Location = New System.Drawing.Point(383, 83)
        Me.btnClearIFLogical2.Name = "btnClearIFLogical2"
        Me.btnClearIFLogical2.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearIFLogical2.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical2.Size = New System.Drawing.Size(60, 30)
        Me.btnClearIFLogical2.TabIndex = 11
        Me.btnClearIFLogical2.Text = "&Clear"
        Me.btnClearIFLogical2.UseVisualStyleBackColor = True
        '
        'btnClearValueFALSE
        '
        Me.btnClearValueFALSE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClearValueFALSE.BackColor = System.Drawing.Color.White
        Me.btnClearValueFALSE.BackgroundImage = CType(resources.GetObject("btnClearValueFALSE.BackgroundImage"), System.Drawing.Image)
        Me.btnClearValueFALSE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClearValueFALSE.BorderColor = System.Drawing.Color.Empty
        Me.btnClearValueFALSE.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClearValueFALSE.FlatAppearance.BorderSize = 0
        Me.btnClearValueFALSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearValueFALSE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearValueFALSE.ForeColor = System.Drawing.Color.Black
        Me.btnClearValueFALSE.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClearValueFALSE.GradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueFALSE.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearValueFALSE.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueFALSE.Location = New System.Drawing.Point(383, 155)
        Me.btnClearValueFALSE.Name = "btnClearValueFALSE"
        Me.btnClearValueFALSE.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearValueFALSE.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueFALSE.Size = New System.Drawing.Size(60, 30)
        Me.btnClearValueFALSE.TabIndex = 0
        Me.btnClearValueFALSE.Text = "&Clear"
        Me.btnClearValueFALSE.UseVisualStyleBackColor = True
        '
        'btnClearValueTRUE
        '
        Me.btnClearValueTRUE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClearValueTRUE.BackColor = System.Drawing.Color.White
        Me.btnClearValueTRUE.BackgroundImage = CType(resources.GetObject("btnClearValueTRUE.BackgroundImage"), System.Drawing.Image)
        Me.btnClearValueTRUE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClearValueTRUE.BorderColor = System.Drawing.Color.Empty
        Me.btnClearValueTRUE.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClearValueTRUE.FlatAppearance.BorderSize = 0
        Me.btnClearValueTRUE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearValueTRUE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearValueTRUE.ForeColor = System.Drawing.Color.Black
        Me.btnClearValueTRUE.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClearValueTRUE.GradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueTRUE.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearValueTRUE.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueTRUE.Location = New System.Drawing.Point(383, 119)
        Me.btnClearValueTRUE.Name = "btnClearValueTRUE"
        Me.btnClearValueTRUE.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearValueTRUE.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClearValueTRUE.Size = New System.Drawing.Size(60, 30)
        Me.btnClearValueTRUE.TabIndex = 14
        Me.btnClearValueTRUE.Text = "&Clear"
        Me.btnClearValueTRUE.UseVisualStyleBackColor = True
        '
        'btnClearIFLogical1
        '
        Me.btnClearIFLogical1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClearIFLogical1.BackColor = System.Drawing.Color.White
        Me.btnClearIFLogical1.BackgroundImage = CType(resources.GetObject("btnClearIFLogical1.BackgroundImage"), System.Drawing.Image)
        Me.btnClearIFLogical1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClearIFLogical1.BorderColor = System.Drawing.Color.Empty
        Me.btnClearIFLogical1.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClearIFLogical1.FlatAppearance.BorderSize = 0
        Me.btnClearIFLogical1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearIFLogical1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearIFLogical1.ForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClearIFLogical1.GradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical1.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearIFLogical1.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical1.Location = New System.Drawing.Point(383, 29)
        Me.btnClearIFLogical1.Name = "btnClearIFLogical1"
        Me.btnClearIFLogical1.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClearIFLogical1.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClearIFLogical1.Size = New System.Drawing.Size(60, 30)
        Me.btnClearIFLogical1.TabIndex = 7
        Me.btnClearIFLogical1.Text = "&Clear"
        Me.btnClearIFLogical1.UseVisualStyleBackColor = True
        '
        'btnAddIFLogical2
        '
        Me.btnAddIFLogical2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddIFLogical2.BackColor = System.Drawing.Color.White
        Me.btnAddIFLogical2.BackgroundImage = CType(resources.GetObject("btnAddIFLogical2.BackgroundImage"), System.Drawing.Image)
        Me.btnAddIFLogical2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddIFLogical2.BorderColor = System.Drawing.Color.Empty
        Me.btnAddIFLogical2.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddIFLogical2.FlatAppearance.BorderSize = 0
        Me.btnAddIFLogical2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddIFLogical2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddIFLogical2.ForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical2.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddIFLogical2.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical2.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddIFLogical2.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical2.Location = New System.Drawing.Point(317, 83)
        Me.btnAddIFLogical2.Name = "btnAddIFLogical2"
        Me.btnAddIFLogical2.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddIFLogical2.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical2.Size = New System.Drawing.Size(60, 30)
        Me.btnAddIFLogical2.TabIndex = 10
        Me.btnAddIFLogical2.Text = "&Add"
        Me.btnAddIFLogical2.UseVisualStyleBackColor = True
        '
        'txtIFLogical2
        '
        Me.txtIFLogical2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIFLogical2.Location = New System.Drawing.Point(104, 89)
        Me.txtIFLogical2.Name = "txtIFLogical2"
        Me.txtIFLogical2.ReadOnly = True
        Me.txtIFLogical2.Size = New System.Drawing.Size(206, 21)
        Me.txtIFLogical2.TabIndex = 9
        '
        'cboComparision
        '
        Me.cboComparision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComparision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComparision.FormattingEnabled = True
        Me.cboComparision.Items.AddRange(New Object() {"Select"})
        Me.cboComparision.Location = New System.Drawing.Point(104, 62)
        Me.cboComparision.Name = "cboComparision"
        Me.cboComparision.Size = New System.Drawing.Size(60, 21)
        Me.cboComparision.TabIndex = 8
        Me.cboComparision.Tag = ""
        '
        'lblValueFALSE
        '
        Me.lblValueFALSE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValueFALSE.Location = New System.Drawing.Point(7, 164)
        Me.lblValueFALSE.Name = "lblValueFALSE"
        Me.lblValueFALSE.Size = New System.Drawing.Size(91, 13)
        Me.lblValueFALSE.TabIndex = 14
        Me.lblValueFALSE.Text = "Value if FALSE"
        '
        'lblValueTRUE
        '
        Me.lblValueTRUE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValueTRUE.Location = New System.Drawing.Point(7, 128)
        Me.lblValueTRUE.Name = "lblValueTRUE"
        Me.lblValueTRUE.Size = New System.Drawing.Size(91, 13)
        Me.lblValueTRUE.TabIndex = 13
        Me.lblValueTRUE.Text = "Value if TRUE"
        '
        'lblLogicalTest
        '
        Me.lblLogicalTest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogicalTest.Location = New System.Drawing.Point(7, 65)
        Me.lblLogicalTest.Name = "lblLogicalTest"
        Me.lblLogicalTest.Size = New System.Drawing.Size(91, 13)
        Me.lblLogicalTest.TabIndex = 12
        Me.lblLogicalTest.Text = "Logical Test"
        '
        'btnAddValueFALSE
        '
        Me.btnAddValueFALSE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddValueFALSE.BackColor = System.Drawing.Color.White
        Me.btnAddValueFALSE.BackgroundImage = CType(resources.GetObject("btnAddValueFALSE.BackgroundImage"), System.Drawing.Image)
        Me.btnAddValueFALSE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddValueFALSE.BorderColor = System.Drawing.Color.Empty
        Me.btnAddValueFALSE.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddValueFALSE.FlatAppearance.BorderSize = 0
        Me.btnAddValueFALSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddValueFALSE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddValueFALSE.ForeColor = System.Drawing.Color.Black
        Me.btnAddValueFALSE.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddValueFALSE.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueFALSE.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddValueFALSE.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueFALSE.Location = New System.Drawing.Point(317, 155)
        Me.btnAddValueFALSE.Name = "btnAddValueFALSE"
        Me.btnAddValueFALSE.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddValueFALSE.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueFALSE.Size = New System.Drawing.Size(60, 30)
        Me.btnAddValueFALSE.TabIndex = 16
        Me.btnAddValueFALSE.Text = "&Add"
        Me.btnAddValueFALSE.UseVisualStyleBackColor = True
        '
        'txtValueFALSE
        '
        Me.txtValueFALSE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValueFALSE.Location = New System.Drawing.Point(104, 161)
        Me.txtValueFALSE.Name = "txtValueFALSE"
        Me.txtValueFALSE.ReadOnly = True
        Me.txtValueFALSE.Size = New System.Drawing.Size(206, 21)
        Me.txtValueFALSE.TabIndex = 15
        '
        'btnAddValueTRUE
        '
        Me.btnAddValueTRUE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddValueTRUE.BackColor = System.Drawing.Color.White
        Me.btnAddValueTRUE.BackgroundImage = CType(resources.GetObject("btnAddValueTRUE.BackgroundImage"), System.Drawing.Image)
        Me.btnAddValueTRUE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddValueTRUE.BorderColor = System.Drawing.Color.Empty
        Me.btnAddValueTRUE.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddValueTRUE.FlatAppearance.BorderSize = 0
        Me.btnAddValueTRUE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddValueTRUE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddValueTRUE.ForeColor = System.Drawing.Color.Black
        Me.btnAddValueTRUE.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddValueTRUE.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueTRUE.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddValueTRUE.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueTRUE.Location = New System.Drawing.Point(317, 119)
        Me.btnAddValueTRUE.Name = "btnAddValueTRUE"
        Me.btnAddValueTRUE.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddValueTRUE.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddValueTRUE.Size = New System.Drawing.Size(60, 30)
        Me.btnAddValueTRUE.TabIndex = 13
        Me.btnAddValueTRUE.Text = "&Add"
        Me.btnAddValueTRUE.UseVisualStyleBackColor = True
        '
        'txtValueTRUE
        '
        Me.txtValueTRUE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValueTRUE.Location = New System.Drawing.Point(104, 125)
        Me.txtValueTRUE.Name = "txtValueTRUE"
        Me.txtValueTRUE.ReadOnly = True
        Me.txtValueTRUE.Size = New System.Drawing.Size(206, 21)
        Me.txtValueTRUE.TabIndex = 12
        '
        'btnAddIFLogical1
        '
        Me.btnAddIFLogical1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddIFLogical1.BackColor = System.Drawing.Color.White
        Me.btnAddIFLogical1.BackgroundImage = CType(resources.GetObject("btnAddIFLogical1.BackgroundImage"), System.Drawing.Image)
        Me.btnAddIFLogical1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddIFLogical1.BorderColor = System.Drawing.Color.Empty
        Me.btnAddIFLogical1.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddIFLogical1.FlatAppearance.BorderSize = 0
        Me.btnAddIFLogical1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddIFLogical1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddIFLogical1.ForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddIFLogical1.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical1.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddIFLogical1.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical1.Location = New System.Drawing.Point(317, 29)
        Me.btnAddIFLogical1.Name = "btnAddIFLogical1"
        Me.btnAddIFLogical1.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddIFLogical1.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddIFLogical1.Size = New System.Drawing.Size(60, 30)
        Me.btnAddIFLogical1.TabIndex = 6
        Me.btnAddIFLogical1.Text = "&Add"
        Me.btnAddIFLogical1.UseVisualStyleBackColor = True
        '
        'txtIFLogical1
        '
        Me.txtIFLogical1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIFLogical1.Location = New System.Drawing.Point(104, 35)
        Me.txtIFLogical1.Name = "txtIFLogical1"
        Me.txtIFLogical1.ReadOnly = True
        Me.txtIFLogical1.Size = New System.Drawing.Size(206, 21)
        Me.txtIFLogical1.TabIndex = 5
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnIFCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnIFOk)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 193)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(459, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnIFCancel
        '
        Me.btnIFCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnIFCancel.BackColor = System.Drawing.Color.White
        Me.btnIFCancel.BackgroundImage = CType(resources.GetObject("btnIFCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnIFCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnIFCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnIFCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnIFCancel.FlatAppearance.BorderSize = 0
        Me.btnIFCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIFCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIFCancel.ForeColor = System.Drawing.Color.Black
        Me.btnIFCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnIFCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnIFCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIFCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnIFCancel.Location = New System.Drawing.Point(346, 10)
        Me.btnIFCancel.Name = "btnIFCancel"
        Me.btnIFCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIFCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnIFCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnIFCancel.TabIndex = 1
        Me.btnIFCancel.Text = "&Cancel"
        Me.btnIFCancel.UseVisualStyleBackColor = True
        '
        'btnIFOk
        '
        Me.btnIFOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnIFOk.BackColor = System.Drawing.Color.White
        Me.btnIFOk.BackgroundImage = CType(resources.GetObject("btnIFOk.BackgroundImage"), System.Drawing.Image)
        Me.btnIFOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnIFOk.BorderColor = System.Drawing.Color.Empty
        Me.btnIFOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnIFOk.FlatAppearance.BorderSize = 0
        Me.btnIFOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIFOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIFOk.ForeColor = System.Drawing.Color.Black
        Me.btnIFOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnIFOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnIFOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIFOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnIFOk.Location = New System.Drawing.Point(243, 10)
        Me.btnIFOk.Name = "btnIFOk"
        Me.btnIFOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnIFOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnIFOk.Size = New System.Drawing.Size(97, 30)
        Me.btnIFOk.TabIndex = 0
        Me.btnIFOk.Text = "&Ok"
        Me.btnIFOk.UseVisualStyleBackColor = True
        '
        'gbAddComputation
        '
        Me.gbAddComputation.BackColor = System.Drawing.Color.Transparent
        Me.gbAddComputation.BorderColor = System.Drawing.Color.Black
        Me.gbAddComputation.Checked = False
        Me.gbAddComputation.CollapseAllExceptThis = False
        Me.gbAddComputation.CollapsedHoverImage = Nothing
        Me.gbAddComputation.CollapsedNormalImage = Nothing
        Me.gbAddComputation.CollapsedPressedImage = Nothing
        Me.gbAddComputation.CollapseOnLoad = False
        Me.gbAddComputation.Controls.Add(Me.lblAnd)
        Me.gbAddComputation.Controls.Add(Me.txtConstantValueFrom)
        Me.gbAddComputation.Controls.Add(Me.cboSubFunction2)
        Me.gbAddComputation.Controls.Add(Me.txtConstantValueTo)
        Me.gbAddComputation.Controls.Add(Me.txtConstantValue)
        Me.gbAddComputation.Controls.Add(Me.cboSubFunction)
        Me.gbAddComputation.Controls.Add(Me.cboCRExpense)
        Me.gbAddComputation.Controls.Add(Me.cboCommonMaster)
        Me.gbAddComputation.Controls.Add(Me.cboSalaryHead)
        Me.gbAddComputation.Controls.Add(Me.cboShift)
        Me.gbAddComputation.Controls.Add(Me.objbtnSearchFunction)
        Me.gbAddComputation.Controls.Add(Me.cboActivity)
        Me.gbAddComputation.Controls.Add(Me.cboMeasurement)
        Me.gbAddComputation.Controls.Add(Me.lblSimpleSlabPeriod)
        Me.gbAddComputation.Controls.Add(Me.cboEffectivePeriod)
        Me.gbAddComputation.Controls.Add(Me.cboPaidUnpaidLeave)
        Me.gbAddComputation.Controls.Add(Me.cboCurrency)
        Me.gbAddComputation.Controls.Add(Me.cboDefaultValue)
        Me.gbAddComputation.Controls.Add(Me.lblDefaultValue)
        Me.gbAddComputation.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbAddComputation.Controls.Add(Me.cboLeaveType)
        Me.gbAddComputation.Controls.Add(Me.txtFormula)
        Me.gbAddComputation.Controls.Add(Me.lblFormula)
        Me.gbAddComputation.Controls.Add(Me.objelLine1)
        Me.gbAddComputation.Controls.Add(Me.pnlComputation)
        Me.gbAddComputation.Controls.Add(Me.btnAdd)
        Me.gbAddComputation.Controls.Add(Me.btnDelete)
        Me.gbAddComputation.Controls.Add(Me.cboTrnHead)
        Me.gbAddComputation.Controls.Add(Me.cboFunction)
        Me.gbAddComputation.Controls.Add(Me.lblFunction)
        Me.gbAddComputation.Controls.Add(Me.lblTrnHead_Leave)
        Me.gbAddComputation.ExpandedHoverImage = Nothing
        Me.gbAddComputation.ExpandedNormalImage = Nothing
        Me.gbAddComputation.ExpandedPressedImage = Nothing
        Me.gbAddComputation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAddComputation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAddComputation.HeaderHeight = 25
        Me.gbAddComputation.HeaderMessage = ""
        Me.gbAddComputation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAddComputation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAddComputation.HeightOnCollapse = 0
        Me.gbAddComputation.LeftTextSpace = 0
        Me.gbAddComputation.Location = New System.Drawing.Point(12, 3)
        Me.gbAddComputation.Name = "gbAddComputation"
        Me.gbAddComputation.OpenHeight = 125
        Me.gbAddComputation.Padding = New System.Windows.Forms.Padding(3)
        Me.gbAddComputation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAddComputation.ShowBorder = True
        Me.gbAddComputation.ShowCheckBox = False
        Me.gbAddComputation.ShowCollapseButton = False
        Me.gbAddComputation.ShowDefaultBorderColor = True
        Me.gbAddComputation.ShowDownButton = False
        Me.gbAddComputation.ShowHeader = True
        Me.gbAddComputation.Size = New System.Drawing.Size(456, 456)
        Me.gbAddComputation.TabIndex = 0
        Me.gbAddComputation.Temp = 0
        Me.gbAddComputation.Text = "Additional Computation"
        Me.gbAddComputation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAnd
        '
        Me.lblAnd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnd.Location = New System.Drawing.Point(272, 89)
        Me.lblAnd.Name = "lblAnd"
        Me.lblAnd.Size = New System.Drawing.Size(41, 14)
        Me.lblAnd.TabIndex = 238
        Me.lblAnd.Text = "And"
        Me.lblAnd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConstantValueFrom
        '
        Me.txtConstantValueFrom.AllowNegative = True
        Me.txtConstantValueFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantValueFrom.DigitsInGroup = 0
        Me.txtConstantValueFrom.Flags = 0
        Me.txtConstantValueFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantValueFrom.Location = New System.Drawing.Point(170, 86)
        Me.txtConstantValueFrom.MaxDecimalPlaces = 6
        Me.txtConstantValueFrom.MaxWholeDigits = 21
        Me.txtConstantValueFrom.Name = "txtConstantValueFrom"
        Me.txtConstantValueFrom.Prefix = ""
        Me.txtConstantValueFrom.RangeMax = 1.7976931348623157E+308
        Me.txtConstantValueFrom.RangeMin = -1.7976931348623157E+308
        Me.txtConstantValueFrom.Size = New System.Drawing.Size(96, 21)
        Me.txtConstantValueFrom.TabIndex = 14
        Me.txtConstantValueFrom.Text = "0"
        Me.txtConstantValueFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboSubFunction2
        '
        Me.cboSubFunction2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubFunction2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSubFunction2.FormattingEnabled = True
        Me.cboSubFunction2.Location = New System.Drawing.Point(170, 86)
        Me.cboSubFunction2.Name = "cboSubFunction2"
        Me.cboSubFunction2.Size = New System.Drawing.Size(64, 21)
        Me.cboSubFunction2.TabIndex = 234
        Me.cboSubFunction2.Tag = ""
        '
        'txtConstantValueTo
        '
        Me.txtConstantValueTo.AllowNegative = True
        Me.txtConstantValueTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantValueTo.DigitsInGroup = 0
        Me.txtConstantValueTo.Flags = 0
        Me.txtConstantValueTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantValueTo.Location = New System.Drawing.Point(319, 86)
        Me.txtConstantValueTo.MaxDecimalPlaces = 6
        Me.txtConstantValueTo.MaxWholeDigits = 21
        Me.txtConstantValueTo.Name = "txtConstantValueTo"
        Me.txtConstantValueTo.Prefix = ""
        Me.txtConstantValueTo.RangeMax = 1.7976931348623157E+308
        Me.txtConstantValueTo.RangeMin = -1.7976931348623157E+308
        Me.txtConstantValueTo.Size = New System.Drawing.Size(96, 21)
        Me.txtConstantValueTo.TabIndex = 15
        Me.txtConstantValueTo.Text = "0"
        Me.txtConstantValueTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtConstantValue
        '
        Me.txtConstantValue.AllowNegative = True
        Me.txtConstantValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantValue.DigitsInGroup = 0
        Me.txtConstantValue.Flags = 0
        Me.txtConstantValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantValue.Location = New System.Drawing.Point(170, 59)
        Me.txtConstantValue.MaxDecimalPlaces = 6
        Me.txtConstantValue.MaxWholeDigits = 21
        Me.txtConstantValue.Name = "txtConstantValue"
        Me.txtConstantValue.Prefix = ""
        Me.txtConstantValue.RangeMax = 1.7976931348623157E+308
        Me.txtConstantValue.RangeMin = -1.7976931348623157E+308
        Me.txtConstantValue.Size = New System.Drawing.Size(25, 21)
        Me.txtConstantValue.TabIndex = 1
        Me.txtConstantValue.Text = "0"
        Me.txtConstantValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboSubFunction
        '
        Me.cboSubFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSubFunction.FormattingEnabled = True
        Me.cboSubFunction.Items.AddRange(New Object() {"Select"})
        Me.cboSubFunction.Location = New System.Drawing.Point(173, 59)
        Me.cboSubFunction.Name = "cboSubFunction"
        Me.cboSubFunction.Size = New System.Drawing.Size(42, 21)
        Me.cboSubFunction.TabIndex = 2
        Me.cboSubFunction.Tag = ""
        '
        'cboCRExpense
        '
        Me.cboCRExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCRExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCRExpense.FormattingEnabled = True
        Me.cboCRExpense.Items.AddRange(New Object() {"Select"})
        Me.cboCRExpense.Location = New System.Drawing.Point(173, 59)
        Me.cboCRExpense.Name = "cboCRExpense"
        Me.cboCRExpense.Size = New System.Drawing.Size(61, 21)
        Me.cboCRExpense.TabIndex = 3
        Me.cboCRExpense.Tag = ""
        '
        'cboCommonMaster
        '
        Me.cboCommonMaster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommonMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommonMaster.FormattingEnabled = True
        Me.cboCommonMaster.Items.AddRange(New Object() {"Select"})
        Me.cboCommonMaster.Location = New System.Drawing.Point(173, 59)
        Me.cboCommonMaster.Name = "cboCommonMaster"
        Me.cboCommonMaster.Size = New System.Drawing.Size(76, 21)
        Me.cboCommonMaster.TabIndex = 4
        Me.cboCommonMaster.Tag = ""
        '
        'cboSalaryHead
        '
        Me.cboSalaryHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryHead.FormattingEnabled = True
        Me.cboSalaryHead.Items.AddRange(New Object() {"Select"})
        Me.cboSalaryHead.Location = New System.Drawing.Point(170, 59)
        Me.cboSalaryHead.Name = "cboSalaryHead"
        Me.cboSalaryHead.Size = New System.Drawing.Size(98, 21)
        Me.cboSalaryHead.TabIndex = 5
        Me.cboSalaryHead.Tag = ""
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Items.AddRange(New Object() {"Select"})
        Me.cboShift.Location = New System.Drawing.Point(170, 59)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(119, 21)
        Me.cboShift.TabIndex = 6
        Me.cboShift.Tag = ""
        '
        'objbtnSearchFunction
        '
        Me.objbtnSearchFunction.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFunction.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFunction.BorderSelected = False
        Me.objbtnSearchFunction.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFunction.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFunction.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFunction.Location = New System.Drawing.Point(421, 32)
        Me.objbtnSearchFunction.Name = "objbtnSearchFunction"
        Me.objbtnSearchFunction.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFunction.TabIndex = 223
        '
        'cboActivity
        '
        Me.cboActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivity.FormattingEnabled = True
        Me.cboActivity.Items.AddRange(New Object() {"Select"})
        Me.cboActivity.Location = New System.Drawing.Point(170, 59)
        Me.cboActivity.Name = "cboActivity"
        Me.cboActivity.Size = New System.Drawing.Size(139, 21)
        Me.cboActivity.TabIndex = 7
        Me.cboActivity.Tag = ""
        '
        'cboMeasurement
        '
        Me.cboMeasurement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMeasurement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMeasurement.FormattingEnabled = True
        Me.cboMeasurement.Items.AddRange(New Object() {"Select"})
        Me.cboMeasurement.Location = New System.Drawing.Point(170, 59)
        Me.cboMeasurement.Name = "cboMeasurement"
        Me.cboMeasurement.Size = New System.Drawing.Size(159, 21)
        Me.cboMeasurement.TabIndex = 8
        Me.cboMeasurement.Tag = ""
        '
        'lblSimpleSlabPeriod
        '
        Me.lblSimpleSlabPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimpleSlabPeriod.Location = New System.Drawing.Point(8, 124)
        Me.lblSimpleSlabPeriod.Name = "lblSimpleSlabPeriod"
        Me.lblSimpleSlabPeriod.Size = New System.Drawing.Size(82, 14)
        Me.lblSimpleSlabPeriod.TabIndex = 218
        Me.lblSimpleSlabPeriod.Text = "Effec. Period"
        Me.lblSimpleSlabPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEffectivePeriod
        '
        Me.cboEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEffectivePeriod.DropDownWidth = 250
        Me.cboEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEffectivePeriod.FormattingEnabled = True
        Me.cboEffectivePeriod.Items.AddRange(New Object() {"Select"})
        Me.cboEffectivePeriod.Location = New System.Drawing.Point(96, 122)
        Me.cboEffectivePeriod.Name = "cboEffectivePeriod"
        Me.cboEffectivePeriod.Size = New System.Drawing.Size(111, 21)
        Me.cboEffectivePeriod.TabIndex = 16
        Me.cboEffectivePeriod.Tag = ""
        '
        'cboPaidUnpaidLeave
        '
        Me.cboPaidUnpaidLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaidUnpaidLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaidUnpaidLeave.FormattingEnabled = True
        Me.cboPaidUnpaidLeave.Items.AddRange(New Object() {"Select"})
        Me.cboPaidUnpaidLeave.Location = New System.Drawing.Point(170, 59)
        Me.cboPaidUnpaidLeave.Name = "cboPaidUnpaidLeave"
        Me.cboPaidUnpaidLeave.Size = New System.Drawing.Size(180, 21)
        Me.cboPaidUnpaidLeave.TabIndex = 9
        Me.cboPaidUnpaidLeave.Tag = ""
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Items.AddRange(New Object() {"Select"})
        Me.cboCurrency.Location = New System.Drawing.Point(170, 59)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(202, 21)
        Me.cboCurrency.TabIndex = 10
        Me.cboCurrency.Tag = ""
        '
        'cboDefaultValue
        '
        Me.cboDefaultValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDefaultValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDefaultValue.FormattingEnabled = True
        Me.cboDefaultValue.Location = New System.Drawing.Point(170, 86)
        Me.cboDefaultValue.Name = "cboDefaultValue"
        Me.cboDefaultValue.Size = New System.Drawing.Size(245, 21)
        Me.cboDefaultValue.TabIndex = 13
        Me.cboDefaultValue.Tag = ""
        '
        'lblDefaultValue
        '
        Me.lblDefaultValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefaultValue.Location = New System.Drawing.Point(8, 90)
        Me.lblDefaultValue.Name = "lblDefaultValue"
        Me.lblDefaultValue.Size = New System.Drawing.Size(156, 29)
        Me.lblDefaultValue.TabIndex = 214
        Me.lblDefaultValue.Text = "Default Value to be Considered if not assigned on ED"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(421, 59)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 212
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Items.AddRange(New Object() {"Select"})
        Me.cboLeaveType.Location = New System.Drawing.Point(170, 59)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(223, 21)
        Me.cboLeaveType.TabIndex = 11
        Me.cboLeaveType.Tag = ""
        '
        'txtFormula
        '
        Me.txtFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormula.Location = New System.Drawing.Point(88, 165)
        Me.txtFormula.Multiline = True
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.ReadOnly = True
        Me.txtFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFormula.Size = New System.Drawing.Size(351, 48)
        Me.txtFormula.TabIndex = 19
        '
        'lblFormula
        '
        Me.lblFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormula.Location = New System.Drawing.Point(8, 169)
        Me.lblFormula.Name = "lblFormula"
        Me.lblFormula.Size = New System.Drawing.Size(74, 13)
        Me.lblFormula.TabIndex = 104
        Me.lblFormula.Text = "Formula"
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 154)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(404, 10)
        Me.objelLine1.TabIndex = 101
        '
        'pnlComputation
        '
        Me.pnlComputation.Controls.Add(Me.lvFormula)
        Me.pnlComputation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlComputation.Location = New System.Drawing.Point(14, 219)
        Me.pnlComputation.Name = "pnlComputation"
        Me.pnlComputation.Size = New System.Drawing.Size(425, 234)
        Me.pnlComputation.TabIndex = 99
        '
        'lvFormula
        '
        Me.lvFormula.BackColorOnChecked = True
        Me.lvFormula.ColumnHeaders = Nothing
        Me.lvFormula.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFunction, Me.colhFormula, Me.colhDefaultValue, Me.colhPeriod, Me.objcolhEndDate})
        Me.lvFormula.CompulsoryColumns = ""
        Me.lvFormula.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvFormula.FullRowSelect = True
        Me.lvFormula.GridLines = True
        Me.lvFormula.GroupingColumn = Nothing
        Me.lvFormula.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvFormula.HideSelection = False
        Me.lvFormula.Location = New System.Drawing.Point(0, 0)
        Me.lvFormula.MinColumnWidth = 50
        Me.lvFormula.MultiSelect = False
        Me.lvFormula.Name = "lvFormula"
        Me.lvFormula.OptionalColumns = ""
        Me.lvFormula.ShowMoreItem = False
        Me.lvFormula.ShowSaveItem = False
        Me.lvFormula.ShowSelectAll = True
        Me.lvFormula.ShowSizeAllColumnsToFit = True
        Me.lvFormula.Size = New System.Drawing.Size(425, 234)
        Me.lvFormula.Sortable = False
        Me.lvFormula.TabIndex = 0
        Me.lvFormula.UseCompatibleStateImageBehavior = False
        Me.lvFormula.View = System.Windows.Forms.View.Details
        '
        'colhFunction
        '
        Me.colhFunction.Tag = "colhFunction"
        Me.colhFunction.Text = "Function"
        Me.colhFunction.Width = 120
        '
        'colhFormula
        '
        Me.colhFormula.Tag = "colhFormula"
        Me.colhFormula.Text = "Formula"
        Me.colhFormula.Width = 220
        '
        'colhDefaultValue
        '
        Me.colhDefaultValue.Tag = "colhDefaultValue"
        Me.colhDefaultValue.Text = "Default Value"
        Me.colhDefaultValue.Width = 80
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 0
        '
        'objcolhEndDate
        '
        Me.objcolhEndDate.Tag = "objcolhEndDate"
        Me.objcolhEndDate.Text = ""
        Me.objcolhEndDate.Width = 0
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(213, 116)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(81, 30)
        Me.btnAdd.TabIndex = 17
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(300, 116)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(115, 30)
        Me.btnDelete.TabIndex = 18
        Me.btnDelete.Text = "&Delete Formula"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Items.AddRange(New Object() {"Select"})
        Me.cboTrnHead.Location = New System.Drawing.Point(170, 59)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(245, 21)
        Me.cboTrnHead.TabIndex = 12
        Me.cboTrnHead.Tag = ""
        '
        'cboFunction
        '
        Me.cboFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFunction.FormattingEnabled = True
        Me.cboFunction.Location = New System.Drawing.Point(170, 32)
        Me.cboFunction.Name = "cboFunction"
        Me.cboFunction.Size = New System.Drawing.Size(245, 21)
        Me.cboFunction.TabIndex = 0
        Me.cboFunction.Tag = ""
        '
        'lblFunction
        '
        Me.lblFunction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFunction.Location = New System.Drawing.Point(8, 36)
        Me.lblFunction.Name = "lblFunction"
        Me.lblFunction.Size = New System.Drawing.Size(156, 13)
        Me.lblFunction.TabIndex = 0
        Me.lblFunction.Text = "Function"
        '
        'lblTrnHead_Leave
        '
        Me.lblTrnHead_Leave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead_Leave.Location = New System.Drawing.Point(8, 63)
        Me.lblTrnHead_Leave.Name = "lblTrnHead_Leave"
        Me.lblTrnHead_Leave.Size = New System.Drawing.Size(156, 13)
        Me.lblTrnHead_Leave.TabIndex = 2
        Me.lblTrnHead_Leave.Text = "Transaction Head"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 465)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(480, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(268, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(371, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAdditionalComputation_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(480, 520)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdditionalComputation_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Additional Computation"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbIFFunction.ResumeLayout(False)
        Me.gbIFFunction.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbAddComputation.ResumeLayout(False)
        Me.gbAddComputation.PerformLayout()
        Me.pnlComputation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbAddComputation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents lblFormula As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlComputation As System.Windows.Forms.Panel
    Friend WithEvents lvFormula As eZee.Common.eZeeListView
    Friend WithEvents colhFunction As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFormula As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboFunction As System.Windows.Forms.ComboBox
    Friend WithEvents lblFunction As System.Windows.Forms.Label
    Friend WithEvents lblTrnHead_Leave As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents txtConstantValue As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDefaultValue As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefaultValue As System.Windows.Forms.Label
    Friend WithEvents colhDefaultValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cboPaidUnpaidLeave As System.Windows.Forms.ComboBox
    Friend WithEvents cboEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblSimpleSlabPeriod As System.Windows.Forms.Label
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboMeasurement As System.Windows.Forms.ComboBox
    Friend WithEvents cboActivity As System.Windows.Forms.ComboBox
    Friend WithEvents gbIFFunction As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnIFOk As eZee.Common.eZeeLightButton
    Friend WithEvents txtIFLogical1 As System.Windows.Forms.TextBox
    Friend WithEvents btnAddValueFALSE As eZee.Common.eZeeLightButton
    Friend WithEvents txtValueFALSE As System.Windows.Forms.TextBox
    Friend WithEvents btnAddValueTRUE As eZee.Common.eZeeLightButton
    Friend WithEvents txtValueTRUE As System.Windows.Forms.TextBox
    Friend WithEvents btnAddIFLogical1 As eZee.Common.eZeeLightButton
    Friend WithEvents btnIFCancel As eZee.Common.eZeeLightButton
    Friend WithEvents lblValueFALSE As System.Windows.Forms.Label
    Friend WithEvents lblValueTRUE As System.Windows.Forms.Label
    Friend WithEvents lblLogicalTest As System.Windows.Forms.Label
    Friend WithEvents btnAddIFLogical2 As eZee.Common.eZeeLightButton
    Friend WithEvents txtIFLogical2 As System.Windows.Forms.TextBox
    Friend WithEvents cboComparision As System.Windows.Forms.ComboBox
    Friend WithEvents btnClearIFLogical2 As eZee.Common.eZeeLightButton
    Friend WithEvents btnClearValueFALSE As eZee.Common.eZeeLightButton
    Friend WithEvents btnClearValueTRUE As eZee.Common.eZeeLightButton
    Friend WithEvents btnClearIFLogical1 As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchFunction As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents cboSalaryHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboCommonMaster As System.Windows.Forms.ComboBox
    Friend WithEvents cboCRExpense As System.Windows.Forms.ComboBox
    Friend WithEvents cboSubFunction As System.Windows.Forms.ComboBox
    Friend WithEvents cboSubFunction2 As System.Windows.Forms.ComboBox
    Friend WithEvents txtConstantValueFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents txtConstantValueTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAnd As System.Windows.Forms.Label
End Class
