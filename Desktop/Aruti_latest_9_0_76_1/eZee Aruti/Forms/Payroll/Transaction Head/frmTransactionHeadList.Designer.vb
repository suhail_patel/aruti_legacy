﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransactionHeadList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransactionHeadList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lvTrnHeadList = New eZee.Common.eZeeListView(Me.components)
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhTranHeadType = New System.Windows.Forms.ColumnHeader
        Me.colhTypeOf = New System.Windows.Forms.ColumnHeader
        Me.colhCalcType = New System.Windows.Forms.ColumnHeader
        Me.colhDefault = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblActiveInactive = New System.Windows.Forms.Label
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTrnHead = New eZee.Common.eZeeGradientButton
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSetAsDefault = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClearDefault = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuImportTranhead = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGenerateHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.lvTrnHeadList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(859, 462)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(859, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "List of Transaction Heads"
        '
        'lvTrnHeadList
        '
        Me.lvTrnHeadList.BackColorOnChecked = True
        Me.lvTrnHeadList.ColumnHeaders = Nothing
        Me.lvTrnHeadList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName, Me.colhTranHeadType, Me.colhTypeOf, Me.colhCalcType, Me.colhDefault})
        Me.lvTrnHeadList.CompulsoryColumns = ""
        Me.lvTrnHeadList.FullRowSelect = True
        Me.lvTrnHeadList.GridLines = True
        Me.lvTrnHeadList.GroupingColumn = Nothing
        Me.lvTrnHeadList.HideSelection = False
        Me.lvTrnHeadList.Location = New System.Drawing.Point(12, 139)
        Me.lvTrnHeadList.MinColumnWidth = 50
        Me.lvTrnHeadList.MultiSelect = False
        Me.lvTrnHeadList.Name = "lvTrnHeadList"
        Me.lvTrnHeadList.OptionalColumns = ""
        Me.lvTrnHeadList.ShowMoreItem = False
        Me.lvTrnHeadList.ShowSaveItem = False
        Me.lvTrnHeadList.ShowSelectAll = True
        Me.lvTrnHeadList.ShowSizeAllColumnsToFit = True
        Me.lvTrnHeadList.Size = New System.Drawing.Size(835, 262)
        Me.lvTrnHeadList.Sortable = True
        Me.lvTrnHeadList.TabIndex = 1
        Me.lvTrnHeadList.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadList.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Tran. Head Code"
        Me.colhCode.Width = 100
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Transaction Head Name"
        Me.colhName.Width = 180
        '
        'colhTranHeadType
        '
        Me.colhTranHeadType.Tag = "colhTranHeadType"
        Me.colhTranHeadType.Text = "Transaction Head Type"
        Me.colhTranHeadType.Width = 180
        '
        'colhTypeOf
        '
        Me.colhTypeOf.Tag = "colhTypeOf"
        Me.colhTypeOf.Text = "Type Of"
        Me.colhTypeOf.Width = 130
        '
        'colhCalcType
        '
        Me.colhCalcType.Tag = "colhCalcType"
        Me.colhCalcType.Text = "Calculation Type"
        Me.colhCalcType.Width = 190
        '
        'colhDefault
        '
        Me.colhDefault.Tag = "colhDefault"
        Me.colhDefault.Text = "Default"
        Me.colhDefault.Width = 50
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrnHeadType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(835, 67)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActiveInactive
        '
        Me.lblActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActiveInactive.Location = New System.Drawing.Point(313, 36)
        Me.lblActiveInactive.Name = "lblActiveInactive"
        Me.lblActiveInactive.Size = New System.Drawing.Size(90, 15)
        Me.lblActiveInactive.TabIndex = 302
        Me.lblActiveInactive.Text = "Active/Inactive"
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboActiveInactive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(409, 33)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(105, 21)
        Me.cboActiveInactive.TabIndex = 1
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Items.AddRange(New Object() {"Select", "Deduction From Employees", "Earning for Employees", "Employees Statutory Deductions", "Employers Statutory Contributions"})
        Me.cboTrnHeadType.Location = New System.Drawing.Point(635, 36)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(132, 21)
        Me.cboTrnHeadType.TabIndex = 1
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 36)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(77, 15)
        Me.lblTrnHead.TabIndex = 74
        Me.lblTrnHead.Text = "Tran. Head"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboTrnHead.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(91, 33)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(184, 21)
        Me.cboTrnHead.TabIndex = 0
        '
        'objbtnSearchTrnHead
        '
        Me.objbtnSearchTrnHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrnHead.BorderSelected = False
        Me.objbtnSearchTrnHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrnHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrnHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrnHead.Location = New System.Drawing.Point(281, 33)
        Me.objbtnSearchTrnHead.Name = "objbtnSearchTrnHead"
        Me.objbtnSearchTrnHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrnHead.TabIndex = 87
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(523, 39)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(106, 15)
        Me.lblTrnHeadType.TabIndex = 16
        Me.lblTrnHeadType.Text = "Tran. Head Type"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(808, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(784, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnActive)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 407)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(859, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 8
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetAsDefault, Me.mnuClearDefault, Me.objSep1, Me.mnuImportTranhead, Me.mnuGenerateHeads})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(217, 98)
        '
        'mnuSetAsDefault
        '
        Me.mnuSetAsDefault.Name = "mnuSetAsDefault"
        Me.mnuSetAsDefault.Size = New System.Drawing.Size(216, 22)
        Me.mnuSetAsDefault.Text = "&Set As Default"
        '
        'mnuClearDefault
        '
        Me.mnuClearDefault.Name = "mnuClearDefault"
        Me.mnuClearDefault.Size = New System.Drawing.Size(216, 22)
        Me.mnuClearDefault.Text = "&ClearDefault"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(213, 6)
        '
        'mnuImportTranhead
        '
        Me.mnuImportTranhead.Name = "mnuImportTranhead"
        Me.mnuImportTranhead.Size = New System.Drawing.Size(216, 22)
        Me.mnuImportTranhead.Text = "&Import Transaction Head"
        '
        'mnuGenerateHeads
        '
        Me.mnuGenerateHeads.Name = "mnuGenerateHeads"
        Me.mnuGenerateHeads.Size = New System.Drawing.Size(216, 22)
        Me.mnuGenerateHeads.Text = "&Generate Transaction Head"
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(658, 13)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(90, 30)
        Me.btnVoid.TabIndex = 7
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'btnActive
        '
        Me.btnActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActive.BackColor = System.Drawing.Color.White
        Me.btnActive.BackgroundImage = CType(resources.GetObject("btnActive.BackgroundImage"), System.Drawing.Image)
        Me.btnActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActive.BorderColor = System.Drawing.Color.Empty
        Me.btnActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActive.FlatAppearance.BorderSize = 0
        Me.btnActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActive.ForeColor = System.Drawing.Color.Black
        Me.btnActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Location = New System.Drawing.Point(554, 13)
        Me.btnActive.Name = "btnActive"
        Me.btnActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Size = New System.Drawing.Size(98, 30)
        Me.btnActive.TabIndex = 6
        Me.btnActive.Text = "Set &Active"
        Me.btnActive.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(554, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(98, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "Set &Inactive"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(754, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(458, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(362, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'frmTransactionHeadList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(859, 462)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransactionHeadList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Transaction Heads List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents lvTrnHeadList As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTranHeadType As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTrnHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents colhTypeOf As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblActiveInactive As System.Windows.Forms.Label
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents btnActive As eZee.Common.eZeeLightButton
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSetAsDefault As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClearDefault As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuImportTranhead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGenerateHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents colhDefault As System.Windows.Forms.ColumnHeader
End Class
