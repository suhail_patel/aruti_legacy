﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTransferStaffRequisitionApprover

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmTransferStaffRequisitionApprover"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

    Private dvOldAllocation As DataView
    Private dvAssignedAllocation As DataView
    Private dvNewAllocation As DataView
    Private dvOldJob As DataView
    Private dvAssignedJob As DataView
    Private dvNewJob As DataView
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmTransferStaffRequisitionApprover_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmTransferStaffRequisitionApprover_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmTransferStaffRequisitionApprover_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTransferStaffRequisitionApprover_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objApproverMapping.GetList("List", strOrderBy:="hrmsConfiguration..cfuser_master.username")
            dtTable = dsList.Tables(0).DefaultView.ToTable(True, "approver", "userapproverunkid")

            Dim dr As DataRow = dtTable.NewRow
            dr.Item("userapproverunkid") = 0
            dr.Item("approver") = Language.getMessage(mstrModuleName, 1, "Select")
            dtTable.Rows.InsertAt(dr, 0)

            With cboOldApprover
                .DisplayMember = "approver"
                .ValueMember = "userapproverunkid"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            'dsList = objApproverLevel.getListForCombo("List", True)
            'With cboOldLevel
            '    .DisplayMember = "Name"
            '    .ValueMember = "levelunkid"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With

            'With cboNewLevel
            '    .DisplayMember = "Name"
            '    .ValueMember = "levelunkid"
            '    .DataSource = dsList.Tables(0).Copy
            '    .SelectedValue = 0
            '    .Enabled = False
            'End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub FillOldAllocation()
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Try
            Dim strFilter As String = ""
            strFilter = " AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(cboOldLevel.SelectedValue) & " "
            If CInt(cboOldApprover.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(cboOldApprover.SelectedValue) & " "
            End If

            If CInt(cboOldAllocation.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_mapping.allocationid = " & CInt(cboOldAllocation.SelectedValue) & " "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            dgOldAllocation.AutoGenerateColumns = False

            Select Case CInt(cboOldAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Branch"
                Case enAllocation.DEPARTMENT_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "DepartmentGroup"
                Case enAllocation.DEPARTMENT
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Department"
                Case enAllocation.SECTION_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "SectionGroup"
                Case enAllocation.SECTION
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Section"
                Case enAllocation.UNIT_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "UnitGroup"
                Case enAllocation.UNIT
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Unit"
                Case enAllocation.TEAM
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Team"
                Case enAllocation.JOB_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "JobGroup"
                Case enAllocation.JOBS
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Job"
                Case enAllocation.CLASS_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "ClassGroup"
                Case enAllocation.CLASSES
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhOldAllocationName.DataPropertyName = "Class"

                Case Else
                    colhOldAllocationName.DataPropertyName = ""
            End Select

            objcolhOldAllocationCheck.DataPropertyName = "IsChecked"

            dsList = objApproverMapping.GetList("List", strFilter:=strFilter, strOrderBy:=colhOldAllocationName.DataPropertyName)

            dvOldAllocation = dsList.Tables(0).DefaultView

            dgOldAllocation.DataSource = Nothing
            dgOldAllocation.DataSource = dvOldAllocation

            dvNewAllocation = dsList.Tables(0).Clone.DefaultView
            dgNewAllocation.AutoGenerateColumns = False
            objcolhNewAllocationCheck.DataPropertyName = "IsChecked"
            colhNewAllocationName.DataPropertyName = colhOldAllocationName.DataPropertyName

            dgNewAllocation.DataSource = Nothing
            dgNewAllocation.DataSource = dvNewAllocation

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOldAllocation", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub FillAssignedAllocation()
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Try
            Dim strFilter As String = ""
            strFilter = " AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(cboNewLevel.SelectedValue) & " "
            If CInt(cboNewApprover.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(cboNewApprover.SelectedValue) & " "
            End If

            If CInt(cboNewAllocation.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_mapping.allocationid = " & CInt(cboNewAllocation.SelectedValue) & " "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            dgAssignedAllocation.AutoGenerateColumns = False

            Select Case CInt(cboNewAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Branch"
                Case enAllocation.DEPARTMENT_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "DepartmentGroup"
                Case enAllocation.DEPARTMENT
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Department"
                Case enAllocation.SECTION_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "SectionGroup"
                Case enAllocation.SECTION
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Section"
                Case enAllocation.UNIT_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "UnitGroup"
                Case enAllocation.UNIT
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Unit"
                Case enAllocation.TEAM
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Team"
                Case enAllocation.JOB_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "JobGroup"
                Case enAllocation.JOBS
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Job"
                Case enAllocation.CLASS_GROUP
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "ClassGroup"
                Case enAllocation.CLASSES
                    'colhOldAllocationCode.DataPropertyName = "Id"
                    colhAssignedAllocationName.DataPropertyName = "Class"

                Case Else
                    colhAssignedAllocationName.DataPropertyName = ""
            End Select

            dsList = objApproverMapping.GetList("List", strFilter:=strFilter, strOrderBy:=colhAssignedAllocationName.DataPropertyName)

            dvAssignedAllocation = dsList.Tables(0).DefaultView

            dgAssignedAllocation.DataSource = Nothing
            dgAssignedAllocation.DataSource = dvAssignedAllocation

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAssignedAllocation", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub FillOldJob()
        Dim objApproverJob As New clsstaffrequisition_approver_jobmapping
        Dim dsList As DataSet
        Try
            Dim strFilter As String = ""
            strFilter = " AND rcstaffrequisition_approver_jobmapping.levelunkid = " & CInt(cboOldLevel.SelectedValue) & " "

            If CInt(cboOldAllocation.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_jobmapping.allocationid = " & CInt(cboOldAllocation.SelectedValue) & " "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            dsList = objApproverJob.GetList("List", CInt(cboOldApprover.SelectedValue), 0, 0, strFilter, , "ISNULL(hrjob_master.job_name,'')")

            dgOldJob.AutoGenerateColumns = False

            colhOldJobName.DataPropertyName = "Job"
            objcolhOldJobCheck.DataPropertyName = "IsChecked"

            dvOldjob = dsList.Tables(0).DefaultView

            dgOldJob.DataSource = Nothing
            dgOldJob.DataSource = dvOldJob

            dvNewJob = dsList.Tables(0).Clone.DefaultView
            dgNewJob.AutoGenerateColumns = False
            objcolhNewJobCheck.DataPropertyName = "IsChecked"
            colhNewJobName.DataPropertyName = colhOldJobName.DataPropertyName

            dgNewJob.DataSource = Nothing
            dgNewJob.DataSource = dvNewJob

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOldJob", mstrModuleName)
        Finally
            objApproverJob = Nothing
        End Try
    End Sub

    Private Sub FillAssignedJob()
        Dim objApproverJob As New clsstaffrequisition_approver_jobmapping
        Dim dsList As DataSet
        Try
            Dim strFilter As String = ""
            strFilter = " AND rcstaffrequisition_approver_jobmapping.levelunkid = " & CInt(cboNewLevel.SelectedValue) & " "

            If CInt(cboNewAllocation.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_jobmapping.allocationid = " & CInt(cboNewAllocation.SelectedValue) & " "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            dsList = objApproverJob.GetList("List", CInt(cboNewApprover.SelectedValue), 0, 0, strFilter, , "ISNULL(hrjob_master.job_name,'')")

            dvAssignedJob = dsList.Tables(0).DefaultView

            dgAssignedJob.AutoGenerateColumns = False

            colhAssignedJobName.DataPropertyName = "Job"

            dgAssignedJob.DataSource = Nothing
            dgAssignedJob.DataSource = dvAssignedJob

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOldJob", mstrModuleName)
        Finally
            objApproverJob = Nothing
        End Try
    End Sub

    Private Sub SetCheckBoxOldAllocationValue()
        Try
            Dim drRow As DataRow() = dvOldAllocation.ToTable.Select("IsChecked = 1")

            RemoveHandler objchkOldAllocationSelect.CheckedChanged, AddressOf objchkOldAllocationSelect_CheckedChanged

            If drRow.Length <= 0 Then
                objchkOldAllocationSelect.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvOldAllocation.Table.Rows.Count Then
                objchkOldAllocationSelect.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvOldAllocation.Table.Rows.Count Then
                objchkOldAllocationSelect.CheckState = CheckState.Checked
            End If
            dvOldAllocation.Table.AcceptChanges()

            AddHandler objchkOldAllocationSelect.CheckedChanged, AddressOf objchkOldAllocationSelect_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxOldAllocationValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxNewAllocationValue()
        Try
            Dim drRow As DataRow() = dvNewAllocation.ToTable.Select("IsChecked = 1")

            RemoveHandler objchkNewAllocationSelect.CheckedChanged, AddressOf objchkNewAllocationSelect_CheckedChanged

            If drRow.Length <= 0 Then
                objchkNewAllocationSelect.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvNewAllocation.Table.Rows.Count Then
                objchkNewAllocationSelect.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvNewAllocation.Table.Rows.Count Then
                objchkNewAllocationSelect.CheckState = CheckState.Checked
            End If
            dvNewAllocation.Table.AcceptChanges()

            AddHandler objchkNewAllocationSelect.CheckedChanged, AddressOf objchkNewAllocationSelect_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxNewAllocationValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxOldJobValue()
        Try
            Dim drRow As DataRow() = dvOldJob.ToTable.Select("IsChecked = 1")

            RemoveHandler objchkOldJobSelect.CheckedChanged, AddressOf objchkOldJobSelect_CheckedChanged

            If drRow.Length <= 0 Then
                objchkOldJobSelect.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvOldJob.Table.Rows.Count Then
                objchkOldJobSelect.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvOldJob.Table.Rows.Count Then
                objchkOldJobSelect.CheckState = CheckState.Checked
            End If
            dvOldJob.Table.AcceptChanges()

            AddHandler objchkOldJobSelect.CheckedChanged, AddressOf objchkOldJobSelect_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxOldJobValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxNewJobValue()
        Try
            Dim drRow As DataRow() = dvNewJob.ToTable.Select("IsChecked = 1")

            RemoveHandler objchkNewJobSelect.CheckedChanged, AddressOf objchkNewJobSelect_CheckedChanged

            If drRow.Length <= 0 Then
                objchkNewJobSelect.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvNewJob.Table.Rows.Count Then
                objchkNewJobSelect.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvNewJob.Table.Rows.Count Then
                objchkNewJobSelect.CheckState = CheckState.Checked
            End If
            dvNewJob.Table.AcceptChanges()

            AddHandler objchkNewJobSelect.CheckedChanged, AddressOf objchkNewJobSelect_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxNewJobValue", mstrModuleName)
        End Try
    End Sub


    Public Function IsValidate() As Boolean
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select From Approver."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Return False
            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select From Level."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Return False
            ElseIf CInt(cboOldAllocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select From Allocation."), enMsgBoxStyle.Information)
                cboOldAllocation.Select()
                Return False
            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select To Approver."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Return False
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Select To Level."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Return False
            ElseIf CInt(cboNewAllocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select To Allocation."), enMsgBoxStyle.Information)
                cboNewAllocation.Select()
                Return False
            ElseIf CInt(cboOldLevel.SelectedValue) <> CInt(cboNewLevel.SelectedValue) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, From Level and To Level must be same."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Return False
            ElseIf CInt(cboOldAllocation.SelectedValue) <> CInt(cboNewAllocation.SelectedValue) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, From Allocation and To Allocation must be same."), enMsgBoxStyle.Information)
                cboNewAllocation.Select()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Combobox Events "

    Private Sub cboOldApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            Dim strFilter As String = ""
            If CInt(cboOldApprover.SelectedValue) > 0 Then
                strFilter &= " AND rcstaffrequisition_approver_mapping.userapproverunkid <> " & CInt(cboOldApprover.SelectedValue) & " "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            dsList = objApproverMapping.GetList("List", strFilter:=strFilter, strOrderBy:="hrmsConfiguration..cfuser_master.username")
            dtTable = dsList.Tables(0).DefaultView.ToTable(True, "approver", "userapproverunkid")

            Dim dr As DataRow = dtTable.NewRow
            dr.Item("userapproverunkid") = 0
            dr.Item("approver") = Language.getMessage(mstrModuleName, 10, "Select")
            dtTable.Rows.InsertAt(dr, 0)

            With cboNewApprover
                .DisplayMember = "approver"
                .ValueMember = "userapproverunkid"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            dsList = objApproverMapping.GetApproverLevel(Nothing, CInt(cboOldApprover.SelectedValue))
            With cboOldLevel
                .DisplayMember = "levelname"
                .ValueMember = "levelunkid"
                .DataSource = dsList.Tables(0)
                If dsList.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub cboOldLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Try
            dsList = objApproverMapping.GetOnlyMappedAllocatonList("List", CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue))
            With cboOldAllocation
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsList.Tables(0)
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldLevel_SelectedIndexChanged", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub cboOldAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOldAllocation.SelectedIndexChanged
        Try

            Call FillOldAllocation()
            Call FillOldJob()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldAllocation_SelectedIndexChanged", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub cboNewApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Try
            dsList = objApproverMapping.GetApproverLevel(Nothing, CInt(cboNewApprover.SelectedValue))
            With cboNewLevel
                .DisplayMember = "levelname"
                .ValueMember = "levelunkid"
                .DataSource = dsList.Tables(0)
                '.SelectedValue = 0
                If dsList.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewApprover_SelectedIndexChanged", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub cboNewLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim dsList As DataSet
        Try
            dsList = objApproverMapping.GetOnlyMappedAllocatonList("List", CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue))
            With cboNewAllocation
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsList.Tables(0)
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewLevel_SelectedIndexChanged", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub cboNewAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNewAllocation.SelectedIndexChanged
        Try

            Call FillAssignedAllocation()
            Call FillAssignedJob()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewAllocation_SelectedIndexChanged", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If IsValidate() = False Then Exit Try

            If dvOldAllocation Is Nothing Then Exit Try

            Dim drCheckedAlloc() As DataRow = dvOldAllocation.Table.Select("IsChecked = 1 ")

            If drCheckedAlloc.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please tick atleast one allocation to transfer."), enMsgBoxStyle.Information)
                dgOldAllocation.Select()
                Exit Try
            End If

            Dim arrAllocIDs() As String = (From p In drCheckedAlloc.Cast(Of DataRow)() Select (p.Item("allocationunkid").ToString)).ToArray
            Dim arrTransferAlloc() As String = {}

            If dvAssignedAllocation IsNot Nothing Then

                Dim arrAssignedAllocIDs() As String = (From p In dvAssignedAllocation.Table Select (p.Item("allocationunkid").ToString)).ToArray

                arrTransferAlloc = arrAllocIDs.Except(arrAssignedAllocIDs).ToArray

            End If

            'Dim lstRow As List(Of DataRow) = (From p In dvOldAllocation.Table Where (arrTransferAlloc.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList
            Dim lstRow As List(Of DataRow) = (From p In dvOldAllocation.Table Where (arrAllocIDs.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList

            'If dvNewAllocation Is Nothing Then
            '    Dim dtTable As DataTable = dvOldAllocation.Table.Clone
            '    dvNewAllocation = dtTable.DefaultView
            'End If


            For Each dtRow As DataRow In lstRow
                dtRow.Item("IsChecked") = False
                dvNewAllocation.Table.ImportRow(dtRow)
            Next

            For Each dtRow As DataRow In drCheckedAlloc
                dvOldAllocation.Table.Rows.Remove(dtRow)
            Next

            RemoveHandler objchkOldAllocationSelect.CheckedChanged, AddressOf objchkOldAllocationSelect_CheckedChanged
            objchkOldAllocationSelect.Checked = False
            AddHandler objchkOldAllocationSelect.CheckedChanged, AddressOf objchkOldAllocationSelect_CheckedChanged

            If drCheckedAlloc.Length <> arrTransferAlloc.Length Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Some of the ticked allocations are already linked with the selected approver."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            If IsValidate() = False Then Exit Try

            If dvNewAllocation Is Nothing Then Exit Try

            Dim drCheckedAlloc() As DataRow = dvNewAllocation.Table.Select("IsChecked = 1 ")

            If drCheckedAlloc.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please tick atleast one allocation to unassign transfer."), enMsgBoxStyle.Information)
                dgNewAllocation.Select()
                Exit Try
            End If

            Dim arrAllocIDs() As String = (From p In drCheckedAlloc.Cast(Of DataRow)() Select (p.Item("allocationunkid").ToString)).ToArray
            Dim arrTransferAlloc() As String = {}

            If dvOldAllocation IsNot Nothing Then

                Dim arrAssignedAllocIDs() As String = (From p In dvOldAllocation.Table Select (p.Item("allocationunkid").ToString)).ToArray

                arrTransferAlloc = arrAllocIDs.Except(arrAssignedAllocIDs).ToArray

            End If

            'Dim lstRow As List(Of DataRow) = (From p In dvNewAllocation.Table Where (arrTransferAlloc.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList
            Dim lstRow As List(Of DataRow) = (From p In dvNewAllocation.Table Where (arrAllocIDs.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList

            For Each dtRow As DataRow In lstRow
                dtRow.Item("IsChecked") = False
                dvOldAllocation.Table.ImportRow(dtRow)
            Next

            For Each dtRow As DataRow In drCheckedAlloc
                dvNewAllocation.Table.Rows.Remove(dtRow)
            Next

            RemoveHandler objchkNewAllocationSelect.CheckedChanged, AddressOf objchkNewAllocationSelect_CheckedChanged
            objchkNewAllocationSelect.Checked = False
            AddHandler objchkNewAllocationSelect.CheckedChanged, AddressOf objchkNewAllocationSelect_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAssignJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssignJob.Click
        Try
            If IsValidate() = False Then Exit Try

            If dvOldJob Is Nothing Then Exit Try

            Dim drCheckedJob() As DataRow = dvOldJob.Table.Select("IsChecked = 1 ")

            If drCheckedJob.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please tick atleast one job to transfer."), enMsgBoxStyle.Information)
                dgOldJob.Select()
                Exit Try
            End If

            Dim arrJobIDs() As String = (From p In drCheckedJob.Cast(Of DataRow)() Select (p.Item("jobunkid").ToString)).ToArray
            Dim arrTransferJob() As String = {}

            If dvAssignedJob IsNot Nothing Then

                Dim arrAssignedJobIDs() As String = (From p In dvAssignedJob.Table Select (p.Item("jobunkid").ToString)).ToArray

                arrTransferJob = arrJobIDs.Except(arrAssignedJobIDs).ToArray

            End If

            'Dim lstRow As List(Of DataRow) = (From p In dvOldAllocation.Table Where (arrTransferAlloc.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList
            Dim lstRow As List(Of DataRow) = (From p In dvOldJob.Table Where (arrJobIDs.Contains(p.Item("jobunkid").ToString) = True) Select (p)).ToList

            For Each dtRow As DataRow In lstRow
                dtRow.Item("IsChecked") = False
                dvNewJob.Table.ImportRow(dtRow)
            Next

            For Each dtRow As DataRow In drCheckedJob
                dvOldJob.Table.Rows.Remove(dtRow)
            Next

            RemoveHandler objchkOldJobSelect.CheckedChanged, AddressOf objchkOldJobSelect_CheckedChanged
            objchkOldJobSelect.Checked = False
            AddHandler objchkOldJobSelect.CheckedChanged, AddressOf objchkOldJobSelect_CheckedChanged

            If drCheckedJob.Length <> arrTransferJob.Length Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Some of the ticked jobs are already linked with the selected approver."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssignJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnAssignJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssignJob.Click
        Try
            If IsValidate() = False Then Exit Try

            If dvNewJob Is Nothing Then Exit Try

            Dim drCheckedJob() As DataRow = dvNewJob.Table.Select("IsChecked = 1 ")

            If drCheckedJob.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please tick atleast one job to unassign transfer."), enMsgBoxStyle.Information)
                dgNewJob.Select()
                Exit Try
            End If

            Dim arrJobIDs() As String = (From p In drCheckedJob.Cast(Of DataRow)() Select (p.Item("jobunkid").ToString)).ToArray
            Dim arrTransferJob() As String = {}

            If dvOldJob IsNot Nothing Then

                Dim arrAssignedJobIDs() As String = (From p In dvOldJob.Table Select (p.Item("jobunkid").ToString)).ToArray

                arrTransferJob = arrJobIDs.Except(arrAssignedJobIDs).ToArray

            End If

            'Dim lstRow As List(Of DataRow) = (From p In dvNewAllocation.Table Where (arrTransferAlloc.Contains(p.Item("allocationunkid").ToString) = True) Select (p)).ToList
            Dim lstRow As List(Of DataRow) = (From p In dvNewJob.Table Where (arrJobIDs.Contains(p.Item("jobunkid").ToString) = True) Select (p)).ToList

            For Each dtRow As DataRow In lstRow
                dtRow.Item("IsChecked") = False
                dvOldJob.Table.ImportRow(dtRow)
            Next

            For Each dtRow As DataRow In drCheckedJob
                dvNewJob.Table.Rows.Remove(dtRow)
            Next

            RemoveHandler objchkNewJobSelect.CheckedChanged, AddressOf objchkNewJobSelect_CheckedChanged
            objchkNewJobSelect.Checked = False
            AddHandler objchkNewJobSelect.CheckedChanged, AddressOf objchkNewJobSelect_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssignJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objApproverMapping As New clsStaffRequisition_approver_mapping
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Try

            If dvNewAllocation.Table.Rows.Count <= 0 AndAlso dvNewJob.Table.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please tick atleast one allocation or job to transfer."), enMsgBoxStyle.Information)
                dgOldAllocation.Select()
                Exit Try
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Are you sure you want to transfer sselected Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.STAFF_REQUISITION, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                Dim arrVoidAllocUnkIDs() As String = (From p In dvNewAllocation.Table Select (p.Item("staffrequisitionapproverunkid").ToString)).ToArray
                Dim arrVoidJobUnkIDs() As String = (From p In dvNewJob.Table Select (p.Item("staffrequisitionjobapproverunkid").ToString)).ToArray

                Dim arrAssignedAllocIDs() As String = (From p In dvAssignedAllocation.Table Select (p.Item("allocationunkid").ToString)).ToArray
                Dim arrAssignedJobIDs() As String = (From p In dvAssignedJob.Table Select (p.Item("jobunkid").ToString)).ToArray

                Dim arrVoidAllocIDs() As String = (From p In dvNewAllocation.Table Select (p.Item("allocationunkid").ToString)).ToArray
                Dim arrVoidJobIDs() As String = (From p In dvNewJob.Table Select (p.Item("jobunkid").ToString)).ToArray

                Dim arrInsertAllocIDs() As String = arrVoidAllocIDs.Except(arrAssignedAllocIDs).ToArray
                Dim arrInsertJobIDs() As String = arrVoidJobIDs.Except(arrAssignedJobIDs).ToArray

                objApproverMapping._WebClientIP = getIP()
                objApproverMapping._WebHostName = getHostName()
                objApproverMapping._WebFormName = mstrModuleName
                objApproverMapping._IsFromWeb = False

                blnFlag = objApproverMapping.TransferApprover(Nothing _
                                                       , intFromUserUnkId:=CInt(cboOldApprover.SelectedValue) _
                                                       , intToUserUnkId:=CInt(cboNewApprover.SelectedValue) _
                                                       , intFromLevelUnkId:=CInt(cboOldLevel.SelectedValue) _
                                                       , intToLevelUnkId:=CInt(cboNewLevel.SelectedValue) _
                                                       , intFromAllocationId:=CInt(cboOldAllocation.SelectedValue) _
                                                       , intToAllocationId:=CInt(cboNewAllocation.SelectedValue) _
                                                       , intUserUnkId:=User._Object._Userunkid _
                                                       , arrVoidAllocUnkIDs:=arrVoidAllocUnkIDs _
                                                       , arrVoidJobUnkIDs:=arrVoidJobUnkIDs _
                                                       , arrInsertAllocIDs:=arrInsertAllocIDs _
                                                       , arrInsertJobIDs:=arrInsertJobIDs _
                                                       , strVoidReason:=mstrVoidReason _
                                                       , dtCurrentDateTime:=DateAndTime.Now _
                                                       )

                If blnFlag = False And objApproverMapping._Message <> "" Then
                    eZeeMsgBox.Show(objApproverMapping._Message, enMsgBoxStyle.Information)
                Else

                End If

                If blnFlag Then
                    mblnCancel = False
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Approver Transferred Successfully."), enMsgBoxStyle.Information)

                    Call FillCombo()
                End If

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            objApproverMapping = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchOldApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldApprover.DataSource, DataTable)
            With cboOldApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewApprover.DataSource, DataTable)
            With cboNewApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOldLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldLevel.DataSource, DataTable)
            With cboOldLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "levelcode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewLevel.DataSource, DataTable)
            With cboNewLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "levelcode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub dgOldAllocation_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOldAllocation.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhOldAllocationCheck.Index Then

                If dgOldAllocation.IsCurrentCellDirty Then
                    dgOldAllocation.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxOldAllocationValue()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldAllocation_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgNewAllocation_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgNewAllocation.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhOldAllocationCheck.Index Then

                If dgNewAllocation.IsCurrentCellDirty Then
                    dgNewAllocation.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxNewAllocationValue()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgNewAllocation_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOldJob_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOldJob.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhOldJobCheck.Index Then

                If dgOldJob.IsCurrentCellDirty Then
                    dgOldJob.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxOldJobValue()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldJob_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgNewJob_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgNewJob.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhNewJobCheck.Index Then

                If dgNewJob.IsCurrentCellDirty Then
                    dgNewJob.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxNewJobValue()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgNewJob_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox Event "

    Private Sub objchkOldAllocationSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldAllocationSelect.CheckedChanged
        Try
            If dvOldAllocation IsNot Nothing Then

                For Each dr As DataRowView In dvOldAllocation
                    dr.Item("IsChecked") = objchkOldAllocationSelect.Checked
                Next
                dvOldAllocation.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldAllocationSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkNewAllocationSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewAllocationSelect.CheckedChanged
        Try
            If dvNewAllocation IsNot Nothing Then
                For Each dr As DataRowView In dvNewAllocation
                    dr.Item("IsChecked") = objchkNewAllocationSelect.Checked
                Next
                dvNewAllocation.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewAllocationSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkOldJobSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldJobSelect.CheckedChanged
        Try
            If dvOldJob IsNot Nothing Then

                For Each dr As DataRowView In dvOldJob
                    dr.Item("IsChecked") = objchkOldJobSelect.Checked
                Next
                dvOldJob.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldJobSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkNewJobSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewJobSelect.CheckedChanged
        Try
            If dvNewJob IsNot Nothing Then

                For Each dr As DataRowView In dvNewJob
                    dr.Item("IsChecked") = objchkNewJobSelect.Checked
                Next
                dvNewJob.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewJobSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region
   
#Region " TextBox Event "

    Private Sub txtSearchOldAllocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchOldAllocation.TextChanged
        Try
            If dvOldAllocation Is Nothing Then Exit Sub

            dvOldAllocation.RowFilter = " " & colhOldAllocationName.DataPropertyName & " like '%" & txtSearchOldAllocation.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchOldAllocation_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchNewAllocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchNewAllocation.TextChanged
        Try
            If dvNewAllocation Is Nothing Then Exit Sub

            dvNewAllocation.RowFilter = " " & colhNewAllocationName.DataPropertyName & " like '%" & txtSearchNewAllocation.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchNewAllocation_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchAssignedAllocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchAssignedAllocation.TextChanged
        Try
            If dvAssignedAllocation Is Nothing Then Exit Sub

            dvAssignedAllocation.RowFilter = " " & colhAssignedAllocationName.DataPropertyName & " like '%" & txtSearchAssignedAllocation.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchAssignedAllocation_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchOldJob_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchOldJob.TextChanged
        Try
            If dvOldJob Is Nothing Then Exit Sub

            dvOldJob.RowFilter = " Job like '%" & txtSearchOldJob.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchOldJob_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchNewJob_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchNewJob.TextChanged
        Try
            If dvNewJob Is Nothing Then Exit Sub

            dvNewJob.RowFilter = " Job like '%" & txtSearchNewJob.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchNewJob_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchAssignedJob_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchAssignedJob.TextChanged
        Try
            If dvAssignedJob Is Nothing Then Exit Sub

            dvAssignedJob.RowFilter = " Job like '%" & txtSearchAssignedJob.Text.Trim & "%' "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchAssignedJob_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnUnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAssignJob.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAssignJob.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnUnAssignJob.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUnAssignJob.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
			Me.tbpMigratedEmployee.Text = Language._Object.getCaption(Me.tbpMigratedEmployee.Name, Me.tbpMigratedEmployee.Text)
			Me.tbpAssignEmployee.Text = Language._Object.getCaption(Me.tbpAssignEmployee.Name, Me.tbpAssignEmployee.Text)
			Me.lblNewAllocation.Text = Language._Object.getCaption(Me.lblNewAllocation.Name, Me.lblNewAllocation.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.lblNewLevel.Text = Language._Object.getCaption(Me.lblNewLevel.Name, Me.lblNewLevel.Text)
			Me.lblNewApprover.Text = Language._Object.getCaption(Me.lblNewApprover.Name, Me.lblNewApprover.Text)
			Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblOldAllocation.Text = Language._Object.getCaption(Me.lblOldAllocation.Name, Me.lblOldAllocation.Text)
			Me.colhAssignedAllocationName.HeaderText = Language._Object.getCaption(Me.colhAssignedAllocationName.Name, Me.colhAssignedAllocationName.HeaderText)
			Me.TabPage1.Text = Language._Object.getCaption(Me.TabPage1.Name, Me.TabPage1.Text)
			Me.TabPage2.Text = Language._Object.getCaption(Me.TabPage2.Name, Me.TabPage2.Text)
			Me.colhAssignedJobName.HeaderText = Language._Object.getCaption(Me.colhAssignedJobName.Name, Me.colhAssignedJobName.HeaderText)
			Me.colhOldJobName.HeaderText = Language._Object.getCaption(Me.colhOldJobName.Name, Me.colhOldJobName.HeaderText)
			Me.colhOldAllocationName.HeaderText = Language._Object.getCaption(Me.colhOldAllocationName.Name, Me.colhOldAllocationName.HeaderText)
			Me.colhNewAllocationName.HeaderText = Language._Object.getCaption(Me.colhNewAllocationName.Name, Me.colhNewAllocationName.HeaderText)
			Me.colhNewJobName.HeaderText = Language._Object.getCaption(Me.colhNewJobName.Name, Me.colhNewJobName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Please Select From Approver.")
			Language.setMessage(mstrModuleName, 3, "Please Select From Level.")
			Language.setMessage(mstrModuleName, 4, "Please Select From Allocation.")
			Language.setMessage(mstrModuleName, 5, "Please Select To Approver.")
			Language.setMessage(mstrModuleName, 6, "Please Select To Level.")
			Language.setMessage(mstrModuleName, 7, "Please Select To Allocation.")
			Language.setMessage(mstrModuleName, 8, "Sorry, From Level and To Level must be same.")
			Language.setMessage(mstrModuleName, 9, "Sorry, From Allocation and To Allocation must be same.")
			Language.setMessage(mstrModuleName, 10, "Select")
			Language.setMessage(mstrModuleName, 11, "Please tick atleast one allocation to transfer.")
			Language.setMessage(mstrModuleName, 12, "Sorry, Some of the ticked allocations are already linked with the selected approver.")
			Language.setMessage(mstrModuleName, 13, "Please tick atleast one allocation to unassign transfer.")
			Language.setMessage(mstrModuleName, 14, "Please tick atleast one job to transfer.")
			Language.setMessage(mstrModuleName, 15, "Sorry, Some of the ticked jobs are already linked with the selected approver.")
			Language.setMessage(mstrModuleName, 16, "Please tick atleast one job to unassign transfer.")
			Language.setMessage(mstrModuleName, 17, "Please tick atleast one allocation or job to transfer.")
			Language.setMessage(mstrModuleName, 18, "Are you sure you want to transfer sselected Approver?")
			Language.setMessage(mstrModuleName, 19, "Approver Transferred Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class