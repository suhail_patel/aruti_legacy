﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmApproveDisapproveStaffRequisition

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveStaffRequisition"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private trd As Thread

    Private objStaffReqApproval As clsStaffrequisition_approval_Tran
    Private objStaffReq As clsStaffrequisition_Tran

    Private mintStaffrequisitionapprovaltranunkid As Integer
    Private mintStaffrequisitiontranunkid As Integer
    Private mintCurrApprovalStatus As Integer

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

    Private mdicAllocation As New Dictionary(Of Integer, String)
    Private mdicStatus As New Dictionary(Of Integer, String)
    Private mstrAdvanceFilter As String = ""

    Private mdtList As DataTable = Nothing 'Sohail (12 Oct 2018)


    'Gajanan [21-April-2020] -- Start   
    Private mstrFolderName As String = ""
    Dim mdtStaffRequisitionDocument As DataTable
    Dim objDocument As clsScan_Attach_Documents
    'Gajanan [21-April-2020] -- End


#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByVal Action As enAction, ByVal intStaffrequisitionapprovaltranunkid As Integer, ByVal intStaffrequisitiontranunkid As Integer, ByVal intCurrApprovalStatus As Integer) As Boolean
        Try
            menAction = Action
            mintStaffrequisitionapprovaltranunkid = intStaffrequisitionapprovaltranunkid
            mintStaffrequisitiontranunkid = intStaffrequisitiontranunkid
            mintCurrApprovalStatus = intCurrApprovalStatus

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods & Functions "
    Private Sub SetColor()
        Try
            cboStatus.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            txtFormNo.BackColor = GUI.ColorOptional
            txtStatus.BackColor = GUI.ColorOptional
            txtAllocation.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorOptional
            txtClassGroup.BackColor = GUI.ColorOptional
            txtClass.BackColor = GUI.ColorOptional
            'txtEmployee.BackColor = GUI.ColorOptional 'Sohail (18 Mar 2015)
            txtLeavingReason.BackColor = GUI.ColorOptional
            txtAddStaffReason.BackColor = GUI.ColorOptional
            txtJob.BackColor = GUI.ColorOptional
            txtJobDesc.BackColor = GUI.ColorOptional
            txtNoofPosition.BackColor = GUI.ColorOptional
            txtWorkStartDate.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim dsCombo As DataSet
        Dim dtTable As DataTable

        Try

            dsCombo = objMaster.GetEAllocation_Notification("List")
            mdicAllocation = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.GetListForStaffRequisitionStatus(False, "Status")
            mdicStatus = (From p In dsCombo.Tables("Status").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            If mintCurrApprovalStatus = enApprovalStatus.PENDING Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , True, True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            ElseIf mintCurrApprovalStatus = enApprovalStatus.APPROVED Then
                dsCombo = objMaster.getApprovalStatus("ApprovalStatus", False, , , , , True)
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus")).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables("ApprovalStatus"), "ID <> " & mintCurrApprovalStatus & " ", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintMinPriority = objApproverLevel.GetMinPriority(objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintMaxPriority = objApproverLevel.GetMaxPriority(objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)

                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, ConfigParameter._Object._EmployeeAsOnDate, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintMinPriority = objApproverLevel.GetMinPriority(ConfigParameter._Object._EmployeeAsOnDate, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintMaxPriority = objApproverLevel.GetMaxPriority(ConfigParameter._Object._EmployeeAsOnDate, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, ConfigParameter._Object._EmployeeAsOnDate, objStaffReq._Staffrequisitionbyid, objStaffReq._Allocationunkid)
                'Pinkal (16-Nov-2021)-- End


                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                    objApproverName.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver").ToString
                    objApproverLevelVal.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver_level").ToString
                    objLevelPriorityVal.Text = mintCurrLevelPriority.ToString
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub FillInfo()
        Dim objJob As New clsJobs
        Dim dsList As DataSet
        Dim strFilter As String = ""
        'Sohail (12 Oct 2018) -- Start
        'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
        'Dim mdtList As New DataTable 'Sohail (18 Mar 2015)
        mdtList = New DataTable 'Sohail (18 Mar 2015)
        'Sohail (12 Oct 2018) -- End

        Try

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            mdtList.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtList.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("actionreasonunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (18 Mar 2015) -- End

            dsList = objStaffReq.GetList("List", "rcstaffrequisition_tran.staffrequisitiontranunkid = " & mintStaffrequisitiontranunkid & " ")
            If dsList.Tables("List").Rows.Count > 0 Then

                With dsList.Tables("List").Rows(0)
                    txtFormNo.Text = .Item("formno").ToString
                    txtStatus.Text = mdicStatus.Item(CInt(.Item("staffrequisitiontypeid")))
                    txtStatus.Tag = CInt(.Item("staffrequisitiontypeid"))

                    txtAllocation.Text = mdicAllocation.Item(CInt(.Item("staffrequisitionbyid")))
                    txtAllocation.Tag = CInt(.Item("staffrequisitionbyid"))

                    Select Case CInt(.Item("staffrequisitionbyid"))

                        Case enAllocation.BRANCH
                            txtName.Text = .Item("Branch").ToString

                        Case enAllocation.DEPARTMENT_GROUP
                            txtName.Text = .Item("DepartmentGroup").ToString

                        Case enAllocation.DEPARTMENT
                            txtName.Text = .Item("Department").ToString

                        Case enAllocation.SECTION_GROUP
                            txtName.Text = .Item("SectionGroup").ToString

                        Case enAllocation.SECTION
                            txtName.Text = .Item("Section").ToString

                        Case enAllocation.UNIT_GROUP
                            txtName.Text = .Item("UnitGroup").ToString

                        Case enAllocation.UNIT
                            txtName.Text = .Item("Unit").ToString

                        Case enAllocation.TEAM
                            txtName.Text = .Item("Team").ToString

                        Case enAllocation.JOB_GROUP
                            txtName.Text = .Item("JobGroup").ToString

                        Case enAllocation.JOBS
                            txtName.Text = .Item("Job").ToString

                        Case enAllocation.CLASS_GROUP
                            txtName.Text = .Item("ClassGroup").ToString

                        Case enAllocation.CLASSES
                            txtName.Text = .Item("Class").ToString

                    End Select
                    txtName.Tag = CInt(.Item("allocationunkid"))

                    txtClassGroup.Text = .Item("Staff_ClassGroup").ToString
                    txtClassGroup.Tag = CInt(.Item("classgroupunkid"))

                    txtClass.Text = .Item("Staff_Class").ToString
                    txtClass.Tag = CInt(.Item("classunkid"))

                    'Sohail (18 Mar 2015) -- Start
                    'Enhancement - Allow more than one employee to be replaced in staff requisition.
                    'txtEmployee.Text = .Item("employeename").ToString
                    'txtEmployee.Tag = CInt(.Item("employeeunkid"))
                    'Sohail (18 Mar 2015) -- End

                    txtLeavingReason.Text = .Item("actionreason").ToString
                    txtLeavingReason.Tag = CInt(.Item("actionreasonunkid"))

                    txtAddStaffReason.Text = .Item("additionalstaffreason").ToString

                    txtJob.Text = .Item("JobTitle").ToString
                    txtJob.Tag = CInt(.Item("jobunkid"))

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                    'txtJobDesc.Text = .Item("jobdescrription").ToString
                    If .Item("jobdescrription").ToString.StartsWith("{\rtf") = True Then
                        txtJobDesc.Rtf = .Item("jobdescrription").ToString
                    Else
                        txtJobDesc.Text = .Item("jobdescrription").ToString
                    End If
                    'Sohail (12 Oct 2018) -- End
                    txtNoofPosition.Text = .Item("noofposition").ToString
                    txtWorkStartDate.Text = eZeeDate.convertDate(.Item("workstartdate").ToString).ToShortDateString
                End With

                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                mdtList.Rows.Clear()

                objcolhCheckAll.DataPropertyName = "IsChecked"
                objcolhEmpID.DataPropertyName = "employeeunkid"
                colhEmpCode.DataPropertyName = "employeecode"
                colhEmpName.DataPropertyName = "name"
                colhGrade.DataPropertyName = "grade"
                colhSalaryBand.DataPropertyName = "gradelevel"
                objcolhActionReasonId.DataPropertyName = "actionreasonunkid"
                objcolhAUD.DataPropertyName = "AUD"


                Dim objStaffEmp As New clsStaffrequisition_emp_tran
                Dim objEmp As New clsEmployee_Master
                objStaffEmp._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
                Dim mdtTable As DataTable = objStaffEmp._Datasource
                Dim strEmpIDs As String = String.Join(",", (From p In mdtTable Select (p.Item("employeeunkid").ToString)).ToArray)
                If strEmpIDs.Trim <> "" Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'strFilter = "employeeunkid IN (" & strEmpIDs & ") "
                    strFilter = "hremployee_master.employeeunkid IN (" & strEmpIDs & ") "
                    'Shani(24-Aug-2015) -- End


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsEmp As DataSet = objEmp.GetList("Emp", True, False, , , , , , strFilter)
                    Dim dsEmp As DataSet = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                                          User._Object._Userunkid, _
                                                          FinancialYear._Object._YearUnkid, _
                                                          Company._Object._Companyunkid, _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                                          True, _
                                                          "Emp", _
                                                          ConfigParameter._Object._ShowFirstAppointmentDate, , , strFilter)
                    'S.SANDEEP [04 JUN 2015] -- END

                    Dim result = (From s In mdtTable.AsEnumerable Join e In dsEmp.Tables("Emp").AsEnumerable On s.Item("employeeunkid").ToString Equals e.Item("employeeunkid").ToString Select New With _
                                  {.employeeunkid = CInt(s.Item("employeeunkid")) _
                                   , .employeecode = e.Item("employeecode").ToString _
                                   , .name = e.Item("name").ToString _
                                   , .grade = e.Item("grade").ToString _
                                   , .gradelevel = e.Item("gradelevel").ToString _
                                   , .actionreasonunkid = CInt(e.Item("actionreasonunkid")) _
                                   }).ToList.OrderBy(Function(x) x.name)

                    Dim dRow As DataRow = Nothing
                    For Each item In result
                        dRow = mdtList.NewRow

                        dRow.Item("IsChecked") = False
                        dRow.Item("employeeunkid") = item.employeeunkid
                        dRow.Item("employeecode") = item.employeecode
                        dRow.Item("name") = item.name
                        dRow.Item("grade") = item.grade
                        dRow.Item("gradelevel") = item.gradelevel
                        dRow.Item("actionreasonunkid") = item.actionreasonunkid
                        dRow.Item("AUD") = ""

                        mdtList.Rows.Add(dRow)
                    Next
                End If
                'Sohail (18 Mar 2015) -- End

            Else
                txtJob.Tag = 0
            End If


            objlblPlanned.Text = Format(0, "0")
            objlblAvailable.Text = Format(0, "0")
            objlblVariation.Text = Format(0, "0")
            If CInt(txtJob.Tag) > 0 Then
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
                'dsList = objJob.getHeadCount("Count", , CInt(txtJob.Tag))
                dsList = objJob.getHeadCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, CDate(txtWorkStartDate.Text), CDate(txtWorkStartDate.Text), ConfigParameter._Object._UserAccessModeSetting, True, False, "Count", , CInt(txtJob.Tag))
                'Sohail (12 Oct 2018) -- End
                If dsList.Tables("Count").Rows.Count > 0 Then
                    objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                    objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                    objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                End If
            End If


            'Gajanan [21-April-2020] -- Start   
            mdtStaffRequisitionDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.STAFF_REQUISITION, mintStaffrequisitiontranunkid, ConfigParameter._Object._Document_Path)


            If mdtStaffRequisitionDocument Is Nothing Then Exit Sub
            dgvStaffRequisitionAttachment.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            objcolhfile_path.DataPropertyName = "file_path"
            objcolhlocalpath.DataPropertyName = "localpath"
            objcolhfileuniquename.DataPropertyName = "fileuniquename"

            dgvStaffRequisitionAttachment.DataSource = mdtStaffRequisitionDocument

            'Gajanan [21-April-2020] -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        Finally
            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.           
            dgvEmployeeList.DataSource = mdtList
            dgvEmployeeList.Refresh()
           'Sohail (18 Mar 2015) -- End
            objJob = Nothing
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStaffReqApproval._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid
            objStaffReqApproval._Levelunkid = mintCurrLevelID
            objStaffReqApproval._Priority = mintCurrLevelPriority
            objStaffReqApproval._Approval_Date = ConfigParameter._Object._CurrentDateAndTime
            objStaffReqApproval._Statusunkid = CInt(cboStatus.SelectedValue)
            objStaffReqApproval._Remarks = txtRemarks.Text.Trim
            objStaffReqApproval._Userunkid = User._Object._Userunkid
            objStaffReqApproval._Isvoid = False
            objStaffReqApproval._Voiduserunkid = -1
            objStaffReqApproval._Voiddatetime = Nothing
            objStaffReqApproval._Voidreason = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approval Status."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            ElseIf mintCurrLevelPriority < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! You can not not Approve / Disapprove Staff Requisition. Please contact Administrator."), enMsgBoxStyle.Information)
                Return False
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - System not to allow raising a staff requisition on a position if manpower planning on that position is not planned in 75.1.
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No. 6 : During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs).
            If ConfigParameter._Object._EnforceManpowerPlanning = True Then
                'Hemant (03 Sep 2019) -- End
            If (CInt(objlblVariation.Text) + mdtList.Rows.Count) < CInt(txtNoofPosition.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + mdtList.Rows.Count) & "]. " & Language.getMessage(mstrModuleName, 10, "Please add vacancies on Job master screen."), enMsgBoxStyle.Information)
                Return False
            End If
            End If 'Hemant (03 Sep 2019)

            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'Dim ds As DataSet = objStaffReqApproval.GetList("List", User._Object._Userunkid, , mintStaffrequisitiontranunkid.ToString(), , , , , True, "", True, CInt(enApprovalStatus.PENDING))
            Dim ds As DataSet = objStaffReqApproval.GetList("List", User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, , mintStaffrequisitiontranunkid.ToString(), , , , , True, "", True, CInt(enApprovalStatus.PENDING))
            'Pinkal (16-Nov-2021) -- End


            If ds.Tables("List").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), enMsgBoxStyle.Information)
                Exit Function
            End If
            'Sohail (12 Oct 2018) -- End

            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sending Email(s) process is in progress from other module. Please wait for some time."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmApproveDisapproveStaffRequisition_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStaffReqApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisition_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveStaffRequisition_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisition_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveStaffRequisition_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objStaffReqApproval = New clsStaffrequisition_approval_Tran
        objStaffReq = New clsStaffrequisition_Tran

        'Gajanan [21-April-2020] -- Start   
        objDocument = New clsScan_Attach_Documents
        'Gajanan [21-April-2020] -- End

        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            objStaffReqApproval._Staffrequisitionapprovaltranunkid = mintStaffrequisitionapprovaltranunkid
            objStaffReq._Staffrequisitiontranunkid = mintStaffrequisitiontranunkid

            Call SetColor()
            Call FillCombo()
            Call FillInfo()

            'Hemant (17 Oct 2019) -- Start
            lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (17 Oct 2019) -- End



            'Gajanan [21-April-2020] -- Start   
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.STAFF_REQUISITION).Tables(0).Rows(0)("Name").ToString
            'Gajanan [21-April-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisition_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffrequisition_approval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffrequisition_approval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

#End Region


#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnResult As Boolean
        Dim intIsApproved As Integer = 0
        Dim strMsg As String = ""

        Try
            If IsValid() = False Then Exit Sub

            If CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                'Sohail (02 Dec 2021) -- Start
                'Issue : OLD-529 : WARMA Zambia - Approved Staff requisition Still appears with a Pending Status.
                'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                    'Sohail (02 Dec 2021) -- End
                    strMsg = Language.getMessage(mstrModuleName, 4, "Are you sure you want to Final Approve this Staff Requisition?")
                Else
                    strMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Approve this Staff Requisition?")
                End If

            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                strMsg = Language.getMessage(mstrModuleName, 6, "Are you sure you want to Cancel this Staff Requisition?")
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                strMsg = Language.getMessage(mstrModuleName, 7, "Are you sure you want to Reject this Staff Requisition?")
            End If

            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            objStaffReqApproval._Staffrequisitionapprovaltranunkid = mintStaffrequisitionapprovaltranunkid
            Call SetValue()

            Dim mstrDisApprovalReason As String = String.Empty
            If CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
                If txtRemarks.Text.Trim = "" Then
                    'Hemant (03 Sep 2019) -- End
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only.)
                    'Dim frm As New frmReasonSelection
                    'If User._Object._Isrighttoleft = True Then
                    '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    '    frm.RightToLeftLayout = True
                    '    Call Language.ctlRightToLeftlayOut(frm)
                    'End If


                    'frm.displayDialog(enVoidCategoryType.STAFF_REQUISITION, mstrDisApprovalReason)
                    'If mstrDisApprovalReason.Length <= 0 Then
                    '    Exit Try
                    'End If
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Enter Remarks for Rejection."), enMsgBoxStyle.Information)
                    txtRemarks.Focus()
                    Exit Try
                    'Hemant (07 Oct 2019) -- End
                End If 'Hemant (03 Sep 2019)
            End If
            objStaffReqApproval._Disapprovalreason = mstrDisApprovalReason

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
            If txtRemarks.Text.Trim = "" Then
                objStaffReqApproval._Remarks = mstrDisApprovalReason
            End If
            'Hemant (03 Sep 2019) -- End

            'Sohail (02 Dec 2021) -- Start
            'Issue : OLD-529 : WARMA Zambia - Approved Staff requisition Still appears with a Pending Status.
            'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
            If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CInt(cboStatus.SelectedValue) = enApprovalStatus.APPROVED Then
                'Sohail (02 Dec 2021) -- End
                intIsApproved = enApprovalStatus.APPROVED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.REJECTED Then
                intIsApproved = enApprovalStatus.REJECTED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.CANCELLED Then
                intIsApproved = enApprovalStatus.CANCELLED
            ElseIf CInt(cboStatus.SelectedValue) = enApprovalStatus.PUBLISHED Then
                intIsApproved = enApprovalStatus.PUBLISHED
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnResult = objStaffReqApproval.UpdatestaffrequisitionApproval(mintStaffrequisitionapprovaltranunkid, intIsApproved)
            blnResult = objStaffReqApproval.UpdatestaffrequisitionApproval(mintStaffrequisitionapprovaltranunkid, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)
            'Sohail (21 Aug 2015) -- End

            If blnResult = False And objStaffReqApproval._Message <> "" Then
                eZeeMsgBox.Show(objStaffReqApproval._Message, enMsgBoxStyle.Information)
            Else

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, txtRemarks.Text.Trim, , enLogin_Mode.DESKTOP, 0)
                'Sohail (13 Jun 2016) -- Start
                'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, txtRemarks.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, enLogin_Mode.DESKTOP, 0)
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, txtRemarks.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, enLogin_Mode.DESKTOP, 0)
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 17 : In case of rejection of staff requisition, there is no need for the second dialogue box for user rejecting staff requisition to type in the rejection reason. Remarks section should be enforced if the reject status is selected only ).
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, txtRemarks.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, enLogin_Mode.DESKTOP, 0, ConfigParameter._Object._StaffRequisitionFinalApprovedNotificationUserIds)
                'Sohail (11 Aug 2021) -- Start
                'NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, objStaffReqApproval._Remarks, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, enLogin_Mode.DESKTOP, 0, ConfigParameter._Object._StaffRequisitionFinalApprovedNotificationUserIds)

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, objStaffReqApproval._Remarks, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, FinancialYear._Object._DatabaseName, enLogin_Mode.DESKTOP, 0, ConfigParameter._Object._StaffRequisitionFinalApprovedNotificationUserIds)
                objStaffReqApproval.SendMailToApprover(CInt(cboStatus.SelectedValue), User._Object._Userunkid, CInt(txtAllocation.Tag), CInt(txtName.Tag), txtJob.Text, mintStaffrequisitionapprovaltranunkid, mintStaffrequisitiontranunkid, objStaffReqApproval._Remarks, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, FinancialYear._Object._DatabaseName, ConfigParameter._Object._EmployeeAsOnDate, enLogin_Mode.DESKTOP, 0, ConfigParameter._Object._StaffRequisitionFinalApprovedNotificationUserIds)
                'Pinkal (16-Nov-2021)-- End

                'Sohail (11 Aug 2021) -- End
                'Hemant (03 Sep 2019) -- End
                'Sohail (12 Oct 2018) -- End
                'Sohail (13 Jun 2016) -- End
                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Staff Requisition Approval saved successfully."), enMsgBoxStyle.Information)

                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    cboStatus.SelectedIndex = 0
                    txtRemarks.Text = ""
                Else
                    mintStaffrequisitionapprovaltranunkid = objStaffReqApproval._Staffrequisitionapprovaltranunkid
                    Me.Close()
                End If


                mblnCancel = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Controls Events "
    'Hemant (22 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub lnkViewJobDescription_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewJobDescription.Click
        Try
            Dim frm As New ArutiReports.frmJobReport
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm.DisplayDialog(CInt(txtJob.Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewJobDescription_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Aug 2019) -- End


    'Gajanan [21-April-2020] -- Start   
    Private Sub dgvQualification_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStaffRequisitionAttachment.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtStaffRequisitionDocument.Select("scanattachtranunkid = " & CInt(dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtStaffRequisitionDocument.Select("GUID = '" & dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then
                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtStaffRequisitionDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, The file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualification_CellContentClick :", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
    'Gajanan [21-April-2020] -- End

    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-515) - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
    Private Sub lnkViewStaffRequisitionFormReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewStaffRequisitionFormReport.Click
        Try
            Dim frm As New ArutiReports.frmStaffRequisitionFormReport
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm._StaffRequisitionTranunkid = mintStaffrequisitiontranunkid
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewStaffRequisitionFormReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (01 Nov 2021) -- End

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbStaffReqApprovalnfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStaffReqApprovalnfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbStaffRequisitionList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStaffRequisitionList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.gbStaffReqApprovalnfo.Text = Language._Object.getCaption(Me.gbStaffReqApprovalnfo.Name, Me.gbStaffReqApprovalnfo.Text)
			Me.lblLevelPriority.Text = Language._Object.getCaption(Me.lblLevelPriority.Name, Me.lblLevelPriority.Text)
			Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.gbStaffRequisitionList.Text = Language._Object.getCaption(Me.gbStaffRequisitionList.Name, Me.gbStaffRequisitionList.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
			Me.lblAddStaffReason.Text = Language._Object.getCaption(Me.lblAddStaffReason.Name, Me.lblAddStaffReason.Text)
			Me.lblLeavingReason.Text = Language._Object.getCaption(Me.lblLeavingReason.Name, Me.lblLeavingReason.Text)
			Me.lblWorkStartDate.Text = Language._Object.getCaption(Me.lblWorkStartDate.Name, Me.lblWorkStartDate.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblJobDesc.Text = Language._Object.getCaption(Me.lblJobDesc.Name, Me.lblJobDesc.Text)
			Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
			Me.lblAvailable.Text = Language._Object.getCaption(Me.lblAvailable.Name, Me.lblAvailable.Text)
			Me.lblVariation.Text = Language._Object.getCaption(Me.lblVariation.Name, Me.lblVariation.Text)
			Me.lblPlanned.Text = Language._Object.getCaption(Me.lblPlanned.Name, Me.lblPlanned.Text)
			Me.lblJobHeadCountInfo.Text = Language._Object.getCaption(Me.lblJobHeadCountInfo.Name, Me.lblJobHeadCountInfo.Text)
			Me.lblApproverInfo.Text = Language._Object.getCaption(Me.lblApproverInfo.Name, Me.lblApproverInfo.Text)
			Me.lblNoofPosition.Text = Language._Object.getCaption(Me.lblNoofPosition.Name, Me.lblNoofPosition.Text)
			Me.colhEmpCode.HeaderText = Language._Object.getCaption(Me.colhEmpCode.Name, Me.colhEmpCode.HeaderText)
			Me.colhEmpName.HeaderText = Language._Object.getCaption(Me.colhEmpName.Name, Me.colhEmpName.HeaderText)
			Me.colhGrade.HeaderText = Language._Object.getCaption(Me.colhGrade.Name, Me.colhGrade.HeaderText)
			Me.colhSalaryBand.HeaderText = Language._Object.getCaption(Me.colhSalaryBand.Name, Me.colhSalaryBand.HeaderText)
            Me.lnkViewJobDescription.Text = Language._Object.getCaption(Me.lnkViewJobDescription.Name, Me.lnkViewJobDescription.Text)
			Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
            Me.objcolhDownload.HeaderText = Language._Object.getCaption(Me.objcolhDownload.HeaderText, Me.objcolhDownload.HeaderText)
            Me.objcolhDownload.Text = Language._Object.getCaption(Me.objcolhDownload.Text, Me.objcolhDownload.Text)
            Me.lnkViewStaffRequisitionFormReport.Text = Language._Object.getCaption(Me.lnkViewStaffRequisitionFormReport.Name, Me.lnkViewStaffRequisitionFormReport.Text)
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Approval Status.")
			Language.setMessage(mstrModuleName, 2, "Sorry! You can not not Approve / Disapprove Staff Requisition. Please contact Administrator.")
			Language.setMessage(mstrModuleName, 3, "Sending Email(s) process is in progress from other module. Please wait for some time.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to Final Approve this Staff Requisition?")
			Language.setMessage(mstrModuleName, 5, "Are you sure you want to Approve this Staff Requisition?")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to Cancel this Staff Requisition?")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to Reject this Staff Requisition?")
			Language.setMessage(mstrModuleName, 8, "Staff Requisition Approval saved successfully.")
			Language.setMessage(mstrModuleName, 9, "Sorry, No. of position should not be greater than Manpower variations.")
			Language.setMessage(mstrModuleName, 10, "Please add vacancies on Job master screen.")
			Language.setMessage(mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level.")
			Language.setMessage(mstrModuleName, 12, "Please Enter Remarks for Rejection.")
			Language.setMessage(mstrModuleName, 13, "Sorry, The file you are trying to access does not exists on Aruti self service application folder.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class