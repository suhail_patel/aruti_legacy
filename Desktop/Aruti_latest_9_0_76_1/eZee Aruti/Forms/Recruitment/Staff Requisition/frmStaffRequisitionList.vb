﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmStaffRequisitionList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmStaffRequisitionList"
    Private objStaffRequisition As clsStaffrequisition_Tran
    Private mdicAllocaions As New Dictionary(Of Integer, String)
    Private mdicApprovalStatus As New Dictionary(Of Integer, String)

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboAllocation.BackColor = GUI.ColorComp
            cboName.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsStaffRequisitionApproverlevel_master
        Dim objOption As New clsPassowdOptions
        Dim objMaster As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'dsCombo = objMaster.GetEAllocation_Notification("List")
            dsCombo = objMaster.GetEAllocation_Notification("List", , , True)
            'Sohail (12 Oct 2018) -- End
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedIndex = 0
            End With
            mdicAllocaions = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("ApprovalStatus", True, , True, True, True, True, True)
            mdicApprovalStatus = (From p In dsCombo.Tables("ApprovalStatus").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("List", True, True, True, True, True, True, True)
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = enApprovalStatus.PENDING
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Dim lvItem As ListViewItem
        Try
            lvStaffReqList.Items.Clear()

            If User._Object.Privilege._AllowToViewStaffRequisitionList = False Then Exit Try

            If CInt(cboAllocation.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_tran.staffrequisitionbyid = " & CInt(cboAllocation.SelectedValue) & " "
            End If

            If CInt(cboName.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_tran.allocationunkid = " & CInt(cboName.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_tran.form_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            Dim intUserUnkId As Integer = 0
            If chkMyApprovals.Checked = True Then
                intUserUnkId = User._Object._Userunkid
            End If
            'Sohail (12 Oct 2018) -- End

            If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'dsList = objStaffRequisition.GetList("List", StrSearch)
            dsList = objStaffRequisition.GetList("List", StrSearch, , intUserUnkId)
            Dim rtf As New RichTextBox
            'Sohail (12 Oct 2018) -- End

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = dtRow.Item("staffrequisitiontranunkid")

                lvItem.SubItems.Add(dtRow.Item("formno").ToString)

                lvItem.SubItems.Add(mdicAllocaions.Item(CInt(dtRow.Item("staffrequisitionbyid"))))
                lvItem.SubItems(colhAllocation.Index).Tag = CInt(dtRow.Item("staffrequisitionbyid"))

                Select Case CInt(dtRow.Item("staffrequisitionbyid"))

                    Case enAllocation.BRANCH
                        lvItem.SubItems.Add(dtRow.Item("Branch").ToString)

                    Case enAllocation.DEPARTMENT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("DepartmentGroup").ToString)

                    Case enAllocation.DEPARTMENT
                        lvItem.SubItems.Add(dtRow.Item("Department").ToString)

                    Case enAllocation.SECTION_GROUP
                        lvItem.SubItems.Add(dtRow.Item("SectionGroup").ToString)

                    Case enAllocation.SECTION
                        lvItem.SubItems.Add(dtRow.Item("Section").ToString)

                    Case enAllocation.UNIT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("UnitGroup").ToString)

                    Case enAllocation.UNIT
                        lvItem.SubItems.Add(dtRow.Item("Unit").ToString)

                    Case enAllocation.TEAM
                        lvItem.SubItems.Add(dtRow.Item("Team").ToString)

                    Case enAllocation.JOB_GROUP
                        lvItem.SubItems.Add(dtRow.Item("JobGroup").ToString)

                    Case enAllocation.JOBS
                        lvItem.SubItems.Add(dtRow.Item("Job").ToString)

                    Case enAllocation.CLASS_GROUP
                        lvItem.SubItems.Add(dtRow.Item("ClassGroup").ToString)

                    Case enAllocation.CLASSES
                        lvItem.SubItems.Add(dtRow.Item("Class").ToString)

                End Select
                lvItem.SubItems(colhName.Index).Tag = CInt(dtRow.Item("allocationunkid"))

                lvItem.SubItems.Add(dtRow.Item("JobTitle").ToString)
                lvItem.SubItems(colhJob.Index).Tag = CInt(dtRow.Item("jobunkid"))

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                'lvItem.SubItems.Add(dtRow.Item("jobdescrription").ToString)
                If dtRow.Item("jobdescrription").ToString.StartsWith("{\rtf") Then
                    rtf.Rtf = dtRow.Item("jobdescrription").ToString
                Else
                    rtf.Text = dtRow.Item("jobdescrription").ToString
                End If
                lvItem.SubItems.Add(rtf.Text)
                'Sohail (12 Oct 2018) -- End

                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("workstartdate").ToString).ToShortDateString)

                lvItem.SubItems.Add(mdicApprovalStatus.Item(CInt(dtRow.Item("form_statusunkid"))).ToString)
                lvItem.SubItems(colhStatus.Index).Tag = CInt(dtRow.Item("form_statusunkid"))

                lvStaffReqList.Items.Add(lvItem)
            Next

            lvStaffReqList.SortBy(colhAllocation.Index, SortOrder.Ascending) 'Sohail (12 Oct 2018)

            lvStaffReqList.GridLines = False
            lvStaffReqList.GroupingColumn = colhAllocation
            lvStaffReqList.DisplayGroups(True)

            If lvStaffReqList.Items.Count > 3 Then
                colhJobDesc.Width = 150 - 18
            Else
                colhJobDesc.Width = 150
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(lvStaffReqList.Items.Count))
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddStaffRequisition
            btnEdit.Enabled = User._Object.Privilege._AllowToEditStaffRequisition
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteStaffRequisition
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmStaffRequisitionList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objStaffRequisition = New clsStaffrequisition_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call Fill_Combo()

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            chkMyApprovals.Checked = True
            'Sohail (12 Oct 2018) -- End

            If lvStaffReqList.Items.Count > 0 Then lvStaffReqList.Items(0).Selected = True
            lvStaffReqList.Select()
            lvStaffReqList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionList_Closed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffrequisition_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffrequisition_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmStaffRequisition_AddEdit
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvStaffReqList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Staff Requisition from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.APPROVED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.REJECTED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.CANCELLED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.PUBLISHED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        End If
        Dim frm As New frmStaffRequisition_AddEdit
        Try

            If frm.displayDialog(CInt(lvStaffReqList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvStaffReqList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Staff Requisition from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.APPROVED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.REJECTED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.CANCELLED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        ElseIf CInt(lvStaffReqList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.PUBLISHED Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published."), enMsgBoxStyle.Information)
            lvStaffReqList.Select()
            Exit Sub
        End If

        Try
            If objStaffRequisition.isUsed(CInt(lvStaffReqList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot delete this Staff Requisition. Reason: This Staff Requisition is in use."), enMsgBoxStyle.Information) '?2
                lvStaffReqList.Select()
                Exit Try
            End If


            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this Staff Requisition?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.STAFF_REQUISITION, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                If objStaffRequisition.Void(CInt(lvStaffReqList.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
                    lvStaffReqList.SelectedItems(0).Remove()

                    If lvStaffReqList.Items.Count <= 0 Then
                        Exit Try
                    End If
                End If
            End If
            lvStaffReqList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub mnuStaffRequisitionApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStaffRequisitionApproval.Click, btnOperations.Click
        Try
            Dim objFrm As New frmApproveDisapproveStaffRequisitionList
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuStaffRequisitionApproval_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAllocation.SelectedIndex = 0
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'cboStatus.SelectedValue = 0
            cboStatus.SelectedValue = CInt(enApprovalStatus.PENDING)
            'Sohail (12 Oct 2018) -- End
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub



    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   

    'Gajanan [02-SEP-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    'Private Sub mnuStaffRequisitionAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStaffRequisitionAttachment.Click

    '    Dim frm As New frmScanOrAttachmentInfo
    '    Try

    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If


    '        If lvStaffReqList.SelectedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Staff from the list to perform further operation."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If



    '        Dim item As ListViewItem = lvStaffReqList.SelectedItems(0)
    '        frm._TransactionID = CInt(item.Tag)

    'frm.displayDialog(Language.getMessage(mstrModuleName, 13, "Select Employee"), enImg_Email_RefId.Staff_Requisition, enAction.ADD_ONE, "-1", _
    '                  mstrModuleName, True, -1, enScanAttactRefId.STAFF_REQUISITION, , False)

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuStaffRequisitionAttachment_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub
    'Gajanan [02-SEP-2019] -- End


    'Gajanan [6-NOV-2019] -- End

#End Region

#Region " Combobox Events "

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 16, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 17, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 18, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 19, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 20, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 21, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 22, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 23, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 24, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 25, "Classes")

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
                Case Else
                    dsCombos = Nothing
                    'Sohail (12 Oct 2018) -- End

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            Else
                cboName.DataSource = Nothing
                cboName.Items.Clear()
                'Sohail (12 Oct 2018) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
			Me.colhJobDesc.Text = Language._Object.getCaption(CStr(Me.colhJobDesc.Tag), Me.colhJobDesc.Text)
			Me.colhWorkStart.Text = Language._Object.getCaption(CStr(Me.colhWorkStart.Tag), Me.colhWorkStart.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.colhUnkId.Text = Language._Object.getCaption(CStr(Me.colhUnkId.Tag), Me.colhUnkId.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuStaffRequisitionApproval.Text = Language._Object.getCaption(Me.mnuStaffRequisitionApproval.Name, Me.mnuStaffRequisitionApproval.Text)
			Me.colhFormNo.Text = Language._Object.getCaption(CStr(Me.colhFormNo.Tag), Me.colhFormNo.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Staff Requisition from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved.")
			Language.setMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected.")
			Language.setMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot delete this Staff Requisition. Reason: This Staff Requisition is in use.")
			Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this Staff Requisition?")
			Language.setMessage(mstrModuleName, 14, "Branch")
			Language.setMessage(mstrModuleName, 15, "Department Group")
			Language.setMessage(mstrModuleName, 16, "Department")
			Language.setMessage(mstrModuleName, 17, "Section Group")
			Language.setMessage(mstrModuleName, 18, "Section")
			Language.setMessage(mstrModuleName, 19, "Unit Group")
			Language.setMessage(mstrModuleName, 20, "Unit")
			Language.setMessage(mstrModuleName, 21, "Team")
			Language.setMessage(mstrModuleName, 22, "Job Group")
			Language.setMessage(mstrModuleName, 23, "Jobs")
			Language.setMessage(mstrModuleName, 24, "Class Group")
			Language.setMessage(mstrModuleName, 25, "Classes")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class