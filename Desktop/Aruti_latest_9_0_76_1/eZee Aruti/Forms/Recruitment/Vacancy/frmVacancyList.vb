﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmVacancyList

    Private ReadOnly mstrModuleName As String = "frmVacancyList"
    Private mintVacancyunkid As Integer
    Private objVacancyData As clsVacancy

#Region " Private Methods "
    'Sohail (11 Sep 2010) -- Start
    Private Sub SetColor()
        Try
            cboStation.BackColor = GUI.ColorOptional
            cboJobDepartment.BackColor = GUI.ColorOptional
            cboSections.BackColor = GUI.ColorOptional
            cboUnits.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboEmployeementType.BackColor = GUI.ColorOptional
            cboPayType.BackColor = GUI.ColorOptional
            txtNoofPosition.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objStation As New clsStation
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As DataSet
        Try
            dsCombo = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Station")
                .SelectedValue = 0
            End With

            dsCombo = objDepartment.getComboList("Department", True)
            With cboJobDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Department")
                .SelectedValue = 0
            End With

            dsCombo = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombo = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Unit")
                .SelectedValue = 0
            End With

            dsCombo = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("JobGrp")
                .SelectedValue = 0
            End With

            dsCombo = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Job")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
            With cboEmployeementType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("EmplType")
                .SelectedValue = 0
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboPayType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PayType")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsCombo = objVacancyData.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objStation = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objCommon = Nothing
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub FillList()
        Dim strSearching As String = "" 'Sohail (11 Sep 2010)
        Dim dsVacancy As New DataSet
        Dim dtTable As DataTable 'Sohail (11 Sep 2010)

        Try

            If User._Object.Privilege._AllowToViewVacancyMasterList = True Then                'Pinkal (02-Jul-2012) -- Start

                objVacancyData = New clsVacancy
                dsVacancy = objVacancyData.GetList("List", True)

                'Sohail (11 Sep 2010) -- Start
                If CInt(cboStation.SelectedValue) > 0 Then
                    strSearching &= "AND stationunkid = " & CInt(cboStation.SelectedValue) & " "
                End If
                If CInt(cboJobDepartment.SelectedValue) > 0 Then
                    strSearching &= "AND departmentunkid = " & CInt(cboJobDepartment.SelectedValue) & " "
                End If
                If CInt(cboSections.SelectedValue) > 0 Then
                    strSearching &= "AND sectionunkid = " & CInt(cboSections.SelectedValue) & " "
                End If
                If CInt(cboUnits.SelectedValue) > 0 Then
                    strSearching &= "AND unitunkid = " & CInt(cboUnits.SelectedValue) & " "
                End If
                If CInt(cboJobGroup.SelectedValue) > 0 Then
                    strSearching &= "AND jobgroupunkid = " & CInt(cboJobGroup.SelectedValue) & " "
                End If
                If CInt(cboJob.SelectedValue) > 0 Then
                    strSearching &= "AND jobunkid = " & CInt(cboJob.SelectedValue) & " "
                End If
                If CInt(cboEmployeementType.SelectedValue) > 0 Then
                    strSearching &= "AND employeementtypeunkid = " & CInt(cboEmployeementType.SelectedValue) & " "
                End If
                If CInt(cboPayType.SelectedValue) > 0 Then
                    strSearching &= "AND paytypeunkid = " & CInt(cboPayType.SelectedValue) & " "
                End If
                If txtNoofPosition.Decimal <> 0 Then
                    strSearching &= "AND noofposition = " & txtNoofPosition.Decimal & " "
                End If
                If dtpStartDate.Checked = True Then
                    strSearching &= "AND openingdate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' "
                End If
                If dtpEndDate.Checked = True Then
                    strSearching &= "AND closingdate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "' "
                End If

                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES

                If CInt(cboVacancyType.SelectedValue) > 0 Then
                    Select Case CInt(cboVacancyType.SelectedValue)
                        Case enVacancyType.EXTERNAL_VACANCY
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strSearching &= "AND isexternalvacancy = 1 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND (isexternalvacancy = 1 OR ISNULL(isbothintext, 0) = 1) "
                            strSearching &= "AND (isexternalvacancy = 1 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End
                        Case enVacancyType.INTERNAL_VACANCY
                            'Sohail (13 Mar 2020) -- Start
                            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                            'strSearching &= "AND isexternalvacancy = 0 "
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                            'strSearching &= "AND (isexternalvacancy = 0 OR ISNULL(isbothintext, 0) = 1) "
                            strSearching &= "AND (isexternalvacancy = 0 AND ISNULL(isbothintext, 0) = 0) "
                            'Sohail (18 Apr 2020) -- End
                            'Sohail (13 Mar 2020) -- End
                            'Sohail (18 Apr 2020) -- Start
                            'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                        Case enVacancyType.INTERNAL_AND_EXTERNAL
                            strSearching &= "AND ISNULL(isbothintext, 0) = 1 "
                            'Sohail (18 Apr 2020) -- End
                    End Select
                End If
                'S.SANDEEP [ 25 DEC 2011 ] -- END

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtTable = New DataView(dsVacancy.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsVacancy.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                End If
                'Sohail (11 Sep 2010) -- End

                Dim lvItem As ListViewItem

                lvVacancyList.Items.Clear()

                'For Each dtRow As DataRow In dsVacancy.Tables(0).Rows
                For Each dtRow As DataRow In dtTable.Rows 'Sohail (11 Sep 2010)

                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("vacancytitle").ToString
                    'Sohail (11 Feb 2022) -- Start
                    'Enhancement : OLD-564  : NMB - NMB - Vacancy List screen Enhancement - Include a column having the staff requisition number next to the vacancy - Include class and class group columns on the vacancy master screen.
                    lvItem.SubItems.Add(dtRow.Item("formno").ToString)
                    lvItem.SubItems.Add(dtRow.Item("classesname").ToString)
                    lvItem.SubItems.Add(dtRow.Item("classgroupname").ToString)
                    'Sohail ((11 Feb 2022) -- End
                    lvItem.SubItems.Add(dtRow.Item("departmentname").ToString)
                    'Hemant (01 Nov 2021) -- Start
                    'ENHANCEMENT : OLD-510 - Upon final approval of staff requisition,Display Job Title on Vacancy List so that user can easily identify the vacancy title. Currently the system is only showing the number of position(s).
                    lvItem.SubItems.Add(dtRow.Item("jobname").ToString)
                    'Hemant (01 Nov 2021) -- End
                    lvItem.SubItems.Add(dtRow.Item("employeementtypename").ToString)
                    lvItem.SubItems.Add(dtRow.Item("paytype").ToString)
                    lvItem.SubItems.Add(dtRow.Item("noofposition").ToString)
                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("interview_startdate").ToString).ToShortDateString)
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("interview_closedate").ToString).ToShortDateString)
                    'Sohail (28 May 2014) -- Start
                    'Enhancement - Staff Requisition.
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString)
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString)
                    If IsDBNull(dtRow.Item("openingdate")) = True Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("openingdate").ToString).ToShortDateString)
                    End If
                    If IsDBNull(dtRow.Item("closingdate")) = True Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("closingdate").ToString).ToShortDateString)
                    End If
                    'Sohail (28 May 2014) -- End
                    'S.SANDEEP [ 25 DEC 2011 ] -- END

                    lvItem.Tag = dtRow.Item("vacancyunkid")

                    'S.SANDEEP [ 25 DEC 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES

                    If CBool(dtRow.Item("isexternalvacancy")) = True Then
                        lvItem.ForeColor = objpnlExVacancyColor.BackColor
                    End If

                    'S.SANDEEP [ 25 DEC 2011 ] -- END

                    lvVacancyList.Items.Add(lvItem)

                Next

                If lvVacancyList.Items.Count > 11 Then
                    colhVacancy.Width = 170 - 18
                Else
                    colhVacancy.Width = 170
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisiblity()
        Try
            'Sohail (28 May 2014) -- Start
            'Enhancement - Staff Requisition.
            'btnNew.Enabled = User._Object.Privilege._AllowtoAddVacancy
            btnNew.Enabled = User._Object.Privilege._AllowtoAddVacancy AndAlso Not (ConfigParameter._Object._ApplyStaffRequisition)
            'Sohail (28 May 2014) -- End
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditVacancy
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteVacancy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

#Region " Form's Events "
    'Sohail (11 Sep 2010) -- Start
    Private Sub frmVacancyList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{TAB}")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyList_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub frmVacancyList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        'If e.KeyCode = Keys.Delete Then
        If lvVacancyList.Focused AndAlso e.KeyCode = Keys.Delete Then 'Sohail (11 Sep 2010)
            btnDelete.PerformClick()
        End If
    End Sub
    Private Sub frmVacancyList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmVacancyList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objVacancyData = Nothing
    End Sub

    Private Sub frmVacancyList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objVacancyData = New clsVacancy
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            OtherSettings()  'Hemant (15 Nov 2019)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Sohail (11 Sep 2010) -- Start
            Call SetColor()
            Call FillCombo()
            'Sohail (11 Sep 2010) -- End

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement REQ # : Before publishing a vacancy to web / exporting to web, provide option to preview the vacancy (Preview it to get a feel of how the applicant will see it) – TC005	
            lnkPreviewExternal.Visible = False
            lnkPreviewInternal.Visible = False
            lnkPreviewExternal.Links.Clear()
            lnkPreviewInternal.Links.Clear()
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : It should be possible to preview a vacancy before publishing it.
            'If ConfigParameter._Object._WebURL.Trim.Length > 0 Then
            '   lnkPreviewExternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._WebURL, "/login.aspx", "/ALogin.aspx", , , CompareMethod.Text))
            If ConfigParameter._Object._RecruitmentWebServiceLink.Trim.Length > 0 Then
                lnkPreviewExternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._ArutiSelfServiceURL, "/index.aspx", "", , , CompareMethod.Text) & "/Recruitment/Career.aspx?code=" & Company._Object._Code & "&Ext=7")
                'Sohail (13 Mar 2020) -- End
                lnkPreviewExternal.Visible = True
            End If
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'If ConfigParameter._Object._WebURLInternal.Trim.Length > 0 Then
            '    lnkPreviewInternal.Links.Add(0, lnkPreviewInternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._WebURLInternal, "/login.aspx", "/ALogin.aspx", , , CompareMethod.Text))
            If ConfigParameter._Object._RecruitmentWebServiceLink.Trim.Length > 0 Then
                lnkPreviewInternal.Links.Add(0, lnkPreviewExternal.Text.Length, Microsoft.VisualBasic.Strings.Replace(ConfigParameter._Object._ArutiSelfServiceURL, "/index.aspx", "", , , CompareMethod.Text) & "/Recruitment/Career.aspx?code=" & Company._Object._Code & "&Ext=9")
                'Sohail (13 Mar 2020) -- End
                lnkPreviewInternal.Visible = True
            End If
            'Sohail (17 Sep 2019) -- End

            'Call OtherSettings()
            'Call FillCombo()
            Call FillList()

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'chkIsExternalVacancy.Checked = False
            'objpnlExVacancyColor.Visible = False
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            If lvVacancyList.Items.Count > 0 Then lvVacancyList.Items(0).Selected = True
            lvVacancyList.Select()



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisiblity()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsVacancy.SetMessages()
            objfrm._Other_ModuleNames = "clsVacancy"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjfrmVacancy_LoadAddEdit As New frmVacancy_AddEdit
            'Sohail (11 Sep 2010) -- Start
            'ObjfrmVacancy_LoadAddEdit.displayDialog(-1, enAction.ADD_ONE)
            If ObjfrmVacancy_LoadAddEdit.displayDialog(-1, enAction.ADD_ONE) = True Then
                Call FillList()
            End If
            'Sohail (11 Sep 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvVacancyList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvVacancyList.Select()
            Exit Sub
        End If

        Dim frm As New frmVacancy_AddEdit

        Try

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVacancyList.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvVacancyList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            'lvVacancyList.Items(intSelectedIndex).Selected = True
            'lvVacancyList.EnsureVisible(intSelectedIndex)
            'lvVacancyList.Select()



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvVacancyList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvVacancyList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvVacancyList.SelectedItems(0).Index
            objVacancyData._Vacancyunkid = CInt(lvVacancyList.SelectedItems(0).Tag)
            'Sohail (15 Jul 2014) -- Start
            'Enhancement - Consider Max/Min Level as per Last Approver Mapped and NoOfPosition on Staff Requisition.
            If objVacancyData._Staffrequisitiontranunkid > 0 Then
                Dim objStaffReq As New clsStaffrequisition_Tran
                objStaffReq._Staffrequisitiontranunkid = objVacancyData._Staffrequisitiontranunkid
                If objStaffReq._Isvoid = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You can not delete this Vacancy. Reason : This Vacancy is generated from Staff Requision Form No.") & " " & objStaffReq._Formno, enMsgBoxStyle.Information)
                    Exit Try
                End If
            End If
            'Sohail (15 Jul 2014) -- End
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Vacancy?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                objVacancyData._Voiduserunkid = User._Object._Userunkid
                objVacancyData._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.VACANCY, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objVacancyData._Voidreason = mstrVoidReason
                End If
                frm = Nothing


                objVacancyData.Delete(CInt(lvVacancyList.SelectedItems(0).Tag))
                'Sandeep [ 09 Oct 2010 ] -- Start
                'lvVacancyList.SelectedItems(0).Remove()
                If objVacancyData._Message <> "" Then
                    eZeeMsgBox.Show(objVacancyData._Message, enMsgBoxStyle.Information)
                Else
                    lvVacancyList.SelectedItems(0).Remove()
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 

                If lvVacancyList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvVacancyList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvVacancyList.Items.Count - 1
                    lvVacancyList.Items(intSelectedIndex).Selected = True
                    lvVacancyList.EnsureVisible(intSelectedIndex)
                ElseIf lvVacancyList.Items.Count <> 0 Then
                    lvVacancyList.Items(intSelectedIndex).Selected = True
                    lvVacancyList.EnsureVisible(intSelectedIndex)
                End If
            End If

            lvVacancyList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub
#End Region

    'Sohail (11 Sep 2010) -- Start
#Region " Other's Controls Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboStation.SelectedValue = 0
            cboJobDepartment.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnits.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboEmployeementType.SelectedValue = 0
            cboPayType.SelectedValue = 0
            txtNoofPosition.Decimal = 0
            dtpStartDate.Value = DateTime.Today.Date
            dtpStartDate.Checked = False
            dtpEndDate.Value = DateTime.Today.Date
            dtpEndDate.Checked = False
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboVacancyType.SelectedIndex = 0
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement REQ # : Before publishing a vacancy to web / exporting to web, provide option to preview the vacancy (Preview it to get a feel of how the applicant will see it) – TC005	
    Private Sub lnkPreviewExternal_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkPreviewExternal.LinkClicked, lnkPreviewInternal.LinkClicked
        Try
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, CType(sender, LinkLabel).Name & "_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Sep 2019) -- End

#End Region
    'Sohail (11 Sep 2010) -- End


    Private Sub dtpStartDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged

        Try
            If dtpStartDate.Checked = True Then
                dtpEndDate.Checked = True
                dtpEndDate.Value = dtpStartDate.Value
            Else
                dtpEndDate.Checked = False
                dtpEndDate.Value = dtpStartDate.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub



    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub chkIsExternalVacancy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If chkIsExternalVacancy.CheckState = CheckState.Checked Then
    '            objpnlExVacancyColor.Visible = True
    '        Else
    '            objpnlExVacancyColor.Visible = False
    '        End If
    '        'Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkIsExternalVacancy_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#Region " Combobox Events "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Select Case CInt(cboVacancyType.SelectedValue)
                Case enVacancyType.EXTERNAL_VACANCY
                    objpnlExVacancyColor.Visible = True
                Case enVacancyType.INTERNAL_VACANCY
                    objpnlExVacancyColor.Visible = False
                Case Else
                    objpnlExVacancyColor.Visible = True

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 Feb 2022) -- Start
    'Enhancement : OLD-564  : NMB - NMB - Vacancy List screen Enhancement - Make the drop down menus on the vacancy master screen as smart searches.
    Private Sub cboJob_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboJob.KeyPress, cboStation.KeyPress, cboJobDepartment.KeyPress, cboSections.KeyPress, cboUnits.KeyPress, cboJobGroup.KeyPress, cboEmployeementType.KeyPress, cboPayType.KeyPress, cboVacancyType.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = ""
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail ((11 Feb 2022) -- End

#End Region




    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
            Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhEmployeementType.Text = Language._Object.getCaption(CStr(Me.colhEmployeementType.Tag), Me.colhEmployeementType.Text)
            Me.colhPayType.Text = Language._Object.getCaption(CStr(Me.colhPayType.Tag), Me.colhPayType.Text)
            Me.colhNoofPosition.Text = Language._Object.getCaption(CStr(Me.colhNoofPosition.Tag), Me.colhNoofPosition.Text)
            Me.colhVacancyDateFrom.Text = Language._Object.getCaption(CStr(Me.colhVacancyDateFrom.Tag), Me.colhVacancyDateFrom.Text)
            Me.colhVacancyEnd.Text = Language._Object.getCaption(CStr(Me.colhVacancyEnd.Tag), Me.colhVacancyEnd.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuPrintCourse.Text = Language._Object.getCaption(Me.mnuPrintCourse.Name, Me.mnuPrintCourse.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblJobDept.Text = Language._Object.getCaption(Me.lblJobDept.Name, Me.lblJobDept.Text)
            Me.lblEmployeementType.Text = Language._Object.getCaption(Me.lblEmployeementType.Name, Me.lblEmployeementType.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblVacancyEndDate.Text = Language._Object.getCaption(Me.lblVacancyEndDate.Name, Me.lblVacancyEndDate.Text)
            Me.lblVacancyStartDate.Text = Language._Object.getCaption(Me.lblVacancyStartDate.Name, Me.lblVacancyStartDate.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.lblNoofPosition.Text = Language._Object.getCaption(Me.lblNoofPosition.Name, Me.lblNoofPosition.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.lnkPreviewInternal.Text = Language._Object.getCaption(Me.lnkPreviewInternal.Name, Me.lnkPreviewInternal.Text)
			Me.lnkPreviewExternal.Text = Language._Object.getCaption(Me.lnkPreviewExternal.Name, Me.lnkPreviewExternal.Text)
			Me.lblExternalVacancies.Text = Language._Object.getCaption(Me.lblExternalVacancies.Name, Me.lblExternalVacancies.Text)
			Me.colhJobTitle.Text = Language._Object.getCaption(CStr(Me.colhJobTitle.Tag), Me.colhJobTitle.Text)
			Me.colhFormno.Text = Language._Object.getCaption(CStr(Me.colhFormno.Tag), Me.colhFormno.Text)
			Me.colhClass.Text = Language._Object.getCaption(CStr(Me.colhClass.Tag), Me.colhClass.Text)
			Me.colhClassGroup.Text = Language._Object.getCaption(CStr(Me.colhClassGroup.Tag), Me.colhClassGroup.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Vacancy from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Vacancy?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You can not delete this Vacancy. Reason : This Vacancy is generated from Staff Requision Form No.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class