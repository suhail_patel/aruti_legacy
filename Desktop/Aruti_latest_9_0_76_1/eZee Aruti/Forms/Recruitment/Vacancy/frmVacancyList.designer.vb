﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVacancyList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVacancyList))
        Me.pnlCourseScheduling = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.txtNoofPosition = New eZee.TextBox.NumericTextBox
        Me.lblNoofPosition = New System.Windows.Forms.Label
        Me.lblVacancyEndDate = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblPayType = New System.Windows.Forms.Label
        Me.lblVacancyStartDate = New System.Windows.Forms.Label
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.lblJobDept = New System.Windows.Forms.Label
        Me.lblUnits = New System.Windows.Forms.Label
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.cboJobDepartment = New System.Windows.Forms.ComboBox
        Me.cboEmployeementType = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblEmployeementType = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lvVacancyList = New System.Windows.Forms.ListView
        Me.colhVacancy = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhJobTitle = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeementType = New System.Windows.Forms.ColumnHeader
        Me.colhPayType = New System.Windows.Forms.ColumnHeader
        Me.colhNoofPosition = New System.Windows.Forms.ColumnHeader
        Me.colhVacancyDateFrom = New System.Windows.Forms.ColumnHeader
        Me.colhVacancyEnd = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblExternalVacancies = New System.Windows.Forms.Label
        Me.lnkPreviewInternal = New System.Windows.Forms.LinkLabel
        Me.lnkPreviewExternal = New System.Windows.Forms.LinkLabel
        Me.objpnlExVacancyColor = New System.Windows.Forms.Panel
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.mnuPrint = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintCourse = New System.Windows.Forms.ToolStripMenuItem
        Me.colhFormno = New System.Windows.Forms.ColumnHeader
        Me.colhClass = New System.Windows.Forms.ColumnHeader
        Me.colhClassGroup = New System.Windows.Forms.ColumnHeader
        Me.pnlCourseScheduling.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuPrint.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCourseScheduling
        '
        Me.pnlCourseScheduling.Controls.Add(Me.eZeeHeader)
        Me.pnlCourseScheduling.Controls.Add(Me.gbFilterCriteria)
        Me.pnlCourseScheduling.Controls.Add(Me.lvVacancyList)
        Me.pnlCourseScheduling.Controls.Add(Me.objFooter)
        Me.pnlCourseScheduling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCourseScheduling.Location = New System.Drawing.Point(0, 0)
        Me.pnlCourseScheduling.Name = "pnlCourseScheduling"
        Me.pnlCourseScheduling.Size = New System.Drawing.Size(883, 482)
        Me.pnlCourseScheduling.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(883, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Vacancy List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.txtNoofPosition)
        Me.gbFilterCriteria.Controls.Add(Me.lblNoofPosition)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnits)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayType)
        Me.gbFilterCriteria.Controls.Add(Me.lblJobDept)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnits)
        Me.gbFilterCriteria.Controls.Add(Me.cboStation)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployeementType)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeementType)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 117
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(859, 116)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownHeight = 200
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.IntegralHeight = False
        Me.cboVacancyType.Location = New System.Drawing.Point(523, 33)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(118, 21)
        Me.cboVacancyType.TabIndex = 179
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(434, 35)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(80, 16)
        Me.lblVacancyType.TabIndex = 180
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(224, 62)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(80, 16)
        Me.lblJobGroup.TabIndex = 174
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownHeight = 200
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.IntegralHeight = False
        Me.cboJobGroup.Location = New System.Drawing.Point(310, 60)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(118, 21)
        Me.cboJobGroup.TabIndex = 4
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(87, 15)
        Me.lblBranch.TabIndex = 168
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNoofPosition
        '
        Me.txtNoofPosition.AllowNegative = True
        Me.txtNoofPosition.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNoofPosition.DigitsInGroup = 0
        Me.txtNoofPosition.Flags = 0
        Me.txtNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoofPosition.Location = New System.Drawing.Point(743, 33)
        Me.txtNoofPosition.MaxDecimalPlaces = 6
        Me.txtNoofPosition.MaxWholeDigits = 21
        Me.txtNoofPosition.Name = "txtNoofPosition"
        Me.txtNoofPosition.Prefix = ""
        Me.txtNoofPosition.RangeMax = 1.7976931348623157E+308
        Me.txtNoofPosition.RangeMin = -1.7976931348623157E+308
        Me.txtNoofPosition.Size = New System.Drawing.Size(107, 21)
        Me.txtNoofPosition.TabIndex = 6
        Me.txtNoofPosition.Text = "0"
        '
        'lblNoofPosition
        '
        Me.lblNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoofPosition.Location = New System.Drawing.Point(654, 35)
        Me.lblNoofPosition.Name = "lblNoofPosition"
        Me.lblNoofPosition.Size = New System.Drawing.Size(83, 16)
        Me.lblNoofPosition.TabIndex = 164
        Me.lblNoofPosition.Text = "No. of Position"
        Me.lblNoofPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancyEndDate
        '
        Me.lblVacancyEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyEndDate.Location = New System.Drawing.Point(656, 90)
        Me.lblVacancyEndDate.Name = "lblVacancyEndDate"
        Me.lblVacancyEndDate.Size = New System.Drawing.Size(77, 15)
        Me.lblVacancyEndDate.TabIndex = 29
        Me.lblVacancyEndDate.Text = "To"
        Me.lblVacancyEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(523, 87)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpStartDate.TabIndex = 9
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(743, 87)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpEndDate.TabIndex = 10
        '
        'lblPayType
        '
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(656, 63)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(78, 15)
        Me.lblPayType.TabIndex = 37
        Me.lblPayType.Text = "Pay Type"
        Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancyStartDate
        '
        Me.lblVacancyStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyStartDate.Location = New System.Drawing.Point(434, 90)
        Me.lblVacancyStartDate.Name = "lblVacancyStartDate"
        Me.lblVacancyStartDate.Size = New System.Drawing.Size(84, 15)
        Me.lblVacancyStartDate.TabIndex = 28
        Me.lblVacancyStartDate.Text = "Vacancy From"
        Me.lblVacancyStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnits
        '
        Me.cboUnits.DropDownHeight = 200
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.IntegralHeight = False
        Me.cboUnits.Location = New System.Drawing.Point(310, 33)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(118, 21)
        Me.cboUnits.TabIndex = 3
        '
        'cboPayType
        '
        Me.cboPayType.DropDownHeight = 200
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.IntegralHeight = False
        Me.cboPayType.Location = New System.Drawing.Point(743, 60)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(107, 21)
        Me.cboPayType.TabIndex = 8
        '
        'lblJobDept
        '
        Me.lblJobDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDept.Location = New System.Drawing.Point(8, 63)
        Me.lblJobDept.Name = "lblJobDept"
        Me.lblJobDept.Size = New System.Drawing.Size(87, 15)
        Me.lblJobDept.TabIndex = 16
        Me.lblJobDept.Text = "Job Department"
        Me.lblJobDept.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(224, 35)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(80, 16)
        Me.lblUnits.TabIndex = 170
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStation
        '
        Me.cboStation.DropDownHeight = 200
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.IntegralHeight = False
        Me.cboStation.Location = New System.Drawing.Point(101, 33)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(118, 21)
        Me.cboStation.TabIndex = 0
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 90)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(87, 15)
        Me.lblSection.TabIndex = 169
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSections
        '
        Me.cboSections.DropDownHeight = 200
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.IntegralHeight = False
        Me.cboSections.Location = New System.Drawing.Point(101, 87)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(118, 21)
        Me.cboSections.TabIndex = 2
        '
        'cboJobDepartment
        '
        Me.cboJobDepartment.DropDownHeight = 200
        Me.cboJobDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobDepartment.FormattingEnabled = True
        Me.cboJobDepartment.IntegralHeight = False
        Me.cboJobDepartment.Location = New System.Drawing.Point(101, 60)
        Me.cboJobDepartment.Name = "cboJobDepartment"
        Me.cboJobDepartment.Size = New System.Drawing.Size(118, 21)
        Me.cboJobDepartment.TabIndex = 1
        '
        'cboEmployeementType
        '
        Me.cboEmployeementType.DropDownHeight = 200
        Me.cboEmployeementType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeementType.FormattingEnabled = True
        Me.cboEmployeementType.IntegralHeight = False
        Me.cboEmployeementType.Location = New System.Drawing.Point(523, 60)
        Me.cboEmployeementType.Name = "cboEmployeementType"
        Me.cboEmployeementType.Size = New System.Drawing.Size(118, 21)
        Me.cboEmployeementType.TabIndex = 7
        '
        'cboJob
        '
        Me.cboJob.DropDownHeight = 200
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.IntegralHeight = False
        Me.cboJob.Location = New System.Drawing.Point(310, 87)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(118, 21)
        Me.cboJob.TabIndex = 5
        '
        'lblEmployeementType
        '
        Me.lblEmployeementType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeementType.Location = New System.Drawing.Point(434, 63)
        Me.lblEmployeementType.Name = "lblEmployeementType"
        Me.lblEmployeementType.Size = New System.Drawing.Size(83, 15)
        Me.lblEmployeementType.TabIndex = 15
        Me.lblEmployeementType.Text = "Job Type"
        Me.lblEmployeementType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(224, 90)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(80, 15)
        Me.lblJob.TabIndex = 14
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(832, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(809, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'lvVacancyList
        '
        Me.lvVacancyList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhVacancy, Me.colhFormno, Me.colhDepartment, Me.colhJobTitle, Me.colhClass, Me.colhClassGroup, Me.colhEmployeementType, Me.colhPayType, Me.colhNoofPosition, Me.colhVacancyDateFrom, Me.colhVacancyEnd})
        Me.lvVacancyList.FullRowSelect = True
        Me.lvVacancyList.GridLines = True
        Me.lvVacancyList.Location = New System.Drawing.Point(12, 188)
        Me.lvVacancyList.Name = "lvVacancyList"
        Me.lvVacancyList.Size = New System.Drawing.Size(859, 233)
        Me.lvVacancyList.TabIndex = 1
        Me.lvVacancyList.UseCompatibleStateImageBehavior = False
        Me.lvVacancyList.View = System.Windows.Forms.View.Details
        '
        'colhVacancy
        '
        Me.colhVacancy.Text = "Vacancy"
        Me.colhVacancy.Width = 170
        '
        'colhDepartment
        '
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 150
        '
        'colhJobTitle
        '
        Me.colhJobTitle.Text = "Job Title"
        Me.colhJobTitle.Width = 150
        '
        'colhEmployeementType
        '
        Me.colhEmployeementType.Text = "Employeement Type"
        Me.colhEmployeementType.Width = 130
        '
        'colhPayType
        '
        Me.colhPayType.Text = "Pay Type"
        Me.colhPayType.Width = 110
        '
        'colhNoofPosition
        '
        Me.colhNoofPosition.Text = "No. of Position"
        Me.colhNoofPosition.Width = 86
        '
        'colhVacancyDateFrom
        '
        Me.colhVacancyDateFrom.Text = "Vacancy Start"
        Me.colhVacancyDateFrom.Width = 104
        '
        'colhVacancyEnd
        '
        Me.colhVacancyEnd.Text = "Vacancy End"
        Me.colhVacancyEnd.Width = 103
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblExternalVacancies)
        Me.objFooter.Controls.Add(Me.lnkPreviewInternal)
        Me.objFooter.Controls.Add(Me.lnkPreviewExternal)
        Me.objFooter.Controls.Add(Me.objpnlExVacancyColor)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 427)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(883, 55)
        Me.objFooter.TabIndex = 2
        '
        'lblExternalVacancies
        '
        Me.lblExternalVacancies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExternalVacancies.Location = New System.Drawing.Point(62, 18)
        Me.lblExternalVacancies.Name = "lblExternalVacancies"
        Me.lblExternalVacancies.Size = New System.Drawing.Size(112, 15)
        Me.lblExternalVacancies.TabIndex = 319
        Me.lblExternalVacancies.Text = "External Vacancies"
        Me.lblExternalVacancies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkPreviewInternal
        '
        Me.lnkPreviewInternal.BackColor = System.Drawing.Color.Transparent
        Me.lnkPreviewInternal.Location = New System.Drawing.Point(228, 30)
        Me.lnkPreviewInternal.Name = "lnkPreviewInternal"
        Me.lnkPreviewInternal.Size = New System.Drawing.Size(230, 13)
        Me.lnkPreviewInternal.TabIndex = 318
        Me.lnkPreviewInternal.TabStop = True
        Me.lnkPreviewInternal.Text = "Preview Internal Vacancies"
        '
        'lnkPreviewExternal
        '
        Me.lnkPreviewExternal.BackColor = System.Drawing.Color.Transparent
        Me.lnkPreviewExternal.Location = New System.Drawing.Point(228, 13)
        Me.lnkPreviewExternal.Name = "lnkPreviewExternal"
        Me.lnkPreviewExternal.Size = New System.Drawing.Size(230, 13)
        Me.lnkPreviewExternal.TabIndex = 317
        Me.lnkPreviewExternal.TabStop = True
        Me.lnkPreviewExternal.Text = "Preview External Vacancies"
        '
        'objpnlExVacancyColor
        '
        Me.objpnlExVacancyColor.BackColor = System.Drawing.Color.SteelBlue
        Me.objpnlExVacancyColor.Location = New System.Drawing.Point(12, 13)
        Me.objpnlExVacancyColor.Name = "objpnlExVacancyColor"
        Me.objpnlExVacancyColor.Size = New System.Drawing.Size(44, 21)
        Me.objpnlExVacancyColor.TabIndex = 177
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(671, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(568, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(465, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(774, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'mnuPrint
        '
        Me.mnuPrint.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreview, Me.mnuPrintCourse})
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(116, 48)
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(115, 22)
        Me.mnuPreview.Text = "P&review"
        '
        'mnuPrintCourse
        '
        Me.mnuPrintCourse.Name = "mnuPrintCourse"
        Me.mnuPrintCourse.Size = New System.Drawing.Size(115, 22)
        Me.mnuPrintCourse.Text = "&Print"
        '
        'colhFormno
        '
        Me.colhFormno.Text = "Form No."
        '
        'colhClass
        '
        Me.colhClass.Text = "Class"
        Me.colhClass.Width = 120
        '
        'colhClassGroup
        '
        Me.colhClassGroup.Text = "Class Group"
        Me.colhClassGroup.Width = 120
        '
        'frmVacancyList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(883, 482)
        Me.Controls.Add(Me.pnlCourseScheduling)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVacancyList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Vacancy List"
        Me.pnlCourseScheduling.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuPrint.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCourseScheduling As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lvVacancyList As System.Windows.Forms.ListView
    Friend WithEvents colhVacancy As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeementType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPayType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNoofPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVacancyDateFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVacancyEnd As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents mnuPrint As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintCourse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboJobDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployeementType As System.Windows.Forms.ComboBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobDept As System.Windows.Forms.Label
    Friend WithEvents lblEmployeementType As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblVacancyEndDate As System.Windows.Forms.Label
    Friend WithEvents lblVacancyStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents txtNoofPosition As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNoofPosition As System.Windows.Forms.Label
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objpnlExVacancyColor As System.Windows.Forms.Panel
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents lnkPreviewInternal As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPreviewExternal As System.Windows.Forms.LinkLabel
    Friend WithEvents lblExternalVacancies As System.Windows.Forms.Label
    Friend WithEvents colhJobTitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFormno As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClassGroup As System.Windows.Forms.ColumnHeader
End Class
