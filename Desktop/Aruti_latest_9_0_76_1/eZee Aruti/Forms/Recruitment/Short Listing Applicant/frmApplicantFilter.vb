﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.Text
Imports System.IO


Public Class frmApplicantFilter
    Private ReadOnly mstrModuleName As String = "frmApplicantFilter"

#Region " Private Variable(s) "

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintJobunkid As Integer
    Private mdtFilter As DataTable
    Private mdtApplicant As DataTable
    Private objShortListMst As clsshortlist_master
    Private objShortFilter As clsshortlist_filter
    Private arstrAppliFilter As String()
    Private dsFilterApplicant As DataSet = Nothing
    'S.SANDEEP [27-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 17
    Private mdtQualification As DataTable = Nothing
    Private mdtSkillCategory As DataTable = Nothing
    Private mdtSkillSet As DataTable = Nothing

    Private dvQualification As DataView = Nothing
    Private dvSkillCategory As DataView = Nothing
    Private dvSkillSet As DataView = Nothing
    'S.SANDEEP [27-SEP-2017] -- END
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Private mdtQualificationGroup As DataTable = Nothing
    Private dvQualificationGroup As DataView = Nothing
    Private mstrQLevel As String = ""
    'Hemant (07 Oct 2019) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "


    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(True)
            dsList = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End

            ' === START RESULT LEVEL CONDITION====

            cboResultLvlCondition.DisplayMember = "Name"
            cboResultLvlCondition.ValueMember = "id"
            cboResultLvlCondition.DataSource = dsList.Tables(0).Copy

            ' === END RESULT LEVEL CONDITION====

            ' === START GPA CONDITION====

            cboGPACondition.DisplayMember = "Name"
            cboGPACondition.ValueMember = "id"
            cboGPACondition.DataSource = dsList.Tables(0).Copy

            ' === END GPA CONDITION====

            ' === START AGE CONDITION====

            cboAgeCondition.DisplayMember = "Name"
            cboAgeCondition.ValueMember = "id"
            cboAgeCondition.DataSource = dsList.Tables(0).Copy

            ' === END AGE CONDITION====



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            ' ===========START  FOR AWARD YEAR CONDITION=========

            cboYearCondition.DisplayMember = "Name"
            cboYearCondition.ValueMember = "id"
            cboYearCondition.DataSource = dsList.Tables(0).Copy

            ' ===========END FOR AWARD YEAR CONDITION=========

            'Pinkal (06-Feb-2012) -- End

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            ' ===========START  FOR QUALIFICATON LEVEL CONDITION=========

            cboQualificationGrpCondition.DisplayMember = "Name"
            cboQualificationGrpCondition.ValueMember = "id"
            cboQualificationGrpCondition.DataSource = dsList.Tables(0).Copy

            ' ===========END FOR QUALIFICATON LEVEL CONDITION=========
            'Hemant (07 Oct 2019) -- End

            'Hemant (30 Oct 2019) -- Start
            ' ===========START  FOR QUALIFICATON GROUP CONDITION=========

            cboQualificationGroupCondition.DisplayMember = "Name"
            cboQualificationGroupCondition.ValueMember = "id"
            cboQualificationGroupCondition.DataSource = dsList.Tables(0).Copy

            ' ===========END FOR QUALIFICATON GROUP CONDITION=========
            'Hemant (30 Oct 2019) -- End

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
            ' ===========START  FOR EXPERIENCE CONDITION=========

            cboExperienceCondition.DisplayMember = "Name"
            cboExperienceCondition.ValueMember = "id"
            cboExperienceCondition.DataSource = dsList.Tables(0).Copy

            ' ===========END FOR EXPERIENCE CONDITION=========
            'Hemant (07 Oct 2019) -- End


            ' === START FOR GENDER ====

            dsList = Nothing
            dsList = objMaster.getGenderList("Gender", True)
            cboGender.DisplayMember = "Name"
            cboGender.ValueMember = "id"
            cboGender.DataSource = dsList.Tables(0)

            ' === END FOR GENDER ====

            ' === START FOR VACANCY ====

            dsList = Nothing
            dsList = objMaster.getGenderList("Gender", True)
            cboGender.DisplayMember = "Name"
            cboGender.ValueMember = "id"
            cboGender.DataSource = dsList.Tables(0)

            ' === END FOR VACANCY ====

            ' === START FOR VACANCY TYPE ====

            dsList = Nothing
            Dim objVacancy As New clsVacancy
            dsList = objVacancy.getVacancyType()
            cboVacancyType.DisplayMember = "Name"
            cboVacancyType.ValueMember = "Id"
            cboVacancyType.DataSource = dsList.Tables(0)

            ' === END FOR VACANCY ====

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dsList = objMaster.getCountryList("Nationality", True)
            With cboNationality
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsList.Tables("Nationality")
            End With
            'Hemant (02 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub EnableControl()
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvQualification.Items.Count = 0 Then
            '    chkQualification.Checked = False
            '    chkQualification.Enabled = False
            'Else
            '    chkQualification.Enabled = True
            'End If

            'If lvSkillCategory.Items.Count = 0 Then
            '    chkSkillCategory.Checked = False
            '    chkSkillCategory.Enabled = False
            'Else
            '    If lvSkillCategory.Enabled = False Then
            '        chkSkillCategory.Enabled = False
            '    Else
            '        chkSkillCategory.Enabled = True
            '    End If
            'End If

            'If lvSkillSet.Items.Count = 0 Then
            '    chkSkill.Checked = False
            '    chkSkill.Enabled = False
            'Else
            '    chkSkill.Enabled = True
            'End If
            If dgvQualification.RowCount <= 0 Then
                chkQualification.Checked = False
                chkQualification.Enabled = False
            Else
                chkQualification.Enabled = True
            End If

            If dgvSkillCategory.RowCount <= 0 Then
                chkSkillCategory.Checked = False
                chkSkillCategory.Enabled = False
            Else
                If dgvSkillCategory.Enabled = False Then
                    chkSkillCategory.Enabled = False
                Else
                chkSkillCategory.Enabled = True
            End If
            End If

            If dgvSkillName.RowCount <= 0 Then
                chkSkill.Checked = False
                chkSkill.Enabled = False
            Else
                chkSkill.Enabled = True
            End If
            'S.SANDEEP [27-SEP-2017] -- END
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            If dgvQualificationGroup.RowCount <= 0 Then
                chkQualificationGroup.Checked = False
                chkQualificationGroup.Enabled = False
            Else
                chkQualificationGroup.Enabled = True
            End If
            'Hemant (07 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControl", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Private Sub FillQualification()
        Try
            Dim objQualification As New clsqualification_master
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            ' Dim dsList As DataSet = objQualification.GetComboList("List", False, CInt(cboQualificationGrp.SelectedValue))
            Dim strQualificationGroupIds As String = ""
            Dim dsList As DataSet
            If dgvQualificationGroup.RowCount > 0 Then
                strQualificationGroupIds = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())

                'Hemant (30 Oct 2019) -- Start

                If strQualificationGroupIds.Trim.Length > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                    Dim dsQualificationGroupList As DataSet
                    Dim strSearch As String = String.Empty
                    Dim strTmpSearch As String = String.Empty
                    Dim objMaster As New clsCommon_Master
                    Dim strQuaLevels As String = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("qlevel").ToString()).ToArray())

                    Dim arrQuaLevels() As String = Split(strQuaLevels, ",")
                    strSearch = " AND ( #CONDITION# ) "
                    For Each str As String In arrQuaLevels
                        If strTmpSearch.Trim.Length > 0 Then
                            strTmpSearch &= " OR "
                        End If
                        strTmpSearch &= "( qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                    Next
                    strSearch = strSearch.Replace("#CONDITION#", strTmpSearch)
                    dsQualificationGroupList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
                    strQualificationGroupIds = String.Join(",", dsQualificationGroupList.Tables(0).AsEnumerable().Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())

                End If

                'Hemant (30 Oct 2019) -- End

                If strQualificationGroupIds.Trim <> "" Then
                    dsList = objQualification.GetComboList("List", False, -1, strQualificationGroupIds)
                    'Hemant (30 Oct 2019) -- [strSearch]
                Else
                    dsList = objQualification.GetComboList("List", False, 0)
                End If
            Else
                dsList = objQualification.GetComboList("List", False, CInt(cboQualificationGrp.SelectedValue))
            End If
            'Hemant (07 Oct 2019) -- End            

            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvQualification.Items.Clear()
            'chkQualification.Checked = False
            'If dsList.Tables(0).Rows.Count > 0 Then

            '    Dim LvItem As ListViewItem

            '    For Each dr As DataRow In dsList.Tables(0).Rows
            '        LvItem = New ListViewItem
            '        LvItem.Text = dr("Name").ToString()
            '        LvItem.Tag = CInt(dr("Id"))
            '        LvItem.ToolTipText = LvItem.Text
            '        lvQualification.Items.Add(LvItem)
            '    Next

            '    If lvQualification.Items.Count > 5 Then
            '        colhlstQualification.Width = 245 - 18
            '    Else
            '        colhlstQualification.Width = 245
            '    End If

            'End If
            chkQualification.Checked = False
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            mdtQualification = dsList.Tables(0).Copy()
            dvQualification = mdtQualification.DefaultView

            dgvQualification.AutoGenerateColumns = False
            objdgcolhQlCheck.DataPropertyName = "ischeck"
            dgcolhQlName.DataPropertyName = "Name"
            objdgcolhQlId.DataPropertyName = "Id"
            dgvQualification.DataSource = dvQualification
            'S.SANDEEP [27-SEP-2017] -- END

            EnableControl()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSkillCateGory", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End

    Private Sub FillSkillCateGory(ByVal mintJobunkid As Integer)
        Try
            Dim objApplicantBatchInteview As New clsApplicant_Batchschedule_Tran
            Dim dsList As DataSet = objApplicantBatchInteview.GetVacancySkillCategory(mintJobunkid)
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvSkillCategory.Items.Clear()
            'lvSkillSet.Items.Clear()
            'If dsList.Tables(0).Rows.Count > 0 Then

            '    Dim LvItem As ListViewItem

            '    For Each dr As DataRow In dsList.Tables(0).Rows
            '        LvItem = New ListViewItem
            '        LvItem.Text = dr("skillcategory").ToString()
            '        LvItem.Tag = CInt(dr("skillcategoryunkid"))
            '        LvItem.ToolTipText = LvItem.Text
            '        lvSkillCategory.Items.Add(LvItem)
            '    Next

            '    If lvSkillCategory.Items.Count > 5 Then
            '        colhSkillCategory.Width = 255 - 18
            '    Else
            '        colhSkillCategory.Width = 255
            '    End If
            'End If
            chkSkillCategory.Checked = False
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            mdtSkillCategory = dsList.Tables(0).Copy()
            dvSkillCategory = mdtSkillCategory.DefaultView

            dgvSkillCategory.AutoGenerateColumns = False
            objdgcolhSCheck.DataPropertyName = "ischeck"
            dgcolhSkillCategory.DataPropertyName = "skillcategory"
            objdgcolhSkillCategoryId.DataPropertyName = "skillcategoryunkid"
            dgvSkillCategory.DataSource = dvSkillCategory
            'S.SANDEEP [27-SEP-2017] -- END

            EnableControl()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSkillCateGory", mstrModuleName)
        End Try
    End Sub

    Private Sub FillSkill(ByVal mstrSkillCategory As String, ByVal mintJobunkid As Integer)
        Try
            Dim objApplicantBatchInteview As New clsApplicant_Batchschedule_Tran
            Dim dsList As DataSet = objApplicantBatchInteview.GetVacancySkills(mstrSkillCategory, mintJobunkid)

            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    Dim LvItem As ListViewItem
            '    lvSkillSet.Items.Clear()

            '    RemoveHandler lvSkillSet.ItemChecked, AddressOf lvSkillSet_ItemChecked

            '    For Each dr As DataRow In dsList.Tables(0).Rows
            '        LvItem = New ListViewItem
            '        LvItem.Tag = CInt(dr("skillunkid"))
            '        LvItem.Text = dr("skillname").ToString()
            '        LvItem.SubItems.Add(dr("skillcategory").ToString())
            '        LvItem.SubItems.Add(dr("skillcategoryunkid").ToString())
            '        LvItem.ToolTipText = LvItem.Text
            '        lvSkillSet.Items.Add(LvItem)
            '    Next

            '    AddHandler lvSkillSet.ItemChecked, AddressOf lvSkillSet_ItemChecked

            '    If lvSkillSet.Items.Count > 5 Then
            '        colhSkills.Width = 245 - 18
            '    Else
            '        colhSkills.Width = 245
            '    End If

            '    lvSkillSet.GroupingColumn = colhSkillCategorySkills
            '    lvSkillSet.DisplayGroups(True)

            'End If

            chkSkill.Checked = False
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            mdtSkillSet = dsList.Tables(0).Copy()
            dvSkillSet = mdtSkillSet.DefaultView

            dgvSkillName.AutoGenerateColumns = False
            objdgcolhSkillCheck.DataPropertyName = "ischeck"
            dgcolhSkillName.DataPropertyName = "skillname"
            objdgcolhSkillId.DataPropertyName = "skillunkid"
            objdgcolhCategoryName.DataPropertyName = "skillcategory"
            objdgcolhCategoryId.DataPropertyName = "skillcategoryunkid"
            dgvSkillName.DataSource = dvSkillSet
            'S.SANDEEP [27-SEP-2017] -- END


            EnableControl()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSkillCateGory", mstrModuleName)
        End Try
    End Sub

    Private Sub FillApplicant(ByVal dsList As DataSet)
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            RemoveHandler lvApplicant.ItemChecked, AddressOf lvApplicant_ItemChecked
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            'Anjan (25 Oct 2012)-End


            'S.SANDEEP [ 05 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim iTotalCnt As Integer = 0
            'S.SANDEEP [ 05 NOV 2012 ] -- END

            Dim dtView As DataView = dsList.Tables(0).DefaultView
            dtView.Sort = "applicant_code asc"
            mdtApplicant = dtView.ToTable()
            Dim mDicAdded As New Dictionary(Of String, String)

            lvApplicant.Items.Clear()
            Dim lvItem As ListViewItem = Nothing

            Dim objApplicantQulaification As New clsApplicantQualification_tran
            Dim objApplicantskill As New clsApplicantSkill_tran

            For Each dr As DataRow In mdtApplicant.Rows

                If mDicAdded.ContainsKey(CStr(dr("applicant_code"))) = True Then Continue For

                mDicAdded.Add(CStr(dr("applicant_code")), CStr(dr("applicant_code")))

                'If CType(lvApplicant.FindItemWithText(dr("applicant_code").ToString(), ), ListViewItem) IsNot Nothing Then Continue For

                If dr("employeecode").ToString().Length > 0 Then
                    If chkInvalidCode.Checked Then
                        Dim objEmployee As New clsEmployee_Master
                        If objEmployee.GetEmployeeUnkid("", dr("employeecode").ToString()) <= 0 Then
                            Continue For
                        End If
                    End If
                End If



                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.

                If CInt(dr("employeeunkid")) > 0 Then
                    Dim objEmpQualification As New clsEmp_Qualification_Tran
                    Dim dsQulification As DataSet = objEmpQualification.GetRecruitmentEmpQualification(False, CInt(dr("employeeunkid")))

                If dsQulification.Tables(0).Rows.Count > 0 Then
                    dr("qualificationgroup") = dsQulification.Tables(0).Rows(0)("qualificationgrp").ToString()
                    dr("qualificationname") = dsQulification.Tables(0).Rows(0)("qualification").ToString()
                        dr("gpacode_string") = dsQulification.Tables(0).Rows(0)("gpacode").ToString()
                    End If

                    dsQulification = objEmpQualification.GetRecruitmentEmpQualification(True, CInt(dr("employeeunkid")))

                    If dsQulification.Tables(0).Rows.Count > 0 Then
                        dr("other_qualificationgrp") = dsQulification.Tables(0).Rows(0)("qualificationgrp").ToString()
                        dr("other_qualification") = dsQulification.Tables(0).Rows(0)("qualification").ToString()
                    dr("gpacode_string") = dsQulification.Tables(0).Rows(0)("gpacode").ToString()
                    End If

                    If dsQulification IsNot Nothing Then dsQulification.Clear()
                    dsQulification = Nothing
                    objEmpQualification = Nothing

                    Dim objEmpSkill As New clsEmployee_Skill_Tran

                    Dim dsSkill As DataSet = objEmpSkill.GetRecruitmentEmpSkill(False, CInt(dr("employeeunkid")))
                    If dsSkill.Tables(0).Rows.Count > 0 Then
                        dr("skillcategory") = dsSkill.Tables(0).Rows(0)("SkillCategory").ToString()
                        dr("skill") = dsSkill.Tables(0).Rows(0)("Skill").ToString()
                    End If


                    dsSkill = objEmpSkill.GetRecruitmentEmpSkill(True, CInt(dr("employeeunkid")))

                    If dsSkill.Tables(0).Rows.Count > 0 Then
                        dr("other_skillcategory") = dsSkill.Tables(0).Rows(0)("SkillCategory").ToString()
                        dr("other_skill") = dsSkill.Tables(0).Rows(0)("Skill").ToString()
                    End If

                    If dsSkill IsNot Nothing Then dsSkill.Clear()
                    dsSkill = Nothing
                    objEmpSkill = Nothing

                ElseIf CInt(dr("employeeunkid")) <= 0 Then

                    Dim dsQulification As DataSet = objApplicantQulaification.GetApplicantQualification(False, CInt(dr("ApplicantUnkid")))

                    If dsQulification.Tables(0).Rows.Count > 0 Then
                        dr("qualificationgroup") = dsQulification.Tables(0).Rows(0)("qualificationgrp").ToString()
                        dr("qualificationname") = dsQulification.Tables(0).Rows(0)("qualification").ToString()
                        dr("gpacode_string") = dsQulification.Tables(0).Rows(0)("gpacode").ToString()
                End If

                dsQulification = objApplicantQulaification.GetApplicantQualification(True, CInt(dr("ApplicantUnkid")))

                If dsQulification.Tables(0).Rows.Count > 0 Then
                    dr("other_qualificationgrp") = dsQulification.Tables(0).Rows(0)("qualificationgrp").ToString()
                    dr("other_qualification") = dsQulification.Tables(0).Rows(0)("qualification").ToString()
                    dr("gpacode_string") = dsQulification.Tables(0).Rows(0)("gpacode").ToString()
                End If


                    If dsQulification IsNot Nothing Then dsQulification.Clear()
                    dsQulification = Nothing

                Dim dsSkill As DataSet = objApplicantskill.GetApplicantSkillName(False, CInt(dr("ApplicantUnkid")))
                If dsSkill.Tables(0).Rows.Count > 0 Then
                    dr("skillcategory") = dsSkill.Tables(0).Rows(0)("SkillCategory").ToString()
                    dr("skill") = dsSkill.Tables(0).Rows(0)("Skill").ToString()
                End If


                dsSkill = objApplicantskill.GetApplicantSkillName(True, CInt(dr("ApplicantUnkid")))

                If dsSkill.Tables(0).Rows.Count > 0 Then
                    dr("other_skillcategory") = dsSkill.Tables(0).Rows(0)("SkillCategory").ToString()
                    dr("other_skill") = dsSkill.Tables(0).Rows(0)("Skill").ToString()
                End If

                    If dsSkill IsNot Nothing Then dsSkill.Clear()
                    dsList = Nothing

                End If

                'Pinkal (24-Sep-2020) -- End





                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                If dr("experience_days").ToString().Length > 0 AndAlso CInt(dr("experience_days")) > 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'Dim intYears As Integer = CInt((CInt(dr("experience_days")) / 365))
                    'Dim intMonths As Integer = CInt((CInt(dr("experience_days")) - (intYears * 365)) / 30)
                    Dim intYears As Integer = CInt(Math.Floor(CDec(dr("experience_days")) / 365))
                    Dim intMonths As Integer = CInt(Math.Floor(CDec(dr("experience_days")) - (intYears * 365)) / 30)
                    'Pinkal (25-Jan-2022) -- End
                    dr("experience") = IIf(intYears > 0, intYears.ToString() & " Years ", "").ToString & IIf(intMonths > 0, intMonths.ToString & " Months", "").ToString
                End If
                'Hemant (07 Oct 2019) -- End


                lvItem = New ListViewItem
                lvItem.Tag = dr("applicantunkid").ToString()

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                'lvItem.Text = dr("qualificationname").ToString()
                'lvItem.SubItems.Add(dr("applicant_code").ToString())
                'lvItem.SubItems.Add(dr("applicantname").ToString())
                'If dr("referenceno").ToString.Trim.Length > 0 Then
                '    lvItem.SubItems.Add(clsCrypto.Dicrypt(dr("referenceno").ToString()))
                'Else

                '    lvItem.SubItems.Add("")
                'End If

                lvItem.Text = dr("applicant_code").ToString()
                lvItem.SubItems.Add(dr("applicantname").ToString())
                'Sohail (13 Mar 2020) -- Start
                'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously. (changes in desktop)
                'If dr("referenceno").ToString.Trim.Length > 0 Then
                '    lvItem.SubItems.Add(clsCrypto.Dicrypt(dr("referenceno").ToString()))
                'Else

                '    lvItem.SubItems.Add("")
                'End If
                lvItem.SubItems.Add(dr("email").ToString)
                'Sohail (13 Mar 2020) -- End
                lvItem.SubItems.Add(dr("qualificationname").ToString())

                'Anjan (25 Oct 2012)-End 



                lvApplicant.Items.Add(lvItem)
            Next

            ' lvApplicant.GroupingColumn = colhAppliQlfication
            ' lvApplicant.DisplayGroups(True)

            If lvApplicant.Items.Count > 18 Then
                colhApplicant.Width = 200 - 18
            Else
                colhApplicant.Width = 200
            End If

            'S.SANDEEP [ 05 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            iTotalCnt = objShortFilter.Get_Tot_Applicant_Vac(CInt(cboVacancy.SelectedValue))
            If iTotalCnt > 0 Then
                Dim StrText As String = CStr(gbShortListedApplicant.Tag)
                StrText &= " [ " & lvApplicant.Items.Count & Language.getMessage(mstrModuleName, 13, " Out of ") & iTotalCnt.ToString & " ] "
                gbShortListedApplicant.Text = StrText
            End If
            'S.SANDEEP [ 05 NOV 2012 ] -- END

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            AddHandler lvApplicant.ItemChecked, AddressOf lvApplicant_ItemChecked
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            'Anjan (25 Oct 2012)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicant", mstrModuleName)
        End Try
    End Sub

    Private Sub FillShortListFilter()
        Try
            dgFilter.AutoGenerateColumns = False

            Dim dtFilter As DataTable = New DataView(mdtFilter, "AUD <> 'D' AND IsApply = false", "", DataViewRowState.CurrentRows).ToTable()

            dgFilter.DataSource = dtFilter
            colhVacancy.DataPropertyName = "vacancy"
            colhQlGrpLevel.DataPropertyName = "qlevel"
            colhQlGrp.DataPropertyName = "qualificationgrp"
            colhdgQualification.DataPropertyName = "qualification"
            colhResultCode.DataPropertyName = "resultcode"
            colhRLevel.DataPropertyName = "result_level"
            colhRCondition.DataPropertyName = "resultcondition"
            colhGPA.DataPropertyName = "gpacode"
            colhGPACondition.DataPropertyName = "gpacodecondition"
            colhAge.DataPropertyName = "age"
            colhAgeCondition.DataPropertyName = "agecondition"


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            colhAwardYear.DataPropertyName = "award_year"
            colhYearCondition.DataPropertyName = "yearcondition"
            'Pinkal (06-Feb-2012) -- End

            colhGender.DataPropertyName = "gender_name"
            colhFilterSkillCategory.DataPropertyName = "skillcategory"
            colhFilterSkill.DataPropertyName = "skill"
            objcolhFilterunkid.DataPropertyName = "filterunkid"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhFilter.DataPropertyName = "filter"



            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            colhFilterOtherQuliGrp.DataPropertyName = "other_qualificationgrp"
            colhFilterOtherQualification.DataPropertyName = "other_qualification"
            colhFilterOtherResultCode.DataPropertyName = "other_resultcode"
            colhFilterOtherSkillCategory.DataPropertyName = "other_skillcategory"
            colhFilterOtherSkill.DataPropertyName = "other_skill"
            'Pinkal (5-MAY-2012) -- End

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            colhNationality.DataPropertyName = "nationality"
            'Hemant (02 Jul 2019) -- End

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
            colhExperience.DataPropertyName = "experience"
            colhExperienceCondition.DataPropertyName = "experiencecondition"
            'Hemant (07 Oct 2019) -- End


            Dim dRRow As DataRow() = mdtFilter.Select("AUD = 'A'")
            If dRRow.Length > 0 Then
                cboVacancyType.Enabled = False
                cboVacancy.Enabled = False
            Else
                cboVacancyType.Enabled = True
                cboVacancy.Enabled = True
            End If





            If dgFilter.Rows.Count > 0 Then
                'nudAge.Enabled = False
                'cboAgeCondition.Enabled = False
                'cboGender.Enabled = False
                chkSkillCategory.Enabled = False
                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 17
                'lvSkillCategory.Enabled = False
                'chkSkill.Enabled = False
                'lvSkillSet.Enabled = False

                dgvSkillCategory.Enabled = False
                chkSkill.Enabled = False
                dgvSkillName.Enabled = False
                'S.SANDEEP [27-SEP-2017] -- END
            Else
                nudAge.Enabled = True
                cboAgeCondition.Enabled = True
                cboGender.Enabled = True
                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 17
                'If lvSkillCategory.Items.Count > 0 Then chkSkillCategory.Enabled = True
                'lvSkillCategory.Enabled = True
                'If lvSkillSet.Items.Count > 0 Then chkSkill.Enabled = True
                'lvSkillSet.Enabled = True
                chkSkillCategory.Enabled = True
                chkSkillCategory.Enabled = True
                dgvSkillCategory.Enabled = True
                dgvSkillName.Enabled = True
                'S.SANDEEP [27-SEP-2017] -- END
                rdANDCondition.Enabled = True
                rdORCondition.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillShortListFilter", mstrModuleName)
        End Try
    End Sub

    Private Function GetSkillCategory(ByVal IsName As Boolean) As String
        Dim strFilter As String = ""
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvSkillCategory.CheckedItems.Count > 0 Then

            '    For i As Integer = 0 To lvSkillCategory.CheckedItems.Count - 1
            '        If IsName Then
            '            strFilter &= "," & lvSkillCategory.CheckedItems(i).Text
            '        Else
            '            strFilter &= "," & lvSkillCategory.CheckedItems(i).Tag.ToString()
            '        End If
            '    Next
            'End If
            'If strFilter.Trim.Length > 0 Then
            '    strFilter = strFilter.Substring(1, strFilter.Length - 1)
            'End If
            If dgvSkillCategory.RowCount > 0 Then
                mdtSkillCategory.AcceptChanges()
                If mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    If IsName Then
                        strFilter = String.Join(",", mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("skillcategory")).ToArray())
                    Else
                        strFilter = String.Join(",", mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("skillcategoryunkid").ToString()).ToArray())
                    End If
            End If
            End If
            'S.SANDEEP [27-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSkillCategory", mstrModuleName)
        End Try
        Return strFilter
    End Function

    Private Function GetSkill(ByVal IsName As Boolean) As String
        Dim strFilter As String = ""
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvSkillSet.CheckedItems.Count > 0 Then

            '    For i As Integer = 0 To lvSkillSet.CheckedItems.Count - 1
            '        If IsName Then
            '            strFilter &= "," & lvSkillSet.CheckedItems(i).Text
            '        Else
            '            strFilter &= "," & lvSkillSet.CheckedItems(i).Tag.ToString()
            '        End If
            '    Next
            'End If
            'If strFilter.Trim.Length > 0 Then
            '    strFilter = strFilter.Substring(1, strFilter.Length - 1)
            'End If
            If dgvSkillName.RowCount > 0 Then
                mdtSkillSet.AcceptChanges()
                If mdtSkillSet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    If IsName Then
                        strFilter = String.Join(",", mdtSkillSet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("skillname")).ToArray())
                    Else
                        strFilter = String.Join(",", mdtSkillSet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("skillunkid").ToString()).ToArray())
                    End If
            End If
            End If
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSkill", mstrModuleName)
        End Try
        Return strFilter
    End Function

    Private Sub ClearFilterControls()
        Try

            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
            'nudLevel.Value = 0
            nudLevel.Value = -1
            'Hemant (27 Sep 2019) -- End
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            nudLevel_ValueChanged(Nothing, Nothing)
            'Hemant (07 Oct 2019) -- End
            If cboQualificationGrp.Items.Count > 0 Then cboQualificationGrp.SelectedIndex = 0
            ' If cboQualification.Items.Count > 0 Then cboQualification.SelectedIndex = 0
            If cboResultCode.Items.Count > 0 Then cboResultCode.SelectedIndex = 0
            If cboResultLvlCondition.Items.Count > 0 Then cboResultLvlCondition.SelectedIndex = 0
            nudGPA.Value = 0
            If cboGPACondition.Items.Count > 0 Then cboGPACondition.SelectedIndex = 0
            nudAge.Value = 0
            If cboAgeCondition.Items.Count > 0 Then cboAgeCondition.SelectedIndex = 0
            If cboGender.Items.Count > 0 Then cboGender.SelectedIndex = 0

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            If cboAwardedYear.Items.Count > 0 Then cboAwardedYear.SelectedIndex = 0
            If cboYearCondition.Items.Count > 0 Then cboYearCondition.SelectedIndex = 0
            'Pinkal (06-Feb-2012) -- End

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
            nudExperienceYears.Value = 0
            nudExperienceMonths.Value = 0
            If cboExperienceCondition.Items.Count > 0 Then cboExperienceCondition.SelectedIndex = 0
            'Hemant (07 Oct 2019) -- End

            'Hemant (30 Oct 2019) -- Start
            If cboQualificationGroupCondition.Items.Count > 0 Then cboQualificationGroupCondition.SelectedIndex = 0
            cboQualificationGroupCondition.Enabled = False
            'Hemant (30 Oct 2019) -- End

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            If cboNationality.Items.Count > 0 Then cboNationality.SelectedIndex = 0
            'Hemant (02 Jul 2019) -- End

            FillSkillCateGory(mintJobunkid)


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherQualificationGrp.Text = ""
            txtOtherQualification.Text = ""
            txtOtherResultCode.Text = ""
            'Pinkal (5-MAY-2012) -- End

            'S.SANDEEP [ 07 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtOtherSkillCategory.Text = ""
            txtOtherSkill.Text = ""
            'S.SANDEEP [ 07 NOV 2012 ] -- END

            mstrQLevel = "" 'Hemant (07 Oct 2019)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearFilterControls", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objShortListMst._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            objShortListMst._Refno = ConfigParameter._Object._CurrentDateAndTime.Year & ConfigParameter._Object._CurrentDateAndTime.ToString("hhmmss") & ConfigParameter._Object._CurrentDateAndTime.Date.Day.ToString("0#") & ConfigParameter._Object._CurrentDateAndTime.Date.Month.ToString("0#")
            objShortListMst._IsInvalidEmpcode = chkInvalidCode.Checked
            objShortListMst._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            objShortListMst._Statustypid = enShortListing_Status.SC_PENDING
            objShortListMst._Approveuserunkid = 0
            objShortListMst._Remark = ""
            objShortListMst._Issent = False
            'S.SANDEEP [ 14 May 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Jun-2013) -- Start
    'Enhancement : TRA Changes
    'Private Function Export_to_Excel(ByVal BlnIsSave As Boolean, ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
    'Pinkal (18-Jun-2013) -- End
    Private Function Export_to_Excel(ByVal BlnIsSave As Boolean, ByVal flFileName As String, ByVal SavePath As String, ByVal IsOpenAfterExport As Boolean, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try
            'HEADER PART

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)

            strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & objDataReader.Columns.Count - 4 & " ALIGN = 'MIDDLE'> " & vbCrLf)
            strBuilder.Append("<FONT SIZE=3><B>")
            strBuilder.Append(Language.getMessage(mstrModuleName, 9, "Applicant Short Listing") & vbCrLf)
            strBuilder.Append(" </FONT></B></CENTER>")
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            If BlnIsSave Then

                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                strBuilder.Append(" <FONT SIZE=3><B>")
                strBuilder.Append(" Reference No : " & objShortListMst._Refno & vbCrLf)
                strBuilder.Append(" </FONT></B>")
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

            End If

                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            ' Column Caption

            'objDataReader.Columns(j).ColumnName.ToString() = "gpacode" Or 

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).ColumnName.ToString() = "applicantunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "GUID" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "qualificationunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "gender" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "vacancyunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "qlevel" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "qualificationgroupunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "resultunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "result_level" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "age" Or objDataReader.Columns(j).ColumnName.ToString() = "skillcategoryunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "skillunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "award_start_date" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "app_statusid" Or objDataReader.Columns(j).ColumnName.ToString() = "app_issent" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "award_end_date" Or objDataReader.Columns(j).ColumnName.ToString() = "gpacode" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "nationalityunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "experience_days" _
                Then Continue For
                'Hemant (07 Oct 2019) -- [objDataReader.Columns(j).ColumnName.ToString() = "nationalityunkid", objDataReader.Columns(j).ColumnName.ToString() = "experience_days"]

                If objDataReader.Columns(j).ColumnName = "gpacode_string" Then
                    objDataReader.Columns(j).Caption = "gpacode"
                End If

                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            Dim mintApplicantunkid As Integer = 0
            For i As Integer = 0 To objDataReader.Rows.Count - 1

                If mintApplicantunkid <> CInt(objDataReader.Rows(i)("applicantunkid")) Then
                    mintApplicantunkid = CInt(objDataReader.Rows(i)("applicantunkid"))
                Else
                    Continue For
                End If

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1

                    ' objDataReader.Columns(k).ColumnName.ToString() = "gpacode" Or _

                    If objDataReader.Columns(k).ColumnName.ToString() = "applicantunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "GUID" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "qualificationunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "gender" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "vacancyunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "qlevel" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "qualificationgroupunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "resultunkid" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "result_level" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "age" Or objDataReader.Columns(k).ColumnName.ToString() = "skillcategoryunkid" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "skillunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "award_start_date" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "app_statusid" Or objDataReader.Columns(k).ColumnName.ToString() = "app_issent" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "award_end_date" Or objDataReader.Columns(k).ColumnName.ToString() = "gpacode" Or _
                       objDataReader.Columns(k).ColumnName.ToString() = "nationalityunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "experience_days" _
                       Then Continue For
                    'Hemant (07 Oct 2019) -- [objDataReader.Columns(k).ColumnName.ToString() = "nationalityunkid", objDataReader.Columns(k).ColumnName.ToString() = "experience_days"]

                    If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                        If objDataReader.Rows(i)(k).ToString() <> "" Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                        End If
                    Else
                        If objDataReader.Columns(k).ColumnName = "referenceno" Then
                            If objDataReader.Rows(i)(k).ToString().Length > 0 Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2> &nbsp; " & clsCrypto.Dicrypt(objDataReader.Rows(i)(k).ToString()) & "</FONT></TD>" & vbCrLf)
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2></FONT></TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                        End If



                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'START FOR SHOWING FILTER 

            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)

            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & mdtFilter.Columns.Count - 16 & " ALIGN = 'LEFT'> " & vbCrLf)
            strBuilder.Append("<TD WIDTH='10%' COLSPAN = " & mdtFilter.Columns.Count - 11 & " ALIGN = 'LEFT'> " & vbCrLf)
            'Pinkal (5-MAY-2012) -- End


            strBuilder.Append("<FONT SIZE=3><B>")
            strBuilder.Append(Language.getMessage(mstrModuleName, 10, "Filter(s):-") & vbCrLf)
            strBuilder.Append("</FONT></B>")
            strBuilder.Append("</TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            ' Column Caption

            For j As Integer = 0 To mdtFilter.Columns.Count - 1

                If mdtFilter.Columns(j).ColumnName.ToString() = "filterunkid" Or mdtFilter.Columns(j).ColumnName.ToString() = "shortlistunkid" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "vacancyunkid" Or mdtFilter.Columns(j).ColumnName.ToString() = "qualificationgroupunkid" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "qualificationunkid" Or mdtFilter.Columns(j).ColumnName.ToString() = "resultunkid" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "gender" Or mdtFilter.Columns(j).ColumnName.ToString() = "skillcategoryunkid" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "skillunkid" Or mdtFilter.Columns(j).ColumnName.ToString() = "filter" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "IsApply" Or mdtFilter.Columns(j).ColumnName.ToString() = "AUD" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "GUID" Or mdtFilter.Columns(j).ColumnName.ToString() = "result_lvl_condition" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "gpacode_condition" Or mdtFilter.Columns(j).ColumnName.ToString() = "age_condition" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "year_condition" Or mdtFilter.Columns(j).ColumnName.ToString() = "gpacode_string" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "nationalityunkid" Or mdtFilter.Columns(j).ColumnName.ToString() = "experience_days" Or _
                   mdtFilter.Columns(j).ColumnName.ToString() = "experience_condition" Or mdtFilter.Columns(j).ColumnName.ToString() = "qualificationgroup_condition" Then Continue For
                'Hemant (30 Oct 2019) -- [qualificationgroup_condition]
                'Hemant (07 Oct 2019) -- [mdtFilter.Columns(j).ColumnName.ToString() = "nationalityunkid", mdtFilter.Columns(j).ColumnName.ToString() = "experience_days", mdtFilter.Columns(j).ColumnName.ToString() = "experience_condition"]


                If mdtFilter.Columns(j).ColumnName.ToString() = "qlevel" Then
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & lblQualificationGrpLevel.Text & "</B></FONT></TD>" & vbCrLf)
                ElseIf mdtFilter.Columns(j).ColumnName.ToString() = "logicalcondition" Then
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Condtion") & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & mdtFilter.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                End If



            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To mdtFilter.Rows.Count - 1


                If mdtFilter.Rows(i)("AUD").ToString() = "D" Then Continue For

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To mdtFilter.Columns.Count - 1

                    If mdtFilter.Columns(k).ColumnName.ToString() = "filterunkid" Or mdtFilter.Columns(k).ColumnName.ToString() = "shortlistunkid" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "vacancyunkid" Or mdtFilter.Columns(k).ColumnName.ToString() = "qualificationgroupunkid" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "qualificationunkid" Or mdtFilter.Columns(k).ColumnName.ToString() = "resultunkid" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "gender" Or mdtFilter.Columns(k).ColumnName.ToString() = "skillcategoryunkid" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "skillunkid" Or mdtFilter.Columns(k).ColumnName.ToString() = "filter" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "IsApply" Or mdtFilter.Columns(k).ColumnName.ToString() = "AUD" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "GUID" Or mdtFilter.Columns(k).ColumnName.ToString() = "result_lvl_condition" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "gpacode_condition" Or mdtFilter.Columns(k).ColumnName.ToString() = "age_condition" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "year_condition" Or mdtFilter.Columns(k).ColumnName.ToString() = "gpacode_string" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "nationalityunkid" Or mdtFilter.Columns(k).ColumnName.ToString() = "experience_days" Or _
                           mdtFilter.Columns(k).ColumnName.ToString() = "experience_condition" Or mdtFilter.Columns(k).ColumnName.ToString() = "qualificationgroup_condition" Then Continue For
                    'Hemant (30 Oct 2019) -- [qualificationgroup_condition]
                    'Hemant (07 Oct 2019) -- [mdtFilter.Columns(k).ColumnName.ToString() = "nationalityunkid", mdtFilter.Columns(k).ColumnName.ToString() = "experience_days", mdtFilter.Columns(k).ColumnName.ToString() = "experience_condition"]

                    If mdtFilter.Columns(k).ColumnName = "logicalcondition" Then

                        If CInt(mdtFilter.Rows(i)(k)) = 0 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2></FONT></TD>" & vbCrLf)
                            strBuilder.Append(" </TR> " & vbCrLf)
                            strBuilder.Append(" <TR> " & vbCrLf)
                        ElseIf CInt(mdtFilter.Rows(i)(k)) = 1 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK  VALIGN='TOP'><FONT SIZE=2 ><B>" & rdANDCondition.Text & "</B></FONT></TD>" & vbCrLf)
                        ElseIf CInt(mdtFilter.Rows(i)(k)) = 2 Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK VALIGN='TOP'><FONT SIZE=2><B>" & rdORCondition.Text & "</B></FONT></TD>" & vbCrLf)
                        End If

                    Else
                        strBuilder.Append("<TD VALIGN= 'TOP' BORDER=1 BORDERCOLOR=BLACK ><FONT SIZE=2>" & mdtFilter.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                    End If

                Next

                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            'END FOR SHOWING FILTER 

            'Pinkal (06-Feb-2012) -- End



            strBuilder.Append(" </HTML> " & vbCrLf)



            'Pinkal (18-Jun-2013) -- Start
            'Enhancement : TRA Changes


            'If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
            '    SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            'End If

            'blnFlag = SaveExcelfile(SavePath & "\" & flFileName, strBuilder)

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            blnFlag = SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder)

            If IsOpenAfterExport Then
                Call Shell("explorer " & SavePath & "\" & flFileName & ".xls", vbMaximizedFocus)
            End If

            'Pinkal (18-Jun-2013) -- End

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Public Function GetFilterString(ByVal dr As DataRow) As String
        Dim strFilter As String = ""
        Try
            If dr IsNot Nothing Then


                'Pinkal (11-MAY-2012) -- Start
                'Enhancement : TRA Changes

                'If rdORCondition.Checked = True Then
                'strFilter = " vacancyunkid = " & dr("vacancyunkid").ToString()
                'ElseIf rdANDCondition.Checked = True Then
                '    strFilter = " Vac.vacancyunkid = " & dr("vacancyunkid").ToString()
                'End If

                If rdORCondition.Checked = True Then
                strFilter = " vacancyunkid = " & dr("vacancyunkid").ToString()
                ElseIf rdANDCondition.Checked = True Then
                    strFilter = " rcapp_vacancy_mapping.vacancyunkid = " & dr("vacancyunkid").ToString()
                End If
                'Pinkal (11-MAY-2012) -- End



                'Pinkal (11-MAY-2012) -- Start
                'Enhancement : TRA Changes
                'If chkOtherQualification.Checked = False Then 'Sohail (20 Mar 2020)
                    If rdANDCondition.Checked Then
                        'S.SANDEEP [ 05 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND cfcommon_master.qlevel=" & dr("qlevel").ToString()
                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                        'If CInt(dr("qualificationgroupunkid")) > 0 Or CInt(IIf(IsDBNull(dr("qlevel")) = True, 0, dr("qlevel"))) > 0 Then
                        '    strFilter &= " AND cfcommon_master.qlevel=" & dr("qlevel").ToString()
                        If dr("qualificationgroupunkid").ToString().Trim.Length > 0 Or IIf(IsDBNull(dr("qlevel")) = True, 0, dr("qlevel")).ToString.Trim.Length > 0 Then
                            'Hemant (30 Oct 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            If dr("qualificationgroupunkid").ToString().Trim.Length > -1 AndAlso dr("qlevel").ToString().Trim.Length > 0 Then
                                'Hemant (30 Oct 2019) -- End
                                'Hemant (12 Nov 2019) -- Start
                                If CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                                    Dim strTempFilter As String = String.Empty
                                    Dim arrQLevelID() As String = Split(dr("qlevel").ToString(), ",").Distinct.ToArray
                                    strFilter &= " AND ( #CONDITION# ) "
                                    For Each str As String In arrQLevelID

                                        If strTempFilter.Trim.Length > 0 Then
                                            strTempFilter &= " OR "
                                        End If
                                        strTempFilter &= "( cfcommon_master.qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                                    Next
                                    strFilter = strFilter.Replace("#CONDITION#", strTempFilter)
                                Else
                                    'Hemant (12 Nov 2019) -- End
                            strFilter &= " AND cfcommon_master.qlevel in (" & dr("qlevel").ToString() & ")"
                                End If 'Hemant (30 Oct 2019)
                                'Hemant (12 Nov 2019) -- End
                            End If 'Hemant (07 Oct 2019)
                        End If
                        'S.SANDEEP [ 05 NOV 2012 ] -- END
                    Else
                        'S.SANDEEP [ 05 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND qlevel=" & dr("qlevel").ToString()
                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                        'If CInt(dr("qualificationgroupunkid")) > 0 Or CInt(IIf(IsDBNull(dr("qlevel")) = True, 0, dr("qlevel"))) > 0 Then
                        If dr("qualificationgroupunkid").ToString().Trim.Length > 0 Or IIf(IsDBNull(dr("qlevel")) = True, 0, dr("qlevel")).ToString.Trim.Length > 0 Then
                            'Hemant (07 Oct 2019) -- End
                            'Hemant (27 Sep 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
                            'Hemant (07 Oct 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            'If CInt(dr("qlevel").ToString()) > -1 Then
                            '   strFilter &= " AND qlevel=" & dr("qlevel").ToString()
                            If dr("qualificationgroupunkid").ToString().Trim.Length > -1 AndAlso dr("qlevel").ToString().Trim.Length > 0 Then
                                'Hemant (30 Oct 2019) -- Start
                                If CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                                    Dim strTempFilter As String = String.Empty
                                    Dim arrQLevelID() As String = Split(dr("qlevel").ToString(), ",").Distinct.ToArray
                                    strFilter &= " AND ( #CONDITION# ) "
                                    For Each str As String In arrQLevelID

                                        If strTempFilter.Trim.Length > 0 Then
                                            strTempFilter &= " OR "
                                        End If
                                        strTempFilter &= "( qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                                    Next
                                    strFilter = strFilter.Replace("#CONDITION#", strTempFilter)
                                Else
                                    'Hemant (30 Oct 2019) -- End
                                strFilter &= " AND qlevel IN (" & dr("qlevel").ToString() & ")"
                                End If
                                'Hemant (07 Oct 2019) -- End
                                'Hemant (27 Sep 2019) -- End
                            End If 'Hemant (27 Sep 2019)
                End If
                        'S.SANDEEP [ 05 NOV 2012 ] -- END
                    End If

                'End If 'Sohail (20 Mar 2020)

                'Pinkal (11-MAY-2012) -- End




                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                'If CInt(dr("qualificationgroupunkid")) > 0 Then
                If dr("qualificationgroupunkid").ToString().Trim.Length > 0 AndAlso dr("qualificationgroupunkid").ToString().Trim <> "0" Then
                    'Hemant (07 Oct 2019) -- End
                    If rdORCondition.Checked Then
                        'Hemant (30 Oct 2019) -- Start
                        If CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                            Dim strTempFilter As String = String.Empty
                            'Hemant (12 Nov 2019) -- Start
                            'Dim arrQualificationGroupID() As String = Split(dr("qualificationgroupunkid").ToString, ",").Distinct.ToArray
                            'strFilter &= " AND ( #CONDITION# ) "
                            'For Each str As String In arrQualificationGroupID

                            '    If strTempFilter.Trim.Length > 0 Then
                            '        strTempFilter &= " OR "
                            '    End If
                            '    strTempFilter &= "( qualificationgroupunkid " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                            'Next
                            'strFilter = strFilter.Replace("#CONDITION#", strTempFilter)
                            Dim strQualificationGroupIds As String = ""
                            Dim strSearch As String = String.Empty
                            Dim dsQualificationGroupList As DataSet
                            Dim objMaster As New clsCommon_Master
                            Dim strQuaLevels As String = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("qlevel").ToString()).ToArray())

                            Dim arrQuaLevels() As String = Split(strQuaLevels, ",")
                            strSearch = " AND ( #CONDITION# ) "
                            For Each str As String In arrQuaLevels
                                If strTempFilter.Trim.Length > 0 Then
                                    strTempFilter &= " OR "
                                End If
                                strTempFilter &= "( qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                            Next
                            strSearch = strSearch.Replace("#CONDITION#", strTempFilter)
                            dsQualificationGroupList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
                            strQualificationGroupIds = String.Join(",", dsQualificationGroupList.Tables(0).AsEnumerable().Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())
                            strFilter &= " AND qualificationgroupunkid IN ( " & strQualificationGroupIds & ")"
                            'Hemant (12 Nov 2019) -- End
                        Else
                            'Hemant (30 Oct 2019) -- End
                        strFilter &= " AND qualificationgroupunkid  in ( " & dr("qualificationgroupunkid").ToString().Trim & ") "
                        End If
                    Else
                        'Hemant (30 Oct 2019) -- Start
                        If CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                            Dim strTempFilter As String = String.Empty
                            'Hemant (12 Nov 2019) -- Start
                            'Dim arrQualificationGroupID() As String = Split(dr("qualificationgroupunkid").ToString, ",").Distinct.ToArray
                            'strFilter &= " AND ( #CONDITION# ) "
                            'For Each str As String In arrQualificationGroupID

                            '    If strTempFilter.Trim.Length > 0 Then
                            '        strTempFilter &= " OR "
                            '    End If
                            '    strTempFilter &= "( rcapplicantqualification_tran.qualificationgroupunkid " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                            'Next
                            'strFilter = strFilter.Replace("#CONDITION#", strTempFilter)

                            Dim strQualificationGroupIds As String = ""
                            Dim strSearch As String = String.Empty
                            Dim dsQualificationGroupList As DataSet
                            Dim objMaster As New clsCommon_Master
                            Dim strQuaLevels As String = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("qlevel").ToString()).ToArray())

                            Dim arrQuaLevels() As String = Split(strQuaLevels, ",")
                            strSearch = " AND ( #CONDITION# ) "
                            For Each str As String In arrQuaLevels
                                If strTempFilter.Trim.Length > 0 Then
                                    strTempFilter &= " OR "
                                End If
                                strTempFilter &= "( qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                            Next
                            strSearch = strSearch.Replace("#CONDITION#", strTempFilter)
                            dsQualificationGroupList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
                            strQualificationGroupIds = String.Join(",", dsQualificationGroupList.Tables(0).AsEnumerable().Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())
                            strFilter &= " AND rcapplicantqualification_tran.qualificationgroupunkid IN ( " & strQualificationGroupIds & ")"
                            'Hemant (12 Nov 2019) -- End
                    Else
                            'Hemant (30 Oct 2019) -- End
                        strFilter &= " AND rcapplicantqualification_tran.qualificationgroupunkid in (" & dr("qualificationgroupunkid").ToString().Trim & ") "
                End If
                End If
                End If

                'If CInt(dr("qualificationunkid")) > 0 Then
                '    strFilter &= " AND qualificationunkid = " & CInt(dr("qualificationunkid"))
                'End If

                If dr("qualificationunkid").ToString().Trim.Length > 0 Then
                    If rdORCondition.Checked Then
                        strFilter &= " AND qualificationunkid in (" & dr("qualificationunkid").ToString().Trim & ") "
                    Else
                        strFilter &= " AND rcapplicantqualification_tran.qualificationunkid in (" & dr("qualificationunkid").ToString().Trim & ") "
                End If
                End If

                'Pinkal (12-Oct-2011) -- End

                'If CInt(dr("resultunkid")) > 0 Then
                '    strFilter &= " AND rcapplicantqualification_tran.resultunkid = " & CInt(dr("resultunkid"))
                'End If

                If CInt(dr("result_level")) > 0 Then
                    If CInt(dr("result_lvl_condition")) > 0 Then
                        strFilter &= " AND result_level  " & dr("resultcondition").ToString() & " " & CInt(dr("result_level"))
                    Else
                        strFilter &= " AND result_level  = " & CInt(dr("result_level"))
                    End If
                End If


                If CDec(dr("gpacode")) > 0 Then
                    If CInt(dr("gpacode_condition")) > 0 Then
                        strFilter &= " AND gpacode " & dr("gpacondition").ToString() & "  " & CDec(dr("gpacode"))
                    Else
                        strFilter &= " AND gpacode = " & CDec(dr("gpacode"))
                    End If
                End If


                If CInt(dr("age")) > 0 Then
                    If rdORCondition.Checked Then
                    If CInt(dr("age_condition")) > 0 Then
                        strFilter &= " AND age " & dr("agecondition").ToString() & "  " & CInt(dr("age"))
                    Else
                        strFilter &= " AND age = " & CInt(dr("age"))
                    End If
                    Else
                        If CInt(dr("age_condition")) > 0 Then
                            strFilter &= " AND   ISNULL(Year(GETDATE()) - Year(rcapplicant_master.birth_date), 0) " & dr("agecondition").ToString() & "  " & CInt(dr("age"))
                        Else
                            strFilter &= " AND   ISNULL(Year(GETDATE()) - Year(rcapplicant_master.birth_date), 0) = " & CInt(dr("age"))
                        End If

                    End If
                End If


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                If CInt(dr("award_year")) > 0 Then
                    If CInt(dr("year_condition")) > 0 Then
                        strFilter &= " AND YEAR(award_end_date)  " & dr("yearcondition").ToString() & " " & CInt(dr("award_year"))
                    Else
                        strFilter &= " AND YEAR(award_end_date)  = " & CInt(dr("award_year"))
                    End If
                End If
                'Pinkal (06-Feb-2012) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                If CInt(dr("experience_days")) > 0 Then
                    If CInt(dr("experience_condition")) > 0 Then
                        strFilter &= " AND experience_days " & dr("experiencecondition").ToString() & "  " & CInt(dr("experience_days"))
                    Else
                        strFilter &= " AND experience_days = " & CInt(dr("experience_days"))
                    End If
                End If
                'Hemant (07 Oct 2019) -- End


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes

                If rdANDCondition.Checked Then

                If dr("other_qualificationgrp").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND rcapplicantqualification_tran.other_qualificationgrp like '%" & dr("other_qualificationgrp").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_qualificationgrp").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (rcapplicantqualification_tran.other_qualificationgrp like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR rcapplicantqualification_tran.other_qualificationgrp like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                    strFilter &= " AND rcapplicantqualification_tran.other_qualificationgrp like '%" & dr("other_qualificationgrp").ToString().Trim & "%'"
                End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                If dr("other_qualification").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND rcapplicantqualification_tran.other_qualification like '%" & dr("other_qualification").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_qualification").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (rcapplicantqualification_tran.other_qualification like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR rcapplicantqualification_tran.other_qualification like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                    strFilter &= " AND rcapplicantqualification_tran.other_qualification like '%" & dr("other_qualification").ToString().Trim & "%'"
                End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                If dr("other_resultcode").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND rcapplicantqualification_tran.other_resultcode like '%" & dr("other_resultcode").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_resultcode").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (rcapplicantqualification_tran.other_resultcode like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR rcapplicantqualification_tran.other_resultcode like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                    strFilter &= " AND rcapplicantqualification_tran.other_resultcode like '%" & dr("other_resultcode").ToString().Trim & "%'"
                End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                If dr("other_skillcategory").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND rcapplicantskill_tran.other_skillcategory like '%" & dr("other_skillcategory").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_skillcategory").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        'Pinkal (24-Sep-2020) -- Start
                                        'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                                        'strFilter &= " AND (rcapplicantqualification_tran.other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                        strFilter &= " AND (rcapplicantskill_tran.other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                        'Pinkal (24-Sep-2020) -- End
                                    Case Else
                                        'Pinkal (24-Sep-2020) -- Start
                                        'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                                        'strFilter &= " OR rcapplicantqualification_tran.other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                        strFilter &= " OR rcapplicantskill_tran.other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                        'Pinkal (24-Sep-2020) -- End
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                            'Pinkal (24-Sep-2020) -- Start
                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                            'strFilter &= " AND rcapplicantqualification_tran.other_skillcategory like '%" & dr("other_skillcategory").ToString().Trim & "%'"
                            strFilter &= " AND rcapplicantskill_tran.other_skillcategory like '%" & dr("other_skillcategory").ToString().Trim & "%'"
                            'Pinkal (24-Sep-2020) -- End
                        End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                If dr("other_skill").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND rcapplicantskill_tran.other_skill like '%" & dr("other_skill").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_skill").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        'Pinkal (24-Sep-2020) -- Start
                                        'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                                        'strFilter &= " AND (rcapplicantqualification_tran.other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                        strFilter &= " AND (rcapplicantskill_tran.other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                        'Pinkal (24-Sep-2020) -- End
                                    Case Else
                                        'Pinkal (24-Sep-2020) -- Start
                                        'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                                        'strFilter &= " OR rcapplicantqualification_tran.other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                        strFilter &= " OR rcapplicantskill_tran.other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                        'Pinkal (24-Sep-2020) -- End
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                            'Pinkal (24-Sep-2020) -- Start
                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                            'strFilter &= " AND rcapplicantqualification_tran.other_skill like '%" & dr("other_skill").ToString().Trim & "%'"
                            strFilter &= " AND rcapplicantskill_tran.other_skill like '%" & dr("other_skill").ToString().Trim & "%'"
                            'Pinkal (24-Sep-2020) -- End
                        End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                Else

                    If dr("other_qualificationgrp").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND other_qualificationgrp like '%" & dr("other_qualificationgrp").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_qualificationgrp").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (other_qualificationgrp like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR other_qualificationgrp like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                        strFilter &= " AND other_qualificationgrp like '%" & dr("other_qualificationgrp").ToString().Trim & "%'"
                    End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                    If dr("other_qualification").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND other_qualification like '%" & dr("other_qualification").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_qualification").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (other_qualification like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR other_qualification like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                        strFilter &= " AND other_qualification like '%" & dr("other_qualification").ToString().Trim & "%'"
                    End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                    If dr("other_resultcode").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND other_resultcode like '%" & dr("other_resultcode").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_resultcode").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (other_resultcode like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR other_resultcode like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                        strFilter &= " AND other_resultcode like '%" & dr("other_resultcode").ToString().Trim & "%'"
                    End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                    If dr("other_skillcategory").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND other_skillcategory like '%" & dr("other_skillcategory").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_skillcategory").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR other_skillcategory like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                        strFilter &= " AND other_skillcategory like '%" & dr("other_skillcategory").ToString().Trim & "%'"
                    End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                    If dr("other_skill").ToString().Trim <> "" Then
                        'S.SANDEEP [ 07 NOV 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strFilter &= " AND other_skill like '%" & dr("other_skill").ToString().Trim & "%'"
                        Dim StrSplit() As String = dr("other_skill").ToString.Trim.Split(CChar(","))
                        If StrSplit.Length > 0 Then
                            For i As Integer = 0 To StrSplit.Length - 1
                                Select Case i
                                    Case 0
                                        strFilter &= " AND (other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                    Case Else
                                        strFilter &= " OR other_skill like '%" & StrSplit(i).ToString().Trim & "%'"
                                End Select
                            Next
                            strFilter &= ")"
                        Else
                        strFilter &= " AND other_skill like '%" & dr("other_skill").ToString().Trim & "%'"
                    End If
                        'S.SANDEEP [ 07 NOV 2012 ] -- END
                    End If

                End If


                'Pinkal (5-MAY-2012) -- End



                If CInt(dr("gender")) > 0 Then
                    'Sohail (24 Apr 2020) -- Start
                    'NMB Issue # : Ambiguous column name 'gender'.
                    'strFilter &= " AND gender = " & CInt(dr("gender"))
                    'Hemant (03 Jun 2020) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : Error on shortlisting screen when shortlisting by gender.)
                    If rdORCondition.Checked = True Then
                        strFilter &= " AND gender = " & CInt(dr("gender"))
                    ElseIf rdANDCondition.Checked = True Then
                        'Hemant (03 Jun 2020) -- End
                    strFilter &= " AND rcapplicant_master.gender = " & CInt(dr("gender"))
                    End If 'Hemant (03 Jun 2020)
                    'Sohail (24 Apr 2020) -- End
                End If

                If dr("skillcategoryunkid").ToString().Trim.Length > 0 Then
                    strFilter &= " AND skillcategoryunkid in (" & dr("skillcategoryunkid").ToString().Trim & ")"
                End If

                If dr("skillunkid").ToString().Trim.Length > 0 Then
                    strFilter &= " AND skillunkid in (" & dr("skillunkid").ToString().Trim & ")"
                End If

                'Hemant (02 Jul 2019) -- Start
                'ENHANCEMENT :  ZRA minimum Requirements
                If CInt(dr("nationalityunkid")) > 0 Then
                    If rdORCondition.Checked = True Then
                        strFilter &= " AND nationalityunkid = " & dr("nationalityunkid").ToString()
                    ElseIf rdANDCondition.Checked = True Then
                        strFilter &= " AND rcapplicant_master.nationality = " & dr("nationalityunkid").ToString()
                    End If
                End If
                'Hemant (02 Jul 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
        End Try
        Return strFilter
    End Function

    Public Function validation() As Boolean
        Try
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
            'If ConfigParameter._Object._QualificationCertificateAttachmentMandatory = True Then
            '    Dim objScan As New clsScan_Attach_Documents
            '    Dim ds As DataSet = objScan.GetValidationList(enImg_Email_RefId.Applicant_Module, enScanAttactRefId.QUALIFICATIONS, "")
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        Dim objFrm As New frmCommonValidationList
            '        ds.Tables(0).Columns.Remove("employeeunkid")
            '        ds.Tables(0).Columns("Name").SetOrdinal(0)
            '        objFrm.displayDialog(False, Language.getMessage(mstrModuleName, 15, "Qualification attachment is mandatory. Please add atleast one qualification attachment for the following applicants."), ds.Tables(0))
            '        Return False
            '    End If
            'End If

            'If ConfigParameter._Object._OneCurriculamVitaeMandatoryInRecruitment = True Then
            '    Dim objScan As New clsScan_Attach_Documents
            '    Dim ds As DataSet = objScan.GetValidationList(enImg_Email_RefId.Applicant_Module, enScanAttactRefId.CURRICULAM_VITAE, "")
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        Dim objFrm As New frmCommonValidationList
            '        ds.Tables(0).Columns.Remove("employeeunkid")
            '        ds.Tables(0).Columns("Name").SetOrdinal(0)
            '        objFrm.displayDialog(False, Language.getMessage(mstrModuleName, 16, "Curriculum vitae attachment is mandatory. Please add atleast one curriculam vitae attachment for the following applicants."), ds.Tables(0))
            '        Return False
            '    End If
            'End If

            'If ConfigParameter._Object._OneCoverLetterMandatoryInRecruitment = True Then
            '    Dim objScan As New clsScan_Attach_Documents
            '    Dim ds As DataSet = objScan.GetValidationList(enImg_Email_RefId.Applicant_Module, enScanAttactRefId.COVER_LETTER, "")
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        Dim objFrm As New frmCommonValidationList
            '        ds.Tables(0).Columns.Remove("employeeunkid")
            '        ds.Tables(0).Columns("Name").SetOrdinal(0)
            '        objFrm.displayDialog(False, Language.getMessage(mstrModuleName, 17, "Cover letter attachment is mandatory. Please add atleast one cover letter attachment for the following applicants."), ds.Tables(0))
            '        Return False
            '    End If
            'End If
            'Sohail (04 Jul 2019) -- End

            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Vacancy Type is compulsory information.Please Select Vacancy Type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancyType.Select()
                Return False
            ElseIf CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Vacancy is compulsory information.Please Select Vacancy."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancy.Select()
                Return False
            End If

            'Hemant (30 Oct 2019) -- Start
            Dim strQualificationGroupLevels As String = ""
            strQualificationGroupLevels = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("qlevel").ToString()).ToArray())
            Dim arrQuaLevel() As String = Split(strQualificationGroupLevels, ",").Distinct.ToArray()
            If arrQuaLevel.Length > 1 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Multiple Different Types of Levels are not allowed for selected Qualification Group Condition "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgvQualificationGroup.Focus()
                Return False
            End If
            'Hemant (30 Oct 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub GetORFilter()
        Try
            If arstrAppliFilter IsNot Nothing AndAlso arstrAppliFilter.Length > 0 Then
                For i As Integer = 0 To arstrAppliFilter.Length - 1
                    'Sohail (13 Mar 2020) -- Start
                    'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
                    'Dim ds As DataSet = objShortFilter.GetORFilterApplicant(arstrAppliFilter(i).ToString(), dgFilter.Rows(i).Cells(objcolhGUID.Name).Value.ToString())


                    'Pinkal (24-Sep-2020) -- Start
                    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                    'Dim ds As DataSet = objShortFilter.GetORFilterApplicant(arstrAppliFilter(i).ToString(), dgFilter.Rows(i).Cells(objcolhGUID.Name).Value.ToString(), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
                    Dim ds As DataSet = objShortFilter.GetORFilterApplicant(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, arstrAppliFilter(i).ToString() _
                                                                                                    , dgFilter.Rows(i).Cells(objcolhGUID.Name).Value.ToString(), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
                    'Pinkal (24-Sep-2020) -- End   


                    'Sohail (13 Mar 2020) -- End
                    If dsFilterApplicant Is Nothing Then dsFilterApplicant = ds.Clone
                    dsFilterApplicant.Merge(ds, True)
                Next
                FillApplicant(dsFilterApplicant)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetORFilterApplicant", mstrModuleName)
        End Try
    End Sub

    Private Sub GetANDFilter()
        'Sohail (20 Mar 2020) -- Start
        'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
        '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
        Dim arrRemoveApplicant As New ArrayList
        arrRemoveApplicant.Clear()
        'Sohail (20 Mar 2020) -- End
        Try
            lvApplicant.Items.Clear()
            If arstrAppliFilter IsNot Nothing AndAlso arstrAppliFilter.Length > 0 Then



                'Pinkal (11-MAY-2012) -- Start
                'Enhancement : TRA Changes
                'Dim ds As DataSet = Nothing

                'If chkOtherQualification.Checked Then

                '    For i As Integer = 0 To arstrAppliFilter.Length - 1
                '        Dim dsFilter As DataSet = objShortFilter.GetANDFilterApplicant(arstrAppliFilter(i).ToString(), dgFilter.Rows(i).Cells(objcolhGUID.Name).Value.ToString())
                '        If ds Is Nothing Then ds = dsFilter.Clone
                '        ds.Merge(dsFilter)
                '    Next

                'Else
                'ds = objShortFilter.GetANDFilterApplicant(arstrAppliFilter(0).ToString(), dgFilter.Rows(0).Cells(objcolhGUID.Name).Value.ToString())
                'End If

                'Sohail (13 Mar 2020) -- Start
                'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
                'Dim ds As DataSet = objShortFilter.GetANDFilterApplicant(arstrAppliFilter(0).ToString(), dgFilter.Rows(0).Cells(objcolhGUID.Name).Value.ToString())

                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                'Dim ds As DataSet = objShortFilter.GetANDFilterApplicant(arstrAppliFilter(0).ToString(), dgFilter.Rows(0).Cells(objcolhGUID.Name).Value.ToString(), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
                Dim ds As DataSet = objShortFilter.GetANDFilterApplicant(Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, arstrAppliFilter(0).ToString() _
                                                                                                 , dgFilter.Rows(0).Cells(objcolhGUID.Name).Value.ToString(), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
                'Pinkal (24-Sep-2020) -- End


                'Sohail (13 Mar 2020) -- End

                'Pinkal (11-MAY-2012) -- End


                If dsFilterApplicant Is Nothing Then dsFilterApplicant = ds.Clone

                If arstrAppliFilter.Length > 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim objApplicantQulaification As New clsApplicantQualification_tran
                    Dim objApplicantSkill As New clsApplicantSkill_tran


                    'Pinkal (20-Oct-2020) -- Start
                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                    Dim objEmpQulaification As New clsEmp_Qualification_Tran
                    Dim objEmpSkilltran As New clsEmployee_Skill_Tran
                    'Pinkal (20-Oct-2020) -- End


                    Dim j As Integer = 1
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                        'Sohail (20 Mar 2020) -- Start
                        'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                        '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                        'For m As Integer = 1 To arstrAppliFilter.Length - 1
                        For m As Integer = 0 To arstrAppliFilter.Length - 1
                            'Sohail (20 Mar 2020) -- End

                            'Sohail (21 Apr 2020) -- Start
                            'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
                            If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = True Then
                                Continue For
                            End If
                            'Sohail (21 Apr 2020) -- End

                            'Pinkal (10-Apr-2018) -- Start
                            'Bug -  Solved bug when Filter on Other Qualification. (TANAPA)
                            'Dim dRow() As DataRow = mdtFilter.Select("filter = '" & arstrAppliFilter(m) & "'")

                            'Pinkal (21-APR-2018) -- Start
                            'BUG - ISSUE #2209 [ISSUES ON RECRUITMENT PROCESS] .
                            Dim dRow() As DataRow = mdtFilter.Select("filter = '" & arstrAppliFilter(m).ToString().Replace("'", "''") & "'")
                            ''Pinkal (21-APR-2018) -- End

                            'Pinkal (10-Apr-2018) -- End

                            If dRow.Length > 0 Then
                                j = mdtFilter.Rows.IndexOf(dRow(0))
                        End If

                            'Pinkal (20-Oct-2020) -- Start
                            'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                            If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                        objApplicantQulaification._ApplicantUnkid = CInt(ds.Tables(0).Rows(i)("applicantunkid"))
                        objApplicantSkill._ApplicantUnkid = CInt(ds.Tables(0).Rows(i)("applicantunkid"))
                            Else
                                objEmpQulaification._Employeeunkid = CInt(ds.Tables(0).Rows(i)("employeeunkid"))
                                objEmpQulaification._Employeeunkid = CInt(ds.Tables(0).Rows(i)("employeeunkid"))
                            End If
                            'Pinkal (20-Oct-2020) -- End


                            'Hemant (02 Sep 2019) -- Start
                            'ISSUE(NMB) : When we select qualification group  + one gender. After applying, its picking people of both gender after applying filter
                            If CInt(mdtFilter.Rows(j)("gender")) > 0 Then


                                'Pinkal (10-Nov-2020) -- Start
                                'Bug Recruitment NMB -   Problem Resolved for Gender Short Listing for NMB.
                                'Dim drGender As DataRow() = ds.Tables(0).Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & " AND gender = " & CInt(mdtFilter.Rows(j)("gender")))
                                Dim drGender As DataRow() = Nothing
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drGender = ds.Tables(0).Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & " AND gender = " & CInt(mdtFilter.Rows(j)("gender")))
                                Else
                                    drGender = ds.Tables(0).Select("employeeunkid = " & CInt(ds.Tables(0).Rows(i)("employeeunkid")) & " AND gender = " & CInt(mdtFilter.Rows(j)("gender")))
                                End If
                                'Pinkal (10-Nov-2020) -- End



                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drGender.Length <= 0 Then Continue For
                                If drGender.Length <= 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                            End If
                            'Hemant (02 Sep 2019) -- End

                            'Sohail (21 Apr 2020) -- Start
                            'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
                            If CInt(mdtFilter.Rows(j)("nationalityunkid")) > 0 Then

                                'Pinkal (10-Nov-2020) -- Start
                                'Bug Recruitment NMB -   Problem Resolved for Gender Short Listing for NMB.
                                'Dim drNationality As DataRow() = ds.Tables(0).Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & " AND nationalityunkid = " & CInt(mdtFilter.Rows(j)("nationalityunkid")))
                                Dim drNationality As DataRow() = Nothing
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drNationality = ds.Tables(0).Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & " AND nationalityunkid = " & CInt(mdtFilter.Rows(j)("nationalityunkid")))
                                Else
                                    drNationality = ds.Tables(0).Select("employeeunkid = " & CInt(ds.Tables(0).Rows(i)("employeeunkid")) & " AND nationalityunkid = " & CInt(mdtFilter.Rows(j)("nationalityunkid")))
                                End If
                                'Pinkal (10-Nov-2020) -- End


                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drGender.Length <= 0 Then Continue For
                                If drNationality.Length <= 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                            End If
                            'Sohail (21 Apr 2020) -- End

                            'Sohail (20 Mar 2020) -- Start
                            'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                            '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                            'If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                            If mdtFilter.Rows(j)("qualificationgroupunkid").ToString.Trim.Length > 0 AndAlso mdtFilter.Rows(j)("qualificationgroupunkid").ToString.Trim <> "0" Then
                                'Sohail (20 Mar 2020) -- End
                                'Hemant (07 Oct 2019) -- Start
                                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                'Dim drQGroup As DataRow() = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid")))
                                'Sohail (21 Apr 2020) -- Start
                                'NMB Enhancement # : On shortlisting screen, the screen is returning false results when AND condition is used and combined with other different parameters. E.g qualification group AND Nationality.
                                'Dim drQGroup As DataRow() = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND  qualificationgroupunkid IN ( " & mdtFilter.Rows(j)("qualificationgroupunkid").ToString() & ")")


                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                Dim sFilter As String = ""
                                Dim drQGroup As DataRow() = Nothing

                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then

                                    sFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid & " AND ( qualificationgroupunkid IN ( " & mdtFilter.Rows(j)("qualificationgroupunkid").ToString() & ") "
                                If CInt(mdtFilter.Rows(j)("qualificationgroup_condition")) > 0 Then
                                    sFilter &= " OR qlevel " & mdtFilter.Rows(j)("qualificationgroupcondition").ToString & " " & CInt(mdtFilter.Rows(j)("qlevel")) & "  "
                                End If
                                sFilter &= " ) "

                                    drQGroup = objApplicantQulaification._DataList.Select(sFilter)
                                Else

                                    sFilter = "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & " AND ( hremp_qualification_tran.qualificationgroupunkid IN ( " & mdtFilter.Rows(j)("qualificationgroupunkid").ToString() & ") "
                                    If CInt(mdtFilter.Rows(j)("qualificationgroup_condition")) > 0 Then
                                        sFilter &= " OR  cfcommon_master.qlevel " & mdtFilter.Rows(j)("qualificationgroupcondition").ToString & " " & CInt(mdtFilter.Rows(j)("qlevel")) & "  "
                                    End If
                                    sFilter &= " ) "
                                    Dim mdtTable As DataTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                            "List", True, sFilter).Tables(0)

                                    drQGroup = mdtTable.Select("1=1")
                                End If

                                'Pinkal (20-Oct-2020) -- End


                                'Sohail (21 Apr 2020) -- End
                                'Hemant (07 Oct 2019) -- End
                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drQGroup.Length = 0 Then Continue For
                                If drQGroup.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                        End If

                        If mdtFilter.Rows(j)("qualificationunkid").ToString().Length > 0 Then

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                Dim drQualification As DataRow() = Nothing
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drQualification = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString() & ")")
                                Else
                                    Dim mdtTable As DataTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                          "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & " AND hremp_qualification_tran.qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString() & ")").Tables(0)

                                    drQualification = mdtTable.Select("1=1")

                                End If
                                'Pinkal (20-Oct-2020) -- End



                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return  correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drQualification.Length = 0 Then Continue Forthe
                                If drQualification.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                        End If


                            'Sohail (20 Mar 2020) -- Start
                            'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                            '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                            'Dim blnOther As Boolean = False
                            'For k As Integer = 0 To mdtFilter.Rows.Count - 1

                            '    Dim StrFilter As String = String.Empty
                            '    Dim StrSplit() As String = Nothing
                            '    If mdtFilter.Rows(k)("other_qualificationgrp").ToString().Length > 0 Then
                            '        StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                            '        StrSplit = mdtFilter.Rows(k)("other_qualificationgrp").ToString().Trim.Split(CChar(","))
                            '        If StrSplit.Length > 0 Then
                            '            For iSplit As Integer = 0 To StrSplit.Length - 1
                            '                Select Case iSplit
                            '                    Case 0
                            '                        StrFilter &= " AND (other_qualificationgrp like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                    Case Else
                            '                        StrFilter &= " OR other_qualificationgrp like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                End Select
                            '            Next
                            '            StrFilter &= ")"
                            '        End If


                            '        'Pinkal (10-Dec-2012) -- Start
                            '        'Enhancement : TRA Changes

                            '        Dim mstrFilter As String = ""

                            '        If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                            '            'Hemant (07 Oct 2019) -- Start
                            '            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            '            'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                            '            mstrFilter = " AND  qualificationgroupunkid IN (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                            '            'Hemant (07 Oct 2019) -- End
                            '        End If

                            '        If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                            '            mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                            '        End If

                            '        Dim drQGroup As DataRow() = Nothing

                            '        If mstrFilter.Trim.Length > 0 Then
                            '            drQGroup = objApplicantQulaification._DataList.Select(StrFilter & " " & mstrFilter)
                            '        Else
                            '            drQGroup = objApplicantQulaification._DataList.Select(StrFilter)
                            '        End If

                            '        'Pinkal (10-Dec-2012) -- End


                            '        If drQGroup.Length = 0 Then
                            '            blnOther = True
                            '            Exit For
                            '        End If

                            '    End If


                            '    If mdtFilter.Rows(k)("other_qualification").ToString().Length > 0 Then
                            '        StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                            '        StrSplit = mdtFilter.Rows(k)("other_qualification").ToString().Trim.Split(CChar(","))
                            '        If StrSplit.Length > 0 Then
                            '            For iSplit As Integer = 0 To StrSplit.Length - 1
                            '                Select Case iSplit
                            '                    Case 0
                            '                        StrFilter &= " AND (other_qualification like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                    Case Else
                            '                        StrFilter &= " OR other_qualification like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                End Select
                            '            Next
                            '            StrFilter &= ")"
                            '        End If


                            '        'Pinkal (10-Dec-2012) -- Start
                            '        'Enhancement : TRA Changes

                            '        Dim mstrFilter As String = ""

                            '        If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                            '            'Hemant (07 Oct 2019) -- Start
                            '            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            '            'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                            '            mstrFilter = " AND  qualificationgroupunkid IN (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                            '            'Hemant (07 Oct 2019) -- End
                            '        End If

                            '        If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                            '            mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                            '        End If

                            '        Dim drQualification As DataRow() = Nothing

                            '        If mstrFilter.Trim.Length > 0 Then
                            '            drQualification = objApplicantQulaification._DataList.Select(StrFilter & " " & mstrFilter)
                            '        Else
                            '            drQualification = objApplicantQulaification._DataList.Select(StrFilter)
                            '        End If

                            '        'Pinkal (10-Dec-2012) -- End


                            '        If drQualification.Length = 0 Then
                            '            blnOther = True
                            '            Exit For
                            '        End If
                            '    End If

                            '    If mdtFilter.Rows(k)("other_resultcode").ToString().Length > 0 Then
                            '        StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                            '        StrSplit = mdtFilter.Rows(k)("other_resultcode").ToString().Trim.Split(CChar(","))
                            '        If StrSplit.Length > 0 Then
                            '            For iSplit As Integer = 0 To StrSplit.Length - 1
                            '                Select Case iSplit
                            '                    Case 0
                            '                        StrFilter &= " AND (other_resultcode like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                    Case Else
                            '                        StrFilter &= " OR other_resultcode like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                            '                End Select
                            '            Next
                            '            StrFilter &= ")"
                            '        End If


                            '        'Pinkal (10-Dec-2012) -- Start
                            '        'Enhancement : TRA Changes

                            '        Dim mstrFilter As String = ""

                            '        If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                            '            'Hemant (07 Oct 2019) -- Start
                            '            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            '            'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                            '            mstrFilter = " AND  qualificationgroupunkid IN (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                            '            'Hemant (07 Oct 2019) -- End
                            '        End If

                            '        If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                            '            mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                            '        End If

                            '        Dim drResultcode As DataRow() = Nothing

                            '        If mstrFilter.Trim.Length > 0 Then
                            '            drResultcode = objApplicantQulaification._DataList.Select(StrFilter & " " & mstrFilter)
                            '        Else
                            '            drResultcode = objApplicantQulaification._DataList.Select(StrFilter)
                            '        End If

                            '        'Pinkal (10-Dec-2012) -- End

                            '        If drResultcode.Length = 0 Then
                            '            blnOther = True
                            '            Exit For
                            '        End If
                            '    End If

                            'Next

                            'If blnOther = True Then Continue For
                            Dim StrFilter As String = String.Empty
                            Dim StrSplit() As String = Nothing
                            If mdtFilter.Rows(j)("other_qualificationgrp").ToString().Length > 0 Then

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                                Else
                                    StrFilter = "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid
                                End If
                                'Pinkal (20-Oct-2020) -- End


                                StrSplit = mdtFilter.Rows(j)("other_qualificationgrp").ToString().Trim.Split(CChar(","))
                                If StrSplit.Length > 0 Then
                                    For iSplit As Integer = 0 To StrSplit.Length - 1
                                        Select Case iSplit
                                            Case 0
                                                StrFilter &= " AND (other_qualificationgrp like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                            Case Else
                                                StrFilter &= " OR other_qualificationgrp like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                        End Select
                                    Next
                                    StrFilter &= ")"
                                End If

                                    Dim drQGroup As DataRow() = Nothing

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                        drQGroup = objApplicantQulaification._DataList.Select(StrFilter)
                                Else
                                    Dim mdtTable As DataTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                          "List", True, StrFilter).Tables(0)
                                    drQGroup = mdtTable.Select("1=1")
                                End If
                                'Pinkal (20-Oct-2020) -- End


                                If drQGroup.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                        End If
                        End If

                            If mdtFilter.Rows(j)("other_qualification").ToString().Length > 0 Then
                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                                Else
                                    StrFilter = "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid
                                End If
                                'Pinkal (20-Oct-2020) -- End

                                StrSplit = mdtFilter.Rows(j)("other_qualification").ToString().Trim.Split(CChar(","))
                                If StrSplit.Length > 0 Then
                                    For iSplit As Integer = 0 To StrSplit.Length - 1
                                        Select Case iSplit
                                            Case 0
                                                StrFilter &= " AND (other_qualification like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                            Case Else
                                                StrFilter &= " OR other_qualification like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                        End Select
                                    Next
                                    StrFilter &= ")"
                                End If


                                    Dim drQualification As DataRow() = Nothing
                                'Pinkal (20-Oct-2020) -- Start

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                        drQualification = objApplicantQulaification._DataList.Select(StrFilter)
                                Else
                                    Dim mdtTable As DataTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                        "List", True, StrFilter).Tables(0)
                                    drQualification = mdtTable.Select("1=1")
                                End If
                                'Pinkal (20-Oct-2020) -- End

                                If drQualification.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                            End If

                            If mdtFilter.Rows(j)("other_resultcode").ToString().Length > 0 Then

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                                Else
                                    StrFilter = "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid
                                End If
                                'Pinkal (20-Oct-2020) -- End

                                StrSplit = mdtFilter.Rows(j)("other_resultcode").ToString().Trim.Split(CChar(","))
                                If StrSplit.Length > 0 Then
                                    For iSplit As Integer = 0 To StrSplit.Length - 1
                                        Select Case iSplit
                                            Case 0
                                                StrFilter &= " AND (other_resultcode like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                            Case Else
                                                StrFilter &= " OR other_resultcode like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                        End Select
                                    Next
                                    StrFilter &= ")"
                                End If

                                    Dim drResultcode As DataRow() = Nothing


                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                        drResultcode = objApplicantQulaification._DataList.Select(StrFilter)
                                Else
                                    Dim mdtTable As DataTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                        "List", True, StrFilter).Tables(0)
                                    drResultcode = mdtTable.Select("1=1")
                                End If
                                'Pinkal (20-Oct-2020) -- End

                                If drResultcode.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                        End If
                            'Sohail (20 Mar 2020) -- End

                        If CInt(mdtFilter.Rows(j)("resultunkid")) > 0 Then

                            Dim mstrCondition As String = mdtFilter.Rows(j)("resultcondition").ToString()

                            If mstrCondition.Trim = "" Then mstrCondition = "="


                                'Pinkal (10-Dec-2012) -- Start
                                'Enhancement : TRA Changes


                                Dim mstrFilter As String = ""
                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                                '    'Hemant (07 Oct 2019) -- Start
                                '    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                '    'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                                '    mstrFilter = " AND  qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                                '    'Hemant (07 Oct 2019) -- End
                                'End If

                                'If mdtFilter.Rows(j)("qualificationunkid").ToString.Trim.Length > 0 Then
                                '    mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString.Trim & ")"
                                'End If
                                'Sohail (20 Mar 2020) -- End

                                Dim drResultcode As DataRow() = Nothing

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                If mstrFilter.Trim.Length > 0 Then
                                    drResultcode = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND result_level  " & mstrCondition & _
                                                                                  " " & CInt(mdtFilter.Rows(j)("result_level")) & " " & mstrFilter)
                                Else
                                    drResultcode = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND result_level  " & mstrCondition & _
                                                                          " " & CInt(mdtFilter.Rows(j)("result_level")))
                                End If
                                Else
                                    Dim mdtTable As DataTable = Nothing
                                    If mstrFilter.Trim.Length > 0 Then

                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                               "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND result_level  " & mstrCondition & _
                                                                                      " " & CInt(mdtFilter.Rows(j)("result_level")) & " " & mstrFilter).Tables(0)

                                    Else
                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                                 "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND result_level  " & mstrCondition & _
                                                                                        " " & CInt(mdtFilter.Rows(j)("result_level"))).Tables(0)
                                    End If

                                    drResultcode = mdtTable.Select("1=1")
                                End If
                                'Pinkal (20-Oct-2020) -- End`


                                'Pinkal (10-Dec-2012) -- End


                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drResultcode.Length = 0 Then Continue For
                                If drResultcode.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                        End If

                        If CDec(mdtFilter.Rows(j)("gpacode")) > 0 Then

                            Dim mstrCondition As String = mdtFilter.Rows(j)("gpacondition").ToString()

                            If mstrCondition.Trim = "" Then mstrCondition = "="


                                'Pinkal (10-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim mstrFilter As String = ""
                                If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                                    'Hemant (07 Oct 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                    'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))

                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    mstrFilter = " AND  qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                                    Else
                                        mstrFilter = " AND  hremp_qualification_tran.qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                                    End If
                                    'Pinkal (20-Oct-2020) -- End
                                    'Hemant (07 Oct 2019) -- End
                                End If

                                If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then

                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString.Trim & ")"
                                    Else
                                        mstrFilter &= " AND  hremp_qualification_tran.qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString.Trim & ")"
                                    End If
                                    'Pinkal (20-Oct-2020) -- End

                                End If

                                Dim drGPACode As DataRow() = Nothing

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                If mstrFilter.Trim.Length > 0 Then
                                    drGPACode = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND gpacode " & mstrCondition & _
                                                                                   " " & CDec(mdtFilter.Rows(j)("gpacode")) & " " & mstrFilter)
                                Else
                                    drGPACode = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND gpacode " & mstrCondition & _
                                                                           " " & CDec(mdtFilter.Rows(j)("gpacode")))
                                End If
                                Else
                                    Dim mdtTable As DataTable = Nothing
                                    If mstrFilter.Trim.Length > 0 Then

                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                               "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND hremp_qualification_tran.gpacode  " & mstrCondition & _
                                                                                      " " & CDec(mdtFilter.Rows(j)("gpacode")) & " " & mstrFilter).Tables(0)

                                    Else
                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                                 "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND hremp_qualification_tran.gpacode  " & mstrCondition & _
                                                                                          " " & CDec(mdtFilter.Rows(j)("gpacode"))).Tables(0)
                                    End If

                                    drGPACode = mdtTable.Select("1=1")
                                End If

                                'Pinkal (10-Dec-2012) -- End

                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drGPACode.Length = 0 Then Continue For
                                If drGPACode.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                        End If


                        If CInt(mdtFilter.Rows(j)("award_year")) > 0 Then

                            Dim mstrCondition As String = mdtFilter.Rows(j)("yearcondition").ToString()

                            If mstrCondition.Trim = "" Then mstrCondition = "="


                                'Pinkal (10-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim mstrFilter As String = ""
                                If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                                    'Hemant (07 Oct 2019) -- Start
                                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                    'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))

                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    mstrFilter = " AND  qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                                    Else
                                        mstrFilter = " AND  hremp_qualification_tran.qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString().Trim & ")"
                                    End If
                                    'Pinkal (20-Oct-2020) -- End
                                    'Hemant (07 Oct 2019) -- End
                        End If

                                If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                                    Else
                                        mstrFilter &= " AND  hremp_qualification_tran.qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString.Trim & ")"
                                    End If
                                    'Pinkal (20-Oct-2020) -- End
                                End If

                                Dim drAwadYear As DataRow() = Nothing

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                If mstrFilter.Trim.Length > 0 Then
                                    drAwadYear = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND award_end_Year  " & mstrCondition & _
                                                                            " " & CInt(mdtFilter.Rows(j)("award_year")) & " " & mstrFilter)
                                Else
                                    drAwadYear = objApplicantQulaification._DataList.Select("applicantunkid = " & objApplicantQulaification._ApplicantUnkid & "AND award_end_Year  " & mstrCondition & _
                                                                            " " & CInt(mdtFilter.Rows(j)("award_year")))
                                End If
                                Else
                                    Dim mdtTable As DataTable = Nothing
                                    If mstrFilter.Trim.Length > 0 Then

                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                               "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND award_end_Year  " & mstrCondition & _
                                                                                      " " & CDec(mdtFilter.Rows(j)("award_year")) & " " & mstrFilter).Tables(0)

                                    Else
                                        mdtTable = objEmpQulaification.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                                                                 "List", True, "hremp_qualification_tran.employeeunkid = " & objEmpQulaification._Employeeunkid & "AND award_end_Year  " & mstrCondition & _
                                                                                          " " & CDec(mdtFilter.Rows(j)("award_year"))).Tables(0)
                                    End If

                                    drAwadYear = mdtTable.Select("1=1")
                                End If

                                'Pinkal (10-Dec-2012) -- End

                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If drAwadYear.Length = 0 Then Continue For
                                If drAwadYear.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                                'Sohail (20 Mar 2020) -- End
                            End If


                        If mdtFilter.Rows(j)("other_skillcategory").ToString().Length > 0 Then
                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'Dim StrFilter As String = String.Empty
                                'Dim StrSplit() As String = Nothing
                                'Sohail (20 Mar 2020) -- End

                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                            StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                                Else
                                    StrFilter = "hremp_app_skills_tran.emp_app_unkid = " & objEmpSkilltran._Emp_App_Unkid
                                End If

                            StrSplit = mdtFilter.Rows(j)("other_skillcategory").ToString().Trim.Split(CChar(","))
                            If StrSplit.Length > 0 Then
                                For iSplit As Integer = 0 To StrSplit.Length - 1
                                    Select Case iSplit
                                        Case 0
                                            StrFilter &= " AND (other_skillcategory like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                        Case Else
                                            StrFilter &= " OR other_skillcategory like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                    End Select
                                Next
                                StrFilter &= ")"
                            End If


                                'Pinkal (10-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim mstrFilter As String = ""

                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                                '    'Hemant (07 Oct 2019) -- Start
                                '    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                '    'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                                '    mstrFilter = " AND  qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString.Trim & ")"
                                '    'Hemant (07 Oct 2019) -- End
                                'End If 

                                'If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                                '    mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                                'End If
                                'Sohail (20 Mar 2020) -- End

                                Dim drSkillCategory As DataRow() = Nothing

                                If mstrFilter.Trim.Length > 0 Then
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'drSkillCategory = objApplicantQulaification._DataList.Select(StrFilter & " " & mstrFilter)


                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkillCategory = objApplicantSkill._DataList.Select(StrFilter & " " & mstrFilter)
                                    Else
                                        Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                        "List", True, StrFilter & " " & mstrFilter).Tables(0)

                                        drSkillCategory = mdtTable.Select("1=1")
                                    End If
                                    'Pinkal (20-Oct-2020) -- End


                                    'Sohail (20 Mar 2020) -- End
                                Else
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'drSkillCategory = objApplicantQulaification._DataList.Select(StrFilter)


                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkillCategory = objApplicantSkill._DataList.Select(StrFilter)
                                    Else
                                        Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                      "List", True, StrFilter).Tables(0)

                                        drSkillCategory = mdtTable.Select("1=1")
                                    End If
                                    'Pinkal (20-Oct-2020) -- End

                                    'Sohail (20 Mar 2020) -- End
                                End If

                                'Pinkal (10-Dec-2012) -- End


                            If drSkillCategory.Length = 0 Then
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'If drSkillCategory.Length = 0 Then Continue For
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                    'Sohail (20 Mar 2020) -- End
                        End If

                        End If

                        If mdtFilter.Rows(j)("other_skill").ToString().Length > 0 Then
                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'Dim StrFilter As String = String.Empty
                                'Dim StrSplit() As String = Nothing
                                'Sohail (20 Mar 2020) -- End


                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                            StrFilter = "applicantunkid = " & objApplicantQulaification._ApplicantUnkid
                                Else
                                    StrFilter = "hremp_app_skills_tran.emp_app_unkid = " & objEmpSkilltran._Emp_App_Unkid
                                End If
                                'Pinkal (20-Oct-2020) -- End


                            StrSplit = mdtFilter.Rows(j)("other_skill").ToString().Trim.Split(CChar(","))
                            If StrSplit.Length > 0 Then
                                For iSplit As Integer = 0 To StrSplit.Length - 1
                                    Select Case iSplit
                                        Case 0
                                            StrFilter &= " AND (other_skill like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                        Case Else
                                            StrFilter &= " OR other_skill like '%" & StrSplit(iSplit).ToString.Trim & "%'"
                                    End Select
                                Next
                                StrFilter &= ")"
                            End If


                                'Pinkal (10-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim mstrFilter As String = ""

                                'Sohail (20 Mar 2020) -- Start
                                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                'If CInt(mdtFilter.Rows(j)("qualificationgroupunkid")) > 0 Then
                                '    'Hemant (07 Oct 2019) -- Start
                                '    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                                '    'mstrFilter = " AND  qualificationgroupunkid = " & CInt(mdtFilter.Rows(j)("qualificationgroupunkid"))
                                '    mstrFilter = " AND  qualificationgroupunkid in (" & mdtFilter.Rows(j)("qualificationgroupunkid").ToString.Trim & ")"
                                '    'Hemant (07 Oct 2019) -- End
                                'End If

                                'If mdtFilter.Rows(j)("qualificationunkid").ToString().Trim.Length > 0 Then
                                '    mstrFilter &= " AND  qualificationunkid in (" & mdtFilter.Rows(j)("qualificationunkid").ToString().Trim & ")"
                                'End If
                                'Sohail (20 Mar 2020) -- End

                                Dim drSkill As DataRow() = Nothing

                                If mstrFilter.Trim.Length > 0 Then
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'drSkill = objApplicantQulaification._DataList.Select(StrFilter & " " & mstrFilter)


                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkill = objApplicantSkill._DataList.Select(StrFilter & " " & mstrFilter)
                                    Else
                                        Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                  "List", True, StrFilter & " " & mstrFilter).Tables(0)
                                        drSkill = mdtTable.Select("1=1")
                                    End If
                                    'Pinkal (20-Oct-2020) -- End


                                    'Sohail (20 Mar 2020) -- End
                                Else
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'drSkill = objApplicantQulaification._DataList.Select(StrFilter)

                                    'Pinkal (20-Oct-2020) -- Start
                                    'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                    If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkill = objApplicantSkill._DataList.Select(StrFilter)
                                    Else
                                        Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                 "List", True, StrFilter).Tables(0)
                                        drSkill = mdtTable.Select("1=1")
                                    End If
                                    'Pinkal (20-Oct-2020) -- End


                                    'Sohail (20 Mar 2020) -- End
                        End If

                                'Pinkal (10-Dec-2012) -- End



                                If drSkill.Length = 0 Then
                                    'Sohail (20 Mar 2020) -- Start
                                    'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                                    '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                                    'If drSkill.Length = 0 Then Continue For
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                    'Sohail (20 Mar 2020) -- End
                                End If
                            End If

                            'Sohail (20 Mar 2020) -- Start
                            'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                            '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                            If mdtFilter.Rows(j)("skillcategoryunkid").ToString().Trim.Length > 0 Then

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                Dim drSkillCategory As DataRow() = Nothing
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkillCategory = objApplicantSkill._DataList.Select("applicantunkid = " & objApplicantSkill._ApplicantUnkid & " AND skillcategoryunkid in (" & mdtFilter.Rows(j)("skillcategoryunkid").ToString() & ")")
                                Else
                                    Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                                  "List", True, "hremp_app_skills_tran.emp_app_unkid = " & objEmpSkilltran._Emp_App_Unkid & " AND skillcategoryunkid in (" & mdtFilter.Rows(j)("skillcategoryunkid").ToString() & ")").Tables(0)

                                    drSkillCategory = mdtTable.Select("1=1")
                                End If

                                'Pinkal (20-Oct-2020) -- End



                                If drSkillCategory.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                            End If

                            If mdtFilter.Rows(j)("skillunkid").ToString().Trim.Length > 0 Then

                                'Pinkal (20-Oct-2020) -- Start
                                'Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB.
                                Dim drSkill As DataRow() = Nothing
                                If CInt(ds.Tables(0).Rows(i)("employeeunkid")) <= 0 Then
                                    drSkill = objApplicantSkill._DataList.Select("applicantunkid = " & objApplicantSkill._ApplicantUnkid & " AND skillunkid in (" & mdtFilter.Rows(j)("skillunkid").ToString() & ")")
                                Else
                                    Dim mdtTable As DataTable = objEmpSkilltran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, _
                                                                               "List", True, "hremp_app_skills_tran.emp_app_unkid = " & objEmpSkilltran._Emp_App_Unkid & " AND skillunkid in (" & mdtFilter.Rows(j)("skillunkid").ToString() & ")").Tables(0)

                                    drSkill = mdtTable.Select("1=1")
                                End If
                                'Pinkal (20-Oct-2020) -- End

                                If drSkill.Length = 0 Then
                                    If arrRemoveApplicant.Contains(CInt(ds.Tables(0).Rows(i)("applicantunkid"))) = False Then
                                        arrRemoveApplicant.Add(CInt(ds.Tables(0).Rows(i)("applicantunkid")))
                                    End If
                                    Continue For
                                End If
                            End If
                            'Sohail (20 Mar 2020) -- End

                        ds.Tables(0).Rows(i)("GUID") = mdtFilter.Rows(j)("GUID").ToString()

                        dsFilterApplicant.Tables(0).ImportRow(ds.Tables(0).Rows(i))

                        j += 1

                        Next
                    Next

                Else

                    If dsFilterApplicant.Tables(0).Rows.Count = 0 Then
                        dsFilterApplicant = ds
                    Else
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            dsFilterApplicant.Tables(0).ImportRow(ds.Tables(0).Rows(i))
                        Next
                    End If

                End If

                'Sohail (20 Mar 2020) -- Start
                'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
                '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
                If arrRemoveApplicant IsNot Nothing AndAlso arrRemoveApplicant.Count > 0 Then
                    For Each strID As String In arrRemoveApplicant
                        Dim d_r() As DataRow = dsFilterApplicant.Tables(0).Select("applicantunkid = " & CInt(strID) & " ")
                        If d_r.Length > 0 Then
                            For Each r As DataRow In d_r
                                dsFilterApplicant.Tables(0).Rows.Remove(r)
                            Next
                            dsFilterApplicant.Tables(0).AcceptChanges()
                        End If
                    Next
                End If
                'Sohail (20 Mar 2020) -- End
                FillApplicant(dsFilterApplicant)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetANDFilter", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Private Function GetQualification(ByVal IsName As Boolean) As String
        Dim strFilter As String = ""
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvQualification.CheckedItems.Count > 0 Then
            '    For i As Integer = 0 To lvQualification.CheckedItems.Count - 1
            '        If IsName Then
            '            strFilter &= "," & lvQualification.CheckedItems(i).Text
            '        Else
            '            strFilter &= "," & lvQualification.CheckedItems(i).Tag.ToString()
            '        End If
            '    Next
            'End If
            'If strFilter.Trim.Length > 0 Then
            '    strFilter = strFilter.Substring(1, strFilter.Length - 1)
            'End If
            If dgvQualification.RowCount > 0 Then
                mdtQualification.AcceptChanges()
                If mdtQualification.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    If IsName Then
                        strFilter = String.Join(",", mdtQualification.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("Name")).ToArray())
                    Else
                        strFilter = String.Join(",", mdtQualification.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) Convert.ToString(y.Field(Of Integer)("Id"))).ToArray())
                    End If
            End If
            End If
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualification", mstrModuleName)
        End Try
        Return strFilter
    End Function

    Private Sub GetResultCode()
        Try
            Dim mstrQualificationId As String = "-1"
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvQualification.CheckedItems.Count > 0 Then
            '    mstrQualificationId = ""
            '    For i As Integer = 0 To lvQualification.CheckedItems.Count - 1
            '        mstrQualificationId &= lvQualification.CheckedItems(i).Tag.ToString() & ","
            '    Next

            '    If mstrQualificationId.Trim.Length > 0 Then mstrQualificationId = mstrQualificationId.Trim.Substring(0, mstrQualificationId.Trim.Length - 1)
            'End If
            If dgvQualification.RowCount > 0 Then
                mdtQualification.AcceptChanges()
                If mdtQualification.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    mstrQualificationId = String.Join(",", mdtQualification.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) Convert.ToString(y.Field(Of Integer)("Id"))).ToArray())
            End If
            End If
            'S.SANDEEP [27-SEP-2017] -- END

            Dim objQualification As New clsqualification_master
            Dim dsList As DataSet = Nothing
            dsList = objQualification.GetResultCodeFromQualification(mstrQualificationId.Trim)
            cboResultCode.DisplayMember = "resultname"
            cboResultCode.ValueMember = "resultunkid"
            cboResultCode.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetResultCode", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Oct-2011) -- End



    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Public Sub GenerateAwardYear()
        Try
            cboAwardedYear.Items.Add(Language.getMessage(mstrModuleName, 12, "Select"))
            For i As Integer = 0 To 60
                cboAwardedYear.Items.Add(ConfigParameter._Object._CurrentDateAndTime.Year - i)
            Next
            cboAwardedYear.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateAwardYear", mstrModuleName)
        End Try
    End Sub
    'Pinkal (06-Feb-2012) -- End

    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 11 : On shortlisting screen, after exporting, provide option to clear the criteria instead of closing the screen and opening again)
    Private Sub ResetValue()
        Try
            lvApplicant.Items.Clear()
            objchkAll.CheckState = CheckState.Unchecked
            cboVacancyType.Enabled = True
            cboVacancyType.SelectedIndex = 0
            cboVacancy.Enabled = True
            cboVacancy.SelectedIndex = 0
            gbShortListedApplicant.Text = CStr(gbShortListedApplicant.Tag)
            dsFilterApplicant = Nothing
            'Sohail (13 May 2020) -- Start
            'NMB Issue # : On the shortlisting screen,  when you do multiple shortlists and reset the screen then do final save and export, the shortlists that were reset without saving are appearing on the shortlisting report exported.
            mdtFilter.Rows.Clear()
            Call FillShortListFilter()
            'Sohail (13 May 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub
    'Hemant (27 Sep 2019) -- End

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Private Sub FillQualificationGroup()
        Dim strSearch As String = String.Empty
        Dim objMaster As New clsCommon_Master
        Dim dsList As DataSet
        Try

            If CInt(cboQualificationGrpCondition.SelectedValue) > 0 Then
                strSearch = " AND qlevel " & cboQualificationGrpCondition.Text & " " & CInt(nudLevel.Value)
                dsList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
            Else
                If CInt(nudLevel.Value) > -1 Then
                    strSearch = " AND qlevel = " & CInt(nudLevel.Value)
                    dsList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
                Else
                    dsList = objMaster.GetQualificationGrpFromLevel(CInt(nudLevel.Value), False)
                End If
            End If
            chkQualificationGroup.Checked = False
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            mdtQualificationGroup = dsList.Tables(0).Copy()
            dvQualificationGroup = mdtQualificationGroup.DefaultView
            For Each dr As DataRowView In dvQualificationGroup
                If CInt(dr("masterunkid")) = CInt(cboQualificationGrp.SelectedValue) Then
                    dr("ischeck") = "True"
                End If
            Next
            dgvQualificationGroup.AutoGenerateColumns = False
            objdgcolhQlGrpCheck.DataPropertyName = "ischeck"
            dgcolhQlGrpName.DataPropertyName = "Name"
            objdgcolhQlGrpId.DataPropertyName = "Id"
            dgcolhQLevel.DataPropertyName = "qlevel" 'Hemant (30 Oct 2019)
            dgvQualificationGroup.DataSource = dvQualificationGroup

            EnableControl()
            dgvQualificationGroup.Visible = True
            chkQualificationGroup.Visible = True
            FillQualification()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillQualificationGroup", mstrModuleName)
        End Try
    End Sub

    Private Function GetQualificationGroup(ByVal IsName As Boolean) As String
        Dim strFilter As String = ""
        Try
            If dgvQualificationGroup.RowCount > 0 Then
                mdtQualificationGroup.AcceptChanges()
                If mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    If IsName Then
                        strFilter = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("Name")).ToArray())
                    Else
                        strFilter = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) Convert.ToString(y.Field(Of Integer)("masterunkid"))).ToArray())
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualificationGroup", mstrModuleName)
        End Try
        Return strFilter
    End Function
    'Hemant (07 Oct 2019) -- End

#End Region

#Region " Form's Event(s) "

    Private Sub frmApplicantFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvQualification.GridLines = False
            'lvApplicant.GridLines = False
            'lvSkillSet.GridLines = False
            lvApplicant.GridLines = False
            'S.SANDEEP [27-SEP-2017] -- END
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            cboQualificationGrp.Visible = False
            dgvQualificationGroup.Visible = False
            cboQualificationGroupCondition.Enabled = False
            'Hemant (07 Oct 2019) -- End
            FillCombo()

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            GenerateAwardYear()
            'Pinkal (06-Feb-2012) -- End

            'Hemant (30 Oct 2019) -- Start
            nudLevel.Enabled = False
            cboQualificationGrp.Enabled = False
            'Hemant (30 Oct 2019) -- End
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
            nudLevel.Value = -1
            'Hemant (27 Sep 2019) -- End

            nudLevel_ValueChanged(sender, e)
            objShortListMst = New clsshortlist_master
            objShortFilter = New clsshortlist_filter
            mdtFilter = objShortFilter._DtFilter
            EnableControl()

            'S.SANDEEP [ 05 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            gbShortListedApplicant.Tag = gbShortListedApplicant.Text
            'S.SANDEEP [ 05 NOV 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantFilter_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsshortlist_master.SetMessages()
            clsshortlist_filter.SetMessages()
            objfrm._Other_ModuleNames = "clsshortlist_master,clsshortlist_filter"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            If validation() Then


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                Dim mstrQualificationID As String = GetQualification(False)
                Dim mstrQualification As String = GetQualification(True)

                'Pinkal (12-Oct-2011) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                Dim mstrQualificationGroupID As String = GetQualificationGroup(False)
                Dim mstrQualificationGroup As String = GetQualificationGroup(True)
                'Hemant (07 Oct 2019) -- End

                Dim mstrSkillCategroyID As String = GetSkillCategory(False)
                Dim mstrSkillCategroy As String = GetSkillCategory(True)
                Dim mstrSkillID As String = GetSkill(False)
                Dim mstrSkill As String = GetSkill(True)

                Dim mstrFilter As String = ""

                If CInt(cboVacancy.SelectedValue) > 0 Then
                    mstrFilter = " vacancyunkid = " & CInt(cboVacancy.SelectedValue)
                End If


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes

                'If chkOtherQualification.Checked = False Then 'Sohail (20 Mar 2020)
                    'S.SANDEEP [ 05 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND qlevel=" & CInt(nudLevel.Value)
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                    'If CInt(cboQualificationGrp.SelectedValue) > 0 Or CInt(nudLevel.Value) > 0 Then
                    If mstrQualificationGroupID.Trim.Length > 0 Or CInt(nudLevel.Value) > 0 Then
                        'Hemant (07 Oct 2019) -- End
                        'Hemant (27 Sep 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
                        If CInt(nudLevel.Value) > -1 Then
                            'Hemant (27 Sep 2019) -- End
                            'Hemant (07 Oct 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                            'mstrFilter &= " AND qlevel=" & CInt(nudLevel.Value)
                            mstrFilter &= " AND qlevel in (" & mstrQLevel & ")"
                            'Hemant (07 Oct 2019) -- End
                        End If 'Hemant (27 Sep 2019) 
                End If
                    'S.SANDEEP [ 05 NOV 2012 ] -- END
                'End If 'Sohail (20 Mar 2020)

                'Pinkal (5-MAY-2012) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                'If CInt(cboQualificationGrp.SelectedValue) > 0 Then
                '    mstrFilter &= " AND qualificationgroupunkid = " & CInt(cboQualificationGrp.SelectedValue)
                'End If

                If mstrQualificationGroupID.Trim.Length > 0 Then
                    'Hemant (30 Oct 2019) -- Start
                    If CInt(cboQualificationGroupCondition.SelectedValue) > 0 AndAlso CInt(cboQualificationGroupCondition.SelectedValue) <> CInt(enComparison_Operator.EQUAL) Then
                        Dim strTempFilter As String = String.Empty
                        'Hemant (12 Nov 2019) -- Start
                        'Dim arrQualificationGroupID() As String = Split(mstrQualificationGroupID, ",").Distinct.ToArray
                        'mstrFilter &= " AND ( #CONDITION# ) "
                        'For Each str As String In arrQualificationGroupID

                        '    If strTempFilter.Trim.Length > 0 Then
                        '        strTempFilter &= " OR "
                        '    End If
                        '    strTempFilter &= "( qualificationgroupunkid " & cboQualificationGroupCondition.Text & " '" & CInt(str) & "')"
                        'Next
                        'mstrFilter = mstrFilter.Replace("#CONDITION#", strTempFilter)

                        Dim strQualificationGroupIds As String = ""
                        Dim strSearch As String = String.Empty
                        Dim dsQualificationGroupList As DataSet
                        Dim objMaster As New clsCommon_Master
                        Dim strQuaLevels As String = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("qlevel").ToString()).ToArray())

                        Dim arrQuaLevels() As String = Split(strQuaLevels, ",")
                        strSearch = " AND ( #CONDITION# ) "
                        For Each str As String In arrQuaLevels
                            If strTempFilter.Trim.Length > 0 Then
                                strTempFilter &= " OR "
                            End If
                            strTempFilter &= "( qlevel " & cboQualificationGroupCondition.Text & " " & CInt(str) & ")"
                        Next
                        strSearch = strSearch.Replace("#CONDITION#", strTempFilter)
                        dsQualificationGroupList = objMaster.GetQualificationGrpFromLevel(-1, False, strSearch)
                        strQualificationGroupIds = String.Join(",", dsQualificationGroupList.Tables(0).AsEnumerable().Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())
                        mstrFilter &= " AND qualificationgroupunkid IN ( " & strQualificationGroupIds & ")"
                        'Hemant (12 Nov 2019) -- End
                    Else
                        'Hemant (30 Oct 2019) -- End
                    mstrFilter &= " AND qualificationgroupunkid IN ( " & mstrQualificationGroupID & ")"
                End If
                End If
                'Hemant (07 Oct 2019) -- End



                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                'If CInt(cboQualification.SelectedValue) > 0 Then
                '    mstrFilter &= " AND qualificationunkid = " & CInt(cboQualification.SelectedValue)
                'End If

                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 17
                'If lvQualification.CheckedItems.Count > 0 Then
                '    mstrFilter &= " AND qualificationunkid = '" & mstrQualificationID & "'"
                'End If
                If mstrQualificationID.Trim.Length > 0 Then
                    mstrFilter &= " AND qualificationunkid = '" & mstrQualificationID & "'"
                End If
                'S.SANDEEP [27-SEP-2017] -- END

                'Pinkal (12-Oct-2011) -- End

                If CInt(cboResultCode.SelectedValue) > 0 Then
                    mstrFilter &= " AND resultunkid = " & CInt(cboResultCode.SelectedValue)
                End If

                If txtResultLevel.Text.Trim.Length > 0 AndAlso CInt(txtResultLevel.Text.Trim) > 0 Then
                    mstrFilter &= " AND result_level = " & CInt(txtResultLevel.Text.Trim)
                End If

                If CInt(cboResultLvlCondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND result_lvl_condition = " & CInt(cboResultLvlCondition.SelectedValue)
                End If

                If CDec(nudGPA.Value) > 0 Then
                    mstrFilter &= " AND gpacode = " & CDec(nudGPA.Value)
                End If

                If CInt(cboGPACondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND gpacode_condition = " & CInt(cboGPACondition.SelectedValue)
                End If

                If CInt(cboGPACondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND age = " & CInt(nudAge.Value)
                End If

                If CInt(cboAgeCondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND age_condition = " & CInt(cboAgeCondition.SelectedValue)
                End If

                If CInt(cboGender.SelectedValue) > 0 Then
                    mstrFilter &= " AND gender = " & CInt(cboGender.SelectedValue)
                End If

                If mstrSkillCategroyID.Trim.Length > 0 Then
                    mstrFilter &= " AND skillcategoryunkid = '" & mstrSkillCategroyID & "'"
                End If

                If mstrSkillID.Trim.Length > 0 Then
                    mstrFilter &= " AND skillunkid ='" & mstrSkillID & "'"
                End If

                'Hemant (02 Jul 2019) -- Start
                'ENHANCEMENT :  ZRA minimum Requirements
                If CInt(cboNationality.SelectedValue) > 0 Then
                    mstrFilter &= " AND nationalityunkid = " & CInt(cboNationality.SelectedValue)
                End If
                'Hemant (02 Jul 2019) -- End

                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                If CInt(cboAwardedYear.SelectedIndex) > 0 Then
                    mstrFilter &= " AND award_year = " & CInt(cboAwardedYear.Text)
                End If

                If CInt(cboYearCondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND year_condition = " & CInt(cboYearCondition.SelectedValue)
                End If

                'Pinkal (06-Feb-2012) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                If CInt(nudExperienceYears.Value) > 0 OrElse CInt(nudExperienceMonths.Value) > 0 Then
                    mstrFilter &= " AND experience_days = " & ((CInt(nudExperienceYears.Value) * 365) + (CInt(nudExperienceMonths.Value * 30)))
                End If

                If CInt(cboExperienceCondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND experience_condition = " & CInt(cboExperienceCondition.SelectedValue)
                End If
                'Hemant (07 Oct 2019) -- End

                'Hemant (30 Oct 2019) -- Start
                If CInt(cboQualificationGroupCondition.SelectedValue) > 0 Then
                    mstrFilter &= " AND QualificationGroup_condition = " & CInt(cboQualificationGroupCondition.SelectedValue)
                End If
                'Hemant (30 Oct 2019) -- End

                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes

                If txtOtherQualificationGrp.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND other_qualificationgrp like '%" & txtOtherQualificationGrp.Text.Trim & "%'"
                    Dim StrSplit() As String = txtOtherQualificationGrp.Text.Trim.Split(CChar(","))
                    If StrSplit.Length > 0 Then
                        For i As Integer = 0 To StrSplit.Length - 1
                            Select Case i
                                Case 0
                                    mstrFilter &= " AND (other_qualificationgrp like '%" & StrSplit(i).ToString.Trim & "%'"
                                Case Else
                                    mstrFilter &= " OR other_qualificationgrp like '%" & StrSplit(i).ToString.Trim & "%'"
                            End Select
                        Next
                        mstrFilter &= ")"
                    Else
                    mstrFilter &= " AND other_qualificationgrp like '%" & txtOtherQualificationGrp.Text.Trim & "%'"
                End If
                    'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                If txtOtherQualification.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND other_qualification like '%" & txtOtherQualification.Text.Trim & "%'"
                    Dim StrSplit() As String = txtOtherQualification.Text.Trim.Split(CChar(","))
                    If StrSplit.Length > 0 Then
                        For i As Integer = 0 To StrSplit.Length - 1
                            Select Case i
                                Case 0
                                    mstrFilter &= " AND (other_qualification like '%" & StrSplit(i).ToString.Trim & "%'"
                                Case Else
                                    mstrFilter &= " OR other_qualification like '%" & StrSplit(i).ToString.Trim & "%'"
                            End Select
                        Next
                        mstrFilter &= ")"
                    Else
                        mstrFilter &= " AND other_qualification like '%" & txtOtherQualificationGrp.Text.Trim & "%'"
                    End If
                    'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                If txtOtherResultCode.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND other_resultcode like '%" & txtOtherResultCode.Text.Trim & "%'"
                    Dim StrSplit() As String = txtOtherResultCode.Text.Trim.Split(CChar(","))
                    If StrSplit.Length > 0 Then
                        For i As Integer = 0 To StrSplit.Length - 1
                            Select Case i
                                Case 0
                                    mstrFilter &= " AND (other_resultcode like '%" & StrSplit(i).ToString.Trim & "%'"
                                Case Else
                                    mstrFilter &= " OR other_resultcode like '%" & StrSplit(i).ToString.Trim & "%'"
                            End Select
                        Next
                        mstrFilter &= ")"
                    Else
                    mstrFilter &= " AND other_resultcode like '%" & txtOtherResultCode.Text.Trim & "%'"
                End If
                    'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                If txtOtherSkillCategory.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND other_skillcategory like '%" & txtOtherSkillCategory.Text.Trim & "%'"
                    Dim StrSplit() As String = txtOtherSkillCategory.Text.Trim.Split(CChar(","))
                    If StrSplit.Length > 0 Then
                        For i As Integer = 0 To StrSplit.Length - 1
                            Select Case i
                                Case 0
                                    mstrFilter &= " AND (other_skillcategory like '%" & StrSplit(i).ToString.Trim & "%'"
                                Case Else
                                    mstrFilter &= " OR other_skillcategory like '%" & StrSplit(i).ToString.Trim & "%'"
                            End Select
                        Next
                        mstrFilter &= ")"
                    Else
                    mstrFilter &= " AND other_skillcategory like '%" & txtOtherSkillCategory.Text.Trim & "%'"
                End If
                    'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If

                If txtOtherSkill.Text.Trim.Length > 0 Then
                    'S.SANDEEP [ 07 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mstrFilter &= " AND other_skill like '%" & txtOtherSkill.Text.Trim & "%'"
                    Dim StrSplit() As String = txtOtherSkillCategory.Text.Trim.Split(CChar(","))
                    If StrSplit.Length > 0 Then
                        For i As Integer = 0 To StrSplit.Length - 1
                            Select Case i
                                Case 0
                                    mstrFilter &= " AND (other_skill like '%" & StrSplit(i).ToString.Trim & "%'"
                                Case Else
                                    mstrFilter &= " OR other_skill like '%" & StrSplit(i).ToString.Trim & "%'"
                            End Select
                        Next
                        mstrFilter &= ")"
                    Else
                        mstrFilter &= " AND other_skill like '%" & txtOtherSkillCategory.Text.Trim & "%'"
                    End If
                    'S.SANDEEP [ 07 NOV 2012 ] -- END
                End If


                'Pinkal (5-MAY-2012) -- End



                If mdtFilter.Columns.Contains("filter") = False Then
                    mdtFilter.Columns.Add("filter", Type.GetType("System.String"))
                End If

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                If mdtFilter.Columns.Contains("IsApply") = False Then
                    mdtFilter.Columns.Add("IsApply", Type.GetType("System.Boolean"))
                End If

                'Pinkal (12-Oct-2011) -- End


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes

                'Dim dr As DataRow() = mdtFilter.Select(mstrFilter & " AND  AUD <> 'D'")
                Dim dr As DataRow() = mdtFilter.Select(mstrFilter & " AND  AUD <> 'D' AND IsApply = False")
                'Pinkal (06-Feb-2012) -- End


                If dr.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This Filer criteria is already exist in below List.Please define new filter."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    btnAdd.Select()
                    Exit Sub
                End If

                Dim drRow As DataRow = mdtFilter.NewRow
                drRow("filterunkid") = -1
                drRow("shortlistunkid") = -1
                drRow("vacancyunkid") = CInt(cboVacancy.SelectedValue)
                drRow("vacancy") = IIf(CInt(cboVacancy.SelectedValue) > 0, cboVacancy.Text, "")


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes

                'If chkOtherQualification.Checked = False Then 'Sohail (20 Mar 2020)
                    'S.SANDEEP [ 05 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'drRow("qlevel") = CInt(nudLevel.Value)
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                    'If CInt(cboQualificationGrp.SelectedValue) > 0 Or CInt(nudLevel.Value) > 0 Then
                    '    drRow("qlevel") = CInt(nudLevel.Value)
                    'End If
                    If mstrQualificationGroupID.Trim.Length > 0 Or CInt(nudLevel.Value) > 0 Then
                        'Hemant (07 Oct 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                        'drRow("qlevel") = CInt(nudLevel.Value)
                        drRow("qlevel") = mstrQLevel
                        'Hemant (07 Oct 2019) -- End
                End If
                    'Hemant (07 Oct 2019) -- End
                    'S.SANDEEP [ 05 NOV 2012 ] -- END
                'End If 'Sohail (20 Mar 2020)

                'Pinkal (5-MAY-2012) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                'drRow("qualificationgroupunkid") = CInt(cboQualificationGrp.SelectedValue)
                'drRow("qualificationgrp") = IIf(CInt(cboQualificationGrp.SelectedValue) > 0, cboQualificationGrp.Text, "")
                drRow("qlevel_condition") = CInt(cboQualificationGrpCondition.SelectedValue)
                'Hemant (30 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
                'drRow("qualificationgroupunkid") = mstrQualificationGroupID
                drRow("qualificationgroupunkid") = IIf(mstrQualificationGroupID.Trim.Length > 0, mstrQualificationGroupID, "0")
                'Hemant (30 Oct 2019) --
                drRow("qualificationgrp") = mstrQualificationGroup
                'Hemant (07 Oct 2019) -- End

                'Hemant (30 Oct 2019) -- Start
                drRow("qualificationgroup_condition") = CInt(cboQualificationGroupCondition.SelectedValue)
                drRow("qualificationgroupcondition") = IIf(CInt(cboQualificationGroupCondition.SelectedValue) > 0, cboQualificationGroupCondition.Text, "=")
                'Hemant (30 Oct 2019) -- End

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                'drRow("qualificationunkid") = CInt(cboQualification.SelectedValue)
                'drRow("qualification") = IIf(CInt(cboQualification.SelectedValue) > 0, cboQualification.Text, "")

                drRow("qualificationunkid") = mstrQualificationID
                drRow("qualification") = mstrQualification

                'Pinkal (12-Oct-2011) -- End

                drRow("resultunkid") = CInt(cboResultCode.SelectedValue)
                drRow("resultcode") = IIf(CInt(cboResultCode.SelectedValue) > 0, cboResultCode.Text, "")
                drRow("result_level") = CInt(txtResultLevel.Text.Trim)
                drRow("result_lvl_condition") = CInt(cboResultLvlCondition.SelectedValue)
                drRow("resultcondition") = IIf(CInt(cboResultLvlCondition.SelectedValue) > 0, cboResultLvlCondition.Text, "=")
                drRow("gpacode") = CDec(nudGPA.Value)
                drRow("gpacode_condition") = CInt(cboGPACondition.SelectedValue)
                drRow("gpacondition") = IIf(CInt(cboGPACondition.SelectedValue) > 0, cboGPACondition.Text, "=")
                drRow("age") = CInt(nudAge.Value)
                drRow("age_condition") = CInt(cboAgeCondition.SelectedValue)
                drRow("agecondition") = IIf(CInt(cboAgeCondition.SelectedValue) > 0, cboAgeCondition.Text, "=")


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                If cboAwardedYear.SelectedIndex > 0 Then
                    drRow("award_year") = CInt(cboAwardedYear.Text)
                Else
                    drRow("award_year") = 0
                End If
                drRow("year_condition") = CInt(cboYearCondition.SelectedValue)
                drRow("yearcondition") = IIf(CInt(cboYearCondition.SelectedValue) > 0, cboYearCondition.Text, "=")

                'Pinkal (06-Feb-2012) -- End

                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 9 : On shortlisting, system should be able to shortlist based on the cumulative work experience.)
                drRow("experience") = IIf(CInt(nudExperienceYears.Value) > 0, CInt(nudExperienceYears.Value) & " Years" & " ", "").ToString & IIf(CInt(nudExperienceMonths.Value) > 0, CInt(nudExperienceMonths.Value) & " Months", "").ToString
                drRow("experience_days") = ((CInt(nudExperienceYears.Value) * 365) + (CInt(nudExperienceMonths.Value) * 30))
                drRow("experience_condition") = CInt(cboExperienceCondition.SelectedValue)
                drRow("experiencecondition") = IIf(CInt(cboExperienceCondition.SelectedValue) > 0, cboExperienceCondition.Text, "=")
                'Hemant (07 Oct 2019) -- End


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes
                drRow("other_qualificationgrp") = txtOtherQualificationGrp.Text.Trim
                drRow("other_qualification") = txtOtherQualification.Text.Trim
                drRow("other_resultcode") = txtOtherResultCode.Text.Trim
                drRow("other_skillcategory") = txtOtherSkillCategory.Text.Trim
                drRow("other_skill") = txtOtherSkill.Text.Trim
                'Pinkal (5-MAY-2012) -- End


                drRow("gender") = CInt(cboGender.SelectedValue)
                drRow("gender_name") = IIf(CInt(cboGender.SelectedValue) > 0, cboGender.Text, "")
                drRow("skillcategoryunkid") = mstrSkillCategroyID
                drRow("skillcategory") = mstrSkillCategroy
                drRow("skillunkid") = mstrSkillID
                drRow("skill") = mstrSkill
                'Hemant (02 Jul 2019) -- Start
                'ENHANCEMENT :  ZRA minimum Requirements
                drRow("nationalityunkid") = CInt(cboNationality.SelectedValue)
                drRow("nationality") = IIf(CInt(cboNationality.SelectedValue) > 0, cboNationality.Text, "")
                'Hemant (02 Jul 2019) -- End
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                drRow("filter") = GetFilterString(drRow)
                drRow("IsApply") = False


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                If rdANDCondition.Checked Then
                    drRow("logicalcondition") = 1
                ElseIf rdORCondition.Checked Then
                    drRow("logicalcondition") = 2
                Else
                    drRow("logicalcondition") = 0
                End If
                'Pinkal (06-Feb-2012) -- End


                mdtFilter.Rows.Add(drRow)
                ClearFilterControls()
                FillShortListFilter()
                ReDim Preserve arstrAppliFilter(dgFilter.Rows.Count - 1)
                'S.SANDEEP [ 07 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'arstrAppliFilter.SetValue(GetFilterString(drRow), dgFilter.Rows.Count - 1)
                arstrAppliFilter.SetValue(drRow.Item("filter"), dgFilter.Rows.Count - 1)
                'S.SANDEEP [ 07 NOV 2012 ] -- END

                cboVacancy.Select()

                If rdORCondition.Checked Then
                    rdANDCondition.Enabled = False
                    rdORCondition.Enabled = True
                ElseIf rdANDCondition.Checked Then
                    rdORCondition.Enabled = False
                    rdANDCondition.Enabled = True
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If dgFilter.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Filter Criteria from the list to perform further operation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                btnDelete.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure want to delete this Filter Criteria ? "), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim drRow As DataRow() = Nothing

                If CInt(dgFilter.SelectedRows(0).Cells(objcolhFilterunkid.Name).Value) > 0 Then
                    drRow = mdtFilter.Select("filterunkid = '" & dgFilter.SelectedRows(0).Cells(objcolhFilterunkid.Name).Value.ToString() & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("AUD") = "D"
                        mdtFilter.AcceptChanges()
                    End If

                ElseIf dgFilter.SelectedRows(0).Cells(objcolhGUID.Name).Value.ToString() <> "" Then
                    drRow = mdtFilter.Select("GUID = '" & dgFilter.SelectedRows(0).Cells(objcolhGUID.Name).Value.ToString() & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("AUD") = "D"
                        mdtFilter.AcceptChanges()

                        ' START FOR REMOVE FILTER FROM THE ARRAY
                        Dim list As New List(Of String)(arstrAppliFilter)
                        list.RemoveAt(list.IndexOf(drRow(0)("filter").ToString()))
                        arstrAppliFilter = list.ToArray()
                        ' END FOR REMOVE FILTER FROM THE ARRAY

                    End If
                End If
                FillShortListFilter()
                'GetORFilter()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Try

            If dgFilter.Rows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no Filter in below List to apply filter."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If rdORCondition.Checked Then
                GetORFilter()
            Else
                GetANDFilter()
            End If
            For i = 0 To arstrAppliFilter.Length - 1

                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                'Dim drFilter As DataRow() = mdtFilter.Select("Filter= '" & arstrAppliFilter(i) & "'")
                Dim strFilter As String = ""

                If arstrAppliFilter(i).Contains("%") Then
                    strFilter = arstrAppliFilter(i).ToString().Replace("'", "''")
                Else
                    strFilter = arstrAppliFilter(i).ToString
                End If


                Dim drFilter As DataRow() = mdtFilter.Select("Filter= '" & strFilter & "' AND IsApply = False")

                'Pinkal (06-Feb-2012) -- End


                If drFilter.Length > 0 Then
                    drFilter(0)("IsApply") = True
                End If
                mdtFilter.AcceptChanges()
            Next


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            If mdtFilter.Rows.Count > 0 Then
                If mdtFilter.Rows(mdtFilter.Rows.Count - 1)("AUD").ToString() = "A" Then
                    mdtFilter.Rows(mdtFilter.Rows.Count - 1)("logicalcondition") = 0
                    mdtFilter.AcceptChanges()
                End If
            End If

            'Pinkal (06-Feb-2012) -- End

            FillShortListFilter()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFilter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If lvApplicant.Items.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no applicant to export."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                btnExport.Focus()
                Exit Sub
            End If

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If lvApplicant.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please check atleast one applicant in order to export."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                btnExport.Focus()
                Exit Sub
            End If
            Dim dTab As DataTable : Dim StrIds As String = ""
            For Each lItem As ListViewItem In lvApplicant.CheckedItems
                StrIds &= "," & lItem.Tag.ToString
            Next
            If StrIds.Trim.Length > 0 Then StrIds = Mid(StrIds, 2)
            If StrIds.Trim.Length > 0 Then
                dTab = New DataView(mdtApplicant, "applicantunkid in (" & StrIds & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dTab = New DataView(mdtApplicant, "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Anjan (25 Oct 2012)-End 


            'Pinkal (18-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objFile As New SaveFileDialog
            'objFile.OverwritePrompt = True
            'objFile.Filter = "Xls Files (*.xls) |*.xls"
            'If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
            '    Dim obj As New FileInfo(objFile.FileName)
            '    Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))

            '    If Export_to_Excel(False, obj.Name, mstrFilePath, dTab) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                'End If
            'End If

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            If dTab IsNot Nothing Then
                If dTab.Columns.Contains("employeeunkid") Then
                    dTab.Columns.Remove("employeeunkid")
                    dTab.AcceptChanges()
                End If
            End If
            'Pinkal (25-Jan-2022) -- End

            If Export_to_Excel(True, "ShortListing_Applicant_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dTab) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If

            'Pinkal (18-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnflag As Boolean = False
        Try

            If validation() Then

                If lvApplicant.Items.Count = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no applicant to export."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    btnExport.Focus()
                    Exit Sub
                End If

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                If lvApplicant.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please check atleast one applicant in order to export."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    btnExport.Focus()
                    Exit Sub
                End If
                Dim dTab As DataTable : Dim StrIds As String = ""
                For Each lItem As ListViewItem In lvApplicant.CheckedItems
                    StrIds &= "," & lItem.Tag.ToString
                Next
                If StrIds.Trim.Length > 0 Then StrIds = Mid(StrIds, 2)
                If StrIds.Trim.Length > 0 Then
                    dTab = New DataView(mdtApplicant, "applicantunkid in (" & StrIds & ")", "", DataViewRowState.CurrentRows).ToTable
                Else
                    dTab = New DataView(mdtApplicant, "", "", DataViewRowState.CurrentRows).ToTable
                End If
                'Anjan (25 Oct 2012)-End 

                SetValue()

                'Anjan (25 Oct 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                'blnflag = objShortListMst.Insert(mdtFilter, mdtApplicant)
                blnflag = objShortListMst.Insert(mdtFilter, dTab)
                'Anjan (25 Oct 2012)-End


                If blnflag = False And objShortListMst._Message <> "" Then
                    eZeeMsgBox.Show(objShortListMst._Message, enMsgBoxStyle.Information)
                End If



                'Pinkal (12-Nov-2020) -- Start
                'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                If ConfigParameter._Object._SkipApprovalFlowForApplicantShortListing AndAlso objShortListMst._Shortlistunkid > 0 Then

                    '/* START FOR SUBMIT FOR APPROVAL AUTOMATICALLY AND APPROVED AUTOMATICALLY

                    objShortListMst._Issent = True
                    objShortListMst._Approveuserunkid = User._Object._Userunkid
                    objShortListMst._Remark = "Approved"
                    objShortListMst._Statustypid = enShortListing_Status.SC_APPROVED
                    objShortListMst._Appr_Date = ConfigParameter._Object._CurrentDateAndTime
                    objShortListMst._Isvoid = False

                    blnflag = objShortListMst.Update()

                    If blnflag = False And objShortListMst._Message <> "" Then
                        eZeeMsgBox.Show(objShortListMst._Message, enMsgBoxStyle.Information)
                    End If

                    '/* END FOR SUBMIT FOR APPROVAL AUTOMATICALLY AND APPROVED AUTOMATICALLY

                End If
                'Pinkal (12-Nov-2020) -- End


                'Pinkal (18-Jun-2013) -- Start
                'Enhancement : TRA Changes

                'Dim objFile As New SaveFileDialog
                'objFile.OverwritePrompt = True
                'objFile.Filter = "Xls Files (*.xls) |*.xls"


                'If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                '    Dim obj As New FileInfo(objFile.FileName)
                '    Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                '    If Export_to_Excel(True, obj.Name, mstrFilePath, dTab) Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    'End If
                'End If

                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                If dTab IsNot Nothing Then
                    If dTab.Columns.Contains("employeeunkid") Then
                        dTab.Columns.Remove("employeeunkid")
                        dTab.AcceptChanges()
                    End If
                End If
                'Pinkal (24-Sep-2020) -- End


                If Export_to_Excel(True, "ShortListing_Applicant_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, dTab) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    End If

                'Pinkal (18-Jun-2013) -- End



                If blnflag Then
                    mblnCancel = False
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [ 07 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .DataSource = CType(cboVacancy.DataSource, DataTable)
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2012 ] -- END

    'Hemant (27 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 11 : On shortlisting screen, after exporting, provide option to clear the criteria instead of closing the screen and opening again)
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (27 Sep 2019) -- End

#End Region

#Region "ComboBox Event"

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objVacancy.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue), True)
            Dim dsList As DataSet = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue), True)
            'Shani(24-Aug-2015) -- End

            cboVacancy.DisplayMember = "name"
            cboVacancy.ValueMember = "id"
            cboVacancy.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            objVacancy._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            mintJobunkid = objVacancy._Jobunkid
            FillSkillCateGory(mintJobunkid)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboQualificationGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualificationGrp.SelectedIndexChanged
        Try

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'Dim objQualification As New clsqualification_master
            'Dim dsList As DataSet = objQualification.GetComboList("List", True, CInt(cboQualificationGrp.SelectedValue))
            'cboQualification.DisplayMember = "Name"
            'cboQualification.ValueMember = "Id"
            'cboQualification.DataSource = dsList.Tables(0)

            'Pinkal (12-Oct-2011) -- End
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            FillQualificationGroup()
            'Hemant (07 Oct 2019) -- End

            FillQualification()
            GetResultCode()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualificationGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboQualification_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'Dim objQualification As New clsqualification_master
            'Dim dsList As DataSet = objQualification.GetResultCodeFromQualification(CInt(cboQualification.SelectedValue))
            'cboResultCode.DisplayMember = "resultname"
            'cboResultCode.ValueMember = "resultunkid"
            'cboResultCode.DataSource = dsList.Tables(0)

            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualification_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboResultCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboResultCode.SelectedIndexChanged
        Try
            Dim objResult As New clsresult_master
            objResult._Resultunkid = CInt(cboResultCode.SelectedValue)
            txtResultLevel.Text = objResult._ResultLevel.ToString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboResultCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Private Sub cboQualificationGrpCondition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualificationGrpCondition.SelectedIndexChanged
        Try
            FillQualificationGroup()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualificationGrpCondition_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End

    'Hemant (30 Oct 2019) -- Start
    Private Sub cboQualificationGroupCondition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualificationGroupCondition.SelectedIndexChanged
        Try
            FillQualification()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualificationGroupCondition_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (30 Oct 2019) -- End

#End Region

#Region " NumericUpDown Event(s) "

    Private Sub nudLevel_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudLevel.ValueChanged
        Try
            Dim objMaster As New clsCommon_Master
            Dim dsList As DataSet = objMaster.GetQualificationGrpFromLevel(CInt(nudLevel.Value), True)
            cboQualificationGrp.DisplayMember = "name"
            cboQualificationGrp.ValueMember = "masterunkid"
            cboQualificationGrp.DataSource = dsList.Tables(0)
            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
            FillQualificationGroup()
            cboQualificationGrpCondition.SelectedValue = 0
            mdtQualification.Clear()
            mstrQLevel = "" 'Hemant (07 Oct 2019)
            'Hemant (07 Oct 2019) -- End
            'Hemant (30 Oct 2019) -- Start
            If CInt(nudLevel.Value) > -1 Then
                cboQualificationGrpCondition.Enabled = True
            Else
                cboQualificationGrpCondition.Enabled = False
            End If
            'Hemant (30 Oct 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudLevel_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"
    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    'S.SANDEEP [27-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 17
    'Private Sub lvSkillCategory_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try

    '        RemoveHandler chkSkillCategory.CheckedChanged, AddressOf chkSkillCategory_CheckedChanged
    '        If lvSkillCategory.CheckedItems.Count <= 0 Then
    '            chkSkillCategory.CheckState = CheckState.Unchecked

    '        ElseIf lvSkillCategory.CheckedItems.Count < lvSkillCategory.Items.Count Then
    '            chkSkillCategory.CheckState = CheckState.Indeterminate

    '        ElseIf lvSkillCategory.CheckedItems.Count = lvSkillCategory.Items.Count Then
    '            chkSkillCategory.CheckState = CheckState.Checked

    '        End If
    '        AddHandler chkSkillCategory.CheckedChanged, AddressOf chkSkillCategory_CheckedChanged


    '        Dim mstrSkillCategory As String = ""
    '        If lvSkillCategory.CheckedItems.Count > 0 Then
    '            For i As Integer = 0 To lvSkillCategory.CheckedItems.Count - 1
    '                If mstrSkillCategory.Length <= 0 Then
    '                    mstrSkillCategory = CStr(lvSkillCategory.CheckedItems(i).Tag)
    '                Else
    '                    mstrSkillCategory &= "," & CStr(lvSkillCategory.CheckedItems(i).Tag)
    '                End If
    '            Next
    '            FillSkill(mstrSkillCategory, mintJobunkid)
    '        Else
    '            lvSkillSet.Items.Clear()
    '        End If
    '        EnableControl()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSkillCategory_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Private Sub lvQualification_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try

    '        RemoveHandler chkQualification.CheckedChanged, AddressOf chkQualification_CheckedChanged
    '        If lvQualification.CheckedItems.Count <= 0 Then
    '            chkQualification.CheckState = CheckState.Unchecked

    '        ElseIf lvQualification.CheckedItems.Count < lvQualification.Items.Count Then
    '            chkQualification.CheckState = CheckState.Indeterminate

    '        ElseIf lvQualification.CheckedItems.Count = lvQualification.Items.Count Then
    '            chkQualification.CheckState = CheckState.Checked

    '        End If
    '        AddHandler chkQualification.CheckedChanged, AddressOf chkQualification_CheckedChanged


    '        GetResultCode()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvQualification_ItemChecked", mstrModuleName)
    '    End Try

    'End Sub

    'Private Sub lvSkillSet_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try
    '        RemoveHandler chkSkill.CheckedChanged, AddressOf chkSkill_CheckedChanged
    '        If lvSkillSet.CheckedItems.Count <= 0 Then
    '            chkSkill.CheckState = CheckState.Unchecked

    '        ElseIf lvSkillSet.CheckedItems.Count < lvSkillSet.Items.Count Then
    '            chkSkill.CheckState = CheckState.Indeterminate

    '        ElseIf lvSkillSet.CheckedItems.Count = lvSkillSet.Items.Count Then
    '            chkSkill.CheckState = CheckState.Checked

    '        End If
    '        AddHandler chkSkill.CheckedChanged, AddressOf chkSkill_CheckedChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSkillSet_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP [27-SEP-2017] -- END


    'Pinkal (12-Oct-2011) -- End
#End Region

#Region "GridView Event"

    Private Sub dgFilter_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFilter.CellClick
        Try
            'If e.RowIndex < 0 Then Exit Sub
            'ClearFilterControls()
            'cboVacancy.Text = dgFilter.Rows(e.RowIndex).Cells(colhVacancy.Name).Value.ToString()
            'nudLevel.Value = CInt(dgFilter.Rows(e.RowIndex).Cells(colhQlGrpLevel.Name).Value.ToString())
            'cboQualificationGrp.Text = dgFilter.Rows(e.RowIndex).Cells(colhQlGrp.Name).Value.ToString()
            'cboQualification.Text = dgFilter.Rows(e.RowIndex).Cells(colhQualification.Name).Value.ToString()
            'cboResultCode.Text = dgFilter.Rows(e.RowIndex).Cells(colhResultCode.Name).Value.ToString()
            'txtResultLevel.Text = dgFilter.Rows(e.RowIndex).Cells(colhRLevel.Name).Value.ToString()
            'cboResultLvlCondition.Text = dgFilter.Rows(e.RowIndex).Cells(colhRCondition.Name).Value.ToString()
            'nudGPA.Value = CDec(dgFilter.Rows(e.RowIndex).Cells(colhGPA.Name).Value.ToString())
            'cboGPACondition.Text = dgFilter.Rows(e.RowIndex).Cells(colhGPACondition.Name).Value.ToString()
            'nudAge.Value = CInt(dgFilter.Rows(e.RowIndex).Cells(colhAge.Name).Value.ToString())
            'cboAgeCondition.Text = dgFilter.Rows(e.RowIndex).Cells(colhAgeCondition.Name).Value.ToString()
            'cboGender.Text = dgFilter.Rows(e.RowIndex).Cells(colhGender.Name).Value.ToString()
            'SetSkillCategory(dgFilter.Rows(e.RowIndex).Cells(colhFilterSkillCategory.Name).Value.ToString())
            'SetSkill(dgFilter.Rows(e.RowIndex).Cells(colhFilterSkill.Name).Value.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgFilter_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSkillCategory_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSkillCategory.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If dgvSkillCategory.IsCurrentCellDirty Then
                dgvSkillCategory.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objdgcolhSCheck.Index Then
                Dim mstrSkillCategory As String = ""
                mdtSkillCategory.AcceptChanges()
                If mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    mstrSkillCategory = String.Join(",", mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("skillcategoryunkid").ToString()).ToArray())
                End If
                If mstrSkillCategory.Trim.Length > 0 Then
                    FillSkill(mstrSkillCategory, mintJobunkid)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSkillCategory_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvQualification_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQualification.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub
            If dgvQualification.IsCurrentCellDirty Then
                dgvQualification.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            If e.ColumnIndex = objdgcolhQlCheck.Index Then
                Call GetResultCode()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualification_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvSkillName_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvSkillName.RowPostPaint
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.RowIndex = dgvSkillName.Rows.Count - 1 Then Exit Sub
            If CInt(dgvSkillName.Rows(e.RowIndex + 1).Cells(objdgcolhCategoryId.Index).Value) <> CInt(dgvSkillName.Rows(e.RowIndex).Cells(objdgcolhCategoryId.Index).Value) Then
                e.Graphics.DrawLine(Pens.Red, e.RowBounds.Left, e.RowBounds.Bottom - 1, e.RowBounds.Right, e.RowBounds.Bottom - 1)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSkillName_RowPostPaint", mstrModuleName)
        Finally
        End Try
    End Sub

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Private Sub dgvQualificationGroup_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQualificationGroup.CellContentClick
        Dim objCommonMaster As New clsCommon_Master
        Try
            If e.RowIndex < 0 Then Exit Sub

            If dgvQualificationGroup.IsCurrentCellDirty Then
                dgvQualificationGroup.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objdgcolhSCheck.Index Then
                Dim mstrQualificationGroup As String = ""
                mdtQualificationGroup.AcceptChanges()
                If mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    mstrQualificationGroup = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())
                    'Hemant (30 Oct 2019) -- Start
                    cboQualificationGroupCondition.Enabled = True
                Else
                    cboQualificationGroupCondition.Enabled = False
                    'Hemant (30 Oct 2019) -- End
                End If

                If mstrQualificationGroup.Trim.Length > 0 Then
                    mstrQLevel = objCommonMaster.GetCommaSeparateCommonMasterData("qlevel", mstrQualificationGroup, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
                Else
                    mstrQLevel = ""
                End If
                FillQualification()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualificationGroup_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End

#End Region

#Region "CheckBox Event"
    'S.SANDEEP [27-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 17
    'Private Sub chkSkillCategory_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkillCategory.CheckedChanged
    '    Try
    '        If lvSkillCategory.Items.Count > 0 Then

    '            For Each lvItem As ListViewItem In lvSkillCategory.Items

    '                If chkSkillCategory.Checked = True Then
    '                    lvItem.Checked = True
    '                ElseIf chkSkillCategory.Checked = False Then
    '                    RemoveHandler lvSkillCategory.ItemChecked, AddressOf lvSkillCategory_ItemChecked
    '                    lvItem.Checked = False
    '                    AddHandler lvSkillCategory.ItemChecked, AddressOf lvSkillCategory_ItemChecked
    '                    lvSkillSet.Items.Clear()
    '                End If
    '            Next
    '            EnableControl()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSkillCategory_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub chkSkillCategory_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkillCategory.CheckedChanged
        Try
            If dgvSkillCategory.RowCount > 0 Then
                For Each dr As DataRowView In dvSkillCategory
                    dr("ischeck") = CBool(chkSkillCategory.Checked)
                Next
                dgvSkillCategory.Refresh()
                If CBool(chkSkillCategory.Checked) = True Then
                    Dim mstrSkillCategory As String = ""
                    mdtSkillCategory.AcceptChanges()
                    If mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                        mstrSkillCategory = String.Join(",", mdtSkillCategory.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("skillcategoryunkid").ToString()).ToArray())
                    End If
                    If mstrSkillCategory.Trim.Length > 0 Then
                        FillSkill(mstrSkillCategory, mintJobunkid)
                    End If
                Else
                    mdtSkillSet.Rows.Clear()
                    End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSkillCategory_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Private Sub chkQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkQualification.CheckedChanged
    '    Try
    '        If lvQualification.Items.Count > 0 Then

    '            For Each lvItem As ListViewItem In lvQualification.Items
    '                RemoveHandler lvQualification.ItemChecked, AddressOf lvQualification_ItemChecked
    '                lvItem.Checked = chkQualification.Checked
    '                AddHandler lvQualification.ItemChecked, AddressOf lvQualification_ItemChecked
    '            Next
    '        End If
    '        GetResultCode()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkQualification_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub chkQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkQualification.CheckedChanged
        Try
            If dgvQualification.RowCount > 0 Then
                For Each dr As DataRowView In dvQualification
                    dr("ischeck") = CBool(chkQualification.Checked)
                Next
                dgvQualification.Refresh()
                Call GetResultCode()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkQualification_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Private Sub chkSkill_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkill.CheckedChanged
    '    Try
    '        If lvSkillSet.Items.Count > 0 Then

    '            For Each lvItem As ListViewItem In lvSkillSet.Items
    '                RemoveHandler lvSkillSet.ItemChecked, AddressOf lvSkillSet_ItemChecked
    '                lvItem.Checked = chkSkill.Checked
    '                AddHandler lvSkillSet.ItemChecked, AddressOf lvSkillSet_ItemChecked
    '            Next
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSkill_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub chkSkill_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkill.CheckedChanged
        Try
            If dgvSkillName.RowCount > 0 Then
                For Each dr As DataRowView In dvSkillSet
                    dr("ischeck") = CBool(chkSkill.Checked)
                Next
                dgvSkillName.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSkill_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [27-SEP-2017] -- END
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 7 : On shortlisting, System should be able to filter based on the qualification group condition given. E.g Qualification Grp  > Bachelor’s degree. With this condition, all applicants who have any qualification group greater than a bachelors degree will be caught by this filter. .)
    Private Sub chkQualificationGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkQualificationGroup.CheckedChanged
        Dim objCommonMaster As New clsCommon_Master
        Try
            If dgvQualificationGroup.RowCount > 0 Then
                For Each dr As DataRowView In dvQualificationGroup
                    dr("ischeck") = CBool(chkQualificationGroup.Checked)
                Next
                dgvQualificationGroup.Refresh()
                Dim mstrQualificationGroup As String = ""
                If mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
                    mstrQualificationGroup = String.Join(",", mdtQualificationGroup.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("masterunkid").ToString()).ToArray())
                End If
                If mstrQualificationGroup.Trim.Length > 0 Then
                    mstrQLevel = objCommonMaster.GetCommaSeparateCommonMasterData("qlevel", mstrQualificationGroup, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP)
                    mstrQLevel = String.Join(",", mstrQLevel.Split(","c).Distinct().ToArray())
                Else
                    mstrQLevel = ""
                End If

                FillQualification()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkQualificationGroup_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End
#End Region

    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvApplicant.ItemChecked, AddressOf lvApplicant_ItemChecked
            For Each LItem As ListViewItem In lvApplicant.Items
                LItem.Checked = CBool(objchkAll.CheckState)
            Next
            AddHandler lvApplicant.ItemChecked, AddressOf lvApplicant_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvApplicant_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvApplicant.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvApplicant.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvApplicant.CheckedItems.Count < lvApplicant.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvApplicant.CheckedItems.Count = lvApplicant.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApplicant_ItemChecked", mstrModuleName)
        End Try
    End Sub
    'Anjan (25 Oct 2012)-End 


    Private Sub chkOtherQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOtherQualification.CheckedChanged
        Try

            'Pinkal (15-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 6 : On shortlisting screen, remove the qualification group levels and allow the user to select the qualification group from the dropdown freely without selecting the qualification group level.)
            'nudLevel.Value = 0
            nudLevel.Value = -1
            'Hemant (27 Sep 2019) -- End
            'Pinkal (15-MAY-2012) -- End

            cboResultLvlCondition.SelectedIndex = 0
            txtOtherResultCode.Text = ""
            txtResultLevel.Text = "0"
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvSkillCategory.Items.Clear()
            'lvSkillSet.Items.Clear()
            dgvSkillCategory.DataSource = Nothing
            dgvSkillName.DataSource = Nothing
            'S.SANDEEP [27-SEP-2017] -- END
            If chkOtherQualification.Checked Then
                pnlOtherQualification.Visible = True
                txtResultLevel.Enabled = False
                chkSkillCategory.Enabled = False
                chkSkill.Enabled = False
                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 17
                'lvSkillCategory.Enabled = False
                'lvSkillSet.Enabled = False
                dgvSkillCategory.Enabled = False
                dgvSkillName.Enabled = False
                'S.SANDEEP [27-SEP-2017] -- END
                cboResultLvlCondition.Enabled = False
                txtOtherResultCode.Visible = True
                cboResultCode.Visible = False
                'S.SANDEEP [ 07 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lblCaption.Visible = True
                'S.SANDEEP [ 07 NOV 2012 ] -- END
            Else
                pnlOtherQualification.Visible = False
                txtResultLevel.Enabled = True
                chkSkillCategory.Enabled = True
                chkSkill.Enabled = True
                chkSkillCategory.Enabled = True
                'S.SANDEEP [27-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 17
                'lvSkillCategory.Enabled = True
                'lvSkillSet.Enabled = True
                dgvSkillCategory.Enabled = True
                dgvSkillName.Enabled = True
                'S.SANDEEP [27-SEP-2017] -- END
                cboResultLvlCondition.Enabled = True
                txtOtherResultCode.Visible = False
                cboResultCode.Visible = True
                'S.SANDEEP [ 07 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lblCaption.Visible = False
                'S.SANDEEP [ 07 NOV 2012 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOtherQualification_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [12 JUN 2015] -- START
    Private Sub objbtnQualificationSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnQualificationSearch.Click
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvQualification.Items.Count <= 0 Then Exit Sub
            'lvQualification.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvQualification.FindItemWithText(txtQSearch.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvQualification.Focus()
            '    lvQualification.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If dgvQualification.RowCount <= 0 Then Exit Sub
            Dim strSearch As String = ""
            If txtQSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhQlName.DataPropertyName & " LIKE '%" & txtQSearch.Text & "%'"
            End If
            dvQualification.RowFilter = strSearch
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnQualificationSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnQualificationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnQualificationReset.Click
        Try
            txtQSearch.Text = ""
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvQualification.SelectedIndices.Clear()
            'If lvQualification.Items.Count > 0 Then lvQualification.EnsureVisible(0)
            If dvQualification IsNot Nothing Then dvQualification.RowFilter = ""
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnQualificationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSkillCategorySearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSkillCategorySearch.Click
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvSkillCategory.Items.Count <= 0 Then Exit Sub
            'lvSkillCategory.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvSkillCategory.FindItemWithText(txtSCSearch.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvSkillCategory.Focus()
            '    lvSkillCategory.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If dgvSkillCategory.RowCount <= 0 Then Exit Sub
            Dim strSearch As String = ""
            If txtSCSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhSkillCategory.DataPropertyName & " LIKE '%" & txtSCSearch.Text & "%'"
            End If
            dvSkillCategory.RowFilter = strSearch
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSkillCategorySearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSkillCategoryReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSkillCategoryReset.Click
        Try
            txtSCSearch.Text = ""
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvSkillCategory.SelectedIndices.Clear()
            'If lvSkillCategory.Items.Count > 0 Then lvSkillCategory.EnsureVisible(0)
            If dvSkillCategory IsNot Nothing Then dvSkillCategory.RowFilter = ""
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSkillCategoryReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSeachSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSeachSkills.Click
        Try
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'If lvSkillSet.Items.Count <= 0 Then Exit Sub
            'lvSkillSet.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvSkillSet.FindItemWithText(txtSkillSearch.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvSkillSet.Focus()
            '    lvSkillSet.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If dgvSkillName.RowCount <= 0 Then Exit Sub
            Dim strSearch As String = ""
            If txtSkillSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhSkillName.DataPropertyName & " LIKE '%" & txtSkillSearch.Text & "%'"
            End If
            dvSkillSet.RowFilter = strSearch
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSeachSkills_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnResetSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnResetSkills.Click
        Try
            txtSkillSearch.Text = ""
            'S.SANDEEP [27-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 17
            'lvSkillSet.SelectedIndices.Clear()
            'If lvSkillSet.Items.Count > 0 Then lvSkillSet.EnsureVisible(0)
            If dvSkillSet IsNot Nothing Then dvSkillSet.RowFilter = ""
            'S.SANDEEP [27-SEP-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnResetSkills_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [12 JUN 2015] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.gbShortListedApplicant.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShortListedApplicant.ForeColor = GUI._eZeeContainerHeaderForeColor


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.lblQualificationGrpLevel.Text = Language._Object.getCaption(Me.lblQualificationGrpLevel.Name, Me.lblQualificationGrpLevel.Text)
			Me.lblQualificationgrp.Text = Language._Object.getCaption(Me.lblQualificationgrp.Name, Me.lblQualificationgrp.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.lblResultCode.Text = Language._Object.getCaption(Me.lblResultCode.Name, Me.lblResultCode.Text)
			Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
			Me.lblGPA.Text = Language._Object.getCaption(Me.lblGPA.Name, Me.lblGPA.Text)
			Me.lblAgeCondition.Text = Language._Object.getCaption(Me.lblAgeCondition.Name, Me.lblAgeCondition.Text)
			Me.lblAge.Text = Language._Object.getCaption(Me.lblAge.Name, Me.lblAge.Text)
			Me.lblGPACondition.Text = Language._Object.getCaption(Me.lblGPACondition.Name, Me.lblGPACondition.Text)
			Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
			Me.lblSkillCategory.Text = Language._Object.getCaption(Me.lblSkillCategory.Name, Me.lblSkillCategory.Text)
			Me.lblSkill.Text = Language._Object.getCaption(Me.lblSkill.Name, Me.lblSkill.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.lblVacanyType.Text = Language._Object.getCaption(Me.lblVacanyType.Name, Me.lblVacanyType.Text)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.gbShortListedApplicant.Text = Language._Object.getCaption(Me.gbShortListedApplicant.Name, Me.gbShortListedApplicant.Text)
			Me.colhAppliQlfication.Text = Language._Object.getCaption(CStr(Me.colhAppliQlfication.Tag), Me.colhAppliQlfication.Text)
			Me.colhApplicantCode.Text = Language._Object.getCaption(CStr(Me.colhApplicantCode.Tag), Me.colhApplicantCode.Text)
			Me.colhApplicant.Text = Language._Object.getCaption(CStr(Me.colhApplicant.Tag), Me.colhApplicant.Text)
			Me.chkInvalidCode.Text = Language._Object.getCaption(Me.chkInvalidCode.Name, Me.chkInvalidCode.Text)
			Me.grpCondition.Text = Language._Object.getCaption(Me.grpCondition.Name, Me.grpCondition.Text)
			Me.rdANDCondition.Text = Language._Object.getCaption(Me.rdANDCondition.Name, Me.rdANDCondition.Text)
			Me.rdORCondition.Text = Language._Object.getCaption(Me.rdORCondition.Name, Me.rdORCondition.Text)
			Me.chkSkillCategory.Text = Language._Object.getCaption(Me.chkSkillCategory.Name, Me.chkSkillCategory.Text)
			Me.chkSkill.Text = Language._Object.getCaption(Me.chkSkill.Name, Me.chkSkill.Text)
			Me.chkQualification.Text = Language._Object.getCaption(Me.chkQualification.Name, Me.chkQualification.Text)
            Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)
			Me.lblYearCondition.Text = Language._Object.getCaption(Me.lblYearCondition.Name, Me.lblYearCondition.Text)
			Me.lblAwardYear.Text = Language._Object.getCaption(Me.lblAwardYear.Name, Me.lblAwardYear.Text)
			Me.lblOtherSkillCategory.Text = Language._Object.getCaption(Me.lblOtherSkillCategory.Name, Me.lblOtherSkillCategory.Text)
			Me.lblOtherQualificationGrp.Text = Language._Object.getCaption(Me.lblOtherQualificationGrp.Name, Me.lblOtherQualificationGrp.Text)
			Me.lblOtherQualification.Text = Language._Object.getCaption(Me.lblOtherQualification.Name, Me.lblOtherQualification.Text)
			Me.lblOtherSkill.Text = Language._Object.getCaption(Me.lblOtherSkill.Name, Me.lblOtherSkill.Text)
			Me.chkOtherQualification.Text = Language._Object.getCaption(Me.chkOtherQualification.Name, Me.chkOtherQualification.Text)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
			Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
			Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
			Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
			Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
			Me.dgcolhSkillCategory.HeaderText = Language._Object.getCaption(Me.dgcolhSkillCategory.Name, Me.dgcolhSkillCategory.HeaderText)
			Me.dgcolhQlName.HeaderText = Language._Object.getCaption(Me.dgcolhQlName.Name, Me.dgcolhQlName.HeaderText)
			Me.dgcolhSkillName.HeaderText = Language._Object.getCaption(Me.dgcolhSkillName.Name, Me.dgcolhSkillName.HeaderText)
			Me.lblNationality.Text = Language._Object.getCaption(Me.lblNationality.Name, Me.lblNationality.Text)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
            Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
            Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
            Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.lblQualificationGrpCondition.Text = Language._Object.getCaption(Me.lblQualificationGrpCondition.Name, Me.lblQualificationGrpCondition.Text)
            Me.chkQualificationGroup.Text = Language._Object.getCaption(Me.chkQualificationGroup.Name, Me.chkQualificationGroup.Text)
            Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
            Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
            Me.lblExperienceMonths.Text = Language._Object.getCaption(Me.lblExperienceMonths.Name, Me.lblExperienceMonths.Text)
            Me.lblExperienceYears.Text = Language._Object.getCaption(Me.lblExperienceYears.Name, Me.lblExperienceYears.Text)
            Me.lblExperience.Text = Language._Object.getCaption(Me.lblExperience.Name, Me.lblExperience.Text)
			Me.colhVacancy.HeaderText = Language._Object.getCaption(Me.colhVacancy.Name, Me.colhVacancy.HeaderText)
			Me.colhQlGrpLevel.HeaderText = Language._Object.getCaption(Me.colhQlGrpLevel.Name, Me.colhQlGrpLevel.HeaderText)
			Me.colhQlGrp.HeaderText = Language._Object.getCaption(Me.colhQlGrp.Name, Me.colhQlGrp.HeaderText)
			Me.colhdgQualification.HeaderText = Language._Object.getCaption(Me.colhdgQualification.Name, Me.colhdgQualification.HeaderText)
			Me.colhResultCode.HeaderText = Language._Object.getCaption(Me.colhResultCode.Name, Me.colhResultCode.HeaderText)
			Me.colhRLevel.HeaderText = Language._Object.getCaption(Me.colhRLevel.Name, Me.colhRLevel.HeaderText)
			Me.colhRCondition.HeaderText = Language._Object.getCaption(Me.colhRCondition.Name, Me.colhRCondition.HeaderText)
			Me.colhGPA.HeaderText = Language._Object.getCaption(Me.colhGPA.Name, Me.colhGPA.HeaderText)
			Me.colhGPACondition.HeaderText = Language._Object.getCaption(Me.colhGPACondition.Name, Me.colhGPACondition.HeaderText)
			Me.colhAge.HeaderText = Language._Object.getCaption(Me.colhAge.Name, Me.colhAge.HeaderText)
			Me.colhAgeCondition.HeaderText = Language._Object.getCaption(Me.colhAgeCondition.Name, Me.colhAgeCondition.HeaderText)
			Me.colhAwardYear.HeaderText = Language._Object.getCaption(Me.colhAwardYear.Name, Me.colhAwardYear.HeaderText)
			Me.colhYearCondition.HeaderText = Language._Object.getCaption(Me.colhYearCondition.Name, Me.colhYearCondition.HeaderText)
			Me.colhGender.HeaderText = Language._Object.getCaption(Me.colhGender.Name, Me.colhGender.HeaderText)
			Me.colhFilterSkillCategory.HeaderText = Language._Object.getCaption(Me.colhFilterSkillCategory.Name, Me.colhFilterSkillCategory.HeaderText)
			Me.colhFilterSkill.HeaderText = Language._Object.getCaption(Me.colhFilterSkill.Name, Me.colhFilterSkill.HeaderText)
			Me.colhFilterOtherQuliGrp.HeaderText = Language._Object.getCaption(Me.colhFilterOtherQuliGrp.Name, Me.colhFilterOtherQuliGrp.HeaderText)
			Me.colhFilterOtherQualification.HeaderText = Language._Object.getCaption(Me.colhFilterOtherQualification.Name, Me.colhFilterOtherQualification.HeaderText)
			Me.colhFilterOtherResultCode.HeaderText = Language._Object.getCaption(Me.colhFilterOtherResultCode.Name, Me.colhFilterOtherResultCode.HeaderText)
			Me.colhFilterOtherSkillCategory.HeaderText = Language._Object.getCaption(Me.colhFilterOtherSkillCategory.Name, Me.colhFilterOtherSkillCategory.HeaderText)
			Me.colhFilterOtherSkill.HeaderText = Language._Object.getCaption(Me.colhFilterOtherSkill.Name, Me.colhFilterOtherSkill.HeaderText)
			Me.colhNationality.HeaderText = Language._Object.getCaption(Me.colhNationality.Name, Me.colhNationality.HeaderText)
            Me.colhExperience.HeaderText = Language._Object.getCaption(Me.colhExperience.Name, Me.colhExperience.HeaderText)
            Me.colhExperienceCondition.HeaderText = Language._Object.getCaption(Me.colhExperienceCondition.Name, Me.colhExperienceCondition.HeaderText)
            Me.DataGridViewTextBoxColumn35.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn35.Name, Me.DataGridViewTextBoxColumn35.HeaderText)
            Me.DataGridViewTextBoxColumn36.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn36.Name, Me.DataGridViewTextBoxColumn36.HeaderText)
            Me.lblQualificationGroupCondition.Text = Language._Object.getCaption(Me.lblQualificationGroupCondition.Name, Me.lblQualificationGroupCondition.Text)
            Me.dgcolhQlGrpName.HeaderText = Language._Object.getCaption(Me.dgcolhQlGrpName.Name, Me.dgcolhQlGrpName.HeaderText)
            Me.dgcolhQLevel.HeaderText = Language._Object.getCaption(Me.dgcolhQLevel.Name, Me.dgcolhQLevel.HeaderText)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Vacancy Type is compulsory information.Please Select Vacancy Type.")
			Language.setMessage(mstrModuleName, 2, "Vacancy is compulsory information.Please Select Vacancy.")
			Language.setMessage(mstrModuleName, 3, "This Filer criteria is already exist in below List.Please define new filter.")
			Language.setMessage(mstrModuleName, 4, "Please select Filter Criteria from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 5, "Are you sure want to delete this Filter Criteria ?")
			Language.setMessage(mstrModuleName, 6, "There is no Filter in below List to apply filter.")
			Language.setMessage(mstrModuleName, 7, "There is no applicant to export.")
			Language.setMessage(mstrModuleName, 8, "Data Exported successfully.")
            Language.setMessage(mstrModuleName, 9, "Applicant Short Listing")
            Language.setMessage(mstrModuleName, 10, "Filter(s):-")
            Language.setMessage(mstrModuleName, 11, "Condtion")
			Language.setMessage(mstrModuleName, 12, "Select")
			Language.setMessage(mstrModuleName, 13, " Out of")
			Language.setMessage(mstrModuleName, 14, "Please check atleast one applicant in order to export.")
            Language.setMessage(mstrModuleName, 15, "Multiple Different Types of Levels are not allowed for selected Qualification Group Condition")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region
 'Language & UI Settings
	'</Language>

    
    
    
    
End Class


'Private Sub SetSkillCategory(ByVal mstrSkillCategory As String)
'    Try
'        If mstrSkillCategory.Trim.Length > 0 Then
'            Dim arStrCategory As String() = mstrSkillCategory.Split(CChar(","))
'            If arStrCategory.Length > 0 Then
'                For i As Integer = 0 To arStrCategory.Length - 1
'                    Dim lvItem As ListViewItem = lvSkillCategory.FindItemWithText(arStrCategory(i).ToString())
'                    If lvItem IsNot Nothing Then
'                        lvItem.Checked = True
'                    End If
'                Next
'            End If
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "SetSkillCategory", mstrModuleName)
'    End Try
'End Sub

'Private Sub SetSkill(ByVal mstrSkill As String)
'    Try
'        If mstrSkill.Trim.Length > 0 Then
'            Dim arStrSkill As String() = mstrSkill.Split(CChar(","))
'            If arStrSkill.Length > 0 Then
'                For i As Integer = 0 To arStrSkill.Length - 1
'                    Dim lvItem As ListViewItem = lvSkillSet.FindItemWithText(arStrSkill(i).ToString())
'                    If lvItem IsNot Nothing Then
'                        lvItem.Checked = True
'                    End If
'                Next
'            End If
'        End If
'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "SetSkillCategory", mstrModuleName)
'    End Try
'End Sub