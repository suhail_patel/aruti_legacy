﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFinalApplicant

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmFinalApplicant"
    Private objShortList As New clsshortlist_master
    Private objShortListFinal As clsshortlist_finalapplicant
    Private mdtTran As DataTable
    Private mdtFinalTran As DataTable
    Private mdvFinalApplicant As DataView
    'S.SANDEEP [08-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
    Private mdvShortListed As DataView
    'S.SANDEEP [08-JUN-2018] -- END

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try

            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes

            Dim objVacancy As New clsVacancy
            dsCombo = objVacancy.getVacancyType()
            cboVacancyType.DisplayMember = "Name"
            cboVacancyType.ValueMember = "Id"
            cboVacancyType.DataSource = dsCombo.Tables(0)

            'dsCombo = objRef.getComboList("List", True)
            'With cboRefNo
            '    .ValueMember = "shortlistunkid"
            '    .DisplayMember = "refno"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With

            'Pinkal (12-May-2013) -- End


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.GetComboforShortlistingApprovalStatus(True, "List")
            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            Dim dr As DataRow = dsCombo.Tables("List").NewRow()
            dr.Item("Id") = 999
            dr.Item("Name") = mnuSubmitForApproval.Text
            dsCombo.Tables("List").Rows.Add(dr)
            'S.SANDEEP [08-JUN-2018] -- END
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'Pinkal (27-Jun-2013) -- End


            'Pinkal (12-Nov-2020) -- Start
            'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
            If ConfigParameter._Object._SkipApprovalFlowForFinalApplicant Then
                cboStatus.Visible = False
                LblStatus.Visible = False
                txtRemarks.Visible = False
                lblRemarks.Visible = False
            End If
            'Pinkal (12-Nov-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor(ByVal dgGrid As DataGridView, ByVal objColhIsGroup As DataGridViewTextBoxColumn)
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgGrid.RowCount - 1
                If CBool(dgGrid.Rows(i).Cells(objColhIsGroup.Index).Value) = True Then
                    dgGrid.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgGrid.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue(ByVal dgGrid As DataGridView, ByVal colhIsGroup As DataGridViewTextBoxColumn, ByVal colhCollaps As DataGridViewTextBoxColumn, Optional ByVal mstrSign As String = "+")
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgGrid.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgGrid.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionForeColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgGrid.RowCount - 1
                If CBool(dgGrid.Rows(i).Cells(colhIsGroup.Index).Value) = True Then
                    If dgGrid.Rows(i).Cells(colhCollaps.Index).Value Is Nothing Then
                        dgGrid.Rows(i).Cells(colhCollaps.Index).Value = mstrSign
                    End If
                    dgGrid.Rows(i).Cells(colhCollaps.Index).Style = objdgvsCollapseHeader
                Else
                    dgGrid.Rows(i).Cells(colhCollaps.Index).Value = ""
                    dgGrid.Rows(i).Cells(colhCollaps.Index).Style = objdgvsCollapseChild
                    If mstrSign = "+" Then
                        dgGrid.Rows(i).Visible = False
                    End If
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillShortListedApplicant()
        Try

            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes

            'dgShortListApplicant.AutoGenerateColumns = False
            'objcolhSelect.DataPropertyName = "isChecked"
            'colhGender.DataPropertyName = "Gender"
            'colhBirthdate.DataPropertyName = "BDate"
            'colhPhone.DataPropertyName = "Phone"
            'colhEmail.DataPropertyName = "Email"
            'dgShortListApplicant.DataSource = mdtTran

            'Pinkal (12-May-2013) -- End


            dgShortListApplicant.AutoGenerateColumns = False
            colhApplicant.DataPropertyName = "refno"
            objcolhSelect.DataPropertyName = "ischecked"
            'colhVacancy.DataPropertyName = "vacancytitle"
            colhGender.DataPropertyName = "gender"
            colhBirthdate.DataPropertyName = "BDate"
            colhPhone.DataPropertyName = "phone"
            colhEmail.DataPropertyName = "email"
            'objcolhShortListIsGrp.DataPropertyName = "IsGrp"
            objColhShortListId.DataPropertyName = "shortlistunkid"
            objColhShortListApplicantId.DataPropertyName = "applicantunkid"

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            'dgShortListApplicant.DataSource = mdtTran
            mdvShortListed = mdtTran.DefaultView()
            dgShortListApplicant.DataSource = mdvShortListed
            'S.SANDEEP [08-JUN-2018] -- END

            'mdtTran = dtList
            'SetGridColor(dgShortListApplicant, objcolhShortListIsGrp)
            'SetCollapseValue(dgShortListApplicant, objcolhShortListIsGrp, objcolhShortListIsCollaspe)

            If mdtTran.Rows.Count > 0 Then
                objCheckAll.Checked = True
            Else
                objCheckAll.Checked = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillShortListedApplicant", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillFinalApplicantList()
        Try

            If mdtFinalTran IsNot Nothing Then


                mdvFinalApplicant = mdtFinalTran.DefaultView

                'Pinkal (27-Jun-2013) -- Start
                'Enhancement : TRA Changes
                mdvFinalApplicant.RowFilter = ""
                If CInt(cboStatus.SelectedValue) > 0 Then
                    'S.SANDEEP [08-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
                    'mdvFinalApplicant.RowFilter = " AUD <> 'D' AND app_statusid = " & CInt(cboStatus.SelectedValue)
                    If CInt(cboStatus.SelectedValue) = 999 Then
                        mdvFinalApplicant.RowFilter = " AUD <> 'D' AND app_issent = 1 "
                    Else
                        mdvFinalApplicant.RowFilter = " AUD <> 'D' AND app_statusid = " & CInt(cboStatus.SelectedValue)
                    End If
                    'S.SANDEEP [08-JUN-2018] -- END
                Else
                    mdvFinalApplicant.RowFilter = "AUD <> 'D'"
                End If

                'Pinkal (27-Jun-2013) -- End

            End If

            dgFinalApplicant.AutoGenerateColumns = False
            objcolhFinalSelect.DataPropertyName = "ischecked"
            colhFinalApplicantName.DataPropertyName = "refno"
            colhFinalGender.DataPropertyName = "Gender"
            colhFinalBirthdate.DataPropertyName = "BDate"
            colhFinalPhone.DataPropertyName = "Phone"
            colhFinalEmail.DataPropertyName = "Email"
            colhdgStatus.DataPropertyName = "Status"
            colhApplicant.DataPropertyName = "refno"
            objcolhdgFinalApplicantID.DataPropertyName = "applicantunkid"

            'colhFinalApplicantCode.DataPropertyName = "ACode"
            'objcolhFinalApplicantVacancy.DataPropertyName = "vacancytitle"
            'objcolhShort_FinalApplicantStatus.DataPropertyName = "status"
            'objcolhShort_FinalApplicantRemark.DataPropertyName = "remark"
            'objcolhOpeningDate.DataPropertyName = "openingdate"
            'objcolhClosingdate.DataPropertyName = "closingdate"
            'objcolhVacancyId.DataPropertyName = "vacancyunkid"

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            colhFinalApprovalSubmit.DataPropertyName = "submitapproval"
            If mdtFinalTran IsNot Nothing Then
                mdvFinalApplicant = mdtFinalTran.DefaultView
                dgFinalApplicant.DataSource = mdvFinalApplicant
            End If
            'S.SANDEEP [08-JUN-2018] -- END


            If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.Count > 0 Then
                objchkFinalSelectAll.Checked = True
            Else
                objchkFinalSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillFinalApplicantList", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    'Private Sub Fill_List()
    '    Try
    '        If CInt(cboRefNo.SelectedValue) > 0 Then
    '            If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
    '            mdtTran = objShortListFinal.GetShortListFinalApplicant(CInt(cboRefNo.SelectedValue), -1, cboRefNo.Text.Trim, True)

    '            mdtFinalTran = New DataView(mdtTran, "isfinalshortlisted = True ", "", DataViewRowState.CurrentRows).ToTable
    '            mdtTran = New DataView(mdtTran, "isfinalshortlisted = False ", "", DataViewRowState.CurrentRows).ToTable

    '            FillShortListedApplicant()
    '            FillFinalApplicantList()

    '            Dim dtTemp() As DataRow = CType(cboRefNo.DataSource, DataTable).Select("shortlistunkid= '" & CInt(cboRefNo.SelectedValue) & "'")
    '            If dtTemp.Length > 0 Then
    '                'txtVacancy.Text = dtTemp(0)("Vacancy").ToString
    '                'txtVacancy.Tag = dtTemp(0)("vacancyunkid").ToString
    '                txtOpeningDate.Text = eZeeDate.convertDate(dtTemp(0)("StDate").ToString).ToShortDateString
    '                txtClosingDate.Text = eZeeDate.convertDate(dtTemp(0)("EndDate").ToString).ToShortDateString
    '            End If
    '        Else
    '            ' txtVacancy.Text = "" : txtOpeningDate.Text = "" : txtClosingDate.Text = ""
    '            dgShortListApplicant.DataSource = Nothing : dgFinalApplicant.DataSource = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Vacancy Type is compulsory information.Please select vacancy type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancyType.Select()
                Return False

            ElseIf CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Vacancy is compulsory information.Please select vacancy."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboVacancy.Select()
                Return False

            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            mnuSubmitForApproval.Visible = User._Object.Privilege._AllowToSubmitForApprovalInFinalShortlistedApplicant
            btnApprove.Visible = User._Object.Privilege._AllowToApproveFinalShortListedApplicant
            btnDisapprove.Visible = User._Object.Privilege._AllowToDisapproveFinalShortListedApplicant

            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes
            btnUp.Visible = User._Object.Privilege._AllowToMoveFinalShortListedApplicantToShortListed
            'Pinkal (03-Jul-2013) -- End


            'Pinkal (12-Nov-2020) -- Start
            'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
            mnuSubmitForApproval.Visible = Not ConfigParameter._Object._SkipApprovalFlowForFinalApplicant
            mnuViewPendingApplicants.Visible = Not ConfigParameter._Object._SkipApprovalFlowForFinalApplicant
            mnuViewDisapprovedApplicants.Visible = Not ConfigParameter._Object._SkipApprovalFlowForFinalApplicant
            'Pinkal (12-Nov-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim mstrSearching As String = ""
        Try

            If CInt(cboVacancyType.SelectedValue) <= 0 Or CInt(cboVacancy.SelectedValue) <= 0 Then Exit Sub

            objCheckAll.Checked = False
            If CInt(cboRefNo.SelectedValue) > 0 Then
                mstrSearching = " AND  shortlistunkid = " & CInt(cboRefNo.SelectedValue)
            End If

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            'mdtTran = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", True, CInt(cboVacancy.SelectedValue), False, False, True)
            'mdtTran = New DataView(mdtTran, "isfinalshortlisted =  0 " & mstrSearching, "", DataViewRowState.CurrentRows).ToTable(True, _
            '                "ischecked", "finalapplicantunkid", "applicantunkid", "isshortlisted", "isfinalshortlisted", "ACode", "Aname", "Gender", "BDate", "Phone", "Email", "AUD", "app_statusid", "status", "app_issent", "refno")

            'FillShortListedApplicant()

            'If CInt(cboVacancy.SelectedValue) > 0 Then
            '    mdtFinalTran = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", False, CInt(cboVacancy.SelectedValue), False, True)
            '    mdtFinalTran = New DataView(mdtFinalTran, "isfinalshortlisted =  1", "", DataViewRowState.CurrentRows).ToTable(True, _
            '                "ischecked", "finalapplicantunkid", "applicantunkid", "isshortlisted", "isfinalshortlisted", "ACode", "Aname", "Gender", "BDate", "Phone", "Email", "AUD", "app_statusid", "status", "app_issent", "refno")

            '    For Each dr As DataRow In mdtFinalTran.Rows
            '        dr("isChecked") = True
            '    Next
            'Else
            '    If mdtFinalTran IsNot Nothing Then mdtFinalTran.Rows.Clear()
            'End If

            Dim dtFullTable As DataTable
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            'dtFullTable = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", True, CInt(cboVacancy.SelectedValue), False, False, True)
            dtFullTable = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", True, CInt(cboVacancy.SelectedValue), False, False, True, dtAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
            'Sohail (13 Mar 2020) -- End
            mdtTran = New DataView(dtFullTable, "isfinalshortlisted =  0 " & mstrSearching, "", DataViewRowState.CurrentRows).ToTable(True, _
                            "ischecked", "finalapplicantunkid", "applicantunkid", "isshortlisted", "isfinalshortlisted", "ACode", "Aname", "Gender", "BDate", "Phone", "Email", "AUD", "app_statusid", "status", "app_issent", "refno", "submitapproval")

            FillShortListedApplicant()

            If CInt(cboVacancy.SelectedValue) > 0 Then
                mdtFinalTran = New DataView(dtFullTable, "isfinalshortlisted =  1 " & mstrSearching, "", DataViewRowState.CurrentRows).ToTable(True, _
                            "ischecked", "finalapplicantunkid", "applicantunkid", "isshortlisted", "isfinalshortlisted", "ACode", "Aname", "Gender", "BDate", "Phone", "Email", "AUD", "app_statusid", "status", "app_issent", "refno", "submitapproval")

                If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
                    mdtFinalTran.Columns.Remove("ischecked")

                    Dim dCol As New DataColumn
                    With dCol
                        .DataType = GetType(System.Boolean)
                        .DefaultValue = True
                        .ColumnName = "ischecked"
                    End With
                    mdtFinalTran.Columns.Add(dCol)
                End If
            Else
                If mdtFinalTran IsNot Nothing Then mdtFinalTran.Rows.Clear()
            End If
            'S.SANDEEP [08-JUN-2018] -- END

            FillFinalApplicantList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetShortListApplicantParentChecked(ByVal intRowindex As Integer, ByVal intShortListId As Integer)
        Try
            Dim drRow() As DataRow = mdtTran.Select("shortlistunkid = " & intShortListId)
            Dim drCheckRow() As DataRow = mdtTran.Select("shortlistunkid = " & intShortListId & " AND ischecked = true AND IsGrp = false")

            If drRow.Length - 1 = drCheckRow.Length Then
                drRow(0)("ischecked") = CheckState.Checked
            Else
                drRow(0)("ischecked") = CheckState.Unchecked
            End If
            SetShortListApplicantCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetShortListApplicantParentChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub AssignToFinalApplicant()
        Try

            If mdtTran IsNot Nothing Then
                Dim dtTemp() As DataRow = mdtTran.Select("isChecked = true")

                If dtTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check atleast one short - listed applicant."), enMsgBoxStyle.Information)
                    Exit Sub
                End If



                'Pinkal (27-Jun-2013) -- Start
                'Enhancement : TRA Changes

                'If (btnApprove.Visible Or btnDisapprove.Visible) AndAlso mdtFinalTran IsNot Nothing Then
                '    Dim dtRow() As DataRow = mdtFinalTran.Select("Ischecked = True AND AUD <> 'D' AND (app_statusid = " & enShortListing_Status.SC_APPROVED & " OR app_statusid = " & enShortListing_Status.SC_REJECT & ")")
                '    If dtRow.Length > 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "You cannot do this operation.Reason: Some of the applicant(s) are in approved or disapproved status."), enMsgBoxStyle.Information)
                '        Exit Sub
                '    End If
                'End If

                'If (btnApprove.Visible Or btnDisapprove.Visible) AndAlso mdvFinalApplicant IsNot Nothing Then
                '    Dim dtRow() As DataRow = mdvFinalApplicant.ToTable.Select("Ischecked = True AND AUD <> 'D' AND (app_statusid = " & enShortListing_Status.SC_APPROVED & " OR app_statusid = " & enShortListing_Status.SC_REJECT & ")")
                '    If dtRow.Length > 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "You cannot do this operation.Reason: Some of the applicant(s) are in approved or disapproved status."), enMsgBoxStyle.Information)
                '        Exit Sub
                '    End If
                'End If

                'Pinkal (27-Jun-2013) -- End


                For i As Integer = 0 To dtTemp.Length - 1

                    If CInt(dtTemp(i)("applicantunkid")) > 0 Then
                        Dim objApplicant As New clsApplicant_master
                        objApplicant._Applicantunkid = CInt(dtTemp(i)("applicantunkid"))
                        Dim drRow As DataRow = mdtFinalTran.NewRow
                        drRow("isChecked") = True
                        drRow("finalapplicantunkid") = -1
                        'drRow("shortlistunkid") = CInt(dtTemp(i)("shortlistunkid"))
                        drRow("applicantunkid") = CInt(dtTemp(i)("applicantunkid"))
                        drRow("isshortlisted") = True
                        drRow("isfinalshortlisted") = False

                        'Pinkal (12-Nov-2020) -- Start
                        'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                        If objApplicant._Employeeunkid > 0 Then
                            Dim objEmp As New clsEmployee_Master
                            Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name
                            Dim dsList As DataSet = objEmp.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                                   , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, False _
                                                                                                                   , "List", objApplicant._Employeeunkid, False, "", False, False, False, False, Nothing, False)

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                drRow("Acode") = dsList.Tables(0).Rows(0)(Language.getMessage("clsEmployee_Master", 42, "Code")).ToString()
                            Else
                                drRow("Acode") = objApplicant._Applicant_Code
                            End If
                            dsList.Clear()
                            dsList = Nothing
                            objEmp = Nothing
                        Else
                        drRow("Acode") = objApplicant._Applicant_Code
                        End If
                        'Pinkal (12-Nov-2020) -- End


                        drRow("Aname") = objApplicant._Firstname & " " & objApplicant._Othername & " " & objApplicant._Surname
                        drRow("gender") = dtTemp(i)("gender").ToString()

                        If dtTemp(i)("Bdate").ToString().Trim <> "" Then
                            drRow("Bdate") = CDate(dtTemp(i)("Bdate")).ToShortDateString
                        Else
                            drRow("Bdate") = DBNull.Value
                        End If

                        drRow("phone") = dtTemp(i)("phone").ToString()
                        drRow("email") = dtTemp(i)("email").ToString()
                        drRow("app_statusid") = enShortListing_Status.SC_PENDING
                        drRow("status") = Language.getMessage("clsshortlist_finalapplicant", 7, "Pending")
                        drRow("app_issent") = False
                        drRow("refno") = dtTemp(i)("refno").ToString()
                        drRow("AUD") = "A"
                        mdtFinalTran.Rows.Add(drRow)
                    End If
                    mdtTran.Rows.Remove(dtTemp(i))
                Next

                If mdtTran.Rows.Count <= 0 Then
                    objCheckAll.Checked = False
                End If


                FillFinalApplicantList()


                'Pinkal (27-Jun-2013) -- Start
                'Enhancement : TRA Changes


                'If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
                '    Dim dr() As DataRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D'")
                '    If dgFinalApplicant.Rows.Count > 0 AndAlso dr.Length = dgFinalApplicant.Rows.Count Then
                '        objchkFinalSelectAll.Checked = True
                '    Else
                '        objchkFinalSelectAll.Checked = False
                '    End If
                'Else
                '    objchkFinalSelectAll.Checked = False
                'End If

                If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.ToTable.Rows.Count > 0 Then
                    Dim dr() As DataRow = mdvFinalApplicant.ToTable.Select("isChecked = true AND AUD <> 'D'")
                    If dgFinalApplicant.Rows.Count > 0 AndAlso dr.Length = dgFinalApplicant.Rows.Count Then
                        objchkFinalSelectAll.Checked = True
                    Else
                        objchkFinalSelectAll.Checked = False
                    End If
                Else
                    objchkFinalSelectAll.Checked = False
                End If


                'Pinkal (27-Jun-2013) -- End

                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    Dim dr() As DataRow = mdtTran.Select("ischecked = true")
                    If dr.Length = mdtTran.Rows.Count Then
                        objCheckAll.Checked = True
                    Else
                        objCheckAll.Checked = False
                    End If
                Else
                    objCheckAll.Checked = False
                End If

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check atleast one short - listed applicant."), enMsgBoxStyle.Information)
                Exit Sub
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AssignToFinalApplicant", mstrModuleName)
        End Try
    End Sub

    Private Sub SetShortListApplicantCheckBoxValue()
        Try
            Dim drRow As DataRow() = mdtTran.Select("isChecked = True")

            RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged

            If (drRow.Length <= 0 Or drRow.Length < dgShortListApplicant.Rows.Count) Then
                objCheckAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length = dgShortListApplicant.Rows.Count Then
                objCheckAll.CheckState = CheckState.Checked
            End If

            AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetShortListApplicantCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetFinalApplicantCheckBoxValue()
        Try


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'Dim drRow As DataRow() = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")

            'RemoveHandler objchkFinalSelectAll.CheckedChanged, AddressOf objchkFinalSelectAll_CheckedChanged

            'If (drRow.Length <= 0 Or drRow.Length < dgFinalApplicant.Rows.Count) Then
            '    objchkFinalSelectAll.CheckState = CheckState.Unchecked
            'ElseIf drRow.Length = dgFinalApplicant.Rows.Count Then
            '    objchkFinalSelectAll.CheckState = CheckState.Checked
            'End If

            'AddHandler objchkFinalSelectAll.CheckedChanged, AddressOf objchkFinalSelectAll_CheckedChanged

            Dim drRow As DataRow() = mdvFinalApplicant.ToTable.Select("isChecked = true AND AUD <> 'D' ")

            RemoveHandler objchkFinalSelectAll.CheckedChanged, AddressOf objchkFinalSelectAll_CheckedChanged

            If (drRow.Length <= 0 Or drRow.Length < dgFinalApplicant.Rows.Count) Then
                objchkFinalSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length = dgFinalApplicant.Rows.Count Then
                objchkFinalSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkFinalSelectAll.CheckedChanged, AddressOf objchkFinalSelectAll_CheckedChanged


            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFinalApplicantCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-May-2013) -- End

    'S.SANDEEP [08-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
    Private Sub SetButtonLocation()
        Try
            btnApprove.Location = New Point(484, 13)
            btnSave.Location = New Point(580, 13)
            btnDisapprove.Location = New Point(580, 13)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetButtonLocation", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [08-JUN-2018] -- END

#End Region

#Region " Form's Event(s) "

    Private Sub frmFinalApplicant_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objShortList = New clsshortlist_master
        objShortListFinal = New clsshortlist_finalapplicant
        Try
            Call Set_Logo(Me, gApplicationType)
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Hemant (02 Jul 2019) -- End
            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            SetVisibility()
            'Pinkal (12-May-2013) -- End
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalApplicant_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsshortlist_finalapplicant.SetMessages()
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 15 : Provide all recruitment notifications on language. They keep on changing on the contents they want.)
            'objfrm._Other_ModuleNames = "clsshortlist_finalapplicant"
            clsshortlist_finalapplicant.SetMessages()
            objfrm._Other_ModuleNames = "clsshortlist_finalapplicant,clsshortlist_finalapplicant"
            'Hemant (15 Nov 2019) -- End
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRefNo.SelectedValue = 0

            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            cboVacancyType.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            objCheckAll.Checked = False

            If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            If mdtFinalTran IsNot Nothing Then mdtFinalTran.Rows.Clear()
            'Pinkal (12-May-2013) -- End

            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If Validation() Then
                Call Fill_List()
            End If

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            cboStatus.SelectedValue = 0
            'Pinkal (12-Nov-2020) -- End


            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboRefNo.ValueMember
                .DisplayMember = cboRefNo.DisplayMember
                .DataSource = CType(cboRefNo.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboRefNo.SelectedValue = frm.SelectedValue
                cboRefNo.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'If Validation() = False Then Exit Sub

            'Dim drRow() As DataRow = mdtFinalTran.Select("isChecked = True AND AUD <> 'D'")

            'If drRow.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check atleast one applicant to do further operation on it"), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If objShortListFinal.Set_FinalApplicant(mdtFinalTran, CInt(cboVacancy.SelectedValue)) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Applicant(s) successfully assigned to final Applicant list."), enMsgBoxStyle.Information)
            '    Fill_List()
            '    'Me.Close()
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Applicant assignment process fail."), enMsgBoxStyle.Information)
            'End If

            If Validation() = False Then Exit Sub

            Dim drRow() As DataRow = mdvFinalApplicant.ToTable.Select("isChecked = True AND AUD <> 'D'")

            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please check atleast one applicant to do further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objShortListFinal.Set_FinalApplicant(mdvFinalApplicant.ToTable, CInt(cboVacancy.SelectedValue)) Then

                'Pinkal (12-Nov-2020) -- Start
                'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                If ConfigParameter._Object._SkipApprovalFlowForFinalApplicant Then
                    '/* START TO SUBMIT FOR APPROVAL AUTOMATICALLY
                    Dim mblnFlag As Boolean = False
                    For i As Integer = 0 To drRow.Length - 1
                        mblnFlag = objShortListFinal.UpdateApplicantForSubmitForApproval(CInt(cboVacancy.SelectedValue), CInt(drRow(i)("applicantunkid")))
                    Next

                    If mblnFlag = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Applicant assignment process fail."), enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        Fill_List()
                    End If

                    '/*END TO SUBMIT FOR APPROVAL AUTOMATICALLY

                    '/* START TO APPROVE APPLICANT AUTOMATICALLY
                    If objShortListFinal.ApproveFinalApplicant(CInt(cboVacancy.SelectedValue), mdvFinalApplicant.ToTable) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Applicant assignment process fail."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    '/* END TO APPROVE APPLICANT AUTOMATICALLY

                End If
                'Pinkal (12-Nov-2020) -- End

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Applicant(s) successfully assigned to final Applicant list."), enMsgBoxStyle.Information)
                Fill_List()
                'Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Final Applicant assignment process fail."), enMsgBoxStyle.Information)
            End If

            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchVacancyType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancyType.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboVacancyType.ValueMember
                .DisplayMember = cboVacancyType.DisplayMember
                .DataSource = CType(cboVacancyType.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboVacancyType.SelectedValue = frm.SelectedValue
                cboVacancyType.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancyType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancyType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btndown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndown.Click
        Try
            AssignToFinalApplicant()


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes
            cboStatus_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btndown_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        Try


            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            '            If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then

            '                Dim drRow() As DataRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")

            '                If drRow.Length <= 0 Then
            '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Short- listed applicant."), enMsgBoxStyle.Information)
            '                    Exit Sub
            '                End If

            '                If btnApprove.Visible Or btnDisapprove.Visible Then
            '                    Dim dtRow() As DataRow = mdtFinalTran.Select("Ischecked = True AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_APPROVED)
            '                    If dtRow.Length > 0 Then
            '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You cannot do this operation.Reason: Some of the applicant(s) are in approved status."), enMsgBoxStyle.Information)
            '                        Exit Sub

            '                    Else
            '                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "You are about to remove the selected applicant(s). from final shortlisting list, as a result they will be only considerd as shortlisted applicant(s) and will not go for furthur operations.Do you wish to continue?") _
            '                                           , CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

            '                            objShortListFinal.ChangeFinalshortListedApplicant_Status(CInt(cboVacancy.SelectedValue), mdtFinalTran)

            '                            GoTo AddToShortListingList
            '                        Else
            '                            Exit Sub
            '                        End If

            '                    End If

            '                Else
            '                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "You are about to remove the selected applicant(s). from final shortlisting list, as a result they will be only considerd as shortlisted applicant(s) and will not go for furthur operations.Do you wish to continue?") _
            '                                       , CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
            '                        Exit Sub
            '                    End If

            '                End If

            'AddToShortListingList:

            '                If drRow.Length > 0 Then

            '                    For i As Integer = 0 To drRow.Length - 1

            '                        If mdtTran IsNot Nothing Then
            '                            Dim dr As DataRow = Nothing
            '                            dr = mdtTran.NewRow
            '                            dr("ischecked") = CBool(drRow(i)("isChecked"))
            '                            dr("refno") = drRow(i)("ACode").ToString() & " - " & drRow(i)("AName").ToString()
            '                            dr("ACode") = drRow(i)("ACode").ToString()
            '                            dr("AName") = drRow(i)("AName").ToString()
            '                            dr("gender") = drRow(i)("gender").ToString()
            '                            dr("bdate") = CDate(drRow(i)("bdate")).ToShortDateString
            '                            dr("phone") = drRow(i)("phone").ToString()
            '                            dr("email") = drRow(i)("email").ToString()
            '                            dr("isshortlisted") = True
            '                            dr("isfinalshortlisted") = False
            '                            dr("app_statusid") = enShortListing_Status.SC_PENDING
            '                            dr("app_issent") = False
            '                            dr("status") = Language.getMessage("clsshortlist_finalapplicant", 7, "Pending")
            '                            dr("AUD") = ""
            '                            dr("applicantunkid") = CInt(drRow(i)("applicantunkid"))
            '                            mdtTran.Rows.Add(dr)
            '                            drRow(i)("AUD") = "D"
            '                        End If

            '                    Next
            '                    mdtFinalTran.AcceptChanges()
            '                End If


            '                FillFinalApplicantList()

            '                If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
            '                    Dim dr() As DataRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")
            '                    If dgFinalApplicant.Rows.Count > 0 AndAlso dr.Length = dgFinalApplicant.Rows.Count Then
            '                        objchkFinalSelectAll.Checked = True
            '                    Else
            '                        objchkFinalSelectAll.Checked = False
            '                    End If
            '                Else
            '                    objchkFinalSelectAll.Checked = False
            '                End If

            '                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
            '                    Dim dr() As DataRow = mdtTran.Select("ischecked = true")
            '                    If dr.Length = mdtTran.Rows.Count Then
            '                        objCheckAll.Checked = True
            '                    Else
            '                        objCheckAll.Checked = False
            '                    End If
            '                Else
            '                    objCheckAll.Checked = False
            '                End If
            '                dgShortListApplicant.DataSource = mdtTran

            '            Else
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Short- listed applicant."), enMsgBoxStyle.Information)
            '                Exit Sub

            '            End If


            If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.ToTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = Nothing

                If CInt(cboStatus.SelectedValue) > 0 Then
                    drRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' AND app_statusid = " & CInt(cboStatus.SelectedValue))
                Else
                    drRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")
                End If

                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Short- listed applicant."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If btnApprove.Visible Or btnDisapprove.Visible Then
                    Dim dtRow() As DataRow = mdvFinalApplicant.ToTable.Select("Ischecked = True AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_APPROVED)
                    If dtRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "You cannot do this operation.Reason: Some of the applicant(s) are in approved status."), enMsgBoxStyle.Information)
                        Exit Sub

                    Else
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "You are about to remove the selected applicant(s). from final shortlisting list, as a result they will be only considerd as shortlisted applicant(s) and will not go for furthur operations.Do you wish to continue?") _
                                           , CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                            objShortListFinal.ChangeFinalshortListedApplicant_Status(CInt(cboVacancy.SelectedValue), mdvFinalApplicant.ToTable)

                            GoTo AddToShortListingList
                        Else
                            Exit Sub
                        End If

                    End If

                Else
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "You are about to remove the selected applicant(s). from final shortlisting list, as a result they will be only considerd as shortlisted applicant(s) and will not go for furthur operations.Do you wish to continue?") _
                                       , CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    Else

                        'Pinkal (09-Jul-2013) -- Start
                        'Enhancement : TRA Changes
                        objShortListFinal.ChangeFinalshortListedApplicant_Status(CInt(cboVacancy.SelectedValue), mdvFinalApplicant.ToTable)
                        'Pinkal (09-Jul-2013) -- End

                    End If

                End If

AddToShortListingList:

                If drRow.Length > 0 Then

                    For i As Integer = 0 To drRow.Length - 1
                        If mdtTran IsNot Nothing Then
                            Dim dr As DataRow = Nothing
                            dr = mdtTran.NewRow
                            dr("ischecked") = CBool(drRow(i)("isChecked"))
                            dr("refno") = drRow(i)("ACode").ToString() & " - " & drRow(i)("AName").ToString()
                            dr("ACode") = drRow(i)("ACode").ToString()
                            dr("AName") = drRow(i)("AName").ToString()
                            dr("gender") = drRow(i)("gender").ToString()


                            'Pinkal (03-Jul-2013) -- Start
                            'Enhancement : TRA Changes

                            If dr("bdate").ToString().Trim <> "" Then
                                dr("bdate") = CDate(dr("bdate")).ToShortDateString
                            Else
                                dr("bdate") = DBNull.Value
                            End If

                            'Pinkal (03-Jul-2013) -- End

                            dr("phone") = drRow(i)("phone").ToString()
                            dr("email") = drRow(i)("email").ToString()
                            dr("isshortlisted") = True
                            dr("isfinalshortlisted") = False
                            dr("app_statusid") = enShortListing_Status.SC_PENDING
                            dr("app_issent") = False
                            dr("status") = Language.getMessage("clsshortlist_finalapplicant", 7, "Pending")
                            dr("AUD") = ""
                            dr("applicantunkid") = CInt(drRow(i)("applicantunkid"))
                            mdtTran.Rows.Add(dr)
                            drRow(i)("AUD") = "D"
                        End If
                    Next
                    mdtFinalTran.AcceptChanges()
                End If


                FillFinalApplicantList()

                If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.ToTable.Rows.Count > 0 Then
                    Dim dr() As DataRow = mdvFinalApplicant.ToTable.Select("isChecked = true AND AUD <> 'D' ")
                    If dgFinalApplicant.Rows.Count > 0 AndAlso dr.Length = dgFinalApplicant.Rows.Count Then
                        objchkFinalSelectAll.Checked = True
                    Else
                        objchkFinalSelectAll.Checked = False
                    End If
                Else
                    objchkFinalSelectAll.Checked = False
                End If

                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    Dim dr() As DataRow = mdtTran.Select("ischecked = true")
                    If dr.Length = mdtTran.Rows.Count Then
                        objCheckAll.Checked = True
                    Else
                        objCheckAll.Checked = False
                    End If
                Else
                    objCheckAll.Checked = False
                End If
                dgShortListApplicant.DataSource = mdtTran

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Check atleast one Final Short- listed applicant."), enMsgBoxStyle.Information)
                Exit Sub

            End If


            'Pinkal (27-Jun-2013) -- End


            If dgFinalApplicant.RowCount = 0 AndAlso (btnApprove.Visible Or btnDisapprove.Visible) Then
                btnApprove.Visible = False
                btnDisapprove.Visible = False
                btnSave.Visible = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
            '    Dim drRow() As DataRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")

            '    If drRow.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Final Short listed applicant to approve."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If


            '    drRow = mdtFinalTran.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_APPROVED)
            '    If drRow.Length > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot approve some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are already in approved status."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Are You sure want to approve final short listed applicant(s) ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

            '        If objShortListFinal.ApproveFinalApplicant(CInt(cboVacancy.SelectedValue), mdtFinalTran) Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Final shorted Applicant(s) approved successfully."), enMsgBoxStyle.Information)
            '            Me.Close()
            '        Else
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Final shorted Applicant(s) approval process fail ."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If

            '    End If

            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "There is no Final Short listed applicant to approve."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If



            If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.ToTable.Rows.Count > 0 Then

                Dim drCheck As DataRow() = mdvFinalApplicant.ToTable.Select("app_issent = 0")
                If drCheck.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "You cannot approve some of the short listed applicant(s).Reason : Some of the applicant(s) are not final shortlisted applicant.Please Filter applicant by status and save it as final applicant."), enMsgBoxStyle.Information)
                    cboStatus.Select()
                    Exit Sub
                End If

                Dim drRow() As DataRow = mdvFinalApplicant.ToTable.Select("isChecked = true AND AUD <> 'D' ")

                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Final Short listed applicant to approve."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


                drRow = mdvFinalApplicant.ToTable.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_APPROVED)
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot approve some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are already in approved status."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Are You sure want to approve final short listed applicant(s) ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    If objShortListFinal.ApproveFinalApplicant(CInt(cboVacancy.SelectedValue), mdvFinalApplicant.ToTable) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Final short listed Applicant(s) approved successfully."), enMsgBoxStyle.Information)
                        Me.Close()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Final short listed Applicant(s) approval process failed."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "There is no Final Short listed applicant to approve."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDisapprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        Try

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
            '    Dim drRow() As DataRow = mdtFinalTran.Select("isChecked = true AND AUD <> 'D' ")

            '    If drRow.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Check atleast one Final Short listed applicant to disapprove."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    drRow = mdtFinalTran.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_REJECT)
            '    If drRow.Length > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "You cannot disapprove some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are in disapproved status."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            '    ' START FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY

            '    Dim objBatch As New clsBatchSchedule
            '    Dim dtRow() As DataRow = mdtFinalTran.Select("ischecked = true AND AUD <> 'D' AND (app_statusid = " & enShortListing_Status.SC_PENDING & " OR app_statusid = " & enShortListing_Status.SC_APPROVED & ")")
            '    If dtRow.Length > 0 Then
            '        For i As Integer = 0 To dtRow.Length - 1
            '            Dim mdtBatchTran As DataTable = objBatch.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), CInt(dtRow(i)("applicantunkid")))
            '            If mdtBatchTran.Rows.Count > 0 Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "You cannot disapprove some of the applicant(s).Reason : some of the applicant(s) are already exist in batch scheduling transaction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '                Exit Sub
            '            End If
            '        Next
            '    End If

            '    ' END FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY


            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Are You sure want to disapprove final short listed applicant(s) ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '        If objShortListFinal.DisapproveFinalApplicant(CInt(cboVacancy.SelectedValue), mdtFinalTran) Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Final shorted Applicant(s) disapproved successfully."), enMsgBoxStyle.Information)
            '            Me.Close()
            '        Else
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Final shorted Applicant(s) disapproval process fail."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '    End If
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "There is no Final Short listed applicant to disapprove."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            If mdvFinalApplicant IsNot Nothing AndAlso mdvFinalApplicant.ToTable.Rows.Count > 0 Then


                Dim drCheck As DataRow() = mdvFinalApplicant.ToTable.Select("app_issent = 0")
                If drCheck.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "You cannot disapprove some of the short listed applincant(s).Reason : Some of the applicant(s) are not final shortlisted applicant.Please Filter applicant by status and save it as final applicant."), enMsgBoxStyle.Information)
                    cboStatus.Select()
                    Exit Sub
                End If

                Dim drRow() As DataRow = mdvFinalApplicant.ToTable.Select("isChecked = true AND AUD <> 'D' ")

                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please Check atleast one Final Short listed applicant to disapprove."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                drRow = mdvFinalApplicant.ToTable.Select("ischecked = true AND AUD <> 'D' AND app_statusid = " & enShortListing_Status.SC_REJECT)
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "You cannot disapprove some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are in disapproved status."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                ' START FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY

                Dim objBatch As New clsBatchSchedule
                Dim dtRow() As DataRow = mdvFinalApplicant.ToTable.Select("ischecked = true AND AUD <> 'D' AND (app_statusid = " & enShortListing_Status.SC_PENDING & " OR app_statusid = " & enShortListing_Status.SC_APPROVED & ")")
                If dtRow.Length > 0 Then
                    For i As Integer = 0 To dtRow.Length - 1
                        Dim mdtBatchTran As DataTable = objBatch.GetApplicantFromVacancy(CInt(cboVacancy.SelectedValue), CInt(dtRow(i)("applicantunkid")))
                        If mdtBatchTran.Rows.Count > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "You cannot disapprove some of the applicant(s).Reason : some of the applicant(s) are already exist in batch scheduling transaction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Sub
                        End If
                    Next
                End If

                ' END FOR CHECK WHETHER APPLICANT IS EXIST IN BATCH SCHEDULING FOR PARTICULAR VACANCY


                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Are You sure want to disapprove final short listed applicant(s) ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    If objShortListFinal.DisapproveFinalApplicant(CInt(cboVacancy.SelectedValue), mdvFinalApplicant.ToTable) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Final short listed Applicant(s) disapproved successfully."), enMsgBoxStyle.Information)
                        Me.Close()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Final short listed Applicant(s) approval process failed."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "There is no Final Short listed applicant to disapprove."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDisapprove_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-May-2013) -- End


#End Region

#Region " Control's Event(s) "

    Private Sub lnkAddtoExistingList_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddtoExistingList.LinkClicked
        Try
            AssignToFinalApplicant()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddtoExistingList_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                If objCheckAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In mdtTran.Rows
                        dr("isChecked") = objCheckAll.Checked
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objchkFinalSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkFinalSelectAll.CheckedChanged
        Try
            If mdtFinalTran IsNot Nothing AndAlso mdtFinalTran.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtFinalTran.Select("AUD <> 'D'")
                For Each dr As DataRow In drRow
                    dr("isChecked") = objchkFinalSelectAll.Checked
                    dr.AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkFinalSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-May-2013) -- End


#End Region

#Region "ComboBox Event"

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objVacancy.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue), True)
            Dim dsList As DataSet = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue), True)
            'Shani(24-Aug-2015) -- End

            cboVacancy.DisplayMember = "name"
            cboVacancy.ValueMember = "id"
            cboVacancy.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing

            dsList = objShortList.GetList("List", True)

            Dim dtList As DataTable = New DataView(dsList.Tables(0), "vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " AND statustypid = 1", "", DataViewRowState.CurrentRows).ToTable

            If CInt(cboVacancy.SelectedValue) > 0 Then
                Dim objvacacny As New clsVacancy
                objvacacny._Vacancyunkid = CInt(cboVacancy.SelectedValue)
                txtOpeningDate.Text = objvacacny._Openingdate.ToShortDateString
                txtClosingDate.Text = objvacacny._Closingdate.ToShortDateString
            Else
                txtOpeningDate.Text = ""
                txtClosingDate.Text = ""
            End If

            Dim drRow As DataRow = dtList.NewRow
            drRow("refno") = Language.getMessage(mstrModuleName, 5, "Select")
            drRow("shortlistunkid") = 0
            drRow("vacancyunkid") = 0
            drRow("statustypid") = 1
            dtList.Rows.InsertAt(drRow, 0)


            cboRefNo.ValueMember = "shortlistunkid"
            cboRefNo.DisplayMember = "refno"
            cboRefNo.DataSource = dtList


            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            'btnApprove.Location = New Point(383, 13)
            'btnSave.Location = New Point(479, 13)
            'btnDisapprove.Location = New Point(479, 13)
            Call SetButtonLocation()
            'S.SANDEEP [08-JUN-2018] -- END


            'Pinkal (03-Jul-2013) -- End

            If CInt(cboVacancy.SelectedValue) > 0 Then
                'Sohail (13 Mar 2020) -- Start
                'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
                'Dim dtTable As DataTable = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", False, CInt(cboVacancy.SelectedValue), False, True)
                Dim dtTable As DataTable = objShortListFinal.GetShortListFinalApplicant(-1, -1, "", False, CInt(cboVacancy.SelectedValue), False, True, dtAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString))
                'Sohail (13 Mar 2020) -- End
                dtTable = New DataView(dtTable, "app_issent = 1", "", DataViewRowState.CurrentRows).ToTable()
                If dtTable.Rows.Count > 0 Then

                    'Pinkal (12-Nov-2020) -- Start
                    'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                    If ConfigParameter._Object._SkipApprovalFlowForFinalApplicant = False Then
                    If User._Object.Privilege._AllowToApproveFinalShortListedApplicant Then
                        btnApprove.Visible = True
                    End If

                    If User._Object.Privilege._AllowToDisapproveFinalShortListedApplicant Then
                        btnDisapprove.Visible = True
                    End If

                    btnSave.Visible = False
                    End If
                    'Pinkal (12-Nov-2020) -- End

                Else
                    btnApprove.Visible = False
                    btnDisapprove.Visible = False
                    btnSave.Visible = True
                End If


                'Pinkal (03-Jul-2013) -- Start
                'Enhancement : TRA Changes

                If btnApprove.Visible AndAlso btnDisapprove.Visible = False Then
                    btnApprove.Location = btnSave.Location
                End If

                'Pinkal (03-Jul-2013) -- End

            Else
                btnSave.Visible = True
                btnApprove.Visible = False
                btnDisapprove.Visible = False
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (27-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            FillFinalApplicantList()
            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            'btnApprove.Location = New Point(383, 13)
            'btnSave.Location = New Point(479, 13)
            'btnDisapprove.Location = New Point(479, 13)
            Call SetButtonLocation()
            'S.SANDEEP [08-JUN-2018] -- END


            If mdvFinalApplicant IsNot Nothing Then
                Dim dtFinalApplicant As DataTable = mdvFinalApplicant.ToTable()
                Dim drRow() As DataRow = Nothing


                'Pinkal (12-Nov-2020) -- Start
                'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                If ConfigParameter._Object._SkipApprovalFlowForFinalApplicant = False Then

                If CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_PENDING Then

                    drRow = dtFinalApplicant.Select("app_issent = 0")
                    If drRow.Length > 0 Then
                        btnSave.Visible = True
                        btnApprove.Visible = False
                        btnDisapprove.Visible = False
                    Else
                        btnSave.Visible = False
                        btnApprove.Visible = User._Object.Privilege._AllowToApproveFinalShortListedApplicant
                        btnDisapprove.Visible = User._Object.Privilege._AllowToDisapproveFinalShortListedApplicant
                    End If

                ElseIf CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_APPROVED Then
                    btnApprove.Visible = False
                    btnSave.Visible = False
                    btnDisapprove.Visible = User._Object.Privilege._AllowToDisapproveFinalShortListedApplicant

                ElseIf CInt(cboStatus.SelectedValue) = enShortListing_Status.SC_REJECT Then
                    btnApprove.Visible = User._Object.Privilege._AllowToApproveFinalShortListedApplicant
                    btnDisapprove.Visible = False
                    btnSave.Visible = False
                    btnApprove.Location = btnSave.Location
                    'S.SANDEEP [08-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
                ElseIf CInt(cboStatus.SelectedValue) = 999 Then ' SUBMITTED FOR APPROVAL
                    drRow = dtFinalApplicant.Select("app_issent = 1")
                    If drRow.Length > 0 Then
                        btnSave.Visible = False
                        btnApprove.Visible = User._Object.Privilege._AllowToApproveFinalShortListedApplicant
                        btnDisapprove.Visible = User._Object.Privilege._AllowToDisapproveFinalShortListedApplicant
                    Else
                        btnSave.Visible = True
                        btnApprove.Visible = False
                        btnDisapprove.Visible = False
                    End If
                    'S.SANDEEP [08-JUN-2018] -- END
                Else
                    cboVacancy_SelectedIndexChanged(New Object(), New EventArgs())
                End If

            End If
                'Pinkal (12-Nov-2020) -- End

            End If 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (27-Jun-2013) -- End


#End Region

    'S.SANDEEP [08-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
#Region " TextBox Events "

    Private Sub txtSearchShortListed_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchShortListed.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgShortListApplicant.Rows.Count > 0 Then
                        If dgShortListApplicant.SelectedRows(0).Index = dgShortListApplicant.Rows(dgShortListApplicant.RowCount - 1).Index Then Exit Sub
                        dgShortListApplicant.Rows(dgShortListApplicant.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgShortListApplicant.Rows.Count > 0 Then
                        If dgShortListApplicant.SelectedRows(0).Index = 0 Then Exit Sub
                        dgShortListApplicant.Rows(dgShortListApplicant.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchShortListed_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchShortListed_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchShortListed.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchShortListed.Text.Trim.Length > 0 Then
                strSearch = colhApplicant.DataPropertyName & " LIKE '%" & txtSearchShortListed.Text & "%' OR " & _
                            colhGender.DataPropertyName & " LIKE '%" & txtSearchShortListed.Text & "%' OR " & _
                            colhBirthdate.DataPropertyName & " LIKE '%" & txtSearchShortListed.Text & "%' OR " & _
                            colhPhone.DataPropertyName & " LIKE '%" & txtSearchShortListed.Text & "%' OR " & _
                            colhEmail.DataPropertyName & " LIKE '%" & txtSearchShortListed.Text & "%' "
            End If
            If mdvShortListed IsNot Nothing Then mdvShortListed.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchShortListed_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchFinalShortListed_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchFinalShortListed.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgFinalApplicant.Rows.Count > 0 Then
                        If dgFinalApplicant.SelectedRows(0).Index = dgFinalApplicant.Rows(dgFinalApplicant.RowCount - 1).Index Then Exit Sub
                        dgFinalApplicant.Rows(dgFinalApplicant.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgFinalApplicant.Rows.Count > 0 Then
                        If dgFinalApplicant.SelectedRows(0).Index = 0 Then Exit Sub
                        dgFinalApplicant.Rows(dgFinalApplicant.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchFinalShortListed_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchFinalShortListed_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchFinalShortListed.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchFinalShortListed.Text.Trim.Length > 0 Then
                strSearch = colhFinalApplicantName.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhFinalGender.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhFinalBirthdate.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhFinalPhone.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhFinalEmail.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhdgStatus.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%' OR " & _
                            colhFinalApprovalSubmit.DataPropertyName & " LIKE '%" & txtSearchFinalShortListed.Text & "%'"
            End If
            If mdvFinalApplicant IsNot Nothing Then mdvFinalApplicant.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchFinalShortListed_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [08-JUN-2018] -- END

#Region "DataGridView Event"

    Private Sub dgShortListApplicant_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgShortListApplicant.CellContentClick, dgShortListApplicant.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgShortListApplicant.IsCurrentCellDirty Then
                Me.dgShortListApplicant.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            'If CBool(dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhShortListIsGrp.Index).Value) = True Then

            '    Select Case CInt(e.ColumnIndex)
            '        Case 0
            '            If dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhShortListIsCollaspe.Index).Value.ToString = "-" Then
            '                dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhShortListIsCollaspe.Index).Value = "+"
            '            Else
            '                dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhShortListIsCollaspe.Index).Value = "-"
            '            End If

            '            For i = e.RowIndex + 1 To dgShortListApplicant.RowCount - 1
            '                If CInt(dgShortListApplicant.Rows(e.RowIndex).Cells(objColhShortListId.Index).Value) = CInt(dgShortListApplicant.Rows(i).Cells(objColhShortListId.Index).Value) Then
            '                    If dgShortListApplicant.Rows(i).Visible = False Then
            '                        dgShortListApplicant.Rows(i).Visible = True
            '                    Else
            '                        dgShortListApplicant.Rows(i).Visible = False
            '                    End If
            '                Else
            '                    Exit For
            '                End If
            '            Next

            '        Case 1

            '            Dim blnFlg As Boolean = CBool(dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhSelect.Index).Value)
            '            Dim mstrSign As String = dgShortListApplicant.Rows(e.RowIndex).Cells(objcolhShortListIsCollaspe.Index).Value.ToString()
            '            Dim drRow() As DataRow = mdtTran.Select("shortlistunkid = " & CInt(dgShortListApplicant.Rows(e.RowIndex).Cells(objColhShortListId.Index).Value))

            '            If drRow.Length > 0 Then
            '                For k As Integer = 1 To drRow.Length - 1
            '                    dgShortListApplicant.Rows(e.RowIndex + k).Cells(objcolhSelect.Index).Value = blnFlg
            '                    mdtTran.Rows(e.RowIndex + k).AcceptChanges()
            '                Next
            '            End If
            '            mdtTran.AcceptChanges()
            '            SetShortListApplicantCheckBoxValue()
            '            SetGridColor(dgShortListApplicant, objcolhShortListIsGrp)
            '            SetCollapseValue(dgShortListApplicant, objcolhShortListIsGrp, objcolhShortListIsCollaspe, mstrSign)
            '    End Select

            'ElseIf e.ColumnIndex = objcolhSelect.Index Then
            mdtTran.Rows(e.RowIndex).AcceptChanges()
            SetShortListApplicantCheckBoxValue()
            'SetShortListApplicantParentChecked(e.RowIndex, CInt(dgShortListApplicant.Rows(e.RowIndex).Cells(objColhShortListId.Index).Value))
            ' End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgShortListApplicant_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgFinalApplicant_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFinalApplicant.CellContentClick, dgFinalApplicant.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgFinalApplicant.IsCurrentCellDirty Then
                Me.dgFinalApplicant.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objcolhFinalSelect.Index Then
                mdtFinalTran.Rows(e.RowIndex).AcceptChanges()
                SetFinalApplicantCheckBoxValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgFinalApplicant_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Contextmenu Event"

    Private Sub mnuViewShortListedApplicantCV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewShortListedApplicantCV.Click
        Try
            If dgShortListApplicant.SelectedRows.Count > 0 Then
                Dim objApplicant As New ArutiReports.clsApplicantCVReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                Dim intApplicantId As Integer = CInt(dgShortListApplicant.SelectedRows(0).Cells(objColhShortListApplicantId.Index).Value)
                If intApplicantId > 0 Then
                    objApplicant._ApplicantUnkid = intApplicantId
                    objApplicant._ShowJobHistory = True
                    objApplicant._ShowOther_Qualification = True
                    objApplicant._ShowOther_Skills = True
                    objApplicant._ShowProfessional_Qualification = True
                    objApplicant._ShowProfessional_Skills = True
                    objApplicant._ShowReferences = True


                    'Varsha (03 Jan 2018) -- Start                
                    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                    'This filter should allow user to report on all cv's shortlisted for that particular position. 
                    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                    objApplicant._ReportTypeId = 1
                    objApplicant._ReportTypeName = Language.getMessage("frmApplicantCVReport", 3, "Short Listed Applicant CV Report")
                    objApplicant._VacancyTypeId = CInt(cboVacancyType.SelectedValue)
                    objApplicant._VacancyId = CInt(cboVacancy.SelectedValue)
                    'Varsha (03 Jan 2018) -- End


                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                    'objApplicant.generateReport(0, enPrintAction.Preview)
                    objApplicant._DocumentPath = ConfigParameter._Object._Document_Path
                    objApplicant.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.Preview)
                    'Shani(24-Aug-2015) -- End

                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Select ShortListed Applicant to preview Applicant CV."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Select ShortListed Applicant to preview Applicant CV."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewShortListedApplicantCV_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuViewFinalShortListedApplicantCV_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewFinalShortListedApplicantCV.Click
        Try
            If dgFinalApplicant.SelectedRows.Count > 0 Then
                Dim objApplicant As New ArutiReports.clsApplicantCVReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                Dim intApplicantId As Integer = CInt(dgFinalApplicant.SelectedRows(0).Cells(objcolhdgFinalApplicantID.Index).Value)
                If intApplicantId > 0 Then
                    objApplicant._ApplicantUnkid = intApplicantId
                    objApplicant._ShowJobHistory = True
                    objApplicant._ShowOther_Qualification = True
                    objApplicant._ShowOther_Skills = True
                    objApplicant._ShowProfessional_Qualification = True
                    objApplicant._ShowProfessional_Skills = True
                    objApplicant._ShowReferences = True

                    'Varsha (03 Jan 2018) -- Start                
                    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
                    'This filter should allow user to report on all cv's shortlisted for that particular position. 
                    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
                    objApplicant._ReportTypeId = 2
                    objApplicant._ReportTypeName = Language.getMessage("frmApplicantCVReport", 4, "Final Short Listed Applicant CV Report")
                    objApplicant._VacancyTypeId = CInt(cboVacancyType.SelectedValue)
                    objApplicant._VacancyId = CInt(cboVacancy.SelectedValue)
                    'Varsha (03 Jan 2018) -- End

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                    'objApplicant.generateReport(0, enPrintAction.Preview)
                    objApplicant._DocumentPath = ConfigParameter._Object._Document_Path
                    objApplicant.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.Preview)
                    'Shani(24-Aug-2015) -- End

                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Please Select Final ShortListed Applicant to preview Applicant CV."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Please Select Final ShortListed Applicant to preview Applicant CV."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewFinalShortListedApplicantCV_Click", mstrModuleName)
        End Try
    End Sub


    'Anjan [ 29 May 2013 ] -- Start
    'ENHANCEMENT : Recruitment TRA changes requested by Andrew
    Private Sub mnuViewShortListedApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewShortListedApplicant.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyIdShortListReport = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(0, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              0, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Please Select Vacancy in order to preview Shortlisted Applicants"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewShortListedApplicant_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuFinalShortlistedApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFinalShortlistedApplicants.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objFinalApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objFinalApplicantReport._VacancyIdShortListReport = CInt(cboVacancy.SelectedValue)
                objFinalApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objFinalApplicantReport._VacancyName = cboVacancy.Text
                objFinalApplicantReport._IsFinalApplicant = True

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objFinalApplicantReport.generateReport(0, enPrintAction.Preview)
                objFinalApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                          User._Object._Userunkid, _
                                                          FinancialYear._Object._YearUnkid, _
                                                          Company._Object._Companyunkid, _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                                          ConfigParameter._Object._ExportReportPath, _
                                                          ConfigParameter._Object._OpenAfterExport, _
                                                          0, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Please Select Vacancy in order to preview Shortlisted Applicants"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuFinalShortlistedApplicants_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [ 29 May 2013 ] -- End


    Private Sub mnuSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitForApproval.Click
        Try

            'Pinkal (27-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'If CInt(cboVacancy.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Select vacancy to do further operation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'If dgFinalApplicant.RowCount <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no applicant for submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'Dim dFinalRow() As DataRow = mdtFinalTran.Select("ischecked = True AND AUD <> 'D' AND isfinalshortlisted = False")
            'If dFinalRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "You cannot do submit for approval.Reason : Some of the applicant(s) are not final short Listed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'Dim drRow() As DataRow = mdtFinalTran.Select("ischecked = True AND AUD <> 'D'")
            'If drRow.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please check all applicant in order to submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'Dim dtRow() As DataRow = mdtFinalTran.Select("ischecked = False AND AUD <> 'D'")
            'If dtRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please check all applicant in order to submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'dtRow = mdtFinalTran.Select("ischecked = True AND app_issent = True")
            'If dtRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot do submit for approval operation again. Reason : Notification is already been sent."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            'If objShortListFinal.SendNotification(cboVacancy.Text.Trim, cboVacancyType.Text.Trim) Then
            '    Dim blnflag As Boolean = False
            '    For i As Integer = 0 To drRow.Length - 1
            '        blnflag = objShortListFinal.UpdateApplicantForSubmitForApproval(CInt(cboVacancy.SelectedValue), CInt(drRow(i)("applicantunkid")))
            '    Next
            '    If blnflag Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Notification Sent Successfully."), enMsgBoxStyle.Information)
            '        Me.Close()
            '    Else
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Notification has not been sent successfully."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If

            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please Select vacancy to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If dgFinalApplicant.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "There is no applicant for submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim dFinalRow() As DataRow = mdvFinalApplicant.ToTable.Select("ischecked = True AND AUD <> 'D' AND isfinalshortlisted = False")
            If dFinalRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "You cannot do submit for approval.Reason : Some of the applicant(s) are not final short Listed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim drRow() As DataRow = mdvFinalApplicant.ToTable.Select("ischecked = True AND AUD <> 'D'")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please check all applicant in order to submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If



            'Pinkal (09-Jul-2013) -- Start
            'Enhancement : TRA Changes

            Dim dtRow() As DataRow = Nothing
            'Dim dtRow() As DataRow = mdvFinalApplicant.ToTable.Select("ischecked = False AND AUD <> 'D'")
            'If dtRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please check all applicant in order to submit for approval."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If

            ''Pinkal (09-Jul-2013) -- End

            dtRow = mdvFinalApplicant.ToTable.Select("ischecked = True AND app_issent = True")
            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot do submit for approval operation again. Reason : Notification has already been sent."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            If txtRemarks.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Sorry, You cannot do submit for approval operation. Reason : Please Enter Remarks."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            'Hemant (02 Jul 2019) -- End

            'S.SANDEEP [ 31 DEC 2013 ] -- START

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'If objShortListFinal.SendNotification(cboVacancy.Text.Trim, cboVacancyType.Text.Trim, CInt(cboVacancyType.SelectedValue), CInt(cboVacancy.SelectedValue), CInt(cboRefNo.SelectedValue), CInt(cboStatus.SelectedValue)) Then
            If objShortListFinal.SendNotification(cboVacancy.Text.Trim, cboVacancyType.Text.Trim, CInt(cboVacancyType.SelectedValue), CInt(cboVacancy.SelectedValue), CInt(cboRefNo.SelectedValue), CInt(cboStatus.SelectedValue), Company._Object._Companyunkid, txtRemarks.Text.Trim) Then
                'Hemant (02 Jul 2019) -- [txtRemarks.Text.Trim]  - Start/End
                'Sohail (30 Nov 2017) -- End
                'If objShortListFinal.SendNotification(cboVacancy.Text.Trim, cboVacancyType.Text.Trim) Then
                'S.SANDEEP [ 31 DEC 2013 ] -- END

                Dim blnflag As Boolean = False
                For i As Integer = 0 To drRow.Length - 1
                    blnflag = objShortListFinal.UpdateApplicantForSubmitForApproval(CInt(cboVacancy.SelectedValue), CInt(drRow(i)("applicantunkid")))
                Next
                If blnflag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Notification Sent Successfully."), enMsgBoxStyle.Information)
                    Me.Close()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Notification has not been sent successfully."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If


            'Pinkal (27-Jun-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitForApproval_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub mnuViewPendingApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewPendingApplicants.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyIdShortListReport = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._IsFinalApplicant = True
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_PENDING
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 341, "Pending")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(0, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              0, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Please Select Vacancy in order to preview Pending Shortlisted Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewPendingApplicants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuViewApprovedApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewApprovedApplicants.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyIdShortListReport = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._IsFinalApplicant = True
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_APPROVED
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 342, "Approved")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(0, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              0, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Please Select Vacancy in order to preview Approved Shortlisted Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewApprovedApplicants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuViewDisapprovedApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewDisapprovedApplicants.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyIdShortListReport = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._IsFinalApplicant = True
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_REJECT
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 340, "Disapproved")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(0, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              0, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Please Select Vacancy in order to preview Disapproved Shortlisted Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewDisapprovedApplicants_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Jun-2013) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.rbtnSplitButton.GradientBackColor = GUI._ButttonBackColor
            Me.rbtnSplitButton.GradientForeColor = GUI._ButttonFontColor

            Me.btndown.GradientBackColor = GUI._ButttonBackColor
            Me.btndown.GradientForeColor = GUI._ButttonFontColor

            Me.btnUp.GradientBackColor = GUI._ButttonBackColor
            Me.btnUp.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisapprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisapprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.lblFinalApplicant.Text = Language._Object.getCaption(Me.lblFinalApplicant.Name, Me.lblFinalApplicant.Text)
            Me.lblShortListApplicant.Text = Language._Object.getCaption(Me.lblShortListApplicant.Name, Me.lblShortListApplicant.Text)
            Me.lnkAddtoExistingList.Text = Language._Object.getCaption(Me.lnkAddtoExistingList.Name, Me.lnkAddtoExistingList.Text)
            Me.LblVacancyType.Text = Language._Object.getCaption(Me.LblVacancyType.Name, Me.LblVacancyType.Text)
            Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Language._Object.getCaption(Me.lblStartdate.Name, Me.lblStartdate.Text)
            Me.rbtnSplitButton.Text = Language._Object.getCaption(Me.rbtnSplitButton.Name, Me.rbtnSplitButton.Text)
            Me.mnuViewApplicantCV.Text = Language._Object.getCaption(Me.mnuViewApplicantCV.Name, Me.mnuViewApplicantCV.Text)
            Me.mnuViewShortListedApplicant.Text = Language._Object.getCaption(Me.mnuViewShortListedApplicant.Name, Me.mnuViewShortListedApplicant.Text)
            Me.mnuFinalShortlistedApplicants.Text = Language._Object.getCaption(Me.mnuFinalShortlistedApplicants.Name, Me.mnuFinalShortlistedApplicants.Text)
            Me.mnuViewPendingApplicants.Text = Language._Object.getCaption(Me.mnuViewPendingApplicants.Name, Me.mnuViewPendingApplicants.Text)
            Me.mnuViewApprovedApplicants.Text = Language._Object.getCaption(Me.mnuViewApprovedApplicants.Name, Me.mnuViewApprovedApplicants.Text)
            Me.mnuSubmitForApproval.Text = Language._Object.getCaption(Me.mnuSubmitForApproval.Name, Me.mnuSubmitForApproval.Text)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.btndown.Text = Language._Object.getCaption(Me.btndown.Name, Me.btndown.Text)
            Me.btnUp.Text = Language._Object.getCaption(Me.btnUp.Name, Me.btnUp.Text)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.Name, Me.btnDisapprove.Text)
            Me.mnuViewShortListedApplicantCV.Text = Language._Object.getCaption(Me.mnuViewShortListedApplicantCV.Name, Me.mnuViewShortListedApplicantCV.Text)
            Me.mnuViewFinalShortListedApplicantCV.Text = Language._Object.getCaption(Me.mnuViewFinalShortListedApplicantCV.Name, Me.mnuViewFinalShortListedApplicantCV.Text)
            Me.mnuViewDisapprovedApplicants.Text = Language._Object.getCaption(Me.mnuViewDisapprovedApplicants.Name, Me.mnuViewDisapprovedApplicants.Text)
            Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.Name, Me.LblStatus.Text)
            Me.colhApplicant.HeaderText = Language._Object.getCaption(Me.colhApplicant.Name, Me.colhApplicant.HeaderText)
            Me.colhGender.HeaderText = Language._Object.getCaption(Me.colhGender.Name, Me.colhGender.HeaderText)
            Me.colhBirthdate.HeaderText = Language._Object.getCaption(Me.colhBirthdate.Name, Me.colhBirthdate.HeaderText)
            Me.colhPhone.HeaderText = Language._Object.getCaption(Me.colhPhone.Name, Me.colhPhone.HeaderText)
            Me.colhEmail.HeaderText = Language._Object.getCaption(Me.colhEmail.Name, Me.colhEmail.HeaderText)
            Me.colhFinalApplicantName.HeaderText = Language._Object.getCaption(Me.colhFinalApplicantName.Name, Me.colhFinalApplicantName.HeaderText)
            Me.colhFinalGender.HeaderText = Language._Object.getCaption(Me.colhFinalGender.Name, Me.colhFinalGender.HeaderText)
            Me.colhFinalBirthdate.HeaderText = Language._Object.getCaption(Me.colhFinalBirthdate.Name, Me.colhFinalBirthdate.HeaderText)
            Me.colhFinalPhone.HeaderText = Language._Object.getCaption(Me.colhFinalPhone.Name, Me.colhFinalPhone.HeaderText)
            Me.colhFinalEmail.HeaderText = Language._Object.getCaption(Me.colhFinalEmail.Name, Me.colhFinalEmail.HeaderText)
            Me.colhdgStatus.HeaderText = Language._Object.getCaption(Me.colhdgStatus.Name, Me.colhdgStatus.HeaderText)
            Me.colhFinalApprovalSubmit.HeaderText = Language._Object.getCaption(Me.colhFinalApprovalSubmit.Name, Me.colhFinalApprovalSubmit.HeaderText)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Applicant(s) successfully assigned to final Applicant list.")
            Language.setMessage(mstrModuleName, 2, "Final Applicant assignment process fail.")
            Language.setMessage(mstrModuleName, 3, "Please Check atleast one short - listed applicant.")
            Language.setMessage(mstrModuleName, 4, "Please Check atleast one Final Short- listed applicant.")
            Language.setMessage(mstrModuleName, 5, "Select")
            Language.setMessage(mstrModuleName, 6, "Please Select ShortListed Applicant to preview Applicant CV.")
            Language.setMessage("clsshortlist_finalapplicant", 7, "Pending")
            Language.setMessage(mstrModuleName, 8, "Please Select vacancy to do further operation.")
            Language.setMessage(mstrModuleName, 9, "There is no applicant for submit for approval.")
            Language.setMessage(mstrModuleName, 10, "Please check all applicant in order to submit for approval.")
            Language.setMessage(mstrModuleName, 11, "Sorry, You cannot do submit for approval operation again. Reason : Notification has already been sent.")
            Language.setMessage(mstrModuleName, 12, "Notification Sent Successfully.")
            Language.setMessage(mstrModuleName, 13, "Please Check atleast one Final Short listed applicant to approve.")
            Language.setMessage(mstrModuleName, 14, "You cannot approve some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are already in approved status.")
            Language.setMessage(mstrModuleName, 15, "Are You sure want to approve final short listed applicant(s) ?")
            Language.setMessage(mstrModuleName, 16, "Final short listed Applicant(s) approved successfully.")
            Language.setMessage(mstrModuleName, 17, "Final short listed Applicant(s) approval process failed.")
            Language.setMessage(mstrModuleName, 18, "Please Check atleast one Final Short listed applicant to disapprove.")
            Language.setMessage(mstrModuleName, 19, "Are You sure want to disapprove final short listed applicant(s) ?")
            Language.setMessage(mstrModuleName, 20, "Final short listed Applicant(s) disapproved successfully.")
            Language.setMessage(mstrModuleName, 21, "Final short listed Applicant(s) approval process failed.")
            Language.setMessage(mstrModuleName, 22, "Vacancy Type is compulsory information.Please select vacancy type.")
            Language.setMessage(mstrModuleName, 23, "Vacancy is compulsory information.Please select vacancy.")
            Language.setMessage(mstrModuleName, 24, "There is no Final Short listed applicant to approve.")
            Language.setMessage(mstrModuleName, 25, "There is no Final Short listed applicant to disapprove.")
            Language.setMessage(mstrModuleName, 26, "You cannot do submit for approval.Reason : Some of the applicant(s) are not final short Listed.")
            Language.setMessage(mstrModuleName, 27, "Please check atleast one applicant to do further operation.")
            Language.setMessage(mstrModuleName, 28, "You are about to remove the selected applicant(s). from final shortlisting list, as a result they will be only considerd as shortlisted applicant(s) and will not go for furthur operations.Do you wish to continue?")
            Language.setMessage(mstrModuleName, 29, "You cannot disapprove some of the final shorted listed applincant(s).Reason : Some of the applicant(s) are in disapproved status.")
            Language.setMessage(mstrModuleName, 30, "You cannot disapprove some of the applicant(s).Reason : some of the applicant(s) are already exist in batch scheduling transaction.")
            Language.setMessage(mstrModuleName, 31, "You cannot do this operation.Reason: Some of the applicant(s) are in approved status.")
            Language.setMessage(mstrModuleName, 32, "Please Select Final ShortListed Applicant to preview Applicant CV.")
            Language.setMessage(mstrModuleName, 33, "Please Select Vacancy in order to preview Shortlisted Applicants")
            Language.setMessage(mstrModuleName, 34, "Please Select Vacancy in order to preview Pending Shortlisted Applicants.")
            Language.setMessage(mstrModuleName, 35, "Please Select Vacancy in order to preview Approved Shortlisted Applicants.")
            Language.setMessage(mstrModuleName, 36, "Please Select Vacancy in order to preview Disapproved Shortlisted Applicants.")
            Language.setMessage(mstrModuleName, 37, "Notification has not been sent successfully.")
            Language.setMessage(mstrModuleName, 39, "You cannot approve some of the short listed applicant(s).Reason : Some of the applicant(s) are not final shortlisted applicant.Please Filter applicant by status and save it as final applicant.")
            Language.setMessage(mstrModuleName, 40, "You cannot disapprove some of the short listed applincant(s).Reason : Some of the applicant(s) are not final shortlisted applicant.Please Filter applicant by status and save it as final applicant.")
            Language.setMessage(mstrModuleName, 41, "Sorry, You cannot do submit for approval operation. Reason : Please Enter Remarks.")
            Language.setMessage("clsMasterData", 340, "Disapproved")
            Language.setMessage("clsMasterData", 341, "Pending")
            Language.setMessage("clsMasterData", 342, "Approved")
            Language.setMessage("frmApplicantCVReport", 3, "Short Listed Applicant CV Report")
            Language.setMessage("frmApplicantCVReport", 4, "Final Short Listed Applicant CV Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class