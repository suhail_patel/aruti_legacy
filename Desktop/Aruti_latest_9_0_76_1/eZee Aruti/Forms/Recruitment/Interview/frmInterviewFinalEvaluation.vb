﻿Option Strict On

#Region "  Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmInterviewFinalEvaluation

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmInterviewFinalEvaluation"
    Private mblnCancel As Boolean = True
    Private mdtTable As DataTable
    Private objAnalysisMaster As clsInterviewAnalysis_master
    Private mstrAnalysisIds As String = String.Empty
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private mintVacancyId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal _sAnalysisId As String, ByVal _iVacId As Integer) As Boolean
        Try
            mstrAnalysisIds = _sAnalysisId
            mintVacancyId = _iVacId
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmFinalEvaluation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmFinalEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAnalysisMaster = New clsInterviewAnalysis_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call SetVisibility()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalEvaluation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmFinalEvaluation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAnalysisMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            btnApprove.Enabled = User._Object.Privilege._AllowtoApproveApplicantEligibility
            btnDisapprove.Enabled = User._Object.Privilege._AllowtoDisapproveApplicantEligibility
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dtList As New DataTable
        Try
            dtList = objAnalysisMaster.Get_DataTable(mintVacancyId, "List", mstrAnalysisIds)
            mdtTable = New DataView(dtList, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable

            dgvData.AutoGenerateColumns = False

            objdgcolhAnalysisId.DataPropertyName = "analysisunkid"
            objdgcolhAppBatchTranId.DataPropertyName = "appbatchscheduletranunkid"
            objdgcolhApplicantId.DataPropertyName = "applicantunkid"
            objdgcolhGrpId.DataPropertyName = "grp_id"
            objdgcolhVacancyId.DataPropertyName = "vacancyunkid"
            dgcolhAnalysisDate_AD.DataPropertyName = "analysis_date"
            dgcolhInterviewType_AD.DataPropertyName = "interview_type"
            dgcolhRemark_AD.DataPropertyName = "remark"
            dgcolhReviewer_AD.DataPropertyName = "reviewer"
            dgcolhScore_AD.DataPropertyName = "score"
            objdgcolhIsGrp.DataPropertyName = "is_grp"

            dgvData.DataSource = mdtTable

            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            Dim pCell As New clsMergeCell

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgvData.Rows(i).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                    dgvData.Rows(i).Visible = False
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDisapprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        Try
            Dim blnFlag As Boolean = False
            If mstrAnalysisIds.Trim.Length > 0 Then
                Dim objAppVac As New clsApplicant_Vacancy_Mapping
                For Each sKeyId As String In mstrAnalysisIds.ToString.Replace("'", "").Split(CChar(","))
                    objAnalysisMaster._Analysisunkid = CInt(sKeyId)
                    If objAppVac.isExist(objAnalysisMaster._Applicantunkid, mintVacancyId, , , True) = True Then
                        Dim objApplicant As New clsApplicant_master
                        objApplicant._Applicantunkid = objAnalysisMaster._Applicantunkid
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry you cannot disapprove the selected applicant [") & objApplicant._Firstname & " " & objApplicant._Surname & _
                                        Language.getMessage(mstrModuleName, 6, "]. Reason : Selected applicant is already imported as an employee."), enMsgBoxStyle.Information)
                        objApplicant = Nothing
                        Exit Sub
                    End If
                Next
                objAppVac = Nothing
                If txtRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Remark is mandatory information. Please enter remark to continue."), enMsgBoxStyle.Information)
                    txtRemark.Focus()
                    Exit Sub
                End If
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You are about to disapprove all eligible applicant. Due to this all disapproved applicant will be not available for import applicant as an employee.") & vbCrLf & _
                                                        Language.getMessage(mstrModuleName, 7, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    For Each sKeyId As String In mstrAnalysisIds.ToString.Replace("'", "").Split(CChar(","))
                        objAnalysisMaster._Analysisunkid = CInt(sKeyId)
                        objAnalysisMaster._Appr_Date = dtpFinalDate.Value
                        objAnalysisMaster._Appr_Remark = txtRemark.Text
                        objAnalysisMaster._Approveuserunkid = User._Object._Userunkid
                        objAnalysisMaster._Statustypid = enShortListing_Status.SC_REJECT
                        objAnalysisMaster._Issent = False
                        If objAnalysisMaster.Update() = True Then
                            blnFlag = True
                        Else
                            blnFlag = False
                        End If
                    Next
                End If
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Information successfully saved."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDisapprove_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim blnFlag As Boolean = False
            If mstrAnalysisIds.Trim.Length > 0 Then
                If txtRemark.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Remark is mandatory information. Please enter remark to continue."), enMsgBoxStyle.Information)
                    txtRemark.Focus()
                    Exit Sub
                End If

                'S.SANDEEP [ 09 OCT 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim dtmp() As DataRow = mdtTable.Select("is_grp = True")
                If dtmp.Length > 0 Then
                    Dim iTotal_Position, iTotal_Eligible As Integer
                    iTotal_Position = 0 : iTotal_Eligible = 0
                    objAnalysisMaster.Get_Eligible_Count(iTotal_Position, iTotal_Eligible, mintVacancyId)
                    If iTotal_Eligible + dtmp.Length > iTotal_Position Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Total position set for the selected vacancy is/are [ ") & iTotal_Position & _
                                           Language.getMessage(mstrModuleName, 9, " ] and total eligible application approved for this vacancy is/are [ ") & iTotal_Eligible & _
                                           Language.getMessage(mstrModuleName, 10, " ] and total remaining positions are [ ") & (iTotal_Position - iTotal_Eligible) & _
                                           Language.getMessage(mstrModuleName, 11, " ].") & vbCrLf & _
                                           Language.getMessage(mstrModuleName, 12, "You are trying to approve applicant more than the position set for this vacancy.") & vbCrLf & _
                                           Language.getMessage(mstrModuleName, 7, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP [ 09 OCT 2013 ] -- END

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant as an employee.") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 7, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    For Each sKeyId As String In mstrAnalysisIds.ToString.Replace("'", "").Split(CChar(","))
                        objAnalysisMaster._Analysisunkid = CInt(sKeyId)
                        objAnalysisMaster._Appr_Date = dtpFinalDate.Value
                        objAnalysisMaster._Appr_Remark = txtRemark.Text
                        objAnalysisMaster._Approveuserunkid = User._Object._Userunkid
                        objAnalysisMaster._Statustypid = enShortListing_Status.SC_APPROVED
                        If objAnalysisMaster.Update() = True Then
                            blnFlag = True
                        Else
                            blnFlag = False
                        End If
                    Next
                End If
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Information successfully saved."), enMsgBoxStyle.Information)
                    mblnCancel = False
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.btnDisapprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisapprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.Name, Me.btnDisapprove.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.eLine1.Text = Language._Object.getCaption(Me.eLine1.Name, Me.eLine1.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.dgcolhInterviewType_AD.HeaderText = Language._Object.getCaption(Me.dgcolhInterviewType_AD.Name, Me.dgcolhInterviewType_AD.HeaderText)
            Me.dgcolhAnalysisDate_AD.HeaderText = Language._Object.getCaption(Me.dgcolhAnalysisDate_AD.Name, Me.dgcolhAnalysisDate_AD.HeaderText)
            Me.dgcolhReviewer_AD.HeaderText = Language._Object.getCaption(Me.dgcolhReviewer_AD.Name, Me.dgcolhReviewer_AD.HeaderText)
            Me.dgcolhScore_AD.HeaderText = Language._Object.getCaption(Me.dgcolhScore_AD.Name, Me.dgcolhScore_AD.HeaderText)
            Me.dgcolhRemark_AD.HeaderText = Language._Object.getCaption(Me.dgcolhRemark_AD.Name, Me.dgcolhRemark_AD.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Remark is mandatory information. Please enter remark to continue.")
            Language.setMessage(mstrModuleName, 2, "You are about to approve all eligible applicant. Due to this all approved applicant will be available for import applicant as an employee.")
            Language.setMessage(mstrModuleName, 3, "Information successfully saved.")
            Language.setMessage(mstrModuleName, 4, "You are about to disapprove all eligible applicant. Due to this all disapproved applicant will be not available for import applicant as an employee.")
            Language.setMessage(mstrModuleName, 5, "Sorry you cannot disapprove the selected applicant [")
            Language.setMessage(mstrModuleName, 6, "]. Reason : Selected applicant is already imported as an employee.")
            Language.setMessage(mstrModuleName, 7, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 8, "Total position set for the selected vacancy is/are [")
            Language.setMessage(mstrModuleName, 9, " ] and total eligible application approved for this vacancy is/are [")
            Language.setMessage(mstrModuleName, 10, " ] and total remaining positions are [")
            Language.setMessage(mstrModuleName, 11, " ].")
            Language.setMessage(mstrModuleName, 12, "You are trying to approve applicant more than the postion set for this vacancy.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmInterviewFinalEvaluation1

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmInterviewFinalEvaluation"
'    Private mblnCancel As Boolean = True
'    Private mintAnalysisMasterUnkid As Integer = -1
'    Private mintAppBatchSchedulingUnkid As Integer = -1
'    Private mdtAnalysisTran As DataTable
'    Private objAnalysisMaster As clsInterviewAnalysis_master
'    Private objAnalysisTran As clsInteviewAnalysis_tran
'    Private objApplicant As clsApplicant_master
'    Private objInteviewer As clsinterviewer_tran
'    Private objAppBatchSchedule As clsApplicant_Batchschedule_Tran
'    Private objBatchSchedule As clsBatchSchedule


'    'Pinkal (07-Jan-2012) -- Start
'    'Enhancement : TRA Changes
'    Private mintVacancyunkid As Integer = 0
'    'Pinkal (07-Jan-2012) -- End

'    'S.SANDEEP [ 09 MAR 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mblnIsEdit As Boolean = False
'    Private mdtTab As DataTable
'    Private mintMaxInterviewerId As Integer = 0
'    'S.SANDEEP [ 09 MAR 2013 ] -- END

'    Private menAction As enAction
'#End Region

'#Region " Display Dialog "


'    'Pinkal (07-Jan-2012) -- Start
'    'Enhancement : TRA Changes

'    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intAppBatchScheduleId As Integer, ByVal intvacancyunkid As Integer) As Boolean
'    '    Try
'    '        mintAnalysisMasterUnkid = intUnkId
'    '        menAction = eAction
'    '        mintAppBatchSchedulingUnkid = intAppBatchScheduleId

'    '        Me.ShowDialog()

'    '        intUnkId = mintAnalysisMasterUnkid

'    '        Return Not mblnCancel
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'    '    End Try
'    'End Function

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intAppBatchScheduleId As Integer, ByVal intvacancyunkid As Integer, ByVal blnEdit As Boolean) As Boolean 'S.SANDEEP [ 09 MAR 2013 ] -- START -- END
'        Try
'            mintAnalysisMasterUnkid = intUnkId
'            menAction = eAction
'            mintAppBatchSchedulingUnkid = intAppBatchScheduleId
'            mintVacancyunkid = intvacancyunkid

'            'S.SANDEEP [ 09 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mblnIsEdit = blnEdit
'            'S.SANDEEP [ 09 MAR 2013 ] -- END

'            Me.ShowDialog()

'            intUnkId = mintAnalysisMasterUnkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'    'Pinkal (07-Jan-2012) -- End

'#End Region

'#Region " Form's Events "
'    Private Sub frmFinalEvaluation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        If Asc(e.KeyChar) = 27 Then
'            Me.Close()
'        ElseIf Asc(e.KeyChar) = 13 Then
'            Windows.Forms.SendKeys.Send("{Tab}")
'            e.Handled = True
'        End If
'    End Sub

'    Private Sub frmFinalEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objAnalysisMaster = New clsInterviewAnalysis_master

'        objAppBatchSchedule = New clsApplicant_Batchschedule_Tran
'        objInteviewer = New clsinterviewer_tran
'        objApplicant = New clsApplicant_master
'        objAnalysisTran = New clsInteviewAnalysis_tran
'        objBatchSchedule = New clsBatchSchedule
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Call Language.setLanguage(Me.Name)

'            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage



'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'chkMarkAsComplete.Visible = False
'            'chkMarkAsComplete.Checked = True
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'            'Call OtherSettings()
'            If menAction = enAction.EDIT_ONE Then
'                objAnalysisMaster._Analysisunkid = mintAnalysisMasterUnkid
'                objAppBatchSchedule._Appbatchscheduletranunkid = objAnalysisMaster._Appbatchscheduletranunkid
'                objApplicant._Applicantunkid = objAppBatchSchedule._Applicantunkid
'            Else
'                objAppBatchSchedule._Appbatchscheduletranunkid = mintAppBatchSchedulingUnkid
'                objApplicant._Applicantunkid = objAppBatchSchedule._Applicantunkid
'            End If
'            Call FillCombo()
'            Call FillList()
'            Call FillInfo()

'            objAnalysisTran._AnalysisUnkid = mintAnalysisMasterUnkid
'            mdtAnalysisTran = objAnalysisTran._DataTable

'            'S.SANDEEP [ 09 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objInteviewer._VacancyUnkid = mintVacancyunkid
'            mdtTab = objInteviewer._DataTable
'            Dim dTemp() As DataRow = mdtTab.Select("interviewer_level = '" & CInt(mdtTab.Compute("MAX(interviewer_level)", "")) & "'")
'            If dTemp.Length > 0 Then
'                mintMaxInterviewerId = CInt(dTemp(0).Item("interviewertranunkid"))
'            End If
'            'S.SANDEEP [ 09 MAR 2013 ] -- END

'            Call GetValue()

'            If lvAnalysisInfo.Items.Count > 0 Then lvAnalysisInfo.Items(0).Selected = True
'            lvAnalysisInfo.Select()

'            txtApplicantName.Focus()

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call SetVisibility()
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmFinalEvaluation_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmFinalEvaluation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objAnalysisMaster = Nothing
'        objAppBatchSchedule = Nothing
'        objInteviewer = Nothing
'        objApplicant = Nothing
'        objAnaLysisTran = Nothing
'    End Sub

'    Private Sub frmFinalEvaluation_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
'            Call btnSave.PerformClick()
'        End If
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()
'            clsInterviewAnalysis_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            txtInterviewType.BackColor = GUI.ColorComp
'            txtRemark.BackColor = GUI.ColorOptional
'            txtApplicantName.BackColor = GUI.ColorComp
'            cboInterviewer.BackColor = GUI.ColorComp
'            cboScore.BackColor = GUI.ColorComp
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            'If objInteviewer._Analysis_Date <> Nothing Then
'            'dtpFinalDate.Value = objAnalysisMaster._Analysis_Date
'            'End If


'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'chkMarkAsComplete.Checked = objAnalysisMaster._Iscomplete
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'            'S.SANDEEP [ 09 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If mblnIsEdit = True Then
'                If objAnalysisMaster._IsEligible = True Then
'                    radEligible.Checked = True
'                Else
'                    radNotEligible.Checked = True
'                End If
'            End If
'            'S.SANDEEP [ 09 MAR 2013 ] -- END

'            For Each dtRow As DataRow In mdtAnalysisTran.Rows
'                objAnalysisMaster._Analysisunkid = CInt(dtRow.Item("analysisunkid"))
'                If objAnalysisMaster._Iscomplete = True Then
'                    cboInterviewer.SelectedValue = CInt(dtRow.Item("interviewertranunkid"))
'                    cboScore.SelectedValue = CInt(dtRow.Item("resultcodeunkid"))
'                    txtRemark.Text = dtRow.Item("remark").ToString
'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            'objAnalysisMaster._Analysis_Date = dtpFinalDate.Value

'            objAnalysisMaster._Iscomplete = True
'            objAnalysisMaster._Appbatchscheduletranunkid = mintAppBatchSchedulingUnkid


'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If radEligible.Checked = True Then
'                objAnalysisMaster._IsEligible = True
'            ElseIf radNotEligible.Checked = True Then
'                objAnalysisMaster._IsEligible = False
'            End If
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'            'S.SANDEEP [ 09 MAR 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Dim dtRow As DataRow
'            'dtRow = mdtAnalysisTran.NewRow
'            'dtRow.Item("analysistranunkid") = -1
'            'dtRow.Item("analysisunkid") = mintAnalysisMasterUnkid
'            ''S.SANDEEP [ 09 MAR 2013 ] -- START
'            ''ENHANCEMENT : TRA CHANGES
'            ''dtRow.Item("interviewertranunkid") = CInt(cboInterviewer.SelectedValue)
'            'dtRow.Item("interviewertranunkid") = mintMaxInterviewerId
'            ''S.SANDEEP [ 09 MAR 2013 ] -- END
'            'dtRow.Item("analysis_date") = dtpFinalDate.Value
'            ''S.SANDEEP [ 25 DEC 2011 ] -- START
'            ''ENHANCEMENT : TRA CHANGES
'            ''dtRow.Item("resultcodeunkid") = CInt(cboScore.SelectedValue)
'            'dtRow.Item("resultcodeunkid") = 0
'            ''S.SANDEEP [ 25 DEC 2011 ] -- END
'            'dtRow.Item("remark") = txtRemark.Text
'            'dtRow.Item("AUD") = "A"
'            'dtRow.Item("GUID") = Guid.NewGuid.ToString
'            'mdtAnalysisTran.Rows.Add(dtRow)

'            If mblnIsEdit = False Then
'                Dim dtRow As DataRow
'                dtRow = mdtAnalysisTran.NewRow
'                dtRow.Item("analysistranunkid") = -1
'                dtRow.Item("analysisunkid") = mintAnalysisMasterUnkid
'                dtRow.Item("interviewertranunkid") = mintMaxInterviewerId
'                dtRow.Item("analysis_date") = dtpFinalDate.Value
'                dtRow.Item("resultcodeunkid") = 0
'                dtRow.Item("remark") = txtRemark.Text
'                dtRow.Item("AUD") = "A"
'                dtRow.Item("GUID") = Guid.NewGuid.ToString
'                mdtAnalysisTran.Rows.Add(dtRow)
'            End If
'            'S.SANDEEP [ 09 MAR 2013 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsList As New DataSet
'        Dim objResult As New clsresult_master

'        Try
'            objBatchSchedule._Batchscheduleunkid = objAppBatchSchedule._Batchscheduleunkid

'            dsList = objResult.getComboList("Result", True, objBatchSchedule._Resultgroupunkid)
'            With cboScore
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsList.Tables("Result")
'                .SelectedValue = 0
'            End With

'            dsList = objInteviewer.getComboList(CInt(objBatchSchedule._Vacancyunkid), True, "Interviewers", True, False)
'            With cboInterviewer
'                .ValueMember = "interviewertranunkid"
'                .DisplayMember = "NAME"
'                .DataSource = dsList.Tables("Interviewers")
'                .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillList()
'        Dim dsList As New DataSet
'        Dim lvItem As ListViewItem
'        Try


'            'Pinkal (07-Jan-2012) -- Start
'            'Enhancement : TRA Changes

'            'dsList = objAnalysisMaster.Get_Analysis_Data("AnalysisData", mintAppBatchSchedulingUnkid)
'            dsList = objAnalysisMaster.Get_Analysis_Data("AnalysisData", mintAppBatchSchedulingUnkid, mintVacancyunkid)

'            'Pinkal (07-Jan-2012) -- End



'            lvAnalysisInfo.Items.Clear()

'            For Each dtRow As DataRow In dsList.Tables("AnalysisData").Rows
'                lvItem = New ListViewItem


'                'Pinkal (07-Jan-2012) -- Start
'                'Enhancement : TRA Changes
'                'lvItem.Text = dtRow.Item("Reviewer").ToString
'                'lvItem.SubItems.Add(dtRow.Item("Score").ToString)
'                'lvItem.SubItems.Add(dtRow.Item("remark").ToString)
'                'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("analysisdate").ToString).ToShortDateString)


'                lvItem.Text = dtRow.Item("InterviewType").ToString
'                lvItem.SubItems.Add(dtRow.Item("Reviewer").ToString)
'                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("analysisdate").ToString).ToShortDateString)
'                lvItem.SubItems.Add(dtRow.Item("Score").ToString)
'                lvItem.SubItems.Add(dtRow.Item("remark").ToString)
'                'Pinkal (07-Jan-2012) -- End




'                lvAnalysisInfo.Items.Add(lvItem)

'                lvItem = Nothing
'            Next

'            lvAnalysisInfo.GroupingColumn = colhInterviewType
'            lvAnalysisInfo.DisplayGroups(True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillInfo()
'        Dim objCommonMaster As New clsCommon_Master
'        Try
'            objBatchSchedule._Batchscheduleunkid = objAppBatchSchedule._Batchscheduleunkid
'            objCommonMaster._Masterunkid = objBatchSchedule._Interviewtypeunkid
'            txtApplicantName.Text = objApplicant._Firstname & " " & objApplicant._Othername & " " & objApplicant._Surname


'            'Pinkal (07-Jan-2012) -- Start
'            'Enhancement : TRA Changes
'            'txtInterviewType.Text = objCommonMaster._Name
'            'Pinkal (07-Jan-2012) -- End



'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
'        End Try
'    End Sub

'    Private Function IsValid() As Boolean
'        Try
'            'S.SANDEEP [ 28 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If CInt(cboInterviewer.SelectedValue) <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Interviewer is compulsory information. Please select Interviewer to continue."), enMsgBoxStyle.Information)
'            '    cboInterviewer.Focus()
'            '    Return False
'            'End If
'            'S.SANDEEP [ 28 FEB 2012 ] -- END

'            If radEligible.Checked = False AndAlso radNotEligible.Checked = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Eligibility is mandatory information. Please mark Applicant as Eligible or Not Eligible."), enMsgBoxStyle.Information)
'                Return False
'            End If

'            'S.SANDEEP [ 17 AUG 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If radEligible.Checked = True Then
'                Dim iTotal_Position, iTotal_Eligible As Integer
'                iTotal_Position = 0 : iTotal_Eligible = 0
'                objAnalysisMaster.Get_Eligible_Count(iTotal_Position, iTotal_Eligible, mintVacancyunkid)
'                If iTotal_Eligible >= iTotal_Position Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot set this applicant as eligible. Reason : Position for this vacancy is exceeding the limit.") & vbCrLf & _
'                                                                            Language.getMessage(mstrModuleName, 3, "Please increase the no of position from vacancy."), enMsgBoxStyle.Information)
'                    Return False
'                End If
'            End If
'            'S.SANDEEP [ 17 AUG 2012 ] -- END

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
'        End Try
'    End Function

'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub SetVisibility()
'        Try
'            radEligible.Enabled = User._Object.Privilege._AllowtoMarkApplicantEligible
'            radNotEligible.Enabled = User._Object.Privilege._AllowtoMarkApplicantEligible
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END

'#End Region

'#Region " Button Events "
'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If IsValid() = False Then Exit Sub

'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objAnalysisMaster.Update(mdtAnalysisTran, True)
'            Else
'                blnFlag = objAnalysisMaster.Insert(mdtAnalysisTran, True)
'            End If

'            If blnFlag = True Then
'                mblnCancel = True
'                Me.Close()
'            End If



'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try

'    End Sub
'#End Region

'End Class