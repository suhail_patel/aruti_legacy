﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterviewAnalysis_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterviewAnalysis_AddEdit))
        Me.pnlTrainingAnalysis = New System.Windows.Forms.Panel
        Me.gbTrainingAnalysis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddResult = New eZee.Common.eZeeGradientButton
        Me.lvAnalysis = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhPosition = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.colhScore = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhResultId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhInterviewerId = New System.Windows.Forms.ColumnHeader
        Me.objcolhAnalysisDate = New System.Windows.Forms.ColumnHeader
        Me.txtApplicantName = New eZee.TextBox.AlphanumericTextBox
        Me.txtBatchCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.txtBatchName = New eZee.TextBox.AlphanumericTextBox
        Me.dtpAnalysisDate = New System.Windows.Forms.DateTimePicker
        Me.lblBatchCode = New System.Windows.Forms.Label
        Me.lblAnalysisDate = New System.Windows.Forms.Label
        Me.lnTraineeInfo = New eZee.Common.eZeeLine
        Me.txtInterviewType = New eZee.TextBox.AlphanumericTextBox
        Me.lblCourse = New System.Windows.Forms.Label
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblScore = New System.Windows.Forms.Label
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.objLine = New eZee.Common.eZeeLine
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnReviewerInfo = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.pnlOtherInfo = New System.Windows.Forms.Panel
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblOtherEmpContactNo = New System.Windows.Forms.Label
        Me.lblOtherCompany = New System.Windows.Forms.Label
        Me.lblOtherDeptName = New System.Windows.Forms.Label
        Me.objbtnOtherEmployee = New eZee.Common.eZeeGradientButton
        Me.cboOtherEmployee = New System.Windows.Forms.ComboBox
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPosition = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.pnlEmployeeInfo = New System.Windows.Forms.Panel
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objColon3 = New System.Windows.Forms.Label
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblEmployeeContactNo = New System.Windows.Forms.Label
        Me.lblCompanyValue = New System.Windows.Forms.Label
        Me.lblDepartmentValue = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTrainingAnalysis.SuspendLayout()
        Me.gbTrainingAnalysis.SuspendLayout()
        Me.pnlOtherInfo.SuspendLayout()
        Me.pnlEmployeeInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTrainingAnalysis
        '
        Me.pnlTrainingAnalysis.Controls.Add(Me.gbTrainingAnalysis)
        Me.pnlTrainingAnalysis.Controls.Add(Me.objFooter)
        Me.pnlTrainingAnalysis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTrainingAnalysis.Location = New System.Drawing.Point(0, 0)
        Me.pnlTrainingAnalysis.Name = "pnlTrainingAnalysis"
        Me.pnlTrainingAnalysis.Size = New System.Drawing.Size(899, 501)
        Me.pnlTrainingAnalysis.TabIndex = 0
        '
        'gbTrainingAnalysis
        '
        Me.gbTrainingAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.gbTrainingAnalysis.BorderColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.Checked = False
        Me.gbTrainingAnalysis.CollapseAllExceptThis = False
        Me.gbTrainingAnalysis.CollapsedHoverImage = Nothing
        Me.gbTrainingAnalysis.CollapsedNormalImage = Nothing
        Me.gbTrainingAnalysis.CollapsedPressedImage = Nothing
        Me.gbTrainingAnalysis.CollapseOnLoad = False
        Me.gbTrainingAnalysis.Controls.Add(Me.objbtnAddResult)
        Me.gbTrainingAnalysis.Controls.Add(Me.lvAnalysis)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtApplicantName)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtBatchCode)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblBatchName)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtBatchName)
        Me.gbTrainingAnalysis.Controls.Add(Me.dtpAnalysisDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblBatchCode)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblAnalysisDate)
        Me.gbTrainingAnalysis.Controls.Add(Me.lnTraineeInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtInterviewType)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblCourse)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblEmployeeName)
        Me.gbTrainingAnalysis.Controls.Add(Me.cboResult)
        Me.gbTrainingAnalysis.Controls.Add(Me.txtRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblRemark)
        Me.gbTrainingAnalysis.Controls.Add(Me.lblScore)
        Me.gbTrainingAnalysis.Controls.Add(Me.EZeeStraightLine1)
        Me.gbTrainingAnalysis.Controls.Add(Me.objLine)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnEdit)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnDelete)
        Me.gbTrainingAnalysis.Controls.Add(Me.lnReviewerInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.btnAdd)
        Me.gbTrainingAnalysis.Controls.Add(Me.objLine1)
        Me.gbTrainingAnalysis.Controls.Add(Me.radOthers)
        Me.gbTrainingAnalysis.Controls.Add(Me.radEmployee)
        Me.gbTrainingAnalysis.Controls.Add(Me.pnlOtherInfo)
        Me.gbTrainingAnalysis.Controls.Add(Me.pnlEmployeeInfo)
        Me.gbTrainingAnalysis.ExpandedHoverImage = Nothing
        Me.gbTrainingAnalysis.ExpandedNormalImage = Nothing
        Me.gbTrainingAnalysis.ExpandedPressedImage = Nothing
        Me.gbTrainingAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTrainingAnalysis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTrainingAnalysis.HeaderHeight = 25
        Me.gbTrainingAnalysis.HeaderMessage = ""
        Me.gbTrainingAnalysis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTrainingAnalysis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTrainingAnalysis.HeightOnCollapse = 0
        Me.gbTrainingAnalysis.LeftTextSpace = 0
        Me.gbTrainingAnalysis.Location = New System.Drawing.Point(12, 12)
        Me.gbTrainingAnalysis.Name = "gbTrainingAnalysis"
        Me.gbTrainingAnalysis.OpenHeight = 430
        Me.gbTrainingAnalysis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTrainingAnalysis.ShowBorder = True
        Me.gbTrainingAnalysis.ShowCheckBox = False
        Me.gbTrainingAnalysis.ShowCollapseButton = False
        Me.gbTrainingAnalysis.ShowDefaultBorderColor = True
        Me.gbTrainingAnalysis.ShowDownButton = False
        Me.gbTrainingAnalysis.ShowHeader = True
        Me.gbTrainingAnalysis.Size = New System.Drawing.Size(875, 430)
        Me.gbTrainingAnalysis.TabIndex = 33
        Me.gbTrainingAnalysis.Temp = 0
        Me.gbTrainingAnalysis.Text = "Interview Analysis"
        Me.gbTrainingAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddResult
        '
        Me.objbtnAddResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResult.BorderSelected = False
        Me.objbtnAddResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResult.Location = New System.Drawing.Point(842, 77)
        Me.objbtnAddResult.Name = "objbtnAddResult"
        Me.objbtnAddResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResult.TabIndex = 221
        '
        'lvAnalysis
        '
        Me.lvAnalysis.BackColorOnChecked = True
        Me.lvAnalysis.ColumnHeaders = Nothing
        Me.lvAnalysis.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhPosition, Me.colhCompany, Me.colhContactNo, Me.colhScore, Me.colhRemark, Me.objcolhResultId, Me.objcolhGUID, Me.objcolhInterviewerId, Me.objcolhAnalysisDate})
        Me.lvAnalysis.CompulsoryColumns = ""
        Me.lvAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAnalysis.FullRowSelect = True
        Me.lvAnalysis.GridLines = True
        Me.lvAnalysis.GroupingColumn = Nothing
        Me.lvAnalysis.HideSelection = False
        Me.lvAnalysis.Location = New System.Drawing.Point(22, 214)
        Me.lvAnalysis.MinColumnWidth = 50
        Me.lvAnalysis.MultiSelect = False
        Me.lvAnalysis.Name = "lvAnalysis"
        Me.lvAnalysis.OptionalColumns = ""
        Me.lvAnalysis.ShowMoreItem = False
        Me.lvAnalysis.ShowSaveItem = False
        Me.lvAnalysis.ShowSelectAll = True
        Me.lvAnalysis.ShowSizeAllColumnsToFit = True
        Me.lvAnalysis.Size = New System.Drawing.Size(841, 163)
        Me.lvAnalysis.Sortable = True
        Me.lvAnalysis.TabIndex = 219
        Me.lvAnalysis.UseCompatibleStateImageBehavior = False
        Me.lvAnalysis.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 145
        '
        'colhPosition
        '
        Me.colhPosition.Tag = "colhPosition"
        Me.colhPosition.Text = "Position/Department"
        Me.colhPosition.Width = 110
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 110
        '
        'colhContactNo
        '
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 110
        '
        'colhScore
        '
        Me.colhScore.Tag = "colhScore"
        Me.colhScore.Text = "Score"
        Me.colhScore.Width = 110
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 250
        '
        'objcolhResultId
        '
        Me.objcolhResultId.Tag = "objcolhResultId"
        Me.objcolhResultId.Text = ""
        Me.objcolhResultId.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'objcolhInterviewerId
        '
        Me.objcolhInterviewerId.Tag = "objcolhInterviewerId"
        Me.objcolhInterviewerId.Text = ""
        Me.objcolhInterviewerId.Width = 0
        '
        'objcolhAnalysisDate
        '
        Me.objcolhAnalysisDate.Text = "AnalysisDate"
        Me.objcolhAnalysisDate.Width = 0
        '
        'txtApplicantName
        '
        Me.txtApplicantName.Enabled = False
        Me.txtApplicantName.Flags = 0
        Me.txtApplicantName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicantName.Location = New System.Drawing.Point(123, 112)
        Me.txtApplicantName.Name = "txtApplicantName"
        Me.txtApplicantName.ReadOnly = True
        Me.txtApplicantName.Size = New System.Drawing.Size(136, 21)
        Me.txtApplicantName.TabIndex = 217
        '
        'txtBatchCode
        '
        Me.txtBatchCode.Enabled = False
        Me.txtBatchCode.Flags = 0
        Me.txtBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchCode.Location = New System.Drawing.Point(123, 58)
        Me.txtBatchCode.Name = "txtBatchCode"
        Me.txtBatchCode.Size = New System.Drawing.Size(136, 21)
        Me.txtBatchCode.TabIndex = 215
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(19, 88)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(97, 15)
        Me.lblBatchName.TabIndex = 213
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBatchName
        '
        Me.txtBatchName.Enabled = False
        Me.txtBatchName.Flags = 0
        Me.txtBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchName.Location = New System.Drawing.Point(123, 85)
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(136, 21)
        Me.txtBatchName.TabIndex = 212
        '
        'dtpAnalysisDate
        '
        Me.dtpAnalysisDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDate.Checked = False
        Me.dtpAnalysisDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAnalysisDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAnalysisDate.Location = New System.Drawing.Point(122, 166)
        Me.dtpAnalysisDate.Name = "dtpAnalysisDate"
        Me.dtpAnalysisDate.Size = New System.Drawing.Size(137, 21)
        Me.dtpAnalysisDate.TabIndex = 207
        '
        'lblBatchCode
        '
        Me.lblBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchCode.Location = New System.Drawing.Point(19, 61)
        Me.lblBatchCode.Name = "lblBatchCode"
        Me.lblBatchCode.Size = New System.Drawing.Size(97, 15)
        Me.lblBatchCode.TabIndex = 209
        Me.lblBatchCode.Text = "Batch Code"
        Me.lblBatchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAnalysisDate
        '
        Me.lblAnalysisDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnalysisDate.Location = New System.Drawing.Point(19, 169)
        Me.lblAnalysisDate.Name = "lblAnalysisDate"
        Me.lblAnalysisDate.Size = New System.Drawing.Size(97, 15)
        Me.lblAnalysisDate.TabIndex = 206
        Me.lblAnalysisDate.Text = "Date"
        Me.lblAnalysisDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnTraineeInfo
        '
        Me.lnTraineeInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnTraineeInfo.Location = New System.Drawing.Point(16, 38)
        Me.lnTraineeInfo.Name = "lnTraineeInfo"
        Me.lnTraineeInfo.Size = New System.Drawing.Size(243, 17)
        Me.lnTraineeInfo.TabIndex = 208
        Me.lnTraineeInfo.Text = "Applicant Info"
        '
        'txtInterviewType
        '
        Me.txtInterviewType.Flags = 0
        Me.txtInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInterviewType.Location = New System.Drawing.Point(122, 139)
        Me.txtInterviewType.Name = "txtInterviewType"
        Me.txtInterviewType.Size = New System.Drawing.Size(137, 21)
        Me.txtInterviewType.TabIndex = 205
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(19, 140)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(97, 15)
        Me.lblCourse.TabIndex = 203
        Me.lblCourse.Text = "Interview Type"
        Me.lblCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(19, 115)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(97, 15)
        Me.lblEmployeeName.TabIndex = 202
        Me.lblEmployeeName.Text = "Applicant"
        Me.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(663, 77)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(173, 21)
        Me.cboResult.TabIndex = 198
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(663, 104)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(200, 72)
        Me.txtRemark.TabIndex = 196
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(588, 104)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(69, 15)
        Me.lblRemark.TabIndex = 195
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScore
        '
        Me.lblScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(588, 80)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(69, 15)
        Me.lblScore.TabIndex = 192
        Me.lblScore.Text = "Score"
        Me.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(567, 73)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(15, 114)
        Me.EZeeStraightLine1.TabIndex = 190
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'objLine
        '
        Me.objLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine.Location = New System.Drawing.Point(8, 204)
        Me.objLine.Name = "objLine"
        Me.objLine.Size = New System.Drawing.Size(855, 7)
        Me.objLine.TabIndex = 187
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(663, 393)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 180
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(766, 393)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 181
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'lnReviewerInfo
        '
        Me.lnReviewerInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnReviewerInfo.Location = New System.Drawing.Point(278, 36)
        Me.lnReviewerInfo.Name = "lnReviewerInfo"
        Me.lnReviewerInfo.Size = New System.Drawing.Size(585, 17)
        Me.lnReviewerInfo.TabIndex = 186
        Me.lnReviewerInfo.Text = "Reviewer Info"
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(560, 393)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 179
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(265, 49)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(15, 138)
        Me.objLine1.TabIndex = 185
        Me.objLine1.Text = "EZeeStraightLine1"
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(419, 54)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(144, 17)
        Me.radOthers.TabIndex = 176
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(290, 56)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(258, 17)
        Me.radEmployee.TabIndex = 152
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'pnlOtherInfo
        '
        Me.pnlOtherInfo.Controls.Add(Me.Label3)
        Me.pnlOtherInfo.Controls.Add(Me.Label2)
        Me.pnlOtherInfo.Controls.Add(Me.Label1)
        Me.pnlOtherInfo.Controls.Add(Me.lblOtherEmpContactNo)
        Me.pnlOtherInfo.Controls.Add(Me.lblOtherCompany)
        Me.pnlOtherInfo.Controls.Add(Me.lblOtherDeptName)
        Me.pnlOtherInfo.Controls.Add(Me.objbtnOtherEmployee)
        Me.pnlOtherInfo.Controls.Add(Me.cboOtherEmployee)
        Me.pnlOtherInfo.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOtherInfo.Controls.Add(Me.lblCompany)
        Me.pnlOtherInfo.Controls.Add(Me.lblPosition)
        Me.pnlOtherInfo.Controls.Add(Me.lblName)
        Me.pnlOtherInfo.Location = New System.Drawing.Point(289, 79)
        Me.pnlOtherInfo.Name = "pnlOtherInfo"
        Me.pnlOtherInfo.Size = New System.Drawing.Size(275, 108)
        Me.pnlOtherInfo.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(94, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(8, 15)
        Me.Label3.TabIndex = 195
        Me.Label3.Text = ":"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(94, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(8, 15)
        Me.Label2.TabIndex = 194
        Me.Label2.Text = ":"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(94, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(8, 15)
        Me.Label1.TabIndex = 193
        Me.Label1.Text = ":"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherEmpContactNo
        '
        Me.lblOtherEmpContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEmpContactNo.Location = New System.Drawing.Point(108, 80)
        Me.lblOtherEmpContactNo.Name = "lblOtherEmpContactNo"
        Me.lblOtherEmpContactNo.Size = New System.Drawing.Size(154, 21)
        Me.lblOtherEmpContactNo.TabIndex = 191
        Me.lblOtherEmpContactNo.Text = "#OtherContact No"
        Me.lblOtherEmpContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherCompany
        '
        Me.lblOtherCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherCompany.Location = New System.Drawing.Point(108, 53)
        Me.lblOtherCompany.Name = "lblOtherCompany"
        Me.lblOtherCompany.Size = New System.Drawing.Size(154, 21)
        Me.lblOtherCompany.TabIndex = 190
        Me.lblOtherCompany.Text = "#OtherCompany"
        Me.lblOtherCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherDeptName
        '
        Me.lblOtherDeptName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherDeptName.Location = New System.Drawing.Point(108, 26)
        Me.lblOtherDeptName.Name = "lblOtherDeptName"
        Me.lblOtherDeptName.Size = New System.Drawing.Size(154, 21)
        Me.lblOtherDeptName.TabIndex = 189
        Me.lblOtherDeptName.Text = "#OtherEmpDepartmentName"
        Me.lblOtherDeptName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherEmployee
        '
        Me.objbtnOtherEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherEmployee.BorderSelected = False
        Me.objbtnOtherEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherEmployee.Image = CType(resources.GetObject("objbtnOtherEmployee.Image"), System.Drawing.Image)
        Me.objbtnOtherEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherEmployee.Location = New System.Drawing.Point(245, 3)
        Me.objbtnOtherEmployee.Name = "objbtnOtherEmployee"
        Me.objbtnOtherEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherEmployee.TabIndex = 186
        '
        'cboOtherEmployee
        '
        Me.cboOtherEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEmployee.FormattingEnabled = True
        Me.cboOtherEmployee.Location = New System.Drawing.Point(111, 1)
        Me.cboOtherEmployee.Name = "cboOtherEmployee"
        Me.cboOtherEmployee.Size = New System.Drawing.Size(129, 21)
        Me.cboOtherEmployee.TabIndex = 185
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(10, 85)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(94, 15)
        Me.lblTrainerContactNo.TabIndex = 168
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(10, 58)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(94, 15)
        Me.lblCompany.TabIndex = 166
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(10, 31)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(83, 15)
        Me.lblPosition.TabIndex = 165
        Me.lblPosition.Text = "Department"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(10, 4)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(94, 15)
        Me.lblName.TabIndex = 162
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeInfo
        '
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon3)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon2)
        Me.pnlEmployeeInfo.Controls.Add(Me.objColon1)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmpContact)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmpCompany)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblDepartment)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblEmployeeContactNo)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblCompanyValue)
        Me.pnlEmployeeInfo.Controls.Add(Me.lblDepartmentValue)
        Me.pnlEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.pnlEmployeeInfo.Location = New System.Drawing.Point(289, 79)
        Me.pnlEmployeeInfo.Name = "pnlEmployeeInfo"
        Me.pnlEmployeeInfo.Size = New System.Drawing.Size(270, 104)
        Me.pnlEmployeeInfo.TabIndex = 188
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(10, 4)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(94, 15)
        Me.lblEmployee.TabIndex = 195
        Me.lblEmployee.Text = "Name"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(94, 82)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 21)
        Me.objColon3.TabIndex = 194
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(94, 55)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 21)
        Me.objColon2.TabIndex = 193
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(94, 32)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 192
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(10, 82)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(72, 21)
        Me.lblEmpContact.TabIndex = 191
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(10, 55)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(72, 21)
        Me.lblEmpCompany.TabIndex = 190
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(10, 31)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(72, 15)
        Me.lblDepartment.TabIndex = 189
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeContactNo
        '
        Me.lblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeContactNo.Location = New System.Drawing.Point(108, 82)
        Me.lblEmployeeContactNo.Name = "lblEmployeeContactNo"
        Me.lblEmployeeContactNo.Size = New System.Drawing.Size(154, 21)
        Me.lblEmployeeContactNo.TabIndex = 188
        Me.lblEmployeeContactNo.Text = "#Contact No"
        Me.lblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCompanyValue
        '
        Me.lblCompanyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyValue.Location = New System.Drawing.Point(108, 55)
        Me.lblCompanyValue.Name = "lblCompanyValue"
        Me.lblCompanyValue.Size = New System.Drawing.Size(154, 21)
        Me.lblCompanyValue.TabIndex = 187
        Me.lblCompanyValue.Text = "#Company"
        Me.lblCompanyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartmentValue
        '
        Me.lblDepartmentValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentValue.Location = New System.Drawing.Point(108, 28)
        Me.lblDepartmentValue.Name = "lblDepartmentValue"
        Me.lblDepartmentValue.Size = New System.Drawing.Size(154, 21)
        Me.lblDepartmentValue.TabIndex = 186
        Me.lblDepartmentValue.Text = "#DepartmentName"
        Me.lblDepartmentValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(246, 3)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 185
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(111, 1)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(129, 21)
        Me.cboEmployee.TabIndex = 184
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 446)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(899, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(675, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 126
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(778, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 125
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmInterviewAnalysis_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(899, 501)
        Me.Controls.Add(Me.pnlTrainingAnalysis)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInterviewAnalysis_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Interview Analysis"
        Me.pnlTrainingAnalysis.ResumeLayout(False)
        Me.gbTrainingAnalysis.ResumeLayout(False)
        Me.gbTrainingAnalysis.PerformLayout()
        Me.pnlOtherInfo.ResumeLayout(False)
        Me.pnlEmployeeInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTrainingAnalysis As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbTrainingAnalysis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents lnReviewerInfo As eZee.Common.eZeeLine
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine As eZee.Common.eZeeLine
    Friend WithEvents pnlOtherInfo As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents pnlEmployeeInfo As System.Windows.Forms.Panel
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents lblCompanyValue As System.Windows.Forms.Label
    Friend WithEvents lblDepartmentValue As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents dtpAnalysisDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAnalysisDate As System.Windows.Forms.Label
    Friend WithEvents lnTraineeInfo As eZee.Common.eZeeLine
    Friend WithEvents txtInterviewType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents lblBatchCode As System.Windows.Forms.Label
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents txtBatchName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboOtherEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnOtherEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOtherEmpContactNo As System.Windows.Forms.Label
    Friend WithEvents lblOtherCompany As System.Windows.Forms.Label
    Friend WithEvents lblOtherDeptName As System.Windows.Forms.Label
    Friend WithEvents txtApplicantName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvAnalysis As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScore As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhResultId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInterviewerId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAnalysisDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objbtnAddResult As eZee.Common.eZeeGradientButton
End Class
