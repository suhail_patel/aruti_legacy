﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCancelRemark
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCancelRemark))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAccessInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCancelRemark = New System.Windows.Forms.Label
        Me.txtCancelRemark = New eZee.TextBox.AlphanumericTextBox
        Me.txtBatchName = New eZee.TextBox.AlphanumericTextBox
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.txtApplicantName = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplicantName = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbAccessInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.gbAccessInfo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(384, 221)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 166)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(384, 55)
        Me.objFooter.TabIndex = 8
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(172, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(275, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbAccessInfo
        '
        Me.gbAccessInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAccessInfo.Checked = False
        Me.gbAccessInfo.CollapseAllExceptThis = False
        Me.gbAccessInfo.CollapsedHoverImage = Nothing
        Me.gbAccessInfo.CollapsedNormalImage = Nothing
        Me.gbAccessInfo.CollapsedPressedImage = Nothing
        Me.gbAccessInfo.CollapseOnLoad = False
        Me.gbAccessInfo.Controls.Add(Me.txtApplicantName)
        Me.gbAccessInfo.Controls.Add(Me.lblApplicantName)
        Me.gbAccessInfo.Controls.Add(Me.lblCancelRemark)
        Me.gbAccessInfo.Controls.Add(Me.txtCancelRemark)
        Me.gbAccessInfo.Controls.Add(Me.txtBatchName)
        Me.gbAccessInfo.Controls.Add(Me.lblBatchName)
        Me.gbAccessInfo.ExpandedHoverImage = Nothing
        Me.gbAccessInfo.ExpandedNormalImage = Nothing
        Me.gbAccessInfo.ExpandedPressedImage = Nothing
        Me.gbAccessInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAccessInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAccessInfo.HeaderHeight = 25
        Me.gbAccessInfo.HeightOnCollapse = 0
        Me.gbAccessInfo.LeftTextSpace = 0
        Me.gbAccessInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbAccessInfo.Name = "gbAccessInfo"
        Me.gbAccessInfo.OpenHeight = 300
        Me.gbAccessInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAccessInfo.ShowBorder = True
        Me.gbAccessInfo.ShowCheckBox = False
        Me.gbAccessInfo.ShowCollapseButton = False
        Me.gbAccessInfo.ShowDefaultBorderColor = True
        Me.gbAccessInfo.ShowDownButton = False
        Me.gbAccessInfo.ShowHeader = True
        Me.gbAccessInfo.Size = New System.Drawing.Size(360, 151)
        Me.gbAccessInfo.TabIndex = 1
        Me.gbAccessInfo.Temp = 0
        Me.gbAccessInfo.Text = "Cancel Remark"
        Me.gbAccessInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCancelRemark
        '
        Me.lblCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelRemark.Location = New System.Drawing.Point(8, 90)
        Me.lblCancelRemark.Name = "lblCancelRemark"
        Me.lblCancelRemark.Size = New System.Drawing.Size(82, 15)
        Me.lblCancelRemark.TabIndex = 13
        Me.lblCancelRemark.Text = "Cancel Remark"
        Me.lblCancelRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCancelRemark
        '
        Me.txtCancelRemark.Flags = 0
        Me.txtCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancelRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancelRemark.Location = New System.Drawing.Point(96, 87)
        Me.txtCancelRemark.Multiline = True
        Me.txtCancelRemark.Name = "txtCancelRemark"
        Me.txtCancelRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCancelRemark.Size = New System.Drawing.Size(252, 55)
        Me.txtCancelRemark.TabIndex = 11
        '
        'txtBatchName
        '
        Me.txtBatchName.Flags = 0
        Me.txtBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchName.Location = New System.Drawing.Point(96, 33)
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(117, 21)
        Me.txtBatchName.TabIndex = 8
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(8, 36)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(82, 15)
        Me.lblBatchName.TabIndex = 1
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApplicantName
        '
        Me.txtApplicantName.Flags = 0
        Me.txtApplicantName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicantName.Location = New System.Drawing.Point(96, 60)
        Me.txtApplicantName.Name = "txtApplicantName"
        Me.txtApplicantName.Size = New System.Drawing.Size(117, 21)
        Me.txtApplicantName.TabIndex = 16
        '
        'lblApplicantName
        '
        Me.lblApplicantName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicantName.Location = New System.Drawing.Point(8, 63)
        Me.lblApplicantName.Name = "lblApplicantName"
        Me.lblApplicantName.Size = New System.Drawing.Size(82, 15)
        Me.lblApplicantName.TabIndex = 15
        Me.lblApplicantName.Text = "Applicant Name"
        Me.lblApplicantName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCancelRemark
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 221)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCancelRemark"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Cancel Remark"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbAccessInfo.ResumeLayout(False)
        Me.gbAccessInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbAccessInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCancelRemark As System.Windows.Forms.Label
    Friend WithEvents txtCancelRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtApplicantName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicantName As System.Windows.Forms.Label
End Class
