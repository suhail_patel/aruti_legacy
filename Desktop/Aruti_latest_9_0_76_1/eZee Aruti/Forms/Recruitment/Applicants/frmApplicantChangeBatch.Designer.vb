﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantChangeBatch
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantChangeBatch))
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.txtVacancy = New System.Windows.Forms.Label
        Me.lblOldInterviewType = New System.Windows.Forms.Label
        Me.txtInterviewType = New System.Windows.Forms.Label
        Me.lblInterviewDate = New System.Windows.Forms.Label
        Me.txtInterviewDate = New System.Windows.Forms.Label
        Me.lblLocation = New System.Windows.Forms.Label
        Me.txtLocation = New System.Windows.Forms.Label
        Me.lblApplicantName = New System.Windows.Forms.Label
        Me.cboBatchName = New System.Windows.Forms.ComboBox
        Me.lblNewBatch = New System.Windows.Forms.Label
        Me.gbChangeBatchInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOldBatchName = New System.Windows.Forms.Label
        Me.lblOldBatchName = New System.Windows.Forms.Label
        Me.elineOldBatchInfo = New eZee.Common.eZeeLine
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.elNewBatchInfo = New eZee.Common.eZeeLine
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.txtNewInterviewType = New System.Windows.Forms.Label
        Me.lblNewVacancy = New System.Windows.Forms.Label
        Me.lblNewInterviewType = New System.Windows.Forms.Label
        Me.txtApplicantName = New System.Windows.Forms.Label
        Me.lblNewLocation = New System.Windows.Forms.Label
        Me.txtNewLocation = New System.Windows.Forms.Label
        Me.txtNewInterviewDate = New System.Windows.Forms.Label
        Me.lblNewInterviewDate = New System.Windows.Forms.Label
        Me.txtNewVacancy = New System.Windows.Forms.Label
        Me.pnlChangeBatch = New System.Windows.Forms.Panel
        Me.EZeeFooter1.SuspendLayout()
        Me.gbChangeBatchInfo.SuspendLayout()
        Me.pnlChangeBatch.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(294, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 157
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(397, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 156
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 244)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(507, 55)
        Me.EZeeFooter1.TabIndex = 158
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(9, 76)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(85, 13)
        Me.lblVacancy.TabIndex = 159
        Me.lblVacancy.Text = "Vacancy"
        '
        'txtVacancy
        '
        Me.txtVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVacancy.Location = New System.Drawing.Point(101, 76)
        Me.txtVacancy.Name = "txtVacancy"
        Me.txtVacancy.Size = New System.Drawing.Size(135, 13)
        Me.txtVacancy.TabIndex = 160
        Me.txtVacancy.Text = "Label2"
        '
        'lblOldInterviewType
        '
        Me.lblOldInterviewType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOldInterviewType.Location = New System.Drawing.Point(257, 76)
        Me.lblOldInterviewType.Name = "lblOldInterviewType"
        Me.lblOldInterviewType.Size = New System.Drawing.Size(86, 13)
        Me.lblOldInterviewType.TabIndex = 162
        Me.lblOldInterviewType.Text = "Interview Type"
        '
        'txtInterviewType
        '
        Me.txtInterviewType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewType.Location = New System.Drawing.Point(348, 76)
        Me.txtInterviewType.Name = "txtInterviewType"
        Me.txtInterviewType.Size = New System.Drawing.Size(146, 13)
        Me.txtInterviewType.TabIndex = 161
        Me.txtInterviewType.Text = "Label4"
        '
        'lblInterviewDate
        '
        Me.lblInterviewDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewDate.Location = New System.Drawing.Point(257, 52)
        Me.lblInterviewDate.Name = "lblInterviewDate"
        Me.lblInterviewDate.Size = New System.Drawing.Size(85, 13)
        Me.lblInterviewDate.TabIndex = 164
        Me.lblInterviewDate.Text = "Interview Date"
        '
        'txtInterviewDate
        '
        Me.txtInterviewDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewDate.Location = New System.Drawing.Point(348, 52)
        Me.txtInterviewDate.Name = "txtInterviewDate"
        Me.txtInterviewDate.Size = New System.Drawing.Size(146, 13)
        Me.txtInterviewDate.TabIndex = 163
        Me.txtInterviewDate.Text = "Label6"
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(9, 100)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(68, 13)
        Me.lblLocation.TabIndex = 166
        Me.lblLocation.Text = "Location"
        '
        'txtLocation
        '
        Me.txtLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.Location = New System.Drawing.Point(101, 100)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(135, 13)
        Me.txtLocation.TabIndex = 165
        Me.txtLocation.Text = "Label8"
        '
        'lblApplicantName
        '
        Me.lblApplicantName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicantName.Location = New System.Drawing.Point(9, 52)
        Me.lblApplicantName.Name = "lblApplicantName"
        Me.lblApplicantName.Size = New System.Drawing.Size(86, 13)
        Me.lblApplicantName.TabIndex = 167
        Me.lblApplicantName.Text = "Applicant Name"
        '
        'cboBatchName
        '
        Me.cboBatchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatchName.DropDownWidth = 200
        Me.cboBatchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatchName.FormattingEnabled = True
        Me.cboBatchName.Location = New System.Drawing.Point(101, 156)
        Me.cboBatchName.Name = "cboBatchName"
        Me.cboBatchName.Size = New System.Drawing.Size(132, 21)
        Me.cboBatchName.TabIndex = 168
        '
        'lblNewBatch
        '
        Me.lblNewBatch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewBatch.Location = New System.Drawing.Point(9, 160)
        Me.lblNewBatch.Name = "lblNewBatch"
        Me.lblNewBatch.Size = New System.Drawing.Size(85, 13)
        Me.lblNewBatch.TabIndex = 169
        Me.lblNewBatch.Text = "Batch Name"
        '
        'gbChangeBatchInfo
        '
        Me.gbChangeBatchInfo.BorderColor = System.Drawing.Color.Black
        Me.gbChangeBatchInfo.Checked = False
        Me.gbChangeBatchInfo.CollapseAllExceptThis = False
        Me.gbChangeBatchInfo.CollapsedHoverImage = Nothing
        Me.gbChangeBatchInfo.CollapsedNormalImage = Nothing
        Me.gbChangeBatchInfo.CollapsedPressedImage = Nothing
        Me.gbChangeBatchInfo.CollapseOnLoad = False
        Me.gbChangeBatchInfo.Controls.Add(Me.txtOldBatchName)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblOldBatchName)
        Me.gbChangeBatchInfo.Controls.Add(Me.elineOldBatchInfo)
        Me.gbChangeBatchInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbChangeBatchInfo.Controls.Add(Me.elNewBatchInfo)
        Me.gbChangeBatchInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtNewInterviewType)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblNewVacancy)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblNewInterviewType)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtApplicantName)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblNewLocation)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblNewBatch)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtNewLocation)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblVacancy)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtNewInterviewDate)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblNewInterviewDate)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtNewVacancy)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblApplicantName)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtInterviewType)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtVacancy)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblOldInterviewType)
        Me.gbChangeBatchInfo.Controls.Add(Me.cboBatchName)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblLocation)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtInterviewDate)
        Me.gbChangeBatchInfo.Controls.Add(Me.lblInterviewDate)
        Me.gbChangeBatchInfo.Controls.Add(Me.txtLocation)
        Me.gbChangeBatchInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbChangeBatchInfo.ExpandedHoverImage = Nothing
        Me.gbChangeBatchInfo.ExpandedNormalImage = Nothing
        Me.gbChangeBatchInfo.ExpandedPressedImage = Nothing
        Me.gbChangeBatchInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbChangeBatchInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbChangeBatchInfo.HeaderHeight = 25
        Me.gbChangeBatchInfo.HeaderMessage = ""
        Me.gbChangeBatchInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbChangeBatchInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbChangeBatchInfo.HeightOnCollapse = 0
        Me.gbChangeBatchInfo.LeftTextSpace = 0
        Me.gbChangeBatchInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbChangeBatchInfo.Name = "gbChangeBatchInfo"
        Me.gbChangeBatchInfo.OpenHeight = 300
        Me.gbChangeBatchInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbChangeBatchInfo.ShowBorder = True
        Me.gbChangeBatchInfo.ShowCheckBox = False
        Me.gbChangeBatchInfo.ShowCollapseButton = False
        Me.gbChangeBatchInfo.ShowDefaultBorderColor = True
        Me.gbChangeBatchInfo.ShowDownButton = False
        Me.gbChangeBatchInfo.ShowHeader = True
        Me.gbChangeBatchInfo.Size = New System.Drawing.Size(507, 244)
        Me.gbChangeBatchInfo.TabIndex = 171
        Me.gbChangeBatchInfo.Temp = 0
        Me.gbChangeBatchInfo.Text = "Batch Information"
        Me.gbChangeBatchInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOldBatchName
        '
        Me.txtOldBatchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOldBatchName.Location = New System.Drawing.Point(348, 100)
        Me.txtOldBatchName.Name = "txtOldBatchName"
        Me.txtOldBatchName.Size = New System.Drawing.Size(146, 13)
        Me.txtOldBatchName.TabIndex = 177
        Me.txtOldBatchName.Text = "#BatchName"
        '
        'lblOldBatchName
        '
        Me.lblOldBatchName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOldBatchName.Location = New System.Drawing.Point(256, 100)
        Me.lblOldBatchName.Name = "lblOldBatchName"
        Me.lblOldBatchName.Size = New System.Drawing.Size(86, 13)
        Me.lblOldBatchName.TabIndex = 176
        Me.lblOldBatchName.Text = "Old Batch"
        '
        'elineOldBatchInfo
        '
        Me.elineOldBatchInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elineOldBatchInfo.Location = New System.Drawing.Point(6, 31)
        Me.elineOldBatchInfo.Name = "elineOldBatchInfo"
        Me.elineOldBatchInfo.Size = New System.Drawing.Size(488, 17)
        Me.elineOldBatchInfo.TabIndex = 174
        Me.elineOldBatchInfo.Text = "Old Batch Information"
        Me.elineOldBatchInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(241, 148)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(11, 79)
        Me.EZeeStraightLine2.TabIndex = 171
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'elNewBatchInfo
        '
        Me.elNewBatchInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elNewBatchInfo.Location = New System.Drawing.Point(7, 127)
        Me.elNewBatchInfo.Name = "elNewBatchInfo"
        Me.elNewBatchInfo.Size = New System.Drawing.Size(487, 17)
        Me.elNewBatchInfo.TabIndex = 173
        Me.elNewBatchInfo.Text = "New Batch Information"
        Me.elNewBatchInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(241, 47)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(11, 79)
        Me.EZeeStraightLine1.TabIndex = 171
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'txtNewInterviewType
        '
        Me.txtNewInterviewType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewInterviewType.Location = New System.Drawing.Point(348, 186)
        Me.txtNewInterviewType.Name = "txtNewInterviewType"
        Me.txtNewInterviewType.Size = New System.Drawing.Size(146, 13)
        Me.txtNewInterviewType.TabIndex = 161
        Me.txtNewInterviewType.Text = "Label4"
        '
        'lblNewVacancy
        '
        Me.lblNewVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewVacancy.Location = New System.Drawing.Point(9, 186)
        Me.lblNewVacancy.Name = "lblNewVacancy"
        Me.lblNewVacancy.Size = New System.Drawing.Size(85, 13)
        Me.lblNewVacancy.TabIndex = 159
        Me.lblNewVacancy.Text = "Vacancy"
        '
        'lblNewInterviewType
        '
        Me.lblNewInterviewType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewInterviewType.Location = New System.Drawing.Point(257, 186)
        Me.lblNewInterviewType.Name = "lblNewInterviewType"
        Me.lblNewInterviewType.Size = New System.Drawing.Size(86, 13)
        Me.lblNewInterviewType.TabIndex = 162
        Me.lblNewInterviewType.Text = "Interview Type"
        '
        'txtApplicantName
        '
        Me.txtApplicantName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicantName.Location = New System.Drawing.Point(101, 52)
        Me.txtApplicantName.Name = "txtApplicantName"
        Me.txtApplicantName.Size = New System.Drawing.Size(135, 13)
        Me.txtApplicantName.TabIndex = 170
        Me.txtApplicantName.Text = "Label11"
        '
        'lblNewLocation
        '
        Me.lblNewLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewLocation.Location = New System.Drawing.Point(9, 211)
        Me.lblNewLocation.Name = "lblNewLocation"
        Me.lblNewLocation.Size = New System.Drawing.Size(85, 18)
        Me.lblNewLocation.TabIndex = 166
        Me.lblNewLocation.Text = "Location"
        '
        'txtNewLocation
        '
        Me.txtNewLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewLocation.Location = New System.Drawing.Point(101, 211)
        Me.txtNewLocation.Name = "txtNewLocation"
        Me.txtNewLocation.Size = New System.Drawing.Size(135, 18)
        Me.txtNewLocation.TabIndex = 165
        Me.txtNewLocation.Text = "Label8"
        '
        'txtNewInterviewDate
        '
        Me.txtNewInterviewDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewInterviewDate.Location = New System.Drawing.Point(348, 160)
        Me.txtNewInterviewDate.Name = "txtNewInterviewDate"
        Me.txtNewInterviewDate.Size = New System.Drawing.Size(146, 13)
        Me.txtNewInterviewDate.TabIndex = 163
        Me.txtNewInterviewDate.Text = "Label6"
        '
        'lblNewInterviewDate
        '
        Me.lblNewInterviewDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewInterviewDate.Location = New System.Drawing.Point(257, 160)
        Me.lblNewInterviewDate.Name = "lblNewInterviewDate"
        Me.lblNewInterviewDate.Size = New System.Drawing.Size(85, 13)
        Me.lblNewInterviewDate.TabIndex = 164
        Me.lblNewInterviewDate.Text = "Interview Date"
        '
        'txtNewVacancy
        '
        Me.txtNewVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewVacancy.Location = New System.Drawing.Point(101, 186)
        Me.txtNewVacancy.Name = "txtNewVacancy"
        Me.txtNewVacancy.Size = New System.Drawing.Size(135, 13)
        Me.txtNewVacancy.TabIndex = 160
        Me.txtNewVacancy.Text = "Label2"
        '
        'pnlChangeBatch
        '
        Me.pnlChangeBatch.Controls.Add(Me.gbChangeBatchInfo)
        Me.pnlChangeBatch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlChangeBatch.Location = New System.Drawing.Point(0, 0)
        Me.pnlChangeBatch.Name = "pnlChangeBatch"
        Me.pnlChangeBatch.Size = New System.Drawing.Size(507, 244)
        Me.pnlChangeBatch.TabIndex = 172
        '
        'frmApplicantChangeBatch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(507, 299)
        Me.Controls.Add(Me.pnlChangeBatch)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantChangeBatch"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Change Applicant Batch"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbChangeBatchInfo.ResumeLayout(False)
        Me.pnlChangeBatch.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents txtVacancy As System.Windows.Forms.Label
    Friend WithEvents lblOldInterviewType As System.Windows.Forms.Label
    Friend WithEvents txtInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblInterviewDate As System.Windows.Forms.Label
    Friend WithEvents txtInterviewDate As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents txtLocation As System.Windows.Forms.Label
    Friend WithEvents lblApplicantName As System.Windows.Forms.Label
    Friend WithEvents cboBatchName As System.Windows.Forms.ComboBox
    Friend WithEvents lblNewBatch As System.Windows.Forms.Label
    Friend WithEvents gbChangeBatchInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtApplicantName As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents pnlChangeBatch As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblNewVacancy As System.Windows.Forms.Label
    Friend WithEvents txtNewInterviewType As System.Windows.Forms.Label
    Friend WithEvents txtNewVacancy As System.Windows.Forms.Label
    Friend WithEvents lblNewInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblNewLocation As System.Windows.Forms.Label
    Friend WithEvents txtNewInterviewDate As System.Windows.Forms.Label
    Friend WithEvents lblNewInterviewDate As System.Windows.Forms.Label
    Friend WithEvents txtNewLocation As System.Windows.Forms.Label
    Friend WithEvents elNewBatchInfo As eZee.Common.eZeeLine
    Friend WithEvents elineOldBatchInfo As eZee.Common.eZeeLine
    Friend WithEvents txtOldBatchName As System.Windows.Forms.Label
    Friend WithEvents lblOldBatchName As System.Windows.Forms.Label
End Class
