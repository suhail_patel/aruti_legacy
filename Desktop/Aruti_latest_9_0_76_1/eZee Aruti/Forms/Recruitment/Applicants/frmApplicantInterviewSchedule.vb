﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Threading

Public Class frmApplicantInterviewSchedule
    Dim blnNew As Boolean = False
    Private ReadOnly mstrModuleName As String = "frmApplicantInterviewSchedule"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objApplicantBatchInteview As clsApplicant_Batchschedule_Tran
    Private objBatchMaster As clsBatchSchedule
    Private objVacancy As clsVacancy
    Private mintJobunkid As Integer = -1
    Private mstrSkillCategory As String = ""
    Private mintAppBatchScheduletranunkid As Integer
    Private objApplicantMaster As clsApplicant_master
    Private mstrSkills As String = ""
    Private mstrOldId As String = ""
    Private mstrApplicantunkid As String = String.Empty
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Dim dtRow() As DataRow = Nothing
    Dim strLetterContent As String = ""
    Dim rtfTemp As RichTextBox
    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)
    'Hemant (07 Oct 2019) -- End

    'Public WriteOnly Property _IsNew()
    '    Set(ByVal value)
    '        blnNew = value
    '    End Set
    'End Property

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal enAction As enAction) As Boolean

        Try
            mintAppBatchScheduletranunkid = intUnkId

            menAction = enAction
            Me.ShowDialog()
            intUnkId = mintAppBatchScheduletranunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

      
    End Function
#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim dsList As New DataSet
        objVacancy = New clsVacancy
        Try
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objVacancy.getComboList(True, "List")
            'With cboVacancy
            '    .ValueMember = "id"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("List")
            '    .SelectedValue = 0
            'End With

            dsList = objVacancy.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub FillSkillCategory()
    '    Dim dsList As New DataSet
    '    Dim strSearching As String = ""

    '    Try
    '        dsList = objApplicantBatchInteview.GetVacancySkillCategory(mintJobunkid)

    '        'If CInt(cboVacancy.SelectedValue) > 0 Then
    '        '    strSearching = " "
    '        'End If
    '        Dim lvSkillCategory As ListViewItem

    '        lvListCategory.Items.Clear()
    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            lvSkillCategory = New ListViewItem
    '            lvSkillCategory.Text = dtRow.Item("skillcategory").ToString

    '            lvSkillCategory.Tag = dtRow.Item("skillcategoryunkid")
    '            lvListCategory.Items.Add(lvSkillCategory)

    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillSkillCategory", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub FillSkills()
    '    Dim dsList As New DataSet
    '    Dim strSearching As String = ""


    '    Try
    '        dsList = objApplicantBatchInteview.GetVacancySkills(mstrSkillCategory, mintJobunkid)

    '        Dim lvSkills As ListViewItem


    '        lvSkillSet.Items.Clear()

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            lvSkills = New ListViewItem
    '            lvSkills.Text = dtRow.Item("skillname").ToString
    '            lvSkills.SubItems.Add(dtRow.Item("skillcategory").ToString)
    '            lvSkills.SubItems.Add(dtRow.Item("skillcategoryunkid").ToString)
    '            lvSkills.Tag = dtRow.Item("skillunkid").ToString

    '            lvSkillSet.Items.Add(lvSkills)
    '        Next


    '        lvSkillSet.GroupingColumn = colhSkillCategorySkills
    '        lvSkillSet.DisplayGroups(True)

    '        colhSkillCategorySkills.Width += colhSkills.Width
    '        colhSkillCategorySkills.Width = 0

    '        Dim strid() As String = Nothing
    '        If mstrOldId.Length > 0 Then
    '            strid = mstrOldId.Split(CChar(","))
    '            For i As Integer = 0 To strid.Length - 1
    '                For j As Integer = 0 To lvSkillSet.Items.Count - 1
    '                    If CInt(lvSkillSet.Items(j).Tag) = CInt(strid(i)) Then
    '                        lvSkillSet.Items(j).Checked = True
    '                    End If
    '                Next
    '            Next
    '            mstrOldId = ""
    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillSkills", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    Private Sub FillApplicants()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtTable As New DataTable
        Dim dsAppSkil As New DataSet
        Dim dtAppSkill As New DataTable
        Dim objAppSkill As New clsApplicantSkill_tran
        Dim strApplicantunkid As String = ""
        Dim dsEnrolledEmp As New DataSet
        Dim mstrBatchAppId As String = ""
        Dim dtApplicants As New DataTable

        Try

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objApplicantMaster = New clsApplicant_master
            'dsList = objApplicantMaster.GetList("List")


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            Dim objFinalApplicant As New clsshortlist_finalapplicant

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'dsList.Tables.Add(objFinalApplicant.GetShortListFinalApplicant(, , , , CInt(cboVacancy.SelectedValue), True))
            dsList.Tables.Add(objFinalApplicant.GetShortListFinalApplicant(, , , , CInt(cboVacancy.SelectedValue), True, , , True))
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Pinkal (07-Jan-2012) -- End

            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'objApplicantBatchInteview = New clsApplicant_Batchschedule_Tran

            'S.SANDEEP [ 10 June 2011 ] -- START
            'ISSUE : MULTIPLE INTERVIEW TYPE FOR ONE VACANCY
            'dsEnrolledEmp = objApplicantBatchInteview.GetList("List", CInt(mintAppBatchScheduletranunkid))
            dsEnrolledEmp = objApplicantBatchInteview.GetList("List", CInt(mintAppBatchScheduletranunkid), CInt(cboBatchName.SelectedValue))
            'S.SANDEEP [ 10 June 2011 ] -- END 


            If dsEnrolledEmp.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsEnrolledEmp.Tables(0).Rows
                    mstrBatchAppId &= "," & CInt(dtRow.Item("applicantunkid"))
                Next
                mstrBatchAppId = Mid(mstrBatchAppId, 2)
            End If

            If mstrBatchAppId.Length > 0 Then
                dtApplicants = New DataView(dsList.Tables(0), "applicantunkid NOT IN (" & mstrBatchAppId & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtApplicants = dsList.Tables(0)
            End If



            If mstrSkills.Length > 0 Then
                dsAppSkil = objAppSkill.GetApplicantSkills(mstrSkills)


                For Each drSkillRow As DataRow In dsAppSkil.Tables(0).Rows
                    If strApplicantunkid.Length <= 0 Then
                        strApplicantunkid = CStr(drSkillRow.Item("applicantunkid"))
                    Else
                        strApplicantunkid &= "," & CStr(drSkillRow.Item("applicantunkid"))
                    End If
                Next
            End If


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboVacancy.SelectedValue) > 0 Then
            '    strSearching = "AND vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
            'End If
            'If lvSkillSet.CheckedItems.Count > 0 Then
            '    If strApplicantunkid.Length > 0 Then
            '        strSearching &= "AND applicantunkid IN (" & strApplicantunkid & ") " & " "
            '    End If
            'End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtTable = New DataView(dtApplicants, strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dtApplicants
            End If

            lvApplicantShortListed.Items.Clear()
            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem


                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'lvItem.Text = dtRow.Item("applicantname").ToString
                'lvItem.SubItems.Add(dtRow.Item("present_tel_no").ToString)
                'lvItem.SubItems.Add(dtRow.Item("email").ToString)
                'lvItem.Tag = dtRow.Item("applicantunkid".ToString)

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("ACode").ToString)
                lvItem.SubItems.Add(dtRow.Item("AName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Email").ToString)
                lvItem.SubItems.Add(dtRow.Item("Phone").ToString)
                lvItem.Tag = dtRow.Item("applicantunkid".ToString)
                'S.SANDEEP [ 25 DEC 2011 ] -- END



                lvApplicantShortListed.Items.Add(lvItem)

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicants", mstrModuleName)
        Finally
            objApplicantMaster = Nothing
        End Try

    End Sub

    Private Sub SetValue()

        Try
            objApplicantBatchInteview._Batchscheduleunkid = CInt(cboBatchName.SelectedValue)
            objApplicantBatchInteview._Userunkid = 1

            'objApplicantBatchInteview._Canceldatetime =Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()

        Try
            cboBatchName.SelectedValue = objApplicantBatchInteview._Batchscheduleunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub GetApplicantName()

        Try

            mstrApplicantunkid = ""
            If lvApplicantShortListed.CheckedItems.Count > 0 Then
                For Each lvItem As ListViewItem In lvApplicantShortListed.CheckedItems
                    If mstrApplicantunkid.Length <= 0 Then
                        mstrApplicantunkid = CStr(lvItem.Tag)
                    Else
                        mstrApplicantunkid &= "," & CStr(lvItem.Tag)
                    End If
                Next
            Else
                mstrApplicantunkid = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantName", mstrModuleName)
        End Try


    End Sub

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Private Sub ViewMergeData()
        Try
            Dim StrCol As String = ""
            Dim strDataName As String = ""
            For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                StrCol = dtRow(0).Table.Columns(j).ColumnName
                If rtfTemp.Rtf.Contains("#" & StrCol & "#") Then
                    rtfTemp.Focus()
                    strDataName = rtfTemp.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                    rtfTemp.Rtf = strDataName
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub

    Private Sub Send_Notification()
        Try
            If objEmailList.Count > 0 Then
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Stop()
                Dim objSendMail As New clsSendMail
                For Each obj In objEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    'Hemant (12 Nov 2020) -- Start
                    'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
                    objSendMail._CCAddress = obj._CCAddress
                    'Hemant (12 Nov 2020) -- End
                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception

                    End Try
                Next
                objEmailList.Clear()
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Start()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If objEmailList.Count > 0 Then
                objEmailList.Clear()
            End If
        End Try
    End Sub
    'Hemant (07 Oct 2019) -- End

#End Region

#Region " Form Events "
    Private Sub frmInterviewBatchProcess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApplicantBatchInteview = New clsApplicant_Batchschedule_Tran

        Try
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmInterviewBatchProcess_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsApplicant_Batchschedule_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_Batchschedule_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Button Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Hemant (07 Oct 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
        Dim objLetterFields As New clsLetterFields
        Dim objLetterType As New clsLetterType
        Dim dsList As New DataSet
        rtfTemp = New RichTextBox
        'Hemant (07 Oct 2019) -- End

        'Hemant (12 Nov 2020) -- Start
        'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
        Dim objApplicant As New clsApplicant_master
        'Hemant (24 Feb 2022) -- Start            
        'Dim objReportTo As New clsReportingToEmployee
        'Hemant (24 Feb 2022) -- End
        'Hemant (12 Nov 2020) -- End

        If CInt(cboBatchName.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Batch is compulsory information, it can not be blank. Please select Batch to continue."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If lvApplicantShortListed.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Applicant is compulsory information, it can not be blank. Please select Applicant from the list to continue."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Dim blnFlag As Boolean = False
        Try
            Call SetValue()
            Call GetApplicantName()

            Dim strAppId() As String = Nothing
            If menAction = enAction.ADD_ONE Then

                strAppId = mstrApplicantunkid.Split(CChar(","))
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
                objLetterFields._VacancyUnkid = CStr(cboVacancy.SelectedValue)

                If CInt(ConfigParameter._Object._InterviewSchedulingTemplateId) > 0 Then
                    objLetterType._LettertypeUnkId = CInt(ConfigParameter._Object._InterviewSchedulingTemplateId)
                    strLetterContent = objLetterType._Lettercontent
                End If
                objEmailList = New List(Of clsEmailCollection)
                'Hemant (07 Oct 2019) -- End
                For i As Integer = 0 To strAppId.Length - 1
                    objApplicantBatchInteview._Applicantunkid = CInt(strAppId(i))

                    'Hemant (12 Nov 2020) -- Start
                    'Enhancement : NMB Final Recruitment UAT v1 (Point - 5)- On interview scheduling screen, the notification sent to internal applicants should be cc to the employee reporting to
                    'Hemant (24 Feb 2022) -- Start            
                    Dim objReportTo As New clsReportingToEmployee
                    'Hemant (24 Feb 2022) -- End

                    objApplicant._Applicantunkid = CInt(strAppId(i))
                    Dim strReportingToEmail As String = String.Empty
                    If objApplicant._Employeeunkid > 0 Then
                        objReportTo._EmployeeUnkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(objApplicant._Employeeunkid)
                        Dim dt As DataTable = objReportTo._RDataTable
                        Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                        If DefaultReportList.Count > 0 Then
                            strReportingToEmail = DefaultReportList(0).Item("ReportingToEmail").ToString
                        End If
                    End If
                    'Hemant (12 Nov 2020) -- End

                    blnFlag = objApplicantBatchInteview.Insert()
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
                    If CInt(ConfigParameter._Object._InterviewSchedulingTemplateId) > 0 AndAlso strLetterContent.Trim.Length > 0 Then
                        Dim oEmail As New List(Of clsEmailCollection)

                        dsList = objLetterFields.GetEmployeeData(mstrApplicantunkid, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                        If dsList.Tables(0).Rows.Count > 0 Then
                            dtRow = dsList.Tables(0).Select("EmpId = '" & CInt(strAppId(i)) & "'")
                        End If
                        If dtRow.Length > 0 Then
                            rtfTemp.Rtf = strLetterContent
                            Call ViewMergeData()
                        End If

                        Dim htmlOutput = "Document.html"
                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(rtfTemp.Rtf, contentUriPrefix)
                        htmlResult.WriteToFile(htmlOutput)
                        Dim strHtmlEmailContent As String = htmlResult._HTML
                        objApplicantBatchInteview.Set_Notification_Applicants(FinancialYear._Object._DatabaseName, _
                                                                              User._Object._Userunkid, _
                                                                              FinancialYear._Object._YearUnkid, _
                                                                              Company._Object._Companyunkid, _
                                                                              strHtmlEmailContent, _
                                                                              dtRow, enLogin_Mode.DESKTOP, 0, 0, False, _
                                                                              oEmail, strReportingToEmail)
                        'Hemant (12 Nov 2020) --[strReportingToEmail]
                        objEmailList.AddRange(oEmail)
                    End If
                    'Hemant (07 Oct 2019) -- End
                Next
            End If

            Me.Close()

            'Hemant (07 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
            trd = New Thread(AddressOf Send_Notification)
            trd.IsBackground = True
            trd.Start()
            'Hemant (07 Oct 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click

        Try
            Call FillApplicants()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnFilter_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objResetFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

        Try
            cboVacancy.SelectedValue = 0
            lvApplicantShortListed.Items.Clear()
          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objResetFilter_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Control Events "
    Private Sub cboBatchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBatchName.SelectedIndexChanged
        Dim dtTable As DataTable
        Dim dslist As New DataSet
        Try
            If CInt(cboBatchName.SelectedValue) > 0 Then
                dslist = objBatchMaster.GetList("List", , )
                dtTable = New DataView(dslist.Tables(0), "batchscheduleunkid =" & CInt(cboBatchName.SelectedValue), "", DataViewRowState.CurrentRows).ToTable

                txtBatchNo.Text = dtTable.Rows(0).Item("batchcode").ToString
                txtBatchTime.Text = dtTable.Rows(0).Item("interviewtime").ToString
                txtInterviewdate.Text = eZeeDate.convertDate(dtTable.Rows(0).Item("interviewdate").ToString).ToShortDateString
                txtInterviewType.Text = dtTable.Rows(0).Item("InterviewType").ToString
                txtLocation.Text = dtTable.Rows(0).Item("location").ToString
            Else
                txtBatchNo.Text = ""
                txtBatchTime.Text = ""
                txtInterviewdate.Text = ""
                txtInterviewType.Text = ""
                txtLocation.Text = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatchName_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Dim dsBatch As New DataSet
        objBatchMaster = New clsBatchSchedule
        Dim dtTable As DataTable
        Try
            If CInt(cboVacancy.SelectedValue) > 0 Then
                objVacancy._Vacancyunkid = CInt(cboVacancy.SelectedValue)
                mintJobunkid = objVacancy._Jobunkid
            End If



            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsBatch = objBatchMaster.getListForCombo("List", True)
            dsBatch = objBatchMaster.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)
            'Shani(24-Aug-2015) -- End

            'dtTable = New DataView(dsBatch.Tables(0), "vacancyid = " & CInt(cboVacancy.SelectedValue) & " AND interviewdate >= " & eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date) & " AND interviewdate <= " & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date), "", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dsBatch.Tables(0), "vacancyid = " & CInt(cboVacancy.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                With cboBatchName
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dtTable
                End With
            End If

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call FillSkillCategory()
            'S.SANDEEP [ 25 DEC 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                'Shani(24-Aug-2015) -- End

                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvApplicantShortListed.ItemChecked, AddressOf lvApplicantShortListed_ItemChecked
            For Each lvItem As ListViewItem In lvApplicantShortListed.Items
                lvItem.Checked = CBool(objchkAll.CheckState)
                Next
            AddHandler lvApplicantShortListed.ItemChecked, AddressOf lvApplicantShortListed_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvApplicantShortListed_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvApplicantShortListed.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvApplicantShortListed.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvApplicantShortListed.CheckedItems.Count < lvApplicantShortListed.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvApplicantShortListed.CheckedItems.Count = lvApplicantShortListed.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApplicantShortListed_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub lvListCategory_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvListCategory.ItemChecked
    '    Try
    '        mstrSkillCategory = ""

    '        If lvListCategory.CheckedItems.Count > 0 Then
    '            For i As Integer = 0 To lvListCategory.CheckedItems.Count - 1
    '                If mstrSkillCategory.Length <= 0 Then
    '                    mstrSkillCategory = CStr(lvListCategory.CheckedItems(i).Tag)
    '                Else
    '                    mstrSkillCategory &= "," & CStr(lvListCategory.CheckedItems(i).Tag)
    '                End If
    '            Next
    '        End If
    '        If lvListCategory.CheckedItems.Count > 0 Then
    '            If lvSkillSet.Items.Count <= 0 Then mstrOldId = ""
    '            Call FillSkills()
    '            Call lvSkillSet_ItemChecked(sender, e)
    '        Else
    '            lvSkillSet.Items.Clear()
    '            mstrSkillCategory = ""
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvListCategory_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvSkillSet_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSkillSet.ItemChecked

    '    Try
    '        mstrSkills = ""
    '        If lvSkillSet.CheckedItems.Count > 0 Then
    '            For i As Integer = 0 To lvSkillSet.CheckedItems.Count - 1
    '                If mstrSkills.Length <= 0 Then
    '                    mstrSkills = CStr(lvSkillSet.CheckedItems(i).Tag)
    '                Else
    '                    mstrSkills &= "," & CStr(lvSkillSet.CheckedItems(i).Tag)
    '                End If
    '            Next
    '            mstrOldId = mstrSkills
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSkillSet_ItemChecked", mstrModuleName)
    '    End Try

    'End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

    'Private Sub lnkCancelInformation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
    '    pnlBatchCreation.Visible = False
    '    gbBatchGenerationInfo.Controls.RemoveByKey("pnlBatchCreation")
    '    gbBatchGenerationInfo.Text = "Batch Cancellation"

    'End Sub

    'Private Sub objlnkClose_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)

    '    gbBatchGenerationInfo.Controls.RemoveByKey("pnlCancelBatch")
    '    gbBatchGenerationInfo.Controls.Add(pnlBatchCreation)
    '    gbBatchGenerationInfo.Text = "Batch Creation"
    '    pnlBatchCreation.BringToFront()
    '    pnlBatchCreation.Show()

    'End Sub




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbBatchGenerationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBatchGenerationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbBatchGenerationInfo.Text = Language._Object.getCaption(Me.gbBatchGenerationInfo.Name, Me.gbBatchGenerationInfo.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.lblBatchNo.Text = Language._Object.getCaption(Me.lblBatchNo.Name, Me.lblBatchNo.Text)
			Me.lblInterviewTime.Text = Language._Object.getCaption(Me.lblInterviewTime.Name, Me.lblInterviewTime.Text)
			Me.lblInterviewDateFrom.Text = Language._Object.getCaption(Me.lblInterviewDateFrom.Name, Me.lblInterviewDateFrom.Text)
			Me.lblInterview.Text = Language._Object.getCaption(Me.lblInterview.Name, Me.lblInterview.Text)
			Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
			Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
			Me.colhApplicant.Text = Language._Object.getCaption(CStr(Me.colhApplicant.Tag), Me.colhApplicant.Text)
			Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)
			Me.colhMobileNo.Text = Language._Object.getCaption(CStr(Me.colhMobileNo.Tag), Me.colhMobileNo.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Batch is compulsory information, it can not be blank. Please select Batch to continue.")
			Language.setMessage(mstrModuleName, 2, "Applicant is compulsory information, it can not be blank. Please select Applicant from the list to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class