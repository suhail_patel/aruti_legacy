﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmFinalShortListApplicant

#Region " Private Variables "

    Private mblnIsImport As Boolean = False
    Private ReadOnly mstrModuleName As String = "frmFinalShortListApplicant"
    'Sandeep [ 21 Aug 2010 ] -- Start
    Dim dsList As New DataSet
    Dim mstrEmailList As String = String.Empty
    Dim strLetterContent As String = ""
    Dim rtfTemp As RichTextBox
    Dim dtRow() As DataRow = Nothing
    'Sandeep [ 21 Aug 2010 ] -- End 
    Private objInterviewAnalysisMaster As clsInterviewAnalysis_master
    Private mdsList As New DataSet

#End Region

#Region " Properties "

    Public WriteOnly Property _Is_Import() As Boolean
        Set(ByVal value As Boolean)
            mblnIsImport = value
        End Set
    End Property

    Public ReadOnly Property _Dataview() As DataSet
        Get
            Return dsList
        End Get
    End Property

    Public ReadOnly Property _EmailList() As String
        Get
            Return mstrEmailList
        End Get
    End Property

#End Region

#Region " Form Events "

    Private Sub frmFinalShortListApplicant_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name) 'Sohail (09 Jul 2021)
            Call OtherSettings() 'Sohail (09 Jul 2021)
            objInterviewAnalysisMaster = New clsInterviewAnalysis_master

            If mblnIsImport = True Then
                btnGenerateLetter.Visible = False
                btnPrintList.Visible = False
                btnSendMail.Visible = False
            End If
            Call FillCombo()

            Call FillList()


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call Setvisibility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFinalShortListApplicant_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewFinalApplicantList = True Then                'Pinkal (02-Jul-2012) -- Start


                'S.SANDEEP [ 14 May 2013 ] -- START
                'ENHANCEMENT : TRA ENHANCEMENT
                'If cboFinalApplicantType.SelectedIndex = 0 Then
                '    dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), True)
                'Else
                '    dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), False)
                'End If
                Select Case cboFinalApplicantType.SelectedIndex
                    Case enApplicant_Eligibility.AE_ELIGIBLE
                        dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), enApplicant_Eligibility.AE_ELIGIBLE)
                    Case enApplicant_Eligibility.AE_NOTELIGIBLE
                        dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), enApplicant_Eligibility.AE_NOTELIGIBLE)
                    Case enApplicant_Eligibility.AE_ELIGIBLE_APPROVED
                        dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), enApplicant_Eligibility.AE_ELIGIBLE, , enShortListing_Status.SC_APPROVED)
                    Case enApplicant_Eligibility.AE_ELIGIBLE_DISAPPROVED
                        dsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, CInt(cboApplicantType.SelectedIndex), enApplicant_Eligibility.AE_ELIGIBLE, , enShortListing_Status.SC_REJECT)
                End Select
                'S.SANDEEP [ 14 May 2013 ] -- END



                If CInt(cboVacancy.SelectedValue) > 0 Then
                    StrSearching &= "AND vacancyunkid = " & CInt(cboVacancy.SelectedValue) & " "
                End If

                If CInt(cboApplicant.SelectedValue) > 0 Then
                    StrSearching &= "AND applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtTable = New DataView(dsList.Tables("List"), StrSearching, "vacancytitle,applicantname", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables("List"), "", "vacancytitle,applicantname", DataViewRowState.CurrentRows).ToTable
                End If

                lvFinalAppList.Items.Clear()

                RemoveHandler lvFinalAppList.ItemChecked, AddressOf lvFinalAppList_ItemChecked
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.SubItems.Add(dtRow.Item("applicantname").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Address").ToString)
                    lvItem.SubItems.Add(dtRow.Item("mobile").ToString)
                    lvItem.SubItems.Add(dtRow.Item("email").ToString)
                    If dtRow.Item("ODate").ToString.Trim.Length > 0 AndAlso dtRow.Item("CDate").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(dtRow.Item("vacancytitle").ToString & " ( " & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & " ) ")
                    Else
                        lvItem.SubItems.Add(dtRow.Item("vacancytitle").ToString)
                    End If
                    lvItem.SubItems.Add(dtRow.Item("vacancyunkid").ToString)
                    'S.SANDEEP [ 14 May 2013 ] -- START
                    'ENHANCEMENT : TRA ENHANCEMENT
                    lvItem.SubItems.Add(dtRow.Item("I_Status").ToString)
                    'S.SANDEEP [ 14 May 2013 ] -- END
                    lvItem.Tag = dtRow.Item("applicantunkid")
                    lvFinalAppList.Items.Add(lvItem)
                    lvItem = Nothing
                Next
                AddHandler lvFinalAppList.ItemChecked, AddressOf lvFinalAppList_ItemChecked
                lvFinalAppList.GroupingColumn = colhVacancy
                lvFinalAppList.DisplayGroups(True)

                If lvFinalAppList.Items.Count > 5 Then
                    colhApplicantName.Width = 150 - 20
                Else
                    colhApplicantName.Width = 150
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try

    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objApplicantData As New clsApplicant_master
        Dim objVacancyData As New clsVacancy
        Try

            dsList = objApplicantData.GetApplicantList("List", True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objVacancyData.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'dsList = objVacancyData.getComboList(True, "List")
            'With cboVacancy
            '    .ValueMember = "id"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("List")
            '    .SelectedValue = 0
            'End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            With cboApplicantType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With
            'Pinkal (07-Jan-2012) -- End


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'cboFinalApplicantType.Items.Add(Language.getMessage(mstrModuleName, 7, "Eliglible Applicant"))
            'cboFinalApplicantType.Items.Add(Language.getMessage(mstrModuleName, 8, "Non Eliglible Applicant"))
            'cboFinalApplicantType.SelectedIndex = 0
            With cboFinalApplicantType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Eliglible Applicant"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Non Eliglible Applicant"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Eliglible Applicant - Approved"))
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Eliglible Applicant - Disapproved"))
                .SelectedIndex = 0
            End With
            'S.SANDEEP [ 14 May 2013 ] -- END


            'Pinkal (07-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub ViewMergeData()
        Try
            Dim StrCol As String = ""
            Dim strDataName As String = ""
            For j As Integer = 0 To dtRow(0).Table.Columns.Count - 1
                StrCol = dtRow(0).Table.Columns(j).ColumnName
                If rtfTemp.Rtf.Contains("#" & StrCol & "#") Then
                    rtfTemp.Focus()
                    strDataName = rtfTemp.Rtf.Replace("#" & StrCol & "#", dtRow(0).Item(StrCol).ToString)
                    rtfTemp.Rtf = strDataName
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ViewMergeData", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

    Private Sub Do_Operation(ByVal blnOperation As Boolean)
        Try
            For Each LVItem As ListViewItem In lvFinalAppList.Items
                LVItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        Finally
        End Try
    End Sub


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Setvisibility()
        Try
            btnSendMail.Enabled = User._Object.Privilege._AllowtoSendMailApplicant
            btnGenerateLetter.Enabled = User._Object.Privilege._AllowtoGenerateApplicantLetter
            btnPrintList.Enabled = User._Object.Privilege._AllowtoPrintFinalApplicantList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Setvisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            lvFinalAppList.GridLines = False
            Call FillList()

            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objChkAll.Checked = False
            'Pinkal (07-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboApplicant.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            lvFinalAppList.GridLines = False


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            cboFinalApplicantType.SelectedIndex = 0
            objChkAll.Checked = False
            'Pinkal (07-Jan-2012) -- End


            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .DataSource = CType(cboApplicant.DataSource, DataTable)
                .CodeMember = "applicant_code"
            End With

            If objFrm.DisplayDialog Then
                cboApplicant.SelectedValue = objFrm.SelectedValue
                cboApplicant.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
                .CodeMember = "applicant_code"
            End With

            If objFrm.DisplayDialog Then
                cboVacancy.SelectedValue = objFrm.SelectedValue
                cboVacancy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub btnSendMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendMail.Click
        If lvFinalAppList.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one applicant to send mail."), enMsgBoxStyle.Information)
            lvFinalAppList.Select()
            Exit Sub
        End If
        Try
            Dim objLetterFields As New clsLetterFields
            If lvFinalAppList.CheckedItems.Count > 0 Then
                Dim strApplicantIds As String = String.Empty
                Dim strVacancyIds As String = String.Empty 'Hemant (19 Jun 2020)
                mstrEmailList = ""
                For Each lvItem As ListViewItem In lvFinalAppList.CheckedItems
                    With lvItem
                        If .SubItems(colhEmail.Index).Text <> "" Then
                            mstrEmailList &= "," & .SubItems(colhApplicantName.Index).Text & "<" & .SubItems(colhEmail.Index).Text & ">"
                            strApplicantIds &= "," & .Tag.ToString
                            strVacancyIds &= "," & .SubItems(objcolhVacancyunkid.Index).Text 'Hemant (19 Jun 2020)
                        Else
                            Dim strMsg As String = Language.getMessage(mstrModuleName, 2, "Some of the email address(s) are blank. Do you want to continue?")
                            If eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Continue For
                            Else
                                Exit Sub
                            End If
                        End If
                    End With
                Next

                mstrEmailList = Mid(mstrEmailList, 2)
                strApplicantIds = Mid(strApplicantIds, 2)
                strVacancyIds = Mid(strVacancyIds, 2) 'Hemant (19 Jun 2020)

                If strApplicantIds.Length > 0 Then

                    objLetterFields._VacancyUnkid = strVacancyIds 'Hemant (19 Jun 2020)
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module)

                    'S.SANDEEP |09-APR-2019| -- START
                    'dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                    dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                    'S.SANDEEP |09-APR-2019| -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                Dim objFrm As New frmSendMail
                objFrm._MForm = Me
                objFrm.displayDialog(enImg_Email_RefId.Applicant_Module, mstrEmailList, dsList)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSendMail_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGenerateLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateLetter.Click
        If lvFinalAppList.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one applicant to generate letter."), enMsgBoxStyle.Information)
            lvFinalAppList.Select()
            Exit Sub
        End If
        Dim frm As New frmLetterTemplate
        Dim strApplicantIds As String = String.Empty
        Dim strVacancyIds As String = String.Empty 'Hemant (07 Oct 2019)
        Dim objLetterFields As New clsLetterFields
        'Dim exforce As New System.IO.DirectoryNotFoundException
        Try
            rtfTemp = New RichTextBox
            frm.displayDialog(False, True)

            If frm._Letter_Unkid > 0 Then

                If frm._Letter_Content.Length > 0 Then

                    strLetterContent = frm._Letter_Content
                    rtfTemp.Rtf = frm._Letter_Content

                    For Each lvItem As ListViewItem In lvFinalAppList.CheckedItems
                        With lvItem
                            strApplicantIds &= "," & .Tag.ToString
                            strVacancyIds &= "," & .SubItems(objcolhVacancyunkid.Index).Text 'Hemant (07 Oct 2019)
                        End With
                    Next

                    strApplicantIds = Mid(strApplicantIds, 2)
                    strVacancyIds = Mid(strVacancyIds, 2) 'Hemant (07 Oct 2019)

                    If strApplicantIds.Length > 0 Then

                        objLetterFields._VacancyUnkid = strVacancyIds 'Hemant (07 Oct 2019)
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module)

                        'S.SANDEEP |09-APR-2019| -- START
                        'dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid)
                        dsList = objLetterFields.GetEmployeeData(strApplicantIds, enImg_Email_RefId.Applicant_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                        'S.SANDEEP |09-APR-2019| -- END

                        'S.SANDEEP [04 JUN 2015] -- END

                    End If
                    'Sandeep [ 16 Oct 2010 ] -- Start
                    If ConfigParameter._Object._ExportReportPath = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Export Report path is blank. Please Set the Path from Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
                        Exit Sub
                    ElseIf ConfigParameter._Object._ExportReportPath <> "" Then
                        Dim dir As New System.IO.DirectoryInfo(ConfigParameter._Object._ExportReportPath)
                        If dir.Exists = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Directory is invalid. Please set the valid directory."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'Sandeep [ 16 Oct 2010 ] -- End 
                    For Each strids As String In strApplicantIds.Split(CChar(","))
                        If dsList.Tables(0).Rows.Count > 0 Then
                            dtRow = dsList.Tables(0).Select("EmpId = '" & CInt(strids) & "'")
                        End If
                        If dtRow.Length > 0 Then
                            rtfTemp.Rtf = strLetterContent
                            Call ViewMergeData()
                        End If
                        Dim objApplicant As New clsApplicant_master
                        objApplicant._Applicantunkid = CInt(strids)
                        rtfTemp.SaveFile(ConfigParameter._Object._ExportReportPath & "\" & objApplicant._Firstname & "_" & objApplicant._Applicant_Code & "_" & eZeeDate.convertDate(Now) & Format(Now, "hhmmss") & ".rtf")
                        objApplicant = Nothing
                    Next

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "File(s) have been successfully saved."), enMsgBoxStyle.Information)

                    Me.Close()
                End If
            End If

        Catch ex As Exception
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'DisplayError.Show("-1", ex.Message, "btnGenerateLetter_Click", mstrModuleName)
            If ex.Message.Trim = "A required privilege is not held by the client." Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you are not allowed to save file on the selected path. Reason : Permission not given for selected path. Please contact Network Administrator."), enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "btnGenerateLetter_Click", mstrModuleName)
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END
        Finally
            frm = Nothing
            dsList.Dispose()
            rtfTemp.Dispose()
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

    Private Sub btnPrintList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintList.Click
        Dim objFinalApplicntReport As New ArutiReports.clsFinalApplicantListReport
        Try
            objFinalApplicntReport._ApplicantUnkid = CInt(cboApplicant.SelectedValue)
            objFinalApplicntReport._ApplicantName = cboApplicant.Text

            objFinalApplicntReport._VacancyUnkid = CInt(cboVacancy.SelectedValue)
            objFinalApplicntReport._VacancyName = cboVacancy.Text

            Dim StrIds, StrNames As String
            StrIds = "" : StrNames = ""
            For Each lvItem As ListViewItem In lvFinalAppList.CheckedItems
                StrIds &= "," & lvItem.Tag.ToString
                StrNames &= "," & lvItem.SubItems(colhApplicantName.Index).Text
            Next

            If StrIds.Length > 0 And StrNames.Length > 0 Then
                StrIds = Mid(StrIds, 2) : StrNames = Mid(StrNames, 2)
                objFinalApplicntReport._ApplicantUnkid = 0
                objFinalApplicntReport._ApplicantIds = StrIds
                objFinalApplicntReport._ApplicantNames = StrNames
            End If


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List")

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'If CInt(cboFinalApplicantType.SelectedIndex) = 0 Then
            '    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, True)
            'ElseIf CInt(cboFinalApplicantType.SelectedIndex) = 1 Then
            '    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, False)
            'End If

            Select Case cboFinalApplicantType.SelectedIndex
                Case enApplicant_Eligibility.AE_ELIGIBLE
                    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, enApplicant_Eligibility.AE_ELIGIBLE)
                Case enApplicant_Eligibility.AE_NOTELIGIBLE
                    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, enApplicant_Eligibility.AE_NOTELIGIBLE)
                Case enApplicant_Eligibility.AE_ELIGIBLE_APPROVED
                    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, enApplicant_Eligibility.AE_ELIGIBLE, , enShortListing_Status.SC_APPROVED)
                Case enApplicant_Eligibility.AE_ELIGIBLE_DISAPPROVED
                    mdsList = objInterviewAnalysisMaster.GetList_FinalApplicant("List", False, 0, enApplicant_Eligibility.AE_ELIGIBLE, , enShortListing_Status.SC_REJECT)
            End Select
            'S.SANDEEP [ 14 May 2013 ] -- END


            'Pinkal (07-Jan-2012) -- End




            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'Call objFinalApplicntReport.Print_List(mdsList)
            Call objFinalApplicntReport.Print_List(mdsList, _
                                                   User._Object._Userunkid, _
                                                   Company._Object._Companyunkid, _
                                                   ConfigParameter._Object._NoofPagestoPrint)
            'Shani(24-Aug-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPrintList_Click", mstrModuleName)
        Finally
            objFinalApplicntReport = Nothing
        End Try
    End Sub

#End Region

#Region " CheckBox Event's "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvFinalAppList.ItemChecked, AddressOf lvFinalAppList_ItemChecked
            Call Do_Operation(CBool(objChkAll.CheckState))
            AddHandler lvFinalAppList.ItemChecked, AddressOf lvFinalAppList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ListView Event's "

    Private Sub lvFinalAppList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvFinalAppList.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvFinalAppList.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvFinalAppList.CheckedItems.Count < lvFinalAppList.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvFinalAppList.CheckedItems.Count = lvFinalAppList.CheckedItems.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFinalAppList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                Dim dsVacList As New DataSet
                Dim objVac As New clsVacancy

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                dsVacList = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
                'Shani(24-Aug-2015) -- End

                With cboVacancy
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsVacList.Tables("List")
                    .SelectedValue = 0
                End With

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbApplicantInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplicantInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnGenerateLetter.GradientBackColor = GUI._ButttonBackColor
            Me.btnGenerateLetter.GradientForeColor = GUI._ButttonFontColor

            Me.btnSendMail.GradientBackColor = GUI._ButttonBackColor
            Me.btnSendMail.GradientForeColor = GUI._ButttonFontColor

            Me.btnPrintList.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrintList.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbApplicantInfo.Text = Language._Object.getCaption(Me.gbApplicantInfo.Name, Me.gbApplicantInfo.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.colhApplicantName.Text = Language._Object.getCaption(CStr(Me.colhApplicantName.Tag), Me.colhApplicantName.Text)
            Me.colhAddress.Text = Language._Object.getCaption(CStr(Me.colhAddress.Tag), Me.colhAddress.Text)
            Me.colhPhone.Text = Language._Object.getCaption(CStr(Me.colhPhone.Tag), Me.colhPhone.Text)
            Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)
            Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
            Me.btnGenerateLetter.Text = Language._Object.getCaption(Me.btnGenerateLetter.Name, Me.btnGenerateLetter.Text)
            Me.btnSendMail.Text = Language._Object.getCaption(Me.btnSendMail.Name, Me.btnSendMail.Text)
            Me.btnPrintList.Text = Language._Object.getCaption(Me.btnPrintList.Name, Me.btnPrintList.Text)
            Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
            Me.lblFinalApplicantType.Text = Language._Object.getCaption(Me.lblFinalApplicantType.Name, Me.lblFinalApplicantType.Text)
            Me.lblApplicantType.Text = Language._Object.getCaption(Me.lblApplicantType.Name, Me.lblApplicantType.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one applicant to send mail.")
            Language.setMessage(mstrModuleName, 2, "Some of the email address(s) are blank. Do you want to continue?")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one applicant to generate letter.")
            Language.setMessage(mstrModuleName, 4, "Export Report path is blank. Please Set the Path from Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 5, "Directory is invalid. Please set the valid directory.")
            Language.setMessage(mstrModuleName, 6, "File(s) have been successfully saved.")
            Language.setMessage(mstrModuleName, 7, "Eliglible Applicant")
            Language.setMessage(mstrModuleName, 8, "Non Eliglible Applicant")
            Language.setMessage(mstrModuleName, 9, "Eliglible Applicant - Approved")
            Language.setMessage(mstrModuleName, 10, "Eliglible Applicant - Disapproved")
            Language.setMessage(mstrModuleName, 11, "Sorry, you are not allowed to save file on the selected path. Reason : Permission not given for selected path. Please contact Network Administrator.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class