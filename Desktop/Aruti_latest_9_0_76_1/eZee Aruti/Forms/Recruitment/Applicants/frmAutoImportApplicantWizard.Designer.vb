<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutoImportApplicantWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoImportApplicantWizard))
        Me.ewWizard = New eZee.Common.eZeeWizard
        Me.ewpPage1 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMsg1 = New System.Windows.Forms.Label
        Me.lblTitle1 = New System.Windows.Forms.Label
        Me.ewpPage2 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dtpTime = New System.Windows.Forms.DateTimePicker
        Me.lblTime = New System.Windows.Forms.Label
        Me.rbtnMonthly = New System.Windows.Forms.RadioButton
        Me.rbtnWeekly = New System.Windows.Forms.RadioButton
        Me.rbtnDaily = New System.Windows.Forms.RadioButton
        Me.lblTaskType = New System.Windows.Forms.Label
        Me.lblWeekDay = New System.Windows.Forms.Label
        Me.cboDay = New System.Windows.Forms.ComboBox
        Me.lblDay = New System.Windows.Forms.Label
        Me.cboWeekDay = New System.Windows.Forms.ComboBox
        Me.ewpPage4 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.chkRunNow = New System.Windows.Forms.CheckBox
        Me.lblTitle3 = New System.Windows.Forms.Label
        Me.lblMsg3 = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.ewpPage3 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.txtCPWD = New System.Windows.Forms.TextBox
        Me.txtPWD = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.lblMsg2 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblConfirmPassword = New System.Windows.Forms.Label
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblTitle2 = New System.Windows.Forms.Label
        Me.ewWizard.SuspendLayout()
        Me.ewpPage1.SuspendLayout()
        Me.ewpPage2.SuspendLayout()
        Me.ewpPage4.SuspendLayout()
        Me.ewpPage3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ewWizard
        '
        Me.ewWizard.Controls.Add(Me.ewpPage2)
        Me.ewWizard.Controls.Add(Me.ewpPage1)
        Me.ewWizard.Controls.Add(Me.ewpPage4)
        Me.ewWizard.Controls.Add(Me.objbuttonBack)
        Me.ewWizard.Controls.Add(Me.objbuttonCancel)
        Me.ewWizard.Controls.Add(Me.objbuttonNext)
        Me.ewWizard.Controls.Add(Me.ewpPage3)
        Me.ewWizard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ewWizard.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ewWizard.HeaderImage = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.ewWizard.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.ewWizard.Location = New System.Drawing.Point(0, 0)
        Me.ewWizard.Name = "ewWizard"
        Me.ewWizard.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.ewpPage1, Me.ewpPage2, Me.ewpPage3, Me.ewpPage4})
        Me.ewWizard.Size = New System.Drawing.Size(550, 383)
        Me.ewWizard.TabIndex = 1
        Me.ewWizard.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ewWizard.WelcomeImage = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.ewWizard.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'ewpPage1
        '
        Me.ewpPage1.BackColor = System.Drawing.Color.White
        Me.ewpPage1.Controls.Add(Me.lblMsg1)
        Me.ewpPage1.Controls.Add(Me.lblTitle1)
        Me.ewpPage1.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage1.Name = "ewpPage1"
        Me.ewpPage1.Size = New System.Drawing.Size(550, 335)
        Me.ewpPage1.Style = eZee.Common.eZeeWizardPageStyle.Welcome
        Me.ewpPage1.TabIndex = 0
        '
        'lblMsg1
        '
        Me.lblMsg1.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg1.Location = New System.Drawing.Point(168, 44)
        Me.lblMsg1.Name = "lblMsg1"
        Me.lblMsg1.Size = New System.Drawing.Size(370, 34)
        Me.lblMsg1.TabIndex = 3
        Me.lblMsg1.Text = "This wizard will help you to create a windows scheduled task, which will import A" & _
            "pplicants automatically."
        '
        'lblTitle1
        '
        Me.lblTitle1.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle1.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle1.Location = New System.Drawing.Point(168, 9)
        Me.lblTitle1.Name = "lblTitle1"
        Me.lblTitle1.Size = New System.Drawing.Size(370, 23)
        Me.lblTitle1.TabIndex = 2
        Me.lblTitle1.Text = "Auto Import Applicant Schedule Task Wizard"
        '
        'ewpPage2
        '
        Me.ewpPage2.BackColor = System.Drawing.Color.White
        Me.ewpPage2.Controls.Add(Me.dtpTime)
        Me.ewpPage2.Controls.Add(Me.lblTime)
        Me.ewpPage2.Controls.Add(Me.rbtnMonthly)
        Me.ewpPage2.Controls.Add(Me.rbtnWeekly)
        Me.ewpPage2.Controls.Add(Me.rbtnDaily)
        Me.ewpPage2.Controls.Add(Me.lblTaskType)
        Me.ewpPage2.Controls.Add(Me.lblWeekDay)
        Me.ewpPage2.Controls.Add(Me.cboDay)
        Me.ewpPage2.Controls.Add(Me.lblDay)
        Me.ewpPage2.Controls.Add(Me.cboWeekDay)
        Me.ewpPage2.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage2.Name = "ewpPage2"
        Me.ewpPage2.Size = New System.Drawing.Size(550, 335)
        Me.ewpPage2.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpPage2.TabIndex = 2
        '
        'dtpTime
        '
        Me.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpTime.Location = New System.Drawing.Point(171, 43)
        Me.dtpTime.Name = "dtpTime"
        Me.dtpTime.Size = New System.Drawing.Size(95, 20)
        Me.dtpTime.TabIndex = 17
        '
        'lblTime
        '
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(168, 24)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(370, 16)
        Me.lblTime.TabIndex = 13
        Me.lblTime.Text = "Select time when you want this task to start"
        '
        'rbtnMonthly
        '
        Me.rbtnMonthly.AutoSize = True
        Me.rbtnMonthly.Location = New System.Drawing.Point(186, 141)
        Me.rbtnMonthly.Name = "rbtnMonthly"
        Me.rbtnMonthly.Size = New System.Drawing.Size(62, 17)
        Me.rbtnMonthly.TabIndex = 12
        Me.rbtnMonthly.Text = "Monthly"
        Me.rbtnMonthly.UseVisualStyleBackColor = True
        '
        'rbtnWeekly
        '
        Me.rbtnWeekly.AutoSize = True
        Me.rbtnWeekly.Location = New System.Drawing.Point(186, 118)
        Me.rbtnWeekly.Name = "rbtnWeekly"
        Me.rbtnWeekly.Size = New System.Drawing.Size(61, 17)
        Me.rbtnWeekly.TabIndex = 11
        Me.rbtnWeekly.Text = "Weekly"
        Me.rbtnWeekly.UseVisualStyleBackColor = True
        '
        'rbtnDaily
        '
        Me.rbtnDaily.AutoSize = True
        Me.rbtnDaily.Checked = True
        Me.rbtnDaily.Location = New System.Drawing.Point(186, 95)
        Me.rbtnDaily.Name = "rbtnDaily"
        Me.rbtnDaily.Size = New System.Drawing.Size(48, 17)
        Me.rbtnDaily.TabIndex = 10
        Me.rbtnDaily.TabStop = True
        Me.rbtnDaily.Text = "Daily"
        Me.rbtnDaily.UseVisualStyleBackColor = True
        '
        'lblTaskType
        '
        Me.lblTaskType.BackColor = System.Drawing.Color.Transparent
        Me.lblTaskType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaskType.Location = New System.Drawing.Point(168, 73)
        Me.lblTaskType.Name = "lblTaskType"
        Me.lblTaskType.Size = New System.Drawing.Size(370, 16)
        Me.lblTaskType.TabIndex = 9
        Me.lblTaskType.Tag = ""
        Me.lblTaskType.Text = "Perform this task: "
        '
        'lblWeekDay
        '
        Me.lblWeekDay.BackColor = System.Drawing.Color.Transparent
        Me.lblWeekDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeekDay.Location = New System.Drawing.Point(168, 165)
        Me.lblWeekDay.Name = "lblWeekDay"
        Me.lblWeekDay.Size = New System.Drawing.Size(181, 16)
        Me.lblWeekDay.TabIndex = 18
        Me.lblWeekDay.Tag = ""
        Me.lblWeekDay.Text = "Select the day of the week:"
        '
        'cboDay
        '
        Me.cboDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDay.FormattingEnabled = True
        Me.cboDay.Location = New System.Drawing.Point(356, 163)
        Me.cboDay.Name = "cboDay"
        Me.cboDay.Size = New System.Drawing.Size(121, 21)
        Me.cboDay.TabIndex = 21
        '
        'lblDay
        '
        Me.lblDay.BackColor = System.Drawing.Color.Transparent
        Me.lblDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDay.Location = New System.Drawing.Point(168, 165)
        Me.lblDay.Name = "lblDay"
        Me.lblDay.Size = New System.Drawing.Size(181, 16)
        Me.lblDay.TabIndex = 19
        Me.lblDay.Tag = ""
        Me.lblDay.Text = "Select the day:"
        '
        'cboWeekDay
        '
        Me.cboWeekDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWeekDay.FormattingEnabled = True
        Me.cboWeekDay.Location = New System.Drawing.Point(356, 163)
        Me.cboWeekDay.Name = "cboWeekDay"
        Me.cboWeekDay.Size = New System.Drawing.Size(121, 21)
        Me.cboWeekDay.TabIndex = 20
        '
        'ewpPage4
        '
        Me.ewpPage4.Controls.Add(Me.chkRunNow)
        Me.ewpPage4.Controls.Add(Me.lblTitle3)
        Me.ewpPage4.Controls.Add(Me.lblMsg3)
        Me.ewpPage4.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage4.Name = "ewpPage4"
        Me.ewpPage4.Size = New System.Drawing.Size(550, 335)
        Me.ewpPage4.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.ewpPage4.TabIndex = 7
        '
        'chkRunNow
        '
        Me.chkRunNow.BackColor = System.Drawing.Color.Transparent
        Me.chkRunNow.Location = New System.Drawing.Point(171, 285)
        Me.chkRunNow.Name = "chkRunNow"
        Me.chkRunNow.Size = New System.Drawing.Size(372, 16)
        Me.chkRunNow.TabIndex = 17
        Me.chkRunNow.Text = "Run schedule task after create it."
        Me.chkRunNow.UseVisualStyleBackColor = False
        '
        'lblTitle3
        '
        Me.lblTitle3.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle3.Location = New System.Drawing.Point(168, 9)
        Me.lblTitle3.Name = "lblTitle3"
        Me.lblTitle3.Size = New System.Drawing.Size(370, 57)
        Me.lblTitle3.TabIndex = 16
        Me.lblTitle3.Text = "You have successfully scheduled the Auto Import Applicants Task."
        '
        'lblMsg3
        '
        Me.lblMsg3.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg3.Location = New System.Drawing.Point(168, 305)
        Me.lblMsg3.Name = "lblMsg3"
        Me.lblMsg3.Size = New System.Drawing.Size(370, 16)
        Me.lblMsg3.TabIndex = 15
        Me.lblMsg3.Text = "Click Finish to add this task to your Windows schedule."
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(298, 347)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(466, 347)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(382, 347)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'ewpPage3
        '
        Me.ewpPage3.Controls.Add(Me.txtCPWD)
        Me.ewpPage3.Controls.Add(Me.txtPWD)
        Me.ewpPage3.Controls.Add(Me.txtUserName)
        Me.ewpPage3.Controls.Add(Me.lblMsg2)
        Me.ewpPage3.Controls.Add(Me.Label12)
        Me.ewpPage3.Controls.Add(Me.lblConfirmPassword)
        Me.ewpPage3.Controls.Add(Me.lblPassword)
        Me.ewpPage3.Controls.Add(Me.lblUserName)
        Me.ewpPage3.Controls.Add(Me.lblTitle2)
        Me.ewpPage3.Location = New System.Drawing.Point(0, 0)
        Me.ewpPage3.Name = "ewpPage3"
        Me.ewpPage3.Size = New System.Drawing.Size(428, 208)
        Me.ewpPage3.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.ewpPage3.TabIndex = 1
        '
        'txtCPWD
        '
        Me.txtCPWD.Location = New System.Drawing.Point(359, 89)
        Me.txtCPWD.Name = "txtCPWD"
        Me.txtCPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtCPWD.Size = New System.Drawing.Size(179, 20)
        Me.txtCPWD.TabIndex = 22
        '
        'txtPWD
        '
        Me.txtPWD.Location = New System.Drawing.Point(359, 63)
        Me.txtPWD.Name = "txtPWD"
        Me.txtPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWD.Size = New System.Drawing.Size(179, 20)
        Me.txtPWD.TabIndex = 21
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(359, 37)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(179, 20)
        Me.txtUserName.TabIndex = 20
        '
        'lblMsg2
        '
        Me.lblMsg2.BackColor = System.Drawing.Color.Transparent
        Me.lblMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsg2.Location = New System.Drawing.Point(168, 305)
        Me.lblMsg2.Name = "lblMsg2"
        Me.lblMsg2.Size = New System.Drawing.Size(370, 16)
        Me.lblMsg2.TabIndex = 19
        Me.lblMsg2.Text = "If a password is not entered, scheduled tasks might not run."
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(248, 627)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(370, 16)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Select time when you want this task to start"
        '
        'lblConfirmPassword
        '
        Me.lblConfirmPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblConfirmPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfirmPassword.Location = New System.Drawing.Point(168, 91)
        Me.lblConfirmPassword.Name = "lblConfirmPassword"
        Me.lblConfirmPassword.Size = New System.Drawing.Size(180, 16)
        Me.lblConfirmPassword.TabIndex = 17
        Me.lblConfirmPassword.Text = "Confirm password:"
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(168, 65)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(180, 16)
        Me.lblPassword.TabIndex = 16
        Me.lblPassword.Text = "Enter the password:"
        '
        'lblUserName
        '
        Me.lblUserName.BackColor = System.Drawing.Color.Transparent
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(168, 39)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(180, 16)
        Me.lblUserName.TabIndex = 15
        Me.lblUserName.Text = "Enter the user name:"
        '
        'lblTitle2
        '
        Me.lblTitle2.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle2.Location = New System.Drawing.Point(168, 9)
        Me.lblTitle2.Name = "lblTitle2"
        Me.lblTitle2.Size = New System.Drawing.Size(370, 25)
        Me.lblTitle2.TabIndex = 14
        Me.lblTitle2.Text = "Enter the name and password of a windows user. "
        '
        'frmAutoImportApplicantWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(550, 383)
        Me.Controls.Add(Me.ewWizard)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAutoImportApplicantWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auto Import Applicant Schedule Task Wizard"
        Me.ewWizard.ResumeLayout(False)
        Me.ewpPage1.ResumeLayout(False)
        Me.ewpPage2.ResumeLayout(False)
        Me.ewpPage2.PerformLayout()
        Me.ewpPage4.ResumeLayout(False)
        Me.ewpPage3.ResumeLayout(False)
        Me.ewpPage3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ewWizard As eZee.Common.eZeeWizard
    Friend WithEvents ewpPage1 As eZee.Common.eZeeWizardPage
    Friend WithEvents ewpPage3 As eZee.Common.eZeeWizardPage
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents ewpPage2 As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents rbtnMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDaily As System.Windows.Forms.RadioButton
    Friend WithEvents lblTaskType As System.Windows.Forms.Label
    Friend WithEvents lblMsg1 As System.Windows.Forms.Label
    Friend WithEvents lblTitle1 As System.Windows.Forms.Label
    Friend WithEvents dtpTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboWeekDay As System.Windows.Forms.ComboBox
    Friend WithEvents lblDay As System.Windows.Forms.Label
    Friend WithEvents lblWeekDay As System.Windows.Forms.Label
    Friend WithEvents lblTitle2 As System.Windows.Forms.Label
    Friend WithEvents ewpPage4 As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMsg3 As System.Windows.Forms.Label
    Friend WithEvents lblMsg2 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblConfirmPassword As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents txtCPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents lblTitle3 As System.Windows.Forms.Label
    Friend WithEvents chkRunNow As System.Windows.Forms.CheckBox
End Class
