﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.IO

#End Region

Public Class frmEmpAssetDeclarationList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpAssetDeclarationList"
    Private objAssetDeclare As New clsAssetdeclaration_master

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mblnIsSelfServiceInstall As Boolean
    Private mdsDoc As DataSet
    'SHANI (16 JUL 2015) -- End 
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsCombo As DataSet = Nothing
        Try

            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              FinancialYear._Object._Database_Start_Date.Date, _
                                              FinancialYear._Object._Database_End_Date.Date, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Nov 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData 'Sohail (16 Apr 2015)
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewAssetsDeclarationList = True Then                'Pinkal (02-Jul-2012) -- Start

                'Sohail (16 Apr 2015) -- Start
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                'dsList = clsAssetdeclaration_master.GetList("List", , CInt(cboEmployee.SelectedValue), , , , , txtCarRegNo.Text) 'Sohail (07 Mar 2012)
                Cursor.Current = Cursors.WaitCursor
                Dim dicDB As New Dictionary(Of String, String)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                Dim dicDBDate As New Dictionary(Of String, String)
                Dim dicDBYearId As New Dictionary(Of String, Integer)
                'Shani(24-Aug-2015) -- End

                If chkIncludeClosedYearTrans.Checked = True Then
                    Dim ds As DataSet = objMaster.Get_Database_Year_List("List", True, Company._Object._Companyunkid)
                    dicDB = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearName = p.Item("financialyear_name").ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.YearName)

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    dicDBDate = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .Date = (p.Item("start_date").ToString & "|" & p.Item("end_date").ToString).ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.Date)
                    dicDBYearId = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearId = CInt(p.Item("yearunkid"))}).ToDictionary(Function(x) x.DBName, Function(y) y.YearId)
                    'Shani(24-Aug-2015) -- End
                Else
                    dicDB.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._FinancialYear_Name)

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    dicDBDate.Add(FinancialYear._Object._DatabaseName, (eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date) & "|" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date)).ToString)
                    dicDBYearId.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid)
                    'Shani(24-Aug-2015) -- End

                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = clsAssetdeclaration_master.GetList("List", dicDB, , CInt(cboEmployee.SelectedValue), , , , , txtCarRegNo.Text, , "A.employeename")
                dsList = clsAssetdeclaration_master.GetList(User._Object._Userunkid, _
                                                            Company._Object._Companyunkid, _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                            ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                            "List", dicDB, dicDBDate, dicDBYearId, , _
                                                            CInt(cboEmployee.SelectedValue), , , , _
                                                            txtCarRegNo.Text, , "A.employeename")
                'Shani(24-Aug-2015) -- End

                'Sohail (16 Apr 2015) -- End

                lvAssetDeclare.Items.Clear()

                For Each dRow As DataRow In dsList.Tables("List").Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = dRow.Item("assetdeclarationunkid").ToString

                    lvItem.SubItems.Add(dRow.Item("employeename").ToString)
                    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dRow.Item("employeeunkid"))

                    lvItem.SubItems.Add(dRow.Item("noofemployment").ToString)
                    lvItem.SubItems.Add(dRow.Item("centerforwork").ToString)
                    lvItem.SubItems.Add(dRow.Item("position").ToString)
                    lvItem.SubItems.Add(dRow.Item("workstation").ToString) 'Sohail (02 Apr 2012)
                    'Sohail (16 Apr 2015) -- Start
                    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                    lvItem.SubItems.Add(dRow.Item("yearname").ToString)
                    lvItem.SubItems(objcolhYearName.Index).Tag = dRow.Item("databasename").ToString
                    'Sohail (16 Apr 2015) -- End

                    'Sohail (04 Jul 2019) -- Start
                    'TRA Issue - Support Issue Id # 0003897 - 70.1 - System allow us to select form of required year / period but exported report displays form of current year.
                    lvItem.SubItems.Add(dRow.Item("yearunkid").ToString)
                    'Sohail (04 Jul 2019) -- End

                    'Sohail (26 Mar 2012) -- Start
                    'TRA - ENHANCEMENT
                    If CBool(dRow.Item("isfinalsaved")) = True Then
                        lvItem.ForeColor = Color.Blue
                    Else
                        lvItem.ForeColor = Color.Black
                    End If
                    'Sohail (26 Mar 2012) -- End

                    lvAssetDeclare.Items.Add(lvItem)
                Next

                'Sohail (16 Apr 2015) -- Start
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                lvAssetDeclare.GroupingColumn = objcolhYearName
                lvAssetDeclare.DisplayGroups(True)
                'Sohail (16 Apr 2015) -- End

                If lvAssetDeclare.Items.Count > 10 Then
                    colhEmployee.Width = 200 - 18
                Else
                    colhEmployee.Width = colhEmployee.Width
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objMaster = Nothing
            Call objbtnSearch.ShowResult(CStr(lvAssetDeclare.Items.Count))
            Cursor.Current = Cursors.WaitCursor
            'Sohail (16 Apr 2015) -- End
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._Allow_AddAssetDeclaration
            btnEdit.Enabled = User._Object.Privilege._Allow_EditAssetDeclaration
            btnDelete.Enabled = User._Object.Privilege._Allow_DeleteAssetDeclaration
            mnuFinalSave.Enabled = User._Object.Privilege._Allow_FinalSaveAssetDeclaration
            mnuUnlockFinalSave.Enabled = User._Object.Privilege._Allow_UnlockFinalSaveAssetDeclaration

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuScanDocuments.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            mnuRemoveDeclaration.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            'Anjan (25 Oct 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmEmpAssetDeclarationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssetDeclare = New clsAssetdeclaration_master
        Try

            Call Set_Logo(Me, gApplicationType)
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (29 Jan 2013) -- End

            Call SetColor()
            Call FillCombo()
            'Call FillList()
            Call SetVisibility()

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            mblnIsSelfServiceInstall = IsSelfServiceExist()
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            'SHANI (16 JUL 2015) -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetDeclarationList_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssetDeclaration.SetMessages()
            objfrm._Other_ModuleNames = "clsAssetDeclaration"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events"
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmEmpAssetDeclaration
            If objFrm.displayDialog(-1, enAction.ADD_ONE, False) = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim objfrm As New frmEmpAssetDeclaration
        Try
            If lvAssetDeclare.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Asset Decalaration to perform Operation on it."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (16 Apr 2015) -- End

            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End
            If objAssetDeclare._Isfinalsaved = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (26 Mar 2012) -- End

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If


            If objfrm.displayDialog(CInt(lvAssetDeclare.SelectedItems(0).Tag), enAction.EDIT_ONE, False) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrm IsNot Nothing Then objfrm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Asset Decalaration to perform Operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
        If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (16 Apr 2015) -- End

        Try
            'Sohail (26 Mar 2012) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End

            If objAssetDeclare._Isfinalsaved = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (26 Mar 2012) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objAssetDeclare._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'objAssetDeclare._Isvoid = True
                'objAssetDeclare._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'objAssetDeclare._Voiduserunkid = User._Object._Userunkid
                objAssetDeclare.Void(CInt(lvAssetDeclare.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)

                If objAssetDeclare._Message <> "" Then
                    eZeeMsgBox.Show(objAssetDeclare._Message, enMsgBoxStyle.Information)
                Else
                    lvAssetDeclare.SelectedItems(0).Remove()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (16 Apr 2015) -- Start
    'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
#Region " Checkbox Events "
    Private Sub chkIncludePrevYears_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeClosedYearTrans.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIncludePrevYears_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (16 Apr 2015) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            txtCarRegNo.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (07 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuScanDocuments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuScanDocuments.Click
        'Sohail (26 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
        If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (16 Apr 2015) -- End

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        'Shani(24-Aug-2015) -- End

        If objAssetDeclare._Isfinalsaved = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (26 Mar 2012) -- End

        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName, True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanDocuments_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuRemoveDeclaration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemoveDeclaration.Click
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
        If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (16 Apr 2015) -- End

        'Sohail (26 Mar 2012) -- Start
        'TRA - ENHANCEMENT

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        'Shani(24-Aug-2015) -- End
        If objAssetDeclare._Isfinalsaved = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (26 Mar 2012) -- End

        Dim frm As New frmScanAttachmentList
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(enScanAttactRefId.ASSET_DECLARATION, CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sohail (07 Mar 2012) -- End

    'Sohail (26 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFinalSave.Click
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
        If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (16 Apr 2015) -- End

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        'Shani(24-Aug-2015) -- End
        If objAssetDeclare._Isfinalsaved = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim objDocument As New clsScan_Attach_Documents
        'Sohail (29 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        Dim objAssetBank As New clsAsset_bank_tran
        Dim decBankTotal As Decimal = 0
        'Sohail (29 Jan 2013) -- End
        Dim dtTable As DataTable
        Try
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetBank._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetBank._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End

            dtTable = objAssetBank._Datasource

            For Each dtRow As DataRow In dtTable.Rows
                decBankTotal += CDec(dtRow.Item("baseamount"))
            Next
            If decBankTotal.ToString(GUI.fmtCurrency) <> objAssetDeclare._CashExistingBank.ToString(GUI.fmtCurrency) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry! Total amount of respective banks should match with cash in existing bank."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (29 Jan 2013) -- End




            'Anjan [ 05 Dec 2013 ] -- Start
            'ENHANCEMENT : Requested by Andrew for TRA to set as optional.
            'dtTable = New DataView(objDocument.GetList("Attachment").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(lvAssetDeclare.SelectedItems(0).Tag) & " AND UNKID = " & CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag) & " ", "", DataViewRowState.CurrentRows).ToTable
            'If dtTable.Rows.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! First attach document before Final Save."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Anjan [ 05 Dec 2013 ] -- End



            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "After Final Save You can not Edit / Delete Asset Declaration." & vbCrLf & vbCrLf & "Are you sure you want to Final Save this Asset Declaration?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End

            objAssetDeclare._Isfinalsaved = True
            objAssetDeclare._Userunkid = User._Object._Userunkid
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            objAssetDeclare._Finalsavedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Unlockfinalsavedate = Nothing
            'Sohail (29 Jan 2013) -- End

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'If objAssetDeclare.Update() = False AndAlso objAssetDeclare._Message <> "" Then
            If objAssetDeclare.Update(True) = False AndAlso objAssetDeclare._Message <> "" Then
                'Sohail (29 Jan 2013) -- End
                eZeeMsgBox.Show(objAssetDeclare._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This Asset Declaration Final Saved Successfully."), enMsgBoxStyle.Information)
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuFinalSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuUnlockFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlockFinalSave.Click
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'Sohail (16 Apr 2015) -- Start
        'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
        If lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Text <> FinancialYear._Object._FinancialYear_Name Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (16 Apr 2015) -- End

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
        'Shani(24-Aug-2015) -- End

        If objAssetDeclare._Isfinalsaved = False Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Are you sure you want to Unlock Final Save this Asset Declaration?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End

            objAssetDeclare._Isfinalsaved = False
            objAssetDeclare._Userunkid = User._Object._Userunkid
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = ConfigParameter._Object._CurrentDateAndTime
            'Sohail (29 Jan 2013) -- End

            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'If objAssetDeclare.Update() = False AndAlso objAssetDeclare._Message <> "" Then
            If objAssetDeclare.Update(True) = False AndAlso objAssetDeclare._Message <> "" Then
                'Sohail (29 Jan 2013) -- End
                eZeeMsgBox.Show(objAssetDeclare._Message, enMsgBoxStyle.Information)
            Else
                Dim objDocument As New clsScan_Attach_Documents
                Dim strIds As String = ""
                Dim dtTable As DataTable


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dtTable = New DataView(objDocument.GetList("Attachment").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(lvAssetDeclare.SelectedItems(0).Tag) & " AND UNKID = " & CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag) & " ", "", DataViewRowState.CurrentRows).ToTable

                'S.SANDEEP |04-SEP-2021| -- START
                'dtTable = New DataView(objDocument.GetList(ConfigParameter._Object._Document_Path, "Attachment", "").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(lvAssetDeclare.SelectedItems(0).Tag) & " AND UNKID = " & CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag) & " ", "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(objDocument.GetList(ConfigParameter._Object._Document_Path, "Attachment", "", _
                                                           CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag), _
                                                           , , , , CBool(IIf(CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag) <= 0, True, False))).Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(lvAssetDeclare.SelectedItems(0).Tag) & " AND UNKID = " & CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag) & " ", "", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP |04-SEP-2021| -- END

                'Shani(24-Aug-2015) -- End


                If dtTable.Rows.Count > 0 Then
                    For Each dtRow As DataRow In dtTable.Rows
                        strIds &= "," & dtRow.Item("scanattachtranunkid").ToString
                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        Dim strError As String = "" : Dim intScanAttachRefId As Integer = CInt(dtRow("scanattachrefid"))
                        Dim strFileName As String = dtRow("fileuniquename").ToString
                        Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = intScanAttachRefId) Select (p.Item("Name").ToString)).FirstOrDefault
                        If mblnIsSelfServiceInstall Then

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.DeleteFile(dtRow("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                            'Hemant [8-April-2019] -- Start
                            'If clsFileUploadDownload.DeleteFile(dtRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            If clsFileUploadDownload.DeleteFile(dtRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                'Hemant [8-April-2019] -- End
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If File.Exists(strDocLocalPath) Then
                                    File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show("Configuration Path not Exist.", enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                        'SHANI (16 JUL 2015) -- End 
                    Next
                    If strIds.Trim.Length > 0 Then
                        strIds = Mid(strIds, 2)

                        If objDocument.Delete(strIds) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Problem in deleting information."), enMsgBoxStyle.Information)
                            Exit Try
                        End If
                    End If
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlockFinalSave_Click", mstrModuleName)
        Finally
            Call FillList()
        End Try

    End Sub
    'Sohail (26 Mar 2012) -- End

    'Sohail (02 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub mnuPrintDeclaration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDeclaration.Click
        If lvAssetDeclare.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Dim objRptAssetDeclare As New ArutiReports.clsAssetDeclaration
        Try

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta/TRA wants custom1 language in web
            objRptAssetDeclare._LanguageId = User._Object._Languageunkid
            'Anjan [22 Nov 2013 ] -- End



            objRptAssetDeclare._AssetDeclarationUnkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objRptAssetDeclare._EmployeeUnkid = CInt(lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString)
            objRptAssetDeclare._EmployeeCode = lvAssetDeclare.SelectedItems(0).SubItems(colhNoOfEmployment.Index).Text
            objRptAssetDeclare._EmployeeName = lvAssetDeclare.SelectedItems(0).SubItems(colhEmployee.Index).Text
            objRptAssetDeclare._WorkStation = lvAssetDeclare.SelectedItems(0).SubItems(colhWorkstation.Index).Text
            objRptAssetDeclare._Title = lvAssetDeclare.SelectedItems(0).SubItems(colhPosition.Index).Text
            'Sohail (16 Apr 2015) -- Start
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            objRptAssetDeclare._DatabaseName = lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Tag.ToString
            objAssetDeclare._DatabaseName = lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Tag.ToString
            'Sohail (16 Apr 2015) -- End
            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            objAssetDeclare._Assetdeclarationunkid(FinancialYear._Object._DatabaseName) = CInt(lvAssetDeclare.SelectedItems(0).Tag)
            'Shani(24-Aug-2015) -- End

            If objAssetDeclare._Isfinalsaved = True Then
                objRptAssetDeclare._NonOfficialDeclaration = ""
            Else
                objRptAssetDeclare._NonOfficialDeclaration = Language.getMessage(mstrModuleName, 14, "Non Official Declaration")
            End If
            'Sohail (29 Jan 2013) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRptAssetDeclare.generateReport(0, enPrintAction.Preview, enExportAction.None)
            'Sohail (04 Jul 2019) -- Start
            'TRA Issue - Support Issue Id # 0003897 - 70.1 - System allow us to select form of required year / period but exported report displays form of current year.
            'objRptAssetDeclare.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                     User._Object._Userunkid, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                     ConfigParameter._Object._ExportReportPath, _
            '                                     ConfigParameter._Object._OpenAfterExport, _
            '                                     0, enPrintAction.Preview, enExportAction.None)
            objRptAssetDeclare.generateReportNew(lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearName.Index).Tag.ToString, _
                                                 User._Object._Userunkid, _
                                                 CInt(lvAssetDeclare.SelectedItems(0).SubItems(objcolhYearId.Index).Text), _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                 ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, _
                                                 0, enPrintAction.Preview, enExportAction.None)
            'Sohail (04 Jul 2019) -- End
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintDeclaration_Click", mstrModuleName)
        End Try

    End Sub
    'Sohail (02 Apr 2012) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.EZeeSplitButton1.GradientBackColor = GUI._ButttonBackColor 
			Me.EZeeSplitButton1.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.colhCheckBox.Text = Language._Object.getCaption(CStr(Me.colhCheckBox.Tag), Me.colhCheckBox.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhNoOfEmployment.Text = Language._Object.getCaption(CStr(Me.colhNoOfEmployment.Tag), Me.colhNoOfEmployment.Text)
			Me.colhPosition.Text = Language._Object.getCaption(CStr(Me.colhPosition.Tag), Me.colhPosition.Text)
			Me.EZeeSplitButton1.Text = Language._Object.getCaption(Me.EZeeSplitButton1.Name, Me.EZeeSplitButton1.Text)
			Me.mnuScanDocuments.Text = Language._Object.getCaption(Me.mnuScanDocuments.Name, Me.mnuScanDocuments.Text)
			Me.mnuRemoveDeclaration.Text = Language._Object.getCaption(Me.mnuRemoveDeclaration.Name, Me.mnuRemoveDeclaration.Text)
			Me.mnuPrintDeclaration.Text = Language._Object.getCaption(Me.mnuPrintDeclaration.Name, Me.mnuPrintDeclaration.Text)
			Me.ToolStripMenuItem1.Text = Language._Object.getCaption(Me.ToolStripMenuItem1.Name, Me.ToolStripMenuItem1.Text)
			Me.mnuFinalSave.Text = Language._Object.getCaption(Me.mnuFinalSave.Name, Me.mnuFinalSave.Text)
			Me.mnuUnlockFinalSave.Text = Language._Object.getCaption(Me.mnuUnlockFinalSave.Name, Me.mnuUnlockFinalSave.Text)
			Me.colhWorkstation.Text = Language._Object.getCaption(CStr(Me.colhWorkstation.Tag), Me.colhWorkstation.Text)
			Me.lblCarRegistration.Text = Language._Object.getCaption(Me.lblCarRegistration.Name, Me.lblCarRegistration.Text)
			Me.colhCenterForWork.Text = Language._Object.getCaption(CStr(Me.colhCenterForWork.Tag), Me.colhCenterForWork.Text)
			Me.chkIncludeClosedYearTrans.Text = Language._Object.getCaption(Me.chkIncludeClosedYearTrans.Name, Me.chkIncludeClosedYearTrans.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one Asset Decalaration to perform Operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
			Language.setMessage(mstrModuleName, 3, "Select Employee")
			Language.setMessage(mstrModuleName, 4, "Please select atleast one transaction from list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 6, "After Final Save You can not Edit / Delete Asset Declaration." & vbCrLf & vbCrLf & "Are you sure you want to Final Save this Asset Declaration?")
			Language.setMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved.")
			Language.setMessage(mstrModuleName, 8, "Are you sure you want to Unlock Final Save this Asset Declaration?")
			Language.setMessage(mstrModuleName, 9, "This Asset Declaration Final Saved Successfully.")
			Language.setMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully.")
			Language.setMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved.")
			Language.setMessage(mstrModuleName, 12, "Problem in deleting information.")
			Language.setMessage(mstrModuleName, 13, "Sorry! Total amount of respective banks should match with cash in existing bank.")
			Language.setMessage(mstrModuleName, 14, "Non Official Declaration")
			Language.setMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class