﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpAssetDeclaration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objlblExchangeRate = New System.Windows.Forms.Label
        Me.wizAssetDeclaration = New eZee.Common.eZeeWizard
        Me.wizpBankShares = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep2 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtCashExistingBank_Step2 = New eZee.TextBox.NumericTextBox
        Me.pnlShareDividend = New System.Windows.Forms.Panel
        Me.dgvShareDividend_Step2 = New System.Windows.Forms.DataGridView
        Me.colhShareValue_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLocation_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDividendAmount_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyShare = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step3Shares = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step3Shares = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step3Shares = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblShareDividend_step2 = New System.Windows.Forms.Label
        Me.pnlBank = New System.Windows.Forms.Panel
        Me.dgvBank_Step2 = New System.Windows.Forms.DataGridView
        Me.colhBank_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAccount_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAmount_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyBank = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step3 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step3 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step3 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objcolhGUID_step3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblCashExistingBank_Step2 = New System.Windows.Forms.Label
        Me.txtCash_Step2 = New eZee.TextBox.NumericTextBox
        Me.lblCash_Step2 = New System.Windows.Forms.Label
        Me.lblFinanceToDate_Step2 = New System.Windows.Forms.Label
        Me.wizpEmployee = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblDate = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblInstruction = New System.Windows.Forms.Label
        Me.txtADInstruction = New System.Windows.Forms.TextBox
        Me.txtWorkStation_Step1 = New System.Windows.Forms.Label
        Me.lblWorkStation = New System.Windows.Forms.Label
        Me.txtCenterforWork_Step1 = New System.Windows.Forms.Label
        Me.txtOfficePosition_Step1 = New System.Windows.Forms.Label
        Me.txtNoOfEmployment_Step1 = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpFinYearEnd_Step1 = New System.Windows.Forms.DateTimePicker
        Me.lblFinYearEnd = New System.Windows.Forms.Label
        Me.dtpFinYearStart_Step1 = New System.Windows.Forms.DateTimePicker
        Me.lblFinYearStart = New System.Windows.Forms.Label
        Me.EZeeStraightLine4 = New eZee.Common.eZeeStraightLine
        Me.lblCenterforWork = New System.Windows.Forms.Label
        Me.lblOfficePosition = New System.Windows.Forms.Label
        Me.cboEmployee_Step1 = New System.Windows.Forms.ComboBox
        Me.lblStep1_NoOfEmployment = New System.Windows.Forms.Label
        Me.lblStep1_Employee = New System.Windows.Forms.Label
        Me.pic_Step1_Welcome = New System.Windows.Forms.PictureBox
        Me.wizpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.gbStep7 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStep7_Finish = New System.Windows.Forms.Label
        Me.picStep7_Finish = New System.Windows.Forms.PictureBox
        Me.wizpOtherResources = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep6 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblDebt_step6 = New System.Windows.Forms.Label
        Me.pnlDebts = New System.Windows.Forms.Panel
        Me.dgvDebts_step6 = New System.Windows.Forms.DataGridView
        Me.colhDebts_step6Debts = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLocation_step6Debts = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step6Debts = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyDebts = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhEmployee_step6Debts = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step6Debts = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step6Debts = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.wizpOtherBusiness = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep5 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlReources = New System.Windows.Forms.Panel
        Me.dgvOtherResources_step5 = New System.Windows.Forms.DataGridView
        Me.colhResources_step6Resources = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLocation_step6Resources = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step6Resources = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyResources = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhEmployee_step6Resources = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step6Resources = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step6Resources = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblResources_step5 = New System.Windows.Forms.Label
        Me.pnlOtherBusiness = New System.Windows.Forms.Panel
        Me.dgvOtherBusiness_step5 = New System.Windows.Forms.DataGridView
        Me.colhBusinessType_step6OtherBusiness = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPlaceClad_step6OtherBusiness = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step6OtherBusiness = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyBusiness = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step6OtherBusiness = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step6OtherBusiness = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step6OtherBusiness = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblOtherBusiness_step5 = New System.Windows.Forms.Label
        Me.wizpVehicleMachinery = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep4 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblAcqDateFormat = New System.Windows.Forms.Label
        Me.pnlMachinery = New System.Windows.Forms.Panel
        Me.dgvMachinery_step4 = New System.Windows.Forms.DataGridView
        Me.colhMachine_step5Machinery = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLocation_step5Machinery = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step5Machinery = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyMachine = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step5Machinery = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step5Machinery = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step5Machinery = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblGrindingMachine = New System.Windows.Forms.Label
        Me.pnlVehicles = New System.Windows.Forms.Panel
        Me.dgvVehicles_step4 = New System.Windows.Forms.DataGridView
        Me.colhCarMotorcycle_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCarModel_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCarColour_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCarRegNo_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCarUse_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCarAcqDate_step5Vehicle = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.colhValue_step5Vehicle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyVehicle = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step5Vehicle = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step5Vehicle = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step5Vehicle = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblVehicles_step4 = New System.Windows.Forms.Label
        Me.wizpBankAsset1 = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbStep3 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblIncompleteConstruction_step3 = New System.Windows.Forms.Label
        Me.pnlParkFarm = New System.Windows.Forms.Panel
        Me.dgvParkFarm_step3 = New System.Windows.Forms.DataGridView
        Me.colhParkFarmMines_step4Parks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAreaSize_step4Parks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPlaceClad_step4Parks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step4Parks = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyPark = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step4Parks = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step4Parks = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step4Parks = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblParkFarm_step3 = New System.Windows.Forms.Label
        Me.pnlHouseBuilding = New System.Windows.Forms.Panel
        Me.dgvHouse_step3 = New System.Windows.Forms.DataGridView
        Me.colhHomesBuilding_step4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLocation_step4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhValue_step4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrencyHouse = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.colhServant_step4House = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhWifeHusband_step4House = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.colhChildren_step4House = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.lblHouseBuilding_step3 = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.wizAssetDeclaration.SuspendLayout()
        Me.wizpBankShares.SuspendLayout()
        Me.gbStep2.SuspendLayout()
        Me.pnlShareDividend.SuspendLayout()
        CType(Me.dgvShareDividend_Step2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBank.SuspendLayout()
        CType(Me.dgvBank_Step2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpEmployee.SuspendLayout()
        Me.gbEmployee.SuspendLayout()
        CType(Me.pic_Step1_Welcome, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpFinish.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.gbStep7.SuspendLayout()
        CType(Me.picStep7_Finish, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpOtherResources.SuspendLayout()
        Me.gbStep6.SuspendLayout()
        Me.pnlDebts.SuspendLayout()
        CType(Me.dgvDebts_step6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpOtherBusiness.SuspendLayout()
        Me.gbStep5.SuspendLayout()
        Me.pnlReources.SuspendLayout()
        CType(Me.dgvOtherResources_step5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherBusiness.SuspendLayout()
        CType(Me.dgvOtherBusiness_step5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpVehicleMachinery.SuspendLayout()
        Me.gbStep4.SuspendLayout()
        Me.pnlMachinery.SuspendLayout()
        CType(Me.dgvMachinery_step4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlVehicles.SuspendLayout()
        CType(Me.dgvVehicles_step4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wizpBankAsset1.SuspendLayout()
        Me.gbStep3.SuspendLayout()
        Me.pnlParkFarm.SuspendLayout()
        CType(Me.dgvParkFarm_step3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHouseBuilding.SuspendLayout()
        CType(Me.dgvHouse_step3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objlblExchangeRate)
        Me.pnlMainInfo.Controls.Add(Me.wizAssetDeclaration)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(653, 589)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objlblExchangeRate
        '
        Me.objlblExchangeRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExchangeRate.Location = New System.Drawing.Point(9, 567)
        Me.objlblExchangeRate.Name = "objlblExchangeRate"
        Me.objlblExchangeRate.Size = New System.Drawing.Size(376, 16)
        Me.objlblExchangeRate.TabIndex = 206
        Me.objlblExchangeRate.Text = "1 = 1"
        Me.objlblExchangeRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'wizAssetDeclaration
        '
        Me.wizAssetDeclaration.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpEmployee)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpFinish)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpOtherResources)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpOtherBusiness)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpVehicleMachinery)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpBankAsset1)
        Me.wizAssetDeclaration.Controls.Add(Me.wizpBankShares)
        Me.wizAssetDeclaration.Dock = System.Windows.Forms.DockStyle.None
        Me.wizAssetDeclaration.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wizAssetDeclaration.HeaderImage = Nothing
        Me.wizAssetDeclaration.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wizAssetDeclaration.Location = New System.Drawing.Point(12, 12)
        Me.wizAssetDeclaration.Name = "wizAssetDeclaration"
        Me.wizAssetDeclaration.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizpEmployee, Me.wizpBankShares, Me.wizpBankAsset1, Me.wizpVehicleMachinery, Me.wizpOtherBusiness, Me.wizpOtherResources, Me.wizpFinish})
        Me.wizAssetDeclaration.SaveEnabled = True
        Me.wizAssetDeclaration.SaveText = "Save && Finish"
        Me.wizAssetDeclaration.SaveVisible = True
        Me.wizAssetDeclaration.SetSaveIndexBeforeFinishIndex = True
        Me.wizAssetDeclaration.Size = New System.Drawing.Size(631, 547)
        Me.wizAssetDeclaration.TabIndex = 4
        Me.wizAssetDeclaration.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wizAssetDeclaration.WelcomeImage = Nothing
        Me.wizAssetDeclaration.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wizpBankShares
        '
        Me.wizpBankShares.Controls.Add(Me.gbStep2)
        Me.wizpBankShares.Location = New System.Drawing.Point(0, 0)
        Me.wizpBankShares.Name = "wizpBankShares"
        Me.wizpBankShares.Size = New System.Drawing.Size(428, 208)
        Me.wizpBankShares.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpBankShares.TabIndex = 8
        Me.wizpBankShares.Title = "Bank Accounts, Shares Dividends"
        '
        'gbStep2
        '
        Me.gbStep2.BorderColor = System.Drawing.Color.Black
        Me.gbStep2.Checked = False
        Me.gbStep2.CollapseAllExceptThis = False
        Me.gbStep2.CollapsedHoverImage = Nothing
        Me.gbStep2.CollapsedNormalImage = Nothing
        Me.gbStep2.CollapsedPressedImage = Nothing
        Me.gbStep2.CollapseOnLoad = False
        Me.gbStep2.Controls.Add(Me.txtCashExistingBank_Step2)
        Me.gbStep2.Controls.Add(Me.pnlShareDividend)
        Me.gbStep2.Controls.Add(Me.lblShareDividend_step2)
        Me.gbStep2.Controls.Add(Me.pnlBank)
        Me.gbStep2.Controls.Add(Me.lblCashExistingBank_Step2)
        Me.gbStep2.Controls.Add(Me.txtCash_Step2)
        Me.gbStep2.Controls.Add(Me.lblCash_Step2)
        Me.gbStep2.Controls.Add(Me.lblFinanceToDate_Step2)
        Me.gbStep2.ExpandedHoverImage = Nothing
        Me.gbStep2.ExpandedNormalImage = Nothing
        Me.gbStep2.ExpandedPressedImage = Nothing
        Me.gbStep2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep2.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep2.HeaderHeight = 25
        Me.gbStep2.HeaderMessage = ""
        Me.gbStep2.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep2.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep2.HeightOnCollapse = 0
        Me.gbStep2.LeftTextSpace = 0
        Me.gbStep2.Location = New System.Drawing.Point(0, 1)
        Me.gbStep2.Name = "gbStep2"
        Me.gbStep2.OpenHeight = 300
        Me.gbStep2.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep2.ShowBorder = True
        Me.gbStep2.ShowCheckBox = False
        Me.gbStep2.ShowCollapseButton = False
        Me.gbStep2.ShowDefaultBorderColor = True
        Me.gbStep2.ShowDownButton = False
        Me.gbStep2.ShowHeader = True
        Me.gbStep2.Size = New System.Drawing.Size(630, 497)
        Me.gbStep2.TabIndex = 0
        Me.gbStep2.Temp = 0
        Me.gbStep2.Text = "Bank Accounts, Shares Dividends - Step 2 of 6"
        Me.gbStep2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCashExistingBank_Step2
        '
        Me.txtCashExistingBank_Step2.AllowNegative = True
        Me.txtCashExistingBank_Step2.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCashExistingBank_Step2.DigitsInGroup = 3
        Me.txtCashExistingBank_Step2.Enabled = False
        Me.txtCashExistingBank_Step2.Flags = 0
        Me.txtCashExistingBank_Step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCashExistingBank_Step2.Location = New System.Drawing.Point(422, 53)
        Me.txtCashExistingBank_Step2.MaxDecimalPlaces = 6
        Me.txtCashExistingBank_Step2.MaxWholeDigits = 21
        Me.txtCashExistingBank_Step2.Name = "txtCashExistingBank_Step2"
        Me.txtCashExistingBank_Step2.Prefix = ""
        Me.txtCashExistingBank_Step2.RangeMax = 1.7976931348623157E+308
        Me.txtCashExistingBank_Step2.RangeMin = -1.7976931348623157E+308
        Me.txtCashExistingBank_Step2.Size = New System.Drawing.Size(123, 21)
        Me.txtCashExistingBank_Step2.TabIndex = 1
        Me.txtCashExistingBank_Step2.Text = "0"
        Me.txtCashExistingBank_Step2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlShareDividend
        '
        Me.pnlShareDividend.Controls.Add(Me.dgvShareDividend_Step2)
        Me.pnlShareDividend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlShareDividend.Location = New System.Drawing.Point(8, 301)
        Me.pnlShareDividend.Name = "pnlShareDividend"
        Me.pnlShareDividend.Size = New System.Drawing.Size(609, 185)
        Me.pnlShareDividend.TabIndex = 176
        '
        'dgvShareDividend_Step2
        '
        Me.dgvShareDividend_Step2.AllowUserToResizeColumns = False
        Me.dgvShareDividend_Step2.AllowUserToResizeRows = False
        Me.dgvShareDividend_Step2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvShareDividend_Step2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhShareValue_step3, Me.colhLocation_step3, Me.colhDividendAmount_step3, Me.colhCurrencyShare, Me.colhServant_step3Shares, Me.colhWifeHusband_step3Shares, Me.colhChildren_step3Shares})
        Me.dgvShareDividend_Step2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvShareDividend_Step2.Location = New System.Drawing.Point(0, 0)
        Me.dgvShareDividend_Step2.Name = "dgvShareDividend_Step2"
        Me.dgvShareDividend_Step2.RowHeadersWidth = 25
        Me.dgvShareDividend_Step2.Size = New System.Drawing.Size(609, 185)
        Me.dgvShareDividend_Step2.TabIndex = 0
        '
        'colhShareValue_step3
        '
        Me.colhShareValue_step3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle31.Format = "F0"
        Me.colhShareValue_step3.DefaultCellStyle = DataGridViewCellStyle31
        Me.colhShareValue_step3.HeaderText = "Value of Shares"
        Me.colhShareValue_step3.Name = "colhShareValue_step3"
        Me.colhShareValue_step3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhLocation_step3
        '
        Me.colhLocation_step3.HeaderText = "Source"
        Me.colhLocation_step3.Name = "colhLocation_step3"
        '
        'colhDividendAmount_step3
        '
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle32.Format = "F0"
        Me.colhDividendAmount_step3.DefaultCellStyle = DataGridViewCellStyle32
        Me.colhDividendAmount_step3.HeaderText = "Dividend Amount"
        Me.colhDividendAmount_step3.Name = "colhDividendAmount_step3"
        Me.colhDividendAmount_step3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyShare
        '
        Me.colhCurrencyShare.HeaderText = "Currency"
        Me.colhCurrencyShare.Name = "colhCurrencyShare"
        '
        'colhServant_step3Shares
        '
        Me.colhServant_step3Shares.DecimalLength = 2
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle33.Format = "F2"
        DataGridViewCellStyle33.NullValue = Nothing
        Me.colhServant_step3Shares.DefaultCellStyle = DataGridViewCellStyle33
        Me.colhServant_step3Shares.HeaderText = "Employee (%)"
        Me.colhServant_step3Shares.Name = "colhServant_step3Shares"
        Me.colhServant_step3Shares.Width = 60
        '
        'colhWifeHusband_step3Shares
        '
        Me.colhWifeHusband_step3Shares.DecimalLength = 2
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle34.Format = "F2"
        DataGridViewCellStyle34.NullValue = Nothing
        Me.colhWifeHusband_step3Shares.DefaultCellStyle = DataGridViewCellStyle34
        Me.colhWifeHusband_step3Shares.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step3Shares.Name = "colhWifeHusband_step3Shares"
        Me.colhWifeHusband_step3Shares.Width = 60
        '
        'colhChildren_step3Shares
        '
        Me.colhChildren_step3Shares.DecimalLength = 2
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle35.Format = "F2"
        DataGridViewCellStyle35.NullValue = Nothing
        Me.colhChildren_step3Shares.DefaultCellStyle = DataGridViewCellStyle35
        Me.colhChildren_step3Shares.HeaderText = "Children (%)"
        Me.colhChildren_step3Shares.Name = "colhChildren_step3Shares"
        Me.colhChildren_step3Shares.Width = 60
        '
        'lblShareDividend_step2
        '
        Me.lblShareDividend_step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShareDividend_step2.Location = New System.Drawing.Point(13, 278)
        Me.lblShareDividend_step2.Name = "lblShareDividend_step2"
        Me.lblShareDividend_step2.Size = New System.Drawing.Size(340, 20)
        Me.lblShareDividend_step2.TabIndex = 175
        Me.lblShareDividend_step2.Text = "2. Shares and Dividends From Shares"
        '
        'pnlBank
        '
        Me.pnlBank.Controls.Add(Me.dgvBank_Step2)
        Me.pnlBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlBank.Location = New System.Drawing.Point(8, 93)
        Me.pnlBank.Name = "pnlBank"
        Me.pnlBank.Size = New System.Drawing.Size(609, 167)
        Me.pnlBank.TabIndex = 174
        '
        'dgvBank_Step2
        '
        Me.dgvBank_Step2.AllowUserToResizeColumns = False
        Me.dgvBank_Step2.AllowUserToResizeRows = False
        Me.dgvBank_Step2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBank_Step2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhBank_step3, Me.colhAccount_step3, Me.colhAmount_step3, Me.colhCurrencyBank, Me.colhServant_step3, Me.colhWifeHusband_step3, Me.colhChildren_step3, Me.objcolhGUID_step3})
        Me.dgvBank_Step2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBank_Step2.Location = New System.Drawing.Point(0, 0)
        Me.dgvBank_Step2.Name = "dgvBank_Step2"
        Me.dgvBank_Step2.RowHeadersWidth = 25
        Me.dgvBank_Step2.Size = New System.Drawing.Size(609, 167)
        Me.dgvBank_Step2.TabIndex = 0
        '
        'colhBank_step3
        '
        Me.colhBank_step3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhBank_step3.HeaderText = "Bank / Financial Institution"
        Me.colhBank_step3.Name = "colhBank_step3"
        '
        'colhAccount_step3
        '
        Me.colhAccount_step3.HeaderText = "Account Number"
        Me.colhAccount_step3.Name = "colhAccount_step3"
        '
        'colhAmount_step3
        '
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle36.Format = "F0"
        DataGridViewCellStyle36.NullValue = Nothing
        Me.colhAmount_step3.DefaultCellStyle = DataGridViewCellStyle36
        Me.colhAmount_step3.HeaderText = "Amount"
        Me.colhAmount_step3.Name = "colhAmount_step3"
        Me.colhAmount_step3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyBank
        '
        Me.colhCurrencyBank.HeaderText = "Currency"
        Me.colhCurrencyBank.Name = "colhCurrencyBank"
        '
        'colhServant_step3
        '
        Me.colhServant_step3.DecimalLength = 2
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle37.Format = "F2"
        DataGridViewCellStyle37.NullValue = Nothing
        Me.colhServant_step3.DefaultCellStyle = DataGridViewCellStyle37
        Me.colhServant_step3.HeaderText = "Employee (%)"
        Me.colhServant_step3.Name = "colhServant_step3"
        Me.colhServant_step3.Width = 60
        '
        'colhWifeHusband_step3
        '
        Me.colhWifeHusband_step3.DecimalLength = 2
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle38.Format = "F2"
        DataGridViewCellStyle38.NullValue = Nothing
        Me.colhWifeHusband_step3.DefaultCellStyle = DataGridViewCellStyle38
        Me.colhWifeHusband_step3.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step3.Name = "colhWifeHusband_step3"
        Me.colhWifeHusband_step3.Width = 60
        '
        'colhChildren_step3
        '
        Me.colhChildren_step3.DecimalLength = 2
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle39.Format = "F2"
        Me.colhChildren_step3.DefaultCellStyle = DataGridViewCellStyle39
        Me.colhChildren_step3.HeaderText = "Children (%)"
        Me.colhChildren_step3.Name = "colhChildren_step3"
        Me.colhChildren_step3.Width = 60
        '
        'objcolhGUID_step3
        '
        Me.objcolhGUID_step3.HeaderText = "GUID"
        Me.objcolhGUID_step3.Name = "objcolhGUID_step3"
        Me.objcolhGUID_step3.Visible = False
        '
        'lblCashExistingBank_Step2
        '
        Me.lblCashExistingBank_Step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashExistingBank_Step2.Location = New System.Drawing.Point(243, 52)
        Me.lblCashExistingBank_Step2.Name = "lblCashExistingBank_Step2"
        Me.lblCashExistingBank_Step2.Size = New System.Drawing.Size(173, 31)
        Me.lblCashExistingBank_Step2.TabIndex = 172
        Me.lblCashExistingBank_Step2.Text = "Cash Balances in Existing Banks or Financial Institutions"
        Me.lblCashExistingBank_Step2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCash_Step2
        '
        Me.txtCash_Step2.AllowNegative = True
        Me.txtCash_Step2.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCash_Step2.DigitsInGroup = 0
        Me.txtCash_Step2.Flags = 0
        Me.txtCash_Step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCash_Step2.Location = New System.Drawing.Point(95, 53)
        Me.txtCash_Step2.MaxDecimalPlaces = 6
        Me.txtCash_Step2.MaxWholeDigits = 21
        Me.txtCash_Step2.Name = "txtCash_Step2"
        Me.txtCash_Step2.Prefix = ""
        Me.txtCash_Step2.RangeMax = 1.7976931348623157E+308
        Me.txtCash_Step2.RangeMin = -1.7976931348623157E+308
        Me.txtCash_Step2.Size = New System.Drawing.Size(123, 21)
        Me.txtCash_Step2.TabIndex = 0
        Me.txtCash_Step2.Text = "0"
        Me.txtCash_Step2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCash_Step2
        '
        Me.lblCash_Step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCash_Step2.Location = New System.Drawing.Point(29, 55)
        Me.lblCash_Step2.Name = "lblCash_Step2"
        Me.lblCash_Step2.Size = New System.Drawing.Size(60, 16)
        Me.lblCash_Step2.TabIndex = 170
        Me.lblCash_Step2.Text = "Cash"
        Me.lblCash_Step2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFinanceToDate_Step2
        '
        Me.lblFinanceToDate_Step2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinanceToDate_Step2.Location = New System.Drawing.Point(13, 32)
        Me.lblFinanceToDate_Step2.Name = "lblFinanceToDate_Step2"
        Me.lblFinanceToDate_Step2.Size = New System.Drawing.Size(581, 20)
        Me.lblFinanceToDate_Step2.TabIndex = 169
        Me.lblFinanceToDate_Step2.Text = "1. Current Assets:  As per the date when this form was filled"
        '
        'wizpEmployee
        '
        Me.wizpEmployee.Controls.Add(Me.gbEmployee)
        Me.wizpEmployee.Description = "Description"
        Me.wizpEmployee.Location = New System.Drawing.Point(0, 0)
        Me.wizpEmployee.Name = "wizpEmployee"
        Me.wizpEmployee.Size = New System.Drawing.Size(631, 499)
        Me.wizpEmployee.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpEmployee.TabIndex = 7
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.lblDate)
        Me.gbEmployee.Controls.Add(Me.dtpDate)
        Me.gbEmployee.Controls.Add(Me.lblInstruction)
        Me.gbEmployee.Controls.Add(Me.txtADInstruction)
        Me.gbEmployee.Controls.Add(Me.txtWorkStation_Step1)
        Me.gbEmployee.Controls.Add(Me.lblWorkStation)
        Me.gbEmployee.Controls.Add(Me.txtCenterforWork_Step1)
        Me.gbEmployee.Controls.Add(Me.txtOfficePosition_Step1)
        Me.gbEmployee.Controls.Add(Me.txtNoOfEmployment_Step1)
        Me.gbEmployee.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployee.Controls.Add(Me.dtpFinYearEnd_Step1)
        Me.gbEmployee.Controls.Add(Me.lblFinYearEnd)
        Me.gbEmployee.Controls.Add(Me.dtpFinYearStart_Step1)
        Me.gbEmployee.Controls.Add(Me.lblFinYearStart)
        Me.gbEmployee.Controls.Add(Me.EZeeStraightLine4)
        Me.gbEmployee.Controls.Add(Me.lblCenterforWork)
        Me.gbEmployee.Controls.Add(Me.lblOfficePosition)
        Me.gbEmployee.Controls.Add(Me.cboEmployee_Step1)
        Me.gbEmployee.Controls.Add(Me.lblStep1_NoOfEmployment)
        Me.gbEmployee.Controls.Add(Me.lblStep1_Employee)
        Me.gbEmployee.Controls.Add(Me.pic_Step1_Welcome)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployee.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(0, 1)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(630, 497)
        Me.gbEmployee.TabIndex = 1
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.Text = "Employee Selection - Step 1 of 6"
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(252, 353)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(110, 16)
        Me.lblDate.TabIndex = 345
        Me.lblDate.Text = "Transaction Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate
        '
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(368, 351)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpDate.TabIndex = 343
        '
        'lblInstruction
        '
        Me.lblInstruction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstruction.Location = New System.Drawing.Point(249, 32)
        Me.lblInstruction.Name = "lblInstruction"
        Me.lblInstruction.Size = New System.Drawing.Size(350, 16)
        Me.lblInstruction.TabIndex = 342
        Me.lblInstruction.Text = "Instruction"
        Me.lblInstruction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtADInstruction
        '
        Me.txtADInstruction.BackColor = System.Drawing.Color.White
        Me.txtADInstruction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtADInstruction.Location = New System.Drawing.Point(249, 55)
        Me.txtADInstruction.Multiline = True
        Me.txtADInstruction.Name = "txtADInstruction"
        Me.txtADInstruction.ReadOnly = True
        Me.txtADInstruction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtADInstruction.Size = New System.Drawing.Size(350, 263)
        Me.txtADInstruction.TabIndex = 341
        '
        'txtWorkStation_Step1
        '
        Me.txtWorkStation_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkStation_Step1.Location = New System.Drawing.Point(368, 449)
        Me.txtWorkStation_Step1.Name = "txtWorkStation_Step1"
        Me.txtWorkStation_Step1.Size = New System.Drawing.Size(231, 16)
        Me.txtWorkStation_Step1.TabIndex = 339
        Me.txtWorkStation_Step1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorkStation
        '
        Me.lblWorkStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkStation.Location = New System.Drawing.Point(252, 449)
        Me.lblWorkStation.Name = "lblWorkStation"
        Me.lblWorkStation.Size = New System.Drawing.Size(110, 16)
        Me.lblWorkStation.TabIndex = 338
        Me.lblWorkStation.Text = "Work Station"
        Me.lblWorkStation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCenterforWork_Step1
        '
        Me.txtCenterforWork_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCenterforWork_Step1.Location = New System.Drawing.Point(368, 426)
        Me.txtCenterforWork_Step1.Name = "txtCenterforWork_Step1"
        Me.txtCenterforWork_Step1.Size = New System.Drawing.Size(231, 16)
        Me.txtCenterforWork_Step1.TabIndex = 336
        Me.txtCenterforWork_Step1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOfficePosition_Step1
        '
        Me.txtOfficePosition_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOfficePosition_Step1.Location = New System.Drawing.Point(368, 403)
        Me.txtOfficePosition_Step1.Name = "txtOfficePosition_Step1"
        Me.txtOfficePosition_Step1.Size = New System.Drawing.Size(231, 16)
        Me.txtOfficePosition_Step1.TabIndex = 335
        Me.txtOfficePosition_Step1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNoOfEmployment_Step1
        '
        Me.txtNoOfEmployment_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoOfEmployment_Step1.Location = New System.Drawing.Point(368, 379)
        Me.txtNoOfEmployment_Step1.Name = "txtNoOfEmployment_Step1"
        Me.txtNoOfEmployment_Step1.Size = New System.Drawing.Size(231, 16)
        Me.txtNoOfEmployment_Step1.TabIndex = 334
        Me.txtNoOfEmployment_Step1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(578, 324)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 332
        '
        'dtpFinYearEnd_Step1
        '
        Me.dtpFinYearEnd_Step1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinYearEnd_Step1.Checked = False
        Me.dtpFinYearEnd_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinYearEnd_Step1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinYearEnd_Step1.Location = New System.Drawing.Point(125, 474)
        Me.dtpFinYearEnd_Step1.Name = "dtpFinYearEnd_Step1"
        Me.dtpFinYearEnd_Step1.Size = New System.Drawing.Size(99, 21)
        Me.dtpFinYearEnd_Step1.TabIndex = 331
        Me.dtpFinYearEnd_Step1.Visible = False
        '
        'lblFinYearEnd
        '
        Me.lblFinYearEnd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinYearEnd.Location = New System.Drawing.Point(9, 476)
        Me.lblFinYearEnd.Name = "lblFinYearEnd"
        Me.lblFinYearEnd.Size = New System.Drawing.Size(110, 16)
        Me.lblFinYearEnd.TabIndex = 330
        Me.lblFinYearEnd.Text = "Financial Year End"
        Me.lblFinYearEnd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFinYearEnd.Visible = False
        '
        'dtpFinYearStart_Step1
        '
        Me.dtpFinYearStart_Step1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinYearStart_Step1.Checked = False
        Me.dtpFinYearStart_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinYearStart_Step1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinYearStart_Step1.Location = New System.Drawing.Point(347, 474)
        Me.dtpFinYearStart_Step1.Name = "dtpFinYearStart_Step1"
        Me.dtpFinYearStart_Step1.Size = New System.Drawing.Size(99, 21)
        Me.dtpFinYearStart_Step1.TabIndex = 329
        Me.dtpFinYearStart_Step1.Visible = False
        '
        'lblFinYearStart
        '
        Me.lblFinYearStart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinYearStart.Location = New System.Drawing.Point(231, 476)
        Me.lblFinYearStart.Name = "lblFinYearStart"
        Me.lblFinYearStart.Size = New System.Drawing.Size(110, 16)
        Me.lblFinYearStart.TabIndex = 328
        Me.lblFinYearStart.Text = "Financial Year Start"
        Me.lblFinYearStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFinYearStart.Visible = False
        '
        'EZeeStraightLine4
        '
        Me.EZeeStraightLine4.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeStraightLine4.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine4.Location = New System.Drawing.Point(3, 462)
        Me.EZeeStraightLine4.Name = "EZeeStraightLine4"
        Me.EZeeStraightLine4.Size = New System.Drawing.Size(235, 14)
        Me.EZeeStraightLine4.TabIndex = 327
        Me.EZeeStraightLine4.Text = "EZeeStraightLine4"
        Me.EZeeStraightLine4.Visible = False
        '
        'lblCenterforWork
        '
        Me.lblCenterforWork.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCenterforWork.Location = New System.Drawing.Point(252, 426)
        Me.lblCenterforWork.Name = "lblCenterforWork"
        Me.lblCenterforWork.Size = New System.Drawing.Size(110, 16)
        Me.lblCenterforWork.TabIndex = 325
        Me.lblCenterforWork.Text = "Department"
        Me.lblCenterforWork.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOfficePosition
        '
        Me.lblOfficePosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOfficePosition.Location = New System.Drawing.Point(252, 403)
        Me.lblOfficePosition.Name = "lblOfficePosition"
        Me.lblOfficePosition.Size = New System.Drawing.Size(110, 16)
        Me.lblOfficePosition.TabIndex = 323
        Me.lblOfficePosition.Text = "Office / Position"
        Me.lblOfficePosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee_Step1
        '
        Me.cboEmployee_Step1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee_Step1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee_Step1.FormattingEnabled = True
        Me.cboEmployee_Step1.Location = New System.Drawing.Point(368, 324)
        Me.cboEmployee_Step1.Name = "cboEmployee_Step1"
        Me.cboEmployee_Step1.Size = New System.Drawing.Size(204, 21)
        Me.cboEmployee_Step1.TabIndex = 319
        '
        'lblStep1_NoOfEmployment
        '
        Me.lblStep1_NoOfEmployment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1_NoOfEmployment.Location = New System.Drawing.Point(252, 379)
        Me.lblStep1_NoOfEmployment.Name = "lblStep1_NoOfEmployment"
        Me.lblStep1_NoOfEmployment.Size = New System.Drawing.Size(110, 16)
        Me.lblStep1_NoOfEmployment.TabIndex = 321
        Me.lblStep1_NoOfEmployment.Text = "Employment Code"
        Me.lblStep1_NoOfEmployment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep1_Employee
        '
        Me.lblStep1_Employee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1_Employee.Location = New System.Drawing.Point(252, 325)
        Me.lblStep1_Employee.Name = "lblStep1_Employee"
        Me.lblStep1_Employee.Size = New System.Drawing.Size(110, 16)
        Me.lblStep1_Employee.TabIndex = 320
        Me.lblStep1_Employee.Text = "Employee"
        Me.lblStep1_Employee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pic_Step1_Welcome
        '
        Me.pic_Step1_Welcome.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pic_Step1_Welcome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_Step1_Welcome.Image = Global.Aruti.Main.My.Resources.Resources.Asset_wizard
        Me.pic_Step1_Welcome.Location = New System.Drawing.Point(13, 58)
        Me.pic_Step1_Welcome.Name = "pic_Step1_Welcome"
        Me.pic_Step1_Welcome.Size = New System.Drawing.Size(225, 401)
        Me.pic_Step1_Welcome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pic_Step1_Welcome.TabIndex = 1
        Me.pic_Step1_Welcome.TabStop = False
        '
        'wizpFinish
        '
        Me.wizpFinish.BackgroundImage = Global.Aruti.Main.My.Resources.Resources.eZeeLogo
        Me.wizpFinish.Controls.Add(Me.Panel4)
        Me.wizpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wizpFinish.Name = "wizpFinish"
        Me.wizpFinish.Size = New System.Drawing.Size(631, 499)
        Me.wizpFinish.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.wizpFinish.TabIndex = 12
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.gbStep7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(631, 499)
        Me.Panel4.TabIndex = 0
        '
        'gbStep7
        '
        Me.gbStep7.BorderColor = System.Drawing.Color.Black
        Me.gbStep7.Checked = False
        Me.gbStep7.CollapseAllExceptThis = False
        Me.gbStep7.CollapsedHoverImage = Nothing
        Me.gbStep7.CollapsedNormalImage = Nothing
        Me.gbStep7.CollapsedPressedImage = Nothing
        Me.gbStep7.CollapseOnLoad = False
        Me.gbStep7.Controls.Add(Me.lblStep7_Finish)
        Me.gbStep7.Controls.Add(Me.picStep7_Finish)
        Me.gbStep7.ExpandedHoverImage = Nothing
        Me.gbStep7.ExpandedNormalImage = Nothing
        Me.gbStep7.ExpandedPressedImage = Nothing
        Me.gbStep7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep7.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep7.HeaderHeight = 25
        Me.gbStep7.HeaderMessage = ""
        Me.gbStep7.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep7.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep7.HeightOnCollapse = 0
        Me.gbStep7.LeftTextSpace = 0
        Me.gbStep7.Location = New System.Drawing.Point(14, 7)
        Me.gbStep7.Name = "gbStep7"
        Me.gbStep7.OpenHeight = 300
        Me.gbStep7.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep7.ShowBorder = True
        Me.gbStep7.ShowCheckBox = False
        Me.gbStep7.ShowCollapseButton = False
        Me.gbStep7.ShowDefaultBorderColor = True
        Me.gbStep7.ShowDownButton = False
        Me.gbStep7.ShowHeader = True
        Me.gbStep7.Size = New System.Drawing.Size(610, 446)
        Me.gbStep7.TabIndex = 2
        Me.gbStep7.Temp = 0
        Me.gbStep7.Text = "Finish"
        Me.gbStep7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStep7_Finish
        '
        Me.lblStep7_Finish.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7_Finish.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep7_Finish.Location = New System.Drawing.Point(277, 53)
        Me.lblStep7_Finish.Name = "lblStep7_Finish"
        Me.lblStep7_Finish.Size = New System.Drawing.Size(291, 345)
        Me.lblStep7_Finish.TabIndex = 154
        Me.lblStep7_Finish.Text = "The Assets Declaration Wizard Completed Successfully"
        Me.lblStep7_Finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picStep7_Finish
        '
        Me.picStep7_Finish.BackColor = System.Drawing.Color.WhiteSmoke
        Me.picStep7_Finish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep7_Finish.Image = Global.Aruti.Main.My.Resources.Resources.Asset_wizard
        Me.picStep7_Finish.Location = New System.Drawing.Point(13, 34)
        Me.picStep7_Finish.Name = "picStep7_Finish"
        Me.picStep7_Finish.Size = New System.Drawing.Size(225, 401)
        Me.picStep7_Finish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picStep7_Finish.TabIndex = 1
        Me.picStep7_Finish.TabStop = False
        '
        'wizpOtherResources
        '
        Me.wizpOtherResources.Controls.Add(Me.gbStep6)
        Me.wizpOtherResources.Location = New System.Drawing.Point(0, 0)
        Me.wizpOtherResources.Name = "wizpOtherResources"
        Me.wizpOtherResources.Size = New System.Drawing.Size(428, 208)
        Me.wizpOtherResources.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpOtherResources.TabIndex = 14
        '
        'gbStep6
        '
        Me.gbStep6.BorderColor = System.Drawing.Color.Black
        Me.gbStep6.Checked = False
        Me.gbStep6.CollapseAllExceptThis = False
        Me.gbStep6.CollapsedHoverImage = Nothing
        Me.gbStep6.CollapsedNormalImage = Nothing
        Me.gbStep6.CollapsedPressedImage = Nothing
        Me.gbStep6.CollapseOnLoad = False
        Me.gbStep6.Controls.Add(Me.lblDebt_step6)
        Me.gbStep6.Controls.Add(Me.pnlDebts)
        Me.gbStep6.ExpandedHoverImage = Nothing
        Me.gbStep6.ExpandedNormalImage = Nothing
        Me.gbStep6.ExpandedPressedImage = Nothing
        Me.gbStep6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep6.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep6.HeaderHeight = 25
        Me.gbStep6.HeaderMessage = ""
        Me.gbStep6.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep6.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep6.HeightOnCollapse = 0
        Me.gbStep6.LeftTextSpace = 0
        Me.gbStep6.Location = New System.Drawing.Point(0, 1)
        Me.gbStep6.Name = "gbStep6"
        Me.gbStep6.OpenHeight = 300
        Me.gbStep6.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep6.ShowBorder = True
        Me.gbStep6.ShowCheckBox = False
        Me.gbStep6.ShowCollapseButton = False
        Me.gbStep6.ShowDefaultBorderColor = True
        Me.gbStep6.ShowDownButton = False
        Me.gbStep6.ShowHeader = True
        Me.gbStep6.Size = New System.Drawing.Size(630, 497)
        Me.gbStep6.TabIndex = 4
        Me.gbStep6.Temp = 0
        Me.gbStep6.Text = "Debt - Step 6 of 6"
        Me.gbStep6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDebt_step6
        '
        Me.lblDebt_step6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebt_step6.Location = New System.Drawing.Point(15, 40)
        Me.lblDebt_step6.Name = "lblDebt_step6"
        Me.lblDebt_step6.Size = New System.Drawing.Size(256, 20)
        Me.lblDebt_step6.TabIndex = 187
        Me.lblDebt_step6.Text = "9. Debt:"
        '
        'pnlDebts
        '
        Me.pnlDebts.Controls.Add(Me.dgvDebts_step6)
        Me.pnlDebts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlDebts.Location = New System.Drawing.Point(8, 63)
        Me.pnlDebts.Name = "pnlDebts"
        Me.pnlDebts.Size = New System.Drawing.Size(609, 201)
        Me.pnlDebts.TabIndex = 180
        '
        'dgvDebts_step6
        '
        Me.dgvDebts_step6.AllowUserToResizeColumns = False
        Me.dgvDebts_step6.AllowUserToResizeRows = False
        Me.dgvDebts_step6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebts_step6.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhDebts_step6Debts, Me.colhLocation_step6Debts, Me.colhValue_step6Debts, Me.colhCurrencyDebts, Me.colhEmployee_step6Debts, Me.colhWifeHusband_step6Debts, Me.colhChildren_step6Debts})
        Me.dgvDebts_step6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDebts_step6.Location = New System.Drawing.Point(0, 0)
        Me.dgvDebts_step6.Name = "dgvDebts_step6"
        Me.dgvDebts_step6.RowHeadersWidth = 25
        Me.dgvDebts_step6.Size = New System.Drawing.Size(609, 201)
        Me.dgvDebts_step6.TabIndex = 0
        '
        'colhDebts_step6Debts
        '
        Me.colhDebts_step6Debts.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhDebts_step6Debts.HeaderText = "Debt"
        Me.colhDebts_step6Debts.Name = "colhDebts_step6Debts"
        '
        'colhLocation_step6Debts
        '
        Me.colhLocation_step6Debts.HeaderText = "Location"
        Me.colhLocation_step6Debts.Name = "colhLocation_step6Debts"
        '
        'colhValue_step6Debts
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.colhValue_step6Debts.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhValue_step6Debts.HeaderText = "Value"
        Me.colhValue_step6Debts.Name = "colhValue_step6Debts"
        Me.colhValue_step6Debts.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyDebts
        '
        Me.colhCurrencyDebts.HeaderText = "Currency"
        Me.colhCurrencyDebts.Name = "colhCurrencyDebts"
        '
        'colhEmployee_step6Debts
        '
        Me.colhEmployee_step6Debts.DecimalLength = 2
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        Me.colhEmployee_step6Debts.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhEmployee_step6Debts.HeaderText = "Employee (%)"
        Me.colhEmployee_step6Debts.Name = "colhEmployee_step6Debts"
        Me.colhEmployee_step6Debts.Width = 60
        '
        'colhWifeHusband_step6Debts
        '
        Me.colhWifeHusband_step6Debts.DecimalLength = 2
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F2"
        Me.colhWifeHusband_step6Debts.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhWifeHusband_step6Debts.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step6Debts.Name = "colhWifeHusband_step6Debts"
        Me.colhWifeHusband_step6Debts.Width = 60
        '
        'colhChildren_step6Debts
        '
        Me.colhChildren_step6Debts.DecimalLength = 2
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.colhChildren_step6Debts.DefaultCellStyle = DataGridViewCellStyle4
        Me.colhChildren_step6Debts.HeaderText = "Children (%)"
        Me.colhChildren_step6Debts.Name = "colhChildren_step6Debts"
        Me.colhChildren_step6Debts.Width = 60
        '
        'wizpOtherBusiness
        '
        Me.wizpOtherBusiness.Controls.Add(Me.gbStep5)
        Me.wizpOtherBusiness.Location = New System.Drawing.Point(0, 0)
        Me.wizpOtherBusiness.Name = "wizpOtherBusiness"
        Me.wizpOtherBusiness.Size = New System.Drawing.Size(428, 208)
        Me.wizpOtherBusiness.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpOtherBusiness.TabIndex = 10
        '
        'gbStep5
        '
        Me.gbStep5.BorderColor = System.Drawing.Color.Black
        Me.gbStep5.Checked = False
        Me.gbStep5.CollapseAllExceptThis = False
        Me.gbStep5.CollapsedHoverImage = Nothing
        Me.gbStep5.CollapsedNormalImage = Nothing
        Me.gbStep5.CollapsedPressedImage = Nothing
        Me.gbStep5.CollapseOnLoad = False
        Me.gbStep5.Controls.Add(Me.pnlReources)
        Me.gbStep5.Controls.Add(Me.lblResources_step5)
        Me.gbStep5.Controls.Add(Me.pnlOtherBusiness)
        Me.gbStep5.Controls.Add(Me.lblOtherBusiness_step5)
        Me.gbStep5.ExpandedHoverImage = Nothing
        Me.gbStep5.ExpandedNormalImage = Nothing
        Me.gbStep5.ExpandedPressedImage = Nothing
        Me.gbStep5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep5.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep5.HeaderHeight = 25
        Me.gbStep5.HeaderMessage = ""
        Me.gbStep5.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep5.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep5.HeightOnCollapse = 0
        Me.gbStep5.LeftTextSpace = 0
        Me.gbStep5.Location = New System.Drawing.Point(0, 1)
        Me.gbStep5.Name = "gbStep5"
        Me.gbStep5.OpenHeight = 300
        Me.gbStep5.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep5.ShowBorder = True
        Me.gbStep5.ShowCheckBox = False
        Me.gbStep5.ShowCollapseButton = False
        Me.gbStep5.ShowDefaultBorderColor = True
        Me.gbStep5.ShowDownButton = False
        Me.gbStep5.ShowHeader = True
        Me.gbStep5.Size = New System.Drawing.Size(630, 497)
        Me.gbStep5.TabIndex = 3
        Me.gbStep5.Temp = 0
        Me.gbStep5.Text = "Other Business - Step 5 of 6"
        Me.gbStep5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlReources
        '
        Me.pnlReources.Controls.Add(Me.dgvOtherResources_step5)
        Me.pnlReources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlReources.Location = New System.Drawing.Point(8, 299)
        Me.pnlReources.Name = "pnlReources"
        Me.pnlReources.Size = New System.Drawing.Size(609, 187)
        Me.pnlReources.TabIndex = 186
        '
        'dgvOtherResources_step5
        '
        Me.dgvOtherResources_step5.AllowUserToResizeColumns = False
        Me.dgvOtherResources_step5.AllowUserToResizeRows = False
        Me.dgvOtherResources_step5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOtherResources_step5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhResources_step6Resources, Me.colhLocation_step6Resources, Me.colhValue_step6Resources, Me.colhCurrencyResources, Me.colhEmployee_step6Resources, Me.colhWifeHusband_step6Resources, Me.colhChildren_step6Resources})
        Me.dgvOtherResources_step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOtherResources_step5.Location = New System.Drawing.Point(0, 0)
        Me.dgvOtherResources_step5.Name = "dgvOtherResources_step5"
        Me.dgvOtherResources_step5.RowHeadersWidth = 25
        Me.dgvOtherResources_step5.Size = New System.Drawing.Size(609, 187)
        Me.dgvOtherResources_step5.TabIndex = 0
        '
        'colhResources_step6Resources
        '
        Me.colhResources_step6Resources.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhResources_step6Resources.HeaderText = "Resources / Commercial Interests"
        Me.colhResources_step6Resources.Name = "colhResources_step6Resources"
        '
        'colhLocation_step6Resources
        '
        Me.colhLocation_step6Resources.HeaderText = "Location"
        Me.colhLocation_step6Resources.Name = "colhLocation_step6Resources"
        '
        'colhValue_step6Resources
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "F0"
        Me.colhValue_step6Resources.DefaultCellStyle = DataGridViewCellStyle5
        Me.colhValue_step6Resources.HeaderText = "Value"
        Me.colhValue_step6Resources.Name = "colhValue_step6Resources"
        Me.colhValue_step6Resources.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyResources
        '
        Me.colhCurrencyResources.HeaderText = "Currency"
        Me.colhCurrencyResources.Name = "colhCurrencyResources"
        '
        'colhEmployee_step6Resources
        '
        Me.colhEmployee_step6Resources.DecimalLength = 2
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F2"
        Me.colhEmployee_step6Resources.DefaultCellStyle = DataGridViewCellStyle6
        Me.colhEmployee_step6Resources.HeaderText = "Employee (%)"
        Me.colhEmployee_step6Resources.Name = "colhEmployee_step6Resources"
        Me.colhEmployee_step6Resources.Width = 60
        '
        'colhWifeHusband_step6Resources
        '
        Me.colhWifeHusband_step6Resources.DecimalLength = 2
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F2"
        Me.colhWifeHusband_step6Resources.DefaultCellStyle = DataGridViewCellStyle7
        Me.colhWifeHusband_step6Resources.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step6Resources.Name = "colhWifeHusband_step6Resources"
        Me.colhWifeHusband_step6Resources.Width = 60
        '
        'colhChildren_step6Resources
        '
        Me.colhChildren_step6Resources.DecimalLength = 2
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "F2"
        Me.colhChildren_step6Resources.DefaultCellStyle = DataGridViewCellStyle8
        Me.colhChildren_step6Resources.HeaderText = "Children (%)"
        Me.colhChildren_step6Resources.Name = "colhChildren_step6Resources"
        Me.colhChildren_step6Resources.Width = 60
        '
        'lblResources_step5
        '
        Me.lblResources_step5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResources_step5.Location = New System.Drawing.Point(19, 277)
        Me.lblResources_step5.Name = "lblResources_step5"
        Me.lblResources_step5.Size = New System.Drawing.Size(424, 20)
        Me.lblResources_step5.TabIndex = 181
        Me.lblResources_step5.Text = "8. Resources or other commercial interests, including livestock, etc.:"
        '
        'pnlOtherBusiness
        '
        Me.pnlOtherBusiness.Controls.Add(Me.dgvOtherBusiness_step5)
        Me.pnlOtherBusiness.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOtherBusiness.Location = New System.Drawing.Point(8, 63)
        Me.pnlOtherBusiness.Name = "pnlOtherBusiness"
        Me.pnlOtherBusiness.Size = New System.Drawing.Size(609, 201)
        Me.pnlOtherBusiness.TabIndex = 180
        '
        'dgvOtherBusiness_step5
        '
        Me.dgvOtherBusiness_step5.AllowUserToResizeColumns = False
        Me.dgvOtherBusiness_step5.AllowUserToResizeRows = False
        Me.dgvOtherBusiness_step5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOtherBusiness_step5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhBusinessType_step6OtherBusiness, Me.colhPlaceClad_step6OtherBusiness, Me.colhValue_step6OtherBusiness, Me.colhCurrencyBusiness, Me.colhServant_step6OtherBusiness, Me.colhWifeHusband_step6OtherBusiness, Me.colhChildren_step6OtherBusiness})
        Me.dgvOtherBusiness_step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOtherBusiness_step5.Location = New System.Drawing.Point(0, 0)
        Me.dgvOtherBusiness_step5.Name = "dgvOtherBusiness_step5"
        Me.dgvOtherBusiness_step5.RowHeadersWidth = 25
        Me.dgvOtherBusiness_step5.Size = New System.Drawing.Size(609, 201)
        Me.dgvOtherBusiness_step5.TabIndex = 0
        '
        'colhBusinessType_step6OtherBusiness
        '
        Me.colhBusinessType_step6OtherBusiness.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhBusinessType_step6OtherBusiness.HeaderText = "Type of Business"
        Me.colhBusinessType_step6OtherBusiness.Name = "colhBusinessType_step6OtherBusiness"
        '
        'colhPlaceClad_step6OtherBusiness
        '
        Me.colhPlaceClad_step6OtherBusiness.HeaderText = "Location"
        Me.colhPlaceClad_step6OtherBusiness.Name = "colhPlaceClad_step6OtherBusiness"
        '
        'colhValue_step6OtherBusiness
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "F0"
        Me.colhValue_step6OtherBusiness.DefaultCellStyle = DataGridViewCellStyle9
        Me.colhValue_step6OtherBusiness.HeaderText = "Value"
        Me.colhValue_step6OtherBusiness.Name = "colhValue_step6OtherBusiness"
        Me.colhValue_step6OtherBusiness.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyBusiness
        '
        Me.colhCurrencyBusiness.HeaderText = "Currency"
        Me.colhCurrencyBusiness.Name = "colhCurrencyBusiness"
        '
        'colhServant_step6OtherBusiness
        '
        Me.colhServant_step6OtherBusiness.DecimalLength = 2
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "F2"
        Me.colhServant_step6OtherBusiness.DefaultCellStyle = DataGridViewCellStyle10
        Me.colhServant_step6OtherBusiness.HeaderText = "Employee (%)"
        Me.colhServant_step6OtherBusiness.Name = "colhServant_step6OtherBusiness"
        Me.colhServant_step6OtherBusiness.Width = 60
        '
        'colhWifeHusband_step6OtherBusiness
        '
        Me.colhWifeHusband_step6OtherBusiness.DecimalLength = 2
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "F2"
        Me.colhWifeHusband_step6OtherBusiness.DefaultCellStyle = DataGridViewCellStyle11
        Me.colhWifeHusband_step6OtherBusiness.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step6OtherBusiness.Name = "colhWifeHusband_step6OtherBusiness"
        Me.colhWifeHusband_step6OtherBusiness.Width = 60
        '
        'colhChildren_step6OtherBusiness
        '
        Me.colhChildren_step6OtherBusiness.DecimalLength = 2
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "F2"
        Me.colhChildren_step6OtherBusiness.DefaultCellStyle = DataGridViewCellStyle12
        Me.colhChildren_step6OtherBusiness.HeaderText = "Children (%)"
        Me.colhChildren_step6OtherBusiness.Name = "colhChildren_step6OtherBusiness"
        Me.colhChildren_step6OtherBusiness.Width = 60
        '
        'lblOtherBusiness_step5
        '
        Me.lblOtherBusiness_step5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherBusiness_step5.Location = New System.Drawing.Point(15, 40)
        Me.lblOtherBusiness_step5.Name = "lblOtherBusiness_step5"
        Me.lblOtherBusiness_step5.Size = New System.Drawing.Size(340, 20)
        Me.lblOtherBusiness_step5.TabIndex = 179
        Me.lblOtherBusiness_step5.Text = "7. Other businesses (shops, bars, lodges, etc.)"
        '
        'wizpVehicleMachinery
        '
        Me.wizpVehicleMachinery.Controls.Add(Me.gbStep4)
        Me.wizpVehicleMachinery.Location = New System.Drawing.Point(0, 0)
        Me.wizpVehicleMachinery.Name = "wizpVehicleMachinery"
        Me.wizpVehicleMachinery.Size = New System.Drawing.Size(428, 208)
        Me.wizpVehicleMachinery.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpVehicleMachinery.TabIndex = 9
        '
        'gbStep4
        '
        Me.gbStep4.BorderColor = System.Drawing.Color.Black
        Me.gbStep4.Checked = False
        Me.gbStep4.CollapseAllExceptThis = False
        Me.gbStep4.CollapsedHoverImage = Nothing
        Me.gbStep4.CollapsedNormalImage = Nothing
        Me.gbStep4.CollapsedPressedImage = Nothing
        Me.gbStep4.CollapseOnLoad = False
        Me.gbStep4.Controls.Add(Me.lblAcqDateFormat)
        Me.gbStep4.Controls.Add(Me.pnlMachinery)
        Me.gbStep4.Controls.Add(Me.lblGrindingMachine)
        Me.gbStep4.Controls.Add(Me.pnlVehicles)
        Me.gbStep4.Controls.Add(Me.lblVehicles_step4)
        Me.gbStep4.ExpandedHoverImage = Nothing
        Me.gbStep4.ExpandedNormalImage = Nothing
        Me.gbStep4.ExpandedPressedImage = Nothing
        Me.gbStep4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep4.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep4.HeaderHeight = 25
        Me.gbStep4.HeaderMessage = ""
        Me.gbStep4.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep4.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep4.HeightOnCollapse = 0
        Me.gbStep4.LeftTextSpace = 0
        Me.gbStep4.Location = New System.Drawing.Point(0, 1)
        Me.gbStep4.Name = "gbStep4"
        Me.gbStep4.OpenHeight = 300
        Me.gbStep4.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep4.ShowBorder = True
        Me.gbStep4.ShowCheckBox = False
        Me.gbStep4.ShowCollapseButton = False
        Me.gbStep4.ShowDefaultBorderColor = True
        Me.gbStep4.ShowDownButton = False
        Me.gbStep4.ShowHeader = True
        Me.gbStep4.Size = New System.Drawing.Size(630, 497)
        Me.gbStep4.TabIndex = 2
        Me.gbStep4.Temp = 0
        Me.gbStep4.Text = "Vehicles, Machinery - Step 4 of 6"
        Me.gbStep4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAcqDateFormat
        '
        Me.lblAcqDateFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcqDateFormat.Location = New System.Drawing.Point(358, 37)
        Me.lblAcqDateFormat.Name = "lblAcqDateFormat"
        Me.lblAcqDateFormat.Size = New System.Drawing.Size(259, 20)
        Me.lblAcqDateFormat.TabIndex = 177
        Me.lblAcqDateFormat.Text = "*Acquisition Date Format yyyy/MM/dd"
        '
        'pnlMachinery
        '
        Me.pnlMachinery.Controls.Add(Me.dgvMachinery_step4)
        Me.pnlMachinery.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMachinery.Location = New System.Drawing.Point(8, 278)
        Me.pnlMachinery.Name = "pnlMachinery"
        Me.pnlMachinery.Size = New System.Drawing.Size(609, 209)
        Me.pnlMachinery.TabIndex = 179
        '
        'dgvMachinery_step4
        '
        Me.dgvMachinery_step4.AllowUserToResizeColumns = False
        Me.dgvMachinery_step4.AllowUserToResizeRows = False
        Me.dgvMachinery_step4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMachinery_step4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhMachine_step5Machinery, Me.colhLocation_step5Machinery, Me.colhValue_step5Machinery, Me.colhCurrencyMachine, Me.colhServant_step5Machinery, Me.colhWifeHusband_step5Machinery, Me.colhChildren_step5Machinery})
        Me.dgvMachinery_step4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMachinery_step4.Location = New System.Drawing.Point(0, 0)
        Me.dgvMachinery_step4.Name = "dgvMachinery_step4"
        Me.dgvMachinery_step4.RowHeadersWidth = 25
        Me.dgvMachinery_step4.Size = New System.Drawing.Size(609, 209)
        Me.dgvMachinery_step4.TabIndex = 0
        '
        'colhMachine_step5Machinery
        '
        Me.colhMachine_step5Machinery.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhMachine_step5Machinery.HeaderText = "Grinding Machine, Industries/Factories,  other machines"
        Me.colhMachine_step5Machinery.Name = "colhMachine_step5Machinery"
        '
        'colhLocation_step5Machinery
        '
        Me.colhLocation_step5Machinery.HeaderText = "Location"
        Me.colhLocation_step5Machinery.Name = "colhLocation_step5Machinery"
        '
        'colhValue_step5Machinery
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "F0"
        Me.colhValue_step5Machinery.DefaultCellStyle = DataGridViewCellStyle13
        Me.colhValue_step5Machinery.HeaderText = "Value"
        Me.colhValue_step5Machinery.Name = "colhValue_step5Machinery"
        Me.colhValue_step5Machinery.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyMachine
        '
        Me.colhCurrencyMachine.HeaderText = "Currency"
        Me.colhCurrencyMachine.Name = "colhCurrencyMachine"
        '
        'colhServant_step5Machinery
        '
        Me.colhServant_step5Machinery.DecimalLength = 2
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "F2"
        Me.colhServant_step5Machinery.DefaultCellStyle = DataGridViewCellStyle14
        Me.colhServant_step5Machinery.HeaderText = "Employee (%)"
        Me.colhServant_step5Machinery.Name = "colhServant_step5Machinery"
        Me.colhServant_step5Machinery.Width = 60
        '
        'colhWifeHusband_step5Machinery
        '
        Me.colhWifeHusband_step5Machinery.DecimalLength = 2
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "F2"
        Me.colhWifeHusband_step5Machinery.DefaultCellStyle = DataGridViewCellStyle15
        Me.colhWifeHusband_step5Machinery.HeaderText = "Wife Husband (%)"
        Me.colhWifeHusband_step5Machinery.Name = "colhWifeHusband_step5Machinery"
        Me.colhWifeHusband_step5Machinery.Width = 60
        '
        'colhChildren_step5Machinery
        '
        Me.colhChildren_step5Machinery.DecimalLength = 2
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "F2"
        Me.colhChildren_step5Machinery.DefaultCellStyle = DataGridViewCellStyle16
        Me.colhChildren_step5Machinery.HeaderText = "Children (%)"
        Me.colhChildren_step5Machinery.Name = "colhChildren_step5Machinery"
        Me.colhChildren_step5Machinery.Width = 60
        '
        'lblGrindingMachine
        '
        Me.lblGrindingMachine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrindingMachine.Location = New System.Drawing.Point(11, 255)
        Me.lblGrindingMachine.Name = "lblGrindingMachine"
        Me.lblGrindingMachine.Size = New System.Drawing.Size(464, 20)
        Me.lblGrindingMachine.TabIndex = 178
        Me.lblGrindingMachine.Text = "6. Machinery for Grinding Grain, Industries/Factories and other machines"
        '
        'pnlVehicles
        '
        Me.pnlVehicles.Controls.Add(Me.dgvVehicles_step4)
        Me.pnlVehicles.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlVehicles.Location = New System.Drawing.Point(8, 60)
        Me.pnlVehicles.Name = "pnlVehicles"
        Me.pnlVehicles.Size = New System.Drawing.Size(609, 175)
        Me.pnlVehicles.TabIndex = 177
        '
        'dgvVehicles_step4
        '
        Me.dgvVehicles_step4.AllowUserToResizeColumns = False
        Me.dgvVehicles_step4.AllowUserToResizeRows = False
        Me.dgvVehicles_step4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVehicles_step4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhCarMotorcycle_step5Vehicle, Me.colhCarModel_step5Vehicle, Me.colhCarColour_step5Vehicle, Me.colhCarRegNo_step5Vehicle, Me.colhCarUse_step5Vehicle, Me.colhCarAcqDate_step5Vehicle, Me.colhValue_step5Vehicle, Me.colhCurrencyVehicle, Me.colhServant_step5Vehicle, Me.colhWifeHusband_step5Vehicle, Me.colhChildren_step5Vehicle})
        Me.dgvVehicles_step4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvVehicles_step4.Location = New System.Drawing.Point(0, 0)
        Me.dgvVehicles_step4.Name = "dgvVehicles_step4"
        Me.dgvVehicles_step4.RowHeadersWidth = 25
        Me.dgvVehicles_step4.Size = New System.Drawing.Size(609, 175)
        Me.dgvVehicles_step4.TabIndex = 0
        '
        'colhCarMotorcycle_step5Vehicle
        '
        Me.colhCarMotorcycle_step5Vehicle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhCarMotorcycle_step5Vehicle.HeaderText = "Type and name of car / motorcycle"
        Me.colhCarMotorcycle_step5Vehicle.MinimumWidth = 100
        Me.colhCarMotorcycle_step5Vehicle.Name = "colhCarMotorcycle_step5Vehicle"
        '
        'colhCarModel_step5Vehicle
        '
        Me.colhCarModel_step5Vehicle.HeaderText = "Model"
        Me.colhCarModel_step5Vehicle.Name = "colhCarModel_step5Vehicle"
        '
        'colhCarColour_step5Vehicle
        '
        Me.colhCarColour_step5Vehicle.HeaderText = "Colour"
        Me.colhCarColour_step5Vehicle.Name = "colhCarColour_step5Vehicle"
        '
        'colhCarRegNo_step5Vehicle
        '
        Me.colhCarRegNo_step5Vehicle.HeaderText = "Reg. No."
        Me.colhCarRegNo_step5Vehicle.Name = "colhCarRegNo_step5Vehicle"
        '
        'colhCarUse_step5Vehicle
        '
        Me.colhCarUse_step5Vehicle.HeaderText = "Vehicle Use"
        Me.colhCarUse_step5Vehicle.Name = "colhCarUse_step5Vehicle"
        '
        'colhCarAcqDate_step5Vehicle
        '
        DataGridViewCellStyle17.Format = "####/##/##"
        Me.colhCarAcqDate_step5Vehicle.DefaultCellStyle = DataGridViewCellStyle17
        Me.colhCarAcqDate_step5Vehicle.HeaderText = "Acquisition Date"
        Me.colhCarAcqDate_step5Vehicle.Mask = "####/##/##"
        Me.colhCarAcqDate_step5Vehicle.Name = "colhCarAcqDate_step5Vehicle"
        '
        'colhValue_step5Vehicle
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "F0"
        Me.colhValue_step5Vehicle.DefaultCellStyle = DataGridViewCellStyle18
        Me.colhValue_step5Vehicle.HeaderText = "Value"
        Me.colhValue_step5Vehicle.Name = "colhValue_step5Vehicle"
        Me.colhValue_step5Vehicle.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyVehicle
        '
        Me.colhCurrencyVehicle.HeaderText = "Currency"
        Me.colhCurrencyVehicle.Name = "colhCurrencyVehicle"
        '
        'colhServant_step5Vehicle
        '
        Me.colhServant_step5Vehicle.DecimalLength = 2
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.Format = "F2"
        Me.colhServant_step5Vehicle.DefaultCellStyle = DataGridViewCellStyle19
        Me.colhServant_step5Vehicle.HeaderText = "Employee (%)"
        Me.colhServant_step5Vehicle.Name = "colhServant_step5Vehicle"
        Me.colhServant_step5Vehicle.Width = 60
        '
        'colhWifeHusband_step5Vehicle
        '
        Me.colhWifeHusband_step5Vehicle.DecimalLength = 2
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle20.Format = "F2"
        Me.colhWifeHusband_step5Vehicle.DefaultCellStyle = DataGridViewCellStyle20
        Me.colhWifeHusband_step5Vehicle.HeaderText = "Wife Husband (%)"
        Me.colhWifeHusband_step5Vehicle.Name = "colhWifeHusband_step5Vehicle"
        Me.colhWifeHusband_step5Vehicle.Width = 60
        '
        'colhChildren_step5Vehicle
        '
        Me.colhChildren_step5Vehicle.DecimalLength = 2
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Format = "F2"
        Me.colhChildren_step5Vehicle.DefaultCellStyle = DataGridViewCellStyle21
        Me.colhChildren_step5Vehicle.HeaderText = "Children (%)"
        Me.colhChildren_step5Vehicle.Name = "colhChildren_step5Vehicle"
        Me.colhChildren_step5Vehicle.Width = 60
        '
        'lblVehicles_step4
        '
        Me.lblVehicles_step4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVehicles_step4.Location = New System.Drawing.Point(12, 37)
        Me.lblVehicles_step4.Name = "lblVehicles_step4"
        Me.lblVehicles_step4.Size = New System.Drawing.Size(340, 20)
        Me.lblVehicles_step4.TabIndex = 176
        Me.lblVehicles_step4.Text = "5. Vehicle and other transport equipments"
        '
        'wizpBankAsset1
        '
        Me.wizpBankAsset1.Controls.Add(Me.gbStep3)
        Me.wizpBankAsset1.Location = New System.Drawing.Point(0, 0)
        Me.wizpBankAsset1.Name = "wizpBankAsset1"
        Me.wizpBankAsset1.Size = New System.Drawing.Size(428, 208)
        Me.wizpBankAsset1.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizpBankAsset1.TabIndex = 13
        '
        'gbStep3
        '
        Me.gbStep3.BorderColor = System.Drawing.Color.Black
        Me.gbStep3.Checked = False
        Me.gbStep3.CollapseAllExceptThis = False
        Me.gbStep3.CollapsedHoverImage = Nothing
        Me.gbStep3.CollapsedNormalImage = Nothing
        Me.gbStep3.CollapsedPressedImage = Nothing
        Me.gbStep3.CollapseOnLoad = False
        Me.gbStep3.Controls.Add(Me.lblIncompleteConstruction_step3)
        Me.gbStep3.Controls.Add(Me.pnlParkFarm)
        Me.gbStep3.Controls.Add(Me.lblParkFarm_step3)
        Me.gbStep3.Controls.Add(Me.pnlHouseBuilding)
        Me.gbStep3.Controls.Add(Me.lblHouseBuilding_step3)
        Me.gbStep3.ExpandedHoverImage = Nothing
        Me.gbStep3.ExpandedNormalImage = Nothing
        Me.gbStep3.ExpandedPressedImage = Nothing
        Me.gbStep3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbStep3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbStep3.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbStep3.HeaderHeight = 25
        Me.gbStep3.HeaderMessage = ""
        Me.gbStep3.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbStep3.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbStep3.HeightOnCollapse = 0
        Me.gbStep3.LeftTextSpace = 0
        Me.gbStep3.Location = New System.Drawing.Point(0, 1)
        Me.gbStep3.Name = "gbStep3"
        Me.gbStep3.OpenHeight = 300
        Me.gbStep3.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbStep3.ShowBorder = True
        Me.gbStep3.ShowCheckBox = False
        Me.gbStep3.ShowCollapseButton = False
        Me.gbStep3.ShowDefaultBorderColor = True
        Me.gbStep3.ShowDownButton = False
        Me.gbStep3.ShowHeader = True
        Me.gbStep3.Size = New System.Drawing.Size(630, 497)
        Me.gbStep3.TabIndex = 3
        Me.gbStep3.Temp = 0
        Me.gbStep3.Text = "Houses and Other Buildings, Plots, Farms and Mine Sites. - Step 3 of 6"
        Me.gbStep3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIncompleteConstruction_step3
        '
        Me.lblIncompleteConstruction_step3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncompleteConstruction_step3.Location = New System.Drawing.Point(211, 35)
        Me.lblIncompleteConstruction_step3.Name = "lblIncompleteConstruction_step3"
        Me.lblIncompleteConstruction_step3.Size = New System.Drawing.Size(283, 20)
        Me.lblIncompleteConstruction_step3.TabIndex = 179
        Me.lblIncompleteConstruction_step3.Text = "(including buildings where construction is not complete) "
        '
        'pnlParkFarm
        '
        Me.pnlParkFarm.Controls.Add(Me.dgvParkFarm_step3)
        Me.pnlParkFarm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlParkFarm.Location = New System.Drawing.Point(8, 277)
        Me.pnlParkFarm.Name = "pnlParkFarm"
        Me.pnlParkFarm.Size = New System.Drawing.Size(609, 209)
        Me.pnlParkFarm.TabIndex = 178
        '
        'dgvParkFarm_step3
        '
        Me.dgvParkFarm_step3.AllowUserToResizeColumns = False
        Me.dgvParkFarm_step3.AllowUserToResizeRows = False
        Me.dgvParkFarm_step3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvParkFarm_step3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhParkFarmMines_step4Parks, Me.colhAreaSize_step4Parks, Me.colhPlaceClad_step4Parks, Me.colhValue_step4Parks, Me.colhCurrencyPark, Me.colhServant_step4Parks, Me.colhWifeHusband_step4Parks, Me.colhChildren_step4Parks})
        Me.dgvParkFarm_step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvParkFarm_step3.Location = New System.Drawing.Point(0, 0)
        Me.dgvParkFarm_step3.Name = "dgvParkFarm_step3"
        Me.dgvParkFarm_step3.RowHeadersWidth = 25
        Me.dgvParkFarm_step3.Size = New System.Drawing.Size(609, 209)
        Me.dgvParkFarm_step3.TabIndex = 0
        '
        'colhParkFarmMines_step4Parks
        '
        Me.colhParkFarmMines_step4Parks.HeaderText = "Plots, Farms and Mines"
        Me.colhParkFarmMines_step4Parks.Name = "colhParkFarmMines_step4Parks"
        '
        'colhAreaSize_step4Parks
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.Format = "F0"
        Me.colhAreaSize_step4Parks.DefaultCellStyle = DataGridViewCellStyle22
        Me.colhAreaSize_step4Parks.HeaderText = "Size of Area"
        Me.colhAreaSize_step4Parks.Name = "colhAreaSize_step4Parks"
        Me.colhAreaSize_step4Parks.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhPlaceClad_step4Parks
        '
        Me.colhPlaceClad_step4Parks.HeaderText = "Location"
        Me.colhPlaceClad_step4Parks.Name = "colhPlaceClad_step4Parks"
        '
        'colhValue_step4Parks
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle23.Format = "F0"
        Me.colhValue_step4Parks.DefaultCellStyle = DataGridViewCellStyle23
        Me.colhValue_step4Parks.HeaderText = "Value"
        Me.colhValue_step4Parks.Name = "colhValue_step4Parks"
        Me.colhValue_step4Parks.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyPark
        '
        Me.colhCurrencyPark.HeaderText = "Currency"
        Me.colhCurrencyPark.Name = "colhCurrencyPark"
        '
        'colhServant_step4Parks
        '
        Me.colhServant_step4Parks.DecimalLength = 2
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle24.Format = "F2"
        Me.colhServant_step4Parks.DefaultCellStyle = DataGridViewCellStyle24
        Me.colhServant_step4Parks.HeaderText = "Employee (%)"
        Me.colhServant_step4Parks.Name = "colhServant_step4Parks"
        Me.colhServant_step4Parks.Width = 60
        '
        'colhWifeHusband_step4Parks
        '
        Me.colhWifeHusband_step4Parks.DecimalLength = 2
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle25.Format = "F2"
        Me.colhWifeHusband_step4Parks.DefaultCellStyle = DataGridViewCellStyle25
        Me.colhWifeHusband_step4Parks.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step4Parks.Name = "colhWifeHusband_step4Parks"
        Me.colhWifeHusband_step4Parks.Width = 60
        '
        'colhChildren_step4Parks
        '
        Me.colhChildren_step4Parks.DecimalLength = 2
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "F2"
        Me.colhChildren_step4Parks.DefaultCellStyle = DataGridViewCellStyle26
        Me.colhChildren_step4Parks.HeaderText = "Children (%)"
        Me.colhChildren_step4Parks.Name = "colhChildren_step4Parks"
        Me.colhChildren_step4Parks.Width = 60
        '
        'lblParkFarm_step3
        '
        Me.lblParkFarm_step3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblParkFarm_step3.Location = New System.Drawing.Point(9, 254)
        Me.lblParkFarm_step3.Name = "lblParkFarm_step3"
        Me.lblParkFarm_step3.Size = New System.Drawing.Size(340, 20)
        Me.lblParkFarm_step3.TabIndex = 177
        Me.lblParkFarm_step3.Text = "4. Parks, Farms, Mines "
        '
        'pnlHouseBuilding
        '
        Me.pnlHouseBuilding.Controls.Add(Me.dgvHouse_step3)
        Me.pnlHouseBuilding.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlHouseBuilding.Location = New System.Drawing.Point(8, 58)
        Me.pnlHouseBuilding.Name = "pnlHouseBuilding"
        Me.pnlHouseBuilding.Size = New System.Drawing.Size(609, 179)
        Me.pnlHouseBuilding.TabIndex = 176
        '
        'dgvHouse_step3
        '
        Me.dgvHouse_step3.AllowUserToResizeColumns = False
        Me.dgvHouse_step3.AllowUserToResizeRows = False
        Me.dgvHouse_step3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHouse_step3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhHomesBuilding_step4, Me.colhLocation_step4, Me.colhValue_step4, Me.colhCurrencyHouse, Me.colhServant_step4House, Me.colhWifeHusband_step4House, Me.colhChildren_step4House})
        Me.dgvHouse_step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHouse_step3.Location = New System.Drawing.Point(0, 0)
        Me.dgvHouse_step3.Name = "dgvHouse_step3"
        Me.dgvHouse_step3.RowHeadersWidth = 25
        Me.dgvHouse_step3.Size = New System.Drawing.Size(609, 179)
        Me.dgvHouse_step3.TabIndex = 0
        '
        'colhHomesBuilding_step4
        '
        Me.colhHomesBuilding_step4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhHomesBuilding_step4.HeaderText = "Homes/Buildings/Others"
        Me.colhHomesBuilding_step4.Name = "colhHomesBuilding_step4"
        '
        'colhLocation_step4
        '
        Me.colhLocation_step4.HeaderText = "Location"
        Me.colhLocation_step4.Name = "colhLocation_step4"
        '
        'colhValue_step4
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Format = "F0"
        Me.colhValue_step4.DefaultCellStyle = DataGridViewCellStyle27
        Me.colhValue_step4.HeaderText = "Value"
        Me.colhValue_step4.Name = "colhValue_step4"
        Me.colhValue_step4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhCurrencyHouse
        '
        Me.colhCurrencyHouse.HeaderText = "Currency"
        Me.colhCurrencyHouse.Name = "colhCurrencyHouse"
        '
        'colhServant_step4House
        '
        Me.colhServant_step4House.DecimalLength = 2
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle28.Format = "F2"
        Me.colhServant_step4House.DefaultCellStyle = DataGridViewCellStyle28
        Me.colhServant_step4House.HeaderText = "Employee (%)"
        Me.colhServant_step4House.Name = "colhServant_step4House"
        Me.colhServant_step4House.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhServant_step4House.Width = 60
        '
        'colhWifeHusband_step4House
        '
        Me.colhWifeHusband_step4House.DecimalLength = 2
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle29.Format = "F2"
        Me.colhWifeHusband_step4House.DefaultCellStyle = DataGridViewCellStyle29
        Me.colhWifeHusband_step4House.HeaderText = "Wife / Husband (%)"
        Me.colhWifeHusband_step4House.Name = "colhWifeHusband_step4House"
        Me.colhWifeHusband_step4House.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhWifeHusband_step4House.Width = 60
        '
        'colhChildren_step4House
        '
        Me.colhChildren_step4House.DecimalLength = 2
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle30.Format = "F2"
        Me.colhChildren_step4House.DefaultCellStyle = DataGridViewCellStyle30
        Me.colhChildren_step4House.HeaderText = "Children (%)"
        Me.colhChildren_step4House.Name = "colhChildren_step4House"
        Me.colhChildren_step4House.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhChildren_step4House.Width = 60
        '
        'lblHouseBuilding_step3
        '
        Me.lblHouseBuilding_step3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHouseBuilding_step3.Location = New System.Drawing.Point(11, 35)
        Me.lblHouseBuilding_step3.Name = "lblHouseBuilding_step3"
        Me.lblHouseBuilding_step3.Size = New System.Drawing.Size(192, 20)
        Me.lblHouseBuilding_step3.TabIndex = 175
        Me.lblHouseBuilding_step3.Text = "3. Houses and Other Buildings"
        '
        'frmEmpAssetDeclaration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 589)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpAssetDeclaration"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Asset Declaration"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.wizAssetDeclaration.ResumeLayout(False)
        Me.wizpBankShares.ResumeLayout(False)
        Me.gbStep2.ResumeLayout(False)
        Me.gbStep2.PerformLayout()
        Me.pnlShareDividend.ResumeLayout(False)
        CType(Me.dgvShareDividend_Step2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBank.ResumeLayout(False)
        CType(Me.dgvBank_Step2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpEmployee.ResumeLayout(False)
        Me.gbEmployee.ResumeLayout(False)
        Me.gbEmployee.PerformLayout()
        CType(Me.pic_Step1_Welcome, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpFinish.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.gbStep7.ResumeLayout(False)
        CType(Me.picStep7_Finish, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpOtherResources.ResumeLayout(False)
        Me.gbStep6.ResumeLayout(False)
        Me.pnlDebts.ResumeLayout(False)
        CType(Me.dgvDebts_step6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpOtherBusiness.ResumeLayout(False)
        Me.gbStep5.ResumeLayout(False)
        Me.pnlReources.ResumeLayout(False)
        CType(Me.dgvOtherResources_step5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherBusiness.ResumeLayout(False)
        CType(Me.dgvOtherBusiness_step5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpVehicleMachinery.ResumeLayout(False)
        Me.gbStep4.ResumeLayout(False)
        Me.pnlMachinery.ResumeLayout(False)
        CType(Me.dgvMachinery_step4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlVehicles.ResumeLayout(False)
        CType(Me.dgvVehicles_step4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wizpBankAsset1.ResumeLayout(False)
        Me.gbStep3.ResumeLayout(False)
        Me.pnlParkFarm.ResumeLayout(False)
        CType(Me.dgvParkFarm_step3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHouseBuilding.ResumeLayout(False)
        CType(Me.dgvHouse_step3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents wizAssetDeclaration As eZee.Common.eZeeWizard
    Friend WithEvents wizpEmployee As eZee.Common.eZeeWizardPage
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pic_Step1_Welcome As System.Windows.Forms.PictureBox
    Friend WithEvents wizpBankShares As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep2 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wizpBankAsset1 As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep3 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wizpVehicleMachinery As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep4 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wizpOtherBusiness As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep5 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents wizpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents gbStep7 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblStep7_Finish As System.Windows.Forms.Label
    Friend WithEvents picStep7_Finish As System.Windows.Forms.PictureBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpFinYearEnd_Step1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFinYearEnd As System.Windows.Forms.Label
    Friend WithEvents dtpFinYearStart_Step1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFinYearStart As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine4 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblCenterforWork As System.Windows.Forms.Label
    Friend WithEvents lblOfficePosition As System.Windows.Forms.Label
    Friend WithEvents cboEmployee_Step1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblStep1_NoOfEmployment As System.Windows.Forms.Label
    Friend WithEvents lblStep1_Employee As System.Windows.Forms.Label
    Friend WithEvents pnlShareDividend As System.Windows.Forms.Panel
    Friend WithEvents dgvShareDividend_Step2 As System.Windows.Forms.DataGridView
    Friend WithEvents lblShareDividend_step2 As System.Windows.Forms.Label
    Friend WithEvents pnlBank As System.Windows.Forms.Panel
    Friend WithEvents dgvBank_Step2 As System.Windows.Forms.DataGridView
    Friend WithEvents lblCashExistingBank_Step2 As System.Windows.Forms.Label
    Friend WithEvents txtCash_Step2 As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCash_Step2 As System.Windows.Forms.Label
    Friend WithEvents lblFinanceToDate_Step2 As System.Windows.Forms.Label
    Friend WithEvents lblIncompleteConstruction_step3 As System.Windows.Forms.Label
    Friend WithEvents pnlParkFarm As System.Windows.Forms.Panel
    Friend WithEvents dgvParkFarm_step3 As System.Windows.Forms.DataGridView
    Friend WithEvents lblParkFarm_step3 As System.Windows.Forms.Label
    Friend WithEvents pnlHouseBuilding As System.Windows.Forms.Panel
    Friend WithEvents dgvHouse_step3 As System.Windows.Forms.DataGridView
    Friend WithEvents lblHouseBuilding_step3 As System.Windows.Forms.Label
    Friend WithEvents lblResources_step5 As System.Windows.Forms.Label
    Friend WithEvents pnlOtherBusiness As System.Windows.Forms.Panel
    Friend WithEvents dgvOtherBusiness_step5 As System.Windows.Forms.DataGridView
    Friend WithEvents lblOtherBusiness_step5 As System.Windows.Forms.Label
    Friend WithEvents pnlMachinery As System.Windows.Forms.Panel
    Friend WithEvents dgvMachinery_step4 As System.Windows.Forms.DataGridView
    Friend WithEvents lblGrindingMachine As System.Windows.Forms.Label
    Friend WithEvents pnlVehicles As System.Windows.Forms.Panel
    Friend WithEvents dgvVehicles_step4 As System.Windows.Forms.DataGridView
    Friend WithEvents lblVehicles_step4 As System.Windows.Forms.Label
    Friend WithEvents txtNoOfEmployment_Step1 As System.Windows.Forms.Label
    Friend WithEvents txtOfficePosition_Step1 As System.Windows.Forms.Label
    Friend WithEvents txtCenterforWork_Step1 As System.Windows.Forms.Label
    Friend WithEvents txtCashExistingBank_Step2 As eZee.TextBox.NumericTextBox
    Friend WithEvents lblWorkStation As System.Windows.Forms.Label
    Friend WithEvents txtWorkStation_Step1 As System.Windows.Forms.Label
    Friend WithEvents lblInstruction As System.Windows.Forms.Label
    Friend WithEvents txtADInstruction As System.Windows.Forms.TextBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents colhShareValue_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLocation_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDividendAmount_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyShare As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step3Shares As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step3Shares As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step3Shares As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhHomesBuilding_step4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLocation_step4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyHouse As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step4House As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step4House As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step4House As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhMachine_step5Machinery As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLocation_step5Machinery As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step5Machinery As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyMachine As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step5Machinery As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step5Machinery As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step5Machinery As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhBusinessType_step6OtherBusiness As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPlaceClad_step6OtherBusiness As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step6OtherBusiness As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyBusiness As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step6OtherBusiness As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step6OtherBusiness As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step6OtherBusiness As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents colhParkFarmMines_step4Parks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAreaSize_step4Parks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPlaceClad_step4Parks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step4Parks As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyPark As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step4Parks As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step4Parks As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step4Parks As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objlblExchangeRate As System.Windows.Forms.Label
    Friend WithEvents colhCarMotorcycle_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCarModel_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCarColour_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCarRegNo_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCarUse_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCarAcqDate_step5Vehicle As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents colhValue_step5Vehicle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyVehicle As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step5Vehicle As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step5Vehicle As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step5Vehicle As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents wizpOtherResources As eZee.Common.eZeeWizardPage
    Friend WithEvents gbStep6 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDebt_step6 As System.Windows.Forms.Label
    Friend WithEvents pnlDebts As System.Windows.Forms.Panel
    Friend WithEvents dgvDebts_step6 As System.Windows.Forms.DataGridView
    Friend WithEvents pnlReources As System.Windows.Forms.Panel
    Friend WithEvents dgvOtherResources_step5 As System.Windows.Forms.DataGridView
    Friend WithEvents colhDebts_step6Debts As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLocation_step6Debts As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step6Debts As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyDebts As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhEmployee_step6Debts As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step6Debts As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step6Debts As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhResources_step6Resources As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLocation_step6Resources As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhValue_step6Resources As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyResources As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhEmployee_step6Resources As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step6Resources As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step6Resources As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents lblAcqDateFormat As System.Windows.Forms.Label
    Friend WithEvents colhBank_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAccount_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAmount_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrencyBank As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents colhServant_step3 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhWifeHusband_step3 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents colhChildren_step3 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objcolhGUID_step3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
