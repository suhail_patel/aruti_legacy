﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUnits_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmUnits_AddEdit"
    Private mblnCancel As Boolean = True
    Private objUnit As clsUnits
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUnitUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUnitUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintUnitUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboSection.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboUnitGroup.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 07 NOV 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objUnit._Code
        txtName.Text = objUnit._Name
        txtDescription.Text = objUnit._Description
        cboSection.SelectedValue = objUnit._Sectionunkid
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        cboUnitGroup.SelectedValue = objUnit._Unitgroupunkid
        'S.SANDEEP [ 07 NOV 2011 ] -- END
    End Sub

    Private Sub SetValue()
        objUnit._Code = txtCode.Text
        objUnit._Name = txtName.Text
        objUnit._Description = txtDescription.Text
        objUnit._Sectionunkid = CInt(cboSection.SelectedValue)
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objUnit._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
        'S.SANDEEP [ 07 NOV 2011 ] -- END
    End Sub

    Private Sub FillCombo()
        Try
            Dim objUnit As New clsSections
            Dim dsList As New DataSet
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objUnitGrp As New clsUnitGroup
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            dsList = objUnit.getComboList("Section", True)

            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Section")
            End With
            cboSection.SelectedValue = 0


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objUnitGrp.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            cboUnitGroup.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddSection.Enabled = User._Object.Privilege._AddSection
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUnits_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUnit = Nothing
    End Sub

    Private Sub frmUnits_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUnits_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUnits_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUnits_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUnits_AddEdit_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUnit = New clsUnits
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objUnit._Unitunkid = mintUnitUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSections_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUnits.SetMessages()
            objfrm._Other_ModuleNames = "clsUnits"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If


            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Unit cannot be blank. Unit is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objUnit.Update()
            Else
                blnFlag = objUnit.Insert()
            End If

            If blnFlag = False And objUnit._Message <> "" Then
                eZeeMsgBox.Show(objUnit._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objUnit = Nothing
                    objUnit = New clsUnits
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintUnitUnkid = objUnit._Sectionunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call objFrm.displayDialog(txtName.Text, objUnit._Name1, objUnit._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSection.Click
        Try
            Dim objfrmSections_AddEdit As New frmSections_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objSection As New clsSections

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmSections_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmSections_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmSections_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            If objfrmSections_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objSection.getComboList("List", True)
                With cboSection
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboSection.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSection_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddUnitGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddUnitGroup.Click
        Try
            Dim frm As New frmUnitGroup_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objUgrp As New clsUnitGroup

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objUgrp.getComboList("List", True)
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboUnitGroup.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddUnitGroup_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Event's "

    Private Sub cboSection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
        Try
            If CInt(cboSection.SelectedValue) > 0 Then
                Dim dsList As New DataSet : Dim objUnitGrp As New clsUnitGroup
                dsList = objUnitGrp.getComboList("List", True, CInt(cboSection.SelectedValue))
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSection_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbUnitsInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUnitsInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbUnitsInfo.Text = Language._Object.getCaption(Me.gbUnitsInfo.Name, Me.gbUnitsInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Unit cannot be blank. Unit is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class