﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUnitsList

#Region " Private Varaibles "
    Private objUnit As clsUnits
    Private ReadOnly mstrModuleName As String = "frmUnitsList"
#End Region

#Region " Private Function "

    'Sandeep [ 29 Oct 2010 ] -- Start
    'Privilege Changes
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddUnit
            btnEdit.Enabled = User._Object.Privilege._EditUnit
            btnDelete.Enabled = User._Object.Privilege._DeleteUnit
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 29 Oct 2010 ] -- End

    Private Sub FillCombo()
        Try
            Dim dsSection As New DataSet
            Dim objSection As New clsSections

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objUnitGrp As New clsUnitGroup
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            dsSection = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsSection.Tables("Section")
            End With

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsSection = objUnitGrp.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsSection.Tables("List")
            End With
            cboUnitGroup.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsUnits As New DataSet
        Dim strSearching As String = ""
        Dim dtClassTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewUnitList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsUnits = objUnit.GetList("Section")

                If CInt(cboSections.SelectedValue) > 0 Then
                    strSearching = "AND sectionunkid =" & CInt(cboSections.SelectedValue) & " "
                End If

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboUnitGroup.SelectedValue) > 0 Then
                    strSearching = "AND unitgroupunkid =" & CInt(cboUnitGroup.SelectedValue) & " "
                End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtClassTable = New DataView(dsUnits.Tables("Section"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtClassTable = dsUnits.Tables("Section")
                End If


                Dim lvItem As ListViewItem

                lvUnitsList.Items.Clear()
                For Each drRow As DataRow In dtClassTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("unitunkid")
                    lvItem.SubItems.Add(drRow("Section").ToString)
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)

                    'S.SANDEEP [ 07 NOV 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow("UnitGrp").ToString)
                    'S.SANDEEP [ 07 NOV 2011 ] -- END


                    lvUnitsList.Items.Add(lvItem)
                Next

                If lvUnitsList.Items.Count > 16 Then
                    colhDescription.Width = 350 - 18
                Else
                    colhDescription.Width = 350
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsUnits.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmUnitsList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvUnitsList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSectionList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmUnitsList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSectionList_KeyPress", mstrModuleName)
        End Try


    End Sub

    Private Sub frmUnitsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUnit = New clsUnits
        Try

            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 29 Oct 2010 ] -- Start
            'Privilege Changes
            Call SetVisibility()
            'Sandeep [ 29 Oct 2010 ] -- End

            Call FillCombo()

            Call fillList()

            If lvUnitsList.Items.Count > 0 Then lvUnitsList.Items(0).Selected = True
            lvUnitsList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClassesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUnitsList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUnit = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUnits.SetMessages()
            objfrm._Other_ModuleNames = "clsUnits"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvUnitsList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Unit from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvUnitsList.Select()
            Exit Sub
        End If
        'If objUnit.isUsed(CInt(lvUnitsList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Unit. Reason: This Unit is in use."), enMsgBoxStyle.Information) '?2
        '    lvUnitsList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUnitsList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Unit?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objUnit.Delete(CInt(lvUnitsList.SelectedItems(0).Tag))

                If objUnit._Message <> "" Then
                    eZeeMsgBox.Show(objUnit._Message, enMsgBoxStyle.Information)
                Else
                    lvUnitsList.SelectedItems(0).Remove()
                End If

                If lvUnitsList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvUnitsList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvUnitsList.Items.Count - 1
                    lvUnitsList.Items(intSelectedIndex).Selected = True
                    lvUnitsList.EnsureVisible(intSelectedIndex)
                ElseIf lvUnitsList.Items.Count <> 0 Then
                    lvUnitsList.Items(intSelectedIndex).Selected = True
                    lvUnitsList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvUnitsList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvUnitsList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Unit from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvUnitsList.Select()
            Exit Sub
        End If
        Dim frm As New frmUnits_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUnitsList.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvUnitsList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvUnitsList.Items(intSelectedIndex).Selected = True
            lvUnitsList.EnsureVisible(intSelectedIndex)
            lvUnitsList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmUnits_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboSections.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboUnitGroup.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- END
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhSections.Text = Language._Object.getCaption(CStr(Me.colhSections.Tag), Me.colhSections.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.colhUnitGroup.Text = Language._Object.getCaption(CStr(Me.colhUnitGroup.Tag), Me.colhUnitGroup.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Unit from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Unit?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class