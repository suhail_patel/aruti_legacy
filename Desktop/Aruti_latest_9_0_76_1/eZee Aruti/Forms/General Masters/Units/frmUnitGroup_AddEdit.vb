﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUnitGroup_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmUnitGroup_AddEdit"
    Private mblnCancel As Boolean = True
    Private objUnitGroup As clsUnitGroup
    Private menAction As enAction = enAction.ADD_ONE
    Private mintUnitGroupUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintUnitGroupUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintUnitGroupUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboSection.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objUnitGroup._Code
        txtName.Text = objUnitGroup._Name
        txtDescription.Text = objUnitGroup._Description
        cboSection.SelectedValue = objUnitGroup._Sectionunkid
    End Sub

    Private Sub SetValue()
        objUnitGroup._Code = txtCode.Text
        objUnitGroup._Name = txtName.Text
        objUnitGroup._Description = txtDescription.Text
        objUnitGroup._Sectionunkid = CInt(cboSection.SelectedValue)
    End Sub

    Private Sub FillCombo()
        Try
            Dim objSection As New clsSections
            Dim dsList As New DataSet

            dsList = objSection.getComboList("Section", True)

            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Section")
            End With
            cboSection.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUnitGroup_AddEdit_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUnitGroup = Nothing
    End Sub

    Private Sub frmUnitGroup_AddEdit_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmUnitGroup_AddEdit_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmUnitGroup_AddEdit_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUnitGroup = New clsUnitGroup
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objUnitGroup._Unitgroupunkid = mintUnitGroupUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUnitGroup_AddEdit_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUnitGroup.SetMessages()
            objfrm._Other_ModuleNames = "clsUnitGroup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub

            End If
            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Name cannot be blank. Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objUnitGroup.Update()
            Else
                blnFlag = objUnitGroup.Insert()
            End If

            If blnFlag = False And objUnitGroup._Message <> "" Then
                eZeeMsgBox.Show(objUnitGroup._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objUnitGroup = Nothing
                    objUnitGroup = New clsUnitGroup
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintUnitGroupUnkid = objUnitGroup._Unitgroupunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objUnitGroup._Name1, objUnitGroup._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSection.Click
        Try
            Dim frm As New frmSections_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objSection As New clsSections

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objSection.getComboList("List", True)
                With cboSection
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboSection.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSection_Click", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbDeptGroup.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDeptGroup.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbDeptGroup.Text = Language._Object.getCaption(Me.gbDeptGroup.Name, Me.gbDeptGroup.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Name cannot be blank. Name is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class