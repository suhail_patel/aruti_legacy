﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAllocation_Mapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGrade_AddEdit"
    Private mblnCancel As Boolean = True
    Private dView_Allocation1 As DataView
    Private dView_Allocation2 As DataView
    Private objAllocMapping As clsAllocationMapping
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMappingUnkid As Integer = -1
    Private mdTable As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMappingUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsMasterData
        Try
            dsCombo = objCMaster.GetEAllocation_Notification("List", CStr(IIf(ConfigParameter._Object._IsAllocation_Hierarchy_Set = False, "", ConfigParameter._Object._Allocation_Hierarchy)))

            With cboAllocation2
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 1
            End With
            Dim sLen() As String = ConfigParameter._Object._Allocation_Hierarchy.Split(CChar("|"))
            Dim dTable = New DataView(dsCombo.Tables("List").Copy, "Id < '" & CStr(sLen.GetValue(sLen.Length - 1)) & "'", "Id", DataViewRowState.CurrentRows).ToTable

            With cboAllocation1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dTable
                .SelectedValue = 1
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Grid(ByVal intAllocation1Id As Integer, ByVal intAllocation2Id As Integer)
        Dim dsList As New DataSet
        Try
            Select Case intAllocation1Id
                Case enAllocation.BRANCH
                    Dim objStation As New clsStation
                    dsList = objStation.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "stationunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.DEPARTMENT_GROUP

                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "deptgroupunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "departmentunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.SECTION_GROUP

                    Dim objSectionGrp As New clsSectionGroup
                    dsList = objSectionGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "sectiongroupunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "sectionunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.UNIT_GROUP

                    Dim objUnitGrp As New clsUnitGroup
                    dsList = objUnitGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "unitgroupunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "unitunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation1.DataPropertyName = "IsCheck"
                    objdgcolhAllocation1Unkid.DataPropertyName = "teamunkid"
                    dgcolhAllocation1Name.DataPropertyName = "name"

            End Select
            dgcolhAllocation1Name.HeaderText = cboAllocation1.Text
            dgvAllocation1.AutoGenerateColumns = False
            dView_Allocation1 = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows)
            dgvAllocation1.DataSource = dView_Allocation1

            Select Case intAllocation2Id
                Case enAllocation.BRANCH
                    Dim objStation As New clsStation
                    dsList = objStation.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "stationunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "deptgroupunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"


                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "departmentunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.SECTION_GROUP

                    Dim objSectionGrp As New clsSectionGroup
                    dsList = objSectionGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "sectiongroupunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "sectionunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.UNIT_GROUP

                    Dim objUnitGrp As New clsUnitGroup
                    dsList = objUnitGrp.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "unitgroupunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "unitunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    dsList.Tables(0).Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                        dsList.Tables("List").Rows(i)("IsCheck") = False
                    Next
                    dsList.Tables("List").AcceptChanges()

                    objdgcolhChkAllocation2.DataPropertyName = "IsCheck"
                    objdgcolhAllocation2Unkid.DataPropertyName = "teamunkid"
                    dgcolhAllocation2Name.DataPropertyName = "name"

            End Select
            dgcolhAllocation2Name.HeaderText = cboAllocation2.Text
            dgvAllocation2.AutoGenerateColumns = False
            dView_Allocation2 = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows)
            dgvAllocation2.DataSource = dView_Allocation2

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Mapped_Allocation()
        Dim lvItem As ListViewItem
        Try
            lvData.Items.Clear()
            'S.SANDEEP [ 05 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dView As DataView = mdTable.DefaultView
            dView.Sort = "p_allocationid"
            mdTable = dView.ToTable
            'S.SANDEEP [ 05 MAR 2013 ] -- END
            For Each dtRow As DataRow In mdTable.Rows
                If CInt(dtRow("c_referenceunkid")) > 0 Then
                    If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                        lvItem = New ListViewItem
                        lvItem.Text = dtRow("Allocation").ToString
                        lvItem.SubItems.Add(dtRow("DisplayAllocation").ToString)
                        lvItem.SubItems.Add(dtRow("p_allocationid").ToString)
                        lvItem.SubItems.Add(dtRow("c_allocationid").ToString)
                        lvItem.SubItems.Add(dtRow("p_referenceunkid").ToString)
                        lvItem.SubItems.Add(dtRow("c_referenceunkid").ToString)
                        lvItem.SubItems.Add(dtRow("GUID").ToString)
                        lvItem.Tag = dtRow.Item("mappingtranunkid")
                        lvData.Items.Add(lvItem)
                    End If
                End If
            Next
            lvData.GroupingColumn = objcolhPAllocation
            lvData.DisplayGroups(True)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Mapped_Allocation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAllocation_Mapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAllocMapping = New clsAllocationMapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objAllocMapping._MappingUnkid = mintMappingUnkid
            End If
            mdTable = objAllocMapping._MappedData

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAllocation_Mapping_Load", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 02 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAllocationMapping.SetMessages()
            objfrm._Other_ModuleNames = "clsAllocationMapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 02 MAR 2013 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim dAlloc1Row() As DataRow = dView_Allocation1.Table.Select("IsCheck = True")
            Dim dAlloc2Row() As DataRow = dView_Allocation2.Table.Select("IsCheck = True")

            If dAlloc1Row.Length <= 0 Or dAlloc2Row.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one item from both of the allocation in order to map."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow = Nothing
            Dim dTemp() As DataRow = Nothing
            For iCntAlloc1 As Integer = 0 To dAlloc1Row.Length - 1
                dTemp = mdTable.Select("p_referenceunkid = '" & CInt(dAlloc1Row(iCntAlloc1).Item(objdgcolhAllocation1Unkid.DataPropertyName)) & _
                                       "' AND c_referenceunkid = -1 AND p_allocationid ='" & CInt(cboAllocation1.SelectedValue) & "' AND AUD <> 'D'")
                If dTemp.Length <= 0 Then
                    dtRow = mdTable.NewRow
                    dtRow.Item("mappingunkid") = mintMappingUnkid
                    dtRow.Item("p_allocationid") = CInt(cboAllocation1.SelectedValue)
                    dtRow.Item("c_allocationid") = CInt(cboAllocation2.SelectedValue)
                    dtRow.Item("p_referenceunkid") = CInt(dAlloc1Row(iCntAlloc1).Item(objdgcolhAllocation1Unkid.DataPropertyName))
                    dtRow.Item("userunkid") = User._Object._Userunkid
                    dtRow.Item("isvoid") = False
                    dtRow.Item("voiduserunkid") = -1
                    dtRow.Item("voiddatetime") = DBNull.Value
                    dtRow.Item("voidreason") = ""
                    dtRow.Item("c_referenceunkid") = -1
                    dtRow.Item("mappingtranunkid") = -1
                    dtRow.Item("AUD") = "A"
                    dtRow.Item("GUID") = Guid.NewGuid.ToString
                    dtRow.Item("Allocation") = dAlloc1Row(iCntAlloc1).Item(dgcolhAllocation1Name.DataPropertyName).ToString & Space(10) & "[ " & cboAllocation1.Text & " -> " & cboAllocation2.Text & " ]"
                    dtRow.Item("DisplayAllocation") = ""
                    mdTable.Rows.Add(dtRow)
                End If

                For iCntAlloc2 As Integer = 0 To dAlloc2Row.Length - 1
                    dTemp = mdTable.Select("c_referenceunkid = '" & CInt(dAlloc2Row(iCntAlloc2).Item(objdgcolhAllocation2Unkid.DataPropertyName)) & "' AND p_referenceunkid = '" & _
                                           CInt(dAlloc1Row(iCntAlloc1).Item(objdgcolhAllocation1Unkid.DataPropertyName)) & "' AND c_allocationid ='" & CInt(cboAllocation2.SelectedValue) & "'  AND AUD <> 'D'")
                    If dTemp.Length <= 0 Then
                        dtRow = mdTable.NewRow
                        dtRow.Item("mappingunkid") = mintMappingUnkid
                        dtRow.Item("p_allocationid") = CInt(cboAllocation1.SelectedValue)
                        dtRow.Item("c_allocationid") = CInt(cboAllocation2.SelectedValue)
                        dtRow.Item("p_referenceunkid") = CInt(dAlloc1Row(iCntAlloc1).Item(objdgcolhAllocation1Unkid.DataPropertyName))
                        dtRow.Item("userunkid") = User._Object._Userunkid
                        dtRow.Item("isvoid") = False
                        dtRow.Item("voiduserunkid") = -1
                        dtRow.Item("voiddatetime") = DBNull.Value
                        dtRow.Item("voidreason") = ""
                        dtRow.Item("c_referenceunkid") = CInt(dAlloc2Row(iCntAlloc2).Item(objdgcolhAllocation2Unkid.DataPropertyName))
                        dtRow.Item("mappingtranunkid") = -1
                        dtRow.Item("AUD") = "A"
                        dtRow.Item("GUID") = Guid.NewGuid.ToString
                        dtRow.Item("Allocation") = dAlloc1Row(iCntAlloc1).Item(dgcolhAllocation1Name.DataPropertyName).ToString & Space(10) & "[ " & cboAllocation1.Text & " -> " & cboAllocation2.Text & " ]"
                        dtRow.Item("DisplayAllocation") = dAlloc2Row(iCntAlloc2).Item(dgcolhAllocation2Name.DataPropertyName)
                        mdTable.Rows.Add(dtRow)
                    End If
                Next
            Next

            Call Fill_Mapped_Allocation()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvData.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                drTemp = mdTable.Select("GUID = '" & lvData.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Dim iPRefId As Integer = CInt(drTemp(0)("p_referenceunkid"))
                    Dim dTemp() As DataRow
                    dTemp = mdTable.Select("p_referenceunkid = '" & iPRefId & "' AND p_allocationid = '" & CInt(cboAllocation1.SelectedValue) & "' AND AUD <> 'D' AND c_referenceunkid > 0")
                    If dTemp.Length <= 0 Then
                        dTemp = mdTable.Select("p_referenceunkid = '" & iPRefId & "' AND p_allocationid = '" & CInt(cboAllocation1.SelectedValue) & "' AND AUD <> 'D' AND c_referenceunkid = -1")
                        If dTemp.Length > 0 Then
                            dTemp(0).Item("AUD") = "D"
                        End If
                    End If
                End If

                Call Fill_Mapped_Allocation()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            objAllocMapping._MappedData = mdTable
            If objAllocMapping.InsertUpdateDelete_MappedAllocation = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Mapping successfully saved."), enMsgBoxStyle.Information)
                Me.Close()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Problem in saving Mapping."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboAllocation1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation1.SelectedIndexChanged
        Try
            If ConfigParameter._Object._Allocation_Hierarchy.Trim.Length > 0 Then
                For Each Id As String In ConfigParameter._Object._Allocation_Hierarchy.Split(CChar("|"))
                    If CInt(Id) > CInt(cboAllocation1.SelectedValue) Then
                        cboAllocation2.SelectedValue = CInt(Id)
                        cboAllocation2.Enabled = False
                        Exit For
                    End If
                Next
                objchkAllocation1.Checked = False : objchkAllocation2.Checked = False
                txtSAllocation2.Text = "" : txtSAllocation1.Text = ""
                Call Fill_Grid(CInt(cboAllocation1.SelectedValue), CInt(cboAllocation2.SelectedValue))
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocation1_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSAllocation1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSAllocation1.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAllocation1.Rows.Count > 0 Then
                        If dgvAllocation1.SelectedRows(0).Index = dgvAllocation1.Rows(dgvAllocation1.RowCount - 1).Index Then Exit Sub
                        dgvAllocation1.Rows(dgvAllocation1.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAllocation1.Rows.Count > 0 Then
                        If dgvAllocation1.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAllocation1.Rows(dgvAllocation1.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSAllocation1_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSAllocation1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSAllocation1.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSAllocation1.Text.Trim.Length > 0 Then
                strSearch = "name LIKE '%" & txtSAllocation1.Text & "%' "
            End If
            dView_Allocation1.RowFilter = strSearch
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtSAllocation1_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSAllocation2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSAllocation2.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAllocation2.Rows.Count > 0 Then
                        If dgvAllocation2.SelectedRows(0).Index = dgvAllocation2.Rows(dgvAllocation2.RowCount - 1).Index Then Exit Sub
                        dgvAllocation2.Rows(dgvAllocation2.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAllocation2.Rows.Count > 0 Then
                        If dgvAllocation2.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAllocation2.Rows(dgvAllocation2.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSAllocation2_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSAllocation2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSAllocation2.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSAllocation2.Text.Trim.Length > 0 Then
                strSearch = "name LIKE '%" & txtSAllocation2.Text & "%' "
            End If
            dView_Allocation2.RowFilter = strSearch
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtSAllocation2_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAllocation1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocation1.CheckedChanged
        Try
            RemoveHandler dgvAllocation1.CellContentClick, AddressOf dgvAllocation1_CellContentClick
            For Each dRow As DataRow In dView_Allocation1.Table.Rows
                dRow.Item("IsCheck") = CBool(objchkAllocation1.CheckState)
            Next
            AddHandler dgvAllocation1.CellContentClick, AddressOf dgvAllocation1_CellContentClick
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objchkAllocation1_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAllocation1_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAllocation1.CellContentClick, dgvAllocation1.CellContentDoubleClick
        Try
            RemoveHandler objchkAllocation1.CheckedChanged, AddressOf objchkAllocation1_CheckedChanged
            If e.ColumnIndex = objdgcolhChkAllocation1.Index Then
                If Me.dgvAllocation1.IsCurrentCellDirty Then
                    Me.dgvAllocation1.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dView_Allocation1.Table.AcceptChanges()

                Dim drRow As DataRow() = dView_Allocation1.Table.Select("IsCheck = True")
                If drRow.Length > 0 Then
                    If dView_Allocation1.Table.Rows.Count = drRow.Length Then
                        objchkAllocation1.CheckState = CheckState.Checked
                    Else
                        objchkAllocation1.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAllocation1.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAllocation1.CheckedChanged, AddressOf objchkAllocation1_CheckedChanged
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "dgvAllocation1_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAllocation2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllocation2.CheckedChanged
        Try
            RemoveHandler dgvAllocation2.CellContentClick, AddressOf dgvAllocation2_CellContentClick
            For Each dRow As DataRow In dView_Allocation2.Table.Rows
                dRow.Item("IsCheck") = CBool(objchkAllocation2.CheckState)
            Next
            AddHandler dgvAllocation2.CellContentClick, AddressOf dgvAllocation2_CellContentClick
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objchkAllocation2_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAllocation2_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAllocation2.CellContentClick, dgvAllocation2.CellContentDoubleClick
        Try
            RemoveHandler objchkAllocation2.CheckedChanged, AddressOf objchkAllocation2_CheckedChanged
            If e.ColumnIndex = objdgcolhChkAllocation1.Index Then
                If Me.dgvAllocation2.IsCurrentCellDirty Then
                    Me.dgvAllocation2.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dView_Allocation2.Table.AcceptChanges()

                Dim drRow As DataRow() = dView_Allocation2.Table.Select("IsCheck = True")
                If drRow.Length > 0 Then
                    If dView_Allocation2.Table.Rows.Count = drRow.Length Then
                        objchkAllocation2.CheckState = CheckState.Checked
                    Else
                        objchkAllocation2.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAllocation2.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkAllocation2.CheckedChanged, AddressOf objchkAllocation2_CheckedChanged
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "dgvAllocation2_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAllocationMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAllocationMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAdd.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbAllocationMapping.Text = Language._Object.getCaption(Me.gbAllocationMapping.Name, Me.gbAllocationMapping.Text)
			Me.lblAllocation2.Text = Language._Object.getCaption(Me.lblAllocation2.Name, Me.lblAllocation2.Text)
			Me.lblAllocation1.Text = Language._Object.getCaption(Me.lblAllocation1.Name, Me.lblAllocation1.Text)
			Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.dgcolhAllocation2Name.HeaderText = Language._Object.getCaption(Me.dgcolhAllocation2Name.Name, Me.dgcolhAllocation2Name.HeaderText)
			Me.dgcolhAllocation1Name.HeaderText = Language._Object.getCaption(Me.dgcolhAllocation1Name.Name, Me.dgcolhAllocation1Name.HeaderText)
			Me.colhAllocationDisplay.Text = Language._Object.getCaption(CStr(Me.colhAllocationDisplay.Tag), Me.colhAllocationDisplay.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one item from both of the allocation in order to map.")
			Language.setMessage(mstrModuleName, 2, "Mapping successfully saved.")
			Language.setMessage(mstrModuleName, 3, "Problem in saving Mapping.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class