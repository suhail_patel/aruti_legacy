﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSections_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSections_AddEdit"
    Private mblnCancel As Boolean = True
    Private objSection As clsSections
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSectionUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSectionUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintSectionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboDepartment.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboSectionGroup.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 07 NOV 2011 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objSection._Code
        txtName.Text = objSection._Name
        txtDescription.Text = objSection._Description
        cboDepartment.SelectedValue = objSection._Departmentunkid
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        cboSectionGroup.SelectedValue = objSection._Sectiongroupunkid
        'S.SANDEEP [ 07 NOV 2011 ] -- END
    End Sub

    Private Sub SetValue()
        objSection._Code = txtCode.Text
        objSection._Name = txtName.Text
        objSection._Description = txtDescription.Text
        objSection._Departmentunkid = CInt(cboDepartment.SelectedValue)
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objSection._Sectiongroupunkid = CInt(cboSectionGroup.SelectedValue)
        'S.SANDEEP [ 07 NOV 2011 ] -- END
    End Sub

    Private Sub FillCombo()
        Try
            Dim objDept As New clsDepartment
            Dim dsList As New DataSet

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objSecGroup As New clsSectionGroup
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            dsList = objDept.getComboList("DeptList", True)

            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("DeptList")
            End With
            cboDepartment.SelectedValue = 0

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objSecGroup.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            'S.SANDEEP [ 07 NOV 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddDepartment.Enabled = User._Object.Privilege._AddDepartment
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSections_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSection = Nothing
    End Sub

    Private Sub frmSections_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmSections_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmSections_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSection = New clsSections
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objSection._Sectionunkid = mintSectionUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSections_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSections.SetMessages()
            objfrm._Other_ModuleNames = "clsSections"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Section cannot be blank. Section Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objSection.Update()
            Else
                blnFlag = objSection.Insert()
            End If

            If blnFlag = False And objSection._Message <> "" Then
                eZeeMsgBox.Show(objSection._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSection = Nothing
                    objSection = New clsSections
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintSectionUnkid = objSection._Sectionunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objSection._Name1, objSection._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepartment.Click
        Try
            Dim objfrmDepartment_AddEdit As New frmDepartment_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objDepartment As New clsDepartment
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmDepartment_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmDepartment_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmDepartment_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objfrmDepartment_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objDepartment.getComboList("List", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboDepartment.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDepartment_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnAddSectionGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSectionGrp.Click
        Try
            Dim frm As New frmSectionGroup_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombo As New DataSet
            Dim objSGrp As New clsSectionGroup
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombo = objSGrp.getComboList("List", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("List")
                End With
                cboSectionGroup.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSectionGrp_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSectionsInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSectionsInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbSectionsInfo.Text = Language._Object.getCaption(Me.gbSectionsInfo.Name, Me.gbSectionsInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblDeptGroup.Text = Language._Object.getCaption(Me.lblDeptGroup.Name, Me.lblDeptGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Section cannot be blank. Section Name is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class