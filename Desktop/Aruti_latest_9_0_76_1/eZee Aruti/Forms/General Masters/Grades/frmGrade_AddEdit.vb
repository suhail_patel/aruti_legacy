﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGrade_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGrade_AddEdit"
    Private mblnCancel As Boolean = True
    Private objGrade As clsGrade
    Private menAction As enAction = enAction.ADD_ONE
    Private mintGradeUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintGradeUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintGradeUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboGradeGroup.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objGrade._Code
        txtName.Text = objGrade._Name
        txtDescription.Text = objGrade._Description
        cboGradeGroup.SelectedValue = objGrade._Gradegroupunkid
    End Sub

    Private Sub SetValue()
        objGrade._Code = txtCode.Text
        objGrade._Name = txtName.Text
        objGrade._Description = txtDescription.Text
        objGrade._Gradegroupunkid = CInt(cboGradeGroup.SelectedValue)
    End Sub

    Private Sub FillCombo()
        Try
            Dim objGradeGroup As New clsGradeGroup
            Dim dsList As New DataSet

            dsList = objGradeGroup.getComboList("GrpList", True)

            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("GrpList")
            End With
            cboGradeGroup.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddGradeGroup.Enabled = User._Object.Privilege._AddGradeGroup

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmGrade_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objGrade = Nothing
    End Sub

    Private Sub frmGrade_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGrade_AddEdit_KeyDown", mstrModuleName)
        End Try

    End Sub

    Private Sub frmGrade_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGrade_AddEdit_KeyPress", mstrModuleName)
        End Try


    End Sub

    Private Sub frmGrade_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objGrade = New clsGrade
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objGrade._Gradeunkid = mintGradeUnkid
                'Nilay (02-Jul-2016) -- Start
                Dim blnFlag As Boolean = False
                blnFlag = objGrade.isUsed(mintGradeUnkid)
                cboGradeGroup.Enabled = Not blnFlag
                objbtnAddGradeGroup.Enabled = Not blnFlag
                'Nilay (02-Jul-2016) -- End
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAirline_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsGrade.SetMessages()
            objfrm._Other_ModuleNames = "clsGrade"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
           
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Grade Code cannot be blank. Grade Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Grade cannot be blank. Grade is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Grade Group is compulsory information. Grade Group is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objGrade.Update()
            Else
                blnFlag = objGrade.Insert()
            End If

            If blnFlag = False And objGrade._Message <> "" Then
                eZeeMsgBox.Show(objGrade._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objGrade = Nothing
                    objGrade = New clsGrade
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintGradeUnkid = objGrade._Gradeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objGrade._Name1, objGrade._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGradeGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGradeGroup.Click
        Try
            Dim objfrmGradeGroup_AddEdit As New frmGradeGroup_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objGradeGroup As New clsGradeGroup

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmGradeGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGradeGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGradeGroup_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objfrmGradeGroup_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objGradeGroup.getComboList("List", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboGradeGroup.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGradeGroup_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbGradesInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGradesInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbGradesInfo.Text = Language._Object.getCaption(Me.gbGradesInfo.Name, Me.gbGradesInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Grade cannot be blank. Grade is required information.")
			Language.setMessage(mstrModuleName, 2, "Grade Code cannot be blank. Grade Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Grade Group is compulsory information. Grade Group is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class