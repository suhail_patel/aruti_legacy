﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGradelevelList

#Region " Private Varaibles "
    Private objGradeLevel As clsGradeLevel
    Private ReadOnly mstrModuleName As String = "frmGradelevelList"
#End Region

#Region " Private Function "

    'Sandeep [ 29 Oct 2010 ] -- Start
    'Privilege Changes
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddGradeLevel
            btnEdit.Enabled = User._Object.Privilege._EditGradeLevel
            btnDelete.Enabled = User._Object.Privilege._DeleteGradeLevel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 29 Oct 2010 ] -- End

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objGradeGroup As New clsGradeGroup
        Dim objGrade As New clsGrade
        Try
            dsCombos = objGradeGroup.getComboList("GradeGroup", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeGroup")
            End With
            cboGradeGroup.SelectedValue = 0

            dsCombos = objGrade.getComboList("GradeList", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeList")
            End With
            cboGrade.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtDeptTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewGradeLevelList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsList = objGradeLevel.GetList("GradeLevel")

                If CInt(cboGradeGroup.SelectedValue) > 0 Then
                    strSearching &= "AND gradegroupunkid =" & CInt(cboGradeGroup.SelectedValue) & " "
                End If

                If CInt(cboGrade.SelectedValue) > 0 Then
                    strSearching &= "AND gradeunkid =" & CInt(cboGrade.SelectedValue) & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'dtDeptTable = New DataView(dsList.Tables("GradeLevel"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                    dtDeptTable = New DataView(dsList.Tables("GradeLevel"), strSearching, "GradeName", DataViewRowState.CurrentRows).ToTable
                    'Sohail (27 Apr 2016) -- End

                Else
                    'Sohail (27 Apr 2016) -- Start
                    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                    'dtDeptTable = dsList.Tables("GradeLevel")
                    dtDeptTable = New DataView(dsList.Tables("GradeLevel"), "", "GradeName", DataViewRowState.CurrentRows).ToTable
                    'Sohail (27 Apr 2016) -- End
                End If


                Dim lvItem As ListViewItem

                lvGradeLevel.Items.Clear()
                For Each drRow As DataRow In dtDeptTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("GradeGrpName").ToString
                    lvItem.SubItems.Add(drRow("GradeName").ToString)
                    lvItem.SubItems.Add(drRow("code").ToString)
                    lvItem.Tag = drRow("gradelevelunkid")
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("priority").ToString) 'Sohail (27 Apr 2016)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvGradeLevel.Items.Add(lvItem)
                Next

                'Sohail (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                lvGradeLevel.GroupingColumn = colhGrade
                lvGradeLevel.DisplayGroups(True)
                'Sohail (27 Apr 2016) -- End

                If lvGradeLevel.Items.Count > 5 Then
                    colhDescription.Width = 150 - 18
                Else
                    colhDescription.Width = 150
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmGradelevelList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvGradeLevel.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradelevelList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGradelevelList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradelevelList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGradelevelList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objGradeLevel = New clsGradeLevel
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 29 Oct 2010 ] -- Start
            'Privilege Changes
            Call SetVisibility()
            'Sandeep [ 29 Oct 2010 ] -- End

            'Nilay (27 Apr 2016) -- Start
            'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed
            If User._Object.Privilege._AllowToAssignGradePriority = False Then
                mnuAssignPriority.Enabled = False
            End If
            'Nilay (27 Apr 2016) -- End

            Call FillCombo()

            ' Call fillList()

            If lvGradeLevel.Items.Count > 0 Then lvGradeLevel.Items(0).Selected = True
            lvGradeLevel.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradelevelList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGradelevelList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objGradeLevel = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsGradeLevel.SetMessages()
            objfrm._Other_ModuleNames = "clsGradeLevel"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvGradeLevel.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Grade Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvGradeLevel.Select()
            Exit Sub
        End If
        'If objGradeLevel.isUsed(CInt(lvGradeLevel.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Grade Level. Reason: This Grade Level is in use."), enMsgBoxStyle.Information) '?2
        '    lvGradeLevel.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvGradeLevel.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Grade Level?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objGradeLevel.Delete(CInt(lvGradeLevel.SelectedItems(0).Tag))
                If objGradeLevel._Message <> "" Then
                    eZeeMsgBox.Show(objGradeLevel._Message, enMsgBoxStyle.Information)
                Else
                    lvGradeLevel.SelectedItems(0).Remove()
                End If


                If lvGradeLevel.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvGradeLevel.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvGradeLevel.Items.Count - 1
                    lvGradeLevel.Items(intSelectedIndex).Selected = True
                    lvGradeLevel.EnsureVisible(intSelectedIndex)
                ElseIf lvGradeLevel.Items.Count <> 0 Then
                    lvGradeLevel.Items(intSelectedIndex).Selected = True
                    lvGradeLevel.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvGradeLevel.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvGradeLevel.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Grade Level from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvGradeLevel.Select()
            Exit Sub
        End If
        Dim frm As New frmGradeLevel_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvGradeLevel.SelectedItems(0).Index
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvGradeLevel.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvGradeLevel.Items(intSelectedIndex).Selected = True
            lvGradeLevel.EnsureVisible(intSelectedIndex)
            lvGradeLevel.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmGradeLevel_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGradeGroup.SelectedValue = 0
            cboGrade.SelectedValue = 0
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
#Region " Other Control's Events "

    Private Sub mnuAssignPriority_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAssignPriority.Click
        Dim frm As New frmAssignGradePriority
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.ADD_ONE) Then
                Call fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAssignPriority_Click", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (27 Apr 2016) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhGroup.Text = Language._Object.getCaption(CStr(Me.colhGroup.Tag), Me.colhGroup.Text)
            Me.colhGrade.Text = Language._Object.getCaption(CStr(Me.colhGrade.Tag), Me.colhGrade.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuAssignPriority.Text = Language._Object.getCaption(Me.mnuAssignPriority.Name, Me.mnuAssignPriority.Text)
            Me.colhPriority.Text = Language._Object.getCaption(CStr(Me.colhPriority.Tag), Me.colhPriority.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Grade Level from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Grade Level?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class