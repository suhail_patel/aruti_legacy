﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJobsList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboUnitGrp = New System.Windows.Forms.ComboBox
        Me.cboSectionGrp = New System.Windows.Forms.ComboBox
        Me.cboDeparmentGrp = New System.Windows.Forms.ComboBox
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkBranch = New System.Windows.Forms.CheckBox
        Me.chkUnitGroup = New System.Windows.Forms.CheckBox
        Me.chkSectionGroup = New System.Windows.Forms.CheckBox
        Me.chkClass = New System.Windows.Forms.CheckBox
        Me.objbtnSearchInDirectReportTo = New eZee.Common.eZeeGradientButton
        Me.chkDepartmentGroup = New System.Windows.Forms.CheckBox
        Me.objbtnSearchDirectReportTo = New eZee.Common.eZeeGradientButton
        Me.cboInDirectReportTo = New System.Windows.Forms.ComboBox
        Me.LblInDirectReportTo = New System.Windows.Forms.Label
        Me.chkDepartment = New System.Windows.Forms.CheckBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.chkClassGroup = New System.Windows.Forms.CheckBox
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblTerminateDate = New System.Windows.Forms.Label
        Me.lblCreateDate = New System.Windows.Forms.Label
        Me.dtpTerminateDate = New System.Windows.Forms.DateTimePicker
        Me.chkGrade = New System.Windows.Forms.CheckBox
        Me.dtpCreateDate = New System.Windows.Forms.DateTimePicker
        Me.chkSection = New System.Windows.Forms.CheckBox
        Me.chkTeam = New System.Windows.Forms.CheckBox
        Me.chkUnit = New System.Windows.Forms.CheckBox
        Me.cboReportTo = New System.Windows.Forms.ComboBox
        Me.lblReportTo = New System.Windows.Forms.Label
        Me.chkJobGroup = New System.Windows.Forms.CheckBox
        Me.cboTeam = New System.Windows.Forms.ComboBox
        Me.nudPosition = New System.Windows.Forms.NumericUpDown
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblPosition = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lvJobList = New eZee.Common.eZeeListView(Me.components)
        Me.colhJobCode = New System.Windows.Forms.ColumnHeader
        Me.colhJobName = New System.Windows.Forms.ColumnHeader
        Me.colhJobGruop = New System.Windows.Forms.ColumnHeader
        Me.colhBranch = New System.Windows.Forms.ColumnHeader
        Me.colhDepartmentGrp = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhsectionGrp = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhUnitGrp = New System.Windows.Forms.ColumnHeader
        Me.colhUnit = New System.Windows.Forms.ColumnHeader
        Me.colhTeam = New System.Windows.Forms.ColumnHeader
        Me.colhClassGroup = New System.Windows.Forms.ColumnHeader
        Me.colhClass = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhGradeLevel = New System.Windows.Forms.ColumnHeader
        Me.colhJobsBy = New System.Windows.Forms.ColumnHeader
        Me.colhJobLevel = New System.Windows.Forms.ColumnHeader
        Me.colhCreateDate = New System.Windows.Forms.ColumnHeader
        Me.colhTerminateDate = New System.Windows.Forms.ColumnHeader
        Me.colhPosition = New System.Windows.Forms.ColumnHeader
        Me.colhReportTo = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ChkGradeLevel = New System.Windows.Forms.CheckBox
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.lvJobList)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(1018, 572)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnitGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboSectionGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboDeparmentGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.chkSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.chkClass)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchInDirectReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.chkDepartmentGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDirectReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboInDirectReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.LblInDirectReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.chkDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.chkClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblTerminateDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblCreateDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTerminateDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkGrade)
        Me.gbFilterCriteria.Controls.Add(Me.dtpCreateDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkSection)
        Me.gbFilterCriteria.Controls.Add(Me.chkTeam)
        Me.gbFilterCriteria.Controls.Add(Me.chkUnit)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportTo)
        Me.gbFilterCriteria.Controls.Add(Me.chkJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboTeam)
        Me.gbFilterCriteria.Controls.Add(Me.nudPosition)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.lblPosition)
        Me.gbFilterCriteria.Controls.Add(Me.lblName)
        Me.gbFilterCriteria.Controls.Add(Me.cboSection)
        Me.gbFilterCriteria.Controls.Add(Me.txtName)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnit)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(994, 171)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(870, 114)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(113, 21)
        Me.cboClass.TabIndex = 228
        '
        'cboUnitGrp
        '
        Me.cboUnitGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGrp.FormattingEnabled = True
        Me.cboUnitGrp.Location = New System.Drawing.Point(870, 87)
        Me.cboUnitGrp.Name = "cboUnitGrp"
        Me.cboUnitGrp.Size = New System.Drawing.Size(113, 21)
        Me.cboUnitGrp.TabIndex = 227
        '
        'cboSectionGrp
        '
        Me.cboSectionGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGrp.FormattingEnabled = True
        Me.cboSectionGrp.Location = New System.Drawing.Point(870, 33)
        Me.cboSectionGrp.Name = "cboSectionGrp"
        Me.cboSectionGrp.Size = New System.Drawing.Size(113, 21)
        Me.cboSectionGrp.TabIndex = 226
        '
        'cboDeparmentGrp
        '
        Me.cboDeparmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeparmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeparmentGrp.FormattingEnabled = True
        Me.cboDeparmentGrp.Location = New System.Drawing.Point(648, 60)
        Me.cboDeparmentGrp.Name = "cboDeparmentGrp"
        Me.cboDeparmentGrp.Size = New System.Drawing.Size(113, 21)
        Me.cboDeparmentGrp.TabIndex = 224
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(648, 33)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(113, 21)
        Me.cboBranch.TabIndex = 223
        '
        'chkBranch
        '
        Me.chkBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBranch.Location = New System.Drawing.Point(519, 35)
        Me.chkBranch.Name = "chkBranch"
        Me.chkBranch.Size = New System.Drawing.Size(123, 17)
        Me.chkBranch.TabIndex = 217
        Me.chkBranch.Text = "Branch"
        Me.chkBranch.UseVisualStyleBackColor = True
        '
        'chkUnitGroup
        '
        Me.chkUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnitGroup.Location = New System.Drawing.Point(767, 89)
        Me.chkUnitGroup.Name = "chkUnitGroup"
        Me.chkUnitGroup.Size = New System.Drawing.Size(97, 17)
        Me.chkUnitGroup.TabIndex = 220
        Me.chkUnitGroup.Text = "Unit Group"
        Me.chkUnitGroup.UseVisualStyleBackColor = True
        '
        'chkSectionGroup
        '
        Me.chkSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSectionGroup.Location = New System.Drawing.Point(767, 35)
        Me.chkSectionGroup.Name = "chkSectionGroup"
        Me.chkSectionGroup.Size = New System.Drawing.Size(97, 17)
        Me.chkSectionGroup.TabIndex = 219
        Me.chkSectionGroup.Text = "Section Group"
        Me.chkSectionGroup.UseVisualStyleBackColor = True
        '
        'chkClass
        '
        Me.chkClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClass.Location = New System.Drawing.Point(767, 116)
        Me.chkClass.Name = "chkClass"
        Me.chkClass.Size = New System.Drawing.Size(97, 17)
        Me.chkClass.TabIndex = 221
        Me.chkClass.Text = "Class"
        Me.chkClass.UseVisualStyleBackColor = True
        '
        'objbtnSearchInDirectReportTo
        '
        Me.objbtnSearchInDirectReportTo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInDirectReportTo.BorderSelected = False
        Me.objbtnSearchInDirectReportTo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInDirectReportTo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInDirectReportTo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInDirectReportTo.Location = New System.Drawing.Point(296, 87)
        Me.objbtnSearchInDirectReportTo.Name = "objbtnSearchInDirectReportTo"
        Me.objbtnSearchInDirectReportTo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInDirectReportTo.TabIndex = 203
        '
        'chkDepartmentGroup
        '
        Me.chkDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepartmentGroup.Location = New System.Drawing.Point(519, 62)
        Me.chkDepartmentGroup.Name = "chkDepartmentGroup"
        Me.chkDepartmentGroup.Size = New System.Drawing.Size(123, 17)
        Me.chkDepartmentGroup.TabIndex = 218
        Me.chkDepartmentGroup.Text = "Department Group Grp"
        Me.chkDepartmentGroup.UseVisualStyleBackColor = True
        '
        'objbtnSearchDirectReportTo
        '
        Me.objbtnSearchDirectReportTo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDirectReportTo.BorderSelected = False
        Me.objbtnSearchDirectReportTo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDirectReportTo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDirectReportTo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDirectReportTo.Location = New System.Drawing.Point(296, 60)
        Me.objbtnSearchDirectReportTo.Name = "objbtnSearchDirectReportTo"
        Me.objbtnSearchDirectReportTo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDirectReportTo.TabIndex = 202
        '
        'cboInDirectReportTo
        '
        Me.cboInDirectReportTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInDirectReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInDirectReportTo.FormattingEnabled = True
        Me.cboInDirectReportTo.Location = New System.Drawing.Point(119, 87)
        Me.cboInDirectReportTo.Name = "cboInDirectReportTo"
        Me.cboInDirectReportTo.Size = New System.Drawing.Size(171, 21)
        Me.cboInDirectReportTo.TabIndex = 21
        '
        'LblInDirectReportTo
        '
        Me.LblInDirectReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblInDirectReportTo.Location = New System.Drawing.Point(8, 90)
        Me.LblInDirectReportTo.Name = "LblInDirectReportTo"
        Me.LblInDirectReportTo.Size = New System.Drawing.Size(105, 15)
        Me.LblInDirectReportTo.TabIndex = 20
        Me.LblInDirectReportTo.Text = "In-Direct Report To"
        Me.LblInDirectReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDepartment
        '
        Me.chkDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepartment.Location = New System.Drawing.Point(519, 89)
        Me.chkDepartment.Name = "chkDepartment"
        Me.chkDepartment.Size = New System.Drawing.Size(123, 17)
        Me.chkDepartment.TabIndex = 10
        Me.chkDepartment.Text = "Department"
        Me.chkDepartment.UseVisualStyleBackColor = True
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 200
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(648, 87)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(113, 21)
        Me.cboDepartment.TabIndex = 11
        '
        'chkClassGroup
        '
        Me.chkClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClassGroup.Location = New System.Drawing.Point(519, 116)
        Me.chkClassGroup.Name = "chkClassGroup"
        Me.chkClassGroup.Size = New System.Drawing.Size(123, 17)
        Me.chkClassGroup.TabIndex = 12
        Me.chkClassGroup.Text = "Class Group"
        Me.chkClassGroup.UseVisualStyleBackColor = True
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 200
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(648, 114)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(113, 21)
        Me.cboClassGroup.TabIndex = 13
        '
        'lblTerminateDate
        '
        Me.lblTerminateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerminateDate.Location = New System.Drawing.Point(323, 63)
        Me.lblTerminateDate.Name = "lblTerminateDate"
        Me.lblTerminateDate.Size = New System.Drawing.Size(84, 15)
        Me.lblTerminateDate.TabIndex = 22
        Me.lblTerminateDate.Text = "Terminate Date"
        Me.lblTerminateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCreateDate
        '
        Me.lblCreateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreateDate.Location = New System.Drawing.Point(323, 36)
        Me.lblCreateDate.Name = "lblCreateDate"
        Me.lblCreateDate.Size = New System.Drawing.Size(84, 15)
        Me.lblCreateDate.TabIndex = 14
        Me.lblCreateDate.Text = "Create Date"
        Me.lblCreateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpTerminateDate
        '
        Me.dtpTerminateDate.Checked = False
        Me.dtpTerminateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTerminateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTerminateDate.Location = New System.Drawing.Point(413, 60)
        Me.dtpTerminateDate.Name = "dtpTerminateDate"
        Me.dtpTerminateDate.ShowCheckBox = True
        Me.dtpTerminateDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpTerminateDate.TabIndex = 23
        '
        'chkGrade
        '
        Me.chkGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGrade.Location = New System.Drawing.Point(323, 144)
        Me.chkGrade.Name = "chkGrade"
        Me.chkGrade.Size = New System.Drawing.Size(84, 15)
        Me.chkGrade.TabIndex = 8
        Me.chkGrade.Text = "Grade"
        Me.chkGrade.UseVisualStyleBackColor = True
        '
        'dtpCreateDate
        '
        Me.dtpCreateDate.Checked = False
        Me.dtpCreateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCreateDate.Location = New System.Drawing.Point(413, 33)
        Me.dtpCreateDate.Name = "dtpCreateDate"
        Me.dtpCreateDate.ShowCheckBox = True
        Me.dtpCreateDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpCreateDate.TabIndex = 15
        '
        'chkSection
        '
        Me.chkSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSection.Location = New System.Drawing.Point(767, 62)
        Me.chkSection.Name = "chkSection"
        Me.chkSection.Size = New System.Drawing.Size(97, 17)
        Me.chkSection.TabIndex = 6
        Me.chkSection.Text = "Section"
        Me.chkSection.UseVisualStyleBackColor = True
        '
        'chkTeam
        '
        Me.chkTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTeam.Location = New System.Drawing.Point(323, 117)
        Me.chkTeam.Name = "chkTeam"
        Me.chkTeam.Size = New System.Drawing.Size(84, 15)
        Me.chkTeam.TabIndex = 4
        Me.chkTeam.Text = "Team"
        Me.chkTeam.UseVisualStyleBackColor = True
        '
        'chkUnit
        '
        Me.chkUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnit.Location = New System.Drawing.Point(8, 114)
        Me.chkUnit.Name = "chkUnit"
        Me.chkUnit.Size = New System.Drawing.Size(105, 15)
        Me.chkUnit.TabIndex = 2
        Me.chkUnit.Text = "Unit"
        Me.chkUnit.UseVisualStyleBackColor = True
        '
        'cboReportTo
        '
        Me.cboReportTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportTo.FormattingEnabled = True
        Me.cboReportTo.Location = New System.Drawing.Point(119, 60)
        Me.cboReportTo.Name = "cboReportTo"
        Me.cboReportTo.Size = New System.Drawing.Size(171, 21)
        Me.cboReportTo.TabIndex = 17
        '
        'lblReportTo
        '
        Me.lblReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportTo.Location = New System.Drawing.Point(8, 63)
        Me.lblReportTo.Name = "lblReportTo"
        Me.lblReportTo.Size = New System.Drawing.Size(105, 15)
        Me.lblReportTo.TabIndex = 16
        Me.lblReportTo.Text = "Report To"
        Me.lblReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkJobGroup
        '
        Me.chkJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkJobGroup.Location = New System.Drawing.Point(8, 144)
        Me.chkJobGroup.Name = "chkJobGroup"
        Me.chkJobGroup.Size = New System.Drawing.Size(105, 15)
        Me.chkJobGroup.TabIndex = 0
        Me.chkJobGroup.Text = "Job Group"
        Me.chkJobGroup.UseVisualStyleBackColor = True
        '
        'cboTeam
        '
        Me.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeam.DropDownWidth = 200
        Me.cboTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeam.FormattingEnabled = True
        Me.cboTeam.Location = New System.Drawing.Point(413, 114)
        Me.cboTeam.Name = "cboTeam"
        Me.cboTeam.Size = New System.Drawing.Size(100, 21)
        Me.cboTeam.TabIndex = 5
        '
        'nudPosition
        '
        Me.nudPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPosition.Location = New System.Drawing.Point(413, 87)
        Me.nudPosition.Name = "nudPosition"
        Me.nudPosition.Size = New System.Drawing.Size(100, 21)
        Me.nudPosition.TabIndex = 25
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.DropDownWidth = 200
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(413, 141)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(100, 21)
        Me.cboGrade.TabIndex = 9
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(323, 90)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(84, 15)
        Me.lblPosition.TabIndex = 24
        Me.lblPosition.Text = "Vacancies"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 36)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(105, 15)
        Me.lblName.TabIndex = 18
        Me.lblName.Text = "Job"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.DropDownWidth = 200
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(870, 60)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(113, 21)
        Me.cboSection.TabIndex = 7
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(119, 33)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(198, 21)
        Me.txtName.TabIndex = 19
        '
        'cboUnit
        '
        Me.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnit.DropDownWidth = 200
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(119, 114)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(198, 21)
        Me.cboUnit.TabIndex = 3
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 200
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(119, 141)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(198, 21)
        Me.cboJobGroup.TabIndex = 1
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(967, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 139
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(943, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 138
        Me.objbtnSearch.TabStop = False
        '
        'lvJobList
        '
        Me.lvJobList.BackColorOnChecked = True
        Me.lvJobList.ColumnHeaders = Nothing
        Me.lvJobList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhJobCode, Me.colhJobName, Me.colhJobGruop, Me.colhBranch, Me.colhDepartmentGrp, Me.colhDepartment, Me.colhsectionGrp, Me.colhSection, Me.colhUnitGrp, Me.colhUnit, Me.colhTeam, Me.colhClassGroup, Me.colhClass, Me.colhGrade, Me.colhGradeLevel, Me.colhJobsBy, Me.colhJobLevel, Me.colhCreateDate, Me.colhTerminateDate, Me.colhPosition, Me.colhReportTo})
        Me.lvJobList.CompulsoryColumns = ""
        Me.lvJobList.FullRowSelect = True
        Me.lvJobList.GridLines = True
        Me.lvJobList.GroupingColumn = Nothing
        Me.lvJobList.HideSelection = False
        Me.lvJobList.Location = New System.Drawing.Point(12, 241)
        Me.lvJobList.MinColumnWidth = 50
        Me.lvJobList.MultiSelect = False
        Me.lvJobList.Name = "lvJobList"
        Me.lvJobList.OptionalColumns = ""
        Me.lvJobList.ShowMoreItem = False
        Me.lvJobList.ShowSaveItem = False
        Me.lvJobList.ShowSelectAll = True
        Me.lvJobList.ShowSizeAllColumnsToFit = True
        Me.lvJobList.Size = New System.Drawing.Size(994, 270)
        Me.lvJobList.Sortable = True
        Me.lvJobList.TabIndex = 2
        Me.lvJobList.UseCompatibleStateImageBehavior = False
        Me.lvJobList.View = System.Windows.Forms.View.Details
        '
        'colhJobCode
        '
        Me.colhJobCode.Tag = "colhJobCode"
        Me.colhJobCode.Text = "Code"
        Me.colhJobCode.Width = 85
        '
        'colhJobName
        '
        Me.colhJobName.Tag = "colhJobName"
        Me.colhJobName.Text = "Job"
        Me.colhJobName.Width = 250
        '
        'colhJobGruop
        '
        Me.colhJobGruop.Tag = "colhJobGruop"
        Me.colhJobGruop.Text = "Job Group"
        Me.colhJobGruop.Width = 100
        '
        'colhBranch
        '
        Me.colhBranch.Text = "Branch"
        Me.colhBranch.Width = 100
        '
        'colhDepartmentGrp
        '
        Me.colhDepartmentGrp.Text = "Department Grp"
        Me.colhDepartmentGrp.Width = 100
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 100
        '
        'colhsectionGrp
        '
        Me.colhsectionGrp.Text = "Section Grp"
        Me.colhsectionGrp.Width = 100
        '
        'colhSection
        '
        Me.colhSection.Tag = "colhSection"
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 100
        '
        'colhUnitGrp
        '
        Me.colhUnitGrp.Text = "Unit Grp"
        Me.colhUnitGrp.Width = 100
        '
        'colhUnit
        '
        Me.colhUnit.Tag = "colhUnit"
        Me.colhUnit.Text = "Unit"
        Me.colhUnit.Width = 100
        '
        'colhTeam
        '
        Me.colhTeam.Tag = "colhTeam"
        Me.colhTeam.Text = "Team"
        Me.colhTeam.Width = 100
        '
        'colhClassGroup
        '
        Me.colhClassGroup.Tag = "colhClassGroup"
        Me.colhClassGroup.Text = "Class Group"
        Me.colhClassGroup.Width = 100
        '
        'colhClass
        '
        Me.colhClass.Text = "Class"
        Me.colhClass.Width = 100
        '
        'colhGrade
        '
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade"
        Me.colhGrade.Width = 100
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.Text = "Grade Level"
        Me.colhGradeLevel.Width = 0
        '
        'colhJobsBy
        '
        Me.colhJobsBy.Tag = "colhJobsBy"
        Me.colhJobsBy.Text = "Jobs By"
        Me.colhJobsBy.Width = 0
        '
        'colhJobLevel
        '
        Me.colhJobLevel.Tag = "colhJobLevel"
        Me.colhJobLevel.Text = "Job Level"
        Me.colhJobLevel.Width = 0
        '
        'colhCreateDate
        '
        Me.colhCreateDate.Tag = "colhCreateDate"
        Me.colhCreateDate.Text = "Create Date"
        Me.colhCreateDate.Width = 90
        '
        'colhTerminateDate
        '
        Me.colhTerminateDate.Tag = "colhTerminateDate"
        Me.colhTerminateDate.Text = "Terminate Date"
        Me.colhTerminateDate.Width = 90
        '
        'colhPosition
        '
        Me.colhPosition.Tag = "colhPosition"
        Me.colhPosition.Text = "Vacancies"
        Me.colhPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colhPosition.Width = 70
        '
        'colhReportTo
        '
        Me.colhReportTo.Tag = "colhReportTo"
        Me.colhReportTo.Text = "Report To"
        Me.colhReportTo.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.cboGradeLevel)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.ChkGradeLevel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1018, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(806, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(306, 19)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(113, 21)
        Me.cboGradeLevel.TabIndex = 229
        Me.cboGradeLevel.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(703, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(600, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(909, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'ChkGradeLevel
        '
        Me.ChkGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkGradeLevel.Location = New System.Drawing.Point(177, 21)
        Me.ChkGradeLevel.Name = "ChkGradeLevel"
        Me.ChkGradeLevel.Size = New System.Drawing.Size(123, 17)
        Me.ChkGradeLevel.TabIndex = 222
        Me.ChkGradeLevel.Text = "Grade Level"
        Me.ChkGradeLevel.UseVisualStyleBackColor = True
        Me.ChkGradeLevel.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(1018, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Job List"
        '
        'frmJobsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Job List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvJobList As eZee.Common.eZeeListView
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents nudPosition As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents lblTerminateDate As System.Windows.Forms.Label
    Friend WithEvents dtpTerminateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCreateDate As System.Windows.Forms.Label
    Friend WithEvents dtpCreateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblReportTo As System.Windows.Forms.Label
    Friend WithEvents cboReportTo As System.Windows.Forms.ComboBox
    Friend WithEvents colhJobsBy As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCreateDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTerminateDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhReportTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboTeam As System.Windows.Forms.ComboBox
    Friend WithEvents chkJobGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkUnit As System.Windows.Forms.CheckBox
    Friend WithEvents chkTeam As System.Windows.Forms.CheckBox
    Friend WithEvents chkSection As System.Windows.Forms.CheckBox
    Friend WithEvents chkGrade As System.Windows.Forms.CheckBox
    Friend WithEvents colhJobGruop As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTeam As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkDepartment As System.Windows.Forms.CheckBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents chkClassGroup As System.Windows.Forms.CheckBox
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboInDirectReportTo As System.Windows.Forms.ComboBox
    Friend WithEvents LblInDirectReportTo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchInDirectReportTo As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDirectReportTo As eZee.Common.eZeeGradientButton
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClassGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartmentGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhsectionGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnitGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents ChkGradeLevel As System.Windows.Forms.CheckBox
    Friend WithEvents chkBranch As System.Windows.Forms.CheckBox
    Friend WithEvents chkUnitGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkSectionGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkClass As System.Windows.Forms.CheckBox
    Friend WithEvents chkDepartmentGroup As System.Windows.Forms.CheckBox
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents cboDeparmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboSectionGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnitGrp As System.Windows.Forms.ComboBox
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
End Class
