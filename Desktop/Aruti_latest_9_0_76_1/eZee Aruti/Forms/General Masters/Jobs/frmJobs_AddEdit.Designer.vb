﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobs_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJobs_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tabcJobInfo = New System.Windows.Forms.TabControl
        Me.tabpJobInfo = New System.Windows.Forms.TabPage
        Me.gbJobsInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkKeyRole = New System.Windows.Forms.CheckBox
        Me.cboJobType = New System.Windows.Forms.ComboBox
        Me.lblJobType = New System.Windows.Forms.Label
        Me.lblcritical = New System.Windows.Forms.Label
        Me.nudCritical = New System.Windows.Forms.NumericUpDown
        Me.chkUnitGroup = New System.Windows.Forms.CheckBox
        Me.chkSectionGroup = New System.Windows.Forms.CheckBox
        Me.chkClass = New System.Windows.Forms.CheckBox
        Me.chkBranch = New System.Windows.Forms.CheckBox
        Me.chkDepartmentGroup = New System.Windows.Forms.CheckBox
        Me.lblTerminateDate = New System.Windows.Forms.Label
        Me.lblCreateDate = New System.Windows.Forms.Label
        Me.pnlSectionGrp = New System.Windows.Forms.Panel
        Me.objbtnSearchSectionGroup = New eZee.Common.eZeeGradientButton
        Me.cboSectionGrp = New System.Windows.Forms.ComboBox
        Me.pnlClass = New System.Windows.Forms.Panel
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.pnlBranch = New System.Windows.Forms.Panel
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.pnlUnitGrp = New System.Windows.Forms.Panel
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.cboUnitGrp = New System.Windows.Forms.ComboBox
        Me.tabRemarks = New System.Windows.Forms.TabControl
        Me.tabpgExperienceComment = New System.Windows.Forms.TabPage
        Me.txtExperienceRemark = New System.Windows.Forms.TextBox
        Me.tabpgWorkingHrs = New System.Windows.Forms.TabPage
        Me.txtWorkingHrs = New eZee.TextBox.AlphanumericTextBox
        Me.pnlDeprtmentGrp = New System.Windows.Forms.Panel
        Me.objbtnSearchDeparmentGrp = New eZee.Common.eZeeGradientButton
        Me.cboDeparmentGrp = New System.Windows.Forms.ComboBox
        Me.nudExperienceMonth = New System.Windows.Forms.NumericUpDown
        Me.chkClassGroup = New System.Windows.Forms.CheckBox
        Me.chkDepartment = New System.Windows.Forms.CheckBox
        Me.LblExperienceMonth = New System.Windows.Forms.Label
        Me.pnlDepartment = New System.Windows.Forms.Panel
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.objbtnAddDepartment = New eZee.Common.eZeeGradientButton
        Me.nudExperienceYear = New System.Windows.Forms.NumericUpDown
        Me.pnlClassGroup = New System.Windows.Forms.Panel
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchClassGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddClassGroup = New eZee.Common.eZeeGradientButton
        Me.LblExperienceInfo = New System.Windows.Forms.Label
        Me.objbtnSearchInDirectReportTo = New eZee.Common.eZeeGradientButton
        Me.LblInDirectReportTo = New System.Windows.Forms.Label
        Me.cboInDirectReportTo = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDirectReportTo = New eZee.Common.eZeeGradientButton
        Me.lblReportTo = New System.Windows.Forms.Label
        Me.cboReportTo = New System.Windows.Forms.ComboBox
        Me.elLine2 = New eZee.Common.eZeeLine
        Me.pnlJobGrp = New System.Windows.Forms.Panel
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchJGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnAddJobGroup = New eZee.Common.eZeeGradientButton
        Me.elLine1 = New eZee.Common.eZeeLine
        Me.pnlGrades = New System.Windows.Forms.Panel
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnAddGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objbtnAddReminder = New eZee.Common.eZeeGradientButton
        Me.chkTeam = New System.Windows.Forms.CheckBox
        Me.chkUnit = New System.Windows.Forms.CheckBox
        Me.pnlTeam = New System.Windows.Forms.Panel
        Me.cboTeam = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.objbtnAddTeam = New eZee.Common.eZeeGradientButton
        Me.pnlSection = New System.Windows.Forms.Panel
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.objbtnAddSection = New eZee.Common.eZeeGradientButton
        Me.pnlUnit = New System.Windows.Forms.Panel
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnit = New eZee.Common.eZeeGradientButton
        Me.objbtnAddUnit = New eZee.Common.eZeeGradientButton
        Me.chkGrades = New System.Windows.Forms.CheckBox
        Me.lblJobLevel = New System.Windows.Forms.Label
        Me.chkJobGroup = New System.Windows.Forms.CheckBox
        Me.nudJobLevel = New System.Windows.Forms.NumericUpDown
        Me.chkSection = New System.Windows.Forms.CheckBox
        Me.nudPosition = New System.Windows.Forms.NumericUpDown
        Me.lblPosition = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.dtpCreateDate = New System.Windows.Forms.DateTimePicker
        Me.dtpTerminateDate = New System.Windows.Forms.DateTimePicker
        Me.lblCode = New System.Windows.Forms.Label
        Me.tabpJobSkills = New System.Windows.Forms.TabPage
        Me.gbJobSkill = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchSkill = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.objbtnAddSkill = New eZee.Common.eZeeGradientButton
        Me.objbtnAddSkillCategory = New eZee.Common.eZeeGradientButton
        Me.pnlJobSkill = New System.Windows.Forms.Panel
        Me.lvJobSkill = New eZee.Common.eZeeListView(Me.components)
        Me.colhCategory = New System.Windows.Forms.ColumnHeader
        Me.colhSkill = New System.Windows.Forms.ColumnHeader
        Me.objSkillUnkid = New System.Windows.Forms.ColumnHeader
        Me.objSkillCatUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhJobUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblSkills = New System.Windows.Forms.Label
        Me.cboSkillSets = New System.Windows.Forms.ComboBox
        Me.cboSkillCategory = New System.Windows.Forms.ComboBox
        Me.lblSkillCategory = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.tabpQualification = New System.Windows.Forms.TabPage
        Me.gbJobQulification = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQGrp = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnAddQGroup = New eZee.Common.eZeeGradientButton
        Me.pnlQualification = New System.Windows.Forms.Panel
        Me.lvQualification = New eZee.Common.eZeeListView(Me.components)
        Me.colhQualificationGrp = New System.Windows.Forms.ColumnHeader
        Me.colhQualification = New System.Windows.Forms.ColumnHeader
        Me.objQulificationUnkid = New System.Windows.Forms.ColumnHeader
        Me.objQGroupUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhQJobUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhQGUID = New System.Windows.Forms.ColumnHeader
        Me.btnDeleteJobQualification = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddJobQualification = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblQualification = New System.Windows.Forms.Label
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.cboQualificationGrp = New System.Windows.Forms.ComboBox
        Me.lblQualificationGroup = New System.Windows.Forms.Label
        Me.objelLine11 = New eZee.Common.eZeeLine
        Me.tapbpKeyduties = New System.Windows.Forms.TabPage
        Me.gbKeyDutiesResponsiblities = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnKeyDutiesAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtKeyDuties = New System.Windows.Forms.TextBox
        Me.btnKeyDutiesEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblKeyDuties = New System.Windows.Forms.Label
        Me.pnlKeyDuties = New System.Windows.Forms.Panel
        Me.dgJobKeyDuties = New System.Windows.Forms.DataGridView
        Me.objdgcolhKeyDutiesEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhKeyDuties = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhKeyDutiesGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobKeydutiestranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnKeyDutiesDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.tapbpCompetencies = New System.Windows.Forms.TabPage
        Me.gbCompetencies = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objBtnSearchCompetencies = New eZee.Common.eZeeGradientButton
        Me.objbtnSeachCompetenciesCategory = New eZee.Common.eZeeGradientButton
        Me.pnlCompetencies = New System.Windows.Forms.Panel
        Me.lvCompetencies = New eZee.Common.eZeeListView(Me.components)
        Me.colhCompetenciesGroup = New System.Windows.Forms.ColumnHeader
        Me.colhCompetenciesItem = New System.Windows.Forms.ColumnHeader
        Me.objcolhCompetencyGUID = New System.Windows.Forms.ColumnHeader
        Me.btnDeleteCompetencies = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddCompetencies = New eZee.Common.eZeeLightButton(Me.components)
        Me.LblCompetenciesItem = New System.Windows.Forms.Label
        Me.cboCompetencies = New System.Windows.Forms.ComboBox
        Me.cboCompetenciesCategory = New System.Windows.Forms.ComboBox
        Me.LblCompetenciesCategory = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.tapbpJobDescription = New System.Windows.Forms.TabPage
        Me.pnlDescription = New System.Windows.Forms.Panel
        Me.txtJobDescription = New System.Windows.Forms.RichTextBox
        Me.tabpJobLanguage = New System.Windows.Forms.TabPage
        Me.gbJobLanguage = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchLanguage = New eZee.Common.eZeeGradientButton
        Me.objbtnAddLanguage = New eZee.Common.eZeeGradientButton
        Me.pnlJobLAnguage = New System.Windows.Forms.Panel
        Me.lvJobLanguage = New eZee.Common.eZeeListView(Me.components)
        Me.colhLanguage = New System.Windows.Forms.ColumnHeader
        Me.objLanguageUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLangJobUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhLangGUID = New System.Windows.Forms.ColumnHeader
        Me.btnDeleteLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboLanguage = New System.Windows.Forms.ComboBox
        Me.lblLanguage = New System.Windows.Forms.Label
        Me.objelLine4 = New eZee.Common.eZeeLine
        Me.objbtnSearchJobCode = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.ChkGradeLevel = New System.Windows.Forms.CheckBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlGradresLevel = New System.Windows.Forms.Panel
        Me.objbtnSearchGradesLevel = New eZee.Common.eZeeGradientButton
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcJobInfo.SuspendLayout()
        Me.tabpJobInfo.SuspendLayout()
        Me.gbJobsInfo.SuspendLayout()
        CType(Me.nudCritical, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSectionGrp.SuspendLayout()
        Me.pnlClass.SuspendLayout()
        Me.pnlBranch.SuspendLayout()
        Me.pnlUnitGrp.SuspendLayout()
        Me.tabRemarks.SuspendLayout()
        Me.tabpgExperienceComment.SuspendLayout()
        Me.tabpgWorkingHrs.SuspendLayout()
        Me.pnlDeprtmentGrp.SuspendLayout()
        CType(Me.nudExperienceMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDepartment.SuspendLayout()
        CType(Me.nudExperienceYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlClassGroup.SuspendLayout()
        Me.pnlJobGrp.SuspendLayout()
        Me.pnlGrades.SuspendLayout()
        Me.pnlTeam.SuspendLayout()
        Me.pnlSection.SuspendLayout()
        Me.pnlUnit.SuspendLayout()
        CType(Me.nudJobLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpJobSkills.SuspendLayout()
        Me.gbJobSkill.SuspendLayout()
        Me.pnlJobSkill.SuspendLayout()
        Me.tabpQualification.SuspendLayout()
        Me.gbJobQulification.SuspendLayout()
        Me.pnlQualification.SuspendLayout()
        Me.tapbpKeyduties.SuspendLayout()
        Me.gbKeyDutiesResponsiblities.SuspendLayout()
        Me.pnlKeyDuties.SuspendLayout()
        CType(Me.dgJobKeyDuties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tapbpCompetencies.SuspendLayout()
        Me.gbCompetencies.SuspendLayout()
        Me.pnlCompetencies.SuspendLayout()
        Me.tapbpJobDescription.SuspendLayout()
        Me.pnlDescription.SuspendLayout()
        Me.tabpJobLanguage.SuspendLayout()
        Me.gbJobLanguage.SuspendLayout()
        Me.pnlJobLAnguage.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlGradresLevel.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tabcJobInfo)
        Me.pnlMainInfo.Controls.Add(Me.objbtnSearchJobCode)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(782, 567)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tabcJobInfo
        '
        Me.tabcJobInfo.Controls.Add(Me.tabpJobInfo)
        Me.tabcJobInfo.Controls.Add(Me.tabpJobSkills)
        Me.tabcJobInfo.Controls.Add(Me.tabpQualification)
        Me.tabcJobInfo.Controls.Add(Me.tapbpKeyduties)
        Me.tabcJobInfo.Controls.Add(Me.tapbpCompetencies)
        Me.tabcJobInfo.Controls.Add(Me.tapbpJobDescription)
        Me.tabcJobInfo.Controls.Add(Me.tabpJobLanguage)
        Me.tabcJobInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcJobInfo.Location = New System.Drawing.Point(0, 0)
        Me.tabcJobInfo.Multiline = True
        Me.tabcJobInfo.Name = "tabcJobInfo"
        Me.tabcJobInfo.SelectedIndex = 0
        Me.tabcJobInfo.Size = New System.Drawing.Size(782, 512)
        Me.tabcJobInfo.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight
        Me.tabcJobInfo.TabIndex = 0
        '
        'tabpJobInfo
        '
        Me.tabpJobInfo.Controls.Add(Me.gbJobsInfo)
        Me.tabpJobInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpJobInfo.Name = "tabpJobInfo"
        Me.tabpJobInfo.Size = New System.Drawing.Size(774, 486)
        Me.tabpJobInfo.TabIndex = 0
        Me.tabpJobInfo.Text = "Job Info"
        Me.tabpJobInfo.UseVisualStyleBackColor = True
        '
        'gbJobsInfo
        '
        Me.gbJobsInfo.BorderColor = System.Drawing.Color.Black
        Me.gbJobsInfo.Checked = False
        Me.gbJobsInfo.CollapseAllExceptThis = False
        Me.gbJobsInfo.CollapsedHoverImage = Nothing
        Me.gbJobsInfo.CollapsedNormalImage = Nothing
        Me.gbJobsInfo.CollapsedPressedImage = Nothing
        Me.gbJobsInfo.CollapseOnLoad = False
        Me.gbJobsInfo.Controls.Add(Me.chkKeyRole)
        Me.gbJobsInfo.Controls.Add(Me.cboJobType)
        Me.gbJobsInfo.Controls.Add(Me.lblJobType)
        Me.gbJobsInfo.Controls.Add(Me.lblcritical)
        Me.gbJobsInfo.Controls.Add(Me.nudCritical)
        Me.gbJobsInfo.Controls.Add(Me.chkUnitGroup)
        Me.gbJobsInfo.Controls.Add(Me.chkSectionGroup)
        Me.gbJobsInfo.Controls.Add(Me.chkClass)
        Me.gbJobsInfo.Controls.Add(Me.chkBranch)
        Me.gbJobsInfo.Controls.Add(Me.chkDepartmentGroup)
        Me.gbJobsInfo.Controls.Add(Me.lblTerminateDate)
        Me.gbJobsInfo.Controls.Add(Me.lblCreateDate)
        Me.gbJobsInfo.Controls.Add(Me.pnlSectionGrp)
        Me.gbJobsInfo.Controls.Add(Me.pnlClass)
        Me.gbJobsInfo.Controls.Add(Me.pnlBranch)
        Me.gbJobsInfo.Controls.Add(Me.pnlUnitGrp)
        Me.gbJobsInfo.Controls.Add(Me.tabRemarks)
        Me.gbJobsInfo.Controls.Add(Me.pnlDeprtmentGrp)
        Me.gbJobsInfo.Controls.Add(Me.nudExperienceMonth)
        Me.gbJobsInfo.Controls.Add(Me.chkClassGroup)
        Me.gbJobsInfo.Controls.Add(Me.chkDepartment)
        Me.gbJobsInfo.Controls.Add(Me.LblExperienceMonth)
        Me.gbJobsInfo.Controls.Add(Me.pnlDepartment)
        Me.gbJobsInfo.Controls.Add(Me.nudExperienceYear)
        Me.gbJobsInfo.Controls.Add(Me.pnlClassGroup)
        Me.gbJobsInfo.Controls.Add(Me.LblExperienceInfo)
        Me.gbJobsInfo.Controls.Add(Me.objbtnSearchInDirectReportTo)
        Me.gbJobsInfo.Controls.Add(Me.LblInDirectReportTo)
        Me.gbJobsInfo.Controls.Add(Me.cboInDirectReportTo)
        Me.gbJobsInfo.Controls.Add(Me.objbtnSearchDirectReportTo)
        Me.gbJobsInfo.Controls.Add(Me.lblReportTo)
        Me.gbJobsInfo.Controls.Add(Me.cboReportTo)
        Me.gbJobsInfo.Controls.Add(Me.elLine2)
        Me.gbJobsInfo.Controls.Add(Me.pnlJobGrp)
        Me.gbJobsInfo.Controls.Add(Me.elLine1)
        Me.gbJobsInfo.Controls.Add(Me.pnlGrades)
        Me.gbJobsInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbJobsInfo.Controls.Add(Me.objbtnAddReminder)
        Me.gbJobsInfo.Controls.Add(Me.chkTeam)
        Me.gbJobsInfo.Controls.Add(Me.chkUnit)
        Me.gbJobsInfo.Controls.Add(Me.pnlTeam)
        Me.gbJobsInfo.Controls.Add(Me.pnlSection)
        Me.gbJobsInfo.Controls.Add(Me.pnlUnit)
        Me.gbJobsInfo.Controls.Add(Me.chkGrades)
        Me.gbJobsInfo.Controls.Add(Me.lblJobLevel)
        Me.gbJobsInfo.Controls.Add(Me.chkJobGroup)
        Me.gbJobsInfo.Controls.Add(Me.nudJobLevel)
        Me.gbJobsInfo.Controls.Add(Me.chkSection)
        Me.gbJobsInfo.Controls.Add(Me.nudPosition)
        Me.gbJobsInfo.Controls.Add(Me.lblPosition)
        Me.gbJobsInfo.Controls.Add(Me.lblName)
        Me.gbJobsInfo.Controls.Add(Me.txtName)
        Me.gbJobsInfo.Controls.Add(Me.txtCode)
        Me.gbJobsInfo.Controls.Add(Me.dtpCreateDate)
        Me.gbJobsInfo.Controls.Add(Me.dtpTerminateDate)
        Me.gbJobsInfo.Controls.Add(Me.lblCode)
        Me.gbJobsInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobsInfo.ExpandedHoverImage = Nothing
        Me.gbJobsInfo.ExpandedNormalImage = Nothing
        Me.gbJobsInfo.ExpandedPressedImage = Nothing
        Me.gbJobsInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobsInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobsInfo.HeaderHeight = 25
        Me.gbJobsInfo.HeaderMessage = ""
        Me.gbJobsInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobsInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobsInfo.HeightOnCollapse = 0
        Me.gbJobsInfo.LeftTextSpace = 0
        Me.gbJobsInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbJobsInfo.Name = "gbJobsInfo"
        Me.gbJobsInfo.OpenHeight = 300
        Me.gbJobsInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobsInfo.ShowBorder = True
        Me.gbJobsInfo.ShowCheckBox = False
        Me.gbJobsInfo.ShowCollapseButton = False
        Me.gbJobsInfo.ShowDefaultBorderColor = True
        Me.gbJobsInfo.ShowDownButton = False
        Me.gbJobsInfo.ShowHeader = True
        Me.gbJobsInfo.Size = New System.Drawing.Size(774, 486)
        Me.gbJobsInfo.TabIndex = 0
        Me.gbJobsInfo.Temp = 0
        Me.gbJobsInfo.Text = "Job Information"
        Me.gbJobsInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkKeyRole
        '
        Me.chkKeyRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkKeyRole.Location = New System.Drawing.Point(253, 274)
        Me.chkKeyRole.Name = "chkKeyRole"
        Me.chkKeyRole.Size = New System.Drawing.Size(130, 17)
        Me.chkKeyRole.TabIndex = 223
        Me.chkKeyRole.Text = "Successor Required"
        Me.chkKeyRole.UseVisualStyleBackColor = True
        '
        'cboJobType
        '
        Me.cboJobType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobType.FormattingEnabled = True
        Me.cboJobType.Location = New System.Drawing.Point(258, 352)
        Me.cboJobType.Name = "cboJobType"
        Me.cboJobType.Size = New System.Drawing.Size(138, 21)
        Me.cboJobType.TabIndex = 221
        '
        'lblJobType
        '
        Me.lblJobType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobType.Location = New System.Drawing.Point(195, 354)
        Me.lblJobType.Name = "lblJobType"
        Me.lblJobType.Size = New System.Drawing.Size(57, 15)
        Me.lblJobType.TabIndex = 220
        Me.lblJobType.Text = "Job Type"
        Me.lblJobType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblcritical
        '
        Me.lblcritical.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcritical.Location = New System.Drawing.Point(27, 355)
        Me.lblcritical.Name = "lblcritical"
        Me.lblcritical.Size = New System.Drawing.Size(106, 15)
        Me.lblcritical.TabIndex = 218
        Me.lblcritical.Text = "Critical"
        Me.lblcritical.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudCritical
        '
        Me.nudCritical.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudCritical.Location = New System.Drawing.Point(139, 352)
        Me.nudCritical.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudCritical.Name = "nudCritical"
        Me.nudCritical.Size = New System.Drawing.Size(50, 21)
        Me.nudCritical.TabIndex = 217
        Me.nudCritical.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chkUnitGroup
        '
        Me.chkUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnitGroup.Location = New System.Drawing.Point(26, 196)
        Me.chkUnitGroup.Name = "chkUnitGroup"
        Me.chkUnitGroup.Size = New System.Drawing.Size(118, 17)
        Me.chkUnitGroup.TabIndex = 212
        Me.chkUnitGroup.Text = "Unit Group"
        Me.chkUnitGroup.UseVisualStyleBackColor = True
        '
        'chkSectionGroup
        '
        Me.chkSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSectionGroup.Location = New System.Drawing.Point(26, 138)
        Me.chkSectionGroup.Name = "chkSectionGroup"
        Me.chkSectionGroup.Size = New System.Drawing.Size(118, 17)
        Me.chkSectionGroup.TabIndex = 210
        Me.chkSectionGroup.Text = "Section Group"
        Me.chkSectionGroup.UseVisualStyleBackColor = True
        '
        'chkClass
        '
        Me.chkClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClass.Location = New System.Drawing.Point(409, 138)
        Me.chkClass.Name = "chkClass"
        Me.chkClass.Size = New System.Drawing.Size(98, 17)
        Me.chkClass.TabIndex = 214
        Me.chkClass.Text = "Class"
        Me.chkClass.UseVisualStyleBackColor = True
        '
        'chkBranch
        '
        Me.chkBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBranch.Location = New System.Drawing.Point(26, 51)
        Me.chkBranch.Name = "chkBranch"
        Me.chkBranch.Size = New System.Drawing.Size(118, 17)
        Me.chkBranch.TabIndex = 206
        Me.chkBranch.Text = "Branch"
        Me.chkBranch.UseVisualStyleBackColor = True
        '
        'chkDepartmentGroup
        '
        Me.chkDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepartmentGroup.Location = New System.Drawing.Point(26, 80)
        Me.chkDepartmentGroup.Name = "chkDepartmentGroup"
        Me.chkDepartmentGroup.Size = New System.Drawing.Size(118, 17)
        Me.chkDepartmentGroup.TabIndex = 208
        Me.chkDepartmentGroup.Text = "Department Group"
        Me.chkDepartmentGroup.UseVisualStyleBackColor = True
        '
        'lblTerminateDate
        '
        Me.lblTerminateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerminateDate.Location = New System.Drawing.Point(27, 302)
        Me.lblTerminateDate.Name = "lblTerminateDate"
        Me.lblTerminateDate.Size = New System.Drawing.Size(106, 15)
        Me.lblTerminateDate.TabIndex = 98
        Me.lblTerminateDate.Text = "Terminate Date"
        Me.lblTerminateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCreateDate
        '
        Me.lblCreateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreateDate.Location = New System.Drawing.Point(27, 275)
        Me.lblCreateDate.Name = "lblCreateDate"
        Me.lblCreateDate.Size = New System.Drawing.Size(106, 15)
        Me.lblCreateDate.TabIndex = 96
        Me.lblCreateDate.Text = "Create Date"
        Me.lblCreateDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSectionGrp
        '
        Me.pnlSectionGrp.Controls.Add(Me.objbtnSearchSectionGroup)
        Me.pnlSectionGrp.Controls.Add(Me.cboSectionGrp)
        Me.pnlSectionGrp.Location = New System.Drawing.Point(148, 134)
        Me.pnlSectionGrp.Name = "pnlSectionGrp"
        Me.pnlSectionGrp.Size = New System.Drawing.Size(255, 25)
        Me.pnlSectionGrp.TabIndex = 1
        '
        'objbtnSearchSectionGroup
        '
        Me.objbtnSearchSectionGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSectionGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSectionGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSectionGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSectionGroup.BorderSelected = False
        Me.objbtnSearchSectionGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSectionGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSectionGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSectionGroup.Location = New System.Drawing.Point(200, 3)
        Me.objbtnSearchSectionGroup.Name = "objbtnSearchSectionGroup"
        Me.objbtnSearchSectionGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSectionGroup.TabIndex = 189
        '
        'cboSectionGrp
        '
        Me.cboSectionGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGrp.FormattingEnabled = True
        Me.cboSectionGrp.Location = New System.Drawing.Point(2, 2)
        Me.cboSectionGrp.Name = "cboSectionGrp"
        Me.cboSectionGrp.Size = New System.Drawing.Size(192, 21)
        Me.cboSectionGrp.TabIndex = 0
        '
        'pnlClass
        '
        Me.pnlClass.Controls.Add(Me.objbtnSearchClass)
        Me.pnlClass.Controls.Add(Me.cboClass)
        Me.pnlClass.Location = New System.Drawing.Point(512, 134)
        Me.pnlClass.Name = "pnlClass"
        Me.pnlClass.Size = New System.Drawing.Size(255, 25)
        Me.pnlClass.TabIndex = 215
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(200, 3)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 20)
        Me.objbtnSearchClass.TabIndex = 191
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(2, 2)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(192, 21)
        Me.cboClass.TabIndex = 0
        '
        'pnlBranch
        '
        Me.pnlBranch.Controls.Add(Me.objbtnSearchBranch)
        Me.pnlBranch.Controls.Add(Me.cboBranch)
        Me.pnlBranch.Location = New System.Drawing.Point(148, 47)
        Me.pnlBranch.Name = "pnlBranch"
        Me.pnlBranch.Size = New System.Drawing.Size(255, 25)
        Me.pnlBranch.TabIndex = 207
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(200, 3)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 187
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(2, 2)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(192, 21)
        Me.cboBranch.TabIndex = 0
        '
        'pnlUnitGrp
        '
        Me.pnlUnitGrp.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.pnlUnitGrp.Controls.Add(Me.cboUnitGrp)
        Me.pnlUnitGrp.Location = New System.Drawing.Point(148, 192)
        Me.pnlUnitGrp.Name = "pnlUnitGrp"
        Me.pnlUnitGrp.Size = New System.Drawing.Size(255, 25)
        Me.pnlUnitGrp.TabIndex = 2
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(200, 3)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 190
        '
        'cboUnitGrp
        '
        Me.cboUnitGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGrp.FormattingEnabled = True
        Me.cboUnitGrp.Location = New System.Drawing.Point(2, 2)
        Me.cboUnitGrp.Name = "cboUnitGrp"
        Me.cboUnitGrp.Size = New System.Drawing.Size(192, 21)
        Me.cboUnitGrp.TabIndex = 0
        '
        'tabRemarks
        '
        Me.tabRemarks.Controls.Add(Me.tabpgExperienceComment)
        Me.tabRemarks.Controls.Add(Me.tabpgWorkingHrs)
        Me.tabRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabRemarks.Location = New System.Drawing.Point(409, 245)
        Me.tabRemarks.Name = "tabRemarks"
        Me.tabRemarks.SelectedIndex = 0
        Me.tabRemarks.Size = New System.Drawing.Size(358, 234)
        Me.tabRemarks.TabIndex = 10
        '
        'tabpgExperienceComment
        '
        Me.tabpgExperienceComment.Controls.Add(Me.txtExperienceRemark)
        Me.tabpgExperienceComment.Location = New System.Drawing.Point(4, 22)
        Me.tabpgExperienceComment.Name = "tabpgExperienceComment"
        Me.tabpgExperienceComment.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgExperienceComment.Size = New System.Drawing.Size(350, 208)
        Me.tabpgExperienceComment.TabIndex = 0
        Me.tabpgExperienceComment.Text = "Experience Comment"
        Me.tabpgExperienceComment.UseVisualStyleBackColor = True
        '
        'txtExperienceRemark
        '
        Me.txtExperienceRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtExperienceRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExperienceRemark.Location = New System.Drawing.Point(3, 3)
        Me.txtExperienceRemark.Multiline = True
        Me.txtExperienceRemark.Name = "txtExperienceRemark"
        Me.txtExperienceRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtExperienceRemark.Size = New System.Drawing.Size(344, 202)
        Me.txtExperienceRemark.TabIndex = 0
        '
        'tabpgWorkingHrs
        '
        Me.tabpgWorkingHrs.Controls.Add(Me.txtWorkingHrs)
        Me.tabpgWorkingHrs.Location = New System.Drawing.Point(4, 22)
        Me.tabpgWorkingHrs.Name = "tabpgWorkingHrs"
        Me.tabpgWorkingHrs.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgWorkingHrs.Size = New System.Drawing.Size(350, 208)
        Me.tabpgWorkingHrs.TabIndex = 1
        Me.tabpgWorkingHrs.Text = "Working Hours"
        Me.tabpgWorkingHrs.UseVisualStyleBackColor = True
        '
        'txtWorkingHrs
        '
        Me.txtWorkingHrs.BackColor = System.Drawing.SystemColors.Window
        Me.txtWorkingHrs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtWorkingHrs.Flags = 0
        Me.txtWorkingHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkingHrs.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWorkingHrs.Location = New System.Drawing.Point(3, 3)
        Me.txtWorkingHrs.Multiline = True
        Me.txtWorkingHrs.Name = "txtWorkingHrs"
        Me.txtWorkingHrs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtWorkingHrs.Size = New System.Drawing.Size(344, 202)
        Me.txtWorkingHrs.TabIndex = 209
        '
        'pnlDeprtmentGrp
        '
        Me.pnlDeprtmentGrp.Controls.Add(Me.objbtnSearchDeparmentGrp)
        Me.pnlDeprtmentGrp.Controls.Add(Me.cboDeparmentGrp)
        Me.pnlDeprtmentGrp.Location = New System.Drawing.Point(148, 76)
        Me.pnlDeprtmentGrp.Name = "pnlDeprtmentGrp"
        Me.pnlDeprtmentGrp.Size = New System.Drawing.Size(255, 25)
        Me.pnlDeprtmentGrp.TabIndex = 0
        '
        'objbtnSearchDeparmentGrp
        '
        Me.objbtnSearchDeparmentGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeparmentGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeparmentGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeparmentGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeparmentGrp.BorderSelected = False
        Me.objbtnSearchDeparmentGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeparmentGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeparmentGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeparmentGrp.Location = New System.Drawing.Point(200, 1)
        Me.objbtnSearchDeparmentGrp.Name = "objbtnSearchDeparmentGrp"
        Me.objbtnSearchDeparmentGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeparmentGrp.TabIndex = 188
        '
        'cboDeparmentGrp
        '
        Me.cboDeparmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeparmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeparmentGrp.FormattingEnabled = True
        Me.cboDeparmentGrp.Location = New System.Drawing.Point(2, 2)
        Me.cboDeparmentGrp.Name = "cboDeparmentGrp"
        Me.cboDeparmentGrp.Size = New System.Drawing.Size(192, 21)
        Me.cboDeparmentGrp.TabIndex = 0
        '
        'nudExperienceMonth
        '
        Me.nudExperienceMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudExperienceMonth.Location = New System.Drawing.Point(258, 458)
        Me.nudExperienceMonth.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nudExperienceMonth.Name = "nudExperienceMonth"
        Me.nudExperienceMonth.Size = New System.Drawing.Size(48, 21)
        Me.nudExperienceMonth.TabIndex = 9
        Me.nudExperienceMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chkClassGroup
        '
        Me.chkClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkClassGroup.Location = New System.Drawing.Point(409, 109)
        Me.chkClassGroup.Name = "chkClassGroup"
        Me.chkClassGroup.Size = New System.Drawing.Size(98, 17)
        Me.chkClassGroup.TabIndex = 13
        Me.chkClassGroup.Text = "Class Group"
        Me.chkClassGroup.UseVisualStyleBackColor = True
        '
        'chkDepartment
        '
        Me.chkDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDepartment.Location = New System.Drawing.Point(26, 109)
        Me.chkDepartment.Name = "chkDepartment"
        Me.chkDepartment.Size = New System.Drawing.Size(118, 17)
        Me.chkDepartment.TabIndex = 11
        Me.chkDepartment.Text = "Department"
        Me.chkDepartment.UseVisualStyleBackColor = True
        '
        'LblExperienceMonth
        '
        Me.LblExperienceMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExperienceMonth.Location = New System.Drawing.Point(195, 461)
        Me.LblExperienceMonth.Name = "LblExperienceMonth"
        Me.LblExperienceMonth.Size = New System.Drawing.Size(57, 15)
        Me.LblExperienceMonth.TabIndex = 204
        Me.LblExperienceMonth.Text = "Month"
        Me.LblExperienceMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlDepartment
        '
        Me.pnlDepartment.Controls.Add(Me.cboDepartment)
        Me.pnlDepartment.Controls.Add(Me.objbtnSearchDepartment)
        Me.pnlDepartment.Controls.Add(Me.objbtnAddDepartment)
        Me.pnlDepartment.Location = New System.Drawing.Point(148, 105)
        Me.pnlDepartment.Name = "pnlDepartment"
        Me.pnlDepartment.Size = New System.Drawing.Size(255, 25)
        Me.pnlDepartment.TabIndex = 12
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(1, 2)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(192, 21)
        Me.cboDepartment.TabIndex = 0
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 186
        '
        'objbtnAddDepartment
        '
        Me.objbtnAddDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDepartment.BorderSelected = False
        Me.objbtnAddDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDepartment.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddDepartment.Name = "objbtnAddDepartment"
        Me.objbtnAddDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDepartment.TabIndex = 164
        '
        'nudExperienceYear
        '
        Me.nudExperienceYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudExperienceYear.Location = New System.Drawing.Point(139, 458)
        Me.nudExperienceYear.Name = "nudExperienceYear"
        Me.nudExperienceYear.Size = New System.Drawing.Size(50, 21)
        Me.nudExperienceYear.TabIndex = 8
        Me.nudExperienceYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'pnlClassGroup
        '
        Me.pnlClassGroup.Controls.Add(Me.cboClassGroup)
        Me.pnlClassGroup.Controls.Add(Me.objbtnSearchClassGroup)
        Me.pnlClassGroup.Controls.Add(Me.objbtnAddClassGroup)
        Me.pnlClassGroup.Location = New System.Drawing.Point(512, 105)
        Me.pnlClassGroup.Name = "pnlClassGroup"
        Me.pnlClassGroup.Size = New System.Drawing.Size(255, 25)
        Me.pnlClassGroup.TabIndex = 14
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(2, 2)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(192, 21)
        Me.cboClassGroup.TabIndex = 0
        '
        'objbtnSearchClassGroup
        '
        Me.objbtnSearchClassGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGroup.BorderSelected = False
        Me.objbtnSearchClassGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGroup.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchClassGroup.Name = "objbtnSearchClassGroup"
        Me.objbtnSearchClassGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGroup.TabIndex = 186
        '
        'objbtnAddClassGroup
        '
        Me.objbtnAddClassGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddClassGroup.BorderSelected = False
        Me.objbtnAddClassGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddClassGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddClassGroup.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddClassGroup.Name = "objbtnAddClassGroup"
        Me.objbtnAddClassGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddClassGroup.TabIndex = 164
        '
        'LblExperienceInfo
        '
        Me.LblExperienceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExperienceInfo.Location = New System.Drawing.Point(27, 461)
        Me.LblExperienceInfo.Name = "LblExperienceInfo"
        Me.LblExperienceInfo.Size = New System.Drawing.Size(106, 15)
        Me.LblExperienceInfo.TabIndex = 202
        Me.LblExperienceInfo.Text = "Experience Year"
        Me.LblExperienceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchInDirectReportTo
        '
        Me.objbtnSearchInDirectReportTo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInDirectReportTo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInDirectReportTo.BorderSelected = False
        Me.objbtnSearchInDirectReportTo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInDirectReportTo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInDirectReportTo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInDirectReportTo.Location = New System.Drawing.Point(382, 431)
        Me.objbtnSearchInDirectReportTo.Name = "objbtnSearchInDirectReportTo"
        Me.objbtnSearchInDirectReportTo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInDirectReportTo.TabIndex = 201
        '
        'LblInDirectReportTo
        '
        Me.LblInDirectReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblInDirectReportTo.Location = New System.Drawing.Point(27, 434)
        Me.LblInDirectReportTo.Name = "LblInDirectReportTo"
        Me.LblInDirectReportTo.Size = New System.Drawing.Size(106, 15)
        Me.LblInDirectReportTo.TabIndex = 199
        Me.LblInDirectReportTo.Text = "In-Direct Report To"
        Me.LblInDirectReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInDirectReportTo
        '
        Me.cboInDirectReportTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInDirectReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInDirectReportTo.FormattingEnabled = True
        Me.cboInDirectReportTo.Location = New System.Drawing.Point(139, 431)
        Me.cboInDirectReportTo.Name = "cboInDirectReportTo"
        Me.cboInDirectReportTo.Size = New System.Drawing.Size(237, 21)
        Me.cboInDirectReportTo.TabIndex = 7
        '
        'objbtnSearchDirectReportTo
        '
        Me.objbtnSearchDirectReportTo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDirectReportTo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDirectReportTo.BorderSelected = False
        Me.objbtnSearchDirectReportTo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDirectReportTo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDirectReportTo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDirectReportTo.Location = New System.Drawing.Point(382, 404)
        Me.objbtnSearchDirectReportTo.Name = "objbtnSearchDirectReportTo"
        Me.objbtnSearchDirectReportTo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDirectReportTo.TabIndex = 187
        '
        'lblReportTo
        '
        Me.lblReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportTo.Location = New System.Drawing.Point(27, 407)
        Me.lblReportTo.Name = "lblReportTo"
        Me.lblReportTo.Size = New System.Drawing.Size(106, 15)
        Me.lblReportTo.TabIndex = 101
        Me.lblReportTo.Text = "Direct Report To"
        Me.lblReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportTo
        '
        Me.cboReportTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportTo.FormattingEnabled = True
        Me.cboReportTo.Location = New System.Drawing.Point(139, 404)
        Me.cboReportTo.Name = "cboReportTo"
        Me.cboReportTo.Size = New System.Drawing.Size(237, 21)
        Me.cboReportTo.TabIndex = 6
        '
        'elLine2
        '
        Me.elLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine2.Location = New System.Drawing.Point(8, 227)
        Me.elLine2.Name = "elLine2"
        Me.elLine2.Size = New System.Drawing.Size(751, 15)
        Me.elLine2.TabIndex = 191
        Me.elLine2.Text = "Jobs Info."
        Me.elLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlJobGrp
        '
        Me.pnlJobGrp.Controls.Add(Me.cboJobGroup)
        Me.pnlJobGrp.Controls.Add(Me.objbtnSearchJGroup)
        Me.pnlJobGrp.Controls.Add(Me.objbtnAddJobGroup)
        Me.pnlJobGrp.Location = New System.Drawing.Point(512, 192)
        Me.pnlJobGrp.Name = "pnlJobGrp"
        Me.pnlJobGrp.Size = New System.Drawing.Size(255, 25)
        Me.pnlJobGrp.TabIndex = 2
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(2, 2)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(192, 21)
        Me.cboJobGroup.TabIndex = 0
        '
        'objbtnSearchJGroup
        '
        Me.objbtnSearchJGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJGroup.BorderSelected = False
        Me.objbtnSearchJGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJGroup.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchJGroup.Name = "objbtnSearchJGroup"
        Me.objbtnSearchJGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJGroup.TabIndex = 2
        '
        'objbtnAddJobGroup
        '
        Me.objbtnAddJobGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJobGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJobGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJobGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJobGroup.BorderSelected = False
        Me.objbtnAddJobGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJobGroup.Image = CType(resources.GetObject("objbtnAddJobGroup.Image"), System.Drawing.Image)
        Me.objbtnAddJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJobGroup.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddJobGroup.Name = "objbtnAddJobGroup"
        Me.objbtnAddJobGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJobGroup.TabIndex = 161
        '
        'elLine1
        '
        Me.elLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine1.Location = New System.Drawing.Point(8, 29)
        Me.elLine1.Name = "elLine1"
        Me.elLine1.Size = New System.Drawing.Size(751, 15)
        Me.elLine1.TabIndex = 3
        Me.elLine1.Text = "Jobs By"
        Me.elLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlGrades
        '
        Me.pnlGrades.Controls.Add(Me.cboGrade)
        Me.pnlGrades.Controls.Add(Me.objbtnSearchGrade)
        Me.pnlGrades.Controls.Add(Me.objbtnAddGrade)
        Me.pnlGrades.Location = New System.Drawing.Point(512, 163)
        Me.pnlGrades.Name = "pnlGrades"
        Me.pnlGrades.Size = New System.Drawing.Size(255, 25)
        Me.pnlGrades.TabIndex = 10
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(2, 2)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(192, 21)
        Me.cboGrade.TabIndex = 0
        '
        'objbtnSearchGrade
        '
        Me.objbtnSearchGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrade.BorderSelected = False
        Me.objbtnSearchGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrade.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchGrade.Name = "objbtnSearchGrade"
        Me.objbtnSearchGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrade.TabIndex = 186
        '
        'objbtnAddGrade
        '
        Me.objbtnAddGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGrade.BorderSelected = False
        Me.objbtnAddGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGrade.Image = CType(resources.GetObject("objbtnAddGrade.Image"), System.Drawing.Image)
        Me.objbtnAddGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGrade.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddGrade.Name = "objbtnAddGrade"
        Me.objbtnAddGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGrade.TabIndex = 164
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(382, 381)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 160
        '
        'objbtnAddReminder
        '
        Me.objbtnAddReminder.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReminder.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReminder.BorderSelected = False
        Me.objbtnAddReminder.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReminder.Image = Global.Aruti.Main.My.Resources.Resources.Remind1
        Me.objbtnAddReminder.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReminder.Location = New System.Drawing.Point(246, 299)
        Me.objbtnAddReminder.Name = "objbtnAddReminder"
        Me.objbtnAddReminder.Size = New System.Drawing.Size(22, 21)
        Me.objbtnAddReminder.TabIndex = 136
        '
        'chkTeam
        '
        Me.chkTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTeam.Location = New System.Drawing.Point(409, 80)
        Me.chkTeam.Name = "chkTeam"
        Me.chkTeam.Size = New System.Drawing.Size(98, 17)
        Me.chkTeam.TabIndex = 7
        Me.chkTeam.Text = "Team"
        Me.chkTeam.UseVisualStyleBackColor = True
        '
        'chkUnit
        '
        Me.chkUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUnit.Location = New System.Drawing.Point(409, 51)
        Me.chkUnit.Name = "chkUnit"
        Me.chkUnit.Size = New System.Drawing.Size(98, 17)
        Me.chkUnit.TabIndex = 5
        Me.chkUnit.Text = "Unit"
        Me.chkUnit.UseVisualStyleBackColor = True
        '
        'pnlTeam
        '
        Me.pnlTeam.Controls.Add(Me.cboTeam)
        Me.pnlTeam.Controls.Add(Me.objbtnSearchTeam)
        Me.pnlTeam.Controls.Add(Me.objbtnAddTeam)
        Me.pnlTeam.Location = New System.Drawing.Point(512, 76)
        Me.pnlTeam.Name = "pnlTeam"
        Me.pnlTeam.Size = New System.Drawing.Size(255, 25)
        Me.pnlTeam.TabIndex = 8
        '
        'cboTeam
        '
        Me.cboTeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeam.FormattingEnabled = True
        Me.cboTeam.Location = New System.Drawing.Point(2, 2)
        Me.cboTeam.Name = "cboTeam"
        Me.cboTeam.Size = New System.Drawing.Size(192, 21)
        Me.cboTeam.TabIndex = 0
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 183
        '
        'objbtnAddTeam
        '
        Me.objbtnAddTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTeam.BorderSelected = False
        Me.objbtnAddTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTeam.Image = CType(resources.GetObject("objbtnAddTeam.Image"), System.Drawing.Image)
        Me.objbtnAddTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTeam.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddTeam.Name = "objbtnAddTeam"
        Me.objbtnAddTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTeam.TabIndex = 168
        '
        'pnlSection
        '
        Me.pnlSection.Controls.Add(Me.cboSection)
        Me.pnlSection.Controls.Add(Me.objbtnSearchSection)
        Me.pnlSection.Controls.Add(Me.objbtnAddSection)
        Me.pnlSection.Location = New System.Drawing.Point(148, 163)
        Me.pnlSection.Name = "pnlSection"
        Me.pnlSection.Size = New System.Drawing.Size(255, 25)
        Me.pnlSection.TabIndex = 4
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(2, 2)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(192, 21)
        Me.cboSection.TabIndex = 0
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 1
        '
        'objbtnAddSection
        '
        Me.objbtnAddSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSection.BorderSelected = False
        Me.objbtnAddSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSection.Image = CType(resources.GetObject("objbtnAddSection.Image"), System.Drawing.Image)
        Me.objbtnAddSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSection.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddSection.Name = "objbtnAddSection"
        Me.objbtnAddSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSection.TabIndex = 163
        '
        'pnlUnit
        '
        Me.pnlUnit.Controls.Add(Me.cboUnit)
        Me.pnlUnit.Controls.Add(Me.objbtnSearchUnit)
        Me.pnlUnit.Controls.Add(Me.objbtnAddUnit)
        Me.pnlUnit.Location = New System.Drawing.Point(512, 47)
        Me.pnlUnit.Name = "pnlUnit"
        Me.pnlUnit.Size = New System.Drawing.Size(255, 25)
        Me.pnlUnit.TabIndex = 6
        '
        'cboUnit
        '
        Me.cboUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(2, 2)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(192, 21)
        Me.cboUnit.TabIndex = 0
        '
        'objbtnSearchUnit
        '
        Me.objbtnSearchUnit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnit.BorderSelected = False
        Me.objbtnSearchUnit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnit.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnit.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchUnit.Name = "objbtnSearchUnit"
        Me.objbtnSearchUnit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnit.TabIndex = 1
        '
        'objbtnAddUnit
        '
        Me.objbtnAddUnit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddUnit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddUnit.BorderSelected = False
        Me.objbtnAddUnit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddUnit.Image = CType(resources.GetObject("objbtnAddUnit.Image"), System.Drawing.Image)
        Me.objbtnAddUnit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddUnit.Location = New System.Drawing.Point(227, 2)
        Me.objbtnAddUnit.Name = "objbtnAddUnit"
        Me.objbtnAddUnit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddUnit.TabIndex = 162
        '
        'chkGrades
        '
        Me.chkGrades.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGrades.Location = New System.Drawing.Point(409, 167)
        Me.chkGrades.Name = "chkGrades"
        Me.chkGrades.Size = New System.Drawing.Size(98, 17)
        Me.chkGrades.TabIndex = 9
        Me.chkGrades.Text = "Grades"
        Me.chkGrades.UseVisualStyleBackColor = True
        '
        'lblJobLevel
        '
        Me.lblJobLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobLevel.Location = New System.Drawing.Point(27, 329)
        Me.lblJobLevel.Name = "lblJobLevel"
        Me.lblJobLevel.Size = New System.Drawing.Size(106, 15)
        Me.lblJobLevel.TabIndex = 162
        Me.lblJobLevel.Text = "Job Level"
        Me.lblJobLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkJobGroup
        '
        Me.chkJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkJobGroup.Location = New System.Drawing.Point(409, 196)
        Me.chkJobGroup.Name = "chkJobGroup"
        Me.chkJobGroup.Size = New System.Drawing.Size(98, 17)
        Me.chkJobGroup.TabIndex = 1
        Me.chkJobGroup.Text = "Job Group"
        Me.chkJobGroup.UseVisualStyleBackColor = True
        '
        'nudJobLevel
        '
        Me.nudJobLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudJobLevel.Location = New System.Drawing.Point(139, 326)
        Me.nudJobLevel.Name = "nudJobLevel"
        Me.nudJobLevel.Size = New System.Drawing.Size(50, 21)
        Me.nudJobLevel.TabIndex = 1
        Me.nudJobLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chkSection
        '
        Me.chkSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSection.Location = New System.Drawing.Point(26, 167)
        Me.chkSection.Name = "chkSection"
        Me.chkSection.Size = New System.Drawing.Size(118, 17)
        Me.chkSection.TabIndex = 3
        Me.chkSection.Text = "Section"
        Me.chkSection.UseVisualStyleBackColor = True
        '
        'nudPosition
        '
        Me.nudPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPosition.Location = New System.Drawing.Point(258, 326)
        Me.nudPosition.Maximum = New Decimal(New Integer() {999999999, 0, 0, 0})
        Me.nudPosition.Name = "nudPosition"
        Me.nudPosition.Size = New System.Drawing.Size(48, 21)
        Me.nudPosition.TabIndex = 2
        Me.nudPosition.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblPosition
        '
        Me.lblPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPosition.Location = New System.Drawing.Point(195, 329)
        Me.lblPosition.Name = "lblPosition"
        Me.lblPosition.Size = New System.Drawing.Size(57, 15)
        Me.lblPosition.TabIndex = 99
        Me.lblPosition.Text = "Vacancies"
        Me.lblPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(27, 381)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(106, 15)
        Me.lblName.TabIndex = 16
        Me.lblName.Text = "Job "
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(139, 378)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(237, 21)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(139, 245)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(237, 21)
        Me.txtCode.TabIndex = 0
        '
        'dtpCreateDate
        '
        Me.dtpCreateDate.Checked = False
        Me.dtpCreateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCreateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCreateDate.Location = New System.Drawing.Point(139, 272)
        Me.dtpCreateDate.Name = "dtpCreateDate"
        Me.dtpCreateDate.ShowCheckBox = True
        Me.dtpCreateDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpCreateDate.TabIndex = 4
        '
        'dtpTerminateDate
        '
        Me.dtpTerminateDate.Checked = False
        Me.dtpTerminateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTerminateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTerminateDate.Location = New System.Drawing.Point(139, 299)
        Me.dtpTerminateDate.Name = "dtpTerminateDate"
        Me.dtpTerminateDate.ShowCheckBox = True
        Me.dtpTerminateDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpTerminateDate.TabIndex = 5
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(27, 248)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(106, 15)
        Me.lblCode.TabIndex = 13
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpJobSkills
        '
        Me.tabpJobSkills.Controls.Add(Me.gbJobSkill)
        Me.tabpJobSkills.Location = New System.Drawing.Point(4, 22)
        Me.tabpJobSkills.Name = "tabpJobSkills"
        Me.tabpJobSkills.Size = New System.Drawing.Size(774, 486)
        Me.tabpJobSkills.TabIndex = 1
        Me.tabpJobSkills.Text = "Job Skills"
        Me.tabpJobSkills.UseVisualStyleBackColor = True
        '
        'gbJobSkill
        '
        Me.gbJobSkill.BorderColor = System.Drawing.Color.Black
        Me.gbJobSkill.Checked = False
        Me.gbJobSkill.CollapseAllExceptThis = False
        Me.gbJobSkill.CollapsedHoverImage = Nothing
        Me.gbJobSkill.CollapsedNormalImage = Nothing
        Me.gbJobSkill.CollapsedPressedImage = Nothing
        Me.gbJobSkill.CollapseOnLoad = False
        Me.gbJobSkill.Controls.Add(Me.objbtnSearchSkill)
        Me.gbJobSkill.Controls.Add(Me.objbtnSearchCategory)
        Me.gbJobSkill.Controls.Add(Me.objbtnAddSkill)
        Me.gbJobSkill.Controls.Add(Me.objbtnAddSkillCategory)
        Me.gbJobSkill.Controls.Add(Me.pnlJobSkill)
        Me.gbJobSkill.Controls.Add(Me.btnDelete)
        Me.gbJobSkill.Controls.Add(Me.btnAdd)
        Me.gbJobSkill.Controls.Add(Me.lblSkills)
        Me.gbJobSkill.Controls.Add(Me.cboSkillSets)
        Me.gbJobSkill.Controls.Add(Me.cboSkillCategory)
        Me.gbJobSkill.Controls.Add(Me.lblSkillCategory)
        Me.gbJobSkill.Controls.Add(Me.objelLine1)
        Me.gbJobSkill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobSkill.ExpandedHoverImage = Nothing
        Me.gbJobSkill.ExpandedNormalImage = Nothing
        Me.gbJobSkill.ExpandedPressedImage = Nothing
        Me.gbJobSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobSkill.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobSkill.HeaderHeight = 25
        Me.gbJobSkill.HeaderMessage = ""
        Me.gbJobSkill.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobSkill.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobSkill.HeightOnCollapse = 0
        Me.gbJobSkill.LeftTextSpace = 0
        Me.gbJobSkill.Location = New System.Drawing.Point(0, 0)
        Me.gbJobSkill.Name = "gbJobSkill"
        Me.gbJobSkill.OpenHeight = 300
        Me.gbJobSkill.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobSkill.ShowBorder = True
        Me.gbJobSkill.ShowCheckBox = False
        Me.gbJobSkill.ShowCollapseButton = False
        Me.gbJobSkill.ShowDefaultBorderColor = True
        Me.gbJobSkill.ShowDownButton = False
        Me.gbJobSkill.ShowHeader = True
        Me.gbJobSkill.Size = New System.Drawing.Size(774, 486)
        Me.gbJobSkill.TabIndex = 20
        Me.gbJobSkill.Temp = 0
        Me.gbJobSkill.Text = "Job Skills"
        Me.gbJobSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSkill
        '
        Me.objbtnSearchSkill.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSkill.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSkill.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSkill.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSkill.BorderSelected = False
        Me.objbtnSearchSkill.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSkill.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSkill.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSkill.Location = New System.Drawing.Point(722, 60)
        Me.objbtnSearchSkill.Name = "objbtnSearchSkill"
        Me.objbtnSearchSkill.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSkill.TabIndex = 230
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(722, 33)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 229
        '
        'objbtnAddSkill
        '
        Me.objbtnAddSkill.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkill.BorderSelected = False
        Me.objbtnAddSkill.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkill.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSkill.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkill.Location = New System.Drawing.Point(749, 60)
        Me.objbtnAddSkill.Name = "objbtnAddSkill"
        Me.objbtnAddSkill.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkill.TabIndex = 227
        '
        'objbtnAddSkillCategory
        '
        Me.objbtnAddSkillCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkillCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkillCategory.BorderSelected = False
        Me.objbtnAddSkillCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkillCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSkillCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkillCategory.Location = New System.Drawing.Point(749, 33)
        Me.objbtnAddSkillCategory.Name = "objbtnAddSkillCategory"
        Me.objbtnAddSkillCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkillCategory.TabIndex = 226
        '
        'pnlJobSkill
        '
        Me.pnlJobSkill.Controls.Add(Me.lvJobSkill)
        Me.pnlJobSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobSkill.Location = New System.Drawing.Point(11, 133)
        Me.pnlJobSkill.Name = "pnlJobSkill"
        Me.pnlJobSkill.Size = New System.Drawing.Size(759, 334)
        Me.pnlJobSkill.TabIndex = 224
        '
        'lvJobSkill
        '
        Me.lvJobSkill.BackColorOnChecked = True
        Me.lvJobSkill.ColumnHeaders = Nothing
        Me.lvJobSkill.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCategory, Me.colhSkill, Me.objSkillUnkid, Me.objSkillCatUnkid, Me.objcolhJobUnkid, Me.objcolhGUID})
        Me.lvJobSkill.CompulsoryColumns = ""
        Me.lvJobSkill.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobSkill.FullRowSelect = True
        Me.lvJobSkill.GridLines = True
        Me.lvJobSkill.GroupingColumn = Nothing
        Me.lvJobSkill.HideSelection = False
        Me.lvJobSkill.Location = New System.Drawing.Point(0, 0)
        Me.lvJobSkill.MinColumnWidth = 50
        Me.lvJobSkill.MultiSelect = False
        Me.lvJobSkill.Name = "lvJobSkill"
        Me.lvJobSkill.OptionalColumns = ""
        Me.lvJobSkill.ShowMoreItem = False
        Me.lvJobSkill.ShowSaveItem = False
        Me.lvJobSkill.ShowSelectAll = True
        Me.lvJobSkill.ShowSizeAllColumnsToFit = True
        Me.lvJobSkill.Size = New System.Drawing.Size(759, 334)
        Me.lvJobSkill.Sortable = True
        Me.lvJobSkill.TabIndex = 224
        Me.lvJobSkill.UseCompatibleStateImageBehavior = False
        Me.lvJobSkill.View = System.Windows.Forms.View.Details
        '
        'colhCategory
        '
        Me.colhCategory.Tag = "colhCategory"
        Me.colhCategory.Text = "Category"
        Me.colhCategory.Width = 300
        '
        'colhSkill
        '
        Me.colhSkill.Tag = "colhSkill"
        Me.colhSkill.Text = "Skill"
        Me.colhSkill.Width = 350
        '
        'objSkillUnkid
        '
        Me.objSkillUnkid.Tag = "objSkillUnkid"
        Me.objSkillUnkid.Text = ""
        Me.objSkillUnkid.Width = 0
        '
        'objSkillCatUnkid
        '
        Me.objSkillCatUnkid.Tag = "objSkillCatUnkid"
        Me.objSkillCatUnkid.Text = ""
        Me.objSkillCatUnkid.Width = 0
        '
        'objcolhJobUnkid
        '
        Me.objcolhJobUnkid.Tag = "objcolhJobUnkid"
        Me.objcolhJobUnkid.Text = ""
        Me.objcolhJobUnkid.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(680, 97)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 223
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(584, 97)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnAdd.TabIndex = 221
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lblSkills
        '
        Me.lblSkills.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkills.Location = New System.Drawing.Point(8, 63)
        Me.lblSkills.Name = "lblSkills"
        Me.lblSkills.Size = New System.Drawing.Size(103, 15)
        Me.lblSkills.TabIndex = 24
        Me.lblSkills.Text = "Skill "
        Me.lblSkills.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSkillSets
        '
        Me.cboSkillSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkillSets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillSets.FormattingEnabled = True
        Me.cboSkillSets.Location = New System.Drawing.Point(117, 60)
        Me.cboSkillSets.Name = "cboSkillSets"
        Me.cboSkillSets.Size = New System.Drawing.Size(599, 21)
        Me.cboSkillSets.TabIndex = 23
        '
        'cboSkillCategory
        '
        Me.cboSkillCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillCategory.FormattingEnabled = True
        Me.cboSkillCategory.Location = New System.Drawing.Point(117, 33)
        Me.cboSkillCategory.Name = "cboSkillCategory"
        Me.cboSkillCategory.Size = New System.Drawing.Size(599, 21)
        Me.cboSkillCategory.TabIndex = 22
        '
        'lblSkillCategory
        '
        Me.lblSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillCategory.Location = New System.Drawing.Point(8, 36)
        Me.lblSkillCategory.Name = "lblSkillCategory"
        Me.lblSkillCategory.Size = New System.Drawing.Size(78, 15)
        Me.lblSkillCategory.TabIndex = 20
        Me.lblSkillCategory.Text = "Skill Category"
        Me.lblSkillCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 84)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(759, 10)
        Me.objelLine1.TabIndex = 19
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpQualification
        '
        Me.tabpQualification.Controls.Add(Me.gbJobQulification)
        Me.tabpQualification.Location = New System.Drawing.Point(4, 22)
        Me.tabpQualification.Name = "tabpQualification"
        Me.tabpQualification.Size = New System.Drawing.Size(774, 486)
        Me.tabpQualification.TabIndex = 3
        Me.tabpQualification.Text = "Job Qualification"
        Me.tabpQualification.UseVisualStyleBackColor = True
        '
        'gbJobQulification
        '
        Me.gbJobQulification.BorderColor = System.Drawing.Color.Black
        Me.gbJobQulification.Checked = False
        Me.gbJobQulification.CollapseAllExceptThis = False
        Me.gbJobQulification.CollapsedHoverImage = Nothing
        Me.gbJobQulification.CollapsedNormalImage = Nothing
        Me.gbJobQulification.CollapsedPressedImage = Nothing
        Me.gbJobQulification.CollapseOnLoad = False
        Me.gbJobQulification.Controls.Add(Me.objbtnSearchQualification)
        Me.gbJobQulification.Controls.Add(Me.objbtnSearchQGrp)
        Me.gbJobQulification.Controls.Add(Me.objbtnAddQualification)
        Me.gbJobQulification.Controls.Add(Me.objbtnAddQGroup)
        Me.gbJobQulification.Controls.Add(Me.pnlQualification)
        Me.gbJobQulification.Controls.Add(Me.btnDeleteJobQualification)
        Me.gbJobQulification.Controls.Add(Me.btnAddJobQualification)
        Me.gbJobQulification.Controls.Add(Me.lblQualification)
        Me.gbJobQulification.Controls.Add(Me.cboQualification)
        Me.gbJobQulification.Controls.Add(Me.cboQualificationGrp)
        Me.gbJobQulification.Controls.Add(Me.lblQualificationGroup)
        Me.gbJobQulification.Controls.Add(Me.objelLine11)
        Me.gbJobQulification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobQulification.ExpandedHoverImage = Nothing
        Me.gbJobQulification.ExpandedNormalImage = Nothing
        Me.gbJobQulification.ExpandedPressedImage = Nothing
        Me.gbJobQulification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobQulification.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobQulification.HeaderHeight = 25
        Me.gbJobQulification.HeaderMessage = ""
        Me.gbJobQulification.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobQulification.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobQulification.HeightOnCollapse = 0
        Me.gbJobQulification.LeftTextSpace = 0
        Me.gbJobQulification.Location = New System.Drawing.Point(0, 0)
        Me.gbJobQulification.Name = "gbJobQulification"
        Me.gbJobQulification.OpenHeight = 300
        Me.gbJobQulification.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobQulification.ShowBorder = True
        Me.gbJobQulification.ShowCheckBox = False
        Me.gbJobQulification.ShowCollapseButton = False
        Me.gbJobQulification.ShowDefaultBorderColor = True
        Me.gbJobQulification.ShowDownButton = False
        Me.gbJobQulification.ShowHeader = True
        Me.gbJobQulification.Size = New System.Drawing.Size(774, 486)
        Me.gbJobQulification.TabIndex = 21
        Me.gbJobQulification.Temp = 0
        Me.gbJobQulification.Text = "Job Qulification"
        Me.gbJobQulification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchQualification
        '
        Me.objbtnSearchQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualification.BorderSelected = False
        Me.objbtnSearchQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualification.Location = New System.Drawing.Point(722, 60)
        Me.objbtnSearchQualification.Name = "objbtnSearchQualification"
        Me.objbtnSearchQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQualification.TabIndex = 230
        '
        'objbtnSearchQGrp
        '
        Me.objbtnSearchQGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQGrp.BorderSelected = False
        Me.objbtnSearchQGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQGrp.Location = New System.Drawing.Point(722, 33)
        Me.objbtnSearchQGrp.Name = "objbtnSearchQGrp"
        Me.objbtnSearchQGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQGrp.TabIndex = 229
        '
        'objbtnAddQualification
        '
        Me.objbtnAddQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQualification.BorderSelected = False
        Me.objbtnAddQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQualification.Location = New System.Drawing.Point(749, 60)
        Me.objbtnAddQualification.Name = "objbtnAddQualification"
        Me.objbtnAddQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQualification.TabIndex = 227
        '
        'objbtnAddQGroup
        '
        Me.objbtnAddQGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddQGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddQGroup.BorderSelected = False
        Me.objbtnAddQGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddQGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddQGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddQGroup.Location = New System.Drawing.Point(749, 33)
        Me.objbtnAddQGroup.Name = "objbtnAddQGroup"
        Me.objbtnAddQGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddQGroup.TabIndex = 226
        '
        'pnlQualification
        '
        Me.pnlQualification.Controls.Add(Me.lvQualification)
        Me.pnlQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlQualification.Location = New System.Drawing.Point(11, 133)
        Me.pnlQualification.Name = "pnlQualification"
        Me.pnlQualification.Size = New System.Drawing.Size(759, 334)
        Me.pnlQualification.TabIndex = 224
        '
        'lvQualification
        '
        Me.lvQualification.BackColorOnChecked = True
        Me.lvQualification.ColumnHeaders = Nothing
        Me.lvQualification.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhQualificationGrp, Me.colhQualification, Me.objQulificationUnkid, Me.objQGroupUnkid, Me.objcolhQJobUnkid, Me.objcolhQGUID})
        Me.lvQualification.CompulsoryColumns = ""
        Me.lvQualification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvQualification.FullRowSelect = True
        Me.lvQualification.GridLines = True
        Me.lvQualification.GroupingColumn = Nothing
        Me.lvQualification.HideSelection = False
        Me.lvQualification.Location = New System.Drawing.Point(0, 0)
        Me.lvQualification.MinColumnWidth = 50
        Me.lvQualification.MultiSelect = False
        Me.lvQualification.Name = "lvQualification"
        Me.lvQualification.OptionalColumns = ""
        Me.lvQualification.ShowMoreItem = False
        Me.lvQualification.ShowSaveItem = False
        Me.lvQualification.ShowSelectAll = True
        Me.lvQualification.ShowSizeAllColumnsToFit = True
        Me.lvQualification.Size = New System.Drawing.Size(759, 334)
        Me.lvQualification.Sortable = True
        Me.lvQualification.TabIndex = 224
        Me.lvQualification.UseCompatibleStateImageBehavior = False
        Me.lvQualification.View = System.Windows.Forms.View.Details
        '
        'colhQualificationGrp
        '
        Me.colhQualificationGrp.Tag = "colhQualificationGrp"
        Me.colhQualificationGrp.Text = "Qualification Group"
        Me.colhQualificationGrp.Width = 300
        '
        'colhQualification
        '
        Me.colhQualification.Tag = "colhQualification"
        Me.colhQualification.Text = "Qualification"
        Me.colhQualification.Width = 350
        '
        'objQulificationUnkid
        '
        Me.objQulificationUnkid.Tag = "objQulificationUnkid"
        Me.objQulificationUnkid.Text = ""
        Me.objQulificationUnkid.Width = 0
        '
        'objQGroupUnkid
        '
        Me.objQGroupUnkid.Tag = "objQGroupUnkid"
        Me.objQGroupUnkid.Text = ""
        Me.objQGroupUnkid.Width = 0
        '
        'objcolhQJobUnkid
        '
        Me.objcolhQJobUnkid.Tag = "objcolhQJobUnkid"
        Me.objcolhQJobUnkid.Text = ""
        Me.objcolhQJobUnkid.Width = 0
        '
        'objcolhQGUID
        '
        Me.objcolhQGUID.Tag = "objcolhQGUID"
        Me.objcolhQGUID.Text = ""
        Me.objcolhQGUID.Width = 0
        '
        'btnDeleteJobQualification
        '
        Me.btnDeleteJobQualification.BackColor = System.Drawing.Color.White
        Me.btnDeleteJobQualification.BackgroundImage = CType(resources.GetObject("btnDeleteJobQualification.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteJobQualification.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteJobQualification.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteJobQualification.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteJobQualification.FlatAppearance.BorderSize = 0
        Me.btnDeleteJobQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteJobQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteJobQualification.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteJobQualification.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteJobQualification.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteJobQualification.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteJobQualification.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteJobQualification.Location = New System.Drawing.Point(680, 97)
        Me.btnDeleteJobQualification.Name = "btnDeleteJobQualification"
        Me.btnDeleteJobQualification.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteJobQualification.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteJobQualification.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteJobQualification.TabIndex = 223
        Me.btnDeleteJobQualification.Text = "&Delete"
        Me.btnDeleteJobQualification.UseVisualStyleBackColor = True
        '
        'btnAddJobQualification
        '
        Me.btnAddJobQualification.BackColor = System.Drawing.Color.White
        Me.btnAddJobQualification.BackgroundImage = CType(resources.GetObject("btnAddJobQualification.BackgroundImage"), System.Drawing.Image)
        Me.btnAddJobQualification.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddJobQualification.BorderColor = System.Drawing.Color.Empty
        Me.btnAddJobQualification.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddJobQualification.FlatAppearance.BorderSize = 0
        Me.btnAddJobQualification.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddJobQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddJobQualification.ForeColor = System.Drawing.Color.Black
        Me.btnAddJobQualification.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddJobQualification.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddJobQualification.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddJobQualification.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddJobQualification.Location = New System.Drawing.Point(584, 97)
        Me.btnAddJobQualification.Name = "btnAddJobQualification"
        Me.btnAddJobQualification.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddJobQualification.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddJobQualification.Size = New System.Drawing.Size(90, 30)
        Me.btnAddJobQualification.TabIndex = 221
        Me.btnAddJobQualification.Text = "&Add"
        Me.btnAddJobQualification.UseVisualStyleBackColor = True
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(8, 63)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(103, 15)
        Me.lblQualification.TabIndex = 24
        Me.lblQualification.Text = "Qualification"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(117, 60)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(599, 21)
        Me.cboQualification.TabIndex = 23
        '
        'cboQualificationGrp
        '
        Me.cboQualificationGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGrp.FormattingEnabled = True
        Me.cboQualificationGrp.Location = New System.Drawing.Point(117, 33)
        Me.cboQualificationGrp.Name = "cboQualificationGrp"
        Me.cboQualificationGrp.Size = New System.Drawing.Size(599, 21)
        Me.cboQualificationGrp.TabIndex = 22
        '
        'lblQualificationGroup
        '
        Me.lblQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblQualificationGroup.Name = "lblQualificationGroup"
        Me.lblQualificationGroup.Size = New System.Drawing.Size(103, 15)
        Me.lblQualificationGroup.TabIndex = 20
        Me.lblQualificationGroup.Text = "Qualification Group"
        Me.lblQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine11
        '
        Me.objelLine11.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine11.Location = New System.Drawing.Point(11, 84)
        Me.objelLine11.Name = "objelLine11"
        Me.objelLine11.Size = New System.Drawing.Size(759, 10)
        Me.objelLine11.TabIndex = 19
        Me.objelLine11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tapbpKeyduties
        '
        Me.tapbpKeyduties.Controls.Add(Me.gbKeyDutiesResponsiblities)
        Me.tapbpKeyduties.Location = New System.Drawing.Point(4, 22)
        Me.tapbpKeyduties.Name = "tapbpKeyduties"
        Me.tapbpKeyduties.Padding = New System.Windows.Forms.Padding(3)
        Me.tapbpKeyduties.Size = New System.Drawing.Size(774, 486)
        Me.tapbpKeyduties.TabIndex = 4
        Me.tapbpKeyduties.Text = "Key Duties and Responsibilities"
        Me.tapbpKeyduties.UseVisualStyleBackColor = True
        '
        'gbKeyDutiesResponsiblities
        '
        Me.gbKeyDutiesResponsiblities.BorderColor = System.Drawing.Color.Black
        Me.gbKeyDutiesResponsiblities.Checked = False
        Me.gbKeyDutiesResponsiblities.CollapseAllExceptThis = False
        Me.gbKeyDutiesResponsiblities.CollapsedHoverImage = Nothing
        Me.gbKeyDutiesResponsiblities.CollapsedNormalImage = Nothing
        Me.gbKeyDutiesResponsiblities.CollapsedPressedImage = Nothing
        Me.gbKeyDutiesResponsiblities.CollapseOnLoad = False
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.btnKeyDutiesAdd)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.txtKeyDuties)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.btnKeyDutiesEdit)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.LblKeyDuties)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.pnlKeyDuties)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.btnKeyDutiesDelete)
        Me.gbKeyDutiesResponsiblities.Controls.Add(Me.objelLine2)
        Me.gbKeyDutiesResponsiblities.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbKeyDutiesResponsiblities.ExpandedHoverImage = Nothing
        Me.gbKeyDutiesResponsiblities.ExpandedNormalImage = Nothing
        Me.gbKeyDutiesResponsiblities.ExpandedPressedImage = Nothing
        Me.gbKeyDutiesResponsiblities.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbKeyDutiesResponsiblities.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbKeyDutiesResponsiblities.HeaderHeight = 25
        Me.gbKeyDutiesResponsiblities.HeaderMessage = ""
        Me.gbKeyDutiesResponsiblities.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbKeyDutiesResponsiblities.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbKeyDutiesResponsiblities.HeightOnCollapse = 0
        Me.gbKeyDutiesResponsiblities.LeftTextSpace = 0
        Me.gbKeyDutiesResponsiblities.Location = New System.Drawing.Point(3, 3)
        Me.gbKeyDutiesResponsiblities.Name = "gbKeyDutiesResponsiblities"
        Me.gbKeyDutiesResponsiblities.OpenHeight = 300
        Me.gbKeyDutiesResponsiblities.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbKeyDutiesResponsiblities.ShowBorder = True
        Me.gbKeyDutiesResponsiblities.ShowCheckBox = False
        Me.gbKeyDutiesResponsiblities.ShowCollapseButton = False
        Me.gbKeyDutiesResponsiblities.ShowDefaultBorderColor = True
        Me.gbKeyDutiesResponsiblities.ShowDownButton = False
        Me.gbKeyDutiesResponsiblities.ShowHeader = True
        Me.gbKeyDutiesResponsiblities.Size = New System.Drawing.Size(768, 480)
        Me.gbKeyDutiesResponsiblities.TabIndex = 0
        Me.gbKeyDutiesResponsiblities.Temp = 0
        Me.gbKeyDutiesResponsiblities.Text = "Key Duties and Responsibilities"
        Me.gbKeyDutiesResponsiblities.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnKeyDutiesAdd
        '
        Me.btnKeyDutiesAdd.BackColor = System.Drawing.Color.White
        Me.btnKeyDutiesAdd.BackgroundImage = CType(resources.GetObject("btnKeyDutiesAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnKeyDutiesAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnKeyDutiesAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnKeyDutiesAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnKeyDutiesAdd.FlatAppearance.BorderSize = 0
        Me.btnKeyDutiesAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnKeyDutiesAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeyDutiesAdd.ForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnKeyDutiesAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesAdd.Location = New System.Drawing.Point(581, 97)
        Me.btnKeyDutiesAdd.Name = "btnKeyDutiesAdd"
        Me.btnKeyDutiesAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesAdd.Size = New System.Drawing.Size(90, 30)
        Me.btnKeyDutiesAdd.TabIndex = 3
        Me.btnKeyDutiesAdd.Text = "Add"
        Me.btnKeyDutiesAdd.UseVisualStyleBackColor = True
        '
        'txtKeyDuties
        '
        Me.txtKeyDuties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeyDuties.Location = New System.Drawing.Point(178, 33)
        Me.txtKeyDuties.Multiline = True
        Me.txtKeyDuties.Name = "txtKeyDuties"
        Me.txtKeyDuties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtKeyDuties.Size = New System.Drawing.Size(589, 50)
        Me.txtKeyDuties.TabIndex = 1
        '
        'btnKeyDutiesEdit
        '
        Me.btnKeyDutiesEdit.BackColor = System.Drawing.Color.White
        Me.btnKeyDutiesEdit.BackgroundImage = CType(resources.GetObject("btnKeyDutiesEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnKeyDutiesEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnKeyDutiesEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnKeyDutiesEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnKeyDutiesEdit.FlatAppearance.BorderSize = 0
        Me.btnKeyDutiesEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnKeyDutiesEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeyDutiesEdit.ForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnKeyDutiesEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesEdit.Location = New System.Drawing.Point(581, 97)
        Me.btnKeyDutiesEdit.Name = "btnKeyDutiesEdit"
        Me.btnKeyDutiesEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnKeyDutiesEdit.TabIndex = 235
        Me.btnKeyDutiesEdit.Text = "Edit"
        Me.btnKeyDutiesEdit.UseVisualStyleBackColor = True
        '
        'LblKeyDuties
        '
        Me.LblKeyDuties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKeyDuties.Location = New System.Drawing.Point(8, 36)
        Me.LblKeyDuties.Name = "LblKeyDuties"
        Me.LblKeyDuties.Size = New System.Drawing.Size(170, 19)
        Me.LblKeyDuties.TabIndex = 0
        Me.LblKeyDuties.Text = "Key Duties and Responsibilities"
        Me.LblKeyDuties.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlKeyDuties
        '
        Me.pnlKeyDuties.Controls.Add(Me.dgJobKeyDuties)
        Me.pnlKeyDuties.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlKeyDuties.Location = New System.Drawing.Point(11, 133)
        Me.pnlKeyDuties.Name = "pnlKeyDuties"
        Me.pnlKeyDuties.Size = New System.Drawing.Size(756, 334)
        Me.pnlKeyDuties.TabIndex = 224
        '
        'dgJobKeyDuties
        '
        Me.dgJobKeyDuties.AllowUserToAddRows = False
        Me.dgJobKeyDuties.AllowUserToDeleteRows = False
        Me.dgJobKeyDuties.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgJobKeyDuties.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgJobKeyDuties.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhKeyDutiesEdit, Me.dgcolhKeyDuties, Me.objdgcolhKeyDutiesGUID, Me.objdgcolhJobKeydutiestranunkid})
        Me.dgJobKeyDuties.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgJobKeyDuties.Location = New System.Drawing.Point(0, 0)
        Me.dgJobKeyDuties.Name = "dgJobKeyDuties"
        Me.dgJobKeyDuties.ReadOnly = True
        Me.dgJobKeyDuties.RowHeadersVisible = False
        Me.dgJobKeyDuties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgJobKeyDuties.Size = New System.Drawing.Size(756, 334)
        Me.dgJobKeyDuties.TabIndex = 0
        '
        'objdgcolhKeyDutiesEdit
        '
        Me.objdgcolhKeyDutiesEdit.HeaderText = ""
        Me.objdgcolhKeyDutiesEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhKeyDutiesEdit.Name = "objdgcolhKeyDutiesEdit"
        Me.objdgcolhKeyDutiesEdit.ReadOnly = True
        Me.objdgcolhKeyDutiesEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhKeyDutiesEdit.Width = 25
        '
        'dgcolhKeyDuties
        '
        Me.dgcolhKeyDuties.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhKeyDuties.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhKeyDuties.HeaderText = "Key Duties and Responsibilities"
        Me.dgcolhKeyDuties.Name = "dgcolhKeyDuties"
        Me.dgcolhKeyDuties.ReadOnly = True
        Me.dgcolhKeyDuties.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhKeyDutiesGUID
        '
        Me.objdgcolhKeyDutiesGUID.HeaderText = "GUID"
        Me.objdgcolhKeyDutiesGUID.Name = "objdgcolhKeyDutiesGUID"
        Me.objdgcolhKeyDutiesGUID.ReadOnly = True
        Me.objdgcolhKeyDutiesGUID.Visible = False
        '
        'objdgcolhJobKeydutiestranunkid
        '
        Me.objdgcolhJobKeydutiestranunkid.HeaderText = "JobKeydutiestranunkid"
        Me.objdgcolhJobKeydutiestranunkid.Name = "objdgcolhJobKeydutiestranunkid"
        Me.objdgcolhJobKeydutiestranunkid.ReadOnly = True
        Me.objdgcolhJobKeydutiestranunkid.Visible = False
        '
        'btnKeyDutiesDelete
        '
        Me.btnKeyDutiesDelete.BackColor = System.Drawing.Color.White
        Me.btnKeyDutiesDelete.BackgroundImage = CType(resources.GetObject("btnKeyDutiesDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnKeyDutiesDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnKeyDutiesDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnKeyDutiesDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnKeyDutiesDelete.FlatAppearance.BorderSize = 0
        Me.btnKeyDutiesDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnKeyDutiesDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKeyDutiesDelete.ForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnKeyDutiesDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesDelete.Location = New System.Drawing.Point(677, 97)
        Me.btnKeyDutiesDelete.Name = "btnKeyDutiesDelete"
        Me.btnKeyDutiesDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnKeyDutiesDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnKeyDutiesDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnKeyDutiesDelete.TabIndex = 4
        Me.btnKeyDutiesDelete.Text = "Delete"
        Me.btnKeyDutiesDelete.UseVisualStyleBackColor = True
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(11, 84)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(756, 10)
        Me.objelLine2.TabIndex = 2
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tapbpCompetencies
        '
        Me.tapbpCompetencies.Controls.Add(Me.gbCompetencies)
        Me.tapbpCompetencies.Location = New System.Drawing.Point(4, 22)
        Me.tapbpCompetencies.Name = "tapbpCompetencies"
        Me.tapbpCompetencies.Padding = New System.Windows.Forms.Padding(3)
        Me.tapbpCompetencies.Size = New System.Drawing.Size(774, 486)
        Me.tapbpCompetencies.TabIndex = 5
        Me.tapbpCompetencies.Text = "Competencies"
        Me.tapbpCompetencies.UseVisualStyleBackColor = True
        '
        'gbCompetencies
        '
        Me.gbCompetencies.BorderColor = System.Drawing.Color.Black
        Me.gbCompetencies.Checked = False
        Me.gbCompetencies.CollapseAllExceptThis = False
        Me.gbCompetencies.CollapsedHoverImage = Nothing
        Me.gbCompetencies.CollapsedNormalImage = Nothing
        Me.gbCompetencies.CollapsedPressedImage = Nothing
        Me.gbCompetencies.CollapseOnLoad = False
        Me.gbCompetencies.Controls.Add(Me.objBtnSearchCompetencies)
        Me.gbCompetencies.Controls.Add(Me.objbtnSeachCompetenciesCategory)
        Me.gbCompetencies.Controls.Add(Me.pnlCompetencies)
        Me.gbCompetencies.Controls.Add(Me.btnDeleteCompetencies)
        Me.gbCompetencies.Controls.Add(Me.btnAddCompetencies)
        Me.gbCompetencies.Controls.Add(Me.LblCompetenciesItem)
        Me.gbCompetencies.Controls.Add(Me.cboCompetencies)
        Me.gbCompetencies.Controls.Add(Me.cboCompetenciesCategory)
        Me.gbCompetencies.Controls.Add(Me.LblCompetenciesCategory)
        Me.gbCompetencies.Controls.Add(Me.objelLine3)
        Me.gbCompetencies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCompetencies.ExpandedHoverImage = Nothing
        Me.gbCompetencies.ExpandedNormalImage = Nothing
        Me.gbCompetencies.ExpandedPressedImage = Nothing
        Me.gbCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCompetencies.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCompetencies.HeaderHeight = 25
        Me.gbCompetencies.HeaderMessage = ""
        Me.gbCompetencies.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCompetencies.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCompetencies.HeightOnCollapse = 0
        Me.gbCompetencies.LeftTextSpace = 0
        Me.gbCompetencies.Location = New System.Drawing.Point(3, 3)
        Me.gbCompetencies.Name = "gbCompetencies"
        Me.gbCompetencies.OpenHeight = 300
        Me.gbCompetencies.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCompetencies.ShowBorder = True
        Me.gbCompetencies.ShowCheckBox = False
        Me.gbCompetencies.ShowCollapseButton = False
        Me.gbCompetencies.ShowDefaultBorderColor = True
        Me.gbCompetencies.ShowDownButton = False
        Me.gbCompetencies.ShowHeader = True
        Me.gbCompetencies.Size = New System.Drawing.Size(768, 480)
        Me.gbCompetencies.TabIndex = 0
        Me.gbCompetencies.Temp = 0
        Me.gbCompetencies.Text = "Competencies"
        Me.gbCompetencies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objBtnSearchCompetencies
        '
        Me.objBtnSearchCompetencies.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchCompetencies.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchCompetencies.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchCompetencies.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchCompetencies.BorderSelected = False
        Me.objBtnSearchCompetencies.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchCompetencies.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnSearchCompetencies.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchCompetencies.Location = New System.Drawing.Point(738, 60)
        Me.objBtnSearchCompetencies.Name = "objBtnSearchCompetencies"
        Me.objBtnSearchCompetencies.Size = New System.Drawing.Size(21, 21)
        Me.objBtnSearchCompetencies.TabIndex = 5
        '
        'objbtnSeachCompetenciesCategory
        '
        Me.objbtnSeachCompetenciesCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSeachCompetenciesCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSeachCompetenciesCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSeachCompetenciesCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSeachCompetenciesCategory.BorderSelected = False
        Me.objbtnSeachCompetenciesCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSeachCompetenciesCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSeachCompetenciesCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSeachCompetenciesCategory.Location = New System.Drawing.Point(738, 33)
        Me.objbtnSeachCompetenciesCategory.Name = "objbtnSeachCompetenciesCategory"
        Me.objbtnSeachCompetenciesCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSeachCompetenciesCategory.TabIndex = 2
        '
        'pnlCompetencies
        '
        Me.pnlCompetencies.Controls.Add(Me.lvCompetencies)
        Me.pnlCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCompetencies.Location = New System.Drawing.Point(11, 133)
        Me.pnlCompetencies.Name = "pnlCompetencies"
        Me.pnlCompetencies.Size = New System.Drawing.Size(748, 334)
        Me.pnlCompetencies.TabIndex = 224
        '
        'lvCompetencies
        '
        Me.lvCompetencies.BackColorOnChecked = True
        Me.lvCompetencies.ColumnHeaders = Nothing
        Me.lvCompetencies.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCompetenciesGroup, Me.colhCompetenciesItem, Me.objcolhCompetencyGUID})
        Me.lvCompetencies.CompulsoryColumns = ""
        Me.lvCompetencies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvCompetencies.FullRowSelect = True
        Me.lvCompetencies.GridLines = True
        Me.lvCompetencies.GroupingColumn = Nothing
        Me.lvCompetencies.HideSelection = False
        Me.lvCompetencies.Location = New System.Drawing.Point(0, 0)
        Me.lvCompetencies.MinColumnWidth = 50
        Me.lvCompetencies.MultiSelect = False
        Me.lvCompetencies.Name = "lvCompetencies"
        Me.lvCompetencies.OptionalColumns = ""
        Me.lvCompetencies.ShowMoreItem = False
        Me.lvCompetencies.ShowSaveItem = False
        Me.lvCompetencies.ShowSelectAll = True
        Me.lvCompetencies.ShowSizeAllColumnsToFit = True
        Me.lvCompetencies.Size = New System.Drawing.Size(748, 334)
        Me.lvCompetencies.Sortable = True
        Me.lvCompetencies.TabIndex = 0
        Me.lvCompetencies.UseCompatibleStateImageBehavior = False
        Me.lvCompetencies.View = System.Windows.Forms.View.Details
        '
        'colhCompetenciesGroup
        '
        Me.colhCompetenciesGroup.Tag = "colhCompetenciesGroup"
        Me.colhCompetenciesGroup.Text = "Competencies Group"
        Me.colhCompetenciesGroup.Width = 0
        '
        'colhCompetenciesItem
        '
        Me.colhCompetenciesItem.Tag = "colhCompetenciesItem"
        Me.colhCompetenciesItem.Text = "Competencies Items"
        Me.colhCompetenciesItem.Width = 522
        '
        'objcolhCompetencyGUID
        '
        Me.objcolhCompetencyGUID.Tag = "objcolhCompetencyGUID"
        Me.objcolhCompetencyGUID.Text = "CompetencyGUID"
        Me.objcolhCompetencyGUID.Width = 0
        '
        'btnDeleteCompetencies
        '
        Me.btnDeleteCompetencies.BackColor = System.Drawing.Color.White
        Me.btnDeleteCompetencies.BackgroundImage = CType(resources.GetObject("btnDeleteCompetencies.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteCompetencies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteCompetencies.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteCompetencies.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteCompetencies.FlatAppearance.BorderSize = 0
        Me.btnDeleteCompetencies.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteCompetencies.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteCompetencies.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteCompetencies.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteCompetencies.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteCompetencies.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteCompetencies.Location = New System.Drawing.Point(669, 97)
        Me.btnDeleteCompetencies.Name = "btnDeleteCompetencies"
        Me.btnDeleteCompetencies.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteCompetencies.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteCompetencies.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteCompetencies.TabIndex = 7
        Me.btnDeleteCompetencies.Text = "&Delete"
        Me.btnDeleteCompetencies.UseVisualStyleBackColor = True
        '
        'btnAddCompetencies
        '
        Me.btnAddCompetencies.BackColor = System.Drawing.Color.White
        Me.btnAddCompetencies.BackgroundImage = CType(resources.GetObject("btnAddCompetencies.BackgroundImage"), System.Drawing.Image)
        Me.btnAddCompetencies.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddCompetencies.BorderColor = System.Drawing.Color.Empty
        Me.btnAddCompetencies.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddCompetencies.FlatAppearance.BorderSize = 0
        Me.btnAddCompetencies.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddCompetencies.ForeColor = System.Drawing.Color.Black
        Me.btnAddCompetencies.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddCompetencies.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddCompetencies.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddCompetencies.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddCompetencies.Location = New System.Drawing.Point(573, 97)
        Me.btnAddCompetencies.Name = "btnAddCompetencies"
        Me.btnAddCompetencies.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddCompetencies.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddCompetencies.Size = New System.Drawing.Size(90, 30)
        Me.btnAddCompetencies.TabIndex = 6
        Me.btnAddCompetencies.Text = "&Add"
        Me.btnAddCompetencies.UseVisualStyleBackColor = True
        '
        'LblCompetenciesItem
        '
        Me.LblCompetenciesItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCompetenciesItem.Location = New System.Drawing.Point(8, 63)
        Me.LblCompetenciesItem.Name = "LblCompetenciesItem"
        Me.LblCompetenciesItem.Size = New System.Drawing.Size(135, 15)
        Me.LblCompetenciesItem.TabIndex = 3
        Me.LblCompetenciesItem.Text = "Competencies Item"
        Me.LblCompetenciesItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCompetencies
        '
        Me.cboCompetencies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompetencies.DropDownWidth = 500
        Me.cboCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompetencies.FormattingEnabled = True
        Me.cboCompetencies.Location = New System.Drawing.Point(154, 60)
        Me.cboCompetencies.Name = "cboCompetencies"
        Me.cboCompetencies.Size = New System.Drawing.Size(578, 21)
        Me.cboCompetencies.TabIndex = 4
        '
        'cboCompetenciesCategory
        '
        Me.cboCompetenciesCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompetenciesCategory.DropDownWidth = 500
        Me.cboCompetenciesCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompetenciesCategory.FormattingEnabled = True
        Me.cboCompetenciesCategory.Location = New System.Drawing.Point(154, 33)
        Me.cboCompetenciesCategory.Name = "cboCompetenciesCategory"
        Me.cboCompetenciesCategory.Size = New System.Drawing.Size(578, 21)
        Me.cboCompetenciesCategory.TabIndex = 1
        '
        'LblCompetenciesCategory
        '
        Me.LblCompetenciesCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCompetenciesCategory.Location = New System.Drawing.Point(8, 36)
        Me.LblCompetenciesCategory.Name = "LblCompetenciesCategory"
        Me.LblCompetenciesCategory.Size = New System.Drawing.Size(135, 15)
        Me.LblCompetenciesCategory.TabIndex = 0
        Me.LblCompetenciesCategory.Text = "Competencies Category"
        Me.LblCompetenciesCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(11, 84)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(748, 10)
        Me.objelLine3.TabIndex = 19
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tapbpJobDescription
        '
        Me.tapbpJobDescription.Controls.Add(Me.pnlDescription)
        Me.tapbpJobDescription.Location = New System.Drawing.Point(4, 22)
        Me.tapbpJobDescription.Name = "tapbpJobDescription"
        Me.tapbpJobDescription.Size = New System.Drawing.Size(774, 486)
        Me.tapbpJobDescription.TabIndex = 2
        Me.tapbpJobDescription.Text = "Job Description"
        Me.tapbpJobDescription.UseVisualStyleBackColor = True
        '
        'pnlDescription
        '
        Me.pnlDescription.Controls.Add(Me.txtJobDescription)
        Me.pnlDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlDescription.Location = New System.Drawing.Point(0, 0)
        Me.pnlDescription.Name = "pnlDescription"
        Me.pnlDescription.Size = New System.Drawing.Size(774, 486)
        Me.pnlDescription.TabIndex = 107
        '
        'txtJobDescription
        '
        Me.txtJobDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtJobDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtJobDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtJobDescription.Name = "txtJobDescription"
        Me.txtJobDescription.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
        Me.txtJobDescription.Size = New System.Drawing.Size(774, 486)
        Me.txtJobDescription.TabIndex = 0
        Me.txtJobDescription.Text = ""
        '
        'tabpJobLanguage
        '
        Me.tabpJobLanguage.Controls.Add(Me.gbJobLanguage)
        Me.tabpJobLanguage.Location = New System.Drawing.Point(4, 22)
        Me.tabpJobLanguage.Name = "tabpJobLanguage"
        Me.tabpJobLanguage.Size = New System.Drawing.Size(774, 486)
        Me.tabpJobLanguage.TabIndex = 6
        Me.tabpJobLanguage.Text = "Job Language"
        Me.tabpJobLanguage.UseVisualStyleBackColor = True
        '
        'gbJobLanguage
        '
        Me.gbJobLanguage.BorderColor = System.Drawing.Color.Black
        Me.gbJobLanguage.Checked = False
        Me.gbJobLanguage.CollapseAllExceptThis = False
        Me.gbJobLanguage.CollapsedHoverImage = Nothing
        Me.gbJobLanguage.CollapsedNormalImage = Nothing
        Me.gbJobLanguage.CollapsedPressedImage = Nothing
        Me.gbJobLanguage.CollapseOnLoad = False
        Me.gbJobLanguage.Controls.Add(Me.objbtnSearchLanguage)
        Me.gbJobLanguage.Controls.Add(Me.objbtnAddLanguage)
        Me.gbJobLanguage.Controls.Add(Me.pnlJobLAnguage)
        Me.gbJobLanguage.Controls.Add(Me.btnDeleteLanguage)
        Me.gbJobLanguage.Controls.Add(Me.btnAddLanguage)
        Me.gbJobLanguage.Controls.Add(Me.cboLanguage)
        Me.gbJobLanguage.Controls.Add(Me.lblLanguage)
        Me.gbJobLanguage.Controls.Add(Me.objelLine4)
        Me.gbJobLanguage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobLanguage.ExpandedHoverImage = Nothing
        Me.gbJobLanguage.ExpandedNormalImage = Nothing
        Me.gbJobLanguage.ExpandedPressedImage = Nothing
        Me.gbJobLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobLanguage.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobLanguage.HeaderHeight = 25
        Me.gbJobLanguage.HeaderMessage = ""
        Me.gbJobLanguage.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobLanguage.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobLanguage.HeightOnCollapse = 0
        Me.gbJobLanguage.LeftTextSpace = 0
        Me.gbJobLanguage.Location = New System.Drawing.Point(0, 0)
        Me.gbJobLanguage.Name = "gbJobLanguage"
        Me.gbJobLanguage.OpenHeight = 300
        Me.gbJobLanguage.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobLanguage.ShowBorder = True
        Me.gbJobLanguage.ShowCheckBox = False
        Me.gbJobLanguage.ShowCollapseButton = False
        Me.gbJobLanguage.ShowDefaultBorderColor = True
        Me.gbJobLanguage.ShowDownButton = False
        Me.gbJobLanguage.ShowHeader = True
        Me.gbJobLanguage.Size = New System.Drawing.Size(774, 486)
        Me.gbJobLanguage.TabIndex = 21
        Me.gbJobLanguage.Temp = 0
        Me.gbJobLanguage.Text = "Job Languages"
        Me.gbJobLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchLanguage
        '
        Me.objbtnSearchLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLanguage.BorderSelected = False
        Me.objbtnSearchLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLanguage.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLanguage.Location = New System.Drawing.Point(722, 33)
        Me.objbtnSearchLanguage.Name = "objbtnSearchLanguage"
        Me.objbtnSearchLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLanguage.TabIndex = 229
        '
        'objbtnAddLanguage
        '
        Me.objbtnAddLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLanguage.BorderSelected = False
        Me.objbtnAddLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLanguage.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLanguage.Location = New System.Drawing.Point(749, 33)
        Me.objbtnAddLanguage.Name = "objbtnAddLanguage"
        Me.objbtnAddLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLanguage.TabIndex = 226
        '
        'pnlJobLAnguage
        '
        Me.pnlJobLAnguage.Controls.Add(Me.lvJobLanguage)
        Me.pnlJobLAnguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlJobLAnguage.Location = New System.Drawing.Point(11, 109)
        Me.pnlJobLAnguage.Name = "pnlJobLAnguage"
        Me.pnlJobLAnguage.Size = New System.Drawing.Size(759, 374)
        Me.pnlJobLAnguage.TabIndex = 224
        '
        'lvJobLanguage
        '
        Me.lvJobLanguage.BackColorOnChecked = True
        Me.lvJobLanguage.ColumnHeaders = Nothing
        Me.lvJobLanguage.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLanguage, Me.objLanguageUnkid, Me.objcolhLangJobUnkid, Me.objcolhLangGUID})
        Me.lvJobLanguage.CompulsoryColumns = ""
        Me.lvJobLanguage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvJobLanguage.FullRowSelect = True
        Me.lvJobLanguage.GridLines = True
        Me.lvJobLanguage.GroupingColumn = Nothing
        Me.lvJobLanguage.HideSelection = False
        Me.lvJobLanguage.Location = New System.Drawing.Point(0, 0)
        Me.lvJobLanguage.MinColumnWidth = 50
        Me.lvJobLanguage.MultiSelect = False
        Me.lvJobLanguage.Name = "lvJobLanguage"
        Me.lvJobLanguage.OptionalColumns = ""
        Me.lvJobLanguage.ShowMoreItem = False
        Me.lvJobLanguage.ShowSaveItem = False
        Me.lvJobLanguage.ShowSelectAll = True
        Me.lvJobLanguage.ShowSizeAllColumnsToFit = True
        Me.lvJobLanguage.Size = New System.Drawing.Size(759, 374)
        Me.lvJobLanguage.Sortable = True
        Me.lvJobLanguage.TabIndex = 224
        Me.lvJobLanguage.UseCompatibleStateImageBehavior = False
        Me.lvJobLanguage.View = System.Windows.Forms.View.Details
        '
        'colhLanguage
        '
        Me.colhLanguage.Tag = "colhLanguage"
        Me.colhLanguage.Text = "Language"
        Me.colhLanguage.Width = 730
        '
        'objLanguageUnkid
        '
        Me.objLanguageUnkid.Tag = "objLanguageUnkid"
        Me.objLanguageUnkid.Text = ""
        Me.objLanguageUnkid.Width = 0
        '
        'objcolhLangJobUnkid
        '
        Me.objcolhLangJobUnkid.Tag = "objcolhLangJobUnkid"
        Me.objcolhLangJobUnkid.Text = ""
        Me.objcolhLangJobUnkid.Width = 0
        '
        'objcolhLangGUID
        '
        Me.objcolhLangGUID.Tag = "objcolhLangGUID"
        Me.objcolhLangGUID.Text = ""
        Me.objcolhLangGUID.Width = 0
        '
        'btnDeleteLanguage
        '
        Me.btnDeleteLanguage.BackColor = System.Drawing.Color.White
        Me.btnDeleteLanguage.BackgroundImage = CType(resources.GetObject("btnDeleteLanguage.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteLanguage.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteLanguage.FlatAppearance.BorderSize = 0
        Me.btnDeleteLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteLanguage.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteLanguage.Location = New System.Drawing.Point(676, 73)
        Me.btnDeleteLanguage.Name = "btnDeleteLanguage"
        Me.btnDeleteLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteLanguage.Size = New System.Drawing.Size(90, 30)
        Me.btnDeleteLanguage.TabIndex = 223
        Me.btnDeleteLanguage.Text = "&Delete"
        Me.btnDeleteLanguage.UseVisualStyleBackColor = True
        '
        'btnAddLanguage
        '
        Me.btnAddLanguage.BackColor = System.Drawing.Color.White
        Me.btnAddLanguage.BackgroundImage = CType(resources.GetObject("btnAddLanguage.BackgroundImage"), System.Drawing.Image)
        Me.btnAddLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddLanguage.BorderColor = System.Drawing.Color.Empty
        Me.btnAddLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddLanguage.FlatAppearance.BorderSize = 0
        Me.btnAddLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddLanguage.ForeColor = System.Drawing.Color.Black
        Me.btnAddLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddLanguage.Location = New System.Drawing.Point(580, 73)
        Me.btnAddLanguage.Name = "btnAddLanguage"
        Me.btnAddLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddLanguage.Size = New System.Drawing.Size(90, 30)
        Me.btnAddLanguage.TabIndex = 221
        Me.btnAddLanguage.Text = "&Add"
        Me.btnAddLanguage.UseVisualStyleBackColor = True
        '
        'cboLanguage
        '
        Me.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage.FormattingEnabled = True
        Me.cboLanguage.Location = New System.Drawing.Point(117, 33)
        Me.cboLanguage.Name = "cboLanguage"
        Me.cboLanguage.Size = New System.Drawing.Size(599, 21)
        Me.cboLanguage.TabIndex = 22
        '
        'lblLanguage
        '
        Me.lblLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage.Location = New System.Drawing.Point(8, 36)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(78, 15)
        Me.lblLanguage.TabIndex = 20
        Me.lblLanguage.Text = "Language"
        Me.lblLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine4
        '
        Me.objelLine4.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine4.Location = New System.Drawing.Point(11, 60)
        Me.objelLine4.Name = "objelLine4"
        Me.objelLine4.Size = New System.Drawing.Size(759, 10)
        Me.objelLine4.TabIndex = 19
        Me.objelLine4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJobCode
        '
        Me.objbtnSearchJobCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobCode.BorderSelected = False
        Me.objbtnSearchJobCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobCode.Location = New System.Drawing.Point(262, 259)
        Me.objbtnSearchJobCode.Name = "objbtnSearchJobCode"
        Me.objbtnSearchJobCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobCode.TabIndex = 165
        Me.objbtnSearchJobCode.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.ChkGradeLevel)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.pnlGradresLevel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 512)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(782, 55)
        Me.objFooter.TabIndex = 9
        '
        'ChkGradeLevel
        '
        Me.ChkGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkGradeLevel.Location = New System.Drawing.Point(21, 22)
        Me.ChkGradeLevel.Name = "ChkGradeLevel"
        Me.ChkGradeLevel.Size = New System.Drawing.Size(98, 17)
        Me.ChkGradeLevel.TabIndex = 216
        Me.ChkGradeLevel.Text = "Grade Level"
        Me.ChkGradeLevel.UseVisualStyleBackColor = True
        Me.ChkGradeLevel.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(570, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 26
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(673, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 27
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlGradresLevel
        '
        Me.pnlGradresLevel.Controls.Add(Me.objbtnSearchGradesLevel)
        Me.pnlGradresLevel.Controls.Add(Me.cboGradeLevel)
        Me.pnlGradresLevel.Location = New System.Drawing.Point(143, 18)
        Me.pnlGradresLevel.Name = "pnlGradresLevel"
        Me.pnlGradresLevel.Size = New System.Drawing.Size(255, 25)
        Me.pnlGradresLevel.TabIndex = 217
        Me.pnlGradresLevel.Visible = False
        '
        'objbtnSearchGradesLevel
        '
        Me.objbtnSearchGradesLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradesLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradesLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradesLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradesLevel.BorderSelected = False
        Me.objbtnSearchGradesLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradesLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradesLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradesLevel.Location = New System.Drawing.Point(200, 2)
        Me.objbtnSearchGradesLevel.Name = "objbtnSearchGradesLevel"
        Me.objbtnSearchGradesLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradesLevel.TabIndex = 192
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(1, 2)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(192, 21)
        Me.cboGradeLevel.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn1.HeaderText = "Key Duties and Responsibilities"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "GUID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "JobKeydutiestranunkid"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmJobs_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 567)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobs_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Job"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcJobInfo.ResumeLayout(False)
        Me.tabpJobInfo.ResumeLayout(False)
        Me.gbJobsInfo.ResumeLayout(False)
        Me.gbJobsInfo.PerformLayout()
        CType(Me.nudCritical, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSectionGrp.ResumeLayout(False)
        Me.pnlClass.ResumeLayout(False)
        Me.pnlBranch.ResumeLayout(False)
        Me.pnlUnitGrp.ResumeLayout(False)
        Me.tabRemarks.ResumeLayout(False)
        Me.tabpgExperienceComment.ResumeLayout(False)
        Me.tabpgExperienceComment.PerformLayout()
        Me.tabpgWorkingHrs.ResumeLayout(False)
        Me.tabpgWorkingHrs.PerformLayout()
        Me.pnlDeprtmentGrp.ResumeLayout(False)
        CType(Me.nudExperienceMonth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDepartment.ResumeLayout(False)
        CType(Me.nudExperienceYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlClassGroup.ResumeLayout(False)
        Me.pnlJobGrp.ResumeLayout(False)
        Me.pnlGrades.ResumeLayout(False)
        Me.pnlTeam.ResumeLayout(False)
        Me.pnlSection.ResumeLayout(False)
        Me.pnlUnit.ResumeLayout(False)
        CType(Me.nudJobLevel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpJobSkills.ResumeLayout(False)
        Me.gbJobSkill.ResumeLayout(False)
        Me.pnlJobSkill.ResumeLayout(False)
        Me.tabpQualification.ResumeLayout(False)
        Me.gbJobQulification.ResumeLayout(False)
        Me.pnlQualification.ResumeLayout(False)
        Me.tapbpKeyduties.ResumeLayout(False)
        Me.gbKeyDutiesResponsiblities.ResumeLayout(False)
        Me.gbKeyDutiesResponsiblities.PerformLayout()
        Me.pnlKeyDuties.ResumeLayout(False)
        CType(Me.dgJobKeyDuties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tapbpCompetencies.ResumeLayout(False)
        Me.gbCompetencies.ResumeLayout(False)
        Me.pnlCompetencies.ResumeLayout(False)
        Me.tapbpJobDescription.ResumeLayout(False)
        Me.pnlDescription.ResumeLayout(False)
        Me.tabpJobLanguage.ResumeLayout(False)
        Me.gbJobLanguage.ResumeLayout(False)
        Me.pnlJobLAnguage.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.pnlGradresLevel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboReportTo As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportTo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJobCode As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcJobInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpJobInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbJobsInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlGrades As System.Windows.Forms.Panel
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents chkTeam As System.Windows.Forms.CheckBox
    Friend WithEvents pnlTeam As System.Windows.Forms.Panel
    Friend WithEvents cboTeam As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents chkUnit As System.Windows.Forms.CheckBox
    Friend WithEvents pnlUnit As System.Windows.Forms.Panel
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUnit As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddUnit As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlSection As System.Windows.Forms.Panel
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddSection As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlJobGrp As System.Windows.Forms.Panel
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchJGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddJobGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents elLine2 As eZee.Common.eZeeLine
    Friend WithEvents elLine1 As eZee.Common.eZeeLine
    Friend WithEvents chkGrades As System.Windows.Forms.CheckBox
    Friend WithEvents chkSection As System.Windows.Forms.CheckBox
    Friend WithEvents chkJobGroup As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddReminder As eZee.Common.eZeeGradientButton
    Friend WithEvents lblJobLevel As System.Windows.Forms.Label
    Friend WithEvents nudJobLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblTerminateDate As System.Windows.Forms.Label
    Friend WithEvents nudPosition As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPosition As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCreateDate As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpCreateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTerminateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents tabpJobSkills As System.Windows.Forms.TabPage
    Friend WithEvents gbJobSkill As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchSkill As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddSkill As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddSkillCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlJobSkill As System.Windows.Forms.Panel
    Friend WithEvents lvJobSkill As eZee.Common.eZeeListView
    Friend WithEvents colhCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSkill As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSkillUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSkillCatUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJobUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lblSkills As System.Windows.Forms.Label
    Friend WithEvents cboSkillSets As System.Windows.Forms.ComboBox
    Friend WithEvents cboSkillCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillCategory As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents tabpQualification As System.Windows.Forms.TabPage
    Friend WithEvents gbJobQulification As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddQGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlQualification As System.Windows.Forms.Panel
    Friend WithEvents lvQualification As eZee.Common.eZeeListView
    Friend WithEvents colhQualificationGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualification As System.Windows.Forms.ColumnHeader
    Friend WithEvents objQulificationUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objQGroupUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhQJobUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhQGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDeleteJobQualification As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddJobQualification As eZee.Common.eZeeLightButton
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
    Friend WithEvents cboQualificationGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents objelLine11 As eZee.Common.eZeeLine
    Friend WithEvents tapbpJobDescription As System.Windows.Forms.TabPage
    Friend WithEvents pnlDescription As System.Windows.Forms.Panel
    Friend WithEvents txtJobDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents objbtnSearchDirectReportTo As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchInDirectReportTo As eZee.Common.eZeeGradientButton
    Friend WithEvents LblInDirectReportTo As System.Windows.Forms.Label
    Friend WithEvents cboInDirectReportTo As System.Windows.Forms.ComboBox
    Friend WithEvents LblExperienceInfo As System.Windows.Forms.Label
    Friend WithEvents txtExperienceRemark As System.Windows.Forms.TextBox
    Friend WithEvents nudExperienceMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblExperienceMonth As System.Windows.Forms.Label
    Friend WithEvents nudExperienceYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtWorkingHrs As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlDepartment As System.Windows.Forms.Panel
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents chkClassGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkDepartment As System.Windows.Forms.CheckBox
    Friend WithEvents pnlClassGroup As System.Windows.Forms.Panel
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchClassGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddClassGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents tapbpKeyduties As System.Windows.Forms.TabPage
    Friend WithEvents gbKeyDutiesResponsiblities As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlKeyDuties As System.Windows.Forms.Panel
    Friend WithEvents btnKeyDutiesDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnKeyDutiesAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents tapbpCompetencies As System.Windows.Forms.TabPage
    Friend WithEvents LblKeyDuties As System.Windows.Forms.Label
    Friend WithEvents txtKeyDuties As System.Windows.Forms.TextBox
    Friend WithEvents gbCompetencies As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objBtnSearchCompetencies As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSeachCompetenciesCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlCompetencies As System.Windows.Forms.Panel
    Friend WithEvents lvCompetencies As eZee.Common.eZeeListView
    Friend WithEvents colhCompetenciesGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompetenciesItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDeleteCompetencies As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddCompetencies As eZee.Common.eZeeLightButton
    Friend WithEvents LblCompetenciesItem As System.Windows.Forms.Label
    Friend WithEvents cboCompetencies As System.Windows.Forms.ComboBox
    Friend WithEvents cboCompetenciesCategory As System.Windows.Forms.ComboBox
    Friend WithEvents LblCompetenciesCategory As System.Windows.Forms.Label
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents tabRemarks As System.Windows.Forms.TabControl
    Friend WithEvents tabpgExperienceComment As System.Windows.Forms.TabPage
    Friend WithEvents tabpgWorkingHrs As System.Windows.Forms.TabPage
    Friend WithEvents btnKeyDutiesEdit As eZee.Common.eZeeLightButton
    Friend WithEvents dgJobKeyDuties As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhKeyDutiesEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhKeyDuties As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhKeyDutiesGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobKeydutiestranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhCompetencyGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents ChkGradeLevel As System.Windows.Forms.CheckBox
    Friend WithEvents pnlGradresLevel As System.Windows.Forms.Panel
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents pnlClass As System.Windows.Forms.Panel
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents chkUnitGroup As System.Windows.Forms.CheckBox
    Friend WithEvents pnlUnitGrp As System.Windows.Forms.Panel
    Friend WithEvents cboUnitGrp As System.Windows.Forms.ComboBox
    Friend WithEvents chkSectionGroup As System.Windows.Forms.CheckBox
    Friend WithEvents pnlSectionGrp As System.Windows.Forms.Panel
    Friend WithEvents cboSectionGrp As System.Windows.Forms.ComboBox
    Friend WithEvents pnlDeprtmentGrp As System.Windows.Forms.Panel
    Friend WithEvents cboDeparmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents pnlBranch As System.Windows.Forms.Panel
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents chkClass As System.Windows.Forms.CheckBox
    Friend WithEvents chkDepartmentGroup As System.Windows.Forms.CheckBox
    Friend WithEvents chkBranch As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchSectionGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGradesLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDeparmentGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblcritical As System.Windows.Forms.Label
    Friend WithEvents nudCritical As System.Windows.Forms.NumericUpDown
    Friend WithEvents tabpJobLanguage As System.Windows.Forms.TabPage
    Friend WithEvents gbJobLanguage As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlJobLAnguage As System.Windows.Forms.Panel
    Friend WithEvents lvJobLanguage As eZee.Common.eZeeListView
    Friend WithEvents colhLanguage As System.Windows.Forms.ColumnHeader
    Friend WithEvents objLanguageUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLangJobUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLangGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDeleteLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents cboLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents objelLine4 As eZee.Common.eZeeLine
    Friend WithEvents cboJobType As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobType As System.Windows.Forms.Label
    Friend WithEvents chkKeyRole As System.Windows.Forms.CheckBox
End Class
