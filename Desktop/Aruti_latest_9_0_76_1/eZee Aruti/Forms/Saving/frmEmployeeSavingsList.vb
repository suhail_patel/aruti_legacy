﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeSavingsList

#Region "Private Variable"
    Private ReadOnly mstrModuleName As String = "frmEmployeeSavingsList"
    Private objEmployeeSavingData As clsSaving_Tran
    Private objEmployeeData As clsEmployee_Master
    Private objPeriodData As clscommom_period_Tran
    Private objMasterData As clsMasterData
    Private mintSeletedValue As Integer = 0

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private objSavingData As New clsSavingScheme
    'Anjan (02 Mar 2012)-End 

#End Region

#Region " Private Methods "
    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsEmployeeScheme As New DataSet
        Dim dtTable As DataTable
        Dim objExchangeRate As New clsExchangeRate


        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        objPeriodData = New clscommom_period_Tran
        objMasterData = New clsMasterData
        'Shani(24-Aug-2015) -- End

        Try

            If User._Object.Privilege._AllowToViewEmployeeSavingsList = True Then                'Pinkal (02-Jul-2012) -- Start


                objEmployeeSavingData = New clsSaving_Tran
                objExchangeRate = New clsExchangeRate

                'Sohail (09 Mar 2015) -- Start
                'Enhancement - Provide Multi Currency on Saving scheme contribution.
                Dim ds As DataSet = objExchangeRate.getComboList("List", False)
                Dim mdicCurrency As Dictionary(Of Integer, String) = (From p In ds.Tables(0).AsEnumerable Select New With {Key .ID = CInt(p.Item("countryunkid")), Key .NAME = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
                'Sohail (09 Mar 2015) -- End

                objExchangeRate._ExchangeRateunkid = 1



                'Pinkal (12 Jan 2015) -- Start
                'Enhancement - CHANGE IN SAVINGS MODULE.

                'Sohail (13 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'dsEmployeeScheme = objEmployeeSavingData.GetList("List")
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List")
                'End If
                'Sohail (13 Jan 2012) -- End

                Dim mstrEmployeeIDs As String = ""

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrEmployeeIDs = cboEmployee.SelectedValue.ToString()
                End If

                'Sohail (25 Aug 2021) -- Start
                'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                Dim strFilter As String = ""
                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    strFilter &= " AND svsaving_tran.payperiodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
                End If

                If CInt(cboSavingsScheme.SelectedValue) > 0 Then
                    strFilter &= " AND svsaving_tran.savingschemeunkid = " & CInt(cboSavingsScheme.SelectedValue) & " "
                End If

                If strFilter.Length > 0 Then
                    strFilter = strFilter.Substring(4)
                End If


                Dim strOuterFilter As String = ""
                If txtContribution.Decimal <> 0 Then
                    strOuterFilter &= "AND b.contribution = " & txtContribution.Decimal & " "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    strOuterFilter &= " AND a.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If
                'Sohail (25 Aug 2021) -- End

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change Saving GetList method for get active Employee saving

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "", eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mstrEmployeeIDs)
                'Else
                '    dsEmployeeScheme = objEmployeeSavingData.GetList("List", , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), mstrEmployeeIDs)
                'End If

                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriodData._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                    objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dtPeriodStart = objPeriodData._Start_Date
                    dtPeriodEnd = objPeriodData._End_Date
                Else
                    'Sohail (12 Dec 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'Dim intPeriodUnkid As Integer = 0

                    ''S.SANDEEP [04 JUN 2015] -- START
                    ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    ''intPeriodUnkid = objMasterData.getFirstPeriodID(enModuleReference.Payroll)
                    'intPeriodUnkid = objMasterData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid)
                    ''S.SANDEEP [04 JUN 2015] -- END

                    'If intPeriodUnkid > 0 Then
                    '    'Sohail (21 Aug 2015) -- Start
                    '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    '    'objPeriodData._Periodunkid = intPeriodUnkid
                    '    objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
                    '    'Sohail (21 Aug 2015) -- End
                    '    dtPeriodStart = objPeriodData._Start_Date
                    '    dtPeriodEnd = objPeriodData._End_Date
                    'Else
                    '    dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    '    dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    'End If
                    dtPeriodStart = FinancialYear._Object._Database_Start_Date
                    dtPeriodEnd = FinancialYear._Object._Database_End_Date
                    'Sohail (12 Dec 2015) -- End
                End If
                dsEmployeeScheme = objEmployeeSavingData.GetList(FinancialYear._Object._DatabaseName, _
                                                                 User._Object._Userunkid, _
                                                                 FinancialYear._Object._YearUnkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 dtPeriodStart, _
                                                                 dtPeriodEnd, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                 "List", strFilter, , , CInt(cboEmployee.SelectedValue), , , , , , strOuterFilter)
                'Sohail (25 Aug 2021) - [strFilter, strOuterFilter]
                'Sohail (12 Dec 2015) -- [CInt(cboEmployee.SelectedValue)]
                'Shani(24-Aug-2015) -- End

                'Pinkal (12 Jan 2015) -- End

                'Sohail (25 Aug 2021) -- Start
                'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                'If txtContribution.Decimal <> 0 Then 'Sohail (11 May 2011)
                '    strSearching &= "AND contribution =" & txtContribution.Decimal & " " 'Sohail (11 May 2011)
                'End If

                'If CInt(cboStatus.SelectedValue) > 0 Then
                '    strSearching &= "AND savingstatus =" & CInt(cboStatus.SelectedValue) & " "
                'End If

                'If CInt(cboPayPeriod.SelectedValue) > 0 Then
                '    strSearching &= "AND payperiodunkid =" & CInt(cboPayPeriod.SelectedValue) & " "
                'End If


                ''Anjan (02 Mar 2012)-Start
                ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                ''If Trim(txtRemark.Text) <> "" Then
                ''    strSearching &= "AND remark LIKE '%" & txtRemark.Text & "%' "
                ''End If
                'If CInt(cboSavingsScheme.SelectedValue) > 0 Then
                '    strSearching &= "AND savingschemeunkid =" & CInt(cboSavingsScheme.SelectedValue) & " "
                'End If
                ''Anjan (02 Mar 2012)-End 



                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtTable = New DataView(dsEmployeeScheme.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = dsEmployeeScheme.Tables("List")
                'End If
                    dtTable = dsEmployeeScheme.Tables("List")
                'Sohail (25 Aug 2021) -- End

                Dim lvItem As ListViewItem

                lvEmployeeSavings.Items.Clear()

                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    lvItem.Text = drRow("empcode").ToString
                    'Anjan (02 Mar 2012)-End 

                    lvItem.SubItems.Add(drRow("employeename").ToString)
                    lvItem.SubItems(colhEmpName.Index).Tag = CInt(drRow("employeeunkid")) 'Sohail (12 Jan 2015)

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    lvItem.SubItems.Add(drRow("savingscode").ToString)
                    'Anjan (02 Mar 2012)-End 

                    lvItem.SubItems.Add(drRow("savingscheme").ToString)
                    lvItem.SubItems.Add(drRow("period").ToString)
                    lvItem.SubItems.Add(Format(CDec(drRow("balance_amount")), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)


                    'Pinkal (12 Jan 2015) -- Start
                    'Enhancement - CHANGE IN SAVINGS MODULE.
                    lvItem.SubItems.Add(Math.Round(CDec(drRow("interest_rate")), 2).ToString())
                    'Pinkal (12 Jan 2015) -- End

                    lvItem.SubItems.Add(Format(CDec(drRow("interest_amount")), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                    'Sohail (09 Mar 2015) -- Start
                    'Enhancement - Provide Multi Currency on Saving scheme contribution.
                    'lvItem.SubItems.Add(Format(CDec(drRow("contribution")), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                    If mdicCurrency.ContainsKey(CInt(drRow.Item("countryunkid"))) = True Then
                        lvItem.SubItems.Add(Format(CDec(drRow("contribution")), objExchangeRate._fmtCurrency) & " " & mdicCurrency.Item(CInt(drRow.Item("countryunkid"))))
                    Else
                        lvItem.SubItems.Add(Format(CDec(drRow("contribution")), objExchangeRate._fmtCurrency) & " " & objExchangeRate._Currency_Sign)
                    End If
                    'Sohail (09 Mar 2015) -- End
                    'Sohail (25 Aug 2021) -- Start
                    'TRA Issue : AAIP-248 : Slowness on edit employee on employee master screen.
                    'RemoveHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                    'cboStatus.SelectedValue = CInt(drRow.Item("savingstatus"))
                    'AddHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                    'lvItem.SubItems.Add(cboStatus.Text.ToString)
                    lvItem.SubItems.Add(drRow("statusname").ToString)
                    'Sohail (25 Aug 2021) -- End
                    'Anjan (26 Jan 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS
                    If drRow.Item("stopdate").ToString.Trim <> "" Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("stopdate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    'Sohail (12 Dec 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    lvItem.SubItems(colhStopDate.Index).Tag = drRow.Item("effectivedate").ToString
                    'Sohail (12 Dec 2015) -- End

                    'Anjan (26 Jan 2012)-End 
                    lvItem.SubItems(colhStatus.Index).Tag = CInt(drRow("savingstatus").ToString)
                    lvItem.SubItems.Add(drRow.Item("remark").ToString)
                    lvItem.SubItems.Add(Format(CDec(drRow("total_contribution")), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                    If CBool(drRow.Item("broughtforward")) = True Then lvItem.ForeColor = Color.Gray
                    lvItem.Tag = drRow("savingtranunkid")
                    lvEmployeeSavings.Items.Add(lvItem)

                Next

                cboStatus.SelectedValue = mintSeletedValue

                If lvEmployeeSavings.Items.Count > 11 Then
                    colhRemarks.Width = 280 - 18
                Else
                    colhRemarks.Width = 280
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            dsEmployeeScheme.Dispose()
            objExchangeRate = Nothing
        End Try

    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet
        objEmployeeData = New clsEmployee_Master
        objMasterData = New clsMasterData
        objPeriodData = New clscommom_period_Tran

        'Anjan (02 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        objSavingData = New clsSavingScheme
        'Anjan (02 Mar 2012)-End 


        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployeeData.GetEmployeeList("List", True)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployeeData.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployeeData.GetEmployeeList("List", True)
            'End If

            ''Sohail (06 Jan 2012) -- End
            'cboEmployee.ValueMember = "employeeunkid"
            'cboEmployee.DisplayMember = "employeename"
            'cboEmployee.DataSource = dsList.Tables("List")
            'cboEmployee.SelectedValue = 0

            'Shani(24-Aug-2015) -- End
            'Sohail (27 Jul 2010) -- Start
            'dsList = objPeriodData.getListForCombo(enModuleRefenence.Payroll, 1, "List", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'Sohail (27 Jul 2010) -- End
            cboPayPeriod.ValueMember = "periodunkid"
            cboPayPeriod.DisplayMember = "name"
            cboPayPeriod.DataSource = dsList.Tables("List")
            cboPayPeriod.SelectedValue = 0

            dsList = objMasterData.GetLoan_Saving_Status("List", True)
            'Sohail (24 Jul 2010) -- Start
            Dim dtTable As DataTable = New DataView(dsList.Tables("List"), "Id <> 3", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (24 Jul 2010) -- End
            cboStatus.ValueMember = "Id"
            cboStatus.DisplayMember = "name"
            'Sohail (26 Jul 2010) -- Start
            ' cboStatus.DataSource  = dsCombos.Tables("Status")
            cboStatus.DataSource = dtTable
            'Sohail (26 Jul 2010) -- End

            cboStatus.SelectedValue = 0


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            dsList = objSavingData.getComboList(True, "List")
            cboSavingsScheme.ValueMember = "savingschemeunkid"
            cboSavingsScheme.DisplayMember = "name"
            cboSavingsScheme.DataSource = dsList.Tables("List")
            cboSavingsScheme.SelectedValue = 0
            'Anjan (02 Mar 2012)-End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeSavings
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeSavings
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeSavings
            btnChangeStatus.Enabled = User._Object.Privilege._AllowChangeSavingStatus

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub FillEmployee(Optional ByVal intPeriodunkid As Integer = 0)
        Dim dsList As DataSet
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        Dim mintPeriodid As Integer = intPeriodunkid
        objPeriodData = New clscommom_period_Tran
        objMasterData = New clsMasterData
        Try

            If intPeriodunkid > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriodData._Periodunkid = mintPeriodid
                objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodid
                'Sohail (21 Aug 2015) -- End
                dtPeriodStart = objPeriodData._Start_Date
                dtPeriodEnd = objPeriodData._End_Date
            Else

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'mintPeriodid = objMasterData.getFirstPeriodID(enModuleReference.Payroll)

                'Nilay (02 Feb 2017) -- Start
                'Issue : first open payroll period was supposed to be passed and financial year first period was passed so it was not giving active employee in search combo.
                'mintPeriodid = objMasterData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid)
                mintPeriodid = objMasterData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                'Nilay (02 Feb 2017) -- End

                'S.SANDEEP [04 JUN 2015] -- END

                If mintPeriodid > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriodData._Periodunkid = mintPeriodid
                    objPeriodData._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodid
                    'Sohail (21 Aug 2015) -- End
                    dtPeriodStart = objPeriodData._Start_Date
                    dtPeriodEnd = objPeriodData._End_Date
                Else
                    dtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    dtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
            End If

            dsList = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     dtPeriodStart, _
                                                     dtPeriodEnd, _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                                     ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables("List")
            cboEmployee.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

#End Region

#Region " Form's Events "
    Private Sub frmEMployeeSavingList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete Then
            btnDelete.PerformClick()
        End If
    End Sub
    Private Sub frmEmployeeSavingList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmEmployeeSavingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployeeSavingData = Nothing
    End Sub

    Private Sub frmEmployeeSchemeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeSavingData = New clsSaving_Tran
        Try

            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call FillCombo()
            'Call FillList()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Call FillEmployee()
            'Shani(24-Aug-2015) -- End


            If lvEmployeeSavings.Items.Count > 0 Then lvEmployeeSavings.Items(0).Selected = True
            lvEmployeeSavings.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSchemeList_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSaving_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsSaving_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeSavings_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvEmployeeSavings.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditEmployeeSavings = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvEmployeeSavings.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEmployeeSavings.Select()
            Exit Sub
        End If
        Dim frm As New frmEmployeeSavings_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmployeeSavings.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If CInt(lvEmployeeSavings.SelectedItems(0).SubItems(colhStatus.Index).Tag) = 4 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot edit this transaction Reason : This transaction is in completed mode."), enMsgBoxStyle.Information) '?1
                Exit Sub
            End If


            If frm.displayDialog(CInt(lvEmployeeSavings.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvEmployeeSavings.Items(intSelectedIndex).Selected = True
            lvEmployeeSavings.EnsureVisible(intSelectedIndex)
            lvEmployeeSavings.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvEmployeeSavings.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEmployeeSavings.Select()
            Exit Sub
        End If

        If lvEmployeeSavings.SelectedItems(0).ForeColor = Color.Gray Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You cannot delete this transaction. Reason : This transaction is of previous year."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'If objSavingScheme.isUsed(CInt(lvGradeGroup.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason: This Saving Scheme is in use."), enMsgBoxStyle.Information) '?2
        '    lvGradeGroup.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmployeeSavings.SelectedItems(0).Index
            objEmployeeSavingData._Savingtranunkid = CInt(lvEmployeeSavings.SelectedItems(0).Tag)


            If clsPayment_tran.enPayTypeId.REPAYMENT = objEmployeeSavingData._Savingstatus Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot delete this Saving Scheme. Reason :This Saving Scheme is in Complete mode."), enMsgBoxStyle.Information)
                Exit Sub
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'ElseIf objEmployeeSavingData._Balance_Amount > 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason :Deduction of savings is in progress."), enMsgBoxStyle.Information)
                '    Exit Sub
            ElseIf objEmployeeSavingData._Balance_Amount > 0 AndAlso (objEmployeeSavingData._Balance_Amount <> objEmployeeSavingData._Bf_Balance OrElse objEmployeeSavingData._Broughtforward = True) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason :Deduction of savings is in progress."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim objMaster As New clsMasterData
                Dim objTnALeaveTran As New clsTnALeaveTran
                Dim objPeriod As New clscommom_period_Tran
                Dim intOpenPeriod As Integer = 0
                Dim dtOpenPeriodEnd As DateTime = Nothing

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
                intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intOpenPeriod
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intOpenPeriod
                'Sohail (21 Aug 2015) -- End
                dtOpenPeriodEnd = objPeriod._End_Date
                If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, lvEmployeeSavings.SelectedItems(0).SubItems(colhEmpName.Index).Tag.ToString, dtOpenPeriodEnd, enModuleReference.Payroll) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You can not Delete this Saving Scheme. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete this Saving Scheme."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You can not Delete this Saving Scheme. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete this Saving Scheme.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 11, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
                'Sohail (12 Jan 2015) -- End
            End If


            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee's Saving Scheme?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then


                '<To Do>  to set the void user and void date time
                objEmployeeSavingData._Voiduserunkid = User._Object._Userunkid

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objEmployeeSavingData._VoidReason = CStr("Testing")
                'objEmployeeSavingData._Voiddatetime = Now
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.SAVINGS, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEmployeeSavingData._VoidReason = mstrVoidReason
                End If
                frm = Nothing
                objEmployeeSavingData._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployeeSavingData.Delete(CInt(lvEmployeeSavings.SelectedItems(0).Tag))
                objEmployeeSavingData.Delete(CInt(lvEmployeeSavings.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

                lvEmployeeSavings.SelectedItems(0).Remove()

                If lvEmployeeSavings.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvEmployeeSavings.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvEmployeeSavings.Items.Count - 1
                    lvEmployeeSavings.Items(intSelectedIndex).Selected = True
                    lvEmployeeSavings.EnsureVisible(intSelectedIndex)
                ElseIf lvEmployeeSavings.Items.Count <> 0 Then
                    lvEmployeeSavings.Items(intSelectedIndex).Selected = True
                    lvEmployeeSavings.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvEmployeeSavings.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
            'Nilay (02 Feb 2017) -- Start
            Call objbtnSearch.ShowResult(CStr(lvEmployeeSavings.Items.Count))
            'Nilay (02 Feb 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

        Try
            cboEmployee.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            txtContribution.Text = ""
            txtRemark.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnEployeeSearchSavingScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnEmployeeSavingSearch.Click
        Dim objfrm As New frmCommonSearch
        'Nilay (23 Jan 2017) -- Start
        'Dim dsList As DataSet
        'Nilay (23 Jan 2017) -- End

        Try
            ' objEmployeeSavingData = New clsSaving_Tran
            objEmployeeData = New clsEmployee_Master
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployeeData.GetEmployeeList("List", False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployeeData.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployeeData.GetEmployeeList("List", True)
            'End If

            'Nilay (23 Jan 2017) -- Start
            'ISSUE: Employee List count is differ to combobox
            'dsList = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                       User._Object._Userunkid, _
            '                                       FinancialYear._Object._YearUnkid, _
            '                                       Company._Object._Companyunkid, _
            '                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                       ConfigParameter._Object._UserAccessModeSetting, _
            '                                       True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'Nilay (23 Jan 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                'Nilay (23 Jan 2017) -- Start
                'objfrm.DataSource = dsList.Tables("List")
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                'Nilay (23 Jan 2017) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnEployeeSearchSavingScheme_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployeeData = Nothing

        End Try

    End Sub
#End Region

#Region "Control's Event"

    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    Me.Close()
    'End Sub

    'Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
    '    Try
    '        Dim objEmpSaving_AddEdit As New frmEmployeeSavings_AddEdit
    '        objEmpSaving_AddEdit.ShowDialog()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
    '    Try
    '        Dim objSavingStatus As New frmSavingStatus_AddEdit
    '        objSavingStatus.ShowDialog()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub cboStatus_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectionChangeCommitted
        Try
            mintSeletedValue = CInt(cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Dim objSavingStatus As New frmSavingStatus_AddEdit
        Dim decTotContribution As Decimal = 0 'Sohail (11 May 2011)
        Dim decTotInterest As Decimal = 0 'Sohail (11 May 2011)
        Dim decBalAmt As Decimal = 0 'Sohail (11 May 2011)

        Try

            If lvEmployeeSavings.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmployeeSavings.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmployeeSavings.SelectedItems(0).Index

            'Sohail (26 Jul 2010) -- Start
            decTotContribution = CDec(lvEmployeeSavings.Items(intSelectedIndex).SubItems(colhTotalContribution.Index).Text) 'Sohail (11 May 2011)
            decTotInterest = CDec(lvEmployeeSavings.Items(intSelectedIndex).SubItems(colhInterestAccrued.Index).Text) 'Sohail (11 May 2011)
            decBalAmt = CDec(lvEmployeeSavings.Items(intSelectedIndex).SubItems(colhBalAmount.Index).Text) 'Sohail (11 May 2011)
            If CInt(lvEmployeeSavings.Items(intSelectedIndex).SubItems(colhStatus.Index).Tag) = 4 _
                    And Math.Abs(decTotContribution - (decBalAmt - decTotInterest)) >= 1 Then '4=Complete

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot change status when it is complete and payment is done."), enMsgBoxStyle.Information)
                lvEmployeeSavings.Select()
                Exit Sub
            End If
            'If objSavingStatus.displayDialog(CInt(lvEmployeeSavings.SelectedItems(0).Tag), True) Then
            If objSavingStatus.displayDialog(CInt(lvEmployeeSavings.SelectedItems(0).Tag), True, _
                                             CInt(lvEmployeeSavings.Items(intSelectedIndex).SubItems(colhStatus.Index).Tag)) Then
                'Sohail (26 Jul 2010) -- End

                Call FillList()
            End If

            objSavingStatus = Nothing

            lvEmployeeSavings.Items(intSelectedIndex).Selected = True
            lvEmployeeSavings.EnsureVisible(intSelectedIndex)
            lvEmployeeSavings.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try

    End Sub

    'Private Sub btnOperations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOperations.Click
    '    Dim frm As New frmPaymentList
    '    Try
    '        If lvEmployeeSavings.SelectedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
    '            lvEmployeeSavings.Select()
    '            Exit Sub
    '        End If

    '        'If User._Object._RightToLeft = True Then
    '        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    frm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(frm)
    '        'End If

    '        'If lvEmployeeSavings.SelectedItems(0).SubItems(colhIsLoan.Index).Text.ToUpper = "TRUE" Then
    '        frm._Reference_Id = enPaymentRefId.SAVINGS
    '        'Else
    '        'frm._Reference_Id = enPaymentRefId.ADVANCE
    '        'End If
    '        frm._Transaction_Id = CInt(lvEmployeeSavings.SelectedItems(0).Tag)

    '      frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REPAYMENT

    '        frm.ShowDialog()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try

    'End Sub

    'Private Sub mnuRedemption_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRedemption.Click
    '    Dim frm As New frmPaymentList
    '    Try
    '        If lvEmployeeSavings.SelectedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
    '            lvEmployeeSavings.Select()
    '            Exit Sub

    '        End If
    '        'If User._Object._RightToLeft = True Then
    '        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    frm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(frm)
    '        'End If

    '        frm._Reference_Id = clsPayment_tran.enPaymentRefId.SAVINGS
    '        'Else
    '        'frm._Reference_Id = enPaymentRefId.ADVANCE
    '        'End If
    '        frm._Transaction_Id = CInt(lvEmployeeSavings.SelectedItems(0).Tag)

    '        frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REDEMPTION


    '        frm.ShowDialog()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "mnuPayment_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub mnuRepayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmPaymentList

        Try
            If lvEmployeeSavings.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmployeeSavings.Select()
                Exit Sub
            Else
                Select Case CInt(lvEmployeeSavings.Items(lvEmployeeSavings.SelectedItems(0).Index).SubItems(colhStatus.Index).Tag)
                    Case 4 'Complete

                    Case Else 'Inprogress, OnHold, Redemption
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do repayment when status is ") & lvEmployeeSavings.Items(lvEmployeeSavings.SelectedItems(0).Index).SubItems(colhStatus.Index).Text, enMsgBoxStyle.Information)
                        lvEmployeeSavings.Select()
                        Exit Sub
                End Select
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            frm._Reference_Id = clsPayment_tran.enPaymentRefId.SAVINGS
            frm._Transaction_Id = CInt(lvEmployeeSavings.SelectedItems(0).Tag)
            frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REPAYMENT

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRepayment_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub lvEmployeeSavings_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEmployeeSavings.SelectedIndexChanged, lvEmployeeSavings.Click
        Try
            If lvEmployeeSavings.SelectedItems.Count > 0 Then
                If CInt(lvEmployeeSavings.SelectedItems(0).SubItems(colhStatus.Index).Tag) = 4 Then
                    btnEdit.Enabled = False
                    btnChangeStatus.Enabled = False
                Else
                    btnEdit.Enabled = True
                    btnChangeStatus.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeSavings_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Nov 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    'Private Sub btnPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayment.Click
    Private Sub btnPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayment.Click
        'Sohail (21 Nov 2015) -- End
        Dim frm As New frmPaymentList

        Try
            If lvEmployeeSavings.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmployeeSavings.Select()
                Exit Sub
            Else
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'Select Case CInt(lvEmployeeSavings.Items(lvEmployeeSavings.SelectedItems(0).Index).SubItems(colhStatus.Index).Tag)
                '    Case 4 'Complete

                'Case Else 'Inprogress, OnHold, Redemption
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do repayment when status is ") & lvEmployeeSavings.Items(lvEmployeeSavings.SelectedItems(0).Index).SubItems(colhStatus.Index).Text, enMsgBoxStyle.Information)
                '    lvEmployeeSavings.Select()
                '    Exit Sub
                'End Select
                'Sohail (12 Jan 2015) -- End
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            frm._Reference_Id = clsPayment_tran.enPaymentRefId.SAVINGS
            frm._Transaction_Id = CInt(lvEmployeeSavings.SelectedItems(0).Tag)
            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REPAYMENT
            Select Case CInt(lvEmployeeSavings.Items(lvEmployeeSavings.SelectedItems(0).Index).SubItems(colhStatus.Index).Tag)
                Case enSavingStatus.IN_PROGRESS, enSavingStatus.ON_HOLD
                    frm._PaymentType_Id = clsPayment_tran.enPayTypeId.WITHDRAWAL
                Case enSavingStatus.REDEMPTION '<TODO when redemption feature given>
                    frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REPAYMENT
                Case enSavingStatus.COMPLETED
                    frm._PaymentType_Id = clsPayment_tran.enPayTypeId.REPAYMENT
            End Select
            'Sohail (12 Jan 2015) -- End
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = eZeeDate.convertDate(lvEmployeeSavings.SelectedItems(0).SubItems(colhStopDate.Index).Tag.ToString)
            'Sohail (12 Dec 2015) -- End

            frm.ShowDialog()
            Call FillList() 'Sohail (04 Mar 2011)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPayment_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Nov 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Private Sub mnuDeposit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeposit.Click
        Try

            If lvEmployeeSavings.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmployeeSavings.Select()
                Exit Sub
            ElseIf CInt(lvEmployeeSavings.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enSavingStatus.COMPLETED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot perform further operation on this transaction. Reason : This transaction is in completed mode."), enMsgBoxStyle.Information) '?1
                Exit Sub
            End If

            Dim frm As New frmPaymentList

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm._Reference_Id = clsPayment_tran.enPaymentRefId.SAVINGS
            frm._Transaction_Id = CInt(lvEmployeeSavings.SelectedItems(0).Tag)
            frm._PaymentType_Id = clsPayment_tran.enPayTypeId.DEPOSIT
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = eZeeDate.convertDate(lvEmployeeSavings.SelectedItems(0).SubItems(colhStopDate.Index).Tag.ToString)
            'Sohail (12 Dec 2015) -- End

            frm.ShowDialog()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuDeposit_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Nov 2015) -- End

    Private Sub objSavingScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSavingScheme.Click

        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet = Nothing
        objSavingData = New clsSavingScheme
        Try


            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            With cboSavingsScheme
                objfrm.DataSource = CType(cboSavingsScheme.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSavingScheme_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objSavingData = Nothing
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            Call FillEmployee(CInt(cboPayPeriod.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End
#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor

            Me.btnPayment.GradientBackColor = GUI._ButttonBackColor
            Me.btnPayment.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblContribution.Text = Language._Object.getCaption(Me.lblContribution.Name, Me.lblContribution.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.colhSavingScheme.Text = Language._Object.getCaption(CStr(Me.colhSavingScheme.Tag), Me.colhSavingScheme.Text)
            Me.colhPayPeriod.Text = Language._Object.getCaption(CStr(Me.colhPayPeriod.Tag), Me.colhPayPeriod.Text)
            Me.colhBalAmount.Text = Language._Object.getCaption(CStr(Me.colhBalAmount.Tag), Me.colhBalAmount.Text)
            Me.colhInterestAccrued.Text = Language._Object.getCaption(CStr(Me.colhInterestAccrued.Tag), Me.colhInterestAccrued.Text)
            Me.colhContribution.Text = Language._Object.getCaption(CStr(Me.colhContribution.Tag), Me.colhContribution.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhRemarks.Text = Language._Object.getCaption(CStr(Me.colhRemarks.Tag), Me.colhRemarks.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.colhTotalContribution.Text = Language._Object.getCaption(CStr(Me.colhTotalContribution.Tag), Me.colhTotalContribution.Text)
            Me.btnPayment.Text = Language._Object.getCaption(Me.btnPayment.Name, Me.btnPayment.Text)
            Me.colhStopDate.Text = Language._Object.getCaption(CStr(Me.colhStopDate.Tag), Me.colhStopDate.Text)
            Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
            Me.colhSavingCode.Text = Language._Object.getCaption(CStr(Me.colhSavingCode.Tag), Me.colhSavingCode.Text)
            Me.colhInterestRate.Text = Language._Object.getCaption(CStr(Me.colhInterestRate.Tag), Me.colhInterestRate.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuPayment.Text = Language._Object.getCaption(Me.mnuPayment.Name, Me.mnuPayment.Text)
            Me.mnuDeposit.Text = Language._Object.getCaption(Me.mnuDeposit.Name, Me.mnuDeposit.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee Saving Scheme from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason :Deduction of savings is in progress.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee's Saving Scheme?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot do repayment when status is")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot change status when it is complete and payment is done.")
            Language.setMessage(mstrModuleName, 6, "Sorry, You cannot edit this transaction Reason : This transaction is in completed mode.")
            Language.setMessage(mstrModuleName, 7, "You cannot delete this transaction. Reason : This transaction is of previous year.")
            Language.setMessage(mstrModuleName, 8, "Sorry, You cannot delete this Saving Scheme. Reason :This Saving Scheme is in Complete mode.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You can not Delete this Saving Scheme. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete this Saving Scheme.")
            Language.setMessage(mstrModuleName, 10, "Sorry, You cannot perform further operation on this transaction. Reason : This transaction is in completed mode.")
			Language.setMessage(mstrModuleName, 11, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings 'Language & UI Settings
    '</Language>
End Class