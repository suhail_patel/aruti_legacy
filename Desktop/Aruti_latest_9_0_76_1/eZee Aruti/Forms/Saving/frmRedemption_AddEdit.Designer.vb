﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRedemption_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRedemption_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbRedemptionInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRedempCharges = New eZee.TextBox.NumericTextBox
        Me.lblRedempCharges = New System.Windows.Forms.Label
        Me.txtRedempWithinYear = New eZee.TextBox.NumericTextBox
        Me.lblRedempWithinYear = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbRedemptionInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbRedemptionInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(296, 165)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbRedemptionInfo
        '
        Me.gbRedemptionInfo.BorderColor = System.Drawing.Color.Black
        Me.gbRedemptionInfo.Checked = False
        Me.gbRedemptionInfo.CollapseAllExceptThis = False
        Me.gbRedemptionInfo.CollapsedHoverImage = Nothing
        Me.gbRedemptionInfo.CollapsedNormalImage = Nothing
        Me.gbRedemptionInfo.CollapsedPressedImage = Nothing
        Me.gbRedemptionInfo.CollapseOnLoad = False
        Me.gbRedemptionInfo.Controls.Add(Me.txtRedempCharges)
        Me.gbRedemptionInfo.Controls.Add(Me.lblRedempCharges)
        Me.gbRedemptionInfo.Controls.Add(Me.txtRedempWithinYear)
        Me.gbRedemptionInfo.Controls.Add(Me.lblRedempWithinYear)
        Me.gbRedemptionInfo.ExpandedHoverImage = Nothing
        Me.gbRedemptionInfo.ExpandedNormalImage = Nothing
        Me.gbRedemptionInfo.ExpandedPressedImage = Nothing
        Me.gbRedemptionInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRedemptionInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRedemptionInfo.HeaderHeight = 25
        Me.gbRedemptionInfo.HeightOnCollapse = 0
        Me.gbRedemptionInfo.LeftTextSpace = 0
        Me.gbRedemptionInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbRedemptionInfo.Name = "gbRedemptionInfo"
        Me.gbRedemptionInfo.OpenHeight = 300
        Me.gbRedemptionInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRedemptionInfo.ShowBorder = True
        Me.gbRedemptionInfo.ShowCheckBox = False
        Me.gbRedemptionInfo.ShowCollapseButton = False
        Me.gbRedemptionInfo.ShowDefaultBorderColor = True
        Me.gbRedemptionInfo.ShowDownButton = False
        Me.gbRedemptionInfo.ShowHeader = True
        Me.gbRedemptionInfo.Size = New System.Drawing.Size(272, 92)
        Me.gbRedemptionInfo.TabIndex = 0
        Me.gbRedemptionInfo.Temp = 0
        Me.gbRedemptionInfo.Text = "Add Redemption Info"
        Me.gbRedemptionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRedempCharges
        '
        Me.txtRedempCharges.AllowNegative = True
        Me.txtRedempCharges.DigitsInGroup = 0
        Me.txtRedempCharges.Flags = 0
        Me.txtRedempCharges.Location = New System.Drawing.Point(162, 61)
        Me.txtRedempCharges.MaxDecimalPlaces = 6
        Me.txtRedempCharges.MaxWholeDigits = 21
        Me.txtRedempCharges.Name = "txtRedempCharges"
        Me.txtRedempCharges.Prefix = ""
        Me.txtRedempCharges.RangeMax = 1.7976931348623157E+308
        Me.txtRedempCharges.RangeMin = -1.7976931348623157E+308
        Me.txtRedempCharges.Size = New System.Drawing.Size(90, 21)
        Me.txtRedempCharges.TabIndex = 1
        Me.txtRedempCharges.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRedempCharges
        '
        Me.lblRedempCharges.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedempCharges.Location = New System.Drawing.Point(8, 63)
        Me.lblRedempCharges.Name = "lblRedempCharges"
        Me.lblRedempCharges.Size = New System.Drawing.Size(148, 16)
        Me.lblRedempCharges.TabIndex = 9
        Me.lblRedempCharges.Text = "Redemption Charges (%)"
        Me.lblRedempCharges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRedempWithinYear
        '
        Me.txtRedempWithinYear.AllowNegative = True
        Me.txtRedempWithinYear.DigitsInGroup = 0
        Me.txtRedempWithinYear.Flags = 0
        Me.txtRedempWithinYear.Location = New System.Drawing.Point(162, 34)
        Me.txtRedempWithinYear.MaxDecimalPlaces = 6
        Me.txtRedempWithinYear.MaxWholeDigits = 21
        Me.txtRedempWithinYear.Name = "txtRedempWithinYear"
        Me.txtRedempWithinYear.Prefix = ""
        Me.txtRedempWithinYear.RangeMax = 1.7976931348623157E+308
        Me.txtRedempWithinYear.RangeMin = -1.7976931348623157E+308
        Me.txtRedempWithinYear.Size = New System.Drawing.Size(90, 21)
        Me.txtRedempWithinYear.TabIndex = 0
        Me.txtRedempWithinYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRedempWithinYear
        '
        Me.lblRedempWithinYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedempWithinYear.Location = New System.Drawing.Point(8, 36)
        Me.lblRedempWithinYear.Name = "lblRedempWithinYear"
        Me.lblRedempWithinYear.Size = New System.Drawing.Size(148, 16)
        Me.lblRedempWithinYear.TabIndex = 2
        Me.lblRedempWithinYear.Text = "Redemption Within Year"
        Me.lblRedempWithinYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 110)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(296, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(84, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(187, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmRedemption_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(296, 165)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRedemption_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Redemption Info"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbRedemptionInfo.ResumeLayout(False)
        Me.gbRedemptionInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbRedemptionInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtRedempCharges As eZee.TextBox.NumericTextBox
    Friend WithEvents lblRedempCharges As System.Windows.Forms.Label
    Friend WithEvents txtRedempWithinYear As eZee.TextBox.NumericTextBox
    Friend WithEvents lblRedempWithinYear As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
End Class
