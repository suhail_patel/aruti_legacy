﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingStatus_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSavingStatus_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvSavingStatusHistory = New System.Windows.Forms.DataGridView
        Me.dgcolhUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTransactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objSavingstatustranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.gbSavingStatusInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnPeriodSearch = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.txtSavingScheme = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.lblSavingScheme = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvSavingStatusHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbSavingStatusInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvSavingStatusHistory)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbSavingStatusInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(707, 450)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvSavingStatusHistory
        '
        Me.dgvSavingStatusHistory.AllowUserToAddRows = False
        Me.dgvSavingStatusHistory.AllowUserToDeleteRows = False
        Me.dgvSavingStatusHistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvSavingStatusHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvSavingStatusHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhUser, Me.dgcolhTransactionDate, Me.dgcolhStatus, Me.objdgcolhIsGroup, Me.objSavingstatustranunkid})
        Me.dgvSavingStatusHistory.Location = New System.Drawing.Point(10, 186)
        Me.dgvSavingStatusHistory.MultiSelect = False
        Me.dgvSavingStatusHistory.Name = "dgvSavingStatusHistory"
        Me.dgvSavingStatusHistory.ReadOnly = True
        Me.dgvSavingStatusHistory.RowHeadersVisible = False
        Me.dgvSavingStatusHistory.RowHeadersWidth = 5
        Me.dgvSavingStatusHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvSavingStatusHistory.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSavingStatusHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSavingStatusHistory.Size = New System.Drawing.Size(686, 203)
        Me.dgvSavingStatusHistory.TabIndex = 3
        '
        'dgcolhUser
        '
        Me.dgcolhUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhUser.HeaderText = "User"
        Me.dgcolhUser.Name = "dgcolhUser"
        Me.dgcolhUser.ReadOnly = True
        Me.dgcolhUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTransactionDate
        '
        Me.dgcolhTransactionDate.HeaderText = "Transaction Date"
        Me.dgcolhTransactionDate.Name = "dgcolhTransactionDate"
        Me.dgcolhTransactionDate.ReadOnly = True
        Me.dgcolhTransactionDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTransactionDate.Width = 150
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStatus.Width = 170
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGroup.Visible = False
        '
        'objSavingstatustranunkid
        '
        Me.objSavingstatustranunkid.HeaderText = "objSavingstatustranunkid"
        Me.objSavingstatustranunkid.Name = "objSavingstatustranunkid"
        Me.objSavingstatustranunkid.ReadOnly = True
        Me.objSavingstatustranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objSavingstatustranunkid.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.dtpDate)
        Me.objFooter.Controls.Add(Me.lblDate)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 395)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(707, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(10, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 7
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(495, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(598, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(184, 18)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpDate.TabIndex = 0
        Me.dtpDate.Visible = False
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(138, 22)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(57, 15)
        Me.lblDate.TabIndex = 4
        Me.lblDate.Text = "Date"
        Me.lblDate.Visible = False
        '
        'gbSavingStatusInfo
        '
        Me.gbSavingStatusInfo.BorderColor = System.Drawing.Color.Black
        Me.gbSavingStatusInfo.Checked = False
        Me.gbSavingStatusInfo.CollapseAllExceptThis = False
        Me.gbSavingStatusInfo.CollapsedHoverImage = Nothing
        Me.gbSavingStatusInfo.CollapsedNormalImage = Nothing
        Me.gbSavingStatusInfo.CollapsedPressedImage = Nothing
        Me.gbSavingStatusInfo.CollapseOnLoad = False
        Me.gbSavingStatusInfo.Controls.Add(Me.objbtnPeriodSearch)
        Me.gbSavingStatusInfo.Controls.Add(Me.cboPeriod)
        Me.gbSavingStatusInfo.Controls.Add(Me.lblPeriod)
        Me.gbSavingStatusInfo.Controls.Add(Me.txtSavingScheme)
        Me.gbSavingStatusInfo.Controls.Add(Me.txtEmployeeName)
        Me.gbSavingStatusInfo.Controls.Add(Me.lblRemarks)
        Me.gbSavingStatusInfo.Controls.Add(Me.txtRemarks)
        Me.gbSavingStatusInfo.Controls.Add(Me.cboStatus)
        Me.gbSavingStatusInfo.Controls.Add(Me.lblStatus)
        Me.gbSavingStatusInfo.Controls.Add(Me.lblEmpName)
        Me.gbSavingStatusInfo.Controls.Add(Me.lblSavingScheme)
        Me.gbSavingStatusInfo.ExpandedHoverImage = Nothing
        Me.gbSavingStatusInfo.ExpandedNormalImage = Nothing
        Me.gbSavingStatusInfo.ExpandedPressedImage = Nothing
        Me.gbSavingStatusInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSavingStatusInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSavingStatusInfo.HeaderHeight = 25
        Me.gbSavingStatusInfo.HeaderMessage = ""
        Me.gbSavingStatusInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSavingStatusInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSavingStatusInfo.HeightOnCollapse = 0
        Me.gbSavingStatusInfo.LeftTextSpace = 0
        Me.gbSavingStatusInfo.Location = New System.Drawing.Point(10, 63)
        Me.gbSavingStatusInfo.Name = "gbSavingStatusInfo"
        Me.gbSavingStatusInfo.OpenHeight = 300
        Me.gbSavingStatusInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSavingStatusInfo.ShowBorder = True
        Me.gbSavingStatusInfo.ShowCheckBox = False
        Me.gbSavingStatusInfo.ShowCollapseButton = False
        Me.gbSavingStatusInfo.ShowDefaultBorderColor = True
        Me.gbSavingStatusInfo.ShowDownButton = False
        Me.gbSavingStatusInfo.ShowHeader = True
        Me.gbSavingStatusInfo.Size = New System.Drawing.Size(686, 117)
        Me.gbSavingStatusInfo.TabIndex = 0
        Me.gbSavingStatusInfo.Temp = 0
        Me.gbSavingStatusInfo.Text = "Saving Status Info"
        Me.gbSavingStatusInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnPeriodSearch
        '
        Me.objbtnPeriodSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnPeriodSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnPeriodSearch.BorderSelected = False
        Me.objbtnPeriodSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnPeriodSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnPeriodSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnPeriodSearch.Location = New System.Drawing.Point(303, 60)
        Me.objbtnPeriodSearch.Name = "objbtnPeriodSearch"
        Me.objbtnPeriodSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnPeriodSearch.TabIndex = 3
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Items.AddRange(New Object() {"Select"})
        Me.cboPeriod.Location = New System.Drawing.Point(104, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(195, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(92, 15)
        Me.lblPeriod.TabIndex = 240
        Me.lblPeriod.Text = "Period"
        '
        'txtSavingScheme
        '
        Me.txtSavingScheme.Flags = 0
        Me.txtSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSavingScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSavingScheme.Location = New System.Drawing.Point(104, 33)
        Me.txtSavingScheme.Name = "txtSavingScheme"
        Me.txtSavingScheme.ReadOnly = True
        Me.txtSavingScheme.Size = New System.Drawing.Size(195, 21)
        Me.txtSavingScheme.TabIndex = 0
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(429, 33)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(246, 21)
        Me.txtEmployeeName.TabIndex = 1
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(330, 63)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(94, 15)
        Me.lblRemarks.TabIndex = 238
        Me.lblRemarks.Text = "Remarks"
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(429, 60)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(246, 51)
        Me.txtRemarks.TabIndex = 3
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Items.AddRange(New Object() {"Select"})
        Me.cboStatus.Location = New System.Drawing.Point(104, 87)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(195, 21)
        Me.cboStatus.TabIndex = 4
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 90)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(92, 15)
        Me.lblStatus.TabIndex = 233
        Me.lblStatus.Text = "Saving Status"
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(330, 36)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(94, 15)
        Me.lblEmpName.TabIndex = 232
        Me.lblEmpName.Text = "Employee Name"
        '
        'lblSavingScheme
        '
        Me.lblSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSavingScheme.Location = New System.Drawing.Point(8, 36)
        Me.lblSavingScheme.Name = "lblSavingScheme"
        Me.lblSavingScheme.Size = New System.Drawing.Size(92, 15)
        Me.lblSavingScheme.TabIndex = 5
        Me.lblSavingScheme.Text = "Saving Scheme"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(707, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Add / Edit Saving Status"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "User"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 170
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Transaction Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 170
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 170
        '
        'frmSavingStatus_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(707, 450)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSavingStatus_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Saving Status"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvSavingStatusHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbSavingStatusInfo.ResumeLayout(False)
        Me.gbSavingStatusInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbSavingStatusInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtSavingScheme As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents lblSavingScheme As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnPeriodSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents dgvSavingStatusHistory As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents dgcolhUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTransactionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objSavingstatustranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
