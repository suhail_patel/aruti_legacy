﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportSavingWizard

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportSavingWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True


            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("vocno", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("savdate", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("assignperiod", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("savingscheme", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("contribution", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("total_contribution", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("interest_amount", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("balance_amount", System.Type.GetType("System.String")).DefaultValue = ""
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            mdt_ImportData_Others.Columns.Add("interest_rate", System.Type.GetType("System.String")).DefaultValue = ""
            'SHANI [12 JAN 2015]--END 

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = Nothing

            dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")

            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboFirstname.Text.Trim.Length > 0 Then
                    drNewRow.Item("firstname") = dtRow.Item(cboFirstname.Text).ToString.Trim
                Else
                    drNewRow.Item("firstname") = ""
                End If

                If cboLastname.Text.Trim.Length > 0 Then
                    drNewRow.Item("surname") = dtRow.Item(cboLastname.Text).ToString.Trim
                Else
                    drNewRow.Item("surname") = ""
                End If

                If cboSavingVocNo.Text.Trim.Length > 0 Then
                    drNewRow.Item("vocno") = dtRow.Item(cboSavingVocNo.Text).ToString.Trim
                Else
                    drNewRow.Item("vocno") = ""
                End If

                If cboSavingDate.Text.Trim.Length > 0 Then
                    drNewRow.Item("savdate") = dtRow.Item(cboSavingDate.Text).ToString.Trim
                Else
                    drNewRow.Item("savdate") = ""
                End If

                If cboSavingPeriod.Text.Trim.Length > 0 Then
                    drNewRow.Item("assignperiod") = dtRow.Item(cboSavingPeriod.Text).ToString.Trim
                Else
                    drNewRow.Item("assignperiod") = ""
                End If

                If cboSavingScheme.Text.Trim.Length > 0 Then
                    drNewRow.Item("savingscheme") = dtRow.Item(cboSavingScheme.Text).ToString.Trim
                Else
                    drNewRow.Item("savingscheme") = ""
                End If

                If cboContribution.Text.Trim.Length > 0 Then
                    drNewRow.Item("contribution") = dtRow.Item(cboContribution.Text).ToString.Trim
                Else
                    drNewRow.Item("contribution") = 0
                End If

                If cboTotalContribution.Text.Trim.Length > 0 Then
                    drNewRow.Item("total_contribution") = dtRow.Item(cboTotalContribution.Text).ToString.Trim
                Else
                    drNewRow.Item("total_contribution") = 0
                End If

                If cboInterestAmount.Text.Trim.Length > 0 Then
                    drNewRow.Item("interest_amount") = dtRow.Item(cboInterestAmount.Text).ToString.Trim
                Else
                    drNewRow.Item("interest_amount") = 0
                End If

                If cboBalanceAmount.Text.Trim.Length > 0 Then
                    drNewRow.Item("balance_amount") = dtRow.Item(cboBalanceAmount.Text).ToString.Trim
                Else
                    drNewRow.Item("balance_amount") = 0
                End If

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                If cboInterestRate.Text.Trim.Length > 0 Then
                    drNewRow.Item("interest_rate") = dtRow.Item(cboInterestRate.Text).ToString.Trim
                Else
                    drNewRow.Item("interest_rate") = 0
                End If
                'SHANI [12 JAN 2015]--END 

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)

            Next

            If blnIsNotThrown = True Then
                dgData.AutoGenerateColumns = False
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportEmp.BackEnabled = False
            eZeeWizImportEmp.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try

            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'If .Item(cboSavingVocNo.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Saving Voc. No. cannot be blank. Please set Saving Voc. No. in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                If ConfigParameter._Object._SavingsVocNoType = 0 Then
                    If .Item(cboSavingVocNo.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Saving Voc. No. cannot be blank. Please set Saving Voc. No. in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                'Sohail (12 Jan 2015) -- End

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'If .Item(cboSavingDate.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Assignment Date cannot be blank. Please set Saving Assignment Date in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'Sohail (12 Jan 2015) -- End
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                If .Item(cboSavingDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Assignment Date cannot be blank. Please set Saving Assignment Date in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (12 Dec 2015) -- End

                If .Item(cboSavingPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Saving Assignment Period cannot be blank. Please set Saving Assignment Period in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboSavingScheme.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Saving Scheme cannot be blank. Please set Saving Scheme in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboContribution.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Saving Contribution cannot be blank. Please set Saving Contribution in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboTotalContribution.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Total Contibution cannot be blank. Please set Total Contribution in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'If .Item(cboInterestAmount.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'If .Item(cboInterestRate.Text).ToString.Trim.Length > 0 AndAlso .Item(cboInterestAmount.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                If cboInterestRate.Text <> "" Then
                    If .Item(cboInterestRate.Text).ToString.Trim.Length > 0 AndAlso .Item(cboInterestAmount.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                'Sohail (12 Dec 2015) -- End
                'SHANI [12 JAN 2015]--END 

                If .Item(cboBalanceAmount.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Saving Balance Amount cannot be blank. Please set Saving Balance Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    Return False
                End If

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'If .Item(cboInterestRate.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Saving Interest Rate cannot be blank. Please set Saving Interest Rate in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'SHANI [12 JAN 2015]--END 

                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                If CDec(.Item(cboBalanceAmount.Text).ToString.Trim) < CDec(.Item(cboTotalContribution.Text).ToString.Trim) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Opening Balance (Opening Contribution + Opening Interest Amount) cannot be less than Opening Contribution."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (12 Dec 2015) -- End

            End With

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetDataCombo()
        Try

            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    'SHANI [17 Mar 2015]-START
                    'Enhancement - Add Currency Field.
                    If ctrl.Name = cboCurrency.Name Then Continue For
                    'SHANI [09 Mar 2015]--END
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboBalanceAmount.Items.Add(dtColumns.ColumnName)
                cboContribution.Items.Add(dtColumns.ColumnName)
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                '' ''cboFirstname.Items.Add(dtColumns.ColumnName)
                cboInterestAmount.Items.Add(dtColumns.ColumnName)
                '' ''cboLastname.Items.Add(dtColumns.ColumnName)
                cboSavingDate.Items.Add(dtColumns.ColumnName)
                cboSavingPeriod.Items.Add(dtColumns.ColumnName)
                cboSavingScheme.Items.Add(dtColumns.ColumnName)
                cboSavingVocNo.Items.Add(dtColumns.ColumnName)
                cboTotalContribution.Items.Add(dtColumns.ColumnName)
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                cboInterestRate.Items.Add(dtColumns.ColumnName)
                'SHANI [12 JAN 2015]--END 
            Next

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If ConfigParameter._Object._SavingsVocNoType = 1 Then
                cboSavingVocNo.Enabled = False
            Else
                cboSavingVocNo.Enabled = True
            End If
            'Sohail (12 Jan 2015) -- End

            'SHANI [17 MAR 2015]-START
            'Enhancement - Add Currency Field.
            Dim dsList As New DataSet
            Dim objExRate As New clsExchangeRate
            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                With cboCurrency
                    .ValueMember = "countryunkid"
                    .DisplayMember = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    .SelectedValue = 0
                End With
            End If
            'SHANI [17 MAR 2015]--END 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
            AddHandler cboComboBox.Validating, AddressOf Combobox_SelectedIndexChanged
            'Sohail (12 Jan 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If (cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Saving(s)."), enMsgBoxStyle.Information)
                cboEmployeeCode.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If (cboSavingVocNo.Text).ToString.Trim.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Saving Voc. No. cannot be blank. Please set Saving Voc. No. in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
            '    cboSavingVocNo.Focus() 'Sohail (12 Jan 2015)
            '    Return False
            'End If
            If ConfigParameter._Object._SavingsVocNoType = 0 Then
                If (cboSavingVocNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Saving Voc. No. cannot be blank. Please set Saving Voc. No. in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                    cboSavingVocNo.Focus()
                    Return False
                End If
            End If
            'Sohail (12 Jan 2015) -- End

            If (cboSavingPeriod.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Saving Assignment Period cannot be blank. Please set Saving Assignment Period in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboSavingPeriod.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If (cboSavingDate.Text).ToString.Trim.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Assignment Date cannot be blank. Please set Saving Assignment Date in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
            '    cboSavingDate.Focus() 'Sohail (12 Jan 2015)
            '    Return False
            'End If
            'Sohail (12 Jan 2015) -- End
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            If (cboSavingDate.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Assignment Date cannot be blank. Please set Saving Assignment Date in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboSavingDate.Focus()
                Return False
            End If
            'Sohail (12 Dec 2015) -- End

            If (cboSavingScheme.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Saving Scheme cannot be blank. Please set Saving Scheme in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboSavingScheme.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            If (cboContribution.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Saving Contribution cannot be blank. Please set Saving Contribution in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboContribution.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            If (cboTotalContribution.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Total Contibution cannot be blank. Please set Total Contribution in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboTotalContribution.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If (cboInterestAmount.Text).ToString.Trim.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
            '    Return False
            'End If
            If (cboInterestRate.Text).ToString.Trim.Length > 0 AndAlso (cboInterestAmount.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboInterestAmount.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If
            'SHANI [12 JAN 2015]--END 

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            If (cboInterestAmount.Text).ToString.Trim.Length > 0 AndAlso (cboInterestRate.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Interest Rate cannot be blank. Please set Interest Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboInterestAmount.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If
            'Sohail (12 Dec 2015) -- End

            If (cboBalanceAmount.Text).ToString.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Saving Balance Amount cannot be blank. Please set Saving Balance Amount in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboBalanceAmount.Focus() 'Sohail (12 Jan 2015)
                Return False
            End If

            'SHANI [17 MAR 2015]-START
            'Enhancement - Add Currency Field.
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Contribution Currency cannot be blank. Please set Contribution Currency in order to import Employee Saving(s)."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
            End If
            'SHANI [17 MAR 2015]-END
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objSaving As clsSaving_Tran
            Dim objSavingScheme As New clsSavingScheme
            Dim objEmp As New clsEmployee_Master
            Dim objPeriod As New clscommom_period_Tran

            Dim intSavingScheme As Integer = -1
            Dim intEmployeeId As Integer = -1
            Dim intPeriodUnkid As Integer = -1
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Dim mdtContribution As New DataTable
            Dim mdtInterestRate As New DataTable

            Dim objContribution As New clsSaving_contribution_tran
            Dim objInterestRate As New clsSaving_interest_rate_tran

            objContribution._Savingtranunkid = -1
            mdtContribution = objContribution._DataTable

            objInterestRate._Savingtranunkid = -1
            mdtInterestRate = objInterestRate._DataTable

            'SHANI [12 JAN 2015]--END 

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmployeeId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmployeeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Code Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF PERIOD PRESENT.
                If dtRow.Item("assignperiod").ToString.Trim.Length > 0 Then
                    intPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("assignperiod").ToString.Trim, enModuleReference.Payroll)
                    If intPeriodUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = dtRow.Item("assignperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 14, ", Period Name Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        'Sohail (12 Jan 2015) -- Start
                        'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPeriod._Periodunkid = intPeriodUnkid
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (12 Dec 2015) -- Start
                        'Enhancement - Provide Deposit feaure in Employee Saving.
                        'dtRow.Item("savdate") = objPeriod._End_Date.ToShortDateString '[Default End date of selected period]
                        'Sohail (12 Dec 2015) -- End
                        'Sohail (12 Jan 2015) -- End
                        If IsDate(dtRow.Item("savdate")) Then
                            'objPeriod._Periodunkid = intPeriodUnkid 'Sohail (12 Jan 2015
                            If CDate(dtRow.Item("savdate")) >= objPeriod._Start_Date.Date And CDate(dtRow.Item("savdate")) <= objPeriod._End_Date.Date Then
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Effective date should be between period start date and end date.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                            'SHANI [12 JAN 2015]-START
                            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                            If objPeriod._Statusid = enStatusType.Close Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Period Is Closed.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                                'Sohail (12 Jan 2015) -- Start
                                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                            Else
                                Dim objTnALeaveTran As New clsTnALeaveTran
                                If objTnALeaveTran.IsPayrollProcessDone(intPeriodUnkid, intEmployeeId.ToString, objPeriod._End_Date.Date, enModuleReference.Payroll) = True Then
                                    dtRow.Item("image") = imgError
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Process Payroll is already done for last date of selected period for this employee.")
                                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                                    dtRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If
                                'Sohail (12 Jan 2015) -- End
                            End If
                            'SHANI [12 JAN 2015]--END 
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 19, "Invalid Date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                '------------------------------ CHECKING IF SAVINGS SCHEME PRESENT.
                If dtRow.Item("savingscheme").ToString.Trim.Length > 0 Then
                    intSavingScheme = objSavingScheme.GetSavingSchemeUnkid(dtRow.Item("savingscheme").ToString.Trim)
                    If intSavingScheme <= 0 Then
                        If objSavingScheme IsNot Nothing Then objSavingScheme = Nothing
                        objSavingScheme = New clsSavingScheme

                        objSavingScheme._Savingschemecode = dtRow.Item("savingscheme").ToString.Trim
                        objSavingScheme._Savingschemename = dtRow.Item("savingscheme").ToString.Trim
                        objSavingScheme._Mincontribution = CDec(dtRow.Item("contribution"))
                        objSavingScheme._Userunkid = User._Object._Userunkid
                        'SHANI [12 JAN 2015]-START
                        'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                        objSavingScheme._DefualtIntRate = 0
                        objSavingScheme._DefualtContribution = CDec(dtRow.Item("contribution"))
                        'SHANI [12 JAN 2015]--END 
                        If objSavingScheme.Insert Then
                            intSavingScheme = objSavingScheme._Savingschemeunkid
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objSavingScheme._Savingschemecode & "/" & objSavingScheme._Savingschemename & " : " & objSavingScheme._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                If CDec(dtRow.Item("contribution")) <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Contribution Amount should be greater than Zero.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                'Sohail (12 Jan 2015) -- End

                objSaving = New clsSaving_Tran
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                '''' Insert Contribution Reocord In datatable
                mdtContribution.Rows.Clear()
                Dim dRowContribution As DataRow = mdtContribution.NewRow
                dRowContribution.Item("savingcontributiontranunkid") = -1
                dRowContribution.Item("savingtranunkid") = -1
                dRowContribution.Item("period_name") = ""
                dRowContribution.Item("periodunkid") = intPeriodUnkid
                objSavingScheme._Savingschemeunkid = intSavingScheme
                If CDec(dtRow.Item("contribution")) >= objSavingScheme._Mincontribution Then
                    dRowContribution.Item("contribution") = CDec(dtRow.Item("contribution"))
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objSavingScheme._Savingschemename & " : Contribution shoould be greater than minimum contribution."
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                dRowContribution.Item("contribution") = CDec(dtRow.Item("contribution"))
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'dRowContribution.Item("contributiondate") = CStr(CDate(objPeriod._End_Date))
                dRowContribution.Item("contributiondate") = CStr(CDate(dtRow.Item("savdate")))
                'Sohail (12 Dec 2015) -- End
                dRowContribution.Item("userunkid") = User._Object._Userunkid
                dRowContribution.Item("isvoid") = False
                dRowContribution.Item("voiduserunkid") = -1
                dRowContribution.Item("voiddatetime") = DBNull.Value
                dRowContribution.Item("voidreason") = ""
                dRowContribution.Item("AUD") = "A"
                dRowContribution.Item("GUID") = Guid.NewGuid.ToString
                mdtContribution.Rows.Add(dRowContribution)

                '''' Insert Interest Rate Reocord In datatable
                mdtInterestRate.Rows.Clear()
                Dim dRowInterestRate As DataRow = mdtInterestRate.NewRow
                dRowInterestRate.Item("savinginterestratetranunkid") = -1
                dRowInterestRate.Item("savingtranunkid") = -1
                dRowInterestRate.Item("period_name") = ""
                dRowInterestRate.Item("periodunkid") = intPeriodUnkid
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'dRowInterestRate.Item("effectivedate") = objPeriod._End_Date
                dRowInterestRate.Item("effectivedate") = CDate(dtRow.Item("savdate"))
                'Sohail (12 Dec 2015) -- End
                If dtRow.Item("interest_rate").ToString().Trim.Length > 0 AndAlso (cboInterestRate.Text).ToString.Trim.Length > 0 Then
                    dRowInterestRate.Item("interest_rate") = CDec(dtRow.Item("interest_rate"))
                Else
                    dRowInterestRate.Item("interest_rate") = objSavingScheme._DefualtIntRate
                End If
                dRowInterestRate.Item("userunkid") = User._Object._Userunkid
                dRowInterestRate.Item("isvoid") = False
                dRowInterestRate.Item("voiduserunkid") = -1
                dRowInterestRate.Item("voiddatetime") = DBNull.Value
                dRowInterestRate.Item("voidreason") = ""
                dRowInterestRate.Item("AUD") = "A"
                dRowInterestRate.Item("GUID") = Guid.NewGuid.ToString
                mdtInterestRate.Rows.Add(dRowInterestRate)

                'SHANI [12 JAN 2015]--END 

                ''''''''''''''''' Inerst Saving Master Entry'''''''''''''''''''''''

                objSaving._Balance_Amount = CDec(dtRow.Item("balance_amount"))
                objSaving._Broughtforward = False
                objSaving._Contribution = CDec(0) 'Sohail (12 Jan 2015)
                If IsDate(dtRow.Item("savdate")) Then
                    objSaving._Effectivedate = dtRow.Item("savdate")
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Invalid Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                objSaving._Employeeunkid = intEmployeeId
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                If dtRow.Item("interest_rate").ToString().Trim.Length > 0 AndAlso (cboInterestRate.Text).ToString.Trim.Length > 0 Then
                    objSaving._Interest_Rate = CDec(dtRow.Item("interest_rate"))
                Else
                    objSaving._Interest_Rate = objSavingScheme._DefualtIntRate
                End If
                'Sohail (12 Dec 2015) -- End
                objSaving._Interest_Amount = CDec(dtRow.Item("interest_amount"))
                objSaving._Payperiodunkid = intPeriodUnkid
                objSaving._Total_Contribution = CDec(dtRow.Item("total_contribution"))
                objSaving._Userunkid = User._Object._Userunkid
                objSaving._Voiddatetime = Nothing
                objSaving._VoidReason = ""
                objSaving._Voiduserunkid = -1
                objSaving._Voucherno = dtRow.Item("vocno")
                objSaving._Savingschemeunkid = intSavingScheme
                objSaving._Savingstatus = 1
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                objSaving._Contribution = CDec(dtRow.Item("contribution"))
                objSaving._Bf_Amount = CDec(dtRow.Item("total_contribution"))
                objSaving._Bf_Balance = CDec(dtRow.Item("balance_amount"))
                'Sohail (12 Jan 2015) -- End

                'SHANI [17 MAR 2015]-START
                'Enhancement - Add Currency Field.
                objSaving._Countryunkid = cboCurrency.SelectedValue
                'SHANI [17 MAR 2015]-END

                'Shani [ 12 JAN 2015 ] -- START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'If objSaving.Insert Then


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objSaving.Insert(, , mdtContribution, mdtInterestRate) Then
                If objSaving.Insert(ConfigParameter._Object._CurrentDateAndTime, , , mdtContribution, mdtInterestRate) Then
                    'Shani(24-Aug-2015) -- End

                    'Shani [ 12 JAN 2015 ] -- END
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 16, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objSaving._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 13, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmImportSavingWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportSavingWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'If cmb.Text <> "" Then
            If cmb.Text <> "" And cmb.Name <> cboCurrency.Name Then
                'SHANI [09 Mar 2015]--END

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'If cr.Name <> cmb.Name Then
                        If cr.Name <> cmb.Name AndAlso cr.Name <> cboCurrency.Name Then
                            'SHANI [09 Mar 2015]--END

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportEmp_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportEmp.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportEmp_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportEmp.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        If IsValid() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case eZeeWizImportEmp.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportEmp_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeWizImportEmp.CancelText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_CancelText", Me.eZeeWizImportEmp.CancelText)
            Me.eZeeWizImportEmp.NextText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_NextText", Me.eZeeWizImportEmp.NextText)
            Me.eZeeWizImportEmp.BackText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_BackText", Me.eZeeWizImportEmp.BackText)
            Me.eZeeWizImportEmp.FinishText = Language._Object.getCaption(Me.eZeeWizImportEmp.Name & "_FinishText", Me.eZeeWizImportEmp.FinishText)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
            Me.lblSavingDate.Text = Language._Object.getCaption(Me.lblSavingDate.Name, Me.lblSavingDate.Text)
            Me.lblSavingPeriod.Text = Language._Object.getCaption(Me.lblSavingPeriod.Name, Me.lblSavingPeriod.Text)
            Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
            Me.lblContribution.Text = Language._Object.getCaption(Me.lblContribution.Name, Me.lblContribution.Text)
            Me.lblTotalContribution.Text = Language._Object.getCaption(Me.lblTotalContribution.Name, Me.lblTotalContribution.Text)
            Me.lblInterestAmount.Text = Language._Object.getCaption(Me.lblInterestAmount.Name, Me.lblInterestAmount.Text)
            Me.lblBalanceAmount.Text = Language._Object.getCaption(Me.lblBalanceAmount.Name, Me.lblBalanceAmount.Text)
            Me.lblSavingVocNo.Text = Language._Object.getCaption(Me.lblSavingVocNo.Name, Me.lblSavingVocNo.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblInterestRate.Text = Language._Object.getCaption(Me.lblInterestRate.Name, Me.lblInterestRate.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Employee Code cannot be blank. Please set Employee Code in order to import Saving(s).")
            Language.setMessage(mstrModuleName, 3, "Saving Voc. No. cannot be blank. Please set Saving Voc. No. in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 4, "Saving Assignment Date cannot be blank. Please set Saving Assignment Date in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 5, "Saving Assignment Period cannot be blank. Please set Saving Assignment Period in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 6, "Saving Scheme cannot be blank. Please set Saving Scheme in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 7, "Saving Contribution cannot be blank. Please set Saving Contribution in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 8, "Total Contibution cannot be blank. Please set Total Contribution in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 9, "Interest Amount cannot be blank. Please set Interest Amount in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 10, "Saving Balance Amount cannot be blank. Please set Saving Balance Amount in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 11, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 12, "Employee Code Not Found.")
            Language.setMessage(mstrModuleName, 13, "Fail")
            Language.setMessage(mstrModuleName, 14, ", Period Name Not Found.")
            Language.setMessage(mstrModuleName, 15, "Effective date should be in between period start date and end date.")
            Language.setMessage(mstrModuleName, 16, "Success")
            Language.setMessage(mstrModuleName, 17, "Contribution Amount should be greater than Zero.")
            Language.setMessage(mstrModuleName, 18, "Process Payroll is already done for last date of selected period for this employee.")
            Language.setMessage(mstrModuleName, 19, "Invalid Date.")
            Language.setMessage(mstrModuleName, 20, "Invalid Date.")
            Language.setMessage(mstrModuleName, 21, "Period Is Closed.")
            Language.setMessage(mstrModuleName, 22, "Contribution Currency cannot be blank. Please set Contribution Currency in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 23, "Interest Rate cannot be blank. Please set Interest Amount in order to import Employee Saving(s).")
            Language.setMessage(mstrModuleName, 24, "Sorry, Opening Balance (Opening Contribution + Opening Interest Amount) cannot be less than Opening Contribution.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class