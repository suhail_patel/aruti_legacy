﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSavingSchemeList
    Private ReadOnly mstrModuleName As String = "frmSavingSchemeList"
    Private objSavingScheme As clsSavingScheme

#Region " Private Methods "
    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dsSavingScheme As New DataSet
        Dim dtTable As DataTable
        Dim objExchangeRate As New clsExchangeRate

        Try

            If User._Object.Privilege._AllowToViewSavingSchemeList = True Then   'Pinkal (09-Jul-2012) -- Start

                objSavingScheme = New clsSavingScheme
                objExchangeRate = New clsExchangeRate

                objExchangeRate._ExchangeRateunkid = 1
                dsSavingScheme = objSavingScheme.GetList("List")

                If CInt(cboSavingScheme.SelectedValue) > 0 Then
                    strSearching &= "AND savingschemeunkid=" & CInt(cboSavingScheme.SelectedValue) & " "
                End If

                If txtMonthlyMinContribution.Decimal <> 0 Then 'Sohail (11 May 2011)
                    strSearching &= "AND mincontribution =" & txtMonthlyMinContribution.Decimal & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtTable = New DataView(dsSavingScheme.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsSavingScheme.Tables("List")
                End If


                Dim lvItem As ListViewItem

                lvSavingScheme.Items.Clear()

                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("savingschemecode").ToString
                    lvItem.SubItems.Add(drRow("savingschemename").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvItem.SubItems.Add(Format(CDec(drRow("mincontribution")), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                    lvItem.Tag = drRow("savingschemeunkid")
                    lvSavingScheme.Items.Add(lvItem)

                Next

                If lvSavingScheme.Items.Count > 11 Then
                    colhDescription.Width = 280 - 18
                Else
                    colhDescription.Width = 280
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            dsSavingScheme.Dispose()
            objExchangeRate = Nothing
        End Try

    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet
        objSavingScheme = New clsSavingScheme
        Try
            dsList = objSavingScheme.getComboList(True, "List")
            cboSavingScheme.ValueMember = "savingschemeunkid"
            cboSavingScheme.DisplayMember = "name"
            cboSavingScheme.DataSource = dsList.Tables("List")
            cboSavingScheme.SelectedValue = 0


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddSavingScheme
            btnEdit.Enabled = User._Object.Privilege._EditSavingScheme
            btnDelete.Enabled = User._Object.Privilege._DeleteSavingScheme
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmSavingSchemeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete Then
            btnDelete.PerformClick()
        End If
    End Sub
    Private Sub frmSavingSchemeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmSavingSchemeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSavingScheme = Nothing
    End Sub

    Private Sub frmSchemeSchemeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSavingScheme = New clsSavingScheme
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            Call FillList()
            Call FillCombo()

            If lvSavingScheme.Items.Count > 0 Then lvSavingScheme.Items(0).Selected = True
            lvSavingScheme.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSchemeSchemeList_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSavingScheme.SetMessages()
            objfrm._Other_ModuleNames = "clsSavingScheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmSavingScheme_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvSavingScheme.DoubleClick
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditSavingScheme = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        If lvSavingScheme.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSavingScheme.Select()
            Exit Sub
        End If
        Dim frm As New frmSavingScheme_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSavingScheme.SelectedItems(0).Index
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            If frm.displayDialog(CInt(lvSavingScheme.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            'lvSavingScheme.Items(intSelectedIndex).Selected = True
            'lvSavingScheme.EnsureVisible(intSelectedIndex)
            'lvSavingScheme.Select()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSavingScheme.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Saving Scheme from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSavingScheme.Select()
            Exit Sub
        End If
        If objSavingScheme.isUsed(CInt(lvSavingScheme.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason: This Saving Scheme is in use."), enMsgBoxStyle.Information) '?2
            lvSavingScheme.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSavingScheme.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Saving Scheme?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                '<To Do>  to set the void user and void date time
                objSavingScheme._Voiduserunkid = User._Object._Userunkid
                objSavingScheme._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.SAVINGS, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objSavingScheme._VoidReason = mstrVoidReason
                End If
                frm = Nothing



                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objSavingScheme.Delete(CInt(lvSavingScheme.SelectedItems(0).Tag))
                objSavingScheme.Delete(CInt(lvSavingScheme.SelectedItems(0).Tag), User._Object._Userunkid)
                'Shani(24-Aug-2015) -- End

                lvSavingScheme.SelectedItems(0).Remove()

                If lvSavingScheme.Items.Count <= 0 Then
                    Exit Try
                End If

                'If lvSavingScheme.Items.Count = intSelectedIndex Then
                '    intSelectedIndex = lvSavingScheme.Items.Count - 1
                '    lvSavingScheme.Items(intSelectedIndex).Selected = True
                '    lvSavingScheme.EnsureVisible(intSelectedIndex)
                'ElseIf lvSavingScheme.Items.Count <> 0 Then
                '    lvSavingScheme.Items(intSelectedIndex).Selected = True
                '    lvSavingScheme.EnsureVisible(intSelectedIndex)
                'End If
            End If
            lvSavingScheme.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

        Try
            cboSavingScheme.SelectedValue = 0
            txtMonthlyMinContribution.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
    Private Sub objbtnSearchSavingScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSavingScheme.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet

        Try
            objSavingScheme = New clsSavingScheme

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            dsList = objSavingScheme.getComboList(False, "List")
            With cboSavingScheme
                objfrm.DataSource = dsList.Tables("List")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSavingScheme_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objSavingScheme = Nothing
        End Try

    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblMinContribution.Text = Language._Object.getCaption(Me.lblMinContribution.Name, Me.lblMinContribution.Text)
            Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhSavingSchemeName.Text = Language._Object.getCaption(CStr(Me.colhSavingSchemeName.Tag), Me.colhSavingSchemeName.Text)
            Me.colhMinContribution.Text = Language._Object.getCaption(CStr(Me.colhMinContribution.Tag), Me.colhMinContribution.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Saving Scheme from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Saving Scheme. Reason: This Saving Scheme is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Saving Scheme?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class