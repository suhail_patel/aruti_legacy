﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpenseApprovalList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpenseApprovalList"
    Private objClaimReqApproverTran As clsclaim_request_approval_tran
    Dim objExpApprover As clsExpenseApprover_Master
    Dim dsPedingList As DataSet = Nothing
    Dim mstrAdvanceFilter As String = ""
#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try
      
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End


            With cboExpCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            dsCombo = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, False, "List", True)


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "Name"
                .DataSource = dtab
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnChangeStatus.Enabled = User._Object.Privilege._AllowtoProcessClaimExpenseForm
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        Try

            If User._Object.Privilege._AllowtoViewProcessClaimExpenseFormList = False Then Exit Sub

            Me.Cursor = Cursors.WaitCursor

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpCategory.SelectedValue) & "' "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                StrSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text & "%' "
            End If

            If dtpFDate.Checked = True AndAlso dtpTDate.Checked = True Then
                StrSearch &= "AND cmclaim_request_master.transactiondate >= '" & eZeeDate.convertDate(dtpFDate.Value) & "' AND cmclaim_request_master.transactiondate <= '" & eZeeDate.convertDate(dtpTDate.Value) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                If CInt(cboStatus.SelectedValue) = 6 Then
                    StrSearch &= "AND cmclaim_approval_tran.iscancel = 1"
                Else
                    StrSearch &= "AND cmclaim_approval_tran.iscancel = 0 AND cmclaim_request_master.statusunkid = " & CInt(cboStatus.SelectedValue)
                End If
            End If

            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If

            dsPedingList = objClaimReqApproverTran.GetApproverExpesneList("List", True, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                                            , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                                                                            , CInt(cboExpCategory.SelectedValue), True, True, -1, StrSearch, -1)

            StrSearch = ""


            'START FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            If dsPedingList.Tables("List").Rows.Count > 0 Then
                Dim drRow As DataRow() = dsPedingList.Tables("List").Select("mapuserunkid = " & CInt(User._Object._Userunkid), "")
                If drRow.Length > 0 Then
                    Dim objmapuser As New clsapprover_Usermapping
                    objmapuser.GetData(enUserType.crApprover, CInt(drRow(0)("crapproverunkid")), , )
                    objExpApprover._crApproverunkid = objmapuser._Approverunkid
                End If
            End If

            Dim objExpAppLevel As New clsExApprovalLevel
            objExpAppLevel._Crlevelunkid = objExpApprover._crLevelunkid

            'END FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            If chkMyApprovals.Checked AndAlso objExpAppLevel._Crlevelunkid > 0 Then
                StrSearch &= "AND mapuserunkid = " & CInt(User._Object._Userunkid)
            Else
                chkMyApprovals.Checked = False
            End If


            StrSearch &= "AND visibleid <> -1 "


            If mstrAdvanceFilter.Length > 0 Then
                StrSearch &= "AND " & mstrAdvanceFilter
            End If

            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dTable = New DataView(dsPedingList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsPedingList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If


            Dim mstrStaus As String = ""
            Dim mintClaimFormunkid As Integer = 0
            Dim dList As DataTable = Nothing

            lvClaimRequestList.Items.Clear()


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            Dim lvArray As New List(Of ListViewItem)
            lvClaimRequestList.BeginUpdate()
            'Pinkal (10-Jun-2020) -- End


            For Each dRow As DataRow In dTable.Rows
                Dim lvitem As New ListViewItem
                lvitem.Tag = dRow.Item("crmasterunkid").ToString
                lvitem.Text = dRow.Item("claimrequestno").ToString
                lvitem.SubItems.Add(dRow.Item("employeecode").ToString)
                lvitem.SubItems.Add(dRow.Item("employeename").ToString)
                lvitem.SubItems.Add(dRow.Item("approvername").ToString & " - " & dRow("crlevelname").ToString)
                lvitem.SubItems.Add(dRow.Item("period").ToString)
                lvitem.SubItems.Add(dRow.Item("expensetype").ToString)

                If IsDBNull(dRow.Item("approvaldate")) Then
                    lvitem.SubItems.Add("")
                Else
                    lvitem.SubItems.Add(eZeeDate.convertDate(dRow.Item("approvaldate").ToString).ToShortDateString)
                End If

                lvitem.SubItems.Add(Format(CDec(dRow.Item("amount")), GUI.fmtCurrency))


                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

                'mstrStaus = ""
                'If mintClaimFormunkid <> CInt(dRow("crmasterunkid").ToString()) Then
                '    dList = New DataView(dsPedingList.Tables("List"), "employeeunkid = " & CInt(dRow("employeeunkid")) & " AND crmasterunkid = " & CInt(dRow("crmasterunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                '    mintClaimFormunkid = CInt(dRow("crmasterunkid"))
                'End If
                'If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                '    Dim dr As DataRow() = dList.Select("crpriority >= " & CInt(dRow("crpriority")))
                '    If dr.Length > 0 Then
                '        For i As Integer = 0 To dr.Length - 1
                '            If CInt(dRow("statusunkid")) = 1 Then
                '                mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & dRow("approvername").ToString()
                '                Exit For
                '            ElseIf CInt(dRow("statusunkid")) = 2 Then
                '                If CInt(dr(i)("statusunkid")) = 1 Then
                '                    mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & dr(i)("approvername").ToString()
                '                    Exit For
                '                ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                '                    mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & dr(i)("approvername").ToString()
                '                    Exit For
                '                End If
                '            ElseIf CInt(dRow("statusunkid")) = 3 Then
                '                mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & dRow("approvername").ToString()
                '                Exit For
                '            End If
                '        Next
                '    End If
                'End If
                'If mstrStaus <> "" Then
                '    lvitem.SubItems.Add(mstrStaus)
                'Else
                '    lvitem.SubItems.Add(dRow.Item("status").ToString)
                'End If
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                lvitem.SubItems.Add(info1.ToTitleCase(dRow.Item("AppprovalStatus").ToString().ToLower()))
                'Pinkal (13-Aug-2020) -- End

                lvitem.SubItems.Add(dRow.Item("employeeunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("crapproverunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("approveremployeeunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("statusunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("iscancel").ToString)
                lvitem.SubItems.Add(dRow.Item("mapuserunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("crpriority").ToString)
                lvitem.SubItems.Add(eZeeDate.convertDate(dRow.Item("tdate").ToString).ToShortDateString)
                lvitem.SubItems.Add(dRow.Item("isexternalapprover").ToString)

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                'lvClaimRequestList.Items.Add(lvitem)
                lvArray.Add(lvitem)
                'Pinkal (10-Jun-2020) -- End
            Next


            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            lvClaimRequestList.Items.AddRange(lvArray.ToArray)
            'Pinkal (10-Jun-2020) -- End

            lvClaimRequestList.GridLines = False
            lvClaimRequestList.GroupingColumn = colhClaimNo
            lvClaimRequestList.DisplayGroups(True)

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            lvArray.Clear()
            lvArray = Nothing
            lvClaimRequestList.EndUpdate()
            'Pinkal (10-Jun-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExpenseApprovalList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objClaimReqApproverTran = New clsclaim_request_approval_tran
        objExpApprover = New clsExpenseApprover_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()



            Dim objMapping As New clsapprover_Usermapping
            objMapping.GetData(enUserType.crApprover, , User._Object._Userunkid, )

            objExpApprover = New clsExpenseApprover_Master
            objExpApprover._crApproverunkid = objMapping._Approverunkid


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objExpApprover._Employeeunkid
            'txtExpApprover.Text = objEmployee._Firstname & " " & objEmployee._Surname

            Dim dsAppr As New DataSet
            dsAppr = objExpApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "", True, objExpApprover._crApproverunkid)
            If dsAppr.Tables("List").Rows.Count > 0 Then
                txtExpApprover.Text = CStr(dsAppr.Tables("List").Rows(0)("ename"))
            End If
            dsAppr.Dispose()


            'Pinkal (01-Mar-2016) -- End

            RemoveHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged
            chkMyApprovals.Checked = True
            AddHandler chkMyApprovals.CheckedChanged, AddressOf chkMyApprovals_CheckedChanged

            lvClaimRequestList.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpenseApprovalList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsclaim_request_approval_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsclaim_request_approval_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            Dim ObjFrm As New frmExpenseApproval

            If lvClaimRequestList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Expense Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvClaimRequestList.Select()
                Exit Sub
            End If

            If User._Object._Userunkid <> CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhMapUserId.Index).Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You can't Edit this Expense detail. Reason: You are logged in into another user login account."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CBool(lvClaimRequestList.SelectedItems(0).SubItems(objcolhisCancel.Index).Text) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You can't Edit this Expense detail. Reason: This Expense is already Cancelled."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then 'FOR APPROVED
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then 'FOR REJECTED
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'SHANI (06 JUN 2015) -- End


            'START FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT
            Dim mintApproverId As Integer = 0
            Dim dtList As DataTable = New DataView(dsPedingList.Tables("List"), "employeeunkid = " & CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhemployeeunkid.Index).Text) _
                                                    & " AND crmasterunkid = " & CInt(lvClaimRequestList.SelectedItems(0).Tag) _
                                                    & " AND crapproverunkid <> " & CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text), "statusunkid desc", DataViewRowState.CurrentRows).ToTable


            If dtList.Rows.Count > 0 Then
                Dim objClaimMst As New clsclaim_request_master
                Dim objExpapprlevel As New clsExApprovalLevel
                Dim objLeaveApprover As clsleaveapprover_master = Nothing
                Dim objLeaveLevel As clsapproverlevel_master = Nothing

                objClaimMst._Crmasterunkid = CInt(dtList.Rows(0)("crmasterunkid"))


                'Pinkal (18-Feb-2016) -- Start
                'Enhancement - CR Changes for ASP as per Rutta's Request.

                'Pinkal (29-Feb-2016) -- Start
                'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
                'If objClaimMst._FromModuleId = enExpFromModuleID.FROM_LEAVE AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                    'Pinkal (29-Feb-2016) -- End
                    'Pinkal (18-Feb-2016) -- End
                    objLeaveApprover = New clsleaveapprover_master
                    objLeaveLevel = New clsapproverlevel_master
                    objLeaveApprover._Approverunkid = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text)
                    objLeaveLevel._Levelunkid = objLeaveApprover._Levelunkid
                    mintApproverId = objLeaveApprover._Approverunkid
                Else
                    mintApproverId = objExpApprover._crApproverunkid
                End If

                objExpapprlevel._Crlevelunkid = objExpApprover._crLevelunkid

                For i As Integer = 0 To dtList.Rows.Count - 1


                    'Pinkal (18-Feb-2016) -- Start
                    'Enhancement - CR Changes for ASP as per Rutta's Request.

                    'Pinkal (29-Feb-2016) -- Start
                    'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
                    'If objClaimMst._FromModuleId = enExpFromModuleID.FROM_LEAVE AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                    If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                        'Pinkal (29-Feb-2016) -- End

                        'Pinkal (18-Feb-2016) -- End

                        If objLeaveLevel._Priority > CInt(dtList.Rows(i)("crpriority")) Then

                            'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                            Dim dList As DataTable = New DataView(dtList, "crlevelunkid = " & CInt(dtList.Rows(i)("crlevelunkid")) & " AND statusunkid = 1", "", DataViewRowState.CurrentRows).ToTable
                            If dList.Rows.Count > 0 Then Continue For
                            'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                            If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Expense detail. Reason: This Expense approval is still pending."), enMsgBoxStyle.Information)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                                Exit Sub

                            End If

                        ElseIf objLeaveLevel._Priority <= CInt(dtList.Rows(i)("crpriority")) Then

                            If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), enMsgBoxStyle.Information)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                                Exit Sub

                            End If

                        End If

                    Else

                        If objExpapprlevel._Crpriority > CInt(dtList.Rows(i)("crpriority")) Then

                            'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                            Dim dList As DataTable = New DataView(dtList, "crlevelunkid = " & CInt(dtList.Rows(i)("crlevelunkid")) & " AND statusunkid = 1", "", DataViewRowState.CurrentRows).ToTable
                            If dList.Rows.Count > 0 Then Continue For
                            'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                            If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Expense detail. Reason: This Expense approval is still pending."), enMsgBoxStyle.Information)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                                Exit Sub

                            End If

                        ElseIf objExpapprlevel._Crpriority <= CInt(dtList.Rows(i)("crpriority")) Then

                            If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), enMsgBoxStyle.Information)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                                Exit Sub

                            End If

                        End If

                    End If

                Next
                objClaimMst = Nothing
                objLeaveApprover = Nothing
                objLeaveLevel = Nothing
            End If

            'END FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT


            'Shani(08-Aug-2015) -- Start
            'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
            'If CDate(lvClaimRequestList.SelectedItems(0).SubItems(colhDate.Index).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date _
            'And mintApproverId = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) Then

            '    If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then   'FOR APPROVED
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), enMsgBoxStyle.Information)
            '        Exit Sub

            '    ElseIf CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If


            'ElseIf CDate(lvClaimRequestList.SelectedItems(0).SubItems(colhDate.Index).Text).Date >= ConfigParameter._Object._CurrentDateAndTime.Date _
            ' And mintApproverId = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) Then

            '    If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If

            'End If

            If CDate(lvClaimRequestList.SelectedItems(0).SubItems(objcolhTranscationDate.Index).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date _
            And mintApproverId = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) Then

                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then   'FOR APPROVED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), enMsgBoxStyle.Information)
                    Exit Sub

                ElseIf CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                    Exit Sub
                End If


            ElseIf CDate(lvClaimRequestList.SelectedItems(0).SubItems(objcolhTranscationDate.Index).Text).Date >= ConfigParameter._Object._CurrentDateAndTime.Date _
             And mintApproverId = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) Then

                If CInt(lvClaimRequestList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 3 Then   'FOR REJECTED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If
            'Shani(08-Aug-2015) -- End


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If ObjFrm.displayDialog(CInt(lvClaimRequestList.SelectedItems(0).Tag), CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) _
            '                                , CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverEmpID.Index).Text), CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhPriority.Index).Text)) Then

            If ObjFrm.displayDialog(CInt(lvClaimRequestList.SelectedItems(0).Tag), CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text) _
                                      , CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverEmpID.Index).Text), CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhPriority.Index).Text) _
                                      , CBool(lvClaimRequestList.SelectedItems(0).SubItems(objcolhIsExternalApprover.Index).Text)) Then

                'Pinkal (01-Mar-2016) -- End

                Fill_List()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboExpCategory.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            dtpFDate.Checked = False
            dtpTDate.Checked = False
            cboStatus.SelectedValue = 0
            txtClaimNo.Text = ""
            mstrAdvanceFilter = ""

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            'Fill_List()
            lvClaimRequestList.Items.Clear()
            'Pinkal (22-Jun-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Expense Category is compulsory information.Please Select Expense Category."), enMsgBoxStyle.Information)
                cboExpCategory.Select()
                Exit Sub
            End If
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                    cboEmployee.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Expense Category is compulsory information.Please Select Expense Category."), enMsgBoxStyle.Information)
                cboExpCategory.Select()
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            Fill_List()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview Event"

    Private Sub lvClaimRequestList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvClaimRequestList.SelectedIndexChanged
        Dim dsList As DataSet = Nothing
        Try
            If lvClaimRequestList.SelectedItems.Count > 0 Then
                Dim objMapping As New clsapprover_Usermapping
                dsList = objMapping.GetList("Mapping", enUserType.crApprover)

                If dsList.Tables("Mapping").Rows.Count > 0 Then

                    Dim dtList As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid= " & CInt(User._Object._Userunkid), "", DataViewRowState.CurrentRows).ToTable
                    If dtList.Rows.Count > 0 Then
                        objExpApprover._crApproverunkid = CInt(lvClaimRequestList.SelectedItems(0).SubItems(objColhapproverunkid.Index).Text)
                    End If

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvClaimRequestList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.Name, Me.lblExpCategory.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.colhClaimNo.Text = Language._Object.getCaption(CStr(Me.colhClaimNo.Tag), Me.colhClaimNo.Text)
            Me.colhEmployeecode.Text = Language._Object.getCaption(CStr(Me.colhEmployeecode.Tag), Me.colhEmployeecode.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhExpenseApprover.Text = Language._Object.getCaption(CStr(Me.colhExpenseApprover.Tag), Me.colhExpenseApprover.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhExpType.Text = Language._Object.getCaption(CStr(Me.colhExpType.Tag), Me.colhExpType.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Expense Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "You can't Edit this Expense detail. Reason: You are logged in into another user login account.")
            Language.setMessage(mstrModuleName, 3, "You can't Edit this Expense detail. Reason: This Expense is already Cancelled.")
            Language.setMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved.")
            Language.setMessage(mstrModuleName, 5, "You can't Edit this Expense detail. Reason: This Expense approval is still pending.")
            Language.setMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected.")
            Language.setMessage(mstrModuleName, 7, "Expense Category is compulsory information.Please Select Expense Category.")
            Language.setMessage(mstrModuleName, 8, "Approved By :-")
            Language.setMessage(mstrModuleName, 9, "Rejected By :-")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class