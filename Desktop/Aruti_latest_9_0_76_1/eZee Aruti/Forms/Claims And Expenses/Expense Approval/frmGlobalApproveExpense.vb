﻿'Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Web

#End Region

Public Class frmGlobalApproveExpense

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalApproveExpense"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dtTab As DataTable
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mstrWarningMsg As String = ""

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True)
            With cboExpenseCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


            Dim objMasterData As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (1,3)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "Name"
                .DataSource = dtab
            End With
            objMasterData = Nothing


            Dim objcommonMst As New clsCommon_Master
            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSectorRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
            End With
            objcommonMst = Nothing


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", False)
            With dgcolhCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                'If ConfigParameter._Object._NewRequisitionRequestP2PServiceURL.Trim.Length > 0 Then
                'dtTable = dsCombo.Tables(0)
                'Else
                    Dim dRow As DataRow = dsCombo.Tables(0).NewRow
                    dRow("costcenterunkid") = "0"
                    dRow("costcentername") = ""
                    dsCombo.Tables(0).Rows.InsertAt(dRow, 0)
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                'End If
                'Pinkal (11-Sep-2019) -- End
                .DataSource = dtTable
            End With

            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With dgcolhCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables(0)
            End With

            'Pinkal (04-Feb-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillGrid()
        Dim intMode As Integer = 0
        Dim mstrSearch As String = ""
        Try

            Dim objApproval As New clsclaim_request_approval_tran


            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND cmclaim_request_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                'Pinkal (13-Jun-2016) -- Start
                'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
                'mstrSearch &= "AND cmclaim_approval_tran.expenseunkid = " & CInt(cboExpense.SelectedValue) & " "
                mstrSearch &= "AND cmclaim_request_master.crmasterunkid IN (" & objApproval.GetClaimFormFromExpSector(CInt(cboExpense.SelectedValue), 0) & ")"
                'Pinkal (13-Jun-2016) -- End
            End If

            'If CInt(cboSectorRoute.SelectedValue) > 0 Then
            '    mstrSearch &= "AND cmclaim_approval_tran.secrouteunkid = " & CInt(cboSectorRoute.SelectedValue) & " "
            'End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrSearch &= "AND " & mstrAdvanceFilter
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Dim mblnIsExternalApprover As Boolean = False
            If CInt(cboApprover.SelectedValue) > 0 Then
                Dim dr As DataRowView = CType(cboApprover.SelectedItem, DataRowView)
                If dr IsNot Nothing Then
                    mblnIsExternalApprover = CBool(dr("isexternalapprover"))
                End If
            End If
            'Pinkal (01-Mar-2016) -- End




            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'Dim dtList As DataTable = objApproval.GetGlobalApprovalData(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
            '                                                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
            '                                                                                        , ConfigParameter._Object._UserAccessModeSetting, True, False, "List", ConfigParameter._Object._LeaveBalanceSetting _
            '                                                                                        , ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpenseCategory.SelectedValue) _
            '                                                                                        , CInt(cboApprover.SelectedValue), mstrSearch, True)

            Dim dtList As DataTable = objApproval.GetGlobalApprovalData(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                    , ConfigParameter._Object._UserAccessModeSetting, True, False, "List", ConfigParameter._Object._LeaveBalanceSetting _
                                                                                                    , ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(cboExpenseCategory.SelectedValue) _
                                                                                             , CInt(cboApprover.SelectedValue), mstrSearch, True, mblnIsExternalApprover)

            'Pinkal (01-Mar-2016) -- End

            dgvData.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhIsLeaveEncashment.DataPropertyName = "isleaveencashment"
            objdgcolhIsAccrue.DataPropertyName = "isaccrue"
            objdgcolhExpenseID.DataPropertyName = "expenseunkid"
            objdgcolhCrmasterunkid.DataPropertyName = "crmasterunkid"
            objdgcolhCrapprovaltranunkid.DataPropertyName = "crapprovaltranunkid"
            objdgcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            objdgcolhUOMID.DataPropertyName = "uomunkid"
            dgcolhClaimNo.DataPropertyName = "ClaimNo"
            dgcolhClaimExpense.DataPropertyName = "Expense"
            dgcolhUOM.DataPropertyName = "UOM"
            dgcolhBalance.DataPropertyName = "Balance"
            dgcolhSectorRoute.DataPropertyName = "Sector"
            dgcolhAppliedQty.DataPropertyName = "AppliedQty"
            dgcolhAppliedAmount.DataPropertyName = "AppliedAmount"
            dgcolhQuantity.DataPropertyName = "quantity"
            dgcolhUnitPrice.DataPropertyName = "unitprice"
            dgcolhAmount.DataPropertyName = "amount"
            dgcolhExpenseRemark.DataPropertyName = "Expense_Remark"


            'Pinkal (13-Jun-2016) -- Start
            'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).
            dgcolhClaimRemark.DataPropertyName = "Claim_Remark"
            'Pinkal (13-Jun-2016) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dgcolhCostCenter.DataPropertyName = "costcenterunkid"
            dgcolhCurrency.DataPropertyName = "countryunkid"
            'Pinkal (04-Feb-2019) -- End



            dgvData.DataSource = dtList

            dgcolhBalance.DefaultCellStyle.Format = "#0.00"
            dgcolhAppliedQty.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhAppliedAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhQuantity.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhUnitPrice.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency


            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsGroupRow As New DataGridViewCellStyle
            objdgvsGroupRow.Font = New Font(dgvData.Font.FontFamily, 10, FontStyle.Bold)
            objdgvsGroupRow.ForeColor = Color.White
            objdgvsGroupRow.BackColor = Color.Gray
            objdgvsGroupRow.SelectionBackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                    dgvData.Rows(i).Cells(dgcolhClaimNo.Index).Style = objdgvsGroupRow
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (06-Sep-2021)-- Start
    'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
SendEmail:
                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = CInt(intCompanyID(0))
                    End If
                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        gobjEmailList.Remove(objEmail)
                        GoTo SendEmail
                    End If
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Pinkal (06-Sep-2021)-- End

#End Region

#Region " Form's Events "

    Private Sub frmGlobalApproveExpense_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            dgcolhBalance.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAppliedQty.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAppliedAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhQuantity.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhUnitPrice.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalApproveExpense_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboExpenseCategory.ValueMember
                .DisplayMember = cboExpenseCategory.DisplayMember
                .DataSource = CType(cboExpenseCategory.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboExpenseCategory.SelectedValue = objFrm.SelectedValue
                cboExpenseCategory.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExpense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExpense.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboExpense.ValueMember
                .DisplayMember = cboExpense.DisplayMember
                .DataSource = CType(cboExpense.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboExpense.SelectedValue = objFrm.SelectedValue
                cboExpense.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchExpense_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboApprover.ValueMember
                .DisplayMember = cboApprover.DisplayMember
                .DataSource = CType(cboApprover.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboApprover.SelectedValue = objFrm.SelectedValue
                cboApprover.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSecRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSecRoute.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboSectorRoute.ValueMember
                .DisplayMember = cboSectorRoute.DisplayMember
                .DataSource = CType(cboSectorRoute.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboSectorRoute.SelectedValue = objFrm.SelectedValue
                cboSectorRoute.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchSecRoute_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboExpenseCategory.Select()
                Exit Sub
            End If
            FillGrid()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExpenseCategory.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboExpense.SelectedValue = 0
            cboApprover.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""
            txtRemarks.Text = ""
            dgvData.DataSource = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If CInt(cboApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
                cboApprover.Focus()
                Exit Sub
            End If


            Dim dtTab As DataTable = CType(dgvData.DataSource, DataTable)
            If dtTab Is Nothing Then Exit Sub

            Dim dtTemp() As DataRow = Nothing
            dtTemp = dtTab.Select("IsChecked = 1 And IsGrp = True")
            If dtTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select atleast one Claim Expense Form to Approve/Reject."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemarks.Text.Trim.Length <= 0 Then  'Rejected
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Remark is compulsory information.Please Enter Remark."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtRemarks.Focus()
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            Dim objLeaveApprover As New clsleaveapprover_master
            Dim objExpApproverTran As New clsclaim_request_approval_tran
            Dim objExpAppr As New clsExpenseApprover_Master
            Dim objClaimMst As New clsclaim_request_master
            Dim mintApproverPriority As Integer = -1
            Dim mintMaxPriority As Integer = -1
            Dim mstrRejectRemark As String = ""
            Dim blnLastApprover As Boolean = False
            Dim mblnIssued As Boolean = False
            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING

            If CInt(cboApprover.SelectedValue) > 0 Then
                Dim dr As DataRowView = CType(cboApprover.SelectedItem, DataRowView)
                If dr IsNot Nothing Then
                    mintApproverPriority = CInt(dr("priority"))
                End If
            End If

            Dim mblnWarning As Boolean = False

            For i As Integer = 0 To dtTemp.Length - 1

                objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))


                Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, ConfigParameter._Object._PaymentApprovalwithLeaveApproval, FinancialYear._Object._DatabaseName _
                                                                                                                , User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboExpenseCategory.SelectedValue) _
                                                                                                                , False, True, -1, "cmapproverlevel_master.crpriority >= " & mintApproverPriority, CInt(dtTemp(i)("crmasterunkid")))

                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "", "crpriority asc", DataViewRowState.CurrentRows).ToTable
                mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))


                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                    objLeaveApprover = New clsleaveapprover_master
                    objLeaveApprover._Approverunkid = CInt(dtTemp(i)("crmasterunkid"))
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                    If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = mintApproverPriority) Then mblnIssued = True
                End If

                Dim mblnIsRejected As Boolean = False
                Dim mintApproverApplicationStatusId As Integer = 2


                For j As Integer = 0 To dtApproverTable.Rows.Count - 1
                    blnLastApprover = False
                    Dim mintApproverEmpID As Integer = -1
                    Dim mintApproverID As Integer = -1
                    Dim mdtApprovalDate As Date = Nothing

                    If ConfigParameter._Object._PaymentApprovalwithLeaveApproval AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                        objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

                        If CInt(cboApprover.SelectedValue) = objLeaveApprover._Approverunkid Then
                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now
                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objLeaveApprover._Approverunkid Then

                            Dim mintPriority As Integer = -1
                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintApproverPriority & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID

                            Else

                                mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)))

                                If mintPriority <= -1 Then
                                    mintStatusID = 2
                                    mintVisibleID = 1
                                    mintApproverApplicationStatusId = mintStatusID
                                    GoTo AssignApprover
                                ElseIf mblnIsRejected Then
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                ElseIf mintApproverPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1
                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2
                                Else
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If

                                mintApproverApplicationStatusId = mintStatusID
                            End If
                            mdtApprovalDate = Nothing
                        End If


AssignApprover:
                        mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                        mintApproverID = objLeaveApprover._Approverunkid

                    Else

                        objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(j)("crapproverunkid"))

                        If CInt(cboApprover.SelectedValue) = objExpAppr._crApproverunkid Then
                            mintStatusID = CInt(cboStatus.SelectedValue)
                            mintVisibleID = CInt(cboStatus.SelectedValue)
                            mintApproverApplicationStatusId = mintStatusID
                            mdtApprovalDate = DateTime.Now
                            If CInt(cboStatus.SelectedValue) = 1 Then
                                If mintMaxPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then blnLastApprover = True
                            ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                                mstrRejectRemark = txtRemarks.Text.Trim
                                blnLastApprover = True
                                mblnIsRejected = True
                            End If

                        ElseIf CInt(cboApprover.SelectedValue) <> objExpAppr._crApproverunkid Then

                            Dim mintPriority As Integer = -1

                            Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & mintApproverPriority & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                            If dRow.Length > 0 Then
                                mintStatusID = 2
                                mintVisibleID = 1
                                mintApproverApplicationStatusId = mintStatusID
                            Else
                                mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & mintApproverPriority)))

                                If mintPriority <= -1 Then GoTo AssignApprover1

                                If mblnIsRejected Then
                                    mintStatusID = -1
                                    mintVisibleID = -1
                                ElseIf mintApproverPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 1
                                    mintStatusID = 1
                                ElseIf mintPriority = CInt(dtApproverTable.Rows(j)("crpriority")) Then
                                    mintVisibleID = 2
                                    mintStatusID = 2
                                Else
                                    mintVisibleID = -1
                                    mintStatusID = -1
                                End If
                                mintApproverApplicationStatusId = mintStatusID
                                mdtApprovalDate = Nothing
                            End If

                        End If

AssignApprover1:
                        mintApproverEmpID = objExpAppr._Employeeunkid
                        mintApproverID = objExpAppr._crApproverunkid

                    End If

                    Dim dtFilter As DataTable = New DataView(dtTab, "IsChecked = 1 AND crmasterunkid = " & CInt(dtTemp(i)("crmasterunkid")), "", DataViewRowState.CurrentRows).ToTable
                    Dim intExpenseID As Integer = CInt(dtTemp(i)("expenseunkid"))

                    Dim drExpense As List(Of DataRow) = (From row In dtTab.AsEnumerable() Where CBool(row.Item("IsGrp")) = False AndAlso CBool(row.Item("IsChecked")) = True _
                                            AndAlso CInt(row.Item("crmasterunkid")) = objClaimMst._Crmasterunkid AndAlso CInt(row.Item("expenseunkid")) > 0 Select (row)).ToList


                    Dim sMsg As String = String.Empty

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")))


                    'Pinkal (15-Apr-2019) -- Start
                    'Enhancement - Working on Leave & CR Changes for NMB.
                    'sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")), FinancialYear._Object._YearUnkid _
                    '                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(drExpense(0)("tdate").ToString()).Date _
                    '                                                                            , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting))


                    'Pinkal (22-Jan-2020) -- Start
                    'Enhancements -  Working on OT Requisistion Reports for NMB.

                    'sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")), FinancialYear._Object._YearUnkid _
                    '                                                                           , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, CDate(drExpense(0)("ClaimDate")).Date _
                    '                                                                           , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), CInt(drExpense(0)("crmasterunkid")))

                    sMsg = objClaimMst.IsValid_Expense(CInt(dtTemp(i)("employeeunkid")), CInt(drExpense(0)("expenseunkid")), FinancialYear._Object._YearUnkid _
                                                                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, CDate(drExpense(0)("ClaimDate")).Date _
                                                                                            , CType(ConfigParameter._Object._LeaveBalanceSetting, enLeaveBalanceSetting), CInt(drExpense(0)("crmasterunkid")), False)

                    'Pinkal (22-Jan-2020) -- End

                    'Pinkal (15-Apr-2019) -- End

                    
                    'Pinkal (26-Feb-2019) -- End
                    If sMsg <> "" Then
                        Dim idx As Integer = dtTab.Rows.IndexOf(drExpense(0))
                        dgvData.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                        dgvData.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                        mblnWarning = True
                        Continue For
                    End If

                    'Pinkal (01-Mar-2016) -- End

                    Dim drRow() As DataRow = dtFilter.Select("IsGrp = False AND AUD = ''")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            dr("AUD") = "U"
                            dr.AcceptChanges()
                        Next
                        dtFilter.AcceptChanges()
                    End If

                    objExpApproverTran._DataTable = dtFilter
                    objExpApproverTran._YearId = FinancialYear._Object._YearUnkid
                    objExpApproverTran._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                    objExpApproverTran._EmployeeID = CInt(dtTemp(i)("employeeunkid"))
                    If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, CInt(dtTemp(i)("crmasterunkid")), Nothing, mstrRejectRemark, blnLastApprover, mdtApprovalDate) = False Then
                        Exit For
                    End If

                    If mintApproverApplicationStatusId = 1 Then
                        objExpApproverTran.SendMailToApprover(CInt(cboExpenseCategory.SelectedValue), ConfigParameter._Object._PaymentApprovalwithLeaveApproval, CInt(dtTemp(i)("crmasterunkid")) _
                                                                                    , dtTemp(i)("CLno").ToString(), CInt(dtTemp(i)("employeeunkid")), mintApproverPriority, 1, "crpriority > " & mintApproverPriority, FinancialYear._Object._DatabaseName _
                                                                                    , ConfigParameter._Object._EmployeeAsOnDate, Company._Object._Companyunkid, ConfigParameter._Object._ArutiSelfServiceURL.ToString() _
                                                                                    , enLogin_Mode.DESKTOP, , User._Object._Userunkid, "")

                    End If

                    objClaimMst._Crmasterunkid = CInt(dtTemp(i)("crmasterunkid"))

                    If objClaimMst._Statusunkid = 1 AndAlso mintApproverApplicationStatusId = 1 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email

                        If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                            Dim objLeaveForm As New clsleaveform
                            objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                            objLeaveForm._EmployeeCode = objEmployee._Employeecode
                            objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                            objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                            objLeaveForm._EmployeeSurName = objEmployee._Surname
                            objLeaveForm._EmpMail = objEmployee._Email
                            If objLeaveForm._Statusunkid = 7 Then 'only Issued
                                Dim objfrmsendmail As New frmSendMail
                                Dim strPath As String = objfrmsendmail.Export_ELeaveForm(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                                                        , True, ConfigParameter._Object._UserAccessModeSetting, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid, ConfigParameter._Object._LeaveBalanceSetting _
                                                                                                                        , FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date, True)

                                objfrmsendmail = Nothing

                                Dim objLeaveType As New clsleavetype_master
                                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                'objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate, objLeaveForm._Returndate, objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", CInt(dtTemp(i)("crmasterunkid")))
                                objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, objLeaveType._Leavename, objLeaveForm._Startdate, objLeaveForm._Returndate _
                                                                                   , objLeaveForm._Statusunkid, Company._Object._Companyunkid, "", mstrExportEPaySlipPath, strPath, enLogin_Mode.DESKTOP _
                                                                                   , 0, User._Object._Userunkid, "", CInt(dtTemp(i)("crmasterunkid")), objLeaveForm._Formno)
                                'Pinkal (01-Apr-2019) -- End


                                objLeaveType = Nothing
                                objLeaveForm = Nothing
                            End If
                        Else
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, "")
                            objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, "")
                            'Sohail (30 Nov 2017) -- End
                        End If

                        objEmployee = Nothing

                    ElseIf objClaimMst._Statusunkid = 3 AndAlso mintApproverApplicationStatusId = 3 Then
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objClaimMst._Employeeunkid
                        objClaimMst._EmployeeCode = objEmployee._Employeecode
                        objClaimMst._EmployeeFirstName = objEmployee._Firstname
                        objClaimMst._EmployeeMiddleName = objEmployee._Othername
                        objClaimMst._EmployeeSurName = objEmployee._Surname
                        objClaimMst._EmpMail = objEmployee._Email
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtRemarks.Text.Trim)
                        objClaimMst.SendMailToEmployee(CInt(dtTemp(i)("employeeunkid")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, User._Object._Userunkid, txtRemarks.Text.Trim)
                        'Sohail (30 Nov 2017) -- End
                        objEmployee = Nothing
                    End If

                Next

            Next

            If mblnWarning = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Some of the Claim Expense Forms cannot be Approved. Reason : It exceeds the current occurrence limit and will be highlighted in red."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                Call FillGrid()
            End If

            'Pinkal (06-Sep-2021)-- Start
            'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
            If gobjEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = Company._Object._Companyunkid
                    objThread.Start(arr)
                Else
                    Call Send_Notification(Company._Object._Companyunkid)
                End If
            End If
            'Pinkal (06-Sep-2021)-- End

            txtRemarks.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvData.CellValidating
        Try
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Sub

            If (e.ColumnIndex = dgcolhQuantity.Index OrElse e.ColumnIndex = dgcolhUnitPrice.Index) AndAlso CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsAccrue.Index).Value) = True Then

                If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhUOMID.Index).Value) = enExpUoM.UOM_QTY Then
                    If CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhBalance.Index).Value) < CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhQuantity.Index).Value) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot set quantity greater than balance set."))
                        e.Cancel = True
                        Exit Sub
                    End If

                ElseIf CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhUOMID.Index).Value) = enExpUoM.UOM_AMOUNT Then
                    If CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhBalance.Index).Value) < CDec(CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhQuantity.Index).Value) * CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhUnitPrice.Index).Value)) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot set Amount greater than balance set."))
                        e.Cancel = True
                        Exit Sub
                    End If
                End If

            End If

            If e.ColumnIndex = dgcolhQuantity.Index OrElse e.ColumnIndex = dgcolhUnitPrice.Index Then
                dgvData.Rows(e.RowIndex).Cells(dgcolhAmount.Index).Value = CDec(CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhQuantity.Index).Value) * CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhUnitPrice.Index).Value))
            End If


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If e.ColumnIndex = dgcolhQuantity.Index Then
                Dim objExpense As New clsExpense_Master
                objExpense._Expenseunkid = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhExpenseID.Index).Value)
                If objExpense._IsConsiderDependants Then
                    Dim mintEmpDepedentsCoutnForCR As Integer = 0
                    Dim objDependents As New clsDependants_Beneficiary_tran

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeunkid.Index).Value))
                    Dim objClaimMst As New clsclaim_request_master
                    objClaimMst._Crmasterunkid = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhCrmasterunkid.Index).Value)
                    Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmployeeunkid.Index).Value), objClaimMst._Transactiondate)
                    'Sohail (18 May 2019) -- End
                    'Pinkal (04-Feb-2019) -- End
                    mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
                    objDependents = Nothing
                    If mintEmpDepedentsCoutnForCR < CDec(e.FormattedValue) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit for this selected employee."), enMsgBoxStyle.Information)
                        e.Cancel = True
                        Exit Sub
                    End If
                End If


                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(e.FormattedValue) Then
                    eZeeMsgBox.Show(Language.getMessage("frmExpenseApproval", 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), enMsgBoxStyle.Information)
                    e.Cancel = True
                    Exit Sub
                End If
                'Pinkal (10-Jun-2020) -- End


                objExpense = Nothing
            End If
            'Pinkal (25-Oct-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs)
        Try
            'If (Me.dgvData.CurrentCell.ColumnIndex = dgcolhDuration.Index) And Not e.Control Is Nothing Then
            '    Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
            '    AddHandler txt.KeyPress, AddressOf tb_keypress
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCashDenomination_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            If dgvData.DataSource IsNot Nothing AndAlso dgvData.RowCount > 0 Then
                Dim chkSelect As DataGridViewCheckBoxCell = Nothing

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim cboCostCenter As DataGridViewComboBoxCell = Nothing
                Dim cboCurrency As DataGridViewComboBoxCell = Nothing
                'Pinkal (04-Feb-2019) -- End

                For i As Integer = 0 To dgvData.RowCount - 1
                    If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = False Then
                        chkSelect = CType(dgvData.Rows(i).Cells(objdgcolhCheck.Index), DataGridViewCheckBoxCell)
                        chkSelect.ReadOnly = True
                        If CBool(dgvData.Rows(i).Cells(objdgcolhIsLeaveEncashment.Index).Value) = True Then
                            dgvData.Rows(i).Cells(dgcolhUnitPrice.Index).ReadOnly = True
                        End If
                    Else
                        dgvData.Rows(i).ReadOnly = True
                        dgvData.Rows(i).Cells(objdgcolhCheck.Index).ReadOnly = False

                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        cboCostCenter = CType(dgvData.Rows(i).Cells(dgcolhCostCenter.Index), DataGridViewComboBoxCell)
                        cboCostCenter.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                        cboCostCenter.ReadOnly = True

                        cboCurrency = CType(dgvData.Rows(i).Cells(dgcolhCurrency.Index), DataGridViewComboBoxCell)
                        cboCurrency.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                        cboCurrency.ReadOnly = True
                        'Pinkal (04-Feb-2019) -- End
                    End If
                Next
                chkSelect = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)

    End Sub


#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Dim dsCombo As DataSet = Nothing
        Try

            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List", 0, False)
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With


            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval And CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                Dim objLeaveApprover As New clsleaveapprover_master
                dsCombo = objLeaveApprover.GetEmployeeFromUser(FinancialYear._Object._DatabaseName, _
                                                                                             FinancialYear._Object._YearUnkid, _
                                                                                             Company._Object._Companyunkid, _
                                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                             True, User._Object._Userunkid, User._Object.Privilege._AllowtoApproveLeave, User._Object.Privilege._AllowIssueLeave)
                Dim lstIDs As List(Of String) = (From p In dsCombo.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

                objLeaveApprover = Nothing
            Else

                Dim objEmployee As New clsEmployee_Master
                dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, False, "List", True)

            End If

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
            End With

            Dim objExpApproverMst As New clsExpenseApprover_Master
            dsCombo = objExpApproverMst.GetApproverWithFromUserLogin(ConfigParameter._Object._PaymentApprovalwithLeaveApproval, User._Object._Userunkid, CInt(cboExpenseCategory.SelectedValue))
            objExpApproverMst = Nothing
            If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsCombo.Tables(0).NewRow
                drRow("approverunkid") = 0
                drRow("Approver") = Language.getMessage(mstrModuleName, 4, "Select")
                dsCombo.Tables(0).Rows.Add(drRow)
                drRow = Nothing
            End If

            With cboApprover
                .ValueMember = "approverunkid"
                .DisplayMember = "Approver"
                .DataSource = dsCombo.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    'Pinkal (13-Jun-2016) -- Start
    'Enhancement - Working on Global Approval Expense Changes Required by GDS (Annor).

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvData.RowCount <= 0 Then Exit Sub

            Dim Query = From Row In CType(dgvData.DataSource, DataTable).AsEnumerable() Select Row

            'Pinkal (06-Sep-2021)-- Start
            'KBC Bug : Claim Retirement didn't allow other currency even if claim is approved in other currency.
            RemoveHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
            For Each rows In Query
                rows.Item("ischecked") = chkSelectAll.Checked
            Next
            AddHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
            'Pinkal (06-Sep-2021)-- End

            CType(dgvData.DataSource, DataTable).AcceptChanges()
            SetGridColor()
            SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (13-Jun-2016) -- End







    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPending.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPending.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbPending.Text = Language._Object.getCaption(Me.gbPending.Name, Me.gbPending.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.Name, Me.lblExpense.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
            Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.Name, Me.lblSector.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.dgcolhClaimNo.HeaderText = Language._Object.getCaption(Me.dgcolhClaimNo.Name, Me.dgcolhClaimNo.HeaderText)
            Me.dgcolhClaimExpense.HeaderText = Language._Object.getCaption(Me.dgcolhClaimExpense.Name, Me.dgcolhClaimExpense.HeaderText)
            Me.dgcolhSectorRoute.HeaderText = Language._Object.getCaption(Me.dgcolhSectorRoute.Name, Me.dgcolhSectorRoute.HeaderText)
            Me.dgcolhUOM.HeaderText = Language._Object.getCaption(Me.dgcolhUOM.Name, Me.dgcolhUOM.HeaderText)
            Me.dgcolhBalance.HeaderText = Language._Object.getCaption(Me.dgcolhBalance.Name, Me.dgcolhBalance.HeaderText)
            Me.dgcolhAppliedQty.HeaderText = Language._Object.getCaption(Me.dgcolhAppliedQty.Name, Me.dgcolhAppliedQty.HeaderText)
            Me.dgcolhAppliedAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAppliedAmount.Name, Me.dgcolhAppliedAmount.HeaderText)
            Me.dgcolhClaimRemark.HeaderText = Language._Object.getCaption(Me.dgcolhClaimRemark.Name, Me.dgcolhClaimRemark.HeaderText)
            Me.dgcolhQuantity.HeaderText = Language._Object.getCaption(Me.dgcolhQuantity.Name, Me.dgcolhQuantity.HeaderText)
            Me.dgcolhUnitPrice.HeaderText = Language._Object.getCaption(Me.dgcolhUnitPrice.Name, Me.dgcolhUnitPrice.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhExpenseRemark.HeaderText = Language._Object.getCaption(Me.dgcolhExpenseRemark.Name, Me.dgcolhExpenseRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category.")
            Language.setMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue.")
            Language.setMessage(mstrModuleName, 3, "Please Select atleast one Claim Expense Form to Approve/Reject.")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot set quantity greater than balance set.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot set Amount greater than balance set.")
            Language.setMessage(mstrModuleName, 7, "Remark is compulsory information.Please Enter Remark.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Some of the Claim Expense Forms cannot be Approved. Reason : It exceeds the current occurrence limit and will be highlighted in red.")
	    Language.setMessage(mstrModuleName, 9, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit for this selected employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class