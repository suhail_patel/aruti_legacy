﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpApprMigration

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpApprMigration"
    Private objExAssessorMaster As clsExpenseApprover_Master
    Private objExAssessorTran As clsExpenseApprover_Tran
    Private dtOldEmp As DataTable
    Private dtNewEmp As DataTable
    Private dtAssignedEmp As DataTable
    Private dgOldView As DataView
    Private dgNewView As DataView
    Private dgAssignedEmp As DataView

#End Region

#Region " Private Function & Procedure "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            Dim dtTable As DataTable = Nothing


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Pinkal (11-Sep-2019) -- End

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Old_Appr_Employee()
        Try
            dgOldApproverEmp.AutoGenerateColumns = False
            dgOldApproverEmp.DataSource = dgOldView
            objcolhdgSelect.DataPropertyName = "ischeck"
            colhdgEmployeecode.DataPropertyName = "ecode"
            colhdgEmployee.DataPropertyName = "ename"


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If dgNewApproverMigratedEmp.RowCount > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Gajanan [23-May-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Old_Appr_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigneed_Employee()
        Try
            dgNewApproverAssignEmp.AutoGenerateColumns = False
            dgNewApproverAssignEmp.DataSource = dgAssignedEmp
            colhdgAssignEmpCode.DataPropertyName = "ecode"
            colhdgAssignEmployee.DataPropertyName = "ename"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Assigneed_Employee", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Migrated_Employee()
        Try
            dgNewApproverMigratedEmp.AutoGenerateColumns = False
            dgNewApproverMigratedEmp.DataSource = dgNewView
            objColhdgMigratedSelect.DataPropertyName = "ischeck"
            colhdgMigratedEmpCode.DataPropertyName = "ecode"
            colhdgMigratedEmp.DataPropertyName = "ename"


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            If dgNewApproverMigratedEmp.RowCount > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Gajanan [23-May-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Migrated_Employee", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmExpApprMigration_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objExAssessorMaster = New clsExpenseApprover_Master
        objExAssessorTran = New clsExpenseApprover_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpApprMigration_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            clsExpenseApprover_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseApprover_Master,clsExpenseApprover_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchOldApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldApprover.Click, objbtnSearchOldLevel.Click, _
                                                                                                                  objbtnSearchNewApprover.Click, objbtnSearchNewLevel.Click
        Dim frm As New frmCommonSearch
        Try
            Dim cbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchOldApprover.Name.ToUpper
                    cbo = cboOldApprover
                Case objbtnSearchOldLevel.Name.ToUpper
                    cbo = cboOldLevel
                Case objbtnSearchNewApprover.Name.ToUpper
                    cbo = cboNewApprover
                Case objbtnSearchNewLevel.Name.ToUpper
                    cbo = cboNewLevel
            End Select
            If cbo.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .DataSource = CType(cbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cbo.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldApprover_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If dtOldEmp IsNot Nothing Then
                dtOldEmp.AcceptChanges() : Dim drRow() As DataRow = dtOldEmp.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one employee to migrate."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If CInt(cboNewApprover.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                    cboNewApprover.Select()
                    Exit Sub
                ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                    cboNewLevel.Select()
                    Exit Sub
                End If
                Dim blnFlag As Boolean = False
                Dim blnPendingFlag As Boolean = False
                If dgOldApproverEmp.RowCount <= 0 Then Exit Sub
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        If dtAssignedEmp IsNot Nothing AndAlso dtAssignedEmp.Rows.Count > 0 Then
                            Dim dRow() As DataRow = dtAssignedEmp.Select("employeeunkid =" & drRow(i)("employeeunkid").ToString())
                            If dRow.Length > 0 Then
                                blnFlag = True : Continue For
                            End If
                        End If

                        drRow(i)("ischeck") = False

                        'Pinkal (18-Jun-2020) -- Start
                        'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                        'If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.Tag) Then Continue For
                        If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) Then Continue For
                        'Pinkal (18-Jun-2020) -- End

                        If dtNewEmp Is Nothing Then
                            dtNewEmp = dtOldEmp.Clone
                            dtNewEmp.ImportRow(drRow(i))
                            dgNewView = dtNewEmp.DefaultView
                        Else
                            'Pinkal (22-Jun-2017) -- Start
                            'Enhancement -Bug Solved For Claim Approver Migration .
                            Dim dr As DataRow() = dtNewEmp.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
                            If dr.Length <= 0 Then
                            dtNewEmp.ImportRow(drRow(i))
                        End If
                            'Pinkal (22-Jun-2017) -- End
                        End If
                        dtOldEmp.Rows.Remove(drRow(i))
                    Next
                    dtOldEmp.AcceptChanges()
                    objchkOldCheck.Checked = False

                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Some of the checked employees are already linked with the selected approver."), enMsgBoxStyle.Information)
                    End If
                End If
                Call Fill_Migrated_Employee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try
            If dtNewEmp IsNot Nothing Then
                dtNewEmp.AcceptChanges() : Dim drRow() As DataRow = dtNewEmp.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one employee to unassign."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If dgNewApproverMigratedEmp.RowCount <= 0 Then Exit Sub
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("ischeck") = False
                        If dtOldEmp Is Nothing Then
                            dtOldEmp = dtNewEmp.Clone
                            dtOldEmp.ImportRow(drRow(i))
                        Else
                            dtOldEmp.ImportRow(drRow(i))
                        End If
                        dtNewEmp.Rows.Remove(drRow(i))
                    Next
                    dgOldView = dtOldEmp.DefaultView
                    objchkNewCheck.Checked = False
                    Call Fill_Old_Appr_Employee()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Select From Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Exit Sub
            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Exit Sub
            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Exit Sub
            ElseIf dgNewApproverMigratedEmp.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "There is no employee(s) for migration."), enMsgBoxStyle.Information)
                dgNewApproverMigratedEmp.Focus()
                Exit Sub
            End If



            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If objExAssessorTran.Perform_Migration(CInt(cboOldApprover.SelectedValue), CInt(cboNewApprover.SelectedValue), dtNewEmp, User._Object._Userunkid, CInt(cboNewApprover.Tag), ConfigParameter._Object._CurrentDateAndTime) = False Then
            If objExAssessorTran.Perform_Migration(CInt(cboOldApprover.Tag), CInt(cboNewApprover.Tag), dtNewEmp, User._Object._Userunkid, CInt(cboNewApprover.SelectedValue), ConfigParameter._Object._CurrentDateAndTime) = False Then
                'Pinkal (18-Jun-2020) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Problem in Approver Migration."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Approver Migration done successfully."), enMsgBoxStyle.Information)
                cboOldApprover.SelectedValue = 0
                cboOldLevel.SelectedValue = 0
                cboNewApprover.SelectedValue = 0
                cboNewLevel.SelectedValue = 0
                cboOldApprover.Select()

                'Pinkal (22-Jun-2017) -- Start
                'Enhancement -Bug Solved For Claim Approver Migration .
                dtNewEmp.Rows.Clear()
                dtAssignedEmp.Rows.Clear()
                dtOldEmp.Rows.Clear()
                FillCombo()
                'Pinkal (22-Jun-2017) -- End


                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                chkShowInactiveApprovers.Checked = False
                chkShowInActiveEmployees.Checked = False
                chkShowInActiveEmployees.Enabled = True
                'Gajanan [23-May-2020] -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event's "

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

            'dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List")
            dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                         ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                                         True, "List", "", True, chkShowInactiveApprovers.Checked)
            'Gajanan [23-May-2020] -- End


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

            'Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "id", "ApproverEmpunkid", "name")
            'With cboOldApprover
            '    .ValueMember = "id"
            '    .DisplayMember = "Name"
            '    .DataSource = dtTable
            '    .SelectedValue = 0
            'End With

            Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            With cboOldApprover
                .ValueMember = "ApproverEmpunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Pinkal (18-Jun-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            Dim dsList As New DataSet

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim dr As DataRowView = CType(cboOldApprover.SelectedItem, DataRowView)
            'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
            '    dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboOldApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), True, "List")
            'Else
            '    dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboOldApprover.SelectedValue), 0, True, "List")
            'End If
            Dim dr As DataRowView = CType(cboOldApprover.SelectedItem, DataRowView)
            If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboOldApprover.SelectedValue), CBool(dr("isexternalapprover")), True, "List")
            Else
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboOldApprover.SelectedValue), False, True, "List")
            End If
            'Pinkal (18-Jun-2020) -- End


            With cboOldLevel
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            'dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List")
            dsList = objExAssessorMaster.getListForCombo(CInt(cboExCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                        ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                                        True, "List", "", True, chkShowInactiveApprovers.Checked)
            'Gajanan [23-May-2020] -- End



            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

            'Dim dtTable As DataTable = Nothing
            'If CInt(cboOldApprover.SelectedValue) > 0 Then
            '    dtTable = New DataView(dsList.Tables(0), " id <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
            'End If

            'With cboNewApprover
            '    .ValueMember = "id"
            '    .DisplayMember = "Name"
            '    .DataSource = dtTable
            '    .SelectedValue = 0
            'End With


            Dim dtTable As DataTable = Nothing
            If CInt(cboOldApprover.SelectedValue) > 0 Then
                dtTable = New DataView(dsList.Tables(0), " ApproverEmpunkid <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
            End If

            With cboNewApprover
                .ValueMember = "ApproverEmpunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Pinkal (18-Jun-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'Dim dsList As New DataSet
            'Dim dr As DataRowView = CType(cboNewApprover.SelectedItem, DataRowView)
            'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
            '    dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboNewApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), True, "List")
            'Else
            '    dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), CInt(cboNewApprover.SelectedValue), 0, True, "List")
            'End If

            Dim dsList As New DataSet
            Dim dr As DataRowView = CType(cboNewApprover.SelectedItem, DataRowView)
            If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboNewApprover.SelectedValue), CBool(dr("isexternalapprover")), True, "List")
            Else
                dsList = objExAssessorMaster.GetExpApproverLevels(CInt(cboExCategory.SelectedValue), 0, CInt(cboNewApprover.SelectedValue), False, True, "List")
            End If

            'Pinkal (18-Jun-2020) -- End

            With cboNewLevel
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewApprover_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOldLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing
            Dim dr As DataRowView = CType(cboOldApprover.SelectedItem, DataRowView)


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
            '    dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboOldApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), CInt(cboOldLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
            'Else
            '    dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboOldApprover.SelectedValue), 0, CInt(cboOldLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
            'End If
            If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, 0, CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
            Else
                dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, 0, CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
            End If
            'Pinkal (18-Jun-2020) -- End

            dtOldEmp = dsList.Tables(0).Copy()
            dgOldView = dtOldEmp.DefaultView
            Call Fill_Old_Appr_Employee()


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    cboOldApprover.Tag = CInt(dsList.Tables(0).Rows(0)("approverempid"))
            'Else
            '    cboOldApprover.Tag = Nothing
            'End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                cboOldApprover.Tag = CInt(dsList.Tables(0).Rows(0)("crapproverunkid"))
            Else
                cboOldApprover.Tag = 0
            End If
            'Pinkal (18-Jun-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldLevel_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboNewLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing

            Dim dr As DataRowView = CType(cboNewApprover.SelectedItem, DataRowView)
            If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboNewApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), CInt(cboNewLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, 0, CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
                'Pinkal (18-Jun-2020) -- End
            Else
                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboNewApprover.SelectedValue), 0, CInt(cboNewLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                dsList = objExAssessorMaster.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboNewApprover.SelectedValue), 0, CInt(cboNewLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), chkShowInActiveEmployees.Checked)
                'Pinkal (18-Jun-2020) -- End
            End If

            dtAssignedEmp = dsList.Tables(0)
            dgAssignedEmp = dtAssignedEmp.DefaultView
            Call Fill_Assigneed_Employee()

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                dgNewApproverAssignEmp.DataSource = Nothing
                dgNewApproverMigratedEmp.DataSource = Nothing
            End If


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    cboNewApprover.Tag = CInt(dsList.Tables(0).Rows(0)("approverempid"))
            'Else
            '    cboNewApprover.Tag = Nothing
            'End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                cboNewApprover.Tag = CInt(dsList.Tables(0).Rows(0)("crapproverunkid"))
            Else
                cboNewApprover.Tag = 0
            End If
            'Pinkal (18-Jun-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (19-Mar-2015) -- End


#End Region

#Region " Checkbox Event's "

    Private Sub objchkOldCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldCheck.CheckedChanged
        Try

            If dgOldView Is Nothing Then Exit Sub

            RemoveHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
            If dgOldView.Table.Rows.Count > 0 Then
                For Each dr As DataRow In dgOldView.Table.Rows
                    dr("ischeck") = objchkOldCheck.Checked
                Next
                dgOldView.Table.AcceptChanges()
            End If
            AddHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldCheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkNewCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewCheck.CheckedChanged
        Try
            If dgNewView Is Nothing Then Exit Sub
            RemoveHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
            If dgNewView.Table.Rows.Count > 0 Then
                For Each dr As DataRow In dgNewView.Table.Rows
                    dr("ischeck") = objchkNewCheck.Checked
                Next
                dgNewView.Table.AcceptChanges()
            End If
            AddHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'Gajanan [23-May-2020] -- Start
    'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
    Private Sub chkShowInactiveApprovers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInactiveApprovers.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            chkShowInActiveEmployees.Checked = False
            chkShowInActiveEmployees.Enabled = True
            cboExCategory_SelectedIndexChanged(cboExCategory, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInactiveApprovers_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub chkShowInActiveEmployees_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployees.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            cboOldLevel_SelectedIndexChanged(cboOldLevel, New EventArgs())
            cboNewLevel_SelectedIndexChanged(cboNewLevel, New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInActiveEmployees_CheckedChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    'Gajanan [23-May-2020] -- End

#End Region

#Region " Grids Event's "

    Private Sub dgOldApproverEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOldApproverEmp.CellContentClick, dgOldApproverEmp.CellContentDoubleClick
        Try
            RemoveHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = objcolhdgSelect.Index Then
                If dgOldApproverEmp.IsCurrentCellDirty Then
                    dgOldApproverEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = dgOldView.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    objchkOldCheck.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgOldApproverEmp.Rows.Count Then
                    objchkOldCheck.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgOldApproverEmp.Rows.Count Then
                    objchkOldCheck.CheckState = CheckState.Checked
                End If
            End If
            AddHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldApproverEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

    Private Sub dgNewApproverMigratedEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgNewApproverMigratedEmp.CellContentClick, dgNewApproverMigratedEmp.CellContentDoubleClick
        Try
            RemoveHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = objColhdgMigratedSelect.Index Then
                If dgNewApproverMigratedEmp.IsCurrentCellDirty Then
                    dgNewApproverMigratedEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = dgNewView.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    objchkNewCheck.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgNewApproverMigratedEmp.Rows.Count Then
                    objchkNewCheck.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgNewApproverMigratedEmp.Rows.Count Then
                    objchkNewCheck.CheckState = CheckState.Checked
                End If
            End If
            AddHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgNewApproverMigratedEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Textbox Event's "

    Private Sub txtOldSearchEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOldSearchEmployee.TextChanged
        Try
            If dgOldView IsNot Nothing Then dgOldView.RowFilter = "ecode like '%" & txtOldSearchEmployee.Text.Trim & "%' or ename like '%" & txtOldSearchEmployee.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtOldSearchEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverAssignEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverAssignEmp.TextChanged
        Try
            If dgAssignedEmp IsNot Nothing Then dgAssignedEmp.RowFilter = "ecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%' or ename like '%" & txtNewApproverAssignEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverAssignEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverMigratedEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverMigratedEmp.TextChanged
        Try
            If dgNewView IsNot Nothing Then dgNewView.RowFilter = "ecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' or ename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverMigratedEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUnAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblOldLevel.Text = Language._Object.getCaption(Me.lblOldLevel.Name, Me.lblOldLevel.Text)
            Me.colhdgEmployeecode.HeaderText = Language._Object.getCaption(Me.colhdgEmployeecode.Name, Me.colhdgEmployeecode.HeaderText)
            Me.colhdgEmployee.HeaderText = Language._Object.getCaption(Me.colhdgEmployee.Name, Me.colhdgEmployee.HeaderText)
            Me.lblNewLevel.Text = Language._Object.getCaption(Me.lblNewLevel.Name, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Language._Object.getCaption(Me.lblNewApprover.Name, Me.lblNewApprover.Text)
            Me.lblOldApprover.Text = Language._Object.getCaption(Me.lblOldApprover.Name, Me.lblOldApprover.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.tbpMigratedEmployee.Text = Language._Object.getCaption(Me.tbpMigratedEmployee.Name, Me.tbpMigratedEmployee.Text)
            Me.colhdgMigratedEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmpCode.Name, Me.colhdgMigratedEmpCode.HeaderText)
            Me.colhdgMigratedEmp.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmp.Name, Me.colhdgMigratedEmp.HeaderText)
            Me.tbpAssignEmployee.Text = Language._Object.getCaption(Me.tbpAssignEmployee.Name, Me.tbpAssignEmployee.Text)
            Me.colhdgAssignEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmpCode.Name, Me.colhdgAssignEmpCode.HeaderText)
            Me.colhdgAssignEmployee.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmployee.Name, Me.colhdgAssignEmployee.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one employee to migrate.")
            Language.setMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s).")
            Language.setMessage(mstrModuleName, 4, "Sorry, Some of the checked employees are already linked with the selected approver.")
            Language.setMessage(mstrModuleName, 5, "Please check atleast one employee to unassign.")
            Language.setMessage(mstrModuleName, 6, "Please Select From Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 7, "There is no employee(s) for migration.")
            Language.setMessage(mstrModuleName, 8, "Problem in Approver Migration.")
            Language.setMessage(mstrModuleName, 9, "Approver Migration done successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class