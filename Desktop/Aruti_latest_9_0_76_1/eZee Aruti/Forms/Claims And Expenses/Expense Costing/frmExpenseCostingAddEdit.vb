﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExpenseCostingAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExpenseCostingAddEdit"
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCostingUnkid As Integer = -1
    Private objExpenseCosting As clsExpenseCosting
    Private mblnCancel As Boolean = True

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCostingUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCostingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboSecRoute.BackColor = GUI.ColorComp
            dtpEffDate.CalendarForeColor = GUI.ColorComp
            txtCosting.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSecRoute
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCosting.Decimal = objExpenseCosting._Amount
            If objExpenseCosting._Effectivedate <> Nothing Then
                dtpEffDate.Value = objExpenseCosting._Effectivedate
            End If
            cboSecRoute.SelectedValue = objExpenseCosting._Secrouteunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objExpenseCosting._Amount = txtCosting.Decimal
            objExpenseCosting._Effectivedate = dtpEffDate.Value
            objExpenseCosting._Secrouteunkid = CInt(cboSecRoute.SelectedValue)
            objExpenseCosting._Isvoid = objExpenseCosting._Isvoid
            objExpenseCosting._Userunkid = objExpenseCosting._Userunkid
            objExpenseCosting._Voiddatetime = objExpenseCosting._Voiddatetime
            objExpenseCosting._Voidreason = objExpenseCosting._Voidreason
            objExpenseCosting._Voiduserunkid = objExpenseCosting._Voiduserunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboSecRoute.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Sector/Route is mandatory information. Please select Sector/Route to continue."), enMsgBoxStyle.Information)
                cboSecRoute.Focus()
                Return False
            End If

            If txtCosting.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, costing is mandatory information. Please enter costing to continue."), enMsgBoxStyle.Information)
                txtCosting.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmExpenseCosting_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objExpenseCosting = Nothing
    End Sub

    Private Sub frmExpenseCosting_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmExpenseCosting_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmExpenseCosting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExpenseCosting = New clsExpenseCosting
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings() : Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objExpenseCosting._Costingunkid = mintCostingUnkid
                dtpEffDate.Enabled = False
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpenseCosting_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseCosting.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseCosting"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()
            If objExpenseCosting.isExist(objExpenseCosting._Secrouteunkid, dtpEffDate.Value) Then
                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objExpenseCosting.Update()
                Else
                    blnFlag = objExpenseCosting.Insert()
                End If
            Else
                blnFlag = objExpenseCosting.Insert()
            End If

            If blnFlag = False And objExpenseCosting._Message <> "" Then
                eZeeMsgBox.Show(objExpenseCosting._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objExpenseCosting = Nothing
                    objExpenseCosting = New clsExpenseCosting
                    Call GetValue()
                    cboSecRoute.Focus()
                Else
                    mintCostingUnkid = objExpenseCosting._Costingunkid
                    Me.Close()
                End If
            End If

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This Expense Costing was saved successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRoute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRoute.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboSecRoute.ValueMember
                .DisplayMember = cboSecRoute.DisplayMember
                .DataSource = CType(cboSecRoute.DataSource, DataTable)
                If .DisplayDialog Then
                    cboSecRoute.SelectedValue = .SelectedValue
                    cboSecRoute.Focus()
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRoute_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbExpenseCosting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpenseCosting.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbExpenseCosting.Text = Language._Object.getCaption(Me.gbExpenseCosting.Name, Me.gbExpenseCosting.Text)
            Me.lblSector_Route.Text = Language._Object.getCaption(Me.lblSector_Route.Name, Me.lblSector_Route.Text)
            Me.lblCost.Text = Language._Object.getCaption(Me.lblCost.Name, Me.lblCost.Text)
            Me.lblEffDate.Text = Language._Object.getCaption(Me.lblEffDate.Name, Me.lblEffDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Sector/Route is mandatory information. Please select Sector/Route to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, costing is mandatory information. Please enter costing to continue.")
            Language.setMessage(mstrModuleName, 3, "This Exchange rate was saved successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class