﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExApproverList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExApproverList"
    Private objExApprover As clsExpenseApprover_Master
    Private mintUserMappunkid As Integer = -1

#End Region

#Region " Private Functions "

    Private Sub Fill_List()
        Dim dsList As New DataSet
        'Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        Try


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            If User._Object.Privilege._AllowtoViewExpenseApproverList = False Then Exit Sub
            'Pinkal (13-Jul-2015) -- End


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            '   dsList = objExApprover.GetList("List")


            'If CInt(cboApprover.SelectedValue) > 0 Then
            '    StrSearch &= "AND crapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
            'End If

            'If CInt(cboExCategory.SelectedValue) > 0 Then
            '    StrSearch &= "AND expensetypeid = '" & CInt(cboExCategory.SelectedValue) & "' "
            'End If

            'If CInt(cboLevel.SelectedValue) > 0 Then
            '    StrSearch &= "AND crlevelunkid = '" & CInt(cboLevel.SelectedValue) & "' "
            'End If

            'If StrSearch.Trim.Length > 0 Then
            '    StrSearch = StrSearch.Substring(3)
            '    dTable = New DataView(dsList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            'End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.crapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
            End If

            If CInt(cboExCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.expensetypeid = '" & CInt(cboExCategory.SelectedValue) & "' "
            End If

            If CInt(cboLevel.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.crlevelunkid = '" & CInt(cboLevel.SelectedValue) & "' "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If

            dsList = objExApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                    , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                    , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, StrSearch, CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)))


            lvApproverList.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item("ename").ToString
                lvItem.SubItems.Add(dtRow.Item("clevel").ToString)
                lvItem.SubItems.Add(dtRow.Item("department").ToString)
                lvItem.SubItems.Add(dtRow.Item("jobname").ToString)
                lvItem.SubItems.Add(dtRow.Item("usermapped").ToString)
                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                lvItem.SubItems.Add(dtRow.Item("ExAppr").ToString)
                'Pinkal (01-Mar-2016) -- End
                lvItem.SubItems.Add(dtRow.Item("expensetype").ToString)
                lvItem.Tag = dtRow.Item("crapproverunkid")
                lvApproverList.Items.Add(lvItem)
            Next

            'Pinkal (24-Aug-2015) -- End


            lvApproverList.GroupingColumn = objcolhExCat
            lvApproverList.DisplayGroups(True)


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If lvApproverList.Items.Count > 4 Then
            '    colhMappedUser.Width = 120 - 18
            'Else
            '    colhMappedUser.Width = 120
            'End If

            If lvApproverList.Items.Count > 4 Then
                colhExternalApprover.Width = 125 - 18
            Else
                colhExternalApprover.Width = 125
            End If

            'Pinkal (01-Mar-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddExpenseApprover

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'btnEdit.Enabled = User._Object.Privilege._AllowtoEditExpenseApprover
            'btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteExpenseApprover
            If CInt(cboStatus.SelectedValue) = 1 Then
                btnEdit.Enabled = User._Object.Privilege._AllowtoEditExpenseApprover
                btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteExpenseApprover
            Else
                btnEdit.Enabled = False
                btnDelete.Enabled = False
            End If

            'Shani(17-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 1
            End With

            'Shani(17-Aug-2015) -- End


            Dim dtTable As DataTable = Nothing


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboExCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private Sub FillApprover()
        Dim dsList As DataSet = Nothing
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then

                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List", , True)
                dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                            ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                            True, "List", "", True, True)
                'Gajanan [23-May-2020] -- End
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then

                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List", , False)
                dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                            ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                            True, "List", "", False, True)
                'Gajanan [23-May-2020] -- End
            End If

            With cboApprover
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApprover", mstrModuleName)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End
#End Region

#Region " Form's Events "

    Private Sub frmExApproverList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objExApprover = New clsExpenseApprover_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExApproverList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsExpenseApprover_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsExpenseApprover_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmExpApproverAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(enAction.ADD_CONTINUE, -1) = True Then
                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                If CInt(cboExCategory.SelectedValue) > 0 Then
                    Call Fill_List()
                End If
                'Shani(17-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmExpApproverAddEdit
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information)
                lvApproverList.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.EDIT_ONE, CInt(lvApproverList.SelectedItems(0).Tag)) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvApproverList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information)
                lvApproverList.Select()
                Exit Sub
            End If
            If objExApprover.isUsed(CInt(lvApproverList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(objExApprover._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim sVoidReason As String = String.Empty
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.OTHERS, sVoidReason)
                If sVoidReason.Trim.Length <= 0 Then Exit Sub

                objExApprover._Isvoid = True
                objExApprover._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objExApprover._Voidreason = sVoidReason
                objExApprover._Voiduserunkid = User._Object._Userunkid

                objExApprover.Delete(CInt(lvApproverList.SelectedItems(0).Tag))
                lvApproverList.SelectedItems(0).Remove()
                Fill_List()
                If lvApproverList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExCategory.SelectedValue = 0
            'cboApprover.SelectedValue = 0
            'cboLevel.SelectedValue = 0
            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            cboStatus.SelectedValue = 1
            'Shani(17-Aug-2015) -- End
            lvApproverList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Expense Category is mandatory information. Please select Expense Category to continue."), enMsgBoxStyle.Information)
                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                cboExCategory.Focus()
                'Shani(17-Aug-2015) -- End
                Exit Sub
            End If
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboApprover.ValueMember
                .DisplayMember = cboApprover.DisplayMember
                .DataSource = CType(cboApprover.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboApprover.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboLevel.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboLevel.ValueMember
                    .DisplayMember = cboLevel.DisplayMember
                    .CodeMember = "Code"
                    .DataSource = CType(cboLevel.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboLevel.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub cboExCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objExLevel As New clsExApprovalLevel
            dsList = objExLevel.getListForCombo(CInt(cboExCategory.SelectedValue), "List", True)
            With cboLevel
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objExLevel = Nothing

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List", , , , False)
            'With cboApprover
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            'End With
            'Shani(17-Aug-2015) -- End
            dsList.Dispose()

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            lvApproverList.Items.Clear()
            Call FillApprover()
            'Shani(17-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                mnuInActiveApprover.Enabled = User._Object.Privilege._AllowtoSetClaimInactiveApprover
                mnuActiveApprover.Enabled = False
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = User._Object.Privilege._AllowtoSetClaimActiveApprover
            Else
                mnuInActiveApprover.Enabled = False
                mnuActiveApprover.Enabled = False
            End If
            If CInt(cboExCategory.SelectedValue) > 0 Then
                Call SetVisibility()
                Call Fill_List()
                Call FillApprover()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End
#End Region


    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
#Region "Menu Event"

    Private Sub mnuInActiveApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to set selected approver as Inactive ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim objClaimMst As New clsclaim_request_master
                If objClaimMst.GetApproverPendingExpenseFormCount(CInt(lvApproverList.SelectedItems(0).Tag), "") > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You cannot inactivate this approver.Reason :This Approver has Pending Leave Application Form(s)."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objClaimMst = Nothing

                If objExApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, False) = False Then
                    eZeeMsgBox.Show(objExApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    Call Fill_List()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuInActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuActiveApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuActiveApprover.Click
        Try
            If lvApproverList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvApproverList.Select()
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to set selected approver as Active ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                If objExApprover.InActiveApprover(CInt(lvApproverList.SelectedItems(0).Tag), Company._Object._Companyunkid, True) = False Then
                    eZeeMsgBox.Show(objExApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    Call Fill_List()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuActiveApprover_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
         
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOpearation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpearation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.Name, Me.lblExpenseCat.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.colhMappedUser.Text = Language._Object.getCaption(CStr(Me.colhMappedUser.Tag), Me.colhMappedUser.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.btnOpearation.Text = Language._Object.getCaption(Me.btnOpearation.Name, Me.btnOpearation.Text)
            Me.mnuInActiveApprover.Text = Language._Object.getCaption(Me.mnuInActiveApprover.Name, Me.mnuInActiveApprover.Text)
            Me.mnuActiveApprover.Text = Language._Object.getCaption(Me.mnuActiveApprover.Name, Me.mnuActiveApprover.Text)
            Me.colhExternalApprover.Text = Language._Object.getCaption(CStr(Me.colhExternalApprover.Tag), Me.colhExternalApprover.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Approver?")
            Language.setMessage(mstrModuleName, 3, "Sorry, Expense Category is mandatory information. Please select Expense Category to continue.")
            Language.setMessage(mstrModuleName, 4, "You cannot inactivate this approver.Reason :This Approver has Pending Leave Application Form(s).")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to set selected approver as Inactive ?")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to set selected approver as Active ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class