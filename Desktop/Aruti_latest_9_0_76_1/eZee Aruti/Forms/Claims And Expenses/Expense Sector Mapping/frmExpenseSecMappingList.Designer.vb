﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExpenseSecMappingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExpenseSecMappingList))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchSector = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchExpense = New eZee.Common.eZeeGradientButton
        Me.cboSecRoute = New System.Windows.Forms.ComboBox
        Me.LblSecRoute = New System.Windows.Forms.Label
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.LblExpense = New System.Windows.Forms.Label
        Me.cboExpType = New System.Windows.Forms.ComboBox
        Me.lblExpenseType = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgExpSecroute = New System.Windows.Forms.DataGridView
        Me.objdgSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhSectorRoute = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpSectorunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpenseID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSectorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.dgExpSecroute, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSector)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboSecRoute)
        Me.gbFilterCriteria.Controls.Add(Me.LblSecRoute)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpense)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpense)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpType)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpenseType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(767, 64)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSector
        '
        Me.objbtnSearchSector.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSector.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSector.BorderSelected = False
        Me.objbtnSearchSector.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSector.Image = CType(resources.GetObject("objbtnSearchSector.Image"), System.Drawing.Image)
        Me.objbtnSearchSector.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSector.Location = New System.Drawing.Point(741, 33)
        Me.objbtnSearchSector.Name = "objbtnSearchSector"
        Me.objbtnSearchSector.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchSector.TabIndex = 328
        '
        'objbtnSearchExpense
        '
        Me.objbtnSearchExpense.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExpense.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExpense.BorderSelected = False
        Me.objbtnSearchExpense.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExpense.Image = CType(resources.GetObject("objbtnSearchExpense.Image"), System.Drawing.Image)
        Me.objbtnSearchExpense.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExpense.Location = New System.Drawing.Point(470, 33)
        Me.objbtnSearchExpense.Name = "objbtnSearchExpense"
        Me.objbtnSearchExpense.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchExpense.TabIndex = 323
        '
        'cboSecRoute
        '
        Me.cboSecRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSecRoute.DropDownWidth = 300
        Me.cboSecRoute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSecRoute.FormattingEnabled = True
        Me.cboSecRoute.Location = New System.Drawing.Point(580, 33)
        Me.cboSecRoute.Name = "cboSecRoute"
        Me.cboSecRoute.Size = New System.Drawing.Size(158, 21)
        Me.cboSecRoute.TabIndex = 327
        '
        'LblSecRoute
        '
        Me.LblSecRoute.BackColor = System.Drawing.Color.Transparent
        Me.LblSecRoute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSecRoute.Location = New System.Drawing.Point(495, 36)
        Me.LblSecRoute.Name = "LblSecRoute"
        Me.LblSecRoute.Size = New System.Drawing.Size(82, 15)
        Me.LblSecRoute.TabIndex = 326
        Me.LblSecRoute.Text = "Sector/Route"
        Me.LblSecRoute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.DropDownWidth = 300
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(309, 33)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(158, 21)
        Me.cboExpense.TabIndex = 325
        '
        'LblExpense
        '
        Me.LblExpense.BackColor = System.Drawing.Color.Transparent
        Me.LblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpense.Location = New System.Drawing.Point(242, 36)
        Me.LblExpense.Name = "LblExpense"
        Me.LblExpense.Size = New System.Drawing.Size(62, 15)
        Me.LblExpense.TabIndex = 324
        Me.LblExpense.Text = "Expense"
        Me.LblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpType
        '
        Me.cboExpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpType.DropDownWidth = 300
        Me.cboExpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpType.FormattingEnabled = True
        Me.cboExpType.Location = New System.Drawing.Point(92, 33)
        Me.cboExpType.Name = "cboExpType"
        Me.cboExpType.Size = New System.Drawing.Size(140, 21)
        Me.cboExpType.TabIndex = 322
        '
        'lblExpenseType
        '
        Me.lblExpenseType.BackColor = System.Drawing.Color.Transparent
        Me.lblExpenseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseType.Location = New System.Drawing.Point(4, 36)
        Me.lblExpenseType.Name = "lblExpenseType"
        Me.lblExpenseType.Size = New System.Drawing.Size(82, 15)
        Me.lblExpenseType.TabIndex = 321
        Me.lblExpenseType.Text = "Expense Cat."
        Me.lblExpenseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(740, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(717, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 370)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(769, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(557, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(454, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(660, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgExpSecroute
        '
        Me.dgExpSecroute.AllowUserToAddRows = False
        Me.dgExpSecroute.AllowUserToDeleteRows = False
        Me.dgExpSecroute.AllowUserToResizeRows = False
        Me.dgExpSecroute.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgExpSecroute.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgExpSecroute.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgExpSecroute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgExpSecroute.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgSelect, Me.objdgcolhCollapse, Me.dgcolhSectorRoute, Me.objdgcolhExpSectorunkid, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhExpenseID, Me.objdgcolhSectorID})
        Me.dgExpSecroute.Location = New System.Drawing.Point(2, 70)
        Me.dgExpSecroute.MultiSelect = False
        Me.dgExpSecroute.Name = "dgExpSecroute"
        Me.dgExpSecroute.RowHeadersVisible = False
        Me.dgExpSecroute.RowHeadersWidth = 5
        Me.dgExpSecroute.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgExpSecroute.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgExpSecroute.Size = New System.Drawing.Size(767, 298)
        Me.dgExpSecroute.TabIndex = 321
        '
        'objdgSelect
        '
        Me.objdgSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgSelect.Frozen = True
        Me.objdgSelect.HeaderText = ""
        Me.objdgSelect.Name = "objdgSelect"
        Me.objdgSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgSelect.Width = 25
        '
        'objdgcolhCollapse
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 5.0!)
        DataGridViewCellStyle1.NullValue = Nothing
        Me.objdgcolhCollapse.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollapse.Frozen = True
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'dgcolhSectorRoute
        '
        Me.dgcolhSectorRoute.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhSectorRoute.HeaderText = "Sector/Route"
        Me.dgcolhSectorRoute.Name = "dgcolhSectorRoute"
        Me.dgcolhSectorRoute.ReadOnly = True
        Me.dgcolhSectorRoute.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'objdgcolhExpSectorunkid
        '
        Me.objdgcolhExpSectorunkid.HeaderText = "objdgcolhEmpSectorunkid"
        Me.objdgcolhExpSectorunkid.Name = "objdgcolhExpSectorunkid"
        Me.objdgcolhExpSectorunkid.ReadOnly = True
        Me.objdgcolhExpSectorunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhExpSectorunkid.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhExpenseID
        '
        Me.objdgcolhExpenseID.HeaderText = "objdgcolhExpenseID"
        Me.objdgcolhExpenseID.Name = "objdgcolhExpenseID"
        Me.objdgcolhExpenseID.ReadOnly = True
        Me.objdgcolhExpenseID.Visible = False
        '
        'objdgcolhSectorID
        '
        Me.objdgcolhSectorID.HeaderText = "objdgcolhSectorID"
        Me.objdgcolhSectorID.Name = "objdgcolhSectorID"
        Me.objdgcolhSectorID.ReadOnly = True
        Me.objdgcolhSectorID.Visible = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(9, 76)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 322
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'frmExpenseSecMappingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 425)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.dgExpSecroute)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExpenseSecMappingList"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense Sector Mapping List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgExpSecroute, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgExpSecroute As System.Windows.Forms.DataGridView
    Friend WithEvents cboExpType As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseType As System.Windows.Forms.Label
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhSectorRoute As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpSectorunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpenseID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSectorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboSecRoute As System.Windows.Forms.ComboBox
    Friend WithEvents LblSecRoute As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents LblExpense As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSector As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchExpense As eZee.Common.eZeeGradientButton
End Class
