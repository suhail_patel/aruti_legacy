﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmExpenseAdjustment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExpenseAdjustment"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private dsFill As New DataSet
    Private dvEmployee As DataView
    Dim objExpAdjustment As clsexpadjustment_Tran

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Forms Event"

    Private Sub frmExpenseAdjustment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objExpAdjustment = New clsexpadjustment_Tran
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            dgColhAppointmentDt.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgColhAccrueAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIssueAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRemaining_Bal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAdjustmentAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            rdApplytoall.Checked = True
            cboExpense.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExpenseAdjustment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            dsFill = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables(0)
            End With

            Dim objEmp As New clsEmployee_Master
            dsFill = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsFill.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillData()
        Dim objExpBalance As New clsEmployeeExpenseBalance
        Dim strSearch As String = ""
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch = "cmexpbalance_tran.employeeunkid  = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                If strSearch.Trim.Length > 0 Then strSearch &= " AND "
                strSearch &= mstrAdvanceFilter
            End If


            dsFill = objExpBalance.GetEmployeeExpesneDataForAdjustment(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                        , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True _
                                                                                                        , ConfigParameter._Object._LeaveBalanceSetting, CInt(cboExpense.SelectedValue), dtpDate.Value.Date, FinancialYear._Object._Database_End_Date _
                                                                                                        , True, True, strSearch)


            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)

            If objExpense._Uomunkid = enExpUoM.UOM_QTY Then
                dgColhAccrueAmt.DefaultCellStyle.Format = "#00.00"
                dgcolhIssueAmt.DefaultCellStyle.Format = "#00.00"
                dgcolhRemaining_Bal.DefaultCellStyle.Format = "#00.00"
                dgcolhAdjustmentAmt.DefaultCellStyle.Format = "#00.00"
            ElseIf objExpense._Uomunkid = enExpUoM.UOM_AMOUNT Then
                dgColhAccrueAmt.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhIssueAmt.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhRemaining_Bal.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhAdjustmentAmt.DefaultCellStyle.Format = GUI.fmtCurrency
            End If

            objExpense = Nothing

            dvEmployee = dsFill.Tables(0).DefaultView
            dgEmployee.AutoGenerateColumns = False
            objdgcolhSelect.DataPropertyName = "ischeck"
            dgColhEmployee.DataPropertyName = "Employee"
            dgColhEmployee.Tag = "employeeunkid"
            dgColhJobtitle.DataPropertyName = "Job"
            dgColhAppointmentDt.DataPropertyName = "appointeddate"
            dgColhAccrueAmt.DataPropertyName = "Accrue_amount"
            dgcolhIssueAmt.DataPropertyName = "Issue_amount"
            dgcolhRemaining_Bal.DataPropertyName = "Remaining_Bal"
            dgcolhAdjustmentAmt.DataPropertyName = "Adjustment_Amt"
            objdgcolhExpBalanceId.DataPropertyName = "crexpbalanceunkid"

            dgEmployee.DataSource = dsFill.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillData", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), enMsgBoxStyle.Information)
                cboCategory.Select()
                Exit Sub
            ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense is compulsory information.Please Select Expense."), enMsgBoxStyle.Information)
                cboExpense.Select()
                Exit Sub
            End If
            Call FillData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCategory.SelectedValue = 0
            cboExpense.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            dgEmployee.DataSource = Nothing
            txtAdjustmentAmount.Decimal = 0
            TxtSearch.Text = ""
            txtRemark.Text = ""
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            rdApplytoall.Checked = True
            mstrAdvanceFilter = ""
            chkSelectAll.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnsearchExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnsearchExpense.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboExpense.DataSource, DataTable)
            With cboExpense
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchExpense_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnsearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnsearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim drRow As DataRow() = Nothing

            If CInt(cboCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category."), enMsgBoxStyle.Information)
                cboCategory.Select()
                Exit Sub

            ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Expense is compulsory information.Please Select Expense."), enMsgBoxStyle.Information)
                cboExpense.Select()
                Exit Sub

            ElseIf txtRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Remark is compulsory information.Please Select Remark."), enMsgBoxStyle.Information)
                txtRemark.Select()
                Exit Sub
            ElseIf dsFill IsNot Nothing Then
                drRow = dvEmployee.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If drRow.Length > 0 Then
                Dim mblnFlag As Boolean = False
                For Each dr As DataRow In drRow
                    objExpAdjustment._Crexpbalanceunkid = CInt(dr("Crexpbalanceunkid"))
                    objExpAdjustment._Employeeunkid = CInt(dr("employeeunkid"))
                    objExpAdjustment._Expenseunkid = CInt(cboExpense.SelectedValue)
                    objExpAdjustment._Adjustment_Amt = CDec(dr("adjustment_amt"))
                    objExpAdjustment._Remarks = txtRemark.Text.Trim
                    objExpAdjustment._Userunkid = User._Object._Userunkid
                    objExpAdjustment._Isopenelc = CBool(dr("Isopenelc"))
                    objExpAdjustment._Iselc = CBool(dr("Iselc"))
                    objExpAdjustment._Isclose_Fy = CBool(dr("Isclose_Fy"))
                    objExpAdjustment._Isvoid = False
                    objExpAdjustment._WebClientIP = getIP()
                    objExpAdjustment._WebHostName = getHostName()
                    objExpAdjustment._WebFormName = Me.Name
                    objExpAdjustment._IsWeb = False

                    If objExpAdjustment.Insert() = False Then
                        mblnFlag = False
                        eZeeMsgBox.Show(objExpAdjustment._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    mblnFlag = True
                Next

                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Adjustment done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    objbtnReset_Click(objbtnReset, New EventArgs())
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If dgEmployee.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no data to set adjustment amount."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim dtTable As DataTable = CType(dgEmployee.DataSource, DataTable)
                If rdApplytoall.Checked Then
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        For Each dr As DataRow In dtTable.Rows
                            dr("adjustment_amt") = txtAdjustmentAmount.Decimal
                        Next
                        dtTable.AcceptChanges()
                    End If
                ElseIf rdApplytoChecked.Checked Then
                    Dim drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True)
                    If drRow IsNot Nothing AndAlso drRow.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        For Each dr As DataRow In drRow
                            dr("adjustment_amt") = txtAdjustmentAmount.Decimal
                            dr.AcceptChanges()
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dsFill.Tables(0).AcceptChanges()
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButon Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            TxtSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            'Pinkal (18-Aug-2015) -- Start
            'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'Pinkal (18-Aug-2015) -- End
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dsFill.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischeck") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Private Sub TxtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then
                dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR EmpName like '%" & TxtSearch.Text.Trim & "%'"
                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If
            dgEmployee_CellContentClick(dgEmployee, New DataGridViewCellEventArgs(objdgcolhSelect.Index, 0))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Private Sub cboCategory_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedValueChanged
        Dim objExpense As New clsExpense_Master
        Try
            Dim dsList As DataSet = objExpense.getComboList(CInt(cboCategory.SelectedValue), True, "List", 0, False, 0, "", "")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "isaccrue=1", "", DataViewRowState.CurrentRows).ToTable()

            Dim dRow As DataRow = dtTable.NewRow
            dRow("Id") = 0
            dRow("Name") = Language.getMessage(mstrModuleName, 7, "Select")
            dRow("isaccrue") = False
            dtTable.Rows.InsertAt(dRow, 0)
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCategory_SelectedValueChanged", mstrModuleName)
        Finally
            objExpense = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbExpenseAdjustment.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExpenseAdjustment.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSet.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAdjustmentAmount.Text = Language._Object.getCaption(Me.lblAdjustmentAmount.Name, Me.lblAdjustmentAmount.Text)
			Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.Name, Me.LblExpenseCategory.Text)
			Me.gbExpenseAdjustment.Text = Language._Object.getCaption(Me.gbExpenseAdjustment.Name, Me.gbExpenseAdjustment.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.rdApplytoChecked.Text = Language._Object.getCaption(Me.rdApplytoChecked.Name, Me.rdApplytoChecked.Text)
			Me.rdApplytoall.Text = Language._Object.getCaption(Me.rdApplytoall.Name, Me.rdApplytoall.Text)
			Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhAppointmentDt.HeaderText = Language._Object.getCaption(Me.dgColhAppointmentDt.Name, Me.dgColhAppointmentDt.HeaderText)
			Me.dgColhJobtitle.HeaderText = Language._Object.getCaption(Me.dgColhJobtitle.Name, Me.dgColhJobtitle.HeaderText)
			Me.dgColhAccrueAmt.HeaderText = Language._Object.getCaption(Me.dgColhAccrueAmt.Name, Me.dgColhAccrueAmt.HeaderText)
			Me.dgcolhIssueAmt.HeaderText = Language._Object.getCaption(Me.dgcolhIssueAmt.Name, Me.dgcolhIssueAmt.HeaderText)
			Me.dgcolhRemaining_Bal.HeaderText = Language._Object.getCaption(Me.dgcolhRemaining_Bal.Name, Me.dgcolhRemaining_Bal.HeaderText)
			Me.dgcolhAdjustmentAmt.HeaderText = Language._Object.getCaption(Me.dgcolhAdjustmentAmt.Name, Me.dgcolhAdjustmentAmt.HeaderText)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please Select Expense Category.")
			Language.setMessage(mstrModuleName, 2, "Expense is compulsory information.Please Select Expense.")
			Language.setMessage(mstrModuleName, 3, "Remark is compulsory information.Please Select Remark.")
			Language.setMessage(mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee.")
			Language.setMessage(mstrModuleName, 5, "Adjustment done successfully.")
			Language.setMessage(mstrModuleName, 6, "There is no data to set adjustment amount.")
			Language.setMessage(mstrModuleName, 7, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class