﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeExpenses
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeExpenses))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkProcess = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchExp = New eZee.Common.eZeeGradientButton
        Me.pnlAppDate = New System.Windows.Forms.Panel
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.pnlYear = New System.Windows.Forms.Panel
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.LblToYear = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.LblToMonth = New System.Windows.Forms.Label
        Me.LblMonth = New System.Windows.Forms.Label
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.cboTocondition = New System.Windows.Forms.ComboBox
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.cboFromcondition = New System.Windows.Forms.ComboBox
        Me.lblExpense = New System.Windows.Forms.Label
        Me.pnlFliter = New System.Windows.Forms.Panel
        Me.radExpYear = New System.Windows.Forms.RadioButton
        Me.radAppointedDate = New System.Windows.Forms.RadioButton
        Me.eline1 = New eZee.Common.eZeeLine
        Me.cboExpType = New System.Windows.Forms.ComboBox
        Me.lblExpenseType = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboExpense = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.rdDueDate = New System.Windows.Forms.RadioButton
        Me.LblDueDate = New System.Windows.Forms.Label
        Me.dtpDueDate = New System.Windows.Forms.DateTimePicker
        Me.rdNewEmployee = New System.Windows.Forms.RadioButton
        Me.LblValue = New System.Windows.Forms.Label
        Me.LblFrom = New System.Windows.Forms.Label
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeaveInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.txtAccrualAmount = New eZee.TextBox.IntegerTextBox
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.lblAccrualDays = New System.Windows.Forms.Label
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lbltimes = New System.Windows.Forms.Label
        Me.nudOccurance = New System.Windows.Forms.NumericUpDown
        Me.txtCfAmount = New eZee.TextBox.NumericTextBox
        Me.chkNoAction = New System.Windows.Forms.CheckBox
        Me.LblCFAmount = New System.Windows.Forms.Label
        Me.gbAssignmentSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblMonths = New System.Windows.Forms.Label
        Me.nudEligibilityAfterinMonths = New System.Windows.Forms.NumericUpDown
        Me.LblEligibilityAfter = New System.Windows.Forms.Label
        Me.grpFrequencyCycle = New System.Windows.Forms.GroupBox
        Me.rdAppointedDateCycle = New System.Windows.Forms.RadioButton
        Me.rdFinELCcycle = New System.Windows.Forms.RadioButton
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlAppDate.SuspendLayout()
        Me.pnlYear.SuspendLayout()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFliter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveInfo.SuspendLayout()
        CType(Me.nudOccurance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAssignmentSettings.SuspendLayout()
        CType(Me.nudEligibilityAfterinMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFrequencyCycle.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkProcess)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 470)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(724, 50)
        Me.objFooter.TabIndex = 4
        '
        'lnkProcess
        '
        Me.lnkProcess.AutoSize = True
        Me.lnkProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkProcess.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkProcess.Location = New System.Drawing.Point(10, 22)
        Me.lnkProcess.Name = "lnkProcess"
        Me.lnkProcess.Size = New System.Drawing.Size(0, 13)
        Me.lnkProcess.TabIndex = 2
        Me.lnkProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(512, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(615, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExp)
        Me.gbFilterCriteria.Controls.Add(Me.pnlAppDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlYear)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpense)
        Me.gbFilterCriteria.Controls.Add(Me.pnlFliter)
        Me.gbFilterCriteria.Controls.Add(Me.eline1)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpType)
        Me.gbFilterCriteria.Controls.Add(Me.lblExpenseType)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboExpense)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(724, 123)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchExp
        '
        Me.objbtnSearchExp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExp.BorderSelected = False
        Me.objbtnSearchExp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExp.Location = New System.Drawing.Point(691, 33)
        Me.objbtnSearchExp.Name = "objbtnSearchExp"
        Me.objbtnSearchExp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExp.TabIndex = 271
        '
        'pnlAppDate
        '
        Me.pnlAppDate.Controls.Add(Me.lblToDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate1)
        Me.pnlAppDate.Controls.Add(Me.lblFromDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate2)
        Me.pnlAppDate.Location = New System.Drawing.Point(531, 65)
        Me.pnlAppDate.Name = "pnlAppDate"
        Me.pnlAppDate.Size = New System.Drawing.Size(190, 52)
        Me.pnlAppDate.TabIndex = 298
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(6, 32)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(69, 15)
        Me.lblToDate.TabIndex = 305
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(81, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate1.TabIndex = 1
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(6, 5)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(69, 15)
        Me.lblFromDate.TabIndex = 304
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(81, 29)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate2.TabIndex = 304
        '
        'pnlYear
        '
        Me.pnlYear.Controls.Add(Me.lblFromYear)
        Me.pnlYear.Controls.Add(Me.nudToYear)
        Me.pnlYear.Controls.Add(Me.LblToYear)
        Me.pnlYear.Controls.Add(Me.nudFromMonth)
        Me.pnlYear.Controls.Add(Me.LblToMonth)
        Me.pnlYear.Controls.Add(Me.LblMonth)
        Me.pnlYear.Controls.Add(Me.nudToMonth)
        Me.pnlYear.Controls.Add(Me.cboTocondition)
        Me.pnlYear.Controls.Add(Me.nudFromYear)
        Me.pnlYear.Controls.Add(Me.cboFromcondition)
        Me.pnlYear.Location = New System.Drawing.Point(164, 65)
        Me.pnlYear.Name = "pnlYear"
        Me.pnlYear.Size = New System.Drawing.Size(363, 52)
        Me.pnlYear.TabIndex = 303
        '
        'lblFromYear
        '
        Me.lblFromYear.BackColor = System.Drawing.Color.Transparent
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(3, 5)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(57, 15)
        Me.lblFromYear.TabIndex = 279
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(67, 29)
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(55, 21)
        Me.nudToYear.TabIndex = 292
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToYear
        '
        Me.LblToYear.BackColor = System.Drawing.Color.Transparent
        Me.LblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToYear.Location = New System.Drawing.Point(3, 32)
        Me.LblToYear.Name = "LblToYear"
        Me.LblToYear.Size = New System.Drawing.Size(57, 15)
        Me.LblToYear.TabIndex = 291
        Me.LblToYear.Text = "To Year"
        Me.LblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(202, 2)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(55, 21)
        Me.nudFromMonth.TabIndex = 289
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToMonth
        '
        Me.LblToMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToMonth.Location = New System.Drawing.Point(128, 32)
        Me.LblToMonth.Name = "LblToMonth"
        Me.LblToMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblToMonth.TabIndex = 293
        Me.LblToMonth.Text = "To Month"
        Me.LblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonth
        '
        Me.LblMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonth.Location = New System.Drawing.Point(128, 5)
        Me.LblMonth.Name = "LblMonth"
        Me.LblMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblMonth.TabIndex = 288
        Me.LblMonth.Text = "From Month"
        Me.LblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(202, 29)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(55, 21)
        Me.nudToMonth.TabIndex = 294
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTocondition
        '
        Me.cboTocondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTocondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTocondition.FormattingEnabled = True
        Me.cboTocondition.Location = New System.Drawing.Point(263, 29)
        Me.cboTocondition.Name = "cboTocondition"
        Me.cboTocondition.Size = New System.Drawing.Size(97, 21)
        Me.cboTocondition.TabIndex = 297
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(67, 2)
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(55, 21)
        Me.nudFromYear.TabIndex = 281
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboFromcondition
        '
        Me.cboFromcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromcondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromcondition.FormattingEnabled = True
        Me.cboFromcondition.Location = New System.Drawing.Point(263, 2)
        Me.cboFromcondition.Name = "cboFromcondition"
        Me.cboFromcondition.Size = New System.Drawing.Size(97, 21)
        Me.cboFromcondition.TabIndex = 295
        '
        'lblExpense
        '
        Me.lblExpense.BackColor = System.Drawing.Color.Transparent
        Me.lblExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpense.Location = New System.Drawing.Point(418, 35)
        Me.lblExpense.Name = "lblExpense"
        Me.lblExpense.Size = New System.Drawing.Size(55, 17)
        Me.lblExpense.TabIndex = 230
        Me.lblExpense.Text = "Expense"
        Me.lblExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlFliter
        '
        Me.pnlFliter.Controls.Add(Me.radExpYear)
        Me.pnlFliter.Controls.Add(Me.radAppointedDate)
        Me.pnlFliter.Location = New System.Drawing.Point(11, 65)
        Me.pnlFliter.Name = "pnlFliter"
        Me.pnlFliter.Size = New System.Drawing.Size(152, 52)
        Me.pnlFliter.TabIndex = 1
        '
        'radExpYear
        '
        Me.radExpYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExpYear.Location = New System.Drawing.Point(9, 3)
        Me.radExpYear.Name = "radExpYear"
        Me.radExpYear.Size = New System.Drawing.Size(139, 16)
        Me.radExpYear.TabIndex = 303
        Me.radExpYear.TabStop = True
        Me.radExpYear.Text = "Year of Experience"
        Me.radExpYear.UseVisualStyleBackColor = True
        '
        'radAppointedDate
        '
        Me.radAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointedDate.Location = New System.Drawing.Point(9, 30)
        Me.radAppointedDate.Name = "radAppointedDate"
        Me.radAppointedDate.Size = New System.Drawing.Size(139, 16)
        Me.radAppointedDate.TabIndex = 1
        Me.radAppointedDate.TabStop = True
        Me.radAppointedDate.Text = "Appointment Date"
        Me.radAppointedDate.UseVisualStyleBackColor = True
        '
        'eline1
        '
        Me.eline1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eline1.Location = New System.Drawing.Point(10, 57)
        Me.eline1.Name = "eline1"
        Me.eline1.Size = New System.Drawing.Size(713, 5)
        Me.eline1.TabIndex = 1
        Me.eline1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpType
        '
        Me.cboExpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpType.DropDownWidth = 300
        Me.cboExpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpType.FormattingEnabled = True
        Me.cboExpType.Location = New System.Drawing.Point(280, 33)
        Me.cboExpType.Name = "cboExpType"
        Me.cboExpType.Size = New System.Drawing.Size(132, 21)
        Me.cboExpType.TabIndex = 271
        '
        'lblExpenseType
        '
        Me.lblExpenseType.BackColor = System.Drawing.Color.Transparent
        Me.lblExpenseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseType.Location = New System.Drawing.Point(192, 36)
        Me.lblExpenseType.Name = "lblExpenseType"
        Me.lblExpenseType.Size = New System.Drawing.Size(82, 15)
        Me.lblExpenseType.TabIndex = 271
        Me.lblExpenseType.Text = "Expense Cat."
        Me.lblExpenseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.Location = New System.Drawing.Point(572, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(96, 16)
        Me.lnkAllocation.TabIndex = 284
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(697, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'cboExpense
        '
        Me.cboExpense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpense.DropDownWidth = 300
        Me.cboExpense.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpense.FormattingEnabled = True
        Me.cboExpense.Location = New System.Drawing.Point(478, 33)
        Me.cboExpense.Name = "cboExpense"
        Me.cboExpense.Size = New System.Drawing.Size(207, 21)
        Me.cboExpense.TabIndex = 4
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(674, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(65, 33)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(121, 21)
        Me.cboGender.TabIndex = 278
        '
        'lblGender
        '
        Me.lblGender.BackColor = System.Drawing.Color.Transparent
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(8, 36)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(51, 15)
        Me.lblGender.TabIndex = 277
        Me.lblGender.Text = "Gender"
        '
        'rdDueDate
        '
        Me.rdDueDate.Checked = True
        Me.rdDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdDueDate.Location = New System.Drawing.Point(200, 58)
        Me.rdDueDate.Name = "rdDueDate"
        Me.rdDueDate.Size = New System.Drawing.Size(134, 17)
        Me.rdDueDate.TabIndex = 300
        Me.rdDueDate.TabStop = True
        Me.rdDueDate.Text = "Due Date"
        Me.rdDueDate.UseVisualStyleBackColor = True
        '
        'LblDueDate
        '
        Me.LblDueDate.BackColor = System.Drawing.Color.Transparent
        Me.LblDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDueDate.Location = New System.Drawing.Point(351, 58)
        Me.LblDueDate.Name = "LblDueDate"
        Me.LblDueDate.Size = New System.Drawing.Size(120, 16)
        Me.LblDueDate.TabIndex = 301
        Me.LblDueDate.Text = "Due Date"
        '
        'dtpDueDate
        '
        Me.dtpDueDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDueDate.Checked = False
        Me.dtpDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDueDate.Location = New System.Drawing.Point(479, 56)
        Me.dtpDueDate.Name = "dtpDueDate"
        Me.dtpDueDate.Size = New System.Drawing.Size(88, 21)
        Me.dtpDueDate.TabIndex = 300
        '
        'rdNewEmployee
        '
        Me.rdNewEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdNewEmployee.Location = New System.Drawing.Point(199, 31)
        Me.rdNewEmployee.Name = "rdNewEmployee"
        Me.rdNewEmployee.Size = New System.Drawing.Size(135, 17)
        Me.rdNewEmployee.TabIndex = 299
        Me.rdNewEmployee.Text = "New Employee Accrue"
        Me.rdNewEmployee.UseVisualStyleBackColor = True
        '
        'LblValue
        '
        Me.LblValue.BackColor = System.Drawing.Color.Transparent
        Me.LblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValue.Location = New System.Drawing.Point(406, 126)
        Me.LblValue.Name = "LblValue"
        Me.LblValue.Size = New System.Drawing.Size(310, 16)
        Me.LblValue.TabIndex = 292
        Me.LblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFrom
        '
        Me.LblFrom.BackColor = System.Drawing.Color.Transparent
        Me.LblFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFrom.Location = New System.Drawing.Point(346, 126)
        Me.LblFrom.Name = "LblFrom"
        Me.LblFrom.Size = New System.Drawing.Size(59, 16)
        Me.LblFrom.TabIndex = 291
        Me.LblFrom.Text = "From"
        Me.LblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 125)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(339, 21)
        Me.txtSearch.TabIndex = 290
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(10, 154)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 293
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSelect, Me.dgColhEmployee, Me.dgColhAppointdate, Me.dgcolhJobTitle})
        Me.dgEmployee.Location = New System.Drawing.Point(3, 148)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.Size = New System.Drawing.Size(451, 231)
        Me.dgEmployee.TabIndex = 294
        '
        'objSelect
        '
        Me.objSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objSelect.Frozen = True
        Me.objSelect.HeaderText = ""
        Me.objSelect.Name = "objSelect"
        Me.objSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objSelect.Width = 25
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmployee.Frozen = True
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.Width = 250
        '
        'dgColhAppointdate
        '
        Me.dgColhAppointdate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppointdate.HeaderText = "Appointment Date"
        Me.dgColhAppointdate.Name = "dgColhAppointdate"
        Me.dgColhAppointdate.ReadOnly = True
        Me.dgColhAppointdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.Width = 245
        '
        'gbLeaveInfo
        '
        Me.gbLeaveInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.Checked = False
        Me.gbLeaveInfo.CollapseAllExceptThis = False
        Me.gbLeaveInfo.CollapsedHoverImage = Nothing
        Me.gbLeaveInfo.CollapsedNormalImage = Nothing
        Me.gbLeaveInfo.CollapsedPressedImage = Nothing
        Me.gbLeaveInfo.CollapseOnLoad = False
        Me.gbLeaveInfo.Controls.Add(Me.lblStartDate)
        Me.gbLeaveInfo.Controls.Add(Me.txtAccrualAmount)
        Me.gbLeaveInfo.Controls.Add(Me.lblStopDate)
        Me.gbLeaveInfo.Controls.Add(Me.rdDueDate)
        Me.gbLeaveInfo.Controls.Add(Me.lblAccrualDays)
        Me.gbLeaveInfo.Controls.Add(Me.rdNewEmployee)
        Me.gbLeaveInfo.Controls.Add(Me.dtpStopDate)
        Me.gbLeaveInfo.Controls.Add(Me.dtpStartDate)
        Me.gbLeaveInfo.Controls.Add(Me.LblDueDate)
        Me.gbLeaveInfo.Controls.Add(Me.dtpDueDate)
        Me.gbLeaveInfo.ExpandedHoverImage = Nothing
        Me.gbLeaveInfo.ExpandedNormalImage = Nothing
        Me.gbLeaveInfo.ExpandedPressedImage = Nothing
        Me.gbLeaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveInfo.HeaderHeight = 25
        Me.gbLeaveInfo.HeaderMessage = ""
        Me.gbLeaveInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.HeightOnCollapse = 0
        Me.gbLeaveInfo.LeftTextSpace = 0
        Me.gbLeaveInfo.Location = New System.Drawing.Point(0, 384)
        Me.gbLeaveInfo.Name = "gbLeaveInfo"
        Me.gbLeaveInfo.OpenHeight = 300
        Me.gbLeaveInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveInfo.ShowBorder = True
        Me.gbLeaveInfo.ShowCheckBox = False
        Me.gbLeaveInfo.ShowCollapseButton = False
        Me.gbLeaveInfo.ShowDefaultBorderColor = True
        Me.gbLeaveInfo.ShowDownButton = False
        Me.gbLeaveInfo.ShowHeader = True
        Me.gbLeaveInfo.Size = New System.Drawing.Size(724, 83)
        Me.gbLeaveInfo.TabIndex = 295
        Me.gbLeaveInfo.Temp = 0
        Me.gbLeaveInfo.Text = "Assignment Info"
        Me.gbLeaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(8, 31)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(71, 16)
        Me.lblStartDate.TabIndex = 263
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccrualAmount
        '
        Me.txtAccrualAmount.AllowNegative = False
        Me.txtAccrualAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccrualAmount.DigitsInGroup = 0
        Me.txtAccrualAmount.Flags = 65536
        Me.txtAccrualAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccrualAmount.Location = New System.Drawing.Point(479, 29)
        Me.txtAccrualAmount.MaxDecimalPlaces = 0
        Me.txtAccrualAmount.MaxWholeDigits = 21
        Me.txtAccrualAmount.Name = "txtAccrualAmount"
        Me.txtAccrualAmount.Prefix = ""
        Me.txtAccrualAmount.RangeMax = 2147483647
        Me.txtAccrualAmount.RangeMin = -2147483648
        Me.txtAccrualAmount.Size = New System.Drawing.Size(88, 21)
        Me.txtAccrualAmount.TabIndex = 8
        Me.txtAccrualAmount.Text = "0"
        Me.txtAccrualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblStopDate
        '
        Me.lblStopDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(8, 58)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(71, 16)
        Me.lblStopDate.TabIndex = 265
        Me.lblStopDate.Text = "Stop Date"
        Me.lblStopDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAccrualDays
        '
        Me.lblAccrualDays.BackColor = System.Drawing.Color.Transparent
        Me.lblAccrualDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccrualDays.Location = New System.Drawing.Point(351, 31)
        Me.lblAccrualDays.Name = "lblAccrualDays"
        Me.lblAccrualDays.Size = New System.Drawing.Size(120, 16)
        Me.lblAccrualDays.TabIndex = 267
        Me.lblAccrualDays.Text = "Accrue Value/Amount"
        Me.lblAccrualDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(85, 56)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpStopDate.TabIndex = 7
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(85, 29)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpStartDate.TabIndex = 6
        '
        'lbltimes
        '
        Me.lbltimes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltimes.Location = New System.Drawing.Point(7, 100)
        Me.lbltimes.Name = "lbltimes"
        Me.lbltimes.Size = New System.Drawing.Size(87, 17)
        Me.lbltimes.TabIndex = 309
        Me.lbltimes.Text = "Frequency"
        Me.lbltimes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudOccurance
        '
        Me.nudOccurance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudOccurance.Location = New System.Drawing.Point(99, 99)
        Me.nudOccurance.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudOccurance.Name = "nudOccurance"
        Me.nudOccurance.Size = New System.Drawing.Size(56, 21)
        Me.nudOccurance.TabIndex = 308
        Me.nudOccurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCfAmount
        '
        Me.txtCfAmount.AllowNegative = False
        Me.txtCfAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCfAmount.DigitsInGroup = 0
        Me.txtCfAmount.Flags = 65536
        Me.txtCfAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCfAmount.Location = New System.Drawing.Point(195, 178)
        Me.txtCfAmount.MaxDecimalPlaces = 2
        Me.txtCfAmount.MaxWholeDigits = 4
        Me.txtCfAmount.Name = "txtCfAmount"
        Me.txtCfAmount.Prefix = ""
        Me.txtCfAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCfAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCfAmount.Size = New System.Drawing.Size(62, 21)
        Me.txtCfAmount.TabIndex = 291
        Me.txtCfAmount.Text = "0"
        Me.txtCfAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkNoAction
        '
        Me.chkNoAction.BackColor = System.Drawing.Color.Transparent
        Me.chkNoAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoAction.Location = New System.Drawing.Point(8, 152)
        Me.chkNoAction.Name = "chkNoAction"
        Me.chkNoAction.Size = New System.Drawing.Size(247, 19)
        Me.chkNoAction.TabIndex = 295
        Me.chkNoAction.Text = " Carry Forward Existing Balance to Next Year"
        Me.chkNoAction.UseVisualStyleBackColor = False
        '
        'LblCFAmount
        '
        Me.LblCFAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCFAmount.Location = New System.Drawing.Point(8, 176)
        Me.LblCFAmount.Name = "LblCFAmount"
        Me.LblCFAmount.Size = New System.Drawing.Size(175, 19)
        Me.LblCFAmount.TabIndex = 292
        Me.LblCFAmount.Text = "Max Days to be C/F to Next Year"
        Me.LblCFAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbAssignmentSettings
        '
        Me.gbAssignmentSettings.BorderColor = System.Drawing.Color.Black
        Me.gbAssignmentSettings.Checked = False
        Me.gbAssignmentSettings.CollapseAllExceptThis = False
        Me.gbAssignmentSettings.CollapsedHoverImage = Nothing
        Me.gbAssignmentSettings.CollapsedNormalImage = Nothing
        Me.gbAssignmentSettings.CollapsedPressedImage = Nothing
        Me.gbAssignmentSettings.CollapseOnLoad = False
        Me.gbAssignmentSettings.Controls.Add(Me.LblMonths)
        Me.gbAssignmentSettings.Controls.Add(Me.nudEligibilityAfterinMonths)
        Me.gbAssignmentSettings.Controls.Add(Me.LblEligibilityAfter)
        Me.gbAssignmentSettings.Controls.Add(Me.grpFrequencyCycle)
        Me.gbAssignmentSettings.Controls.Add(Me.nudOccurance)
        Me.gbAssignmentSettings.Controls.Add(Me.chkNoAction)
        Me.gbAssignmentSettings.Controls.Add(Me.lbltimes)
        Me.gbAssignmentSettings.Controls.Add(Me.LblCFAmount)
        Me.gbAssignmentSettings.Controls.Add(Me.txtCfAmount)
        Me.gbAssignmentSettings.ExpandedHoverImage = Nothing
        Me.gbAssignmentSettings.ExpandedNormalImage = Nothing
        Me.gbAssignmentSettings.ExpandedPressedImage = Nothing
        Me.gbAssignmentSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignmentSettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignmentSettings.HeaderHeight = 25
        Me.gbAssignmentSettings.HeaderMessage = ""
        Me.gbAssignmentSettings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAssignmentSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignmentSettings.HeightOnCollapse = 0
        Me.gbAssignmentSettings.LeftTextSpace = 0
        Me.gbAssignmentSettings.Location = New System.Drawing.Point(457, 148)
        Me.gbAssignmentSettings.Name = "gbAssignmentSettings"
        Me.gbAssignmentSettings.OpenHeight = 300
        Me.gbAssignmentSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignmentSettings.ShowBorder = True
        Me.gbAssignmentSettings.ShowCheckBox = False
        Me.gbAssignmentSettings.ShowCollapseButton = False
        Me.gbAssignmentSettings.ShowDefaultBorderColor = True
        Me.gbAssignmentSettings.ShowDownButton = False
        Me.gbAssignmentSettings.ShowHeader = True
        Me.gbAssignmentSettings.Size = New System.Drawing.Size(263, 231)
        Me.gbAssignmentSettings.TabIndex = 296
        Me.gbAssignmentSettings.Temp = 0
        Me.gbAssignmentSettings.Text = "Assignment Settings"
        Me.gbAssignmentSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonths
        '
        Me.LblMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonths.Location = New System.Drawing.Point(161, 127)
        Me.LblMonths.Name = "LblMonths"
        Me.LblMonths.Size = New System.Drawing.Size(87, 17)
        Me.LblMonths.TabIndex = 314
        Me.LblMonths.Text = "Months"
        Me.LblMonths.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudEligibilityAfterinMonths
        '
        Me.nudEligibilityAfterinMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEligibilityAfterinMonths.Location = New System.Drawing.Point(99, 125)
        Me.nudEligibilityAfterinMonths.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudEligibilityAfterinMonths.Name = "nudEligibilityAfterinMonths"
        Me.nudEligibilityAfterinMonths.Size = New System.Drawing.Size(56, 21)
        Me.nudEligibilityAfterinMonths.TabIndex = 312
        Me.nudEligibilityAfterinMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblEligibilityAfter
        '
        Me.LblEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEligibilityAfter.Location = New System.Drawing.Point(7, 127)
        Me.LblEligibilityAfter.Name = "LblEligibilityAfter"
        Me.LblEligibilityAfter.Size = New System.Drawing.Size(87, 17)
        Me.LblEligibilityAfter.TabIndex = 313
        Me.LblEligibilityAfter.Text = "Eligibility After"
        Me.LblEligibilityAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpFrequencyCycle
        '
        Me.grpFrequencyCycle.Controls.Add(Me.rdAppointedDateCycle)
        Me.grpFrequencyCycle.Controls.Add(Me.rdFinELCcycle)
        Me.grpFrequencyCycle.Location = New System.Drawing.Point(4, 29)
        Me.grpFrequencyCycle.Name = "grpFrequencyCycle"
        Me.grpFrequencyCycle.Size = New System.Drawing.Size(254, 61)
        Me.grpFrequencyCycle.TabIndex = 311
        Me.grpFrequencyCycle.TabStop = False
        Me.grpFrequencyCycle.Text = "Frequency Cycle"
        '
        'rdAppointedDateCycle
        '
        Me.rdAppointedDateCycle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAppointedDateCycle.Location = New System.Drawing.Point(8, 40)
        Me.rdAppointedDateCycle.Name = "rdAppointedDateCycle"
        Me.rdAppointedDateCycle.Size = New System.Drawing.Size(152, 18)
        Me.rdAppointedDateCycle.TabIndex = 8
        Me.rdAppointedDateCycle.Text = "Appointment Date"
        Me.rdAppointedDateCycle.UseVisualStyleBackColor = True
        '
        'rdFinELCcycle
        '
        Me.rdFinELCcycle.Checked = True
        Me.rdFinELCcycle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdFinELCcycle.Location = New System.Drawing.Point(8, 18)
        Me.rdFinELCcycle.Name = "rdFinELCcycle"
        Me.rdFinELCcycle.Size = New System.Drawing.Size(152, 18)
        Me.rdFinELCcycle.TabIndex = 7
        Me.rdFinELCcycle.TabStop = True
        Me.rdFinELCcycle.Text = "Financial Year / ELC"
        Me.rdFinELCcycle.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.HeaderText = "Appointment Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.Width = 240
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 230
        '
        'frmEmployeeExpenses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 520)
        Me.Controls.Add(Me.gbAssignmentSettings)
        Me.Controls.Add(Me.gbLeaveInfo)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.dgEmployee)
        Me.Controls.Add(Me.LblValue)
        Me.Controls.Add(Me.LblFrom)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeExpenses"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Expense Assignment"
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.pnlAppDate.ResumeLayout(False)
        Me.pnlYear.ResumeLayout(False)
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFliter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveInfo.ResumeLayout(False)
        Me.gbLeaveInfo.PerformLayout()
        CType(Me.nudOccurance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAssignmentSettings.ResumeLayout(False)
        Me.gbAssignmentSettings.PerformLayout()
        CType(Me.nudEligibilityAfterinMonths, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFrequencyCycle.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAppDate As System.Windows.Forms.Panel
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlYear As System.Windows.Forms.Panel
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToYear As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToMonth As System.Windows.Forms.Label
    Friend WithEvents LblMonth As System.Windows.Forms.Label
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboTocondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboFromcondition As System.Windows.Forms.ComboBox
    Friend WithEvents pnlFliter As System.Windows.Forms.Panel
    Friend WithEvents radExpYear As System.Windows.Forms.RadioButton
    Friend WithEvents radAppointedDate As System.Windows.Forms.RadioButton
    Friend WithEvents eline1 As eZee.Common.eZeeLine
    Friend WithEvents cboExpType As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseType As System.Windows.Forms.Label
    Friend WithEvents rdDueDate As System.Windows.Forms.RadioButton
    Friend WithEvents LblDueDate As System.Windows.Forms.Label
    Friend WithEvents dtpDueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents rdNewEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents LblValue As System.Windows.Forms.Label
    Friend WithEvents LblFrom As System.Windows.Forms.Label
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents gbLeaveInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchExp As eZee.Common.eZeeGradientButton
    Friend WithEvents chkNoAction As System.Windows.Forms.CheckBox
    Friend WithEvents lblExpense As System.Windows.Forms.Label
    Friend WithEvents txtAccrualAmount As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtCfAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents LblCFAmount As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents cboExpense As System.Windows.Forms.ComboBox
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAccrualDays As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lbltimes As System.Windows.Forms.Label
    Friend WithEvents nudOccurance As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbAssignmentSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents grpFrequencyCycle As System.Windows.Forms.GroupBox
    Friend WithEvents rdAppointedDateCycle As System.Windows.Forms.RadioButton
    Friend WithEvents rdFinELCcycle As System.Windows.Forms.RadioButton
    Friend WithEvents objSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblMonths As System.Windows.Forms.Label
    Friend WithEvents nudEligibilityAfterinMonths As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblEligibilityAfter As System.Windows.Forms.Label
End Class
