﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeExpenses

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeExpenses"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintYearunkid As Integer = -1
    Private mdtEmployee As DataTable = Nothing
    Private dvEmployee As DataView
    Private objEmpExpBal As clsEmployeeExpenseBalance
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private dsEmployee As DataSet
    Private mblnIsLeaveEncashment As Boolean = False


    'Pinkal (06-Jul-2016) -- Start
    'Enhancement - Working on CCK Changes for Expense Which is Accrued as Amount.
    Private mblnIsAccrue As Boolean = False
    Private mblnIsAccrueAmountASUOM As Boolean = False
    'Pinkal (06-Jul-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmEmployeeExpenses_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpExpBal = New clsEmployeeExpenseBalance
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'radAppointedDate.Checked = True
            'Pinkal (26-Dec-2018) -- End

            mintYearunkid = FinancialYear._Object._YearUnkid

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date
                dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpStopDate.MaxDate = FinancialYear._Object._Database_End_Date

                'Shani(13-Aug-2015) -- Start
                'Changes in Employe Expense Assignment(C&R).
                'LblDueDate.Enabled = False
                'dtpDueDate.Enabled = False
                'rdNewEmployee.Enabled = False
                'rdDueDate.Enabled = False
                LblDueDate.Visible = False
                dtpDueDate.Visible = False
                rdNewEmployee.Visible = False
                rdNewEmployee.Checked = False
                rdDueDate.Visible = False
                rdDueDate.Checked = False
                'Shani(13-Aug-2015) -- End

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                LblDueDate.Enabled = True
                dtpDueDate.Enabled = True
                rdNewEmployee.Enabled = True
                rdDueDate.Visible = True
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpDueDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpStartDate.Enabled = False
                dtpStopDate.Enabled = False
            End If

            If CInt(cboExpType.SelectedValue) <= 0 Then
                'pnlData1.Enabled = False
                Control_Enable_Disble(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExpenses_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmployeeExpenses_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExpenses_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeExpenses_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeExpenses_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeExpenses_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmpExpBal = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsEmployeeExpenseBalance.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeExpenseBalance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsCombo = objMaster.getGenderList("List", True)
            With cboGender
                .DisplayMember = "Name"
                .ValueMember = "id"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombo = objMaster.GetCondition(True)
            dsCombo = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            With cboFromcondition
                .DisplayMember = "Name"
                .ValueMember = "id"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With
            With cboTocondition
                .DisplayMember = "Name"
                .ValueMember = "id"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
            End With


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            With cboExpType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub SetDataSource()
        Try

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtEmployee Is Nothing Then Exit Sub
            'Pinkal (26-Feb-2019) -- End


            dvEmployee = mdtEmployee.DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgEmployee.DataSource = dvEmployee
            objSelect.DataPropertyName = "ischecked"

            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
            'dgColhEmpCode.DataPropertyName = "employeecode"
            'dgColhEmployee.DataPropertyName = "name"
            'dgColhAppointdate.DataPropertyName = "appointeddate"
            'dgcolhJobTitle.DataPropertyName = "job_name"

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'dgColhEmpCode.DataPropertyName = "Code"
            'Pinkal (26-Feb-2019) -- End

            dgColhEmployee.DataPropertyName = "Employee Name"
            dgColhAppointdate.DataPropertyName = "Appointed Date"
            dgcolhJobTitle.DataPropertyName = "Job"
            'Pinkal (14-Dec-2017) -- End

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
            'Pinkal (09-Feb-2016) -- Start
            'Enhancement - As per Matthew Request Put Employee count when searching.
        Finally
            objbtnSearch.ShowResult(dgEmployee.RowCount.ToString())
            'Pinkal (09-Feb-2016) -- End
        End Try
    End Sub

    Private Sub Get_Employee()
        'Pinkal (14-Dec-2017) -- Start
        'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
        Me.Cursor = Cursors.WaitCursor
        'Pinkal (14-Dec-2017) -- End
        Try
            Dim strSearching As String = ""
            Dim objEmployee As New clsEmployee_Master

            Dim blnIncludeReinstatemetDate As Boolean = False
            Dim dtStartDate, dtEndDate As Date

            dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)

            If radAppointedDate.Checked = True Then
                dtStartDate = dtpDate1.Value.Date
                dtEndDate = dtpDate2.Value.Date
            Else
                blnIncludeReinstatemetDate = True
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                If rdNewEmployee.Checked Then

                    Dim mstrEmployeId As String = objEmpExpBal.GetNewEmployeeForELC(FinancialYear._Object._DatabaseName, _
                                                                                    User._Object._Userunkid, _
                                                                                    FinancialYear._Object._YearUnkid, _
                                                                                    Company._Object._Companyunkid, _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                                    CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid)
                    If mstrEmployeId.Trim.Length > 0 Then
                        strSearching = " AND hremployee_master.employeeunkid in (" & mstrEmployeId & ") "
                    Else
                        strSearching = " AND hremployee_master.employeeunkid = 0"
                    End If

                ElseIf rdDueDate.Checked Then
                    Dim mstrEmployeId As String = objEmpExpBal.GetEmployeeForDueDateELC(dtpDueDate.Value.Date, CInt(cboExpense.SelectedValue), FinancialYear._Object._YearUnkid)
                    If mstrEmployeId.Trim.Length > 0 Then
                        strSearching = " AND hremployee_master.employeeunkid in (" & mstrEmployeId & ") "
                    Else
                        strSearching = " AND hremployee_master.employeeunkid = 0"
                    End If
                End If
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                strSearching &= "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
            End If

            If radExpYear.Checked = True Then
                If nudFromYear.Value > 0 Or nudFromMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If nudToYear.Value > 0 Or nudToMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.Text & " '" & eZeeDate.convertDate(mdtDate) & "' "
                End If
            End If



            'Pinkal (22-Oct-2018) -- Start
            'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
            If radAppointedDate.Checked = True Then
                strSearching &= "AND convert(CHAR(8),hremployee_master.appointeddate,112) BETWEEN '" & eZeeDate.convertDate(dtStartDate) & "' AND '" & eZeeDate.convertDate(dtEndDate) & "'"
            End If
            'Pinkal (22-Oct-2018) -- End


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If

            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 dtStartDate, _
            '                                 dtEndDate, _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                 "Employee", ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching, blnIncludeReinstatemetDate)



            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_Job


            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, _
                                              FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             dtStartDate, _
                                             dtEndDate, _
                                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , strSearching)


            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                If dsEmployee.Tables(0).Columns.Contains("startdate") = False Then
                    dsEmployee.Tables(0).Columns.Add("startdate", Type.GetType("System.DateTime"))
                End If

                If dsEmployee.Tables(0).Columns.Contains("stopdate") = False Then
                    dsEmployee.Tables(0).Columns.Add("stopdate", Type.GetType("System.DateTime"))
                End If

            End If

            mdtEmployee = dsEmployee.Tables(0)

            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
            If mdtEmployee.Columns.Contains("ischecked") = False Then
                mdtEmployee.Columns.Add("ischecked", Type.GetType("System.Boolean"))
                mdtEmployee.Columns("ischecked").DefaultValue = False
            End If

            'For Each dr As DataRow In mdtEmployee.Rows
            '    dr("appointeddate") = eZeeDate.convertDate(dr("appointeddate").ToString()).ToShortDateString

            '    Dim mdtDate As Date = Nothing
            '    If mdtEmployee.Columns.Contains("startdate") Then
            '        mdtDate = objEmpExpBal.GetEmployeeDates(CInt(dr("employeeunkid")), CInt(cboExpense.SelectedValue), True)
            '        If mdtDate.Date <> Nothing Then
            '            dr("startdate") = mdtDate
            '        End If
            '    End If

            '    mdtDate = Nothing
            '    If mdtEmployee.Columns.Contains("stopdate") Then
            '        mdtDate = objEmpExpBal.GetEmployeeDates(CInt(dr("employeeunkid")), CInt(cboExpense.SelectedValue), False)
            '        If mdtDate <> Nothing Then
            '            dr("stopdate") = mdtDate
            '        End If
            '    End If
            'Next

            mdtEmployee.AsEnumerable().ToList.ForEach(Function(x) UpdateRow(x))

            'Pinkal (14-Dec-2017) -- End

            Call SetDataSource()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Employee", mstrModuleName)
        Finally
            'Pinkal (14-Dec-2017) -- Start
            'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
            Me.Cursor = Cursors.Default
            'Pinkal (14-Dec-2017) -- End
        End Try
    End Sub

    Private Sub SetValue(ByVal iEmployeeId As Integer)
        Try
            objEmpExpBal._Crexpbalanceunkid = 0
            objEmpExpBal._Employeeunkid = iEmployeeId
            objEmpExpBal._Expenseunkid = CInt(cboExpense.SelectedValue)

            If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    objEmpExpBal._Startdate = dtpStartDate.Value.Date
                    objEmpExpBal._Enddate = CDate(IIf(dtpStopDate.Checked, dtpStopDate.Value.Date, Nothing))
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    objEmpExpBal._Startdate = Nothing
                    objEmpExpBal._Enddate = Nothing
                End If
            Else
                objEmpExpBal._Startdate = dtpStartDate.Value.Date
                objEmpExpBal._Enddate = CDate(IIf(dtpStopDate.Checked, dtpStopDate.Value.Date, Nothing))
            End If

            objEmpExpBal._Userunkid = User._Object._Userunkid
            objEmpExpBal._Cfamount = CDec(IIf(txtCfAmount.Text.Trim.Length > 0, txtCfAmount.Text, 0))
            objEmpExpBal._Eligibilityafter = 0
            objEmpExpBal._Isnoaction = chkNoAction.Checked
            Dim mdtStartdate As DateTime
            Dim mdtStopdate As DateTime

            If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    mdtStartdate = dtpStartDate.Value
                    If dtpStopDate.Checked Then
                        mdtStopdate = dtpStopDate.Value
                    Else
                        mdtStopdate = FinancialYear._Object._Database_End_Date
                    End If
                End If
            Else
                mdtStartdate = dtpStartDate.Value
                If dtpStopDate.Checked Then
                    mdtStopdate = dtpStopDate.Value
                Else
                    mdtStopdate = FinancialYear._Object._Database_End_Date
                End If
            End If

            objEmpExpBal._Yearunkid = mintYearunkid
            objEmpExpBal._Isclose_Fy = False
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = iEmployeeId
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END


            If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE Then

                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    If rdNewEmployee.Checked Then
                        If objEmp._Appointeddate.Date <> Nothing Then
                            mdtStartdate = objEmp._Appointeddate.Date
                        End If

                        If objEmp._Reinstatementdate.Date <> Nothing AndAlso (objEmp._Reinstatementdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date) Then
                            mdtStartdate = objEmp._Reinstatementdate.Date
                        End If

                        If mdtStartdate.Date < FinancialYear._Object._Database_Start_Date Then
                            mdtStartdate = FinancialYear._Object._Database_Start_Date.Date
                        End If

                        If objEmp._Termination_To_Date.Date <> Nothing AndAlso (objEmp._Termination_To_Date.Date < mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)) Then
                            mdtStopdate = objEmp._Termination_To_Date.Date
                        Else
                            mdtStopdate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)
                        End If

                        objEmpExpBal._Startdate = mdtStartdate.Date
                        objEmpExpBal._Enddate = mdtStopdate.Date

                    ElseIf rdDueDate.Checked Then

                        'Shani(13-Aug-2015) -- Start
                        'Changes in Employe Expense Assignment(C&R).
                        Dim objExpBalance As New clsEmployeeExpenseBalance
                        mdtStopdate = objExpBalance.GetEmployeeDates(iEmployeeId, CInt(cboExpense.SelectedValue), False)
                        'Shani(13-Aug-2015) -- End

                        mdtStartdate = CDate(mdtStopdate).AddDays(1).Date
                        mdtStopdate = mdtStartdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).Date.AddDays(-1)

                        objEmpExpBal._Startdate = mdtStartdate.Date
                        objEmpExpBal._Enddate = mdtStopdate.Date
                    End If

                    'Pinkal (06-Jul-2016) -- Start
                    'Enhancement - Working on CCK Changes for Expense Which is Accrued as Amount.
                    If mblnIsAccrue AndAlso mblnIsAccrueAmountASUOM = False Then
                        objEmpExpBal._Daily_Amount = CDec(IIf(txtAccrualAmount.Text.Trim.Length > 0, txtAccrualAmount.Text.Trim, 0)) / DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1))
                    End If
                    'Pinkal (06-Jul-2016) -- End

                    objEmpExpBal._Iselc = True
                    objEmpExpBal._Isopenelc = True

                    'Shani(13-Aug-2015) -- Start
                    'Changes in Employe Expense Assignment(C&R).
                    'Else
                ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
FinancialDateChecking:
                    If objEmp._Appointeddate.Date <> Nothing AndAlso objEmp._Appointeddate.Date > mdtStartdate.Date Then
                        mdtStartdate = objEmp._Appointeddate.Date
                    End If

                    If objEmp._Reinstatementdate.Date <> Nothing AndAlso (objEmp._Reinstatementdate.Date > mdtStartdate.Date) Then
                        mdtStartdate = objEmp._Reinstatementdate.Date
                    End If

                    If mdtStartdate.Date < FinancialYear._Object._Database_Start_Date Then
                        mdtStartdate = FinancialYear._Object._Database_Start_Date.Date
                    End If

                    If dtpStopDate.Checked Then
                        mdtStopdate = dtpStopDate.Value.Date

                        If objEmp._Empl_Enddate <> Nothing AndAlso objEmp._Empl_Enddate.Date < mdtStopdate.Date Then
                            mdtStopdate = objEmp._Empl_Enddate.Date
                        End If

                        If objEmp._Termination_From_Date <> Nothing AndAlso objEmp._Termination_From_Date.Date < mdtStopdate.Date Then
                            mdtStopdate = objEmp._Termination_From_Date.Date
                        End If

                    Else
                        If objEmp._Termination_To_Date.Date <> Nothing AndAlso (objEmp._Termination_To_Date.Date < FinancialYear._Object._Database_End_Date.Date) Then
                            mdtStopdate = objEmp._Termination_To_Date.Date
                        Else
                            mdtStopdate = FinancialYear._Object._Database_End_Date.Date
                        End If
                    End If
                    objEmpExpBal._Startdate = mdtStartdate.Date
                    objEmpExpBal._Enddate = mdtStopdate.Date

                    'Pinkal (06-Jul-2016) -- Start
                    'Enhancement - Working on CCK Changes for Expense Which is Accrued as Amount.
                    If mblnIsAccrue AndAlso mblnIsAccrueAmountASUOM = False Then
                        objEmpExpBal._Daily_Amount = CDec(IIf(txtAccrualAmount.Text.Trim.Length > 0, txtAccrualAmount.Text.Trim, 0)) / DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.Date.AddDays(1))
                    End If
                    'Pinkal (06-Jul-2016) -- End

                    'Shani(13-Aug-2015) -- End
                    objEmpExpBal._Iselc = False
                    objEmpExpBal._Isopenelc = False

                End If

            Else
                GoTo FinancialDateChecking
            End If


            'Pinkal (06-Jul-2016) -- Start
            'Enhancement - Working on CCK Changes for Expense Which is Accrued as Amount.
            Dim intDiff As Integer = 0
            If mblnIsAccrue AndAlso mblnIsAccrueAmountASUOM = False Then
                intDiff = CInt(DateDiff(DateInterval.Day, mdtStartdate.Date, mdtStopdate.Date.AddDays(1)))
            End If
            'Pinkal (06-Jul-2016) -- End

            Dim dsList As DataSet = Nothing : Dim mstrFilter As String = ""

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objEmpExpBal.GetList("List", True, iEmployeeId, CInt(cboExpense.SelectedValue)) : mstrFilter = "yearunkid <>" & mintYearunkid & " "
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objEmpExpBal.GetList("List", True, iEmployeeId, CInt(cboExpense.SelectedValue)) : mstrFilter = ""
            End If

            Dim dtList As DataTable = New DataView(dsList.Tables("List"), mstrFilter, "enddate desc", DataViewRowState.CurrentRows).ToTable

            'Pinkal (06-Jul-2016) -- Start
            'Enhancement - Working on CCK Changes for Expense Which is Accrued as Amount.
            If mblnIsAccrue AndAlso mblnIsAccrueAmountASUOM = False Then
                objEmpExpBal._Accrue_Amount = objEmpExpBal._Daily_Amount * intDiff
            ElseIf mblnIsAccrue AndAlso mblnIsAccrueAmountASUOM = True Then
                objEmpExpBal._Accrue_Amount = txtAccrualAmount.Decimal
            End If
            'Pinkal (06-Jul-2016) -- End


            If dtList.Rows.Count > 0 AndAlso mstrFilter.Trim.Length > 0 Then
                objEmpExpBal._Accrue_Amount = objEmpExpBal._Accrue_Amount + CDec(dtList.Rows(0)("remaining_bal"))
                objEmpExpBal._Expensebf = CDec(dtList.Rows(0)("remaining_bal"))
                objEmpExpBal._Remaining_Bal = objEmpExpBal._Accrue_Amount
            ElseIf dtList.Rows.Count <= 0 AndAlso mstrFilter.Trim.Length <= 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                objEmpExpBal._Accrue_Amount = objEmpExpBal._Accrue_Amount + CDec(dsList.Tables(0).Rows(0)("expensebf"))
                objEmpExpBal._Remaining_Bal = objEmpExpBal._Accrue_Amount
                objEmpExpBal._Expensebf = CDec(dsList.Tables(0).Rows(0)("expensebf"))
            ElseIf dtList.Rows.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso mstrFilter.Trim.Length <= 0 Then
                If CDec(dtList.Rows(0)("remaining_bal")) > txtCfAmount.Decimal AndAlso chkNoAction.Checked = False Then
                    dtList.Rows(0)("remaining_bal") = txtCfAmount.Decimal
                End If
                objEmpExpBal._Accrue_Amount = objEmpExpBal._Accrue_Amount + CDec(dtList.Rows(0)("remaining_bal"))
                objEmpExpBal._Expensebf = CDec(dtList.Rows(0)("remaining_bal"))
                objEmpExpBal._Remaining_Bal = CDec(objEmpExpBal._Accrue_Amount)
            Else
                objEmpExpBal._Expensebf = 0
                objEmpExpBal._Remaining_Bal = CDec(objEmpExpBal._Accrue_Amount)
            End If
            objEmpExpBal._Actualamount = txtAccrualAmount.Decimal
            objEmpExpBal._Isvoid = False
            objEmpExpBal._Issue_Amount = 0
            objEmpExpBal._Voiddatetime = Nothing
            objEmpExpBal._Voidreason = ""
            objEmpExpBal._Voiduserunkid = -1
            objEmpExpBal._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objEmpExpBal._ExpenseTypeId = CInt(cboExpType.SelectedValue)

            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            objEmpExpBal._Occurrence = CInt(nudOccurance.Value)
            objEmpExpBal._Remaining_Occurrence = CInt(nudOccurance.Value)
            'Pinkal (29-Feb-2016) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If rdFinELCcycle.Checked Then
                objEmpExpBal._FrequencyCycleId = enExpenseFrequencyCycle.Financial_ELC
                objEmpExpBal._EligibleYear = 0
            ElseIf rdAppointedDateCycle.Checked Then
                objEmpExpBal._FrequencyCycleId = enExpenseFrequencyCycle.AppointmentDate
                If objEmp._Appointeddate.AddMonths(CInt(nudEligibilityAfterinMonths.Value)).Year <= FinancialYear._Object._Database_Start_Date.Year Then
                    objEmpExpBal._EligibleYear = FinancialYear._Object._Database_Start_Date.Year
                Else
                    objEmpExpBal._EligibleYear = objEmp._Appointeddate.AddMonths(CInt(nudEligibilityAfterinMonths.Value)).Year
                End If
            End If
            objEmpExpBal._Eligibilityafter = CInt(nudEligibilityAfterinMonths.Value)
            'Pinkal (26-Feb-2019) -- End

            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Control_Enable_Disble(ByVal MstBln As Boolean)
        rdNewEmployee.Enabled = MstBln
        rdDueDate.Enabled = MstBln
        txtAccrualAmount.Enabled = MstBln
        dtpDueDate.Enabled = MstBln
        chkNoAction.Enabled = MstBln
        txtCfAmount.Enabled = MstBln
    End Sub

     'Pinkal (14-Dec-2017) -- Start
     'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.
 
    Private Function UpdateRow(ByVal drRow As DataRow) As Boolean
        Try
            drRow("ischecked") = False
            drRow("Appointed Date") = eZeeDate.convertDate(drRow("Appointed Date").ToString()).ToShortDateString()
            Dim mdtDate As Date = Nothing

            If drRow.Table.Columns.Contains("startdate") Then
                mdtDate = objEmpExpBal.GetEmployeeDates(CInt(drRow("employeeunkid")), CInt(cboExpense.SelectedValue), True)
                If mdtDate.Date <> Nothing Then
                    drRow("startdate") = mdtDate
                End If
            End If

            mdtDate = Nothing
            If drRow.Table.Columns.Contains("stopdate") Then
                mdtDate = objEmpExpBal.GetEmployeeDates(CInt(drRow("employeeunkid")), CInt(cboExpense.SelectedValue), False)
                If mdtDate <> Nothing Then
                    drRow("stopdate") = mdtDate
                End If
            End If


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If drRow.Table.Columns.Contains(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) Then
                'Pinkal (01-Jun-2021)-- Start
                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                'drRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) = drRow(Language.getMessage(mstrModuleName, 42, "Code")).ToString() & " - " & drRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString()
                drRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) = drRow(Language.getMessage("clsEmployee_Master", 42, "Code")).ToString() & " - " & drRow(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString()
                'Pinkal (01-Jun-2021) -- End
            End If
            'Pinkal (26-Feb-2019) -- End


            drRow.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateRow", mstrModuleName)
        End Try
        Return True
    End Function
   'Pinkal (14-Dec-2017) -- End
#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dr() As DataRow = Nothing
            If dgEmployee.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no employee to Assign Expense."), enMsgBoxStyle.Information)
                dgEmployee.Select()
                Exit Sub
            ElseIf CInt(cboExpType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue."), enMsgBoxStyle.Information)
                cboExpType.Focus()
                Exit Sub
            ElseIf CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Expense is mandatory information. Please select Expense to continue."), enMsgBoxStyle.Information)
                cboExpType.Focus()
                Exit Sub
            ElseIf mdtEmployee IsNot Nothing Then
                dr = mdtEmployee.Select("ischecked=True")
                If dr.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    dgEmployee.Select()
                    Exit Sub
                End If
            End If

            If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If txtAccrualAmount.Decimal <= 0 AndAlso txtAccrualAmount.Enabled Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Accrue value cannot be blank. Accrue value is required information."), enMsgBoxStyle.Information)
                    txtAccrualAmount.Select()
                    Exit Sub
                End If
            End If

            If txtAccrualAmount.Enabled AndAlso (txtCfAmount.Decimal > 0 Or chkNoAction.Checked = True) Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You have set CF amount or No action is checked. This will be applicable to all selected employee(s).") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 10, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If
            lnkProcess.Text = ""
            Dim blnExists As Boolean = False
            Dim blnShown As Boolean = False

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            'For i As Integer = 0 To dr.Length - 1
            '    blnExists = False
            '    Application.DoEvents() : lnkProcess.Text = Language.getMessage(mstrModuleName, 11, "Processed : ") & i + 1 & "/" & dr.Length
            '    If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '        blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(dr(i).Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, False, False)
            '    ElseIf CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '        blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(dr(i).Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, True, True, False, dtpDueDate.Value.Date)
            '    Else
            '        blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(dr(i).Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, False, False)
            '    End If


            '    If blnExists = False Then
            '        Call SetValue(CInt(dr(i).Item("employeeunkid")))
            '        blnFlag = objEmpExpBal.Insert()
            '        If blnFlag = False AndAlso objEmpExpBal._Message <> "" Then
            '            eZeeMsgBox.Show(objEmpExpBal._Message & " - [ " & dr(i).Item("name").ToString & " ] ", enMsgBoxStyle.Information)
            '        End If
            '    Else
            '        If blnShown = False Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Some of the checked employees are already assigned the selected expense." & vbCrLf & _
            '                                                "Due to this some employees will be discarded from the expense assignment process."), enMsgBoxStyle.Information)
            '            blnShown = True
            '        End If
            '        Continue For
            '    End If
            'Next


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Me.ControlBox = False
            Me.Enabled = False
            'Pinkal (26-Dec-2018) -- End



            Dim reader As DataTableReader = dr.CopyToDataTable().CreateDataReader()
            Dim i As Integer = 0
            While reader.Read
                blnExists = False
                i += 1

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'Application.DoEvents() : lnkProcess.Text = Language.getMessage(mstrModuleName, 11, "Processed : ") & i + 1 & "/" & dr.Length
                GC.Collect()
                Application.DoEvents() : lnkProcess.Text = Language.getMessage(mstrModuleName, 11, "Processed : ") & i & "/" & dr.Length
                'Pinkal (18-Mar-2021) -- End



                If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(reader.Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, False, False)
                ElseIf CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE AndAlso ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(reader.Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, True, True, False, dtpDueDate.Value.Date)
                Else
                    blnExists = objEmpExpBal.isExist(mintYearunkid, CInt(reader.Item("employeeunkid")), CInt(cboExpense.SelectedValue), -1, False, False)
                End If

                If blnExists = False Then
                    Call SetValue(CInt(reader.Item("employeeunkid")))
                    blnFlag = objEmpExpBal.Insert()
                    If blnFlag = False AndAlso objEmpExpBal._Message <> "" Then
                        eZeeMsgBox.Show(objEmpExpBal._Message & " - [ " & reader.Item("name").ToString & " ] ", enMsgBoxStyle.Information)
                    End If
                Else
                    If blnShown = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Some of the checked employees are already assigned the selected expense." & vbCrLf & _
                                                            "Due to this some employees will be discarded from the expense assignment process."), enMsgBoxStyle.Information)
                        blnShown = True
                    End If
                    Continue While
                End If

            End While

            'Pinkal (20-Nov-2018) -- End

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Employee Expenses successfully assigned."), enMsgBoxStyle.Information)
                Call objbtnReset_Click(sender, e)
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                Me.ControlBox = True
                Me.Enabled = True
                'Pinkal (26-Dec-2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            Me.ControlBox = True
            Me.Enabled = True
            'Pinkal (26-Dec-2018) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGender.SelectedValue = 0
            nudFromYear.Value = 0
            nudFromMonth.Value = 0
            nudToYear.Value = 0
            nudToMonth.Value = 0
            cboFromcondition.SelectedValue = 0
            cboTocondition.SelectedValue = 0
            mstrAdvanceFilter = ""
            rdDueDate.Checked = True
            chkSelectAll.Checked = False
            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dgEmployee.DataSource = Nothing
            cboExpense.SelectedValue = 0
            cboExpType.SelectedValue = 0
            txtSearch.Text = ""
            LblFrom.Text = ""
            LblValue.Text = ""
            txtAccrualAmount.Text = ""
            txtCfAmount.Text = ""
            lnkProcess.Text = ""

            'Shani(13-Aug-2015) -- Start
            'Changes in Employe Expense Assignment(C&R).
            chkNoAction.Checked = False
            chkSelectAll.Checked = False
            dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpStopDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpStopDate.Checked = False
            'Shani(13-Aug-2015) -- End

            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            nudOccurance.Value = 0
            'Pinkal (29-Feb-2016) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            rdFinELCcycle.Checked = True
            nudEligibilityAfterinMonths.Value = 0
            'Pinkal (26-Feb-2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchExp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExp.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboExpense.ValueMember
                .DisplayMember = cboExpense.DisplayMember
                .DataSource = CType(cboExpense.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboExpense.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExp_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboExpType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue."), enMsgBoxStyle.Information)
                cboExpType.Focus()
                Exit Sub
            End If

            If CInt(cboExpense.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Expense is mandatory information. Please select Expense to continue."), enMsgBoxStyle.Information)
                cboExpense.Focus()
                Exit Sub
            End If

            If radAppointedDate.Checked = True Then
                If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To date cannot be less than From date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (nudFromYear.Value > 0 Or nudFromMonth.Value > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition."), enMsgBoxStyle.Information)
                    cboFromcondition.Select()
                    Exit Sub
                ElseIf (nudToYear.Value > 0 Or nudToMonth.Value > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition."), enMsgBoxStyle.Information)
                    cboTocondition.Select()
                    Exit Sub
                End If
            End If
            Call Get_Employee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combox Event's "

    Private Sub cboExpType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpType.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpType.SelectedValue), True, "List")
            With cboExpense
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            Select Case CInt(cboExpType.SelectedValue)
                Case enExpenseType.EXP_LEAVE
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dtpStartDate.Enabled = False
                        dtpStopDate.Enabled = False

                        'Shani(13-Aug-2015) -- Start
                        'Changes in Employe Expense Assignment(C&R).
                        rdNewEmployee.Visible = True
                        rdDueDate.Visible = True
                        rdNewEmployee.Checked = False
                        rdDueDate.Checked = True
                        LblDueDate.Visible = True
                        dtpDueDate.Visible = True
                        dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                        'Shani(13-Aug-2015) -- End

                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dtpStartDate.Enabled = True
                        dtpStopDate.Enabled = True
                        'Shani(13-Aug-2015) -- Start
                        'Changes in Employe Expense Assignment(C&R).
                        rdNewEmployee.Visible = False
                        rdDueDate.Visible = False
                        rdNewEmployee.Checked = False
                        rdDueDate.Checked = False
                        LblDueDate.Visible = False
                        dtpDueDate.Visible = False
                        'Shani(13-Aug-2015) -- End
                    End If
                    'pnlData1.Enabled = True
                    Control_Enable_Disble(True)
                Case enExpenseType.EXP_MEDICAL, enExpenseType.EXP_TRAINING
                    'dtpStartDate.Enabled = True
                    'dtpStopDate.Enabled = True
                    'pnlData1.Enabled = True
                    Control_Enable_Disble(False)

                    'SHANI (06 JUN 2015) -- Start
                    'Enhancement : Changes in C & R module given by Mr.Andrew.

                    'Shani(13-Aug-2015) -- Start
                    'Changes in Employe Expense Assignment(C&R).
                    'rdDueDate.Enabled = True
                    'rdNewEmployee.Enabled = True
                    dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                    dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date
                    dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date
                    dtpStopDate.MaxDate = FinancialYear._Object._Database_End_Date
                    rdDueDate.Checked = False
                    rdNewEmployee.Checked = False
                    rdNewEmployee.Visible = False
                    rdDueDate.Visible = False
                    LblDueDate.Visible = False
                    dtpDueDate.Visible = False
                    'Shani(13-Aug-2015) -- End

                    'SHANI (06 JUN 2015) -- End 

                    'SHANI (06 JUN 2015) -- Start
                    'Enhancement : Changes in C & R module given by Mr.Andrew.
                Case enExpenseType.EXP_MISCELLANEOUS
                    Control_Enable_Disble(False)

                    'Shani(13-Aug-2015) -- Start
                    'Changes in Employe Expense Assignment(C&R).
                    'rdDueDate.Enabled = True
                    'rdNewEmployee.Enabled = True
                    dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
                    dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date
                    dtpStopDate.MinDate = FinancialYear._Object._Database_Start_Date
                    dtpStopDate.MaxDate = FinancialYear._Object._Database_End_Date
                    rdDueDate.Checked = False
                    rdNewEmployee.Checked = False
                    rdNewEmployee.Visible = False
                    rdDueDate.Visible = False
                    LblDueDate.Visible = False
                    dtpDueDate.Visible = False
                    'Shani(13-Aug-2015) -- End


                    'SHANI (06 JUN 2015) -- End
                Case Else
                    'pnlData1.Enabled = False
                    Control_Enable_Disble(False)
                    dtpStartDate.Enabled = False
                    dtpStopDate.Enabled = False

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckBox's Event "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If mdtEmployee.Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " LinkButton Events "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            txtSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " NumericUpdown Event "

    Private Sub nudFromYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudFromYear.ValueChanged, nudFromMonth.Validated, nudFromMonth.ValueChanged, nudFromMonth.Validated
        Try
            mdtDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudFromYear.Value) * -1).AddMonths(CInt(nudFromMonth.Value) * -1)
            nudToYear.Minimum = nudFromYear.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudFromYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudToYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudToYear.ValueChanged, nudToYear.Validated, nudToMonth.ValueChanged, nudToMonth.Validated
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudToYear.Value) * -1).AddMonths(CInt(nudToMonth.Value) * -1)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudToYear_ValueChanged", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " RadioButton Event "

    Private Sub rdDueDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDueDate.CheckedChanged
        Try
            If rdDueDate.Checked Then
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                rdNewEmployee.Checked = False
                dtpDueDate.Enabled = True
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                SetDataSource()
                'Pinkal (26-Feb-2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdDueDate_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub rdNewEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdNewEmployee.CheckedChanged
        Try
            If rdNewEmployee.Checked Then
                dtpDueDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                rdDueDate.Checked = False
                dtpDueDate.Enabled = False
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                SetDataSource()
                'Pinkal (26-Feb-2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdNewEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radAppointedDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAppointedDate.CheckedChanged, radExpYear.CheckedChanged
        Try
            Select Case CType(sender, RadioButton).Name.ToUpper
                Case radAppointedDate.Name.ToUpper
                    If radAppointedDate.Checked = True Then
                        pnlAppDate.Enabled = True : pnlYear.Enabled = False
                    End If
                Case radExpYear.Name.ToUpper
                    If radExpYear.Checked = True Then
                        pnlAppDate.Enabled = False : pnlYear.Enabled = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAppointedDate_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoAction.CheckedChanged
        Try
            If chkNoAction.Checked Then
                txtCfAmount.Enabled = False
                txtCfAmount.Decimal = 0
            Else
                txtCfAmount.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNoAction_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then
                'Pinkal (22-Oct-2018) -- Start
                'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
                'dvEmployee.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%'"

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dvEmployee.RowFilter = "[" & Language.getMessage("clsEmployee_Master", 42, "Code") & "] like '%" & txtSearch.Text.Trim & "%'  OR [" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "] like '%" & txtSearch.Text.Trim & "%'"
                dvEmployee.RowFilter = "[" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "] like '%" & txtSearch.Text.Trim & "%'"
                'Pinkal (26-Feb-2019) -- End

                'Pinkal (22-Oct-2018) -- End
                dgEmployee.Refresh()
            End If



            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If dgEmployee.Rows.Count > 0 Then
            '    chkSelectAll.Enabled = True
            'Else
            '    chkSelectAll.Enabled = False
            '    chkSelectAll.Checked = False
            'End If
            dgEmployee_CellContentClick(dgEmployee, New DataGridViewCellEventArgs(objSelect.Index, 0))
            SetDataSource()
            'Pinkal (26-Feb-2019) -- End


          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If e.ColumnIndex = objSelect.Index Then
                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtEmployee.AcceptChanges()
                End If
                Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischecked = true")
                If drRow.Length <= 0 Then
                    chkSelectAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgEmployee.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgEmployee.Rows.Count Then
                    chkSelectAll.CheckState = CheckState.Checked
                End If
            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpense_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Dim objExpMaster As clsExpense_Master
        Try
            objExpMaster = New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            If CInt(cboExpType.SelectedValue) = enExpenseType.EXP_LEAVE Then
                txtAccrualAmount.Enabled = objExpMaster._Isaccrue
                txtCfAmount.Enabled = objExpMaster._Isaccrue
                chkNoAction.Enabled = objExpMaster._Isaccrue
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'nudOccurance.Enabled = objExpMaster._Isaccrue
                'Pinkal (26-Dec-2018) -- End

                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment

            ElseIf CInt(cboExpType.SelectedValue) = enExpenseType.EXP_TRAINING Then
                txtAccrualAmount.Enabled = True
                txtCfAmount.Enabled = True
                chkNoAction.Enabled = True
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'nudOccurance.Enabled = objExpMaster._Isaccrue
                'Pinkal (26-Dec-2018) -- End

            ElseIf CInt(cboExpType.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS OrElse CInt(cboExpType.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                dtpStartDate.Enabled = True : dtpStopDate.Enabled = True
                'If objExpMaster._Uomunkid <= 0 Then
                If objExpMaster._Isaccrue = False Then
                    txtAccrualAmount.Enabled = False
                    txtAccrualAmount.Decimal = 0
                    txtCfAmount.Enabled = False
                    chkNoAction.Enabled = False
                Else
                    txtAccrualAmount.Enabled = True
                    txtAccrualAmount.Decimal = 0
                    txtCfAmount.Enabled = True
                    chkNoAction.Enabled = True
                End If
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'nudOccurance.Enabled = objExpMaster._Isaccrue
                'Pinkal (26-Dec-2018) -- End
            Else
                txtAccrualAmount.Enabled = False
                txtCfAmount.Enabled = False
                chkNoAction.Enabled = False
                'Pinkal (26-Dec-2018) -- Start
                'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
                'nudOccurance.Enabled = objExpMaster._Isaccrue
                'Pinkal (26-Dec-2018) -- End
            End If

            If objExpMaster._Isaccrue = True AndAlso objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                mblnIsAccrueAmountASUOM = True
            Else
                mblnIsAccrueAmountASUOM = False
            End If
            mblnIsAccrue = objExpMaster._Isaccrue


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpense_SelectedIndexChanged", mstrModuleName)
        Finally
            objExpMaster = Nothing
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLeaveInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
            Me.LblToYear.Text = Language._Object.getCaption(Me.LblToYear.Name, Me.LblToYear.Text)
            Me.LblToMonth.Text = Language._Object.getCaption(Me.LblToMonth.Name, Me.LblToMonth.Text)
            Me.LblMonth.Text = Language._Object.getCaption(Me.LblMonth.Name, Me.LblMonth.Text)
            Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.Name, Me.radExpYear.Text)
            Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.Name, Me.radAppointedDate.Text)
            Me.eline1.Text = Language._Object.getCaption(Me.eline1.Name, Me.eline1.Text)
            Me.lblExpenseType.Text = Language._Object.getCaption(Me.lblExpenseType.Name, Me.lblExpenseType.Text)
            Me.rdDueDate.Text = Language._Object.getCaption(Me.rdDueDate.Name, Me.rdDueDate.Text)
            Me.LblDueDate.Text = Language._Object.getCaption(Me.LblDueDate.Name, Me.LblDueDate.Text)
            Me.rdNewEmployee.Text = Language._Object.getCaption(Me.rdNewEmployee.Name, Me.rdNewEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.LblValue.Text = Language._Object.getCaption(Me.LblValue.Name, Me.LblValue.Text)
            Me.LblFrom.Text = Language._Object.getCaption(Me.LblFrom.Name, Me.LblFrom.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.gbLeaveInfo.Text = Language._Object.getCaption(Me.gbLeaveInfo.Name, Me.gbLeaveInfo.Text)
            Me.chkNoAction.Text = Language._Object.getCaption(Me.chkNoAction.Name, Me.chkNoAction.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.Name, Me.lblExpense.Text)
            Me.LblCFAmount.Text = Language._Object.getCaption(Me.LblCFAmount.Name, Me.LblCFAmount.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.lblAccrualDays.Text = Language._Object.getCaption(Me.lblAccrualDays.Name, Me.lblAccrualDays.Text)
            Me.lnkProcess.Text = Language._Object.getCaption(Me.lnkProcess.Name, Me.lnkProcess.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.lbltimes.Text = Language._Object.getCaption(Me.lbltimes.Name, Me.lbltimes.Text)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhAppointdate.HeaderText = Language._Object.getCaption(Me.dgColhAppointdate.Name, Me.dgColhAppointdate.HeaderText)
			Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Expense Type is mandatory information. Please select Expense Type to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Expense is mandatory information. Please select Expense to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, To date cannot be less than From date.")
            Language.setMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition.")
            Language.setMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition.")
            Language.setMessage(mstrModuleName, 6, "There is no employee to Assign Expense.")
            Language.setMessage(mstrModuleName, 7, "Employee is compulsory information.Please check atleast one employee.")
            Language.setMessage(mstrModuleName, 8, "Accrue value cannot be blank. Accrue value is required information.")
            Language.setMessage(mstrModuleName, 9, "You have set CF amount or No action is checked. This will be applicable to all selected employee(s).")
            Language.setMessage(mstrModuleName, 10, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 11, "Processed :")
            Language.setMessage(mstrModuleName, 12, "Employee Expenses successfully assigned.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Some of the checked employees are already assigned the selected expense." & vbCrLf & _
                                                                     "Due to this some employees will be discarded from the expense assignment process.")
			Language.setMessage(mstrModuleName, 42, "Code")
			Language.setMessage("clsEmployee_Master", 46, "Employee Name")
			Language.setMessage("clsEmployee_Master", 42, "Code")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class