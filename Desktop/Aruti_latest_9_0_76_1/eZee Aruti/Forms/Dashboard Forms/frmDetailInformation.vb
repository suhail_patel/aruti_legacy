﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Drawing.Printing

#End Region

Public Class frmDetailInformation

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmDetailInformation"
    Private dsDataSet As New DataSet
    Private mintViewDetailIdx As Integer = -1
    Private mblnCancel As Boolean = True
    Private mstrChartTitle As String = ""
    Private mintDetailValue As Integer = -1
    Private objDash As clsDashboard_Class
    Private mStrSeriesTag As String = String.Empty

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intViewIdx As Integer, ByVal dsData As DataSet, ByVal strTitle As String, ByVal intDetalVaue As Integer, Optional ByVal SeriesTag As String = "") As Boolean
        Try
            mintViewDetailIdx = intViewIdx
            dsDataSet = dsData
            mstrChartTitle = strTitle
            mintDetailValue = intDetalVaue
            mStrSeriesTag = SeriesTag
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_Chart()
        Try
            Select Case mintViewDetailIdx
                Case clsDashboard_Class.enViewDetails.SALARY_DETAIL
                    Call Fill_Salary_Detail()
                Case clsDashboard_Class.enViewDetails.LEAVE_DETAIL
                    Call Fill_Leave_Details()
                Case clsDashboard_Class.enViewDetails.TRAINING_COST
                    Call Fill_Training_Cost()
                Case clsDashboard_Class.enViewDetails.STAFF_DETAILS
                    Call Fill_Staff_Detail()
            End Select

            For Each dp As DataPoint In chDetail.Series(0).Points
                If dp.YValues(0) = 0 Then
                    dp.Label = " "
                    dp.IsVisibleInLegend = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Chart", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Salary_Detail()
        Try
            If dsDataSet.Tables(0).Rows.Count <= 0 Then
                chDetail.ChartAreas(0).Visible = False
                chDetail.Legends(0).Enabled = False
                Exit Sub
            End If

            If chDetail.ChartAreas(0).Visible = False Then chDetail.ChartAreas(0).Visible = True
            If chDetail.Legends(0).Enabled = False Then chDetail.Legends(0).Enabled = True

            chDetail.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "Name", dsDataSet.Tables(0).DefaultView, "SalPer")
            chDetail.Series(0).ChartType = SeriesChartType.Pie
            chDetail.Series(0)("LabelStyle") = "Inside"
            chDetail.Series(0).IsValueShownAsLabel = True
            chDetail.ChartAreas(0).AxisX.MinorGrid.Enabled = False
            chDetail.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chDetail.Series(0).LabelFormat = "{0.##}%"
            chDetail.Legends(0).Docking = Docking.Bottom
            Select Case mStrSeriesTag.ToUpper
                Case "TBS"
                    chDetail.Titles(0).Text = Language.getMessage(mstrModuleName, 1, "Bank Salary As On Period :") & " " & mstrChartTitle
                Case "TCS"
                    chDetail.Titles(0).Text = Language.getMessage(mstrModuleName, 2, "Cash Salary As On Period :") & " " & mstrChartTitle
                Case "THS"
                    chDetail.Titles(0).Text = Language.getMessage(mstrModuleName, 3, "Hold Salary As On Period :") & " " & mstrChartTitle
            End Select
            chDetail.Titles(0).TextStyle = TextStyle.Emboss
            chDetail.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Salary_Detail", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Leave_Details()
        Try
            If dsDataSet.Tables(0).Rows.Count <= 0 Then
                chDetail.ChartAreas(0).Visible = False
                chDetail.Legends(0).Enabled = False
                Exit Sub
            End If

            If chDetail.ChartAreas(0).Visible = False Then chDetail.ChartAreas(0).Visible = True
            If chDetail.Legends(0).Enabled = False Then chDetail.Legends(0).Enabled = True

            chDetail.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "Name", dsDataSet.Tables(0).DefaultView, "LvPer")
            chDetail.Series(0).ChartType = SeriesChartType.Pie
            chDetail.Series(0)("LabelStyle") = "Top"
            chDetail.Series(0).IsValueShownAsLabel = True
            chDetail.ChartAreas(0).AxisX.MinorGrid.Enabled = False
            chDetail.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chDetail.Series(0).LabelFormat = "{0.##}%"
            chDetail.Legends(0).Docking = Docking.Bottom
            Dim objLType As New clsleavetype_master
            objLType._Leavetypeunkid = CInt(mStrSeriesTag)
            chDetail.Titles(0).Text = objLType._Leavename
            chDetail.Titles(0).Text &= Language.getMessage(mstrModuleName, 4, " Detail As On Month :") & " " & mstrChartTitle
            chDetail.Titles(0).TextStyle = TextStyle.Emboss
            chDetail.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
            objLType = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Leave_Details", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Training_Cost()
        Try
            If dsDataSet.Tables(0).Rows.Count <= 0 Then
                chDetail.ChartAreas(0).Visible = False
                chDetail.Legends(0).Enabled = False
                Exit Sub
            End If

            If chDetail.ChartAreas(0).Visible = False Then chDetail.ChartAreas(0).Visible = True
            If chDetail.Legends(0).Enabled = False Then chDetail.Legends(0).Enabled = True

            chDetail.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "TName", dsDataSet.Tables(0).DefaultView, "TPercentage")
            chDetail.Series(0).ChartType = SeriesChartType.Pie
            chDetail.Series(0)("LabelStyle") = "InSide"
            chDetail.Series(0).IsValueShownAsLabel = True
            chDetail.ChartAreas(0).AxisX.MinorGrid.Enabled = False
            chDetail.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chDetail.Series(0).LabelFormat = "{0.##}%"
            chDetail.Legends(0).Docking = Docking.Bottom
            chDetail.Titles(0).Text = Language.getMessage(mstrModuleName, 5, "Trainig Cost :") & " " & mstrChartTitle
            chDetail.Titles(0).TextStyle = TextStyle.Emboss
            chDetail.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Training_Cost", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Staff_Detail()
        Try
            If dsDataSet.Tables(0).Rows.Count <= 0 Then
                chDetail.ChartAreas(0).Visible = False
                chDetail.Legends(0).Enabled = False
                Exit Sub
            End If

            If chDetail.ChartAreas(0).Visible = False Then chDetail.ChartAreas(0).Visible = True
            If chDetail.Legends(0).Enabled = False Then chDetail.Legends(0).Enabled = True

            chDetail.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "NAME", dsDataSet.Tables(0).DefaultView, "aCnt")
            chDetail.Series(0).ChartType = SeriesChartType.Column

            chDetail.Series(0).IsValueShownAsLabel = True
            chDetail.Series(0)("LabelStyle") = "Top"
            chDetail.Series(0).LabelForeColor = Color.White
            chDetail.Series(0).Font = New Font(Me.Font.Name, 8, FontStyle.Bold)
            chDetail.Series(0).SmartLabelStyle.Enabled = True

            chDetail.ChartAreas(0).AxisX.MajorGrid.Enabled = False
            chDetail.ChartAreas(0).AxisX.MinorGrid.Enabled = False

            chDetail.ChartAreas(0).AxisY.MinorGrid.Enabled = False
            chDetail.ChartAreas(0).AxisY.MajorGrid.Enabled = False

            chDetail.ChartAreas(0).AxisX.LabelAutoFitStyle = LabelAutoFitStyles.WordWrap

            chDetail.ChartAreas(0).BackColor = Color.DarkGray
            

            chDetail.ChartAreas(0).ShadowOffset = 2

            chDetail.ChartAreas(0).AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount

            chDetail.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            chDetail.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)


            'chDetail.ChartAreas(0).AxisY.Maximum = CDbl(dsDataSet.Tables(0).Compute("MAX(aCnt)", ""))

            chDetail.Legends(0).Docking = Docking.Top
            chDetail.Titles(0).Text = Language.getMessage(mstrModuleName, 6, "Employee Details As On Month :") & " " & mstrChartTitle
            chDetail.Titles(0).TextStyle = TextStyle.Emboss
            chDetail.Titles(0).Font = New Font(Me.Font.Name, 12, FontStyle.Bold)
            chDetail.ChartAreas(0).Area3DStyle.Enable3D = False
            chDetail.Palette = ChartColorPalette.BrightPastel
            chDetail.Series(0)("DrawingStyle") = "Cylinder"
            chDetail.Series(0)("PixelPointWidth") = "20"

            chDetail.ChartAreas(0).AxisX.ScrollBar.Enabled = True
            chDetail.ChartAreas(0).AxisX.ScrollBar.IsPositionedInside = False
            chDetail.ChartAreas(0).AxisX.ScrollBar.Size = 15
            chDetail.ChartAreas(0).AxisX.ScaleView.Zoom(0, 5)
            chDetail.ChartAreas(0).AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll
            chDetail.ChartAreas(0).AxisX.ScrollBar.BackColor = Color.Gray
            chDetail.ChartAreas(0).AxisX.ScrollBar.ButtonColor = Color.LightGray

            Select Case CInt(cboView.SelectedValue)
                Case 1  'DEPARTMENT
                    chDetail.Series(0).LegendText = Language.getMessage(mstrModuleName, 7, "Department")
                    chDetail.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 7, "Department")
                Case 2  'BRANCH
                    chDetail.Series(0).LegendText = Language.getMessage(mstrModuleName, 8, "Branch")
                    chDetail.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 8, "Branch")
                Case 3  'COST CENTER
                    chDetail.Series(0).LegendText = Language.getMessage(mstrModuleName, 9, "Cost Center")
                    chDetail.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 9, "Cost Center")
                Case 4  'SECTION
                    chDetail.Series(0).LegendText = Language.getMessage(mstrModuleName, 10, "Section")
                    chDetail.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 10, "Section")
                Case 5  'UNIT
                    chDetail.Series(0).LegendText = Language.getMessage(mstrModuleName, 11, "Unit")
                    chDetail.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 11, "Unit")
            End Select

            chDetail.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 12, "Employee")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Staff_Detail", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Try
            dsCombo = objDash.ViewDetailsByIdx("List")
            With cboView
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillCombo ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDetailInformation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'objDash = New clsDashboard_Class(False)
        objDash = New clsDashboard_Class(FinancialYear._Object._DatabaseName, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         FinancialYear._Object._Database_Start_Date, _
                                         FinancialYear._Object._Database_End_Date, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._CurrentDateAndTime, True, True, _
                                         User._Object.Privilege._Show_Probation_Dates, _
                                         User._Object.Privilege._Show_Suspension_Dates, _
                                         User._Object.Privilege._Show_Appointment_Dates, _
                                         User._Object.Privilege._Show_Confirmation_Dates, _
                                         User._Object.Privilege._Show_BirthDates, _
                                         User._Object.Privilege._Show_Anniversary_Dates, _
                                         User._Object.Privilege._Show_Contract_Ending_Dates, _
                                         User._Object.Privilege._Show_TodayRetirement_Dates, _
                                         User._Object.Privilege._Show_ForcastedRetirement_Dates, _
                                         User._Object.Privilege._Show_ForcastedEOC_Dates, _
                                         User._Object.Privilege._Show_ForcastedELC_Dates, _
                                         ConfigParameter._Object._Forcasted_ViewSetting, _
                                         ConfigParameter._Object._ForcastedValue, _
                                         ConfigParameter._Object._ForcastedEOC_ViewSetting, _
                                         ConfigParameter._Object._ForcastedEOCValue, _
                                         ConfigParameter._Object._ForcastedELCViewSetting, _
                                         ConfigParameter._Object._ForcastedELCValue, _
                                         ConfigParameter._Object._LeaveBalanceSetting, False, _
                                         User._Object.Privilege._AllowtoViewPendingAccrueData)
        'S.SANDEEP [12-Apr-2018] -- START {Ref#218|#ARUTI-106} [User._Object.Privilege._AllowtoViewPendingAccrueData] -- END
        'Shani(24-Aug-2015) -- End

        Try
            Call Set_Logo(Me, enArutiApplicatinType.Aruti_Payroll)
            Call FillCombo()
            Call Fill_Chart()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDetailInformation_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

#End Region

#Region " Controls "

    Private Sub chDetail_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chDetail.MouseDown
        Try

            If mintViewDetailIdx = clsDashboard_Class.enViewDetails.STAFF_DETAILS Then Exit Sub

            Dim result As HitTestResult = chDetail.HitTest(e.X, e.Y)
            Dim exploded As Boolean

            If result.PointIndex = -1 Then Exit Sub

            If chDetail.Series(0).Points(result.PointIndex).CustomProperties = "Exploded=true" Then
                exploded = True
            Else
                exploded = False
            End If

            Dim point As DataPoint
            For Each point In chDetail.Series(0).Points
                point.CustomProperties = ""
            Next point

            If exploded Then
                Return
            End If

            If result.ChartElementType = ChartElementType.DataPoint Then
                point = chDetail.Series(0).Points(result.PointIndex)
                point.CustomProperties = "Exploded = true"
            End If

            If result.ChartElementType = ChartElementType.LegendItem Then
                point = chDetail.Series(0).Points(result.PointIndex)
                point.CustomProperties = "Exploded = true"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chDetail_MouseDown", mstrModuleName)
        End Try
    End Sub

    Private Sub cboView_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboView.SelectionChangeCommitted
        Try
            Select Case mintViewDetailIdx
                Case clsDashboard_Class.enViewDetails.SALARY_DETAIL
                    Select Case mStrSeriesTag.ToUpper
                        Case "TBS"
                            dsDataSet = objDash.Detail_Information(mintViewDetailIdx, mintDetailValue, CInt(cboView.SelectedValue), "List", 1)
                        Case "TCS"
                            dsDataSet = objDash.Detail_Information(mintViewDetailIdx, mintDetailValue, CInt(cboView.SelectedValue), "List", 2)
                        Case "THS"
                            dsDataSet = objDash.Detail_Information(mintViewDetailIdx, mintDetailValue, CInt(cboView.SelectedValue), "List", 3)
                    End Select
                    Call Fill_Salary_Detail()
                Case clsDashboard_Class.enViewDetails.LEAVE_DETAIL
                    dsDataSet = objDash.Detail_Information(mintViewDetailIdx, mintDetailValue, CInt(cboView.SelectedValue), "List", CInt(mStrSeriesTag))
                    Call Fill_Leave_Details()
                Case clsDashboard_Class.enViewDetails.STAFF_DETAILS
                    dsDataSet = objDash.Staff_Detail_Informatiom(mintViewDetailIdx, mintDetailValue, CInt(cboView.SelectedValue), mStrSeriesTag, "List")
                    Call Fill_Staff_Detail()
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : cboView_SelectionChangeCommitted ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chDetail_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles chDetail.MouseMove
        Try
            If mintViewDetailIdx = clsDashboard_Class.enViewDetails.STAFF_DETAILS Then Exit Sub

            Dim result As HitTestResult = chDetail.HitTest(e.X, e.Y)

            For Each points In chDetail.Series(0).Points
                points.BackSecondaryColor = Color.Black
                points.BackHatchStyle = ChartHatchStyle.None
                points.BorderWidth = 1
            Next points

            If result.ChartElementType = ChartElementType.DataPoint Or result.ChartElementType = ChartElementType.LegendItem Then
                Me.Cursor = Cursors.Hand
                Dim point As DataPoint = chDetail.Series(0).Points(result.PointIndex)
                point.BackSecondaryColor = Color.White
                point.BackHatchStyle = ChartHatchStyle.Cross
                point.BorderWidth = 1
            Else
                Me.Cursor = Cursors.Default
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : chDetail_MouseMove ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSave.Click
        Try
            Dim saveFileDialog1 As New SaveFileDialog()
            saveFileDialog1.Filter = "Bitmap (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|EMF (*.emf)|*.emf|PNG (*.png)|*.png|GIF (*.gif)|*.gif|TIFF (*.tif)|*.tif"
            saveFileDialog1.FilterIndex = 2
            saveFileDialog1.RestoreDirectory = True

            If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                Dim format As ChartImageFormat = ChartImageFormat.Bmp
                If saveFileDialog1.FileName.EndsWith("bmp") Then
                    format = ChartImageFormat.Bmp
                ElseIf saveFileDialog1.FileName.EndsWith("jpg") Then
                    format = ChartImageFormat.Jpeg
                ElseIf saveFileDialog1.FileName.EndsWith("emf") Then
                    format = ChartImageFormat.Emf
                ElseIf saveFileDialog1.FileName.EndsWith("gif") Then
                    format = ChartImageFormat.Gif
                ElseIf saveFileDialog1.FileName.EndsWith("png") Then
                    format = ChartImageFormat.Png
                ElseIf saveFileDialog1.FileName.EndsWith("tif") Then
                    format = ChartImageFormat.Tiff
                End If
                chDetail.SaveImage(saveFileDialog1.FileName, format)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintPreview.Click
        Try
            chDetail.Printing.PrintDocument.DefaultPageSettings.Landscape = False

            chDetail.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)

            Dim pr As PrinterResolution
            For Each pr In chDetail.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                If pr.Kind.ToString() = "Low" Then
                    chDetail.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                End If
            Next
            chDetail.Printing.PrintPreview()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintPreview_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            chDetail.Printing.PrintDocument.DefaultPageSettings.Landscape = True
            chDetail.Printing.PrintDocument.DefaultPageSettings.Margins = New System.Drawing.Printing.Margins(100, 100, 100, 100)
            Dim pr As PrinterResolution
            For Each pr In chDetail.Printing.PrintDocument.PrinterSettings.PrinterResolutions
                If pr.Kind.ToString() = "Low" Then
                    chDetail.Printing.PrintDocument.DefaultPageSettings.PrinterResolution = pr
                End If
            Next
            chDetail.Printing.Print(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeading.Text = Language._Object.getCaption(Me.eZeeHeading.Name, Me.eZeeHeading.Text)
			Me.chDetail.Text = Language._Object.getCaption(Me.chDetail.Name, Me.chDetail.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuSave.Text = Language._Object.getCaption(Me.mnuSave.Name, Me.mnuSave.Text)
			Me.mnuPrintPreview.Text = Language._Object.getCaption(Me.mnuPrintPreview.Name, Me.mnuPrintPreview.Text)
			Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Bank Salary As On Period :")
			Language.setMessage(mstrModuleName, 2, "Cash Salary As On Period :")
			Language.setMessage(mstrModuleName, 3, "Hold Salary As On Period :")
			Language.setMessage(mstrModuleName, 4, " Detail As On Month :")
			Language.setMessage(mstrModuleName, 5, "Trainig Cost :")
			Language.setMessage(mstrModuleName, 6, "Employee Details As On Month :")
			Language.setMessage(mstrModuleName, 7, "Department")
			Language.setMessage(mstrModuleName, 8, "Branch")
			Language.setMessage(mstrModuleName, 9, "Cost Center")
			Language.setMessage(mstrModuleName, 10, "Section")
			Language.setMessage(mstrModuleName, 11, "Unit")
			Language.setMessage(mstrModuleName, 12, "Employee")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class