﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 21

Public Class frmImportdata

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImportdata"
    Private objExport As clsExportData
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim mdtOther As DataTable = Nothing
    Dim mdsPayroll As DataSet = Nothing
    Dim mdtGridData As DataTable = Nothing
    Dim dvGriddata As DataView = Nothing
    Dim mintFilter As Integer = 0
    Dim m_Strarray(1, 1) As String
    Private m_timesheetValue As New Dictionary(Of String, String)

    Dim imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Dim imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            ' mintDemoUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            ' intUnkId = mintDemoUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Sohail (04 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet

        Try
            dsCombo = objMaster.GetCommunictionDevice()
            With cboCommunicationDevice
                .ValueMember = "Id"
                .DisplayMember = "Device"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = enFingerPrintDevice.ZKSoftware
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Oct 2013) -- End

    Private Sub setStatus(ByVal dtPRow As DataRow, ByVal blnResult As Boolean, ByVal isReplace As Boolean)
        Try
            If blnResult Then
                If isReplace Then
                    dtPRow.Item("Message") = Language.getMessage(mstrModuleName, 2, "Replaced")
                    dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Success")
                Else
                    dtPRow.Item("Message") = ""
                    dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Success")
                End If
                dtPRow.Item("image") = imgAccept
                dtPRow.Item("objStatus") = 1
                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
            Else
                'dtPRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                dtPRow.Item("image") = imgError
                dtPRow.Item("objStatus") = 2
                'objError.Text = CStr(Val(objError.Text) + 1)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setStatus", mstrModuleName)
        End Try
    End Sub

    Private Sub ImportData(ByVal isReplace As Boolean)
        objWarning.Text = "0"
        objError.Text = "0"
        objSuccess.Text = "0"

        wzImportData.CancelEnabled = False
        btnFilter.Enabled = False
        btnReplace.Enabled = False

        ezWait.Active = True

        Try

            Dim drPRow As DataRow()

            If mintFilter = 0 Or mintFilter = 1 Then   'FOR TIMESHEET
                drPRow = mdtGridData.Select("objImportedFor = 1")
                If drPRow.Length > 0 Then

                    'Pinkal (09-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    'ImportTimesheetData(drPRow, isReplace)
                    ImportTimesheetData(mdtGridData, isReplace)
                    'Pinkal (09-Jan-2014) -- End


                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportData", mstrModuleName)
        Finally
            ezWait.Active = False
            wzImportData.CancelEnabled = True
            btnFilter.Enabled = True
            btnReplace.Enabled = True
        End Try
    End Sub

    Private Sub FillGridData()
        Try

            Dim strName As String = ""
            Dim strDate As String = ""

            mdtGridData = New DataTable("GridData")

            mdtGridData.Columns.Add("image", System.Type.GetType("System.Object"))
            mdtGridData.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("employeename", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("date", System.Type.GetType("System.DateTime"))
            mdtGridData.Columns.Add("objdate", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("Message", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("Status", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("objStatus", System.Type.GetType("System.Int32"))
            mdtGridData.Columns.Add("objImportedFor", System.Type.GetType("System.Int32"))


            If rdPayrollexportFile.Checked Then
                mdsPayroll.Tables(0).Columns("Logindate").ColumnName = "date"
            End If

            For tb As Integer = 0 To mdsPayroll.Tables.Count - 1

                For i As Integer = 0 To mdsPayroll.Tables(tb).Rows.Count - 1

                    Dim drRow As DataRow
                    drRow = mdtGridData.NewRow()

                    Dim mdtdate As String = ""


                    'Pinkal (06-Dec-2013) -- Start
                    'Enhancement : Oman Changes

                    Select Case CInt(cboCommunicationDevice.SelectedValue)
                        Case enFingerPrintDevice.ZKSoftware
                            If IsDate(mdsPayroll.Tables(tb).Rows(i)("date")) Then
                                mdtdate = CStr(eZeeDate.convertDate(CDate(mdsPayroll.Tables(tb).Rows(i)("date"))))
                            Else
                                drRow.Item("date") = eZeeDate.convertDate(mdsPayroll.Tables(tb).Rows(i)("date").ToString).ToShortDateString
                                mdtdate = CStr(mdsPayroll.Tables(tb).Rows(i)("date")).ToString
                            End If

                        Case CInt(enFingerPrintDevice.BioStar)
                            mdtdate = mdsPayroll.Tables(tb).Rows(i)("date").ToString()
                    End Select

                    'Pinkal (06-Dec-2013) -- End



                    'Pinkal (29-Aug-2012) -- Start
                    'Enhancement : TRA Changes

                    Dim drExist As DataRow() = mdtGridData.Select("employeecode = '" & mdsPayroll.Tables(tb).Rows(i)("employeecode").ToString & "' AND objdate  = '" & mdtdate & "'")

                    If drExist.Length > 0 Then
                        Continue For
                    End If

                    'Pinkal (29-Aug-2012) -- End


                    drRow("image") = New Drawing.Bitmap(1, 1)

                    'Pinkal (25-Oct-2012) -- Start
                    'Enhancement : TRA Changes
                    drRow("employeecode") = CStr(mdsPayroll.Tables(tb).Rows(i)("employeecode")).ToString
                    drRow("employeename") = CStr(mdsPayroll.Tables(tb).Rows(i)("employeename")).ToString
                    'Pinkal (25-Oct-2012) -- End


                    drRow("Message") = ""
                    drRow("Status") = ""
                    drRow("objStatus") = 0

                    If mdsPayroll.Tables(tb).TableName = "Timesheet" Then
                        drRow.Item("objImportedFor") = 1
                        Dim ImpFile As New IO.FileInfo(txtFilePath.Text)

                        If IsDate(mdsPayroll.Tables(tb).Rows(i)("date")) Then
                            drRow.Item("date") = CDate(mdsPayroll.Tables(tb).Rows(i)("date"))

                            'Pinkal (29-Aug-2012) -- Start
                            'Enhancement : TRA Changes
                            'drRow.Item("objdate") = eZeeDate.convertDate(mdsPayroll.Tables(tb).Rows(i)("date").ToString)
                            'Sohail (04 Oct 2013) -- Start
                            'TRA - ENHANCEMENT
                            'drRow.Item("objdate") = eZeeDate.convertDate(CDate(mdsPayroll.Tables(tb).Rows(i)("date")))
                            Select Case CInt(cboCommunicationDevice.SelectedValue)
                                Case enFingerPrintDevice.ZKSoftware
                                    drRow.Item("objdate") = eZeeDate.convertDate(CDate(mdsPayroll.Tables(tb).Rows(i)("date")))
                                Case enFingerPrintDevice.BioStar
                                    drRow.Item("objdate") = CDate(mdsPayroll.Tables(tb).Rows(i)("date")).ToString
                            End Select
                            'Sohail (04 Oct 2013) -- End
                            'Pinkal (29-Aug-2012) -- End


                        Else
                            drRow.Item("date") = eZeeDate.convertDate(mdsPayroll.Tables(tb).Rows(i)("date").ToString).ToShortDateString
                            drRow.Item("objdate") = mdsPayroll.Tables(tb).Rows(i)("date")
                        End If

                    End If

                    mdtGridData.Rows.Add(drRow)
                    objTotal.Text = CStr(Val(objTotal.Text) + 1)
                Next

            Next

            dgEmployeedata.AutoGenerateColumns = False

            'VISIBLE COLUMN
            colhImage.DataPropertyName = "image"
            colhEmployeeCode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employeename"
            colhlogindate.DataPropertyName = "date"
            colhMessage.DataPropertyName = "Message"
            colhStatus.DataPropertyName = "Status"


            'INVISIBLE COLUMN
            objcolhImortedFor.DataPropertyName = "objImportedFor"
            objcolhDate.DataPropertyName = "objdate"
            objcolhstatus.DataPropertyName = "objStatus"


            dvGriddata = New DataView(mdtGridData)


            'Pinkal (06-Dec-2013) -- Start
            'Enhancement : Oman Changes
            dvGriddata.Sort = "employeecode asc,date asc"
            'Pinkal (06-Dec-2013) -- End


            dgEmployeedata.DataSource = dvGriddata

            ImportData(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridData", mstrModuleName)
        End Try
    End Sub

    'Private Sub ImportTimesheetData(ByVal drRow As DataRow(), ByVal isReplace As Boolean)
    Private Sub ImportTimesheetData(ByVal mdtGridData As DataTable, ByVal isReplace As Boolean)
        Dim blnResult As Boolean = True
        Dim objlogin As New clslogin_Tran
        Dim dsList As DataSet = Nothing
        Try


            Dim intError As Integer = 0
            Dim i As Integer = -1
            Dim mstrPreviousEmpCode As String = ""


            For Each dRow As DataRow In mdtGridData.Rows

                If CBool(dRow("objImportedFor")) = False Then Continue For

                intError = 0
                i = i + 1
                If dRow.Item("objStatus").ToString <> "0" Then
                    Continue For
                End If
                blnResult = True


                Dim strdate As String = ""

                'S.SANDEEP |20-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
                Try
                    dgEmployeedata.FirstDisplayedScrollingRowIndex = mdtGridData.Rows.IndexOf(dRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                'S.SANDEEP |20-JUL-2019| -- END

                'Sohail (04 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                Dim objEmployee As New clsEmployee_Master
                Dim dtRows As DataRow()
                Select Case CInt(cboCommunicationDevice.SelectedValue)
                    Case enFingerPrintDevice.ZKSoftware

                        If IsDate(dRow("objdate")) Then
                            strdate = eZeeDate.convertDate(dRow("objdate").ToString()).Date.ToString
                        Else
                            strdate = dRow("objdate").ToString()
                        End If

                        If objEmployee.isExist(dRow("employeecode").ToString) = False Then
                            blnResult = False
                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 20, "Employee not found.")
                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                            dRow.Item("image") = imgError
                            dRow.Item("objStatus") = 2
                            If intError = 0 Then
                                objError.Text = CStr(Val(objError.Text) + 1)
                                intError = 1
                            End If
                            Continue For
                        Else
                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objEmployee._Employeeunkid = objExport.getEmployeeID(dRow.Item("employeecode").ToString)
                            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objExport.getEmployeeID(dRow.Item("employeecode").ToString)
                            'S.SANDEEP [04 JUN 2015] -- END
                            dRow("employeename") = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                        End If

                        If objlogin.IsEmployeeLoginExist(strdate, dRow("employeecode").ToString) Then

                            If isReplace Then

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'If Not objlogin.Delete(objExport.getEmployeeID(dRow.Item("employeecode").ToString), eZeeDate.convertDate(strdate).Date) Then
                                If Not objlogin.Delete(objExport.getEmployeeID(dRow.Item("employeecode").ToString), eZeeDate.convertDate(strdate).Date, ConfigParameter._Object._DonotAttendanceinSeconds) Then
                                    'S.SANDEEP [04 JUN 2015] -- END
                                    blnResult = False
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                    dRow.Item("image") = imgError
                                    dRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If
                            Else
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 7, "Already Exists")
                                dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                dRow.Item("image") = imgWarring
                                dRow.Item("objStatus") = 0
                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                Continue For
                            End If

                        End If

                        'Pinkal (29-Aug-2012) -- Start
                        'Enhancement : TRA Changes

                        dtRows = mdsPayroll.Tables("Timesheet").Select("employeecode = '" & dRow("employeecode").ToString() & "' AND date = '" & CDate(dRow("date")).ToString() & "'")

                        'Pinkal (29-Aug-2012) -- End

                        If dtRows.Length <= 0 Then
                            blnResult = False
                        Else
                            blnResult = True
                        End If

                        Try


                            'Pinkal (15-Oct-2013) -- Start
                            'Enhancement : TRA Changes
                            Dim objShifttran As New clsshift_tran
                            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                            'Pinkal (15-Oct-2013) -- End


                            For Each dtRow As DataRow In dtRows

                                'S.SANDEEP |20-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
                                Application.DoEvents()
                                'S.SANDEEP |20-JUL-2019| -- END

                                objlogin = New clslogin_Tran

                                If CInt(dtRow.Item("employeeunkid")) < 0 Then
                                    objlogin._Employeeunkid = objExport.getEmployeeID(dRow.Item("employeecode").ToString)
                                Else
                                    objlogin._Employeeunkid = CInt(dtRow.Item("employeeunkid"))
                                End If


                                'Pinkal (25-Oct-2012) -- Start
                                'Enhancement : TRA Changes
                                If objlogin._Employeeunkid <= 0 Then
                                    blnResult = False
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                    dRow.Item("image") = imgError
                                    dRow.Item("objStatus") = 2

                                    If intError = 0 Then
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        intError = 1
                                    End If
                                    Continue For
                                End If
                                'Pinkal (25-Oct-2012) -- End

                                'S.SANDEEP |20-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
                                If IsDBNull(dtRow.Item("checkintime")) Then
                                    blnResult = False
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
                                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                    dRow.Item("image") = imgError
                                    dRow.Item("objStatus") = 2

                                    If intError = 0 Then
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        intError = 1
                                    End If
                                    Continue For
                                End If
                                'S.SANDEEP |20-JUL-2019| -- END


                                'Pinkal (03-Nov-2012) -- Start
                                'Enhancement : TRA Changes
                                'Dim objEmployee As New clsEmployee_Master

                                If CInt(dtRow.Item("shiftunkid")) <= 0 Then

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'objEmployee._Employeeunkid = objlogin._Employeeunkid
                                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objlogin._Employeeunkid
                                    'S.SANDEEP [04 JUN 2015] -- END



                                    'Pinkal (15-Oct-2013) -- Start
                                    'Enhancement : TRA Changes
                                    'objShifttran.GetShiftTran(objEmployee._Shiftunkid)

                                    'S.SANDEEP [04 JUN 2015] -- START
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'Dim intShiftId As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(dtRow.Item("date")).Date, objEmployee._Employeeunkid)
                                    Dim intShiftId As Integer = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(dtRow.Item("date")).Date, objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)))
                                    'S.SANDEEP [04 JUN 2015] -- END
                                    objShifttran.GetShiftTran(intShiftId)
                                    'Pinkal (15-Oct-2013) -- End


                                    Dim drRowShift() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(CDate(dtRow.Item("date")).Date.DayOfWeek.ToString()))
                                    If drRowShift.Length > 0 Then

                                        If CDate(dtRow.Item("checkintime")) >= CDate(CDate(dtRow.Item("checkintime")).Date & " " & CDate(drRowShift(0)("starttime")).ToShortTimeString()) Then

                                            'S.SANDEEP |20-JUL-2019| -- START
                                            'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
                                            'If CDate(drRowShift(0)("endtime")).ToLongTimeString.Contains("AM") Then
                                            '    'Sohail (04 Oct 2013) -- Start
                                            '    'TRA - ENHANCEMENT - AddDays(1) removed. it was used for checkout time contains only time and no date in checkout time
                                            '    drRowShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date & " " & CDate(drRowShift(0)("endtime")).ToShortTimeString
                                            '    'Sohail (04 Oct 2013) -- End
                                            'Else
                                            '    drRowShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date & " " & CDate(drRowShift(0)("endtime")).ToShortTimeString
                                            'End If
                                            If IsDBNull(dtRow.Item("checkouttime")) = False Then
                                            If CDate(drRowShift(0)("endtime")).ToLongTimeString.Contains("AM") Then
                                                'Sohail (04 Oct 2013) -- Start
                                                'TRA - ENHANCEMENT - AddDays(1) removed. it was used for checkout time contains only time and no date in checkout time
                                                drRowShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date & " " & CDate(drRowShift(0)("endtime")).ToShortTimeString
                                                'Sohail (04 Oct 2013) -- End
                                            Else
                                                drRowShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date & " " & CDate(drRowShift(0)("endtime")).ToShortTimeString
                                            End If
                                            End If
                                            'S.SANDEEP |20-JUL-2019| -- END
                                            
                                            

                                            'Sohail (04 Oct 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'If CDate(dtRow.Item("checkintime")) <= CDate(drRowShift(0)("endtime")) Then 'If employee checkin time is in between shift start-end time
                                            '    dtRow.Item("shiftunkid") = objEmployee._Shiftunkid
                                            'Else 'If employee checkin time is after shift end time

                                            '    Dim objEmpShift As New clsEmployee_Shift_Tran
                                            '    objEmpShift._EmployeeUnkid = objlogin._Employeeunkid

                                            '    If objEmpShift._SDataTable.Rows.Count > 0 Then
                                            '        Dim drShift() As DataRow = objEmpShift._SDataTable.Select("isdefault = 0")
                                            '        If drShift.Length > 0 Then
                                            '            For i As Integer = 0 To drShift.Length - 1
                                            '                objShifttran.GetShiftTran(CInt(drShift(i)("shiftunkid")))
                                            '                Dim drcheckShift() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(CDate(dtRow.Item("date")).Date.DayOfWeek.ToString()))
                                            '                If drRow.Length > 0 Then

                                            '                    If CDate(dtRow.Item("checkintime")) >= CDate(CDate(dtRow.Item("checkintime")).Date & " " & CDate(drcheckShift(0)("starttime")).ToShortTimeString()) Then

                                            '                        If CDate(drcheckShift(0)("endtime")).ToShortTimeString.Contains("AM") Then
                                            '                            drcheckShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date.AddDays(1) & " " & CDate(drcheckShift(0)("endtime")).ToShortTimeString
                                            '                        Else
                                            '                            drcheckShift(0)("endtime") = CDate(dtRow.Item("checkouttime")).Date & " " & CDate(drcheckShift(0)("endtime")).ToShortTimeString
                                            '                        End If

                                            '                        If CDate(dtRow.Item("checkintime")) <= CDate(drcheckShift(0)("endtime")) Then
                                            '                            dtRow.Item("shiftunkid") = CInt(drShift(i)("shiftunkid"))
                                            '                        End If

                                            '                    End If

                                            '                End If

                                            '            Next

                                            '        End If

                                            '    End If

                                            'End If
                                            'dtRow.Item("shiftunkid") = objEmployee._Shiftunkid
                                            'Sohail (04 Oct 2013) -- End
                                        End If

                                    End If

                                End If

                                'If CInt(dtRow.Item("shiftunkid")) <= 0 Then dtRow.Item("shiftunkid") = objEmployee._Shiftunkid

                                'Pinkal (03-Nov-2012) -- End

                                If IsDate(dtRow.Item("date")) Then
                                    objlogin._Logindate = CDate(dtRow.Item("date").ToString)
                                Else
                                    objlogin._Logindate = eZeeDate.convertDate(dtRow.Item("date").ToString).Date
                                End If

                                'Sohail (04 Oct 2013) -- Start
                                'TRA - ENHANCEMENT
                                'objlogin._Shiftunkid = CInt(dtRow.Item("shiftunkid"))
                                Dim objEmpShift As New clsEmployee_Shift_Tran
                                Dim intCurrentShiftID As Integer = 0

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(objlogin._Logindate, objEmployee._Employeeunkid)
                                intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(objlogin._Logindate, objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)))
                                'S.SANDEEP [04 JUN 2015] -- END


                                'If intCurrentShiftID <= 0 Then
                                '    objlogin._Shiftunkid = objEmployee._Shiftunkid
                                'Else
                                objlogin._Shiftunkid = intCurrentShiftID
                                'End If
                                'Sohail (04 Oct 2013) -- End




                                objlogin._InOutType = CInt(dtRow.Item("inouttype"))


                                'Pinkal (03-Nov-2012) -- Start
                                'Enhancement : TRA Changes


                                'S.SANDEEP |20-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
                                'If CDate(dtRow.Item("checkintime")) > CDate(dtRow.Item("checkouttime")) Then
                                '    blnResult = False
                                '    dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
                                '    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                '    dRow.Item("image") = imgError
                                '    dRow.Item("objStatus") = 2
                                '    If intError = 0 Then
                                '        objError.Text = CStr(Val(objError.Text) + 1)
                                '        intError = 1
                                '    End If
                                '    Continue For
                                'End If
                                'Pinkal (03-Nov-2012) -- End

                                'objlogin._checkintime = CDate(dtRow.Item("checkintime"))
                                'objlogin._Checkouttime = CDate(dtRow.Item("checkouttime"))
                                If IsDBNull(dtRow.Item("checkouttime")) = False Then
                                If CDate(dtRow.Item("checkintime")) > CDate(dtRow.Item("checkouttime")) Then
                                    blnResult = False
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
                                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                    dRow.Item("image") = imgError
                                    dRow.Item("objStatus") = 2
                                    If intError = 0 Then
                                        objError.Text = CStr(Val(objError.Text) + 1)
                                        intError = 1
                                    End If
                                    Continue For
                                End If

                                objlogin._checkintime = CDate(dtRow.Item("checkintime"))
                                objlogin._Checkouttime = CDate(dtRow.Item("checkouttime"))
                                Else
                                    objlogin._checkintime = CDate(dtRow.Item("checkintime"))
                                    objlogin._Checkouttime = Nothing
                                End If
                                'S.SANDEEP |20-JUL-2019| -- END

                                'Pinkal (15-Nov-2013) -- Start
                                'Enhancement : Oman Changes
                                objlogin._Original_InTime = objlogin._checkintime
                                objlogin._Original_OutTime = objlogin._Checkouttime
                                'Pinkal (15-Nov-2013) -- End


                                objlogin._Remark = dtRow.Item("Remark").ToString
                                objlogin._Workhour = CInt(dtRow.Item("workhour"))
                                objlogin._Holdunkid = CInt(dtRow.Item("holdunkid"))
                                objlogin._SourceType = enInOutSource.Import
                                objlogin._Userunkid = User._Object._Userunkid

                                If dtRow.Table.Columns.Contains("voiddatetime") Then
                                    If dtRow.Item("voiddatetime") IsNot Nothing And dtRow.Item("voiddatetime") IsNot DBNull.Value Then
                                        objlogin._Voiddatetime = CDate(dtRow.Item("voiddatetime"))
                                    End If
                                End If

                                objlogin._Isvoid = CBool(dtRow.Item("isvoid"))
                                If Not dtRow.Item("voidreason") IsNot DBNull.Value And dtRow.Item("voidreason").ToString() <> "" Then
                                    objlogin._Voidreason = dtRow.Item("voidreason").ToString()
                                End If


                                'Pinkal (11-AUG-2017) -- Start
                                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                'If Not objlogin.Insert(FinancialYear._Object._DatabaseName, _
                                '                  User._Object._Userunkid, _
                                '                  FinancialYear._Object._YearUnkid, _
                                '                  Company._Object._Companyunkid, _
                                '                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                  ConfigParameter._Object._UserAccessModeSetting, _
                                '                  True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                '                  ConfigParameter._Object._PolicyManagementTNA, _
                                '                  ConfigParameter._Object._DonotAttendanceinSeconds, _
                                '                  ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") Then
                                '    blnResult = False
                                '    Exit For
                                'End If

                                If Not objlogin.Insert(FinancialYear._Object._DatabaseName _
                                                             , User._Object._Userunkid _
                                                             , FinancialYear._Object._YearUnkid _
                                                             , Company._Object._Companyunkid _
                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                             , ConfigParameter._Object._UserAccessModeSetting _
                                                             , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                             , ConfigParameter._Object._PolicyManagementTNA _
                                                             , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                             , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                             , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                             , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                             , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                             , False, -1, Nothing, "", "") Then
                                    blnResult = False
                                    Exit For
                                End If

                                'Pinkal (11-AUG-2017) -- End

                            Next

                        Catch ex As Exception
                            blnResult = False
                        End Try


                    Case enFingerPrintDevice.BioStar

                        strdate = dRow("objdate").ToString()

                        Dim objDeviceUser As New clsEmpid_devicemapping
                        Dim intEmployeeUnkID As Integer = 0

                        intEmployeeUnkID = objDeviceUser.GetEmployeeUnkID(dRow.Item("employeecode").ToString)

                        If intEmployeeUnkID <= 0 Then
                            dRow.Item("Message") = dRow.Item("employeecode").ToString & " " & Language.getMessage(mstrModuleName, 21, "Not mapped with any employee")
                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                            dRow.Item("image") = imgError
                            dRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objEmployee._Employeeunkid = intEmployeeUnkID
                            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmployeeUnkID
                            'S.SANDEEP [04 JUN 2015] -- END
                            dRow("employeecode") = objEmployee._Employeecode
                            dRow("employeename") = objEmployee._Firstname & " " & objEmployee._Surname
                        End If

                        dsList = objlogin.GetEmployeeLoginDate("List", intEmployeeUnkID, CDate(dRow("date")))

                        If dsList.Tables("List").Rows.Count > 0 Then

                            If isReplace Then

                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                If Not objlogin.Delete(objExport.getEmployeeID(dRow.Item("employeecode").ToString), eZeeDate.convertDate(strdate).Date, ConfigParameter._Object._DonotAttendanceinSeconds) Then
                                    'If Not objlogin.Delete(objExport.getEmployeeID(dRow.Item("employeecode").ToString), eZeeDate.convertDate(strdate).Date) Then
                                    'S.SANDEEP [04 JUN 2015] -- END
                                    blnResult = False
                                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                    dRow.Item("image") = imgError
                                    dRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If
                            Else
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 7, "Already Exists")
                                dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                dRow.Item("image") = imgWarring
                                dRow.Item("objStatus") = 0
                                objWarning.Text = CStr(Val(objWarning.Text) + 1)
                                Continue For
                            End If

                        End If


                        Try
                            Dim blnFlag As Boolean = True
                            objlogin = New clslogin_Tran
                            Dim dtLogin As Date

                            objlogin._Employeeunkid = intEmployeeUnkID
                            objlogin._SourceType = enInOutSource.Import
                            objlogin._Userunkid = User._Object._Userunkid

                            If ConfigParameter._Object._FirstCheckInLastCheckOut = False Then

                                If objlogin.GetLoginType(objlogin._Employeeunkid) = 0 Then

                                    dtLogin = CDate(dRow("date")).Date
                                    objlogin._Logindate = dtLogin
                                    objlogin._checkintime = CDate(dRow("date"))
                                    objlogin._Original_InTime = CDate(dRow("date"))

                                    Dim objEmpShift As New clsEmployee_Shift_Tran
                                    Dim intCurrentShiftID As Integer = 0
                                    intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(dtLogin, intEmployeeUnkID)
                                    objlogin._Shiftunkid = intCurrentShiftID
                                    objlogin._InOutType = 0
                                    objlogin._Workhour = 0
                                    objlogin._Holdunkid = 0
                                    objlogin._Voiddatetime = Nothing
                                    objlogin._Isvoid = False
                                    objlogin._Voidreason = ""

                                    objlogin._IsHoliday = False
                                    objlogin._IsDayOffShift = False
                                    objlogin._IsWeekend = False

                                    objlogin._Checkouttime = Nothing
                                    objlogin._Original_OutTime = Nothing



                                    'Pinkal (11-AUG-2017) -- Start
                                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                    'If objlogin.Insert(FinancialYear._Object._DatabaseName, _
                                    '                   User._Object._Userunkid, _
                                    '                   FinancialYear._Object._YearUnkid, _
                                    '                   Company._Object._Companyunkid, _
                                    '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    '                   ConfigParameter._Object._UserAccessModeSetting, _
                                    '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                    '                   ConfigParameter._Object._PolicyManagementTNA, _
                                    '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                    '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                    If objlogin.Insert(FinancialYear._Object._DatabaseName _
                                                            , User._Object._Userunkid _
                                                            , FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid _
                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            , ConfigParameter._Object._UserAccessModeSetting _
                                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                           , ConfigParameter._Object._PolicyManagementTNA _
                                                           , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                           , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                           , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                           , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                           , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                           , False, -1, Nothing, "", "") = False Then

                                        'Pinkal (11-AUG-2017) -- End


                                        blnResult = False
                                        dRow.Item("Message") = objlogin._Message
                                        dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                        dRow.Item("image") = imgError
                                        dRow.Item("objStatus") = 2
                                        If intError = 0 Then
                                            objError.Text = CStr(Val(objError.Text) + 1)
                                            intError = 1
                                        End If
                                        Continue For
                                    End If


                                Else

                                    'Pinkal (06-Dec-2013) -- Start
                                    'Enhancement : Oman Changes   'FOR SET ALL PROERTY OF THAT TABLE
                                    If objlogin._Loginunkid > 0 Then
                                        objlogin._Loginunkid = objlogin._Loginunkid
                                    End If
                                    'Pinkal (06-Dec-2013) -- End


                                    dtLogin = objlogin._Logindate
                                    objlogin._Checkouttime = CDate(dRow("date"))
                                    objlogin._Original_OutTime = CDate(dRow("date"))
                                    Dim objEmpShift As New clsEmployee_Shift_Tran
                                    Dim intCurrentShiftID As Integer = 0
                                    intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(dtLogin, intEmployeeUnkID)
                                    objlogin._Shiftunkid = intCurrentShiftID


                                    If objlogin._checkintime > objlogin._Checkouttime Then
                                        blnResult = False
                                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 16, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
                                        dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                        dRow.Item("image") = imgError
                                        dRow.Item("objStatus") = 2
                                        If intError = 0 Then
                                            objError.Text = CStr(Val(objError.Text) + 1)
                                            intError = 1
                                        End If
                                        Continue For
                                    End If

                                    If objlogin._checkintime < objlogin._Checkouttime Then
                                        Dim wkmins As Double = DateDiff(DateInterval.Second, objlogin._checkintime, objlogin._Checkouttime)
                                        objlogin._Workhour = CInt(wkmins)

                                        objlogin._InOutType = 1



                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                        'If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                        '                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                        '                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                        '                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                        'ConfigParameter._Object._DonotAttendanceinSeconds, _
                                        'ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                        If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                  , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                  , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                  , False, -1, Nothing, "", "") = False Then

                                            'Pinkal (11-AUG-2017) -- End

                                            blnResult = False
                                            dRow.Item("Message") = objlogin._Message
                                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                            dRow.Item("image") = imgError
                                            dRow.Item("objStatus") = 2
                                            If intError = 0 Then
                                                objError.Text = CStr(Val(objError.Text) + 1)
                                                intError = 1
                                            End If
                                            Continue For
                                        End If
                                    End If
                                End If

                            ElseIf ConfigParameter._Object._FirstCheckInLastCheckOut Then

                                If mstrPreviousEmpCode.Trim <> "" AndAlso mstrPreviousEmpCode <> dRow.Item("employeecode").ToString() Then
                                    Dim intEmId As Integer = objEmployee.GetEmployeeUnkid("", mstrPreviousEmpCode)

                                    If intEmId > 0 Then
                                        Dim drLastRow() As DataRow = mdtGridData.Select("employeecode ='" & mstrPreviousEmpCode & "'", "date desc")

                                        If drLastRow.Length > 0 Then

                                            Dim dtMaxEmployeeDate As DataTable = objlogin.GetEmployeeMaxLoginDate(CDate(drLastRow(0)("date")).Date.AddDays(1), intEmId)

                                            If dtMaxEmployeeDate IsNot Nothing AndAlso dtMaxEmployeeDate.Rows.Count > 0 Then

                                                objlogin._Loginunkid = CInt(dtMaxEmployeeDate.Rows(0)("loginunkid"))

                                                If objlogin._Checkouttime = Nothing Then
                                                    drLastRow(0).Item("Message") = Language.getMessage(mstrModuleName, 22, "MisMatch Login/Logout .")
                                                    drLastRow(0).Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                                    drLastRow(0).Item("image") = imgError
                                                    drLastRow(0).Item("objStatus") = 2
                                                    If intError = 0 Then
                                                        objError.Text = CStr(Val(objError.Text) + 1)
                                                        intError = 1
                                                    End If
                                                    mdtGridData.AcceptChanges()
                                                End If

                                            End If

                                        End If

                                    End If

                                End If

                                objlogin._Employeeunkid = intEmployeeUnkID


                                'Pinkal (09-Jan-2014) -- Start
                                'Enhancement : Oman Changes

                                '                                Dim dtMaxEmployeeDate As DataTable = objlogin.GetEmployeeMaxLoginDate(CDate(dRow("date")), objlogin._Employeeunkid)

                                '                                If dtMaxEmployeeDate IsNot Nothing AndAlso dtMaxEmployeeDate.Rows.Count > 0 Then
                                '                                    objlogin._Loginunkid = CInt(dtMaxEmployeeDate.Rows(0)("loginunkid"))
                                '                                Else
                                '                                    objlogin._Logindate = CDate(dRow("date")).Date
                                '                                End If

                                '                                Dim objShiftTran As New clsshift_tran
                                '                                Dim objEmpShift As New clsEmployee_Shift_Tran
                                '                                Dim intCurrentShiftID As Integer = 0
                                '                                intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(objlogin._Logindate.Date, objlogin._Employeeunkid)

                                '                                objlogin._Shiftunkid = intCurrentShiftID

                                '                                If dtMaxEmployeeDate Is Nothing OrElse dtMaxEmployeeDate.Rows.Count <= 0 Then   'WHEN EMPLOYEE DOESN'T HAVE ENTRY IN PREVIOUS DAY
                                '                                    GoTo NewDayEntry
                                '                                End If

                                '                                objShiftTran.GetShiftTran(objlogin._Shiftunkid)
                                '                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(objlogin._Logindate.Date))))
                                '                                If drShiftTran.Length > 0 Then
                                '                                    Dim mdtDayStartTime As DateTime = CDate(objlogin._Logindate & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                '                                    If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dRow("date"))) >= 1440 Then
                                '                                        If objlogin._InOutType = 0 Then
                                '                                            Dim drError As DataRow = drRow(i - 1)
                                '                                            drError.Item("Message") = Language.getMessage(mstrModuleName, 22, "MisMatch Login/Logout .")
                                '                                            drError.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                '                                            drError.Item("image") = imgError
                                '                                            drError.Item("objStatus") = 2
                                '                                            If intError = 0 Then
                                '                                                objError.Text = CStr(Val(objError.Text) + 1)
                                '                                                intError = 1
                                '                                            End If
                                '                                            blnFlag = True
                                '                                        End If

                                'NewDayEntry:
                                '                                        objlogin._Logindate = CDate(dRow("date")).Date
                                '                                        objlogin._checkintime = CDate(dRow("date"))
                                '                                        objlogin._Original_InTime = objlogin._checkintime
                                '                                        objlogin._Checkouttime = Nothing
                                '                                        objlogin._Original_OutTime = Nothing
                                '                                        objlogin._InOutType = 0
                                '                                        objlogin._Workhour = 0
                                '                                        objlogin._Holdunkid = 0
                                '                                        objlogin._Voiddatetime = Nothing
                                '                                        objlogin._Isvoid = False
                                '                                        objlogin._Voidreason = ""
                                '                                        objlogin._IsHoliday = False
                                '                                        objlogin._IsDayOffShift = False
                                '                                        objlogin._IsWeekend = False

                                '                                        If objlogin.Insert() = False Then
                                '                                            blnResult = False
                                '                                            dRow.Item("Message") = objlogin._Message
                                '                                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                '                                            dRow.Item("image") = imgError
                                '                                            dRow.Item("objStatus") = 2
                                '                                            If intError = 0 Then
                                '                                                objError.Text = CStr(Val(objError.Text) + 1)
                                '                                                intError = 1
                                '                                            End If
                                '                                            Continue For
                                '                                        End If


                                '                                    Else

                                '                                        objlogin._Checkouttime = CDate(dRow("date"))
                                '                                        objlogin._Original_OutTime = objlogin._Checkouttime
                                '                                        Dim wkmins As Double = DateDiff(DateInterval.Second, objlogin._checkintime, objlogin._Checkouttime)
                                '                                        objlogin._Workhour = CInt(wkmins)
                                '                                        objlogin._InOutType = 1
                                '                                        If objlogin.Update() = False Then
                                '                                            blnResult = False
                                '                                            dRow.Item("Message") = objlogin._Message
                                '                                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                '                                            dRow.Item("image") = imgError
                                '                                            dRow.Item("objStatus") = 2
                                '                                            If intError = 0 Then
                                '                                                objError.Text = CStr(Val(objError.Text) + 1)
                                '                                                intError = 1
                                '                                            End If
                                '                                            Continue For
                                '                                        End If
                                '                                    End If

                                '                                End If


                                Dim objShiftTran As New clsshift_tran
                                Dim objEmpShift As New clsEmployee_Shift_Tran
                                Dim intCurrentShiftID As Integer = 0
                                intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(CDate(dRow("date")).Date, objlogin._Employeeunkid)

                                objlogin._Shiftunkid = intCurrentShiftID

                                objShiftTran.GetShiftTran(objlogin._Shiftunkid)
                                'Pinkal (17-Sep-2014) -- Start
                                'Enhancement - VOLTAMP WEEKDAY NAME ISSUE WHEN FIRST DAY OF WEEK IS NOT SUNDAY.
                                'Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dRow("date")).Date))))
                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dRow("date")).Date), False, FirstDayOfWeek.Sunday)))
                                'Pinkal (17-Sep-2014) -- End
                                If drShiftTran.Length > 0 Then
                                    Dim mdtDayStartTime As DateTime = CDate(CDate(dRow("date")).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm tt"))


                                    If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dRow("date"))) < 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                        objlogin._Logindate = CDate(dRow("date")).Date.AddDays(-1)

                                    ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dRow("date"))) > 0 Then
                                        objlogin._Logindate = CDate(dRow("date")).Date

                                    End If


                                    Dim intLoginId As Integer = -1
                                    If objlogin.IsEmployeeLoginExist(eZeeDate.convertDate(objlogin._Logindate.Date), "", objlogin._Employeeunkid, intLoginId) Then
                                        objlogin._Loginunkid = intLoginId
                                        objlogin._Checkouttime = CDate(dRow("date"))
                                        objlogin._Original_OutTime = objlogin._Checkouttime
                                        Dim wkmins As Double = DateDiff(DateInterval.Second, objlogin._checkintime, objlogin._Checkouttime)
                                        objlogin._Workhour = CInt(wkmins)
                                        objlogin._InOutType = 1


                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                        'If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                        '                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                        '                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                        '                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                        'ConfigParameter._Object._DonotAttendanceinSeconds, _
                                        'ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                        If objlogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                 , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                 , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                 , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                 , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                 , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                 , False, -1, Nothing, "", "") = False Then

                                            'Pinkal (11-AUG-2017) -- End



                                            blnResult = False
                                            dRow.Item("Message") = objlogin._Message
                                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                            dRow.Item("image") = imgError
                                            dRow.Item("objStatus") = 2
                                            If intError = 0 Then
                                                objError.Text = CStr(Val(objError.Text) + 1)
                                                intError = 1
                                            End If
                                            Continue For
                                        End If

                                    Else

                                        objlogin._checkintime = CDate(dRow("date"))
                                        objlogin._Original_InTime = objlogin._checkintime
                                        objlogin._Checkouttime = Nothing
                                        objlogin._Original_OutTime = Nothing
                                        objlogin._InOutType = 0
                                        objlogin._Workhour = 0
                                        objlogin._Holdunkid = 0
                                        objlogin._Voiddatetime = Nothing
                                        objlogin._Isvoid = False
                                        objlogin._Voidreason = ""


                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                        'If objlogin.Insert(FinancialYear._Object._DatabaseName, _
                                        '                   User._Object._Userunkid, _
                                        '                   FinancialYear._Object._YearUnkid, _
                                        '                   Company._Object._Companyunkid, _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   ConfigParameter._Object._UserAccessModeSetting, _
                                        '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                        '                   ConfigParameter._Object._PolicyManagementTNA, _
                                        '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                        '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then


                                        If objlogin.Insert(FinancialYear._Object._DatabaseName _
                                                               , User._Object._Userunkid _
                                                               , FinancialYear._Object._YearUnkid _
                                                               , Company._Object._Companyunkid _
                                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                               , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                               , ConfigParameter._Object._UserAccessModeSetting _
                                                               , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                               , ConfigParameter._Object._PolicyManagementTNA _
                                                               , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                               , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                               , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                               , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                               , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                               , False, -1, Nothing, "", "") = False Then

                                            'Pinkal (11-AUG-2017) -- End



                                            blnResult = False
                                            dRow.Item("Message") = objlogin._Message
                                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                            dRow.Item("image") = imgError
                                            dRow.Item("objStatus") = 2
                                            If intError = 0 Then
                                                objError.Text = CStr(Val(objError.Text) + 1)
                                                intError = 1
                                            End If
                                            Continue For
                                        End If

                                    End If

                                    Dim dtMaxEmployeeDate As DataTable = objlogin.GetEmployeeMaxLoginDate(objlogin._Logindate.Date, objlogin._Employeeunkid)
                                    If dtMaxEmployeeDate IsNot Nothing AndAlso dtMaxEmployeeDate.Rows.Count > 0 Then
                                        objlogin._Loginunkid = CInt(dtMaxEmployeeDate.Rows(0)("loginunkid"))

                                        If objlogin._Checkouttime = Nothing Then

                                            Dim drError() As DataRow = mdtGridData.Select("date = '" & objlogin._Original_InTime & "' AND employeecode ='" & dRow.Item("employeecode").ToString() & "'")
                                            If drError.Length > 0 Then
                                                drError(0).Item("Message") = Language.getMessage(mstrModuleName, 22, "MisMatch Login/Logout .")
                                                drError(0).Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                                drError(0).Item("image") = imgError
                                                drError(0).Item("objStatus") = 2
                                                If intError = 0 Then
                                                    objError.Text = CStr(Val(objError.Text) + 1)
                                                    intError = 1
                                                End If
                                                mdtGridData.AcceptChanges()
                                                'blnResult = False
                                            End If

                                        End If

                                    End If

                                End If

                            End If

                            'Pinkal (09-Jan-2014) -- End

                        Catch ex As Exception
                            blnResult = False
                        End Try


                End Select
                'Sohail (04 Oct 2013) -- End

                setStatus(dRow, blnResult, isReplace)
                mstrPreviousEmpCode = dRow.Item("employeecode").ToString()

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportTimesheetData", mstrModuleName)
        End Try
    End Sub

    Private Sub ReadFileToArray(ByVal strfilename As String)
        Dim num_rows As Long
        Dim num_cols As Long

        'Check if file exist
        If IO.File.Exists(strfilename) Then
            Dim tmpstream As IO.StreamReader = IO.File.OpenText(strfilename)
            Dim strlines() As String
            Dim strline() As String

            'Load content of file to strLines array
            strlines = tmpstream.ReadToEnd().Split(CChar(Environment.NewLine))

            ' Redimension the array.
            num_rows = UBound(strlines)

            If strlines(0).Split(CChar(",")).GetValue(0).ToString <> "" Then
                strline = strlines(0).Split(CChar(","))
            Else
                strline = strlines(1).Split(CChar(","))
            End If
            num_cols = UBound(strline)
            ReDim m_Strarray(CInt(num_rows), CInt(num_cols))

            ' Copy the data into the array.
            For x As Long = 0 To num_rows
                If strlines(CInt(x)).Split(CChar(",")).GetValue(0).ToString <> "" Then
                    strline = strlines(CInt(x)).Split(CChar(","))
                    If strline.Length - 1 <> num_cols Then Continue For
                    For y As Long = 0 To CInt(num_cols)
                        m_Strarray(CInt(x), CInt(y)) = strline(CInt(y)).Trim(CChar("""")).Trim
                    Next
                End If
            Next

        End If

    End Sub

    Private Sub CreateOtherFileData()
        Try

            Dim strEmpCode As String() = m_timesheetValue(cboEmployeeCode.Text).Split(CChar(","))
            If strEmpCode.Length <> 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            'Sohail (04 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'Dim strDisplay As String() = m_timesheetValue(cboEmployee.Text).Split(CChar(","))
            'If strDisplay.Length <> 1 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    Exit Sub
            'End If
            Dim strDisplay As String() = {""}
            'Sohail (04 Oct 2013) -- End

            Dim strDate As String() = m_timesheetValue(cboLoginDate.Text).Split(CChar(","))
            If strDate.Length <> 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            'Sohail (04 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            Dim strIn As String() = Nothing
            Dim strOut As String() = Nothing
            Select Case CInt(cboCommunicationDevice.SelectedValue)

                Case enFingerPrintDevice.ZKSoftware
                    strIn = m_timesheetValue(cboCheckin.Text).Split(CChar(","))
                    strOut = m_timesheetValue(cboCheckout.Text).Split(CChar(","))

                    If strIn.Length <> strOut.Length Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                Case enFingerPrintDevice.BioStar

            End Select
            'Sohail (04 Oct 2013) -- End




            Dim dtTimsheet As DataTable = New DataTable("Timesheet")

            dtTimsheet.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("shiftunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("date", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("holdunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("workhour", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("breakhr", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("checkintime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("checkouttime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("remark", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("inouttype", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtTimsheet.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("voidreason", System.Type.GetType("System.String"))

            Dim GetBound As Integer = 0

            'If m_Strarray(0, 0) Is Nothing Then
            '    GetBound = m_Strarray.GetUpperBound(0)
            'Else
            '    GetBound = m_Strarray.GetUpperBound(0) - 1
            'End If

            'Sohail (04 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'For i As Integer = 1 To m_Strarray.GetUpperBound(0)
            For i As Integer = 0 To m_Strarray.GetUpperBound(0)
                'Sohail (04 Oct 2013) -- End
                For iCnt As Integer = 0 To strIn.Length - 1

                    Dim drRow As DataRow = dtTimsheet.NewRow

                    drRow("employeeunkid") = -1
                    drRow("shiftunkid") = -1

                    If m_Strarray(i, CInt(strEmpCode(0))) = Nothing Then Continue For
                    drRow("employeecode") = m_Strarray(i, CInt(strEmpCode(0)))

                    'Sohail (04 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    'If m_Strarray(i, CInt(strDisplay(0))) = Nothing Then Continue For
                    'drRow("employeename") = m_Strarray(i, CInt(strDisplay(0)))
                    drRow("employeename") = ""
                    'Sohail (04 Oct 2013) -- End


                    If m_Strarray(i, CInt(strDate(0))) = Nothing Then Continue For
                    drRow("date") = m_Strarray(i, CInt(strDate(0)))

                    If m_Strarray(i, CInt(strDate(0))) = Nothing Then Continue For
                    drRow("checkintime") = CDate(m_Strarray(i, CInt(strDate(0)))).Date & " " & CDate(m_Strarray(i, CInt(strIn(iCnt)))).ToString("HH:mm:ss")

                    If m_Strarray(i, CInt(strDate(0))) = Nothing Then Continue For
                    If CDate(m_Strarray(i, CInt(strOut(iCnt)))).Date = Nothing Then 'No date given in checkout time
                        drRow("checkouttime") = CDate(m_Strarray(i, CInt(strDate(0)))).Date & " " & CDate(m_Strarray(i, CInt(strOut(iCnt)))).ToString("HH:mm:ss")
                    Else
                        drRow("checkouttime") = CDate(m_Strarray(i, CInt(strOut(iCnt))))
                    End If


                    If CDate(drRow("checkintime")) > CDate(drRow("checkouttime")) Then
                        drRow("checkintime") = CDate(m_Strarray(i, CInt(strDate(0)))).AddDays(-1).Date & " " & CDate(m_Strarray(i, CInt(strIn(iCnt)))).ToString("HH:mm:ss")
                    End If

                    drRow("holdunkid") = 0
                    drRow("workhour") = CInt(DateDiff(DateInterval.Minute, CDate(drRow("checkintime")), CDate(drRow("checkouttime")))) * 60
                    drRow("breakhr") = 0
                    drRow("remark") = ""
                    drRow("inouttype") = 1
                    drRow("isvoid") = False
                    drRow("userunkid") = 0
                    drRow("voiduserunkid") = 0
                    drRow("voiddatetime") = DBNull.Value
                    drRow("voidreason") = ""
                    dtTimsheet.Rows.Add(drRow)

                Next
            Next


            'Pinkal (06-Dec-2013) -- Start
            'Enhancement : Oman Changes
            dtTimsheet = New DataView(dtTimsheet, "", "employeecode asc,checkintime asc ", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (06-Dec-2013) -- End


            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtTimsheet)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateData_Other", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataForExcel()
        Try
            Dim dtTimsheet As DataTable = New DataTable("Timesheet")

            dtTimsheet.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("shiftunkid", System.Type.GetType("System.Int32"))

            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes
            dtTimsheet.Columns.Add("employeecode", System.Type.GetType("System.String"))
            'Pinkal (25-Oct-2012) -- End

            dtTimsheet.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("date", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("holdunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("workhour", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("breakhr", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("checkintime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("checkouttime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("remark", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("inouttype", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtTimsheet.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("voidreason", System.Type.GetType("System.String"))

            For i As Integer = 0 To mdtOther.Rows.Count - 1

                Dim drRow As DataRow = dtTimsheet.NewRow

                drRow("employeeunkid") = -1
                drRow("shiftunkid") = -1

                'Pinkal (25-Oct-2012) -- Start
                'Enhancement : TRA Changes
                drRow("employeecode") = mdtOther.Rows(i)(cboEmployeeCode.Text)
                'Pinkal (25-Oct-2012) -- End

                'Sohail (04 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                'drRow("employeename") = mdtOther.Rows(i)(cboEmployee.Text)
                drRow("employeename") = ""
                'Sohail (04 Oct 2013) -- End

                drRow("date") = mdtOther.Rows(i)(cboLoginDate.Text)
                'Sohail (04 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                'drRow("checkintime") = mdtOther.Rows(i)(cboCheckin.Text)
                'drRow("checkouttime") = mdtOther.Rows(i)(cboCheckout.Text)
                'drRow("holdunkid") = 0
                'drRow("workhour") = CInt(DateDiff(DateInterval.Second, CDate(mdtOther.Rows(i)(cboCheckin.Text)), CDate(mdtOther.Rows(i)(cboCheckout.Text))))
                drRow("holdunkid") = 0
                If IsDBNull(mdtOther.Rows(i)(cboCheckin.Text)) = True OrElse mdtOther.Rows(i)(cboCheckin.Text).ToString.Trim.Length <= 0 Then
'S.SANDEEP |20-JUL-2019| -- START                    
'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date
'Continue For
drRow("checkintime") = DBNull.Value                    
'S.SANDEEP |20-JUL-2019| -- START
                Else
                    'S.SANDEEP |20-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date


                    'Pinkal (21-Apr-2020) -- Start
                    'Enhancement Voltamp -   Change for voltamp as checkintime and checkoutime only taking time not date due to that it was creating problem for night shift and another problem is for date format.
                    drRow("checkintime") = Convert.ToDateTime(mdtOther.Rows(i)(cboCheckin.Text))
                    'drRow("checkintime") = Convert.ToDateTime(CDate(drRow("date")) & " " & mdtOther.Rows(i)(cboCheckin.Text).ToString)
                    'Pinkal (21-Apr-2020) -- End

                   

                    'S.SANDEEP |20-JUL-2019| -- END
                End If
                If IsDBNull(mdtOther.Rows(i)(cboCheckout.Text)) = True OrElse mdtOther.Rows(i)(cboCheckout.Text).ToString.Trim.Length <= 0 Then
                    drRow("checkouttime") = DBNull.Value
                    drRow("workhour") = 0
                Else
                    'S.SANDEEP |20-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : Issue for Import Attandance Data In CheckIn & CheckOut Time Adding Current Date

                    'Pinkal (21-Apr-2020) -- Start
                    'Enhancement Voltamp -   Change for voltamp as checkintime and checkoutime only taking time not date due to that it was creating problem for night shift and another problem is for date format.
                    drRow("checkouttime") = Convert.ToDateTime(mdtOther.Rows(i)(cboCheckout.Text))
                    'drRow("checkouttime") = Convert.ToDateTime(CDate(drRow("date")) & " " & mdtOther.Rows(i)(cboCheckout.Text).ToString)
                    'Pinkal (21-Apr-2020) -- End

                    If IsDBNull(drRow("checkintime")) = False Then
                        drRow("workhour") = CInt(DateDiff(DateInterval.Second, CDate(drRow("checkintime")), CDate(drRow("checkouttime"))))
                    Else
                        drRow("workhour") = 0
                    End If
                    'S.SANDEEP |20-JUL-2019| -- END
                End If
                'Sohail (04 Oct 2013) -- End                
                drRow("breakhr") = 0
                drRow("remark") = ""
                drRow("inouttype") = 1
                drRow("isvoid") = False
                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                dtTimsheet.Rows.Add(drRow)

            Next

            'Pinkal (06-Dec-2013) -- Start
            'Enhancement : Oman Changes
            dtTimsheet = New DataView(dtTimsheet, "", "employeecode asc,checkintime asc ", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (06-Dec-2013) -- End

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtTimsheet)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForExcel", mstrModuleName)
        End Try
    End Sub

    'Sohail (04 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub CreateDataForBioStarExcel()
        Try
            Dim dtTimsheet As DataTable = New DataTable("Timesheet")

            dtTimsheet.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("shiftunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("date", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("holdunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("workhour", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("breakhr", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("checkintime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("checkouttime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("remark", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("inouttype", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtTimsheet.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("voidreason", System.Type.GetType("System.String"))

            For i As Integer = 0 To mdtOther.Rows.Count - 1

                If mdtOther.Rows(i)(cboEmployeeCode.Text) Is Nothing OrElse mdtOther.Rows(i)(cboEmployeeCode.Text).ToString.Trim = "" OrElse mdtOther.Rows(i)(cboEmployeeCode.Text).ToString.Trim = "0" Then Continue For

                Dim drRow As DataRow = dtTimsheet.NewRow

                drRow("employeeunkid") = -1
                drRow("shiftunkid") = -1

                drRow("employeecode") = mdtOther.Rows(i)(cboEmployeeCode.Text)
                drRow("employeename") = ""

                drRow("date") = mdtOther.Rows(i)(cboLoginDate.Text)
                drRow("holdunkid") = 0
                drRow("workhour") = 0 'CInt(DateDiff(DateInterval.Minute, CDate(drRow("checkintime")), CDate(drRow("checkouttime")))) * 60
                drRow("breakhr") = 0
                drRow("remark") = ""
                drRow("inouttype") = 1
                drRow("isvoid") = False
                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                dtTimsheet.Rows.Add(drRow)

            Next

            'Pinkal (06-Dec-2013) -- Start
            'Enhancement : Oman Changes
            dtTimsheet = New DataView(dtTimsheet, "", "employeecode asc,date asc ", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (06-Dec-2013) -- End

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtTimsheet)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForBioStarExcel", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Oct 2013) -- End


    'Sohail (04 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub CreateDataForBioStarCSV()
        Try

            Dim strEmpCode As String() = m_timesheetValue(cboEmployeeCode.Text).Split(CChar(","))
            If strEmpCode.Length <> 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If


            Dim strDate As String() = m_timesheetValue(cboLoginDate.Text).Split(CChar(","))
            If strDate.Length <> 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If



            Dim dtTimsheet As DataTable = New DataTable("Timesheet")

            dtTimsheet.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("shiftunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("date", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("holdunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("workhour", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("breakhr", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("checkintime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("checkouttime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("remark", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("inouttype", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtTimsheet.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("voidreason", System.Type.GetType("System.String"))

            Dim GetBound As Integer = 0



            For i As Integer = 0 To m_Strarray.GetUpperBound(0)

                If m_Strarray(i, CInt(strEmpCode(0))) = Nothing OrElse m_Strarray(i, CInt(strEmpCode(0))).Trim = "" OrElse m_Strarray(i, CInt(strEmpCode(0))).Trim = "0" Then Continue For

                Dim drRow As DataRow = dtTimsheet.NewRow

                drRow("employeecode") = m_Strarray(i, CInt(strEmpCode(0)))
                drRow("shiftunkid") = -1

                drRow("employeeunkid") = -1
                drRow("employeename") = ""

                If m_Strarray(i, CInt(strDate(0))) = Nothing Then Continue For
                drRow("date") = m_Strarray(i, CInt(strDate(0)))

                drRow("holdunkid") = 0
                drRow("workhour") = 0 'CInt(DateDiff(DateInterval.Minute, CDate(drRow("checkintime")), CDate(drRow("checkouttime")))) * 60
                drRow("breakhr") = 0
                drRow("remark") = ""
                drRow("inouttype") = 1
                drRow("isvoid") = False
                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                drRow("voidreason") = ""
                dtTimsheet.Rows.Add(drRow)

            Next

            'Pinkal (06-Dec-2013) -- Start
            'Enhancement : Oman Changes
            dtTimsheet = New DataView(dtTimsheet, "", "employeecode asc,date asc ", DataViewRowState.CurrentRows).ToTable()
            'Pinkal (06-Dec-2013) -- End

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtTimsheet)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForBioStarCSV", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Oct 2013) -- End

#End Region

#Region " Form's Events "

    Private Sub frmImportdata_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Sohail (04 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            Call FillCombo()
            'Sohail (04 Oct 2013) -- End

            objExport = New clsExportData
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportdata_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileDialog As New OpenFileDialog


            'Pinkal (26-Jul-2011) -- Start

            If rdPayrollexportFile.Checked Then
                objFileDialog.Filter = "XML files (*.xml)|*.xml"

            ElseIf rdOtherfile.Checked Then
                objFileDialog.Filter = "XML files (*.xml)|*.xml|CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|Excel files(*.xlsx)|*.xlsx|All files (.*)|*.*"
            End If

            'Pinkal (26-Jul-2011) -- End


            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes
            'objFileDialog.FilterIndex = 5
            objFileDialog.FilterIndex = 4
            'Pinkal (29-Aug-2012) -- End


            If objFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileDialog.FileName
            End If
            objFileDialog = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReplace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReplace.Click
        Try
            ImportData(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReplace_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ContextMenu's Event"

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus = 2"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("objstatus")
                        dtTable.Columns.Remove("objimportedfor")
                        dtTable.Columns.Remove("objdate")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Timesheet Wizard") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus = 0"
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    Dim savDialog As New SaveFileDialog
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("objstatus")
                        dtTable.Columns.Remove("objimportedfor")
                        dtTable.Columns.Remove("objdate")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Timesheet Wizard") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Wizard's Event"

    Private Sub wzImportData_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzImportData.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wzImportData.Pages.IndexOf(wpFileSelection)

                    'Sohail (04 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    If CInt(cboCommunicationDevice.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please the select Communication Device."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                        'Sohail (04 Oct 2013) -- End
                    ElseIf Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    'Sohail (04 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    Select Case CInt(cboCommunicationDevice.SelectedValue)

                        Case enFingerPrintDevice.ZKSoftware
                            objLblEmpCode.Text = Language.getMessage(mstrModuleName, 19, "Employee Code")

                            lblCheckintime.Visible = True
                            lblCheckouttime.Visible = True
                            cboCheckin.Visible = True
                            cboCheckout.Visible = True

                        Case enFingerPrintDevice.BioStar
                            objLblEmpCode.Text = Language.getMessage(mstrModuleName, 18, "Device User ID")

                            lblCheckintime.Visible = False
                            lblCheckouttime.Visible = False
                            cboCheckin.Visible = False
                            cboCheckout.Visible = False

                    End Select
                    'Sohail (04 Oct 2013) -- End


                    If rdPayrollexportFile.Checked Then  'PAYROLL XML FILE

                        mdsPayroll = New DataSet
                        mdsPayroll.ReadXml(txtFilePath.Text)

                        If mdsPayroll.Tables.Count > 0 Then

                            If mdsPayroll.Tables(0).Rows.Count > 0 Then
                                mdsPayroll.Tables(0).Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("shiftunkid", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("holdunkid", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("remark", System.Type.GetType("System.String"))
                                mdsPayroll.Tables(0).Columns.Add("inouttype", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
                                mdsPayroll.Tables(0).Columns.Add("userunkid", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
                                mdsPayroll.Tables(0).Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
                                mdsPayroll.Tables(0).Columns.Add("voidreason", System.Type.GetType("System.String"))
                                For i As Integer = 0 To mdsPayroll.Tables(0).Rows.Count - 1
                                    mdsPayroll.Tables(0).Rows(i)("employeeunkid") = -1
                                    mdsPayroll.Tables(0).Rows(i)("shiftunkid") = -1
                                    mdsPayroll.Tables(0).Rows(i)("holdunkid") = -1
                                    mdsPayroll.Tables(0).Rows(i)("Logindate") = DateTime.Parse(mdsPayroll.Tables(0).Rows(i)("Logindate").ToString())
                                    'Sohail (04 Oct 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    'mdsPayroll.Tables(0).Rows(i)("Checkintime") = DateTime.Parse(mdsPayroll.Tables(0).Rows(i)("Checkintime").ToString())
                                    'mdsPayroll.Tables(0).Rows(i)("Checkouttime") = DateTime.Parse(mdsPayroll.Tables(0).Rows(i)("Checkouttime").ToString())
                                    If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Checkintime")) = True Then
                                        Continue For
                                    End If
                                    If IsDBNull(mdsPayroll.Tables(0).Rows(i)("Checkouttime")) = True Then
                                        mdsPayroll.Tables(0).Rows(i)("Checkouttime") = DBNull.Value
                                    Else
                                        mdsPayroll.Tables(0).Rows(i)("Checkouttime") = DateTime.Parse(mdsPayroll.Tables(0).Rows(i)("Checkouttime").ToString())
                                    End If
                                    'Sohail (04 Oct 2013) -- End
                                    mdsPayroll.Tables(0).Rows(i)("remark") = ""
                                    mdsPayroll.Tables(0).Rows(i)("inouttype") = 1
                                    mdsPayroll.Tables(0).Rows(i)("isvoid") = False
                                    mdsPayroll.Tables(0).Rows(i)("userunkid") = 0
                                    mdsPayroll.Tables(0).Rows(i)("voiduserunkid") = 0
                                    mdsPayroll.Tables(0).Rows(i)("voiddatetime") = DBNull.Value
                                Next

                            End If


                        End If

                        If Not (mdsPayroll.Tables.Contains("Timesheet") Or _
                           mdsPayroll.Tables.Contains("Holiday") Or _
                           mdsPayroll.Tables.Contains("Leave")) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            mdsPayroll = Nothing
                            e.Cancel = True
                            Exit Sub
                        End If


                    ElseIf rdOtherfile.Checked Then

                        Dim ImpFile As New IO.FileInfo(txtFilePath.Text)

                        If ImpFile.Extension.ToLower = ".txt" Or ImpFile.Extension.ToLower = ".csv" Then
                            ReadFileToArray(txtFilePath.Text)

                            If m_Strarray.Length <> 0 Then

                                'Pinkal (25-Oct-2012) -- Start
                                'Enhancement : TRA Changes

                                cboEmployeeCode.Items.Clear()
                                cboEmployee.Items.Clear()
                                cboLoginDate.Items.Clear()
                                cboCheckin.Items.Clear()
                                cboCheckout.Items.Clear()

                                'Pinkal (25-Oct-2012) -- End

                                For j As Integer = 0 To m_Strarray.GetUpperBound(1)
                                    Dim key As String = Nothing
                                    If m_Strarray(0, j) Is Nothing Then
                                        key = m_Strarray(1, j)
                                    Else
                                        key = m_Strarray(0, j)
                                    End If
                                    If m_timesheetValue.ContainsKey(key) Then
                                        Dim strNewVal As String = m_timesheetValue.Item(key)
                                        m_timesheetValue(key) = strNewVal & "," & j
                                    Else


                                        'Pinkal (25-Oct-2012) -- Start
                                        'Enhancement : TRA Changes

                                        cboEmployeeCode.Items.Add(key)
                                        cboEmployee.Items.Add(key)
                                        cboLoginDate.Items.Add(key)
                                        cboCheckin.Items.Add(key)
                                        cboCheckout.Items.Add(key)

                                        'Pinkal (25-Oct-2012) -- End

                                        m_timesheetValue.Add(key, j.ToString)

                                    End If
                                Next
                            Else
                                e.Cancel = True
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            End If


                        ElseIf ImpFile.Extension.ToLower = ".xls" Or ImpFile.Extension.ToLower = ".xlsx" Then
                            'S.SANDEEP [12-Jan-2018] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # 0001843
                            'Dim iExcelData As New ExcelData
                            'S.SANDEEP [12-Jan-2018] -- END

                            'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text, "Timesheet")

                            'Pinkal (24-Jan-2011) -- Start

                            'S.SANDEEP [12-Jan-2018] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # 0001843
                            'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                            ''Pinkal (24-Jan-2011) -- End
                            'mdtOther = iExcelData.Import(txtFilePath.Text).Tables(0).Copy

                            Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                            mdtOther = OpenXML_Import(txtFilePath.Text).Tables(0).Copy
                            'S.SANDEEP [12-Jan-2018] -- END

                            'Pinkal (25-Oct-2012) -- Start
                            'Enhancement : TRA Changes

                            cboEmployeeCode.Items.Clear()
                            cboEmployee.Items.Clear()
                            cboLoginDate.Items.Clear()
                            cboCheckin.Items.Clear()
                            cboCheckout.Items.Clear()

                            For Each dtColumns As DataColumn In mdtOther.Columns
                                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                                cboEmployee.Items.Add(dtColumns.ColumnName)
                                cboLoginDate.Items.Add(dtColumns.ColumnName)
                                cboCheckin.Items.Add(dtColumns.ColumnName)
                                cboCheckout.Items.Add(dtColumns.ColumnName)
                            Next

                            'Pinkal (25-Oct-2012) -- End

                        ElseIf ImpFile.Extension.ToLower = ".xml" Then

                            Dim ds As New DataSet
                            ds.ReadXml(txtFilePath.Text)
                            If ds IsNot Nothing Then
                                mdtOther = ds.Tables(0)


                                'Pinkal (25-Oct-2012) -- Start
                                'Enhancement : TRA Changes

                                cboEmployeeCode.Items.Clear()
                                cboEmployee.Items.Clear()
                                cboLoginDate.Items.Clear()
                                cboCheckin.Items.Clear()
                                cboCheckout.Items.Clear()

                                For Each dtColumns As DataColumn In mdtOther.Columns
                                    cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                                    cboEmployee.Items.Add(dtColumns.ColumnName)
                                    cboLoginDate.Items.Add(dtColumns.ColumnName)
                                    cboCheckin.Items.Add(dtColumns.ColumnName)
                                    cboCheckout.Items.Add(dtColumns.ColumnName)
                                Next

                                'Pinkal (25-Oct-2012) -- End

                            End If

                        Else
                            e.Cancel = True
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Sub
                        End If

                    End If

                    'Pinkal (29-Aug-2012) -- Start
                    'Enhancement : TRA Changes

                Case wzImportData.Pages.IndexOf(wpMapfield)

                    If cboEmployeeCode.SelectedIndex < 0 Then
                        wzImportData.SelectedPage = wpMapfield
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Employee Code is compulsory field to map.Please map this field."))
                        cboEmployeeCode.Select()
                        e.Cancel = True
                        Exit Sub
                        'Sohail (04 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        'ElseIf cboEmployee.SelectedIndex < 0 Then
                        '    wzImportData.SelectedPage = wpMapfield
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Employee Name is compulsory field to map.Please map this field."))
                        '    cboEmployee.Select()
                        '    e.Cancel = True
                        '    Exit Sub
                        'Sohail (04 Oct 2013) -- End
                    ElseIf cboLoginDate.SelectedIndex < 0 Then
                        wzImportData.SelectedPage = wpMapfield
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Login Date is compulsory field to map.Please map this field."))
                        cboLoginDate.Select()
                        e.Cancel = True
                        Exit Sub
                        'Sohail (04 Oct 2013) -- Start
                        'TRA - ENHANCEMENT
                        'ElseIf cboCheckin.SelectedIndex < 0 Then
                        '    wzImportData.SelectedPage = wpMapfield
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Check InTime is compulsory field to map.Please map this field."))
                        '    cboCheckin.Select()
                        '    e.Cancel = True
                        '    Exit Sub
                        'ElseIf cboCheckout.SelectedIndex < 0 Then
                        '    wzImportData.SelectedPage = wpMapfield
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Check OutTime is compulsory field to map.Please map this field."))
                        '    cboCheckout.Select()
                        '    e.Cancel = True
                        '    Exit Sub
                        'Sohail (04 Oct 2013) -- End
                    End If

                    'Sohail (04 Oct 2013) -- Start
                    'TRA - ENHANCEMENT
                    Select Case CInt(cboCommunicationDevice.SelectedValue)
                        Case enFingerPrintDevice.ZKSoftware
                            If cboCheckin.SelectedIndex < 0 Then
                                wzImportData.SelectedPage = wpMapfield
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Check InTime is compulsory field to map.Please map this field."))
                                cboCheckin.Select()
                                e.Cancel = True
                                Exit Sub
                            ElseIf cboCheckout.SelectedIndex < 0 Then
                                wzImportData.SelectedPage = wpMapfield
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Check OutTime is compulsory field to map.Please map this field."))
                                cboCheckout.Select()
                                e.Cancel = True
                                Exit Sub
                            End If
                    End Select
                    'Sohail (04 Oct 2013) -- End

                    'Pinkal (29-Aug-2012) -- End

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub wzImportData_AfterSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles wzImportData.AfterSwitchPages
        Try

            Select Case e.NewIndex
                Case wzImportData.Pages.IndexOf(wpMapfield)
                    If rdPayrollexportFile.Checked Then
                        wzImportData.SelectedPage = wpImportData
                        FillGridData()
                    End If

                Case wzImportData.Pages.IndexOf(wpImportData)

                    'Pinkal (03-Nov-2012) -- Start
                    'Enhancement : TRA Changes
                    wzImportData.CancelText = Language.getMessage(mstrModuleName, 13, "Finish")
                    'Pinkal (03-Nov-2012) -- End

                    If rdPayrollexportFile.Checked Then
                        FillGridData()
                    ElseIf rdOtherfile.Checked Then
                        Dim ImpFile As New IO.FileInfo(txtFilePath.Text)
                        If ImpFile.Extension.ToLower = ".csv" Or ImpFile.Extension.ToLower = ".txt" Then
                            'Sohail (04 Oct 2013) -- Start
                            'TRA - ENHANCEMENT
                            'CreateOtherFileData()
                            Select Case CInt(cboCommunicationDevice.SelectedValue)
                                Case enFingerPrintDevice.ZKSoftware
                                    CreateOtherFileData()
                                Case enFingerPrintDevice.BioStar
                                    CreateDataForBioStarCSV()
                            End Select
                            'Sohail (04 Oct 2013) -- End
                        ElseIf ImpFile.Extension.ToLower = ".xls" Or ImpFile.Extension.ToLower = ".xlsx" Or ImpFile.Extension.ToLower = ".xml" Then
                            'Sohail (04 Oct 2013) -- Start
                            'TRA - ENHANCEMENT
                            'CreateDataForExcel()
                            Select Case CInt(cboCommunicationDevice.SelectedValue)
                                Case enFingerPrintDevice.ZKSoftware
                                    CreateDataForExcel()
                                Case enFingerPrintDevice.BioStar
                                    CreateDataForBioStarExcel()
                            End Select
                            'Sohail (04 Oct 2013) -- End
                        End If
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeCode.SelectedIndexChanged, cboEmployee.SelectedIndexChanged, _
                                                                                                                                                                               cboLoginDate.SelectedIndexChanged, cboCheckin.SelectedIndexChanged, _
                                                                                                                                                                               cboCheckout.SelectedIndexChanged
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then


                For Each cr As Control In pnlControl.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
         
            Call SetLanguage()

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReplace.GradientBackColor = GUI._ButttonBackColor
            Me.btnReplace.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wzImportData.CancelText = Language._Object.getCaption(Me.wzImportData.Name & "_CancelText", Me.wzImportData.CancelText)
            Me.wzImportData.NextText = Language._Object.getCaption(Me.wzImportData.Name & "_NextText", Me.wzImportData.NextText)
            Me.wzImportData.BackText = Language._Object.getCaption(Me.wzImportData.Name & "_BackText", Me.wzImportData.BackText)
            Me.wzImportData.FinishText = Language._Object.getCaption(Me.wzImportData.Name & "_FinishText", Me.wzImportData.FinishText)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.ShowTimeSheetToolStripMenuItem.Text = Language._Object.getCaption(Me.ShowTimeSheetToolStripMenuItem.Name, Me.ShowTimeSheetToolStripMenuItem.Text)
            Me.ShowHolidayDataToolStripMenuItem.Text = Language._Object.getCaption(Me.ShowHolidayDataToolStripMenuItem.Name, Me.ShowHolidayDataToolStripMenuItem.Text)
            Me.ShowLeaveToolStripMenuItem.Text = Language._Object.getCaption(Me.ShowLeaveToolStripMenuItem.Name, Me.ShowLeaveToolStripMenuItem.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.rdPayrollexportFile.Text = Language._Object.getCaption(Me.rdPayrollexportFile.Name, Me.rdPayrollexportFile.Text)
            Me.rdOtherfile.Text = Language._Object.getCaption(Me.rdOtherfile.Name, Me.rdOtherfile.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblMapfield.Text = Language._Object.getCaption(Me.lblMapfield.Name, Me.lblMapfield.Text)
            Me.lblCheckouttime.Text = Language._Object.getCaption(Me.lblCheckouttime.Name, Me.lblCheckouttime.Text)
            Me.lblCheckintime.Text = Language._Object.getCaption(Me.lblCheckintime.Name, Me.lblCheckintime.Text)
            Me.btnReplace.Text = Language._Object.getCaption(Me.btnReplace.Name, Me.btnReplace.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.lblLogindate.Text = Language._Object.getCaption(Me.lblLogindate.Name, Me.lblLogindate.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeeCode.HeaderText = Language._Object.getCaption(Me.colhEmployeeCode.Name, Me.colhEmployeeCode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblComProduct.Text = Language._Object.getCaption(Me.lblComProduct.Name, Me.lblComProduct.Text)
            Me.chkUseMappedDeviceUser.Text = Language._Object.getCaption(Me.chkUseMappedDeviceUser.Name, Me.chkUseMappedDeviceUser.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Replaced")
            Language.setMessage(mstrModuleName, 3, "Success")
            Language.setMessage(mstrModuleName, 4, "Error!")
            Language.setMessage(mstrModuleName, 5, "Fail")
            Language.setMessage(mstrModuleName, 6, "Please the select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 7, "Already Exists")
            Language.setMessage(mstrModuleName, 8, "Employee Code is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 10, "Login Date is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 11, "Check InTime is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 12, "Check OutTime is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 13, "Finish")
            Language.setMessage(mstrModuleName, 14, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 16, "Please check your In Time, it seems incorrect. In Time should be less than Out Time.")
            Language.setMessage(mstrModuleName, 17, "Please the select Communication Device.")
            Language.setMessage(mstrModuleName, 18, "Device User ID")
            Language.setMessage(mstrModuleName, 19, "Employee Code")
            Language.setMessage(mstrModuleName, 20, "Employee not found.")
            Language.setMessage(mstrModuleName, 21, "Not mapped with any employee")
            Language.setMessage(mstrModuleName, 22, "MisMatch Login/Logout .")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

