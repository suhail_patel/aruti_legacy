﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExportdata
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportdata))
        Me.pnlExportdata = New System.Windows.Forms.Panel
        Me.wzExportData = New eZee.Common.eZeeWizard
        Me.wzpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.objlblFinishMsg = New System.Windows.Forms.Label
        Me.lblFinish = New System.Windows.Forms.Label
        Me.wzpFilterData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblEmployeeSelection = New System.Windows.Forms.Label
        Me.tvEmployee = New System.Windows.Forms.TreeView
        Me.chkAll = New System.Windows.Forms.CheckBox
        Me.lblFilterdata = New System.Windows.Forms.Label
        Me.lblSelectDate = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cboFileType = New System.Windows.Forms.ComboBox
        Me.lblFileType = New System.Windows.Forms.Label
        Me.rabAll = New System.Windows.Forms.RadioButton
        Me.lblTodate = New System.Windows.Forms.Label
        Me.rabDate = New System.Windows.Forms.RadioButton
        Me.dtpFromdate = New System.Windows.Forms.DateTimePicker
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.wzpSelectData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.chkSelectall = New System.Windows.Forms.CheckBox
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.colhData = New System.Windows.Forms.ColumnHeader
        Me.lblData = New System.Windows.Forms.Label
        Me.lblSelectData = New System.Windows.Forms.Label
        Me.wzpPathSelection = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnBrowse = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblFolderName = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.pnlExportdata.SuspendLayout()
        Me.wzExportData.SuspendLayout()
        Me.wzpFinish.SuspendLayout()
        Me.wzpFilterData.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.wzpSelectData.SuspendLayout()
        Me.wzpPathSelection.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlExportdata
        '
        Me.pnlExportdata.Controls.Add(Me.wzExportData)
        Me.pnlExportdata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlExportdata.Location = New System.Drawing.Point(0, 0)
        Me.pnlExportdata.Name = "pnlExportdata"
        Me.pnlExportdata.Size = New System.Drawing.Size(654, 456)
        Me.pnlExportdata.TabIndex = 0
        '
        'wzExportData
        '
        Me.wzExportData.Controls.Add(Me.wzpFilterData)
        Me.wzExportData.Controls.Add(Me.wzpSelectData)
        Me.wzExportData.Controls.Add(Me.wzpPathSelection)
        Me.wzExportData.Controls.Add(Me.wzpFinish)
        Me.wzExportData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzExportData.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzExportData.HeaderImage = Global.Aruti.Main.My.Resources.Resources.exportdata
        Me.wzExportData.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzExportData.Location = New System.Drawing.Point(0, 0)
        Me.wzExportData.Name = "wzExportData"
        Me.wzExportData.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wzpPathSelection, Me.wzpSelectData, Me.wzpFilterData, Me.wzpFinish})
        Me.wzExportData.Size = New System.Drawing.Size(654, 456)
        Me.wzExportData.TabIndex = 0
        Me.wzExportData.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzExportData.WelcomeImage = Global.Aruti.Main.My.Resources.Resources.exportdata
        Me.wzExportData.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wzpFinish
        '
        Me.wzpFinish.Controls.Add(Me.objlblFinishMsg)
        Me.wzpFinish.Controls.Add(Me.lblFinish)
        Me.wzpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wzpFinish.Name = "wzpFinish"
        Me.wzpFinish.Size = New System.Drawing.Size(654, 408)
        Me.wzpFinish.Style = eZee.Common.eZeeWizardPageStyle.Finish
        Me.wzpFinish.TabIndex = 10
        '
        'objlblFinishMsg
        '
        Me.objlblFinishMsg.BackColor = System.Drawing.Color.Transparent
        Me.objlblFinishMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblFinishMsg.Location = New System.Drawing.Point(177, 61)
        Me.objlblFinishMsg.Name = "objlblFinishMsg"
        Me.objlblFinishMsg.Size = New System.Drawing.Size(370, 34)
        Me.objlblFinishMsg.TabIndex = 7
        Me.objlblFinishMsg.Text = "Data Export Successfully to "
        '
        'lblFinish
        '
        Me.lblFinish.BackColor = System.Drawing.Color.Transparent
        Me.lblFinish.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinish.Location = New System.Drawing.Point(179, 20)
        Me.lblFinish.Name = "lblFinish"
        Me.lblFinish.Size = New System.Drawing.Size(370, 23)
        Me.lblFinish.TabIndex = 6
        Me.lblFinish.Text = "Finish"
        '
        'wzpFilterData
        '
        Me.wzpFilterData.Controls.Add(Me.lblEmployeeSelection)
        Me.wzpFilterData.Controls.Add(Me.tvEmployee)
        Me.wzpFilterData.Controls.Add(Me.chkAll)
        Me.wzpFilterData.Controls.Add(Me.lblFilterdata)
        Me.wzpFilterData.Controls.Add(Me.lblSelectDate)
        Me.wzpFilterData.Controls.Add(Me.Panel2)
        Me.wzpFilterData.Location = New System.Drawing.Point(0, 0)
        Me.wzpFilterData.Name = "wzpFilterData"
        Me.wzpFilterData.Size = New System.Drawing.Size(654, 408)
        Me.wzpFilterData.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wzpFilterData.TabIndex = 9
        '
        'lblEmployeeSelection
        '
        Me.lblEmployeeSelection.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmployeeSelection.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEmployeeSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeSelection.Location = New System.Drawing.Point(183, 127)
        Me.lblEmployeeSelection.Name = "lblEmployeeSelection"
        Me.lblEmployeeSelection.Size = New System.Drawing.Size(459, 19)
        Me.lblEmployeeSelection.TabIndex = 18
        Me.lblEmployeeSelection.Text = "Select Employee"
        Me.lblEmployeeSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tvEmployee
        '
        Me.tvEmployee.CheckBoxes = True
        Me.tvEmployee.Location = New System.Drawing.Point(183, 148)
        Me.tvEmployee.Name = "tvEmployee"
        Me.tvEmployee.Size = New System.Drawing.Size(459, 223)
        Me.tvEmployee.TabIndex = 17
        '
        'chkAll
        '
        Me.chkAll.BackColor = System.Drawing.Color.Transparent
        Me.chkAll.Location = New System.Drawing.Point(183, 381)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(107, 15)
        Me.chkAll.TabIndex = 15
        Me.chkAll.Text = "Select All"
        Me.chkAll.UseVisualStyleBackColor = False
        '
        'lblFilterdata
        '
        Me.lblFilterdata.BackColor = System.Drawing.Color.Transparent
        Me.lblFilterdata.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterdata.Location = New System.Drawing.Point(179, 13)
        Me.lblFilterdata.Name = "lblFilterdata"
        Me.lblFilterdata.Size = New System.Drawing.Size(413, 20)
        Me.lblFilterdata.TabIndex = 16
        Me.lblFilterdata.Text = "Filter Data"
        '
        'lblSelectDate
        '
        Me.lblSelectDate.BackColor = System.Drawing.Color.Gainsboro
        Me.lblSelectDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSelectDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectDate.Location = New System.Drawing.Point(183, 45)
        Me.lblSelectDate.Name = "lblSelectDate"
        Me.lblSelectDate.Size = New System.Drawing.Size(459, 20)
        Me.lblSelectDate.TabIndex = 19
        Me.lblSelectDate.Text = "Select Date"
        Me.lblSelectDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.cboFileType)
        Me.Panel2.Controls.Add(Me.lblFileType)
        Me.Panel2.Controls.Add(Me.rabAll)
        Me.Panel2.Controls.Add(Me.lblTodate)
        Me.Panel2.Controls.Add(Me.rabDate)
        Me.Panel2.Controls.Add(Me.dtpFromdate)
        Me.Panel2.Controls.Add(Me.dtpTodate)
        Me.Panel2.Location = New System.Drawing.Point(183, 68)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(459, 56)
        Me.Panel2.TabIndex = 20
        '
        'cboFileType
        '
        Me.cboFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFileType.FormattingEnabled = True
        Me.cboFileType.Items.AddRange(New Object() {"Select", "Xml", "Xls", "CSV"})
        Me.cboFileType.Location = New System.Drawing.Point(92, 29)
        Me.cboFileType.Name = "cboFileType"
        Me.cboFileType.Size = New System.Drawing.Size(100, 21)
        Me.cboFileType.TabIndex = 6
        '
        'lblFileType
        '
        Me.lblFileType.Location = New System.Drawing.Point(14, 32)
        Me.lblFileType.Name = "lblFileType"
        Me.lblFileType.Size = New System.Drawing.Size(71, 15)
        Me.lblFileType.TabIndex = 5
        Me.lblFileType.Text = "File Type"
        Me.lblFileType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rabAll
        '
        Me.rabAll.BackColor = System.Drawing.Color.Transparent
        Me.rabAll.Checked = True
        Me.rabAll.Location = New System.Drawing.Point(14, 6)
        Me.rabAll.Name = "rabAll"
        Me.rabAll.Size = New System.Drawing.Size(62, 18)
        Me.rabAll.TabIndex = 0
        Me.rabAll.TabStop = True
        Me.rabAll.Text = "All"
        Me.rabAll.UseVisualStyleBackColor = False
        '
        'lblTodate
        '
        Me.lblTodate.BackColor = System.Drawing.Color.Transparent
        Me.lblTodate.Location = New System.Drawing.Point(304, 6)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(26, 18)
        Me.lblTodate.TabIndex = 3
        Me.lblTodate.Text = "To"
        Me.lblTodate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rabDate
        '
        Me.rabDate.BackColor = System.Drawing.Color.Transparent
        Me.rabDate.Location = New System.Drawing.Point(95, 6)
        Me.rabDate.Name = "rabDate"
        Me.rabDate.Size = New System.Drawing.Size(107, 18)
        Me.rabDate.TabIndex = 1
        Me.rabDate.Text = "Date From"
        Me.rabDate.UseVisualStyleBackColor = False
        '
        'dtpFromdate
        '
        Me.dtpFromdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromdate.Location = New System.Drawing.Point(206, 5)
        Me.dtpFromdate.Name = "dtpFromdate"
        Me.dtpFromdate.Size = New System.Drawing.Size(87, 21)
        Me.dtpFromdate.TabIndex = 2
        '
        'dtpTodate
        '
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(339, 5)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(87, 21)
        Me.dtpTodate.TabIndex = 4
        '
        'wzpSelectData
        '
        Me.wzpSelectData.Controls.Add(Me.chkSelectall)
        Me.wzpSelectData.Controls.Add(Me.lvData)
        Me.wzpSelectData.Controls.Add(Me.lblData)
        Me.wzpSelectData.Controls.Add(Me.lblSelectData)
        Me.wzpSelectData.Location = New System.Drawing.Point(0, 0)
        Me.wzpSelectData.Name = "wzpSelectData"
        Me.wzpSelectData.Size = New System.Drawing.Size(654, 408)
        Me.wzpSelectData.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wzpSelectData.TabIndex = 8
        '
        'chkSelectall
        '
        Me.chkSelectall.Location = New System.Drawing.Point(182, 380)
        Me.chkSelectall.Name = "chkSelectall"
        Me.chkSelectall.Size = New System.Drawing.Size(94, 21)
        Me.chkSelectall.TabIndex = 24
        Me.chkSelectall.Text = "Select All"
        Me.chkSelectall.UseVisualStyleBackColor = True
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = False
        Me.lvData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvData.CheckBoxes = True
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhData})
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(182, 76)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(460, 300)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 23
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.List
        '
        'colhData
        '
        Me.colhData.Tag = "colhData"
        Me.colhData.Text = "Data"
        Me.colhData.Width = 400
        '
        'lblData
        '
        Me.lblData.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.lblData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData.Location = New System.Drawing.Point(182, 55)
        Me.lblData.Name = "lblData"
        Me.lblData.Size = New System.Drawing.Size(460, 20)
        Me.lblData.TabIndex = 22
        Me.lblData.Text = "Select Data From"
        Me.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSelectData
        '
        Me.lblSelectData.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectData.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectData.Location = New System.Drawing.Point(182, 20)
        Me.lblSelectData.Name = "lblSelectData"
        Me.lblSelectData.Size = New System.Drawing.Size(420, 23)
        Me.lblSelectData.TabIndex = 21
        Me.lblSelectData.Text = "Select Data"
        '
        'wzpPathSelection
        '
        Me.wzpPathSelection.Controls.Add(Me.btnBrowse)
        Me.wzpPathSelection.Controls.Add(Me.txtFilePath)
        Me.wzpPathSelection.Controls.Add(Me.lblFolderName)
        Me.wzpPathSelection.Controls.Add(Me.lblMessage)
        Me.wzpPathSelection.Controls.Add(Me.lblTitle)
        Me.wzpPathSelection.Location = New System.Drawing.Point(0, 0)
        Me.wzpPathSelection.Name = "wzpPathSelection"
        Me.wzpPathSelection.Size = New System.Drawing.Size(654, 408)
        Me.wzpPathSelection.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wzpPathSelection.TabIndex = 7
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.Color.White
        Me.btnBrowse.BackgroundImage = CType(resources.GetObject("btnBrowse.BackgroundImage"), System.Drawing.Image)
        Me.btnBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBrowse.BorderColor = System.Drawing.Color.Empty
        Me.btnBrowse.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnBrowse.FlatAppearance.BorderSize = 0
        Me.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowse.ForeColor = System.Drawing.Color.Black
        Me.btnBrowse.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBrowse.GradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Location = New System.Drawing.Point(584, 212)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBrowse.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBrowse.Size = New System.Drawing.Size(23, 20)
        Me.btnBrowse.TabIndex = 15
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(177, 212)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(404, 21)
        Me.txtFilePath.TabIndex = 12
        '
        'lblFolderName
        '
        Me.lblFolderName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFolderName.Location = New System.Drawing.Point(177, 185)
        Me.lblFolderName.Name = "lblFolderName"
        Me.lblFolderName.Size = New System.Drawing.Size(111, 20)
        Me.lblFolderName.TabIndex = 16
        Me.lblFolderName.Text = "Select Folder ..."
        Me.lblFolderName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(176, 60)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(370, 34)
        Me.lblMessage.TabIndex = 14
        Me.lblMessage.Text = "This wizard will Export 'Employee Check In' and 'Employee Check Out' records made" & _
            " by system for other use."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(176, 20)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(370, 23)
        Me.lblTitle.TabIndex = 13
        Me.lblTitle.Text = "Data Export Wizard"
        '
        'frmExportdata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 456)
        Me.Controls.Add(Me.pnlExportdata)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportdata"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Export Data"
        Me.pnlExportdata.ResumeLayout(False)
        Me.wzExportData.ResumeLayout(False)
        Me.wzpFinish.ResumeLayout(False)
        Me.wzpFilterData.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.wzpSelectData.ResumeLayout(False)
        Me.wzpPathSelection.ResumeLayout(False)
        Me.wzpPathSelection.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlExportdata As System.Windows.Forms.Panel
    Friend WithEvents wzExportData As eZee.Common.eZeeWizard
    Friend WithEvents wzpSelectData As eZee.Common.eZeeWizardPage
    Friend WithEvents wzpPathSelection As eZee.Common.eZeeWizardPage
    Friend WithEvents btnBrowse As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFolderName As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lvData As eZee.Common.eZeeListView
    Friend WithEvents lblData As System.Windows.Forms.Label
    Friend WithEvents lblSelectData As System.Windows.Forms.Label
    Friend WithEvents colhData As System.Windows.Forms.ColumnHeader
    Friend WithEvents wzpFilterData As eZee.Common.eZeeWizardPage
    Friend WithEvents lblEmployeeSelection As System.Windows.Forms.Label
    Friend WithEvents tvEmployee As System.Windows.Forms.TreeView
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblFilterdata As System.Windows.Forms.Label
    Friend WithEvents lblSelectDate As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents rabAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents rabDate As System.Windows.Forms.RadioButton
    Friend WithEvents dtpFromdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkSelectall As System.Windows.Forms.CheckBox
    Friend WithEvents wzpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents objlblFinishMsg As System.Windows.Forms.Label
    Friend WithEvents lblFinish As System.Windows.Forms.Label
    Friend WithEvents lblFileType As System.Windows.Forms.Label
    Friend WithEvents cboFileType As System.Windows.Forms.ComboBox
End Class
