﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmGroupattendance

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmGroupattendance"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim dsEmployee As DataSet
    Dim dtLogin As DataTable = Nothing

#End Region

#Region "Form's Event"

    Private Sub frmGroupattendance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            dtpdate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpdate.MaxDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 1, "Clock In"))
            cboStatus.Items.Add(Language.getMessage(mstrModuleName, 2, "Clock Out"))
            FillCombo()
            cboStatus.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroupattendance_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroupattendance_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroupattendance_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetList("List")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsEmployee = objEmployee.GetList("List", , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsEmployee = objEmployee.GetList("List")
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                    User._Object._Userunkid, _
            '                    FinancialYear._Object._YearUnkid, _
            '                    Company._Object._Companyunkid, _
            '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                    ConfigParameter._Object._UserAccessModeSetting, True, _
            '                    ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                    "List", _
            '                    ConfigParameter._Object._ShowFirstAppointmentDate)

            Dim strColList As String = clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_ShiftName & "," & _
            clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Display_Name

            dsEmployee = objEmployee.GetListForDynamicField(strColList, FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                Company._Object._Companyunkid, _
                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List")
            'S.SANDEEP [15 NOV 2016] -- END


            
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End

            'FOR SHIFT 
            Dim dsShift As DataSet = Nothing

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'Dim objshiftmaster As New clsshift_master
            Dim objshiftmaster As New clsNewshift_master

            'Pinkal (14-Dec-2012) -- End

            dsShift = objshiftmaster.getListForCombo("Shift", True)
            cboShift.ValueMember = "shiftunkid"
            cboShift.DisplayMember = "name"
            cboShift.DataSource = dsShift.Tables(0)


            'FOR DEPARTMENT
            Dim dsDepartment As DataSet = Nothing
            Dim objDepartment As New clsDepartment
            dsDepartment = objDepartment.getComboList("Department", True)
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DisplayMember = "name"
            cboDepartment.DataSource = dsDepartment.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If CInt(cboShift.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Shift is compulsory information.Please Select Shift."), enMsgBoxStyle.Information)
                cboShift.Select()
                Exit Sub
            ElseIf CInt(cboDepartment.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Department is compulsory information.Please Select Department."), enMsgBoxStyle.Information)
                cboDepartment.Select()
                Exit Sub
            End If

            Dim drrow As DataRow() = dtLogin.Select("Select = false", "")

            If drrow.Length = dtLogin.Rows.Count And dtLogin.Rows.Count <> 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is compulsory information.Please select atleast one employee for further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dtLogin IsNot Nothing AndAlso dtLogin.Rows.Count > 0 Then

                For Each dr As DataRow In dtLogin.Rows

                    If CBool(dr("Select")) Then
                        Dim objLogin As New clslogin_Tran
                        objLogin._Employeeunkid = CInt(dr("Employeeunkid"))
                        objLogin._Logindate = dtpdate.Value.Date
                        objLogin._SourceType = enInOutSource.GroupLogin
                        objLogin._InOutType = CInt(cboStatus.SelectedIndex)
                        objLogin._Userunkid = User._Object._Userunkid
                        objLogin._Holdunkid = objLogin.CheckforHoldEmployee(objLogin._Employeeunkid, dtpdate.Value.Date)
                        If CInt(cboStatus.SelectedIndex) = 0 Then   'FOR LOGIN
                            objLogin._checkintime = ConfigParameter._Object._CurrentDateAndTime
                            objLogin._Original_InTime = objLogin._checkintime
                            objLogin._Original_OutTime = objLogin._Checkouttime


                            'Pinkal (11-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                            'objLogin.Insert(FinancialYear._Object._DatabaseName, _
                            '                User._Object._Userunkid, _
                            '                FinancialYear._Object._YearUnkid, _
                            '                Company._Object._Companyunkid, _
                            '                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                            '                ConfigParameter._Object._UserAccessModeSetting, _
                            '                True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                            '                ConfigParameter._Object._PolicyManagementTNA, _
                            '                ConfigParameter._Object._DonotAttendanceinSeconds, _
                            '                ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "")


                            objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                 , User._Object._Userunkid _
                                                 , FinancialYear._Object._YearUnkid _
                                                 , Company._Object._Companyunkid _
                                                 , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                 , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                 , ConfigParameter._Object._UserAccessModeSetting _
                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                 , ConfigParameter._Object._PolicyManagementTNA _
                                                 , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                 , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                 , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                 , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                 , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                 , False, -1, Nothing, "", "")

                            'Pinkal (11-AUG-2017) -- End


                        ElseIf CInt(cboStatus.SelectedIndex) = 1 Then  'FOR LOGOUT
                            objLogin._Shiftunkid = CInt(cboShift.SelectedValue)
                            objLogin.GetLoginType(objLogin._Employeeunkid)
                            objLogin._Checkouttime = ConfigParameter._Object._CurrentDateAndTime
                            Dim wkmins As Double = DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime)
                            objLogin._Workhour = CInt(wkmins * 60)
                            objLogin._Original_InTime = objLogin._checkintime
                            objLogin._Original_OutTime = objLogin._Checkouttime



                            'Pinkal (11-AUG-2017) -- Start
                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                            'objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                            '                                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                            '                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                            '                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                            '            ConfigParameter._Object._DonotAttendanceinSeconds, _
                            '            ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "")

                            objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                     , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                     , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                     , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                     , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                     , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                     , False, -1, Nothing, "", "")

                            'Pinkal (11-AUG-2017) -- End


                        End If

                    End If
                Next
                cboShift.SelectedIndex = 0
                cboDepartment.SelectedIndex = 0
                cboStatus.SelectedIndex = 0

            Else

                If CInt(cboStatus.SelectedIndex) = 0 And dtLogin.Rows.Count = 0 Then   'FOR LOGIN
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "There is no employee for login."), enMsgBoxStyle.Information)
                    Exit Sub

                ElseIf CInt(cboStatus.SelectedIndex) = 1 Then 'FOR LOGOUT
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There is no employee for logout."), enMsgBoxStyle.Information)
                    Exit Sub

                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub cboShift_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboShift.SelectedValueChanged, cboDepartment.SelectedValueChanged, cboStatus.SelectedIndexChanged
        Try

            If cboShift.SelectedValue IsNot Nothing And cboDepartment.SelectedValue IsNot Nothing Then
                chkSelectAll.Checked = False
                Dim dtEmployee As DataTable = Nothing


                'Pinkal (24-Jun-2011) -- Start
                'ISSUE : CHECK FOR ACTIVE EMPLOYEE

                'If dsEmployee.Tables(0).Rows.Count > 0 Then
                '    dtEmployee = New DataView(dsEmployee.Tables(0), "shiftunkid = " & CInt(cboShift.SelectedValue) & " AND departmentunkid = " & CInt(cboDepartment.SelectedValue), "employeecode asc", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtEmployee = dsEmployee.Tables(0)
                'End If

                'S.SANDEEP [15 NOV 2016] -- START
                'ENHANCEMENT : QUERY OPTIMIZATION
                'If ConfigParameter._Object._IsIncludeInactiveEmp Then
                '    If dsEmployee.Tables(0).Rows.Count > 0 Then
                '        dtEmployee = New DataView(dsEmployee.Tables(0), "shiftunkid = " & CInt(cboShift.SelectedValue) & " AND departmentunkid = " & CInt(cboDepartment.SelectedValue), "employeecode asc", DataViewRowState.CurrentRows).ToTable
                '    Else
                '        dtEmployee = dsEmployee.Tables(0)
                '    End If
                'Else
                '    If dsEmployee.Tables(0).Rows.Count > 0 Then
                '        dtEmployee = New DataView(dsEmployee.Tables(0), "shiftunkid = " & CInt(cboShift.SelectedValue) & " AND departmentunkid = " & CInt(cboDepartment.SelectedValue) & "  AND isactive = 1", "employeecode asc", DataViewRowState.CurrentRows).ToTable
                '    Else
                '        dtEmployee = New DataView(dsEmployee.Tables(0), "isactive = 1 ", "employeecode asc", DataViewRowState.CurrentRows).ToTable
                '    End If
                'End If

                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                If dsEmployee.Tables(0).Rows.Count > 0 Then
                        dtEmployee = New DataView(dsEmployee.Tables(0), "shiftunkid = " & CInt(cboShift.SelectedValue) & " AND departmentunkid = " & CInt(cboDepartment.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtEmployee = dsEmployee.Tables(0)
                End If
                Else
                    If dsEmployee.Tables(0).Rows.Count > 0 Then
                        dtEmployee = New DataView(dsEmployee.Tables(0), "shiftunkid = " & CInt(cboShift.SelectedValue) & " AND departmentunkid = " & CInt(cboDepartment.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtEmployee = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END




                'Pinkal (24-Jun-2011) -- End


                dtLogin = New DataTable
                dtLogin.Columns.Add("Select", Type.GetType("System.Boolean"))
                dtLogin.Columns.Add("Employeeunkid", Type.GetType("System.Int32"))
                dtLogin.Columns.Add("EmployeeCode", Type.GetType("System.String"))
                dtLogin.Columns.Add("Employee", Type.GetType("System.String"))
                dtLogin.Columns.Add("Displayname", Type.GetType("System.String"))

                For Each dr As DataRow In dtEmployee.Rows
                    Dim objLogin As New clslogin_Tran
                    Dim mintInOutType As Integer = objLogin.GetLoginType(CInt(dr("employeeunkid")))
                    Dim drrow As DataRow = Nothing

                    'S.SANDEEP [15 NOV 2016] -- START
                    'ENHANCEMENT : QUERY OPTIMIZATION
                    'If mintInOutType = 0 And CInt(cboStatus.SelectedIndex) = 0 Then   'FOR LOGIN
                    '    drrow = dtLogin.NewRow
                    '    drrow("Employeeunkid") = dr("employeeunkid")
                    '    drrow("EmployeeCode") = dr("employeecode")
                    '    drrow("Employee") = dr("name")
                    '    drrow("Displayname") = dr("displayname")
                    '    dtLogin.Rows.Add(drrow)
                    'End If
                    'If mintInOutType = 1 And CInt(cboStatus.SelectedIndex) = 1 Then   'FOR LOGOUT
                    '    drrow = dtLogin.NewRow
                    '    drrow("Employeeunkid") = dr("employeeunkid")
                    '    drrow("EmployeeCode") = dr("employeecode")
                    '    drrow("Employee") = dr("name")
                    '    drrow("Displayname") = dr("displayname")
                    '    dtLogin.Rows.Add(drrow)
                    'End If
                        drrow = dtLogin.NewRow
                        drrow("Employeeunkid") = dr("employeeunkid")
                    drrow("EmployeeCode") = dr(IIf(Language.getMessage("clsEmployee_Master", 42, "Code") = "", "Code", Language.getMessage("clsEmployee_Master", 42, "Code")).ToString)
                    drrow("Employee") = dr(IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString)
                    drrow("Displayname") = dr(IIf(Language.getMessage("clsEmployee_Master", 53, "Display Name") = "", "Display Name", Language.getMessage("clsEmployee_Master", 53, "Display Name")).ToString)
                        dtLogin.Rows.Add(drrow)
                    'S.SANDEEP [15 NOV 2016] -- END


                    
                Next

                If dtLogin.Rows.Count > 0 Then
                    chkSelectAll.Enabled = True
                    chkSelectAll.Checked = True
                Else
                    chkSelectAll.Enabled = False
                    chkSelectAll.Checked = False
                End If

                dgEmployee.AutoGenerateColumns = False
                colhEmployeeunkid.DataPropertyName = "employeeunkid"
                objSelect.DataPropertyName = "select"
                colhEmpCode.DataPropertyName = "EmployeeCode"
                colhEmplyoeeName.DataPropertyName = "Employee"
                colhDisplayName.DataPropertyName = "Displayname"
                dgEmployee.DataSource = dtLogin

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboShift_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dtLogin IsNot Nothing AndAlso dtLogin.Rows.Count > 0 Then
                If chkSelectAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In dtLogin.Rows
                        dr("Select") = chkSelectAll.Checked
                    Next
                    dtLogin.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick
        Try
            If e.ColumnIndex = objSelect.Index Then
                dtLogin.Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(dtLogin.Rows(e.RowIndex)(e.ColumnIndex))
                dgEmployee.RefreshEdit()
                Dim drRow As DataRow() = dtLogin.Select("Select = true", "")
                If drRow.Length > 0 Then
                    If dtLogin.Rows.Count = drRow.Length Then
                        chkSelectAll.CheckState = CheckState.Checked
                    Else
                        chkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellClick", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lbldate.Text = Language._Object.getCaption(Me.lbldate.Name, Me.lbldate.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.colhEmpCode.HeaderText = Language._Object.getCaption(Me.colhEmpCode.Name, Me.colhEmpCode.HeaderText)
			Me.colhEmplyoeeName.HeaderText = Language._Object.getCaption(Me.colhEmplyoeeName.Name, Me.colhEmplyoeeName.HeaderText)
			Me.colhDisplayName.HeaderText = Language._Object.getCaption(Me.colhDisplayName.Name, Me.colhDisplayName.HeaderText)
			Me.colhEmployeeunkid.HeaderText = Language._Object.getCaption(Me.colhEmployeeunkid.Name, Me.colhEmployeeunkid.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Clock In")
			Language.setMessage(mstrModuleName, 2, "Clock Out")
			Language.setMessage(mstrModuleName, 3, "Employee is compulsory information.Please select atleast one employee for further operation.")
			Language.setMessage(mstrModuleName, 4, "There is no employee for login.")
			Language.setMessage(mstrModuleName, 5, "There is no employee for logout.")
			Language.setMessage(mstrModuleName, 6, "Shift is compulsory information.Please Select Shift.")
			Language.setMessage(mstrModuleName, 7, "Department is compulsory information.Please Select Department.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class