﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

'Last Message Index = 13

Public Class frmShift_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmShift_AddEdit"
    Private mblnCancel As Boolean = True
    Private objShiftMaster As clsshift_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintShiftMasterUnkid As Integer = -1
    Dim wkMins As Double

    'Pinkal (02-Sep-2011) -- Start
    Dim Separator As String = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    'Pinkal (02-Sep-2011) -- End


#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintShiftMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintShiftMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmShift_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objShiftMaster = New clsshift_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objShiftMaster._Shiftunkid = mintShiftMasterUnkid
            End If
            'Pinkal (02-Sep-2011) -- Start
            GetDays()
            'Pinkal (02-Sep-2011) -- End
            FillCombo()
            GetValue()
            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objShiftMaster = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsshift_master.SetMessages()
            objfrm._Other_ModuleNames = "clsshift_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region " DateTime Picker "

    Private Sub dtpStartTime_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartTime.ValueChanged, dtpEndTime.ValueChanged, txtBreakTime.TextChanged
        Try
            CalculateWorkingHours()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartTime_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Shift Code cannot be blank. Shift Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf CInt(cboShiftType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Shift Type is compulsory information.Please Select Shift Type."), enMsgBoxStyle.Information)
                cboShiftType.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Shift Name cannot be blank. Shift Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            ElseIf lvDay.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Working Day is compulsory information.Please Select Working Day."), enMsgBoxStyle.Information)
                lvDay.Select()
                Exit Sub
            ElseIf CInt(txtOverTimeMin.Text) > CInt(txtHours.Tag) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Over Time Minutes cannot be greater than shift hours."), enMsgBoxStyle.Information)
                txtOverTimeMin.Select()
                Exit Sub
            ElseIf CInt(txtShortTimeMin.Text) > CInt(txtHours.Tag) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Short time Minutes cannot be greater than shift hours."), enMsgBoxStyle.Information)
                txtShortTimeMin.Select()
                Exit Sub
            ElseIf txtFromHalfhour.Text.Trim = "" Or txtFromHalfhour.Text.Trim = "." Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Invalid Starting Half Hour.Please Enter valid Start Half hour."), enMsgBoxStyle.Information)
                txtFromHalfhour.Select()
                Exit Sub
            ElseIf txtToHalfhour.Text.Trim = "" Or txtToHalfhour.Text.Trim = "." Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Invalid End Half Hour.Please Enter valid End Half Hour."), enMsgBoxStyle.Information)
                txtToHalfhour.Select()
                Exit Sub
            ElseIf CDec(txtFromHalfhour.Text) >= CDec(txtHours.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Starting Half hour cannot be greater than equal to shift hours."), enMsgBoxStyle.Information)
                txtFromHalfhour.Select()
                Exit Sub
            ElseIf CDec(txtToHalfhour.Text) >= CDec(txtHours.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "End Half hour cannot be greater than equal to shift hours."), enMsgBoxStyle.Information)
                txtToHalfhour.Select()
                Exit Sub
            ElseIf CDec(txtFromHalfhour.Text) > CDec(txtToHalfhour.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Starting Half hour cannot be greater than equal to End Half hour."), enMsgBoxStyle.Information)
                txtFromHalfhour.Select()
                Exit Sub
            ElseIf CalculateWorkingHours() = False Then
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objShiftMaster.Update()
            Else
                blnFlag = objShiftMaster.Insert()
            End If

            If blnFlag = False And objShiftMaster._Message <> "" Then
                eZeeMsgBox.Show(objShiftMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objShiftMaster = Nothing
                    objShiftMaster = New clsshift_master
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintShiftMasterUnkid = objShiftMaster._Shiftunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            Call objFrm.displayDialog(txtName.Text, objShiftMaster._Shiftname1, objShiftMaster._Shiftname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtBreakTime.BackColor = GUI.ColorOptional
            txtOverTimeMin.BackColor = GUI.ColorOptional
            txtShortTimeMin.BackColor = GUI.ColorOptional
            txtFromHalfhour.BackColor = GUI.ColorOptional
            txtToHalfhour.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            If objShiftMaster._Shifttypeunkid > 0 Then
                'Sandeep [ 12 MARCH 2011 ] -- Start
                'dtpStartTime.Value = objShiftMaster._Starttime
                'dtpEndTime.Value = objShiftMaster._Endtime

                If objShiftMaster._Starttime <> Nothing Then
                    dtpStartTime.Value = objShiftMaster._Starttime
                End If

                If objShiftMaster._Endtime <> Nothing Then
                    dtpEndTime.Value = objShiftMaster._Endtime
                End If
                'Sandeep [ 12 MARCH 2011 ] -- End 

                GetWorkingdays()
            Else
                txtBreakTime.Text = objShiftMaster._Breaktime.ToString()
                dtpStartTime.Value = DateTime.Now
                dtpEndTime.Value = DateTime.Now
                ClearWorkingDays()
            End If
            txtCode.Text = objShiftMaster._Shiftcode
            txtName.Text = objShiftMaster._Shiftname
            cboShiftType.SelectedValue = objShiftMaster._Shifttypeunkid
            chkParttimeshift.Checked = objShiftMaster._Isparttime
            txtBreakTime.Text = objShiftMaster._Breaktime.ToString()
            Dim calcMinute = objShiftMaster._Workinghours / 60
            Dim calMinute As Double = calcMinute Mod 60
            Dim calHour As Double = calcMinute / 60
            txtHours.Text = CStr(CDec(Int(calHour) + (calMinute / 100)))
            txtOverTimeMin.Text = CalculateTime(False, objShiftMaster._CalcOvertimeAter).ToString
            txtShortTimeMin.Text = CalculateTime(False, objShiftMaster._CalcShorttimeBefore).ToString
            txtFromHalfhour.Text = CalculateTime(True, objShiftMaster._FromHrs).ToString("#0.00")
            txtToHalfhour.Text = CalculateTime(True, objShiftMaster._ToHrs).ToString("#0.00")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Dim inthrsec As Integer = 0
        Dim intminsec As Integer = 0
        Try
            objShiftMaster._Shiftcode = txtCode.Text
            objShiftMaster._Shiftname = txtName.Text
            objShiftMaster._Shifttypeunkid = CInt(cboShiftType.SelectedValue)
            objShiftMaster._Isparttime = chkParttimeshift.Checked
            objShiftMaster._Breaktime = CInt(txtBreakTime.Text.Trim)
            objShiftMaster._Starttime = dtpStartTime.Value
            objShiftMaster._Endtime = dtpEndTime.Value
            objShiftMaster._Workinghours = CInt(wkMins * 60)
            objShiftMaster._CalcOvertimeAter = CInt(txtOverTimeMin.Text) * 60
            objShiftMaster._CalcShorttimeBefore = CInt(txtShortTimeMin.Text) * 60

            'Pinkal (02-Sep-2011) -- Start

            'FOR HALF FROM HOUR CONVERT INTO SECONDS
            If txtFromHalfhour.Text.Trim <> "0" Then
                ' inthrsec = CInt(CDec(txtFromHalfhour.Text).ToString("#0.00").Substring(0, CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf("."))) * 3600 'Sohail (28 Dec 2010)
                ' intminsec = CInt(CDec(txtFromHalfhour.Text).ToString("#0.00").Substring(CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf(".") + 1, CDec(txtFromHalfhour.Text).ToString("#0.00").Length - CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf(".") - 1)) * 60

                inthrsec = CInt(CDec(txtFromHalfhour.Text).ToString("#0.00").Substring(0, CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf(Separator))) * 3600 'Sohail (28 Dec 2010)
                intminsec = CInt(CDec(txtFromHalfhour.Text).ToString("#0.00").Substring(CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf(Separator) + 1, CDec(txtFromHalfhour.Text).ToString("#0.00").Length - CDec(txtFromHalfhour.Text).ToString("#0.00").IndexOf(Separator) - 1)) * 60
                objShiftMaster._FromHrs = inthrsec + intminsec
            Else
                objShiftMaster._FromHrs = CInt(txtFromHalfhour.Text)
            End If
            'FOR HALF FROM HOUR CONVERT INTO SECONDS

            'FOR HALF TO HOUR CONVERT INTO SECONDS
            If txtToHalfhour.Text.Trim <> "0" Then
                inthrsec = CInt(CDec(txtToHalfhour.Text).ToString("#0.00").Substring(0, CDec(txtToHalfhour.Text).ToString("#0.00").IndexOf(Separator))) * 3600
                intminsec = CInt(CDec(txtToHalfhour.Text).ToString("#0.00").Substring(CDec(txtToHalfhour.Text).ToString("#0.00").IndexOf(Separator) + 1, CDec(txtToHalfhour.Text).ToString("#0.00").Length - CDec(txtToHalfhour.Text).ToString("#0.00").IndexOf(Separator) - 1)) * 60
                objShiftMaster._ToHrs = inthrsec + intminsec
            Else
                objShiftMaster._ToHrs = CInt(txtToHalfhour.Text)
            End If
            'FOR HALF TO HOUR CONVERT INTO SECONDS

            'Pinkal (02-Sep-2011) -- End

            objShiftMaster._Shiftdays = SetWorkingdays()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsShifttype As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "Shift Type")
            cboShiftType.ValueMember = "masterunkid"
            cboShiftType.DisplayMember = "name"
            cboShiftType.DataSource = dsShifttype.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function SetWorkingdays() As String
        Dim wkdays As String = String.Empty
        Try
            For i As Integer = 0 To lvDay.CheckedItems.Count - 1
                wkdays = wkdays & lvDay.CheckedItems.Item(i).Text & "|"
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetWorkingdays", mstrModuleName)
        End Try
        Return wkdays.Substring(0, wkdays.Length - 1)
    End Function

    Private Sub GetWorkingdays()
        Try
            Dim ar As Array = objShiftMaster._Shiftdays.Split(CChar("|"))
            If ar.Length > 0 Then
                For i As Integer = 0 To lvDay.Items.Count - 1
                    For j As Integer = 0 To ar.Length - 1
                        If lvDay.Items(i).Text.Trim() = ar.GetValue(j).ToString() Then
                            lvDay.Items(i).Checked = True
                            Exit For
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetWorkingdays", mstrModuleName)
        End Try
    End Sub

    Private Function CalculateWorkingHours() As Boolean
        Dim dt As DateTime
        Try
            wkMins = 0
            If dtpStartTime.Value > dtpEndTime.Value Then
                dt = dtpEndTime.Value.AddDays(1)
                wkMins = DateDiff(DateInterval.Minute, dtpStartTime.Value, dt)
            Else
                wkMins = DateDiff(DateInterval.Minute, dtpStartTime.Value, dtpEndTime.Value)
            End If

            If wkMins < Val(txtBreakTime.Text.Trim()) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Invalid Break Time. Break Time cannot Greater than working hours."), enMsgBoxStyle.Information)
                txtBreakTime.Focus()
                Return False
            Else
                wkMins = wkMins - Val(txtBreakTime.Text.Trim())
            End If
            Dim calMinute As Double = wkMins Mod 60
            Dim calHour As Double = wkMins / 60
            txtHours.Text = CStr(CDec(Int(calHour) + (calMinute / 100)))

            'START FOR STORE WOKING WORKING HOURS IN MINTUES
            txtHours.Tag = CInt(calHour * 60) + CInt(calMinute / 100)
            'END FOR STORE WOKING WORKING HOURS IN MINTUES
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CalculateWorkingHours", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ClearWorkingDays()
        Try
            If lvDay.CheckedItems.Count > 0 Then
                For i As Integer = 0 To lvDay.Items.Count - 1
                    If lvDay.Items(i).Checked Then
                        lvDay.Items(i).Checked = False
                    End If
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, " ClearWorkingDays", mstrModuleName)
        End Try
    End Sub

    Private Sub TextBoxTimeValidation(ByVal txt As TextBox)
        Try
            If txt.Text = "" Then
                txt.Text = "0"
            ElseIf txt.Text.Trim.Contains(".") Then
                If txt.Text.Trim.Contains(".60") Then
calFromHr:          txt.Text = CDec(CDec(txt.Text.Trim) + 0.4).ToString("#0.00")
                ElseIf txt.Text <> "" And txt.Text <> "." Then
                    If txt.Text.Trim.IndexOf(".") = 0 And txt.Text.Trim.Contains(".60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf("."), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(".")) = "." Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(txt.Text.Substring(txt.Text.Trim.IndexOf("."), txt.Text.Trim.Length - txt.Text.Trim.IndexOf("."))) > 0.6 Then
                        GoTo calFromHr
                    Else
                        txt.Text = CDec(txt.Text).ToString("#0.00")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TextBoxTimeValidation", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddShiftType.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'Pinkal (02-Sep-2011) -- Start

    Private Sub GetDays()
        Try
            Dim ardays() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames()

            For i As Integer = 0 To ardays.Length - 1
                lvDay.Items.Add(ardays(i).ToString())
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDays", mstrModuleName)
        End Try
    End Sub

    'Pinkal (02-Sep-2011) -- End

#End Region

#Region "Textbox's Event"

    Private Sub txtFromHalfhour_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFromHalfhour.Validated
        Try
            TextBoxTimeValidation(txtFromHalfhour)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFromHalfhour_Validated", mstrModuleName)
        End Try
    End Sub

    Private Sub txtToHalfhour_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtToHalfhour.Validated
        Try
            TextBoxTimeValidation(txtToHalfhour)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtToHalfhour_Validated", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Mar-2011) -- Start

    Private Sub txtBreakTime_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBreakTime.Validated, txtOverTimeMin.Validated, txtShortTimeMin.Validated
        If CType(sender, TextBox).Text = "" Then
            CType(sender, TextBox).Text = "0"
        End If
    End Sub

    'Pinkal (10-Mar-2011) -- End

#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddShiftType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddShiftType.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SHIFT_TYPE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "Shift")
                With cboShiftType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Shift")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbShiftSetting.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShiftSetting.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblMinute.Text = Language._Object.getCaption(Me.lblMinute.Name, Me.lblMinute.Text)
            Me.lblcode.Text = Language._Object.getCaption(Me.lblcode.Name, Me.lblcode.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblStartTime.Text = Language._Object.getCaption(Me.lblStartTime.Name, Me.lblStartTime.Text)
            Me.lblEndTime.Text = Language._Object.getCaption(Me.lblEndTime.Name, Me.lblEndTime.Text)
            Me.lblWorkingHours.Text = Language._Object.getCaption(Me.lblWorkingHours.Name, Me.lblWorkingHours.Text)
            Me.lblDay.Text = Language._Object.getCaption(Me.lblDay.Name, Me.lblDay.Text)
            Me.lblBreakTime.Text = Language._Object.getCaption(Me.lblBreakTime.Name, Me.lblBreakTime.Text)
            Me.chkParttimeshift.Text = Language._Object.getCaption(Me.chkParttimeshift.Name, Me.chkParttimeshift.Text)
            Me.lblShiftType.Text = Language._Object.getCaption(Me.lblShiftType.Name, Me.lblShiftType.Text)
            Me.gbShiftSetting.Text = Language._Object.getCaption(Me.gbShiftSetting.Name, Me.gbShiftSetting.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.lblOvertime.Text = Language._Object.getCaption(Me.lblOvertime.Name, Me.lblOvertime.Text)
            Me.Label4.Text = Language._Object.getCaption(Me.Label4.Name, Me.Label4.Text)
            Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
            Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
            Me.lblShorttime.Text = Language._Object.getCaption(Me.lblShorttime.Name, Me.lblShorttime.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Shift Code cannot be blank. Shift Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Shift Type is compulsory information.Please Select Shift Type.")
            Language.setMessage(mstrModuleName, 3, "Shift Name cannot be blank. Shift Name is required information.")
            Language.setMessage(mstrModuleName, 4, "Working Day is compulsory information.Please Select Working Day.")
            Language.setMessage(mstrModuleName, 5, "Invalid Break Time. Break Time cannot Greater than working hours.")
            Language.setMessage(mstrModuleName, 6, "Over Time Minutes cannot be greater than shift hours.")
            Language.setMessage(mstrModuleName, 7, "Short time Minutes cannot be greater than shift hours.")
            Language.setMessage(mstrModuleName, 8, "Starting Half hour cannot be greater than equal to shift hours.")
            Language.setMessage(mstrModuleName, 9, "End Half hour cannot be greater than equal to shift hours.")
            Language.setMessage(mstrModuleName, 10, "Starting Half hour cannot be greater than equal to End Half hour.")
            Language.setMessage(mstrModuleName, 11, "Invalid Starting Half Hour.Please Enter valid Start Half hour.")
            Language.setMessage(mstrModuleName, 12, "Invalid End Half Hour.Please Enter valid End Half Hour.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class