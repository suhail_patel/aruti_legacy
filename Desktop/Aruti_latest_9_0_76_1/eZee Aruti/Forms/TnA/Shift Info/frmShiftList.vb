﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmShiftList

#Region "Private Variable"

    Private objshiftMaster As clsNewshift_master
    Private ReadOnly mstrModuleName As String = "frmShiftList"

#End Region

#Region "Form's Event"

    Private Sub frmShiftList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objshiftMaster = New clsNewshift_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            fillList()

            If lvshiftcodes.Items.Count > 0 Then lvshiftcodes.Items(0).Selected = True
            lvshiftcodes.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShiftList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShiftList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShiftList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmShiftList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objshiftMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()


            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'clsshift_master.SetMessages()
            'objfrm._Other_ModuleNames = "clsNewshift_master"

            clsNewshift_master.SetMessages()
            objfrm._Other_ModuleNames = "clsNewshift_master"


            'Pinkal (03-Jul-2013) -- End
            objfrm.displayDialog(Me)



            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try

            Dim objfrmShift_AddEdit As New frmNewShift_AddEdit
            If objfrmShift_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvshiftcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Shift from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvshiftcodes.Select()
            Exit Sub
        End If
        Dim objfrmShift_AddEdit As New frmNewShift_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvshiftcodes.SelectedItems(0).Index
            If objfrmShift_AddEdit.displayDialog(CInt(lvshiftcodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmShift_AddEdit = Nothing

            lvshiftcodes.Items(intSelectedIndex).Selected = True
            lvshiftcodes.EnsureVisible(intSelectedIndex)
            lvshiftcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmShift_AddEdit IsNot Nothing Then objfrmShift_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvshiftcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Shift from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvshiftcodes.Select()
            Exit Sub
        End If
        If objshiftMaster.isUsed(CInt(lvshiftcodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Shift. Reason: This Shift is in use."), enMsgBoxStyle.Information) '?2
            lvshiftcodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvshiftcodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Shift?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objshiftMaster.Delete(CInt(lvshiftcodes.SelectedItems(0).Tag))
                lvshiftcodes.SelectedItems(0).Remove()

                If lvshiftcodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvshiftcodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvshiftcodes.Items.Count - 1
                    lvshiftcodes.Items(intSelectedIndex).Selected = True
                    lvshiftcodes.EnsureVisible(intSelectedIndex)
                ElseIf lvshiftcodes.Items.Count <> 0 Then
                    lvshiftcodes.Items(intSelectedIndex).Selected = True
                    lvshiftcodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvshiftcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsShiftList As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in TNA module.
            If User._Object.Privilege._AllowToViewShiftList = False Then Exit Sub

            'Varsha Rana (17-Oct-2017) -- End


            dsShiftList = objshiftMaster.GetList("List")

            Dim lvItem As ListViewItem

            lvshiftcodes.Items.Clear()
            For Each drRow As DataRow In dsShiftList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("shiftcode").ToString
                lvItem.Tag = drRow("shiftunkid")
                lvItem.SubItems.Add(drRow("shiftname").ToString)
                lvItem.SubItems.Add(drRow("weekhours").ToString)
                lvshiftcodes.Items.Add(lvItem)
            Next

            If lvshiftcodes.Items.Count > 16 Then
                colhWeekHrs.Width = 205 - 18
            Else
                colhWeekHrs.Width = 205
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsShiftList.Dispose()
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddShiftInformation
            btnEdit.Enabled = User._Object.Privilege._EditShiftInformation
            btnDelete.Enabled = User._Object.Privilege._DeleteShiftInformation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhsShiftcode.Text = Language._Object.getCaption(CStr(Me.colhsShiftcode.Tag), Me.colhsShiftcode.Text)
            Me.colhShiftname.Text = Language._Object.getCaption(CStr(Me.colhShiftname.Tag), Me.colhShiftname.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhWeekHrs.Text = Language._Object.getCaption(CStr(Me.colhWeekHrs.Tag), Me.colhWeekHrs.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Shift from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Shift. Reason: This Shift is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Shift?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class