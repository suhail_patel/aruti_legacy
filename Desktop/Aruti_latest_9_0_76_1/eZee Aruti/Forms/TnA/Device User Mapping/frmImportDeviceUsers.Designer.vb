﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportDeviceUsers
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportDeviceUsers))
        Me.PnlImportdata = New System.Windows.Forms.Panel
        Me.wzImportData = New eZee.Common.eZeeWizard
        Me.wpImportData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnReplace = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgEmployeedata = New System.Windows.Forms.DataGridView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.wpMapfield = New eZee.Common.eZeeWizardPage(Me.components)
        Me.pnlControl = New System.Windows.Forms.Panel
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.objLblEmpCode = New System.Windows.Forms.Label
        Me.cboDeviceUserId = New System.Windows.Forms.ComboBox
        Me.lblDeviceUserId = New System.Windows.Forms.Label
        Me.lblMapfield = New System.Windows.Forms.Label
        Me.wpFileSelection = New eZee.Common.eZeeWizardPage(Me.components)
        Me.rdOtherfile = New System.Windows.Forms.RadioButton
        Me.rdPayrollexportFile = New System.Windows.Forms.RadioButton
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.colhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDeviceUserId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhImortedFor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PnlImportdata.SuspendLayout()
        Me.wzImportData.SuspendLayout()
        Me.wpImportData.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.wpMapfield.SuspendLayout()
        Me.pnlControl.SuspendLayout()
        Me.wpFileSelection.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlImportdata
        '
        Me.PnlImportdata.Controls.Add(Me.wzImportData)
        Me.PnlImportdata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlImportdata.Location = New System.Drawing.Point(0, 0)
        Me.PnlImportdata.Name = "PnlImportdata"
        Me.PnlImportdata.Size = New System.Drawing.Size(618, 424)
        Me.PnlImportdata.TabIndex = 1
        '
        'wzImportData
        '
        Me.wzImportData.Controls.Add(Me.wpImportData)
        Me.wzImportData.Controls.Add(Me.wpMapfield)
        Me.wzImportData.Controls.Add(Me.wpFileSelection)
        Me.wzImportData.Controls.Add(Me.objbuttonBack)
        Me.wzImportData.Controls.Add(Me.objbuttonCancel)
        Me.wzImportData.Controls.Add(Me.objbuttonNext)
        Me.wzImportData.Controls.Add(Me.objbuttonSave)
        Me.wzImportData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzImportData.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.wzImportData.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzImportData.Location = New System.Drawing.Point(0, 0)
        Me.wzImportData.Name = "wzImportData"
        Me.wzImportData.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpFileSelection, Me.wpMapfield, Me.wpImportData})
        Me.wzImportData.SaveEnabled = True
        Me.wzImportData.SaveText = "Save && Finish"
        Me.wzImportData.SaveVisible = False
        Me.wzImportData.SetSaveIndexBeforeFinishIndex = False
        Me.wzImportData.Size = New System.Drawing.Size(618, 424)
        Me.wzImportData.TabIndex = 0
        Me.wzImportData.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.WelcomeImage = Nothing
        Me.wzImportData.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wpImportData
        '
        Me.wpImportData.Controls.Add(Me.btnReplace)
        Me.wpImportData.Controls.Add(Me.btnFilter)
        Me.wpImportData.Controls.Add(Me.dgEmployeedata)
        Me.wpImportData.Controls.Add(Me.Panel1)
        Me.wpImportData.Location = New System.Drawing.Point(0, 0)
        Me.wpImportData.Name = "wpImportData"
        Me.wpImportData.Size = New System.Drawing.Size(618, 376)
        Me.wpImportData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpImportData.TabIndex = 8
        '
        'btnReplace
        '
        Me.btnReplace.BackColor = System.Drawing.Color.White
        Me.btnReplace.BackgroundImage = CType(resources.GetObject("btnReplace.BackgroundImage"), System.Drawing.Image)
        Me.btnReplace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReplace.BorderColor = System.Drawing.Color.Empty
        Me.btnReplace.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReplace.Enabled = False
        Me.btnReplace.FlatAppearance.BorderSize = 0
        Me.btnReplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplace.ForeColor = System.Drawing.Color.Black
        Me.btnReplace.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReplace.GradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Location = New System.Drawing.Point(525, 335)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Size = New System.Drawing.Size(81, 30)
        Me.btnReplace.TabIndex = 20
        Me.btnReplace.Text = "&Replace"
        Me.btnReplace.UseVisualStyleBackColor = True
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 335)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(91, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 19
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(187, 92)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(186, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'dgEmployeedata
        '
        Me.dgEmployeedata.AllowUserToAddRows = False
        Me.dgEmployeedata.AllowUserToDeleteRows = False
        Me.dgEmployeedata.AllowUserToResizeColumns = False
        Me.dgEmployeedata.AllowUserToResizeRows = False
        Me.dgEmployeedata.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployeedata.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployeedata.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhImage, Me.colhEmployeeCode, Me.colhEmployee, Me.colhDeviceUserId, Me.colhStatus, Me.colhMessage, Me.objcolhImortedFor, Me.objcolhstatus, Me.objcolhDate})
        Me.dgEmployeedata.Location = New System.Drawing.Point(12, 64)
        Me.dgEmployeedata.MultiSelect = False
        Me.dgEmployeedata.Name = "dgEmployeedata"
        Me.dgEmployeedata.ReadOnly = True
        Me.dgEmployeedata.RowHeadersVisible = False
        Me.dgEmployeedata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgEmployeedata.Size = New System.Drawing.Size(594, 265)
        Me.dgEmployeedata.TabIndex = 18
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ezWait)
        Me.Panel1.Controls.Add(Me.objError)
        Me.Panel1.Controls.Add(Me.objWarning)
        Me.Panel1.Controls.Add(Me.objSuccess)
        Me.Panel1.Controls.Add(Me.lblWarning)
        Me.Panel1.Controls.Add(Me.lblError)
        Me.Panel1.Controls.Add(Me.objTotal)
        Me.Panel1.Controls.Add(Me.lblSuccess)
        Me.Panel1.Controls.Add(Me.lblTotal)
        Me.Panel1.Location = New System.Drawing.Point(12, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 51)
        Me.Panel1.TabIndex = 1
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(472, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(358, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(472, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(404, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(517, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(358, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(517, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(404, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpMapfield
        '
        Me.wpMapfield.Controls.Add(Me.pnlControl)
        Me.wpMapfield.Controls.Add(Me.lblMapfield)
        Me.wpMapfield.Location = New System.Drawing.Point(0, 0)
        Me.wpMapfield.Name = "wpMapfield"
        Me.wpMapfield.Size = New System.Drawing.Size(618, 376)
        Me.wpMapfield.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpMapfield.TabIndex = 9
        '
        'pnlControl
        '
        Me.pnlControl.Controls.Add(Me.cboEmployeeCode)
        Me.pnlControl.Controls.Add(Me.objLblEmpCode)
        Me.pnlControl.Controls.Add(Me.cboDeviceUserId)
        Me.pnlControl.Controls.Add(Me.lblDeviceUserId)
        Me.pnlControl.Location = New System.Drawing.Point(183, 57)
        Me.pnlControl.Name = "pnlControl"
        Me.pnlControl.Size = New System.Drawing.Size(390, 72)
        Me.pnlControl.TabIndex = 23
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(119, 9)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(121, 21)
        Me.cboEmployeeCode.TabIndex = 23
        '
        'objLblEmpCode
        '
        Me.objLblEmpCode.Location = New System.Drawing.Point(14, 10)
        Me.objLblEmpCode.Name = "objLblEmpCode"
        Me.objLblEmpCode.Size = New System.Drawing.Size(90, 19)
        Me.objLblEmpCode.TabIndex = 24
        Me.objLblEmpCode.Text = "Employee Code"
        Me.objLblEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDeviceUserId
        '
        Me.cboDeviceUserId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeviceUserId.FormattingEnabled = True
        Me.cboDeviceUserId.Location = New System.Drawing.Point(119, 36)
        Me.cboDeviceUserId.Name = "cboDeviceUserId"
        Me.cboDeviceUserId.Size = New System.Drawing.Size(121, 21)
        Me.cboDeviceUserId.TabIndex = 2
        '
        'lblDeviceUserId
        '
        Me.lblDeviceUserId.Location = New System.Drawing.Point(14, 37)
        Me.lblDeviceUserId.Name = "lblDeviceUserId"
        Me.lblDeviceUserId.Size = New System.Drawing.Size(90, 19)
        Me.lblDeviceUserId.TabIndex = 20
        Me.lblDeviceUserId.Text = "Device User Id"
        Me.lblDeviceUserId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMapfield
        '
        Me.lblMapfield.BackColor = System.Drawing.Color.Transparent
        Me.lblMapfield.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapfield.Location = New System.Drawing.Point(179, 19)
        Me.lblMapfield.Name = "lblMapfield"
        Me.lblMapfield.Size = New System.Drawing.Size(124, 23)
        Me.lblMapfield.TabIndex = 10
        Me.lblMapfield.Text = "Map Fields"
        '
        'wpFileSelection
        '
        Me.wpFileSelection.Controls.Add(Me.rdOtherfile)
        Me.wpFileSelection.Controls.Add(Me.rdPayrollexportFile)
        Me.wpFileSelection.Controls.Add(Me.btnOpenFile)
        Me.wpFileSelection.Controls.Add(Me.txtFilePath)
        Me.wpFileSelection.Controls.Add(Me.lblSelectfile)
        Me.wpFileSelection.Controls.Add(Me.lblMessage)
        Me.wpFileSelection.Controls.Add(Me.lblTitle)
        Me.wpFileSelection.Location = New System.Drawing.Point(0, 0)
        Me.wpFileSelection.Name = "wpFileSelection"
        Me.wpFileSelection.Size = New System.Drawing.Size(618, 376)
        Me.wpFileSelection.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpFileSelection.TabIndex = 7
        '
        'rdOtherfile
        '
        Me.rdOtherfile.BackColor = System.Drawing.Color.Transparent
        Me.rdOtherfile.Checked = True
        Me.rdOtherfile.Location = New System.Drawing.Point(183, 139)
        Me.rdOtherfile.Name = "rdOtherfile"
        Me.rdOtherfile.Size = New System.Drawing.Size(123, 19)
        Me.rdOtherfile.TabIndex = 15
        Me.rdOtherfile.TabStop = True
        Me.rdOtherfile.Text = "Other File"
        Me.rdOtherfile.UseVisualStyleBackColor = False
        Me.rdOtherfile.Visible = False
        '
        'rdPayrollexportFile
        '
        Me.rdPayrollexportFile.BackColor = System.Drawing.Color.Transparent
        Me.rdPayrollexportFile.Location = New System.Drawing.Point(183, 114)
        Me.rdPayrollexportFile.Name = "rdPayrollexportFile"
        Me.rdPayrollexportFile.Size = New System.Drawing.Size(225, 19)
        Me.rdPayrollexportFile.TabIndex = 14
        Me.rdPayrollexportFile.Text = "Payroll Export File (Only Xml File)"
        Me.rdPayrollexportFile.UseVisualStyleBackColor = False
        Me.rdPayrollexportFile.Visible = False
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(535, 206)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 13
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(183, 206)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(346, 21)
        Me.txtFilePath.TabIndex = 12
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(183, 179)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(96, 20)
        Me.lblSelectfile.TabIndex = 11
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(179, 59)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(370, 34)
        Me.lblMessage.TabIndex = 10
        Me.lblMessage.Text = "This wizard will import 'Employee Device User ID' from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(179, 19)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(370, 23)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "Data Import Wizard"
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(366, 388)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(534, 388)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(450, 388)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'objbuttonSave
        '
        Me.objbuttonSave.BackColor = System.Drawing.Color.White
        Me.objbuttonSave.BackgroundImage = CType(resources.GetObject("objbuttonSave.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonSave.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonSave.FlatAppearance.BorderSize = 0
        Me.objbuttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonSave.ForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonSave.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Location = New System.Drawing.Point(229, 224)
        Me.objbuttonSave.Name = "objbuttonSave"
        Me.objbuttonSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonSave.Size = New System.Drawing.Size(99, 29)
        Me.objbuttonSave.TabIndex = 7
        Me.objbuttonSave.Text = "Save && Finish"
        Me.objbuttonSave.UseVisualStyleBackColor = False
        Me.objbuttonSave.Visible = False
        '
        'colhImage
        '
        Me.colhImage.HeaderText = ""
        Me.colhImage.Name = "colhImage"
        Me.colhImage.ReadOnly = True
        Me.colhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhImage.Width = 30
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.HeaderText = "Employee Code"
        Me.colhEmployeeCode.Name = "colhEmployeeCode"
        Me.colhEmployeeCode.ReadOnly = True
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhDeviceUserId
        '
        Me.colhDeviceUserId.HeaderText = "Device User ID"
        Me.colhDeviceUserId.Name = "colhDeviceUserId"
        Me.colhDeviceUserId.ReadOnly = True
        Me.colhDeviceUserId.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhImortedFor
        '
        Me.objcolhImortedFor.HeaderText = "objcolhImortedFor"
        Me.objcolhImortedFor.Name = "objcolhImortedFor"
        Me.objcolhImortedFor.ReadOnly = True
        Me.objcolhImortedFor.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'frmImportDeviceUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(618, 424)
        Me.Controls.Add(Me.PnlImportdata)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportDeviceUsers"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmImportDeviceUsers"
        Me.PnlImportdata.ResumeLayout(False)
        Me.wzImportData.ResumeLayout(False)
        Me.wpImportData.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.wpMapfield.ResumeLayout(False)
        Me.pnlControl.ResumeLayout(False)
        Me.wpFileSelection.ResumeLayout(False)
        Me.wpFileSelection.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlImportdata As System.Windows.Forms.Panel
    Friend WithEvents wzImportData As eZee.Common.eZeeWizard
    Friend WithEvents wpFileSelection As eZee.Common.eZeeWizardPage
    Friend WithEvents rdOtherfile As System.Windows.Forms.RadioButton
    Friend WithEvents rdPayrollexportFile As System.Windows.Forms.RadioButton
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents wpImportData As eZee.Common.eZeeWizardPage
    Friend WithEvents btnReplace As eZee.Common.eZeeLightButton
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents dgEmployeedata As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonSave As eZee.Common.eZeeLightButton
    Friend WithEvents wpMapfield As eZee.Common.eZeeWizardPage
    Friend WithEvents pnlControl As System.Windows.Forms.Panel
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents objLblEmpCode As System.Windows.Forms.Label
    Friend WithEvents cboDeviceUserId As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeviceUserId As System.Windows.Forms.Label
    Friend WithEvents lblMapfield As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDeviceUserId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhImortedFor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
