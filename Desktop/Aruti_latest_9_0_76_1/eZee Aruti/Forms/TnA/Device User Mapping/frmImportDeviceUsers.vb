﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmImportDeviceUsers

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImportDeviceUsers"
    Private objExport As clsExportData
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim mdtOther As DataTable = Nothing
    Dim mdsPayroll As DataSet = Nothing
    Dim mdtGridData As DataTable = Nothing
    Dim dvGriddata As DataView = Nothing
    Dim m_Strarray(1, 1) As String
    Dim mintEmpcodeIdx As Integer = 0
    Dim mintDeviceIDIdx As Integer = 0

    Dim imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Dim imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setStatus(ByVal dtPRow As DataRow, ByVal blnResult As Boolean, ByVal isReplace As Boolean)
        Try
            If blnResult Then
                If isReplace Then
                    dtPRow.Item("Message") = Language.getMessage(mstrModuleName, 2, "Replaced")
                    dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Success")
                Else
                    dtPRow.Item("Message") = ""
                    dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Success")
                End If
                dtPRow.Item("image") = imgAccept
                dtPRow.Item("objStatus") = 1
                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
            Else
                dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                dtPRow.Item("image") = imgError
                dtPRow.Item("objStatus") = 2
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setStatus", mstrModuleName)
        End Try
    End Sub

    Private Sub ImportData(ByVal isReplace As Boolean)
        objWarning.Text = "0"
        objError.Text = "0"
        objSuccess.Text = "0"

        wzImportData.CancelEnabled = False
        btnFilter.Enabled = False
        btnReplace.Enabled = False

        ezWait.Active = True

        Try

            Dim drPRow As DataRow()

            If isReplace = False Then
                drPRow = mdtGridData.Select("objImportedFor = 1")
            Else
                drPRow = mdtGridData.Select("objStatus = 0")
            End If
            If drPRow.Length > 0 Then
                ImportUserData(drPRow, isReplace)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportData", mstrModuleName)
        Finally
            ezWait.Active = False
            wzImportData.CancelEnabled = True
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub FillGridData()
        Try

            mdtGridData = New DataTable("GridData")

            mdtGridData.Columns.Add("image", System.Type.GetType("System.Object"))
            mdtGridData.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("employeename", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("deviceuserid", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("Message", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("Status", System.Type.GetType("System.String"))
            mdtGridData.Columns.Add("objStatus", System.Type.GetType("System.Int32"))
            mdtGridData.Columns.Add("objImportedFor", System.Type.GetType("System.Int32"))


            If rdPayrollexportFile.Checked Then
                'mdsPayroll.Tables(0).Columns("Logindate").ColumnName = "date"
            End If

            For tb As Integer = 0 To mdsPayroll.Tables.Count - 1

                For i As Integer = 0 To mdsPayroll.Tables(tb).Rows.Count - 1

                    Dim drRow As DataRow
                    drRow = mdtGridData.NewRow()

                    Dim mdtdate As String = ""

                    drRow("image") = New Drawing.Bitmap(1, 1)
                    drRow.Item("objImportedFor") = 1

                    drRow("employeecode") = CStr(mdsPayroll.Tables(tb).Rows(i)("employeecode")).ToString
                    drRow("employeename") = CStr(mdsPayroll.Tables(tb).Rows(i)("employeename")).ToString
                    drRow("deviceuserid") = CStr(mdsPayroll.Tables(tb).Rows(i)("deviceuserid")).ToString

                    drRow("Message") = ""
                    drRow("Status") = ""
                    drRow("objStatus") = 2

                    mdtGridData.Rows.Add(drRow)
                    objTotal.Text = CStr(Val(objTotal.Text) + 1)
                Next

            Next

            dgEmployeedata.AutoGenerateColumns = False

            'VISIBLE COLUMN
            colhImage.DataPropertyName = "image"
            colhEmployeeCode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employeename"
            colhDeviceUserId.DataPropertyName = "deviceuserid"
            colhMessage.DataPropertyName = "Message"
            colhStatus.DataPropertyName = "Status"


            'INVISIBLE COLUMN
            objcolhstatus.DataPropertyName = "objStatus"

            dvGriddata = New DataView(mdtGridData)
            dgEmployeedata.DataSource = dvGriddata

            ImportData(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridData", mstrModuleName)
        End Try
    End Sub

    Private Sub ImportUserData(ByVal drRow As DataRow(), ByVal isReplace As Boolean)
        Dim blnResult As Boolean = True
        Dim objDeviceUser As New clsEmpid_devicemapping
        Dim intError As Integer = 0
        Dim intOldEmpID As Integer = 0
        Dim strOldDeviceUserID As String = ""

        Try




            For Each dRow As DataRow In drRow
                intError = 0

                blnResult = True

                Dim objEmployee As New clsEmployee_Master

                If dRow.Item("employeecode").ToString.Trim = "" Then
                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 15, "Employee code not set.")
                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                    dRow.Item("image") = imgError
                    dRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                If objEmployee.isExist(dRow.Item("employeecode").ToString) = False Then
                    dRow.Item("Message") = Language.getMessage(mstrModuleName, 7, "Employee code does not exist.")
                    dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                    dRow.Item("image") = imgError
                    dRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                Else
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = objExport.getEmployeeID(dRow.Item("employeecode").ToString)
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objExport.getEmployeeID(dRow.Item("employeecode").ToString)
                    'S.SANDEEP [04 JUN 2015] -- END

                    dRow("employeename") = objEmployee._Firstname & " " & objEmployee._Surname
                End If

                intOldEmpID = objDeviceUser.GetEmployeeUnkID(dRow.Item("deviceuserid").ToString)
                If intOldEmpID > 0 Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objEmployee._Employeeunkid = intOldEmpID Then
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intOldEmpID Then
                        'S.SANDEEP [04 JUN 2015] -- END

                        dRow.Item("Message") = Language.getMessage(mstrModuleName, 9, "Already Exists")
                        dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                        dRow.Item("image") = imgError
                        dRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        If isReplace Then

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If Not objDeviceUser.Void(objEmployee._Employeeunkid) Then
                            If Not objDeviceUser.Void(objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))) Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                blnResult = False
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                                dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                dRow.Item("image") = imgError
                                dRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            Dim objTmpEmp As New clsEmployee_Master

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'objTmpEmp._Employeeunkid = intOldEmpID
                            objTmpEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intOldEmpID
                            'S.SANDEEP [04 JUN 2015] -- END

                            dRow.Item("Message") = Language.getMessage(mstrModuleName, 11, "This Device User ID is already mapped with") & " " & objTmpEmp._Firstname & " " & objTmpEmp._Surname
                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                            dRow.Item("image") = imgWarring
                            dRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            objTmpEmp = Nothing
                            Continue For
                        End If

                    End If
                Else

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'strOldDeviceUserID = objDeviceUser.GetDeviceUserId(objEmployee._Employeeunkid)
                    strOldDeviceUserID = objDeviceUser.GetDeviceUserId(objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)))
                    'S.SANDEEP [04 JUN 2015] -- END

                    If strOldDeviceUserID.Trim <> "" AndAlso strOldDeviceUserID.Trim <> dRow.Item("deviceuserid").ToString.Trim Then
                        If isReplace Then

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If Not objDeviceUser.Void(objEmployee._Employeeunkid) Then
                            If Not objDeviceUser.Void(objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))) Then
                                'S.SANDEEP [04 JUN 2015] -- END
                                blnResult = False
                                dRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                                dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                                dRow.Item("image") = imgError
                                dRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dRow.Item("Message") = dRow("employeename").ToString & " " & Language.getMessage(mstrModuleName, 12, "is already mapped with Device User ID:") & " " & strOldDeviceUserID
                            dRow.Item("Status") = Language.getMessage(mstrModuleName, 5, "Fail")
                            dRow.Item("image") = imgWarring
                            dRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDeviceUser._Employeeunkid = objEmployee._Employeeunkid
                objDeviceUser._Employeeunkid = objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'S.SANDEEP [04 JUN 2015] -- END

                objDeviceUser._Deviceuserid = dRow.Item("deviceuserid").ToString.Trim
                objDeviceUser._Userunkid = User._Object._Userunkid
                objDeviceUser._Isactive = True
                If objDeviceUser.Insert() = False Then
                    blnResult = False
                    eZeeMsgBox.Show(objDeviceUser._Message, enMsgBoxStyle.Information)
                    Exit For
                End If



                setStatus(dRow, blnResult, isReplace)

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportData", mstrModuleName)
        End Try
    End Sub

    Private Sub ReadFileToArray(ByVal strfilename As String)
        Dim num_rows As Long
        Dim num_cols As Long

        'Check if file exist
        If IO.File.Exists(strfilename) Then
            Dim tmpstream As IO.StreamReader = IO.File.OpenText(strfilename)
            Dim strlines() As String
            Dim strline() As String

            'Load content of file to strLines array
            strlines = tmpstream.ReadToEnd().Split(CChar(Environment.NewLine))

            ' Redimension the array.
            num_rows = UBound(strlines)

            If strlines(0).Split(CChar(",")).GetValue(0).ToString <> "" Then
                strline = strlines(0).Split(CChar(","))
            Else
                strline = strlines(1).Split(CChar(","))
            End If
            num_cols = UBound(strline)
            ReDim m_Strarray(CInt(num_rows), CInt(num_cols))

            ' Copy the data into the array.
            For x As Long = 0 To num_rows
                If strlines(CInt(x)).Split(CChar(",")).GetValue(0).ToString <> "" Then
                    strline = strlines(CInt(x)).Split(CChar(","))
                    If strline.Length - 1 <> num_cols Then Continue For
                    For y As Long = 0 To CInt(num_cols)
                        m_Strarray(CInt(x), CInt(y)) = strline(CInt(y)).Trim(CChar("""")).Trim
                    Next
                End If
            Next

        End If

    End Sub

    Private Sub CreateOtherFileData()
        Try

            Dim dtDeviceUser As DataTable = New DataTable("dtDeviceUser")

            dtDeviceUser.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtDeviceUser.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtDeviceUser.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtDeviceUser.Columns.Add("deviceuserid", System.Type.GetType("System.String"))
            dtDeviceUser.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtDeviceUser.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtDeviceUser.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtDeviceUser.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtDeviceUser.Columns.Add("voidreason", System.Type.GetType("System.String"))

            Dim GetBound As Integer = 0

            For i As Integer = 0 To m_Strarray.GetUpperBound(0)

                Dim drRow As DataRow = dtDeviceUser.NewRow

                drRow("employeeunkid") = -1

                If m_Strarray(i, mintEmpcodeIdx) = Nothing Then Continue For
                drRow("employeecode") = m_Strarray(i, mintEmpcodeIdx)

                drRow("employeename") = ""

                If m_Strarray(i, mintDeviceIDIdx) = Nothing Then Continue For
                drRow("deviceuserid") = m_Strarray(i, mintDeviceIDIdx)

                drRow("isvoid") = False
                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                drRow("voidreason") = ""
                dtDeviceUser.Rows.Add(drRow)

            Next
            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtDeviceUser)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateData_Other", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataForExcel()
        Try
            Dim dtTimsheet As DataTable = New DataTable("Timesheet")

            dtTimsheet.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("deviceuserid", System.Type.GetType("System.String"))
            dtTimsheet.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtTimsheet.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtTimsheet.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtTimsheet.Columns.Add("voidreason", System.Type.GetType("System.String"))

            For i As Integer = 0 To mdtOther.Rows.Count - 1

                If IsDBNull(mdtOther.Rows(i)(cboEmployeeCode.Text)) = True OrElse mdtOther.Rows(i)(cboEmployeeCode.Text).ToString.Trim = "" Then Continue For

                Dim drRow As DataRow = dtTimsheet.NewRow

                drRow("employeeunkid") = -1

                drRow("employeecode") = mdtOther.Rows(i)(cboEmployeeCode.Text)
                drRow("employeename") = ""

                If IsDBNull(mdtOther.Rows(i)(cboDeviceUserId.Text)) = True Then
                    drRow("deviceuserid") = ""
                Else
                    drRow("deviceuserid") = mdtOther.Rows(i)(cboDeviceUserId.Text)
                End If

                drRow("isvoid") = False

                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                drRow("voidreason") = ""
                dtTimsheet.Rows.Add(drRow)

            Next

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtTimsheet)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForExcel", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportDeviceUsers_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            If mdtGridData IsNot Nothing AndAlso mdtGridData.Select("objStatus = 1").Length > 0 Then
                mblnCancel = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportDeviceUsers_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportDeviceUsers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            objExport = New clsExportData

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportDeviceUsers_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileDialog As New OpenFileDialog

            If rdPayrollexportFile.Checked Then
                objFileDialog.Filter = "XML files (*.xml)|*.xml"

            ElseIf rdOtherfile.Checked Then
                'objFileDialog.Filter = "XML files (*.xml)|*.xml|CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|Execl files(*.xlsx)|*.xlsx"|Excel files(*.xlsx)|*.xlsx|All files (.*)|*.*"
                objFileDialog.Filter = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|Execl files(*.xlsx)|*.xlsx"
            End If

            objFileDialog.FilterIndex = 0


            If objFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileDialog.FileName
            End If
            objFileDialog = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReplace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReplace.Click
        Try
            ImportData(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReplace_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ContextMenu's Event "

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            btnReplace.Enabled = False
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            btnReplace.Enabled = False
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            btnReplace.Enabled = True
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            btnReplace.Enabled = False
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Wizard's Event "

    Private Sub wzImportData_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzImportData.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wzImportData.Pages.IndexOf(wpFileSelection)

                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If


                    If rdPayrollexportFile.Checked Then


                    ElseIf rdOtherfile.Checked Then

                        Dim ImpFile As New IO.FileInfo(txtFilePath.Text)

                        If ImpFile.Extension.ToLower = ".txt" OrElse ImpFile.Extension.ToLower = ".csv" Then
                            ReadFileToArray(txtFilePath.Text)

                            If m_Strarray.Length <> 0 Then

                                cboEmployeeCode.Items.Clear()
                                cboDeviceUserId.Items.Clear()


                                For j As Integer = 0 To m_Strarray.GetUpperBound(1)
                                    Dim key As String = Nothing
                                    If m_Strarray(0, j) Is Nothing Then
                                        key = m_Strarray(1, j)
                                    Else
                                        key = m_Strarray(0, j)
                                    End If

                                    cboEmployeeCode.Items.Add(key)
                                    cboDeviceUserId.Items.Add(key)


                                Next
                            Else
                                e.Cancel = True
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Exit Sub
                            End If


                        ElseIf ImpFile.Extension.ToLower = ".xls" OrElse ImpFile.Extension.ToLower = ".xlsx" Then
                            'S.SANDEEP [12-Jan-2018] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # 0001843
                            'Dim iExcelData As New ExcelData
                            'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                            'mdtOther = iExcelData.Import(txtFilePath.Text).Tables(0).Copy

                            Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                            mdtOther = OpenXML_Import(txtFilePath.Text).Tables(0).Copy
                            'S.SANDEEP [12-Jan-2018] -- END

                            cboEmployeeCode.Items.Clear()
                            cboDeviceUserId.Items.Clear()

                            For Each dtColumns As DataColumn In mdtOther.Columns
                                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                                cboDeviceUserId.Items.Add(dtColumns.ColumnName)
                            Next

                        ElseIf ImpFile.Extension.ToLower = ".xml" Then

                            Dim ds As New DataSet
                            ds.ReadXml(txtFilePath.Text)
                            If ds IsNot Nothing Then
                                mdtOther = ds.Tables(0)


                                cboEmployeeCode.Items.Clear()
                                cboDeviceUserId.Items.Clear()

                                For Each dtColumns As DataColumn In mdtOther.Columns
                                    cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                                    cboDeviceUserId.Items.Add(dtColumns.ColumnName)
                                Next

                            End If

                        Else
                            e.Cancel = True
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Sub
                        End If

                    End If


                Case wzImportData.Pages.IndexOf(wpMapfield)

                    If cboEmployeeCode.SelectedIndex < 0 Then
                        wzImportData.SelectedPage = wpMapfield
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Employee Code is compulsory field to map.Please map this field."))
                        cboEmployeeCode.Select()
                        e.Cancel = True
                        Exit Sub
                    ElseIf cboDeviceUserId.SelectedIndex < 0 Then
                        wzImportData.SelectedPage = wpMapfield
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Device User ID is compulsory field to map.Please map this field."))
                        cboDeviceUserId.Select()
                        e.Cancel = True
                        Exit Sub
                    End If

                    mintEmpcodeIdx = cboEmployeeCode.SelectedIndex
                    mintDeviceIDIdx = cboDeviceUserId.SelectedIndex

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub wzImportData_AfterSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles wzImportData.AfterSwitchPages
        Try

            Select Case e.NewIndex
                Case wzImportData.Pages.IndexOf(wpMapfield)
                    If rdPayrollexportFile.Checked Then
                        'wzImportData.SelectedPage = wpImportData
                        'FillGridData()
                    End If

                Case wzImportData.Pages.IndexOf(wpImportData)

                    wzImportData.CancelText = Language.getMessage(mstrModuleName, 13, "Finish")

                    If rdPayrollexportFile.Checked Then
                        'FillGridData()
                    ElseIf rdOtherfile.Checked Then
                        Dim ImpFile As New IO.FileInfo(txtFilePath.Text)
                        If ImpFile.Extension.ToLower = ".csv" OrElse ImpFile.Extension.ToLower = ".txt" Then
                            CreateOtherFileData()
                        ElseIf ImpFile.Extension.ToLower = ".xls" OrElse ImpFile.Extension.ToLower = ".xlsx" OrElse ImpFile.Extension.ToLower = ".xml" Then
                            CreateDataForExcel()
                        End If
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox Event "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeCode.SelectedIndexChanged, _
                                                                                                                                                                               cboDeviceUserId.SelectedIndexChanged
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then


                For Each cr As Control In pnlControl.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnReplace.GradientBackColor = GUI._ButttonBackColor
            Me.btnReplace.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonSave.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wzImportData.CancelText = Language._Object.getCaption(Me.wzImportData.Name & "_CancelText", Me.wzImportData.CancelText)
            Me.wzImportData.NextText = Language._Object.getCaption(Me.wzImportData.Name & "_NextText", Me.wzImportData.NextText)
            Me.wzImportData.BackText = Language._Object.getCaption(Me.wzImportData.Name & "_BackText", Me.wzImportData.BackText)
            Me.wzImportData.FinishText = Language._Object.getCaption(Me.wzImportData.Name & "_FinishText", Me.wzImportData.FinishText)
            Me.rdOtherfile.Text = Language._Object.getCaption(Me.rdOtherfile.Name, Me.rdOtherfile.Text)
            Me.rdPayrollexportFile.Text = Language._Object.getCaption(Me.rdPayrollexportFile.Name, Me.rdPayrollexportFile.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnReplace.Text = Language._Object.getCaption(Me.btnReplace.Name, Me.btnReplace.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.lblDeviceUserId.Text = Language._Object.getCaption(Me.lblDeviceUserId.Name, Me.lblDeviceUserId.Text)
            Me.lblMapfield.Text = Language._Object.getCaption(Me.lblMapfield.Name, Me.lblMapfield.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeeCode.HeaderText = Language._Object.getCaption(Me.colhEmployeeCode.Name, Me.colhEmployeeCode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhDeviceUserId.HeaderText = Language._Object.getCaption(Me.colhDeviceUserId.Name, Me.colhDeviceUserId.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please the select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Replaced")
            Language.setMessage(mstrModuleName, 3, "Success")
            Language.setMessage(mstrModuleName, 4, "Error!")
            Language.setMessage(mstrModuleName, 5, "Fail")
            Language.setMessage(mstrModuleName, 7, "Employee code does not exist.")
            Language.setMessage(mstrModuleName, 8, "Employee Code is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 9, "Already Exists")
            Language.setMessage(mstrModuleName, 10, "Device User ID is compulsory field to map.Please map this field.")
            Language.setMessage(mstrModuleName, 11, "This Device User ID is already mapped with")
            Language.setMessage(mstrModuleName, 12, "is already mapped with Device User ID:")
            Language.setMessage(mstrModuleName, 13, "Finish")
            Language.setMessage(mstrModuleName, 14, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 15, "Employee code not set.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class