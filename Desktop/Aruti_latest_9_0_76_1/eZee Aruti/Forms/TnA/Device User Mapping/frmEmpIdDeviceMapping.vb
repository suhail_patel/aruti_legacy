﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmpIdDeviceMapping

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmEmpIdDeviceMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_CONTINUE

    Private mstrAdvanceFilter As String = ""

    Private objEmpIdDeviceMapping As clsEmpid_devicemapping
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region " Private Method & Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("List", True, True, , , , , , , , , , , , , , , , , , , False)
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillGridView()
        Dim dsList As DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            If User._Object.Privilege._AllowToViewDeviceUserMapping = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmpIdDeviceMapping.GetList("List", True, CInt(cboEmployee.SelectedValue), , mstrAdvanceFilter)
            dsList = objEmpIdDeviceMapping.GetList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, False, _
                                                   chkIncludeInactiveEmp.Checked, "List", _
                                                   True, CInt(cboEmployee.SelectedValue), , mstrAdvanceFilter)
            'S.SANDEEP [04 JUN 2015] -- END


            dgcolhUnkId.DataPropertyName = "devicemappingunkid"
            dgcolhEmpId.DataPropertyName = "employeeunkid"
            dgcolhEmpCode.DataPropertyName = "employeecode"
            dgcolhEmpName.DataPropertyName = "employeename"
            dgcolhDept.DataPropertyName = "deptname"
            dgcolhDeviceUserId.DataPropertyName = "deviceuserid"

            With dgvEmployee
                .AutoGenerateColumns = False
                .DataSource = dsList.Tables("List")
                .Refresh()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridView", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEmpIdDeviceMapping._Isactive = True
            objEmpIdDeviceMapping._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToMapDeviceUser
            btnImport.Enabled = User._Object.Privilege._AllowToImportDeviceUser
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssignFingerPrintId_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpIdDeviceMapping = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignFingerPrintId_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignFingerPrintId_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignFingerPrintId_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignFingerPrintId_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpid_DeviceMapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignFingerPrintId_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmAssignFingerPrintId_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpIdDeviceMapping = New clsEmpid_devicemapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            
            Call OtherSettings()

            Call FillCombo()

            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignFingerPrintId_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CType(dgvEmployee.DataSource, DataTable).Select("AUD = 'U'").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please update ID for atleast one employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            Dim dtTable As DataTable
            Dim dRow() As DataRow = CType(dgvEmployee.DataSource, DataTable).Select("AUD = 'U'")
            dtTable = dRow.CopyToDataTable
            If objEmpIdDeviceMapping.InsertUpdate(dtTable) = False Then
                Exit Sub
            End If

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "IDs updated successfully."), enMsgBoxStyle.Information)

            mblnCancel = False
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            Dim objFrm As New frmImportDeviceUsers
            If objFrm.displayDialog(enAction.ADD_CONTINUE) = True Then
                Call FillGridView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Datagridview Events "

    Private Sub dgvEmployee_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvEmployee.CellValidating
        Try
            If e.ColumnIndex = dgcolhDeviceUserId.Index Then
                'Pinkal (28-Oct-2015) -- Start
                'Enhancement - WORKING ON MSBR CHANGES ON DEVICE USER MAPPING.
                If IsDBNull(e.FormattedValue) OrElse e.FormattedValue.ToString() = "" Then Exit Sub
                'Pinkal (28-Oct-2015) -- End

                Dim intEmpID As Integer = objEmpIdDeviceMapping.GetEmployeeUnkID(e.FormattedValue.ToString)
                If intEmpID > 0 Then
                    If dgvEmployee.CurrentRow.Cells(dgcolhEmpId.Index).Value.ToString <> intEmpID.ToString Then
                        Dim objEmp As New clsEmployee_Master

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = intEmpID
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
                        'S.SANDEEP [04 JUN 2015] -- END

                        'Pinkal (20-Jan-2018) -- Start
                        'Bug - 0001880: Unable to map device codes for multiple staff at once.
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This ID is already mapped with employee") & " [" & objEmp._Employeecode & "] " & objEmp._Firstname.Trim() & " " & objEmp._Surname.Trim() & ".", enMsgBoxStyle.Information)
                        'Pinkal (20-Jan-2018) -- End
                        objEmp = Nothing
                        dgvEmployee.CancelEdit()
                        e.Cancel = True
                        Exit Sub
                    End If
                    'Pinkal (20-Jan-2018) -- Start
                    'Bug - 0001880: Unable to map device codes for multiple staff at once.
                Else
                    Dim dtTable As DataTable = CType(dgvEmployee.DataSource, DataTable)
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dgvEmployee.CurrentRow.Cells(dgcolhEmpId.Index).Value)
                    If dtTable.Select("employeecode <> '" & dgvEmployee.Rows(e.RowIndex).Cells(dgcolhEmpCode.Index).Value.ToString() & "' AND deviceuserid <> '' AND deviceuserid ='" & e.FormattedValue.ToString() & "'").Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This ID is already mapped with employee") & " [" & objEmp._Employeecode & "] " & objEmp._Firstname.Trim() & " " & objEmp._Surname.Trim() & ".", enMsgBoxStyle.Information)
                        objEmp = Nothing
                        dgvEmployee.CancelEdit()
                        e.Cancel = True
                        Exit Sub
                    End If
                    'Pinkal (20-Jan-2018) -- End
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmployee_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            'Pinkal (20-Jan-2018) -- Start
            'Bug - 0001880: Unable to map device codes for multiple staff at once.
            'dgvEmployee.UpdateCellValue(e.ColumnIndex, e.RowIndex)
            'CType(dgvEmployee.DataSource, DataTable).Rows(e.RowIndex)("AUD") = "U"
            Dim dtTable As DataTable = CType(dgvEmployee.DataSource, DataTable)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("employeecode = '" & dgvEmployee.CurrentRow.Cells(dgcolhEmpCode.Index).Value.ToString() & "'")
                If drRow.Length > 0 Then
                    If dtTable.Select("employeecode <> '" & dgvEmployee.CurrentRow.Cells(dgcolhEmpCode.Index).Value.ToString() & "' AND deviceuserid <> '' AND deviceuserid = '" & dgvEmployee.CurrentRow.Cells(dgcolhDeviceUserId.Index).Value.ToString() & "'").Length <= 0 Then
                        drRow(0)("AUD") = "U"
                    End If
                End If
            End If
            'Pinkal (20-Jan-2018) -- End
            CType(dgvEmployee.DataSource, DataTable).AcceptChanges()
            dgvEmployee.CurrentRow.Cells(dgcolhDeviceUserId.Index).Selected = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmployee_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEmployee.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGridView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            mstrAdvanceFilter = ""
            'Pinkal (28-Oct-2015) -- Start
            'Enhancement - WORKING ON MSBR CHANGES ON DEVICE USER MAPPING.
            chkIncludeInactiveEmp.Checked = False
            'Pinkal (28-Oct-2015) -- Enb
            Call FillGridView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.dgcolhUnkId.HeaderText = Language._Object.getCaption(Me.dgcolhUnkId.Name, Me.dgcolhUnkId.HeaderText)
			Me.dgcolhEmpId.HeaderText = Language._Object.getCaption(Me.dgcolhEmpId.Name, Me.dgcolhEmpId.HeaderText)
			Me.dgcolhEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmpCode.Name, Me.dgcolhEmpCode.HeaderText)
			Me.dgcolhEmpName.HeaderText = Language._Object.getCaption(Me.dgcolhEmpName.Name, Me.dgcolhEmpName.HeaderText)
			Me.dgcolhDept.HeaderText = Language._Object.getCaption(Me.dgcolhDept.Name, Me.dgcolhDept.HeaderText)
			Me.dgcolhDeviceUserId.HeaderText = Language._Object.getCaption(Me.dgcolhDeviceUserId.Name, Me.dgcolhDeviceUserId.HeaderText)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please update ID for atleast one employee.")
			Language.setMessage(mstrModuleName, 2, "IDs updated successfully.")
			Language.setMessage(mstrModuleName, 3, "Sorry, This ID is already mapped with employee")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class