﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalEditDelete_Timesheet
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalEditDelete_Timesheet))
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkOverWriteIfExist = New System.Windows.Forms.CheckBox
        Me.radAddTimesheet = New System.Windows.Forms.RadioButton
        Me.gbWorkDetail = New eZee.Common.eZeeLine
        Me.LblOuttime = New System.Windows.Forms.Label
        Me.LblIntime = New System.Windows.Forms.Label
        Me.dtpOutTime = New System.Windows.Forms.DateTimePicker
        Me.dtpInTime = New System.Windows.Forms.DateTimePicker
        Me.chkIncludeDayOff = New System.Windows.Forms.CheckBox
        Me.chkIncludeWeekend = New System.Windows.Forms.CheckBox
        Me.chkIncludeHoliday = New System.Windows.Forms.CheckBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.radDeleteTimesheet = New System.Windows.Forms.RadioButton
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.radEditTimesheet = New System.Windows.Forms.RadioButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPolicy = New eZee.Common.eZeeGradientButton
        Me.CboPolicy = New System.Windows.Forms.ComboBox
        Me.LblPolicy = New System.Windows.Forms.Label
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.LblShift = New System.Windows.Forms.Label
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFdate = New System.Windows.Forms.Label
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.chkOverWriteIfExist)
        Me.gbFilter.Controls.Add(Me.radAddTimesheet)
        Me.gbFilter.Controls.Add(Me.gbWorkDetail)
        Me.gbFilter.Controls.Add(Me.LblOuttime)
        Me.gbFilter.Controls.Add(Me.LblIntime)
        Me.gbFilter.Controls.Add(Me.dtpOutTime)
        Me.gbFilter.Controls.Add(Me.dtpInTime)
        Me.gbFilter.Controls.Add(Me.chkIncludeDayOff)
        Me.gbFilter.Controls.Add(Me.chkIncludeWeekend)
        Me.gbFilter.Controls.Add(Me.chkIncludeHoliday)
        Me.gbFilter.Controls.Add(Me.Label1)
        Me.gbFilter.Controls.Add(Me.radDeleteTimesheet)
        Me.gbFilter.Controls.Add(Me.objLine1)
        Me.gbFilter.Controls.Add(Me.radEditTimesheet)
        Me.gbFilter.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilter.Controls.Add(Me.objbtnSearchPolicy)
        Me.gbFilter.Controls.Add(Me.CboPolicy)
        Me.gbFilter.Controls.Add(Me.LblPolicy)
        Me.gbFilter.Controls.Add(Me.objbtnSearchShift)
        Me.gbFilter.Controls.Add(Me.cboShift)
        Me.gbFilter.Controls.Add(Me.LblShift)
        Me.gbFilter.Controls.Add(Me.LblEmployee)
        Me.gbFilter.Controls.Add(Me.lnkAllocation)
        Me.gbFilter.Controls.Add(Me.cboEmployee)
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.dtpToDate)
        Me.gbFilter.Controls.Add(Me.lblToDate)
        Me.gbFilter.Controls.Add(Me.dtpFromDate)
        Me.gbFilter.Controls.Add(Me.lblFdate)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(0, 1)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(550, 198)
        Me.gbFilter.TabIndex = 0
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOverWriteIfExist
        '
        Me.chkOverWriteIfExist.BackColor = System.Drawing.Color.Transparent
        Me.chkOverWriteIfExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverWriteIfExist.Location = New System.Drawing.Point(376, 34)
        Me.chkOverWriteIfExist.Name = "chkOverWriteIfExist"
        Me.chkOverWriteIfExist.Size = New System.Drawing.Size(134, 17)
        Me.chkOverWriteIfExist.TabIndex = 390
        Me.chkOverWriteIfExist.Text = "Overwrite If Exist"
        Me.chkOverWriteIfExist.UseVisualStyleBackColor = False
        '
        'radAddTimesheet
        '
        Me.radAddTimesheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radAddTimesheet.BackColor = System.Drawing.Color.Transparent
        Me.radAddTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAddTimesheet.Location = New System.Drawing.Point(101, 34)
        Me.radAddTimesheet.Name = "radAddTimesheet"
        Me.radAddTimesheet.Size = New System.Drawing.Size(124, 17)
        Me.radAddTimesheet.TabIndex = 388
        Me.radAddTimesheet.Text = "Add Timesheet"
        Me.radAddTimesheet.UseVisualStyleBackColor = False
        '
        'gbWorkDetail
        '
        Me.gbWorkDetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWorkDetail.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.gbWorkDetail.Location = New System.Drawing.Point(373, 121)
        Me.gbWorkDetail.Name = "gbWorkDetail"
        Me.gbWorkDetail.Size = New System.Drawing.Size(169, 14)
        Me.gbWorkDetail.TabIndex = 110
        Me.gbWorkDetail.Tag = ""
        Me.gbWorkDetail.Text = "Time Detail"
        '
        'LblOuttime
        '
        Me.LblOuttime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblOuttime.Location = New System.Drawing.Point(374, 173)
        Me.LblOuttime.Name = "LblOuttime"
        Me.LblOuttime.Size = New System.Drawing.Size(59, 17)
        Me.LblOuttime.TabIndex = 386
        Me.LblOuttime.Text = "Out Time"
        Me.LblOuttime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblIntime
        '
        Me.LblIntime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIntime.Location = New System.Drawing.Point(374, 145)
        Me.LblIntime.Name = "LblIntime"
        Me.LblIntime.Size = New System.Drawing.Size(59, 17)
        Me.LblIntime.TabIndex = 385
        Me.LblIntime.Text = "In Time"
        Me.LblIntime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpOutTime
        '
        Me.dtpOutTime.Checked = False
        Me.dtpOutTime.CustomFormat = "hh:mm tt"
        Me.dtpOutTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpOutTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpOutTime.Location = New System.Drawing.Point(438, 171)
        Me.dtpOutTime.Name = "dtpOutTime"
        Me.dtpOutTime.ShowCheckBox = True
        Me.dtpOutTime.Size = New System.Drawing.Size(100, 21)
        Me.dtpOutTime.TabIndex = 384
        '
        'dtpInTime
        '
        Me.dtpInTime.Checked = False
        Me.dtpInTime.CustomFormat = "hh:mm tt"
        Me.dtpInTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpInTime.Location = New System.Drawing.Point(438, 143)
        Me.dtpInTime.Name = "dtpInTime"
        Me.dtpInTime.ShowCheckBox = True
        Me.dtpInTime.Size = New System.Drawing.Size(100, 21)
        Me.dtpInTime.TabIndex = 383
        '
        'chkIncludeDayOff
        '
        Me.chkIncludeDayOff.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeDayOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeDayOff.Location = New System.Drawing.Point(376, 99)
        Me.chkIncludeDayOff.Name = "chkIncludeDayOff"
        Me.chkIncludeDayOff.Size = New System.Drawing.Size(167, 17)
        Me.chkIncludeDayOff.TabIndex = 14
        Me.chkIncludeDayOff.Text = "Include Day Off"
        Me.chkIncludeDayOff.UseVisualStyleBackColor = False
        '
        'chkIncludeWeekend
        '
        Me.chkIncludeWeekend.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeWeekend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeWeekend.Location = New System.Drawing.Point(376, 78)
        Me.chkIncludeWeekend.Name = "chkIncludeWeekend"
        Me.chkIncludeWeekend.Size = New System.Drawing.Size(167, 17)
        Me.chkIncludeWeekend.TabIndex = 13
        Me.chkIncludeWeekend.Text = "Include Weekend"
        Me.chkIncludeWeekend.UseVisualStyleBackColor = False
        '
        'chkIncludeHoliday
        '
        Me.chkIncludeHoliday.BackColor = System.Drawing.Color.Transparent
        Me.chkIncludeHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeHoliday.Location = New System.Drawing.Point(376, 56)
        Me.chkIncludeHoliday.Name = "chkIncludeHoliday"
        Me.chkIncludeHoliday.Size = New System.Drawing.Size(167, 17)
        Me.chkIncludeHoliday.TabIndex = 12
        Me.chkIncludeHoliday.Text = "Include Holiday"
        Me.chkIncludeHoliday.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 17)
        Me.Label1.TabIndex = 155
        Me.Label1.Text = "Operation Type"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDeleteTimesheet
        '
        Me.radDeleteTimesheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radDeleteTimesheet.BackColor = System.Drawing.Color.Transparent
        Me.radDeleteTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDeleteTimesheet.Location = New System.Drawing.Point(232, 57)
        Me.radDeleteTimesheet.Name = "radDeleteTimesheet"
        Me.radDeleteTimesheet.Size = New System.Drawing.Size(127, 17)
        Me.radDeleteTimesheet.TabIndex = 380
        Me.radDeleteTimesheet.Text = "Delete Timesheet"
        Me.radDeleteTimesheet.UseVisualStyleBackColor = False
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(363, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(8, 169)
        Me.objLine1.TabIndex = 154
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'radEditTimesheet
        '
        Me.radEditTimesheet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radEditTimesheet.BackColor = System.Drawing.Color.Transparent
        Me.radEditTimesheet.Checked = True
        Me.radEditTimesheet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEditTimesheet.Location = New System.Drawing.Point(101, 57)
        Me.radEditTimesheet.Name = "radEditTimesheet"
        Me.radEditTimesheet.Size = New System.Drawing.Size(124, 17)
        Me.radEditTimesheet.TabIndex = 379
        Me.radEditTimesheet.TabStop = True
        Me.radEditTimesheet.Text = "Edit Timesheet"
        Me.radEditTimesheet.UseVisualStyleBackColor = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(338, 107)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 148
        '
        'objbtnSearchPolicy
        '
        Me.objbtnSearchPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPolicy.BorderSelected = False
        Me.objbtnSearchPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPolicy.Location = New System.Drawing.Point(338, 161)
        Me.objbtnSearchPolicy.Name = "objbtnSearchPolicy"
        Me.objbtnSearchPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPolicy.TabIndex = 153
        '
        'CboPolicy
        '
        Me.CboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboPolicy.DropDownWidth = 350
        Me.CboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboPolicy.FormattingEnabled = True
        Me.CboPolicy.Location = New System.Drawing.Point(101, 161)
        Me.CboPolicy.Name = "CboPolicy"
        Me.CboPolicy.Size = New System.Drawing.Size(233, 21)
        Me.CboPolicy.TabIndex = 151
        '
        'LblPolicy
        '
        Me.LblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPolicy.Location = New System.Drawing.Point(8, 163)
        Me.LblPolicy.Name = "LblPolicy"
        Me.LblPolicy.Size = New System.Drawing.Size(88, 17)
        Me.LblPolicy.TabIndex = 152
        Me.LblPolicy.Text = "Policy"
        Me.LblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(338, 134)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 150
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 350
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(101, 134)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(233, 21)
        Me.cboShift.TabIndex = 148
        '
        'LblShift
        '
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(8, 136)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(88, 17)
        Me.LblShift.TabIndex = 149
        Me.LblShift.Text = "Shift"
        Me.LblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(8, 109)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(88, 17)
        Me.LblEmployee.TabIndex = 146
        Me.LblEmployee.Text = "Employee"
        Me.LblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(402, 3)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(92, 17)
        Me.lnkAllocation.TabIndex = 144
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(101, 107)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(233, 21)
        Me.cboEmployee.TabIndex = 147
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(525, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 143
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(499, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 142
        Me.objbtnSearch.TabStop = False
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(268, 80)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpToDate.TabIndex = 33
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(197, 82)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(65, 17)
        Me.lblToDate.TabIndex = 34
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(101, 80)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpFromDate.TabIndex = 31
        '
        'lblFdate
        '
        Me.lblFdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFdate.Location = New System.Drawing.Point(8, 82)
        Me.lblFdate.Name = "lblFdate"
        Me.lblFdate.Size = New System.Drawing.Size(88, 17)
        Me.lblFdate.TabIndex = 32
        Me.lblFdate.Text = "From Date"
        Me.lblFdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearch, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel1, 0, 1)
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(2, 203)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(548, 243)
        Me.tblpAssessorEmployee.TabIndex = 108
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(542, 21)
        Me.txtSearch.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkEmployee)
        Me.Panel1.Controls.Add(Me.dgvData)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(542, 211)
        Me.Panel1.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvData.ColumnHeadersHeight = 21
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(542, 211)
        Me.dgvData.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 452)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(552, 55)
        Me.objFooter.TabIndex = 109
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(340, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(443, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmGlobalEditDelete_Timesheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(552, 507)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.tblpAssessorEmployee)
        Me.Controls.Add(Me.gbFilter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalEditDelete_Timesheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Timesheet"
        Me.gbFilter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFdate As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents LblShift As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents CboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents LblPolicy As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radDeleteTimesheet As System.Windows.Forms.RadioButton
    Friend WithEvents radEditTimesheet As System.Windows.Forms.RadioButton
    Friend WithEvents chkIncludeDayOff As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeWeekend As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncludeHoliday As System.Windows.Forms.CheckBox
    Friend WithEvents dtpOutTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpInTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblOuttime As System.Windows.Forms.Label
    Friend WithEvents LblIntime As System.Windows.Forms.Label
    Friend WithEvents gbWorkDetail As eZee.Common.eZeeLine
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radAddTimesheet As System.Windows.Forms.RadioButton
    Friend WithEvents chkOverWriteIfExist As System.Windows.Forms.CheckBox
End Class
