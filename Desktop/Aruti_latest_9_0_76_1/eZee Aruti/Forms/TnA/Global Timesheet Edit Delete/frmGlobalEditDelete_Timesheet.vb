﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalEditDelete_Timesheet

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalEditDelete_Timesheet"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dgView As DataView
    Private mdtEmployee As DataTable = Nothing
    Private mstrAllocationFilter As String = String.Empty
    Private objLogin As clslogin_Tran

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim ObjEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , )
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , )
            'End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                        True, False, "Emp", True)


            'Pinkal (06-Jan-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0


            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("Shift", True)
            cboShift.DisplayMember = "name"
            cboShift.ValueMember = "shiftunkid"
            cboShift.DataSource = dsList.Tables(0)
            cboShift.SelectedValue = 0

            Dim objPolicy As New clspolicy_master
            dsList = objPolicy.getListForCombo("Shift", True)
            CboPolicy.DisplayMember = "name"
            CboPolicy.ValueMember = "policyunkid"
            CboPolicy.DataSource = dsList.Tables(0)
            CboPolicy.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Employee_List()
        Try
            Dim ObjEmp As New clsEmployee_Master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'Dim dsEmployee As DataSet = ObjEmp.GetEmployeeShift_PolicyList(dtpFromDate.Value.Date, dtpToDate.Value.Date, CInt(cboEmployee.SelectedValue), _
            '                                                                                                 CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue), mstrAllocationFilter)


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'Dim dsEmployee As DataSet = ObjEmp.GetEmployeeShift_PolicyList(FinancialYear._Object._DatabaseName, _
            '                                                               User._Object._Userunkid, _
            '                                                               FinancialYear._Object._YearUnkid, _
            '                                                               Company._Object._Companyunkid, _
            '                                                               ConfigParameter._Object._UserAccessModeSetting, _
            '                                                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
            '                                                               dtpFromDate.Value.Date, _
            '                                                               dtpToDate.Value.Date, _
            '                                                               ConfigParameter._Object._PolicyManagementTNA, _
            '                                                               CInt(cboEmployee.SelectedValue), _
            '                                                               CInt(cboShift.SelectedValue), _
            '                                                               CInt(CboPolicy.SelectedValue), _
            '                                                               mstrAllocationFilter)

            Dim dsEmployee As DataSet = ObjEmp.GetEmployeeShift_PolicyList(FinancialYear._Object._DatabaseName, _
                                                                           User._Object._Userunkid, _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           Company._Object._Companyunkid, _
                                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                                           False, "List", _
                                                                           dtpFromDate.Value.Date, _
                                                                           dtpToDate.Value.Date, _
                                                                           ConfigParameter._Object._PolicyManagementTNA, _
                                                                           CInt(cboEmployee.SelectedValue), _
                                                                           CInt(cboShift.SelectedValue), _
                                                                           CInt(CboPolicy.SelectedValue), _
                                                                           mstrAllocationFilter)

            'Pinkal (06-Jan-2016) -- End

            'Shani(24-Aug-2015) -- End

            If dsEmployee.Tables(0).Columns.Contains("ischeck") = False Then
                dsEmployee.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            End If

            If dsEmployee.Tables(0).Columns.Contains("shiftunkid") Then
                dsEmployee.Tables(0).Columns.Remove("shiftunkid")
            End If

            If dsEmployee.Tables(0).Columns.Contains("shiftname") Then
                dsEmployee.Tables(0).Columns.Remove("shiftname")
            End If

            If dsEmployee.Tables(0).Columns.Contains("Shiftrowno") Then
                dsEmployee.Tables(0).Columns.Remove("Shiftrowno")
            End If

            If dsEmployee.Tables(0).Columns.Contains("ShitEffectivedate") Then
                dsEmployee.Tables(0).Columns.Remove("ShitEffectivedate")
            End If

            If dsEmployee.Tables(0).Columns.Contains("policyunkid") Then
                dsEmployee.Tables(0).Columns.Remove("policyunkid")
            End If

            If dsEmployee.Tables(0).Columns.Contains("policyname") Then
                dsEmployee.Tables(0).Columns.Remove("policyname")
            End If

            If dsEmployee.Tables(0).Columns.Contains("Policyrowno") Then
                dsEmployee.Tables(0).Columns.Remove("Policyrowno")
            End If

            If dsEmployee.Tables(0).Columns.Contains("PolicyEffectivedate") Then
                dsEmployee.Tables(0).Columns.Remove("PolicyEffectivedate")
            End If



            mdtEmployee = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "employeename", "ischeck")
            dgView = mdtEmployee.DefaultView

            dgvData.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvData.DataSource = dgView
            objchkEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Employee_List", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmGlobalEditDelete_Timesheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objLogin = New clslogin_Tran
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - CHANGING FOR VOLTAMP NET BF ISSUE WHEN CLOSED PERIOD.
            If ConfigParameter._Object._PolicyManagementTNA Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, True, enStatusType.Open)
                Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    'Sohail (21 Aug 2015) -- End
                    dtpFromDate.MinDate = objPeriod._TnA_StartDate.Date
                    'dtpFromDate.MaxDate = objPeriod._TnA_EndDate.Date
                    dtpToDate.MinDate = objPeriod._TnA_StartDate.Date
                    'dtpToDate.MaxDate = objPeriod._TnA_EndDate.Date
                End If
            Else
                dtpFromDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                dtpFromDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                dtpToDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                dtpToDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
            End If
            ''Pinkal (25-Feb-2015) -- End

            FillCombo()
            Fill_Employee_List()
            radAddTimesheet.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalEditDelete_Timesheet_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            Call SetMessages()
            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            SetLanguage()
            objfrm.displayDialog(Me)
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            radAddTimesheet.Checked = True



            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpFromDate.Value = FinancialYear._Object._Database_End_Date.Date
                dtpToDate.Value = FinancialYear._Object._Database_End_Date.Date
            Else
                dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            
            cboEmployee.SelectedValue = 0
            cboShift.SelectedValue = 0
            CboPolicy.SelectedValue = 0
            chkIncludeHoliday.Checked = False
            chkIncludeWeekend.Checked = False
            chkIncludeDayOff.Checked = False

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
            dtpInTime.Value = ConfigParameter._Object._CurrentDateAndTime
                dtpOutTime.Value = ConfigParameter._Object._CurrentDateAndTime
            Else
                dtpInTime.Value = FinancialYear._Object._Database_End_Date
                dtpOutTime.Value = FinancialYear._Object._Database_End_Date
            End If
            dtpInTime.Checked = False
            dtpOutTime.Checked = False
            txtSearch.Text = ""
            dgvData.DataSource = Nothing
            mstrAllocationFilter = ""
            chkOverWriteIfExist.Checked = False
            Fill_Employee_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Fill_Employee_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboShift.DataSource, DataTable)
            With cboShift
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPolicy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPolicy.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(CboPolicy.DataSource, DataTable)
            With CboPolicy
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPolicy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If dgView Is Nothing AndAlso dgView.ToTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no employee(s) to Edit/Delete timings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim drRow() As DataRow = dgView.ToTable.Select("ischeck=true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If radAddTimesheet.Checked OrElse radEditTimesheet.Checked Then
                If (dtpInTime.Checked AndAlso dtpOutTime.Checked) AndAlso (dtpInTime.Value > dtpOutTime.Value) Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Out Time is less than In time.So Out Time will be considered for next day.Are you sure you want to continue ? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
            End If

            If radAddTimesheet.Checked AndAlso dtpInTime.Checked = False AndAlso dtpOutTime.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Enter In time or Out time to Add timesheet for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            ElseIf radEditTimesheet.Checked AndAlso dtpInTime.Checked = False AndAlso dtpOutTime.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Enter In time or Out time to Edit timesheet for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim lstEmpoyeeId As List(Of String) = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
            Dim mstrEmployeeIDs As String = String.Join(",", lstEmpoyeeId.ToArray)

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMstdata As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpFromDate.Value.Date, 0, 0, True)
                Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpFromDate.Value.Date)
                'Pinkal (20-Jan-2014) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPeriod
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't Add/Edit/Delete employee(s) timings .Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                If intPeriod > 0 Then

                    'Pinkal (29-Jul-2016) -- Start
                    'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.

                    If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then

                        Dim objPaymentTran As New clsPayment_tran
                        Dim intPeriodId As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpFromDate.Value.Date)

                        Dim mdtTnAStartdate As Date = Nothing
                        If intPeriodId > 0 Then
                            Dim objTnAPeriod As New clscommom_period_Tran
                            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        End If

                        If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, dtpFromDate.Value.Date) > 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If

                    Else
                        Dim objLeaveTran As New clsTnALeaveTran
                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < dtpFromDate.Value.Date Then
                        mdtTnADate = dtpFromDate.Value.Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), mstrEmployeeIDs, mdtTnADate.Date, enModuleReference.TnA) Then
                            'Sohail (19 Apr 2019) -- Start
                            'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "you can't Add/Edit/Delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "you can't Add/Edit/Delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 12, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Dim objFrm As New frmProcessPayroll
                                objFrm.ShowDialog()
                            End If
                            'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If
                    End If

                    'Pinkal (29-Jul-2016) -- End

                End If

            End If

            Me.Cursor = Cursors.WaitCursor

            Dim objTimeSheet As New clslogin_Tran

            Dim mdtFromDate As DateTime = Nothing
            Dim mdtToDate As DateTime = Nothing

            If dtpInTime.Checked Then
                mdtFromDate = CDate(dtpFromDate.Value.Date & " " & dtpInTime.Value.ToShortTimeString)
            End If

            If dtpOutTime.ShowCheckBox = True AndAlso dtpOutTime.Checked Then

                'Pinkal (30-May-2019) -- Start
                'Bug [0003868] - Error in Timesheet & Holiday worked Payroll Function
                'If dtpOutTime.Value.ToString().Contains("AM") AndAlso dtpOutTime.Value < dtpInTime.Value Then
                If dtpOutTime.Value < dtpInTime.Value Then
                    'Pinkal (30-May-2019) -- End
                    mdtToDate = CDate(dtpFromDate.Value.Date.AddDays(1) & " " & dtpOutTime.Value.ToShortTimeString)
                Else
                    mdtToDate = CDate(dtpFromDate.Value.Date & " " & dtpOutTime.Value.ToShortTimeString)
                End If
            Else

                'Pinkal (30-May-2019) -- Start
                'Bug [0003868] - Error in Timesheet & Holiday worked Payroll Function
                'If dtpOutTime.Value.ToString().Contains("AM") AndAlso dtpOutTime.Value < dtpInTime.Value Then
                If dtpOutTime.Value < dtpInTime.Value Then
                    'Pinkal (30-May-2019) -- End
                    mdtToDate = CDate(dtpFromDate.Value.Date.AddDays(1) & " " & dtpOutTime.Value.ToShortTimeString)
                Else
                    mdtToDate = CDate(dtpFromDate.Value.Date & " " & dtpOutTime.Value.ToShortTimeString)
                End If
            End If


            objLogin._Userunkid = User._Object._Userunkid

            'Pinkal (30-Jul-2015) -- Start
            'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
            objLogin._CompanyId = Company._Object._Companyunkid
            'Pinkal (30-Jul-2015) -- End


            If radAddTimesheet.Checked Then


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'If objLogin.GlobalAddTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                '                                                 , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                '                                                 , mdtFromDate, mdtToDate, chkOverWriteIfExist.Checked _
                '                                            , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                            , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True _
                '                                            , ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                            , ConfigParameter._Object._PolicyManagementTNA _
                '                                            , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then


                If objLogin.GlobalAddTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                                                            , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                                                            , mdtFromDate, mdtToDate, chkOverWriteIfExist.Checked _
                                                            , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting, True _
                                                            , ConfigParameter._Object._IsIncludeInactiveEmp _
                                                            , ConfigParameter._Object._PolicyManagementTNA _
                                                         , ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                         , ConfigParameter._Object._IsHolidayConsiderOnWeekend, ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                         , ConfigParameter._Object._IsHolidayConsiderOnDayoff, False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then


                    'Pinkal (11-AUG-2017) -- End


                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Global Timesheet Added successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    objbtnReset_Click(sender, e)
                Else
                    eZeeMsgBox.Show(objLogin._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            ElseIf radEditTimesheet.Checked Then


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.


                'If objLogin.GlobalEditTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                '                                                   , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                '                                                 , mdtFromDate, mdtToDate, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                '                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                                   , ConfigParameter._Object._PolicyManagementTNA, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then

                If objLogin.GlobalEditTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                                                                 , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                                                                 , mdtFromDate, mdtToDate, FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                    , ConfigParameter._Object._PolicyManagementTNA, ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                    , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                    , False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then


                    'Pinkal (11-AUG-2017) -- End

                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Global Timesheet Edited successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    objbtnReset_Click(sender, e)
                Else
                    eZeeMsgBox.Show(objLogin._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            ElseIf radDeleteTimesheet.Checked Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TNA, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLogin._Voidreason = mstrVoidReason
                End If



                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                'If objLogin.GlobalVoidtTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                '                                                      , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                '                                                    , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                '                                                    , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                '                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                '                                                    , ConfigParameter._Object._PolicyManagementTNA, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then


                If objLogin.GlobalVoidtTimesheetData(mstrEmployeeIDs, dtpFromDate.Value.Date, dtpToDate.Value.Date _
                                                                    , chkIncludeHoliday.Checked, chkIncludeWeekend.Checked, chkIncludeDayOff.Checked _
                                                                    , FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                      , ConfigParameter._Object._PolicyManagementTNA, ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                      , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                      , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                      , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                      , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                      , False, -1, Nothing, "", "", CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue)) Then

                    'Pinkal (11-AUG-2017) -- End


                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Global Timesheet Deleted done successfully for selected employee(s)."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    objbtnReset_Click(sender, e)
                Else
                    eZeeMsgBox.Show(objLogin._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridview Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                mdtEmployee.AcceptChanges()
                Dim drRow As DataRow() = mdtEmployee.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mdtEmployee.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
            End If
            dgView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In dgView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAllocationFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "Radiobutton Event"

    Private Sub radEditTimesheet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAddTimesheet.CheckedChanged, radEditTimesheet.CheckedChanged, radDeleteTimesheet.CheckedChanged
        Try
            If CType(sender, RadioButton).Name = "radAddTimesheet" Then
                dtpInTime.ShowCheckBox = False
                dtpOutTime.ShowCheckBox = False
                dtpInTime.Enabled = True
                dtpOutTime.Enabled = True

                'Pinkal (24-Dec-2013) -- Start
                'Enhancement : Oman Changes
                chkOverWriteIfExist.Checked = False
                chkOverWriteIfExist.Enabled = True
                'Pinkal (24-Dec-2013) -- End

            ElseIf CType(sender, RadioButton).Name = "radEditTimesheet" Then
                dtpInTime.ShowCheckBox = True
                dtpOutTime.ShowCheckBox = True
                dtpInTime.Enabled = True
                dtpOutTime.Enabled = True

                'Pinkal (24-Dec-2013) -- Start
                'Enhancement : Oman Changes
                chkOverWriteIfExist.Checked = False
                chkOverWriteIfExist.Enabled = False
                'Pinkal (24-Dec-2013) -- End

            ElseIf CType(sender, RadioButton).Name = "radDeleteTimesheet" Then
                dtpInTime.Value = ConfigParameter._Object._CurrentDateAndTime
                dtpInTime.Checked = False
                dtpOutTime.Value = ConfigParameter._Object._CurrentDateAndTime
                dtpOutTime.Checked = False
                dtpInTime.Enabled = False
                dtpOutTime.Enabled = False

                'Pinkal (24-Dec-2013) -- Start
                'Enhancement : Oman Changes
                chkOverWriteIfExist.Checked = False
                chkOverWriteIfExist.Enabled = False
                'Pinkal (24-Dec-2013) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEditTimesheet_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFdate.Text = Language._Object.getCaption(Me.lblFdate.Name, Me.lblFdate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
            Me.LblPolicy.Text = Language._Object.getCaption(Me.LblPolicy.Name, Me.LblPolicy.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.radDeleteTimesheet.Text = Language._Object.getCaption(Me.radDeleteTimesheet.Name, Me.radDeleteTimesheet.Text)
            Me.radEditTimesheet.Text = Language._Object.getCaption(Me.radEditTimesheet.Name, Me.radEditTimesheet.Text)
            Me.chkIncludeDayOff.Text = Language._Object.getCaption(Me.chkIncludeDayOff.Name, Me.chkIncludeDayOff.Text)
            Me.chkIncludeWeekend.Text = Language._Object.getCaption(Me.chkIncludeWeekend.Name, Me.chkIncludeWeekend.Text)
            Me.chkIncludeHoliday.Text = Language._Object.getCaption(Me.chkIncludeHoliday.Name, Me.chkIncludeHoliday.Text)
            Me.LblOuttime.Text = Language._Object.getCaption(Me.LblOuttime.Name, Me.LblOuttime.Text)
            Me.LblIntime.Text = Language._Object.getCaption(Me.LblIntime.Name, Me.LblIntime.Text)
            Me.gbWorkDetail.Text = Language._Object.getCaption(Me.gbWorkDetail.Name, Me.gbWorkDetail.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.radAddTimesheet.Text = Language._Object.getCaption(Me.radAddTimesheet.Name, Me.radAddTimesheet.Text)
			Me.chkOverWriteIfExist.Text = Language._Object.getCaption(Me.chkOverWriteIfExist.Name, Me.chkOverWriteIfExist.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no employee(s) to Edit/Delete timings.")
            Language.setMessage(mstrModuleName, 2, "Please select employee(s) in order to Edit/Delete timings.")
            Language.setMessage(mstrModuleName, 3, "Please Enter In time or Out time to Add timesheet for selected employee(s).")
            Language.setMessage(mstrModuleName, 4, "Please Enter In time or Out time to Edit timesheet for selected employee(s).")
            Language.setMessage(mstrModuleName, 5, "you can't Add/Edit/Delete employee(s) timings .Reason:Period is already over.")
            Language.setMessage(mstrModuleName, 6, "you can't Add/Edit/Delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
            Language.setMessage(mstrModuleName, 7, "Global Timesheet Added successfully for selected employee(s).")
            Language.setMessage(mstrModuleName, 8, "Global Timesheet Edited successfully for selected employee(s).")
            Language.setMessage(mstrModuleName, 9, "Global Timesheet Deleted done successfully for selected employee(s).")
            Language.setMessage(mstrModuleName, 10, "Out Time is less than In time.So Out Time will be considered for next day.Are you sure you want to continue ?")
			Language.setMessage(mstrModuleName, 11, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?")
			Language.setMessage(mstrModuleName, 12, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class