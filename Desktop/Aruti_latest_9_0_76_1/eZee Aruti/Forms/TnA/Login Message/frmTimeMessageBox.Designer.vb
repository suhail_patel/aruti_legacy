<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimeMessageBox
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimeMessageBox))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.btnclose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objlblTime = New System.Windows.Forms.Label
        Me.objlblMessage = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objlblName = New System.Windows.Forms.Label
        Me.tmrTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.lblTimer = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Transparent
        Me.pnlMain.Controls.Add(Me.lblTimer)
        Me.pnlMain.Controls.Add(Me.btnclose)
        Me.pnlMain.Controls.Add(Me.Panel2)
        Me.pnlMain.Controls.Add(Me.objlblTime)
        Me.pnlMain.Controls.Add(Me.objlblMessage)
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(504, 181)
        Me.pnlMain.TabIndex = 2
        '
        'btnclose
        '
        Me.btnclose.BackColor = System.Drawing.Color.White
        Me.btnclose.BackgroundImage = CType(resources.GetObject("btnclose.BackgroundImage"), System.Drawing.Image)
        Me.btnclose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnclose.BorderColor = System.Drawing.Color.Empty
        Me.btnclose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnclose.FlatAppearance.BorderSize = 0
        Me.btnclose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnclose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.ForeColor = System.Drawing.Color.Black
        Me.btnclose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnclose.GradientForeColor = System.Drawing.Color.Black
        Me.btnclose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Location = New System.Drawing.Point(402, 140)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnclose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnclose.Size = New System.Drawing.Size(90, 30)
        Me.btnclose.TabIndex = 28
        Me.btnclose.Text = "&Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(0, 129)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(502, 1)
        Me.Panel2.TabIndex = 27
        '
        'objlblTime
        '
        Me.objlblTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTime.ForeColor = System.Drawing.Color.Black
        Me.objlblTime.Location = New System.Drawing.Point(336, 66)
        Me.objlblTime.Name = "objlblTime"
        Me.objlblTime.Size = New System.Drawing.Size(156, 43)
        Me.objlblTime.TabIndex = 26
        Me.objlblTime.Text = "10:00 AM"
        Me.objlblTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblMessage
        '
        Me.objlblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblMessage.ForeColor = System.Drawing.Color.Black
        Me.objlblMessage.Location = New System.Drawing.Point(12, 66)
        Me.objlblMessage.Name = "objlblMessage"
        Me.objlblMessage.Size = New System.Drawing.Size(318, 43)
        Me.objlblMessage.TabIndex = 25
        Me.objlblMessage.Text = "Message"
        Me.objlblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objlblName)
        Me.Panel1.Location = New System.Drawing.Point(0, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(504, 60)
        Me.Panel1.TabIndex = 23
        '
        'objlblName
        '
        Me.objlblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblName.ForeColor = System.Drawing.Color.Black
        Me.objlblName.Location = New System.Drawing.Point(3, 3)
        Me.objlblName.Name = "objlblName"
        Me.objlblName.Size = New System.Drawing.Size(499, 49)
        Me.objlblName.TabIndex = 22
        Me.objlblName.Text = "Name"
        Me.objlblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tmrTimeOut
        '
        Me.tmrTimeOut.Interval = 1000
        '
        'lblTimer
        '
        Me.lblTimer.Location = New System.Drawing.Point(373, 143)
        Me.lblTimer.Name = "lblTimer"
        Me.lblTimer.Size = New System.Drawing.Size(23, 27)
        Me.lblTimer.TabIndex = 29
        Me.lblTimer.Text = "5"
        Me.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTimer.Visible = False
        '
        'frmTimeMessageBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(504, 181)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimeMessageBox"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Message Box"
        Me.pnlMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents tmrTimeOut As System.Windows.Forms.Timer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objlblName As System.Windows.Forms.Label
    Friend WithEvents objlblMessage As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objlblTime As System.Windows.Forms.Label
    Friend WithEvents btnclose As eZee.Common.eZeeLightButton
    Friend WithEvents lblTimer As System.Windows.Forms.Label
End Class
