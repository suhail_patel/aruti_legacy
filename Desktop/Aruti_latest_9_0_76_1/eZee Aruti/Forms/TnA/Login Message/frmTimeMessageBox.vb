Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTimeMessageBox

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTimeMessageBox"
    Private mblnCancel As Boolean = True
    Friend mstrMessage As String = String.Empty
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal mstrEmplyeeName As String) As Boolean
        Try
            objlblMessage.Text = mstrMessage
            objlblName.Text = mstrEmplyeeName
            objlblTime.Text = Now.ToShortTimeString
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTimeMessageBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            tmrTimeOut.Enabled = True
            lblTimer.Text = "5"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimeMessageBox_Load", mstrMessage)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

#End Region

#Region " Timer's Event"

    Private Sub tmrTimeOut_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrTimeOut.Tick
        Try
            If Val(lblTimer.Text) > 0 Then
                lblTimer.Text = (Val(lblTimer.Text) - 1).ToString
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrTimeOut_Tick", mstrMessage)
        End Try
    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.btnclose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnclose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnclose.Text = Language._Object.getCaption(Me.btnclose.Name, Me.btnclose.Text)
			Me.lblTimer.Text = Language._Object.getCaption(Me.lblTimer.Name, Me.lblTimer.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
