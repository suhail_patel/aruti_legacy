﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

Public Class frmOT_Penalty

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmOT_Penalty"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintOTPenaltyunkid As Integer = -1
    Private mintPolicyMasterUnkid As Integer = 0
    Dim objOTPenalty As New clsOT_Penalty
    Dim mdtOTPenalty As DataTable = Nothing
    Dim mdtPolicy As DataTable = Nothing
    Dim blnCellEndEdit As Boolean = False
    Dim mintLastEditedRow As Integer = -1
    Dim mdvPenalty As DataView = Nothing
    Dim imgAdd As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.add_16)
    Dim imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1)

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal xPolicyId As Integer, ByVal xPolicy As String, ByVal eAction As enAction, ByVal dtPolicy As DataTable, ByRef dtOTPenalty As DataTable) As Boolean
        Try
            mintPolicyMasterUnkid = xPolicyId
            LblPolicyValue.Text = xPolicy
            menAction = eAction
            mdtPolicy = dtPolicy
            mdtOTPenalty = dtOTPenalty
            Me.ShowDialog()
            If mblnCancel = True Then dtOTPenalty = mdtOTPenalty
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmOT_Penalty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objOTPenalty = New clsOT_Penalty
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            If mdtOTPenalty Is Nothing OrElse mdtOTPenalty.Rows.Count <= 0 Then
                objOTPenalty.GetList("List", True, mintPolicyMasterUnkid)
                mdtOTPenalty = objOTPenalty._dtOTPenalty.Copy
            End If

            mdtOTPenalty.Columns("penaltyunkid").DefaultValue = -1
            mdtOTPenalty.Columns("ot1_penalty").DefaultValue = 0
            mdtOTPenalty.Columns("ot2_penalty").DefaultValue = 0
            mdtOTPenalty.Columns("ot3_penalty").DefaultValue = 0
            mdtOTPenalty.Columns("ot4_penalty").DefaultValue = 0
            mdtOTPenalty.Columns("isvoid").DefaultValue = False
            mdtOTPenalty.Columns("voiduserunkid").DefaultValue = -1
            mdtOTPenalty.Columns("AUD").DefaultValue = ""
            mdtOTPenalty.Columns("GUID").DefaultValue = ""

            If mdtOTPenalty.Columns.Contains("image") = False Then
                mdtOTPenalty.Columns.Add("image", System.Type.GetType("System.Object")).DefaultValue = imgAdd
            End If

            FillGrid()

            If mdtOTPenalty IsNot Nothing AndAlso mdtOTPenalty.Rows.Count <= 0 Then
                dgOTPenalty_CellContentClick(New Object, New DataGridViewCellEventArgs(objdgcolhADD.Index, 0))

            ElseIf mdtOTPenalty IsNot Nothing AndAlso mdtOTPenalty.Rows.Count > 0 Then
                Dim index As Integer = 0
                For i As Integer = 0 To mdtOTPenalty.Rows.Count - 1
                    If mdtOTPenalty.Rows(i)("AUD").ToString() <> "D" Then
                        If index = dgOTPenalty.Rows.Count - 1 Then
                            mdtOTPenalty.Rows(i)("image") = imgAdd
                        Else
                            mdtOTPenalty.Rows(i)("image") = imgBlank
                        End If
                        mdtOTPenalty.Rows(i)("GUID") = Guid.NewGuid.ToString()
                        mdtOTPenalty.Rows(i)("uptomins") = CInt(mdtOTPenalty.Rows(i)("uptominsinsec")) / 60
                        mdtOTPenalty.Rows(i)("ot1_penalty") = CInt(mdtOTPenalty.Rows(i)("ot1_penaltyinsec")) / 60
                        mdtOTPenalty.Rows(i)("ot2_penalty") = CInt(mdtOTPenalty.Rows(i)("ot2_penaltyinsec")) / 60
                        mdtOTPenalty.Rows(i)("ot3_penalty") = CInt(mdtOTPenalty.Rows(i)("ot3_penaltyinsec")) / 60
                        mdtOTPenalty.Rows(i)("ot4_penalty") = CInt(mdtOTPenalty.Rows(i)("ot4_penaltyinsec")) / 60
                        SetGridCellMode(False, index)
                        index += 1
                    End If
                Next
                mdtOTPenalty.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOT_Penalty_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPolicy_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objOTPenalty = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            objfrm.displayDialog(Me)
        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub FillGrid()
        Try
            dgOTPenalty.AutoGenerateColumns = False
            objdgcolhADD.DataPropertyName = "image"
            dgcolhUptoMins.DataPropertyName = "uptomins"
            dgcolhOT1.DataPropertyName = "ot1_penalty"
            dgcolhOT2.DataPropertyName = "ot2_penalty"
            dgcolhOT3.DataPropertyName = "ot3_penalty"
            dgcolhOT4.DataPropertyName = "ot4_penalty"
            objdgcolhPenaltyunkid.DataPropertyName = "penaltyunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            mdvPenalty = mdtOTPenalty.DefaultView
            mdvPenalty.RowFilter = "AUD <> 'D' "
            dgOTPenalty.DataSource = mdvPenalty
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objOTPenalty._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridCellMode(ByVal mblnFlag As Boolean, ByVal iRowIndex As Integer)
        Try
            dgOTPenalty.Rows(iRowIndex).Cells(dgcolhOT1.Index).ReadOnly = mblnFlag
            dgOTPenalty.Rows(iRowIndex).Cells(dgcolhOT2.Index).ReadOnly = mblnFlag
            dgOTPenalty.Rows(iRowIndex).Cells(dgcolhOT3.Index).ReadOnly = mblnFlag
            dgOTPenalty.Rows(iRowIndex).Cells(dgcolhOT4.Index).ReadOnly = mblnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridCellMode", mstrModuleName)
        End Try
    End Sub

    Private Function Validation(ByVal intRowIndex As Integer) As Boolean
        Try
            If mdvPenalty Is Nothing OrElse mdvPenalty.Table.Rows.Count <= 0 Then Return True

            If IsDBNull(mdvPenalty.ToTable.Rows(intRowIndex)("uptomins")) OrElse CInt(mdvPenalty.ToTable.Rows(intRowIndex)("uptomins")) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Up to mins is compulsory information.Please define Up to mins."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhUptoMins.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhUptoMins.Index)
                SendKeys.Send("{F2}")
                Return False

            Else
                Dim drRow() As DataRow = mdvPenalty.ToTable.Select("uptomins >= " & CInt(dgOTPenalty.Rows(intRowIndex).Cells(dgcolhUptoMins.Index).Value) & " AND GUID <> '" & dgOTPenalty.Rows(intRowIndex).Cells(objdgcolhGUID.Index).Value.ToString() & "'")
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Up to mins must be greater than all previous up to mins.Please define proper up to mins."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    dgOTPenalty.Focus()
                    dgOTPenalty.Rows(intRowIndex).Cells(dgcolhUptoMins.Index).Selected = True
                    dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhUptoMins.Index)
                    SendKeys.Send("{F2}")
                    Return False
                End If
            End If

            If (IsDBNull(mdvPenalty.ToTable.Rows(intRowIndex)("ot1_penalty")) OrElse CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot1_penalty")) <= 0) AndAlso _
                                 (IsDBNull(mdvPenalty.ToTable.Rows(intRowIndex)("ot2_penalty")) OrElse CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot2_penalty")) <= 0) AndAlso _
                                 (IsDBNull(mdvPenalty.ToTable.Rows(intRowIndex)("ot3_penalty")) OrElse CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot3_penalty")) <= 0) AndAlso _
                                 (IsDBNull(mdvPenalty.ToTable.Rows(intRowIndex)("ot4_penalty")) OrElse CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot4_penalty")) <= 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "OT Deduction Mins is compulsory information.Please define atleast anyone OT Deduction Mins."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT1.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT1.Index)
                SendKeys.Send("{F2}")
                Return False

            ElseIf CInt(mdtPolicy.Rows(0)("ot1insec")) < (CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot1_penalty")) * 60) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "OT1 Penalty can not be greater than policy OT1 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT1.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT1.Index)
                SendKeys.Send("{F2}")
                Return False

            ElseIf CInt(mdtPolicy.Rows(0)("ot2insec")) < (CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot2_penalty")) * 60) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "OT2 Penalty can not be greater than policy OT2 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT2.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT2.Index)
                SendKeys.Send("{F2}")
                Return False

            ElseIf CInt(mdtPolicy.Rows(0)("ot3insec")) < (CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot3_penalty")) * 60) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "OT3 Penalty can not be greater than policy OT3 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT3.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT3.Index)
                SendKeys.Send("{F2}")
                Return False

            ElseIf CInt(mdtPolicy.Rows(0)("ot4insec")) < (CInt(mdvPenalty.ToTable.Rows(intRowIndex)("ot4_penalty")) * 60) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "OT4 Penalty can not be greater than policy OT4 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgOTPenalty.Focus()
                dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT4.Index).Selected = True
                dgOTPenalty.CurrentCell = dgOTPenalty.Rows(intRowIndex).Cells(dgcolhOT4.Index)
                SendKeys.Send("{F2}")
                Return False

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region " Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If mdvPenalty IsNot Nothing AndAlso mdvPenalty.Table.Rows.Count > 0 Then
                If Validation(mdvPenalty.ToTable.Rows.Count - 1) = False Then Exit Sub
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "DataGrid Event"

    'Private Sub dgOTPenalty_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOTPenalty.CellEndEdit
    '    Try
    '        If e.RowIndex < 0 Then Exit Sub
    '        dgOTPenalty_KeyDown(New Object, New KeyEventArgs(Keys.Enter))
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgOTPenalty_CellEndEdit", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub dgOTPenalty_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgOTPenalty.CellValidating
    '    Try
    '        Dim intPenaltyunkid As Integer = -1

    '        If e.RowIndex < 0 Then Exit Sub

    '        'RemoveHandler dgOTPenalty.CellEndEdit, AddressOf dgOTPenalty_CellEndEdit

    '        If mdtPolicy IsNot Nothing AndAlso mdtPolicy.Rows.Count > 0 Then

    '            If IsDBNull(dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhPenaltyunkid.Index).Value) = False Then
    '                intPenaltyunkid = CInt(dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhPenaltyunkid.Index).Value)
    '            End If

    '            'If e.ColumnIndex = dgcolhUptoMins.Index Then

    '            'If CInt(CInt(IIf(e.FormattedValue.ToString().Length > 0, e.FormattedValue, 0)) * 60) < 0 Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Up to mins is compulsory information.Please define up to mins greater than zero."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '    e.Cancel = True
    '            'Else

    '            '    If dgOTPenalty.Rows(e.RowIndex).Cells(e.ColumnIndex).Value Is Nothing Then Exit Sub

    '            '    Dim drRow() As DataRow = Nothing
    '            '    drRow = mdtOTPenalty.Select("uptomins >= " & CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0)) & " AND AUD <> 'D'  AND GUID <> '" & mdtOTPenalty.Rows(e.RowIndex)("GUID").ToString() & "'")
    '            '    If drRow.Length > 0 Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please define proper up to mins."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '        dgOTPenalty.CurrentCell = dgOTPenalty.CurrentRow.Cells(dgcolhUptoMins.Index)
    '            '        SendKeys.Send("{f2}")
    '            '        Exit Sub
    '            '    Else
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("uptomins") = CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0))
    '            '    End If

    '            'End If

    '            'ElseIf e.ColumnIndex = dgcolhOT1.Index Then

    '            '    If CInt(mdtPolicy.Rows(0)("ot1insec")) < CInt(CInt(IIf(e.FormattedValue.ToString().Length > 0, e.FormattedValue, 0)) * 60) Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "OT1 Penalty can not be greater than policy OT1 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '        e.Cancel = True
    '            '    Else
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("ot1_penalty") = CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0))
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("AUD") = "U"
    '            '    End If

    '            'ElseIf e.ColumnIndex = dgcolhOT2.Index Then

    '            '    If CInt(mdtPolicy.Rows(0)("ot2insec")) < CInt(CInt(IIf(e.FormattedValue.ToString().Length > 0, e.FormattedValue, 0)) * 60) Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "OT2 Penalty can not be greater than policy OT2 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '        e.Cancel = True
    '            '    Else
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("ot2_penalty") = CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0))
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("AUD") = "U"
    '            '    End If

    '            'ElseIf e.ColumnIndex = dgcolhOT3.Index Then

    '            '    If CInt(mdtPolicy.Rows(0)("ot3insec")) < CInt(CInt(IIf(e.FormattedValue.ToString().Length > 0, e.FormattedValue, 0)) * 60) Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "OT3 Penalty can not be greater than policy OT3 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '        e.Cancel = True
    '            '    Else
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("ot3_penalty") = CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0))
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("AUD") = "U"
    '            '    End If

    '            'ElseIf e.ColumnIndex = dgcolhOT4.Index Then

    '            '    If CInt(mdtPolicy.Rows(0)("ot4insec")) < CInt(CInt(IIf(e.FormattedValue.ToString().Length > 0, e.FormattedValue, 0)) * 60) Then
    '            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "OT4Penalty can not be greater than policy OT4 hours."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
    '            '        e.Cancel = True
    '            '    Else
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("ot4_penalty") = CInt(IIf(e.FormattedValue.ToString.Trim.Length > 0, e.FormattedValue, 0))
    '            '        mdvPenalty.Table.Rows(e.RowIndex)("AUD") = "U"
    '            '    End If

    '            'End If

    '            If e.RowIndex <= mdtOTPenalty.Rows.Count - 1 Then
    '                If intPenaltyunkid <= 0 Then
    '                    mdvPenalty.Table.Rows(e.RowIndex)("AUD") = "A"
    '                    mdvPenalty.Table.Rows(e.RowIndex)("penaltyunkid") = -1
    '                    mdvPenalty.Table.Rows(e.RowIndex)("GUID") = Guid.NewGuid.ToString()
    '                End If
    '                If mdvPenalty.Table.Rows(e.RowIndex)("AUD").ToString() <> "D" Then
    '                    mdvPenalty.Table.Rows(e.RowIndex)("isvoid") = False
    '                    mdvPenalty.Table.Rows(e.RowIndex)("voiduserunkid") = -1
    '                    mdvPenalty.Table.Rows(e.RowIndex)("voiddatetime") = DBNull.Value
    '                    mdvPenalty.Table.Rows(e.RowIndex)("voidreason") = ""
    '                End If

    '            End If

    '        End If

    '        'AddHandler dgOTPenalty.CellEndEdit, AddressOf dgOTPenalty_CellEndEdit

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgOTPenalty_CellValidating", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub dgOTPenalty_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOTPenalty.CellContentClick, dgOTPenalty.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If Me.dgOTPenalty.IsCurrentCellDirty Then
                Me.dgOTPenalty.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objdgcolhADD.Index Then

                Dim mblnRowAdded As Boolean = False

                If Validation(e.RowIndex) = False Then Exit Sub

                If mdtOTPenalty IsNot Nothing Then

                    If mdtOTPenalty.Rows.Count <= 0 Then
FromDelete:
                        mdtOTPenalty.Rows.Add(mdtOTPenalty.NewRow)
                        mdvPenalty = mdtOTPenalty.DefaultView
                        mdvPenalty.RowFilter = "AUD <> 'D' "
                        mdtOTPenalty.Rows(mdtOTPenalty.Rows.Count - 1)("GUID") = Guid.NewGuid.ToString()
                        mdtOTPenalty.Rows(mdtOTPenalty.Rows.Count - 1)("AUD") = "A"
                        mdtOTPenalty.AcceptChanges()
                        mblnRowAdded = True


                    ElseIf CInt(mdvPenalty.ToTable.Rows(e.RowIndex)("Ot1_Penalty")) > 0 OrElse _
                            CInt(mdvPenalty.ToTable.Rows(e.RowIndex)("Ot2_Penalty")) > 0 OrElse _
                            CInt(mdvPenalty.ToTable.Rows(e.RowIndex)("Ot3_Penalty")) > 0 OrElse _
                            CInt(mdvPenalty.ToTable.Rows(e.RowIndex)("Ot4_Penalty")) > 0 Then

                        mdtOTPenalty.Rows.Add(mdtOTPenalty.NewRow)
                        mdtOTPenalty.Rows(mdtOTPenalty.Rows.Count - 1)("GUID") = Guid.NewGuid.ToString()
                        mdtOTPenalty.Rows(mdtOTPenalty.Rows.Count - 1)("AUD") = "A"
                        mdtOTPenalty.AcceptChanges()
                        SetGridCellMode(True, e.RowIndex + 1)
                        mblnRowAdded = True
                    End If


                    If mblnRowAdded Then
                        Dim iRowIdx = 0
                        If mdvPenalty.ToTable.Rows.Count > 1 Then
                            iRowIdx = e.RowIndex + 1
                        End If

                        If (iRowIdx - 1) >= 0 Then
                            dgOTPenalty.Rows(iRowIdx - 1).Cells(objdgcolhADD.Index).Value = imgBlank
                        End If

                        dgOTPenalty.Rows(iRowIdx).Cells(dgcolhUptoMins.Index).Selected = True
                        dgOTPenalty.CurrentCell = dgOTPenalty.Rows(iRowIdx).Cells(dgcolhUptoMins.Index)
                        SendKeys.Send("{f2}")
                    End If

                End If

            ElseIf e.ColumnIndex = objdgcolhDelete.Index Then

                Dim mstrVoidReason As String = ""

                If mdtOTPenalty IsNot Nothing AndAlso mdtOTPenalty.Rows.Count > 0 Then

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this penalty?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                        If dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhPenaltyunkid.Index).Value IsNot Nothing AndAlso CInt(dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhPenaltyunkid.Index).Value) >= 0 Then

                            Dim frm As New frmReasonSelection
                            frm.displayDialog(enVoidCategoryType.TNA, mstrVoidReason)
                            If mstrVoidReason.Length <= 0 Then
                                Exit Sub
                            Else
Delete:
                                Dim iIndex As Integer = 0
                                If dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value IsNot Nothing AndAlso _
                                  dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString().Length > 0 Then
                                    iIndex = mdvPenalty.Table.Rows.IndexOf(mdtOTPenalty.Select("GUID ='" & dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString() & "'")(0))
                                    mdvPenalty.Table.Rows(iIndex)("AUD") = "D"
                                    mdvPenalty.Table.Rows(iIndex)("voidreason") = mstrVoidReason
                                    mdtOTPenalty.AcceptChanges()
                                End If


                                If (mdvPenalty.ToTable.Rows.Count - 1) >= 0 AndAlso e.RowIndex > mdvPenalty.ToTable.Rows.Count - 1 Then
                                    dgOTPenalty.Rows(e.RowIndex - 1).Cells(objdgcolhADD.Index).Value = imgAdd
                                End If

                                If dgOTPenalty.Rows.Count = 0 Then
                                    GoTo FromDelete
                                End If

                            End If
                            frm = Nothing

                        Else
                            mstrVoidReason = ""
                            GoTo Delete
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOTPenalty_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOTPenalty_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgOTPenalty.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                If dgOTPenalty.CurrentCell.ColumnIndex <= dgOTPenalty.ColumnCount - 3 Then
                    If dgOTPenalty.CurrentCell.ColumnIndex = dgcolhOT4.Index Then
                        dgOTPenalty.CurrentCell = dgOTPenalty.CurrentRow.Cells(objdgcolhADD.Index)
                    Else
                        SendKeys.Send("{tab}")
                    End If
                    SendKeys.Send("{f2}")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOTPenalty_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOTPenalty_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOTPenalty.CellValueChanged
        Try
            If dgOTPenalty.CurrentCell Is Nothing Then Exit Sub
            Dim iIndex As Integer = 0
            iIndex = mdvPenalty.Table.Rows.IndexOf(mdtOTPenalty.Select("GUID ='" & dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString() & "'")(0))

            If dgOTPenalty.CurrentCell.ColumnIndex = dgcolhUptoMins.Index Then
                If CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) >= 0 Then
                    SetGridCellMode(False, e.RowIndex)
                Else
                    SetGridCellMode(True, e.RowIndex)
                End If
                mdvPenalty.Table.Rows(iIndex)("Uptominsinsec") = CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) * 60

            ElseIf dgOTPenalty.CurrentCell.ColumnIndex = dgcolhOT1.Index Then
                mdvPenalty.Table.Rows(iIndex)("Ot1_Penaltyinsec") = CInt(IIf(IsDBNull(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value), 0, CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) * 60))

            ElseIf dgOTPenalty.CurrentCell.ColumnIndex = dgcolhOT2.Index Then
                mdvPenalty.Table.Rows(iIndex)("Ot2_Penaltyinsec") = CInt(IIf(IsDBNull(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value), 0, CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) * 60))

            ElseIf dgOTPenalty.CurrentCell.ColumnIndex = dgcolhOT3.Index Then
                mdvPenalty.Table.Rows(iIndex)("Ot3_Penaltyinsec") = CInt(IIf(IsDBNull(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value), 0, CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) * 60))

            ElseIf dgOTPenalty.CurrentCell.ColumnIndex = dgcolhOT4.Index Then
                mdvPenalty.Table.Rows(iIndex)("Ot4_Penaltyinsec") = CInt(IIf(IsDBNull(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value), 0, CInt(dgOTPenalty.CurrentRow.Cells(e.ColumnIndex).Value) * 60))
            End If


            If CInt(mdvPenalty.ToTable.Rows(e.RowIndex)("penaltyunkid")) > 0 Then
                iIndex = mdvPenalty.Table.Rows.IndexOf(mdtOTPenalty.Select("GUID ='" & dgOTPenalty.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value.ToString() & "'")(0))
                mdvPenalty.Table.Rows(iIndex)("AUD") = "U"
            End If

            mdtOTPenalty.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOTPenalty_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOTPenalty_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOTPenalty.CellEnter
        Try
            If e.ColumnIndex = dgcolhUptoMins.Index OrElse e.ColumnIndex = dgcolhOT1.Index OrElse _
              e.ColumnIndex = dgcolhOT2.Index OrElse e.ColumnIndex = dgcolhOT3.Index OrElse _
              e.ColumnIndex = dgcolhOT4.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOTPenalty_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgOTPenalty_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgOTPenalty.DataError

    End Sub


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblPolicyValue.Text = Language._Object.getCaption(Me.LblPolicyValue.Name, Me.LblPolicyValue.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.dgcolhUptoMins.HeaderText = Language._Object.getCaption(Me.dgcolhUptoMins.Name, Me.dgcolhUptoMins.HeaderText)
            Me.dgcolhOT1.HeaderText = Language._Object.getCaption(Me.dgcolhOT1.Name, Me.dgcolhOT1.HeaderText)
            Me.dgcolhOT2.HeaderText = Language._Object.getCaption(Me.dgcolhOT2.Name, Me.dgcolhOT2.HeaderText)
            Me.dgcolhOT3.HeaderText = Language._Object.getCaption(Me.dgcolhOT3.Name, Me.dgcolhOT3.HeaderText)
            Me.dgcolhOT4.HeaderText = Language._Object.getCaption(Me.dgcolhOT4.Name, Me.dgcolhOT4.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Up to mins is compulsory information.Please define Up to mins.")
            Language.setMessage(mstrModuleName, 2, "OT Deduction Mins is compulsory information.Please define atleast anyone OT Deduction Mins.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this penalty?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class