﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmHoldemployee

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmHoldemployee"
    Private mblnCancel As Boolean = True
    Private objHoldEmployee As clsholdemployee_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintHoldMasterUnkid As Integer = -1
    Dim wkMins As Double
    Private mstrAdvanceFilter As String = ""
    Dim mdvEmployee As DataView = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintHoldMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintHoldMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmHoldemployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objHoldEmployee = New clsholdemployee_master
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()

            dtpStartdate.MinDate = FinancialYear._Object._Database_Start_Date

            If menAction = enAction.EDIT_ONE Then
                objHoldEmployee._Holdunkid = mintHoldMasterUnkid
            End If
            FillCombo()
            GetValue()

            dtpStartdate.Select()
            chkPostTimesheet.Visible = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHoldemployee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHoldemployee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHoldemployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHoldemployee_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objHoldEmployee = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsholdemployee_master.SetMessages()
            objfrm._Other_ModuleNames = "clsholdemployee_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboOperationType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Operation Type is compulsory information.Please Select Operation Type."))
                cboOperationType.Focus()
                Exit Sub
            End If
            FillEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            objChkSelectAll.Checked = False
            dtpStartdate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dgvData.DataSource = Nothing
            mstrAdvanceFilter = ""
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnHoldEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHoldEmployee.Click, btnUnHoldEmployees.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() Then

                If CheckForPeriod() Then


                    Dim mstrMessage As String = ""

                    If CInt(cboOperationType.SelectedIndex) = 1 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Hold selected Employees?")
                    ElseIf CInt(cboOperationType.SelectedIndex) = 2 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 6, "Are you sure you want to Unhold selected Employees?")
                    End If

                    If eZeeMsgBox.Show(mstrMessage, CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                        Dim objFrmRemark As New frmRemark
                        objFrmRemark.objgbRemarks.Text = Me.Text & " " & Language.getMessage(mstrModuleName, 9, "Remark")
                        If objFrmRemark.displayDialog(objHoldEmployee._Remark, enArutiApplicatinType.Aruti_Payroll) = False Then Exit Sub

                        If menAction = enAction.ADD_ONE Then
                            Dim drRow() As DataRow = mdvEmployee.Table.Select("ischeck = True")
                            If drRow.Length > 0 Then

                                SetValue()

                                For i As Integer = 0 To drRow.Length - 1

                                    If objHoldEmployee.GetEmployeeExistForHoldUnhold(dtpStartdate.Value.Date, CInt(drRow(i)("employeeunkid"))) = False Then
                                        drRow(i)("message") = Language.getMessage(mstrModuleName, 14, "Sorry,This Employee can not") & " " & cboOperationType.Text & "." & Language.getMessage(mstrModuleName, 15, "Reason : On Same day you can't do multiple operation.")
                                        drRow(i)("IsError") = True
                                    Else
                                        objHoldEmployee._Employeeunkid = CInt(drRow(i)("employeeunkid"))
                                        If objHoldEmployee.Insert() = False Then
                                            drRow(i)("IsError") = True
                                            drRow(i)("message") = objHoldEmployee._Message
                                        Else
                                            drRow(i)("IsError") = False
                                            drRow(i)("message") = cboOperationType.Text & " " & Language.getMessage(mstrModuleName, 16, "Successfully.")
                                        End If
                                    End If
                                    drRow(i).AcceptChanges()
                                    mdvEmployee.ToTable.AcceptChanges()
                                Next
                            End If
                        End If

                        If blnFlag = False And objHoldEmployee._Message <> "" Then
                            eZeeMsgBox.Show(objHoldEmployee._Message, enMsgBoxStyle.Information)
                        End If

                        If blnFlag Then
                            mblnCancel = False
                            mintHoldMasterUnkid = 0
                            Me.Close()
                        End If
                    End If

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Function Validation() As Boolean
        Try
            If mdvEmployee Is Nothing Then Return False
            Dim drRow() As DataRow = mdvEmployee.ToTable.Select("ischeck = True")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one Employee from the list."), enMsgBoxStyle.Information)
                dgvData.Select()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub setColor()
        Try
            cboOperationType.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objHoldEmployee._Effectivedate = dtpStartdate.Value.Date
            objHoldEmployee._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objHoldEmployee._Ispost = False
            objHoldEmployee._Operationtype = CInt(cboOperationType.SelectedIndex)
            objHoldEmployee._Userunkid = User._Object._Userunkid
            objHoldEmployee._Isvoid = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 10, "Select"))
            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 11, "Hold"))
            cboOperationType.Items.Add(Language.getMessage(mstrModuleName, 12, "Unhold"))
            cboOperationType.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As New DataSet
        Dim strSearching As String = ""
        Dim dtEmployee As New DataTable
        Try

            Me.Cursor = Cursors.WaitCursor


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            dsEmployee = objHoldEmployee.GetEmployeeFromOperation(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                     , dtpStartdate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                                     , CInt(IIf(cboOperationType.SelectedIndex <= 0, 0, cboOperationType.SelectedIndex)), , True, strSearching)


            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Columns.Contains("ischeck") = False Then
                dsEmployee.Tables(0).Columns.Add("ischeck", Type.GetType("System.Boolean")).DefaultValue = False
            End If

            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Columns.Contains("message") = False Then
                dsEmployee.Tables(0).Columns.Add("message", Type.GetType("System.String")).DefaultValue = ""
            End If

            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Columns.Contains("isError") = False Then
                dsEmployee.Tables(0).Columns.Add("isError", Type.GetType("System.Boolean")).DefaultValue = False
            End If

            mdvEmployee = dsEmployee.Tables(0).DefaultView

            dgvData.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "ischeck"
            dgcolhCode.DataPropertyName = "employeecode"
            dgcolhEmployee.DataPropertyName = "Employee"
            dgcolhOperation.DataPropertyName = "operation"
            dgcolhMessage.DataPropertyName = "message"
            dgvData.DataSource = mdvEmployee

            objChkSelectAll.Checked = False
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())

            If CInt(cboOperationType.SelectedIndex) = 1 Then
                btnHoldEmployee.Enabled = True
                btnUnHoldEmployees.Enabled = False

            ElseIf CInt(cboOperationType.SelectedIndex) = 2 Then
                btnUnHoldEmployees.Enabled = True
                btnHoldEmployee.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Function CheckForPeriod() As Boolean
        Try
            Dim dsList As DataSet = Nothing
            Dim objperiod As New clscommom_period_Tran
            dsList = objperiod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)

            If dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString) <= dtpStartdate.Value.Date _
                    And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("end_date").ToString) >= dtpStartdate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select correct start date. Reason:This date period was already closed."), enMsgBoxStyle.Information)
                        dtpStartdate.Select()
                        Return False
                    End If

                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForPeriod", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = mdvEmployee.ToTable.Select("ischeck = true")

            RemoveHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                objChkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                objChkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                objChkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " LinkButton's Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub objChkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkSelectAll.CheckedChanged
        Try
            If mdvEmployee Is Nothing Then Exit Sub

            For Each dr As DataRowView In mdvEmployee
                RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                dr("ischeck") = objChkSelectAll.Checked
                AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
                dr.EndEdit()
            Next
            mdvEmployee.ToTable.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            RemoveHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_CheckedChanged

            If e.ColumnIndex = objdgcolhCheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdvEmployee.Table.AcceptChanges()
                End If
                SetCheckBoxValue()
            End If

            AddHandler objChkSelectAll.CheckedChanged, AddressOf objChkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilter.TextChanged
        Dim strSearch As String = ""
        Try
            If mdvEmployee Is Nothing Then Exit Sub

            If txtFilter.Text.Trim.Length > 0 Then
                strSearch = dgcolhCode.DataPropertyName & " LIKE '%" & txtFilter.Text & "%' OR " & _
                            dgcolhEmployee.DataPropertyName & " LIKE '%" & txtFilter.Text & "%'"
            End If
            mdvEmployee.RowFilter = strSearch
            SetCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFilter_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.EZeeCollapsibleContainer2.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer2.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.chkPostTimesheet.Text = Language._Object.getCaption(Me.chkPostTimesheet.Name, Me.chkPostTimesheet.Text)
            Me.EZeeCollapsibleContainer2.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer2.Name, Me.EZeeCollapsibleContainer2.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.gbFilterEmployee.Text = Language._Object.getCaption(Me.gbFilterEmployee.Name, Me.gbFilterEmployee.Text)
            Me.LblOperationType.Text = Language._Object.getCaption(Me.LblOperationType.Name, Me.LblOperationType.Text)
            Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhOperation.HeaderText = Language._Object.getCaption(Me.dgcolhOperation.Name, Me.dgcolhOperation.HeaderText)
            Me.dgcolhMessage.HeaderText = Language._Object.getCaption(Me.dgcolhMessage.Name, Me.dgcolhMessage.HeaderText)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnHoldEmployee.Text = Language._Object.getCaption(Me.btnHoldEmployee.Name, Me.btnHoldEmployee.Text)
            Me.btnUnHoldEmployees.Text = Language._Object.getCaption(Me.btnUnHoldEmployees.Name, Me.btnUnHoldEmployees.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 4, "Please check atleast one Employee from the list.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to Hold selected Employees?")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to Unhold selected Employees?")
            Language.setMessage(mstrModuleName, 7, "Please select proper start date. Reason:This date period was already closed.")
            Language.setMessage(mstrModuleName, 9, "Remark")
            Language.setMessage(mstrModuleName, 10, "Select")
            Language.setMessage(mstrModuleName, 11, "Hold")
            Language.setMessage(mstrModuleName, 12, "Unhold")
            Language.setMessage(mstrModuleName, 13, "Operation Type is compulsory information.Please Select Operation Type.")
            Language.setMessage(mstrModuleName, 14, "Sorry,This Employee can not")
            Language.setMessage(mstrModuleName, 15, "Reason : On Same day you can't do multiple operation.")
            Language.setMessage(mstrModuleName, 16, "Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class