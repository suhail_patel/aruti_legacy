﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmholdunholdemployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmholdunholdemployee))
        Me.pnlHoldunholdemployee = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.chkPostTimesheet = New System.Windows.Forms.CheckBox
        Me.btnhold = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUnhold = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.elDateInfo = New eZee.Common.eZeeLine
        Me.objDepartment = New System.Windows.Forms.Label
        Me.objShift = New System.Windows.Forms.Label
        Me.objEmployee = New System.Windows.Forms.Label
        Me.objlblColon3 = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objlblColon2 = New System.Windows.Forms.Label
        Me.elAssignInfo = New eZee.Common.eZeeLine
        Me.objlblColon = New System.Windows.Forms.Label
        Me.lblshift = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.pnlHoldunholdemployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlHoldunholdemployee
        '
        Me.pnlHoldunholdemployee.Controls.Add(Me.objFooter)
        Me.pnlHoldunholdemployee.Controls.Add(Me.lblDate)
        Me.pnlHoldunholdemployee.Controls.Add(Me.dtpFromDate)
        Me.pnlHoldunholdemployee.Controls.Add(Me.elDateInfo)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objDepartment)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objShift)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objEmployee)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objlblColon3)
        Me.pnlHoldunholdemployee.Controls.Add(Me.lblDepartment)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objlblColon2)
        Me.pnlHoldunholdemployee.Controls.Add(Me.elAssignInfo)
        Me.pnlHoldunholdemployee.Controls.Add(Me.objlblColon)
        Me.pnlHoldunholdemployee.Controls.Add(Me.lblshift)
        Me.pnlHoldunholdemployee.Controls.Add(Me.lblEmployee)
        Me.pnlHoldunholdemployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlHoldunholdemployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlHoldunholdemployee.Location = New System.Drawing.Point(0, 0)
        Me.pnlHoldunholdemployee.Name = "pnlHoldunholdemployee"
        Me.pnlHoldunholdemployee.Size = New System.Drawing.Size(378, 209)
        Me.pnlHoldunholdemployee.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.chkPostTimesheet)
        Me.objFooter.Controls.Add(Me.btnhold)
        Me.objFooter.Controls.Add(Me.btnUnhold)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 154)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(378, 55)
        Me.objFooter.TabIndex = 177
        '
        'chkPostTimesheet
        '
        Me.chkPostTimesheet.Checked = True
        Me.chkPostTimesheet.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPostTimesheet.Location = New System.Drawing.Point(12, 18)
        Me.chkPostTimesheet.Name = "chkPostTimesheet"
        Me.chkPostTimesheet.Size = New System.Drawing.Size(145, 19)
        Me.chkPostTimesheet.TabIndex = 3
        Me.chkPostTimesheet.Text = "Post To Timesheet"
        Me.chkPostTimesheet.UseVisualStyleBackColor = True
        '
        'btnhold
        '
        Me.btnhold.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnhold.BackColor = System.Drawing.Color.White
        Me.btnhold.BackgroundImage = CType(resources.GetObject("btnhold.BackgroundImage"), System.Drawing.Image)
        Me.btnhold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnhold.BorderColor = System.Drawing.Color.Empty
        Me.btnhold.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnhold.FlatAppearance.BorderSize = 0
        Me.btnhold.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnhold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnhold.ForeColor = System.Drawing.Color.Black
        Me.btnhold.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnhold.GradientForeColor = System.Drawing.Color.Black
        Me.btnhold.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnhold.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnhold.Location = New System.Drawing.Point(166, 12)
        Me.btnhold.Name = "btnhold"
        Me.btnhold.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnhold.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnhold.Size = New System.Drawing.Size(97, 30)
        Me.btnhold.TabIndex = 4
        Me.btnhold.Text = "&Hold"
        Me.btnhold.UseVisualStyleBackColor = True
        '
        'btnUnhold
        '
        Me.btnUnhold.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnhold.BackColor = System.Drawing.Color.White
        Me.btnUnhold.BackgroundImage = CType(resources.GetObject("btnUnhold.BackgroundImage"), System.Drawing.Image)
        Me.btnUnhold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnhold.BorderColor = System.Drawing.Color.Empty
        Me.btnUnhold.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnhold.FlatAppearance.BorderSize = 0
        Me.btnUnhold.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnhold.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnhold.ForeColor = System.Drawing.Color.Black
        Me.btnUnhold.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnhold.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnhold.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnhold.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnhold.Location = New System.Drawing.Point(166, 12)
        Me.btnUnhold.Name = "btnUnhold"
        Me.btnUnhold.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnhold.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnhold.Size = New System.Drawing.Size(97, 30)
        Me.btnUnhold.TabIndex = 2
        Me.btnUnhold.Text = "&UnHold"
        Me.btnUnhold.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(269, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(27, 126)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(117, 15)
        Me.lblDate.TabIndex = 174
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(150, 123)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(90, 21)
        Me.dtpFromDate.TabIndex = 1
        '
        'elDateInfo
        '
        Me.elDateInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elDateInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDateInfo.Location = New System.Drawing.Point(12, 104)
        Me.elDateInfo.Name = "elDateInfo"
        Me.elDateInfo.Size = New System.Drawing.Size(342, 16)
        Me.elDateInfo.TabIndex = 172
        Me.elDateInfo.Text = "Date Info"
        '
        'objDepartment
        '
        Me.objDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objDepartment.Location = New System.Drawing.Point(147, 80)
        Me.objDepartment.Name = "objDepartment"
        Me.objDepartment.Size = New System.Drawing.Size(207, 15)
        Me.objDepartment.TabIndex = 171
        Me.objDepartment.Text = "## Department"
        Me.objDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objShift
        '
        Me.objShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objShift.Location = New System.Drawing.Point(147, 55)
        Me.objShift.Name = "objShift"
        Me.objShift.Size = New System.Drawing.Size(207, 15)
        Me.objShift.TabIndex = 170
        Me.objShift.Text = "## Shift"
        Me.objShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objEmployee
        '
        Me.objEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEmployee.Location = New System.Drawing.Point(147, 31)
        Me.objEmployee.Name = "objEmployee"
        Me.objEmployee.Size = New System.Drawing.Size(207, 15)
        Me.objEmployee.TabIndex = 169
        Me.objEmployee.Text = "## Employee"
        Me.objEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblColon3
        '
        Me.objlblColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon3.Location = New System.Drawing.Point(132, 80)
        Me.objlblColon3.Name = "objlblColon3"
        Me.objlblColon3.Size = New System.Drawing.Size(9, 15)
        Me.objlblColon3.TabIndex = 168
        Me.objlblColon3.Text = ":"
        Me.objlblColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(27, 80)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(99, 15)
        Me.lblDepartment.TabIndex = 167
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblColon2
        '
        Me.objlblColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon2.Location = New System.Drawing.Point(132, 55)
        Me.objlblColon2.Name = "objlblColon2"
        Me.objlblColon2.Size = New System.Drawing.Size(9, 15)
        Me.objlblColon2.TabIndex = 166
        Me.objlblColon2.Text = ":"
        Me.objlblColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elAssignInfo
        '
        Me.elAssignInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elAssignInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAssignInfo.Location = New System.Drawing.Point(12, 6)
        Me.elAssignInfo.Name = "elAssignInfo"
        Me.elAssignInfo.Size = New System.Drawing.Size(342, 16)
        Me.elAssignInfo.TabIndex = 165
        Me.elAssignInfo.Text = "Employee Info"
        '
        'objlblColon
        '
        Me.objlblColon.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColon.Location = New System.Drawing.Point(132, 31)
        Me.objlblColon.Name = "objlblColon"
        Me.objlblColon.Size = New System.Drawing.Size(9, 15)
        Me.objlblColon.TabIndex = 164
        Me.objlblColon.Text = ":"
        Me.objlblColon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblshift
        '
        Me.lblshift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblshift.Location = New System.Drawing.Point(27, 55)
        Me.lblshift.Name = "lblshift"
        Me.lblshift.Size = New System.Drawing.Size(99, 15)
        Me.lblshift.TabIndex = 163
        Me.lblshift.Text = "Shift"
        Me.lblshift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(27, 31)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(99, 15)
        Me.lblEmployee.TabIndex = 162
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmholdunholdemployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(378, 209)
        Me.Controls.Add(Me.pnlHoldunholdemployee)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmholdunholdemployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmholdunholdemployee"
        Me.pnlHoldunholdemployee.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlHoldunholdemployee As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnhold As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents elDateInfo As eZee.Common.eZeeLine
    Friend WithEvents objDepartment As System.Windows.Forms.Label
    Friend WithEvents objShift As System.Windows.Forms.Label
    Friend WithEvents objEmployee As System.Windows.Forms.Label
    Friend WithEvents objlblColon3 As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objlblColon2 As System.Windows.Forms.Label
    Friend WithEvents elAssignInfo As eZee.Common.eZeeLine
    Friend WithEvents objlblColon As System.Windows.Forms.Label
    Friend WithEvents lblshift As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents btnUnhold As eZee.Common.eZeeLightButton
    Friend WithEvents chkPostTimesheet As System.Windows.Forms.CheckBox
End Class
