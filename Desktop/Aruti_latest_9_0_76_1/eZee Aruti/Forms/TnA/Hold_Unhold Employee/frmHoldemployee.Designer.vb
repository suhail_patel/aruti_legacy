﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHoldemployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHoldemployee))
        Me.dtpStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.pnlHoldEmployee = New System.Windows.Forms.Panel
        Me.txtFilter = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objbtnImport = New eZee.Common.eZeeSplitButton
        Me.cmnuHoldUnhold = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnHoldEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.btnUnHoldEmployees = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkPostTimesheet = New System.Windows.Forms.CheckBox
        Me.EZeeCollapsibleContainer2 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objChkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOperation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboOperationType = New System.Windows.Forms.ComboBox
        Me.LblOperationType = New System.Windows.Forms.Label
        Me.pnlHoldEmployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuHoldUnhold.SuspendLayout()
        Me.EZeeCollapsibleContainer2.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterEmployee.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpStartdate
        '
        Me.dtpStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartdate.Location = New System.Drawing.Point(69, 30)
        Me.dtpStartdate.Name = "dtpStartdate"
        Me.dtpStartdate.Size = New System.Drawing.Size(101, 21)
        Me.dtpStartdate.TabIndex = 1
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(8, 32)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(57, 16)
        Me.lblDate.TabIndex = 1
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlHoldEmployee
        '
        Me.pnlHoldEmployee.Controls.Add(Me.txtFilter)
        Me.pnlHoldEmployee.Controls.Add(Me.objFooter)
        Me.pnlHoldEmployee.Controls.Add(Me.EZeeCollapsibleContainer2)
        Me.pnlHoldEmployee.Controls.Add(Me.gbFilterEmployee)
        Me.pnlHoldEmployee.Location = New System.Drawing.Point(3, 1)
        Me.pnlHoldEmployee.Name = "pnlHoldEmployee"
        Me.pnlHoldEmployee.Size = New System.Drawing.Size(485, 470)
        Me.pnlHoldEmployee.TabIndex = 2
        '
        'txtFilter
        '
        Me.txtFilter.BackColor = System.Drawing.Color.White
        Me.txtFilter.Flags = 0
        Me.txtFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilter.InvalidChars = New Char(-1) {}
        Me.txtFilter.Location = New System.Drawing.Point(10, 69)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(467, 21)
        Me.txtFilter.TabIndex = 121
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objbtnImport)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.chkPostTimesheet)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 415)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(485, 55)
        Me.objFooter.TabIndex = 11
        '
        'objbtnImport
        '
        Me.objbtnImport.BorderColor = System.Drawing.Color.Black
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnImport.Location = New System.Drawing.Point(264, 11)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.ShowDefaultBorderColor = True
        Me.objbtnImport.Size = New System.Drawing.Size(110, 30)
        Me.objbtnImport.SplitButtonMenu = Me.cmnuHoldUnhold
        Me.objbtnImport.TabIndex = 42
        Me.objbtnImport.Text = "Opearation"
        '
        'cmnuHoldUnhold
        '
        Me.cmnuHoldUnhold.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnHoldEmployee, Me.btnUnHoldEmployees, Me.btnExportError})
        Me.cmnuHoldUnhold.Name = "cmImportAccrueLeave"
        Me.cmnuHoldUnhold.Size = New System.Drawing.Size(182, 70)
        '
        'btnHoldEmployee
        '
        Me.btnHoldEmployee.Name = "btnHoldEmployee"
        Me.btnHoldEmployee.Size = New System.Drawing.Size(181, 22)
        Me.btnHoldEmployee.Text = "Hold Employee(s)"
        '
        'btnUnHoldEmployees
        '
        Me.btnUnHoldEmployees.Name = "btnUnHoldEmployees"
        Me.btnUnHoldEmployees.Size = New System.Drawing.Size(181, 22)
        Me.btnUnHoldEmployees.Text = "Unhold Employee(s)"
        '
        'btnExportError
        '
        Me.btnExportError.Name = "btnExportError"
        Me.btnExportError.Size = New System.Drawing.Size(181, 22)
        Me.btnExportError.Text = "Export Error"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(380, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkPostTimesheet
        '
        Me.chkPostTimesheet.Checked = True
        Me.chkPostTimesheet.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPostTimesheet.Location = New System.Drawing.Point(10, 18)
        Me.chkPostTimesheet.Name = "chkPostTimesheet"
        Me.chkPostTimesheet.Size = New System.Drawing.Size(145, 19)
        Me.chkPostTimesheet.TabIndex = 8
        Me.chkPostTimesheet.Text = "Post To Timesheet"
        Me.chkPostTimesheet.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer2
        '
        Me.EZeeCollapsibleContainer2.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer2.Checked = False
        Me.EZeeCollapsibleContainer2.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer2.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer2.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer2.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer2.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer2.Controls.Add(Me.objbtnReset)
        Me.EZeeCollapsibleContainer2.Controls.Add(Me.objbtnSearch)
        Me.EZeeCollapsibleContainer2.Controls.Add(Me.lnkAllocation)
        Me.EZeeCollapsibleContainer2.Controls.Add(Me.objChkSelectAll)
        Me.EZeeCollapsibleContainer2.Controls.Add(Me.dgvData)
        Me.EZeeCollapsibleContainer2.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer2.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer2.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer2.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer2.HeaderHeight = 25
        Me.EZeeCollapsibleContainer2.HeaderMessage = ""
        Me.EZeeCollapsibleContainer2.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer2.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer2.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer2.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer2.Location = New System.Drawing.Point(10, 94)
        Me.EZeeCollapsibleContainer2.Name = "EZeeCollapsibleContainer2"
        Me.EZeeCollapsibleContainer2.OpenHeight = 300
        Me.EZeeCollapsibleContainer2.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer2.ShowBorder = True
        Me.EZeeCollapsibleContainer2.ShowCheckBox = False
        Me.EZeeCollapsibleContainer2.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer2.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer2.ShowDownButton = False
        Me.EZeeCollapsibleContainer2.ShowHeader = True
        Me.EZeeCollapsibleContainer2.Size = New System.Drawing.Size(467, 316)
        Me.EZeeCollapsibleContainer2.TabIndex = 119
        Me.EZeeCollapsibleContainer2.Temp = 0
        Me.EZeeCollapsibleContainer2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(438, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 259
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(415, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 258
        Me.objbtnSearch.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(313, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(94, 15)
        Me.lnkAllocation.TabIndex = 257
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objChkSelectAll
        '
        Me.objChkSelectAll.Location = New System.Drawing.Point(10, 30)
        Me.objChkSelectAll.Name = "objChkSelectAll"
        Me.objChkSelectAll.Size = New System.Drawing.Size(13, 14)
        Me.objChkSelectAll.TabIndex = 261
        Me.objChkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhCode, Me.dgcolhEmployee, Me.dgcolhOperation, Me.dgcolhMessage})
        Me.dgvData.Location = New System.Drawing.Point(4, 26)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(459, 277)
        Me.dgvData.TabIndex = 119
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhCode
        '
        Me.dgcolhCode.HeaderText = "Code"
        Me.dgcolhCode.Name = "dgcolhCode"
        Me.dgcolhCode.ReadOnly = True
        Me.dgcolhCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCode.Width = 90
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 200
        '
        'dgcolhOperation
        '
        Me.dgcolhOperation.HeaderText = "Operation"
        Me.dgcolhOperation.Name = "dgcolhOperation"
        Me.dgcolhOperation.ReadOnly = True
        Me.dgcolhOperation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMessage
        '
        Me.dgcolhMessage.HeaderText = "Message"
        Me.dgcolhMessage.Name = "dgcolhMessage"
        Me.dgcolhMessage.ReadOnly = True
        Me.dgcolhMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMessage.Width = 250
        '
        'gbFilterEmployee
        '
        Me.gbFilterEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbFilterEmployee.Checked = False
        Me.gbFilterEmployee.CollapseAllExceptThis = False
        Me.gbFilterEmployee.CollapsedHoverImage = Nothing
        Me.gbFilterEmployee.CollapsedNormalImage = Nothing
        Me.gbFilterEmployee.CollapsedPressedImage = Nothing
        Me.gbFilterEmployee.CollapseOnLoad = False
        Me.gbFilterEmployee.Controls.Add(Me.cboOperationType)
        Me.gbFilterEmployee.Controls.Add(Me.LblOperationType)
        Me.gbFilterEmployee.Controls.Add(Me.dtpStartdate)
        Me.gbFilterEmployee.Controls.Add(Me.lblDate)
        Me.gbFilterEmployee.ExpandedHoverImage = Nothing
        Me.gbFilterEmployee.ExpandedNormalImage = Nothing
        Me.gbFilterEmployee.ExpandedPressedImage = Nothing
        Me.gbFilterEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterEmployee.HeaderHeight = 25
        Me.gbFilterEmployee.HeaderMessage = ""
        Me.gbFilterEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterEmployee.HeightOnCollapse = 0
        Me.gbFilterEmployee.LeftTextSpace = 0
        Me.gbFilterEmployee.Location = New System.Drawing.Point(10, 6)
        Me.gbFilterEmployee.Name = "gbFilterEmployee"
        Me.gbFilterEmployee.OpenHeight = 300
        Me.gbFilterEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterEmployee.ShowBorder = True
        Me.gbFilterEmployee.ShowCheckBox = False
        Me.gbFilterEmployee.ShowCollapseButton = False
        Me.gbFilterEmployee.ShowDefaultBorderColor = True
        Me.gbFilterEmployee.ShowDownButton = False
        Me.gbFilterEmployee.ShowHeader = True
        Me.gbFilterEmployee.Size = New System.Drawing.Size(467, 60)
        Me.gbFilterEmployee.TabIndex = 1
        Me.gbFilterEmployee.Temp = 0
        Me.gbFilterEmployee.Text = "Filter Employee"
        Me.gbFilterEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOperationType
        '
        Me.cboOperationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperationType.FormattingEnabled = True
        Me.cboOperationType.Location = New System.Drawing.Point(303, 30)
        Me.cboOperationType.Name = "cboOperationType"
        Me.cboOperationType.Size = New System.Drawing.Size(158, 21)
        Me.cboOperationType.TabIndex = 120
        '
        'LblOperationType
        '
        Me.LblOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblOperationType.Location = New System.Drawing.Point(206, 32)
        Me.LblOperationType.Name = "LblOperationType"
        Me.LblOperationType.Size = New System.Drawing.Size(91, 16)
        Me.LblOperationType.TabIndex = 119
        Me.LblOperationType.Text = "Select Operation"
        Me.LblOperationType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmHoldemployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(495, 474)
        Me.Controls.Add(Me.pnlHoldEmployee)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHoldemployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Hold Employee"
        Me.pnlHoldEmployee.ResumeLayout(False)
        Me.pnlHoldEmployee.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuHoldUnhold.ResumeLayout(False)
        Me.EZeeCollapsibleContainer2.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterEmployee.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dtpStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents pnlHoldEmployee As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents chkPostTimesheet As System.Windows.Forms.CheckBox
    Friend WithEvents EZeeCollapsibleContainer2 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objChkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents gbFilterEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboOperationType As System.Windows.Forms.ComboBox
    Friend WithEvents LblOperationType As System.Windows.Forms.Label
    Friend WithEvents txtFilter As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOperation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnImport As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuHoldUnhold As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnHoldEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnUnHoldEmployees As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnExportError As System.Windows.Forms.ToolStripMenuItem
End Class
