﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Text
Imports System.Collections.ObjectModel

'Last Message index = 5

Public Class frmImportAttendancedata

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImportAttendancedata"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim dsDownload As DataSet = Nothing
    Dim mStrtimeSeparator As String = DateTimeFormatInfo.CurrentInfo.TimeSeparator
    Dim m_Handle As Integer = 0
    Private m_NumOfLog As Integer = 0
    Dim imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Dim imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Dim strIp As String = ""
    Dim intPort As Integer = 0
    Dim mintMachineSrNo As Integer = 0
    Dim mstrDeviceCode As String = ""
    Dim intDeviceType As Integer = -1
    Dim mstrCommunicationKey As String = ""
    Private mstrUserID As String = ""
    Private mstrPassword As String = ""
    'Sohail (29 Jun 2019) -- Start
    'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
    Private mstrDeviceModel As String = ""
    'Sohail (29 Jun 2019) -- End

    ' Start
    'INTEGRATION OF ANVIZ FOR TMJ

    'Pinkal (22-Apr-2020) -- Start
    'Enhancement - New Anviz Integration for Gran Melia.
    Dim anviz_handle As IntPtr
    'Pinkal (22-Apr-2020) -- End


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Dim mdtFromDate As Date = Nothing
    Dim mdtToDate As Date = Nothing
    'Pinkal (27-Oct-2020) -- End


    'Pinkal (30-Sep-2021)-- Start
    'DERM Enhancement : HIK Vision Biometric Integration.
    Dim mintHIKUserID As Integer = -1
    Dim m_lGetAcsEventHandle As Integer = -1
    'Pinkal (30-Sep-2021) -- End


    <DllImport("Kernel32.dll")> _
 Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.CLOCKINGRECORD, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFO, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFOEX, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Integer, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Byte, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Sub GetLocalTime(ByRef lpSystemTime As clsAnviz.SYSTEMTIME)
    End Sub

    Private ReaderNo As Integer
    Private ReaderIpAddress As String
    Private clocking As New clsAnviz.CLOCKINGRECORD()

    Private Structure SYSTEMTIME
        Dim wYear As Short
        Dim wMonth As Short
        Dim wDayOfWeek As Short
        Dim wDay As Short
        Dim wHour As Short
        Dim wMinute As Short
        Dim wSecond As Short
        Dim wMilliseconds As Short
    End Structure
    Dim IDNumber As Int32

    Private Declare Sub GetLocalTime Lib "kernel32" (ByRef lpSystemTime As SYSTEMTIME)

    ' End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As ExecutionEngineException

        Catch ex As AccessViolationException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Form Event"

    Private Sub frmImportAttendancedata_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Pinkal (22-Apr-2020) -- Start
        'Enhancement - New Anviz Integration for Gran Melia.
        If anviz_handle <> IntPtr.Zero Then clsAnvizNew.CChex_Stop(anviz_handle)
        'Pinkal (22-Apr-2020) -- End
    End Sub

    Private Sub frmImportAttendancedata_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()


            If ConfigParameter._Object._PolicyManagementTNA Then
                Dim objPeriod As New clscommom_period_Tran
                Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    dtpfromdate.MinDate = objPeriod._TnA_StartDate.Date
                    dtpTodate.MinDate = objPeriod._TnA_StartDate.Date
                End If
            Else
                dtpfromdate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpfromdate.MaxDate = FinancialYear._Object._Database_End_Date
                dtpTodate.MinDate = FinancialYear._Object._Database_Start_Date
                dtpTodate.MaxDate = FinancialYear._Object._Database_End_Date
            End If

            dtpfromdate.Select()


            'Pinkal (22-Apr-2020) -- Start
            'Enhancement - New Anviz Integration for Gran Melia.

            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") = False Then Exit Sub

            Dim dsMachineSetting As New DataSet
            dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
                Dim drRow As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid = '" & Company._Object._Companyunkid & "' AND  commdeviceid = " & enFingerPrintDevice.NewAnviz)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    clsAnvizNew.CChex_Init()
                    anviz_handle = clsAnvizNew.CChex_Start()
                    If anviz_handle = IntPtr.Zero Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Startup errors,Please restart the program."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                'Pinkal (28-Oct-2021)-- Start
                'Problem in Assigning Leave Accrue Issue.

                'Pinkal (30-Nov-2021)-- Start
                'Kadco getting Error when loading this form.
                ' Dim dHIKRow = dsMachineSetting.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("companyunkid") = Company._Object._Companyunkid And x.Field(Of Integer)("commdeviceid") = enFingerPrintDevice.HIKVision).ToList()
                Dim dHIKRow As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid = '" & Company._Object._Companyunkid & "' AND  commdeviceid = " & enFingerPrintDevice.HIKVision)
                'Pinkal (30-Nov-2021) -- End
                If dHIKRow IsNot Nothing AndAlso dHIKRow.Length > 0 Then
                    CHCNetSDK.NET_DVR_Init()
                End If
                'Pinkal (28-Oct-2021)-- End

            End If
            'Pinkal (22-Apr-2020) -- End

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            'Call FillCombo()
            'Pinkal (27-Mar-2017) -- End

            'Pinkal (27-Oct-2020) -- Start
            'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.

            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            'If ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString.Trim <> "" Then
            If ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().ToString <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString.Trim <> "" Then
                'Pinkal (27-Apr-2021) -- End
                GetAutomaticDownloadedData()
            End If
            'Pinkal (27-Oct-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAttendancedata_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportAttendancedata_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAttendancedata_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImportAttendancedata_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try


            'Pinkal (30-Sep-2021)-- Start
            'DERM Enhancement : HIK Vision Biometric Integration.
            If ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "00:00:00" OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "00:00" _
            OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "12:00:00 AM" OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString.Trim() = "12:00 AM" _
            OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() = "" Then
                'Pinkal (30-Sep-2021) -- End

                Dim objLogin As New clslogin_Tran
                If objLogin.DeleteDeviceAttendanceData() = False Then
                    eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
                End If
                objLogin = Nothing
            End If
            'Pinkal (27-Apr-2021) -- End

            'Pinkal (30-Sep-2021)-- Start
            'DERM Enhancement : HIK Vision Biometric Integration.
            If mintHIKUserID >= 0 Then
                CHCNetSDK.NET_DVR_Logout_V30(mintHIKUserID)
                mintHIKUserID = -1
                CHCNetSDK.NET_DVR_Cleanup()
            End If
            'Pinkal (30-Sep-2021) -- End

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As ExecutionEngineException

        Catch ex As AccessViolationException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAttendancedata_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            FingerPrintDevice.SetMessages()
            objfrm._Other_ModuleNames = "FingerPrintDevice"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Function Validation() As Boolean
        Try

            'Pinkal (27-Oct-2020) -- Start
            'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
            If lnkGetDeviceList.Enabled AndAlso lnkGetDeviceList.Enabled Then
            If lvDeviceList.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "There is no device configured in Aruti Configuration.Please configure atleast one device in order to do further operation on it."), enMsgBoxStyle.Information)
                Return False
            ElseIf lvDeviceList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select atleast device from list to do further operation on it."), enMsgBoxStyle.Information)
                Return False
            End If
            End If
            'Pinkal (27-Oct-2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "validation", mstrModuleName)
        End Try
        Return True
    End Function


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    '    Private Sub DownloadZkData()
    '        Dim objZKConnection As New zkemkeeper.CZKEM
    '        Try
    '            Dim idwVerifyMode As Integer
    '            Dim idwInOutMode As Integer = -1
    '            Dim mstrTime As String = ""
    '            Dim idwErrorCode As Integer

    '            If dsDownload Is Nothing Then
    '                CreateDataset()
    '            End If

    '            If mstrCommunicationKey.Trim.Length > 0 Then
    '                objZKConnection.SetCommPassword(CInt(mstrCommunicationKey))
    '            End If

    '            Dim bIsConnected As Boolean = objZKConnection.Connect_Net(strIp, intPort)
    '            If bIsConnected = False Then
    '                objZKConnection.GetLastError(idwErrorCode)
    '                objZKConnection.Disconnect()
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                objZKConnection = Nothing
    '                Exit Sub
    '            Else
    '                objZKConnection.RegEvent(1, 65535)
    '            End If

    '            Dim sOption As String = "~ZKFPVersion"
    '            Dim sValue As String = ""
    '            objZKConnection.GetSysOption(mintMachineSrNo, sOption, sValue)

    '            Dim mstrPlatform As String = ""
    '            objZKConnection.GetPlatform(mintMachineSrNo, mstrPlatform)

    '            objZKConnection.EnableDevice(mintMachineSrNo, False)
    '            'CType(objZKConnection, zkemkeeper.CZKEM).EnableDevice(mintMachineSrNo, False) 'disable the device 

    '            If objZKConnection.ReadGeneralLogData(mintMachineSrNo) Then 'read all the attendance records to the memory 
    '                Dim objEmployee As New clsEmployee_Master
    '                Dim objshift As New clsNewshift_master
    '                Dim objDeviceUser As New clsEmpid_devicemapping
    '                If sValue = "" OrElse sValue = "9" Then

    '                    If mstrCommunicationKey.Trim.Length > 0 Then



    '                        'Pinkal (16-May-2016) -- Start
    '                        'Enhancement For ZKTECO Device which is having algorithem 9.0 with new Method of Data Getting From Device.
    'Alg9newDevice:
    '                        'Pinkal (16-May-2016) -- End


    '                        Dim idwYear As Integer
    '                        Dim idwMonth As Integer
    '                        Dim idwDay As Integer
    '                        Dim idwHour As Integer
    '                        Dim idwMinute As Integer
    '                        Dim idwSecond As Integer
    '                        Dim idwWorkcode As Integer
    '                        Dim sdwEnrollNumber As String = ""

    '                        While objZKConnection.SSR_GetGeneralLogData(mintMachineSrNo, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)

    '                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

    '                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
    '                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then

    '                                Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                                objEmployee = New clsEmployee_Master
    '                                Dim intEmpID As Integer = 0
    '                                If chkUseMappedDeviceUser.Checked = True Then
    '                                    intEmpID = objDeviceUser.GetEmployeeUnkID(sdwEnrollNumber.ToString)
    '                                Else
    '                                    intEmpID = CInt(sdwEnrollNumber)
    '                                End If
    '                                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

    '                                If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                                    If chkUseMappedDeviceUser.Checked = True Then
    '                                        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT MAPPED ***"
    '                                    Else
    '                                        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    End If

    '                                    drRow("EmployeeCode") = ""
    '                                    drRow("EmployeeId") = -1
    '                                    drRow("ShiftId") = 0
    '                                    drRow("ShiftName") = ""
    '                                    drRow("IsError") = True
    '                                    drRow.Item("Message") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                Else
    '                                    drRow("EmployeeId") = intEmpID
    '                                    drRow("EmployeeCode") = objEmployee._Employeecode
    '                                    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                                    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                                    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
    '                                    drRow("ShiftId") = objshift._Shiftunkid
    '                                    drRow("ShiftName") = objshift._Shiftname
    '                                    drRow("IsError") = False
    '                                    drRow.Item("Message") = ""
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                End If
    '                                drRow("VerifyMode") = idwVerifyMode
    '                                drRow("InOutMode") = idwInOutMode
    '                                drRow("Logindate") = CDate(mstrTime).Date
    '                                drRow("Logintime") = CDate(mstrTime)
    '                                drRow("Device") = mstrDeviceCode
    '                                dsDownload.Tables("Download").Rows.Add(drRow)
    '                                dsDownload.AcceptChanges()

    '                            End If

    '                        End While

    '                    ElseIf mstrCommunicationKey.Trim.Length <= 0 Then

    'FetchData:
    '                        'Pinkal (16-May-2016) -- Start
    '                        'Enhancement For ZKTECO Device which is having algorithem 9.0 with new Method of Data Getting From Device.
    '                        If mstrPlatform.ToString.Trim.ToUpper.Contains("_TFT") Then
    '                            GoTo Alg9newDevice
    '                        End If
    '                        'Pinkal (16-May-2016) -- End



    '                        Dim idwEnrollNumber As Integer = 0

    '                        While objZKConnection.GetGeneralLogDataStr(mintMachineSrNo, idwEnrollNumber, idwVerifyMode, idwInOutMode, mstrTime)

    '                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
    '                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then

    '                                Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                                objEmployee = New clsEmployee_Master
    '                                Dim intEmpID As Integer = 0
    '                                If chkUseMappedDeviceUser.Checked = True Then
    '                                    intEmpID = objDeviceUser.GetEmployeeUnkID(idwEnrollNumber.ToString)
    '                                Else
    '                                    intEmpID = idwEnrollNumber
    '                                End If

    '                                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

    '                                If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                                    If chkUseMappedDeviceUser.Checked = True Then
    '                                        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT MAPPED ***"
    '                                    Else
    '                                        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    End If
    '                                    drRow("EmployeeCode") = ""
    '                                    drRow("EmployeeId") = -1
    '                                    drRow("ShiftId") = 0
    '                                    drRow("ShiftName") = ""
    '                                    drRow("IsError") = True
    '                                    drRow.Item("Message") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                Else
    '                                    drRow("EmployeeId") = intEmpID
    '                                    drRow("EmployeeCode") = objEmployee._Employeecode
    '                                    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                                    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                                    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
    '                                    drRow("ShiftId") = objshift._Shiftunkid
    '                                    drRow("ShiftName") = objshift._Shiftname
    '                                    drRow("IsError") = False
    '                                    drRow.Item("Message") = ""
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                End If
    '                                drRow("VerifyMode") = idwVerifyMode
    '                                drRow("InOutMode") = idwInOutMode
    '                                drRow("Logindate") = CDate(mstrTime).Date
    '                                drRow("Logintime") = CDate(mstrTime)
    '                                drRow("Device") = mstrDeviceCode
    '                                dsDownload.Tables("Download").Rows.Add(drRow)
    '                                dsDownload.AcceptChanges()

    '                            End If

    '                        End While

    '                    End If


    '                ElseIf sValue = "10" OrElse sValue = "1" Then

    '                    If mstrPlatform.ToString.Trim.ToUpper.Contains("_TFT") Then

    '                        Dim idwYear As Integer
    '                        Dim idwMonth As Integer
    '                        Dim idwDay As Integer
    '                        Dim idwHour As Integer
    '                        Dim idwMinute As Integer
    '                        Dim idwSecond As Integer
    '                        Dim idwWorkcode As Integer
    '                        Dim sdwEnrollNumber As String = ""

    '                        While objZKConnection.SSR_GetGeneralLogData(mintMachineSrNo, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)

    '                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

    '                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
    '                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then

    '                                Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                                objEmployee = New clsEmployee_Master
    '                                Dim intEmpID As Integer = 0
    '                                If chkUseMappedDeviceUser.Checked = True Then
    '                                    intEmpID = objDeviceUser.GetEmployeeUnkID(sdwEnrollNumber.ToString)
    '                                Else
    '                                    intEmpID = CInt(sdwEnrollNumber)
    '                                End If

    '                                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

    '                                If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                                    If chkUseMappedDeviceUser.Checked = True Then
    '                                        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT MAPPED ***"
    '                                    Else
    '                                        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    End If
    '                                    drRow("EmployeeCode") = ""
    '                                    drRow("EmployeeId") = -1
    '                                    drRow("ShiftId") = 0
    '                                    drRow("ShiftName") = ""
    '                                    drRow("IsError") = True
    '                                    drRow.Item("Message") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                Else
    '                                    drRow("EmployeeId") = intEmpID
    '                                    drRow("EmployeeCode") = objEmployee._Employeecode
    '                                    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                                    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                                    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
    '                                    drRow("ShiftId") = objshift._Shiftunkid
    '                                    drRow("ShiftName") = objshift._Shiftname
    '                                    drRow("IsError") = False
    '                                    drRow.Item("Message") = ""
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                End If
    '                                drRow("VerifyMode") = idwVerifyMode
    '                                drRow("InOutMode") = idwInOutMode
    '                                drRow("Logindate") = CDate(mstrTime).Date
    '                                drRow("Logintime") = CDate(mstrTime)
    '                                drRow("Device") = mstrDeviceCode
    '                                dsDownload.Tables("Download").Rows.Add(drRow)
    '                                dsDownload.AcceptChanges()

    '                            End If
    '                        End While

    '                    Else
    '                        GoTo FetchData
    '                    End If

    '                End If

    '            Else
    '                objZKConnection.GetLastError(idwErrorCode)
    '                objZKConnection.Disconnect()
    '                If idwErrorCode <> 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)
    '                Else
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
    '                End If
    '                Exit Sub
    '            End If


    '            objZKConnection.EnableDevice(mintMachineSrNo, True)
    '            objZKConnection.Disconnect()

    '        Catch ex As Runtime.InteropServices.SEHException

    '        Catch ex As AccessViolationException

    '        Catch ex As ExecutionEngineException

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "DownloadZkData", mstrModuleName)
    '        Finally
    '            objZKConnection = Nothing
    '        End Try
    '    End Sub

    Private Sub DownloadZkData()
        Dim objZKConnection As New zkemkeeper.CZKEM
        Try
            Dim idwVerifyMode As Integer
            Dim idwInOutMode As Integer = -1
            Dim mstrTime As String = ""
            Dim idwErrorCode As Integer

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            If mstrCommunicationKey.Trim.Length > 0 Then
                objZKConnection.SetCommPassword(CInt(mstrCommunicationKey))
            End If

            Dim bIsConnected As Boolean = objZKConnection.Connect_Net(strIp, intPort)
            If bIsConnected = False Then
                objZKConnection.GetLastError(idwErrorCode)
                objZKConnection.Disconnect()

                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")
                'Pinkal (27-Mar-2017) -- End

                objZKConnection = Nothing
                Exit Sub
            Else
                objZKConnection.RegEvent(1, 65535)
            End If

            Dim sOption As String = "~ZKFPVersion"
            Dim sValue As String = ""
            objZKConnection.GetSysOption(mintMachineSrNo, sOption, sValue)

            Dim mstrPlatform As String = ""
            objZKConnection.GetPlatform(mintMachineSrNo, mstrPlatform)

            objZKConnection.EnableDevice(mintMachineSrNo, False)


            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing

            'Pinkal (27-Mar-2017) -- End


            If objZKConnection.ReadGeneralLogData(mintMachineSrNo) Then 'read all the attendance records to the memory 
                Dim objEmployee As New clsEmployee_Master
                Dim objshift As New clsNewshift_master
                Dim objDeviceUser As New clsEmpid_devicemapping

                If sValue = "" OrElse sValue = "9" Then

                    If mstrCommunicationKey.Trim.Length > 0 Then

Alg9newDevice:

                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objZKConnection.SSR_GetGeneralLogData(mintMachineSrNo, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString


                            'Pinkal (27-Apr-2018) -- Start
                            'Bug - Download Attendance Issue for Day/Night shift employee for B5 Plus.
                            'If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                            '   And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then
                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                            And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then
                                'Pinkal (27-Apr-2018) -- End

                                'Pinkal (27-Mar-2017) -- Start
                                'Enhancement - Working On Import Device Attendance Data.

                                'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                                'objEmployee = New clsEmployee_Master
                                'Dim intEmpID As Integer = 0
                                'If chkUseMappedDeviceUser.Checked = True Then
                                '    intEmpID = objDeviceUser.GetEmployeeUnkID(sdwEnrollNumber.ToString)
                                'Else
                                '    intEmpID = CInt(sdwEnrollNumber)
                                'End If
                                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                                'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                                '    If chkUseMappedDeviceUser.Checked = True Then
                                '        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT MAPPED ***"
                                '    Else
                                '        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    End If

                                '    drRow("EmployeeCode") = ""
                                '    drRow("EmployeeId") = -1
                                '    drRow("ShiftId") = 0
                                '    drRow("ShiftName") = ""
                                '    drRow("IsError") = True
                                '    drRow.Item("Message") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                    'Else
                                '    drRow("EmployeeId") = intEmpID
                                '    drRow("EmployeeCode") = objEmployee._Employeecode
                                '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                                '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                                '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
                                '    drRow("ShiftId") = objshift._Shiftunkid
                                '    drRow("ShiftName") = objshift._Shiftname
                                '    drRow("IsError") = False
                                '    drRow.Item("Message") = ""
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                    'End If
                                'drRow("VerifyMode") = idwVerifyMode
                                'drRow("InOutMode") = idwInOutMode
                                'drRow("Logindate") = CDate(mstrTime).Date
                                'drRow("Logintime") = CDate(mstrTime)
                                'drRow("Device") = mstrDeviceCode
                                'dsDownload.Tables("Download").Rows.Add(drRow)
                                'dsDownload.AcceptChanges()

                                If InsertDeviceData(mstrDeviceCode, strIp, sdwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                    End If

                                'Pinkal (27-Mar-2017) -- End

                            End If

                        End While

                    ElseIf mstrCommunicationKey.Trim.Length <= 0 Then

FetchData:

                        If mstrPlatform.ToString.Trim.ToUpper.Contains("_TFT") Then
                            GoTo Alg9newDevice
                        End If


                        Dim idwEnrollNumber As Integer = 0

                        While objZKConnection.GetGeneralLogDataStr(mintMachineSrNo, idwEnrollNumber, idwVerifyMode, idwInOutMode, mstrTime)

                            'Pinkal (27-Apr-2018) -- Start
                            'Bug - Download Attendance Issue for Day/Night shift employee for B5 Plus.
                            'If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                            '   And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then
                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                             And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then
                                'Pinkal (27-Apr-2018) -- End

                                'Pinkal (27-Mar-2017) -- Start
                                'Enhancement - Working On Import Device Attendance Data.

                                'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                                'objEmployee = New clsEmployee_Master
                                'Dim intEmpID As Integer = 0
                                'If chkUseMappedDeviceUser.Checked = True Then
                                '    intEmpID = objDeviceUser.GetEmployeeUnkID(idwEnrollNumber.ToString)
                                'Else
                                '    intEmpID = idwEnrollNumber
                                'End If

                                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                                'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                                '    If chkUseMappedDeviceUser.Checked = True Then
                                '        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT MAPPED ***"
                                '    Else
                                '        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    End If
                                '    drRow("EmployeeCode") = ""
                                '    drRow("EmployeeId") = -1
                                '    drRow("ShiftId") = 0
                                '    drRow("ShiftName") = ""
                                '    drRow("IsError") = True
                                '    drRow.Item("Message") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'Else
                                '    drRow("EmployeeId") = intEmpID
                                '    drRow("EmployeeCode") = objEmployee._Employeecode
                                '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                                '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                                '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
                                '    drRow("ShiftId") = objshift._Shiftunkid
                                '    drRow("ShiftName") = objshift._Shiftname
                                '    drRow("IsError") = False
                                '    drRow.Item("Message") = ""
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'End If
                                'drRow("VerifyMode") = idwVerifyMode
                                'drRow("InOutMode") = idwInOutMode
                                'drRow("Logindate") = CDate(mstrTime).Date
                                'drRow("Logintime") = CDate(mstrTime)
                                'drRow("Device") = mstrDeviceCode
                                'dsDownload.Tables("Download").Rows.Add(drRow)
                                'dsDownload.AcceptChanges()

                                If InsertDeviceData(mstrDeviceCode, strIp, idwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                    End If

                                'Pinkal (27-Mar-2017) -- End

                            End If

                        End While


                    End If


                ElseIf sValue = "10" OrElse sValue = "1" Then

                    If mstrPlatform.ToString.Trim.ToUpper.Contains("_TFT") Then

                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objZKConnection.SSR_GetGeneralLogData(mintMachineSrNo, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

                            'Pinkal (27-Apr-2018) -- Start
                            'Bug - Download Attendance Issue for Day/Night shift employee for B5 Plus.
                            'If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                            '   And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then
                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then
                                'Pinkal (27-Apr-2018) -- End


                                'Pinkal (27-Mar-2017) -- Start
                                'Enhancement - Working On Import Device Attendance Data.

                                'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                                'objEmployee = New clsEmployee_Master
                                'Dim intEmpID As Integer = 0
                                'If chkUseMappedDeviceUser.Checked = True Then
                                '    intEmpID = objDeviceUser.GetEmployeeUnkID(sdwEnrollNumber.ToString)
                                'Else
                                '    intEmpID = CInt(sdwEnrollNumber)
                                'End If

                                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                                'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                                '    If chkUseMappedDeviceUser.Checked = True Then
                                '        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT MAPPED ***"
                                '    Else
                                '        drRow("EmployeeName") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    End If
                                '    drRow("EmployeeCode") = ""
                                '    drRow("EmployeeId") = -1
                                '    drRow("ShiftId") = 0
                                '    drRow("ShiftName") = ""
                                '    drRow("IsError") = True
                                '    drRow.Item("Message") = sdwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'Else
                                '    drRow("EmployeeId") = intEmpID
                                '    drRow("EmployeeCode") = objEmployee._Employeecode
                                '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                                '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                                '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
                                '    drRow("ShiftId") = objshift._Shiftunkid
                                '    drRow("ShiftName") = objshift._Shiftname
                                '    drRow("IsError") = False
                                '    drRow.Item("Message") = ""
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'End If
                                'drRow("VerifyMode") = idwVerifyMode
                                'drRow("InOutMode") = idwInOutMode
                                'drRow("Logindate") = CDate(mstrTime).Date
                                'drRow("Logintime") = CDate(mstrTime)
                                'drRow("Device") = mstrDeviceCode
                                'dsDownload.Tables("Download").Rows.Add(drRow)
                                'dsDownload.AcceptChanges()

                                If InsertDeviceData(mstrDeviceCode, strIp, sdwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                End If

                                'Pinkal (27-Mar-2017) -- End

                                    End If

                        End While

                    Else
                        GoTo FetchData
                    End If

                End If

            Else
                objZKConnection.GetLastError(idwErrorCode)
                objZKConnection.Disconnect()

                If idwErrorCode <> 0 Then

                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)
                    ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode)
                    'Pinkal (27-Mar-2017) -- End
                Else
                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
                    ListViewError(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."))
                    'Pinkal (27-Mar-2017) -- End
                End If
                Exit Sub
            End If

            objZKConnection.EnableDevice(mintMachineSrNo, True)
            objZKConnection.Disconnect()

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            Dim lvItem As ListViewItem = lvDeviceList.FindItemWithText(strIp, True, 0)
            If lvItem IsNot Nothing Then
                lvItem.ForeColor = Color.Green
            End If
            'Pinkal (27-Mar-2017) -- End

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            objZKConnection.EnableDevice(mintMachineSrNo, True)
            objZKConnection.Disconnect()
            DisplayError.Show("-1", ex.Message, "DownloadZkData", mstrModuleName)
        Finally
            objZKConnection = Nothing
        End Try
    End Sub

    'Pinkal (27-Mar-2017) -- End


    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    'Private Sub SaveAttendanceData()
    Private Sub SaveAttendanceData(ByRef mstrMsg As String)
        'Pinkal (27-Oct-2020) -- End

        Dim dsList As DataSet = Nothing
        Me.Cursor = Cursors.WaitCursor
        Dim intEmployeeId As Integer = -1
        Dim intShiftId As Integer = -1
        Dim dtLogindate As Date = Nothing
        Dim dtCheckIntime As DateTime = Nothing
        Dim dtCheckOuttime As DateTime = Nothing
        Dim objLogin As clslogin_Tran = Nothing
        Try

            Dim dtTable As DataTable = CType(dgDownloaddata.DataSource, DataTable)

            If dtTable IsNot Nothing Then  ' dtTable IsNot Nothing 


                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'Dim objLogin As clslogin_Tran
                Dim objImgConverter As New ImageConverter
                'Pinkal (27-Mar-2017) -- End
                Dim mblnFlag As Boolean = True
                If dtTable.Rows.Count > 0 Then   'dtTable.Rows.Count > 0
                    For Each dr As DataRow In dtTable.Rows
                        Dim mblnAttendanceOnDeviceInOutStatus As Boolean = False

                        If CInt(dr.Item("EmployeeId")) = -1 Then
                            'Pinkal (27-Mar-2017) -- Start
                            'Enhancement - Working On Import Device Attendance Data.
                            'dr.Item("image") = imgError
                            dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                            'Pinkal (27-Mar-2017) -- End
                            mblnFlag = False
                            Continue For
                        End If

                        Dim objShiftTran As New clsshift_tran

                        Dim objDeviceUser As New clsEmpid_devicemapping
                        objLogin = New clslogin_Tran
                        dsList = objLogin.GetEmployeeLoginDate("List", CInt(dr("EmployeeId")), CDate(dr("Logintime")))

                        If dsList.Tables("List").Rows.Count <= 0 Then   'dsList.Tables("List").Rows.Count <= 0

                            objLogin._Employeeunkid = CInt(dr("EmployeeId"))
                            objLogin._SourceType = enInOutSource.Import
                            objLogin._Userunkid = User._Object._Userunkid


                            'Pinkal (25-Jul-2018) -- Start
                            'Bug - Resolved Bug for TRZ for Ticket No [2399] unable to process payroll due to the Employee Absent Process, while it has been done multiple times.
                            intEmployeeId = CInt(dr("EmployeeId"))
                            'Pinkal (25-Jul-2018) -- End



                            If ConfigParameter._Object._FirstCheckInLastCheckOut = False Then

                                Dim objEmpShift As New clsEmployee_Shift_Tran
                                Dim intCurrentShiftID As Integer = 0
                                intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                If intCurrentShiftID <= 0 Then
                                    objLogin._Shiftunkid = CInt(dr("shiftid"))
                                Else
                                    objLogin._Shiftunkid = intCurrentShiftID
                                End If

                                objLogin._Logindate = CDate(dr("Logindate")).Date

                                objShiftTran.GetShiftTran(objLogin._Shiftunkid)
                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logindate")).Date), False, FirstDayOfWeek.Sunday)))
                                If drShiftTran.Length > 0 Then

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid = objLogin._Shiftunkid

                                    'Pinkal (06-May-2016) -- Start
                                    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus
                                    'Pinkal (06-May-2016) -- End

                                    If objShift._IsOpenShift = False Then
                                        Dim mdtDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm:ss tt"))

                                        'Pinkal (01-Dec-2017) -- Start
                                        'Enhancement - Solved Problem when employee logged exact on Day start time.
                                        'If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) < 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                            'Pinkal (01-Dec-2017) -- End


                                            'Pinkal (06-Nov-2017) -- Start
                                            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                                            'objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)

                                            Dim mintPreviousShift As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logintime")).Date.AddDays(-1).Date, intEmployeeId)

                                            'Pinkal (22-Nov-2018) -- Start
                                            'BUG - Solved Bug For Papaaye for fetching wrong shift.
                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                            If mintPreviousShift <> intShiftId Then
                                                objShiftTran.GetShiftTran(mintPreviousShift)
                                                Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logintime")).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                If drPreviousShiftTran.Length > 0 Then
                                                    Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                    If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(dr("Logintime"))) >= 24 Then
                                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    Else
                                                        'Pinkal (18-APR-2018) -- Start
                                                        'Bug - Bug Solved for B5 Plus for Attendance Issue (Day shift Issue)
                                                        objLogin._Employeeunkid = intEmployeeId

                                                            'Pinkal (01-Apr-2019) -- Start
                                                            'Enhancement - Working on Leave Changes for NMB.
                                                            'If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1, Nothing) Then
                                                            '    objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            'Else
                                                            '    objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            'End If
                                                        If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1, Nothing) Then
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            Else
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    End If
                                                            'Pinkal (01-Apr-2019) -- End


                                                        'Pinkal (18-APR-2018) -- End
                                                    End If
                                                End If
                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                            End If

                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                            End If
                                            objPrevShift = Nothing
                                            'Pinkal (22-Nov-2018) -- End


                                            'Pinkal (06-Nov-2017) -- End

                                            'Pinkal (01-Dec-2017) -- Start
                                            'Enhancement - Solved Problem when employee logged exact on Day start time.
                                            'ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) > 0 Then
                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) >= 0 Then
                                            'Pinkal (01-Dec-2017) -- End

                                            'Pinkal (06-Nov-2017) -- Start
                                            'Enhancement - Working on B5 Plus Day Start Overlap issue.
                                            '  objLogin._Logindate = CDate(dr("Logintime")).Date
                                            'DayStartTimeChecking(objLogin, intEmployeeId, dr, objLogin._Shiftunkid)
                                             'Pinkal (05-APR-2018) -- Start
                                            'Bug - Solved for B5 Plus when downloading attendance data issue.
                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                            'DayStartTimeChecking(objLogin, intEmployeeId, dr, objLogin._Shiftunkid)
                                            'Pinkal (05-APR-2018) -- End                                            
                                             'Pinkal (06-Nov-2017) -- End

                                        End If
                                    Else
                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                    End If
                                    objShift = Nothing
                                End If

                                'Pinkal (06-May-2016) -- Start
                                'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                                'If ConfigParameter._Object._SetDeviceInOutStatus Then
                                If mblnAttendanceOnDeviceInOutStatus Then
                                    'Pinkal (06-May-2016) -- End

                                    intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                    If intCurrentShiftID <= 0 Then
                                        objLogin._Shiftunkid = CInt(dr("shiftid"))
                                    Else
                                        objLogin._Shiftunkid = intCurrentShiftID
                                    End If

                                    If CInt(dr("InOutMode")) = 0 Then 'IN STATUS

                                        objLogin._checkintime = CDate(dr("Logintime"))
                                        objLogin._Original_InTime = objLogin._checkintime
                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""


                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                        'If objLogin.Insert(FinancialYear._Object._DatabaseName, _
                                        '                   User._Object._Userunkid, _
                                        '                   FinancialYear._Object._YearUnkid, _
                                        '                   Company._Object._Companyunkid, _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   ConfigParameter._Object._UserAccessModeSetting, _
                                        '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                        '                   ConfigParameter._Object._PolicyManagementTNA, _
                                        '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                        '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then


                                        If objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                                  , User._Object._Userunkid _
                                                                  , FinancialYear._Object._YearUnkid _
                                                                  , Company._Object._Companyunkid _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                  , ConfigParameter._Object._UserAccessModeSetting _
                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                  , ConfigParameter._Object._PolicyManagementTNA _
                                                                  , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                  , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                  , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                  , False, -1, Nothing, "", "") = False Then


                                            'Pinkal (11-AUG-2017) -- End


                                            'Pinkal (27-Mar-2017) -- Start
                                            'Enhancement - Working On Import Device Attendance Data.
                                            'dr.Item("image") = imgError
                                            dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                            'Pinkal (27-Mar-2017) -- End

                                            dr.Item("message") = objLogin._Message
                                            dr.Item("IsError") = True
                                            mblnFlag = False
                                            Continue For
                                        Else

                                            'Pinkal (27-Mar-2017) -- Start
                                            'Enhancement - Working On Import Device Attendance Data.
                                            'dr.Item("image") = imgAccept
                                            dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                            'Pinkal (27-Mar-2017) -- End
                                            dr.Item("message") = ""
                                        End If

                                    ElseIf CInt(dr("InOutMode")) = 1 Then 'OUT STATUS

                                        Dim intLoginId As Integer = -1
                                        If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                            objLogin._Loginunkid = intLoginId
                                            objLogin._Checkouttime = CDate(dr("Logintime"))
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1


                                            'Pinkal (12-Jun-2017) -- Start
                                            'Enhancement - Attendance changes for Source Type Issue   .
                                            objLogin._SourceType = enInOutSource.Import
                                            'Pinkal (12-Jun-2017) -- End

                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.
                                            'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                            '                          , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                            '                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                            '                          , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                            '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                            '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then


                                            'Pinkal (01-Apr-2019) -- Start
                                            'Bug - Solved Bug for Papaaye for NIGHT SHIFT to next day any other shift then it will take morning shift on night shift day which is showing on employee timesheet report.
                                            objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(objLogin._Logindate.Date, CInt(dr("EmployeeId")))
                                            'Pinkal (01-Apr-2019) -- End


                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                      , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                    , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                    , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                    , False, -1, Nothing, "", "") = False Then

                                                'Pinkal (11-AUG-2017) -- End


                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgError
                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else

                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgAccept
                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End
                                                dr.Item("message") = ""
                                            End If
                                        End If

                                    End If

                                ElseIf mblnAttendanceOnDeviceInOutStatus = False Then


                                    'Pinkal (22-Mar-2016) -- Start
                                    'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                                    'If objLogin.GetLoginType(objLogin._Employeeunkid) = 0 Then
                                    'Dim intLoginId As Integer = -1
                                    'If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) = False Then
                                    If objLogin.GetLoginType(objLogin._Employeeunkid, objLogin._Logindate.Date) = 0 Then
                                        'Pinkal (22-Mar-2016) -- End
                                        objLogin._Logindate = CDate(dr("Logindate")).Date
                                        objLogin._checkintime = CDate(dr("Logintime"))
                                        objLogin._Original_InTime = objLogin._checkintime

                                        intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                        If intCurrentShiftID <= 0 Then
                                            objLogin._Shiftunkid = CInt(dr("shiftid"))
                                        Else
                                            objLogin._Shiftunkid = intCurrentShiftID
                                        End If

                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""



                                        'Pinkal (11-AUG-2017) -- Start
                                        'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                        'If objLogin.Insert(FinancialYear._Object._DatabaseName, _
                                        '                   User._Object._Userunkid, _
                                        '                   FinancialYear._Object._YearUnkid, _
                                        '                   Company._Object._Companyunkid, _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        '                   ConfigParameter._Object._UserAccessModeSetting, _
                                        '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                        '                   ConfigParameter._Object._PolicyManagementTNA, _
                                        '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                        '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                        If objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                                , User._Object._Userunkid _
                                                                , FinancialYear._Object._YearUnkid _
                                                                , Company._Object._Companyunkid _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                , ConfigParameter._Object._UserAccessModeSetting _
                                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                , ConfigParameter._Object._PolicyManagementTNA _
                                                                , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                , False, -1, Nothing, "", "") = False Then

                                            'Pinkal (11-AUG-2017) -- End


                                            'Pinkal (27-Mar-2017) -- Start
                                            'Enhancement - Working On Import Device Attendance Data.
                                            'dr.Item("image") = imgError
                                            dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                            'Pinkal (27-Mar-2017) -- End

                                            dr.Item("message") = objLogin._Message
                                            dr.Item("IsError") = True
                                            mblnFlag = False
                                            Continue For
                                        Else

                                            'Pinkal (27-Mar-2017) -- Start
                                            'Enhancement - Working On Import Device Attendance Data.
                                            'dr.Item("image") = imgAccept
                                            dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                            'Pinkal (27-Mar-2017) -- End

                                            dr.Item("message") = ""
                                        End If

                                    Else

                                        'Pinkal (22-Mar-2016) -- Start
                                        'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                                        objLogin._Loginunkid = objLogin._Loginunkid
                                        'objLogin._Loginunkid = intLoginId
                                        'Pinkal (22-Mar-2016) -- End

                                        objLogin._Checkouttime = CDate(dr("Logintime"))
                                        objLogin._Original_OutTime = objLogin._Checkouttime

                                        intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(objLogin._Logindate.Date, CInt(dr("EmployeeId")))

                                        If intCurrentShiftID <= 0 Then
                                            objLogin._Shiftunkid = CInt(dr("shiftid"))
                                        Else
                                            objLogin._Shiftunkid = intCurrentShiftID
                                        End If


                                        If objLogin._checkintime > objLogin._Checkouttime Then
                                            Continue For
                                        End If


                                        If objLogin._checkintime < objLogin._Checkouttime Then
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1

                                            'Pinkal (12-Jun-2017) -- Start
                                            'Enhancement - Attendance changes for Source Type Issue   .
                                            objLogin._SourceType = enInOutSource.Import
                                            'Pinkal (12-Jun-2017) -- End

                                            'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                            '                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                            '                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                            '                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                            '                            ConfigParameter._Object._DonotAttendanceinSeconds, _
                                            '                            ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                                  , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                  , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                  , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                  , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                  , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                  , False, -1, Nothing, "", "") = False Then


                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgError
                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else

                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgAccept
                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End


                                                dr.Item("message") = ""
                                            End If
                                        End If

                                    End If

                                End If


                            ElseIf ConfigParameter._Object._FirstCheckInLastCheckOut Then   'ConfigParameter._Object._FirstCheckInLastCheckOut

                                Dim objEmpShift As New clsEmployee_Shift_Tran
                                Dim intCurrentShiftID As Integer = 0

                                If IsDBNull(dr("Logindate")) Then Continue For

                                intCurrentShiftID = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logindate")).Date, CInt(dr("EmployeeId")))

                                If intCurrentShiftID <= 0 Then
                                    objLogin._Shiftunkid = CInt(dr("shiftid"))
                                Else
                                    objLogin._Shiftunkid = intCurrentShiftID
                                End If

                                objShiftTran.GetShiftTran(objLogin._Shiftunkid)
                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logindate")).Date), False, FirstDayOfWeek.Sunday)))
                                intEmployeeId = CInt(dr("EmployeeId"))
                                intShiftId = objLogin._Shiftunkid
                                dtLogindate = CDate(dr("Logindate")).Date
                                dtCheckIntime = CDate((dr("Logintime")))
                                dtCheckOuttime = CDate((dr("Logintime")))

                                If drShiftTran.Length > 0 Then   'drShiftTran.Length > 0

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid = objLogin._Shiftunkid

                                    'Pinkal (06-May-2016) -- Start
                                    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus
                                    'Pinkal (06-May-2016) -- End


                                    If objShift._IsOpenShift = False Then
                                        If IsDBNull(drShiftTran(0)("daystart_time")) Then Continue For
                                        Dim mdtDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm tt"))


                                        'Pinkal (01-Dec-2017) -- Start
                                        'Enhancement - Solved Problem when employee logged exact on Day start time.
                                        'If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) < 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                            'Pinkal (01-Dec-2017) -- End

                                            'Pinkal (06-Nov-2017) -- Start
                                            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

                                            Dim mintPreviousShift As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logintime")).Date.AddDays(-1).Date, intEmployeeId)

                                            'Pinkal (22-Nov-2018) -- Start
                                            'BUG - Solved Bug For Papaaye for fetching wrong shift.

                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                            If mintPreviousShift <> intShiftId Then
                                                objShiftTran.GetShiftTran(mintPreviousShift)
                                                Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logintime")).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                If drPreviousShiftTran.Length > 0 Then
                                                    Dim mdtPreviousDayStartTime As DateTime = CDate(CDate(dr("Logindate")).Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                    If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, CDate(dr("Logintime"))) >= 24 Then
                                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    Else
                                                        'Pinkal (18-APR-2018) -- Start
                                                        'Bug - Bug Solved for B5 Plus for Attendance Issue (Day shift Issue)
                                                        objLogin._Employeeunkid = intEmployeeId

                                                            'Pinkal (01-Apr-2019) -- Start
                                                            'Enhancement - Working on Leave Changes for NMB.

                                                            'If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1, Nothing) Then
                                                            '    objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            'Else
                                                            '    objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            'End If

                                                        If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date.AddDays(-1).Date).ToString(), "", intEmployeeId, -1, Nothing) Then
                                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                                            Else
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    End If

                                                            'Pinkal (01-Apr-2019) -- End

                                                        'Pinkal (18-APR-2018) -- End
                                                    End If
                                                End If
                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date.AddDays(-1)
                                            End If

                                            'Pinkal (06-Nov-2017) -- End
                                            Else
                                                objLogin._Logindate = CDate(dr("Logintime")).Date
                                            End If  'If objPrevShift._IsOpenShift = False Then

                                            'Pinkal (22-Nov-2018) -- End

                                            'Pinkal (01-Dec-2017) -- Start
                                            'Enhancement - Solved Problem when employee logged exact on Day start time.
                                            'ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) > 0 Then
                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, CDate(dr("Logintime"))) >= 0 Then
                                            'Pinkal (01-Dec-2017) -- Start

                                            'Pinkal (06-Nov-2017) -- Start
                                            'Enhancement - Working on B5 Plus Day Start Overlap issue.
                                            'objLogin._Logindate = CDate(dr("Logintime")).Date
                                            'DayStartTimeChecking(objLogin, intEmployeeId, dr, intShiftId)

                                            'Pinkal (05-APR-2018) -- Start
                                            'Bug - Solved for B5 Plus when downloading attendance data issue.
                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                            'DayStartTimeChecking(objLogin, intEmployeeId, dr, intShiftId)
                                            'Pinkal (05-APR-2018) -- End
                                             'Pinkal (06-Nov-2017) -- End
                                        End If
                                    Else
                                        objLogin._Logindate = CDate(dr("Logintime")).Date
                                    End If
                                    objShift = Nothing

                                    If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment = False Then

                                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                                            Dim mdtTnAStartdate As Date = Nothing
                                            Dim objPaymentTran As New clsPayment_tran
                                            Dim objMaster As New clsMasterData
                                            Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, objLogin._Logindate.Date)

                                            If intPeriodId > 0 Then
                                                Dim objTnAPeriod As New clscommom_period_Tran
                                                mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                                            End If

                                            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, intEmployeeId, mdtTnAStartdate, intPeriodId, objLogin._Logindate.Date) > 0 Then

                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgError
                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End

                                                dr.Item("message") = Language.getMessage(mstrModuleName, 13, "Sorry, Payroll payment is already done for the this employee for the selected date tenure, due to this employee will be skipped for that particular date during this process. ")
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            End If
                                        End If

                                    End If

                                    If mblnAttendanceOnDeviceInOutStatus Then  'ConfigParameter._Object._SetDeviceInOutStatus

                                        If CInt(dr("InOutMode")) = 0 Then 'IN STATUS

                                            Dim mintLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date), "", intEmployeeId, mintLoginId) Then   'objLogin.IsEmployeeLoginExist
                                                objLogin._Loginunkid = mintLoginId

                                                If objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing Then  ' objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing
                                                    objLogin._checkintime = CDate(dr("Logintime"))
                                                    objLogin._Original_InTime = objLogin._checkintime

                                                    Dim objShiftInMst As New clsNewshift_master
                                                    objShiftInMst._Shiftunkid = intCurrentShiftID
                                                    If objShiftInMst._MaxHrs > 0 Then  'objShiftInMst._MaxHrs > 0 
DeviceInStatus:
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1

                                                            'Pinkal (12-Jun-2017) -- Start
                                                            'Enhancement - Attendance changes for Source Type Issue   .
                                                            objLogin._SourceType = enInOutSource.Import
                                                            'Pinkal (12-Jun-2017) -- End


                                                            'Pinkal (11-AUG-2017) -- Start
                                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                            'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            '                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            '                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                            '                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                                            '                        , ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                            '                        , ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then   'objLogin.Update() = False

                                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                                 , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                 , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                 , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                 , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                 , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                 , False, -1, Nothing, "", "") = False Then

                                                                'Pinkal (11-AUG-2017) -- End


                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgError
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else

                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgAccept
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = ""
                                                            End If  'objLogin.Update() = False

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) > CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            GoTo DeviceInStatus1
                                                        End If   'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)

                                                    ElseIf objShiftInMst._MaxHrs <= 0 Then  'objShiftInMst._MaxHrs > 0 
                                                        objLogin._checkintime = CDate(dr("Logintime"))
                                                        objLogin._Original_InTime = objLogin._checkintime
                                                        GoTo DeviceInStatus
                                                    End If  'objShiftInMst._MaxHrs > 0 

                                                End If 'objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing

                                            Else
DeviceInStatus1:
                                                objLogin._Logindate = CDate(dr("Logintime")).Date

                                                objLogin._checkintime = CDate(dr("Logintime"))
                                                objLogin._Original_InTime = objLogin._checkintime
                                                objLogin._Checkouttime = Nothing
                                                objLogin._Original_OutTime = Nothing
                                                objLogin._InOutType = 0
                                                objLogin._Workhour = 0
                                                objLogin._Holdunkid = 0
                                                objLogin._Voiddatetime = Nothing
                                                objLogin._Isvoid = False
                                                objLogin._Voidreason = ""


                                                'Pinkal (11-AUG-2017) -- Start
                                                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                'If objLogin.Insert(FinancialYear._Object._DatabaseName, _
                                                '                   User._Object._Userunkid, _
                                                '                   FinancialYear._Object._YearUnkid, _
                                                '                   Company._Object._Companyunkid, _
                                                '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                '                   ConfigParameter._Object._UserAccessModeSetting, _
                                                '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                '                   ConfigParameter._Object._PolicyManagementTNA, _
                                                '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then  'objLogin.Insert

                                                If objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                                        , User._Object._Userunkid _
                                                                        , FinancialYear._Object._YearUnkid _
                                                                        , Company._Object._Companyunkid _
                                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                        , ConfigParameter._Object._UserAccessModeSetting _
                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                        , ConfigParameter._Object._PolicyManagementTNA _
                                                                        , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                        , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                        , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                        , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                        , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                        , False, -1, Nothing, "", "") = False Then  'objLogin.Insert


                                                    'Pinkal (11-AUG-2017) -- End


                                                    'Pinkal (27-Mar-2017) -- Start
                                                    'Enhancement - Working On Import Device Attendance Data.
                                                    'dr.Item("image") = imgError
                                                    dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                    'Pinkal (27-Mar-2017) -- End

                                                    dr.Item("message") = objLogin._Message
                                                    dr.Item("IsError") = True
                                                    mblnFlag = False
                                                    Continue For
                                                Else

                                                    'Pinkal (27-Mar-2017) -- Start
                                                    'Enhancement - Working On Import Device Attendance Data.
                                                    'dr.Item("image") = imgAccept
                                                    dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                    'Pinkal (27-Mar-2017) -- End

                                                    dr.Item("message") = ""
                                                End If 'objLogin.Insert

                                            End If  'objLogin.IsEmployeeLoginExist


                                        ElseIf CInt(dr("InOutMode")) = 1 Then 'OUT STATUS
                                            Dim intLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(CDate(dr("Logintime")).Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                                objLogin._Loginunkid = intLoginId
                                                objLogin._Checkouttime = CDate(dr("Logintime"))
                                                objLogin._Original_OutTime = objLogin._Checkouttime

                                                If objLogin._checkintime <> Nothing Then
                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid = intCurrentShiftID

                                                    If objShiftMst._MaxHrs > 0 Then
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs) Then  'objShiftMst._MaxHrs > 0 
DeviceOutStatus:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1

                                                            'Pinkal (12-Jun-2017) -- Start
                                                            'Enhancement - Attendance changes for Source Type Issue   .
                                                            objLogin._SourceType = enInOutSource.Import
                                                            'Pinkal (12-Jun-2017) -- End


                                                            'Pinkal (11-AUG-2017) -- Start
                                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                            'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            '                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            '                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                            '                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                                            '                        ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                            '                        ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                               , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                               , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                               , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                               , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                               , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                               , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                               , False, -1, Nothing, "", "") = False Then

                                                                'Pinkal (11-AUG-2017) -- End


                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgError
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else

                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgAccept
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = ""
                                                            End If

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) > CInt(objShiftMst._MaxHrs) Then  ''objShiftMst._MaxHrs > 0 
DeviceOutStatus1:
                                                            objLogin._checkintime = Nothing
                                                            objLogin._Original_InTime = Nothing
                                                            objLogin._InOutType = 0
                                                            objLogin._Workhour = 0
                                                            objLogin._Holdunkid = 0
                                                            objLogin._Voiddatetime = Nothing
                                                            objLogin._Isvoid = False
                                                            objLogin._Voidreason = ""



                                                            'Pinkal (11-AUG-2017) -- Start
                                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                            'If objLogin.Insert(FinancialYear._Object._DatabaseName, _
                                                            '                   User._Object._Userunkid, _
                                                            '                   FinancialYear._Object._YearUnkid, _
                                                            '                   Company._Object._Companyunkid, _
                                                            '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            '                   ConfigParameter._Object._UserAccessModeSetting, _
                                                            '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                            '                   ConfigParameter._Object._PolicyManagementTNA, _
                                                            '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                            '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                                            If objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                                                     , User._Object._Userunkid _
                                                                                     , FinancialYear._Object._YearUnkid _
                                                                                     , Company._Object._Companyunkid _
                                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                     , ConfigParameter._Object._UserAccessModeSetting _
                                                                                     , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                                     , ConfigParameter._Object._PolicyManagementTNA _
                                                                                     , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                     , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                     , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                     , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                     , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                     , False, -1, Nothing, "", "") = False Then

                                                                'Pinkal (11-AUG-2017) -- End


                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgError
                                                                dr.Item("image") = imgError
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgAccept
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = ""
                                                            End If

                                                        End If

                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0 
                                                        If objLogin._checkintime <> Nothing Then   'objShiftMst._MaxHrs <= 0 
DeviceOutStatus2:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1

                                                            'Pinkal (12-Jun-2017) -- Start
                                                            'Enhancement - Attendance changes for Source Type Issue   .
                                                            objLogin._SourceType = enInOutSource.Import
                                                            'Pinkal (12-Jun-2017) -- End


                                                            'Pinkal (11-AUG-2017) -- Start
                                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                            'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                            '                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                            '                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                            '                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                                            '                        ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                            '                        ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                                   , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                   , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                   , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                   , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                   , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                   , False, -1, Nothing, "", "") = False Then

                                                                'Pinkal (11-AUG-2017) -- End


                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgError
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = objLogin._Message
                                                                dr.Item("IsError") = True
                                                                mblnFlag = False
                                                                Continue For
                                                            Else
                                                                'Pinkal (27-Mar-2017) -- Start
                                                                'Enhancement - Working On Import Device Attendance Data.
                                                                'dr.Item("image") = imgAccept
                                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                                'Pinkal (27-Mar-2017) -- End

                                                                dr.Item("message") = ""
                                                            End If

                                                        ElseIf objLogin._checkintime = Nothing Then   'objShift._MaxHrs <= 0 
                                                            Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(CDate(dr("Logintime")).Date, objLogin._Employeeunkid)
                                                            If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then
                                                                objLogin._Loginunkid = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                                objLogin._Checkouttime = CDate(dr("Logintime"))
                                                                objLogin._Original_OutTime = objLogin._Checkouttime
                                                                Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                                objLogin._Workhour = CInt(wkmins)
                                                                objLogin._InOutType = 1

                                                                'Pinkal (12-Jun-2017) -- Start
                                                                'Enhancement - Attendance changes for Source Type Issue   .
                                                                objLogin._SourceType = enInOutSource.Import
                                                                'Pinkal (12-Jun-2017) -- End


                                                                'Pinkal (11-AUG-2017) -- Start
                                                                'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                                                'If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                '                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                '                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                '                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                                                '                    ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                                '                    ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                                                If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                                           , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                                           , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                                           , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                                           , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                                           , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                                           , False, -1, Nothing, "", "") = False Then

                                                                    'Pinkal (11-AUG-2017) -- End


                                                                    'Pinkal (27-Mar-2017) -- Start
                                                                    'Enhancement - Working On Import Device Attendance Data.
                                                                    'dr.Item("image") = imgError
                                                                    dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                                    'Pinkal (27-Mar-2017) -- End

                                                                    dr.Item("message") = objLogin._Message
                                                                    dr.Item("IsError") = True
                                                                    mblnFlag = False
                                                                    Continue For
                                                                Else
                                                                    'Pinkal (27-Mar-2017) -- Start
                                                                    'Enhancement - Working On Import Device Attendance Data.
                                                                    'dr.Item("image") = imgAccept
                                                                    dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                                    'Pinkal (27-Mar-2017) -- End


                                                                    dr.Item("message") = ""
                                                                End If
                                                            Else
                                                                Continue For
                                                            End If
                                                        End If   'objShiftMst._MaxHrs <= 0 

                                                    End If   'objShiftMst._MaxHrs <= 0 

                                                ElseIf objLogin._checkintime = Nothing Then  'objLogin._checkintime <> Nothing
                                                    GoTo DeviceOutStatus1
                                                End If

                                            Else  'IF employee login is not exist.

                                                Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(CDate(dr("Logintime")).Date, objLogin._Employeeunkid)
                                                If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then  'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Loginunkid = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                    objLogin._Checkouttime = CDate(dr("Logintime"))
                                                    objLogin._Original_OutTime = objLogin._Checkouttime

                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid = intCurrentShiftID
                                                    If objShiftMst._MaxHrs > 0 Then   'objShiftMst._MaxHrs > 0
                                                        If objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs) Then
                                                            GoTo DeviceOutStatus
                                                        ElseIf objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) > CInt(objShiftMst._MaxHrs) Then
                                                            objLogin._Logindate = CDate(dr("Logintime")).Date
                                                            GoTo DeviceOutStatus1
                                                        End If 'objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs)
                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0
                                                        GoTo DeviceOutStatus2
                                                    End If  'objShiftMst._MaxHrs > 0
                                                Else  ' 'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Logindate = CDate(dr("Logintime")).Date
                                                    objLogin._Checkouttime = CDate(dr("Logintime"))
                                                    objLogin._Original_OutTime = objLogin._Checkouttime
                                                    GoTo DeviceOutStatus1
                                                End If  'dtDeviceTable.Rows.Count > 0

                                            End If   'IF employee login is not exist.


                                        End If   'CInt(dr("InOutMode")) = 1


                                    ElseIf mblnAttendanceOnDeviceInOutStatus = False Then  'ConfigParameter._Object._SetDeviceInOutStatus = False

                                        Dim intLoginId As Integer = -1
                                        If objLogin.IsEmployeeLoginExist(eZeeDate.convertDate(objLogin._Logindate.Date), "", CInt(dr("EmployeeId")), intLoginId) Then
                                            objLogin._Loginunkid = intLoginId
                                            objLogin._Checkouttime = CDate(dr("Logintime"))
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1

                                            'Pinkal (12-Jun-2017) -- Start
                                            'Enhancement - Attendance changes for Source Type Issue   .
                                            objLogin._SourceType = enInOutSource.Import
                                            'Pinkal (12-Jun-2017) -- End


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            '    If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                            '                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                            '                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                            '                                            , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                                            'ConfigParameter._Object._DonotAttendanceinSeconds, _
                                            'ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then


                                            'Pinkal (01-Apr-2019) -- Start
                                            'Bug - Solved Bug for Papaaye for NIGHT SHIFT to next day any other shift then it will take morning shift on night shift day which is showing on employee timesheet report.
                                            objLogin._Shiftunkid = objEmpShift.GetEmployee_Current_ShiftId(objLogin._Logindate.Date, CInt(dr("EmployeeId")))
                                            'Pinkal (01-Apr-2019) -- End


                                            If objLogin.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                    , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                                                        , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                        , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                        , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                        , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                        , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                        , False, -1, Nothing, "", "") = False Then


                                                'Pinkal (11-AUG-2017) -- End


                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgError
                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else

                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgAccept
                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End
                                                dr.Item("message") = ""
                                            End If


                                        Else
                                            'objLogin._Logindate = CDate(dr("Logindate"))
                                            objLogin._checkintime = CDate(dr("Logintime"))
                                            objLogin._Original_InTime = objLogin._checkintime
                                            objLogin._Checkouttime = Nothing
                                            objLogin._Original_OutTime = Nothing
                                            objLogin._InOutType = 0
                                            objLogin._Workhour = 0
                                            objLogin._Holdunkid = 0
                                            objLogin._Voiddatetime = Nothing
                                            objLogin._Isvoid = False
                                            objLogin._Voidreason = ""


                                            'Pinkal (11-AUG-2017) -- Start
                                            'Enhancement - Working On B5 Plus Company TnA Enhancements.

                                            'If objLogin.Insert(FinancialYear._Object._DatabaseName, _
                                            '                   User._Object._Userunkid, _
                                            '                   FinancialYear._Object._YearUnkid, _
                                            '                   Company._Object._Companyunkid, _
                                            '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            '                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            '                   ConfigParameter._Object._UserAccessModeSetting, _
                                            '                   True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                            '                   ConfigParameter._Object._PolicyManagementTNA, _
                                            '                   ConfigParameter._Object._DonotAttendanceinSeconds, _
                                            '                   ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, Nothing, "", "") = False Then

                                            If objLogin.Insert(FinancialYear._Object._DatabaseName _
                                                                    , User._Object._Userunkid _
                                                                    , FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid _
                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                    , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                    , ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                    , ConfigParameter._Object._PolicyManagementTNA _
                                                                    , ConfigParameter._Object._DonotAttendanceinSeconds _
                                                                    , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                                    , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                                    , False, -1, Nothing, "", "") = False Then


                                                'Pinkal (11-AUG-2017) -- End


                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgError
                                                dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End

                                                dr.Item("message") = objLogin._Message
                                                dr.Item("IsError") = True
                                                mblnFlag = False
                                                Continue For
                                            Else
                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'dr.Item("image") = imgAccept
                                                dr.Item("image") = objImgConverter.ConvertTo(imgAccept, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (27-Mar-2017) -- End
                                                dr.Item("message") = ""
                                            End If

                                        End If

                                    End If  'ConfigParameter._Object._SetDeviceInOutStatus


                                    Dim dtMaxEmployeeDate As DataTable = objLogin.GetEmployeeMaxLoginDate(objLogin._Logindate.Date, CInt(dr("EmployeeId")))
                                    If dtMaxEmployeeDate IsNot Nothing AndAlso dtMaxEmployeeDate.Rows.Count > 0 Then  'dtMaxEmployeeDate.Rows.Count > 0
                                        objLogin._Loginunkid = CInt(dtMaxEmployeeDate.Rows(0)("loginunkid"))

                                        If objLogin._Checkouttime = Nothing Then
                                            Dim drError() As DataRow = dtTable.Select("Logintime = '" & objLogin._Original_InTime & "' AND EmployeeId = " & objLogin._Employeeunkid)
                                            If drError.Length > 0 Then
                                                drError(0)("Message") = Language.getMessage(mstrModuleName, 10, "MisMatch Login/Logout.")

                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'drError(0)("image") = imgError

                                                'Pinkal (12-Jun-2017) -- Start
                                                'Enhancement - Attendance changes for Source Type Issue   .
                                                'dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                drError(0)("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (12-Jun-2017) -- End

                                                'Pinkal (27-Mar-2017) -- End

                                                drError(0)("IsError") = True
                                                mblnFlag = False
                                                dtTable.AcceptChanges()
                                            End If
                                        ElseIf objLogin._checkintime = Nothing Then
                                            Dim drError() As DataRow = dtTable.Select("Logintime = '" & objLogin._Original_OutTime & "' AND EmployeeId = " & objLogin._Employeeunkid)
                                            If drError.Length > 0 Then  'drError.Length > 0
                                                drError(0)("Message") = Language.getMessage(mstrModuleName, 10, "MisMatch Login/Logout.")
                                                'Pinkal (27-Mar-2017) -- Start
                                                'Enhancement - Working On Import Device Attendance Data.
                                                'drError(0)("image") = imgError

                                                'Pinkal (12-Jun-2017) -- Start
                                                'Enhancement - Attendance changes for Source Type Issue   .
                                                'dr.Item("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                drError(0)("image") = objImgConverter.ConvertTo(imgError, System.Type.GetType("System.Byte[]"))
                                                'Pinkal (12-Jun-2017) -- End

                                                'Pinkal (27-Mar-2017) -- End

                                                drError(0)("IsError") = True
                                                mblnFlag = False
                                                dtTable.AcceptChanges()
                                            End If  'drError.Length > 0

                                        End If  ' objLogin._Checkouttime = Nothing AND objLogin._checkintime = Nothing

                                    End If   'dtMaxEmployeeDate.Rows.Count > 0

                                End If   'drShiftTran.Length > 0

                            End If  'ConfigParameter._Object._FirstCheckInLastCheckOut

                        End If  'dsList.Tables("List").Rows.Count <= 0

                    Next

                    If mblnFlag Then

                        'Pinkal (27-Oct-2020) -- Start
                        'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.

                        'Pinkal (27-Apr-2021)-- Start
                        'KBC Enhancement  -  Working on Claim Retirement Enhancement.
                        'If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") Then
                        If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") Then
                            'Pinkal (27-Apr-2021) -- End
                            mstrMsg = Language.getMessage(mstrModuleName, 4, "Data successfully import.")
                        Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data successfully import."), enMsgBoxStyle.Information)
                        End If
                        'Pinkal (27-Oct-2020) -- End

                        dsDownload.Tables(0).Rows.Clear()
                        dgDownloaddata.DataSource = dsDownload.Tables(0)
                        If objLogin.DeleteDeviceAttendanceData() = False Then
                            eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
                        End If

                    Else

                        'Pinkal (27-Oct-2020) -- Start
                        'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.

                        'Pinkal (27-Apr-2021)-- Start
                        'KBC Enhancement  -  Working on Claim Retirement Enhancement.
                        'If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") Then
                        If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") Then
                            'Pinkal (27-Apr-2021) -- End
                            mstrMsg = Language.getMessage(mstrModuleName, 9, "Some Data did not import due to error.")
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Some Data did not import due to error."), enMsgBoxStyle.Information)
                        End If
                        'Pinkal (27-Oct-2020) -- End

                        dtTable = New DataView(dtTable, "IsError = 1", "", DataViewRowState.CurrentRows).ToTable()
                        dgDownloaddata.DataSource = dtTable
                        If objLogin.DeleteDeviceAttendanceData() = False Then
                            eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If



                    If ConfigParameter._Object._CurrentDateAndTime.Date > FinancialYear._Object._Database_End_Date.Date Then
                        dtpfromdate.Value = FinancialYear._Object._Database_End_Date.Date
                        dtpTodate.Value = FinancialYear._Object._Database_End_Date.Date
                    Else
                        dtpfromdate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

                        dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    End If

                    dtpfromdate.Select()
                ElseIf dtTable.Rows.Count <= 0 Then   'dtTable.Rows.Count > 0
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Download Device Data First."), enMsgBoxStyle.Information)
                    Exit Sub

                End If   'dtTable.Rows.Count > 0

            End If   ' dtTable IsNot Nothing 
        Catch ex As Exception
            Dim strMessage As String = "Employee :- " & intEmployeeId & ",Shift :-" & intShiftId & ",LoginDate :- " & dtLogindate & ",CheckIntime :- " & dtCheckIntime & ",CheckOuttime :- " & dtCheckOuttime
            DisplayError.Show("-1", ex.Message, "SaveAttendanceData :- " & strMessage, mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    'Private Sub FillCombo()
    '    Dim dsList As DataSet
    '    Try
    '        If IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
    '            dsList = New DataSet
    '            dsList.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

    '            Dim drrow As DataRow = dsList.Tables(0).NewRow
    '            drrow("deviceunkid") = 0
    '            drrow("devicecode") = Language.getMessage(mstrModuleName, 6, "Select")
    '            dsList.Tables(0).Rows.InsertAt(drrow, 0)

    '            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

    '                If i = 0 Then
    '                    cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim)
    '                Else
    '                    cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Remove(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||"), _
    '                                                                                                          dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Length - dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||")))
    '                End If

    '            Next

    '            cboDeviceCode.SelectedIndex = 0
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub DownloadBioStarData()
    '    Try
    '        Dim idwEnrollNumber As UInteger
    '        Dim deviceId As UInteger = 0
    '        Dim deviceType As Integer = 0
    '        Dim idwErrorCode As Integer

    '        If dsDownload Is Nothing Then
    '            CreateDataset()
    '        End If

    '        idwErrorCode = clsBioStar.BS_OpenSocket(strIp, intPort, m_Handle)
    '        If idwErrorCode <> clsBioStar.BS_SUCCESS Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        clsBioStar.BS_GetDeviceID(m_Handle, deviceId, deviceType)
    '        If deviceId <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If
    '        clsBioStar.BS_SetDeviceID(m_Handle, deviceId, deviceType)

    '        idwErrorCode = clsBioStar.BS_GetLogCount(m_Handle, m_NumOfLog)

    '        If idwErrorCode <> clsBioStar.BS_SUCCESS Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim logRecord As IntPtr = Marshal.AllocHGlobal(m_NumOfLog * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))

    '        Dim logTotalCount As Integer = 0
    '        Dim logCount As Integer = 0

    '        Dim nMaxLogPerTrial As Integer = 0
    '        If deviceId = clsBioStar.BS_DEVICE_BIOSTATION Then
    '            nMaxLogPerTrial = 32768
    '        Else
    '            nMaxLogPerTrial = 8192
    '        End If

    '        Do
    '            Dim buf As New IntPtr(logRecord.ToInt64() + logTotalCount * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))

    '            Dim startTime As Integer = CInt((dtpfromdate.Value.Date - CDate("01-Jan-1970")).TotalSeconds)
    '            Dim endTime As Integer = CInt((dtpTodate.Value.Date.AddDays(1) - CDate("01-Jan-1970")).TotalSeconds)

    '            If logTotalCount = 0 Then
    '                idwErrorCode = clsBioStar.BS_ReadLog(m_Handle, startTime, endTime, logCount, buf)
    '            Else
    '                idwErrorCode = clsBioStar.BS_ReadNextLog(m_Handle, startTime, endTime, logCount, buf)
    '            End If

    '            If idwErrorCode <> clsBioStar.BS_SUCCESS Then
    '                Marshal.FreeHGlobal(logRecord)

    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)

    '                Cursor.Current = Cursors.WaitCursor
    '                Exit Sub
    '            End If

    '            logTotalCount += logCount

    '        Loop While logCount = nMaxLogPerTrial

    '        Dim objDeviceUser As New clsEmpid_devicemapping
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim objshift As New clsNewshift_master
    '        Dim eventTime As New DateTime(1970, 1, 1)
    '        Dim dtDate As Date = Nothing

    '        For i As Integer = 0 To logTotalCount - 1
    '            Dim record As clsBioStar.BSLogRecord = CType(Marshal.PtrToStructure(New IntPtr(logRecord.ToInt64() + i * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord))), GetType(clsBioStar.BSLogRecord)), clsBioStar.BSLogRecord)
    '            idwEnrollNumber = CUInt(record.userID)

    '            eventTime = New DateTime(1970, 1, 1)
    '            dtDate = eventTime.AddSeconds(record.eventTime)

    '            Dim drRow As DataRow = dsDownload.Tables("Download").NewRow

    '            Dim intEmpID As Integer = 0
    '            If chkUseMappedDeviceUser.Checked = True Then
    '                intEmpID = objDeviceUser.GetEmployeeUnkID(idwEnrollNumber.ToString)
    '            Else
    '                If idwEnrollNumber > 2147483647 Then
    '                    intEmpID = 0
    '                Else
    '                    intEmpID = CInt(idwEnrollNumber)
    '                End If
    '            End If

    '            If idwEnrollNumber > 0 Then
    '                objEmployee = New clsEmployee_Master
    '                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

    '                If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                    If chkUseMappedDeviceUser.Checked = True Then
    '                        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT MAPPED ***"
    '                    Else
    '                        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                    End If
    '                    drRow("EmployeeCode") = ""
    '                    drRow("EmployeeId") = -1
    '                    drRow("ShiftId") = 0
    '                    drRow("ShiftName") = ""
    '                    drRow("IsError") = True
    '                    drRow.Item("Message") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
    '                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                Else
    '                    drRow("EmployeeId") = intEmpID
    '                    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                    drRow("EmployeeCode") = objEmployee._Employeecode
    '                    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtDate.Date, intEmpID)
    '                    drRow("ShiftId") = objshift._Shiftunkid
    '                    drRow("ShiftName") = objshift._Shiftname
    '                    drRow("IsError") = False
    '                    drRow.Item("Message") = ""
    '                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                End If
    '                drRow("VerifyMode") = 0
    '                drRow("InOutMode") = -1
    '                drRow("Logindate") = dtDate.Date
    '                drRow("Logintime") = dtDate
    '                drRow("Device") = mstrDeviceCode
    '                dsDownload.Tables("Download").Rows.Add(drRow)
    '                dsDownload.AcceptChanges()
    '            End If

    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DownloadBioStarData", mstrModuleName)
    '    Finally
    '        clsBioStar.BS_CloseSocket(m_Handle)
    '    End Try
    'End Sub

    Private Sub DownloadBioStarData()
        Try
            Dim idwEnrollNumber As UInteger
            Dim deviceId As UInteger = 0
            Dim deviceType As Integer = 0
            Dim idwErrorCode As Integer

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            idwErrorCode = clsBioStar.BS_OpenSocket(strIp, intPort, m_Handle)
            If idwErrorCode <> clsBioStar.BS_SUCCESS Then
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")
                'Pinkal (27-Mar-2017) -- End
                Exit Sub
            End If

            clsBioStar.BS_GetDeviceID(m_Handle, deviceId, deviceType)
            If deviceId <= 0 Then
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")
                'Pinkal (27-Mar-2017) -- End
                Exit Sub
            End If
            clsBioStar.BS_SetDeviceID(m_Handle, deviceId, deviceType)

            idwErrorCode = clsBioStar.BS_GetLogCount(m_Handle, m_NumOfLog)

            If idwErrorCode <> clsBioStar.BS_SUCCESS Then
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode)
                'Pinkal (27-Mar-2017) -- End
                Exit Sub
            End If

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing
            'Pinkal (27-Mar-2017) -- End

            Dim logRecord As IntPtr = Marshal.AllocHGlobal(m_NumOfLog * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))

            Dim logTotalCount As Integer = 0
            Dim logCount As Integer = 0

            Dim nMaxLogPerTrial As Integer = 0
            If deviceId = clsBioStar.BS_DEVICE_BIOSTATION Then
                nMaxLogPerTrial = 32768
            Else
                nMaxLogPerTrial = 8192
            End If

            Do
                Dim buf As New IntPtr(logRecord.ToInt64() + logTotalCount * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord)))

                Dim startTime As Integer = CInt((dtpfromdate.Value.Date - CDate("01-Jan-1970")).TotalSeconds)
                Dim endTime As Integer = CInt((dtpTodate.Value.Date.AddDays(1) - CDate("01-Jan-1970")).TotalSeconds)

                If logTotalCount = 0 Then
                    idwErrorCode = clsBioStar.BS_ReadLog(m_Handle, startTime, endTime, logCount, buf)
                Else
                    idwErrorCode = clsBioStar.BS_ReadNextLog(m_Handle, startTime, endTime, logCount, buf)
                End If

                If idwErrorCode <> clsBioStar.BS_SUCCESS Then
                    Marshal.FreeHGlobal(logRecord)
                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode, enMsgBoxStyle.Information)
                    ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode)
                    'Pinkal (27-Mar-2017) -- End
                    Cursor.Current = Cursors.WaitCursor
                    Exit Sub
                End If

                logTotalCount += logCount

            Loop While logCount = nMaxLogPerTrial

            Dim objDeviceUser As New clsEmpid_devicemapping
            Dim objEmployee As New clsEmployee_Master
            Dim objshift As New clsNewshift_master
            Dim eventTime As New DateTime(1970, 1, 1)
            Dim dtDate As Date = Nothing

            For i As Integer = 0 To logTotalCount - 1
                Dim record As clsBioStar.BSLogRecord = CType(Marshal.PtrToStructure(New IntPtr(logRecord.ToInt64() + i * Marshal.SizeOf(GetType(clsBioStar.BSLogRecord))), GetType(clsBioStar.BSLogRecord)), clsBioStar.BSLogRecord)
                idwEnrollNumber = CUInt(record.userID)

                eventTime = New DateTime(1970, 1, 1)
                dtDate = eventTime.AddSeconds(record.eventTime)

                Dim drRow As DataRow = dsDownload.Tables("Download").NewRow

                Dim intEmpID As Integer = 0
                If chkUseMappedDeviceUser.Checked = True Then
                    intEmpID = objDeviceUser.GetEmployeeUnkID(idwEnrollNumber.ToString)
                Else
                    If idwEnrollNumber > 2147483647 Then
                        intEmpID = 0
                    Else
                        intEmpID = CInt(idwEnrollNumber)
                    End If
                End If

                If idwEnrollNumber > 0 Then

                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.

                    'objEmployee = New clsEmployee_Master
                    'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
                    'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                    '    If chkUseMappedDeviceUser.Checked = True Then
                    '        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT MAPPED ***"
                    '    Else
                    '        drRow("EmployeeName") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                    '    End If
                    '    drRow("EmployeeCode") = ""
                    '    drRow("EmployeeId") = -1
                    '    drRow("ShiftId") = 0
                    '    drRow("ShiftName") = ""
                    '    drRow("IsError") = True
                    '    drRow.Item("Message") = idwEnrollNumber.ToString & " *** NOT REGISTERED ***"
                    '    drRow("image") = New Drawing.Bitmap(1, 1)
                    'Else
                    '    drRow("EmployeeId") = intEmpID
                    '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                    '    drRow("EmployeeCode") = objEmployee._Employeecode
                    '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                    '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtDate.Date, intEmpID)
                    '    drRow("ShiftId") = objshift._Shiftunkid
                    '    drRow("ShiftName") = objshift._Shiftname
                    '    drRow("IsError") = False
                    '    drRow.Item("Message") = ""
                    '    drRow("image") = New Drawing.Bitmap(1, 1)
                    'End If
                    'drRow("VerifyMode") = 0
                    'drRow("InOutMode") = -1
                    'drRow("Logindate") = dtDate.Date
                    'drRow("Logintime") = dtDate
                    'drRow("Device") = mstrDeviceCode
                    'dsDownload.Tables("Download").Rows.Add(drRow)
                    'dsDownload.AcceptChanges()

                    If InsertDeviceData(mstrDeviceCode, strIp, idwEnrollNumber.ToString(), dtDate.Date, dtDate, "0", "-1") = False Then
                        Exit For
                        End If

                    'Pinkal (27-Mar-2017) -- End

                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadBioStarData", mstrModuleName)
        Finally
            clsBioStar.BS_CloseSocket(m_Handle)
        End Try
    End Sub

    'Pinkal (27-Mar-2017) -- End

    Private Function GetEventStr(ByVal eventType As Byte) As String
        Select Case eventType
            Case clsBioStar.BE_EVENT_SCAN_SUCCESS
                Return "Scan Success"

            Case clsBioStar.BE_EVENT_ENROLL_BAD_FINGER
                Return "Enroll Bad Finger"

            Case clsBioStar.BE_EVENT_ENROLL_SUCCESS
                Return "Enroll Success"

            Case clsBioStar.BE_EVENT_ENROLL_FAIL
                Return "Enroll Fail"

            Case clsBioStar.BE_EVENT_ENROLL_CANCELED
                Return "Enroll Canceled"

            Case clsBioStar.BE_EVENT_VERIFY_BAD_FINGER
                Return "Verify Bad Finger"

            Case clsBioStar.BE_EVENT_VERIFY_SUCCESS
                Return "Verify Success"

            Case clsBioStar.BE_EVENT_VERIFY_FAIL
                Return "Verify Fail"

            Case clsBioStar.BE_EVENT_VERIFY_CANCELED
                Return "Verify Canceled"

            Case clsBioStar.BE_EVENT_VERIFY_NO_FINGER
                Return "Verify No Finger"

            Case clsBioStar.BE_EVENT_IDENTIFY_BAD_FINGER
                Return "Identify Bad Finger"

            Case clsBioStar.BE_EVENT_IDENTIFY_SUCCESS
                Return "Identify Success"

            Case clsBioStar.BE_EVENT_IDENTIFY_FAIL
                Return "Identify Fail"

            Case clsBioStar.BE_EVENT_IDENTIFY_CANCELED
                Return "Identify Canceled"

            Case clsBioStar.BE_EVENT_IDENTIFY_FINGER
                Return "Identify Success(Finger Only)"

            Case clsBioStar.BE_EVENT_IDENTIFY_FINGER_PIN
                Return "Identify Success(Finger and PIN)"

            Case clsBioStar.BE_EVENT_DELETE_BAD_FINGER
                Return "Delete Bad Finger"

            Case clsBioStar.BE_EVENT_DELETE_SUCCESS
                Return "Delete Success"

            Case clsBioStar.BE_EVENT_DELETE_FAIL
                Return "Delete Fail"

            Case clsBioStar.BE_EVENT_DELETE_ALL_SUCCESS
                Return "Delete All"

            Case clsBioStar.BE_EVENT_VERIFY_DURESS
                Return "Verify Duress"

            Case clsBioStar.BE_EVENT_IDENTIFY_DURESS
                Return "Identify Duress"

            Case clsBioStar.BE_EVENT_TAMPER_SWITCH_ON
                Return "Tamper On"

            Case clsBioStar.BE_EVENT_TAMPER_SWITCH_OFF
                Return "Tamper Off"

            Case clsBioStar.BE_EVENT_SYS_STARTED
                Return "System Started"

            Case clsBioStar.BE_EVENT_IDENTIFY_NOT_GRANTED
                Return "Identify Not Granted"

            Case clsBioStar.BE_EVENT_VERIFY_NOT_GRANTED
                Return "Verify Not Granted"

            Case clsBioStar.BE_EVENT_IDENTIFY_LIMIT_COUNT
                Return "Identify Limit Count"

            Case clsBioStar.BE_EVENT_IDENTIFY_LIMIT_TIME
                Return "Identify Limit Time"

            Case clsBioStar.BE_EVENT_IDENTIFY_DISABLED
                Return "Identify Disabled"

            Case clsBioStar.BE_EVENT_IDENTIFY_EXPIRED
                Return "Identify Expired"

            Case clsBioStar.BE_EVENT_APB_FAIL
                Return "Anti-passback Failed"

            Case clsBioStar.BE_EVENT_COUNT_LIMIT
                Return "Entrance Limit Count"

            Case clsBioStar.BE_EVENT_TIME_INTERVAL_LIMIT
                Return "Entrance Time Interval"

            Case clsBioStar.BE_EVENT_INVALID_AUTH_MODE
                Return "Invalid Auth Mode"

            Case clsBioStar.BE_EVENT_EXPIRED_USER
                Return "Expired User"

            Case clsBioStar.BE_EVENT_NOT_GRANTED
                Return "Access Not Granted"

            Case clsBioStar.BE_EVENT_DOOR0_OPEN
                Return "Door 0 Open"

            Case clsBioStar.BE_EVENT_DOOR0_CLOSED
                Return "Door 0 Closed"

            Case clsBioStar.BE_EVENT_DOOR1_OPEN
                Return "Door 1 Open"

            Case clsBioStar.BE_EVENT_DOOR1_CLOSED
                Return "Door 1 Closed"

            Case clsBioStar.BE_EVENT_DOOR0_FORCED_OPEN
                Return "Door 0 Forced Open"

            Case clsBioStar.BE_EVENT_DOOR1_FORCED_OPEN
                Return "Door 1 Forced Open"

            Case clsBioStar.BE_EVENT_DOOR0_HELD_OPEN
                Return "Door 0 Held Open"

            Case clsBioStar.BE_EVENT_DOOR1_HELD_OPEN
                Return "Door 1 Held Open"

            Case clsBioStar.BE_EVENT_DOOR0_RELAY_ON
                Return "Door 0 Relay On"

            Case clsBioStar.BE_EVENT_DOOR1_RELAY_ON
                Return "Door 1 Relay On"

            Case clsBioStar.BE_EVENT_TIMEOUT
                Return "Timeout"

                ' IO event
            Case clsBioStar.BE_EVENT_INTERNAL_INPUT0
                Return "Detect Internal Input 0"

            Case clsBioStar.BE_EVENT_INTERNAL_INPUT1
                Return "Detect Internal Input 1"

            Case clsBioStar.BE_EVENT_SECONDARY_INPUT0
                Return "Detect Secondary Input 0"

            Case clsBioStar.BE_EVENT_SECONDARY_INPUT1
                Return "Detect Secondary Input 1"

            Case clsBioStar.BE_EVENT_SIO0_INPUT0
                Return "Detect SIO 0 Input 0"

            Case clsBioStar.BE_EVENT_SIO0_INPUT1
                Return "Detect SIO 0 Input 1"

            Case clsBioStar.BE_EVENT_SIO0_INPUT2
                Return "Detect SIO 0 Input 2"

            Case clsBioStar.BE_EVENT_SIO0_INPUT3
                Return "Detect SIO 0 Input 3"

            Case clsBioStar.BE_EVENT_SIO1_INPUT0
                Return "Detect SIO 1 Input 0"

            Case clsBioStar.BE_EVENT_SIO1_INPUT1
                Return "Detect SIO 1 Input 1"

            Case clsBioStar.BE_EVENT_SIO1_INPUT2
                Return "Detect SIO 1 Input 2"

            Case clsBioStar.BE_EVENT_SIO1_INPUT3
                Return "Detect SIO 1 Input 3"

            Case clsBioStar.BE_EVENT_SIO2_INPUT0
                Return "Detect SIO 2 Input 0"

            Case clsBioStar.BE_EVENT_SIO2_INPUT1
                Return "Detect SIO 2 Input 1"

            Case clsBioStar.BE_EVENT_SIO2_INPUT2
                Return "Detect SIO 2 Input 2"

            Case clsBioStar.BE_EVENT_SIO2_INPUT3
                Return "Detect SIO 2 Input 3"

            Case clsBioStar.BE_EVENT_SIO3_INPUT0
                Return "Detect SIO 3 Input 0"

            Case clsBioStar.BE_EVENT_SIO3_INPUT1
                Return "Detect SIO 3 Input 1"

            Case clsBioStar.BE_EVENT_SIO3_INPUT2
                Return "Detect SIO 3 Input 2"

            Case clsBioStar.BE_EVENT_SIO3_INPUT3
                Return "Detect SIO 3 Input 3"

            Case clsBioStar.BE_EVENT_LOCKED
                Return "Locked"

            Case clsBioStar.BE_EVENT_UNLOCKED
                Return "Unlocked"

            Case clsBioStar.BE_EVENT_TIME_SET
                Return "Set Time"

            Case clsBioStar.BE_EVENT_SOCK_CONN
                Return "Connected"

            Case clsBioStar.BE_EVENT_SOCK_DISCONN
                Return "Disconnected"

            Case clsBioStar.BE_EVENT_SERVER_SOCK_CONN
                Return "Server Connected"

            Case clsBioStar.BE_EVENT_SERVER_SOCK_DISCONN
                Return "Server Disconnected"

            Case clsBioStar.BE_EVENT_LINK_CONN
                Return "Link Connected"

            Case clsBioStar.BE_EVENT_LINK_DISCONN
                Return "Link Disconnected"

            Case clsBioStar.BE_EVENT_INIT_IP
                Return "IP Initialized"

            Case clsBioStar.BE_EVENT_INIT_DHCP
                Return "DHCP Initialized"

            Case clsBioStar.BE_EVENT_DHCP_SUCCESS
                Return "DHCP Succeed"

            Case Else
                Return eventType.ToString()
        End Select
    End Function

    Private Function GetTNAStr(ByVal tnaEvent As UShort) As String
        Select Case tnaEvent
            Case 0
                Return "IN"

            Case 1
                Return "OUT"

            Case Else
                Return ""
        End Select
    End Function

    Private Sub CreateDataset()
        Try
            dsDownload = New DataSet()
            dsDownload.Tables.Add("Download")

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            'dsDownload.Tables("Download").Columns.Add("image", System.Type.GetType("System.Object"))
            dsDownload.Tables("Download").Columns.Add("image", System.Type.GetType("System.Byte[]"))
            'Pinkal (27-Mar-2017) -- End

            dsDownload.Tables("Download").Columns.Add("Device", Type.GetType("System.String"))
            dsDownload.Tables("Download").Columns.Add("EmployeeId", Type.GetType("System.Int32"))
            dsDownload.Tables("Download").Columns.Add("EmployeeCode", Type.GetType("System.String"))
            dsDownload.Tables("Download").Columns.Add("EmployeeName", Type.GetType("System.String"))
            dsDownload.Tables("Download").Columns.Add("ShiftId", Type.GetType("System.Int32"))
            dsDownload.Tables("Download").Columns.Add("ShiftName", Type.GetType("System.String"))
            dsDownload.Tables("Download").Columns.Add("VerifyMode", Type.GetType("System.Int32"))
            dsDownload.Tables("Download").Columns.Add("InOutMode", Type.GetType("System.Int32"))
            dsDownload.Tables("Download").Columns.Add("Logindate", Type.GetType("System.DateTime"))
            dsDownload.Tables("Download").Columns.Add("Logintime", Type.GetType("System.DateTime"))
            dsDownload.Tables("Download").Columns.Add("Message", System.Type.GetType("System.String"))
            dsDownload.Tables("Download").Columns.Add("IsError", Type.GetType("System.Boolean"))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataset", mstrModuleName)
        End Try
    End Sub

    Private Sub DeviceIntegration()
        Try
            Me.Cursor = Cursors.WaitCursor
            If intDeviceType = enFingerPrintDevice.ZKSoftware Then
                DownloadZkData()

            ElseIf intDeviceType = enFingerPrintDevice.BioStar Then
                DownloadBioStarData()

            ElseIf intDeviceType = enFingerPrintDevice.ZKAcessControl Then
                DownloadZkAccessControlData()

            ElseIf intDeviceType = enFingerPrintDevice.Anviz Then
                DownloadAnvizData()

            ElseIf intDeviceType = enFingerPrintDevice.ACTAtek Then
                DownloadACTAtekData()

                'Pinkal (12-Jun-2018) -- Start
                'Enhancement - Integrating biostar 2 with Aruti for voltamp.
            ElseIf intDeviceType = enFingerPrintDevice.BioStar2 Then
                DownloadBioStar2Data()
                'Pinkal (12-Jun-2018) -- End

                'Sohail (24 Jun 2019) -- Start
                'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
            ElseIf intDeviceType = enFingerPrintDevice.FingerTec Then
                DownloadFingerTecData()
                'Sohail (24 Jun 2019) -- End


                'Pinkal (30-Sep-2021)-- Start
                'DERM Enhancement : HIK Vision Biometric Integration.
            ElseIf intDeviceType = enFingerPrintDevice.HIKVision Then
                DownloadHIKVisionData()
                'Pinkal (30-Sep-2021) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeviceIntegration", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    'Private Sub DownloadZkAccessControlData()
    '    Dim h As IntPtr = IntPtr.Zero
    '    Try

    '        If dsDownload Is Nothing Then
    '            CreateDataset()
    '        End If

    '        Dim ret As Integer = 0
    '        Dim str As String = "protocol=TCP,ipaddress=" & strIp.Trim & ",port=" & intPort.ToString.Trim & ",timeout=2000,passwd="

    '        h = clsZKAccessControl.Connect(str)
    '        If h = IntPtr.Zero Then
    '            ret = clsZKAccessControl.PullLastError
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Connect device Failed! The error id is: ") & ret, enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        ret = 0
    '        Dim devtablename As String = "transaction"
    '        Dim devdatfilter As String = ""
    '        Dim strFileName As String = "*"
    '        Dim BUFFERSIZE As Integer = 10 * 1024 * 1024
    '        Dim buffer As Byte() = New Byte(BUFFERSIZE) {}
    '        Dim options As String = ""

    '        If IntPtr.Zero <> h Then
    '            ret = clsZKAccessControl.GetDeviceData(h, buffer(0), BUFFERSIZE, devtablename, strFileName, devdatfilter, options)
    '        Else
    '            ret = clsZKAccessControl.PullLastError
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret, enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        If (ret >= 0) Then
    '            Dim strText As String = System.Text.Encoding.Default.GetString(buffer)
    '            Dim arTransaction() As String = strText.Split(CChar(vbCrLf))

    '            If arTransaction IsNot Nothing AndAlso arTransaction.Length > 1 Then

    '                Dim objEmployee As New clsEmployee_Master
    '                Dim objshift As New clsNewshift_master
    '                Dim objDeviceUser As New clsEmpid_devicemapping
    '                System.IO.File.WriteAllText(Application.StartupPath & "\" & "write1.txt", strText)
    '                For i As Integer = 0 To arTransaction.Length - 1

    '                    If i = 0 Then Continue For

    '                    Dim arSplitField() As String = arTransaction(i).Split(CChar(","))

    '                    If arSplitField IsNot Nothing AndAlso arSplitField.Length > 1 Then

    '                        If CInt(arSplitField(1)) > 0 Then  'CHECK USER ID > 0 OR NOT

    '                            Dim mintTime As Long = Convert.ToInt64(arSplitField(6))  ' GET USER LOGIN/LOGOUT TIME
    '                            Dim mintSec As Integer = CInt(Math.Floor(mintTime Mod 60))
    '                            Dim mintMin As Integer = CInt(Math.Floor((mintTime / 60) Mod 60))
    '                            Dim mintHr As Integer = CInt(Math.Floor((mintTime / 3600) Mod 24))
    '                            Dim mintday As Integer = CInt(Math.Floor(((mintTime / 86400) Mod 31) + 1))
    '                            Dim mintmonth As Integer = CInt(Math.Floor(((mintTime / 2678400) Mod 12) + 1))
    '                            Dim mintYear As Integer = CInt(Math.Floor((mintTime / 32140800) + 2000))
    '                            Dim mdtDatetime As DateTime = CDate(DateAndTime.DateSerial(CInt(mintYear), CInt(mintmonth), CInt(mintday)) & " " & DateAndTime.TimeSerial(CInt(mintHr), CInt(mintMin), CInt(mintSec)))

    '                            If CDate(mdtDatetime).Date >= dtpfromdate.Value.Date And CDate(mdtDatetime).Date <= dtpTodate.Value.Date Then

    '                                Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                                objEmployee = New clsEmployee_Master
    '                                Dim intEmpID As Integer = 0
    '                                If chkUseMappedDeviceUser.Checked = True Then
    '                                    intEmpID = objDeviceUser.GetEmployeeUnkID(CInt(arSplitField(1)).ToString)
    '                                Else
    '                                    intEmpID = CInt(arSplitField(1))
    '                                End If

    '                                'S.SANDEEP [04 JUN 2015] -- START
    '                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                                'objEmployee._Employeeunkid = intEmpID
    '                                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
    '                                'S.SANDEEP [04 JUN 2015] -- END


    '                                If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                                    If chkUseMappedDeviceUser.Checked = True Then
    '                                        drRow("EmployeeName") = CInt(arSplitField(1)).ToString & " *** NOT MAPPED ***"
    '                                    Else
    '                                        drRow("EmployeeName") = CInt(arSplitField(1)).ToString & " *** NOT REGISTERED ***"
    '                                    End If
    '                                    drRow("EmployeeCode") = ""
    '                                    drRow("EmployeeId") = -1
    '                                    drRow("ShiftId") = 0
    '                                    drRow("ShiftName") = ""
    '                                    drRow("IsError") = True
    '                                    drRow.Item("Message") = CInt(arSplitField(1)).ToString & " *** NOT REGISTERED ***"
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                Else
    '                                    drRow("EmployeeId") = intEmpID
    '                                    drRow("EmployeeCode") = objEmployee._Employeecode
    '                                    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                                    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                                    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtDatetime.Date, intEmpID)
    '                                    drRow("ShiftId") = objshift._Shiftunkid
    '                                    drRow("ShiftName") = objshift._Shiftname
    '                                    drRow("IsError") = False
    '                                    drRow.Item("Message") = ""
    '                                    drRow("image") = New Drawing.Bitmap(1, 1)
    '                                End If

    '                                drRow("VerifyMode") = 0
    '                                'Pinkal (09-Jun-2015) -- Start
    '                                'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
    '                                'drRow("InOutMode") = 0
    '                                drRow("InOutMode") = -1
    '                                'Pinkal (09-Jun-2015) -- End

    '                                drRow("Logindate") = CDate(mdtDatetime).Date
    '                                drRow("Logintime") = CDate(mdtDatetime)
    '                                drRow("Device") = mstrDeviceCode
    '                                dsDownload.Tables("Download").Rows.Add(drRow)
    '                                dsDownload.AcceptChanges()

    '                            End If

    '                        End If

    '                    End If

    '                Next

    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
    '            End If
    '        Else
    '            ret = clsZKAccessControl.PullLastError
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret, enMsgBoxStyle.Information)
    '        End If

    '    Catch ex As Runtime.InteropServices.SEHException

    '    Catch ex As AccessViolationException

    '    Catch ex As ExecutionEngineException

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DownloadZkAccessControlData", mstrModuleName)
    '    Finally
    '        If IntPtr.Zero <> h Then
    '            clsZKAccessControl.Disconnect(h)
    '            h = IntPtr.Zero
    '        End If
    '    End Try
    'End Sub


    Private Sub DownloadZkAccessControlData()
        Dim h As IntPtr = IntPtr.Zero
        Try

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            Dim ret As Integer = 0
            Dim str As String = "protocol=TCP,ipaddress=" & strIp.Trim & ",port=" & intPort.ToString.Trim & ",timeout=2000,passwd="

            h = clsZKAccessControl.Connect(str)
            If h = IntPtr.Zero Then
                ret = clsZKAccessControl.PullLastError
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Connect device Failed! The error id is: ") & ret, enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 12, "Connect device Failed! The error id is: ") & ret)
                Exit Sub
            End If

            ret = 0
            Dim devtablename As String = "transaction"
            Dim devdatfilter As String = ""
            Dim strFileName As String = "*"
            Dim BUFFERSIZE As Integer = 10 * 1024 * 1024
            Dim buffer As Byte() = New Byte(BUFFERSIZE) {}
            Dim options As String = ""


            If IntPtr.Zero <> h Then
                ret = clsZKAccessControl.GetDeviceData(h, buffer(0), BUFFERSIZE, devtablename, strFileName, devdatfilter, options)
            Else
                ret = clsZKAccessControl.PullLastError
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret, enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret)
                Exit Sub
            End If

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing
            'Pinkal (27-Mar-2017) -- End

            If (ret >= 0) Then
                Dim strText As String = System.Text.Encoding.Default.GetString(buffer)
                Dim arTransaction() As String = strText.Split(CChar(vbCrLf))

                If arTransaction IsNot Nothing AndAlso arTransaction.Length > 1 Then

                    Dim objEmployee As New clsEmployee_Master
                    Dim objshift As New clsNewshift_master
                    Dim objDeviceUser As New clsEmpid_devicemapping
                    System.IO.File.WriteAllText(Application.StartupPath & "\" & "write1.txt", strText)
                    For i As Integer = 0 To arTransaction.Length - 1

                        If i = 0 Then Continue For

                        Dim arSplitField() As String = arTransaction(i).Split(CChar(","))

                        If arSplitField IsNot Nothing AndAlso arSplitField.Length > 1 Then

                            If CInt(arSplitField(1)) > 0 Then  'CHECK USER ID > 0 OR NOT

                                Dim mintTime As Long = Convert.ToInt64(arSplitField(6))  ' GET USER LOGIN/LOGOUT TIME
                                Dim mintSec As Integer = CInt(Math.Floor(mintTime Mod 60))
                                Dim mintMin As Integer = CInt(Math.Floor((mintTime / 60) Mod 60))
                                Dim mintHr As Integer = CInt(Math.Floor((mintTime / 3600) Mod 24))
                                Dim mintday As Integer = CInt(Math.Floor(((mintTime / 86400) Mod 31) + 1))
                                Dim mintmonth As Integer = CInt(Math.Floor(((mintTime / 2678400) Mod 12) + 1))
                                Dim mintYear As Integer = CInt(Math.Floor((mintTime / 32140800) + 2000))
                                Dim mdtDatetime As DateTime = CDate(DateAndTime.DateSerial(CInt(mintYear), CInt(mintmonth), CInt(mintday)) & " " & DateAndTime.TimeSerial(CInt(mintHr), CInt(mintMin), CInt(mintSec)))

                                If CDate(mdtDatetime).Date >= dtpfromdate.Value.Date And CDate(mdtDatetime).Date <= dtpTodate.Value.Date Then

                                    'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                                    'objEmployee = New clsEmployee_Master
                                    'Dim intEmpID As Integer = 0
                                    'If chkUseMappedDeviceUser.Checked = True Then
                                    '    intEmpID = objDeviceUser.GetEmployeeUnkID(CInt(arSplitField(1)).ToString)
                                    'Else
                                    '    intEmpID = CInt(arSplitField(1))
                                    'End If

                                    'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                                    'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                                    '    If chkUseMappedDeviceUser.Checked = True Then
                                    '        drRow("EmployeeName") = CInt(arSplitField(1)).ToString & " *** NOT MAPPED ***"
                                    '    Else
                                    '        drRow("EmployeeName") = CInt(arSplitField(1)).ToString & " *** NOT REGISTERED ***"
                                    '    End If
                                    '    drRow("EmployeeCode") = ""
                                    '    drRow("EmployeeId") = -1
                                    '    drRow("ShiftId") = 0
                                    '    drRow("ShiftName") = ""
                                    '    drRow("IsError") = True
                                    '    drRow.Item("Message") = CInt(arSplitField(1)).ToString & " *** NOT REGISTERED ***"
                                    '    drRow("image") = New Drawing.Bitmap(1, 1)
                                    'Else
                                    '    drRow("EmployeeId") = intEmpID
                                    '    drRow("EmployeeCode") = objEmployee._Employeecode
                                    '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                                    '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                                    '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtDatetime.Date, intEmpID)
                                    '    drRow("ShiftId") = objshift._Shiftunkid
                                    '    drRow("ShiftName") = objshift._Shiftname
                                    '    drRow("IsError") = False
                                    '    drRow.Item("Message") = ""
                                    '    drRow("image") = New Drawing.Bitmap(1, 1)
                                    'End If

                                    'drRow("VerifyMode") = 0
                                    'drRow("InOutMode") = -1
                                    'drRow("Logindate") = CDate(mdtDatetime).Date
                                    'drRow("Logintime") = CDate(mdtDatetime)
                                    'drRow("Device") = mstrDeviceCode
                                    'dsDownload.Tables("Download").Rows.Add(drRow)
                                    'dsDownload.AcceptChanges()


                                    If InsertDeviceData(mstrDeviceCode, strIp, CInt(arSplitField(1)).ToString(), CDate(mdtDatetime).Date, CDate(mdtDatetime), "0", "-1") = False Then
                                        Exit For
                                    End If

                                End If

                            End If

                        End If

                    Next

                Else
                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
                    ListViewError(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."))
                    'Pinkal (27-Mar-2017) -- End
                End If
            Else
                ret = clsZKAccessControl.PullLastError
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret, enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & ret)
                'Pinkal (27-Mar-2017) -- End
            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadZkAccessControlData", mstrModuleName)
        Finally
            If IntPtr.Zero <> h Then
                clsZKAccessControl.Disconnect(h)
                h = IntPtr.Zero
            End If
        End Try
    End Sub


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    'Private Sub DownloadAnvizData()
    '    Try
    '        Dim mintErrorNo As Integer

    '        If dsDownload Is Nothing Then
    '            CreateDataset()
    '        End If

    '        mintErrorNo = clsAnviz.CKT_RegisterNet(mintMachineSrNo, strIp.Trim)

    '        If mintErrorNo <> clsAnviz.CKT_RESULT_OK Then

    '            Select Case mintErrorNo

    '                Case clsAnviz.CKT_ERROR_INVPARAM
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Invalid Parameter.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_NETDAEMONREADY
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Error in Netdaemonreday.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_CHECKSUMERR
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Checksum Error.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_MEMORYFULL
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Memory Full.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_INVFILENAME
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Invalid File Name.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_FILECANNOTOPEN
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "File Cannot Open.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_FILECONTENTBAD
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "File Content Bad.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_FILECANNOTCREATED
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "File Cannot Create.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case clsAnviz.CKT_ERROR_NOTHISPERSON
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Not this Person.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '                Case Else
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '                    clsAnviz.CKT_Disconnect()
    '                    Exit Sub
    '            End Select

    '        End If

    '        Dim i As Integer
    '        Dim RecordCount As New Integer()
    '        Dim RetCount As New Integer()
    '        Dim pClockings As New Integer()
    '        Dim pLongRun As New Integer()
    '        Dim ptemp As Integer
    '        Dim tempptr As Integer
    '        Dim ret As Integer
    '        Dim j As Integer

    '        If clsAnviz.CKT_GetClockingRecordEx(mintMachineSrNo, pLongRun) = 1 Then
    '            While True
    '                ret = clsAnviz.CKT_GetClockingRecordProgress(pLongRun, RecordCount, RetCount, pClockings)

    '                If ret <> 0 Then
    '                    ptemp = Marshal.SizeOf(clocking)
    '                    tempptr = pClockings
    '                    For i = 0 To RetCount - 1
    '                        RtlMoveMemory(clocking, pClockings, ptemp)
    '                        pClockings = pClockings + ptemp
    '                        If clocking.PersonID < 0 Then
    '                            Continue For
    '                        End If

    '                        Dim objEmployee As New clsEmployee_Master
    '                        Dim objshift As New clsNewshift_master
    '                        Dim objDeviceUser As New clsEmpid_devicemapping

    '                        Dim mstrTime As String = Encoding.[Default].GetString(clocking.Time).ToString()

    '                        If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
    '                           And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then

    '                            Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                            objEmployee = New clsEmployee_Master
    '                            Dim intEmpID As Integer = 0
    '                            If chkUseMappedDeviceUser.Checked = True Then
    '                                intEmpID = objDeviceUser.GetEmployeeUnkID(clocking.PersonID.ToString)
    '                            Else
    '                                intEmpID = clocking.PersonID
    '                            End If

    '                            'S.SANDEEP [04 JUN 2015] -- START
    '                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                            'objEmployee._Employeeunkid = intEmpID
    '                            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
    '                            'S.SANDEEP [04 JUN 2015] -- END

    '                            If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                                If chkUseMappedDeviceUser.Checked = True Then
    '                                    drRow("EmployeeName") = clocking.PersonID.ToString & " *** NOT MAPPED ***"
    '                                Else
    '                                    drRow("EmployeeName") = clocking.PersonID.ToString & " *** NOT REGISTERED ***"
    '                                End If
    '                                drRow("EmployeeCode") = ""
    '                                drRow("EmployeeId") = -1
    '                                drRow("ShiftId") = 0
    '                                drRow("ShiftName") = ""
    '                                drRow("IsError") = True
    '                                drRow.Item("Message") = clocking.PersonID.ToString & " *** NOT REGISTERED ***"
    '                                drRow("image") = New Drawing.Bitmap(1, 1)
    '                            Else
    '                                drRow("EmployeeId") = intEmpID
    '                                drRow("EmployeeCode") = objEmployee._Employeecode
    '                                drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                                Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                                objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
    '                                drRow("ShiftId") = objshift._Shiftunkid
    '                                drRow("ShiftName") = objshift._Shiftname
    '                                drRow("IsError") = False
    '                                drRow.Item("Message") = ""
    '                                drRow("image") = New Drawing.Bitmap(1, 1)
    '                            End If
    '                            drRow("VerifyMode") = 0

    '                            'Pinkal (09-Jun-2015) -- Start
    '                            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
    '                            'drRow("InOutMode") = 0
    '                            drRow("InOutMode") = -1
    '                            'Pinkal (09-Jun-2015) -- End
    '                            drRow("Logindate") = CDate(mstrTime).Date
    '                            drRow("Logintime") = CDate(mstrTime)
    '                            drRow("Device") = mstrDeviceCode
    '                            dsDownload.Tables("Download").Rows.Add(drRow)
    '                            dsDownload.AcceptChanges()
    '                        End If
    '                        j += 1
    '                    Next

    '                    If tempptr <> 0 Then
    '                        clsAnviz.CKT_FreeMemory(tempptr)
    '                    End If

    '                    If ret = 1 Then
    '                        Exit While
    '                    End If

    '                End If

    '            End While
    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
    '        End If
    '    Catch ex As Runtime.InteropServices.SEHException

    '    Catch ex As AccessViolationException

    '    Catch ex As ExecutionEngineException

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DownloadAnvizData", mstrModuleName)
    '    Finally
    '        clsAnviz.CKT_Disconnect()
    '    End Try
    'End Sub


    Private Sub DownloadAnvizData()
        Try
            Dim mintErrorNo As Integer

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            mintErrorNo = clsAnviz.CKT_RegisterNet(mintMachineSrNo, strIp.Trim)

            If mintErrorNo <> clsAnviz.CKT_RESULT_OK Then

                Select Case mintErrorNo

                    Case clsAnviz.CKT_ERROR_INVPARAM
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Invalid Parameter.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 22, "Invalid Parameter.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_NETDAEMONREADY
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Error in Netdaemonreday.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 14, "Error in Netdaemonreday.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_CHECKSUMERR
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Checksum Error.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 15, "Checksum Error.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_MEMORYFULL
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Memory Full.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 16, "Memory Full.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_INVFILENAME
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Invalid File Name.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 17, "Invalid File Name.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_FILECANNOTOPEN
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "File Cannot Open.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 18, "File Cannot Open.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_FILECONTENTBAD
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "File Content Bad.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 19, "File Content Bad.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_FILECANNOTCREATED
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "File Cannot Create.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 20, "File Cannot Create.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case clsAnviz.CKT_ERROR_NOTHISPERSON
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Not this Person.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 21, "Not this Person.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                    Case Else
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                        ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")
                        clsAnviz.CKT_Disconnect()
                        Exit Sub
                End Select

            End If

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing
            'Pinkal (27-Mar-2017) -- End

            Dim i As Integer
            Dim RecordCount As New Integer()
            Dim RetCount As New Integer()
            Dim pClockings As New Integer()
            Dim pLongRun As New Integer()
            Dim ptemp As Integer
            Dim tempptr As Integer
            Dim ret As Integer
            Dim j As Integer

            If clsAnviz.CKT_GetClockingRecordEx(mintMachineSrNo, pLongRun) = 1 Then
                While True
                    ret = clsAnviz.CKT_GetClockingRecordProgress(pLongRun, RecordCount, RetCount, pClockings)

                    If ret <> 0 Then
                        ptemp = Marshal.SizeOf(clocking)
                        tempptr = pClockings
                        For i = 0 To RetCount - 1
                            RtlMoveMemory(clocking, pClockings, ptemp)
                            pClockings = pClockings + ptemp
                            If clocking.PersonID < 0 Then
                                Continue For
                            End If

                            Dim objEmployee As New clsEmployee_Master
                            Dim objshift As New clsNewshift_master
                            Dim objDeviceUser As New clsEmpid_devicemapping

                            Dim mstrTime As String = Encoding.[Default].GetString(clocking.Time).ToString()

                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date.AddDays(1)) Then

                                'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                                'objEmployee = New clsEmployee_Master
                                'Dim intEmpID As Integer = 0
                                'If chkUseMappedDeviceUser.Checked = True Then
                                '    intEmpID = objDeviceUser.GetEmployeeUnkID(clocking.PersonID.ToString)
                                'Else
                                '    intEmpID = clocking.PersonID
                                'End If

                                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                                'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                                '    If chkUseMappedDeviceUser.Checked = True Then
                                '        drRow("EmployeeName") = clocking.PersonID.ToString & " *** NOT MAPPED ***"
                                '    Else
                                '        drRow("EmployeeName") = clocking.PersonID.ToString & " *** NOT REGISTERED ***"
                                '    End If
                                '    drRow("EmployeeCode") = ""
                                '    drRow("EmployeeId") = -1
                                '    drRow("ShiftId") = 0
                                '    drRow("ShiftName") = ""
                                '    drRow("IsError") = True
                                '    drRow.Item("Message") = clocking.PersonID.ToString & " *** NOT REGISTERED ***"
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'Else
                                '    drRow("EmployeeId") = intEmpID
                                '    drRow("EmployeeCode") = objEmployee._Employeecode
                                '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                                '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                                '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(mstrTime).Date, intEmpID)
                                '    drRow("ShiftId") = objshift._Shiftunkid
                                '    drRow("ShiftName") = objshift._Shiftname
                                '    drRow("IsError") = False
                                '    drRow.Item("Message") = ""
                                '    drRow("image") = New Drawing.Bitmap(1, 1)
                                'End If
                                'drRow("VerifyMode") = 0
                                'drRow("InOutMode") = -1
                                'drRow("Logindate") = CDate(mstrTime).Date
                                'drRow("Logintime") = CDate(mstrTime)
                                'drRow("Device") = mstrDeviceCode
                                'dsDownload.Tables("Download").Rows.Add(drRow)
                                'dsDownload.AcceptChanges()


                                If InsertDeviceData(mstrDeviceCode, strIp, clocking.PersonID.ToString(), CDate(mstrTime).Date, CDate(mstrTime), "0", "-1") = False Then
                                    Exit While
                                    End If

                            End If
                            j += 1
                        Next

                        If tempptr <> 0 Then
                            clsAnviz.CKT_FreeMemory(tempptr)
                        End If

                        If ret = 1 Then
                            Exit While
                        End If

                    End If

                End While
            Else
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."))
                'Pinkal (27-Mar-2017) -- End
            End If
        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadAnvizData", mstrModuleName)
        Finally
            clsAnviz.CKT_Disconnect()
        End Try
    End Sub

    'Private Sub DownloadACTAtekData()
    '    Try
    '        If dsDownload Is Nothing Then
    '            CreateDataset()
    '        End If

    '        Dim ActaService As New ACTAtekWSDL.ACTAtek
    '        ActaService.Url = "http://" & strIp & "/cgi-bin/rpcrouter"
    '        Dim mLngSessionID As Long = ActaService.login(mstrUserID, mstrPassword)


    '        'Dim EventTrigger As ACTAtekWSDL.triggerType
    '        'EventTrigger = CType(ACTAtekWSDL.triggerType.UNKNOWN Or ACTAtekWSDL.triggerType.IN Or ACTAtekWSDL.triggerType.OUT Or ACTAtekWSDL.triggerType.F1 Or ACTAtekWSDL.triggerType.F2 Or ACTAtekWSDL.triggerType.F3 Or ACTAtekWSDL.triggerType.F4 Or _
    '        '                                ACTAtekWSDL.triggerType.F5 Or ACTAtekWSDL.triggerType.F6 Or ACTAtekWSDL.triggerType.F7 Or ACTAtekWSDL.triggerType.F8 Or ACTAtekWSDL.triggerType.F9 Or ACTAtekWSDL.triggerType.F10 Or _
    '        '                                ACTAtekWSDL.triggerType.F11 Or ACTAtekWSDL.triggerType.F12 Or ACTAtekWSDL.triggerType.F13 Or ACTAtekWSDL.triggerType.F14 Or ACTAtekWSDL.triggerType.F15 Or ACTAtekWSDL.triggerType.F16 Or _
    '        '                                ACTAtekWSDL.triggerType.F17 Or ACTAtekWSDL.triggerType.F18 Or ACTAtekWSDL.triggerType.F19 Or ACTAtekWSDL.triggerType.F20 Or ACTAtekWSDL.triggerType.F21 Or ACTAtekWSDL.triggerType.F22 Or _
    '        '                                ACTAtekWSDL.triggerType.F23 Or ACTAtekWSDL.triggerType.F24 Or ACTAtekWSDL.triggerType.F25 Or ACTAtekWSDL.triggerType.F26 Or ACTAtekWSDL.triggerType.F27 Or ACTAtekWSDL.triggerType.F28 Or _
    '        '                                ACTAtekWSDL.triggerType.F29 Or ACTAtekWSDL.triggerType.F30 Or ACTAtekWSDL.triggerType.F31 Or ACTAtekWSDL.triggerType.F32 Or ACTAtekWSDL.triggerType.F33 Or ACTAtekWSDL.triggerType.F34 Or _
    '        '                                ACTAtekWSDL.triggerType.F35 Or ACTAtekWSDL.triggerType.F36 Or ACTAtekWSDL.triggerType.F37 Or ACTAtekWSDL.triggerType.F39 Or ACTAtekWSDL.triggerType.F40, ACTAtekWSDL.triggerType)




    '        If mLngSessionID > 0 Then

    '            ActaService = New ACTAtekWSDL.ACTAtek
    '            ActaService.Url = "http://" & strIp & "/cgi-bin/rpcrouter"

    '            Dim LogCritera As New ACTAtekWSDL.getLogsCriteria
    '            LogCritera.from = dtpfromdate.Value.Date
    '            LogCritera.to = dtpTodate.Value.Date
    '            'LogCritera.trigger = CType(EventTrigger, Global.Aruti.Main.ACTAtekWSDL.eventType?)
    '            LogCritera.triggerSpecified = False ' Boolean 
    '            LogCritera.departmentIDSpecified = False ' Boolean
    '            LogCritera.fromSpecified = True ' Boolean 
    '            LogCritera.toSpecified = True  ' Boolean 
    '            LogCritera.triggerSpecified = True ' Boolean 

    '            Dim Logs() As ACTAtekWSDL.LogDetail
    '            Dim log As ACTAtekWSDL.LogDetail
    '            Logs = ActaService.getFullLogs(mLngSessionID, LogCritera, 1, False)
    '            If Logs IsNot Nothing Then
    '                Dim objEmployee As New clsEmployee_Master
    '                Dim objshift As New clsNewshift_master
    '                Dim objDeviceUser As New clsEmpid_devicemapping

    '                For Each log In Logs  'Get Full Logs

    '                    'EventTrigger = CType(log.trigger, ACTAtekWSDL.triggerType)

    '                    If log.trigger = ACTAtekWSDL.triggerType.UNKNOWN OrElse log.trigger = ACTAtekWSDL.triggerType.IN OrElse log.trigger = ACTAtekWSDL.triggerType.OUT _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F1 OrElse log.trigger = ACTAtekWSDL.triggerType.F2 OrElse log.trigger = ACTAtekWSDL.triggerType.F3 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F4 OrElse log.trigger = ACTAtekWSDL.triggerType.F5 OrElse log.trigger = ACTAtekWSDL.triggerType.F6 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F7 OrElse log.trigger = ACTAtekWSDL.triggerType.F8 OrElse log.trigger = ACTAtekWSDL.triggerType.F9 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F10 OrElse log.trigger = ACTAtekWSDL.triggerType.F11 OrElse log.trigger = ACTAtekWSDL.triggerType.F12 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F13 OrElse log.trigger = ACTAtekWSDL.triggerType.F14 OrElse log.trigger = ACTAtekWSDL.triggerType.F15 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F16 OrElse log.trigger = ACTAtekWSDL.triggerType.F17 OrElse log.trigger = ACTAtekWSDL.triggerType.F18 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F19 OrElse log.trigger = ACTAtekWSDL.triggerType.F20 OrElse log.trigger = ACTAtekWSDL.triggerType.F21 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F22 OrElse log.trigger = ACTAtekWSDL.triggerType.F23 OrElse log.trigger = ACTAtekWSDL.triggerType.F24 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F25 OrElse log.trigger = ACTAtekWSDL.triggerType.F26 OrElse log.trigger = ACTAtekWSDL.triggerType.F27 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F28 OrElse log.trigger = ACTAtekWSDL.triggerType.F29 OrElse log.trigger = ACTAtekWSDL.triggerType.F30 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F31 OrElse log.trigger = ACTAtekWSDL.triggerType.F32 OrElse log.trigger = ACTAtekWSDL.triggerType.F33 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F34 OrElse log.trigger = ACTAtekWSDL.triggerType.F35 OrElse log.trigger = ACTAtekWSDL.triggerType.F36 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F37 OrElse log.trigger = ACTAtekWSDL.triggerType.F38 OrElse log.trigger = ACTAtekWSDL.triggerType.F39 _
    '                    OrElse log.trigger = ACTAtekWSDL.triggerType.F40 Then

    '                        Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
    '                        objEmployee = New clsEmployee_Master
    '                        Dim intEmpID As Integer = 0
    '                        If chkUseMappedDeviceUser.Checked = True Then
    '                            intEmpID = objDeviceUser.GetEmployeeUnkID(log.userID.ToString)
    '                        Else
    '                            intEmpID = CInt(log.userID)
    '                        End If

    '                        'S.SANDEEP [04 JUN 2015] -- START
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEmployee._Employeeunkid = intEmpID
    '                        objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID
    '                        'S.SANDEEP [04 JUN 2015] -- END


    '                        If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
    '                            If chkUseMappedDeviceUser.Checked = True Then
    '                                drRow("EmployeeName") = log.userID.ToString & " *** NOT MAPPED ***"
    '                            Else
    '                                drRow("EmployeeName") = log.userID.ToString & " *** NOT REGISTERED ***"
    '                            End If
    '                            drRow("EmployeeCode") = ""
    '                            drRow("EmployeeId") = -1
    '                            drRow("ShiftId") = 0
    '                            drRow("ShiftName") = ""
    '                            drRow("IsError") = True
    '                            drRow.Item("Message") = log.userID.ToString & " *** NOT REGISTERED ***"
    '                            drRow("image") = New Drawing.Bitmap(1, 1)
    '                        Else
    '                            drRow("EmployeeId") = intEmpID
    '                            drRow("EmployeeCode") = objEmployee._Employeecode
    '                            drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
    '                            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
    '                            objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(log.timestamp.Date, intEmpID)
    '                            drRow("ShiftId") = objshift._Shiftunkid
    '                            drRow("ShiftName") = objshift._Shiftname
    '                            drRow("IsError") = False
    '                            drRow.Item("Message") = ""
    '                            drRow("image") = New Drawing.Bitmap(1, 1)
    '                        End If

    '                        drRow("VerifyMode") = 0
    '                        drRow("InOutMode") = log.trigger
    '                        drRow("Logindate") = log.timestamp.Date
    '                        drRow("Logintime") = log.timestamp
    '                        drRow("Device") = mstrDeviceCode
    '                        dsDownload.Tables("Download").Rows.Add(drRow)
    '                        dsDownload.AcceptChanges()

    '                    Else
    '                        Continue For
    '                    End If

    '                Next
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
    '            End If
    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '    Catch ex As Runtime.InteropServices.SEHException

    '    Catch ex As AccessViolationException

    '    Catch ex As ExecutionEngineException

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DownloadACTAtekData", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub DownloadACTAtekData()
        Try
            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            Dim ActaService As New ACTAtekWSDL.ACTAtek
            ActaService.Url = "http://" & strIp & "/cgi-bin/rpcrouter"
            Dim mLngSessionID As Long = ActaService.login(mstrUserID, mstrPassword)


            'Dim EventTrigger As ACTAtekWSDL.triggerType
            'EventTrigger = CType(ACTAtekWSDL.triggerType.UNKNOWN Or ACTAtekWSDL.triggerType.IN Or ACTAtekWSDL.triggerType.OUT Or ACTAtekWSDL.triggerType.F1 Or ACTAtekWSDL.triggerType.F2 Or ACTAtekWSDL.triggerType.F3 Or ACTAtekWSDL.triggerType.F4 Or _
            '                                ACTAtekWSDL.triggerType.F5 Or ACTAtekWSDL.triggerType.F6 Or ACTAtekWSDL.triggerType.F7 Or ACTAtekWSDL.triggerType.F8 Or ACTAtekWSDL.triggerType.F9 Or ACTAtekWSDL.triggerType.F10 Or _
            '                                ACTAtekWSDL.triggerType.F11 Or ACTAtekWSDL.triggerType.F12 Or ACTAtekWSDL.triggerType.F13 Or ACTAtekWSDL.triggerType.F14 Or ACTAtekWSDL.triggerType.F15 Or ACTAtekWSDL.triggerType.F16 Or _
            '                                ACTAtekWSDL.triggerType.F17 Or ACTAtekWSDL.triggerType.F18 Or ACTAtekWSDL.triggerType.F19 Or ACTAtekWSDL.triggerType.F20 Or ACTAtekWSDL.triggerType.F21 Or ACTAtekWSDL.triggerType.F22 Or _
            '                                ACTAtekWSDL.triggerType.F23 Or ACTAtekWSDL.triggerType.F24 Or ACTAtekWSDL.triggerType.F25 Or ACTAtekWSDL.triggerType.F26 Or ACTAtekWSDL.triggerType.F27 Or ACTAtekWSDL.triggerType.F28 Or _
            '                                ACTAtekWSDL.triggerType.F29 Or ACTAtekWSDL.triggerType.F30 Or ACTAtekWSDL.triggerType.F31 Or ACTAtekWSDL.triggerType.F32 Or ACTAtekWSDL.triggerType.F33 Or ACTAtekWSDL.triggerType.F34 Or _
            '                                ACTAtekWSDL.triggerType.F35 Or ACTAtekWSDL.triggerType.F36 Or ACTAtekWSDL.triggerType.F37 Or ACTAtekWSDL.triggerType.F39 Or ACTAtekWSDL.triggerType.F40, ACTAtekWSDL.triggerType)




            If mLngSessionID > 0 Then

                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                Dim objLogin As New clslogin_Tran
                If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                    ListViewError(objLogin._Message)
                End If
                objLogin = Nothing
                'Pinkal (27-Mar-2017) -- End

                ActaService = New ACTAtekWSDL.ACTAtek
                ActaService.Url = "http://" & strIp & "/cgi-bin/rpcrouter"

                Dim LogCritera As New ACTAtekWSDL.getLogsCriteria
                LogCritera.from = dtpfromdate.Value.Date
                LogCritera.to = dtpTodate.Value.Date
                'LogCritera.trigger = CType(EventTrigger, Global.Aruti.Main.ACTAtekWSDL.eventType?)
                LogCritera.triggerSpecified = False ' Boolean 
                LogCritera.departmentIDSpecified = False ' Boolean
                LogCritera.fromSpecified = True ' Boolean 
                LogCritera.toSpecified = True  ' Boolean 
                LogCritera.triggerSpecified = True ' Boolean 

                Dim Logs() As ACTAtekWSDL.LogDetail
                Dim log As ACTAtekWSDL.LogDetail
                Logs = ActaService.getFullLogs(mLngSessionID, LogCritera, 1, False)
                If Logs IsNot Nothing Then
                    Dim objEmployee As New clsEmployee_Master
                    Dim objshift As New clsNewshift_master
                    Dim objDeviceUser As New clsEmpid_devicemapping

                    For Each log In Logs  'Get Full Logs

                        If log.trigger = ACTAtekWSDL.triggerType.UNKNOWN OrElse log.trigger = ACTAtekWSDL.triggerType.IN OrElse log.trigger = ACTAtekWSDL.triggerType.OUT _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F1 OrElse log.trigger = ACTAtekWSDL.triggerType.F2 OrElse log.trigger = ACTAtekWSDL.triggerType.F3 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F4 OrElse log.trigger = ACTAtekWSDL.triggerType.F5 OrElse log.trigger = ACTAtekWSDL.triggerType.F6 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F7 OrElse log.trigger = ACTAtekWSDL.triggerType.F8 OrElse log.trigger = ACTAtekWSDL.triggerType.F9 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F10 OrElse log.trigger = ACTAtekWSDL.triggerType.F11 OrElse log.trigger = ACTAtekWSDL.triggerType.F12 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F13 OrElse log.trigger = ACTAtekWSDL.triggerType.F14 OrElse log.trigger = ACTAtekWSDL.triggerType.F15 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F16 OrElse log.trigger = ACTAtekWSDL.triggerType.F17 OrElse log.trigger = ACTAtekWSDL.triggerType.F18 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F19 OrElse log.trigger = ACTAtekWSDL.triggerType.F20 OrElse log.trigger = ACTAtekWSDL.triggerType.F21 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F22 OrElse log.trigger = ACTAtekWSDL.triggerType.F23 OrElse log.trigger = ACTAtekWSDL.triggerType.F24 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F25 OrElse log.trigger = ACTAtekWSDL.triggerType.F26 OrElse log.trigger = ACTAtekWSDL.triggerType.F27 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F28 OrElse log.trigger = ACTAtekWSDL.triggerType.F29 OrElse log.trigger = ACTAtekWSDL.triggerType.F30 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F31 OrElse log.trigger = ACTAtekWSDL.triggerType.F32 OrElse log.trigger = ACTAtekWSDL.triggerType.F33 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F34 OrElse log.trigger = ACTAtekWSDL.triggerType.F35 OrElse log.trigger = ACTAtekWSDL.triggerType.F36 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F37 OrElse log.trigger = ACTAtekWSDL.triggerType.F38 OrElse log.trigger = ACTAtekWSDL.triggerType.F39 _
                        OrElse log.trigger = ACTAtekWSDL.triggerType.F40 Then

                            'Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                            'objEmployee = New clsEmployee_Master
                            'Dim intEmpID As Integer = 0
                            'If chkUseMappedDeviceUser.Checked = True Then
                            '    intEmpID = objDeviceUser.GetEmployeeUnkID(log.userID.ToString)
                            'Else
                            '    intEmpID = CInt(log.userID)
                            'End If

                            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                            'If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                            '    If chkUseMappedDeviceUser.Checked = True Then
                            '        drRow("EmployeeName") = log.userID.ToString & " *** NOT MAPPED ***"
                            '    Else
                            '        drRow("EmployeeName") = log.userID.ToString & " *** NOT REGISTERED ***"
                            '    End If
                            '    drRow("EmployeeCode") = ""
                            '    drRow("EmployeeId") = -1
                            '    drRow("ShiftId") = 0
                            '    drRow("ShiftName") = ""
                            '    drRow("IsError") = True
                            '    drRow.Item("Message") = log.userID.ToString & " *** NOT REGISTERED ***"
                            '    drRow("image") = New Drawing.Bitmap(1, 1)
                            'Else
                            '    drRow("EmployeeId") = intEmpID
                            '    drRow("EmployeeCode") = objEmployee._Employeecode
                            '    drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                            '    Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                            '    objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(log.timestamp.Date, intEmpID)
                            '    drRow("ShiftId") = objshift._Shiftunkid
                            '    drRow("ShiftName") = objshift._Shiftname
                            '    drRow("IsError") = False
                            '    drRow.Item("Message") = ""
                            '    drRow("image") = New Drawing.Bitmap(1, 1)
                            'End If

                            'drRow("VerifyMode") = 0
                            'drRow("InOutMode") = log.trigger
                            'drRow("Logindate") = log.timestamp.Date
                            'drRow("Logintime") = log.timestamp
                            'drRow("Device") = mstrDeviceCode
                            'dsDownload.Tables("Download").Rows.Add(drRow)
                            'dsDownload.AcceptChanges()


                            If InsertDeviceData(mstrDeviceCode, strIp, log.userID.ToString(), log.timestamp.Date, log.timestamp, "0", log.trigger.ToString()) = False Then
                                Exit For
                            End If

                        Else
                            Continue For
                        End If

                    Next
                Else
                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
                    ListViewError(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."))
                    'Pinkal (27-Mar-2017) -- End
                End If
            Else
                'Pinkal (27-Mar-2017) -- Start
                'Enhancement - Working On Import Device Attendance Data.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].", enMsgBoxStyle.Information)
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")
                'Pinkal (27-Mar-2017) -- End
                Exit Sub
            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadACTAtekData", mstrModuleName)
        End Try
    End Sub


    'Pinkal (27-Mar-2017) -- End


    Private Sub DownloadHandpunch_Data()
        Dim objHandpunch As New clsHandpunch
        Try
            objHandpunch._Fromdate = dtpfromdate.Value.Date
            objHandpunch._Todate = dtpTodate.Value.Date
            Dim dtTable As DataTable = objHandpunch.GetHandpunchAttendanceData()


            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim objEmployee As New clsEmployee_Master
                Dim objshift As New clsNewshift_master
                Dim objDeviceUser As New clsEmpid_devicemapping

                'Pinkal (22-Nov-2017) -- Start
                'Enhancement - Windsor Golf Hotel - issue # 0001607: Error when downloading device data
                Dim objImgConverter As New ImageConverter
                Dim img As Image = New Drawing.Bitmap(1, 1)
                'Pinkal (22-Nov-2017) -- End

                For Each dr As DataRow In dtTable.Rows
                    Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                    objEmployee = New clsEmployee_Master
                    Dim intEmpID As Integer = 0

                    If chkUseMappedDeviceUser.Checked = True Then
                        intEmpID = objDeviceUser.GetEmployeeUnkID(dr("DeviceUserID").ToString())
                    End If

                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                    If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                        If chkUseMappedDeviceUser.Checked = True Then
                            drRow("EmployeeName") = dr("DeviceUserID").ToString() & " *** NOT MAPPED ***"
                        Else
                            drRow("EmployeeName") = dr("DeviceUserID").ToString() & " *** NOT REGISTERED ***"
                        End If
                        drRow("EmployeeCode") = ""
                        drRow("EmployeeId") = -1
                        drRow("ShiftId") = 0
                        drRow("ShiftName") = ""
                        drRow("IsError") = True
                        drRow.Item("Message") = dr("DeviceUserID").ToString() & " *** NOT REGISTERED ***"

                        'Pinkal (22-Nov-2017) -- Start
                        'Enhancement - Windsor Golf Hotel - issue # 0001607: Error when downloading device data
                        'drRow("image") = New Drawing.Bitmap(1, 1)
                        drRow("image") = objImgConverter.ConvertTo(img, System.Type.GetType("System.Byte[]"))
                        'Pinkal (22-Nov-2017) -- End
                    Else
                        drRow("EmployeeId") = intEmpID
                        drRow("EmployeeCode") = objEmployee._Employeecode
                        drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                        Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                        objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(dr("AttendanceTime")).Date, intEmpID)
                        drRow("ShiftId") = objshift._Shiftunkid
                        drRow("ShiftName") = objshift._Shiftname
                        drRow("IsError") = False
                        drRow.Item("Message") = ""
                        'Pinkal (22-Nov-2017) -- Start
                        'Enhancement - Windsor Golf Hotel - issue # 0001607: Error when downloading device data
                        'drRow("image") = New Drawing.Bitmap(1, 1)
                        drRow("image") = objImgConverter.ConvertTo(img, System.Type.GetType("System.Byte[]"))
                        'Pinkal (22-Nov-2017) -- End
                    End If
                    drRow("VerifyMode") = 0
                    drRow("InOutMode") = -1
                    drRow("Logindate") = CDate(dr("AttendanceTime")).Date
                    drRow("Logintime") = CDate(dr("AttendanceTime"))
                    drRow("Device") = dr("DeviceName").ToString()
                    dsDownload.Tables("Download").Rows.Add(drRow)
                    dsDownload.AcceptChanges()
                Next

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadHandpunch_Data", mstrModuleName)
        Finally
            objHandpunch = Nothing
        End Try
    End Sub



    'Pinkal (04-Oct-2017) -- Start
    'Enhancement - Working SAGEM Database Device Integration.

    Private Sub DownloadSAGEM_Data()
        Dim objSAGEM As New clsSAGEM
        Try
            objSAGEM._Fromdate = dtpfromdate.Value.Date
            objSAGEM._Todate = dtpTodate.Value.Date
            objSAGEM._DatabaseServer = ConfigParameter._Object._SagemDataServerAddress
            objSAGEM._DatabaseName = ConfigParameter._Object._SagemDatabaseName
            objSAGEM._DatabaseUserName = ConfigParameter._Object._SagemDatabaseUserName
            objSAGEM._DatabasePassword = clsSecurity.Decrypt(ConfigParameter._Object._SagemDatabasePassword, "ezee")
            Dim dtTable As DataTable = objSAGEM.GetSAGEMAttendanceData()

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim objEmployee As New clsEmployee_Master
                Dim objshift As New clsNewshift_master
                Dim objDeviceUser As New clsEmpid_devicemapping
                Dim objImgConverter As New ImageConverter
                Dim img As Image = New Drawing.Bitmap(1, 1)
                Dim tzCollection As ReadOnlyCollection(Of TimeZoneInfo) = TimeZoneInfo.GetSystemTimeZones()


                For Each dr As DataRow In dtTable.Rows
                    Dim drRow As DataRow = dsDownload.Tables("Download").NewRow
                    objEmployee = New clsEmployee_Master
                    Dim intEmpID As Integer = 0

                    If chkUseMappedDeviceUser.Checked = True Then
                        intEmpID = objDeviceUser.GetEmployeeUnkID(dr("MORPHOACCESSIDENTIFIER").ToString())
                    End If

                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpID

                    If objEmployee._Employeecode.Trim = "" AndAlso objEmployee._Firstname.Trim = "" Then
                        If chkUseMappedDeviceUser.Checked = True Then
                            drRow("EmployeeName") = dr("MORPHOACCESSIDENTIFIER").ToString() & " *** NOT MAPPED ***"
                        Else
                            drRow("EmployeeName") = dr("MORPHOACCESSIDENTIFIER").ToString() & " *** NOT REGISTERED ***"
                        End If
                        drRow("EmployeeCode") = ""
                        drRow("EmployeeId") = -1
                        drRow("ShiftId") = 0
                        drRow("ShiftName") = ""
                        drRow("IsError") = True
                        drRow.Item("Message") = dr("MORPHOACCESSIDENTIFIER").ToString() & " *** NOT REGISTERED ***"
                        drRow("image") = objImgConverter.ConvertTo(img, System.Type.GetType("System.Byte[]"))
                    Else
                        drRow("EmployeeId") = intEmpID
                        drRow("EmployeeCode") = objEmployee._Employeecode
                        drRow("EmployeeName") = objEmployee._Firstname & " " & objEmployee._Surname
                        Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                        objshift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(CDate(dr("LOGDATETIME")).Date, intEmpID)
                        drRow("ShiftId") = objshift._Shiftunkid
                        drRow("ShiftName") = objshift._Shiftname
                        drRow("IsError") = False
                        drRow.Item("Message") = ""
                        drRow("image") = objImgConverter.ConvertTo(img, System.Type.GetType("System.Byte[]"))
                    End If
                    drRow("VerifyMode") = 0
                    drRow("InOutMode") = -1

                    Dim mstrTimeZoneDisplayName As String = dr("Timezone").ToString()
                    Dim drTimeZoneList = From objtimezone As TimeZoneInfo In tzCollection Where objtimezone.DisplayName = mstrTimeZoneDisplayName Select objtimezone
                    Dim mdtLoginDate As DateTime
                    If drTimeZoneList.Count > 0 Then
                        mdtLoginDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(dr("LOGDATETIME")), drTimeZoneList(0))
                    Else
                        mdtLoginDate = Convert.ToDateTime(dr("LOGDATETIME"))
                    End If

                    drRow("Logindate") = mdtLoginDate.Date
                    drRow("Logintime") = mdtLoginDate
                    drRow("Device") = dr("DeviceName").ToString()
                    dsDownload.Tables("Download").Rows.Add(drRow)
                    dsDownload.AcceptChanges()
                Next

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadSAGEM_Data", mstrModuleName)
        Finally
            objSAGEM = Nothing
        End Try
    End Sub
    'Pinkal (04-Oct-2017) -- End


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    Private Sub FillDeviceData()
        Try

            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") = False Then Exit Sub

            Dim dsMachineSetting As New DataSet
            dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

            lvDeviceList.Items.Clear()

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
                Dim drRow As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid ='" & Company._Object._Companyunkid & "'")

                If drRow.Length > 0 Then
                    Dim lvItem As ListViewItem

                    For Each dr In drRow
                        lvItem = New ListViewItem
                        lvItem.Text = ""
                        lvItem.SubItems.Add(CStr(dr("devicecode").ToString()).Trim.Remove(dr("devicecode").ToString().IndexOf("||"), dr("devicecode").ToString().Trim.Length - dr("devicecode").ToString().IndexOf("||")))
                        lvItem.SubItems.Add(dr("ip").ToString())
                        lvItem.SubItems.Add("")
                        lvDeviceList.Items.Add(lvItem)
                    Next

                    If lvDeviceList.Items.Count > 16 Then
                        colhIPAddress.Width = 130 - 18
                    Else
                        colhIPAddress.Width = 130
                    End If

                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDeviceData", mstrModuleName)
        End Try
    End Sub

    Private Function InsertDeviceData(ByVal mstrDeviceCode As String, ByVal mstrIPAddress As String, ByVal mstrEnrollNo As String, ByVal mdtLoginDate As Date _
                                                    , ByVal mdtLoginTime As Date, ByVal mstrVerifyCode As String, ByVal mstrInOutMode As String) As Boolean
        Dim objLogin As New clslogin_Tran
        Try

            objLogin._DeviceAtt_DeviceNo = mstrDeviceCode

            objLogin._DeviceAtt_EnrollNo = mstrEnrollNo
            objLogin._DeviceAtt_IPAddress = mstrIPAddress
            objLogin._DeviceAtt_LoginDate = mdtLoginDate.Date
            objLogin._DeviceAtt_LoginTime = mdtLoginTime
            objLogin._DeviceAtt_VerifyMode = mstrVerifyCode
            objLogin._DeviceAtt_InOutMode = mstrInOutMode
            objLogin._DeviceAtt_IsError = False

            If objLogin.InsertDeviceAttendanceData() = False Then
                eZeeMsgBox.Show(objLogin._Message)
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDeviceData", mstrModuleName)
        Finally
            objLogin = Nothing
        End Try
        Return True
    End Function

    Private Sub ListViewError(ByVal mstrError As String)
        Try
            Dim lvItemError As ListViewItem = lvDeviceList.FindItemWithText(strIp, True, 0)
            If lvItemError IsNot Nothing Then
                lvItemError.UseItemStyleForSubItems = False
                lvItemError.Text = Language.getMessage(mstrModuleName, 24, "Error")
                lvItemError.ForeColor = Color.Blue
                lvItemError.Font = New Font(Me.Font, FontStyle.Bold)
                lvItemError.SubItems(colhDeviceList.Index).ForeColor = Color.Red
                lvItemError.SubItems(colhIPAddress.Index).ForeColor = Color.Red
                lvItemError.Tag = mstrError
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ListViewError", mstrModuleName)
        End Try
    End Sub

    'Pinkal (27-Mar-2017) -- End


    'Pinkal (06-Nov-2017) -- Start
    'Enhancement - Working on B5 Plus Day Start Overlap issue.

    Private Sub DayStartTimeChecking(ByVal objLogin As clslogin_Tran, ByVal intEmployeeId As Integer, ByVal dr As DataRow, ByVal intShiftId As Integer)
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objShiftTran As New clsshift_tran
        Try
            Dim mintPreviousShift As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dr("Logintime")).Date.AddDays(-1).Date, intEmployeeId)

            If mintPreviousShift <> intShiftId Then
                objShiftTran.GetShiftTran(mintPreviousShift)
                Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dr("Logintime")).Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                If drPreviousShiftTran.Length > 0 Then

                    Dim mdtStartTime As DateTime = Nothing
                    Dim mdtEndTime As DateTime = Nothing

                    mdtStartTime = CDate(CDate(dr("Logintime")).AddDays(-1).ToShortDateString() & " " & CDate(drPreviousShiftTran(0)("starttime")).ToString("hh:mm tt"))
                    mdtEndTime = CDate(CDate(dr("Logintime")).AddDays(-1).ToShortDateString() & " " & CDate(drPreviousShiftTran(0)("endtime")).ToString("hh:mm tt"))


                    If mdtEndTime < mdtStartTime Then
                        mdtEndTime = CDate(mdtEndTime.AddDays(1).ToShortDateString() & " " & CDate(drPreviousShiftTran(0)("endtime")).ToString("hh:mm tt"))

                        If mdtEndTime >= CDate(dr("Logintime")) Then
                            objLogin._Logindate = CDate(dr("Logintime")).AddDays(-1).Date
                        Else
                            objLogin._Logindate = CDate(dr("Logintime")).Date
                        End If
                    Else
                        objLogin._Logindate = CDate(dr("Logintime")).Date
                    End If
                Else
                    objLogin._Logindate = CDate(dr("Logintime")).Date
                End If
            Else
                objLogin._Logindate = CDate(dr("Logintime")).Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DayStartTimeChecking", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-Nov-2017) -- End


    'Pinkal (12-Jun-2018) -- Start
    'Enhancement - Integrating biostar 2 with Aruti for voltamp.

    Private Sub DownloadBioStar2Data()
        Dim objBiostar2 As ArutiBS2.BS2Integration
        Try

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            objBiostar2 = New ArutiBS2.BS2Integration

            Dim xDeviceID As UInteger = 0
            Dim strErrorMsg As String = ""

            If objBiostar2.IsConnected(strIp, Convert.ToUInt16(intPort), xDeviceID, strErrorMsg) = False Then
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "] " & strErrorMsg)
                Exit Sub
            End If

            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing

            Dim objDeviceUser As New clsEmpid_devicemapping
            Dim dtDate As Date = Nothing

            Dim dtUserLoginData As DataTable = objBiostar2.GetUserLog(strIp, Convert.ToUInt16(intPort), New DateTime(dtpfromdate.Value.Year, dtpfromdate.Value.Month, dtpfromdate.Value.Day, 0, 0, 0, DateTimeKind.Utc), New DateTime(dtpTodate.Value.AddDays(1).Year, dtpTodate.Value.AddDays(1).Month, dtpTodate.Value.AddDays(1).Day, 23, 59, 59, DateTimeKind.Utc), xDeviceID, strErrorMsg)

            If dtUserLoginData IsNot Nothing AndAlso dtUserLoginData.Rows.Count > 0 Then

                For Each dr As DataRow In dtUserLoginData.Rows

                    Dim drRow As DataRow = dsDownload.Tables("Download").NewRow

                    Dim intEmpID As Integer = 0
                    If chkUseMappedDeviceUser.Checked = True Then
                        intEmpID = objDeviceUser.GetEmployeeUnkID(dr("UserId").ToString())
                    Else
                        intEmpID = 0
                    End If

                    If dr("UserId").ToString().Trim.Length > 0 Then
                        If InsertDeviceData(mstrDeviceCode, strIp, dr("UserId").ToString().Trim, Convert.ToDateTime(dr("LogTime")).Date, Convert.ToDateTime(dr("LogTime")), "0", "-1") = False Then
                            Exit For
                        End If
                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadBioStar2Data", mstrModuleName)
        Finally
            objBiostar2 = Nothing
        End Try
    End Sub


    'Pinkal (12-Jun-2018) -- End

    'Sohail (29 Jun 2019) -- Start
    'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
    Private Sub DownloadFingerTecData()
        Dim objFingerTec As New BioBridgeSDKDLL.BioBridgeSDKClass
        Try
            Dim idwVerifyMode As Integer
            Dim idwInOutMode As Integer = -1
            Dim mstrTime As String = ""
            Dim idwErrorCode As Integer
            Dim xDeviceID As Integer = 0

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            If mstrCommunicationKey.Trim.Length > 0 Then
                objFingerTec.SetDeviceCommKey(CInt(mstrCommunicationKey))
            End If

            Dim bIsConnected As Boolean = False
            If objFingerTec.Connect_TCPIP(mstrDeviceModel, xDeviceID, strIp, intPort, CInt(mstrCommunicationKey)) = 0 Then
                bIsConnected = True
            End If
            If bIsConnected = False Then
                'objZKConnection.GetLastError(idwErrorCode)
                objFingerTec.Disconnect()

                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device.") & mstrDeviceCode & " [" & strIp & "].")

                objFingerTec = Nothing
                Exit Sub
            Else
                'objFingerTec.RegEvent(1, 65535)
            End If

            Dim sOption As String = "~ZKFPVersion"
            Dim sValue As String = ""
            'objFingerTec.GetSysOption(mintMachineSrNo, sOption, sValue)

            Dim mstrPlatform As String = ""
            objFingerTec.GetPlatform(mstrPlatform)

            objFingerTec.DisableDevice()

            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                ListViewError(objLogin._Message)
            End If
            objLogin = Nothing

            Dim intLogSize As Integer = 0
            If objFingerTec.ReadGeneralLog(intLogSize) = 0 Then 'read all the attendance records to the memory 
                Dim objEmployee As New clsEmployee_Master
                Dim objshift As New clsNewshift_master
                Dim objDeviceUser As New clsEmpid_devicemapping

                If sValue = "" OrElse sValue = "9" Then

                    'Sohail (12 Dec 2019) -- Start
                    'Eko Supreme Issue : Different method to be used for R2 and M2 device model to fetch data as current method was giving wrong invalid date (31-jun-2119) and giving conversion error.
                    'If mstrCommunicationKey.Trim.Length > 0 Then
                    If Not (mstrDeviceModel = "R2" OrElse mstrDeviceModel = "M2") Then
                        'Sohail (12 Dec 2019) -- End
Alg9newDevice:

                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objFingerTec.SSR_GetGeneralLog(sdwEnrollNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwVerifyMode, idwInOutMode, idwWorkcode) = 0

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString


                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                                And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then

                                If InsertDeviceData(mstrDeviceCode, strIp, sdwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                End If

                            End If

                        End While

                        'Sohail (12 Dec 2019) -- Start
                        'Eko Supreme Issue : Different method to be used for R2 and M2 device model to fetch data as current method was giving wrong invalid date (31-jun-2119) and giving conversion error.
                        'ElseIf mstrCommunicationKey.Trim.Length <= 0 Then
                    ElseIf mstrDeviceModel = "R2" OrElse mstrDeviceModel = "M2" Then
                        'Sohail (12 Dec 2019) -- End
FetchData:

                        If mstrPlatform.ToString.Trim.ToUpper.Contains("_TFT") Then
                            GoTo Alg9newDevice
                        End If


                        Dim idwEnrollNumber As Integer = 0
                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objFingerTec.GetGeneralLog(idwEnrollNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwVerifyMode, idwInOutMode, idwWorkcode) = 0

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                                And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then

                                If InsertDeviceData(mstrDeviceCode, strIp, idwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                End If

                            End If

                        End While


                    End If


                ElseIf sValue = "10" OrElse sValue = "1" Then

                    'Sohail (12 Dec 2019) -- Start
                    'Eko Supreme Issue : Different method to be used for R2 and M2 device model to fetch data as current method was giving wrong invalid date (31-jun-2119) and giving conversion error.
                    If Not (mstrDeviceModel = "R2" OrElse mstrDeviceModel = "M2") Then
                        'Sohail (12 Dec 2019) -- End

                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objFingerTec.SSR_GetGeneralLog(sdwEnrollNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwVerifyMode, idwInOutMode, idwWorkcode) = 0

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                               And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then

                                If InsertDeviceData(mstrDeviceCode, strIp, sdwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                End If

                            End If

                        End While

                        'Sohail (12 Dec 2019) -- Start
                        'Eko Supreme Issue : Different method to be used for R2 and M2 device model to fetch data as current method was giving wrong invalid date (31-jun-2119) and giving conversion error.
                        'ElseIf mstrCommunicationKey.Trim.Length <= 0 Then
                    ElseIf mstrDeviceModel = "R2" OrElse mstrDeviceModel = "M2" Then

                        Dim idwEnrollNumber As Integer = 0
                        Dim idwYear As Integer
                        Dim idwMonth As Integer
                        Dim idwDay As Integer
                        Dim idwHour As Integer
                        Dim idwMinute As Integer
                        Dim idwSecond As Integer
                        Dim idwWorkcode As Integer
                        Dim sdwEnrollNumber As String = ""

                        While objFingerTec.GetGeneralLog(idwEnrollNumber, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwVerifyMode, idwInOutMode, idwWorkcode) = 0

                            mstrTime = CDate(eZeeDate.convertDate(idwYear & idwMonth.ToString("#00") & idwDay.ToString("#00")) & " " & idwHour & ":" & idwMinute & ":" & idwSecond).ToString

                            If eZeeDate.convertDate(CDate(mstrTime)) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                                And eZeeDate.convertDate(CDate(mstrTime)) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then

                                If InsertDeviceData(mstrDeviceCode, strIp, idwEnrollNumber.ToString(), CDate(mstrTime).Date, CDate(mstrTime), idwVerifyMode.ToString(), idwInOutMode.ToString()) = False Then
                                    Exit While
                                End If

                            End If

                        End While
                        'Sohail (12 Dec 2019) -- End

                    Else
                        GoTo FetchData
                    End If

                End If

            Else
                'objFingerTec.GetLastError(idwErrorCode)
                objFingerTec.Disconnect()

                If idwErrorCode <> 0 Then

                    ListViewError(Language.getMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode: ") & idwErrorCode)
                Else
                    ListViewError(Language.getMessage(mstrModuleName, 3, "There is no data to download from device."))
                End If
                Exit Sub
            End If

            objFingerTec.EnableDevice()
            objFingerTec.Disconnect()

            Dim lvItem As ListViewItem = lvDeviceList.FindItemWithText(strIp, True, 0)
            If lvItem IsNot Nothing Then
                lvItem.ForeColor = Color.Green
            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            objFingerTec.EnableDevice()
            objFingerTec.Disconnect()
            DisplayError.Show("-1", ex.Message, "DownloadFingerTecData", mstrModuleName)
        Finally
            objFingerTec = Nothing
        End Try
    End Sub
    'Sohail (29 Jun 2019) -- End


    'Pinkal (22-Apr-2020) -- Start
    'Enhancement - New Anviz Integration for Gran Melia.

    Private Sub DownloadNewAnvizData(ByVal drMachineDetails As DataRow())
        Dim dtMachineDetail As DataTable = Nothing
        Try

            If anviz_handle = IntPtr.Zero Then
                ListViewError(Language.getMessage(mstrModuleName, 8, "Unable to connect the device."))
            Else

                Dim objLogin As New clslogin_Tran
                If objLogin.DeleteDeviceAttendanceData() = False Then
                    ListViewError(objLogin._Message)
                End If
                objLogin = Nothing

                dtMachineDetail = New DataTable
                dtMachineDetail.Columns.Add("MachineId", System.Type.GetType("System.String"))
                dtMachineDetail.Columns.Add("MachineIndex", System.Type.GetType("System.String"))
                dtMachineDetail.Columns.Add("MachineIP", System.Type.GetType("System.String"))
                dtMachineDetail.Columns.Add("MachineVersion", System.Type.GetType("System.String"))

                Dim Type As Integer() = New Integer(0) {}
                Dim pBuff As IntPtr
                Dim len As Integer = 16000
                Dim dev_idx As Integer() = New Integer(0) {}
                Dim ret As Integer = 0
                Dim Ipstr As Byte() = New Byte(15) {}


                pBuff = Marshal.AllocHGlobal(len)

                While True
                    If anviz_handle = IntPtr.Zero Then
                        Exit While
                    End If

                    ret = clsAnvizNew.CChex_Update(anviz_handle, dev_idx, Type, pBuff, len)
                    If ret > 0 Then

                        Select Case Type(0)
                            Case CInt(clsAnvizNew.MsgType.CCHEX_RET_DEV_LOGIN_TYPE)
                                Dim dev_info As clsAnvizNew.CCHEX_RET_DEV_LOGIN_STRU
                                dev_info = CType(Marshal.PtrToStructure(pBuff, GetType(clsAnvizNew.CCHEX_RET_DEV_LOGIN_STRU)), clsAnvizNew.CCHEX_RET_DEV_LOGIN_STRU)
                                Dim mstrGetIPAddress As String = byte_to_string(dev_info.Addr).ToString().Trim.Substring(0, byte_to_string(dev_info.Addr).Trim().IndexOf(":"))
                                dtMachineDetail.Rows.Add(dev_info.MachineId, dev_info.DevIdx, mstrGetIPAddress.Trim(), byte_to_string(dev_info.Version))
                        End Select

                    ElseIf ret = 0 Then
                        Exit While
                    ElseIf ret < 0 Then
                        len = len * 2
                    Else
                        Exit While
                    End If
                End While

                Marshal.FreeHGlobal(pBuff)

                If dtMachineDetail IsNot Nothing AndAlso dtMachineDetail.Rows.Count > 0 Then
                    If dsDownload Is Nothing Then
                        CreateDataset()
                    End If

                    'MsgBox("dtMachineDetail : " & dtMachineDetail.Rows.Count)

                    For Each dr As DataRow In dtMachineDetail.Rows
                        Dim intdev_idx As Integer = CInt(dr("MachineIndex"))
                        Dim TypeData As Integer() = New Integer(0) {}
                        Dim pBuffData As IntPtr
                        Dim lenData As Integer = 16000
                        Dim dev_idxData As Integer() = New Integer(0) {}
                        Dim retData As Integer = 0


                        retData = clsAnvizNew.CChex_DownloadAllRecords(anviz_handle, intdev_idx)
                        Thread.Sleep(40000)

                        If retData = 1 Then
                            pBuffData = Marshal.AllocHGlobal(lenData)

                            While (True)

                                If anviz_handle = IntPtr.Zero Then
                                    Exit While
                                End If

                                Dim ret1 As Integer = clsAnvizNew.CChex_Update(anviz_handle, dev_idxData, TypeData, pBuffData, lenData)
                                If ret1 > 0 Then
                                    Select Case TypeData(0)
                                        Case CInt(clsAnvizNew.MsgType.CCHEX_RET_RECORD_INFO_TYPE), CInt(clsAnvizNew.MsgType.CCHEX_RET_GET_NEW_RECORD_INFO_TYPE)
                                            Dim record_info As clsAnvizNew.CCHEX_RET_RECORD_INFO_STRU
                                            record_info = CType(Marshal.PtrToStructure(pBuffData, GetType(clsAnvizNew.CCHEX_RET_RECORD_INFO_STRU)), clsAnvizNew.CCHEX_RET_RECORD_INFO_STRU)
                                            Dim dtdate As Date = New DateTime(2000, 1, 2).AddSeconds(swapInt32(BitConverter.ToUInt32(record_info.Date, 0)))
                                            If CInt(Employee_array_to_string(record_info.EmployeeId)) > 0 AndAlso eZeeDate.convertDate(dtdate.Date) >= eZeeDate.convertDate(dtpfromdate.Value.Date) _
                                                AndAlso eZeeDate.convertDate(dtdate.Date) <= eZeeDate.convertDate(dtpTodate.Value.Date) Then
                                                Dim mstrIp As String = dr("MachineIP").ToString().Trim()
                                                If drMachineDetails IsNot Nothing AndAlso drMachineDetails.Length > 0 Then
                                                    Dim xDeviceCode = drMachineDetails.ToList().Where(Function(x) x.Field(Of String)("ip") = mstrIp).DefaultIfEmpty()
                                                    If xDeviceCode IsNot Nothing AndAlso xDeviceCode.Count > 0 Then
                                                        mstrDeviceCode = xDeviceCode(0)("devicecode").ToString().Trim.Remove(xDeviceCode(0)("devicecode").ToString().Trim().IndexOf("||"), xDeviceCode(0)("devicecode").ToString().Trim.Length - xDeviceCode(0)("devicecode").ToString().IndexOf("||"))
                                                    Else
                                                        mstrDeviceCode = ""
                                                    End If
                                                Else
                                                    mstrDeviceCode = ""
                                                End If
                                                If InsertDeviceData(mstrDeviceCode, mstrIp, Employee_array_to_string(record_info.EmployeeId), dtdate.Date, CDate(dtdate), "0", "-1") = False Then
                                                    Exit While
                                                End If
                                            End If

                                    End Select

                                ElseIf ret1 = 0 Then
                                    Exit While

                                ElseIf ret1 < 0 Then
                                    lenData = lenData * 2

                                Else
                                    Exit While
                                End If   'If ret1 > 0 Then

                            End While

                            Marshal.FreeHGlobal(pBuffData) '// free the memory  

                        End If   'If retData = 1 Then

                    Next


                End If   'If dtMachineDetail IsNot Nothing AndAlso dtMachineDetail.Rows.Count > 0 Then


            End If  'If anviz_handle = IntPtr.Zero Then
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadNewAnvizData", mstrModuleName)
        Finally
        End Try
        'Return dtMachineDetail
    End Sub

    Private Function byte_to_string(ByVal StringData As Byte()) As String
        Try
            Return Encoding.Default.GetString(StringData).Replace(vbNullChar, "")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "byte_to_string", mstrModuleName)
        End Try
    End Function

    Private Function swapInt32(ByVal value As UInteger) As UInteger
        Try
            Return CUInt(((value And &HFF) << 24) Or ((value And &HFF00) << 8) Or ((value And &HFF0000) >> 8) Or ((value And &HFF000000L) >> 24))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "swapInt32", mstrModuleName)
        End Try
    End Function

    Private Function Employee_array_to_string(ByVal EmployeeId() As Byte) As String
        Try
            Dim temp(7) As Byte
            Dim i As Integer
            For i = 0 To 4
                temp(8 - 4 - i) = EmployeeId(i)
            Next i
            Return BitConverter.ToInt64(temp, 0).ToString()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Employee_array_to_string", mstrModuleName)
        End Try
    End Function

    'Pinkal (22-Apr-2020) -- End

    'Pinkal (27-Oct-2020) -- Start
    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
    Private Sub GetAutomaticDownloadedData()
        Try
            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            Dim dtFinalTable As New DataTable()
            dtFinalTable.Columns.Add("SrNo", Type.GetType("System.Int64"))
            dtFinalTable.Columns("SrNo").AutoIncrement = True
            dtFinalTable.Columns("SrNo").AutoIncrementSeed = 1
            dtFinalTable.Columns("SrNo").AutoIncrementStep = 1

            Dim objImgConverter As New ImageConverter()
            Dim dcColumn As New DataColumn("image", Type.GetType("System.Byte[]"))
            dcColumn.DefaultValue = objImgConverter.ConvertTo(New Drawing.Bitmap(1, 1), System.Type.GetType("System.Byte[]"))
            dtFinalTable.Columns.Add(dcColumn)
            objImgConverter = Nothing

            Dim objLogin As New clslogin_Tran
            Dim dtTable As DataTable = Nothing

            Me.Cursor = Cursors.WaitCursor

            objLogin.GetMinMaxDeviceAttendanceDate(mdtFromDate, mdtToDate)

            dtTable = objLogin.GetDeviceAttendanceData(chkUseMappedDeviceUser.Checked, mdtFromDate.Date, mdtToDate.Date, chkIncludeSuspendedEmpolyee.Checked)

            dtFinalTable.Merge(dtTable)

            ColhImage.DataPropertyName = "image"
            colhSrno.DataPropertyName = "SrNo"
            colhDevice.DataPropertyName = "Device"
            colhEmployeeName.DataPropertyName = "EmployeeName"
            colhShift.DataPropertyName = "ShiftName"
            colhLogindate.DataPropertyName = "Logindate"
            colhLoginTime.DataPropertyName = "Logintime"
            colhLoginTime.DefaultCellStyle.Format = "hh:mm:ss tt"
            colhMessage.DataPropertyName = "message"
            dgDownloaddata.AutoGenerateColumns = False
            dgDownloaddata.DataSource = dtFinalTable

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                dtpfromdate.Value = mdtFromDate.Date
                dtpTodate.Value = mdtToDate.Date
                lnkFillData.Enabled = False
                lnkGetDeviceList.Enabled = False
                dtpfromdate.Enabled = False
                dtpTodate.Enabled = False
            Else
                mdtFromDate = Nothing
                mdtToDate = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAutomaticDownloadedData", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    'Pinkal (27-Oct-2020) -- End


    'Pinkal (30-Sep-2021)-- Start
    'DERM Enhancement : HIK Vision Biometric Integration.
    Private Sub DownloadHIKVisionData()
        Try
            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.
            'CHCNetSDK.NET_DVR_Init()
            'Pinkal (28-Oct-2021)-- End

            mintHIKUserID = -1
            m_lGetAcsEventHandle = -1

            If dsDownload Is Nothing Then
                CreateDataset()
            End If

            Dim struLoginInfo As New CHCNetSDK.NET_DVR_USER_LOGIN_INFO()
            Dim struDeviceInfoV40 As New CHCNetSDK.NET_DVR_DEVICEINFO_V40()
            struDeviceInfoV40.struDeviceV30.sSerialNumber = New Byte(CHCNetSDK.SERIALNO_LEN - 1) {}

            struLoginInfo.sDeviceAddress = strIp
            struLoginInfo.sUserName = mstrUserID
            struLoginInfo.sPassword = mstrPassword
            UShort.TryParse(intPort.ToString(), struLoginInfo.wPort)

            mintHIKUserID = CHCNetSDK.NET_DVR_Login_V40(struLoginInfo, struDeviceInfoV40)

            If mintHIKUserID >= 0 Then

                Dim CsTemp As String = ""
                Dim MinorType As String = ""
                Dim MajorType As String = ""

                Dim struCond As New CHCNetSDK.NET_DVR_ACS_EVENT_COND()
                struCond.Init()
                struCond.dwSize = CUInt(Marshal.SizeOf(struCond))


                If GetAcsEventType.MajorTypeDictionary.Keys.Count <= 0 Then
                    GetAcsEventType.enMajorTypeDictionary()
                End If

                If GetAcsEventType.MinorTypeDictionary.Keys.Count <= 0 Then
                    GetAcsEventType.enMinorTypeDictionary()
                End If

                If GetAcsEventType.ComInductiveEvent.Keys.Count <= 0 Then
                    GetAcsEventType.enComInductiveEvent()
                End If

                struCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue("Event")
                struCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue("All")

                struCond.struStartTime.dwYear = dtpfromdate.Value.Year
                struCond.struStartTime.dwMonth = dtpfromdate.Value.Month
                struCond.struStartTime.dwDay = dtpfromdate.Value.Day
                struCond.struStartTime.dwHour = 0
                struCond.struStartTime.dwMinute = 0
                struCond.struStartTime.dwSecond = 0

                struCond.struEndTime.dwYear = dtpTodate.Value.Year
                struCond.struEndTime.dwMonth = dtpTodate.Value.Month
                struCond.struEndTime.dwDay = dtpTodate.Value.Day
                struCond.struEndTime.dwHour = 23
                struCond.struEndTime.dwMinute = 59
                struCond.struEndTime.dwSecond = 0

                struCond.byPicEnable = 0
                struCond.szMonitorID = ""
                struCond.wInductiveEventType = 65535

                Dim dwSize As UInteger = struCond.dwSize
                Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CInt(dwSize))
                Marshal.StructureToPtr(struCond, ptrCond, False)

                m_lGetAcsEventHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(mintHIKUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CInt(dwSize), Nothing, IntPtr.Zero)

                If m_lGetAcsEventHandle = -1 Then
                    Marshal.FreeHGlobal(ptrCond)
                    ListViewError("NET_DVR_StartRemoteConfig FAIL, ERROR CODE : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Exit Sub
                End If

                Dim objLogin As New clslogin_Tran
                If objLogin.DeleteDeviceAttendanceData(strIp) = False Then
                    ListViewError(objLogin._Message)
                End If
                objLogin = Nothing

                ProcessEvent()
                Marshal.FreeHGlobal(ptrCond)

            Else
                Dim nErr As UInteger = CHCNetSDK.NET_DVR_GetLastError()

                If nErr = CHCNetSDK.NET_DVR_PASSWORD_ERROR Then
                    ListViewError(Language.getMessage(mstrModuleName, 29, "Invalid User Name or password."))
                    If 1 = struDeviceInfoV40.bySupportLock Then
                        ListViewError(Language.getMessage(mstrModuleName, 30, "you have left") & " " & struDeviceInfoV40.byRetryLoginTime & " " & Language.getMessage(mstrModuleName, 31, "try opportunity."))
                    End If
                    Exit Sub
                ElseIf nErr = CHCNetSDK.NET_DVR_USER_LOCKED Then
                    If 1 = struDeviceInfoV40.bySupportLock Then
                        ListViewError(Language.getMessage(mstrModuleName, 32, "User is locked now.The Remaining Lock Time is") & " " & struDeviceInfoV40.dwSurplusLockTime)
                        Exit Sub
                    End If
                Else
                    ListViewError(Language.getMessage(mstrModuleName, 33, "net error or dvr is busy!"))
                    Exit Sub
                End If
            End If

            Dim lvItem As ListViewItem = lvDeviceList.FindItemWithText(strIp, True, 0)
            If lvItem IsNot Nothing Then
                lvItem.ForeColor = Color.Green
            End If

            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.
            'If mintHIKUserID >= 0 Then
            '    CHCNetSDK.NET_DVR_Logout_V30(mintHIKUserID)
            '    mintHIKUserID = -1
            'End If
            'CHCNetSDK.NET_DVR_Cleanup()
            'Pinkal (28-Oct-2021)-- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadHIKVisionData", mstrModuleName)
        End Try
    End Sub

    Public Sub ProcessEvent()
        Dim dwStatus As Integer = 0
        Dim Flag As Boolean = True
        Dim struCFG As New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
        struCFG.dwSize = CUInt(Marshal.SizeOf(struCFG))
        Dim dwOutBuffSize As Integer = CInt(Math.Truncate(struCFG.dwSize))
        struCFG.init()


        Do While Flag

            dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lGetAcsEventHandle, struCFG, dwOutBuffSize)

            Select Case dwStatus
                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                    AddAcsEventToList(struCFG)

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                    Thread.Sleep(10)

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                    ListViewError("NET_SDK_GET_NEXT_STATUS_FAILED : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Flag = False

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                    Flag = False
                Case Else
                    ListViewError("NET_SDK_GET_NEXT_STATUS_UNKOWN : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Flag = False
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
            End Select
        Loop
    End Sub

    Private Sub AddAcsEventToList(ByVal struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
        Try
            If struEventCfg.dwMinor <> CHCNetSDK.MINOR_FINGERPRINT_COMPARE_PASS AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_PASS _
              AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_CARD_FINGERPRINT_VERIFY_PASS AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_FINGERPRINT_PASSWD_VERIFY_PASS Then Exit Sub

            Dim mdtLogTime As String = GetStrLogTime(struEventCfg.struTime)
            If InsertDeviceData(mstrDeviceCode, strIp, System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).ToString(), CDate(mdtLogTime).Date, CDate(mdtLogTime), "0", "-1") = False Then
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddAcsEventToList", mstrModuleName)
        End Try
    End Sub

    Private Function GetStrLogTime(ByRef time As CHCNetSDK.NET_DVR_TIME) As String
        Dim res As String = ""
        Try
            res = time.dwYear.ToString() & "-" & time.dwMonth.ToString() & "-" & time.dwDay.ToString() & " " & time.dwHour.ToString("#00") & ":" & time.dwMinute.ToString("#00") & ":" & time.dwSecond.ToString("#00")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetStrLogTime", mstrModuleName)
        End Try
        Return res
    End Function

    'Pinkal (30-Sep-2021) -- End



#End Region

#Region "Button Event"


    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

    'Private Sub btnDownloaddata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownloaddata.Click

    '    Try

    '        If Validation() Then

    '            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") = False Then Exit Sub

    '            Dim dsMachineSetting As New DataSet
    '            dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

    '            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
    '                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid ='" & Company._Object._Companyunkid & "'")

    '                If dr.Length > 0 Then

    '                    btnExportError.Enabled = False
    '                    btnExportData.Enabled = True
    '                    If dsDownload IsNot Nothing Then
    '                        dsDownload.Tables(0).Rows.Clear()
    '                        dsDownload.Dispose()
    '                        dsDownload = Nothing
    '                        dgDownloaddata.DataSource = Nothing
    '                    End If

    '                    Dim t As Threading.Thread

    '                    For i As Integer = 0 To dr.Length - 1
    '                        intDeviceType = CInt(dr(i)("commdeviceid").ToString())
    '                        mstrDeviceCode = CStr(dr(i)("devicecode").ToString()).Trim.Remove(dr(i)("devicecode").ToString().IndexOf("||"), dr(i)("devicecode").ToString().Trim.Length - dr(i)("devicecode").ToString().IndexOf("||"))
    '                        strIp = dr(i)("ip").ToString()
    '                        intPort = CInt(dr(i)("port").ToString())
    '                        mintMachineSrNo = CInt(dr(i)("machinesrno").ToString())

    '                        If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
    '                            If IsDBNull(dr(i)("commkey")) = False AndAlso dr(i)("commkey").ToString.Trim.Length > 0 Then
    '                                mstrCommunicationKey = dr(i)("commkey").ToString()
    '                            End If
    '                        End If

    '                        If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
    '                            If IsDBNull(dr(i)("userid")) = False AndAlso dr(i)("userid").ToString.Trim.Length > 0 Then
    '                                mstrUserID = dr(i)("userid").ToString()
    '                            End If
    '                        End If

    '                        If dsMachineSetting.Tables(0).Columns.Contains("password") Then
    '                            If IsDBNull(dr(i)("password")) = False AndAlso dr(i)("password").ToString.Trim.Length > 0 Then
    '                                mstrPassword = dr(i)("password").ToString()
    '                            End If
    '                        End If

    '                        If intDeviceType <> enFingerPrintDevice.Handpunch Then
    '                            t = New Thread(AddressOf DeviceIntegration)
    '                            t.Start()
    '                            t.Join()
    '                            t = Nothing
    '                        ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
    '                            DownloadHandpunch_Data()
    '                            Exit For
    '                        End If
    '                    Next
    '                End If

    '            End If
    '            dsMachineSetting.Dispose()
    '            dsMachineSetting = Nothing

    '            Me.Cursor = Cursors.WaitCursor

    '            If dsDownload IsNot Nothing Then

    '                Dim dtTable As DataTable = New DataView(dsDownload.Tables("Download"), "", "EmployeeName asc,Logintime asc ", DataViewRowState.CurrentRows).ToTable()

    '                Dim dtFinalTable As New DataTable()
    '                dtFinalTable.Columns.Add("SrNo", Type.GetType("System.Int64"))
    '                dtFinalTable.Columns("SrNo").AutoIncrement = True
    '                dtFinalTable.Columns("SrNo").AutoIncrementSeed = 1
    '                dtFinalTable.Columns("SrNo").AutoIncrementStep = 1

    '                dtFinalTable.Merge(dtTable)

    '                ColhImage.DataPropertyName = "image"
    '                colhSrno.DataPropertyName = "SrNo"
    '                colhDevice.DataPropertyName = "Device"
    '                colhEmployeeName.DataPropertyName = "EmployeeName"
    '                colhShift.DataPropertyName = "ShiftName"
    '                colhLogindate.DataPropertyName = "Logindate"
    '                colhLoginTime.DataPropertyName = "Logintime"
    '                colhLoginTime.DefaultCellStyle.Format = "hh:mm:ss tt"
    '                colhMessage.DataPropertyName = "message"
    '                dgDownloaddata.AutoGenerateColumns = False
    '                dgDownloaddata.DataSource = dtFinalTable

    '            End If

    '        End If

    '    Catch ex As Runtime.InteropServices.SEHException

    '    Catch ex As AccessViolationException

    '    Catch ex As ExecutionEngineException

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnDownloaddata_Click", mstrModuleName)
    '    Finally
    '        Me.Cursor = Cursors.Default
    '    End Try
    'End Sub

    'Pinkal (27-Mar-2017) -- End

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim mstrMsg As String = ""
            If Validation() Then
                SaveAttendanceData(mstrMsg)
                If dgDownloaddata.Rows.Count > 0 Then
                    btnExportError.Enabled = True
                    btnExportData.Enabled = False
                End If
            End If


            'Pinkal (27-Oct-2020) -- Start
            'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
            Me.Cursor = Cursors.WaitCursor

            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            'If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") AndAlso mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
            If (ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() <> "00:00" AndAlso ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() <> "") AndAlso mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                'Pinkal (27-Apr-2021) -- End

                Dim objLogin As New clslogin_Tran
                Dim objfrmsendmail As New frmSendMail
                Dim strFileName As String = ""
                Dim objUser As New clsUserAddEdit

                If ConfigParameter._Object._SendTnAEarlyLateReportsUserIds.Trim.Length > 0 Then

                    'mdtFromDate = eZeeDate.convertDate("20180101").Date
                    'mdtToDate = eZeeDate.convertDate("20180131").Date

                    For Each xUserId As String In ConfigParameter._Object._SendTnAEarlyLateReportsUserIds.Trim.Split(CChar(","))

                        If xUserId.Trim = "" Then Continue For

                        strFileName = objfrmsendmail.Export_ETnAEarlyLateReport(FinancialYear._Object._DatabaseName, CInt(xUserId), FinancialYear._Object._YearUnkid _
                                                                                                      , Company._Object._Companyunkid, mdtFromDate.Date, mdtToDate.Date, True _
                                                                                                      , ConfigParameter._Object._UserAccessModeSetting)

                        objUser._Userunkid = CInt(xUserId)
                        Dim mstrUserName As String = ""

                        If objUser._Firstname.Trim().Length > 0 AndAlso objUser._Lastname.Trim.Length > 0 Then
                            mstrUserName = objUser._Firstname & " " & objUser._Lastname
                        ElseIf objUser._Firstname.Trim.Length > 0 AndAlso objUser._Lastname.Trim.Length <= 0 Then
                            mstrUserName = objUser._Firstname
                        Else
                            mstrUserName = objUser._Username
                        End If

                        objLogin.SendMailToUsersForEarlyLate(Company._Object._Companyunkid, CInt(xUserId), mstrUserName, objUser._Email, mstrExportEPaySlipPath, strFileName, enLogin_Mode.DESKTOP, 0)

                    Next

                End If

                If ConfigParameter._Object._TnAFailureNotificationUserIds.Trim.Length > 0 Then

                    Dim dtTable As DataTable = CType(dgDownloaddata.DataSource, DataTable)

                    dtTable = New DataView(dtTable, "IsError=1", "", DataViewRowState.CurrentRows).ToTable()

                    If dtTable.Rows.Count > 0 Then

                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("EmployeeId")
                        dtTable.Columns.Remove("ShiftId")
                        dtTable.Columns.Remove("VerifyMode")
                        dtTable.Columns.Remove("InOutMode")
                        dtTable.Columns.Remove("IsError")


                        Dim strReportName As String = String.Empty
                        Dim strOriginalReportName As String = String.Empty
                        strReportName = "Employees Attendance Error Log"
                        strOriginalReportName = strReportName

                        strReportName = strReportName.Replace(" ", "_") & "_" & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss") & ".xlsx"

                        If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                            System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
                        End If

                        modGlobal.Export_ErrorList(mstrExportEPaySlipPath & "/" & strReportName, dtTable, "Employees Attendance Error Log")

                        For Each xUserId As String In ConfigParameter._Object._TnAFailureNotificationUserIds.Trim.Split(CChar(","))

                            If xUserId.Trim = "" Then Continue For

                            objUser._Userunkid = CInt(xUserId)
                            Dim mstrUserName As String = ""

                            If objUser._Firstname.Trim().Length > 0 AndAlso objUser._Lastname.Trim.Length > 0 Then
                                mstrUserName = objUser._Firstname & " " & objUser._Lastname
                            ElseIf objUser._Firstname.Trim.Length > 0 AndAlso objUser._Lastname.Trim.Length <= 0 Then
                                mstrUserName = objUser._Firstname
                            Else
                                mstrUserName = objUser._Username
                            End If

                            objLogin.SendFailedNotificationToUser(Company._Object._Companyunkid, CInt(xUserId), mstrUserName, objUser._Email, mstrExportEPaySlipPath, strReportName, enLogin_Mode.DESKTOP, 0)

                        Next

                        If IO.File.Exists(mstrExportEPaySlipPath & "/" & strReportName) Then
                            IO.File.Delete(mstrExportEPaySlipPath & "/" & strReportName)
                        End If

                    End If

                End If

                eZeeMsgBox.Show(mstrMsg, enMsgBoxStyle.Information)
                objfrmsendmail = Nothing
                mdtFromDate = Nothing
                mdtToDate = Nothing
            End If
            'Pinkal (27-Oct-2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            If dsDownload IsNot Nothing Then
                dsDownload.Dispose()
                dsDownload = Nothing
            End If
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (27-Oct-2020) -- Start
            'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.


            'Pinkal (30-Sep-2021)-- Start
            'DERM Enhancement : HIK Vision Biometric Integration.
            If ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "00:00:00" OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "00:00" _
            OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString().Trim() = "12:00:00 AM" OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToShortTimeString.Trim() = "12:00 AM" _
            OrElse ConfigParameter._Object._RunDailyAttendanceServiceTime.ToString().Trim() = "" Then
                'Pinkal (30-Sep-2021) -- End

            Dim objLogin As New clslogin_Tran
            If objLogin.DeleteDeviceAttendanceData() = False Then
                eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
            End If
            objLogin = Nothing
            End If
            'Pinkal (27-Oct-2020) -- End

            'Pinkal (30-Sep-2021)-- Start
            'DERM Enhancement : HIK Vision Biometric Integration.
            If mintHIKUserID >= 0 Then
                CHCNetSDK.NET_DVR_Logout_V30(mintHIKUserID)
                mintHIKUserID = -1
                CHCNetSDK.NET_DVR_Cleanup()
            End If
            'Pinkal (30-Sep-2021) -- End


            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnExportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportData.Click
        Try
            Dim dtTable As DataTable = CType(dgDownloaddata.DataSource, DataTable)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                dtTable = dtTable.Copy
                Dim savDialog As New SaveFileDialog
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("IsError")
                    dtTable.Columns.Remove("EmployeeId")
                    dtTable.Columns.Remove("ShiftId")
                    dtTable.Columns.Remove("VerifyMode")
                    dtTable.Columns.Remove("InOutMode")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Timesheet Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportData_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgDownloaddata_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgDownloaddata.DataError

    End Sub

#End Region

#Region "ContextMenu Event"

    Private Sub btnExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportError.Click
        Try
            Dim dtTable As DataTable = CType(dgDownloaddata.DataSource, DataTable)

            If dtTable Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "There is no error in attendance data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If


            dtTable = New DataView(dtTable, "IsError=1", "", DataViewRowState.CurrentRows).ToTable()
            If dtTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "There is no error in attendance data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                dtTable.Columns.Remove("image")
                dtTable.Columns.Remove("EmployeeId")
                dtTable.Columns.Remove("ShiftId")
                dtTable.Columns.Remove("VerifyMode")
                dtTable.Columns.Remove("InOutMode")
                dtTable.Columns.Remove("IsError")
                If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Export Device Attendance Data") = True Then
                    Process.Start(savDialog.FileName)
                    dtTable.Rows.Clear()
                    dgDownloaddata.DataSource = dtTable

                    'Pinkal (24-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    'cmnuAttendanceData.Enabled = False
                    btnExportError.Enabled = False
                    'Pinkal (24-Jan-2014) -- End

                End If
            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportError_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (27-Mar-2017) -- Start
    'Enhancement - Working On Import Device Attendance Data.

#Region "Link Button Events"

    Private Sub lnkFillData_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkFillData.LinkClicked
        Try

            dgDownloaddata.DataSource = Nothing

            If Validation() Then


                If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") = False Then Exit Sub

                Dim dsMachineSetting As New DataSet
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                Me.Cursor = Cursors.WaitCursor

                If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                    Dim mstrIPList As String = ""
                    If lvDeviceList.CheckedItems.Count > 0 Then
                        mstrIPList = String.Join("','", (From p In lvDeviceList.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhIPAddress.Index).Text.ToString)).ToArray)
                    End If

                    Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid ='" & Company._Object._Companyunkid & "' AND IP in ('" & mstrIPList.Trim & "')")

                    If dr.Length > 0 Then

                        btnExportError.Enabled = False
                        btnExportData.Enabled = True
                        If dsDownload IsNot Nothing Then
                            dsDownload.Tables(0).Rows.Clear()
                            dsDownload.Dispose()
                            dsDownload = Nothing
                            dgDownloaddata.DataSource = Nothing
                        End If



                        'Pinkal (22-Apr-2020) -- Start
                        'Enhancement - New Anviz Integration for Gran Melia.
                        Dim drRow As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid = '" & Company._Object._Companyunkid & "' AND IP in ('" & mstrIPList.Trim & "') AND  commdeviceid = " & enFingerPrintDevice.NewAnviz)
                        If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                            DownloadNewAnvizData(drRow)
                        Else
                        Dim t As Threading.Thread

                        For i As Integer = 0 To dr.Length - 1
                            intDeviceType = CInt(dr(i)("commdeviceid").ToString())
                            mstrDeviceCode = CStr(dr(i)("devicecode").ToString()).Trim.Remove(dr(i)("devicecode").ToString().IndexOf("||"), dr(i)("devicecode").ToString().Trim.Length - dr(i)("devicecode").ToString().IndexOf("||"))
                            strIp = dr(i)("ip").ToString()

                            'Pinkal (04-Oct-2017) -- Start
                            'Enhancement - Working SAGEM Database Device Integration.
                            If IsDBNull(dr(i)("port")) = False AndAlso dr(i)("port").ToString() <> "" Then
                            intPort = CInt(dr(i)("port").ToString())
                            End If
                            'Pinkal (04-Oct-2017) -- End

                            mintMachineSrNo = CInt(dr(i)("machinesrno").ToString())

                            If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
                                If IsDBNull(dr(i)("commkey")) = False AndAlso dr(i)("commkey").ToString.Trim.Length > 0 Then
                                    mstrCommunicationKey = dr(i)("commkey").ToString()
                                End If
                            End If

                            If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
                                If IsDBNull(dr(i)("userid")) = False AndAlso dr(i)("userid").ToString.Trim.Length > 0 Then
                                    mstrUserID = dr(i)("userid").ToString()
                                End If
                            End If

                            If dsMachineSetting.Tables(0).Columns.Contains("password") Then
                                If IsDBNull(dr(i)("password")) = False AndAlso dr(i)("password").ToString.Trim.Length > 0 Then
                                    mstrPassword = dr(i)("password").ToString()
                                End If
                            End If

                            'Sohail (29 Jun 2019) -- Start
                            'Eko Supreme Enhancement # 0003891 - 76.1 - Integration with FingerTec attendance device.
                            If dsMachineSetting.Tables(0).Columns.Contains("devicemodel") Then
                                If IsDBNull(dr(i)("devicemodel")) = False AndAlso dr(i)("devicemodel").ToString.Trim.Length > 0 Then
                                    mstrDeviceModel = dr(i)("devicemodel").ToString()
                                End If
                            End If
                            'Sohail (29 Jun 2019) -- End



                                'Pinkal (30-Sep-2021)-- Start
                                'DERM Enhancement : HIK Vision Biometric Integration.

                            If intDeviceType <> enFingerPrintDevice.Handpunch AndAlso intDeviceType <> enFingerPrintDevice.SAGEM Then
                                t = New Thread(AddressOf DeviceIntegration)
                                t.Start()
                                t.Join()
                                t = Nothing

                            ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
                                DownloadHandpunch_Data()
                                Exit For

                            ElseIf intDeviceType = enFingerPrintDevice.SAGEM Then
                                DownloadSAGEM_Data()
                                Exit For

                                    'ElseIf intDeviceType = enFingerPrintDevice.HIKVision Then
                                    '    If i = 0 Then CHCNetSDK.NET_DVR_Init()
                                    '    DownloadHIKVisionData()
                                    '    Exit For

                            End If

                                'Pinkal (30-Sep-2021) -- End

                        Next
                    End If
                        'Pinkal (22-Apr-2020) -- End



                    End If

                End If
                dsMachineSetting.Dispose()
                dsMachineSetting = Nothing

                If dsDownload IsNot Nothing Then


                    Dim dtFinalTable As New DataTable()
                    dtFinalTable.Columns.Add("SrNo", Type.GetType("System.Int64"))
                    dtFinalTable.Columns("SrNo").AutoIncrement = True
                    dtFinalTable.Columns("SrNo").AutoIncrementSeed = 1
                    dtFinalTable.Columns("SrNo").AutoIncrementStep = 1

                    'Pinkal (27-Mar-2017) -- Start
                    'Enhancement - Working On Import Device Attendance Data.
                    Dim dtTable As DataTable = Nothing
                    If dsDownload.Tables(0).Rows.Count > 0 Then
                        dtTable = New DataView(dsDownload.Tables("Download"), "", "EmployeeName asc,Logintime asc ", DataViewRowState.CurrentRows).ToTable()
                    Else
                        Dim objLogin As New clslogin_Tran

                        'Pinkal (06-Mar-2018) -- Start
                        'BUG - BUG SOLVED FOR SHIFT SHOWING BLANK WHEN IMPORT DEVICE ATTENDANCE.

                        'Pinkal (21-Dec-2019) -- Start
                        'Enhancement HILL PACKAGING COMPANY LTD [0003462] -   Feature to exclude suspended employees from attendance in case they clock in while on suspension.
                        'dtTable = objLogin.GetDeviceAttendanceData(chkUseMappedDeviceUser.Checked, dtpfromdate.Value.Date, dtpTodate.Value.Date)
                        dtTable = objLogin.GetDeviceAttendanceData(chkUseMappedDeviceUser.Checked, dtpfromdate.Value.Date, dtpTodate.Value.Date, chkIncludeSuspendedEmpolyee.Checked)
                        'Pinkal (21-Dec-2019) -- End

                        'Pinkal (06-Mar-2018) -- End

                        objLogin = Nothing

                        Dim objImgConverter As New ImageConverter()
                        Dim dcColumn As New DataColumn("image", Type.GetType("System.Byte[]"))
                        dcColumn.DefaultValue = objImgConverter.ConvertTo(New Drawing.Bitmap(1, 1), System.Type.GetType("System.Byte[]"))
                        dtFinalTable.Columns.Add(dcColumn)
                        objImgConverter = Nothing
                    End If
                    'Pinkal (27-Mar-2017) -- End

                    dtFinalTable.Merge(dtTable)

                    ColhImage.DataPropertyName = "image"
                    colhSrno.DataPropertyName = "SrNo"
                    colhDevice.DataPropertyName = "Device"
                    colhEmployeeName.DataPropertyName = "EmployeeName"
                    colhShift.DataPropertyName = "ShiftName"
                    colhLogindate.DataPropertyName = "Logindate"
                    colhLoginTime.DataPropertyName = "Logintime"
                    colhLoginTime.DefaultCellStyle.Format = "hh:mm:ss tt"
                    colhMessage.DataPropertyName = "message"
                    dgDownloaddata.AutoGenerateColumns = False
                    dgDownloaddata.DataSource = dtFinalTable

                End If

            End If

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkFillData_LinkClicked", mstrModuleName)
        Finally

            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub lnkGetDeviceList_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkGetDeviceList.LinkClicked
        Try
            FillDeviceData()
            objchkSelectAll.Checked = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Listview Events"

    Private Sub lvDeviceList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDeviceList.ItemChecked
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            If lvDeviceList.CheckedItems.Count <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvDeviceList.CheckedItems.Count < lvDeviceList.Items.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvDeviceList.CheckedItems.Count = lvDeviceList.Items.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDeviceList_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvDeviceList_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvDeviceList.MouseClick
        Try
            If lvDeviceList.SelectedItems.Count > 0 Then
                If lvDeviceList.SelectedItems(0).ForeColor = Color.Blue AndAlso lvDeviceList.SelectedItems(0).Text = Language.getMessage(mstrModuleName, 24, "Error") Then
                    If lvDeviceList.SelectedItems(0).Tag IsNot Nothing AndAlso lvDeviceList.SelectedItems(0).Tag.ToString().Trim.Length > 0 Then
                        Dim objFrm As New frmRemark
                        objFrm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 25, "Error / Warning")
                        objFrm.Text = Language.getMessage(mstrModuleName, 25, "Error / Warning")
                        objFrm.displayDialog(lvDeviceList.SelectedItems(0).Tag.ToString(), enArutiApplicatinType.Aruti_Payroll)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDeviceList_MouseClick", mstrModuleName)
        End Try
    End Sub

    'Private Sub lvDeviceList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDeviceList.SelectedIndexChanged
    '    Try

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvDeviceList_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "Checkbox Event"

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            RemoveHandler lvDeviceList.ItemChecked, AddressOf lvDeviceList_ItemChecked
            For Each lvItem As ListViewItem In lvDeviceList.Items
                lvItem.Checked = CBool(objchkSelectAll.CheckState)
            Next
            AddHandler lvDeviceList.ItemChecked, AddressOf lvDeviceList_ItemChecked
        Catch ex As Exception

        End Try
    End Sub

#End Region

    'Pinkal (27-Mar-2017) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbDeviceList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDeviceList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDownloaddata.GradientBackColor = GUI._ButttonBackColor
            Me.btnDownloaddata.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnImport.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblfromdate.Text = Language._Object.getCaption(Me.lblfromdate.Name, Me.lblfromdate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDownloaddata.Text = Language._Object.getCaption(Me.btnDownloaddata.Name, Me.btnDownloaddata.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.chkUseMappedDeviceUser.Text = Language._Object.getCaption(Me.chkUseMappedDeviceUser.Name, Me.chkUseMappedDeviceUser.Text)
            Me.btnExportError.Text = Language._Object.getCaption(Me.btnExportError.Name, Me.btnExportError.Text)
            Me.btnExportData.Text = Language._Object.getCaption(Me.btnExportData.Name, Me.btnExportData.Text)
            Me.gbDeviceList.Text = Language._Object.getCaption(Me.gbDeviceList.Name, Me.gbDeviceList.Text)
            Me.colhDeviceList.Text = Language._Object.getCaption(CStr(Me.colhDeviceList.Tag), Me.colhDeviceList.Text)
            Me.lnkFillData.Text = Language._Object.getCaption(Me.lnkFillData.Name, Me.lnkFillData.Text)
            Me.colhIPAddress.Text = Language._Object.getCaption(CStr(Me.colhIPAddress.Tag), Me.colhIPAddress.Text)
            Me.lnkGetDeviceList.Text = Language._Object.getCaption(Me.lnkGetDeviceList.Name, Me.lnkGetDeviceList.Text)
            Me.ColhImage.HeaderText = Language._Object.getCaption(Me.ColhImage.Name, Me.ColhImage.HeaderText)
            Me.colhSrno.HeaderText = Language._Object.getCaption(Me.colhSrno.Name, Me.colhSrno.HeaderText)
            Me.colhDevice.HeaderText = Language._Object.getCaption(Me.colhDevice.Name, Me.colhDevice.HeaderText)
            Me.colhEmployeeName.HeaderText = Language._Object.getCaption(Me.colhEmployeeName.Name, Me.colhEmployeeName.HeaderText)
            Me.colhLogindate.HeaderText = Language._Object.getCaption(Me.colhLogindate.Name, Me.colhLogindate.HeaderText)
            Me.colhShift.HeaderText = Language._Object.getCaption(Me.colhShift.Name, Me.colhShift.HeaderText)
            Me.colhLoginTime.HeaderText = Language._Object.getCaption(Me.colhLoginTime.Name, Me.colhLoginTime.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.LblErrorInfo.Text = Language._Object.getCaption(Me.LblErrorInfo.Name, Me.LblErrorInfo.Text)
            Me.LblSuccessInfo.Text = Language._Object.getCaption(Me.LblSuccessInfo.Name, Me.LblSuccessInfo.Text)
			Me.chkIncludeSuspendedEmpolyee.Text = Language._Object.getCaption(Me.chkIncludeSuspendedEmpolyee.Name, Me.chkIncludeSuspendedEmpolyee.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Reading data from terminal failed,ErrorCode:")
            Language.setMessage(mstrModuleName, 3, "There is no data to download from device.")
            Language.setMessage(mstrModuleName, 4, "Data successfully import.")
            Language.setMessage(mstrModuleName, 5, "Please Download Device Data First.")
            Language.setMessage(mstrModuleName, 7, "Please select atleast device from list to do further operation on it.")
            Language.setMessage(mstrModuleName, 8, "Unable to connect the device.")
            Language.setMessage(mstrModuleName, 9, "Some Data did not import due to error.")
            Language.setMessage(mstrModuleName, 10, "MisMatch Login/Logout.")
            Language.setMessage(mstrModuleName, 11, "There is no error in attendance data.")
            Language.setMessage(mstrModuleName, 12, "Connect device Failed! The error id is:")
            Language.setMessage(mstrModuleName, 13, "Sorry, Payroll payment is already done for the this employee for the selected date tenure, due to this employee will be skipped for that particular date during this process.")
            Language.setMessage(mstrModuleName, 14, "Error in Netdaemonreday.")
            Language.setMessage(mstrModuleName, 15, "Checksum Error.")
            Language.setMessage(mstrModuleName, 16, "Memory Full.")
            Language.setMessage(mstrModuleName, 17, "Invalid File Name.")
            Language.setMessage(mstrModuleName, 18, "File Cannot Open.")
            Language.setMessage(mstrModuleName, 19, "File Content Bad.")
            Language.setMessage(mstrModuleName, 20, "File Cannot Create.")
            Language.setMessage(mstrModuleName, 21, "Not this Person.")
            Language.setMessage(mstrModuleName, 22, "Invalid Parameter.")
            Language.setMessage(mstrModuleName, 23, "There is no device configured in Aruti Configuration.Please configure atleast one device in order to do further operation on it.")
            Language.setMessage(mstrModuleName, 24, "Error")
            Language.setMessage(mstrModuleName, 25, "Error / Warning")
			Language.setMessage(mstrModuleName, 26, "Startup errors,Please restart the program.")
            Language.setMessage(mstrModuleName, 29, "Invalid User Name or password.")
            Language.setMessage(mstrModuleName, 30, "you have left")
            Language.setMessage(mstrModuleName, 31, "try opportunity.")
            Language.setMessage(mstrModuleName, 32, "User is locked now.The Remaining Lock Time is")
            Language.setMessage(mstrModuleName, 33, "net error or dvr is busy!")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
