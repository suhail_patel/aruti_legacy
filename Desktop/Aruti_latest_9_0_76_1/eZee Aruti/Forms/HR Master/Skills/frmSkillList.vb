﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmSkillList

#Region "Private Variable"

    Private objSkillMaster As clsskill_master
    Private ReadOnly mstrModuleName As String = "frmSkillList"

#End Region

#Region "Form's Event"

    Private Sub frmSkillList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSkillMaster = New clsskill_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            fillList()

            If lvSkillcodes.Items.Count > 0 Then lvSkillcodes.Items(0).Selected = True
            lvSkillcodes.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSkillList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvSkillcodes.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkillList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSkillList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSkillMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsskill_master.SetMessages()
            objfrm._Other_ModuleNames = "clsskill_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmSkill_AddEdit As New frmSkill_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmSkill_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmSkill_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmSkill_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmSkill_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvSkillcodes.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Skill from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvSkillcodes.Select()
                Exit Sub
            End If
            Dim objfrmSkill_AddEdit As New frmSkill_AddEdit
            Try
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmSkill_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmSkill_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmSkill_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvSkillcodes.SelectedItems(0).Index
                If objfrmSkill_AddEdit.displayDialog(CInt(lvSkillcodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmSkill_AddEdit = Nothing

                lvSkillcodes.Items(intSelectedIndex).Selected = True
                lvSkillcodes.EnsureVisible(intSelectedIndex)
                lvSkillcodes.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmSkill_AddEdit IsNot Nothing Then objfrmSkill_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSkillcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Skill from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSkillcodes.Select()
            Exit Sub
        End If
        If objSkillMaster.isUsed(CInt(lvSkillcodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Skill. Reason: This Skill is in use."), enMsgBoxStyle.Information) '?2
            lvSkillcodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSkillcodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Skill?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objSkillMaster.Delete(CInt(lvSkillcodes.SelectedItems(0).Tag))
                lvSkillcodes.SelectedItems(0).Remove()

                If lvSkillcodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvSkillcodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvSkillcodes.Items.Count - 1
                    lvSkillcodes.Items(intSelectedIndex).Selected = True
                    lvSkillcodes.EnsureVisible(intSelectedIndex)
                ElseIf lvSkillcodes.Items.Count <> 0 Then
                    lvSkillcodes.Items(intSelectedIndex).Selected = True
                    lvSkillcodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvSkillcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsSkillList As New DataSet
        Try

            If User._Object.Privilege._AllowToViewSkillMasterList = True Then   'Pinkal (09-Jul-2012) -- Start

                dsSkillList = objSkillMaster.GetList("List")

                Dim lvItem As ListViewItem

                lvSkillcodes.Items.Clear()
                For Each drRow As DataRow In dsSkillList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("name").ToString
                    lvItem.Tag = drRow("skillunkid")
                    lvItem.SubItems.Add(drRow("skillcode").ToString)
                    lvItem.SubItems.Add(drRow("skillname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvSkillcodes.Items.Add(lvItem)
                Next

                If lvSkillcodes.Items.Count > 16 Then
                    colhDescription.Width = 350 - 18
                Else
                    colhDescription.Width = 350
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsSkillList.Dispose()
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddSkills
            btnEdit.Enabled = User._Object.Privilege._EditSkills
            btnDelete.Enabled = User._Object.Privilege._DeleteSkills

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhSkillCategory.Text = Language._Object.getCaption(CStr(Me.colhSkillCategory.Tag), Me.colhSkillCategory.Text)
            Me.colhSkillCode.Text = Language._Object.getCaption(CStr(Me.colhSkillCode.Tag), Me.colhSkillCode.Text)
            Me.colhSkillName.Text = Language._Object.getCaption(CStr(Me.colhSkillName.Tag), Me.colhSkillName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Skill from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Skill. Reason: This Skill is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Skill?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class