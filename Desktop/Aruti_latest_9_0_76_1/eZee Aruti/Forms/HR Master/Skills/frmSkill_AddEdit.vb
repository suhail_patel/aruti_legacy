﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmSkill_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmSkill_AddEdit"
    Private mblnCancel As Boolean = True
    Private objSkillMaster As clsskill_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSkillMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSkillMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintSkillMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmSkill_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSkillMaster = New clsskill_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objSkillMaster._Skillunkid = mintSkillMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkill_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    Private Sub frmSkill_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmSkill_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSkill_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSkill_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSkillMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsskill_master.SetMessages()
            objfrm._Other_ModuleNames = "clsskill_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboCategory.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Skill Category is compulsory information.Please Select Skill Category."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Skill Code cannot be blank. Skill Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Skill Name cannot be blank. Skill Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objSkillMaster.Update()
            Else
                blnFlag = objSkillMaster.Insert()
            End If

            If blnFlag = False And objSkillMaster._Message <> "" Then
                eZeeMsgBox.Show(objSkillMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objSkillMaster = Nothing
                    objSkillMaster = New clsskill_master
                    Call GetValue()
                    cboCategory.Select()
                Else
                    mintSkillMasterUnkid = objSkillMaster._Skillunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrmLangPopup.displayDialog(txtName.Text, objSkillMaster._Skillname1, objSkillMaster._Skillname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Try
            Dim objfrmCommonmaster As New frmCommonMaster
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmCommonmaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonmaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonmaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            objfrmCommonmaster.displayDialog(-1, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
            FillCombo()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            txtCode.Text = objSkillMaster._Skillcode
            txtName.Text = objSkillMaster._Skillname
            cboCategory.SelectedValue = objSkillMaster._Skillcategoryunkid
            txtDescription.Text = objSkillMaster._Description
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objSkillMaster._Skillcode = txtCode.Text.Trim
            objSkillMaster._Skillname = txtName.Text.Trim
            objSkillMaster._Skillcategoryunkid = CInt(cboCategory.SelectedValue)
            objSkillMaster._Description = txtDescription.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsCategory As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
            cboCategory.ValueMember = "masterunkid"
            cboCategory.DisplayMember = "name"
            cboCategory.DataSource = dsCategory.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddCategory.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSkill.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSkill.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.gbSkill.Text = Language._Object.getCaption(Me.gbSkill.Name, Me.gbSkill.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Skill Category is compulsory information.Please Select Skill Category.")
			Language.setMessage(mstrModuleName, 2, "Skill Code cannot be blank. Skill Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Skill Name cannot be blank. Skill Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class