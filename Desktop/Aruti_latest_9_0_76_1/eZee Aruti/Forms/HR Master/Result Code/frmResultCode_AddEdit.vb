﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmResultCode_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmResultCode_AddEdit"
    Private mblnCancel As Boolean = True
    Private objResultMaster As clsresult_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintResultMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintResultMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintResultMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmResultCode_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objResultMaster = New clsresult_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objResultMaster._Resultunkid = mintResultMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboResultGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmResultCode_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmResultCode_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmResultCode_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmResultCode_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmResultCode_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmResultCode_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objResultMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsresult_master.SetMessages()
            objfrm._Other_ModuleNames = "clsresult_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboResultGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Result Group is compulsory information.Please Select Result Group."), enMsgBoxStyle.Information)
                cboResultGroup.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Result Code cannot be blank. Result Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Result Name cannot be blank. Result Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objResultMaster.Update()
            Else
                blnFlag = objResultMaster.Insert()
            End If

            If blnFlag = False And objResultMaster._Message <> "" Then
                eZeeMsgBox.Show(objResultMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objResultMaster = Nothing
                    objResultMaster = New clsresult_master
                    Call GetValue()
                    cboResultGroup.Select()
                Else
                    mintResultMasterUnkid = objResultMaster._Resultunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            objFrmLangPopup.displayDialog(txtName.Text, objResultMaster._Resultname1, objResultMaster._Resultname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objCommonMaster As New frmCommonMaster
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommonMaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            objCommonMaster.displayDialog(-1, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE)
            FillCombo()
            cboResultGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            nudLevel.BackColor = GUI.ColorOptional
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objResultMaster._Resultcode
            txtName.Text = objResultMaster._Resultname
            cboResultGroup.SelectedValue = objResultMaster._Resultgroupunkid
            txtDescription.Text = objResultMaster._Description


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            nudLevel.Value = CInt(objResultMaster._ResultLevel)
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objResultMaster._Resultcode = txtCode.Text.Trim
            objResultMaster._Resultname = txtName.Text.Trim
            objResultMaster._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
            objResultMaster._Description = txtDescription.Text.Trim


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objResultMaster._ResultLevel = CInt(nudLevel.Value)
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonmaster As New clsCommon_Master
            Dim dsResultGroup As DataSet = objCommonmaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "Result Group")
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.DataSource = dsResultGroup.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			Me.gbResultCode.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbResultCode.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbResultCode.Text = Language._Object.getCaption(Me.gbResultCode.Name, Me.gbResultCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblResultName.Text = Language._Object.getCaption(Me.lblResultName.Name, Me.lblResultName.Text)
			Me.lblResultCode.Text = Language._Object.getCaption(Me.lblResultCode.Name, Me.lblResultCode.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.lblLevelDesc.Text = Language._Object.getCaption(Me.lblLevelDesc.Name, Me.lblLevelDesc.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Result Group is compulsory information.Please Select Result Group.")
			Language.setMessage(mstrModuleName, 2, "Result Code cannot be blank. Result Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Result Name cannot be blank. Result Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class