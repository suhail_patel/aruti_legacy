﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmQualificationCourseList

#Region "Private Variable"

    Private objQualificationMaster As clsqualification_master
    Private ReadOnly mstrModuleName As String = "frmQualificationCourseList"

#End Region

#Region "Form's Event"

    Private Sub frmQualificationCourseList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objQualificationMaster = New clsqualification_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'fillList()
            FillCombo()
            lvQualificationcodes.GridLines = False
            'Pinkal (22-MAY-2012) -- End

            Call SetVisibility()


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'If lvQualificationcodes.Items.Count > 0 Then lvQualificationcodes.Items(0).Selected = True
            'lvQualificationcodes.Select()
            cboQualificationGrp.Select()
            'Pinkal (22-MAY-2012) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationCourseList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationCourseList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvQualificationcodes.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationCourseList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationCourseList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objQualificationMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsqualification_master.SetMessages()
            objfrm._Other_ModuleNames = "clsqualification_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmQualificationCourse_AddEdit As New frmQualificationCourse_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmQualificationCourse_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmQualificationCourse_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmQualificationCourse_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmQualificationCourse_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvQualificationcodes.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Qualification from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvQualificationcodes.Select()
                Exit Sub
            End If
            Dim objfrmQualificationCourse_AddEdit As New frmQualificationCourse_AddEdit
            Try
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                If User._Object._Isrighttoleft = True Then
                    objfrmQualificationCourse_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmQualificationCourse_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmQualificationCourse_AddEdit)
                End If
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvQualificationcodes.SelectedItems(0).Index
                If objfrmQualificationCourse_AddEdit.displayDialog(CInt(lvQualificationcodes.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmQualificationCourse_AddEdit = Nothing

                lvQualificationcodes.Items(intSelectedIndex).Selected = True
                lvQualificationcodes.EnsureVisible(intSelectedIndex)
                lvQualificationcodes.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmQualificationCourse_AddEdit IsNot Nothing Then objfrmQualificationCourse_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvQualificationcodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Qualification from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvQualificationcodes.Select()
            Exit Sub
        End If
        If objQualificationMaster.isUsed(CInt(lvQualificationcodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Qualification. Reason: This Qualification is in use."), enMsgBoxStyle.Information) '?2
            lvQualificationcodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvQualificationcodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Qualification?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objQualificationMaster.Delete(CInt(lvQualificationcodes.SelectedItems(0).Tag))
                lvQualificationcodes.SelectedItems(0).Remove()

                If lvQualificationcodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvQualificationcodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvQualificationcodes.Items.Count - 1
                    lvQualificationcodes.Items(intSelectedIndex).Selected = True
                    lvQualificationcodes.EnsureVisible(intSelectedIndex)
                ElseIf lvQualificationcodes.Items.Count <> 0 Then
                    lvQualificationcodes.Items(intSelectedIndex).Selected = True
                    lvQualificationcodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvQualificationcodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboQualificationGrp.SelectedIndex = 0
            cboQualification.SelectedIndex = 0
            cboResultGrp.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQualification.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboQualification.DataSource, DataTable)
            With cboQualification
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQualification_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchQualificationGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQualificationGrp.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboQualificationGrp.DataSource, DataTable)
            With cboQualificationGrp
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQualificationGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objBtnSearchResultGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchResultGrp.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboResultGrp.DataSource, DataTable)
            With cboResultGrp
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchResultGrp_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (22-MAY-2012) -- End


#End Region

#Region " Private Methods "


    'Pinkal (22-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objQualificatioGrp As New clsCommon_Master
            dsList = objQualificatioGrp.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QualificationGrp", -1, False)
            cboQualificationGrp.DisplayMember = "name"
            cboQualificationGrp.ValueMember = "masterunkid"
            cboQualificationGrp.DataSource = dsList.Tables("QualificationGrp")


            dsList = Nothing
            dsList = objQualificatioGrp.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGrp", -1, False)
            cboResultGrp.DisplayMember = "name"
            cboResultGrp.ValueMember = "masterunkid"
            cboResultGrp.DataSource = dsList.Tables("ResultGrp")

            dsList = Nothing
            dsList = objQualificationMaster.GetComboList("Qualification", True, -1)
            cboQualification.DisplayMember = "Name"
            cboQualification.ValueMember = "Id"
            cboQualification.DataSource = dsList.Tables("Qualification")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsQualificationList As New DataSet
        Dim strSearching As String = ""
        Try

            If User._Object.Privilege._AllowToViewQualificationMasterList = True Then   'Pinkal (09-Jul-2012) -- Start

                dsQualificationList = objQualificationMaster.GetList("List")

                If CInt(cboQualificationGrp.SelectedValue) > 0 Then
                    strSearching = "AND qualificationgroupunkid =" & CInt(cboQualificationGrp.SelectedValue) & " "
                End If

                If CInt(cboResultGrp.SelectedValue) > 0 Then
                    strSearching &= "AND resultgroupunkid =" & CInt(cboResultGrp.SelectedValue) & " "
                End If

                If CInt(cboQualification.SelectedValue) > 0 Then
                    strSearching &= "AND qualificationunkid =" & CInt(cboQualification.SelectedValue) & " "
                End If

                Dim dtQualification As New DataTable
                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    'SHANI (22 APR 2015)-START
                    'Issue : Order By Qualification Group Name
                    'dtQualification = New DataView(dsQualificationList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                    dtQualification = New DataView(dsQualificationList.Tables("List"), strSearching, "name ASC", DataViewRowState.CurrentRows).ToTable
                    'SHANI (22 APR 2015)--END 
                Else
                    'SHANI (22 APR 2015)-START
                    'Issue : Order By Qualification Group Name
                    'dtQualification = dsQualificationList.Tables("List")
                    dtQualification = New DataView(dsQualificationList.Tables("List"), "", "name ASC", DataViewRowState.CurrentRows).ToTable
                    'SHANI (22 APR 2015)--END 
                End If

                Dim lvItem As ListViewItem

                lvQualificationcodes.Items.Clear()
                For Each drRow As DataRow In dtQualification.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("name").ToString
                    lvItem.Tag = drRow("qualificationunkid")

                    'Pinkal (12-Oct-2011) -- Start
                    'Enhancement : TRA Changes
                    lvItem.SubItems.Add(drRow("resultgroup").ToString)


                    lvItem.SubItems.Add(drRow("qualificationcode").ToString)
                    lvItem.SubItems.Add(drRow("qualificationname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvQualificationcodes.Items.Add(lvItem)
                Next

                lvQualificationcodes.GroupingColumn = colhQualificationGroup
                lvQualificationcodes.DisplayGroups(True)

                If lvQualificationcodes.Items.Count > 16 Then
                    colhDescription.Width = 320 - 18
                Else
                    colhDescription.Width = 320
                End If

            End If

            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsQualificationList.Dispose()
        End Try
    End Sub

    'Pinkal (22-MAY-2012) -- End


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddQualification_Course
            btnEdit.Enabled = User._Object.Privilege._EditQualification_Course
            btnDelete.Enabled = User._Object.Privilege._DeleteQualification_Course

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.colhQualificationGroup.Text = Language._Object.getCaption(CStr(Me.colhQualificationGroup.Tag), Me.colhQualificationGroup.Text)
            Me.colhQualificationCode.Text = Language._Object.getCaption(CStr(Me.colhQualificationCode.Tag), Me.colhQualificationCode.Text)
            Me.colhQualificationName.Text = Language._Object.getCaption(CStr(Me.colhQualificationName.Tag), Me.colhQualificationName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhResultGroup.Text = Language._Object.getCaption(CStr(Me.colhResultGroup.Tag), Me.colhResultGroup.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
            Me.lblQualificationGrp.Text = Language._Object.getCaption(Me.lblQualificationGrp.Name, Me.lblQualificationGrp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Qualification from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Qualification. Reason: This Qualification is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Qualification?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class