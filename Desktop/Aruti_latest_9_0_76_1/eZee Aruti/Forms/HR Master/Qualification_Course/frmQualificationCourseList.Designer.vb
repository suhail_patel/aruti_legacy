﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQualificationCourseList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQualificationCourseList))
        Me.pnlSkillList = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objBtnSearchResultGrp = New eZee.Common.eZeeGradientButton
        Me.cboResultGrp = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.objbtnSearchQualificationGrp = New eZee.Common.eZeeGradientButton
        Me.cboQualificationGrp = New System.Windows.Forms.ComboBox
        Me.lblQualificationGrp = New System.Windows.Forms.Label
        Me.objbtnSearchQualification = New eZee.Common.eZeeGradientButton
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.lblQualification = New System.Windows.Forms.Label
        Me.lvQualificationcodes = New eZee.Common.eZeeListView(Me.components)
        Me.colhQualificationGroup = New System.Windows.Forms.ColumnHeader
        Me.colhResultGroup = New System.Windows.Forms.ColumnHeader
        Me.colhQualificationCode = New System.Windows.Forms.ColumnHeader
        Me.colhQualificationName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.pnlSkillList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlSkillList
        '
        Me.pnlSkillList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlSkillList.Controls.Add(Me.lvQualificationcodes)
        Me.pnlSkillList.Controls.Add(Me.objFooter)
        Me.pnlSkillList.Controls.Add(Me.EZeeHeader1)
        Me.pnlSkillList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSkillList.Location = New System.Drawing.Point(0, 0)
        Me.pnlSkillList.Name = "pnlSkillList"
        Me.pnlSkillList.Size = New System.Drawing.Size(751, 456)
        Me.pnlSkillList.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objBtnSearchResultGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboResultGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchQualificationGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboQualificationGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualificationGrp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchQualification)
        Me.gbFilterCriteria.Controls.Add(Me.cboQualification)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualification)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(725, 86)
        Me.gbFilterCriteria.TabIndex = 13
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(698, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(675, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'objBtnSearchResultGrp
        '
        Me.objBtnSearchResultGrp.BackColor = System.Drawing.Color.Transparent
        Me.objBtnSearchResultGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objBtnSearchResultGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objBtnSearchResultGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objBtnSearchResultGrp.BorderSelected = False
        Me.objBtnSearchResultGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objBtnSearchResultGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objBtnSearchResultGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objBtnSearchResultGrp.Location = New System.Drawing.Point(320, 57)
        Me.objBtnSearchResultGrp.Name = "objBtnSearchResultGrp"
        Me.objBtnSearchResultGrp.Size = New System.Drawing.Size(22, 21)
        Me.objBtnSearchResultGrp.TabIndex = 240
        '
        'cboResultGrp
        '
        Me.cboResultGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGrp.DropDownWidth = 350
        Me.cboResultGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGrp.FormattingEnabled = True
        Me.cboResultGrp.Location = New System.Drawing.Point(122, 57)
        Me.cboResultGrp.Name = "cboResultGrp"
        Me.cboResultGrp.Size = New System.Drawing.Size(194, 21)
        Me.cboResultGrp.TabIndex = 3
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(7, 60)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(109, 15)
        Me.lblResultGroup.TabIndex = 239
        Me.lblResultGroup.Text = "Result Group"
        '
        'objbtnSearchQualificationGrp
        '
        Me.objbtnSearchQualificationGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualificationGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualificationGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualificationGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualificationGrp.BorderSelected = False
        Me.objbtnSearchQualificationGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualificationGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualificationGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualificationGrp.Location = New System.Drawing.Point(320, 30)
        Me.objbtnSearchQualificationGrp.Name = "objbtnSearchQualificationGrp"
        Me.objbtnSearchQualificationGrp.Size = New System.Drawing.Size(22, 21)
        Me.objbtnSearchQualificationGrp.TabIndex = 237
        '
        'cboQualificationGrp
        '
        Me.cboQualificationGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGrp.DropDownWidth = 350
        Me.cboQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGrp.FormattingEnabled = True
        Me.cboQualificationGrp.Location = New System.Drawing.Point(122, 30)
        Me.cboQualificationGrp.Name = "cboQualificationGrp"
        Me.cboQualificationGrp.Size = New System.Drawing.Size(194, 21)
        Me.cboQualificationGrp.TabIndex = 1
        '
        'lblQualificationGrp
        '
        Me.lblQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGrp.Location = New System.Drawing.Point(7, 33)
        Me.lblQualificationGrp.Name = "lblQualificationGrp"
        Me.lblQualificationGrp.Size = New System.Drawing.Size(109, 15)
        Me.lblQualificationGrp.TabIndex = 236
        Me.lblQualificationGrp.Text = "Qualification Group"
        '
        'objbtnSearchQualification
        '
        Me.objbtnSearchQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualification.BorderSelected = False
        Me.objbtnSearchQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualification.Location = New System.Drawing.Point(696, 30)
        Me.objbtnSearchQualification.Name = "objbtnSearchQualification"
        Me.objbtnSearchQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQualification.TabIndex = 86
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.DropDownWidth = 500
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(443, 30)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(247, 21)
        Me.cboQualification.TabIndex = 2
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(348, 33)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(91, 15)
        Me.lblQualification.TabIndex = 85
        Me.lblQualification.Text = "Qualification"
        '
        'lvQualificationcodes
        '
        Me.lvQualificationcodes.BackColorOnChecked = True
        Me.lvQualificationcodes.ColumnHeaders = Nothing
        Me.lvQualificationcodes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhQualificationGroup, Me.colhResultGroup, Me.colhQualificationCode, Me.colhQualificationName, Me.colhDescription})
        Me.lvQualificationcodes.CompulsoryColumns = ""
        Me.lvQualificationcodes.FullRowSelect = True
        Me.lvQualificationcodes.GridLines = True
        Me.lvQualificationcodes.GroupingColumn = Nothing
        Me.lvQualificationcodes.HideSelection = False
        Me.lvQualificationcodes.Location = New System.Drawing.Point(12, 159)
        Me.lvQualificationcodes.MinColumnWidth = 50
        Me.lvQualificationcodes.MultiSelect = False
        Me.lvQualificationcodes.Name = "lvQualificationcodes"
        Me.lvQualificationcodes.OptionalColumns = ""
        Me.lvQualificationcodes.ShowMoreItem = False
        Me.lvQualificationcodes.ShowSaveItem = False
        Me.lvQualificationcodes.ShowSelectAll = True
        Me.lvQualificationcodes.ShowSizeAllColumnsToFit = True
        Me.lvQualificationcodes.Size = New System.Drawing.Size(724, 234)
        Me.lvQualificationcodes.Sortable = True
        Me.lvQualificationcodes.TabIndex = 4
        Me.lvQualificationcodes.UseCompatibleStateImageBehavior = False
        Me.lvQualificationcodes.View = System.Windows.Forms.View.Details
        '
        'colhQualificationGroup
        '
        Me.colhQualificationGroup.Tag = "colhQualificationGroup"
        Me.colhQualificationGroup.Text = "Qualification / Course Group"
        Me.colhQualificationGroup.Width = 0
        '
        'colhResultGroup
        '
        Me.colhResultGroup.Tag = "colhResultGroup"
        Me.colhResultGroup.Text = "Result Group"
        Me.colhResultGroup.Width = 125
        '
        'colhQualificationCode
        '
        Me.colhQualificationCode.Tag = "colhQualificationCode"
        Me.colhQualificationCode.Text = "Code"
        Me.colhQualificationCode.Width = 100
        '
        'colhQualificationName
        '
        Me.colhQualificationName.Tag = "colhQualificationName"
        Me.colhQualificationName.Text = "Name"
        Me.colhQualificationName.Width = 175
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 320
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 401)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(751, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(647, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(551, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(455, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(359, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(751, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = "Qualification / Course List"
        '
        'frmQualificationCourseList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 456)
        Me.Controls.Add(Me.pnlSkillList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQualificationCourseList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Qualification / Course List"
        Me.pnlSkillList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlSkillList As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents lvQualificationcodes As eZee.Common.eZeeListView
    Friend WithEvents colhQualificationGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualificationCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualificationName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResultGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchQualificationGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboQualificationGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualificationGrp As System.Windows.Forms.Label
    Friend WithEvents objBtnSearchResultGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResultGrp As System.Windows.Forms.ComboBox
End Class
