﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmMembership_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmMembership_AddEdit"
    Private mblnCancel As Boolean = True
    Private objMembershipMaster As clsmembership_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMemberMasterUnkid As Integer = -1
    'Nilay (13-Oct-2016) -- Start
    'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
    Private mintPrevEmpTranHeadUnkid As Integer = 0
    Private mintPrevCoTranHeadUnkid As Integer = 0
    'Nilay (13-Oct-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMemberMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMemberMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmMembership_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMembershipMaster = New clsmembership_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objMembershipMaster._Membershipunkid = mintMemberMasterUnkid
                'Sandeep [ 25 APRIL 2011 ] -- Start
                Dim StrMessage As String = objMembershipMaster._Membershipname & " " & Language.getMessage(mstrModuleName, 4, "Membership binded with") & " "
                StrMessage &= objMembershipMaster.GetHeadName(mintMemberMasterUnkid)
                txtMappedInfo.Text = StrMessage
                'Sandeep [ 25 APRIL 2011 ] -- End 

                'Nilay (13-Oct-2016) -- Start
                'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
                cboCategory.Enabled = False
                objbtnAddCategory.Enabled = False
                mintPrevEmpTranHeadUnkid = objMembershipMaster._ETransHead_Id
                mintPrevCoTranHeadUnkid = objMembershipMaster._CTransHead_Id
                'Nilay (13-Oct-2016) -- End
            End If
            FillCombo()
            GetValue()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembership_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMembership_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembership_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMembership_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembership_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMembership_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMembershipMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmembership_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmembership_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboCategory.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership Category is compulsory information.Please Select Member Category."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership Code cannot be blank. Membership Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Membership Name cannot be blank. Membership Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            'Sandeep [ 25 APRIL 2011 ] -- Start

            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'If CInt(cboETranHead.SelectedValue) > 0 Then
            '    Dim StrMessage As String = String.Empty
            '    StrMessage = objMembershipMaster.GetMembershipName(CInt(cboETranHead.SelectedValue), -1, mintMemberMasterUnkid)
            '    If StrMessage.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Selected Employee head is already mapped with ") & " " & StrMessage & " " & Language.getMessage(mstrModuleName, 6, "membership."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            If mintPrevEmpTranHeadUnkid <> CInt(cboETranHead.SelectedValue) Then
            If CInt(cboETranHead.SelectedValue) > 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = objMembershipMaster.GetMembershipName(CInt(cboETranHead.SelectedValue), -1, mintMemberMasterUnkid)
                If StrMessage.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Selected Employee head is already mapped with ") & " " & StrMessage & " " & Language.getMessage(mstrModuleName, 6, "membership."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            End If
            'Nilay (13-Oct-2016) -- End
            'Sandeep [ 25 APRIL 2011 ] -- End 

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'If CInt(cboCTranHead.SelectedValue) > 0 Then
            '    Dim StrMessage As String = String.Empty
            '    StrMessage = objMembershipMaster.GetMembershipName(-1, CInt(cboCTranHead.SelectedValue), mintMemberMasterUnkid)
            '    If StrMessage.Trim.Length > 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Employer head is already mapped with ") & " " & StrMessage & " " & Language.getMessage(mstrModuleName, 6, "membership."), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If
            If mintPrevCoTranHeadUnkid <> CInt(cboCTranHead.SelectedValue) Then
            If CInt(cboCTranHead.SelectedValue) > 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = objMembershipMaster.GetMembershipName(-1, CInt(cboCTranHead.SelectedValue), mintMemberMasterUnkid)
                If StrMessage.Trim.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Employer head is already mapped with ") & " " & StrMessage & " " & Language.getMessage(mstrModuleName, 6, "membership."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            End If
            'Nilay (13-Oct-2016) -- End

            If menAction = enAction.EDIT_ONE Then
                If (objMembershipMaster._ETransHead_Id <> CInt(cboETranHead.SelectedValue) Or objMembershipMaster._CTransHead_Id <> CInt(cboCTranHead.SelectedValue)) Then
                    If objMembershipMaster.Is_Membership_Used(mintMemberMasterUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot map head(s) to this membership. ") & vbCrLf & _
                                                            Language.getMessage(mstrModuleName, 9, "Reason : This membership is already assigned to some employee(s). Please remove them first."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'S.SANDEEP [ 17 OCT 2012 ] -- END


            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMembershipMaster.Update()
            Else
                blnFlag = objMembershipMaster.Insert()
            End If

            If blnFlag = False And objMembershipMaster._Message <> "" Then
                eZeeMsgBox.Show(objMembershipMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMembershipMaster = Nothing
                    objMembershipMaster = New clsmembership_master
                    Call GetValue()
                    cboCategory.Select()
                Else
                    mintMemberMasterUnkid = objMembershipMaster._Membershipunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrmLangPopup.displayDialog(txtName.Text, objMembershipMaster._Membershipname1, objMembershipMaster._Membershipname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Try
            Dim objCommonmaster As New frmCommonMaster
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objCommonmaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommonmaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommonmaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            objCommonmaster.displayDialog(-1, clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, enAction.ADD_ONE)
            FillCombo()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'Sandeep [ 25 APRIL 2011 ] -- Start
            cboETranHead.BackColor = GUI.ColorOptional
            cboCTranHead.BackColor = GUI.ColorOptional
            'Sandeep [ 25 APRIL 2011 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objMembershipMaster._Membershipcode
            txtName.Text = objMembershipMaster._Membershipname
            cboCategory.SelectedValue = objMembershipMaster._Membershipcategoryunkid
            txtDescription.Text = objMembershipMaster._Description
            'Sandeep [ 25 APRIL 2011 ] -- Start
            cboETranHead.SelectedValue = objMembershipMaster._ETransHead_Id
            cboCTranHead.SelectedValue = objMembershipMaster._CTransHead_Id
            'Sandeep [ 25 APRIL 2011 ] -- End 
            chkAppearOnPayslip.Checked = objMembershipMaster._Isappearonpayslip 'Sohail (02 Oct 2014)
            'Nilay (05-Feb-2016) -- Start
            'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
            txtEmployerMemNo.Text = objMembershipMaster._EmployerMemNo
            'Nilay (05-Feb-2016) -- End

            'Nilay (03-Oct-2016) -- Start
            'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
            chkShowBasicSalaryOnStatutoryReport.Checked = objMembershipMaster._IsShowBasicSalary
            'Nilay (03-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMembershipMaster._Membershipcode = txtCode.Text.Trim
            objMembershipMaster._Membershipname = txtName.Text.Trim
            objMembershipMaster._Membershipcategoryunkid = CInt(cboCategory.SelectedValue)
            objMembershipMaster._Description = txtDescription.Text.Trim
            'Sandeep [ 25 APRIL 2011 ] -- Start
            objMembershipMaster._ETransHead_Id = CInt(cboETranHead.SelectedValue)
            objMembershipMaster._CTransHead_Id = CInt(cboCTranHead.SelectedValue)
            'Sandeep [ 25 APRIL 2011 ] -- End 
            objMembershipMaster._Isappearonpayslip = chkAppearOnPayslip.Checked  'Sohail (02 Oct 2014)
            'Nilay (05-Feb-2016) -- Start
            'Enhancement - Set Employer Membership Number Textbox option on Membership master screen and show membership wise employer membership number on EPF Period wise report in 57.2 and 58.1 SP as well - (From: moses.shembilu, Sent: Thursday, February 04, 2016 3:00 PM, To: 'Anjan' , Cc: 'Rutta anatory' ; glory.ponsian@npktechnologies.com ; 'Sohail Aruti' , Subject: GEPF Number not reflected - Picks NSSF Number only )
            objMembershipMaster._EmployerMemNo = txtEmployerMemNo.Text.Trim
            'Nilay (05-Feb-2016) -- End

            'Nilay (03-Oct-2016) -- Start
            'Enhancement : Membership wise option 'Show Basic Salary on Statutory Report and ignore that option on configuration
            objMembershipMaster._IsShowBasicSalary = chkShowBasicSalaryOnStatutoryReport.Checked
            'Nilay (03-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsCategory As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "Category")
            cboCategory.ValueMember = "masterunkid"
            cboCategory.DisplayMember = "name"
            cboCategory.DataSource = dsCategory.Tables(0)

            'Sandeep [ 25 APRIL 2011 ] -- Start
            Dim dsCombo As New DataSet
            Dim objTranHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("List", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            'Sohail (21 Aug 2015) -- End
            With cboETranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboCTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With
            'Sandeep [ 25 APRIL 2011 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddCategory.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMembership.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMembership.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMembership.Text = Language._Object.getCaption(Me.gbMembership.Name, Me.gbMembership.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.chkAppearOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOnPayslip.Name, Me.chkAppearOnPayslip.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership Category is compulsory information.Please Select Member Category.")
			Language.setMessage(mstrModuleName, 2, "Membership Code cannot be blank. Membership Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Membership Name cannot be blank. Membership Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Membership binded with")
			Language.setMessage(mstrModuleName, 5, "Selected Employee head is already mapped with")
			Language.setMessage(mstrModuleName, 6, "membership.")
			Language.setMessage(mstrModuleName, 7, "Selected Employer head is already mapped with")
			Language.setMessage(mstrModuleName, 8, "Sorry, You cannot map head(s) to this membership.")
			Language.setMessage(mstrModuleName, 9, "Reason : This membership is already assigned to some employee(s). Please remove them first.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class