﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportReviewerAssessment

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportReviewerAssessment"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

#End Region

#Region " From's Events "

    Private Sub frmImportReviewerAssessment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportReviewerAssessment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizImportReviewerAssessment_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportReviewerAssessment.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportReviewerAssessment.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportReviewerAssessment_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportReviewerAssessment_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportReviewerAssessment.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportReviewerAssessment.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)
                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select proper file to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                Case WizImportReviewerAssessment.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                Select Case CType(ctrl, ComboBox).Name.ToUpper
                                    Case cboAssessmentSubItemCode.Name.ToUpper, _
                                         cboImpImprovement.Name.ToUpper, _
                                         cboImpActivity.Name.ToUpper, _
                                         cboImpSupportRequired.Name.ToUpper, _
                                         cboImpOtherTraining.Name.ToUpper, _
                                         cboImpTimeFrame.Name.ToUpper, _
                                         cboImpTrainingLearningObjective.Name.ToUpper, _
                                         cboPDImprovement.Name.ToUpper, _
                                         cboPDActivity.Name.ToUpper, _
                                         cboPDSupportRequired.Name.ToUpper, _
                                         cboPDOtherTraining.Name.ToUpper, _
                                         cboPDTimeFrame.Name.ToUpper, _
                                         cboPDTrainingLearningObjective.Name.ToUpper, _
                                         cboRemark1.Name.ToUpper, _
                                         cboRemark2.Name.ToUpper
                                        Continue For
                                    Case Else
                                        If CType(ctrl, ComboBox).Text = "" Then
                                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                            e.Cancel = True
                                            CType(ctrl, ComboBox).Focus()
                                            Exit For
                                        End If
                                End Select
                            End If
                        Next
                    End If
                Case WizImportReviewerAssessment.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportReviewerAssessment_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("rcode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("period", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("gcode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("icode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("scode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("result", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_improvement", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_activity", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_support", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_othertraining", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_timeframe", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("imp_learningobjective", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_improvement", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_activity", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_support", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_othertraining", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_timeframe", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("pd_learningobjective", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("remark1", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("remark2", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")

            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow = Nothing

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("rcode") = dtRow.Item(cboReviewerCode.Text).ToString
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString
                drNewRow.Item("period") = dtRow.Item(cboPeriod.Text).ToString
                drNewRow.Item("gcode") = dtRow.Item(cboAssessmentGrpCode.Text).ToString
                drNewRow.Item("icode") = dtRow.Item(cboAssessmentItemCode.Text).ToString
                drNewRow.Item("result") = dtRow.Item(cboResult.Text).ToString

                If cboAssessmentSubItemCode.Text.Trim.Length > 0 Then
                    drNewRow.Item("scode") = dtRow.Item(cboAssessmentSubItemCode.Text).ToString
                Else
                    drNewRow.Item("scode") = ""
                End If


                If cboImpImprovement.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_improvement") = dtRow.Item(cboImpImprovement.Text).ToString
                Else
                    drNewRow.Item("imp_improvement") = ""
                End If

                If cboImpActivity.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_activity") = dtRow.Item(cboImpActivity.Text).ToString
                Else
                    drNewRow.Item("imp_activity") = ""
                End If

                If cboImpSupportRequired.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_support") = dtRow.Item(cboImpSupportRequired.Text).ToString
                Else
                    drNewRow.Item("imp_support") = ""
                End If

                If cboImpOtherTraining.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_othertraining") = dtRow.Item(cboImpOtherTraining.Text).ToString
                Else
                    drNewRow.Item("imp_othertraining") = ""
                End If

                If cboImpTimeFrame.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_timeframe") = dtRow.Item(cboImpTimeFrame.Text).ToString
                Else
                    drNewRow.Item("imp_timeframe") = ""
                End If

                If cboImpTrainingLearningObjective.Text.Trim.Length > 0 Then
                    drNewRow.Item("imp_learningobjective") = dtRow.Item(cboImpTrainingLearningObjective.Text).ToString
                Else
                    drNewRow.Item("imp_learningobjective") = ""
                End If


                If cboPDImprovement.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_improvement") = dtRow.Item(cboPDImprovement.Text).ToString
                Else
                    drNewRow.Item("pd_improvement") = ""
                End If

                If cboPDActivity.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_activity") = dtRow.Item(cboPDActivity.Text).ToString
                Else
                    drNewRow.Item("pd_activity") = ""
                End If

                If cboPDSupportRequired.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_support") = dtRow.Item(cboPDSupportRequired.Text).ToString
                Else
                    drNewRow.Item("pd_support") = ""
                End If

                If cboPDOtherTraining.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_othertraining") = dtRow.Item(cboPDOtherTraining.Text).ToString
                Else
                    drNewRow.Item("pd_othertraining") = ""
                End If

                If cboPDTimeFrame.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_timeframe") = dtRow.Item(cboPDTimeFrame.Text).ToString
                Else
                    drNewRow.Item("pd_timeframe") = ""
                End If

                If cboPDTrainingLearningObjective.Text.Trim.Length > 0 Then
                    drNewRow.Item("pd_learningobjective") = dtRow.Item(cboPDTrainingLearningObjective.Text).ToString
                Else
                    drNewRow.Item("pd_learningobjective") = ""
                End If

                If cboRemark1.Text.Trim.Length > 0 Then
                    drNewRow.Item("remark1") = dtRow.Item(cboRemark1.Text).ToString.Trim
                Else
                    drNewRow.Item("remark1") = ""
                End If

                If cboRemark2.Text.Trim.Length > 0 Then
                    drNewRow.Item("remark2") = dtRow.Item(cboRemark2.Text).ToString.Trim
                Else
                    drNewRow.Item("remark2") = ""
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)

            Next

            If blnIsNotThrown = True Then
                colhReviewerCode.DataPropertyName = "rcode"
                colhEmployee.DataPropertyName = "ecode"
                colhGroupCode.DataPropertyName = "gcode"
                colhItemCode.DataPropertyName = "icode"
                colhSubItemCode.DataPropertyName = "scode"
                colhResult.DataPropertyName = "result"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportReviewerAssessment.BackEnabled = False
            WizImportReviewerAssessment.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then Exit Sub
            Dim iEmpId, iYearId, iPeriodId, iGroupId, _
                iItemId, iSubItemId, iResultId, iImpLearningObjective, iAnalysisId, iREmpId, iRMasterId, iPDLearningObjective As Integer
            Dim mDecWeight As Decimal = 0

            Dim objEmp As New clsEmployee_Master
            Dim objPrd As New clscommom_period_Tran
            Dim objGrp As New clsassess_group_master
            Dim objItm As New clsassess_item_master
            Dim objStm As New clsassess_subitem_master
            Dim objRst As New clsresult_master
            Dim objCom As New clsCommon_Master
            Dim objAsr As New clsAssessor

            Dim objAnalysis As clsassess_analysis_master
            Dim objAnalysis_Tran As clsassess_analysis_tran
            Dim objRemarks_Tran As clsassess_remarks_tran

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try
                iEmpId = 0 : iYearId = 0 : iPeriodId = 0 : iGroupId = 0
                iItemId = 0 : iSubItemId = 0 : iResultId = 0 : iImpLearningObjective = 0
                mDecWeight = 0 : iAnalysisId = 0 : iREmpId = 0 : iRMasterId = 0 : iPDLearningObjective = 0

                '>>>>>>>>>>>>>>>>>>>>> IF REVIEWER PERESET IN THE SYSTEM
                If dtRow.Item("rcode").ToString.Trim.Length > 0 Then
                    iREmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("rcode").ToString.Trim)
                    If iREmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Assessor Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF EMPLOYEE PERESET IN THE SYSTEM
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF PERIOD IN THE SYSTEM
                If dtRow.Item("period").ToString.Trim.Length > 0 Then
                    iPeriodId = objPrd.GetPeriodByName(dtRow.Item("period").ToString.Trim, enModuleReference.Assessment)
                    If iPeriodId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Period Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPrd._Periodunkid = iPeriodId
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriodId
                        'Sohail (21 Aug 2015) -- End
                        iYearId = objPrd._Yearunkid
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF ASSESSMENT GROUP IN THE SYSTEM
                If dtRow.Item("gcode").ToString.Trim.Length > 0 Then
                    iGroupId = objGrp.Get_Group_Id(dtRow.Item("gcode").ToString.Trim, "")
                    If iGroupId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Assessment Group Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF ASSESSMENT ITEMS IN THE SYSTEM
                If dtRow.Item("icode").ToString.Trim.Length > 0 Then
                    iItemId = objItm.Get_Item_Id(dtRow.Item("icode").ToString.Trim, "", iGroupId, mDecWeight)
                    If iItemId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 15, "Assessment Item Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF ASSESSMENT SUB ITEMS IN THE SYSTEM
                If dtRow.Item("scode").ToString.Trim.Length > 0 Then
                    iSubItemId = objStm.Get_SubItem_Id(dtRow.Item("scode").ToString.Trim, "", iItemId)
                Else
                    iSubItemId = 0
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF RESULT IN THE SYSTEM
                If dtRow.Item("result").ToString.Trim.Length > 0 Then
                    If IsNumeric(dtRow.Item("result").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 16, "Result should be number.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("result").ToString.Trim.Length > 0 Then
                    iResultId = objRst.GetResultUnkid(dtRow.Item("result").ToString.Trim)
                    If iResultId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 17, "Result Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF TRAINING COURSE IN THE SYSTEM
                If dtRow.Item("imp_learningobjective").ToString.Trim.Length > 0 Then
                    iImpLearningObjective = objCom.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, dtRow.Item("imp_learningobjective").ToString.Trim)
                Else
                    iImpLearningObjective = 0
                End If

                If dtRow.Item("pd_learningobjective").ToString.Trim.Length > 0 Then
                    iPDLearningObjective = objCom.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, dtRow.Item("pd_learningobjective").ToString.Trim)
                Else
                    iPDLearningObjective = 0
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF RESULT IS GREATER THAN WEIGHT
                If CDec(dtRow.Item("result").ToString.Trim) > mDecWeight Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 18, "Result Cannot Exceed Item Weight [ ") & mDecWeight.ToString & Language.getMessage(mstrModuleName, 19, " ].")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF TIMEFRAME IS VALID DATE
                If dtRow.Item("imp_timeframe").ToString.Trim.Length > 0 Then
                    If IsDate(dtRow.Item("imp_timeframe").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Invalid Time frame Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If dtRow.Item("pd_timeframe").ToString.Trim.Length > 0 Then
                    If IsDate(dtRow.Item("pd_timeframe").ToString.Trim) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 20, "Invalid Time frame Date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '>>>>>>>>>>>>>>>>>>>>> IF EMPLOYEE MAPPED WITH REVIEWER

                'Shani(01-MAR-2016) -- Start
                'Enhancement:PA External Approver Flow
                'iRMasterId = objAsr.GetAssessorMasterId(iREmpId, True)
                iRMasterId = objAsr.GetAssessorMasterId(iREmpId, True, False)
                'Shani(01-MAR-2016)-- End

                If iRMasterId <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Reviewer Not Defined For assessment.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                Else
                    'S.SANDEEP [27 DEC 2016] -- START
                    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                    'If objAsr.IsEmpMapped(iRMasterId, iEmpId) = False Then
                    If objAsr.IsEmpMapped(iRMasterId, iEmpId, clsAssessor.enARVisibilityTypeId.VISIBLE) = False Then
                        'S.SANDEEP [27 DEC 2016] -- END

                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 22, "Employee not assigned to this assessor.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                objAnalysis = New clsassess_analysis_master
                objAnalysis_Tran = New clsassess_analysis_tran
                objRemarks_Tran = New clsassess_remarks_tran

                Dim mdtAssessTran, mdtRemarkTran As DataTable
                Dim dRow As DataRow = Nothing

                iAnalysisId = objAnalysis.Get_Analysis_Id(enAssessmentMode.REVIEWER_ASSESSMENT, iEmpId, iPeriodId, iGroupId, , iREmpId)
                If iAnalysisId <= 0 Then
                    objAnalysis._Assessedemployeeunkid = iEmpId
                    objAnalysis._Assessgroupunkid = iGroupId
                    objAnalysis._Assessmentdate = ConfigParameter._Object._CurrentDateAndTime
                    objAnalysis._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                    objAnalysis._Assessoremployeeunkid = 0
                    objAnalysis._Assessormasterunkid = iRMasterId
                    objAnalysis._Committeddatetime = Nothing
                    objAnalysis._Ext_Assessorunkid = 0
                    objAnalysis._Iscommitted = False
                    objAnalysis._Isvoid = False
                    objAnalysis._Periodunkid = iPeriodId
                    objAnalysis._Reviewer_Remark1 = dtRow.Item("remark1").ToString.Trim
                    objAnalysis._Reviewer_Remark2 = dtRow.Item("remark2").ToString.Trim
                    objAnalysis._Reviewerunkid = iREmpId
                    objAnalysis._Selfemployeeunkid = 0
                    objAnalysis._Userunkid = User._Object._Userunkid
                    objAnalysis._Voiddatetime = Nothing
                    objAnalysis._Voidreason = ""
                    objAnalysis._Voiduserunkid = 0
                    objAnalysis._Yearunkid = iYearId
                    objAnalysis._Iscommitted = True
                    objAnalysis._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime

                    objAnalysis_Tran._AnalysisUnkid = iAnalysisId
                    mdtAssessTran = objAnalysis_Tran._DataTable

                    dRow = mdtAssessTran.NewRow
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = iAnalysisId
                    dRow.Item("assessitemunkid") = iItemId
                    dRow.Item("assess_subitemunkid") = iSubItemId
                    dRow.Item("resultunkid") = iResultId
                    dRow.Item("remark") = ""
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    mdtAssessTran.Rows.Add(dRow)

                    objRemarks_Tran._AnalysisUnkid = iAnalysisId
                    mdtRemarkTran = objRemarks_Tran._DataTable

                    If dtRow.Item("imp_improvement").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_activity").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_support").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_othertraining").ToString.Trim.Length > 0 Or _
                       iImpLearningObjective > 0 Then

                        dRow = mdtRemarkTran.NewRow
                        dRow.Item("remarkstranunkid") = -1
                        dRow.Item("analysisunkid") = iAnalysisId
                        dRow.Item("coursemasterunkid") = iImpLearningObjective
                        dRow.Item("timeframe_date") = CDate(dtRow.Item("imp_timeframe").ToString.Trim).Date
                        dRow.Item("major_area") = dtRow.Item("imp_improvement").ToString.Trim
                        dRow.Item("activity") = dtRow.Item("imp_activity").ToString.Trim
                        dRow.Item("support_required") = dtRow.Item("imp_support").ToString.Trim
                        dRow.Item("other_training") = dtRow.Item("imp_othertraining").ToString.Trim
                        dRow.Item("isimporvement") = True
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("AUD") = "A"
                        mdtRemarkTran.Rows.Add(dRow)

                    End If

                    If dtRow.Item("pd_improvement").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_activity").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_support").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_othertraining").ToString.Trim.Length > 0 Or _
                       iPDLearningObjective > 0 Then

                        dRow = mdtRemarkTran.NewRow
                        dRow.Item("remarkstranunkid") = -1
                        dRow.Item("analysisunkid") = iAnalysisId
                        dRow.Item("coursemasterunkid") = iPDLearningObjective
                        dRow.Item("timeframe_date") = CDate(dtRow.Item("pd_timeframe").ToString.Trim).Date
                        dRow.Item("major_area") = dtRow.Item("pd_improvement").ToString.Trim
                        dRow.Item("activity") = dtRow.Item("pd_activity").ToString.Trim
                        dRow.Item("support_required") = dtRow.Item("pd_support").ToString.Trim
                        dRow.Item("other_training") = dtRow.Item("pd_othertraining").ToString.Trim
                        dRow.Item("isimporvement") = False
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("AUD") = "A"
                        mdtRemarkTran.Rows.Add(dRow)
                    End If
                    

                    If objAnalysis.Insert(mdtAssessTran, mdtRemarkTran) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objAnalysis._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    Else
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 23, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    End If
                Else
                    objAnalysis._Analysisunkid = iAnalysisId

                    objAnalysis_Tran._AnalysisUnkid = iAnalysisId
                    mdtAssessTran = objAnalysis_Tran._DataTable

                    Dim dtmp() As DataRow = Nothing
                    If mdtAssessTran.Rows.Count > 0 Then
                        If iSubItemId > 0 Then
                            dtmp = mdtAssessTran.Select("assess_subitemunkid = '" & iSubItemId & "'")
                        End If
                        If iItemId > 0 Then
                            dtmp = mdtAssessTran.Select("assessitemunkid = '" & iItemId & "'")
                        End If
                    End If

                    If dtmp.Length > 0 Then
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Assessment Item Already Exists.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Warning")
                        dtRow.Item("objStatus") = 0
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        Continue For
                    End If

                    dRow = mdtAssessTran.NewRow
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = iAnalysisId
                    dRow.Item("assessitemunkid") = iItemId
                    dRow.Item("assess_subitemunkid") = iSubItemId
                    dRow.Item("resultunkid") = iResultId
                    dRow.Item("remark") = ""
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    mdtAssessTran.Rows.Add(dRow)

                    objRemarks_Tran._AnalysisUnkid = iAnalysisId
                    mdtRemarkTran = objRemarks_Tran._DataTable

                    If dtRow.Item("imp_improvement").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_activity").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_support").ToString.Trim.Length > 0 Or _
                       dtRow.Item("imp_othertraining").ToString.Trim.Length > 0 Or _
                       iImpLearningObjective > 0 Then

                        If mdtRemarkTran.Rows.Count > 0 Then
                            If iImpLearningObjective > 0 Then
                                dtmp = mdtRemarkTran.Select("coursemasterunkid = '" & iImpLearningObjective & "'")
                            End If
                        End If

                        If dtmp.Length > 0 Then
                            dtRow.Item("image") = imgWarring
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Training or Learning Objective Already Exists.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Warning")
                            dtRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            Continue For
                        End If

                        dRow = mdtRemarkTran.NewRow
                        dRow.Item("remarkstranunkid") = -1
                        dRow.Item("analysisunkid") = iAnalysisId
                        dRow.Item("coursemasterunkid") = iImpLearningObjective
                        dRow.Item("timeframe_date") = CDate(dtRow.Item("imp_timeframe").ToString.Trim).Date
                        dRow.Item("major_area") = dtRow.Item("imp_improvement").ToString.Trim
                        dRow.Item("activity") = dtRow.Item("imp_activity").ToString.Trim
                        dRow.Item("support_required") = dtRow.Item("imp_support").ToString.Trim
                        dRow.Item("other_training") = dtRow.Item("imp_othertraining").ToString.Trim
                        dRow.Item("isimporvement") = True
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("AUD") = "A"
                        mdtRemarkTran.Rows.Add(dRow)

                    End If

                    If dtRow.Item("pd_improvement").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_activity").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_support").ToString.Trim.Length > 0 Or _
                       dtRow.Item("pd_othertraining").ToString.Trim.Length > 0 Or _
                       iPDLearningObjective > 0 Then

                        If mdtRemarkTran.Rows.Count > 0 Then
                            If iImpLearningObjective > 0 Then
                                dtmp = mdtRemarkTran.Select("coursemasterunkid = '" & iPDLearningObjective & "'")
                            End If
                        End If

                        If dtmp.Length > 0 Then
                            dtRow.Item("image") = imgWarring
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Training or Learning Objective Already Exists.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Warning")
                            dtRow.Item("objStatus") = 0
                            objWarning.Text = CStr(Val(objWarning.Text) + 1)
                            Continue For
                        End If

                        dRow = mdtRemarkTran.NewRow
                        dRow.Item("remarkstranunkid") = -1
                        dRow.Item("analysisunkid") = iAnalysisId
                        dRow.Item("coursemasterunkid") = iPDLearningObjective
                        dRow.Item("timeframe_date") = CDate(dtRow.Item("pd_timeframe").ToString.Trim).Date
                        dRow.Item("major_area") = dtRow.Item("pd_improvement").ToString.Trim
                        dRow.Item("activity") = dtRow.Item("pd_activity").ToString.Trim
                        dRow.Item("support_required") = dtRow.Item("pd_support").ToString.Trim
                        dRow.Item("other_training") = dtRow.Item("pd_othertraining").ToString.Trim
                        dRow.Item("isimporvement") = False
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("AUD") = "A"
                        mdtRemarkTran.Rows.Add(dRow)
                    End If


                    If objAnalysis.Update(mdtAssessTran, mdtRemarkTran) = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objAnalysis._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    Else
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 23, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    End If

                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboReviewerCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Reviewer Code is mandatory information. Please select Reviewer Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboAssessmentGrpCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assessment Group Code is mandatory information. Please select Assessment Group Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboAssessmentItemCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessment Item Code is mandatory information. Please select Assessment Item Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboResult.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Result is mandatory information. Please select Result to continue."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    dtTable.Columns.Remove("employeeid")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Reviewer Assessment Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.WizImportReviewerAssessment.CancelText = Language._Object.getCaption(Me.WizImportReviewerAssessment.Name & "_CancelText" , Me.WizImportReviewerAssessment.CancelText)
			Me.WizImportReviewerAssessment.NextText = Language._Object.getCaption(Me.WizImportReviewerAssessment.Name & "_NextText" , Me.WizImportReviewerAssessment.NextText)
			Me.WizImportReviewerAssessment.BackText = Language._Object.getCaption(Me.WizImportReviewerAssessment.Name & "_BackText" , Me.WizImportReviewerAssessment.BackText)
			Me.WizImportReviewerAssessment.FinishText = Language._Object.getCaption(Me.WizImportReviewerAssessment.Name & "_FinishText" , Me.WizImportReviewerAssessment.FinishText)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblAssessmentGrpCode.Text = Language._Object.getCaption(Me.lblAssessmentGrpCode.Name, Me.lblAssessmentGrpCode.Text)
			Me.lblIsDefault.Text = Language._Object.getCaption(Me.lblIsDefault.Name, Me.lblIsDefault.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.LblResult.Text = Language._Object.getCaption(Me.LblResult.Name, Me.LblResult.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.LblReviewerCode.Text = Language._Object.getCaption(Me.LblReviewerCode.Name, Me.LblReviewerCode.Text)
			Me.Label11.Text = Language._Object.getCaption(Me.Label11.Name, Me.Label11.Text)
			Me.LblImpTrainingLearningObjective.Text = Language._Object.getCaption(Me.LblImpTrainingLearningObjective.Name, Me.LblImpTrainingLearningObjective.Text)
			Me.LblImpSupportRequired.Text = Language._Object.getCaption(Me.LblImpSupportRequired.Name, Me.LblImpSupportRequired.Text)
			Me.LblImpActivity.Text = Language._Object.getCaption(Me.LblImpActivity.Name, Me.LblImpActivity.Text)
			Me.LblImpImprovement.Text = Language._Object.getCaption(Me.LblImpImprovement.Name, Me.LblImpImprovement.Text)
			Me.lblImpTimeframe.Text = Language._Object.getCaption(Me.lblImpTimeframe.Name, Me.lblImpTimeframe.Text)
			Me.lblImpOtherTraining.Text = Language._Object.getCaption(Me.lblImpOtherTraining.Name, Me.lblImpOtherTraining.Text)
			Me.LblPDTrainingLearningObj.Text = Language._Object.getCaption(Me.LblPDTrainingLearningObj.Name, Me.LblPDTrainingLearningObj.Text)
			Me.LblPDSupportRequired.Text = Language._Object.getCaption(Me.LblPDSupportRequired.Name, Me.LblPDSupportRequired.Text)
			Me.LblPDActivity.Text = Language._Object.getCaption(Me.LblPDActivity.Name, Me.LblPDActivity.Text)
			Me.LblPDImprovement.Text = Language._Object.getCaption(Me.LblPDImprovement.Name, Me.LblPDImprovement.Text)
			Me.LblPDTimeFrame.Text = Language._Object.getCaption(Me.LblPDTimeFrame.Name, Me.LblPDTimeFrame.Text)
			Me.LblPDOtherTraining.Text = Language._Object.getCaption(Me.LblPDOtherTraining.Name, Me.LblPDOtherTraining.Text)
			Me.colhReviewerCode.HeaderText = Language._Object.getCaption(Me.colhReviewerCode.Name, Me.colhReviewerCode.HeaderText)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhGroupCode.HeaderText = Language._Object.getCaption(Me.colhGroupCode.Name, Me.colhGroupCode.HeaderText)
			Me.colhItemCode.HeaderText = Language._Object.getCaption(Me.colhItemCode.Name, Me.colhItemCode.HeaderText)
			Me.colhSubItemCode.HeaderText = Language._Object.getCaption(Me.colhSubItemCode.Name, Me.colhSubItemCode.HeaderText)
			Me.colhResult.HeaderText = Language._Object.getCaption(Me.colhResult.Name, Me.colhResult.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.LblRemark2.Text = Language._Object.getCaption(Me.LblRemark2.Name, Me.LblRemark2.Text)
			Me.LblRemark1.Text = Language._Object.getCaption(Me.LblRemark1.Name, Me.LblRemark1.Text)
			Me.elPersonal.Text = Language._Object.getCaption(Me.elPersonal.Name, Me.elPersonal.Text)
			Me.elImprovement.Text = Language._Object.getCaption(Me.elImprovement.Name, Me.elImprovement.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select proper file to Import Data.")
			Language.setMessage(mstrModuleName, 2, "Please select proper field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 4, "Reviewer Code is mandatory information. Please select Reviewer Code to continue.")
			Language.setMessage(mstrModuleName, 5, "Employee Code is mandatory information. Please select Employee Code to continue.")
			Language.setMessage(mstrModuleName, 6, "Assessment Group Code is mandatory information. Please select Assessment Group Code to continue.")
			Language.setMessage(mstrModuleName, 7, "Assessment Item Code is mandatory information. Please select Assessment Item Code to continue.")
			Language.setMessage(mstrModuleName, 8, "Result is mandatory information. Please select Result to continue.")
			Language.setMessage(mstrModuleName, 10, "Assessor Not Found.")
			Language.setMessage(mstrModuleName, 11, "Fail")
			Language.setMessage(mstrModuleName, 12, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 13, "Period Not Found.")
			Language.setMessage(mstrModuleName, 14, "Assessment Group Not Found.")
			Language.setMessage(mstrModuleName, 15, "Assessment Item Not Found.")
			Language.setMessage(mstrModuleName, 16, "Result should be number.")
			Language.setMessage(mstrModuleName, 17, "Result Not Found.")
			Language.setMessage(mstrModuleName, 18, "Result Cannot Exceed Item Weight [")
			Language.setMessage(mstrModuleName, 19, " ].")
			Language.setMessage(mstrModuleName, 20, "Invalid Time frame Date.")
			Language.setMessage(mstrModuleName, 21, "Assessor Not Defined For assessment.")
			Language.setMessage(mstrModuleName, 22, "Employee not assigned to this assessor.")
			Language.setMessage(mstrModuleName, 23, "Success")
			Language.setMessage(mstrModuleName, 24, "Assessment Item Already Exists.")
			Language.setMessage(mstrModuleName, 25, "Warning")
			Language.setMessage(mstrModuleName, 26, "Training or Learning Objective Already Exists.")
			Language.setMessage(mstrModuleName, 27, "This field is already selected.Please Select New field.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class