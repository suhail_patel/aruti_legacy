﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportReviewerAssessment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportReviewerAssessment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.WizImportReviewerAssessment = New eZee.Common.eZeeWizard
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPDImprovement = New System.Windows.Forms.ComboBox
        Me.cboPDSupportRequired = New System.Windows.Forms.ComboBox
        Me.LblPDTrainingLearningObj = New System.Windows.Forms.Label
        Me.cboPDTrainingLearningObjective = New System.Windows.Forms.ComboBox
        Me.cboPDActivity = New System.Windows.Forms.ComboBox
        Me.LblPDSupportRequired = New System.Windows.Forms.Label
        Me.cboPDTimeFrame = New System.Windows.Forms.ComboBox
        Me.LblPDOtherTraining = New System.Windows.Forms.Label
        Me.cboPDOtherTraining = New System.Windows.Forms.ComboBox
        Me.LblPDActivity = New System.Windows.Forms.Label
        Me.LblPDTimeFrame = New System.Windows.Forms.Label
        Me.LblPDImprovement = New System.Windows.Forms.Label
        Me.objlblSign12 = New System.Windows.Forms.Label
        Me.lblImpOtherTraining = New System.Windows.Forms.Label
        Me.cboImpImprovement = New System.Windows.Forms.ComboBox
        Me.LblImpTrainingLearningObjective = New System.Windows.Forms.Label
        Me.lblImpTimeframe = New System.Windows.Forms.Label
        Me.cboImpOtherTraining = New System.Windows.Forms.ComboBox
        Me.LblImpImprovement = New System.Windows.Forms.Label
        Me.cboImpTimeFrame = New System.Windows.Forms.ComboBox
        Me.cboImpTrainingLearningObjective = New System.Windows.Forms.ComboBox
        Me.LblImpActivity = New System.Windows.Forms.Label
        Me.cboImpActivity = New System.Windows.Forms.ComboBox
        Me.LblImpSupportRequired = New System.Windows.Forms.Label
        Me.cboImpSupportRequired = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.LblRemark2 = New System.Windows.Forms.Label
        Me.cboRemark2 = New System.Windows.Forms.ComboBox
        Me.LblRemark1 = New System.Windows.Forms.Label
        Me.cboRemark1 = New System.Windows.Forms.ComboBox
        Me.cboReviewerCode = New System.Windows.Forms.ComboBox
        Me.LblReviewerCode = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.LblResult = New System.Windows.Forms.Label
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.lblIsDefault = New System.Windows.Forms.Label
        Me.cboAssessmentSubItemCode = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.cboAssessmentItemCode = New System.Windows.Forms.ComboBox
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblAssessmentGrpCode = New System.Windows.Forms.Label
        Me.cboAssessmentGrpCode = New System.Windows.Forms.ComboBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhReviewerCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGroupCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSubItemCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhResult = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.elImprovement = New eZee.Common.eZeeLine
        Me.elPersonal = New eZee.Common.eZeeLine
        Me.WizImportReviewerAssessment.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(816, 447)
        Me.pnlMainInfo.TabIndex = 0
        '
        'WizImportReviewerAssessment
        '
        Me.WizImportReviewerAssessment.Controls.Add(Me.wizPageMapping)
        Me.WizImportReviewerAssessment.Controls.Add(Me.wizPageFile)
        Me.WizImportReviewerAssessment.Controls.Add(Me.wizPageData)
        Me.WizImportReviewerAssessment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizImportReviewerAssessment.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizImportReviewerAssessment.Location = New System.Drawing.Point(0, 0)
        Me.WizImportReviewerAssessment.Name = "WizImportReviewerAssessment"
        Me.WizImportReviewerAssessment.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizImportReviewerAssessment.SaveEnabled = True
        Me.WizImportReviewerAssessment.SaveText = "Save && Finish"
        Me.WizImportReviewerAssessment.SaveVisible = False
        Me.WizImportReviewerAssessment.SetSaveIndexBeforeFinishIndex = False
        Me.WizImportReviewerAssessment.Size = New System.Drawing.Size(816, 447)
        Me.WizImportReviewerAssessment.TabIndex = 1
        Me.WizImportReviewerAssessment.WelcomeImage = Nothing
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(816, 399)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.cboPDImprovement)
        Me.gbFiledMapping.Controls.Add(Me.LblPDTrainingLearningObj)
        Me.gbFiledMapping.Controls.Add(Me.cboPDSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.cboPDTrainingLearningObjective)
        Me.gbFiledMapping.Controls.Add(Me.elPersonal)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.Controls.Add(Me.cboPDTimeFrame)
        Me.gbFiledMapping.Controls.Add(Me.LblPDOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.LblPDTimeFrame)
        Me.gbFiledMapping.Controls.Add(Me.LblPDSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.cboPDOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.elImprovement)
        Me.gbFiledMapping.Controls.Add(Me.cboPDActivity)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign12)
        Me.gbFiledMapping.Controls.Add(Me.lblImpOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.cboImpImprovement)
        Me.gbFiledMapping.Controls.Add(Me.lblPeriod)
        Me.gbFiledMapping.Controls.Add(Me.LblImpTrainingLearningObjective)
        Me.gbFiledMapping.Controls.Add(Me.lblImpTimeframe)
        Me.gbFiledMapping.Controls.Add(Me.cboPeriod)
        Me.gbFiledMapping.Controls.Add(Me.LblPDActivity)
        Me.gbFiledMapping.Controls.Add(Me.cboImpOtherTraining)
        Me.gbFiledMapping.Controls.Add(Me.LblRemark2)
        Me.gbFiledMapping.Controls.Add(Me.LblImpImprovement)
        Me.gbFiledMapping.Controls.Add(Me.cboRemark2)
        Me.gbFiledMapping.Controls.Add(Me.LblPDImprovement)
        Me.gbFiledMapping.Controls.Add(Me.cboImpTimeFrame)
        Me.gbFiledMapping.Controls.Add(Me.LblRemark1)
        Me.gbFiledMapping.Controls.Add(Me.cboImpTrainingLearningObjective)
        Me.gbFiledMapping.Controls.Add(Me.cboRemark1)
        Me.gbFiledMapping.Controls.Add(Me.LblImpActivity)
        Me.gbFiledMapping.Controls.Add(Me.cboReviewerCode)
        Me.gbFiledMapping.Controls.Add(Me.cboImpActivity)
        Me.gbFiledMapping.Controls.Add(Me.LblReviewerCode)
        Me.gbFiledMapping.Controls.Add(Me.LblImpSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.Label11)
        Me.gbFiledMapping.Controls.Add(Me.cboImpSupportRequired)
        Me.gbFiledMapping.Controls.Add(Me.Label1)
        Me.gbFiledMapping.Controls.Add(Me.LblResult)
        Me.gbFiledMapping.Controls.Add(Me.cboResult)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign6)
        Me.gbFiledMapping.Controls.Add(Me.lblIsDefault)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentSubItemCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.lblAssessmentItemCode)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentItemCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.lblAssessmentGrpCode)
        Me.gbFiledMapping.Controls.Add(Me.cboAssessmentGrpCode)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(651, 398)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPDImprovement
        '
        Me.cboPDImprovement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDImprovement.FormattingEnabled = True
        Me.cboPDImprovement.Location = New System.Drawing.Point(437, 213)
        Me.cboPDImprovement.Name = "cboPDImprovement"
        Me.cboPDImprovement.Size = New System.Drawing.Size(198, 21)
        Me.cboPDImprovement.TabIndex = 172
        '
        'cboPDSupportRequired
        '
        Me.cboPDSupportRequired.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDSupportRequired.FormattingEnabled = True
        Me.cboPDSupportRequired.Location = New System.Drawing.Point(437, 267)
        Me.cboPDSupportRequired.Name = "cboPDSupportRequired"
        Me.cboPDSupportRequired.Size = New System.Drawing.Size(198, 21)
        Me.cboPDSupportRequired.TabIndex = 176
        '
        'LblPDTrainingLearningObj
        '
        Me.LblPDTrainingLearningObj.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDTrainingLearningObj.Location = New System.Drawing.Point(344, 350)
        Me.LblPDTrainingLearningObj.Name = "LblPDTrainingLearningObj"
        Me.LblPDTrainingLearningObj.Size = New System.Drawing.Size(87, 29)
        Me.LblPDTrainingLearningObj.TabIndex = 185
        Me.LblPDTrainingLearningObj.Text = "Training Or Learning Obj."
        '
        'cboPDTrainingLearningObjective
        '
        Me.cboPDTrainingLearningObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDTrainingLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDTrainingLearningObjective.FormattingEnabled = True
        Me.cboPDTrainingLearningObjective.Location = New System.Drawing.Point(437, 348)
        Me.cboPDTrainingLearningObjective.Name = "cboPDTrainingLearningObjective"
        Me.cboPDTrainingLearningObjective.Size = New System.Drawing.Size(198, 21)
        Me.cboPDTrainingLearningObjective.TabIndex = 177
        '
        'cboPDActivity
        '
        Me.cboPDActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDActivity.FormattingEnabled = True
        Me.cboPDActivity.Location = New System.Drawing.Point(437, 240)
        Me.cboPDActivity.Name = "cboPDActivity"
        Me.cboPDActivity.Size = New System.Drawing.Size(198, 21)
        Me.cboPDActivity.TabIndex = 175
        '
        'LblPDSupportRequired
        '
        Me.LblPDSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDSupportRequired.Location = New System.Drawing.Point(344, 269)
        Me.LblPDSupportRequired.Name = "LblPDSupportRequired"
        Me.LblPDSupportRequired.Size = New System.Drawing.Size(87, 15)
        Me.LblPDSupportRequired.TabIndex = 184
        Me.LblPDSupportRequired.Text = "Support Required"
        Me.LblPDSupportRequired.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPDTimeFrame
        '
        Me.cboPDTimeFrame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDTimeFrame.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDTimeFrame.FormattingEnabled = True
        Me.cboPDTimeFrame.Location = New System.Drawing.Point(437, 321)
        Me.cboPDTimeFrame.Name = "cboPDTimeFrame"
        Me.cboPDTimeFrame.Size = New System.Drawing.Size(198, 21)
        Me.cboPDTimeFrame.TabIndex = 174
        '
        'LblPDOtherTraining
        '
        Me.LblPDOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDOtherTraining.Location = New System.Drawing.Point(344, 296)
        Me.LblPDOtherTraining.Name = "LblPDOtherTraining"
        Me.LblPDOtherTraining.Size = New System.Drawing.Size(87, 15)
        Me.LblPDOtherTraining.TabIndex = 181
        Me.LblPDOtherTraining.Text = "Other Training"
        Me.LblPDOtherTraining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPDOtherTraining
        '
        Me.cboPDOtherTraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPDOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPDOtherTraining.FormattingEnabled = True
        Me.cboPDOtherTraining.Location = New System.Drawing.Point(437, 294)
        Me.cboPDOtherTraining.Name = "cboPDOtherTraining"
        Me.cboPDOtherTraining.Size = New System.Drawing.Size(198, 21)
        Me.cboPDOtherTraining.TabIndex = 173
        '
        'LblPDActivity
        '
        Me.LblPDActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDActivity.Location = New System.Drawing.Point(344, 242)
        Me.LblPDActivity.Name = "LblPDActivity"
        Me.LblPDActivity.Size = New System.Drawing.Size(87, 15)
        Me.LblPDActivity.TabIndex = 183
        Me.LblPDActivity.Text = "Activity"
        Me.LblPDActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPDTimeFrame
        '
        Me.LblPDTimeFrame.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDTimeFrame.Location = New System.Drawing.Point(344, 323)
        Me.LblPDTimeFrame.Name = "LblPDTimeFrame"
        Me.LblPDTimeFrame.Size = New System.Drawing.Size(87, 15)
        Me.LblPDTimeFrame.TabIndex = 182
        Me.LblPDTimeFrame.Text = "Time Frame"
        Me.LblPDTimeFrame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPDImprovement
        '
        Me.LblPDImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPDImprovement.Location = New System.Drawing.Point(344, 215)
        Me.LblPDImprovement.Name = "LblPDImprovement"
        Me.LblPDImprovement.Size = New System.Drawing.Size(87, 15)
        Me.LblPDImprovement.TabIndex = 180
        Me.LblPDImprovement.Text = "Improvement"
        Me.LblPDImprovement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign12
        '
        Me.objlblSign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign12.ForeColor = System.Drawing.Color.Red
        Me.objlblSign12.Location = New System.Drawing.Point(11, 40)
        Me.objlblSign12.Name = "objlblSign12"
        Me.objlblSign12.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign12.TabIndex = 248
        Me.objlblSign12.Text = "*"
        Me.objlblSign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblImpOtherTraining
        '
        Me.lblImpOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpOtherTraining.Location = New System.Drawing.Point(29, 296)
        Me.lblImpOtherTraining.Name = "lblImpOtherTraining"
        Me.lblImpOtherTraining.Size = New System.Drawing.Size(87, 15)
        Me.lblImpOtherTraining.TabIndex = 138
        Me.lblImpOtherTraining.Text = "Other Training"
        Me.lblImpOtherTraining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboImpImprovement
        '
        Me.cboImpImprovement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpImprovement.FormattingEnabled = True
        Me.cboImpImprovement.Location = New System.Drawing.Point(122, 213)
        Me.cboImpImprovement.Name = "cboImpImprovement"
        Me.cboImpImprovement.Size = New System.Drawing.Size(198, 21)
        Me.cboImpImprovement.TabIndex = 137
        '
        'LblImpTrainingLearningObjective
        '
        Me.LblImpTrainingLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpTrainingLearningObjective.Location = New System.Drawing.Point(29, 350)
        Me.LblImpTrainingLearningObjective.Name = "LblImpTrainingLearningObjective"
        Me.LblImpTrainingLearningObjective.Size = New System.Drawing.Size(87, 29)
        Me.LblImpTrainingLearningObjective.TabIndex = 151
        Me.LblImpTrainingLearningObjective.Text = "Training Or Learning Obj."
        Me.LblImpTrainingLearningObjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImpTimeframe
        '
        Me.lblImpTimeframe.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpTimeframe.Location = New System.Drawing.Point(29, 323)
        Me.lblImpTimeframe.Name = "lblImpTimeframe"
        Me.lblImpTimeframe.Size = New System.Drawing.Size(87, 15)
        Me.lblImpTimeframe.TabIndex = 140
        Me.lblImpTimeframe.Text = "Time Frame"
        Me.lblImpTimeframe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboImpOtherTraining
        '
        Me.cboImpOtherTraining.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpOtherTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpOtherTraining.FormattingEnabled = True
        Me.cboImpOtherTraining.Location = New System.Drawing.Point(122, 294)
        Me.cboImpOtherTraining.Name = "cboImpOtherTraining"
        Me.cboImpOtherTraining.Size = New System.Drawing.Size(198, 21)
        Me.cboImpOtherTraining.TabIndex = 139
        '
        'LblImpImprovement
        '
        Me.LblImpImprovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpImprovement.Location = New System.Drawing.Point(29, 215)
        Me.LblImpImprovement.Name = "LblImpImprovement"
        Me.LblImpImprovement.Size = New System.Drawing.Size(87, 15)
        Me.LblImpImprovement.TabIndex = 136
        Me.LblImpImprovement.Text = "Improvement"
        Me.LblImpImprovement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboImpTimeFrame
        '
        Me.cboImpTimeFrame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpTimeFrame.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpTimeFrame.FormattingEnabled = True
        Me.cboImpTimeFrame.Location = New System.Drawing.Point(122, 321)
        Me.cboImpTimeFrame.Name = "cboImpTimeFrame"
        Me.cboImpTimeFrame.Size = New System.Drawing.Size(198, 21)
        Me.cboImpTimeFrame.TabIndex = 141
        '
        'cboImpTrainingLearningObjective
        '
        Me.cboImpTrainingLearningObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpTrainingLearningObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpTrainingLearningObjective.FormattingEnabled = True
        Me.cboImpTrainingLearningObjective.Location = New System.Drawing.Point(122, 348)
        Me.cboImpTrainingLearningObjective.Name = "cboImpTrainingLearningObjective"
        Me.cboImpTrainingLearningObjective.Size = New System.Drawing.Size(198, 21)
        Me.cboImpTrainingLearningObjective.TabIndex = 152
        '
        'LblImpActivity
        '
        Me.LblImpActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpActivity.Location = New System.Drawing.Point(29, 242)
        Me.LblImpActivity.Name = "LblImpActivity"
        Me.LblImpActivity.Size = New System.Drawing.Size(87, 15)
        Me.LblImpActivity.TabIndex = 142
        Me.LblImpActivity.Text = "Activity"
        Me.LblImpActivity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboImpActivity
        '
        Me.cboImpActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpActivity.FormattingEnabled = True
        Me.cboImpActivity.Location = New System.Drawing.Point(122, 240)
        Me.cboImpActivity.Name = "cboImpActivity"
        Me.cboImpActivity.Size = New System.Drawing.Size(198, 21)
        Me.cboImpActivity.TabIndex = 143
        '
        'LblImpSupportRequired
        '
        Me.LblImpSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblImpSupportRequired.Location = New System.Drawing.Point(29, 269)
        Me.LblImpSupportRequired.Name = "LblImpSupportRequired"
        Me.LblImpSupportRequired.Size = New System.Drawing.Size(87, 15)
        Me.LblImpSupportRequired.TabIndex = 144
        Me.LblImpSupportRequired.Text = "Support Required"
        Me.LblImpSupportRequired.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboImpSupportRequired
        '
        Me.cboImpSupportRequired.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboImpSupportRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboImpSupportRequired.FormattingEnabled = True
        Me.cboImpSupportRequired.Location = New System.Drawing.Point(122, 267)
        Me.cboImpSupportRequired.Name = "cboImpSupportRequired"
        Me.cboImpSupportRequired.Size = New System.Drawing.Size(198, 21)
        Me.cboImpSupportRequired.TabIndex = 145
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(29, 41)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(87, 15)
        Me.lblPeriod.TabIndex = 246
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(122, 38)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(198, 21)
        Me.cboPeriod.TabIndex = 247
        '
        'LblRemark2
        '
        Me.LblRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRemark2.Location = New System.Drawing.Point(344, 117)
        Me.LblRemark2.Name = "LblRemark2"
        Me.LblRemark2.Size = New System.Drawing.Size(87, 15)
        Me.LblRemark2.TabIndex = 244
        Me.LblRemark2.Text = "Remark2"
        Me.LblRemark2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRemark2
        '
        Me.cboRemark2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRemark2.FormattingEnabled = True
        Me.cboRemark2.Location = New System.Drawing.Point(437, 115)
        Me.cboRemark2.Name = "cboRemark2"
        Me.cboRemark2.Size = New System.Drawing.Size(198, 21)
        Me.cboRemark2.TabIndex = 245
        '
        'LblRemark1
        '
        Me.LblRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRemark1.Location = New System.Drawing.Point(344, 90)
        Me.LblRemark1.Name = "LblRemark1"
        Me.LblRemark1.Size = New System.Drawing.Size(87, 15)
        Me.LblRemark1.TabIndex = 242
        Me.LblRemark1.Text = "Remark1"
        Me.LblRemark1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboRemark1
        '
        Me.cboRemark1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRemark1.FormattingEnabled = True
        Me.cboRemark1.Location = New System.Drawing.Point(437, 88)
        Me.cboRemark1.Name = "cboRemark1"
        Me.cboRemark1.Size = New System.Drawing.Size(198, 21)
        Me.cboRemark1.TabIndex = 243
        '
        'cboReviewerCode
        '
        Me.cboReviewerCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReviewerCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReviewerCode.FormattingEnabled = True
        Me.cboReviewerCode.Location = New System.Drawing.Point(122, 65)
        Me.cboReviewerCode.Name = "cboReviewerCode"
        Me.cboReviewerCode.Size = New System.Drawing.Size(198, 21)
        Me.cboReviewerCode.TabIndex = 116
        '
        'LblReviewerCode
        '
        Me.LblReviewerCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReviewerCode.Location = New System.Drawing.Point(29, 68)
        Me.LblReviewerCode.Name = "LblReviewerCode"
        Me.LblReviewerCode.Size = New System.Drawing.Size(87, 15)
        Me.LblReviewerCode.TabIndex = 115
        Me.LblReviewerCode.Text = "Reviewer Code"
        Me.LblReviewerCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(11, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(12, 17)
        Me.Label11.TabIndex = 114
        Me.Label11.Text = "*"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(326, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(12, 17)
        Me.Label1.TabIndex = 101
        Me.Label1.Text = "*"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'LblResult
        '
        Me.LblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblResult.Location = New System.Drawing.Point(344, 64)
        Me.LblResult.Name = "LblResult"
        Me.LblResult.Size = New System.Drawing.Size(87, 15)
        Me.LblResult.TabIndex = 86
        Me.LblResult.Text = "Result"
        Me.LblResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(437, 61)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(198, 21)
        Me.cboResult.TabIndex = 87
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(326, 38)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign6.TabIndex = 81
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.objlblSign6.Visible = False
        '
        'lblIsDefault
        '
        Me.lblIsDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsDefault.Location = New System.Drawing.Point(344, 36)
        Me.lblIsDefault.Name = "lblIsDefault"
        Me.lblIsDefault.Size = New System.Drawing.Size(87, 15)
        Me.lblIsDefault.TabIndex = 82
        Me.lblIsDefault.Text = "Sub Item Code"
        Me.lblIsDefault.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentSubItemCode
        '
        Me.cboAssessmentSubItemCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentSubItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentSubItemCode.FormattingEnabled = True
        Me.cboAssessmentSubItemCode.Location = New System.Drawing.Point(437, 34)
        Me.cboAssessmentSubItemCode.Name = "cboAssessmentSubItemCode"
        Me.cboAssessmentSubItemCode.Size = New System.Drawing.Size(198, 21)
        Me.cboAssessmentSubItemCode.TabIndex = 83
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(11, 148)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign3.TabIndex = 75
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(29, 149)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(87, 15)
        Me.lblAssessmentItemCode.TabIndex = 76
        Me.lblAssessmentItemCode.Text = "Item Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentItemCode
        '
        Me.cboAssessmentItemCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentItemCode.FormattingEnabled = True
        Me.cboAssessmentItemCode.Location = New System.Drawing.Point(122, 146)
        Me.cboAssessmentItemCode.Name = "cboAssessmentItemCode"
        Me.cboAssessmentItemCode.Size = New System.Drawing.Size(198, 21)
        Me.cboAssessmentItemCode.TabIndex = 77
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(11, 121)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign2.TabIndex = 72
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblAssessmentGrpCode
        '
        Me.lblAssessmentGrpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentGrpCode.Location = New System.Drawing.Point(29, 122)
        Me.lblAssessmentGrpCode.Name = "lblAssessmentGrpCode"
        Me.lblAssessmentGrpCode.Size = New System.Drawing.Size(87, 15)
        Me.lblAssessmentGrpCode.TabIndex = 73
        Me.lblAssessmentGrpCode.Text = "Group Code"
        Me.lblAssessmentGrpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssessmentGrpCode
        '
        Me.cboAssessmentGrpCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessmentGrpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessmentGrpCode.FormattingEnabled = True
        Me.cboAssessmentGrpCode.Location = New System.Drawing.Point(122, 119)
        Me.cboAssessmentGrpCode.Name = "cboAssessmentGrpCode"
        Me.cboAssessmentGrpCode.Size = New System.Drawing.Size(198, 21)
        Me.cboAssessmentGrpCode.TabIndex = 74
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(122, 92)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(198, 21)
        Me.cboEmployeeCode.TabIndex = 71
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(29, 95)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(87, 15)
        Me.lblEmployeeCode.TabIndex = 70
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(11, 94)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign1.TabIndex = 69
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(480, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(167, 19)
        Me.lblCaption.TabIndex = 68
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(816, 399)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(779, 196)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 21)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(180, 196)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(593, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 174)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(635, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Reviewer Assessment Import Wizard"
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(816, 399)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhReviewerCode, Me.colhEmployee, Me.colhGroupCode, Me.colhItemCode, Me.colhSubItemCode, Me.colhResult, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(792, 289)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhReviewerCode
        '
        Me.colhReviewerCode.HeaderText = "Reviewer Code"
        Me.colhReviewerCode.Name = "colhReviewerCode"
        Me.colhReviewerCode.ReadOnly = True
        Me.colhReviewerCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.Width = 80
        '
        'colhGroupCode
        '
        Me.colhGroupCode.HeaderText = "Group Code"
        Me.colhGroupCode.Name = "colhGroupCode"
        Me.colhGroupCode.ReadOnly = True
        Me.colhGroupCode.Width = 80
        '
        'colhItemCode
        '
        Me.colhItemCode.HeaderText = "Item Code"
        Me.colhItemCode.Name = "colhItemCode"
        Me.colhItemCode.ReadOnly = True
        Me.colhItemCode.Width = 80
        '
        'colhSubItemCode
        '
        Me.colhSubItemCode.HeaderText = "Sub Item Code"
        Me.colhSubItemCode.Name = "colhSubItemCode"
        Me.colhSubItemCode.ReadOnly = True
        '
        'colhResult
        '
        Me.colhResult.HeaderText = "Result"
        Me.colhResult.Name = "colhResult"
        Me.colhResult.ReadOnly = True
        Me.colhResult.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 80
        '
        'colhMessage
        '
        Me.colhMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 364)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(92, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(187, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(186, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(186, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(186, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(792, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(670, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(556, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(670, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(602, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(715, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(556, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(715, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(602, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elImprovement
        '
        Me.elImprovement.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elImprovement.Location = New System.Drawing.Point(11, 193)
        Me.elImprovement.Name = "elImprovement"
        Me.elImprovement.Size = New System.Drawing.Size(309, 17)
        Me.elImprovement.TabIndex = 2
        Me.elImprovement.Text = "Improvement"
        '
        'elPersonal
        '
        Me.elPersonal.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPersonal.Location = New System.Drawing.Point(326, 193)
        Me.elPersonal.Name = "elPersonal"
        Me.elPersonal.Size = New System.Drawing.Size(309, 17)
        Me.elPersonal.TabIndex = 252
        Me.elPersonal.Text = "Personal Development"
        '
        'frmImportReviewerAssessment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 447)
        Me.Controls.Add(Me.WizImportReviewerAssessment)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportReviewerAssessment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Reviewer Assessment"
        Me.WizImportReviewerAssessment.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents WizImportReviewerAssessment As eZee.Common.eZeeWizard
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentGrpCode As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentGrpCode As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents lblIsDefault As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentSubItemCode As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents cboAssessmentItemCode As System.Windows.Forms.ComboBox
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LblResult As System.Windows.Forms.Label
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboReviewerCode As System.Windows.Forms.ComboBox
    Friend WithEvents LblReviewerCode As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents LblImpTrainingLearningObjective As System.Windows.Forms.Label
    Friend WithEvents cboImpTrainingLearningObjective As System.Windows.Forms.ComboBox
    Friend WithEvents LblImpSupportRequired As System.Windows.Forms.Label
    Friend WithEvents cboImpSupportRequired As System.Windows.Forms.ComboBox
    Friend WithEvents LblImpActivity As System.Windows.Forms.Label
    Friend WithEvents cboImpActivity As System.Windows.Forms.ComboBox
    Friend WithEvents LblImpImprovement As System.Windows.Forms.Label
    Friend WithEvents lblImpTimeframe As System.Windows.Forms.Label
    Friend WithEvents cboImpTimeFrame As System.Windows.Forms.ComboBox
    Friend WithEvents lblImpOtherTraining As System.Windows.Forms.Label
    Friend WithEvents cboImpOtherTraining As System.Windows.Forms.ComboBox
    Friend WithEvents cboImpImprovement As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDTrainingLearningObjective As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDSupportRequired As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDActivity As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDTimeFrame As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDOtherTraining As System.Windows.Forms.ComboBox
    Friend WithEvents cboPDImprovement As System.Windows.Forms.ComboBox
    Friend WithEvents LblPDTrainingLearningObj As System.Windows.Forms.Label
    Friend WithEvents LblPDSupportRequired As System.Windows.Forms.Label
    Friend WithEvents LblPDActivity As System.Windows.Forms.Label
    Friend WithEvents LblPDImprovement As System.Windows.Forms.Label
    Friend WithEvents LblPDTimeFrame As System.Windows.Forms.Label
    Friend WithEvents LblPDOtherTraining As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhReviewerCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGroupCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSubItemCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhResult As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblSign12 As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblRemark2 As System.Windows.Forms.Label
    Friend WithEvents cboRemark2 As System.Windows.Forms.ComboBox
    Friend WithEvents LblRemark1 As System.Windows.Forms.Label
    Friend WithEvents cboRemark1 As System.Windows.Forms.ComboBox
    Friend WithEvents elPersonal As eZee.Common.eZeeLine
    Friend WithEvents elImprovement As eZee.Common.eZeeLine
End Class
