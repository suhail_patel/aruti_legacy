﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoadCompetencies

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmLoadCompetencies"
    Private mblnCancel As Boolean = True
    Private objCompetencies As clsassess_competencies_master
    Private mdtDataTable As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objCPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet

        'S.SANDEEP [21 NOV 2015] -- START
        Dim objCMaster As New clsCommon_Master
        'S.SANDEEP [21 NOV 2015] -- END

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))            
            'S.SANDEEP |02-MAR-2021| -- END

            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [21 NOV 2015] -- START
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, True, "List")
            With cboComptenceGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [21 NOV 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim xTypeId As Integer = -1
            If radGroup.Checked = True Then
                xTypeId = 0
            ElseIf radItems.Checked = True Then
                xTypeId = 1
            End If
            mdtDataTable = objCompetencies.LoadPerDefinedLibrary(xTypeId, txtSearch.Text.Trim)

            dgvData.AutoGenerateColumns = False
            objdgcolhAllCheck.DataPropertyName = "check"
            objdgcolhGrpName.DataPropertyName = "GrpName"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            dgcolhItems.DataPropertyName = "Item"

            dgvData.DataSource = mdtDataTable

            Call SetGridColor()
            Call SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            dgvcsHeader.Font = New Font(Me.Font.Name, 8, FontStyle.Bold)

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 9, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = "+"
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Visible = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmLoadCompetencies_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompetencies = New clsassess_competencies_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoadCompetencies_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmLoadCompetencies_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCompetencies = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_competencies_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_competencies_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Period is mandatory information. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            If mdtDataTable IsNot Nothing Then
                Me.Enabled = False
                Dim xRow() As DataRow = mdtDataTable.Select("check = True AND IsGrp = False")
                If xRow.Length > 0 Then
                    objlnkData.Visible = True
                    Dim xCategoryUnkid As Integer = -1
                    Dim objCMaster As clsCommon_Master
                    For xInt As Integer = 0 To xRow.Length - 1
                        Application.DoEvents()
                        objCMaster = New clsCommon_Master
                        xCategoryUnkid = objCMaster.GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, CStr(xRow(xInt).Item("GrpName")))
                        If xCategoryUnkid <= 0 Then
                            objCMaster._Code = CStr(xRow(xInt).Item("GrpName"))
                            objCMaster._Name = CStr(xRow(xInt).Item("GrpName"))
                            objCMaster._Mastertype = clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES
                            If objCMaster.Insert() Then
                                xCategoryUnkid = objCMaster._Masterunkid
                            Else
                                eZeeMsgBox.Show(objCMaster._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If

                        'S.SANDEEP [21 NOV 2015] -- START
                        'If objCompetencies.isExist(xCategoryUnkid, CStr(xRow(xInt).Item("Item").ToString.Trim), CInt(cboPeriod.SelectedValue)) = False Then
                        If objCompetencies.isExist(xCategoryUnkid, CStr(xRow(xInt).Item("Item").ToString.Trim), CInt(cboPeriod.SelectedValue), CInt(cboComptenceGroup.SelectedValue)) = False Then
                            'S.SANDEEP [21 NOV 2015] -- END

                            objCompetencies._Periodunkid = CInt(cboPeriod.SelectedValue)
                            objCompetencies._Competence_Categoryunkid = xCategoryUnkid
                            objCompetencies._Description = ""
                            objCompetencies._Isactive = True
                            objCompetencies._Name = CStr(xRow(xInt).Item("Item").ToString.Trim)
                            objCompetencies._Name1 = CStr(xRow(xInt).Item("Item").ToString.Trim)
                            objCompetencies._Name2 = CStr(xRow(xInt).Item("Item").ToString.Trim)
                            objCompetencies._Scalemasterunkid = CInt(cboScore.SelectedValue)
                            'S.SANDEEP [21 NOV 2015] -- START
                            objCompetencies._CompetenceGroupUnkid = CInt(cboComptenceGroup.SelectedValue)
                            'S.SANDEEP [21 NOV 2015] -- END

                            'S.SANDEEP |29-MAR-2021| -- START
                            'ISSUE/ENHANCEMENT : Competencies Training
                            'If objCompetencies.Insert() = False Then
                            If objCompetencies.Insert(Nothing) = False Then
                                'S.SANDEEP |29-MAR-2021| -- END
                                eZeeMsgBox.Show(objCompetencies._Message, enMsgBoxStyle.Information)
                            End If
                        End If
                        objlnkData.Text = CStr(xInt + 1) & "/" & xRow.Length
                    Next
                End If
                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
            objlnkData.Visible = False : objlnkData.Text = "" : Me.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If txtSearch.Text.Trim.Length > 0 Then
                If radGroup.Checked = False AndAlso radItems.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one searching type."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtSearch.Text = ""
            radGroup.Checked = False
            radItems.Checked = False
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [21 NOV 2015] -- START
    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboComptenceGroup.ValueMember
                .DisplayMember = cboComptenceGroup.DisplayMember
                .DataSource = CType(cboComptenceGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboComptenceGroup.SelectedValue = .SelectedValue
                    cboComptenceGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [21 NOV 2015] -- END

#End Region

#Region " DataGrid Events "

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError
        e.Cancel = True
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub
            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhCollapse.Index
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is "-" Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "-"
                        End If
                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpName.Index).Value) = CStr(dgvData.Rows(i).Cells(objdgcolhGrpName.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case objdgcolhAllCheck.Index
                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpName.Index).Value) = CStr(dgvData.Rows(i).Cells(objdgcolhGrpName.Index).Value) Then
                                dgvData.Rows(i).Cells(objdgcolhAllCheck.Index).Value = dgvData.Rows(e.RowIndex).Cells(objdgcolhAllCheck.Index).Value
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuExpandGroups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExpandGroups.Click
        Try
            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = "-"
                Else
                    dgvData.Rows(i).Visible = True
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExpandGroups_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuCollapseGroups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCollapseGroups.Click
        Try
            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = "+"
                Else
                    dgvData.Rows(i).Visible = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCollapseGroups_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                dgvRow.Cells(objdgcolhAllCheck.Index).Value = objchkAll.CheckState
            Next
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet : Dim objAScale As New clsAssessment_Scale
            dsList = objAScale.getComboList_ScaleGroup(CInt(cboPeriod.SelectedValue), "List", True)
            With cboScore
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssignment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssignment.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbAssignment.Text = Language._Object.getCaption(Me.gbAssignment.Name, Me.gbAssignment.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblScoreScale.Text = Language._Object.getCaption(Me.lblScoreScale.Name, Me.lblScoreScale.Text)
            Me.dgcolhItems.HeaderText = Language._Object.getCaption(Me.dgcolhItems.Name, Me.dgcolhItems.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class