﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmGroupList

#Region "Private Variable"

    Private objAssessGroupmaster As clsassess_group_master
    Private ReadOnly mstrModuleName As String = "frmGroupList"
    Private mintReferenceUnkid As Integer = -1

#End Region

#Region "Form's Event"

    Private Sub frmGroupList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessGroupmaster = New clsassess_group_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            radBranch.Checked = True

            If lvAssessmentGroup.Items.Count > 0 Then lvAssessmentGroup.Items(0).Selected = True
            lvAssessmentGroup.Select()

            Call SetVisibility()

            lvAssessmentGroup.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroupList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroupList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAssessmentGroup.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGroupList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGroupList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessGroupmaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_group_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_group_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmGroup_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                'S.SANDEEP [ 05 NOV 2014 ] -- START
                'fillList()
                'S.SANDEEP [ 05 NOV 2014 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvAssessmentGroup.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvAssessmentGroup.Select()
                Exit Sub
            End If
            Dim objfrmGroup_AddEdit As New frmGroup_AddEdit
            Try
                If User._Object._Isrighttoleft = True Then
                    objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objfrmGroup_AddEdit.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
                End If

                If objfrmGroup_AddEdit.displayDialog(CInt(lvAssessmentGroup.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If

                objfrmGroup_AddEdit = Nothing

            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmGroup_AddEdit IsNot Nothing Then objfrmGroup_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssessmentGroup.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssessmentGroup.Select()
            Exit Sub
        End If

        If objAssessGroupmaster.isUsed(CInt(lvAssessmentGroup.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Group. Reason: This Assess Group is in use."), enMsgBoxStyle.Information) '?2
            lvAssessmentGroup.Select()
            Exit Sub
        End If

        Try
            If chkDelete.Checked = False Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Assess Group Allocation?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    objAssessGroupmaster.DeleteAllocationTran(CInt(lvAssessmentGroup.SelectedItems(0).SubItems(objcolhTranId.Index).Text), CInt(lvAssessmentGroup.SelectedItems(0).Tag))
                    If objAssessGroupmaster._Message <> "" Then
                        eZeeMsgBox.Show(objAssessGroupmaster._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    lvAssessmentGroup.SelectedItems(0).Remove()
                End If
            Else
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Group with all Allocation?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    objAssessGroupmaster.Delete(CInt(lvAssessmentGroup.SelectedItems(0).Tag))
                    If objAssessGroupmaster._Message <> "" Then
                        eZeeMsgBox.Show(objAssessGroupmaster._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    lvAssessmentGroup.SelectedItems(0).Remove()
                End If
            End If

            Call fillList()

            lvAssessmentGroup.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'S.SANDEEP [ 05 NOV 2014 ] -- START
            If radAllocation.Checked = False AndAlso radEmployee.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select at-least one mode to fill below list."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 05 NOV 2014 ] -- END
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'S.SANDEEP [ 05 NOV 2014 ] -- START
            radAllocation.Checked = True
            'S.SANDEEP [ 05 NOV 2014 ] -- END
            cboAllocation.SelectedIndex = 0
            txtName.Text = ""
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocation.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboAllocation.ValueMember
                .DisplayMember = cboAllocation.DisplayMember
                .DataSource = CType(cboAllocation.DataSource, DataTable)
                'S.SANDEEP [ 05 NOV 2014 ] -- START
                If radEmployee.Checked = True Then
                    .CodeMember = "employeecode"
                End If
                'S.SANDEEP [ 05 NOV 2014 ] -- END
            End With

            If frm.DisplayDialog Then
                cboAllocation.SelectedValue = frm.SelectedValue
                cboAllocation.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim objStation As New clsStation
            Dim dsStation As DataSet = objStation.getComboList("Station", True)
            cboAllocation.ValueMember = "stationunkid"
            cboAllocation.DisplayMember = "name"
            cboAllocation.DataSource = dsStation.Tables("Station")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsAssessmentGroup As DataSet = Nothing
        Dim strSearching As String = ""
        Dim dtAssessmentGroup As DataTable = Nothing
        Try

            If User._Object.Privilege._AllowtoViewAssessmentGroupList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsAssessmentGroup = objAssessGroupmaster.GetList("AssessGroup", mintReferenceUnkid)

                If txtName.Text.Trim.Length > 0 Then
                    strSearching &= "AND assessgroup_name like '%" & txtName.Text.Trim & "%' "
                End If

                If CInt(cboAllocation.SelectedValue) > 0 Then
                    strSearching &= "AND Id = '" & CInt(cboAllocation.SelectedValue) & "' "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtAssessmentGroup = New DataView(dsAssessmentGroup.Tables("AssessGroup"), strSearching, "Allocation", DataViewRowState.CurrentRows).ToTable
                Else
                    dtAssessmentGroup = New DataView(dsAssessmentGroup.Tables("AssessGroup"), "", "Allocation", DataViewRowState.CurrentRows).ToTable
                End If

                Dim lvItem As ListViewItem

                lvAssessmentGroup.Items.Clear()
                For Each drRow As DataRow In dtAssessmentGroup.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = drRow.Item("Allocation").ToString
                    lvItem.SubItems.Add(drRow("assessgroup_code").ToString)
                    lvItem.SubItems.Add(drRow("assessgroup_name").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvItem.SubItems.Add(drRow("assessgrouptranunkid").ToString)
                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow("weight").ToString)
                    'S.SANDEEP [ 09 AUG 2013 ] -- END
                    lvItem.Tag = drRow("assessgroupunkid")

                    lvAssessmentGroup.Items.Add(lvItem)
                Next

                If lvAssessmentGroup.Items.Count > 2 Then
                    colhDescription.Width = 210 - 20
                Else
                    colhDescription.Width = 210
                End If

                lvAssessmentGroup.GroupingColumn = objcolhAllocation
                lvAssessmentGroup.DisplayGroups(True)

                lvAssessmentGroup.GridLines = False

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            If dsAssessmentGroup IsNot Nothing Then dsAssessmentGroup.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AddAssessmentGroup
            'btnEdit.Enabled = User._Object.Privilege._EditAssessmentGroup
            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentGroup

            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessmentGroup
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessmentGroup
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessmentGroup
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Controls "

    Private Sub radAllocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAllocation.CheckedChanged
        Try
            If radAllocation.Checked = True Then
                cboMode.Enabled = True
                objlblValue.Text = radAllocation.Text
                Dim objMData As New clsMasterData
                Dim dsList As New DataSet
                dsList = objMData.GetEAllocation_Notification("List")
                'S.SANDEEP [31 AUG 2016] -- START
                'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                Dim dr As DataRow = dsList.Tables(0).NewRow
                dr.Item("Id") = enAllocation.EMPLOYEE_GRADES
                dr.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grades")
                dsList.Tables(0).Rows.Add(dr)
                'S.SANDEEP [31 AUG 2016] -- START
                cboMode.DataSource = Nothing
                With cboMode
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = 1
                End With
                objMData = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAllocation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub radBranch_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radBranch.CheckedChanged, radDepartment.CheckedChanged, _
    '                                                                                                         radSection.CheckedChanged, radUnit.CheckedChanged, _
    '                                                                                                         radJobGroup.CheckedChanged, radJobs.CheckedChanged, _
    '                                                                                                         radDepartmentGroup.CheckedChanged, radSectionGroup.CheckedChanged, _
    '                                                                                                         radUnitGroup.CheckedChanged, radTeam.CheckedChanged

    '    Try
    '        Dim dsList As New DataSet
    '        cboAllocation.DataSource = Nothing

    '        Select Case CType(sender, RadioButton).Name.ToUpper
    '            Case "RADBRANCH"
    '                Dim objBranch As New clsStation
    '                dsList = objBranch.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "stationunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objBranch = Nothing
    '                mintReferenceUnkid = enAllocation.BRANCH
    '            Case "RADDEPARTMENT"
    '                Dim objDept As New clsDepartment
    '                dsList = objDept.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "departmentunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objDept = Nothing
    '                mintReferenceUnkid = enAllocation.DEPARTMENT
    '            Case "RADSECTION"
    '                Dim objSection As New clsSections
    '                dsList = objSection.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "sectionunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objSection = Nothing
    '                mintReferenceUnkid = enAllocation.SECTION
    '            Case "RADUNIT"
    '                Dim objUnit As New clsUnits
    '                dsList = objUnit.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "unitunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objUnit = Nothing
    '                mintReferenceUnkid = enAllocation.UNIT
    '            Case "RADJOBGROUP"
    '                Dim objJobGrp As New clsJobGroup
    '                dsList = objJobGrp.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "jobgroupunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objJobGrp = Nothing
    '                mintReferenceUnkid = enAllocation.JOB_GROUP
    '            Case "RADJOBS"
    '                Dim objJob As New clsJobs
    '                dsList = objJob.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "jobunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objJob = Nothing
    '                mintReferenceUnkid = enAllocation.JOBS
    '            Case "RADDEPARTMENTGROUP"
    '                Dim objDeptGrp As New clsDepartmentGroup
    '                dsList = objDeptGrp.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "deptgroupunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objDeptGrp = Nothing
    '                mintReferenceUnkid = enAllocation.DEPARTMENT_GROUP
    '            Case "RADSECTIONGROUP"
    '                Dim objSectionGrp As New clsSectionGroup
    '                dsList = objSectionGrp.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "sectiongroupunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objSectionGrp = Nothing
    '                mintReferenceUnkid = enAllocation.SECTION_GROUP
    '            Case "RADUNITGROUP"
    '                Dim objUnitGrp As New clsUnitGroup
    '                dsList = objUnitGrp.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "unitgroupunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objUnitGrp = Nothing
    '                mintReferenceUnkid = enAllocation.UNIT_GROUP
    '            Case "RADTEAM"
    '                Dim objTeam As New clsTeams
    '                dsList = objTeam.getComboList("List", True)
    '                With cboAllocation
    '                    .ValueMember = "teamunkid"
    '                    .DisplayMember = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .SelectedValue = 0
    '                End With
    '                objTeam = Nothing
    '                mintReferenceUnkid = enAllocation.TEAM
    '        End Select

    '        objlblCaption.Text = CType(sender, RadioButton).Text

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radBranch_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub radEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged
        Try
            If radEmployee.Checked = True Then
                mintReferenceUnkid = enAllocation.EMPLOYEE
                cboMode.DataSource = Nothing
                cboMode.Enabled = False : objlblValue.Text = ""
                objlblCaption.Text = radEmployee.Text
                Dim objEmp As New clsEmployee_Master
                Dim dsList As New DataSet

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , , , , , , , , True)
                dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True)
                'S.SANDEEP [04 JUN 2015] -- END

                cboAllocation.DataSource = Nothing
                With cboAllocation
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = 0
                End With
                objEmp = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            If CInt(cboMode.SelectedValue) > 0 Then
                mintReferenceUnkid = CInt(cboMode.SelectedValue)
                objlblCaption.Text = cboMode.Text
                cboAllocation.DataSource = Nothing
                Dim dsList As New DataSet
                If radAllocation.Checked = True Then
                    Select Case CInt(cboMode.SelectedValue)
                        Case enAllocation.BRANCH
                            Dim objBranch As New clsStation
                            dsList = objBranch.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "stationunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objBranch = Nothing
                        Case enAllocation.DEPARTMENT_GROUP
                            Dim objDeptGrp As New clsDepartmentGroup
                            dsList = objDeptGrp.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "deptgroupunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objDeptGrp = Nothing
                        Case enAllocation.DEPARTMENT
                            Dim objDept As New clsDepartment
                            dsList = objDept.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "departmentunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objDept = Nothing
                        Case enAllocation.SECTION_GROUP
                            Dim objSectionGrp As New clsSectionGroup
                            dsList = objSectionGrp.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "sectiongroupunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objSectionGrp = Nothing
                        Case enAllocation.SECTION
                            Dim objSection As New clsSections
                            dsList = objSection.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "sectionunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objSection = Nothing
                        Case enAllocation.UNIT_GROUP
                            Dim objUnitGrp As New clsUnitGroup
                            dsList = objUnitGrp.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "unitgroupunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objUnitGrp = Nothing
                        Case enAllocation.UNIT
                            Dim objUnit As New clsUnits
                            dsList = objUnit.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "unitunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objUnit = Nothing
                        Case enAllocation.TEAM
                            Dim objTeam As New clsTeams
                            dsList = objTeam.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "teamunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objTeam = Nothing
                        Case enAllocation.JOB_GROUP
                            Dim objJobGrp As New clsJobGroup
                            dsList = objJobGrp.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "jobgroupunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objJobGrp = Nothing
                        Case enAllocation.JOBS
                            Dim objJob As New clsJobs
                            dsList = objJob.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "jobunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objJob = Nothing
                        Case enAllocation.CLASS_GROUP
                            Dim objClassGrp As New clsClassGroup
                            dsList = objClassGrp.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "classgroupunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objClassGrp = Nothing
                        Case enAllocation.CLASSES
                            Dim objClass As New clsClass
                            dsList = objClass.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "classesunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objClass = Nothing
                        Case enAllocation.COST_CENTER
                            Dim objCC As New clscostcenter_master
                            dsList = objCC.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "costcenterunkid"
                                .DisplayMember = "costcentername"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objCC = Nothing

                            'S.SANDEEP [31 AUG 2016] -- START
                            'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                        Case enAllocation.EMPLOYEE_GRADES
                            Dim objGrd As New clsGrade
                            dsList = objGrd.getComboList("List", True)
                            With cboAllocation
                                .ValueMember = "gradeunkid"
                                .DisplayMember = "name"
                                .DataSource = dsList.Tables(0)
                                .SelectedValue = 0
                            End With
                            objGrd = Nothing
                            'S.SANDEEP [31 AUG 2016] -- START
                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAllocation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAllocation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.gbAllocation.Text = Language._Object.getCaption(Me.gbAllocation.Name, Me.gbAllocation.Text)
            Me.radSectionGroup.Text = Language._Object.getCaption(Me.radSectionGroup.Name, Me.radSectionGroup.Text)
            Me.radDepartmentGroup.Text = Language._Object.getCaption(Me.radDepartmentGroup.Name, Me.radDepartmentGroup.Text)
            Me.radSection.Text = Language._Object.getCaption(Me.radSection.Name, Me.radSection.Text)
            Me.radDepartment.Text = Language._Object.getCaption(Me.radDepartment.Name, Me.radDepartment.Text)
            Me.radBranch.Text = Language._Object.getCaption(Me.radBranch.Name, Me.radBranch.Text)
            Me.radTeam.Text = Language._Object.getCaption(Me.radTeam.Name, Me.radTeam.Text)
            Me.radUnitGroup.Text = Language._Object.getCaption(Me.radUnitGroup.Name, Me.radUnitGroup.Text)
            Me.radJobs.Text = Language._Object.getCaption(Me.radJobs.Name, Me.radJobs.Text)
            Me.radJobGroup.Text = Language._Object.getCaption(Me.radJobGroup.Name, Me.radJobGroup.Text)
            Me.radUnit.Text = Language._Object.getCaption(Me.radUnit.Name, Me.radUnit.Text)
            Me.chkDelete.Text = Language._Object.getCaption(Me.chkDelete.Name, Me.chkDelete.Text)
            Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
            Me.radAllocation.Text = Language._Object.getCaption(Me.radAllocation.Name, Me.radAllocation.Text)
            Me.lblFilterMode.Text = Language._Object.getCaption(Me.lblFilterMode.Name, Me.lblFilterMode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Assess Group from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Group. Reason: This Assess Group is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Group with all Allocation?")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Assess Group Allocation?")
            Language.setMessage(mstrModuleName, 9, "Grades")
            Language.setMessage(mstrModuleName, 4, "Please select at-least one mode to fill below list.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmGroupList

'#Region "Private Variable"

'    Private objAssessGroupmaster As clsassess_group_master
'    Private ReadOnly mstrModuleName As String = "frmGroupList"

'#End Region

'#Region "Form's Event"

'    Private Sub frmGroupList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objAssessGroupmaster = New clsassess_group_master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            FillCombo()
'            fillList()
'            Call SetVisibility()

'            If lvAssessmentGroup.Items.Count > 0 Then lvAssessmentGroup.Items(0).Selected = True
'            lvAssessmentGroup.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGroupList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmGroupList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
'        Try
'            If e.KeyCode = Keys.Delete And lvAssessmentGroup.Focused = True Then
'                'Sohail (28 Jun 2011) -- Start
'                'Issue : Delete event fired even if there is no delete privilege
'                'btnDelete_Click(sender, e)
'                Call btnDelete.PerformClick()
'                'Sohail (28 Jun 2011) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmGroupList_KeyUp", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmGroupList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objAssessGroupmaster = Nothing
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsassess_group_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsassess_group_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END

'#End Region

'#Region "Button's Event"

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Try
'            Dim ObjFrm As New frmGroup_AddEdit
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                ObjFrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(ObjFrm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
'                fillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        Try
'            If lvAssessmentGroup.SelectedItems.Count < 1 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Group from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvAssessmentGroup.Select()
'                Exit Sub
'            End If
'            Dim objfrmGroup_AddEdit As New frmGroup_AddEdit
'            Try
'                'S.SANDEEP [ 20 AUG 2011 ] -- START
'                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'                If User._Object._Isrighttoleft = True Then
'                    objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
'                    objfrmGroup_AddEdit.RightToLeftLayout = True
'                    Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
'                End If
'                'S.SANDEEP [ 20 AUG 2011 ] -- END
'                Dim intSelectedIndex As Integer
'                intSelectedIndex = lvAssessmentGroup.SelectedItems(0).Index
'                'If objfrmGroup_AddEdit.displayDialog(CInt(lvAssessmentGroup.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
'                '    fillList()
'                'End If
'                objfrmGroup_AddEdit = Nothing

'                lvAssessmentGroup.Items(intSelectedIndex).Selected = True
'                lvAssessmentGroup.EnsureVisible(intSelectedIndex)
'                lvAssessmentGroup.Select()
'            Catch ex As Exception
'                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'            Finally
'                If objfrmGroup_AddEdit IsNot Nothing Then objfrmGroup_AddEdit.Dispose()
'            End Try
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If lvAssessmentGroup.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Group from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvAssessmentGroup.Select()
'            Exit Sub
'        End If
'        If objAssessGroupmaster.isUsed(CInt(lvAssessmentGroup.SelectedItems(0).Tag)) Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Group. Reason: This Assess Group is in use."), enMsgBoxStyle.Information) '?2
'            lvAssessmentGroup.Select()
'            Exit Sub
'        End If
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvAssessmentGroup.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Group?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                objAssessGroupmaster.Delete(CInt(lvAssessmentGroup.SelectedItems(0).Tag))
'                lvAssessmentGroup.SelectedItems(0).Remove()

'                If lvAssessmentGroup.Items.Count <= 0 Then
'                    Exit Try
'                End If

'                If lvAssessmentGroup.Items.Count = intSelectedIndex Then
'                    intSelectedIndex = lvAssessmentGroup.Items.Count - 1
'                    lvAssessmentGroup.Items(intSelectedIndex).Selected = True
'                    lvAssessmentGroup.EnsureVisible(intSelectedIndex)
'                ElseIf lvAssessmentGroup.Items.Count <> 0 Then
'                    lvAssessmentGroup.Items(intSelectedIndex).Selected = True
'                    lvAssessmentGroup.EnsureVisible(intSelectedIndex)
'                End If
'            End If
'            lvAssessmentGroup.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            fillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboBranch.SelectedIndex = 0
'            txtCode.Text = ""
'            txtName.Text = ""
'            fillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Try
'            Dim objStation As New clsStation
'            Dim dsStation As DataSet = objStation.getComboList("Station", True)
'            cboBranch.ValueMember = "stationunkid"
'            cboBranch.DisplayMember = "name"
'            cboBranch.DataSource = dsStation.Tables("Station")
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub fillList()
'        Dim dsAssessmentGroup As DataSet = Nothing
'        Dim strSearching As String = ""
'        Dim dtAssessmentGroup As DataTable = Nothing
'        Try
'            dsAssessmentGroup = objAssessGroupmaster.GetList("AssessGroup")

'            If CInt(cboBranch.SelectedValue) > 0 Then
'                strSearching = "AND stationunkid =" & CInt(cboBranch.SelectedValue) & " "
'            End If

'            If txtCode.Text.Trim.Length > 0 Then
'                strSearching &= "AND assessgroup_code like '%" & txtCode.Text.Trim & "%' "
'            End If

'            If txtName.Text.Trim.Length > 0 Then
'                strSearching &= "AND assessgroup_name like '%" & txtName.Text.Trim & "%' "
'            End If

'            If strSearching.Length > 0 Then
'                strSearching = strSearching.Substring(3)
'                dtAssessmentGroup = New DataView(dsAssessmentGroup.Tables("AssessGroup"), strSearching, "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtAssessmentGroup = dsAssessmentGroup.Tables("AssessGroup")
'            End If

'            Dim lvItem As ListViewItem

'            lvAssessmentGroup.Items.Clear()
'            For Each drRow As DataRow In dtAssessmentGroup.Rows
'                lvItem = New ListViewItem
'                lvItem.Text = drRow("assessgroup_code").ToString
'                lvItem.Tag = drRow("assessgroupunkid")
'                lvItem.SubItems.Add(drRow("assessgroup_name").ToString)
'                lvItem.SubItems.Add(drRow("stationname").ToString)
'                lvItem.SubItems.Add(drRow("description").ToString)
'                lvAssessmentGroup.Items.Add(lvItem)
'            Next

'            If lvAssessmentGroup.Items.Count > 16 Then
'                colhDescription.Width = 275 - 18
'            Else
'                colhDescription.Width = 275
'            End If

'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
'        Finally
'            dsAssessmentGroup.Dispose()
'        End Try
'    End Sub

'    Private Sub SetVisibility()

'        Try
'            btnNew.Enabled = User._Object.Privilege._AddAssessmentGroup
'            btnEdit.Enabled = User._Object.Privilege._EditAssessmentGroup
'            btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentGroup

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'#End Region


'End Class


