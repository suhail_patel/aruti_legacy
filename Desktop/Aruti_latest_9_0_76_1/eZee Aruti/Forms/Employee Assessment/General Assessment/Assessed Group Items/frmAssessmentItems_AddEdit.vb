﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 11

Public Class frmAssessmentItems_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssessmentItems_AddEdit"
    Private mblnCancel As Boolean = True
    Private objAssessItemmaster As clsassess_item_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssessItemunkid As Integer = -1
    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Dim mdecRemain As Decimal = 0
    Dim mdecAssignedWeight As Decimal = 0
    'S.SANDEEP [ 29 DEC 2011 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintAssessItemunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintAssessItemunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAssessmentItems_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessItemmaster = New clsassess_item_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objAssessItemmaster._Assessitemunkid = mintAssessItemunkid
                cboYear.Enabled = False : cboPeriod.Enabled = False 'S.SANDEEP [ 28 DEC 2012 ] -- START
            End If
            FillCombo()
            GetValue()
            txtItemCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentItems_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentItems_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentItems_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentItems_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentItems_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentItems_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessItemmaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_item_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_item_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            If IsValid() = False Then Exit Sub
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssessItemmaster.Update()
            Else
                blnFlag = objAssessItemmaster.Insert()
            End If

            If blnFlag = False And objAssessItemmaster._Message <> "" Then
                eZeeMsgBox.Show(objAssessItemmaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAssessItemmaster = Nothing
                    objAssessItemmaster = New clsassess_item_master
                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    cboGroup.Tag = CInt(cboGroup.SelectedValue)
                    Call GetValue()
                    cboGroup.SelectedValue = CInt(cboGroup.Tag)
                    'Sohail (28 Jan 2012) -- End
                    cboGroup.Select()
                Else
                    mintAssessItemunkid = objAssessItemmaster._Assessitemunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrmLangPopup.displayDialog(txtAssessmentName.Text, objAssessItemmaster._Name1, objAssessItemmaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResultGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResultGroup.Click
        Try
            Dim objCommon As New frmCommonMaster
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objCommon.RightToLeft = Windows.Forms.RightToLeft.Yes
                objCommon.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objCommon)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Dim dsCombos As New DataSet
            Dim objResultGroup As New clsCommon_Master
            If objCommon.displayDialog(-1, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE) Then
                dsCombos = objResultGroup.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "List")
                With cboResultGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objfrmGroup_AddEdit As New frmGroup_AddEdit
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGroup_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmGroup_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillCombo()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            txtItemCode.BackColor = GUI.ColorComp
            txtAssessmentName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtWeight.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboYear.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtItemCode.Text = objAssessItemmaster._Code
            txtAssessmentName.Text = objAssessItemmaster._Name
            cboGroup.SelectedValue = CInt(objAssessItemmaster._Assessgroupunkid)
            cboResultGroup.SelectedValue = CInt(objAssessItemmaster._Resultgroupunkid)
            txtDescription.Text = objAssessItemmaster._Description
            txtWeight.Decimal = objAssessItemmaster._Weight
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboYear.SelectedValue = objAssessItemmaster._Yearunkid
            cboPeriod.SelectedValue = objAssessItemmaster._Periodunkid
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssessItemmaster._Code = txtItemCode.Text.Trim
            objAssessItemmaster._Name = txtAssessmentName.Text.Trim
            objAssessItemmaster._Assessgroupunkid = CInt(cboGroup.SelectedValue)
            objAssessItemmaster._Resultgroupunkid = CInt(cboResultGroup.SelectedValue)
            objAssessItemmaster._Description = txtDescription.Text.Trim
            objAssessItemmaster._Weight = txtWeight.Decimal
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAssessItemmaster._Yearunkid = CInt(cboYear.SelectedValue)
            objAssessItemmaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsGroup As DataSet = Nothing
        Try
            'FOR ASSESSMENT GROUP 
            Dim objAssessGroup As New clsassess_group_master
            dsGroup = objAssessGroup.getListForCombo("Group", True)
            cboGroup.ValueMember = "assessgroupunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsGroup.Tables(0)

            ' For Result Group
            Dim objCommonmaster As New clsCommon_Master
            dsGroup = objCommonmaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "Result Group")
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.DataSource = dsGroup.Tables(0)

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsGroup = objMaster.getComboListPAYYEAR("List", True, , , , True)
            dsGroup = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "List", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsGroup.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'objbtnAddGroup.Enabled = User._Object.Privilege._AddAssessmentGroup
            'S.SANDEEP [28 MAY 2015] -- END
            objbtnAddResultGroup.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function IsValid() As Boolean
        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Assessment Group is compulsory information.Please Select Assessment Group."), enMsgBoxStyle.Information)
                cboGroup.Focus()
                Return False
            ElseIf CInt(cboResultGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Result Group is compulsory information.Please Select Result Group."), enMsgBoxStyle.Information)
                cboResultGroup.Select()
                Return False
            ElseIf Trim(txtItemCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Assessment Item Code cannot be blank. Assessment Item Code is required information."), enMsgBoxStyle.Information)
                txtItemCode.Focus()
                Return False
            ElseIf Trim(txtAssessmentName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Assessment Item Name cannot be blank. Assessment Item Name is required information."), enMsgBoxStyle.Information)
                txtAssessmentName.Focus()
                Return False
            ElseIf txtWeight.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Weight cannot be blank. Weight is required information."), enMsgBoxStyle.Information)
                txtWeight.Focus()
                Return False
            ElseIf txtWeight.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Weight percent cannot be greater then 100 %."), enMsgBoxStyle.Information)
                txtWeight.Focus()
                Return False
            End If

            If CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Year is mandatory information. Please select Year to continue."), enMsgBoxStyle.Information)
                cboYear.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            'If mdecAssignedWeight + txtWeight.Decimal > 100 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Weight percent cannot be greater then 100 %."), enMsgBoxStyle.Information)
            '    txtWeight.Focus()
            '    Return False
            'End If

            If txtWeight.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Weight percent cannot be greater then 100 %."), enMsgBoxStyle.Information)
                txtWeight.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Control's Events "

    Private Sub cboGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedIndexChanged
        Try
            'If CInt(cboGroup.SelectedValue) > 0 Then
            '    objAssessItemmaster.RemainingWeight(CInt(cboGroup.SelectedValue), mdecRemain, mdecAssignedWeight, objAssessItemmaster._Assessitemunkid)
            '    If objAssessItemmaster._Assessitemunkid > 0 Then
            '        objlblCaption.Text = Language.getMessage(mstrModuleName, 7, "Total Assigned : ") & " " & (mdecRemain + mdecAssignedWeight).ToString & " " & _
            '                         Language.getMessage(mstrModuleName, 9, "%")
            '    Else
            '        objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Remaining : ") & " " & mdecRemain.ToString & " " & _
            '                         Language.getMessage(mstrModuleName, 9, "%")
            '    End If
            'Else
            '   objlblCaption.Text = ""
            'End If

            Dim mintResultGroupunkid As Integer = objAssessItemmaster.GetResultGroupFromAssessmentGroup()
            If mintResultGroupunkid > 0 Then
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = False
                objbtnAddResultGroup.Enabled = False
            Else
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = True
                objbtnAddResultGroup.Enabled = True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            Dim dsList As New DataSet

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbAssessmentItems.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAssessmentItems.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAssessmentItems.Text = Language._Object.getCaption(Me.gbAssessmentItems.Name, Me.gbAssessmentItems.Text)
			Me.lblAssessmentDescription.Text = Language._Object.getCaption(Me.lblAssessmentDescription.Name, Me.lblAssessmentDescription.Text)
			Me.lblAssessmentItem.Text = Language._Object.getCaption(Me.lblAssessmentItem.Name, Me.lblAssessmentItem.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.lblGroupCode.Text = Language._Object.getCaption(Me.lblGroupCode.Name, Me.lblGroupCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblResultGroup.Text = Language._Object.getCaption(Me.lblResultGroup.Name, Me.lblResultGroup.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Assessment Group is compulsory information.Please Select Assessment Group.")
			Language.setMessage(mstrModuleName, 2, "Result Group is compulsory information.Please Select Result Group.")
			Language.setMessage(mstrModuleName, 3, "Assessment Item Code cannot be blank. Assessment Item Code is required information.")
			Language.setMessage(mstrModuleName, 4, "Assessment Item Name cannot be blank. Assessment Item Name is required information.")
			Language.setMessage(mstrModuleName, 5, "Weight cannot be blank. Weight is required information.")
			Language.setMessage(mstrModuleName, 6, "Weight percent cannot be greater then 100 %.")
			Language.setMessage(mstrModuleName, 7, "Total Assigned :")
			Language.setMessage(mstrModuleName, 8, "Total Remaining :")
			Language.setMessage(mstrModuleName, 9, "%")
			Language.setMessage(mstrModuleName, 10, "Year is mandatory information. Please select Year to continue.")
			Language.setMessage(mstrModuleName, 11, "Period is mandatory information. Please select Period to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class