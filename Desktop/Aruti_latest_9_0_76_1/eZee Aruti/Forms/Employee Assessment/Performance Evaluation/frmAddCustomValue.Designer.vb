﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddCustomValue
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddCustomValue))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.plnMain = New System.Windows.Forms.Panel
        Me.tlbpCItems = New System.Windows.Forms.TableLayoutPanel
        Me.objlblCustomHeader = New System.Windows.Forms.Label
        Me.pnlItems = New System.Windows.Forms.Panel
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgcolhItems = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhItemTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSelectionModeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSelectedId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhReadOnly = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsSystemEntry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiscompletedtraining = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.plnMain.SuspendLayout()
        Me.tlbpCItems.SuspendLayout()
        Me.pnlItems.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'plnMain
        '
        Me.plnMain.Controls.Add(Me.tlbpCItems)
        Me.plnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.plnMain.Location = New System.Drawing.Point(0, 0)
        Me.plnMain.Name = "plnMain"
        Me.plnMain.Size = New System.Drawing.Size(752, 353)
        Me.plnMain.TabIndex = 0
        '
        'tlbpCItems
        '
        Me.tlbpCItems.ColumnCount = 1
        Me.tlbpCItems.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlbpCItems.Controls.Add(Me.objlblCustomHeader, 0, 0)
        Me.tlbpCItems.Controls.Add(Me.pnlItems, 0, 1)
        Me.tlbpCItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlbpCItems.Location = New System.Drawing.Point(0, 0)
        Me.tlbpCItems.Name = "tlbpCItems"
        Me.tlbpCItems.RowCount = 2
        Me.tlbpCItems.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.tlbpCItems.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 203.0!))
        Me.tlbpCItems.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlbpCItems.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlbpCItems.Size = New System.Drawing.Size(752, 353)
        Me.tlbpCItems.TabIndex = 2
        '
        'objlblCustomHeader
        '
        Me.objlblCustomHeader.BackColor = System.Drawing.Color.Transparent
        Me.objlblCustomHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.objlblCustomHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCustomHeader.Location = New System.Drawing.Point(3, 0)
        Me.objlblCustomHeader.Name = "objlblCustomHeader"
        Me.objlblCustomHeader.Size = New System.Drawing.Size(746, 25)
        Me.objlblCustomHeader.TabIndex = 20
        Me.objlblCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlItems
        '
        Me.pnlItems.Controls.Add(Me.dgvItems)
        Me.pnlItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlItems.Location = New System.Drawing.Point(3, 28)
        Me.pnlItems.Name = "pnlItems"
        Me.pnlItems.Size = New System.Drawing.Size(746, 322)
        Me.pnlItems.TabIndex = 21
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        Me.dgvItems.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhItems, Me.dgcolhValue, Me.objdgcolhItemTypeId, Me.objdgcolhSelectionModeId, Me.objdgcolhdate, Me.objdgcolhSelectedId, Me.objdgcolhReadOnly, Me.objdgcolhIsSystemEntry, Me.objdgcolhiscompletedtraining})
        Me.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvItems.Size = New System.Drawing.Size(746, 322)
        Me.dgvItems.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 353)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(752, 50)
        Me.objFooter.TabIndex = 3
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(540, 11)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(643, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgcolhItems
        '
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhItems.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhItems.HeaderText = "Custom Items"
        Me.dgcolhItems.Name = "dgcolhItems"
        Me.dgcolhItems.ReadOnly = True
        Me.dgcolhItems.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhItems.Width = 443
        '
        'dgcolhValue
        '
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhValue.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhValue.HeaderText = "Custom Value"
        Me.dgcolhValue.Name = "dgcolhValue"
        Me.dgcolhValue.ReadOnly = True
        Me.dgcolhValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhValue.Width = 300
        '
        'objdgcolhItemTypeId
        '
        Me.objdgcolhItemTypeId.HeaderText = "objdgcolhItemTypeId"
        Me.objdgcolhItemTypeId.Name = "objdgcolhItemTypeId"
        Me.objdgcolhItemTypeId.ReadOnly = True
        Me.objdgcolhItemTypeId.Visible = False
        '
        'objdgcolhSelectionModeId
        '
        Me.objdgcolhSelectionModeId.HeaderText = "objdgcolhSelectionModeId"
        Me.objdgcolhSelectionModeId.Name = "objdgcolhSelectionModeId"
        Me.objdgcolhSelectionModeId.ReadOnly = True
        Me.objdgcolhSelectionModeId.Visible = False
        '
        'objdgcolhdate
        '
        Me.objdgcolhdate.HeaderText = "objdgcolhdate"
        Me.objdgcolhdate.Name = "objdgcolhdate"
        Me.objdgcolhdate.ReadOnly = True
        Me.objdgcolhdate.Visible = False
        '
        'objdgcolhSelectedId
        '
        Me.objdgcolhSelectedId.HeaderText = "objdgcolhSelectedId"
        Me.objdgcolhSelectedId.Name = "objdgcolhSelectedId"
        Me.objdgcolhSelectedId.ReadOnly = True
        Me.objdgcolhSelectedId.Visible = False
        '
        'objdgcolhReadOnly
        '
        Me.objdgcolhReadOnly.HeaderText = "objdgcolhReadOnly"
        Me.objdgcolhReadOnly.Name = "objdgcolhReadOnly"
        Me.objdgcolhReadOnly.Visible = False
        '
        'objdgcolhIsSystemEntry
        '
        Me.objdgcolhIsSystemEntry.HeaderText = "objdgcolhIsSystemEntry"
        Me.objdgcolhIsSystemEntry.Name = "objdgcolhIsSystemEntry"
        Me.objdgcolhIsSystemEntry.Visible = False
        '
        'objdgcolhiscompletedtraining
        '
        Me.objdgcolhiscompletedtraining.HeaderText = "objdgcolhiscompletedtraining"
        Me.objdgcolhiscompletedtraining.Name = "objdgcolhiscompletedtraining"
        Me.objdgcolhiscompletedtraining.ReadOnly = True
        Me.objdgcolhiscompletedtraining.Visible = False
        '
        'frmAddCustomValue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(752, 403)
        Me.Controls.Add(Me.plnMain)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddCustomValue"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Custom Items Value"
        Me.plnMain.ResumeLayout(False)
        Me.tlbpCItems.ResumeLayout(False)
        Me.pnlItems.ResumeLayout(False)
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents plnMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tlbpCItems As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblCustomHeader As System.Windows.Forms.Label
    Friend WithEvents pnlItems As System.Windows.Forms.Panel
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhItems As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhItemTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSelectionModeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSelectedId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhReadOnly As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsSystemEntry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiscompletedtraining As System.Windows.Forms.DataGridViewTextBoxColumn
Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
