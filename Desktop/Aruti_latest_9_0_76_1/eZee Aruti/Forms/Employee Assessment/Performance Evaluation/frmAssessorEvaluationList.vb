﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessorEvaluationList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessorEvaluationList"
    Private objEvaluation As clsevaluation_analysis_master
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIsFormLoad As Boolean = False

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim mstrAssessorIds As String = String.Empty
        Try
            dsCombo = objEvaluation.getAssessorComboList(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                         ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, True, False) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END
            With cboAssessor
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Year")
                .SelectedValue = 0
            End With

            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing : objYear = Nothing : dsCombo.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AddAssessmentAnalysis
            'btnEdit.Enabled = User._Object.Privilege._EditAssessmentAnalysis
            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentAnalysis
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment

            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessorEvaluation
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessorEvaluation
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessorEvaluation
            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedAssessorEvaluation
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function UpdateScore(ByVal dr As DataRow) As Boolean
        Try
            If dr.Item("competency_value").ToString.Trim.Length > 0 Then
                For Each xVal As String In dr.Item("competency_value").ToString.Split(CChar(","))
                    If CInt(dr.Item("assessgroupunkid")) = CInt(xVal.Split(CChar("|"))(0)) Then
                        dr.Item("total_score") = CDec(xVal.Split(CChar("|"))(1)).ToString("#######################0.#0")
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateScore", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try
            If User._Object.Privilege._AllowtoViewAssessorEvaluationList = True Then

                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Period is mandatory information. Please select period to fill list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hrevaluation_analysis_master.assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
                End If

                If CInt(cboAssessor.SelectedValue) > 0 Then
                    StrSearching &= "AND hrevaluation_analysis_master.assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
                End If

                If CInt(cboYear.SelectedValue) > 0 Then
                    StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue)
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue)
                End If

                If dtpAssessmentdate.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
                End If

                If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                    StrSearching &= "AND ISNULL(iscommitted,0) = 1 "
                ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                    StrSearching &= "AND ISNULL(iscommitted,0) = 0 "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If chkShowMyEmp.Checked = True Then
                    StrSearching &= "AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
                End If


                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                dsList = objEvaluation.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                               enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboPeriod.SelectedValue), ConfigParameter._Object._IsCalibrationSettingActive, StrSearching)
                'S.SANDEEP |27-MAY-2019| -- END

                dtTable = New DataView(dsList.Tables(0), "", "assessoremployeeunkid,EmpName,smodeid", DataViewRowState.CurrentRows).ToTable

                dtTable.AsEnumerable().ToList.ForEach(Function(x) UpdateScore(x))

                dgvAssessorAssessList.AutoGenerateColumns = False

                dgcolhemp.DataPropertyName = "EmpName"
                dgcolhassessor.DataPropertyName = "Assessor"
                dgcolhyear.DataPropertyName = "YearName"
                dgcolhperiod.DataPropertyName = "PName"
                dgcolhassessdate.DataPropertyName = "assessmentdate"
                dgcolhmode.DataPropertyName = "smode"
                dgcolhscore.DataPropertyName = "total_score"
                objdgcolhperiodid.DataPropertyName = "periodunkid"
                objdgcolhstatusid.DataPropertyName = "Sid"
                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                'objdgcolhempid.DataPropertyName = "selfemployeeunkid"
                objdgcolhempid.DataPropertyName = "assessedemployeeunkid"
                'S.SANDEEP |13-NOV-2019| -- END
                objdgcolhanalysisid.DataPropertyName = "analysisunkid"
                objdgcolhiscommitted.DataPropertyName = "iscommitted"
                objdgcolhsmodeid.DataPropertyName = "smodeid"
                objdgcolhassessgroupunkid.DataPropertyName = "assessgroupunkid"
                objdgcolhmapuserid.DataPropertyName = "mapuserid"

                dgvAssessorAssessList.DataSource = dtTable

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssessorEvaluationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEvaluation = New clsevaluation_analysis_master
        Try
            chkShowCommited.CheckState = CheckState.Checked
            Call Set_Logo(Me, gApplicationType)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
            'S.SANDEEP [04 JUN 2015] -- START
            mblnIsFormLoad = True
            chkShowUncommited.Checked = True
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessorEvaluationList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessorEvaluationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessorEvaluationList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessorEvaluationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEvaluation = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsevaluation_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmPerformanceEvaluation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvAssessorAssessList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhmapuserid.Index).Value) <> User._Object._Userunkid Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you are not the assessor of the selected employee, Due to this you are not authorized to perform edit operation for this evaluation record."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'S.SANDEEP |25-MAR-2019| -- START
        If CBool(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhiscommitted.Index).Value) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'S.SANDEEP |25-MAR-2019| -- END

        Dim frm As New frmPerformanceEvaluation
        Try
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If objEvaluation.CheckComputionIsExist(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                       enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                       CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
            '        Dim frmVoid As New frmReasonSelection
            '        Dim sVoidReason As String = String.Empty
            '        If User._Object._Isrighttoleft = True Then
            '            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
            '            frmVoid.RightToLeftLayout = True
            '            Call Language.ctlRightToLeftlayOut(frmVoid)
            '        End If
            '        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
            '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

            '        Dim objComputeMst As New clsComputeScore_master
            '        objComputeMst._Isvoid = True
            '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '        objComputeMst._Voidreason = sVoidReason
            '        objComputeMst._Voiduserunkid = User._Object._Userunkid
            '        If objComputeMst.DeleteEmployeeWise(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
            '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), _
            '                                            enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
            '        Else
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '    Else
            '        Exit Sub
            '    End If
            'End If
            'S.SANDEEP |12-FEB-2019| -- END

            'Shani (24-May-2016) -- End

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), enAction.EDIT_ONE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvAssessorAssessList.SelectedRows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        'S.SANDEEP |25-MAR-2019| -- START
        If CBool(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhiscommitted.Index).Value) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'S.SANDEEP |25-MAR-2019| -- END

        Try
            'Shani (24-May-2016) -- Start

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If objEvaluation.CheckComputionIsExist(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                       enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                       CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

            '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
            '        Dim frmVoid As New frmReasonSelection
            '        Dim sVoidReason As String = String.Empty
            '        If User._Object._Isrighttoleft = True Then
            '            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
            '            frmVoid.RightToLeftLayout = True
            '            Call Language.ctlRightToLeftlayOut(frmVoid)
            '        End If
            '        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
            '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

            '        Dim objComputeMst As New clsComputeScore_master
            '        objComputeMst._Isvoid = True
            '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '        objComputeMst._Voidreason = sVoidReason
            '        objComputeMst._Voiduserunkid = User._Object._Userunkid
            '        If objComputeMst.DeleteEmployeeWise(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
            '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
            '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
            '        Else
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '    Else
            '        Exit Sub
            '    End If
            'End If
            'S.SANDEEP |12-FEB-2019| -- END

            'Shani (24-May-2016) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEvaluation._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objEvaluation._Isvoid = True
                objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objEvaluation._Voiduserunkid = User._Object._Userunkid

                objEvaluation.Delete(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value))

                If objEvaluation._Message <> "" Then
                    eZeeMsgBox.Show(objEvaluation._Message, enMsgBoxStyle.Information)
                Else
                    'S.SANDEEP [04 MAR 2015] -- START
                    'lvAssessorList.SelectedItems(0).Remove()
                    Call FillList()
                    'S.SANDEEP [04 MAR 2015] -- END
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAssessor.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            chkShowCommited.CheckState = CheckState.Checked
            mstrAdvanceFilter = String.Empty
            Call FillList()
            'S.SANDEEP [04 JUN 2015] -- START
            chkShowUncommited.CheckState = CheckState.Checked
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
        Try
            If dgvAssessorAssessList.SelectedRows.Count > 0 Then
                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                         CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
                                         CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'Shani (24-May-2016) -- Start

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If objEvaluation.CheckComputionIsExist(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
                '                                       enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                       CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value)) = True Then

                '    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
                '        Dim frmVoid As New frmReasonSelection
                '        Dim sVoidReason As String = String.Empty
                '        If User._Object._Isrighttoleft = True Then
                '            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
                '            frmVoid.RightToLeftLayout = True
                '            Call Language.ctlRightToLeftlayOut(frmVoid)
                '        End If
                '        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                '        If sVoidReason.Trim.Length <= 0 Then Exit Sub

                '        Dim objComputeMst As New clsComputeScore_master
                '        objComputeMst._Isvoid = True
                '        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '        objComputeMst._Voidreason = sVoidReason
                '        objComputeMst._Voiduserunkid = User._Object._Userunkid
                '        If objComputeMst.DeleteEmployeeWise(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value), _
                '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), _
                '                                            CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
                '        Else
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
                '            Exit Sub
                '        End If
                '    Else
                '        Exit Sub
                '    End If
                'End If
                'S.SANDEEP |12-FEB-2019| -- END

                'Shani (24-May-2016) -- End

                'S.SANDEEP |13-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                Dim strMessage As String = String.Empty
                'S.SANDEEP |13-NOV-2019| -- END
                If CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhstatusid.Index).Value) = enStatusType.Open Then
                    'S.SANDEEP |13-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                    'If objEvaluation.Unlock_Commit(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), _
                    '                             CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value)) = True Then
                    If objEvaluation.Unlock_Commit(CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhperiodid.Index).Value), _
                                                 CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhempid.Index).Value), strMessage) = True Then
                        'S.SANDEEP |13-NOV-2019| -- END
                        objEvaluation._Analysisunkid = CInt(dgvAssessorAssessList.SelectedRows(0).Cells(objdgcolhanalysisid.Index).Value)
                        objEvaluation._Iscommitted = False
                        objEvaluation._Committeddatetime = Nothing

                        'S.SANDEEP [27-APR-2017] -- START
                        'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                        'objEvaluation.Update()
                        objEvaluation.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, Nothing, False)
                        'S.SANDEEP [27-APR-2017] -- END

                        If objEvaluation._Message <> "" Then
                            eZeeMsgBox.Show(objEvaluation._Message)
                        End If
                        Call FillList()
                    Else
                        'S.SANDEEP |13-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                        If strMessage.Trim.Length > 0 Then eZeeMsgBox.Show(strMessage, enMsgBoxStyle.Information)
                        'S.SANDEEP |13-NOV-2019| -- END
                        Exit Sub
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowMyEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowMyEmp.CheckedChanged
        Try
            If mblnIsFormLoad = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowMyEmp_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ComboBox Event "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            Dim objAssessor As New clsevaluation_analysis_master
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "List", True)
            Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(FinancialYear._Object._DatabaseName, _
                                                                         User._Object._Userunkid, _
                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                         CInt(cboAssessor.SelectedValue), "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.DisplayMember = "NAME"
            cboEmployee.ValueMember = "Id"
            cboEmployee.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvAssessorAssessList_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvAssessorAssessList.CellPainting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If CBool(dgvAssessorAssessList.Rows(e.RowIndex).Cells(objdgcolhiscommitted.Index).Value) = True Then
                dgvAssessorAssessList.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Blue
            End If
            If dgvAssessorAssessList.Columns(e.ColumnIndex).DataPropertyName = dgcolhassessdate.DataPropertyName Then
                If dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgvAssessorAssessList.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If
            'If dgvSelfAssessList.Columns(e.ColumnIndex).DataPropertyName = dgcolhscore.DataPropertyName Then
            '    Dim xScore As List(Of Decimal)
            '    If dtComputeScore.AsEnumerable.Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)).Count > 0 Then
            '        If CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhsmodeid.Index).Value) = 1 Then
            '            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            '            If xScore IsNot Nothing AndAlso xScore.Count > 0 Then
            '                dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = xScore(0).ToString("#########0.#0")
            '            End If
            '        Else
            '            Dim xValue As String = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of String)("competency_value")).ToList.First()
            '            If xValue.Contains(",") = False Then
            '                xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
            '                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT And x.Field(Of Integer)("employeeunkid") = CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhempid.Index).Value)) _
            '                                                        .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
            '                If xScore IsNot Nothing AndAlso xScore.Count > 0 Then
            '                    dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = xScore(0).ToString("#########0.#0")
            '                End If
            '            Else
            '                For Each xVal As String In xValue.Split(CChar(","))
            '                    If CInt(dgvSelfAssessList.Rows(e.RowIndex).Cells(objdgcolhassessgroupunkid.Index).Value) = CInt(xVal.Split(CChar("|"))(0)) Then
            '                        dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = CDec(xVal.Split(CChar("|"))(1)).ToString("#########0.#0")
            '                    End If
            '                Next
            '            End If
            '        End If
            '    End If
            'Else
            '    dgvSelfAssessList.Rows(e.RowIndex).Cells(dgcolhscore.Index).Value = 0
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssessorAssessList_CellPainting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAssessorAssessList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvAssessorAssessList.DataBindingComplete
        If dgvAssessorAssessList.SelectedRows.Count <= 0 Then Exit Sub
        If dgvAssessorAssessList.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
            btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
        Else
            btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
        End If
    End Sub

    Private Sub dgvAssessorAssessList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvAssessorAssessList.DataError

    End Sub

    Private Sub dgvAssessorAssessList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAssessorAssessList.SelectionChanged
        Try
            If dgvAssessorAssessList.SelectedRows.Count <= 0 Then Exit Sub
            If dgvAssessorAssessList.SelectedRows(0).DefaultCellStyle.ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
            End If
            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedSelfEvaluation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssessorAssessList_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor
            Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
            Me.mnuImportAssessment.Text = Language._Object.getCaption(Me.mnuImportAssessment.Name, Me.mnuImportAssessment.Text)
            Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
            Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
            Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
            Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
            Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
            Me.chkShowMyEmp.Text = Language._Object.getCaption(Me.chkShowMyEmp.Name, Me.chkShowMyEmp.Text)
            Me.dgvAssessorAssessList.Text = Language._Object.getCaption(Me.dgvAssessorAssessList.Name, Me.dgvAssessorAssessList.Text)
            Me.dgcolhassessor.HeaderText = Language._Object.getCaption(Me.dgcolhassessor.Name, Me.dgcolhassessor.HeaderText)
            Me.dgcolhemp.HeaderText = Language._Object.getCaption(Me.dgcolhemp.Name, Me.dgcolhemp.HeaderText)
            Me.dgcolhyear.HeaderText = Language._Object.getCaption(Me.dgcolhyear.Name, Me.dgcolhyear.HeaderText)
            Me.dgcolhperiod.HeaderText = Language._Object.getCaption(Me.dgcolhperiod.Name, Me.dgcolhperiod.HeaderText)
            Me.dgcolhassessdate.HeaderText = Language._Object.getCaption(Me.dgcolhassessdate.Name, Me.dgcolhassessdate.HeaderText)
            Me.dgcolhmode.HeaderText = Language._Object.getCaption(Me.dgcolhmode.Name, Me.dgcolhmode.HeaderText)
            Me.dgcolhscore.HeaderText = Language._Object.getCaption(Me.dgcolhscore.Name, Me.dgcolhscore.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you are not the assessor of the selected employee, Due to this you are not authorized to perform edit operation for this evaluation record.")
            Language.setMessage(mstrModuleName, 9, "Successfully voided Computation score.")
            Language.setMessage(mstrModuleName, 10, "Failed to void Computation score process.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 13, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
            Language.setMessage(mstrModuleName, 14, "Sorry, Period is mandatory information. Please select period to fill list.")
            Language.setMessage(mstrModuleName, 15, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted.")
            Language.setMessage(mstrModuleName, 16, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'*************************************** S.SANDEEP [27-APR-2017] -- ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE ***************************************
'Public Class frmAssessorEvaluationList

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmAssessorEvaluationList"
'    Private objEvaluation As clsevaluation_analysis_master
'    Private mstrAdvanceFilter As String = String.Empty
'    'S.SANDEEP [04 JUN 2015] -- START
'    Private mblnIsFormLoad As Boolean = False
'    'S.SANDEEP [04 JUN 2015] -- END

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Dim objPeriod As New clscommom_period_Tran
'        Dim objYear As New clsMasterData
'        Dim dsCombo As DataSet = Nothing
'        Dim mstrAssessorIds As String = String.Empty
'        Try
'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objEvaluation.getAssessorComboList("List", True, False, User._Object._Userunkid)
'            dsCombo = objEvaluation.getAssessorComboList(FinancialYear._Object._DatabaseName, _
'                                                         User._Object._Userunkid, _
'                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
'                                                         ConfigParameter._Object._IsIncludeInactiveEmp, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, True, False) 'S.SANDEEP [27 DEC 2016] -- START {clsAssessor.enARVisibilityTypeId.VISIBLE}-- END
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboAssessor
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombo.Tables("List")
'                .SelectedValue = 0
'            End With

'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombo = objYear.getComboListPAYYEAR("Year", True, , , , True)
'            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            With cboYear
'                .ValueMember = "Id"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("Year")
'                .SelectedValue = 0
'            End With

'            'Sohail (21 Aug 2015) -- Start
'            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
'            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0)
'            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
'            'Sohail (21 Aug 2015) -- End
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("Period")
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objPeriod = Nothing : objYear = Nothing : dsCombo.Dispose()
'        End Try
'    End Sub

'    Private Sub SetVisibility()
'        Try
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'btnNew.Enabled = User._Object.Privilege._AddAssessmentAnalysis
'            'btnEdit.Enabled = User._Object.Privilege._EditAssessmentAnalysis
'            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentAnalysis
'            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment

'            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessorEvaluation
'            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessorEvaluation
'            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessorEvaluation
'            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedAssessorEvaluation
'            'S.SANDEEP [28 MAY 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub FillList()
'        Dim dsList As New DataSet
'        Dim StrSearching As String = String.Empty
'        Dim dtTable As DataTable
'        Try
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'If User._Object.Privilege._AllowToViewAssessorAssessmentList = True Then
'            If User._Object.Privilege._AllowtoViewAssessorEvaluationList = True Then
'                'S.SANDEEP [28 MAY 2015] -- END

'                'S.SANDEEP [04 JUN 2015] -- START
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'dsList = objEvaluation.GetList("List", enAssessmentMode.APPRAISER_ASSESSMENT)

'                'If CInt(cboEmployee.SelectedValue) > 0 Then
'                '    StrSearching &= "AND assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
'                'End If

'                'If CInt(cboAssessor.SelectedValue) > 0 Then
'                '    StrSearching &= "AND assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
'                'End If

'                'If CInt(cboYear.SelectedValue) > 0 Then
'                '    StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
'                'End If

'                'If CInt(cboPeriod.SelectedValue) > 0 Then
'                '    StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
'                'End If

'                'If dtpAssessmentdate.Checked = True Then
'                '    StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
'                'End If

'                'If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
'                '    StrSearching &= "AND iscommitted = " & True & " "
'                'ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
'                '    StrSearching &= "AND iscommitted = " & False & " "
'                'End If

'                'If mstrAdvanceFilter.Length > 0 Then
'                '    StrSearching &= "AND " & mstrAdvanceFilter
'                'End If

'                ''S.SANDEEP [04 JUN 2015] -- START
'                'If chkShowMyEmp.Checked = True Then
'                '    StrSearching &= "AND mapuserid = " & User._Object._Userunkid
'                'End If
'                ''S.SANDEEP [04 JUN 2015] -- END


'                'If StrSearching.Length > 0 Then
'                '    StrSearching = StrSearching.Substring(3)
'                '    dtTable = New DataView(dsList.Tables(0), StrSearching, "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable
'                'Else
'                '    dtTable = New DataView(dsList.Tables(0), "", "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable
'                'End If

'                If CInt(cboEmployee.SelectedValue) > 0 Then
'                    StrSearching &= "AND hrevaluation_analysis_master.assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
'                End If

'                If CInt(cboAssessor.SelectedValue) > 0 Then
'                    StrSearching &= "AND hrevaluation_analysis_master.assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
'                End If

'                If CInt(cboYear.SelectedValue) > 0 Then
'                    StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue)
'                End If

'                If CInt(cboPeriod.SelectedValue) > 0 Then
'                    StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue)
'                End If

'                If dtpAssessmentdate.Checked = True Then
'                    StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
'                End If

'                'Sandeep [21 November 2016] -- Start
'                'If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
'                '    StrSearching &= "AND ISNULL(iscommitted,0) = " & True & " "
'                'ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
'                '    StrSearching &= "AND ISNULL(iscommitted,0) = " & False & " "
'                'End If

'                If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
'                    StrSearching &= "AND ISNULL(iscommitted,0) = 1 "
'                ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
'                    StrSearching &= "AND ISNULL(iscommitted,0) = 0 "
'                End If
'                'Anjan [21 November 2016] -- End



'                If mstrAdvanceFilter.Length > 0 Then
'                    StrSearching &= "AND " & mstrAdvanceFilter
'                End If

'                If chkShowMyEmp.Checked = True Then
'                    StrSearching &= "AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
'                End If


'                If StrSearching.Length > 0 Then
'                    StrSearching = StrSearching.Substring(3)
'                End If

'                dsList = objEvaluation.GetList(FinancialYear._Object._DatabaseName, _
'                                               User._Object._Userunkid, _
'                                               FinancialYear._Object._YearUnkid, _
'                                               Company._Object._Companyunkid, _
'                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                               ConfigParameter._Object._UserAccessModeSetting, True, _
'                                               ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
'                                               enAssessmentMode.APPRAISER_ASSESSMENT, StrSearching)

'                dtTable = New DataView(dsList.Tables(0), "", "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable

'                'S.SANDEEP [04 JUN 2015] -- END

'                lvAssessorList.Items.Clear()
'                'S.SANDEEP [04 JUN 2015] -- START
'                'For Each dRow As DataRow In dtTable.Rows
'                '    Dim lvItem As New ListViewItem

'                '    lvItem.Text = dRow.Item("smode").ToString
'                '    lvItem.SubItems.Add(dRow.Item("EmpName").ToString)
'                '    lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("assessmentdate").ToString).ToShortDateString)
'                '    lvItem.SubItems.Add(dRow.Item("PName").ToString)
'                '    lvItem.SubItems(colhAssessmentPeriod.Index).Tag = dRow.Item("Sid")
'                '    lvItem.SubItems.Add(dRow.Item("YearName").ToString)
'                '    lvItem.SubItems.Add(dRow.Item("Assessor").ToString)
'                '    'S.SANDEEP [04 JUN 2015] -- START
'                '    lvItem.SubItems(colhAssessor.Index).Tag = CStr(dRow.Item("userunkid"))  'FOR CHECKING VALID LOGIN
'                '    'S.SANDEEP [04 JUN 2015] -- END


'                '    'S.SANDEEP [ 16 JAN 2015 ] -- START
'                '    'If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
'                '    '    lvItem.SubItems.Add(dRow.Item("wscore").ToString)
'                '    'Else
'                '    '    lvItem.SubItems.Add(dRow.Item("rscore").ToString)
'                '    'End If

'                '    'S.SANDEEP [21 JAN 2015] -- START
'                '    'lvItem.SubItems.Add(dRow.Item("rscore").ToString)
'                '    Dim xTotalScore As Decimal = 0
'                '    Select Case CInt(dRow.Item("smodeid"))
'                '        Case 1  'BSC
'                '            xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, CInt(dRow.Item("assessedemployeeunkid")), CInt(dRow.Item("periodunkid")), , , CInt(dRow.Item("assessormasterunkid")))
'                '        Case 2  'ASSESSMENT GROUP
'                '            xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, CInt(dRow.Item("assessedemployeeunkid")), CInt(dRow.Item("periodunkid")), CInt(dRow.Item("assessgroupunkid")), , CInt(dRow.Item("assessormasterunkid")))
'                '    End Select
'                '    lvItem.SubItems.Add(xTotalScore.ToString("#########0.#0"))
'                '    'S.SANDEEP [21 JAN 2015] -- END

'                '    'S.SANDEEP [ 16 JAN 2015 ] -- END
'                '    lvItem.SubItems.Add(dRow.Item("periodunkid").ToString)
'                '    lvItem.SubItems.Add(dRow.Item("assessedemployeeunkid").ToString)

'                '    If CBool(dRow.Item("iscommitted")) = True Then
'                '        lvItem.ForeColor = Color.Blue
'                'End If

'                '    lvItem.Tag = dRow.Item("analysisunkid")

'                '    lvAssessorList.Items.Add(lvItem)
'                'Next
'                'lvAssessorList.GroupingColumn = colhAssessor
'                'lvAssessorList.DisplayGroups(True)

'                Dim lvItems As ListViewItem() = New ListViewItem(dtTable.Rows.Count - 1) {}
'                For index As Integer = 0 To dtTable.Rows.Count - 1
'                    Dim lvItem As New ListViewItem(dtTable.Rows(index).Item("smode").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("EmpName").ToString)
'                    lvItem.SubItems.Add(eZeeDate.convertDate(dtTable.Rows(index).Item("assessmentdate").ToString).ToShortDateString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("PName").ToString)
'                    lvItem.SubItems(colhAssessmentPeriod.Index).Tag = dtTable.Rows(index).Item("Sid")
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("YearName").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("Assessor").ToString)
'                    lvItem.SubItems(colhAssessor.Index).Tag = CStr(dtTable.Rows(index).Item("mapuserid"))  'FOR CHECKING VALID LOGIN
'                    Dim xTotalScore As Decimal = 0

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'Select Case CInt(dtTable.Rows(index).Item("smodeid"))
'                    '    Case 1  'BSC
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), , , CInt(dtTable.Rows(index).Item("assessormasterunkid")))
'                    '    Case 2  'ASSESSMENT GROUP
'                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), CInt(dtTable.Rows(index).Item("assessgroupunkid")), , CInt(dtTable.Rows(index).Item("assessormasterunkid")))
'                    'End Select

'                    Select Case CInt(dtTable.Rows(index).Item("smodeid"))
'                        Case 1  'BSC

'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), , , CInt(dtTable.Rows(index).Item("assessormasterunkid")), , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
'                            xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                                      IsBalanceScoreCard:=True, _
'                                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
'                                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'                                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                      xEmployeeId:=CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), _
'                                                                      xPeriodId:=CInt(dtTable.Rows(index).Item("periodunkid")), _
'                                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
'                                                                      xAssessorReviewerId:=CInt(dtTable.Rows(index).Item("assessormasterunkid")), _
'                                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
'                            'Shani (23-Nov-2016) -- End
'                        Case 2  'ASSESSMENT GROUP

'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, ConfigParameter._Object._ScoringOptionId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), CInt(dtTable.Rows(index).Item("periodunkid")), CInt(dtTable.Rows(index).Item("assessgroupunkid")), , CInt(dtTable.Rows(index).Item("assessormasterunkid")), , ConfigParameter._Object._Self_Assign_Competencies) 'S.SANDEEP [08 Jan 2016] -- START{ConfigParameter._Object._Self_Assign_Competencies} -- END
'                            xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                                      IsBalanceScoreCard:=False, _
'                                                                      xScoreOptId:=ConfigParameter._Object._ScoringOptionId, _
'                                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'                                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                      xEmployeeId:=CInt(dtTable.Rows(index).Item("assessedemployeeunkid")), _
'                                                                      xPeriodId:=CInt(dtTable.Rows(index).Item("periodunkid")), _
'                                                                      xUsedAgreedScore:=ConfigParameter._Object._IsUseAgreedScore, _
'                                                                      xAssessGrpId:=CInt(dtTable.Rows(index).Item("assessgroupunkid")), _
'                                                                      xAssessorReviewerId:=CInt(dtTable.Rows(index).Item("assessormasterunkid")), _
'                                                                      xSelfAssignedCompetencies:=ConfigParameter._Object._Self_Assign_Competencies)
'                            'Shani (23-Nov-2016) -- End


'                    End Select
'                    'S.SANDEEP [04 JUN 2015] -- END

'                    lvItem.SubItems.Add(xTotalScore.ToString("#########0.#0"))
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("periodunkid").ToString)
'                    lvItem.SubItems.Add(dtTable.Rows(index).Item("assessedemployeeunkid").ToString)

'                    If CBool(dtTable.Rows(index).Item("iscommitted")) = True Then
'                        lvItem.ForeColor = Color.Blue
'                    End If

'                    lvItem.Tag = dtTable.Rows(index).Item("analysisunkid")

'                    lvItems(index) = lvItem
'                Next

'                lvAssessorList.BeginUpdate()
'                lvAssessorList.Items.AddRange(lvItems)

'                lvAssessorList.GroupingColumn = colhAssessor
'                lvAssessorList.DisplayGroups(True)
'                lvAssessorList.EndUpdate()
'                'S.SANDEEP [04 JUN 2015] -- END

'                If lvAssessorList.Items.Count > 4 Then
'                    colhMode.Width = 200 - 20
'                Else
'                    colhMode.Width = 200
'                End If

'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmAssessorEvaluationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objEvaluation = New clsevaluation_analysis_master
'        Try
'            chkShowCommited.CheckState = CheckState.Checked
'            Call Set_Logo(Me, gApplicationType)
'            Call OtherSettings()
'            Call FillCombo()
'            Call SetVisibility()
'            'S.SANDEEP [04 JUN 2015] -- START
'            mblnIsFormLoad = True
'            chkShowUncommited.Checked = True
'            'S.SANDEEP [04 JUN 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmAssessorEvaluationList_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmAssessorEvaluationList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmAssessorEvaluationList_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmAssessorEvaluationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objEvaluation = Nothing
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsevaluation_analysis_master.SetMessages()
'            objfrm._Other_ModuleNames = "clsevaluation_analysis_master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Dim frm As New frmPerformanceEvaluation
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        If lvAssessorList.SelectedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If CInt(lvAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        'S.SANDEEP [04 JUN 2015] -- START
'        If CInt(lvAssessorList.SelectedItems(0).SubItems(colhAssessor.Index).Tag) <> User._Object._Userunkid Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you are not the assessor of the selected employee, Due to this you are not authorized to perform edit operation for this evaluation record."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If
'        'S.SANDEEP [04 JUN 2015] -- END

'        Dim frm As New frmPerformanceEvaluation
'        Try
'            'Shani (24-May-2016) -- Start
'            If objEvaluation.CheckComputionIsExist(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                   enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                   CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                    Dim frmVoid As New frmReasonSelection
'                    Dim sVoidReason As String = String.Empty
'                    If User._Object._Isrighttoleft = True Then
'                        frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
'                        frmVoid.RightToLeftLayout = True
'                        Call Language.ctlRightToLeftlayOut(frmVoid)
'                    End If
'                    frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                    If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                    Dim objComputeMst As New clsComputeScore_master
'                    objComputeMst._Isvoid = True
'                    objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                    objComputeMst._Voidreason = sVoidReason
'                    objComputeMst._Voiduserunkid = User._Object._Userunkid
'                    If objComputeMst.DeleteEmployeeWise(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                        CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                        CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
'                                                        enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
'                    Else
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Else
'                    Exit Sub
'                End If
'            End If
'            'Shani (24-May-2016) -- End

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(CInt(lvAssessorList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If lvAssessorList.SelectedItems.Count <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If

'        If CInt(lvAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
'            Exit Sub
'        End If
'        Try
'            'Shani (24-May-2016) -- Start
'            If objEvaluation.CheckComputionIsExist(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                   enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                   CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                    Dim frmVoid As New frmReasonSelection
'                    Dim sVoidReason As String = String.Empty
'                    If User._Object._Isrighttoleft = True Then
'                        frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
'                        frmVoid.RightToLeftLayout = True
'                        Call Language.ctlRightToLeftlayOut(frmVoid)
'                    End If
'                    frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                    If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                    Dim objComputeMst As New clsComputeScore_master
'                    objComputeMst._Isvoid = True
'                    objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                    objComputeMst._Voidreason = sVoidReason
'                    objComputeMst._Voiduserunkid = User._Object._Userunkid
'                    If objComputeMst.DeleteEmployeeWise(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                        CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                        CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
'                    Else
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Else
'                    Exit Sub
'                End If
'            End If
'            'Shani (24-May-2016) -- End

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                Dim frm As New frmReasonSelection
'                Dim mstrVoidReason As String = String.Empty

'                If User._Object._Isrighttoleft = True Then
'                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                    frm.RightToLeftLayout = True
'                    Call Language.ctlRightToLeftlayOut(frm)
'                End If

'                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
'                If mstrVoidReason.Length <= 0 Then
'                    Exit Sub
'                Else
'                    objEvaluation._Voidreason = mstrVoidReason
'                End If
'                frm = Nothing
'                objEvaluation._Isvoid = True
'                objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                objEvaluation._Voiduserunkid = User._Object._Userunkid

'                objEvaluation.Delete(CInt(lvAssessorList.SelectedItems(0).Tag), enAssessmentMode.APPRAISER_ASSESSMENT, CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))

'                If objEvaluation._Message <> "" Then
'                    eZeeMsgBox.Show(objEvaluation._Message, enMsgBoxStyle.Information)
'                Else
'                    'S.SANDEEP [04 MAR 2015] -- START
'                    'lvAssessorList.SelectedItems(0).Remove()
'                    Call FillList()
'                    'S.SANDEEP [04 MAR 2015] -- END
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.ValueMember = cboEmployee.ValueMember
'            frm.DisplayMember = cboEmployee.DisplayMember
'            frm.CodeMember = "Code"
'            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.ValueMember = cboAssessor.ValueMember
'            frm.DisplayMember = cboAssessor.DisplayMember
'            frm.CodeMember = "Code"
'            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
'            If frm.DisplayDialog Then
'                cboAssessor.SelectedValue = frm.SelectedValue
'                cboAssessor.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Call FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboAssessor.SelectedValue = 0
'            cboEmployee.SelectedValue = 0
'            cboPeriod.SelectedValue = 0
'            cboYear.SelectedValue = 0
'            dtpAssessmentdate.Checked = False
'            chkShowCommited.CheckState = CheckState.Checked
'            mstrAdvanceFilter = String.Empty
'            Call FillList()
'            'S.SANDEEP [04 JUN 2015] -- START
'            chkShowUncommited.CheckState = CheckState.Checked
'            'S.SANDEEP [04 JUN 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
'        Try
'            If lvAssessorList.SelectedItems.Count > 0 Then
'                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                         CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                         CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'                'Shani (24-May-2016) -- Start
'                If objEvaluation.CheckComputionIsExist(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                       enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                       CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then

'                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?"), enMsgBoxStyle.OkCancel) = Windows.Forms.DialogResult.OK Then
'                        Dim frmVoid As New frmReasonSelection
'                        Dim sVoidReason As String = String.Empty
'                        If User._Object._Isrighttoleft = True Then
'                            frmVoid.RightToLeft = Windows.Forms.RightToLeft.Yes
'                            frmVoid.RightToLeftLayout = True
'                            Call Language.ctlRightToLeftlayOut(frmVoid)
'                        End If
'                        frmVoid.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
'                        If sVoidReason.Trim.Length <= 0 Then Exit Sub

'                        Dim objComputeMst As New clsComputeScore_master
'                        objComputeMst._Isvoid = True
'                        objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
'                        objComputeMst._Voidreason = sVoidReason
'                        objComputeMst._Voiduserunkid = User._Object._Userunkid
'                        If objComputeMst.DeleteEmployeeWise(CInt(lvAssessorList.SelectedItems(0).Tag), _
'                                                            CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), _
'                                                            CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Successfully voided Computation score."), enMsgBoxStyle.Information)
'                        Else
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Failed to void Computation score process."), enMsgBoxStyle.Information)
'                            Exit Sub
'                        End If
'                    Else
'                        Exit Sub
'                    End If
'                End If
'                'Shani (24-May-2016) -- End

'                If CInt(lvAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Open Then
'                    If objEvaluation.Unlock_Commit(CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
'                                                 CInt(lvAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
'                        objEvaluation._Analysisunkid = CInt(lvAssessorList.SelectedItems(0).Tag)
'                        objEvaluation._Iscommitted = False
'                        objEvaluation._Committeddatetime = Nothing
'                        objEvaluation.Update()
'                        If objEvaluation._Message <> "" Then
'                            eZeeMsgBox.Show(objEvaluation._Message)
'                        End If
'                        Call FillList()
'                    Else
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Else
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
'        Try
'            Dim frm As New frmAdvanceSearch
'            frm.ShowDialog()
'            mstrAdvanceFilter = frm._GetFilterString
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvAssessorList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAssessorList.ItemChecked
'        Try
'            If lvAssessorList.CheckedItems.Count <= 0 Then Exit Sub
'            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
'            If CInt(e.Item.SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAssessorList_ItemChecked", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub lvAssessorList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAssessorList.SelectedIndexChanged
'        Try
'            If lvAssessorList.SelectedItems.Count <= 0 Then Exit Sub
'            If lvAssessorList.SelectedItems(0).ForeColor = Color.Blue Then
'                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True
'            Else
'                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False
'            End If
'            'S.SANDEEP [28 MAY 2015] -- START
'            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedGeneralAssessment
'            btnUnlockCommit.Enabled = User._Object.Privilege._AllowtoUnlockcommittedAssessorEvaluation
'            'S.SANDEEP [28 MAY 2015] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvAssessorList_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [04 JUN 2015] -- START
'    Private Sub chkShowMyEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowMyEmp.CheckedChanged
'        Try
'            If mblnIsFormLoad = True Then
'                Call FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkShowMyEmp_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [04 JUN 2015] -- END

'#End Region

'#Region " ComboBox Event "

'    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
'        Try
'            Dim objAssessor As New clsevaluation_analysis_master
'            'S.SANDEEP [04 JUN 2015] -- START
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "List", True)
'            Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(FinancialYear._Object._DatabaseName, _
'                                                                         User._Object._Userunkid, _
'                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
'                                                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
'                                                                         CInt(cboAssessor.SelectedValue), "List", True)
'            'S.SANDEEP [04 JUN 2015] -- END

'            cboEmployee.DisplayMember = "NAME"
'            cboEmployee.ValueMember = "Id"
'            cboEmployee.DataSource = dsList.Tables(0)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


'            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
'            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
'            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
'            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
'            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


'            Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor
'            Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor

'            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
'            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

'            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
'            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

'            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
'            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

'            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
'            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.mnuUnlockCommitted.Text = Language._Object.getCaption(Me.mnuUnlockCommitted.Name, Me.mnuUnlockCommitted.Text)
'            Me.mnuGetFileFormat.Text = Language._Object.getCaption(Me.mnuGetFileFormat.Name, Me.mnuGetFileFormat.Text)
'            Me.mnuImportAssessment.Text = Language._Object.getCaption(Me.mnuImportAssessment.Name, Me.mnuImportAssessment.Text)
'            Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)
'            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
'            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
'            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
'            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
'            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
'            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
'            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
'            Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
'            Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
'            Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
'            Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
'            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
'            Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
'            Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
'            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
'            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
'            Me.colhAssessmentPeriod.Text = Language._Object.getCaption(CStr(Me.colhAssessmentPeriod.Tag), Me.colhAssessmentPeriod.Text)
'            Me.colhAssessmentYear.Text = Language._Object.getCaption(CStr(Me.colhAssessmentYear.Tag), Me.colhAssessmentYear.Text)
'            Me.colhAssessor.Text = Language._Object.getCaption(CStr(Me.colhAssessor.Tag), Me.colhAssessor.Text)
'            Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
'            Me.colhMode.Text = Language._Object.getCaption(CStr(Me.colhMode.Tag), Me.colhMode.Text)
'            Me.chkShowMyEmp.Text = Language._Object.getCaption(Me.chkShowMyEmp.Name, Me.chkShowMyEmp.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Please select atleast one Assessment to perform Operation.")
'            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed.")
'            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed.")
'            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
'            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer.")
'            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal.")
'            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed.")
'            Language.setMessage(mstrModuleName, 8, "Sorry, you are not the assessor of the selected employee, Due to this you are not authorized to perform edit operation for this evaluation record.")
'            Language.setMessage(mstrModuleName, 9, "Successfully voided Computation score.")
'            Language.setMessage(mstrModuleName, 10, "Failed to void Computation score process.")
'            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
'            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")
'            Language.setMessage(mstrModuleName, 13, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Compute score process is already done for the selected period. Are you sure you want to Void computation score?")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>
'End Class