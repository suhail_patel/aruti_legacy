﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelfEvaluationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelfEvaluationList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.dgvSelfAssessList = New Aruti.Data.GroupByGrid
        Me.dgcolhemp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhyear = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhperiod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhassessdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhmode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhscore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhperiodid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhstatusid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhempid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhanalysisid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiscommitted = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsmodeid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhassessgroupunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.chkShowUncommited = New System.Windows.Forms.CheckBox
        Me.chkShowCommited = New System.Windows.Forms.CheckBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblAssessmentdate = New System.Windows.Forms.Label
        Me.dtpAssessmentdate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblYears = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblAssessmentPeriods = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuUnlockCommitted = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportSelfAssessment = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnUnlockCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvSelfAssessList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmnuOperation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.dgvSelfAssessList)
        Me.pnlMain.Controls.Add(Me.gbEmployeeInfo)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(726, 471)
        Me.pnlMain.TabIndex = 0
        '
        'dgvSelfAssessList
        '
        Me.dgvSelfAssessList.AllowUserToAddRows = False
        Me.dgvSelfAssessList.AllowUserToDeleteRows = False
        Me.dgvSelfAssessList.AllowUserToResizeRows = False
        Me.dgvSelfAssessList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvSelfAssessList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvSelfAssessList.ColumnHeadersHeight = 30
        Me.dgvSelfAssessList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvSelfAssessList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhemp, Me.dgcolhyear, Me.dgcolhperiod, Me.dgcolhassessdate, Me.dgcolhmode, Me.dgcolhscore, Me.objdgcolhperiodid, Me.objdgcolhstatusid, Me.objdgcolhempid, Me.objdgcolhanalysisid, Me.objdgcolhiscommitted, Me.objdgcolhsmodeid, Me.objdgcolhassessgroupunkid})
        Me.dgvSelfAssessList.IgnoreFirstColumn = False
        Me.dgvSelfAssessList.Location = New System.Drawing.Point(12, 161)
        Me.dgvSelfAssessList.MultiSelect = False
        Me.dgvSelfAssessList.Name = "dgvSelfAssessList"
        Me.dgvSelfAssessList.ReadOnly = True
        Me.dgvSelfAssessList.RowHeadersVisible = False
        Me.dgvSelfAssessList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSelfAssessList.Size = New System.Drawing.Size(703, 249)
        Me.dgvSelfAssessList.TabIndex = 119
        '
        'dgcolhemp
        '
        Me.dgcolhemp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhemp.HeaderText = "Employee"
        Me.dgcolhemp.Name = "dgcolhemp"
        Me.dgcolhemp.ReadOnly = True
        Me.dgcolhemp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhyear
        '
        Me.dgcolhyear.HeaderText = "Year"
        Me.dgcolhyear.Name = "dgcolhyear"
        Me.dgcolhyear.ReadOnly = True
        Me.dgcolhyear.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhyear.Width = 80
        '
        'dgcolhperiod
        '
        Me.dgcolhperiod.HeaderText = "Period"
        Me.dgcolhperiod.Name = "dgcolhperiod"
        Me.dgcolhperiod.ReadOnly = True
        Me.dgcolhperiod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhassessdate
        '
        Me.dgcolhassessdate.HeaderText = "Assess. Date"
        Me.dgcolhassessdate.Name = "dgcolhassessdate"
        Me.dgcolhassessdate.ReadOnly = True
        Me.dgcolhassessdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhassessdate.Width = 90
        '
        'dgcolhmode
        '
        Me.dgcolhmode.HeaderText = "Mode"
        Me.dgcolhmode.Name = "dgcolhmode"
        Me.dgcolhmode.ReadOnly = True
        Me.dgcolhmode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhmode.Width = 180
        '
        'dgcolhscore
        '
        Me.dgcolhscore.HeaderText = "Score"
        Me.dgcolhscore.Name = "dgcolhscore"
        Me.dgcolhscore.ReadOnly = True
        Me.dgcolhscore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhscore.Width = 50
        '
        'objdgcolhperiodid
        '
        Me.objdgcolhperiodid.HeaderText = "objdgcolhperiodid"
        Me.objdgcolhperiodid.Name = "objdgcolhperiodid"
        Me.objdgcolhperiodid.ReadOnly = True
        Me.objdgcolhperiodid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhperiodid.Visible = False
        '
        'objdgcolhstatusid
        '
        Me.objdgcolhstatusid.HeaderText = "objdgcolhstatusid"
        Me.objdgcolhstatusid.Name = "objdgcolhstatusid"
        Me.objdgcolhstatusid.ReadOnly = True
        Me.objdgcolhstatusid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhstatusid.Visible = False
        '
        'objdgcolhempid
        '
        Me.objdgcolhempid.HeaderText = "objdgcolhempid"
        Me.objdgcolhempid.Name = "objdgcolhempid"
        Me.objdgcolhempid.ReadOnly = True
        Me.objdgcolhempid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhempid.Visible = False
        '
        'objdgcolhanalysisid
        '
        Me.objdgcolhanalysisid.HeaderText = "objdgcolhanalysisid"
        Me.objdgcolhanalysisid.Name = "objdgcolhanalysisid"
        Me.objdgcolhanalysisid.ReadOnly = True
        Me.objdgcolhanalysisid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhanalysisid.Visible = False
        '
        'objdgcolhiscommitted
        '
        Me.objdgcolhiscommitted.HeaderText = "objdgcolhiscommitted"
        Me.objdgcolhiscommitted.Name = "objdgcolhiscommitted"
        Me.objdgcolhiscommitted.ReadOnly = True
        Me.objdgcolhiscommitted.Visible = False
        '
        'objdgcolhsmodeid
        '
        Me.objdgcolhsmodeid.HeaderText = "objdgcolhsmodeid"
        Me.objdgcolhsmodeid.Name = "objdgcolhsmodeid"
        Me.objdgcolhsmodeid.ReadOnly = True
        Me.objdgcolhsmodeid.Visible = False
        '
        'objdgcolhassessgroupunkid
        '
        Me.objdgcolhassessgroupunkid.HeaderText = "objdgcolhassessgroupunkid"
        Me.objdgcolhassessgroupunkid.Name = "objdgcolhassessgroupunkid"
        Me.objdgcolhassessgroupunkid.ReadOnly = True
        Me.objdgcolhassessgroupunkid.Visible = False
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.objStLine1)
        Me.gbEmployeeInfo.Controls.Add(Me.chkShowUncommited)
        Me.gbEmployeeInfo.Controls.Add(Me.chkShowCommited)
        Me.gbEmployeeInfo.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeInfo.Controls.Add(Me.lblAssessmentdate)
        Me.gbEmployeeInfo.Controls.Add(Me.dtpAssessmentdate)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboYear)
        Me.gbEmployeeInfo.Controls.Add(Me.lblYears)
        Me.gbEmployeeInfo.Controls.Add(Me.cboPeriod)
        Me.gbEmployeeInfo.Controls.Add(Me.lblAssessmentPeriods)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(12, 64)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(703, 91)
        Me.gbEmployeeInfo.TabIndex = 117
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Filter Criteria"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(503, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(8, 66)
        Me.objStLine1.TabIndex = 246
        Me.objStLine1.Text = "EZeeStraightLine2"
        '
        'chkShowUncommited
        '
        Me.chkShowUncommited.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkShowUncommited.Location = New System.Drawing.Point(517, 62)
        Me.chkShowUncommited.Name = "chkShowUncommited"
        Me.chkShowUncommited.Size = New System.Drawing.Size(141, 17)
        Me.chkShowUncommited.TabIndex = 240
        Me.chkShowUncommited.Text = "Show Uncommited"
        Me.chkShowUncommited.UseVisualStyleBackColor = True
        '
        'chkShowCommited
        '
        Me.chkShowCommited.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkShowCommited.Location = New System.Drawing.Point(517, 35)
        Me.chkShowCommited.Name = "chkShowCommited"
        Me.chkShowCommited.Size = New System.Drawing.Size(141, 17)
        Me.chkShowCommited.TabIndex = 239
        Me.chkShowCommited.Text = "Show Commited"
        Me.chkShowCommited.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(535, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(111, 17)
        Me.lnkAllocation.TabIndex = 242
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(677, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(652, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'lblAssessmentdate
        '
        Me.lblAssessmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentdate.Location = New System.Drawing.Point(314, 63)
        Me.lblAssessmentdate.Name = "lblAssessmentdate"
        Me.lblAssessmentdate.Size = New System.Drawing.Size(41, 15)
        Me.lblAssessmentdate.TabIndex = 231
        Me.lblAssessmentdate.Text = "Date"
        Me.lblAssessmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAssessmentdate
        '
        Me.dtpAssessmentdate.Checked = False
        Me.dtpAssessmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessmentdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessmentdate.Location = New System.Drawing.Point(361, 60)
        Me.dtpAssessmentdate.Name = "dtpAssessmentdate"
        Me.dtpAssessmentdate.ShowCheckBox = True
        Me.dtpAssessmentdate.Size = New System.Drawing.Size(136, 21)
        Me.dtpAssessmentdate.TabIndex = 232
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(287, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 215
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(79, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(202, 21)
        Me.cboEmployee.TabIndex = 229
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(65, 15)
        Me.lblEmployee.TabIndex = 227
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(361, 33)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(136, 21)
        Me.cboYear.TabIndex = 7
        '
        'lblYears
        '
        Me.lblYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYears.Location = New System.Drawing.Point(314, 36)
        Me.lblYears.Name = "lblYears"
        Me.lblYears.Size = New System.Drawing.Size(41, 15)
        Me.lblYears.TabIndex = 225
        Me.lblYears.Text = "Year"
        Me.lblYears.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(79, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(202, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'lblAssessmentPeriods
        '
        Me.lblAssessmentPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentPeriods.Location = New System.Drawing.Point(8, 63)
        Me.lblAssessmentPeriods.Name = "lblAssessmentPeriods"
        Me.lblAssessmentPeriods.Size = New System.Drawing.Size(65, 15)
        Me.lblAssessmentPeriods.TabIndex = 223
        Me.lblAssessmentPeriods.Text = "Period"
        Me.lblAssessmentPeriods.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(726, 58)
        Me.eZeeHeader.TabIndex = 116
        Me.eZeeHeader.Title = "Self Evaluation List"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUnlockCommitted, Me.mnuGetFileFormat, Me.mnuImportSelfAssessment})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(198, 70)
        '
        'mnuUnlockCommitted
        '
        Me.mnuUnlockCommitted.Name = "mnuUnlockCommitted"
        Me.mnuUnlockCommitted.Size = New System.Drawing.Size(197, 22)
        Me.mnuUnlockCommitted.Tag = "mnuUnlockCommitted"
        Me.mnuUnlockCommitted.Text = "&Unlock Committed"
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(197, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "&Get File Format"
        '
        'mnuImportSelfAssessment
        '
        Me.mnuImportSelfAssessment.Name = "mnuImportSelfAssessment"
        Me.mnuImportSelfAssessment.Size = New System.Drawing.Size(197, 22)
        Me.mnuImportSelfAssessment.Tag = "mnuImportSelfAssessment"
        Me.mnuImportSelfAssessment.Text = "&Import Self Assessment"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnUnlockCommit)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 416)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(726, 55)
        Me.objFooter.TabIndex = 117
        '
        'btnUnlockCommit
        '
        Me.btnUnlockCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlockCommit.BackColor = System.Drawing.Color.White
        Me.btnUnlockCommit.BackgroundImage = CType(resources.GetObject("btnUnlockCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlockCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlockCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlockCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUnlockCommit.FlatAppearance.BorderSize = 0
        Me.btnUnlockCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlockCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlockCommit.ForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlockCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.Location = New System.Drawing.Point(11, 13)
        Me.btnUnlockCommit.Name = "btnUnlockCommit"
        Me.btnUnlockCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockCommit.Size = New System.Drawing.Size(118, 30)
        Me.btnUnlockCommit.TabIndex = 76
        Me.btnUnlockCommit.Text = "&Unlock Commited"
        Me.btnUnlockCommit.UseVisualStyleBackColor = True
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(11, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(102, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 117
        Me.btnOperation.Text = "&Operation"
        Me.btnOperation.Visible = False
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(331, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(525, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(428, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(621, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmSelfEvaluationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 471)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSelfEvaluationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Self Performance Evaluation"
        Me.pnlMain.ResumeLayout(False)
        CType(Me.dgvSelfAssessList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeInfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmnuOperation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuUnlockCommitted As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportSelfAssessment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnUnlockCommit As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowUncommited As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCommited As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblAssessmentdate As System.Windows.Forms.Label
    Friend WithEvents dtpAssessmentdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYears As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentPeriods As System.Windows.Forms.Label
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dgvSelfAssessList As Aruti.Data.GroupByGrid
    Friend WithEvents dgcolhemp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhyear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhperiod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhassessdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhmode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhscore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhperiodid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhstatusid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhempid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhanalysisid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiscommitted As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhsmodeid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhassessgroupunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
