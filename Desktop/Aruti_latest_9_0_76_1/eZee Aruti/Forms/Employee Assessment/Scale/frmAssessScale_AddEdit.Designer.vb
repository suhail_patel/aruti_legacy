﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessScale_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessScale_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbScaleInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchCategory = New eZee.Common.eZeeGradientButton
        Me.cboScaleGroup = New System.Windows.Forms.ComboBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtScale = New eZee.TextBox.IntegerTextBox
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblScale = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblTotalScoreT = New System.Windows.Forms.Label
        Me.lblTotalPercF = New System.Windows.Forms.Label
        Me.txtScoreTo = New eZee.TextBox.NumericTextBox
        Me.txtScoreFrom = New eZee.TextBox.NumericTextBox
        Me.lblLastRatingDefined = New System.Windows.Forms.Label
        Me.objlblLastRating = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.gbScaleInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbScaleInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(443, 235)
        Me.pnlMain.TabIndex = 0
        '
        'gbScaleInfo
        '
        Me.gbScaleInfo.BorderColor = System.Drawing.Color.Black
        Me.gbScaleInfo.Checked = False
        Me.gbScaleInfo.CollapseAllExceptThis = False
        Me.gbScaleInfo.CollapsedHoverImage = Nothing
        Me.gbScaleInfo.CollapsedNormalImage = Nothing
        Me.gbScaleInfo.CollapsedPressedImage = Nothing
        Me.gbScaleInfo.CollapseOnLoad = False
        Me.gbScaleInfo.Controls.Add(Me.lblTotalScoreT)
        Me.gbScaleInfo.Controls.Add(Me.lblTotalPercF)
        Me.gbScaleInfo.Controls.Add(Me.txtScoreTo)
        Me.gbScaleInfo.Controls.Add(Me.txtScoreFrom)
        Me.gbScaleInfo.Controls.Add(Me.objbtnAddCategory)
        Me.gbScaleInfo.Controls.Add(Me.lblPeriod)
        Me.gbScaleInfo.Controls.Add(Me.cboPeriod)
        Me.gbScaleInfo.Controls.Add(Me.objbtnSearchCategory)
        Me.gbScaleInfo.Controls.Add(Me.cboScaleGroup)
        Me.gbScaleInfo.Controls.Add(Me.lblName)
        Me.gbScaleInfo.Controls.Add(Me.lblDescription)
        Me.gbScaleInfo.Controls.Add(Me.txtScale)
        Me.gbScaleInfo.Controls.Add(Me.txtDescription)
        Me.gbScaleInfo.Controls.Add(Me.lblScale)
        Me.gbScaleInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbScaleInfo.ExpandedHoverImage = Nothing
        Me.gbScaleInfo.ExpandedNormalImage = Nothing
        Me.gbScaleInfo.ExpandedPressedImage = Nothing
        Me.gbScaleInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbScaleInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbScaleInfo.HeaderHeight = 25
        Me.gbScaleInfo.HeaderMessage = ""
        Me.gbScaleInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbScaleInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbScaleInfo.HeightOnCollapse = 0
        Me.gbScaleInfo.LeftTextSpace = 0
        Me.gbScaleInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbScaleInfo.Name = "gbScaleInfo"
        Me.gbScaleInfo.OpenHeight = 300
        Me.gbScaleInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbScaleInfo.ShowBorder = True
        Me.gbScaleInfo.ShowCheckBox = False
        Me.gbScaleInfo.ShowCollapseButton = False
        Me.gbScaleInfo.ShowDefaultBorderColor = True
        Me.gbScaleInfo.ShowDownButton = False
        Me.gbScaleInfo.ShowHeader = True
        Me.gbScaleInfo.Size = New System.Drawing.Size(443, 180)
        Me.gbScaleInfo.TabIndex = 0
        Me.gbScaleInfo.Temp = 0
        Me.gbScaleInfo.Text = "Scale/Score Information"
        Me.gbScaleInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(311, 62)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 10
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(71, 16)
        Me.lblPeriod.TabIndex = 0
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(89, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(342, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'objbtnSearchCategory
        '
        Me.objbtnSearchCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCategory.BorderSelected = False
        Me.objbtnSearchCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCategory.Location = New System.Drawing.Point(284, 62)
        Me.objbtnSearchCategory.Name = "objbtnSearchCategory"
        Me.objbtnSearchCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCategory.TabIndex = 4
        '
        'cboScaleGroup
        '
        Me.cboScaleGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScaleGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScaleGroup.FormattingEnabled = True
        Me.cboScaleGroup.Location = New System.Drawing.Point(89, 62)
        Me.cboScaleGroup.Name = "cboScaleGroup"
        Me.cboScaleGroup.Size = New System.Drawing.Size(189, 21)
        Me.cboScaleGroup.TabIndex = 3
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(12, 64)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(71, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Scale Group"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(12, 92)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(71, 16)
        Me.lblDescription.TabIndex = 7
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScale
        '
        Me.txtScale.AllowNegative = True
        Me.txtScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScale.DigitsInGroup = 0
        Me.txtScale.Flags = 0
        Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScale.Location = New System.Drawing.Point(385, 62)
        Me.txtScale.MaxDecimalPlaces = 2
        Me.txtScale.MaxWholeDigits = 9
        Me.txtScale.Name = "txtScale"
        Me.txtScale.Prefix = ""
        Me.txtScale.RangeMax = 2147483647
        Me.txtScale.RangeMin = -2147483648
        Me.txtScale.Size = New System.Drawing.Size(46, 21)
        Me.txtScale.TabIndex = 6
        Me.txtScale.Text = "0"
        Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.Color.White
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(89, 89)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(342, 55)
        Me.txtDescription.TabIndex = 8
        Me.txtDescription.TabStop = False
        '
        'lblScale
        '
        Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScale.Location = New System.Drawing.Point(338, 64)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(41, 16)
        Me.lblScale.TabIndex = 5
        Me.lblScale.Text = "Value"
        Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblLastRating)
        Me.objFooter.Controls.Add(Me.lblLastRatingDefined)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 180)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(443, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(337, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(237, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblTotalScoreT
        '
        Me.lblTotalScoreT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalScoreT.Location = New System.Drawing.Point(158, 152)
        Me.lblTotalScoreT.Name = "lblTotalScoreT"
        Me.lblTotalScoreT.Size = New System.Drawing.Size(49, 17)
        Me.lblTotalScoreT.TabIndex = 15
        Me.lblTotalScoreT.Text = "To"
        Me.lblTotalScoreT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalPercF
        '
        Me.lblTotalPercF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPercF.Location = New System.Drawing.Point(12, 152)
        Me.lblTotalPercF.Name = "lblTotalPercF"
        Me.lblTotalPercF.Size = New System.Drawing.Size(71, 17)
        Me.lblTotalPercF.TabIndex = 14
        Me.lblTotalPercF.Text = "(%) From"
        Me.lblTotalPercF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScoreTo
        '
        Me.txtScoreTo.AllowNegative = True
        Me.txtScoreTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreTo.DigitsInGroup = 0
        Me.txtScoreTo.Flags = 0
        Me.txtScoreTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreTo.Location = New System.Drawing.Point(213, 150)
        Me.txtScoreTo.MaxDecimalPlaces = 4
        Me.txtScoreTo.MaxWholeDigits = 9
        Me.txtScoreTo.Name = "txtScoreTo"
        Me.txtScoreTo.Prefix = ""
        Me.txtScoreTo.RangeMax = 1.7976931348623157E+308
        Me.txtScoreTo.RangeMin = -1.7976931348623157E+308
        Me.txtScoreTo.Size = New System.Drawing.Size(63, 21)
        Me.txtScoreTo.TabIndex = 13
        Me.txtScoreTo.Text = "0"
        Me.txtScoreTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtScoreFrom
        '
        Me.txtScoreFrom.AllowNegative = True
        Me.txtScoreFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreFrom.DigitsInGroup = 0
        Me.txtScoreFrom.Flags = 0
        Me.txtScoreFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreFrom.Location = New System.Drawing.Point(89, 150)
        Me.txtScoreFrom.MaxDecimalPlaces = 4
        Me.txtScoreFrom.MaxWholeDigits = 9
        Me.txtScoreFrom.Name = "txtScoreFrom"
        Me.txtScoreFrom.Prefix = ""
        Me.txtScoreFrom.RangeMax = 1.7976931348623157E+308
        Me.txtScoreFrom.RangeMin = -1.7976931348623157E+308
        Me.txtScoreFrom.Size = New System.Drawing.Size(63, 21)
        Me.txtScoreFrom.TabIndex = 12
        Me.txtScoreFrom.Text = "0"
        Me.txtScoreFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLastRatingDefined
        '
        Me.lblLastRatingDefined.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastRatingDefined.Location = New System.Drawing.Point(15, 20)
        Me.lblLastRatingDefined.Name = "lblLastRatingDefined"
        Me.lblLastRatingDefined.Size = New System.Drawing.Size(107, 16)
        Me.lblLastRatingDefined.TabIndex = 2
        Me.lblLastRatingDefined.Text = "Last (%) Rating"
        '
        'objlblLastRating
        '
        Me.objlblLastRating.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLastRating.Location = New System.Drawing.Point(128, 20)
        Me.objlblLastRating.Name = "objlblLastRating"
        Me.objlblLastRating.Size = New System.Drawing.Size(103, 16)
        Me.objlblLastRating.TabIndex = 3
        Me.objlblLastRating.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmAssessScale_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(443, 235)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessScale_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Scale/Score Add/Edit"
        Me.pnlMain.ResumeLayout(False)
        Me.gbScaleInfo.ResumeLayout(False)
        Me.gbScaleInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbScaleInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtScale As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents cboScaleGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTotalScoreT As System.Windows.Forms.Label
    Friend WithEvents lblTotalPercF As System.Windows.Forms.Label
    Friend WithEvents txtScoreTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtScoreFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents objlblLastRating As System.Windows.Forms.Label
    Friend WithEvents lblLastRatingDefined As System.Windows.Forms.Label
End Class
