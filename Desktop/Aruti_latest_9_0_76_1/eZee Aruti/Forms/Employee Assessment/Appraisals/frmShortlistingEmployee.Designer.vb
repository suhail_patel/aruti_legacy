﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShortlistingEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShortlistingEmployee))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.tblpanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.fpnlFlow = New System.Windows.Forms.FlowLayoutPanel
        Me.gbPeriod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.lblFinalResult = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.cboFinalResult = New System.Windows.Forms.ComboBox
        Me.lblAppointment = New System.Windows.Forms.Label
        Me.cboAppointment = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.gbOverAllScore = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOverallScore = New eZee.TextBox.NumericTextBox
        Me.lblOverallScore = New System.Windows.Forms.Label
        Me.cboOverallCondition = New System.Windows.Forms.ComboBox
        Me.lblOverallCondition = New System.Windows.Forms.Label
        Me.gbSummaryFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboGETotalCondition = New System.Windows.Forms.ComboBox
        Me.gbSummOperation = New System.Windows.Forms.GroupBox
        Me.objbtnSummaryReset = New eZee.Common.eZeeGradientButton
        Me.radSummaryAND = New System.Windows.Forms.RadioButton
        Me.radSummaryOR = New System.Windows.Forms.RadioButton
        Me.elGeneralEvaluation = New eZee.Common.eZeeLine
        Me.txtGETotalScore = New eZee.TextBox.NumericTextBox
        Me.txtBSCTotalScore = New eZee.TextBox.NumericTextBox
        Me.lblGETotalCondition = New System.Windows.Forms.Label
        Me.lblBSCTotalScore = New System.Windows.Forms.Label
        Me.elBalanceScorecard = New eZee.Common.eZeeLine
        Me.lblGETotalScore = New System.Windows.Forms.Label
        Me.cboBSCTotalCondition = New System.Windows.Forms.ComboBox
        Me.lblBSCTotalCondition = New System.Windows.Forms.Label
        Me.gbDetailFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlDetailCondition = New System.Windows.Forms.Panel
        Me.objbtnResetPerspective = New eZee.Common.eZeeGradientButton
        Me.radDetailAND = New System.Windows.Forms.RadioButton
        Me.radDetailOR = New System.Windows.Forms.RadioButton
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhItems = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCondition = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhItemId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lvEmployee = New eZee.Common.eZeeListView(Me.components)
        Me.colhEcode = New System.Windows.Forms.ColumnHeader
        Me.colhEName = New System.Windows.Forms.ColumnHeader
        Me.colhAppDate = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.objcolhFRefId = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhOverallScore = New System.Windows.Forms.ColumnHeader
        Me.lvFilterCriteria = New eZee.Common.eZeeListView(Me.components)
        Me.colhFieldName = New System.Windows.Forms.ColumnHeader
        Me.colhValue = New System.Windows.Forms.ColumnHeader
        Me.colhCondition = New System.Windows.Forms.ColumnHeader
        Me.objcolhFilterId = New System.Windows.Forms.ColumnHeader
        Me.objcolhConId = New System.Windows.Forms.ColumnHeader
        Me.objcolhOprId = New System.Windows.Forms.ColumnHeader
        Me.objcolhGuid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGrpName = New System.Windows.Forms.ColumnHeader
        Me.colhOperation = New System.Windows.Forms.ColumnHeader
        Me.objcolhColTags = New System.Windows.Forms.ColumnHeader
        Me.gbRemarks = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlRemark = New System.Windows.Forms.Panel
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.btnRemove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApplyFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.tblpanel1.SuspendLayout()
        Me.fpnlFlow.SuspendLayout()
        Me.gbPeriod.SuspendLayout()
        Me.gbOverAllScore.SuspendLayout()
        Me.gbSummaryFilter.SuspendLayout()
        Me.gbSummOperation.SuspendLayout()
        Me.gbDetailFilter.SuspendLayout()
        Me.pnlDetailCondition.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRemarks.SuspendLayout()
        Me.pnlRemark.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.tblpanel1)
        Me.pnlMain.Controls.Add(Me.lvEmployee)
        Me.pnlMain.Controls.Add(Me.lvFilterCriteria)
        Me.pnlMain.Controls.Add(Me.gbRemarks)
        Me.pnlMain.Controls.Add(Me.btnRemove)
        Me.pnlMain.Controls.Add(Me.btnAdd)
        Me.pnlMain.Controls.Add(Me.btnApplyFilter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1034, 574)
        Me.pnlMain.TabIndex = 0
        '
        'tblpanel1
        '
        Me.tblpanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tblpanel1.ColumnCount = 1
        Me.tblpanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblpanel1.Controls.Add(Me.fpnlFlow, 0, 0)
        Me.tblpanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.tblpanel1.Location = New System.Drawing.Point(0, 0)
        Me.tblpanel1.Name = "tblpanel1"
        Me.tblpanel1.RowCount = 1
        Me.tblpanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblpanel1.Size = New System.Drawing.Size(386, 574)
        Me.tblpanel1.TabIndex = 7
        '
        'fpnlFlow
        '
        Me.fpnlFlow.AutoScroll = True
        Me.fpnlFlow.BackColor = System.Drawing.SystemColors.Control
        Me.fpnlFlow.Controls.Add(Me.gbPeriod)
        Me.fpnlFlow.Controls.Add(Me.gbOverAllScore)
        Me.fpnlFlow.Controls.Add(Me.gbSummaryFilter)
        Me.fpnlFlow.Controls.Add(Me.gbDetailFilter)
        Me.fpnlFlow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.fpnlFlow.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.fpnlFlow.Location = New System.Drawing.Point(4, 4)
        Me.fpnlFlow.Name = "fpnlFlow"
        Me.fpnlFlow.Size = New System.Drawing.Size(378, 566)
        Me.fpnlFlow.TabIndex = 7
        Me.fpnlFlow.WrapContents = False
        '
        'gbPeriod
        '
        Me.gbPeriod.BorderColor = System.Drawing.Color.Black
        Me.gbPeriod.Checked = False
        Me.gbPeriod.CollapseAllExceptThis = False
        Me.gbPeriod.CollapsedHoverImage = Nothing
        Me.gbPeriod.CollapsedNormalImage = Nothing
        Me.gbPeriod.CollapsedPressedImage = Nothing
        Me.gbPeriod.CollapseOnLoad = False
        Me.gbPeriod.Controls.Add(Me.objlblCaption)
        Me.gbPeriod.Controls.Add(Me.lblFinalResult)
        Me.gbPeriod.Controls.Add(Me.dtpDate2)
        Me.gbPeriod.Controls.Add(Me.lblTo)
        Me.gbPeriod.Controls.Add(Me.dtpDate1)
        Me.gbPeriod.Controls.Add(Me.cboFinalResult)
        Me.gbPeriod.Controls.Add(Me.lblAppointment)
        Me.gbPeriod.Controls.Add(Me.cboAppointment)
        Me.gbPeriod.Controls.Add(Me.cboPeriod)
        Me.gbPeriod.Controls.Add(Me.lblPeriod)
        Me.gbPeriod.ExpandedHoverImage = Nothing
        Me.gbPeriod.ExpandedNormalImage = Nothing
        Me.gbPeriod.ExpandedPressedImage = Nothing
        Me.gbPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPeriod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPeriod.HeaderHeight = 25
        Me.gbPeriod.HeaderMessage = ""
        Me.gbPeriod.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPeriod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPeriod.HeightOnCollapse = 0
        Me.gbPeriod.LeftTextSpace = 0
        Me.gbPeriod.Location = New System.Drawing.Point(3, 3)
        Me.gbPeriod.Name = "gbPeriod"
        Me.gbPeriod.OpenHeight = 300
        Me.gbPeriod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPeriod.ShowBorder = True
        Me.gbPeriod.ShowCheckBox = False
        Me.gbPeriod.ShowCollapseButton = False
        Me.gbPeriod.ShowDefaultBorderColor = True
        Me.gbPeriod.ShowDownButton = False
        Me.gbPeriod.ShowHeader = True
        Me.gbPeriod.Size = New System.Drawing.Size(354, 142)
        Me.gbPeriod.TabIndex = 6
        Me.gbPeriod.Temp = 0
        Me.gbPeriod.Text = "Assessment Period"
        Me.gbPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(8, 116)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(90, 16)
        Me.objlblCaption.TabIndex = 443
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFinalResult
        '
        Me.lblFinalResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinalResult.Location = New System.Drawing.Point(7, 62)
        Me.lblFinalResult.Name = "lblFinalResult"
        Me.lblFinalResult.Size = New System.Drawing.Size(90, 16)
        Me.lblFinalResult.TabIndex = 446
        Me.lblFinalResult.Text = "Score Based On"
        Me.lblFinalResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.Checked = False
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(243, 114)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.ShowCheckBox = True
        Me.dtpDate2.Size = New System.Drawing.Size(103, 21)
        Me.dtpDate2.TabIndex = 442
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(213, 116)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(24, 16)
        Me.lblTo.TabIndex = 441
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.Checked = False
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(104, 114)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.ShowCheckBox = True
        Me.dtpDate1.Size = New System.Drawing.Size(103, 21)
        Me.dtpDate1.TabIndex = 440
        '
        'cboFinalResult
        '
        Me.cboFinalResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFinalResult.DropDownWidth = 200
        Me.cboFinalResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFinalResult.FormattingEnabled = True
        Me.cboFinalResult.Location = New System.Drawing.Point(103, 60)
        Me.cboFinalResult.Name = "cboFinalResult"
        Me.cboFinalResult.Size = New System.Drawing.Size(243, 21)
        Me.cboFinalResult.TabIndex = 445
        '
        'lblAppointment
        '
        Me.lblAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointment.Location = New System.Drawing.Point(8, 89)
        Me.lblAppointment.Name = "lblAppointment"
        Me.lblAppointment.Size = New System.Drawing.Size(90, 16)
        Me.lblAppointment.TabIndex = 439
        Me.lblAppointment.Text = "Appointment"
        Me.lblAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAppointment
        '
        Me.cboAppointment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppointment.FormattingEnabled = True
        Me.cboAppointment.Location = New System.Drawing.Point(103, 87)
        Me.cboAppointment.Name = "cboAppointment"
        Me.cboAppointment.Size = New System.Drawing.Size(243, 21)
        Me.cboAppointment.TabIndex = 438
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(103, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(243, 21)
        Me.cboPeriod.TabIndex = 2
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(90, 16)
        Me.lblPeriod.TabIndex = 1
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbOverAllScore
        '
        Me.gbOverAllScore.BorderColor = System.Drawing.Color.Black
        Me.gbOverAllScore.Checked = False
        Me.gbOverAllScore.CollapseAllExceptThis = False
        Me.gbOverAllScore.CollapsedHoverImage = Nothing
        Me.gbOverAllScore.CollapsedNormalImage = Nothing
        Me.gbOverAllScore.CollapsedPressedImage = Nothing
        Me.gbOverAllScore.CollapseOnLoad = False
        Me.gbOverAllScore.Controls.Add(Me.txtOverallScore)
        Me.gbOverAllScore.Controls.Add(Me.lblOverallScore)
        Me.gbOverAllScore.Controls.Add(Me.cboOverallCondition)
        Me.gbOverAllScore.Controls.Add(Me.lblOverallCondition)
        Me.gbOverAllScore.ExpandedHoverImage = Nothing
        Me.gbOverAllScore.ExpandedNormalImage = Nothing
        Me.gbOverAllScore.ExpandedPressedImage = Nothing
        Me.gbOverAllScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOverAllScore.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOverAllScore.HeaderHeight = 25
        Me.gbOverAllScore.HeaderMessage = ""
        Me.gbOverAllScore.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOverAllScore.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOverAllScore.HeightOnCollapse = 0
        Me.gbOverAllScore.LeftTextSpace = 0
        Me.gbOverAllScore.Location = New System.Drawing.Point(3, 151)
        Me.gbOverAllScore.Name = "gbOverAllScore"
        Me.gbOverAllScore.OpenHeight = 300
        Me.gbOverAllScore.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOverAllScore.ShowBorder = True
        Me.gbOverAllScore.ShowCheckBox = True
        Me.gbOverAllScore.ShowCollapseButton = False
        Me.gbOverAllScore.ShowDefaultBorderColor = True
        Me.gbOverAllScore.ShowDownButton = False
        Me.gbOverAllScore.ShowHeader = True
        Me.gbOverAllScore.Size = New System.Drawing.Size(354, 61)
        Me.gbOverAllScore.TabIndex = 7
        Me.gbOverAllScore.Temp = 0
        Me.gbOverAllScore.Text = "Overall Score"
        Me.gbOverAllScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOverallScore
        '
        Me.txtOverallScore.AllowNegative = False
        Me.txtOverallScore.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOverallScore.DigitsInGroup = 0
        Me.txtOverallScore.Flags = 65536
        Me.txtOverallScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOverallScore.Location = New System.Drawing.Point(103, 32)
        Me.txtOverallScore.MaxDecimalPlaces = 4
        Me.txtOverallScore.MaxWholeDigits = 9
        Me.txtOverallScore.Name = "txtOverallScore"
        Me.txtOverallScore.Prefix = ""
        Me.txtOverallScore.RangeMax = 1.7976931348623157E+308
        Me.txtOverallScore.RangeMin = -1.7976931348623157E+308
        Me.txtOverallScore.Size = New System.Drawing.Size(76, 21)
        Me.txtOverallScore.TabIndex = 9
        Me.txtOverallScore.Text = "0"
        Me.txtOverallScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOverallScore
        '
        Me.lblOverallScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverallScore.Location = New System.Drawing.Point(8, 34)
        Me.lblOverallScore.Name = "lblOverallScore"
        Me.lblOverallScore.Size = New System.Drawing.Size(89, 16)
        Me.lblOverallScore.TabIndex = 8
        Me.lblOverallScore.Text = "Overall Score"
        Me.lblOverallScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOverallCondition
        '
        Me.cboOverallCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOverallCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOverallCondition.FormattingEnabled = True
        Me.cboOverallCondition.Location = New System.Drawing.Point(255, 32)
        Me.cboOverallCondition.Name = "cboOverallCondition"
        Me.cboOverallCondition.Size = New System.Drawing.Size(91, 21)
        Me.cboOverallCondition.TabIndex = 6
        '
        'lblOverallCondition
        '
        Me.lblOverallCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOverallCondition.Location = New System.Drawing.Point(185, 34)
        Me.lblOverallCondition.Name = "lblOverallCondition"
        Me.lblOverallCondition.Size = New System.Drawing.Size(64, 16)
        Me.lblOverallCondition.TabIndex = 7
        Me.lblOverallCondition.Text = "Condition"
        Me.lblOverallCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSummaryFilter
        '
        Me.gbSummaryFilter.BorderColor = System.Drawing.Color.Black
        Me.gbSummaryFilter.Checked = False
        Me.gbSummaryFilter.CollapseAllExceptThis = False
        Me.gbSummaryFilter.CollapsedHoverImage = Nothing
        Me.gbSummaryFilter.CollapsedNormalImage = Nothing
        Me.gbSummaryFilter.CollapsedPressedImage = Nothing
        Me.gbSummaryFilter.CollapseOnLoad = True
        Me.gbSummaryFilter.Controls.Add(Me.cboGETotalCondition)
        Me.gbSummaryFilter.Controls.Add(Me.gbSummOperation)
        Me.gbSummaryFilter.Controls.Add(Me.elGeneralEvaluation)
        Me.gbSummaryFilter.Controls.Add(Me.txtGETotalScore)
        Me.gbSummaryFilter.Controls.Add(Me.txtBSCTotalScore)
        Me.gbSummaryFilter.Controls.Add(Me.lblGETotalCondition)
        Me.gbSummaryFilter.Controls.Add(Me.lblBSCTotalScore)
        Me.gbSummaryFilter.Controls.Add(Me.elBalanceScorecard)
        Me.gbSummaryFilter.Controls.Add(Me.lblGETotalScore)
        Me.gbSummaryFilter.Controls.Add(Me.cboBSCTotalCondition)
        Me.gbSummaryFilter.Controls.Add(Me.lblBSCTotalCondition)
        Me.gbSummaryFilter.ExpandedHoverImage = Nothing
        Me.gbSummaryFilter.ExpandedNormalImage = Nothing
        Me.gbSummaryFilter.ExpandedPressedImage = Nothing
        Me.gbSummaryFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSummaryFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSummaryFilter.HeaderHeight = 25
        Me.gbSummaryFilter.HeaderMessage = ""
        Me.gbSummaryFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbSummaryFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSummaryFilter.HeightOnCollapse = 0
        Me.gbSummaryFilter.LeftTextSpace = 0
        Me.gbSummaryFilter.Location = New System.Drawing.Point(3, 218)
        Me.gbSummaryFilter.Name = "gbSummaryFilter"
        Me.gbSummaryFilter.OpenHeight = 180
        Me.gbSummaryFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSummaryFilter.ShowBorder = True
        Me.gbSummaryFilter.ShowCheckBox = True
        Me.gbSummaryFilter.ShowCollapseButton = False
        Me.gbSummaryFilter.ShowDefaultBorderColor = True
        Me.gbSummaryFilter.ShowDownButton = False
        Me.gbSummaryFilter.ShowHeader = True
        Me.gbSummaryFilter.Size = New System.Drawing.Size(354, 181)
        Me.gbSummaryFilter.TabIndex = 0
        Me.gbSummaryFilter.Temp = 0
        Me.gbSummaryFilter.Text = "Summary Filter"
        Me.gbSummaryFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGETotalCondition
        '
        Me.cboGETotalCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGETotalCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGETotalCondition.FormattingEnabled = True
        Me.cboGETotalCondition.Location = New System.Drawing.Point(255, 150)
        Me.cboGETotalCondition.Name = "cboGETotalCondition"
        Me.cboGETotalCondition.Size = New System.Drawing.Size(91, 21)
        Me.cboGETotalCondition.TabIndex = 9
        '
        'gbSummOperation
        '
        Me.gbSummOperation.Controls.Add(Me.objbtnSummaryReset)
        Me.gbSummOperation.Controls.Add(Me.radSummaryAND)
        Me.gbSummOperation.Controls.Add(Me.radSummaryOR)
        Me.gbSummOperation.Location = New System.Drawing.Point(11, 79)
        Me.gbSummOperation.Name = "gbSummOperation"
        Me.gbSummOperation.Size = New System.Drawing.Size(305, 40)
        Me.gbSummOperation.TabIndex = 14
        Me.gbSummOperation.TabStop = False
        Me.gbSummOperation.Text = "Operation"
        '
        'objbtnSummaryReset
        '
        Me.objbtnSummaryReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSummaryReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSummaryReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSummaryReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSummaryReset.BorderSelected = False
        Me.objbtnSummaryReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSummaryReset.Image = CType(resources.GetObject("objbtnSummaryReset.Image"), System.Drawing.Image)
        Me.objbtnSummaryReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSummaryReset.Location = New System.Drawing.Point(278, 14)
        Me.objbtnSummaryReset.Name = "objbtnSummaryReset"
        Me.objbtnSummaryReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSummaryReset.TabIndex = 34
        '
        'radSummaryAND
        '
        Me.radSummaryAND.Location = New System.Drawing.Point(92, 16)
        Me.radSummaryAND.Name = "radSummaryAND"
        Me.radSummaryAND.Size = New System.Drawing.Size(70, 17)
        Me.radSummaryAND.TabIndex = 1
        Me.radSummaryAND.TabStop = True
        Me.radSummaryAND.Text = "AND"
        Me.radSummaryAND.UseVisualStyleBackColor = True
        '
        'radSummaryOR
        '
        Me.radSummaryOR.Location = New System.Drawing.Point(16, 16)
        Me.radSummaryOR.Name = "radSummaryOR"
        Me.radSummaryOR.Size = New System.Drawing.Size(70, 17)
        Me.radSummaryOR.TabIndex = 0
        Me.radSummaryOR.TabStop = True
        Me.radSummaryOR.Text = "OR"
        Me.radSummaryOR.UseVisualStyleBackColor = True
        '
        'elGeneralEvaluation
        '
        Me.elGeneralEvaluation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elGeneralEvaluation.Location = New System.Drawing.Point(8, 125)
        Me.elGeneralEvaluation.Name = "elGeneralEvaluation"
        Me.elGeneralEvaluation.Size = New System.Drawing.Size(338, 17)
        Me.elGeneralEvaluation.TabIndex = 6
        Me.elGeneralEvaluation.Tag = "GE"
        Me.elGeneralEvaluation.Text = "Competence Evaluation"
        Me.elGeneralEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGETotalScore
        '
        Me.txtGETotalScore.AllowNegative = False
        Me.txtGETotalScore.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGETotalScore.DigitsInGroup = 0
        Me.txtGETotalScore.Flags = 65536
        Me.txtGETotalScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGETotalScore.Location = New System.Drawing.Point(103, 150)
        Me.txtGETotalScore.MaxDecimalPlaces = 4
        Me.txtGETotalScore.MaxWholeDigits = 9
        Me.txtGETotalScore.Name = "txtGETotalScore"
        Me.txtGETotalScore.Prefix = ""
        Me.txtGETotalScore.RangeMax = 1.7976931348623157E+308
        Me.txtGETotalScore.RangeMin = -1.7976931348623157E+308
        Me.txtGETotalScore.Size = New System.Drawing.Size(76, 21)
        Me.txtGETotalScore.TabIndex = 10
        Me.txtGETotalScore.Text = "0"
        Me.txtGETotalScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBSCTotalScore
        '
        Me.txtBSCTotalScore.AllowNegative = False
        Me.txtBSCTotalScore.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBSCTotalScore.DigitsInGroup = 0
        Me.txtBSCTotalScore.Flags = 65536
        Me.txtBSCTotalScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBSCTotalScore.Location = New System.Drawing.Point(103, 51)
        Me.txtBSCTotalScore.MaxDecimalPlaces = 4
        Me.txtBSCTotalScore.MaxWholeDigits = 9
        Me.txtBSCTotalScore.Name = "txtBSCTotalScore"
        Me.txtBSCTotalScore.Prefix = ""
        Me.txtBSCTotalScore.RangeMax = 1.7976931348623157E+308
        Me.txtBSCTotalScore.RangeMin = -1.7976931348623157E+308
        Me.txtBSCTotalScore.Size = New System.Drawing.Size(76, 21)
        Me.txtBSCTotalScore.TabIndex = 5
        Me.txtBSCTotalScore.Text = "0"
        Me.txtBSCTotalScore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGETotalCondition
        '
        Me.lblGETotalCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGETotalCondition.Location = New System.Drawing.Point(185, 152)
        Me.lblGETotalCondition.Name = "lblGETotalCondition"
        Me.lblGETotalCondition.Size = New System.Drawing.Size(64, 16)
        Me.lblGETotalCondition.TabIndex = 7
        Me.lblGETotalCondition.Text = "Condition"
        Me.lblGETotalCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBSCTotalScore
        '
        Me.lblBSCTotalScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBSCTotalScore.Location = New System.Drawing.Point(24, 53)
        Me.lblBSCTotalScore.Name = "lblBSCTotalScore"
        Me.lblBSCTotalScore.Size = New System.Drawing.Size(73, 16)
        Me.lblBSCTotalScore.TabIndex = 3
        Me.lblBSCTotalScore.Text = "Total Score"
        Me.lblBSCTotalScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elBalanceScorecard
        '
        Me.elBalanceScorecard.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elBalanceScorecard.Location = New System.Drawing.Point(8, 28)
        Me.elBalanceScorecard.Name = "elBalanceScorecard"
        Me.elBalanceScorecard.Size = New System.Drawing.Size(338, 17)
        Me.elBalanceScorecard.TabIndex = 4
        Me.elBalanceScorecard.Tag = "BSC"
        Me.elBalanceScorecard.Text = "Balanced Score Card"
        Me.elBalanceScorecard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGETotalScore
        '
        Me.lblGETotalScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGETotalScore.Location = New System.Drawing.Point(24, 152)
        Me.lblGETotalScore.Name = "lblGETotalScore"
        Me.lblGETotalScore.Size = New System.Drawing.Size(73, 16)
        Me.lblGETotalScore.TabIndex = 8
        Me.lblGETotalScore.Text = "Total Score"
        Me.lblGETotalScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBSCTotalCondition
        '
        Me.cboBSCTotalCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBSCTotalCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBSCTotalCondition.FormattingEnabled = True
        Me.cboBSCTotalCondition.Location = New System.Drawing.Point(255, 51)
        Me.cboBSCTotalCondition.Name = "cboBSCTotalCondition"
        Me.cboBSCTotalCondition.Size = New System.Drawing.Size(91, 21)
        Me.cboBSCTotalCondition.TabIndex = 3
        '
        'lblBSCTotalCondition
        '
        Me.lblBSCTotalCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBSCTotalCondition.Location = New System.Drawing.Point(185, 53)
        Me.lblBSCTotalCondition.Name = "lblBSCTotalCondition"
        Me.lblBSCTotalCondition.Size = New System.Drawing.Size(64, 16)
        Me.lblBSCTotalCondition.TabIndex = 3
        Me.lblBSCTotalCondition.Text = "Condition"
        Me.lblBSCTotalCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbDetailFilter
        '
        Me.gbDetailFilter.BorderColor = System.Drawing.Color.Black
        Me.gbDetailFilter.Checked = False
        Me.gbDetailFilter.CollapseAllExceptThis = False
        Me.gbDetailFilter.CollapsedHoverImage = Nothing
        Me.gbDetailFilter.CollapsedNormalImage = Nothing
        Me.gbDetailFilter.CollapsedPressedImage = Nothing
        Me.gbDetailFilter.CollapseOnLoad = True
        Me.gbDetailFilter.Controls.Add(Me.pnlDetailCondition)
        Me.gbDetailFilter.Controls.Add(Me.EZeeLine1)
        Me.gbDetailFilter.Controls.Add(Me.pnlGrid)
        Me.gbDetailFilter.ExpandedHoverImage = Nothing
        Me.gbDetailFilter.ExpandedNormalImage = Nothing
        Me.gbDetailFilter.ExpandedPressedImage = Nothing
        Me.gbDetailFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetailFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDetailFilter.HeaderHeight = 25
        Me.gbDetailFilter.HeaderMessage = ""
        Me.gbDetailFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDetailFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDetailFilter.HeightOnCollapse = 0
        Me.gbDetailFilter.LeftTextSpace = 0
        Me.gbDetailFilter.Location = New System.Drawing.Point(3, 405)
        Me.gbDetailFilter.Name = "gbDetailFilter"
        Me.gbDetailFilter.OpenHeight = 574
        Me.gbDetailFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDetailFilter.ShowBorder = True
        Me.gbDetailFilter.ShowCheckBox = True
        Me.gbDetailFilter.ShowCollapseButton = False
        Me.gbDetailFilter.ShowDefaultBorderColor = True
        Me.gbDetailFilter.ShowDownButton = False
        Me.gbDetailFilter.ShowHeader = True
        Me.gbDetailFilter.Size = New System.Drawing.Size(354, 370)
        Me.gbDetailFilter.TabIndex = 4
        Me.gbDetailFilter.Temp = 0
        Me.gbDetailFilter.Text = "Detail Filter"
        Me.gbDetailFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlDetailCondition
        '
        Me.pnlDetailCondition.Controls.Add(Me.objbtnResetPerspective)
        Me.pnlDetailCondition.Controls.Add(Me.radDetailAND)
        Me.pnlDetailCondition.Controls.Add(Me.radDetailOR)
        Me.pnlDetailCondition.Location = New System.Drawing.Point(156, 52)
        Me.pnlDetailCondition.Name = "pnlDetailCondition"
        Me.pnlDetailCondition.Size = New System.Drawing.Size(190, 25)
        Me.pnlDetailCondition.TabIndex = 41
        '
        'objbtnResetPerspective
        '
        Me.objbtnResetPerspective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnResetPerspective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnResetPerspective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnResetPerspective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnResetPerspective.BorderSelected = False
        Me.objbtnResetPerspective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnResetPerspective.Image = CType(resources.GetObject("objbtnResetPerspective.Image"), System.Drawing.Image)
        Me.objbtnResetPerspective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnResetPerspective.Location = New System.Drawing.Point(166, 2)
        Me.objbtnResetPerspective.Name = "objbtnResetPerspective"
        Me.objbtnResetPerspective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnResetPerspective.TabIndex = 39
        '
        'radDetailAND
        '
        Me.radDetailAND.Location = New System.Drawing.Point(81, 3)
        Me.radDetailAND.Name = "radDetailAND"
        Me.radDetailAND.Size = New System.Drawing.Size(72, 17)
        Me.radDetailAND.TabIndex = 38
        Me.radDetailAND.Text = "AND"
        Me.radDetailAND.UseVisualStyleBackColor = True
        '
        'radDetailOR
        '
        Me.radDetailOR.Location = New System.Drawing.Point(3, 3)
        Me.radDetailOR.Name = "radDetailOR"
        Me.radDetailOR.Size = New System.Drawing.Size(72, 17)
        Me.radDetailOR.TabIndex = 37
        Me.radDetailOR.Text = "OR"
        Me.radDetailOR.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(8, 29)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(338, 17)
        Me.EZeeLine1.TabIndex = 27
        Me.EZeeLine1.Text = "Competence Evaluation"
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlGrid
        '
        Me.pnlGrid.Controls.Add(Me.picStayView)
        Me.pnlGrid.Controls.Add(Me.dgvData)
        Me.pnlGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlGrid.Location = New System.Drawing.Point(11, 83)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(335, 282)
        Me.pnlGrid.TabIndex = 34
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(273, 103)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 34
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeight = 20
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.dgcolhItems, Me.dgcolhScore, Me.dgcolhCondition, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhItemId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvData.Size = New System.Drawing.Size(335, 282)
        Me.dgvData.TabIndex = 33
        '
        'objdgcolhCollaps
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhCollaps.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollaps.Width = 25
        '
        'dgcolhItems
        '
        Me.dgcolhItems.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhItems.HeaderText = "Items"
        Me.dgcolhItems.Name = "dgcolhItems"
        Me.dgcolhItems.ReadOnly = True
        Me.dgcolhItems.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhItems.Width = 175
        '
        'dgcolhScore
        '
        Me.dgcolhScore.HeaderText = "Score"
        Me.dgcolhScore.MaxInputLength = 6
        Me.dgcolhScore.Name = "dgcolhScore"
        Me.dgcolhScore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhScore.Width = 50
        '
        'dgcolhCondition
        '
        Me.dgcolhCondition.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.dgcolhCondition.HeaderText = "Condition"
        Me.dgcolhCondition.Name = "dgcolhCondition"
        Me.dgcolhCondition.Width = 80
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhItemId
        '
        Me.objdgcolhItemId.HeaderText = "objdgcolhItemId"
        Me.objdgcolhItemId.Name = "objdgcolhItemId"
        Me.objdgcolhItemId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhItemId.Visible = False
        '
        'lvEmployee
        '
        Me.lvEmployee.BackColorOnChecked = True
        Me.lvEmployee.ColumnHeaders = Nothing
        Me.lvEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEcode, Me.colhEName, Me.colhAppDate, Me.colhEmail, Me.objcolhFRefId, Me.colhJob, Me.colhDepartment, Me.colhOverallScore})
        Me.lvEmployee.CompulsoryColumns = ""
        Me.lvEmployee.FullRowSelect = True
        Me.lvEmployee.GridLines = True
        Me.lvEmployee.GroupingColumn = Nothing
        Me.lvEmployee.HideSelection = False
        Me.lvEmployee.Location = New System.Drawing.Point(388, 231)
        Me.lvEmployee.MinColumnWidth = 50
        Me.lvEmployee.MultiSelect = False
        Me.lvEmployee.Name = "lvEmployee"
        Me.lvEmployee.OptionalColumns = ""
        Me.lvEmployee.ShowMoreItem = False
        Me.lvEmployee.ShowSaveItem = False
        Me.lvEmployee.ShowSelectAll = True
        Me.lvEmployee.ShowSizeAllColumnsToFit = True
        Me.lvEmployee.Size = New System.Drawing.Size(643, 251)
        Me.lvEmployee.Sortable = True
        Me.lvEmployee.TabIndex = 3
        Me.lvEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEmployee.View = System.Windows.Forms.View.Details
        '
        'colhEcode
        '
        Me.colhEcode.Tag = "colhEcode"
        Me.colhEcode.Text = "Code"
        Me.colhEcode.Width = 70
        '
        'colhEName
        '
        Me.colhEName.Tag = "colhEName"
        Me.colhEName.Text = "Employee"
        Me.colhEName.Width = 150
        '
        'colhAppDate
        '
        Me.colhAppDate.Tag = "colhAppDate"
        Me.colhAppDate.Text = "Appoint. Date"
        Me.colhAppDate.Width = 80
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 0
        '
        'objcolhFRefId
        '
        Me.objcolhFRefId.Tag = "objcolhFRefId"
        Me.objcolhFRefId.Text = "objcolhFRefId"
        Me.objcolhFRefId.Width = 0
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job Title"
        Me.colhJob.Width = 130
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 120
        '
        'colhOverallScore
        '
        Me.colhOverallScore.Tag = "colhOverallScore"
        Me.colhOverallScore.Text = "Overall Score"
        '
        'lvFilterCriteria
        '
        Me.lvFilterCriteria.BackColorOnChecked = False
        Me.lvFilterCriteria.ColumnHeaders = Nothing
        Me.lvFilterCriteria.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFieldName, Me.colhValue, Me.colhCondition, Me.objcolhFilterId, Me.objcolhConId, Me.objcolhOprId, Me.objcolhGuid, Me.objcolhGrpName, Me.colhOperation, Me.objcolhColTags})
        Me.lvFilterCriteria.CompulsoryColumns = ""
        Me.lvFilterCriteria.FullRowSelect = True
        Me.lvFilterCriteria.GridLines = True
        Me.lvFilterCriteria.GroupingColumn = Nothing
        Me.lvFilterCriteria.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvFilterCriteria.HideSelection = False
        Me.lvFilterCriteria.Location = New System.Drawing.Point(388, 3)
        Me.lvFilterCriteria.MinColumnWidth = 50
        Me.lvFilterCriteria.MultiSelect = False
        Me.lvFilterCriteria.Name = "lvFilterCriteria"
        Me.lvFilterCriteria.OptionalColumns = ""
        Me.lvFilterCriteria.ShowMoreItem = False
        Me.lvFilterCriteria.ShowSaveItem = False
        Me.lvFilterCriteria.ShowSelectAll = True
        Me.lvFilterCriteria.ShowSizeAllColumnsToFit = True
        Me.lvFilterCriteria.Size = New System.Drawing.Size(643, 190)
        Me.lvFilterCriteria.Sortable = True
        Me.lvFilterCriteria.TabIndex = 0
        Me.lvFilterCriteria.UseCompatibleStateImageBehavior = False
        Me.lvFilterCriteria.View = System.Windows.Forms.View.Details
        '
        'colhFieldName
        '
        Me.colhFieldName.Tag = "colhFieldName"
        Me.colhFieldName.Text = "Filter Criteria"
        Me.colhFieldName.Width = 400
        '
        'colhValue
        '
        Me.colhValue.Text = "Value"
        Me.colhValue.Width = 80
        '
        'colhCondition
        '
        Me.colhCondition.Tag = "colhCondition"
        Me.colhCondition.Text = "Condition"
        Me.colhCondition.Width = 72
        '
        'objcolhFilterId
        '
        Me.objcolhFilterId.Tag = "objcolhFilterId"
        Me.objcolhFilterId.Text = "objcolhFilterId"
        Me.objcolhFilterId.Width = 0
        '
        'objcolhConId
        '
        Me.objcolhConId.Tag = "objcolhConId"
        Me.objcolhConId.Text = "objcolhConId"
        Me.objcolhConId.Width = 0
        '
        'objcolhOprId
        '
        Me.objcolhOprId.Tag = "objcolhOprId"
        Me.objcolhOprId.Text = "objcolhOprId"
        Me.objcolhOprId.Width = 0
        '
        'objcolhGuid
        '
        Me.objcolhGuid.Tag = "objcolhGuid"
        Me.objcolhGuid.Text = "objcolhGuid"
        Me.objcolhGuid.Width = 0
        '
        'objcolhGrpName
        '
        Me.objcolhGrpName.Tag = "objcolhGrpName"
        Me.objcolhGrpName.Text = ""
        Me.objcolhGrpName.Width = 0
        '
        'colhOperation
        '
        Me.colhOperation.Tag = "colhOperation"
        Me.colhOperation.Text = "Operator"
        '
        'objcolhColTags
        '
        Me.objcolhColTags.Tag = "objcolhColTags"
        Me.objcolhColTags.Text = ""
        Me.objcolhColTags.Width = 0
        '
        'gbRemarks
        '
        Me.gbRemarks.BorderColor = System.Drawing.Color.Black
        Me.gbRemarks.Checked = False
        Me.gbRemarks.CollapseAllExceptThis = False
        Me.gbRemarks.CollapsedHoverImage = Nothing
        Me.gbRemarks.CollapsedNormalImage = Nothing
        Me.gbRemarks.CollapsedPressedImage = Nothing
        Me.gbRemarks.CollapseOnLoad = False
        Me.gbRemarks.Controls.Add(Me.pnlRemark)
        Me.gbRemarks.ExpandedHoverImage = Nothing
        Me.gbRemarks.ExpandedNormalImage = Nothing
        Me.gbRemarks.ExpandedPressedImage = Nothing
        Me.gbRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemarks.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemarks.HeaderHeight = 25
        Me.gbRemarks.HeaderMessage = ""
        Me.gbRemarks.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbRemarks.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemarks.HeightOnCollapse = 0
        Me.gbRemarks.LeftTextSpace = 0
        Me.gbRemarks.Location = New System.Drawing.Point(388, 486)
        Me.gbRemarks.Name = "gbRemarks"
        Me.gbRemarks.OpenHeight = 300
        Me.gbRemarks.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemarks.ShowBorder = True
        Me.gbRemarks.ShowCheckBox = False
        Me.gbRemarks.ShowCollapseButton = False
        Me.gbRemarks.ShowDefaultBorderColor = True
        Me.gbRemarks.ShowDownButton = False
        Me.gbRemarks.ShowHeader = True
        Me.gbRemarks.Size = New System.Drawing.Size(643, 87)
        Me.gbRemarks.TabIndex = 5
        Me.gbRemarks.Temp = 0
        Me.gbRemarks.Text = "Remark"
        Me.gbRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlRemark
        '
        Me.pnlRemark.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlRemark.Controls.Add(Me.txtRemark)
        Me.pnlRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlRemark.Location = New System.Drawing.Point(2, 26)
        Me.pnlRemark.Name = "pnlRemark"
        Me.pnlRemark.Size = New System.Drawing.Size(639, 59)
        Me.pnlRemark.TabIndex = 1
        '
        'txtRemark
        '
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(639, 59)
        Me.txtRemark.TabIndex = 0
        '
        'btnRemove
        '
        Me.btnRemove.BackColor = System.Drawing.Color.White
        Me.btnRemove.BackgroundImage = CType(resources.GetObject("btnRemove.BackgroundImage"), System.Drawing.Image)
        Me.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemove.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRemove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemove.FlatAppearance.BorderSize = 0
        Me.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.ForeColor = System.Drawing.Color.Black
        Me.btnRemove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemove.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Location = New System.Drawing.Point(491, 197)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Size = New System.Drawing.Size(93, 30)
        Me.btnRemove.TabIndex = 2
        Me.btnRemove.Text = "&Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(388, 197)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnApplyFilter
        '
        Me.btnApplyFilter.BackColor = System.Drawing.Color.White
        Me.btnApplyFilter.BackgroundImage = CType(resources.GetObject("btnApplyFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnApplyFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApplyFilter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnApplyFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApplyFilter.FlatAppearance.BorderSize = 0
        Me.btnApplyFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApplyFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApplyFilter.ForeColor = System.Drawing.Color.Black
        Me.btnApplyFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApplyFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnApplyFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplyFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApplyFilter.Location = New System.Drawing.Point(932, 197)
        Me.btnApplyFilter.Name = "btnApplyFilter"
        Me.btnApplyFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplyFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApplyFilter.Size = New System.Drawing.Size(97, 30)
        Me.btnApplyFilter.TabIndex = 1
        Me.btnApplyFilter.Text = "&Apply Filter"
        Me.btnApplyFilter.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnExport)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 574)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1034, 50)
        Me.objFooter.TabIndex = 6
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(801, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(118, 30)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "&Save And Export"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(698, 11)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "E&xport"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(925, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmShortlistingEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1034, 624)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShortlistingEmployee"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appraisal Analysis"
        Me.pnlMain.ResumeLayout(False)
        Me.tblpanel1.ResumeLayout(False)
        Me.fpnlFlow.ResumeLayout(False)
        Me.gbPeriod.ResumeLayout(False)
        Me.gbOverAllScore.ResumeLayout(False)
        Me.gbOverAllScore.PerformLayout()
        Me.gbSummaryFilter.ResumeLayout(False)
        Me.gbSummaryFilter.PerformLayout()
        Me.gbSummOperation.ResumeLayout(False)
        Me.gbDetailFilter.ResumeLayout(False)
        Me.pnlDetailCondition.ResumeLayout(False)
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRemarks.ResumeLayout(False)
        Me.pnlRemark.ResumeLayout(False)
        Me.pnlRemark.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lvFilterCriteria As eZee.Common.eZeeListView
    Friend WithEvents btnApplyFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnRemove As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lvEmployee As eZee.Common.eZeeListView
    Friend WithEvents colhFieldName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCondition As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFilterId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhConId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhOprId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGuid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGrpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOperation As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhColTags As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEcode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAppDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFRefId As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents gbRemarks As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlRemark As System.Windows.Forms.Panel
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents fpnlFlow As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbPeriod As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents gbSummaryFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboGETotalCondition As System.Windows.Forms.ComboBox
    Friend WithEvents gbSummOperation As System.Windows.Forms.GroupBox
    Friend WithEvents objbtnSummaryReset As eZee.Common.eZeeGradientButton
    Friend WithEvents radSummaryAND As System.Windows.Forms.RadioButton
    Friend WithEvents radSummaryOR As System.Windows.Forms.RadioButton
    Friend WithEvents elGeneralEvaluation As eZee.Common.eZeeLine
    Friend WithEvents txtGETotalScore As eZee.TextBox.NumericTextBox
    Friend WithEvents txtBSCTotalScore As eZee.TextBox.NumericTextBox
    Friend WithEvents lblGETotalCondition As System.Windows.Forms.Label
    Friend WithEvents lblBSCTotalScore As System.Windows.Forms.Label
    Friend WithEvents elBalanceScorecard As eZee.Common.eZeeLine
    Friend WithEvents lblGETotalScore As System.Windows.Forms.Label
    Friend WithEvents cboBSCTotalCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblBSCTotalCondition As System.Windows.Forms.Label
    Friend WithEvents gbDetailFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOverallScore As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbOverAllScore As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOverallScore As eZee.TextBox.NumericTextBox
    Friend WithEvents lblOverallScore As System.Windows.Forms.Label
    Friend WithEvents cboOverallCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblOverallCondition As System.Windows.Forms.Label
    Friend WithEvents lblAppointment As System.Windows.Forms.Label
    Friend WithEvents cboAppointment As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhItems As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCondition As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhItemId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tblpanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblFinalResult As System.Windows.Forms.Label
    Friend WithEvents cboFinalResult As System.Windows.Forms.ComboBox
    Friend WithEvents pnlDetailCondition As System.Windows.Forms.Panel
    Friend WithEvents objbtnResetPerspective As eZee.Common.eZeeGradientButton
    Friend WithEvents radDetailAND As System.Windows.Forms.RadioButton
    Friend WithEvents radDetailOR As System.Windows.Forms.RadioButton
End Class
