﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppraisals
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAppraisals))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.txtSearchFinal = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.radByReference = New System.Windows.Forms.RadioButton
        Me.radByGrades = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlAppdate = New System.Windows.Forms.Panel
        Me.lblFinalResult = New System.Windows.Forms.Label
        Me.cboFinalResult = New System.Windows.Forms.ComboBox
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.lblTo = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblAppointment = New System.Windows.Forms.Label
        Me.cboAppointment = New System.Windows.Forms.ComboBox
        Me.pnlGradeWise = New System.Windows.Forms.Panel
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.txtScoreTo = New eZee.TextBox.NumericTextBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblTotalScoreT = New System.Windows.Forms.Label
        Me.lblGrades = New System.Windows.Forms.Label
        Me.lblTotalScoreF = New System.Windows.Forms.Label
        Me.txtScoreFrom = New eZee.TextBox.NumericTextBox
        Me.cboGrades_Award = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGrades = New eZee.Common.eZeeGradientButton
        Me.pnlRefWise = New System.Windows.Forms.Panel
        Me.lblReferenceDate = New System.Windows.Forms.Label
        Me.txtOpeningDate = New eZee.TextBox.AlphanumericTextBox
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboRefNo = New System.Windows.Forms.ComboBox
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.objbtndown = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFinalCheckAll = New System.Windows.Forms.CheckBox
        Me.dgFinalEmployee = New System.Windows.Forms.DataGridView
        Me.objcolhFinalSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhFinalEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhfinal_Job = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhfinalDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalMode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOperation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIncrement = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFinalEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnFilter_Action = New eZee.Common.eZeeGradientButton
        Me.objResetFinalEmployee = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objSearchFinalEmployee = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboAppraisalMode = New System.Windows.Forms.ComboBox
        Me.lblAppraisalMode = New System.Windows.Forms.Label
        Me.gbApplyAppraisalMode = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnApply_Action = New eZee.Common.eZeeGradientButton
        Me.lnkApplyAction = New System.Windows.Forms.LinkLabel
        Me.cboApplyAppMode = New System.Windows.Forms.ComboBox
        Me.lblApplyAppMode = New System.Windows.Forms.Label
        Me.objCheckAll = New System.Windows.Forms.CheckBox
        Me.objEMailFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgShortListEmployee = New System.Windows.Forms.DataGridView
        Me.objcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAction = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSalaryIncrement = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidSalaryIncrement = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRemider = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuBonus = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidBonus = New System.Windows.Forms.ToolStripMenuItem
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabAppEmployee = New System.Windows.Forms.TabControl
        Me.tabpgShotList = New System.Windows.Forms.TabPage
        Me.tabpgFinal = New System.Windows.Forms.TabPage
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchShortListed = New System.Windows.Forms.TextBox
        Me.pnlMain.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAppdate.SuspendLayout()
        Me.pnlGradeWise.SuspendLayout()
        Me.pnlRefWise.SuspendLayout()
        CType(Me.dgFinalEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFinalEmployee.SuspendLayout()
        CType(Me.objResetFinalEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objSearchFinalEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbApplyAppraisalMode.SuspendLayout()
        Me.objEMailFooter.SuspendLayout()
        CType(Me.dgShortListEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.tabAppEmployee.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.objbtndown)
        Me.pnlMain.Controls.Add(Me.objbtnUp)
        Me.pnlMain.Controls.Add(Me.objFinalCheckAll)
        Me.pnlMain.Controls.Add(Me.dgFinalEmployee)
        Me.pnlMain.Controls.Add(Me.gbFinalEmployee)
        Me.pnlMain.Controls.Add(Me.gbApplyAppraisalMode)
        Me.pnlMain.Controls.Add(Me.objCheckAll)
        Me.pnlMain.Controls.Add(Me.objEMailFooter)
        Me.pnlMain.Controls.Add(Me.dgShortListEmployee)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.txtSearchShortListed)
        Me.pnlMain.Controls.Add(Me.txtSearchFinal)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(774, 572)
        Me.pnlMain.TabIndex = 1
        '
        'txtSearchFinal
        '
        Me.txtSearchFinal.Location = New System.Drawing.Point(638, 342)
        Me.txtSearchFinal.Name = "txtSearchFinal"
        Me.txtSearchFinal.Size = New System.Drawing.Size(129, 21)
        Me.txtSearchFinal.TabIndex = 459
        Me.txtSearchFinal.Visible = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.radByReference)
        Me.gbFilterCriteria.Controls.Add(Me.radByGrades)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.pnlAppdate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlGradeWise)
        Me.gbFilterCriteria.Controls.Add(Me.pnlRefWise)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(7, 7)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(760, 87)
        Me.gbFilterCriteria.TabIndex = 447
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(628, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 17)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'radByReference
        '
        Me.radByReference.BackColor = System.Drawing.Color.Transparent
        Me.radByReference.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radByReference.Location = New System.Drawing.Point(538, 4)
        Me.radByReference.Name = "radByReference"
        Me.radByReference.Size = New System.Drawing.Size(84, 17)
        Me.radByReference.TabIndex = 459
        Me.radByReference.TabStop = True
        Me.radByReference.Text = "Reference"
        Me.radByReference.UseVisualStyleBackColor = False
        '
        'radByGrades
        '
        Me.radByGrades.BackColor = System.Drawing.Color.Transparent
        Me.radByGrades.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radByGrades.Location = New System.Drawing.Point(343, 4)
        Me.radByGrades.Name = "radByGrades"
        Me.radByGrades.Size = New System.Drawing.Size(189, 17)
        Me.radByGrades.TabIndex = 459
        Me.radByGrades.TabStop = True
        Me.radByGrades.Text = "Pre-Defined Grades/Awards"
        Me.radByGrades.UseVisualStyleBackColor = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(735, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 454
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(711, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 453
        Me.objbtnSearch.TabStop = False
        '
        'pnlAppdate
        '
        Me.pnlAppdate.Controls.Add(Me.lblFinalResult)
        Me.pnlAppdate.Controls.Add(Me.cboFinalResult)
        Me.pnlAppdate.Controls.Add(Me.dtpDate2)
        Me.pnlAppdate.Controls.Add(Me.lblTo)
        Me.pnlAppdate.Controls.Add(Me.objlblCaption)
        Me.pnlAppdate.Controls.Add(Me.dtpDate1)
        Me.pnlAppdate.Controls.Add(Me.lblAppointment)
        Me.pnlAppdate.Controls.Add(Me.cboAppointment)
        Me.pnlAppdate.Location = New System.Drawing.Point(2, 57)
        Me.pnlAppdate.Name = "pnlAppdate"
        Me.pnlAppdate.Size = New System.Drawing.Size(751, 26)
        Me.pnlAppdate.TabIndex = 465
        '
        'lblFinalResult
        '
        Me.lblFinalResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinalResult.Location = New System.Drawing.Point(3, 4)
        Me.lblFinalResult.Name = "lblFinalResult"
        Me.lblFinalResult.Size = New System.Drawing.Size(70, 16)
        Me.lblFinalResult.TabIndex = 448
        Me.lblFinalResult.Text = "Score Based"
        Me.lblFinalResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFinalResult
        '
        Me.cboFinalResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFinalResult.DropDownWidth = 200
        Me.cboFinalResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFinalResult.FormattingEnabled = True
        Me.cboFinalResult.Location = New System.Drawing.Point(77, 3)
        Me.cboFinalResult.Name = "cboFinalResult"
        Me.cboFinalResult.Size = New System.Drawing.Size(153, 21)
        Me.cboFinalResult.TabIndex = 449
        '
        'dtpDate2
        '
        Me.dtpDate2.Checked = False
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(641, 2)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.ShowCheckBox = True
        Me.dtpDate2.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate2.TabIndex = 447
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(618, 4)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(19, 16)
        Me.lblTo.TabIndex = 446
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(444, 4)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(58, 16)
        Me.objlblCaption.TabIndex = 445
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.Checked = False
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(508, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.ShowCheckBox = True
        Me.dtpDate1.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate1.TabIndex = 444
        '
        'lblAppointment
        '
        Me.lblAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointment.Location = New System.Drawing.Point(236, 4)
        Me.lblAppointment.Name = "lblAppointment"
        Me.lblAppointment.Size = New System.Drawing.Size(87, 17)
        Me.lblAppointment.TabIndex = 441
        Me.lblAppointment.Text = "Appointment"
        Me.lblAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAppointment
        '
        Me.cboAppointment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppointment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppointment.FormattingEnabled = True
        Me.cboAppointment.Location = New System.Drawing.Point(329, 2)
        Me.cboAppointment.Name = "cboAppointment"
        Me.cboAppointment.Size = New System.Drawing.Size(109, 21)
        Me.cboAppointment.TabIndex = 440
        '
        'pnlGradeWise
        '
        Me.pnlGradeWise.Controls.Add(Me.lblPeriod)
        Me.pnlGradeWise.Controls.Add(Me.txtScoreTo)
        Me.pnlGradeWise.Controls.Add(Me.cboPeriod)
        Me.pnlGradeWise.Controls.Add(Me.lblTotalScoreT)
        Me.pnlGradeWise.Controls.Add(Me.lblGrades)
        Me.pnlGradeWise.Controls.Add(Me.lblTotalScoreF)
        Me.pnlGradeWise.Controls.Add(Me.txtScoreFrom)
        Me.pnlGradeWise.Controls.Add(Me.cboGrades_Award)
        Me.pnlGradeWise.Controls.Add(Me.objbtnSearchGrades)
        Me.pnlGradeWise.Location = New System.Drawing.Point(2, 29)
        Me.pnlGradeWise.Name = "pnlGradeWise"
        Me.pnlGradeWise.Size = New System.Drawing.Size(751, 26)
        Me.pnlGradeWise.TabIndex = 463
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(5, 4)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(66, 17)
        Me.lblPeriod.TabIndex = 464
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScoreTo
        '
        Me.txtScoreTo.AllowNegative = True
        Me.txtScoreTo.BackColor = System.Drawing.Color.White
        Me.txtScoreTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreTo.DigitsInGroup = 0
        Me.txtScoreTo.Flags = 0
        Me.txtScoreTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreTo.Location = New System.Drawing.Point(700, 2)
        Me.txtScoreTo.MaxDecimalPlaces = 4
        Me.txtScoreTo.MaxWholeDigits = 9
        Me.txtScoreTo.Name = "txtScoreTo"
        Me.txtScoreTo.Prefix = ""
        Me.txtScoreTo.RangeMax = 1.7976931348623157E+308
        Me.txtScoreTo.RangeMin = -1.7976931348623157E+308
        Me.txtScoreTo.ReadOnly = True
        Me.txtScoreTo.Size = New System.Drawing.Size(45, 21)
        Me.txtScoreTo.TabIndex = 465
        Me.txtScoreTo.Text = "0"
        Me.txtScoreTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(77, 2)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(153, 21)
        Me.cboPeriod.TabIndex = 465
        '
        'lblTotalScoreT
        '
        Me.lblTotalScoreT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalScoreT.Location = New System.Drawing.Point(658, 4)
        Me.lblTotalScoreT.Name = "lblTotalScoreT"
        Me.lblTotalScoreT.Size = New System.Drawing.Size(36, 17)
        Me.lblTotalScoreT.TabIndex = 467
        Me.lblTotalScoreT.Text = "To"
        Me.lblTotalScoreT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGrades
        '
        Me.lblGrades.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrades.Location = New System.Drawing.Point(236, 4)
        Me.lblGrades.Name = "lblGrades"
        Me.lblGrades.Size = New System.Drawing.Size(87, 17)
        Me.lblGrades.TabIndex = 439
        Me.lblGrades.Text = "Grades/Awards"
        Me.lblGrades.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalScoreF
        '
        Me.lblTotalScoreF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalScoreF.Location = New System.Drawing.Point(534, 4)
        Me.lblTotalScoreF.Name = "lblTotalScoreF"
        Me.lblTotalScoreF.Size = New System.Drawing.Size(67, 17)
        Me.lblTotalScoreF.TabIndex = 466
        Me.lblTotalScoreF.Text = "Score From"
        Me.lblTotalScoreF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScoreFrom
        '
        Me.txtScoreFrom.AllowNegative = True
        Me.txtScoreFrom.BackColor = System.Drawing.Color.White
        Me.txtScoreFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreFrom.DigitsInGroup = 0
        Me.txtScoreFrom.Flags = 0
        Me.txtScoreFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreFrom.Location = New System.Drawing.Point(607, 2)
        Me.txtScoreFrom.MaxDecimalPlaces = 4
        Me.txtScoreFrom.MaxWholeDigits = 9
        Me.txtScoreFrom.Name = "txtScoreFrom"
        Me.txtScoreFrom.Prefix = ""
        Me.txtScoreFrom.RangeMax = 1.7976931348623157E+308
        Me.txtScoreFrom.RangeMin = -1.7976931348623157E+308
        Me.txtScoreFrom.ReadOnly = True
        Me.txtScoreFrom.Size = New System.Drawing.Size(45, 21)
        Me.txtScoreFrom.TabIndex = 464
        Me.txtScoreFrom.Text = "0"
        Me.txtScoreFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboGrades_Award
        '
        Me.cboGrades_Award.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrades_Award.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrades_Award.FormattingEnabled = True
        Me.cboGrades_Award.Location = New System.Drawing.Point(329, 2)
        Me.cboGrades_Award.Name = "cboGrades_Award"
        Me.cboGrades_Award.Size = New System.Drawing.Size(172, 21)
        Me.cboGrades_Award.TabIndex = 441
        '
        'objbtnSearchGrades
        '
        Me.objbtnSearchGrades.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrades.BorderSelected = False
        Me.objbtnSearchGrades.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrades.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrades.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrades.Location = New System.Drawing.Point(507, 2)
        Me.objbtnSearchGrades.Name = "objbtnSearchGrades"
        Me.objbtnSearchGrades.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrades.TabIndex = 440
        '
        'pnlRefWise
        '
        Me.pnlRefWise.Controls.Add(Me.lblReferenceDate)
        Me.pnlRefWise.Controls.Add(Me.txtOpeningDate)
        Me.pnlRefWise.Controls.Add(Me.lblRefNo)
        Me.pnlRefWise.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlRefWise.Controls.Add(Me.lblEmployee)
        Me.pnlRefWise.Controls.Add(Me.cboEmployee)
        Me.pnlRefWise.Controls.Add(Me.cboRefNo)
        Me.pnlRefWise.Controls.Add(Me.objbtnSearchRefNo)
        Me.pnlRefWise.Location = New System.Drawing.Point(2, 29)
        Me.pnlRefWise.Name = "pnlRefWise"
        Me.pnlRefWise.Size = New System.Drawing.Size(751, 26)
        Me.pnlRefWise.TabIndex = 459
        '
        'lblReferenceDate
        '
        Me.lblReferenceDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceDate.Location = New System.Drawing.Point(534, 4)
        Me.lblReferenceDate.Name = "lblReferenceDate"
        Me.lblReferenceDate.Size = New System.Drawing.Size(67, 17)
        Me.lblReferenceDate.TabIndex = 439
        Me.lblReferenceDate.Text = "Ref. date"
        Me.lblReferenceDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpeningDate
        '
        Me.txtOpeningDate.BackColor = System.Drawing.Color.White
        Me.txtOpeningDate.Flags = 0
        Me.txtOpeningDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpeningDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOpeningDate.Location = New System.Drawing.Point(607, 2)
        Me.txtOpeningDate.Name = "txtOpeningDate"
        Me.txtOpeningDate.ReadOnly = True
        Me.txtOpeningDate.Size = New System.Drawing.Size(138, 21)
        Me.txtOpeningDate.TabIndex = 456
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(3, 4)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(66, 17)
        Me.lblRefNo.TabIndex = 434
        Me.lblRefNo.Text = "Ref. No"
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(614, 3)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 461
        Me.objbtnSearchEmployee.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(550, 6)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(61, 15)
        Me.lblEmployee.TabIndex = 460
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(617, 3)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(20, 21)
        Me.cboEmployee.TabIndex = 459
        Me.cboEmployee.Visible = False
        '
        'cboRefNo
        '
        Me.cboRefNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefNo.FormattingEnabled = True
        Me.cboRefNo.Location = New System.Drawing.Point(77, 2)
        Me.cboRefNo.Name = "cboRefNo"
        Me.cboRefNo.Size = New System.Drawing.Size(424, 21)
        Me.cboRefNo.TabIndex = 438
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(507, 2)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 435
        '
        'objbtndown
        '
        Me.objbtndown.BackColor = System.Drawing.Color.White
        Me.objbtndown.BackgroundImage = CType(resources.GetObject("objbtndown.BackgroundImage"), System.Drawing.Image)
        Me.objbtndown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtndown.BorderColor = System.Drawing.Color.Empty
        Me.objbtndown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtndown.FlatAppearance.BorderSize = 0
        Me.objbtndown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtndown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtndown.ForeColor = System.Drawing.Color.Black
        Me.objbtndown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtndown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtndown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtndown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtndown.Image = Global.Aruti.Main.My.Resources.Resources.MoveDown_16
        Me.objbtndown.Location = New System.Drawing.Point(342, 311)
        Me.objbtndown.Name = "objbtndown"
        Me.objbtndown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtndown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtndown.Size = New System.Drawing.Size(60, 26)
        Me.objbtndown.TabIndex = 458
        Me.objbtndown.UseVisualStyleBackColor = True
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Main.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(342, 280)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(60, 26)
        Me.objbtnUp.TabIndex = 457
        Me.objbtnUp.UseVisualStyleBackColor = True
        '
        'objFinalCheckAll
        '
        Me.objFinalCheckAll.AutoSize = True
        Me.objFinalCheckAll.Location = New System.Drawing.Point(14, 348)
        Me.objFinalCheckAll.Name = "objFinalCheckAll"
        Me.objFinalCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objFinalCheckAll.TabIndex = 456
        Me.objFinalCheckAll.UseVisualStyleBackColor = True
        '
        'dgFinalEmployee
        '
        Me.dgFinalEmployee.AllowUserToAddRows = False
        Me.dgFinalEmployee.AllowUserToDeleteRows = False
        Me.dgFinalEmployee.AllowUserToResizeColumns = False
        Me.dgFinalEmployee.AllowUserToResizeRows = False
        Me.dgFinalEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgFinalEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgFinalEmployee.ColumnHeadersHeight = 21
        Me.dgFinalEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgFinalEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhFinalSelect, Me.colhFinalEmpCode, Me.colhFinalEmpName, Me.colhFinalGender, Me.colhFinalAppointdate, Me.colhFinalEmail, Me.dgcolhfinal_Job, Me.dgcolhfinalDept, Me.colhFinalMode, Me.dgcolhOperation, Me.objdgcolhIncrement})
        Me.dgFinalEmployee.Location = New System.Drawing.Point(7, 342)
        Me.dgFinalEmployee.Name = "dgFinalEmployee"
        Me.dgFinalEmployee.RowHeadersVisible = False
        Me.dgFinalEmployee.RowHeadersWidth = 25
        Me.dgFinalEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgFinalEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgFinalEmployee.Size = New System.Drawing.Size(760, 169)
        Me.dgFinalEmployee.TabIndex = 452
        '
        'objcolhFinalSelect
        '
        Me.objcolhFinalSelect.HeaderText = ""
        Me.objcolhFinalSelect.Name = "objcolhFinalSelect"
        Me.objcolhFinalSelect.Width = 25
        '
        'colhFinalEmpCode
        '
        Me.colhFinalEmpCode.HeaderText = "Code"
        Me.colhFinalEmpCode.Name = "colhFinalEmpCode"
        Me.colhFinalEmpCode.ReadOnly = True
        Me.colhFinalEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalEmpCode.Width = 80
        '
        'colhFinalEmpName
        '
        Me.colhFinalEmpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhFinalEmpName.HeaderText = "Employee"
        Me.colhFinalEmpName.Name = "colhFinalEmpName"
        Me.colhFinalEmpName.ReadOnly = True
        Me.colhFinalEmpName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhFinalGender
        '
        Me.colhFinalGender.HeaderText = "Gender"
        Me.colhFinalGender.Name = "colhFinalGender"
        Me.colhFinalGender.ReadOnly = True
        Me.colhFinalGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalGender.Width = 60
        '
        'colhFinalAppointdate
        '
        Me.colhFinalAppointdate.HeaderText = "Appoint. Date"
        Me.colhFinalAppointdate.Name = "colhFinalAppointdate"
        Me.colhFinalAppointdate.ReadOnly = True
        Me.colhFinalAppointdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalAppointdate.Width = 80
        '
        'colhFinalEmail
        '
        Me.colhFinalEmail.HeaderText = "Email"
        Me.colhFinalEmail.Name = "colhFinalEmail"
        Me.colhFinalEmail.ReadOnly = True
        Me.colhFinalEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalEmail.Visible = False
        Me.colhFinalEmail.Width = 150
        '
        'dgcolhfinal_Job
        '
        Me.dgcolhfinal_Job.HeaderText = "Job Title"
        Me.dgcolhfinal_Job.Name = "dgcolhfinal_Job"
        Me.dgcolhfinal_Job.ReadOnly = True
        '
        'dgcolhfinalDept
        '
        Me.dgcolhfinalDept.HeaderText = "Department"
        Me.dgcolhfinalDept.Name = "dgcolhfinalDept"
        Me.dgcolhfinalDept.ReadOnly = True
        '
        'colhFinalMode
        '
        Me.colhFinalMode.HeaderText = "Action"
        Me.colhFinalMode.Name = "colhFinalMode"
        Me.colhFinalMode.ReadOnly = True
        '
        'dgcolhOperation
        '
        Me.dgcolhOperation.HeaderText = "Operation Done"
        Me.dgcolhOperation.Name = "dgcolhOperation"
        Me.dgcolhOperation.ReadOnly = True
        Me.dgcolhOperation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIncrement
        '
        Me.objdgcolhIncrement.HeaderText = "objdgcolhIncrement"
        Me.objdgcolhIncrement.Name = "objdgcolhIncrement"
        Me.objdgcolhIncrement.Visible = False
        '
        'gbFinalEmployee
        '
        Me.gbFinalEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbFinalEmployee.Checked = False
        Me.gbFinalEmployee.CollapseAllExceptThis = False
        Me.gbFinalEmployee.CollapsedHoverImage = Nothing
        Me.gbFinalEmployee.CollapsedNormalImage = Nothing
        Me.gbFinalEmployee.CollapsedPressedImage = Nothing
        Me.gbFinalEmployee.CollapseOnLoad = False
        Me.gbFinalEmployee.Controls.Add(Me.objbtnFilter_Action)
        Me.gbFinalEmployee.Controls.Add(Me.objResetFinalEmployee)
        Me.gbFinalEmployee.Controls.Add(Me.objSearchFinalEmployee)
        Me.gbFinalEmployee.Controls.Add(Me.cboAppraisalMode)
        Me.gbFinalEmployee.Controls.Add(Me.lblAppraisalMode)
        Me.gbFinalEmployee.ExpandedHoverImage = Nothing
        Me.gbFinalEmployee.ExpandedNormalImage = Nothing
        Me.gbFinalEmployee.ExpandedPressedImage = Nothing
        Me.gbFinalEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFinalEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFinalEmployee.HeaderHeight = 25
        Me.gbFinalEmployee.HeaderMessage = ""
        Me.gbFinalEmployee.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFinalEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFinalEmployee.HeightOnCollapse = 0
        Me.gbFinalEmployee.LeftTextSpace = 0
        Me.gbFinalEmployee.Location = New System.Drawing.Point(7, 280)
        Me.gbFinalEmployee.Name = "gbFinalEmployee"
        Me.gbFinalEmployee.OpenHeight = 300
        Me.gbFinalEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFinalEmployee.ShowBorder = True
        Me.gbFinalEmployee.ShowCheckBox = False
        Me.gbFinalEmployee.ShowCollapseButton = False
        Me.gbFinalEmployee.ShowDefaultBorderColor = True
        Me.gbFinalEmployee.ShowDownButton = False
        Me.gbFinalEmployee.ShowHeader = True
        Me.gbFinalEmployee.Size = New System.Drawing.Size(329, 57)
        Me.gbFinalEmployee.TabIndex = 453
        Me.gbFinalEmployee.Temp = 0
        Me.gbFinalEmployee.Text = "Filter Criteria"
        Me.gbFinalEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnFilter_Action
        '
        Me.objbtnFilter_Action.BackColor = System.Drawing.Color.Transparent
        Me.objbtnFilter_Action.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnFilter_Action.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnFilter_Action.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnFilter_Action.BorderSelected = False
        Me.objbtnFilter_Action.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnFilter_Action.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnFilter_Action.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnFilter_Action.Location = New System.Drawing.Point(305, 30)
        Me.objbtnFilter_Action.Name = "objbtnFilter_Action"
        Me.objbtnFilter_Action.Size = New System.Drawing.Size(21, 21)
        Me.objbtnFilter_Action.TabIndex = 463
        '
        'objResetFinalEmployee
        '
        Me.objResetFinalEmployee.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objResetFinalEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objResetFinalEmployee.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objResetFinalEmployee.Image = CType(resources.GetObject("objResetFinalEmployee.Image"), System.Drawing.Image)
        Me.objResetFinalEmployee.Location = New System.Drawing.Point(305, 0)
        Me.objResetFinalEmployee.Name = "objResetFinalEmployee"
        Me.objResetFinalEmployee.ResultMessage = ""
        Me.objResetFinalEmployee.SearchMessage = ""
        Me.objResetFinalEmployee.Size = New System.Drawing.Size(24, 24)
        Me.objResetFinalEmployee.TabIndex = 454
        Me.objResetFinalEmployee.TabStop = False
        Me.objResetFinalEmployee.Visible = False
        '
        'objSearchFinalEmployee
        '
        Me.objSearchFinalEmployee.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSearchFinalEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objSearchFinalEmployee.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objSearchFinalEmployee.Image = CType(resources.GetObject("objSearchFinalEmployee.Image"), System.Drawing.Image)
        Me.objSearchFinalEmployee.Location = New System.Drawing.Point(281, 0)
        Me.objSearchFinalEmployee.Name = "objSearchFinalEmployee"
        Me.objSearchFinalEmployee.ResultMessage = ""
        Me.objSearchFinalEmployee.SearchMessage = ""
        Me.objSearchFinalEmployee.Size = New System.Drawing.Size(24, 24)
        Me.objSearchFinalEmployee.TabIndex = 453
        Me.objSearchFinalEmployee.TabStop = False
        Me.objSearchFinalEmployee.Visible = False
        '
        'cboAppraisalMode
        '
        Me.cboAppraisalMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppraisalMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppraisalMode.FormattingEnabled = True
        Me.cboAppraisalMode.Location = New System.Drawing.Point(99, 30)
        Me.cboAppraisalMode.Name = "cboAppraisalMode"
        Me.cboAppraisalMode.Size = New System.Drawing.Size(200, 21)
        Me.cboAppraisalMode.TabIndex = 438
        '
        'lblAppraisalMode
        '
        Me.lblAppraisalMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppraisalMode.Location = New System.Drawing.Point(8, 32)
        Me.lblAppraisalMode.Name = "lblAppraisalMode"
        Me.lblAppraisalMode.Size = New System.Drawing.Size(85, 17)
        Me.lblAppraisalMode.TabIndex = 434
        Me.lblAppraisalMode.Text = "Appraisal Action"
        Me.lblAppraisalMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbApplyAppraisalMode
        '
        Me.gbApplyAppraisalMode.BorderColor = System.Drawing.Color.Black
        Me.gbApplyAppraisalMode.Checked = False
        Me.gbApplyAppraisalMode.CollapseAllExceptThis = False
        Me.gbApplyAppraisalMode.CollapsedHoverImage = Nothing
        Me.gbApplyAppraisalMode.CollapsedNormalImage = Nothing
        Me.gbApplyAppraisalMode.CollapsedPressedImage = Nothing
        Me.gbApplyAppraisalMode.CollapseOnLoad = False
        Me.gbApplyAppraisalMode.Controls.Add(Me.objbtnApply_Action)
        Me.gbApplyAppraisalMode.Controls.Add(Me.lnkApplyAction)
        Me.gbApplyAppraisalMode.Controls.Add(Me.cboApplyAppMode)
        Me.gbApplyAppraisalMode.Controls.Add(Me.lblApplyAppMode)
        Me.gbApplyAppraisalMode.ExpandedHoverImage = Nothing
        Me.gbApplyAppraisalMode.ExpandedNormalImage = Nothing
        Me.gbApplyAppraisalMode.ExpandedPressedImage = Nothing
        Me.gbApplyAppraisalMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplyAppraisalMode.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplyAppraisalMode.HeaderHeight = 25
        Me.gbApplyAppraisalMode.HeaderMessage = ""
        Me.gbApplyAppraisalMode.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApplyAppraisalMode.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplyAppraisalMode.HeightOnCollapse = 0
        Me.gbApplyAppraisalMode.LeftTextSpace = 0
        Me.gbApplyAppraisalMode.Location = New System.Drawing.Point(408, 280)
        Me.gbApplyAppraisalMode.Name = "gbApplyAppraisalMode"
        Me.gbApplyAppraisalMode.OpenHeight = 300
        Me.gbApplyAppraisalMode.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplyAppraisalMode.ShowBorder = True
        Me.gbApplyAppraisalMode.ShowCheckBox = False
        Me.gbApplyAppraisalMode.ShowCollapseButton = False
        Me.gbApplyAppraisalMode.ShowDefaultBorderColor = True
        Me.gbApplyAppraisalMode.ShowDownButton = False
        Me.gbApplyAppraisalMode.ShowHeader = True
        Me.gbApplyAppraisalMode.Size = New System.Drawing.Size(359, 57)
        Me.gbApplyAppraisalMode.TabIndex = 454
        Me.gbApplyAppraisalMode.Temp = 0
        Me.gbApplyAppraisalMode.Text = "Apply Appraisal Action"
        Me.gbApplyAppraisalMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnApply_Action
        '
        Me.objbtnApply_Action.BackColor = System.Drawing.Color.Transparent
        Me.objbtnApply_Action.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnApply_Action.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnApply_Action.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnApply_Action.BorderSelected = False
        Me.objbtnApply_Action.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnApply_Action.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnApply_Action.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnApply_Action.Location = New System.Drawing.Point(328, 30)
        Me.objbtnApply_Action.Name = "objbtnApply_Action"
        Me.objbtnApply_Action.Size = New System.Drawing.Size(21, 21)
        Me.objbtnApply_Action.TabIndex = 458
        '
        'lnkApplyAction
        '
        Me.lnkApplyAction.BackColor = System.Drawing.Color.Transparent
        Me.lnkApplyAction.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkApplyAction.Location = New System.Drawing.Point(256, 4)
        Me.lnkApplyAction.Name = "lnkApplyAction"
        Me.lnkApplyAction.Size = New System.Drawing.Size(98, 17)
        Me.lnkApplyAction.TabIndex = 453
        Me.lnkApplyAction.TabStop = True
        Me.lnkApplyAction.Text = "Apply Action"
        Me.lnkApplyAction.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboApplyAppMode
        '
        Me.cboApplyAppMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplyAppMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplyAppMode.FormattingEnabled = True
        Me.cboApplyAppMode.Location = New System.Drawing.Point(117, 30)
        Me.cboApplyAppMode.Name = "cboApplyAppMode"
        Me.cboApplyAppMode.Size = New System.Drawing.Size(205, 21)
        Me.cboApplyAppMode.TabIndex = 438
        '
        'lblApplyAppMode
        '
        Me.lblApplyAppMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplyAppMode.Location = New System.Drawing.Point(12, 32)
        Me.lblApplyAppMode.Name = "lblApplyAppMode"
        Me.lblApplyAppMode.Size = New System.Drawing.Size(99, 17)
        Me.lblApplyAppMode.TabIndex = 434
        Me.lblApplyAppMode.Text = "Appraisal Action"
        Me.lblApplyAppMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objCheckAll
        '
        Me.objCheckAll.AutoSize = True
        Me.objCheckAll.Location = New System.Drawing.Point(14, 102)
        Me.objCheckAll.Name = "objCheckAll"
        Me.objCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objCheckAll.TabIndex = 455
        Me.objCheckAll.UseVisualStyleBackColor = True
        '
        'objEMailFooter
        '
        Me.objEMailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objEMailFooter.Controls.Add(Me.btnOk)
        Me.objEMailFooter.Controls.Add(Me.btnEClose)
        Me.objEMailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objEMailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEMailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objEMailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objEMailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objEMailFooter.Location = New System.Drawing.Point(0, 462)
        Me.objEMailFooter.Name = "objEMailFooter"
        Me.objEMailFooter.Size = New System.Drawing.Size(774, 55)
        Me.objEMailFooter.TabIndex = 456
        '
        'btnOk
        '
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(573, 12)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 449
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnEClose
        '
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(672, 12)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(90, 30)
        Me.btnEClose.TabIndex = 450
        Me.btnEClose.Text = "&Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'dgShortListEmployee
        '
        Me.dgShortListEmployee.AllowUserToAddRows = False
        Me.dgShortListEmployee.AllowUserToDeleteRows = False
        Me.dgShortListEmployee.AllowUserToResizeColumns = False
        Me.dgShortListEmployee.AllowUserToResizeRows = False
        Me.dgShortListEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgShortListEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgShortListEmployee.ColumnHeadersHeight = 21
        Me.dgShortListEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgShortListEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhSelect, Me.colhEmpCode, Me.colhEmpName, Me.colhGender, Me.colhAppointdate, Me.colhEmail, Me.dgcolhJobTitle, Me.dgcolhDepartment, Me.dgcolhScore, Me.dgcolhAction})
        Me.dgShortListEmployee.Location = New System.Drawing.Point(7, 98)
        Me.dgShortListEmployee.Name = "dgShortListEmployee"
        Me.dgShortListEmployee.RowHeadersVisible = False
        Me.dgShortListEmployee.RowHeadersWidth = 25
        Me.dgShortListEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgShortListEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgShortListEmployee.Size = New System.Drawing.Size(760, 177)
        Me.dgShortListEmployee.TabIndex = 451
        '
        'objcolhSelect
        '
        Me.objcolhSelect.Frozen = True
        Me.objcolhSelect.HeaderText = ""
        Me.objcolhSelect.Name = "objcolhSelect"
        Me.objcolhSelect.Width = 25
        '
        'colhEmpCode
        '
        Me.colhEmpCode.HeaderText = "Code"
        Me.colhEmpCode.Name = "colhEmpCode"
        Me.colhEmpCode.ReadOnly = True
        Me.colhEmpCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmpCode.Width = 80
        '
        'colhEmpName
        '
        Me.colhEmpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmpName.HeaderText = "Employee"
        Me.colhEmpName.Name = "colhEmpName"
        Me.colhEmpName.ReadOnly = True
        Me.colhEmpName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhGender
        '
        Me.colhGender.HeaderText = "Gender"
        Me.colhGender.Name = "colhGender"
        Me.colhGender.ReadOnly = True
        Me.colhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhGender.Width = 60
        '
        'colhAppointdate
        '
        Me.colhAppointdate.HeaderText = "Appoint. Date"
        Me.colhAppointdate.Name = "colhAppointdate"
        Me.colhAppointdate.ReadOnly = True
        Me.colhAppointdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAppointdate.Width = 80
        '
        'colhEmail
        '
        Me.colhEmail.HeaderText = "Email"
        Me.colhEmail.Name = "colhEmail"
        Me.colhEmail.ReadOnly = True
        Me.colhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmail.Visible = False
        Me.colhEmail.Width = 150
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.ReadOnly = True
        Me.dgcolhDepartment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDepartment.Width = 90
        '
        'dgcolhScore
        '
        Me.dgcolhScore.HeaderText = "Score"
        Me.dgcolhScore.Name = "dgcolhScore"
        Me.dgcolhScore.ReadOnly = True
        '
        'dgcolhAction
        '
        Me.dgcolhAction.HeaderText = "Action"
        Me.dgcolhAction.Name = "dgcolhAction"
        Me.dgcolhAction.ReadOnly = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnOperations)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.tabAppEmployee)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 517)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(774, 55)
        Me.EZeeFooter1.TabIndex = 446
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(7, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 449
        Me.btnOperations.Text = "&Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSalaryIncrement, Me.mnuVoidSalaryIncrement, Me.mnuRemider, Me.mnuBonus, Me.mnuVoidBonus})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(189, 114)
        '
        'mnuSalaryIncrement
        '
        Me.mnuSalaryIncrement.Name = "mnuSalaryIncrement"
        Me.mnuSalaryIncrement.Size = New System.Drawing.Size(188, 22)
        Me.mnuSalaryIncrement.Text = "&Salary Increment"
        '
        'mnuVoidSalaryIncrement
        '
        Me.mnuVoidSalaryIncrement.Name = "mnuVoidSalaryIncrement"
        Me.mnuVoidSalaryIncrement.Size = New System.Drawing.Size(188, 22)
        Me.mnuVoidSalaryIncrement.Text = "&Void Salary Increment"
        '
        'mnuRemider
        '
        Me.mnuRemider.Name = "mnuRemider"
        Me.mnuRemider.Size = New System.Drawing.Size(188, 22)
        Me.mnuRemider.Text = "&Reminder"
        '
        'mnuBonus
        '
        Me.mnuBonus.Name = "mnuBonus"
        Me.mnuBonus.Size = New System.Drawing.Size(188, 22)
        Me.mnuBonus.Tag = "mnuBonus"
        Me.mnuBonus.Text = "&Bonus"
        '
        'mnuVoidBonus
        '
        Me.mnuVoidBonus.Name = "mnuVoidBonus"
        Me.mnuVoidBonus.Size = New System.Drawing.Size(188, 22)
        Me.mnuVoidBonus.Tag = "mnuVoidBonus"
        Me.mnuVoidBonus.Text = "Void Bonus"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(576, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 447
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'tabAppEmployee
        '
        Me.tabAppEmployee.Controls.Add(Me.tabpgShotList)
        Me.tabAppEmployee.Controls.Add(Me.tabpgFinal)
        Me.tabAppEmployee.Location = New System.Drawing.Point(121, 13)
        Me.tabAppEmployee.Name = "tabAppEmployee"
        Me.tabAppEmployee.SelectedIndex = 0
        Me.tabAppEmployee.Size = New System.Drawing.Size(195, 30)
        Me.tabAppEmployee.TabIndex = 455
        Me.tabAppEmployee.Visible = False
        '
        'tabpgShotList
        '
        Me.tabpgShotList.Location = New System.Drawing.Point(4, 22)
        Me.tabpgShotList.Name = "tabpgShotList"
        Me.tabpgShotList.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgShotList.Size = New System.Drawing.Size(187, 4)
        Me.tabpgShotList.TabIndex = 0
        Me.tabpgShotList.Text = "Appraisal Analysis"
        Me.tabpgShotList.UseVisualStyleBackColor = True
        '
        'tabpgFinal
        '
        Me.tabpgFinal.Location = New System.Drawing.Point(4, 22)
        Me.tabpgFinal.Name = "tabpgFinal"
        Me.tabpgFinal.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpgFinal.Size = New System.Drawing.Size(187, 4)
        Me.tabpgFinal.TabIndex = 1
        Me.tabpgFinal.Text = "Appraisal Actions"
        Me.tabpgFinal.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(672, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 448
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicant Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 60
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "BirthDate"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.HeaderText = "Applicant Code"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn10.HeaderText = "BirthDate"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        Me.DataGridViewTextBoxColumn11.Width = 150
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn12.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Mode"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        Me.DataGridViewTextBoxColumn13.Width = 90
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Width = 80
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        Me.DataGridViewTextBoxColumn15.Width = 90
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Score"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Action"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Width = 90
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Score"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Action"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        '
        'txtSearchShortListed
        '
        Me.txtSearchShortListed.Location = New System.Drawing.Point(384, 98)
        Me.txtSearchShortListed.Name = "txtSearchShortListed"
        Me.txtSearchShortListed.Size = New System.Drawing.Size(383, 21)
        Me.txtSearchShortListed.TabIndex = 460
        Me.txtSearchShortListed.Visible = False
        '
        'frmAppraisals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 572)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppraisals"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appraisals"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAppdate.ResumeLayout(False)
        Me.pnlGradeWise.ResumeLayout(False)
        Me.pnlGradeWise.PerformLayout()
        Me.pnlRefWise.ResumeLayout(False)
        Me.pnlRefWise.PerformLayout()
        CType(Me.dgFinalEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFinalEmployee.ResumeLayout(False)
        CType(Me.objResetFinalEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objSearchFinalEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbApplyAppraisalMode.ResumeLayout(False)
        Me.objEMailFooter.ResumeLayout(False)
        CType(Me.dgShortListEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.tabAppEmployee.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lnkApplyAction As System.Windows.Forms.LinkLabel
    Friend WithEvents dgFinalEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents dgShortListEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOpeningDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboRefNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReferenceDate As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents tabAppEmployee As System.Windows.Forms.TabControl
    Friend WithEvents tabpgShotList As System.Windows.Forms.TabPage
    Friend WithEvents tabpgFinal As System.Windows.Forms.TabPage
    Friend WithEvents objCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents gbFinalEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objResetFinalEmployee As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objSearchFinalEmployee As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboAppraisalMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblAppraisalMode As System.Windows.Forms.Label
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSalaryIncrement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbApplyAppraisalMode As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboApplyAppMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplyAppMode As System.Windows.Forms.Label
    Friend WithEvents objFinalCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents mnuVoidSalaryIncrement As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRemider As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objEMailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtndown As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnFilter_Action As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnApply_Action As eZee.Common.eZeeGradientButton
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlRefWise As System.Windows.Forms.Panel
    Friend WithEvents pnlGradeWise As System.Windows.Forms.Panel
    Friend WithEvents lblGrades As System.Windows.Forms.Label
    Friend WithEvents cboGrades_Award As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGrades As eZee.Common.eZeeGradientButton
    Friend WithEvents txtScoreTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalScoreT As System.Windows.Forms.Label
    Friend WithEvents lblTotalScoreF As System.Windows.Forms.Label
    Friend WithEvents txtScoreFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents radByReference As System.Windows.Forms.RadioButton
    Friend WithEvents radByGrades As System.Windows.Forms.RadioButton
    Friend WithEvents objcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAction As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlAppdate As System.Windows.Forms.Panel
    Friend WithEvents lblAppointment As System.Windows.Forms.Label
    Friend WithEvents cboAppointment As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblFinalResult As System.Windows.Forms.Label
    Friend WithEvents cboFinalResult As System.Windows.Forms.ComboBox
    Friend WithEvents mnuBonus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVoidBonus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhFinalSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhFinalEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhfinal_Job As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhfinalDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalMode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOperation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIncrement As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtSearchFinal As System.Windows.Forms.TextBox
    Friend WithEvents txtSearchShortListed As System.Windows.Forms.TextBox
End Class
