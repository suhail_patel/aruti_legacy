﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCoyFieldsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCoyFieldsList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lblGoalOwner = New System.Windows.Forms.Label
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.objbtnSearchField5 = New eZee.Common.eZeeGradientButton
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.cboFieldValue3 = New System.Windows.Forms.ComboBox
        Me.objlblField5 = New System.Windows.Forms.Label
        Me.objlblField3 = New System.Windows.Forms.Label
        Me.cboFieldValue5 = New System.Windows.Forms.ComboBox
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.objbtnSearchField3 = New eZee.Common.eZeeGradientButton
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboFieldValue4 = New System.Windows.Forms.ComboBox
        Me.objbtnSearchField4 = New eZee.Common.eZeeGradientButton
        Me.objlblField4 = New System.Windows.Forms.Label
        Me.objbtnSearchField2 = New eZee.Common.eZeeGradientButton
        Me.objlblField1 = New System.Windows.Forms.Label
        Me.cboFieldValue2 = New System.Windows.Forms.ComboBox
        Me.objlblField2 = New System.Windows.Forms.Label
        Me.objbtnSearchField1 = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboFieldValue1 = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCommit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlock = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.objbtnSearchPerspective = New eZee.Common.eZeeGradientButton
        Me.pnlMain.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objspc1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(877, 495)
        Me.pnlMain.TabIndex = 0
        '
        'objspc1
        '
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.IsSplitterFixed = True
        Me.objspc1.Location = New System.Drawing.Point(0, 0)
        Me.objspc1.Margin = New System.Windows.Forms.Padding(0)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.dgvData)
        Me.objspc1.Size = New System.Drawing.Size(877, 495)
        Me.objspc1.SplitterDistance = 163
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 6
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchOwner)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboOwner)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.lblGoalOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField5)
        Me.gbFilterCriteria.Controls.Add(Me.lblStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue3)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField5)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField3)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue5)
        Me.gbFilterCriteria.Controls.Add(Me.lblPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField3)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField4)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField1)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(877, 163)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(638, 134)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchOwner.TabIndex = 480
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(18, 29)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(206, 17)
        Me.lblPeriod.TabIndex = 478
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(12, 49)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(199, 21)
        Me.cboPeriod.TabIndex = 479
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 300
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(453, 134)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(179, 21)
        Me.cboOwner.TabIndex = 473
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(456, 115)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(176, 17)
        Me.objlblCaption.TabIndex = 474
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(242, 134)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(179, 21)
        Me.cboAllocations.TabIndex = 471
        '
        'lblGoalOwner
        '
        Me.lblGoalOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalOwner.Location = New System.Drawing.Point(242, 115)
        Me.lblGoalOwner.Name = "lblGoalOwner"
        Me.lblGoalOwner.Size = New System.Drawing.Size(179, 17)
        Me.lblGoalOwner.TabIndex = 472
        Me.lblGoalOwner.Text = "Goal Owner"
        Me.lblGoalOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.DropDownWidth = 250
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(12, 91)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(199, 21)
        Me.cboPerspective.TabIndex = 6
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(124, 134)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpEndDate.TabIndex = 82
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(12, 134)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpStartDate.TabIndex = 81
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(121, 115)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(103, 16)
        Me.lblEndDate.TabIndex = 84
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField5
        '
        Me.objbtnSearchField5.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField5.BorderSelected = False
        Me.objbtnSearchField5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField5.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField5.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField5.Location = New System.Drawing.Point(849, 49)
        Me.objbtnSearchField5.Name = "objbtnSearchField5"
        Me.objbtnSearchField5.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchField5.TabIndex = 469
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(12, 115)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(96, 16)
        Me.lblStartDate.TabIndex = 83
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue3
        '
        Me.cboFieldValue3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue3.DropDownWidth = 250
        Me.cboFieldValue3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue3.FormattingEnabled = True
        Me.cboFieldValue3.Location = New System.Drawing.Point(453, 49)
        Me.cboFieldValue3.Name = "cboFieldValue3"
        Me.cboFieldValue3.Size = New System.Drawing.Size(179, 21)
        Me.cboFieldValue3.TabIndex = 464
        '
        'objlblField5
        '
        Me.objlblField5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField5.Location = New System.Drawing.Point(671, 29)
        Me.objlblField5.Name = "objlblField5"
        Me.objlblField5.Size = New System.Drawing.Size(179, 16)
        Me.objlblField5.TabIndex = 468
        Me.objlblField5.Text = "#Caption"
        Me.objlblField5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblField3
        '
        Me.objlblField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField3.Location = New System.Drawing.Point(457, 29)
        Me.objlblField3.Name = "objlblField3"
        Me.objlblField3.Size = New System.Drawing.Size(179, 16)
        Me.objlblField3.TabIndex = 465
        Me.objlblField3.Text = "#Caption"
        Me.objlblField3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue5
        '
        Me.cboFieldValue5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue5.DropDownWidth = 250
        Me.cboFieldValue5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue5.FormattingEnabled = True
        Me.cboFieldValue5.Location = New System.Drawing.Point(664, 49)
        Me.cboFieldValue5.Name = "cboFieldValue5"
        Me.cboFieldValue5.Size = New System.Drawing.Size(179, 21)
        Me.cboFieldValue5.TabIndex = 467
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(12, 73)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(179, 15)
        Me.lblPerspective.TabIndex = 5
        Me.lblPerspective.Text = "Perspective"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField3
        '
        Me.objbtnSearchField3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField3.BorderSelected = False
        Me.objbtnSearchField3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField3.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField3.Location = New System.Drawing.Point(638, 49)
        Me.objbtnSearchField3.Name = "objbtnSearchField3"
        Me.objbtnSearchField3.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchField3.TabIndex = 466
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(664, 91)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(179, 21)
        Me.cboStatus.TabIndex = 71
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(668, 72)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(179, 16)
        Me.lblStatus.TabIndex = 72
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue4
        '
        Me.cboFieldValue4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue4.DropDownWidth = 250
        Me.cboFieldValue4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue4.FormattingEnabled = True
        Me.cboFieldValue4.Location = New System.Drawing.Point(453, 91)
        Me.cboFieldValue4.Name = "cboFieldValue4"
        Me.cboFieldValue4.Size = New System.Drawing.Size(179, 21)
        Me.cboFieldValue4.TabIndex = 464
        '
        'objbtnSearchField4
        '
        Me.objbtnSearchField4.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField4.BorderSelected = False
        Me.objbtnSearchField4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField4.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField4.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField4.Location = New System.Drawing.Point(638, 91)
        Me.objbtnSearchField4.Name = "objbtnSearchField4"
        Me.objbtnSearchField4.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchField4.TabIndex = 466
        '
        'objlblField4
        '
        Me.objlblField4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField4.Location = New System.Drawing.Point(457, 72)
        Me.objlblField4.Name = "objlblField4"
        Me.objlblField4.Size = New System.Drawing.Size(179, 16)
        Me.objlblField4.TabIndex = 465
        Me.objlblField4.Text = "#Caption"
        Me.objlblField4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField2
        '
        Me.objbtnSearchField2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField2.BorderSelected = False
        Me.objbtnSearchField2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField2.Location = New System.Drawing.Point(427, 91)
        Me.objbtnSearchField2.Name = "objbtnSearchField2"
        Me.objbtnSearchField2.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchField2.TabIndex = 466
        '
        'objlblField1
        '
        Me.objlblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField1.Location = New System.Drawing.Point(246, 29)
        Me.objlblField1.Name = "objlblField1"
        Me.objlblField1.Size = New System.Drawing.Size(168, 16)
        Me.objlblField1.TabIndex = 465
        Me.objlblField1.Text = "#Caption"
        Me.objlblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue2
        '
        Me.cboFieldValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue2.DropDownWidth = 250
        Me.cboFieldValue2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue2.FormattingEnabled = True
        Me.cboFieldValue2.Location = New System.Drawing.Point(242, 91)
        Me.cboFieldValue2.Name = "cboFieldValue2"
        Me.cboFieldValue2.Size = New System.Drawing.Size(179, 21)
        Me.cboFieldValue2.TabIndex = 464
        '
        'objlblField2
        '
        Me.objlblField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField2.Location = New System.Drawing.Point(246, 72)
        Me.objlblField2.Name = "objlblField2"
        Me.objlblField2.Size = New System.Drawing.Size(168, 16)
        Me.objlblField2.TabIndex = 465
        Me.objlblField2.Text = "#Caption"
        Me.objlblField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField1
        '
        Me.objbtnSearchField1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField1.BorderSelected = False
        Me.objbtnSearchField1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField1.Location = New System.Drawing.Point(427, 49)
        Me.objbtnSearchField1.Name = "objbtnSearchField1"
        Me.objbtnSearchField1.Size = New System.Drawing.Size(20, 20)
        Me.objbtnSearchField1.TabIndex = 466
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(850, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'cboFieldValue1
        '
        Me.cboFieldValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue1.DropDownWidth = 250
        Me.cboFieldValue1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue1.FormattingEnabled = True
        Me.cboFieldValue1.Location = New System.Drawing.Point(242, 49)
        Me.cboFieldValue1.Name = "cboFieldValue1"
        Me.cboFieldValue1.Size = New System.Drawing.Size(179, 21)
        Me.cboFieldValue1.TabIndex = 464
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(825, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(877, 330)
        Me.dgvData.TabIndex = 5
        '
        'objdgcolhAdd
        '
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 495)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(877, 55)
        Me.objFooter.TabIndex = 4
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(549, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(213, 13)
        Me.objlblTotalWeight.TabIndex = 12
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(103, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 11
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCommit, Me.mnuUnlock})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(176, 48)
        '
        'mnuCommit
        '
        Me.mnuCommit.Name = "mnuCommit"
        Me.mnuCommit.Size = New System.Drawing.Size(175, 22)
        Me.mnuCommit.Tag = "mnuCommit"
        Me.mnuCommit.Text = "C&ommit"
        '
        'mnuUnlock
        '
        Me.mnuUnlock.Name = "mnuUnlock"
        Me.mnuUnlock.Size = New System.Drawing.Size(175, 22)
        Me.mnuUnlock.Tag = "mnuUnlock"
        Me.mnuUnlock.Text = "&Unlock Committed"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(768, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnSearchPerspective
        '
        Me.objbtnSearchPerspective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPerspective.BorderSelected = False
        Me.objbtnSearchPerspective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPerspective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPerspective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPerspective.Location = New System.Drawing.Point(215, 91)
        Me.objbtnSearchPerspective.Name = "objbtnSearchPerspective"
        Me.objbtnSearchPerspective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPerspective.TabIndex = 487
        '
        'frmCoyFieldsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(877, 550)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCoyFieldsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Company Goals List"
        Me.pnlMain.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objlblField1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField1 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFieldValue1 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchField2 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFieldValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField2 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField3 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField3 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFieldValue5 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField5 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField5 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFieldValue4 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchField4 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField4 As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCommit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents lblGoalOwner As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPerspective As eZee.Common.eZeeGradientButton
End Class
