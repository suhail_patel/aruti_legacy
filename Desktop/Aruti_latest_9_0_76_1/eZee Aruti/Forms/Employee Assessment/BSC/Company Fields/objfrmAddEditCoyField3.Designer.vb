﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmAddEditCoyField3
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(objfrmAddEditCoyField3))
        Me.objlblField1 = New System.Windows.Forms.Label
        Me.txtFieldValue1 = New System.Windows.Forms.TextBox
        Me.objbtnSearchField2 = New eZee.Common.eZeeGradientButton
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblField3 = New System.Windows.Forms.Label
        Me.objlblField2 = New System.Windows.Forms.Label
        Me.txtFieldValue3 = New System.Windows.Forms.TextBox
        Me.cboFieldValue2 = New System.Windows.Forms.ComboBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblWeight = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.objcolhAllocations = New System.Windows.Forms.ColumnHeader
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lblGoalOwner = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.objtabcRemarks = New System.Windows.Forms.TabControl
        Me.objtabpRemark1 = New System.Windows.Forms.TabPage
        Me.txtRemark1 = New System.Windows.Forms.TextBox
        Me.objtabpRemark2 = New System.Windows.Forms.TabPage
        Me.txtRemark2 = New System.Windows.Forms.TextBox
        Me.objtabpRemark3 = New System.Windows.Forms.TabPage
        Me.txtRemark3 = New System.Windows.Forms.TextBox
        Me.txtPeriod = New System.Windows.Forms.TextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        Me.objtabcRemarks.SuspendLayout()
        Me.objtabpRemark1.SuspendLayout()
        Me.objtabpRemark2.SuspendLayout()
        Me.objtabpRemark3.SuspendLayout()
        Me.SuspendLayout()
        '
        'objlblField1
        '
        Me.objlblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField1.Location = New System.Drawing.Point(12, 53)
        Me.objlblField1.Name = "objlblField1"
        Me.objlblField1.Size = New System.Drawing.Size(310, 17)
        Me.objlblField1.TabIndex = 464
        Me.objlblField1.Text = "#Caption"
        Me.objlblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFieldValue1
        '
        Me.txtFieldValue1.BackColor = System.Drawing.Color.White
        Me.txtFieldValue1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFieldValue1.Location = New System.Drawing.Point(12, 73)
        Me.txtFieldValue1.Name = "txtFieldValue1"
        Me.txtFieldValue1.ReadOnly = True
        Me.txtFieldValue1.Size = New System.Drawing.Size(310, 21)
        Me.txtFieldValue1.TabIndex = 465
        '
        'objbtnSearchField2
        '
        Me.objbtnSearchField2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField2.BorderSelected = False
        Me.objbtnSearchField2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField2.Location = New System.Drawing.Point(328, 118)
        Me.objbtnSearchField2.Name = "objbtnSearchField2"
        Me.objbtnSearchField2.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchField2.TabIndex = 463
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 379)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(658, 55)
        Me.EZeeFooter1.TabIndex = 461
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(452, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(552, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objlblField3
        '
        Me.objlblField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField3.Location = New System.Drawing.Point(12, 141)
        Me.objlblField3.Name = "objlblField3"
        Me.objlblField3.Size = New System.Drawing.Size(310, 17)
        Me.objlblField3.TabIndex = 443
        Me.objlblField3.Text = "#Caption"
        '
        'objlblField2
        '
        Me.objlblField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField2.Location = New System.Drawing.Point(12, 97)
        Me.objlblField2.Name = "objlblField2"
        Me.objlblField2.Size = New System.Drawing.Size(310, 17)
        Me.objlblField2.TabIndex = 338
        Me.objlblField2.Text = "#Caption"
        Me.objlblField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFieldValue3
        '
        Me.txtFieldValue3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFieldValue3.Location = New System.Drawing.Point(12, 161)
        Me.txtFieldValue3.Multiline = True
        Me.txtFieldValue3.Name = "txtFieldValue3"
        Me.txtFieldValue3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFieldValue3.Size = New System.Drawing.Size(310, 97)
        Me.txtFieldValue3.TabIndex = 442
        '
        'cboFieldValue2
        '
        Me.cboFieldValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue2.FormattingEnabled = True
        Me.cboFieldValue2.Location = New System.Drawing.Point(12, 117)
        Me.cboFieldValue2.Name = "cboFieldValue2"
        Me.cboFieldValue2.Size = New System.Drawing.Size(310, 21)
        Me.cboFieldValue2.TabIndex = 337
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.objchkAll)
        Me.objpnlData.Controls.Add(Me.dtpStartDate)
        Me.objpnlData.Controls.Add(Me.lblWeight)
        Me.objpnlData.Controls.Add(Me.lblEndDate)
        Me.objpnlData.Controls.Add(Me.txtWeight)
        Me.objpnlData.Controls.Add(Me.lblStartDate)
        Me.objpnlData.Controls.Add(Me.dtpEndDate)
        Me.objpnlData.Controls.Add(Me.cboStatus)
        Me.objpnlData.Controls.Add(Me.lvAllocation)
        Me.objpnlData.Controls.Add(Me.lblStatus)
        Me.objpnlData.Controls.Add(Me.cboAllocations)
        Me.objpnlData.Controls.Add(Me.lblGoalOwner)
        Me.objpnlData.Controls.Add(Me.txtSearch)
        Me.objpnlData.Location = New System.Drawing.Point(354, 5)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(292, 369)
        Me.objpnlData.TabIndex = 466
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(271, 142)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 74
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(3, 24)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpStartDate.TabIndex = 77
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(239, 4)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(50, 17)
        Me.lblWeight.TabIndex = 458
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(121, 4)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(112, 17)
        Me.lblEndDate.TabIndex = 80
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(239, 24)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(50, 21)
        Me.txtWeight.TabIndex = 459
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(4, 4)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(111, 17)
        Me.lblStartDate.TabIndex = 79
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(121, 24)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpEndDate.TabIndex = 78
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(3, 68)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(286, 21)
        Me.cboStatus.TabIndex = 69
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(3, 167)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(286, 199)
        Me.lvAllocation.TabIndex = 73
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'objcolhAllocations
        '
        Me.objcolhAllocations.Tag = "objcolhAllocations"
        Me.objcolhAllocations.Text = ""
        Me.objcolhAllocations.Width = 235
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(4, 48)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(285, 17)
        Me.lblStatus.TabIndex = 70
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(3, 112)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(286, 21)
        Me.cboAllocations.TabIndex = 71
        '
        'lblGoalOwner
        '
        Me.lblGoalOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalOwner.Location = New System.Drawing.Point(4, 92)
        Me.lblGoalOwner.Name = "lblGoalOwner"
        Me.lblGoalOwner.Size = New System.Drawing.Size(285, 17)
        Me.lblGoalOwner.TabIndex = 72
        Me.lblGoalOwner.Text = "Goal Owner"
        Me.lblGoalOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(3, 139)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(262, 21)
        Me.txtSearch.TabIndex = 75
        '
        'objtabcRemarks
        '
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark1)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark2)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark3)
        Me.objtabcRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objtabcRemarks.Location = New System.Drawing.Point(12, 264)
        Me.objtabcRemarks.Name = "objtabcRemarks"
        Me.objtabcRemarks.SelectedIndex = 0
        Me.objtabcRemarks.Size = New System.Drawing.Size(310, 110)
        Me.objtabcRemarks.TabIndex = 460
        '
        'objtabpRemark1
        '
        Me.objtabpRemark1.Controls.Add(Me.txtRemark1)
        Me.objtabpRemark1.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark1.Name = "objtabpRemark1"
        Me.objtabpRemark1.Size = New System.Drawing.Size(302, 84)
        Me.objtabpRemark1.TabIndex = 0
        Me.objtabpRemark1.Tag = "objtabpRemark1"
        Me.objtabpRemark1.UseVisualStyleBackColor = True
        '
        'txtRemark1
        '
        Me.txtRemark1.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark1.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark1.Multiline = True
        Me.txtRemark1.Name = "txtRemark1"
        Me.txtRemark1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark1.Size = New System.Drawing.Size(302, 84)
        Me.txtRemark1.TabIndex = 443
        '
        'objtabpRemark2
        '
        Me.objtabpRemark2.Controls.Add(Me.txtRemark2)
        Me.objtabpRemark2.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark2.Name = "objtabpRemark2"
        Me.objtabpRemark2.Size = New System.Drawing.Size(281, 99)
        Me.objtabpRemark2.TabIndex = 1
        Me.objtabpRemark2.Tag = "objtabpRemark2"
        Me.objtabpRemark2.UseVisualStyleBackColor = True
        '
        'txtRemark2
        '
        Me.txtRemark2.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark2.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark2.Multiline = True
        Me.txtRemark2.Name = "txtRemark2"
        Me.txtRemark2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark2.Size = New System.Drawing.Size(281, 99)
        Me.txtRemark2.TabIndex = 444
        '
        'objtabpRemark3
        '
        Me.objtabpRemark3.Controls.Add(Me.txtRemark3)
        Me.objtabpRemark3.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark3.Name = "objtabpRemark3"
        Me.objtabpRemark3.Size = New System.Drawing.Size(281, 99)
        Me.objtabpRemark3.TabIndex = 2
        Me.objtabpRemark3.Tag = "objtabpRemark3"
        Me.objtabpRemark3.UseVisualStyleBackColor = True
        '
        'txtRemark3
        '
        Me.txtRemark3.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark3.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark3.Multiline = True
        Me.txtRemark3.Name = "txtRemark3"
        Me.txtRemark3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark3.Size = New System.Drawing.Size(281, 99)
        Me.txtRemark3.TabIndex = 444
        '
        'txtPeriod
        '
        Me.txtPeriod.BackColor = System.Drawing.Color.White
        Me.txtPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriod.Location = New System.Drawing.Point(12, 29)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.ReadOnly = True
        Me.txtPeriod.Size = New System.Drawing.Size(310, 21)
        Me.txtPeriod.TabIndex = 482
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 9)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(310, 17)
        Me.lblPeriod.TabIndex = 481
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objfrmAddEditCoyField3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 434)
        Me.Controls.Add(Me.txtPeriod)
        Me.Controls.Add(Me.lblPeriod)
        Me.Controls.Add(Me.objtabcRemarks)
        Me.Controls.Add(Me.objpnlData)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.objbtnSearchField2)
        Me.Controls.Add(Me.objlblField3)
        Me.Controls.Add(Me.objlblField1)
        Me.Controls.Add(Me.txtFieldValue3)
        Me.Controls.Add(Me.txtFieldValue1)
        Me.Controls.Add(Me.cboFieldValue2)
        Me.Controls.Add(Me.objlblField2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmAddEditCoyField3"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.EZeeFooter1.ResumeLayout(False)
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        Me.objtabcRemarks.ResumeLayout(False)
        Me.objtabpRemark1.ResumeLayout(False)
        Me.objtabpRemark1.PerformLayout()
        Me.objtabpRemark2.ResumeLayout(False)
        Me.objtabpRemark2.PerformLayout()
        Me.objtabpRemark3.ResumeLayout(False)
        Me.objtabpRemark3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents objbtnSearchField2 As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objlblField3 As System.Windows.Forms.Label
    Friend WithEvents objlblField2 As System.Windows.Forms.Label
    Friend WithEvents txtFieldValue3 As System.Windows.Forms.TextBox
    Friend WithEvents cboFieldValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents txtFieldValue1 As System.Windows.Forms.TextBox
    Friend WithEvents objlblField1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents objcolhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents lblGoalOwner As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents objtabcRemarks As System.Windows.Forms.TabControl
    Friend WithEvents objtabpRemark1 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark1 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark2 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark2 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark3 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
End Class
