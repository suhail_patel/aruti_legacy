﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmAddEditCoyField4

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "objfrmAddEditCoyField4"
    Private mblnCancel As Boolean = True
    Private mintCoyField4Unkid As Integer = 0
    Private objCoyField4 As clsassess_coyfield4_master
    Private objCoyOwner As clsassess_coyowner_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtOwner As DataTable
    Private mintFieldUnkid As Integer
    Private objFieldMaster As New clsAssess_Field_Master(True)
    Private mdicFieldData As New Dictionary(Of Integer, String)
    Private objWSetting As New clsWeight_Setting(True)
    Private mintParentId As Integer = 0
    Private mblnDropDownClosed As Boolean = False
    Private mintSelectedPeriodId As Integer = 0
    Private mintLinkedFieldId As Integer = 0
    Private mintMaintParentId As Integer = 0
    Private iOwnerRefId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal intFieldId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  Optional ByVal iParentId As Integer = 0, _
                                  Optional ByVal iMainParentId As Integer = 0) As Boolean
        Try
            mintCoyField4Unkid = intUnkId
            mintFieldUnkid = intFieldId
            mintParentId = iParentId
            mintSelectedPeriodId = iPeriodId
            menAction = eAction
            mintMaintParentId = iMainParentId

            objtabcRemarks.Enabled = False
            objpnlData.Enabled = False

            Me.ShowDialog()

            intUnkId = mintCoyField4Unkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Form_Information()
        Try
            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field4_Caption & " " & _
                      Language.getMessage(mstrModuleName, 11, "Information")

            objlblField1.Text = objFieldMaster._Field1_Caption
            objlblField2.Text = objFieldMaster._Field2_Caption
            objlblField3.Text = objFieldMaster._Field3_Caption
            cboFieldValue3.Tag = objFieldMaster._Field3Unkid

            objlblField4.Text = objFieldMaster._Field4_Caption
            txtFieldValue4.Tag = objFieldMaster._Field4Unkid

            If mintFieldUnkid = mintLinkedFieldId Then
                If objFieldMaster._Field6_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark1)
                Else
                    objtabpRemark1.Text = objFieldMaster._Field6_Caption
                    txtRemark1.Tag = objFieldMaster._Field6Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark2)
                Else
                    objtabpRemark2.Text = objFieldMaster._Field7_Caption
                    txtRemark2.Tag = objFieldMaster._Field7Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark3)
                Else
                    objtabpRemark3.Text = objFieldMaster._Field8_Caption
                    txtRemark3.Tag = objFieldMaster._Field8Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicFieldData.Keys.Count > 0 Then objtabcRemarks.Enabled = True
            Else
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Information", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim objCoyField3 As New clsassess_coyfield3_master
        Dim dsList As New DataSet
        Try
            RemoveHandler cboFieldValue3.SelectedIndexChanged, AddressOf cboFieldValue3_SelectedIndexChanged
            dsList = objCoyField3.getComboList("List", True, mintMaintParentId)
            With cboFieldValue3
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            cboFieldValue3.DrawMode = DrawMode.OwnerDrawFixed
            AddHandler cboFieldValue3.DrawItem, AddressOf cboFieldValue3_DrawItem
            AddHandler cboFieldValue3.DropDownClosed, AddressOf cboFieldValue3_DropDownClosed
            AddHandler cboFieldValue3.SelectedIndexChanged, AddressOf cboFieldValue3_SelectedIndexChanged

            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtFieldValue4.BackColor = GUI.ColorComp
            cboAllocations.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
            cboFieldValue3.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)
                If mdtOwner.Rows.Count > 0 Then
                    Dim dRow As DataRow() = mdtOwner.Select("allocationid = '" & CInt(dtRow.Item(StrIdColName)) & "' AND AUD <> 'D'")
                    If dRow.Length > 0 Then
                        lvItem.Checked = True
                    End If
                End If
                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 7 Then
                objcolhAllocations.Width = 235 - 35
            Else
                objcolhAllocations.Width = 235
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub GoalOwnerOperation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            If mdtOwner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwner.Select("allocationid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("coyfieldunkid") = mintCoyField4Unkid
                        dRow.Item("allocationid") = iTagUnkid
                        dRow.Item("coyfieldtypeid") = enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwner.Rows.Add(dRow)
                    End If
                    mdtOwner.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GoalOwnerOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim iMsg As String = String.Empty

            'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
            If CInt(cboFieldValue3.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 1, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName, 9, " is mandatory information. Please select ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName, 3, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                cboFieldValue3.Focus()
                Return False
            End If
            'End If

            If txtFieldValue4.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 1, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName, 2, " is mandatory information. Please provide ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName, 3, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                txtFieldValue4.Focus()
                Return False
            End If

            If mintFieldUnkid = mintLinkedFieldId Then

                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Weight is mandatory information. Please provide Weight to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                If txtWeight.Decimal > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, enWeight_Types.WEIGHT_FIELD4, txtWeight.Decimal, mintSelectedPeriodId, mintLinkedFieldId, 0, 0, menAction, mintCoyField4Unkid)
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        txtWeight.Focus()
                        Return False
                    End If
                End If

                'If txtWeight.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100."), enMsgBoxStyle.Information)
                '    txtWeight.Focus()
                '    Return False
                'End If

                If CInt(cboStatus.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                    cboStatus.Focus()
                    Return False
                End If

                If lvAllocation.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue."), enMsgBoxStyle.Information)
                    lvAllocation.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            objCoyField4._Coyfield3unkid = CInt(cboFieldValue3.SelectedValue)
            If dtpEndDate.Checked = True Then
                objCoyField4._Enddate = dtpEndDate.Value
            Else
                objCoyField4._Enddate = Nothing
            End If
            objCoyField4._Field_Data = txtFieldValue4.Text
            objCoyField4._Fieldunkid = mintFieldUnkid
            objCoyField4._Isvoid = False
            objCoyField4._Periodunkid = mintSelectedPeriodId
            If dtpStartDate.Checked = True Then
                objCoyField4._Startdate = dtpStartDate.Value
            Else
                objCoyField4._Startdate = Nothing
            End If
            objCoyField4._Userunkid = User._Object._Userunkid
            objCoyField4._Voiddatetime = Nothing
            objCoyField4._Voidreason = ""
            objCoyField4._Voiduserunkid = 0
            objCoyField4._Weight = txtWeight.Decimal
            objCoyField4._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD4
            If mintFieldUnkid = mintLinkedFieldId Then
                objCoyField4._Ownerrefid = CInt(cboAllocations.SelectedValue)
                objCoyField4._Statusunkid = CInt(cboStatus.SelectedValue)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objCoyField4._Enddate <> Nothing Then
                dtpEndDate.Value = objCoyField4._Enddate
                dtpEndDate.Checked = True
            End If
            txtFieldValue4.Text = objCoyField4._Field_Data
            cboAllocations.SelectedValue = IIf(objCoyField4._Ownerrefid <= 0, 1, objCoyField4._Ownerrefid)
            cboFieldValue3.SelectedValue = objCoyField4._Coyfield3unkid
            If objCoyField4._Startdate <> Nothing Then
                dtpStartDate.Value = objCoyField4._Startdate
                dtpStartDate.Checked = True
            End If
            cboStatus.SelectedValue = IIf(objCoyField4._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objCoyField4._Statusunkid)
            txtWeight.Decimal = CDec(objCoyField4._Weight)
            If menAction = enAction.EDIT_ONE Then
                Dim objCoyInfoField As New clsassess_coyinfofield_tran
                mdicFieldData = objCoyInfoField.Get_Data(mintCoyField4Unkid, enWeight_Types.WEIGHT_FIELD4)
                If mdicFieldData.Keys.Count > 0 Then
                    If mdicFieldData.ContainsKey(CInt(txtRemark1.Tag)) Then
                        txtRemark1.Text = mdicFieldData(CInt(txtRemark1.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark2.Tag)) Then
                        txtRemark2.Text = mdicFieldData(CInt(txtRemark2.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark3.Tag)) Then
                        txtRemark3.Text = mdicFieldData(CInt(txtRemark3.Tag))
                    End If
                End If
                objCoyInfoField = Nothing
            End If

            iOwnerRefId = objCoyField4.GetOwnerRefId
            If iOwnerRefId > 0 Then
                cboAllocations.SelectedValue = iOwnerRefId : cboAllocations.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAddEditCoyField4_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCoyField4 = New clsassess_coyfield4_master
        objCoyOwner = New clsassess_coyowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            Call Set_Form_Information()
            If menAction = enAction.EDIT_ONE Then
                objCoyField4._Coyfield4unkid = mintCoyField4Unkid
                cboAllocations.Enabled = False
                'cboFieldValue3.Enabled = False : objbtnSearchField3.Enabled = False
            End If
            mdtOwner = objCoyOwner.Get_Data(mintCoyField4Unkid, enWeight_Types.WEIGHT_FIELD4)
            Call FillCombo()
            Call Fill_Data()
            Call GetValue()
            If mintParentId > 0 Then
                cboFieldValue3.SelectedValue = mintParentId
                'cboFieldValue3.Enabled = False : objbtnSearchField3.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField4_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAddEditCoyField4_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCoyField4 = Nothing : objCoyOwner = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddEditCoyField4_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_coyfield4_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_coyfield4_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

            Me.Text = Language.getMessage(mstrModuleName, 10, "Add/Edit Company") & " " & objFieldMaster._Field4_Caption & " " & _
                      Language.getMessage(mstrModuleName, 11, "Information")

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iblnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objCoyField4.Update(mdtOwner, mdicFieldData)
            Else
                iblnFlag = objCoyField4.Insert(mdtOwner, mdicFieldData)
            End If
            If iblnFlag = False Then
                If objCoyField4._Message <> "" Then
                    eZeeMsgBox.Show(objCoyField4._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals."), enMsgBoxStyle.Information)
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Company Goals are saved successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    objCoyField4 = New clsassess_coyfield4_master
                    objCoyOwner = New clsassess_coyowner_tran
                    mdtOwner.Rows.Clear()
                    objchkAll.Checked = False
                    RemoveHandler cboFieldValue3.SelectedIndexChanged, AddressOf cboFieldValue3_SelectedIndexChanged
                    Call GetValue()
                    txtRemark1.Text = "" : txtRemark2.Text = "" : txtRemark3.Text = ""
                    If mintParentId > 0 Then
                        cboFieldValue3.SelectedValue = mintParentId
                    End If
                    AddHandler cboFieldValue3.SelectedIndexChanged, AddressOf cboFieldValue3_SelectedIndexChanged
                Else
                    Call btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField3.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboFieldValue3.ValueMember
                .DisplayMember = cboFieldValue3.DisplayMember
                .DataSource = CType(cboFieldValue3.DataSource, DataTable)
                If .DisplayDialog Then
                    cboFieldValue3.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField3_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            objcolhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
                lvFoundItem.EnsureVisible()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = objchkAll.Checked
                Call GoalOwnerOperation(CInt(LItem.Tag), LItem.Checked)
            Next
            If iOwnerRefId <= 0 Then
                If lvAllocation.CheckedItems.Count <= 0 Then
                    cboAllocations.Enabled = True
                Else
                    cboAllocations.Enabled = False
                End If
            End If

            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                cboAllocations.Enabled = True
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                cboAllocations.Enabled = False
                objchkAll.CheckState = CheckState.Checked
            End If
            Call GoalOwnerOperation(CInt(e.Item.Tag), e.Item.Checked)
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark1.TextChanged
        Try
            mdicFieldData(CInt(txtRemark1.Tag)) = txtRemark1.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark1_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark2.TextChanged
        Try
            mdicFieldData(CInt(txtRemark2.Tag)) = txtRemark2.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark2_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark3.TextChanged
        Try
            mdicFieldData(CInt(txtRemark3.Tag)) = txtRemark3.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark3_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            mintParentId = CInt(cboFieldValue3.SelectedValue)
            If CInt(cboFieldValue3.SelectedValue) > 0 Then
                Dim objField3 As New clsassess_coyfield3_master
                Dim objField2 As New clsassess_coyfield2_master
                Dim objField1 As New clsassess_coyfield1_master
                Dim objPrd As New clscommom_period_Tran
                objField3._Coyfield3unkid = CInt(cboFieldValue3.SelectedValue)
                objField2._Coyfield2unkid = objField3._Coyfield2unkid
                objField1._Coyfield1unkid = objField2._Coyfield1unkid
                txtFieldValue1.Text = objField1._Field_Data
                txtFieldValue2.Text = objField2._Field_Data
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = mintSelectedPeriodId
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = mintSelectedPeriodId
                'Sohail (21 Aug 2015) -- End
                txtPeriod.Text = objPrd._Period_Name
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing : objPrd = Nothing
                Dim objMapping As New clsAssess_Field_Mapping
                mintLinkedFieldId = objMapping.Get_Map_FieldId(mintSelectedPeriodId)
                If mintLinkedFieldId = mintFieldUnkid Then
                    objpnlData.Enabled = True
                    Call Set_Form_Information()
                Else
                    objtabcRemarks.Enabled = False
                    objpnlData.Enabled = False
                End If
                objMapping = Nothing
            Else
                txtFieldValue1.Text = "" : txtFieldValue2.Text = "" : txtPeriod.Text = ""
                objtabcRemarks.Enabled = False
                objpnlData.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue3.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ToolTip1.Hide(cboFieldValue3) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = cboFieldValue3.GetItemText(cboFieldValue3.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, cboFieldValue3, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblGoalOwner.Text = Language._Object.getCaption(Me.lblGoalOwner.Name, Me.lblGoalOwner.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry,")
            Language.setMessage(mstrModuleName, 2, " is mandatory information. Please provide")
            Language.setMessage(mstrModuleName, 3, " to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Weight is mandatory information. Please provide Weight to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Status is mandatory information. Please select Status to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Goal Owner mandatory information. Please check atleast one Goal Owner to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, problem in saving Company Goals.")
            Language.setMessage(mstrModuleName, 8, "Company Goals are saved successfully.")
            Language.setMessage(mstrModuleName, 9, " is mandatory information. Please select")
            Language.setMessage(mstrModuleName, 10, "Add/Edit Company")
            Language.setMessage(mstrModuleName, 11, "Information")
            Language.setMessage(mstrModuleName, 12, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class