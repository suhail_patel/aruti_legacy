﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCaptionMappingAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCaptionMappingAddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbMapInformationalField = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objradField5 = New System.Windows.Forms.RadioButton
        Me.objradField4 = New System.Windows.Forms.RadioButton
        Me.objradField3 = New System.Windows.Forms.RadioButton
        Me.objradField2 = New System.Windows.Forms.RadioButton
        Me.objradField1 = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.pnlMain.SuspendLayout()
        Me.gbMapInformationalField.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbMapInformationalField)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(409, 239)
        Me.pnlMain.TabIndex = 0
        '
        'gbMapInformationalField
        '
        Me.gbMapInformationalField.BorderColor = System.Drawing.Color.Black
        Me.gbMapInformationalField.Checked = False
        Me.gbMapInformationalField.CollapseAllExceptThis = False
        Me.gbMapInformationalField.CollapsedHoverImage = Nothing
        Me.gbMapInformationalField.CollapsedNormalImage = Nothing
        Me.gbMapInformationalField.CollapsedPressedImage = Nothing
        Me.gbMapInformationalField.CollapseOnLoad = False
        Me.gbMapInformationalField.Controls.Add(Me.lblWeight)
        Me.gbMapInformationalField.Controls.Add(Me.txtWeight)
        Me.gbMapInformationalField.Controls.Add(Me.lblPeriod)
        Me.gbMapInformationalField.Controls.Add(Me.cboPeriod)
        Me.gbMapInformationalField.Controls.Add(Me.objradField5)
        Me.gbMapInformationalField.Controls.Add(Me.objradField4)
        Me.gbMapInformationalField.Controls.Add(Me.objradField3)
        Me.gbMapInformationalField.Controls.Add(Me.objradField2)
        Me.gbMapInformationalField.Controls.Add(Me.objradField1)
        Me.gbMapInformationalField.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMapInformationalField.ExpandedHoverImage = Nothing
        Me.gbMapInformationalField.ExpandedNormalImage = Nothing
        Me.gbMapInformationalField.ExpandedPressedImage = Nothing
        Me.gbMapInformationalField.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMapInformationalField.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMapInformationalField.HeaderHeight = 25
        Me.gbMapInformationalField.HeaderMessage = ""
        Me.gbMapInformationalField.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbMapInformationalField.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMapInformationalField.HeightOnCollapse = 0
        Me.gbMapInformationalField.LeftTextSpace = 0
        Me.gbMapInformationalField.Location = New System.Drawing.Point(0, 0)
        Me.gbMapInformationalField.Name = "gbMapInformationalField"
        Me.gbMapInformationalField.OpenHeight = 300
        Me.gbMapInformationalField.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMapInformationalField.ShowBorder = True
        Me.gbMapInformationalField.ShowCheckBox = False
        Me.gbMapInformationalField.ShowCollapseButton = False
        Me.gbMapInformationalField.ShowDefaultBorderColor = True
        Me.gbMapInformationalField.ShowDownButton = False
        Me.gbMapInformationalField.ShowHeader = True
        Me.gbMapInformationalField.Size = New System.Drawing.Size(409, 184)
        Me.gbMapInformationalField.TabIndex = 1
        Me.gbMapInformationalField.Temp = 0
        Me.gbMapInformationalField.Text = "Map Informational Field"
        Me.gbMapInformationalField.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(49, 17)
        Me.lblPeriod.TabIndex = 478
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(67, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(211, 21)
        Me.cboPeriod.TabIndex = 479
        '
        'objradField5
        '
        Me.objradField5.Enabled = False
        Me.objradField5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objradField5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objradField5.Location = New System.Drawing.Point(12, 155)
        Me.objradField5.Name = "objradField5"
        Me.objradField5.Size = New System.Drawing.Size(385, 17)
        Me.objradField5.TabIndex = 5
        Me.objradField5.UseVisualStyleBackColor = True
        '
        'objradField4
        '
        Me.objradField4.Enabled = False
        Me.objradField4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objradField4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objradField4.Location = New System.Drawing.Point(12, 132)
        Me.objradField4.Name = "objradField4"
        Me.objradField4.Size = New System.Drawing.Size(385, 17)
        Me.objradField4.TabIndex = 4
        Me.objradField4.UseVisualStyleBackColor = True
        '
        'objradField3
        '
        Me.objradField3.Enabled = False
        Me.objradField3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objradField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objradField3.Location = New System.Drawing.Point(12, 109)
        Me.objradField3.Name = "objradField3"
        Me.objradField3.Size = New System.Drawing.Size(385, 17)
        Me.objradField3.TabIndex = 3
        Me.objradField3.UseVisualStyleBackColor = True
        '
        'objradField2
        '
        Me.objradField2.Enabled = False
        Me.objradField2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objradField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objradField2.Location = New System.Drawing.Point(12, 86)
        Me.objradField2.Name = "objradField2"
        Me.objradField2.Size = New System.Drawing.Size(385, 17)
        Me.objradField2.TabIndex = 2
        Me.objradField2.UseVisualStyleBackColor = True
        '
        'objradField1
        '
        Me.objradField1.Enabled = False
        Me.objradField1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objradField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objradField1.Location = New System.Drawing.Point(12, 63)
        Me.objradField1.Name = "objradField1"
        Me.objradField1.Size = New System.Drawing.Size(385, 17)
        Me.objradField1.TabIndex = 1
        Me.objradField1.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 184)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(409, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(203, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(303, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(284, 36)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(50, 17)
        Me.lblWeight.TabIndex = 481
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(340, 34)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(57, 21)
        Me.txtWeight.TabIndex = 482
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmCaptionMappingAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 239)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCaptionMappingAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessment Informational Field Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.gbMapInformationalField.ResumeLayout(False)
        Me.gbMapInformationalField.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbMapInformationalField As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objradField5 As System.Windows.Forms.RadioButton
    Friend WithEvents objradField4 As System.Windows.Forms.RadioButton
    Friend WithEvents objradField3 As System.Windows.Forms.RadioButton
    Friend WithEvents objradField2 As System.Windows.Forms.RadioButton
    Friend WithEvents objradField1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
End Class
